import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/utils/string_util.dart';

import '../../main.dart';

///set-cookie
const HEADER_KEY_SET_COOKIE = "set-cookie";
const HEADER_KEY_REFRESH_TOKEN = "refreshtoken";
const HEADER_KEY_CSRFTOKEN = "csrftoken";
const HEADER_KEY_CSRFTOKEN_EXPIRE = "csrftoken_expire";
const HEADER_KEY_CSRFTOKEN_EXPIRE_KEY = "expires";
const HEADER_KEY_COOKIE = "Cookie";
const HEADER_ACCEPT = "Accept";
const HEADER_CONTENT_TYPE = "Content-Type";
const HEADER_MULTIPART_FORM_DATA = "multipart/form-data; boundary=";
const HEADER_AUTHORIZATION = "Authorization";
const HEADER_APPLICATION = "application/json";
const HEADER_API_VERSION = "X-API-VERSION";
const HEADER_API_TOKEN_CSRF = "X-CSRFTOKEN";
const HEADER_API_COOKIE = HEADER_KEY_COOKIE;
const HEADER_APP_VERSION = "APP-VERSION";
const HEADER_OS_TYPE = "OS-TYPE";
const HEADER_OS_VERSION = "OS-VERSION";
const HEADER_DEVICE_BRAND = "DEVICE-BRAND";
const HEADER_DEVICE_MODEL = "DEVICE_MODEL";
const HEADER_REFERER = "Referer";
const HEADER_X_UID_KEY = "X-UID-KEY";

class ApiHeader {
  static final ApiHeader _apiHeader = ApiHeader._init();

  factory ApiHeader() {
    return _apiHeader;
  }

  late BasicApiHeader _basicApiHeader;
  late PublicApiHeader _publicApiHeader;
  late ProtectedApiHeader _protectedApiHeader;
  late ChatApiHeader _chatApiHeader;
  late KakaoApiHeader _kakaoApiHeader;

  Map<String, String> get basicApiHeader => _basicApiHeader.header;

  Map<String, String> get publicApiHeader => _publicApiHeader.header;

  Map<String, String> get protectedApiHeader => _protectedApiHeader.header;

  Map<String, String> get chatApiHeader => _chatApiHeader.header;

  Map<String, String> get kakaoApiHeader => _kakaoApiHeader.header;

  ApiHeader._init() {
    _basicApiHeader = BasicApiHeader._init();
    _publicApiHeader = PublicApiHeader._init();
    _protectedApiHeader = ProtectedApiHeader._init();
    _chatApiHeader = ChatApiHeader._init();
    _kakaoApiHeader = KakaoApiHeader();
  }

  ///버전체크 및 로그인 시 세팅
  void setTokenCSRF(String value) {
    value = value == null ? '' : value;
    try {
      String cookie = HEADER_KEY_CSRFTOKEN + '=' + value;
      _apiHeader.basicApiHeader[HEADER_API_TOKEN_CSRF] = value;
      _apiHeader.basicApiHeader[HEADER_KEY_COOKIE] = cookie;

      _apiHeader.publicApiHeader[HEADER_API_TOKEN_CSRF] = value;
      _apiHeader.publicApiHeader[HEADER_KEY_COOKIE] = cookie;

      _apiHeader.protectedApiHeader[HEADER_API_TOKEN_CSRF] = value;
      _apiHeader.protectedApiHeader[HEADER_KEY_COOKIE] = cookie;

      _chatApiHeader.chatApiHeader[HEADER_API_TOKEN_CSRF] = value;
      _chatApiHeader.chatApiHeader[HEADER_KEY_COOKIE] = cookie;
    } catch (e) {
      log.e('『GGUMBI』>>> setTokenCSRF : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e, HEADER_KEY_CSRFTOKEN: $HEADER_KEY_CSRFTOKEN, value: $value,');
    }
  }

  ///토큰 갱신시사용
  void setTokenRefresh(String csrfToken, String refreshToken) {
    String value = getTokenRefresh(csrfToken, refreshToken);
    _apiHeader.basicApiHeader[HEADER_KEY_COOKIE] = value;

    _apiHeader.publicApiHeader[HEADER_KEY_COOKIE] = value;

    _apiHeader.protectedApiHeader[HEADER_KEY_COOKIE] = value;

    _chatApiHeader.chatApiHeader[HEADER_KEY_COOKIE] = value;
  }

  ///토큰 갱신시사용
  String getTokenRefresh(String csrfToken, String refreshToken) {
    log.d('『GGUMBI』>>> setTokenRefresh : csrfToken: $csrfToken, refreshToken: $refreshToken, <<< ');
    StringBuffer sb = StringBuffer();
    sb.write(HEADER_KEY_CSRFTOKEN + '=' + csrfToken);
    sb.write('; ' + HEADER_KEY_REFRESH_TOKEN + '=' + refreshToken);
    return sb.toString();
  }

  ///로그인시 토큰
  void setTokenAccess(String accessToken) {
    log.d('『GGUMBI』>>> setTokenAccess : value: $accessToken,  <<< ');
    if (!StringUtils.validateString(accessToken)) {
      return;
    }

    String value = getTokenAccess(accessToken);
    _apiHeader.publicApiHeader[HEADER_AUTHORIZATION] = value;
    _apiHeader.protectedApiHeader[HEADER_AUTHORIZATION] = value;
    _chatApiHeader.chatApiHeader[HEADER_AUTHORIZATION] = value;
  }

  ///로그인시 토큰
  String getTokenAccess(String value) {
    log.d('『GGUMBI』>>> setTokenAccess : value: $value,  <<< ');
    return "Bearer " + value;
  }

  ///로그아웃시 토큰 삭제 (CSRF 토큰은 삭제 하지 않는다.)
  void setTokenClear() {
    _apiHeader.publicApiHeader.remove(HEADER_AUTHORIZATION);
    _apiHeader.protectedApiHeader.remove(HEADER_AUTHORIZATION);
    _chatApiHeader.chatApiHeader.remove(HEADER_AUTHORIZATION);
  }

  ///X-UID-KEY set [apiHelper.requestLogin]시 세팅한다.
  void setUidKey(String value) {
    log.d('『GGUMBI』>>> setUidKey : value: $value,  <<< ');
    _apiHeader.basicApiHeader[HEADER_X_UID_KEY] = value;
    _apiHeader.publicApiHeader[HEADER_X_UID_KEY] = value;
    _apiHeader.protectedApiHeader[HEADER_X_UID_KEY] = value;
    _chatApiHeader.chatApiHeader[HEADER_X_UID_KEY] = value;
  }

  ///appVersion, osType, osVersion, deviceBrand, deviceModel
  void setApiHeaderDevice(String csrfToken, String cookies, String appVersion, String osType, String osVersion, String deviceBrand, String deviceModel) {
    log.d('『GGUMBI』>>> setApiHeaderDevice : {ApiEndPoint.BASE_URL}: ${ApiEndPoint.BASE_URL},  <<< ');
    _apiHeader.basicApiHeader[HEADER_API_TOKEN_CSRF] = csrfToken;
    _apiHeader.basicApiHeader[HEADER_API_COOKIE] = cookies;
    _apiHeader.basicApiHeader[HEADER_APP_VERSION] = appVersion;
    _apiHeader.basicApiHeader[HEADER_OS_TYPE] = osType;
    _apiHeader.basicApiHeader[HEADER_OS_VERSION] = osVersion;
    _apiHeader.basicApiHeader[HEADER_DEVICE_BRAND] = deviceBrand;
    _apiHeader.basicApiHeader[HEADER_DEVICE_MODEL] = deviceModel;
    _apiHeader.basicApiHeader[HEADER_REFERER] = ApiEndPoint.getBaseUrl();

    _apiHeader.publicApiHeader[HEADER_API_TOKEN_CSRF] = _apiHeader.basicApiHeader[HEADER_API_TOKEN_CSRF] ?? '';
    _apiHeader.publicApiHeader[HEADER_API_COOKIE] = _apiHeader.basicApiHeader[HEADER_API_COOKIE] ?? '';
    _apiHeader.publicApiHeader[HEADER_APP_VERSION] = _apiHeader.basicApiHeader[HEADER_APP_VERSION] ?? '';
    _apiHeader.publicApiHeader[HEADER_OS_TYPE] = _apiHeader.basicApiHeader[HEADER_OS_TYPE] ?? '';
    _apiHeader.publicApiHeader[HEADER_OS_VERSION] = _apiHeader.basicApiHeader[HEADER_OS_VERSION] ?? '';
    _apiHeader.publicApiHeader[HEADER_DEVICE_BRAND] = _apiHeader.basicApiHeader[HEADER_DEVICE_BRAND] ?? '';
    _apiHeader.publicApiHeader[HEADER_DEVICE_MODEL] = _apiHeader.basicApiHeader[HEADER_DEVICE_MODEL] ?? '';
    _apiHeader.publicApiHeader[HEADER_REFERER] = _apiHeader.basicApiHeader[HEADER_REFERER] ?? '';

    _apiHeader.protectedApiHeader[HEADER_API_TOKEN_CSRF] = _apiHeader.basicApiHeader[HEADER_API_TOKEN_CSRF] ?? '';
    _apiHeader.protectedApiHeader[HEADER_API_COOKIE] = _apiHeader.basicApiHeader[HEADER_API_COOKIE] ?? '';
    _apiHeader.protectedApiHeader[HEADER_APP_VERSION] = _apiHeader.basicApiHeader[HEADER_APP_VERSION] ?? '';
    _apiHeader.protectedApiHeader[HEADER_OS_TYPE] = _apiHeader.basicApiHeader[HEADER_OS_TYPE] ?? '';
    _apiHeader.protectedApiHeader[HEADER_OS_VERSION] = _apiHeader.basicApiHeader[HEADER_OS_VERSION] ?? '';
    _apiHeader.protectedApiHeader[HEADER_DEVICE_BRAND] = _apiHeader.basicApiHeader[HEADER_DEVICE_BRAND] ?? '';
    _apiHeader.protectedApiHeader[HEADER_DEVICE_MODEL] = _apiHeader.basicApiHeader[HEADER_DEVICE_MODEL] ?? '';
    _apiHeader.protectedApiHeader[HEADER_REFERER] = _apiHeader.basicApiHeader[HEADER_REFERER] ?? '';

    _apiHeader.chatApiHeader[HEADER_API_TOKEN_CSRF] = _apiHeader.basicApiHeader[HEADER_API_TOKEN_CSRF] ?? '';
    _apiHeader.chatApiHeader[HEADER_API_COOKIE] = _apiHeader.basicApiHeader[HEADER_API_COOKIE] ?? '';
    _apiHeader.chatApiHeader[HEADER_APP_VERSION] = _apiHeader.basicApiHeader[HEADER_APP_VERSION] ?? '';
    _apiHeader.chatApiHeader[HEADER_OS_TYPE] = _apiHeader.basicApiHeader[HEADER_OS_TYPE] ?? '';
    _apiHeader.chatApiHeader[HEADER_OS_VERSION] = _apiHeader.basicApiHeader[HEADER_OS_VERSION] ?? '';
    _apiHeader.chatApiHeader[HEADER_DEVICE_BRAND] = _apiHeader.basicApiHeader[HEADER_DEVICE_BRAND] ?? '';
    _apiHeader.chatApiHeader[HEADER_DEVICE_MODEL] = _apiHeader.basicApiHeader[HEADER_DEVICE_MODEL] ?? '';
    _apiHeader.chatApiHeader[HEADER_REFERER] = ApiEndPoint.getBaseChatUrl();
  }
}

class BasicApiHeader {
  BasicApiHeader._init() {
    header;
  }

  Map<String, String> get header => basicHeader;
  static Map<String, String> basicHeader = {HEADER_CONTENT_TYPE: HEADER_APPLICATION};
}

class PublicApiHeader {
  PublicApiHeader._init() {
    header;
  }

  Map<String, String> get header => publicHeader;
  Map<String, String> publicHeader = {HEADER_CONTENT_TYPE: HEADER_APPLICATION};

  PublicApiHeader(String apikey, String apiVersion, String userAgent) {
    publicHeader[HEADER_AUTHORIZATION] = apikey;
  }
}

class FileApiHeader {
  FileApiHeader._init() {
    header;
  }

  Map<String, String> get header => fileHeader;
  Map<String, String> fileHeader = {
    /*HEADER_CONTENT_TYPE: HEADER_MULTIPART_FORM_DATA*/
  }; //헤더 옵션 사양으로 세팅한다.

  FileApiHeader(String apikey, String apiVersion, String userAgent) {
    fileHeader[HEADER_AUTHORIZATION] = apikey;
  }
}

class ProtectedApiHeader {
  ProtectedApiHeader._init() {
    header;
  }

  Map<String, String> get header => protectedHeader;

  Map<String, String> protectedHeader = {HEADER_CONTENT_TYPE: HEADER_APPLICATION};

  ProtectedApiHeader(String apikey, String apiVersion, String userAgent, String token) {
    protectedHeader[HEADER_AUTHORIZATION] = apikey;
    protectedHeader[HEADER_API_VERSION] = apiVersion;
    protectedHeader[HEADER_API_TOKEN_CSRF] = token;
  }
}

class ChatApiHeader {
  ChatApiHeader._init() {
    header;
  }

  Map<String, String> get header => chatApiHeader;
  Map<String, String> chatApiHeader = {HEADER_CONTENT_TYPE: HEADER_APPLICATION};

  ChatApiHeader(String apikey, String apiVersion, String userAgent) {
    chatApiHeader[HEADER_AUTHORIZATION] = apikey;
  }
}

class KakaoApiHeader {
  Map<String, String> get header => kakaoApiHeader;
  Map<String, String> kakaoApiHeader = {HEADER_CONTENT_TYPE: HEADER_APPLICATION};

  KakaoApiHeader() {
    kakaoApiHeader[HEADER_AUTHORIZATION] = 'KakaoAK 8e473207540c0a6d9ecec0de313312fa';
  }
}
