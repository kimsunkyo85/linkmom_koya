enum SERVER_TYPE {
  DEBUG,
  DEBUG_LOG,
  RELEASE,
  DEBUG_LOCAL,
  DEBUG_LOCAL2,
}

class ApiEndPoint {
  ///운영 도메인 - https://api.linkmom.com/
  static var _URL_RELEASE = 'https://api.linkmom.com/';

  ///운영 서버 채팅 - chat.linkmom.com
  static var _URL_CHAT = 'https://chat.linkmom.com/';

  ///운영 서버 MQTT 채팅 - mqtt.linkmom.com
  static var _URL_CHAT_MQTT = 'mqtt.linkmom.com';

  ///개발 서버 MQTT 채팅 - staging.linkmom.com
  static var _URL_CHAT_MQTT_DEBUG = 'staging.linkmom.com';

  ///개발 서버 - https://staging.linkmom.com:8443/
  static var _URL_DEBUG = 'https://staging.linkmom.com:8443/';

  ///개발 서버(로그) - http://192.168.0.62:8000/
  static var _URL_DEBUG_LOG = 'http://192.168.0.62:8000/';

  ///개발 서버 - http://192.168.0.62:8443/
  static var _URL_DEBUG_LOCAL = 'http://192.168.0.62:8443/';

  static var _URL_DEBUG_LOCAL_2 = 'http://172.17.100.2:8000/';

  ///기본 도메인 - https://api.linkmom.com/
  static var _URL_BASE = _URL_RELEASE;

  ///기본 채팅 도메인 - chat.linkmom.com
  static var _URL_BASE_CHAT = _URL_CHAT;

  ///기본 채팅 MQTT 도메인 - mqtt.linkmom.com
  static var _URL_BASE_CHAT_MQTT = _URL_CHAT_MQTT;

  static bool isRelease = true;
  static SERVER_TYPE server_type = SERVER_TYPE.RELEASE;

  static setMode({SERVER_TYPE type = SERVER_TYPE.RELEASE}) {
    server_type = type;
    switch (type) {
      case SERVER_TYPE.RELEASE:
        _URL_BASE = _URL_RELEASE; //운영 서버
        _URL_BASE_CHAT = _URL_CHAT;
        _URL_BASE_CHAT_MQTT = _URL_CHAT_MQTT;
        break;
      case SERVER_TYPE.DEBUG:
        _URL_BASE = _URL_DEBUG; //개발 서버
        _URL_BASE_CHAT = _URL_BASE;
        _URL_BASE_CHAT_MQTT = _URL_CHAT_MQTT_DEBUG;
        break;
      case SERVER_TYPE.DEBUG_LOG:
        _URL_BASE = _URL_DEBUG_LOG; //개발 서버(개인-로그)
        _URL_BASE_CHAT = _URL_BASE;
        _URL_BASE_CHAT_MQTT = _URL_CHAT_MQTT_DEBUG;
        break;
      case SERVER_TYPE.DEBUG_LOCAL:
        _URL_BASE = _URL_DEBUG_LOCAL; //개발 서버(개인)
        _URL_BASE_CHAT = _URL_BASE;
        _URL_BASE_CHAT_MQTT = _URL_CHAT_MQTT_DEBUG;
        break;
      case SERVER_TYPE.DEBUG_LOCAL2:
        _URL_BASE = _URL_DEBUG_LOCAL_2; //개발 서버(개인)
        _URL_BASE_CHAT = _URL_BASE;
        _URL_BASE_CHAT_MQTT = _URL_CHAT_MQTT_DEBUG;
        break;
    }
  }

  static String get BASE_URL => _URL_BASE;

  static String getBaseUrl() {
    return _URL_BASE;
  }

  static String get URL_BASE_CHAT => _URL_BASE_CHAT;

  static String getBaseChatUrl() {
    return _URL_BASE_CHAT;
  }

  static String get URL_BASE_CHAT_MQTT => _URL_BASE_CHAT_MQTT;

  static String getBaseChatMqttUrl() {
    return _URL_BASE_CHAT_MQTT;
  }

  static String KAKAO_CHAT_URL = "http://pf.kakao.com/_sgSxks/chat";


  /// KAKAO Address API -
  static String get EP_KAKAO_API => 'https://dapi.kakao.com/v2';

  /// KAKAO Address API - keyword
  static String get EP_KAKAO_ADDR_KEYWORD => EP_KAKAO_API + "/local/search/keyword.json";

  /// KAKAO Address API -
  static String get EP_KAKAO_ADDR => EP_KAKAO_API + "/local/search/address.json";

  /// KAKAO Address Coordiate API -
  static String get EP_KAKAO_ADDR_COORDINATE => EP_KAKAO_API + "/local/geo/coord2regioncode.json";

  // NICE Checkplus
  static String get CHECKPLUS_URL => "${getBaseUrl()}members/nice/checkplus/";

  static String get CHECKPLUS_SUCCESS_URL => "${getBaseUrl()}members/nice/checkplus_success/";

  static String get EP_SPLASH => getBaseUrl() + "app/splash/";

  ///버전체크 app/version/check/
  static String get EP_VERSION_CHECK => getBaseUrl() + "app/version/check/";

  ///회원가입 members/register/
  static String get EP_REGISTER => getBaseUrl() + "members/app/register/";

  ///FCM 토큰 등록 members/fcm/token/
  static String get EP_FCM_TOKEN => getBaseUrl() + "members/app/fcm/token/";

  ///로그인 members/login/
  static String get EP_LOGIN => getBaseUrl() + "members/login/";

  ///RSA Down members/account/pku/
  static String get EP_PKU => getBaseUrl() + "members/account/pku/";

  ///토큰갱신 members/login/refresh/
  static String get EP_TOKEN_REFRESH => getBaseUrl() + "members/app/login/refresh/";

  ///로그아웃 members/logout/
  static String get EP_LOGOUT => getBaseUrl() + "members/app/logout/";

  ///아이디 중복체크 members/account/exists/
  static String get EP_EXISTS => getBaseUrl() + "members/app/account/exists/";

  ///인증번호 members/account/reqauth/
  static String get EP_REQAUTH => getBaseUrl() + "members/app/account/reqauth/";

  ///아이디찾기 members/account/find/id/
  static String get EP_FIND_ID => getBaseUrl() + "members/app/account/find/id/";

  /// 비밀번호찾기 members/account/find/pw/
  static String get EP_FIND_PW => getBaseUrl() + "members/app/account/find/pw/";

  /// 비밀번호재설정 members/account/reset/pw/
  static String get EP_RESET_PW => getBaseUrl() + "members/account/reset/pw/";

  /// 회원탈퇴 members/account/disable/
  static String get EP_DISABLE => getBaseUrl() + "members/account/disable/";

  /// 환경인증 authcenter/home/state/view/
  static String get EP_AUTH_CENTER_HOME_VIEW => getBaseUrl() + "authcenter/home/state/view/";

  /// 환경인증 authcenter/home/state/save/
  static String get EP_AUTH_CENTER_HOME_SAVE => getBaseUrl() + "authcenter/home/state/save/";

  /// 환경인증 authcenter/home/state/delete/
  static String get EP_AUTH_CENTER_HOME_DELETE => getBaseUrl() + "authcenter/home/state/img/delete/";

  /// 마이페이지(아이정보)가져오기 mypage/child/info/list/
  static String get EP_MYPAGE_CHILD_INFO_LIST => getBaseUrl() + "mypage/child/info/list/";

  /// 마이페이지(아이정보)추가 mypage/child/info/save/
  static String get EP_MYPAGE_CHILD_INFO_SAVE => getBaseUrl() + "mypage/child/info/save/";

  /// 마이페이지(아이정보)업데이트 mypage/child/info/update/9/
  static String get EP_MYPAGE_CHILD_INFO_UPDATE => getBaseUrl() + "mypage/child/info/update/";

  /// 마이페이지(아이정보)삭제 mypage/child/info/delete/
  static String get EP_MYPAGE_CHILD_INFO_DELETE => getBaseUrl() + "mypage/child/info/delete/";

  /// 마이페이지(아이정보-투약의뢰서)가져오기 mypage/child/drug/list/
  static String get EP_MYPAGE_CHILD_DRUG_LIST => getBaseUrl() + "mypage/child/drug/list/";

  /// 마이페이지(아이정보-투약의뢰서)추가 mypage/child/drug/save/
  static String get EP_MYPAGE_CHILD_DRUG_SAVE => getBaseUrl() + "mypage/child/drug/save/";

  /// 마이페이지(아이정보-투약의뢰서)업데이트 mypage/child/drug/update/
  static String get EP_MYPAGE_CHILD_DRUG_UPDATE => getBaseUrl() + "mypage/child/drug/update/";

  /// 마이페이지(아이정보-투약의뢰서)삭제 mypage/child/drug/delete/
  static String get EP_MYPAGE_CHILD_DRUG_DELETE => getBaseUrl() + "mypage/child/drug/delete/";

  /// 성향테스트 survey/list/
  static String get EP_SURVEY_LIST => getBaseUrl() + "survey/list/";

  /// 성향테스트 survey/answer/
  static String get EP_SURVEY_ANSWER => getBaseUrl() + "survey/answer/";

  /// 성향테스트 survey/question/1/
  static String get EP_SURVEY_LIST_NUMBER => getBaseUrl() + "survey/question/";

  /// 돌봄 임시저장 (부모님0, 돌봄이1)
  static String get EP_BOOKING_TEMP_SAVE => getBaseUrl() + "booking/temp/save/";

  /// 돌봄 임시저장 가져오기 (부모님0, 돌봄이1)
  static String get EP_BOOKING_TEMP_VIEW => getBaseUrl() + "booking/temp/view/";

  /// 돌봄 임시저장 삭제  (부모님0, 돌봄이1)
  static String get EP_BOOKING_TEMP_DELETE => getBaseUrl() + "booking/temp/delete/";

  /// 돌봄 신청 저정하기 (부모님0, 돌봄이1)
  static String get EP_BOOKING_CARES_SAVE => getBaseUrl() + "booking/cares/save/";

  /// 돌봄 신청 숫정하기 (부모님0, 돌봄이1)
  static String get EP_BOOKING_CARES_UPDATE => getBaseUrl() + "booking/cares/save/";

  /// 돌봄 신청 삭제 (부모님0, 돌봄이1)
  static String get EP_BOOKING_CARES_DELETE => getBaseUrl() + "booking/cares/delete/";

  /// 맘대디 리스트 조회(돌봄신청)
  static String get EP_BOOKING_CARES_LIST => getBaseUrl() + "booking/cares/list/";

  /// 맘대디 리스트 상세 조회(돌봄신청)
  static String get EP_BOOKING_CARES_VIEW => getBaseUrl() + "booking/cares/view/";

  /// 나의프로필(구직 조회)
  static String get EP_BOOKING_LINKMOM_MYINFO_VIEW => getBaseUrl() + "booking/linkmom/job/myinfo/";

  /// 구직 신청 저정하기
  static String get EP_BOOKING_LINKMOM_SAVE => getBaseUrl() + "booking/linkmom/job/save/";

  /// 링크쌤 리스트 조회(링크쌤 신청)
  static String get EP_BOOKING_LINKMOM_LIST => getBaseUrl() + "booking/linkmom/list/";

  /// 링크쌤 리스트 상세 조회(링크쌤 신청)
  static String get EP_BOOKING_LINKMOM_VIEW => getBaseUrl() + "booking/linkmom/view/";

  ///돌봄장소관리 View
  static String get EP_BOOKING_TEMP_AREA_VIEW => getBaseUrl() + "booking/temp/area/view/";

  ///돌봄장소관리 save
  static String get EP_BOOKING_TEMP_AREA_SAVE => getBaseUrl() + "booking/temp/area/save/";

  ///돌봄장소관리 delete
  static String get EP_BOOKING_TEMP_AREA_DELETE => getBaseUrl() + "booking/temp/area/delete/";

  /// 주소 위치 검색(행정동, 시도, 시군구, 읍면동) url+시도+시군구코드
  static String get EP_AREA_CODE_SEARCH => getBaseUrl() + "geo/area/code/search/";

  /// 나의 정보 가져오기
  static String get EP_MYPAGE_MY_INFO_VIEW => getBaseUrl() + "mypage/myinfo/";

  /// 나의 정보 저장하기
  static String get EP_MYPAGE_MY_INFO_SAVE => getBaseUrl() + "mypage/myinfo/save/";

  /// 나의 정보 프로필 사진 삭제
  static String get EP_MYPAGE_MY_INFO_PHOTO_DELETE => getBaseUrl() + "mypage/myinfo/profile/delete/";

  /// 동네인증 최근 인증목록 가져오기
  static String get EP_AREA_ADDRESS_VIEW => getBaseUrl() + "authcenter/area/addr/view/";

  /// 동네 인증 - 주소 저장하기
  static String get EP_AREA_ADDRESS_SAVE => getBaseUrl() + "authcenter/area/addr/save/";

  /// 동네 인증 여부 판단하기
  static String get EP_AREA_GPS_AUTH => getBaseUrl() + "authcenter/area/gps/auth/";

  /// 동네인증 주소가져오기
  static String get EP_AREA_GPS_VIEW => getBaseUrl() + "authcenter/area/gps/view/";

  /// 기본동네인증 설정하기
  static String get EP_AREA_DEFAULT_SET => getBaseUrl() + "authcenter/area/default/set/";

  /// 카카오 주소 가져오기
  static String get EP_KAKAO_REGIONCODE => "https://dapi.kakao.com/v2/local/geo/coord2regioncode";

  ///돌봄/일반채팅 정보 읽어오기 POST
  static String get EP_CHAT_ROOM_VIEW => getBaseChatUrl() + "chat/chatting/room/view/";

  ///돌봄/일반채팅 시작 PUT
  static String get EP_CHAT_ROOM_INIT => getBaseChatUrl() + "chat/chatting/room/init/";

  ///일반채팅 → 돌봄채팅 PATCH
  static String get EP_CHAT_ROOM_UPDATE => getBaseChatUrl() + "chat/chatting/room/update/";

  ///채팅방 삭제 DELETE
  static String get EP_CHAT_ROOM_DELETE => getBaseChatUrl() + "chat/chatting/room/delete/";

  ///채팅 메시지 수신하기 (+금칙어 처리) PUT
  static String get EP_CHAT_MSG_SEND => getBaseChatUrl() + "chat/chatting/message/send/";

  ///채팅 목록 가져가기 POST
  static String get EP_CHAT_MSG_LIST => getBaseChatUrl() + "chat/chatting/message/list/";

  ///대화목록 전체 (맘대디 / 링크쌤 / 일반채팅) linkmom, momdady, normal POST
  static String get EP_CHAT_MAIN => getBaseChatUrl() + "chat/chatting/main/";

  ///사용자별 ONOFF API PUT
  static String get EP_CHAT_PUSH_ONOFF_STATE => getBaseChatUrl() + "chat/push/onoff/state/";

  /// Account
  static String get EP_MYPAGE_MY_ACCOUNT => getBaseUrl() + "mypage/myaccount/";

  /// Account
  static String get EP_MYPAGE_MY_ACCOUNT_RESET_EMAIL => getBaseUrl() + "mypage/myaccount/reset/email/";

  /// Account
  static String get EP_MYPAGE_MY_ACCOUNT_RESET_PW => getBaseUrl() + "mypage/myaccount/reset/pw/";

  /// Account
  static String get EP_MYPAGE_MY_ACCOUNT_RESET_HP => getBaseUrl() + "mypage/myaccount/reset/hp/";

  /// Survey
  static String get EP_MYPAGE_SURVEY_LIST => getBaseUrl() + "survey/list/1/";

  static String get EP_MYPAGE_SURVEY_LIST_STRESS => getBaseUrl() + "survey/list/2/";

  static String get EP_MYPAGE_SURVEY_LIST_PERSONALITY => getBaseUrl() + "survey/list/3/";

  static String get EP_MYPAGE_SURVEY_LIST_COVID19 => getBaseUrl() + "survey/list/9/";

  /// Survey - questions
  static String get EP_MYPAGE_SURVEY_Q => getBaseUrl() + "survey/question/surveyid/";

  static String get EP_MYPAGE_SURVEY_STRESS_Q => getBaseUrl() + "survey/question/surveytype/2/";

  static String get EP_MYPAGE_SURVEY_PERSONALITY_Q => getBaseUrl() + "survey/question/surveyid/3/";

  static String get EP_MYPAGE_SURVEY_COVID19_Q => getBaseUrl() + "survey/question/surveytype/9/";

  /// Survey - answer
  static String get EP_MYPAGE_SURVEY_ANSWER => getBaseUrl() + "survey/answer/";

  /// Settings
  static String get EP_MYPAGE_PUSH_SETTINGS => getBaseUrl() + "mypage/push/settings/";

  /// authcenter save img
  static String get EP_AUTHCENTER_IMG_SAVE => getBaseUrl() + "authcenter/auth/etcimg/save/";

  /// authcenter criminal
  static String get EP_AUTHCENTER_CRIMINAL_SAVE => getBaseUrl() + "authcenter/criminal/save/";

  /// authcenter deungbon
  static String get EP_AUTHCENTER_DEUNGBOM_SAVE => getBaseUrl() + "authcenter/deungbon/save/";

  /// authcenter main
  static String get EP_AUTHCENTER_MAIN => getBaseUrl() + "authcenter/authmain/";

  /// 관심목록 - 맘대디가 링크쌤을 추가
  static String get EP_CARES_WISH_SAVE => getBaseUrl() + "booking/cares/wish/save/";

  /// 관심목록 - 맘대디가 링크쌤을 삭제
  static String get EP_CARES_WISH_DELETE => getBaseUrl() + "booking/cares/wish/delete/";

  /// 관심목록 - 맘대디가 추가한 링크쌤을 조회
  static String get EP_CARES_WISH_LIST => getBaseUrl() + "booking/cares/wish/list/";

  /// 관심목록 - 링크쌤이 맘대디를 추가
  static String get EP_LINKMOM_WISH_SAVE => getBaseUrl() + "booking/linkmom/wish/save/";

  /// 관심목록 - 링크쌤이 맘대디를 삭제
  static String get EP_LINKMOM_WISH_DELETE => getBaseUrl() + "booking/linkmom/wish/delete/";

  /// 관심목록 - 링크쌤이 추가한 맘대디를 조회
  static String get EP_LINKMOM_WISH_LIST => getBaseUrl() + "booking/linkmom/wish/list/";

  /// 차단 리스트 조회
  static String get EP_USER_BLOCK_LIST => getBaseUrl() + "mypage/block/list/";

  /// 사용자 차단
  static String get EP_USER_BLOCK_SAVE => getBaseUrl() + "mypage/block/save/";

  /// 사용자 차단 해제
  static String get EP_USER_BLOCK_DELETE => getBaseUrl() + "mypage/block/delete/";

  /// 매칭요청 및 수락하기 (계약서 생성 & 싸인)
  static String get EP_MATCHING_CONTRACT_INIT => getBaseUrl() + "matching/contract/init/";

  /// 재매칭요청 및 수락하기 (계약서 생성 & 싸인)
  static String get EP_MATCHING_CONTRACT_RETRY => getBaseUrl() + "matching/contract/retry/";

  /// 매칭요청 매칭취소하기
  static String get EP_MATCHING_CONTRACT_CANCEL => getBaseUrl() + "matching/contract/cancel/";

  /// 매칭요청 거절하기
  static String get EP_MATCHING_CONTRACT_DENY => getBaseUrl() + "matching/contract/deny/";

  /// 서명요청하기 (계약서 작성 후, 상대방 싸인 대기)
  static String get EP_MATCHING_CONTRACT_SIGN => getBaseUrl() + "matching/contract/sign/";

  /// 계약서 확인
  static String get EP_MATCHING_CONTRACT_VIEW => getBaseUrl() + "matching/contract/view/";

  /// 맘대디에게 결제요청하기
  static String get EP_MATCHING_CONTRACT_PAY => getBaseUrl() + "matching/contract/pay/";

  /// 맘대디 신청서 리스트 (내 돌봄신청서 목록(s_date:-1, e_date:-1, status:0))
  static String get EP_MOMDADY_MYCARES_LIST => getBaseUrl() + "matching/momdady/mycares/list/";

  /// 맘대디 매칭 리스트
  static String get EP_MOMDADY_MATCHING_LIST => getBaseUrl() + "matching/momdady/matching/list/";

  /// 맘대디 지원 리스트
  static String get EP_MOMDADY_APPLY_LIST => getBaseUrl() + "matching/momdady/apply/list/";

  /// 맘대디 신청서 상태변경
  static String get EP_MOMDADY_MYCARES_UPDATE => getBaseUrl() + "matching/momdady/mycares/status/update/";

  /// 맘대디 매칭 삭제(구인종료)
  static String get EP_MOMDADY_APPLY_DELETE => getBaseUrl() + "matching/apply/delete/momdady/";

  /// 링크쌤 지원 리스트
  static String get EP_LINKMOM_APPLY_LIST => getBaseUrl() + "matching/linkmom/apply/list/";

  /// 링크쌤 매칭 리스트
  static String get EP_LINKMOM_MATCHING_LIST => getBaseUrl() + "matching/linkmom/matching/list/";

  /// 링크쌤 매칭 삭제
  static String get EP_LINKMOM_APPLY_DELETE => getBaseUrl() + "matching/apply/delete/linkmom/";

  /// 사용자 신고하기
  static String get EP_CS_REPORT => getBaseUrl() + "cs/report/";

  /// 후기 저장
  static String get EP_MYPAGE_REVIEW_SAVE => getBaseUrl() + "mypage/review/save/";

  /// 후기 리스트
  static String get EP_MYPAGE_REVIEW_LIST => getBaseUrl() + "mypage/review/list/";

  /// 후기 보기
  static String get EP_MYPAGE_REVIEW_VIEW => getBaseUrl() + "mypage/review/view/";

  /// nice di check
  static String get EP_EXISTS_DI => getBaseUrl() + "members/app/account/exists/di/";

  /// diary list
  static String get EP_CAREDIARY_LIST => getBaseUrl() + "carediary/list/";

  /// diary
  static String get EP_CAREDIARY_VIEW => getBaseUrl() + "carediary/view/";

  /// diary
  static String get EP_CAREDIARY_FORM => getBaseUrl() + "carediary/form/";

  /// diary
  static String get EP_CAREDIARY_SAVE => getBaseUrl() + "carediary/save/";

  /// diary
  static String get EP_CAREDIARY_UPDATE => getBaseUrl() + "carediary/update/";

  /// diary
  static String get EP_CAREDIARY_CHILDDRUG_VIEW => getBaseUrl() + "carediary/childdrug/view/";

  /// diary
  static String get EP_CAREDIARY_CHILDDRUG_OK => getBaseUrl() + "carediary/childdrug/ok/";

  /// diary
  static String get EP_CAREDIARY_LIKE => getBaseUrl() + "carediary/like/";

  /// diary
  static String get EP_CAREDIARY_REPLY_SAVE => getBaseUrl() + "carediary/reply/save/";

  /// diary
  static String get EP_CAREDIARY_CLAIM_SAVE => getBaseUrl() + "carediary/claim/save/";

  /// diary
  static String get EP_CAREDIARY_CLAIM_VIEW => getBaseUrl() + "carediary/claim/view/";

  /// diary
  static String get EP_CAREDIARY_CLARIM_CANCEL => getBaseUrl() + "carediary/claim/cancel/";

  /// diary
  static String get EP_PENALTY => getBaseUrl() + "mypage/penalty/";

  /// diary
  static String get EP_PENALTY_REMOVE => getBaseUrl() + "mypage/penalty/remove/";

  /// diary
  static String get EP_WORKTIME_LIST => getBaseUrl() + "matching/linkmom/worktime/list/";

  /// diary
  static String get EP_WORKTIME_ALLIM => getBaseUrl() + "matching/linkmom/worktime/allim/";

  /// community list
  static String get EP_COMMUNITY_SHARE_LIST => getBaseUrl() + "community/share/list/";

  /// community view
  static String get EP_COMMUNITY_SHARE_VIEW => getBaseUrl() + "community/share/view/";

  /// community save
  static String get EP_COMMUNITY_SHARE_SAVE => getBaseUrl() + "community/share/save/";

  /// community update
  static String get EP_COMMUNITY_SHARE_UPDATE => getBaseUrl() + "community/share/update/";

  /// community delete
  static String get EP_COMMUNITY_SHARE_DELETE => getBaseUrl() + "community/share/delete/";

  /// community share reply save
  static String get EP_COMMUNITY_SHARE_REPLY_SAVE => getBaseUrl() + "community/share/reply/save/";

  /// community share reply delete
  static String get EP_COMMUNITY_SHARE_REPLY_DELETE => getBaseUrl() + "community/share/reply/delete/";

  /// community share scrap
  static String get EP_COMMUNITY_SHARE_SCRAP => getBaseUrl() + "community/share/scrap/";

  /// community share scrap delete
  static String get EP_COMMUNITY_SHARE_SCRAP_DELETE => getBaseUrl() + "community/share/scrap/delete/";

  /// community share like
  static String get EP_COMMUNITY_SHARE_LIKE => getBaseUrl() + "community/share/like/";

  /// community share like
  static String get EP_COMMUNITY_SHARE_LIKE_DELETE => getBaseUrl() + "community/share/like/delete/";

  /// community event list
  static String get EP_COMMUNITY_EVENT => getBaseUrl() + "community/event/list/";

  /// community event view
  static String get EP_COMMUNITY_EVENT_VIEW => getBaseUrl() + "community/event/view/";

  /// community event reply save
  static String get EP_COMMUNITY_EVENT_REPLY_SAVE => getBaseUrl() + "community/event/reply/save/";

  /// community event reply delete
  static String get EP_COMMUNITY_EVENT_REPLY_DELETE => getBaseUrl() + "community/event/reply/delete/";

  /// community event reply save
  static String get EP_COMMUNITY_RANK_LIST => getBaseUrl() + "community/ranking/list/";

  /// community event reply save
  static String get EP_COMMUNITY_RANK_VIEW => getBaseUrl() + "community/ranking/view/";

  /// cs notice list
  static String get EP_CS_NOTICE_LIST => getBaseUrl() + "cs/notice/list/";

  /// cs faq list
  static String get EP_CS_FAQ_LIST => getBaseUrl() + "cs/faq/list/";

  /// cs service list
  static String get EP_CS_SERVICE_LIST => getBaseUrl() + "cs/service/list/";

  /// cs guide list
  static String get EP_CS_USERGUIDE_LIST => getBaseUrl() + "cs/userguide/list/";

  /// cs terms list
  static String get EP_CS_TERMS_LIST => getBaseUrl() + "cs/terms/list/";

  /// cs terms view
  static String get EP_CS_TEMRS_VIEW => getBaseUrl() + "cs/terms/view/";

  /// cs contact type
  static String get EP_CS_CONTACT_TYPE => getBaseUrl() + "cs/contactus/type/";

  /// cs contact list
  static String get EP_CS_CONTACT_LIST => getBaseUrl() + "cs/contactus/list/";

  /// cs contact save
  static String get EP_CS_CONTACT_SAVE => getBaseUrl() + "cs/contactus/save/";

  /// cs contact delete
  static String get EP_CS_CONTACT_DELETE => getBaseUrl() + "cs/contactus/delete/";

  /// cs contact update
  static String get EP_CS_CONTACT_UPDATE => getBaseUrl() + "cs/contactus/update/";

  ///결제 요청 전
  static String get EP_PAY_INIT => getBaseUrl() + "payments/pay/init/";

  ///캐시/포인트 사전결제
  static String get EP_PAY_PREV_USED => getBaseUrl() + "payments/pay/prev/used/";

  ///캐시/포인트 사전결제 취소
  static String get EP_PAY_PREV_BACK => getBaseUrl() + "payments/pay/prev/back/";

  ///결제요청후 저장
  static String get EP_PAY_SAVE => getBaseUrl() + "payments/pay/save/";

  ///결체취소 요청전
  static String get EP_PAY_CANCEL_INIT => getBaseUrl() + "payments/cancel/init/";

  ///결제취소 요청 저장
  static String get EP_PAY_CANCEL_SAVE => getBaseUrl() + "payments/cancel/save/";

  ///결제취소 요청 저장
  static String get EP_PAY_CANCEL_VIEW => getBaseUrl() + "payments/cancel/view/";

  ///결제취소 소명하기
  static String get EP_PAY_CANCEL_EXPLAIN => getBaseUrl() + "payments/cancel/explain/";

  ///결제취소 소명하기
  static String get EP_PAY_CANCEL_INFO => getBaseUrl() + "payments/cancel/info/";

  ///결제리스트
  static String get EP_PAY_LIST => getBaseUrl() + "payments/pay/list/";

  ///캐시리스트
  static String get EP_PAY_CASH_LIST => getBaseUrl() + "payments/cash/list/";

  ///포인트리스트
  static String get EP_PAY_POINT_LIST => getBaseUrl() + "payments/point/list/";

  /// momdady matching view
  static String get EP_MOMDADY_MATCHING_VIEw => getBaseUrl() + "matching/momdady/matching/view/";

  /// linkmom matching view
  static String get EP_LINKMOM_MATCHING_VIEW => getBaseUrl() + "matching/linkmom/matching/view/";

  /// notification list
  static String get EP_NOTIFICATION_LIST => getBaseUrl() + "allim/";

  /// notification delete
  static String get EP_NOTIFICATION_DELETE => getBaseUrl() + "allim/delete/";

  /// cs contact type
  static String get EP_CS_CONTACT_VIEW => getBaseUrl() + "cs/contactus/view/";

  /// home index
  static String get EP_HOME_INDEX => getBaseUrl() + "index/";

  /// 돌봄일기 정산
  static String get EP_CAREDIARY_PAYMENT => getBaseUrl() + "carediary/payment/";

  /// 홈  Refresh
  static String get EP_INDEX_REFRESH => getBaseUrl() + "index/refresh/";

  /// 결제내역  결제
  static String get EP_PAYMENT_PAY => getBaseUrl() + "payments/pay/list/";

  /// 결제내역 캐시
  static String get EP_PAYMENT_CASH => getBaseUrl() + "payments/cash/list/";

  /// 결제내역 포인트
  static String get EP_PAYMENT_POINT => getBaseUrl() + "payments/point/list/";

  /// 회원탈퇴 확인
  static String get EP_DISABLE_VALIDATE => getBaseUrl() + "members/account/disable/validate/";

  /// 인출내역 리스트
  static String get EP_WITHDRAWAL_LIST => getBaseUrl() + "mypage/withdrawal/list/";

  /// 인출신청
  static String get EP_WITHDRAWAL => getBaseUrl() + "mypage/withdrawal/";

  /// Settings
  static String get EP_MYPAGE_PUSH_SETTINGS_VIEW => getBaseUrl() + "mypage/push/settings/view/";

  /// cs guide view
  static String get EP_CS_USERGUIDE_VIEW => getBaseUrl() + "cs/userguide/view/";

  /// payments cancel agree
  static String get EP_PAYMENTS_CANCEL_AGREE => getBaseUrl() + "payments/cancel/reason/agree/";

  /// Coupon List
  static String get EP_COUPON_LIST => getBaseUrl() + "mypage/coupon/list/";

  /// Coupon View
  static String get EP_COUPON_VIEW => getBaseUrl() + "mypage/coupon/view/";

  /// Auth state
  static String get EP_AUTHCENTER_ETCIMG => getBaseUrl() + "authcenter/auth/etcimg/";

  /// Auth state - deungbon
  static String get EP_AUTHCENTER_DEUNGBON => getBaseUrl() + "authcenter/deungbon/";

  /// Auth state - criminal
  static String get EP_AUTHCENTER_CRIMINAL => getBaseUrl() + "authcenter/criminal/";
}
