import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/addr_keyword_response.dart';
import 'package:linkmom/data/network/models/apply_delete_request.dart';
import 'package:linkmom/data/network/models/area_code_search_request.dart';
import 'package:linkmom/data/network/models/area_code_search_response.dart';
import 'package:linkmom/data/network/models/area_gps_auth_request.dart';
import 'package:linkmom/data/network/models/authcenter_authmain_request.dart';
import 'package:linkmom/data/network/models/authcenter_authmain_response.dart';
import 'package:linkmom/data/network/models/authcenter_criminal_request.dart';
import 'package:linkmom/data/network/models/authcenter_criminal_response.dart';
import 'package:linkmom/data/network/models/authcenter_home_request.dart';
import 'package:linkmom/data/network/models/authcenter_home_response.dart';
import 'package:linkmom/data/network/models/block_delete_response.dart';
import 'package:linkmom/data/network/models/block_list_response.dart';
import 'package:linkmom/data/network/models/block_requests.dart';
import 'package:linkmom/data/network/models/block_save_response.dart';
import 'package:linkmom/data/network/models/care_request.dart';
import 'package:linkmom/data/network/models/care_response.dart';
import 'package:linkmom/data/network/models/care_view_request.dart';
import 'package:linkmom/data/network/models/care_view_response.dart';
import 'package:linkmom/data/network/models/carediary_childdrug_ok_request.dart';
import 'package:linkmom/data/network/models/carediary_claim_cancel_request.dart';
import 'package:linkmom/data/network/models/carediary_claim_save_request.dart';
import 'package:linkmom/data/network/models/carediary_claim_view_request.dart';
import 'package:linkmom/data/network/models/carediary_form_response.dart';
import 'package:linkmom/data/network/models/carediary_like_request.dart';
import 'package:linkmom/data/network/models/carediary_list_request.dart';
import 'package:linkmom/data/network/models/cares_wish_delete_response.dart';
import 'package:linkmom/data/network/models/cares_wish_list_response.dart';
import 'package:linkmom/data/network/models/cares_wish_requests.dart';
import 'package:linkmom/data/network/models/cares_wish_save_response.dart';
import 'package:linkmom/data/network/models/cash_list_response.dart';
import 'package:linkmom/data/network/models/chat_main_response.dart';
import 'package:linkmom/data/network/models/chat_message_list_request.dart';
import 'package:linkmom/data/network/models/chat_message_list_response.dart';
import 'package:linkmom/data/network/models/chat_message_send_request.dart';
import 'package:linkmom/data/network/models/chat_message_send_response.dart';
import 'package:linkmom/data/network/models/chat_push_onoff_state_request.dart';
import 'package:linkmom/data/network/models/chat_push_onoff_state_response.dart';
import 'package:linkmom/data/network/models/chat_room_init_response.dart';
import 'package:linkmom/data/network/models/chat_room_update_request.dart';
import 'package:linkmom/data/network/models/child_drug_delete_request.dart';
import 'package:linkmom/data/network/models/child_drug_delete_response.dart';
import 'package:linkmom/data/network/models/child_drug_request.dart';
import 'package:linkmom/data/network/models/child_info_response.dart';
import 'package:linkmom/data/network/models/cs_contact_list_response.dart';
import 'package:linkmom/data/network/models/cs_contact_save_request.dart';
import 'package:linkmom/data/network/models/cs_contact_save_response.dart';
import 'package:linkmom/data/network/models/cs_faq_request.dart';
import 'package:linkmom/data/network/models/cs_report_request.dart';
import 'package:linkmom/data/network/models/cs_report_response.dart';
import 'package:linkmom/data/network/models/cs_response.dart';
import 'package:linkmom/data/network/models/disable_request.dart';
import 'package:linkmom/data/network/models/disable_validate_request.dart';
import 'package:linkmom/data/network/models/disable_validate_response.dart';
import 'package:linkmom/data/network/models/exists_request.dart';
import 'package:linkmom/data/network/models/fcm_token_request.dart';
import 'package:linkmom/data/network/models/findid_request.dart';
import 'package:linkmom/data/network/models/findid_response.dart';
import 'package:linkmom/data/network/models/findpw_request.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/data/network/models/index_request.dart';
import 'package:linkmom/data/network/models/linkmom_apply_list_request.dart';
import 'package:linkmom/data/network/models/linkmom_apply_list_response.dart';
import 'package:linkmom/data/network/models/linkmom_list_response.dart';
import 'package:linkmom/data/network/models/linkmom_matching_list_response.dart';
import 'package:linkmom/data/network/models/linkmom_myinfo_response.dart';
import 'package:linkmom/data/network/models/linkmom_view_request.dart';
import 'package:linkmom/data/network/models/linkmom_view_response.dart';
import 'package:linkmom/data/network/models/linkmom_wish_delete_response.dart';
import 'package:linkmom/data/network/models/linkmom_wish_list_requests.dart';
import 'package:linkmom/data/network/models/linkmom_wish_list_response.dart';
import 'package:linkmom/data/network/models/linkmom_wish_save_response.dart';
import 'package:linkmom/data/network/models/linkmomd_matching_view_response.dart';
import 'package:linkmom/data/network/models/login_request.dart';
import 'package:linkmom/data/network/models/logout_request.dart';
import 'package:linkmom/data/network/models/matching_deny_response.dart';
import 'package:linkmom/data/network/models/matching_init_request.dart';
import 'package:linkmom/data/network/models/matching_init_response.dart';
import 'package:linkmom/data/network/models/matching_view_request.dart';
import 'package:linkmom/data/network/models/momdady_apply_list_request.dart';
import 'package:linkmom/data/network/models/momdady_matching_list_response.dart';
import 'package:linkmom/data/network/models/momdady_mycares_list_request.dart';
import 'package:linkmom/data/network/models/momdady_mycares_list_response.dart';
import 'package:linkmom/data/network/models/momdady_mycares_status_update_request.dart';
import 'package:linkmom/data/network/models/myaccount_reset_request.dart';
import 'package:linkmom/data/network/models/myaccount_reset_response.dart';
import 'package:linkmom/data/network/models/myaccount_response.dart';
import 'package:linkmom/data/network/models/mypage_myinfo_response.dart';
import 'package:linkmom/data/network/models/notification_delete_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_explain_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_explain_response.dart';
import 'package:linkmom/data/network/models/pay_cancel_init_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_init_response.dart';
import 'package:linkmom/data/network/models/pay_cash_list_request.dart';
import 'package:linkmom/data/network/models/pay_cash_list_response.dart';
import 'package:linkmom/data/network/models/pay_list_response.dart';
import 'package:linkmom/data/network/models/pay_pay_list_request.dart';
import 'package:linkmom/data/network/models/pay_pay_list_response.dart';
import 'package:linkmom/data/network/models/pay_point_list_request.dart';
import 'package:linkmom/data/network/models/pay_point_list_response.dart';
import 'package:linkmom/data/network/models/pay_prev_request.dart';
import 'package:linkmom/data/network/models/pay_save_response.dart';
import 'package:linkmom/data/network/models/refresh_request.dart';
import 'package:linkmom/data/network/models/refresh_response.dart';
import 'package:linkmom/data/network/models/register_di_response.dart';
import 'package:linkmom/data/network/models/register_request.dart';
import 'package:linkmom/data/network/models/reqauth_request.dart';
import 'package:linkmom/data/network/models/resetpw_request.dart';
import 'package:linkmom/data/network/models/review_list_request.dart';
import 'package:linkmom/data/network/models/review_list_response.dart';
import 'package:linkmom/data/network/models/review_save_request.dart';
import 'package:linkmom/data/network/models/review_save_response.dart';
import 'package:linkmom/data/network/models/review_view_request.dart';
import 'package:linkmom/data/network/models/review_view_response.dart';
import 'package:linkmom/data/network/models/settings_request.dart';
import 'package:linkmom/data/network/models/settings_response.dart';
import 'package:linkmom/data/network/models/survey_answer_request.dart';
import 'package:linkmom/data/network/models/survey_answer_response.dart';
import 'package:linkmom/data/network/models/survey_list_request.dart';
import 'package:linkmom/data/network/models/survey_list_response.dart';
import 'package:linkmom/data/network/models/survey_question_request.dart';
import 'package:linkmom/data/network/models/survey_question_response.dart';
import 'package:linkmom/data/network/models/temp_area_request.dart';
import 'package:linkmom/data/network/models/temp_area_response.dart';
import 'package:linkmom/data/network/models/temp_response.dart';
import 'package:linkmom/data/network/models/version_check_response.dart';
import 'package:linkmom/data/network/models/withdrawal_list_response.dart';
import 'package:linkmom/data/network/models/withdrawal_request.dart';
import 'package:linkmom/data/storage/storage_help.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/view/main/mypage/review/review_list_reviewinfo_response.dart';
import 'package:logger/logger.dart';

import '../../main.dart';
import '../../route_name.dart';
import 'api_end_point.dart';
import 'api_header.dart';
import 'models/addr_coordinate_response.dart';
import 'models/addr_search_response.dart';
import 'models/area_address_request.dart';
import 'models/area_address_response.dart';
import 'models/area_default_request.dart';
import 'models/area_gps_auth_response.dart';
import 'models/area_gps_view_request.dart';
import 'models/area_gps_view_response.dart';
import 'models/auth_state_request.dart';
import 'models/auth_state_response.dart';
import 'models/authcenter_home_delete_request.dart';
import 'models/care_list_response.dart';
import 'models/care_update_request.dart';
import 'models/carediary_childdrug_ok_response.dart';
import 'models/carediary_childdrug_view_request.dart';
import 'models/carediary_childdrug_view_response.dart';
import 'models/carediary_claim_cancel_response.dart';
import 'models/carediary_claim_save_response.dart';
import 'models/carediary_claim_view_response.dart';
import 'models/carediary_form_request.dart';
import 'models/carediary_like_response.dart';
import 'models/carediary_list_response.dart';
import 'models/carediary_payment_request.dart';
import 'models/carediary_payment_response.dart';
import 'models/carediary_reply_delete_request.dart';
import 'models/carediary_reply_delete_response.dart';
import 'models/carediary_reply_save_request.dart';
import 'models/carediary_reply_save_response.dart';
import 'models/carediary_save_request.dart';
import 'models/carediary_save_response.dart';
import 'models/carediary_update_request.dart';
import 'models/carediary_update_response.dart';
import 'models/cash_list_request.dart';
import 'models/chat_main_request.dart';
import 'models/chat_room_delete_request.dart';
import 'models/chat_room_delete_response.dart';
import 'models/chat_room_init_request.dart';
import 'models/chat_room_update_response.dart';
import 'models/chat_room_view_request.dart';
import 'models/chat_room_view_response.dart';
import 'models/child_drug_list_request.dart';
import 'models/child_drug_list_response.dart';
import 'models/child_drug_response.dart';
import 'models/child_info_delete_request.dart';
import 'models/child_info_request.dart';
import 'models/common_matching_view_request.dart';
import 'models/community_delete_request.dart';
import 'models/community_delete_response.dart';
import 'models/community_event_list_response.dart';
import 'models/community_event_reply_save_request.dart';
import 'models/community_event_request.dart';
import 'models/community_event_view_request.dart';
import 'models/community_event_view_response.dart';
import 'models/community_like_request.dart';
import 'models/community_like_response.dart';
import 'models/community_list_request.dart';
import 'models/community_list_response.dart';
import 'models/community_rank_request.dart';
import 'models/community_rank_response.dart';
import 'models/community_rank_view_request.dart';
import 'models/community_rank_view_response.dart';
import 'models/community_replay_save_response.dart';
import 'models/community_reply_save_request.dart';
import 'models/community_save_request.dart';
import 'models/community_save_response.dart';
import 'models/community_scrap_request.dart';
import 'models/community_scrap_response.dart';
import 'models/community_update_request.dart';
import 'models/community_update_response.dart';
import 'models/community_view_request.dart';
import 'models/community_view_response.dart';
import 'models/coupon_list_request.dart';
import 'models/coupon_list_responst.dart';
import 'models/coupon_view_request.dart';
import 'models/coupon_view_response.dart';
import 'models/cs_contact_delete_request.dart';
import 'models/cs_contact_update_request.dart';
import 'models/cs_contact_view_request.dart';
import 'models/cs_contact_view_response.dart';
import 'models/cs_guide_request.dart';
import 'models/cs_service_request.dart';
import 'models/cs_terms_response.dart';
import 'models/cs_terms_view_request.dart';
import 'models/cs_terms_view_response.dart';
import 'models/data/matching_list_request_data.dart';
import 'models/diary_view_request.dart';
import 'models/diary_view_response.dart';
import 'models/findpw_response.dart';
import 'models/index_response.dart';
import 'models/linkmom_save_request.dart';
import 'models/login_response.dart';
import 'models/matching_cancel_request.dart';
import 'models/matching_cancel_response.dart';
import 'models/matching_deny_request.dart';
import 'models/matching_pay_request.dart';
import 'models/matching_pay_response.dart';
import 'models/matching_sign_request.dart';
import 'models/matching_sign_response.dart';
import 'models/matching_view_response.dart';
import 'models/message_response.dart';
import 'models/momdady_apply_list_response.dart';
import 'models/momdady_matching_view_response.dart';
import 'models/momdady_mycares_status_update_response.dart';
import 'models/mypage_myinfo_save_request.dart';
import 'models/notification_list_request.dart';
import 'models/notification_list_response.dart';
import 'models/pay_cancel_info_request.dart';
import 'models/pay_cancel_info_response.dart';
import 'models/pay_cancel_save_request.dart';
import 'models/pay_cancel_save_response.dart';
import 'models/pay_cancel_view_request.dart';
import 'models/pay_init_request.dart';
import 'models/pay_init_response.dart';
import 'models/pay_list_request.dart';
import 'models/pay_prev_response.dart';
import 'models/pay_save_request.dart';
import 'models/payments_cancel_agree_request.dart';
import 'models/payments_cancel_agree_response.dart';
import 'models/penalty_remove_request.dart';
import 'models/penalty_remove_response.dart';
import 'models/penalty_response.dart';
import 'models/pku_response.dart';
import 'models/point_list_request.dart';
import 'models/point_list_response.dart';
import 'models/register_response.dart';
import 'models/resetpw_response.dart';
import 'models/temp_request.dart';
import 'models/temp_save_request.dart';
import 'models/withdrawal_list_request.dart';
import 'models/withdrawal_response.dart';
import 'models/worktime_allim_request.dart';
import 'models/worktime_allim_response.dart';
import 'models/worktime_list_response.dart';

class ApiHelper extends ChangeNotifier {
  List<RequestItem> requestList = [
    RequestItem(header: {}),
    RequestItem(header: {}),
  ];

  Dio dio = createDio();

  ///기본 시간 20초
  static int connectTimeout = 20000;

  ///기본 시간 10초
  static int receiveTimeout10 = 10000;

  ///기본 시간 10초
  static int sendTimeout10 = 10000;

  ///시간 20초 (version check, login, download)
  static int receiveTimeout20 = 20000;

  ///시간 20초 (version check, login, download)
  static int sendTimeout20 = 20000;

  ///기본 타이머 Splash, download 20초<br/>
  ///Timeout 10000(10초)<br/>
  ///connectTimeout 10000(10초)<br/>
  ///sendTimeout 10000(10초)<br/>
  static BaseOptions baseOption = BaseOptions(
    baseUrl: ApiEndPoint.BASE_URL,
    // receiveTimeout: 12000,
    // connectTimeout: 12000,
    // sendTimeout: 12000,
    connectTimeout: connectTimeout,
    receiveTimeout: receiveTimeout10,
    sendTimeout: sendTimeout10,
    validateStatus: Commons.isDebugMode ? (status) => true : null,
  );

  ApiHelper_init() {
    dio.options.contentType = HEADER_APPLICATION;
  }

  static bool isDlg = false;

  static Dio createDio() {
    var dio = Dio(baseOption);

    // dio.interceptors.addAll({
    //   AppInterceptors(dio),
    // });
    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (options, handler) async {
        log.i({
          '--- TITLE    ': '--------------- CALL PAGE RESPONSE START onRequest ---------------',
          '--- M.T Name ': 'onRequest',
          '--- headers ': options.headers,
          '--- path ': options.path,
          '--- method ': options.method,
          '--- data ': options.data is FormData ? (options.data as FormData).fields : options.data,
          '--- file ': options.data is FormData ? (options.data as FormData).files.toList().toString() : '',
          '--- END      ': '=================================================================',
        });
        return handler.next(options);
      },
      onResponse: (response, handler) async {
        log.i({
          '--- TITLE    ': '--------------- CALL PAGE RESPONSE END onResponse ---------------',
          '--- M.T Name ': 'onResponse',
          '--- statusCode ': response.statusCode,
          '--- realUri ': response.realUri,
          '--- statusMessage ': response.statusMessage,
          '--- response ': response.data,
          '--- END      ': '=================================================================',
        });

        //200, 201, 205, 250까지 data-> msg 확인 나머지 에러 처리
        if (response.statusCode! <= KEY_250_RESOLVE) {
          try {
            HeaderResponse headerResponse = HeaderResponse.fromJson(response.data);
            // log.d('『GGUMBI』>>> createDio : response token 1 : $headerResponse, [${response.realUri}], <<< ');
            if (headerResponse.getCode() == KEY_1402_UNAUTHORIZED) {
              // log.d('『GGUMBI』>>> createDio : response token 1-1 : $headerResponse, [${response.realUri}], <<< ');
              return handler.next(response);
            } else if (headerResponse.getCode() == KEY_401_UNAUTHORIZED || headerResponse.getCode() == KEY_1401_UNAUTHORIZED) {
              // log.d('『GGUMBI』>>> createDio : response token 2 : $headerResponse, [${response.realUri}],  <<< ');
              // log.d('『GGUMBI』>>> createDio : response token 2-1 : ${response.requestOptions.data}, ${response.requestOptions.extra}, ${response.requestOptions}  <<< ');
              return handler.reject(DioError(requestOptions: response.requestOptions, response: response, type: DioErrorType.response), true);
            } else {
              // log.d('『GGUMBI』>>> createDio : response token 3 : $headerResponse, [${response.realUri}],  <<< ');
              return handler.next(response);
            }
          } catch (e) {
            Logger logger = Logger(
              // level: level,
              printer: PrettyPrinter(
                  methodCount: 1,
                  // number of method calls to be displayed 실체 로그 위치
                  errorMethodCount: 8,
                  // number of method calls if stacktrace is provided
                  lineLength: 100,
                  // width of the output
                  colors: false,
                  // Colorful log messages
                  printEmojis: true,
                  // Print an emoji for each log message
                  printTime: true // Should each log print contain a timestamp
                  ),
              output: FileOutput(),
            );
            logger.e({
              '--- TITLE    ': '--------------- CALL PAGE RESPONSE END onResponse ---------------',
              '--- M.T Name ': 'onResponse',
              '--- statusCode ': response.statusCode,
              '--- requestOptions headers ': response.requestOptions.headers,
              '--- requestOptions data ': response.requestOptions.data,
              '--- requestOptions url ': response.requestOptions.path,
              '--- response ': response.data,
              '--- END      ': '=================================================================',
            });
          }

          // handler.next(response);
        } else {
          return handler.reject(DioError(requestOptions: response.requestOptions, response: response, type: DioErrorType.response), true);
        }
      },
      onError: (err, handler) async {
        Logger logger = Logger(
          // level: level,
          printer: PrettyPrinter(
              methodCount: 1,
              // number of method calls to be displayed 실체 로그 위치
              errorMethodCount: 8,
              // number of method calls if stacktrace is provided
              lineLength: 100,
              // width of the output
              colors: false,
              // Colorful log messages
              printEmojis: true,
              // Print an emoji for each log message
              printTime: true // Should each log print contain a timestamp
              ),
          output: FileOutput(),
        );
        logger.e({
          '--- TITLE    ': '--------------- CALL PAGE RESPONSE END onResponse ERROR ---------------',
          '--- M.T Name ': 'onError',
          '--- type ': err.type,
          '--- statusCode ': err.response?.statusCode,
          '--- requestOptions headers ': err.requestOptions.headers,
          '--- requestOptions data ': err.requestOptions.data,
          '--- requestOptions url ': err.requestOptions.path,
          '--- message ': err.message,
          '--- response ': err.response,
          '--- stackTrace ': err.stackTrace,
        });

        log.e({
          '--- TITLE    ': '--------------- CALL PAGE RESPONSE END onResponse ERROR ---------------',
          '--- M.T Name ': 'onError',
          '--- type ': err.type,
          '--- statusCode ': err.response?.statusCode,
          '--- requestOptions headers ': err.requestOptions.headers,
          '--- requestOptions data ': err.requestOptions.data,
          '--- requestOptions url ': err.requestOptions.path,
          '--- message ': err.message,
          '--- response ': err.response,
          '--- stackTrace ': err.stackTrace,
        });
        switch (err.type) {
          case DioErrorType.connectTimeout:
            if (Commons.isDebugMode) {
              if (!isDlg) showErrorDlg(callBack: (action) => isDlg = false);
              isDlg = true;
            }
            break;
          case DioErrorType.sendTimeout:
          case DioErrorType.receiveTimeout:
            if (Commons.isDebugMode) {
              if (!isDlg)
                showNormalDlg(
                    message: "시스템에러".tr(),
                    onClickAction: (action) {
                      switch (action) {
                        case DialogAction.yes:
                          isDlg = false;
                          break;
                        default:
                      }
                    });
              isDlg = true;
            }
            break;
          case DioErrorType.response:
            String msg = "시스템에러".tr();
            int code = 0;

            try {
              HeaderResponse response = HeaderResponse.fromJson(err.response!.data);
              code = response.getCode();
            } catch (e) {
              code = err.response!.statusCode!;
            }

            switch (code) {
              case KEY_100_NETWORK:
                return showNetwork();
              case KEY_400_BAD_REQUEST:
                msg = "에러_400".tr();
                break;
              case KEY_401_UNAUTHORIZED:
              case KEY_1401_UNAUTHORIZED:
                if (err.requestOptions.path.contains('/app/') || err.requestOptions.path.contains('/static/files/')) {
                  return handler.next(err);
                }
                // log.d('『GGUMBI』>>> createDio : response token 4 : ${err.response}, [${err.requestOptions.path}],  <<< ');
                RefreshRequest data = RefreshRequest(refreshtoken: auth.user.token_refresh, csrftoken: auth.user.token_csrf);
                // log.d('『GGUMBI』>>> createDio : response token 5-1 : $data,  <<< ');
                apiHeader.setTokenRefresh(data.csrftoken, data.refreshtoken);
                Options options = Options(
                  headers: apiHeader.basicApiHeader,
                  contentType: HEADER_CONTENT_TYPE,
                  receiveTimeout: receiveTimeout10,
                  sendTimeout: sendTimeout10,
                  validateStatus: Commons.isDebugMode ? (status) => true : null,
                );
                await Dio(baseOption).post(ApiEndPoint.EP_TOKEN_REFRESH, data: data, options: options).then((responseToken) async {
                  var responseJson = Commons.returnResponse(responseToken);
                  RefreshResponse responseData = RefreshResponse.fromJson(responseJson);
                  // log.d('『GGUMBI』>>> createDio : response token 5-2 : $responseToken, $responseData,  <<< ');

                  if (responseToken.headers.map[HEADER_KEY_SET_COOKIE] != null) {
                    var cookies = updateCookie(responseToken);
                    responseData.dataCookies = cookies;
                  }

                  if (responseData.getCode() == KEY_SUCCESS) {
                    String? csrfToken = responseData.dataCookies[HEADER_KEY_CSRFTOKEN];
                    String? csrfTokenExpire = responseData.dataCookies[HEADER_KEY_CSRFTOKEN_EXPIRE];

                    ///csrf값이 없으면 기존 값으로 대처한다.
                    if (!StringUtils.validateString(csrfToken)) {
                      csrfToken = auth.user.token_csrf;
                      csrfTokenExpire = auth.user.token_csrf_expire;
                    }
                    await auth.onUpdateUser(
                      auth.user.id,
                      auth.user.username,
                      auth.user.password,
                      auth.user.auto_login,
                      responseData.getData().access,
                      responseData.dataCookies[HEADER_KEY_REFRESH_TOKEN],
                      csrfToken,
                      csrfTokenExpire,
                    );

                    err.requestOptions.headers[HEADER_API_TOKEN_CSRF] = auth.user.token_csrf;
                    err.requestOptions.headers[HEADER_KEY_COOKIE] = apiHeader.getTokenRefresh(auth.user.token_csrf, auth.user.token_csrf);
                    err.requestOptions.headers[HEADER_AUTHORIZATION] = apiHeader.getTokenAccess(auth.user.token_access);

                    final options = new Options(
                      method: err.requestOptions.method,
                      headers: err.requestOptions.headers,
                    );
                    // log.d('『GGUMBI』>>> createDio : response token 6-1 : ${err.requestOptions.headers}, ${err.requestOptions.data}, ${err.requestOptions.queryParameters},  <<< ');
                    await dio.request<dynamic>(err.requestOptions.path, data: err.requestOptions.data, queryParameters: err.requestOptions.queryParameters, options: options).then((responseRequest) {
                      // log.d('『GGUMBI』>>> createDio : response token 6-2 : $responseRequest,  <<< ');
                      return handler.resolve(responseRequest);
                    });
                  } else if (responseData.getCode() == KEY_1401_UNAUTHORIZED) {
                    options.receiveTimeout = receiveTimeout20;
                    options.sendTimeout = sendTimeout20;
                    LoginRequest data = LoginRequest(username: auth.user.username, password: auth.user.password, is_user_linkmom: auth.user.linkmom, is_user_momdady: auth.user.momdaddy);
                    await Dio(baseOption).post(ApiEndPoint.EP_LOGIN, data: data, options: options).then((responseLogin) async {
                      // log.d('『GGUMBI』>>> createDio : response token 7 : $responseLogin,  <<< ');
                      var cookies = updateCookie(responseLogin);

                      var responseJson = Commons.returnResponse(responseLogin);
                      LoginResponse responseData = LoginResponse.fromJson(responseJson);
                      responseData.dataCookies = cookies;

                      if (responseData.getCode() == KEY_SUCCESS) {
                        String? csrfToken = responseData.dataCookies[HEADER_KEY_CSRFTOKEN];
                        String? csrfTokenExpire = responseData.dataCookies[HEADER_KEY_CSRFTOKEN_EXPIRE];

                        ///csrf값이 없으면 기존 값으로 대처한다.
                        if (!StringUtils.validateString(csrfToken)) {
                          csrfToken = auth.user.token_csrf;
                          csrfTokenExpire = auth.user.token_csrf_expire;
                        }
                        // auth.setAutoLogin = true;
                        String key = encryptHelper.crateKey();
                        await auth.onUpdateUser(
                          responseData.getData().user!.id,
                          data.username,
                          data.password,
                          auth.user.auto_login,
                          responseData.getData().access,
                          responseData.dataCookies[HEADER_KEY_REFRESH_TOKEN],
                          csrfToken,
                          csrfTokenExpire,
                          aesKey: key,
                        );
                        apiHeader.setUidKey(key);

                        err.requestOptions.headers[HEADER_API_TOKEN_CSRF] = auth.user.token_csrf;
                        err.requestOptions.headers[HEADER_KEY_COOKIE] = apiHeader.getTokenRefresh(auth.user.token_csrf, auth.user.token_csrf);
                        err.requestOptions.headers[HEADER_AUTHORIZATION] = apiHeader.getTokenAccess(auth.user.token_access);

                        final options = new Options(
                          method: err.requestOptions.method,
                          headers: err.requestOptions.headers,
                        );
                        // log.d('『GGUMBI』>>> createDio : response token 8-1 :  ${err.requestOptions.headers}, ${err.requestOptions.data}, ${err.requestOptions.queryParameters},  <<< ');
                        await dio.request<dynamic>(err.requestOptions.path, data: err.requestOptions.data, queryParameters: err.requestOptions.queryParameters, options: options).then((responseRequest) {
                          // log.d('『GGUMBI』>>> createDio : response token 8-2 : $responseRequest,  <<< ');
                          return handler.resolve(responseRequest);
                        });
                      } else {
                        //2022/01/11 로그인 에러코는 1402로 구분되기 때문에 성공이 아니면 전부 로그인 화면으로 보낸다.
                        showNormalDlg(
                            message: responseData.getMsg(),
                            onClickAction: (action) {
                              switch (action) {
                                case DialogAction.yes:
                                  Commons.pageClear(navigatorKey.currentContext!, routeLogin);
                                  break;
                                default:
                              }
                            });
                      }
                    });
                  }
                });
                return handler.next(err);
              // msg = "에러_401".tr();
              // if (Commons.isDebugMode) {
              //   if (!isDlg)
              //     showNormalDlg(
              //         message: msg,
              //         onClickAction: (action) {
              //           isDlg = false;
              //           //실패시 모든 정보를 클리어한다.
              //           auth.setUserAllClear();
              //           Commons.pageToMain(navigatorKey.currentContext!, routeLogin);
              //         });
              //   isDlg = true;
              // }
              // break;
              case KEY_403_UNAUTHORIZED:
                msg = "에러_403".tr();
                break;
              case KEY_404_NOT_FOUND:
                msg = "에러_404".tr();
                return handler.next(err);
              // break;
              case KEY_405_METHOD_NOT_ALLOWED:
                msg = "에러_405".tr();
                break;
              case KEY_413_REQUEST_ENTITY_TOO_LARGE:
                msg = "에러_413".tr();
                if (!isDlg) showNormalDlg(message: msg, onClickAction: (action) => isDlg = false);
                break;
              case KEY_429_TOO_MANY_REQUESTS:
                msg = "에러_429".tr();
                break;
              case KEY_431_REQUEST_HEADER_FIELDS_TOO_LARGE:
                msg = "에러_431".tr();
                break;
              case KEY_500_INTERNAL_SERVER_ERROR:
              case KEY_501_NOT_IMPLEMENTED:
              case KEY_502_BAD_GATEWAY:
              case KEY_503_SERVICE_UNAVAILABLE:
              case KEY_504_GATEWAY_TIMEOUT:
              case KEY_505_HTTP_VERSION_NOT_SUPPORTED:
              case KEY_506_VARIANT_ALSO_NEGOTIATES:
              case KEY_507_INSUFFICIENT_STORAGE:
              case KEY_508_LOOP_DETECTED:
              case KEY_509_BANDWIDTH_LIMIT_EXCEEDED:
              case KEY_510_NOT_EXTENDED:
              case KEY_511_NETWORK_AUTHENTICATION_REQUIRED:
              case KEY_2002_NOT:
                msg = "에러_500".tr();
                break;
              default:
                msg = "시스템에러".tr();
                break;
            }
            if (Commons.isDebugMode) {
              if (!isDlg) showErrorDlg(msg: msg, callBack: (action) => isDlg = false);
              isDlg = true;
            }
            break;
          case DioErrorType.cancel:
            // if (!isDlg) showErrorDlg(callBack: (action) => isDlg = false);
            // isDlg = true;
            break;
          case DioErrorType.other:
            // if (!isDlg) showErrorDlg(callBack: (action) => isDlg = false);
            // isDlg = true;
            return handler.resolve(err.response ??
                Response(
                  requestOptions: err.requestOptions,
                  statusMessage: "시스템에러".tr(),
                  statusCode: KEY_ERROR,
                ));
          // throw NoInternetConnectionException(err.requestOptions);
        }
        return handler.next(err);
      },
    ));
    return dio;
  }

  Future<Response<dynamic>> _retry(RequestOptions requestOptions) async {
    final options = new Options(
      method: requestOptions.method,
      headers: requestOptions.headers,
    );
    return dio.request<dynamic>(requestOptions.path, data: requestOptions.data, queryParameters: requestOptions.queryParameters, options: options);
  }

  CancelToken _cancelToken = CancelToken();

  void cancelRequest({String? reason}) {
    // TODO: add cancel token at dio.api();
    // TODO: add baseStateful{dispose();}
    _cancelToken.cancel({
      '--- TITLE    ': '--------------- HTTP REQUEST CANCLED BY USER  ---------------',
      '--- REASON   ': reason ?? 'User manually canceled the request.',
    });
  }

  ///API호출 <br/>
  ///default time 시간 - 10s
  ///splash, download, login - 20s
  Future<Response> apiCall(
    HttpType callType,
    String url,
    Map<String, String> header,
    dynamic data, {
    isFile = false,
    Options? options,
    String contentType = HEADER_CONTENT_TYPE,
    int? receiveTimeout,
    int? sendTimeout,
    Function? callBack,
  }) async {
    Response response;
    bool isConnect = await Commons.isConnect(onClick: () {
      AppSettings.openWIFISettings();
    });

    if (!isConnect) {
      return Response(statusMessage: KEY_100_NETWORK.toString(), statusCode: KEY_SUCCESS, requestOptions: RequestOptions(path: ''));
    }

    receiveTimeout = receiveTimeout ?? receiveTimeout10;
    sendTimeout = sendTimeout ?? sendTimeout10;
    // log.d('『GGUMBI』>>> apiCall : : $receiveTimeout, $sendTimeout, $url <<< ');
    // receiveTimeout = receiveTimeout ?? 100;
    // sendTimeout = sendTimeout ?? 100;
    if (isFile) {
      FormData formData = await data as FormData;
      //이미지 또는 파일 업로드시 시간을 늘려서 사용
      int dataLength = formData.files.length <= 0 ? 1 : formData.files.length;
      baseOption.connectTimeout = connectTimeout * dataLength;
      receiveTimeout = receiveTimeout10 * dataLength;
      Options options = Options(
        headers: header,
        contentType: HEADER_MULTIPART_FORM_DATA + formData.boundary,
        receiveTimeout: receiveTimeout,
        sendTimeout: sendTimeout,
      );

      if (HttpType.put == callType) {
        response = await dio.put(
          url,
          data: formData,
          options: options,
          onReceiveProgress: (count, total) {
            double progressPercent = count / total * 100;
          },
          onSendProgress: (count, total) {
            double progressPercent = count / total * 100;
            if (callBack != null) callBack(progressPercent);
            // if (progressPercent == 100) {
            //   Commons.showToast("완료하였습니다.");
            // }
          },
        );
      } else if (HttpType.patch == callType) {
        response = await dio.patch(url, data: formData, options: options);
      } else if (HttpType.delete == callType) {
        response = await dio.delete(url, data: formData, options: options);
      } else if (HttpType.head == callType) {
        response = await dio.head(url, data: formData, options: options);
      } else {
        response = await dio.post(url, data: formData, options: options);
      }
    } else {
      dynamic _data = data;
      data = jsonEncode(data);
      baseOption.connectTimeout = connectTimeout;
      if (options == null) {
        options = Options(
          headers: header,
          contentType: HEADER_CONTENT_TYPE,
          receiveTimeout: receiveTimeout,
          sendTimeout: sendTimeout,
          validateStatus: Commons.isDebugMode ? (status) => true : null,
        );
      }
      if (HttpType.post == callType) {
        response = await dio.post(url, options: options, data: data);
      } else if (HttpType.get == callType) {
        response = await dio.get(url, options: options, queryParameters: _data);
      } else if (HttpType.put == callType) {
        response = await dio.put(url, options: options, data: data);
      } else if (HttpType.patch == callType) {
        response = await dio.patch(url, options: options, data: data);
      } else if (HttpType.delete == callType) {
        response = await dio.delete(url, options: options, data: data);
      } else if (HttpType.head == callType) {
        response = await dio.head(url, options: options);
      } else {
        response = await dio.post(ApiEndPoint.EP_SPLASH);
      }
    }

    return response;
  }

  ///쿠키 가져오기
  static Map<String, String> updateCookie(Response response) {
    Map<String, String> _cookies = {};
    String? rawCookie = '', csrfCookie = '';
    try {
      ///헤더 확인용
      // response.headers.forEach((name, values) {
      //   log.i('『GGUMBI』>>> updateCookie : name: ${name}, \nvalues: $values, <<< ');
      //   values.asMap().forEach((key, value) {
      //     log.i('『GGUMBI』>>> updateCookie : key: $key, \nvalue: $value,  <<< ');
      //   });
      // });
      // log.d('『GGUMBI』>>> updateCookie : response.headers.map[HEADER_KEY_SET_COOKIE]: ${response.headers.map[HEADER_KEY_SET_COOKIE]?.length},  <<< ');
      rawCookie = response.headers.map[HEADER_KEY_SET_COOKIE].toString();
      csrfCookie = response.headers.map[HEADER_KEY_SET_COOKIE]?.length == 1 ? rawCookie : response.headers.map[HEADER_KEY_SET_COOKIE]![1];

      if (rawCookie != null) {
        _cookies = Commons.parseSetCookieValue(rawCookie);
        log.i('『GGUMBI』>>> updateCookie : _cookies: $_cookies, \nrawCookie: $rawCookie,<<< ');
        DateTime expires = Commons.parseCookieDate(_cookies[HEADER_KEY_CSRFTOKEN_EXPIRE] ?? '');
        log.d('『GGUMBI』>>> updateCookie : expires: $expires,  <<< ');
        StorageHelper().setCsrfExpire(expires);
      }

      log.i({
        'rawCookie': rawCookie.toString(),
        '_cookies': _cookies.toString(),
      });
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'updateCookie',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      final List<String> cookie = (rawCookie ?? '').split(';')[0].split('=');
      _cookies[HEADER_KEY_CSRFTOKEN] = cookie[1];
      _cookies[HEADER_KEY_COOKIE] = HEADER_KEY_CSRFTOKEN + '=' + cookie[1];
      return _cookies;
    }
    return _cookies;
  }

  ///쿠기 정보 가져오기
  Future<Map<String, String>> getCsrfCheck() async {
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_SPLASH, {}, '');
      var _cookies = updateCookie(response);
      return _cookies;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'getCsrfCheck',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return {};
    }
  }

  ///버전 체크 및 쿠키 정보에서 csrf쿠기 가져오기
  Future<VersionCheckResponse> requestVersionCheck() async {
    ///버전 체크 응답 데이터
    VersionCheckResponse responseData = VersionCheckResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_VERSION_CHECK, apiHeader.basicApiHeader, '', receiveTimeout: receiveTimeout20, sendTimeout: sendTimeout20);
      var cookies = updateCookie(response);
      var responseJson = Commons.returnResponse(response);
      responseData = VersionCheckResponse.fromJson(responseJson);
      responseData.dataCookies = cookies;
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestVersionCheck',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///로그인 요청 및 응답
  Future<LoginResponse> requestLogin(LoginRequest data) async {
    ///로그인 데이터
    LoginResponse responseData = LoginResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_LOGIN, apiHeader.basicApiHeader, data, receiveTimeout: receiveTimeout20, sendTimeout: sendTimeout20);
      var cookies = updateCookie(response);

      var responseJson = Commons.returnResponse(response);
      responseData = LoginResponse.fromJson(responseJson);
      responseData.dataCookies = cookies;
      Commons.setLogined = responseData.getCode() == KEY_SUCCESS;
      if (responseData.getCode() == KEY_SUCCESS) {
        String? csrfToken = responseData.dataCookies[HEADER_KEY_CSRFTOKEN];
        String? csrfTokenExpire = responseData.dataCookies[HEADER_KEY_CSRFTOKEN_EXPIRE];

        ///csrf값이 없으면 기존 값으로 대처한다.
        if (!StringUtils.validateString(csrfToken)) {
          csrfToken = auth.user.token_csrf;
          csrfTokenExpire = auth.user.token_csrf_expire;
        }
        // auth.setAutoLogin = true;
        String key = encryptHelper.crateKey();
        await auth.onUpdateUser(
          responseData.getData().user!.id,
          data.username,
          data.password,
          auth.user.auto_login,
          responseData.getData().access,
          responseData.dataCookies[HEADER_KEY_REFRESH_TOKEN],
          csrfToken,
          csrfTokenExpire,
          aesKey: key,
        );
        apiHeader.setUidKey(key);
        String _localPath = await Commons.findLocalPath();
        Commons.downloadPku(_localPath);
      }
      //2022/06/22 Firebase, FingerPush, MQTT init 등록 변경 -> 로그인 후 모든 정보 세팅!!!!
      initManager();
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestLogin',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///Token 갱신 요청 및 응답
  Future<RefreshResponse> requestRefresh(RefreshRequest data) async {
    ///Token 갱신
    RefreshResponse responseData = RefreshResponse();

    try {
      apiHeader.setTokenRefresh(data.csrftoken, data.refreshtoken);
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_TOKEN_REFRESH, apiHeader.basicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = RefreshResponse.fromJson(responseJson);
      if (response.headers.map[HEADER_KEY_SET_COOKIE] != null) {
        var cookies = updateCookie(response);
        responseData.dataCookies = cookies;
      }

      if (responseData.getCode() == KEY_SUCCESS) {
        String? csrfToken = responseData.dataCookies[HEADER_KEY_CSRFTOKEN];
        String? csrfTokenExpire = responseData.dataCookies[HEADER_KEY_CSRFTOKEN_EXPIRE];

        ///csrf값이 없으면 기존 값으로 대처한다.
        if (!StringUtils.validateString(csrfToken)) {
          csrfToken = auth.user.token_csrf;
          csrfTokenExpire = auth.user.token_csrf_expire;
        }
        auth.onUpdateUser(
          auth.user.id,
          auth.user.username,
          auth.user.password,
          auth.user.auto_login,
          responseData.getData().access,
          responseData.dataCookies[HEADER_KEY_REFRESH_TOKEN],
          csrfToken,
          csrfTokenExpire,
        );
      }
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestRefresh',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///FCM 토큰 등록
  Future<HeaderResponse> requestFcmToken(FcmTokenRequest data) async {
    HeaderResponse responseData = HeaderResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_FCM_TOKEN, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = HeaderResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestFcmToken',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///회원가입 요청 및 응답
  Future<RegisterResponse> requestRegister(RegisterRequest data) async {
    ///회원가입 데이터
    RegisterResponse responseData = RegisterResponse();

    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_REGISTER, apiHeader.basicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = RegisterResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestRegister',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///로그아웃 요청 및 응답 body no data
  Future<HeaderResponse> requestLogout(LogoutRequest data) async {
    ///로그아웃
    HeaderResponse responseData = HeaderResponse();

    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_LOGOUT, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = HeaderResponse.fromJson(responseJson);
      responseData.dataCookies = cookies;
      Commons.setLogined = responseData.getCode() != KEY_SUCCESS;
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestLogout',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///아이디 중복 체크 요청 및 응답 body no data
  Future<HeaderResponse> requestExists(ExistsRequest data) async {
    ///아이디 중복 체크
    HeaderResponse responseData = HeaderResponse();

    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_EXISTS, apiHeader.basicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = HeaderResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestExists',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///인증번호 요청 및 응답 body no data
  Future<MessageResponse> requestReqauth(ReqauthRequest data) async {
    ///인증번호
    MessageResponse responseData = MessageResponse(dataHeader: HeaderResponse(), dataList: []);
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_REQAUTH, apiHeader.basicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = MessageResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestReqauth',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///아이디 찾기 요청 및 응답 body no data
  Future<FindIdResponse> requestFindId(FindIdRequest data) async {
    ///아이디 찾기
    FindIdResponse responseData = FindIdResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_FIND_ID, apiHeader.basicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = FindIdResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestFindId',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///비밀번호 찾기 요청 및 응답 body no data
  Future<FindPwResponse> requestFindPw(FindPwRequest data) async {
    ///비밀번호 찾기
    FindPwResponse responseData = FindPwResponse();

    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_FIND_PW, apiHeader.basicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = FindPwResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestFindPw',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///비밀번호 재설정 요청 및 응답 body no data
  Future<ResetPwResponse> requestPwReset(ResetPwRequest data, String token) async {
    ///비밀번호 재설정
    ResetPwResponse responseData = ResetPwResponse();
    try {
      apiHeader.setTokenAccess(token);
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_RESET_PW, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = ResetPwResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestPwReset',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///회원탈퇴 요청 및 응답 body no data
  Future<MessageResponse> requestDisable(DisableRequest data) async {
    ///회원탈퇴
    MessageResponse responseData = MessageResponse(dataHeader: HeaderResponse(), dataList: []);
    try {
      FormData formData = FormData();
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_DISABLE, apiHeader.publicApiHeader, data.toFormData(formData), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = MessageResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDisable',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///환경인증 요청 및 응답 body no data
  Future<AuthCenterHomeResponse> requestHomeStateView(HttpType type, String url) async {
    AuthCenterHomeResponse responseData = AuthCenterHomeResponse();
    try {
      var response = await apiCall(type, url, apiHeader.publicApiHeader, null);

      var responseJson = Commons.returnResponse(response);
      responseData = AuthCenterHomeResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestHomeStateView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  AuthCenterHomeResponse authCenterHomeResponse = AuthCenterHomeResponse();

  ///환경인증 요청 및 응답 body no data
  Future<AuthCenterHomeResponse> requestHomeStateSave(AuthCenterHomeRequest data, HttpType type, String url) async {
    try {
      FormData formData = FormData();
      formData = Commons.setFormDataFiles(data);
      formData = data.toFormData(formData);
      var response = await apiCall(type, url, apiHeader.publicApiHeader, formData, isFile: true);

      var responseJson = Commons.returnResponse(response);
      authCenterHomeResponse = AuthCenterHomeResponse.fromJson(responseJson);

      return authCenterHomeResponse;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestHomeStateSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return authCenterHomeResponse;
    }
  }

  ///환경인증 이미지삭제 및 응답 body no data
  Future<AuthCenterHomeResponse> requestHomeStateDelete(AuthCenterHomeDeleteRequest id, HttpType type, String url) async {
    AuthCenterHomeResponse responseData = AuthCenterHomeResponse();

    try {
      var response = await apiCall(type, url, apiHeader.publicApiHeader, id);

      var responseJson = Commons.returnResponse(response);
      responseData = AuthCenterHomeResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestHomeStateDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///마이페이지 - 아이정보 요청 및 응답 body no data
  Future<ChildInfoResponse> requestMyPageChildInfo(ChildInfoRequest data, HttpType type, String url) async {
    ChildInfoResponse responseData = ChildInfoResponse();

    try {
      var response = await apiCall(type, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = ChildInfoResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMyPageChildInfo',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///마이페이지 - 아이정보 삭제 요청 및 응답 body no data
  Future<HeaderResponse> requestMyPageChildInfoDelete(ChildInfoDeleteRequest data, String url) async {
    HeaderResponse responseData = HeaderResponse();

    try {
      var response = await apiCall(HttpType.delete, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = HeaderResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMyPageChildInfoDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///돌봄 신청 - 임시저장 가져오기 (아이정보, 서비스타입:99이면 전체 조회, depth:99이면 전체 조회)
  Future<TempResponse> requestTempView(TempRequest data) async {
    TempResponse responseData = TempResponse();
    String url = ApiEndPoint.EP_BOOKING_TEMP_VIEW + storageHelper.user_type.index.toString() + '/';
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = TempResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestTempView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///돌봄 신청 - 임시저장 저장하기
  Future<TempResponse> requestTempSave(TempSaveRequest data) async {
    TempResponse responseData = TempResponse();
    String url = ApiEndPoint.EP_BOOKING_TEMP_SAVE + storageHelper.user_type.index.toString() + '/';
    try {
      var response = await apiCall(HttpType.put, url, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = TempResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestTempSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///돌봄 신청 - 임시저장 삭제하기
  Future<TempResponse> requestTempDelete(TempRequest data) async {
    TempResponse responseData = TempResponse();
    String url = ApiEndPoint.EP_BOOKING_TEMP_DELETE + storageHelper.user_type.index.toString() + '/';
    try {
      var response = await apiCall(HttpType.delete, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = TempResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestTempDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///맘대디 리스트(돌봄신청) 조회
  Future<CareListResponse> requestCareList(dynamic data, String url) async {
    CareListResponse responseData = CareListResponse();
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.publicApiHeader, data, isFile: true);

      var responseJson = Commons.returnResponse(response);
      responseData = CareListResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestCareList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///맘대디 리스트 상세(돌봄신청) 조회
  Future<CareViewResponse> requestCareView(CareViewRequest data) async {
    CareViewResponse responseData = CareViewResponse();
    String url = ApiEndPoint.EP_BOOKING_CARES_VIEW;
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = CareViewResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestCareView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///돌봄 신청 - save
  Future<CareResponse> requestCaresSave(CareRequest data) async {
    CareResponse responseData = CareResponse();
    String url = ApiEndPoint.EP_BOOKING_CARES_SAVE;
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = CareResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestCaresSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///돌봄 신청 - update
  Future<CareResponse> requestCaresUpdate(CareUpdateRequest data) async {
    CareResponse responseData = CareResponse();
    String url = ApiEndPoint.EP_BOOKING_CARES_UPDATE;
    try {
      var response = await apiCall(HttpType.put, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = CareResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestCaresUpdate',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///돌봄 신청 - delete
  Future<CareResponse> requestCaresDelete(CareRequest data) async {
    CareResponse responseData = CareResponse();
    String url = ApiEndPoint.EP_BOOKING_CARES_DELETE;
    try {
      var response = await apiCall(HttpType.delete, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = CareResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestCaresDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///구직(프로필 조회)
  Future<LinkMomMyInfoResponse> requestJobMyInfo() async {
    LinkMomMyInfoResponse responseData = LinkMomMyInfoResponse();
    String url = ApiEndPoint.EP_BOOKING_LINKMOM_MYINFO_VIEW;
    try {
      var response = await apiCall(HttpType.patch, url, apiHeader.publicApiHeader, null, receiveTimeout: receiveTimeout20, sendTimeout: sendTimeout20);
      // var response = await apiCall(HttpType.patch, url, apiHeader.publicApiHeader, null);

      var responseJson = Commons.returnResponse(response);
      responseData = LinkMomMyInfoResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestJobMyInfo',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///구직 신청 - save
  Future<LinkMomMyInfoResponse> requestLinkMomSave(LinkMomSaveRequest data) async {
    LinkMomMyInfoResponse responseData = LinkMomMyInfoResponse();
    String url = ApiEndPoint.EP_BOOKING_LINKMOM_SAVE;
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = LinkMomMyInfoResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestLinkMomSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///출발/도착장소 가져오기
  Future<TempAreaResponse> requestTempAreaView() async {
    TempAreaResponse responseData = TempAreaResponse(dataHeader: null, dataList: []);
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_BOOKING_TEMP_AREA_VIEW, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = TempAreaResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'TempAreaResponse',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///출발/도착장소 수정 (장소 번호 (수정시에만 추가, 최초 생성 : Null))
  Future<TempAreaResponse> requestTempAreaSave(TempAreaRequest data) async {
    TempAreaResponse responseData = TempAreaResponse(dataHeader: null, dataList: []);
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_BOOKING_TEMP_AREA_SAVE, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = TempAreaResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustTempAreaSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///출발/도착장소 삭제 (장소 번호 (수정시에만 추가, 최초 생성 : Null))
  Future<HeaderResponse> requestTempAreaDelete(TempAreaRequest data) async {
    HeaderResponse responseData = HeaderResponse();
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_BOOKING_TEMP_AREA_DELETE, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = HeaderResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustTempAreaDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///링크쌤 리스트(돌봄신청) 조회
  Future<LinkMomListResponse> requestLinkMomList(dynamic data, String url) async {
    LinkMomListResponse responseData = LinkMomListResponse();
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.publicApiHeader, data, isFile: true);

      var responseJson = Commons.returnResponse(response);
      responseData = LinkMomListResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestLinkMomList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///링크쌤 리스트 상세(돌봄신청) 조회
  Future<LinkMomViewResponse> requestLinkMomView(LinkMomViewRequest data) async {
    LinkMomViewResponse responseData = LinkMomViewResponse();
    String url = ApiEndPoint.EP_BOOKING_LINKMOM_VIEW;
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = LinkMomViewResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestLinkMomView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///위치검색 (행정동 검색)
  Future<AreaCodeSearchResponse> requestAreaCodeSearch(String code, {AreaCodeSearchRequest? data}) async {
    AreaCodeSearchResponse responseData = AreaCodeSearchResponse();
    String url = ApiEndPoint.EP_AREA_CODE_SEARCH + '$code/';
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = AreaCodeSearchResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestAreaCodeSearch',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///나의 정보 가져오기
  Future<MyPageMyInfoResponse> requestMyInfoView() async {
    MyPageMyInfoResponse responseData = MyPageMyInfoResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_MYPAGE_MY_INFO_VIEW, apiHeader.publicApiHeader, null);

      var responseJson = Commons.returnResponse(response);
      responseData = MyPageMyInfoResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMyInfoView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///나의 정보 저장하기
  Future<MyPageMyInfoResponse> requestMyInfoSave(MyPageMyInfoSaveRequest data) async {
    MyPageMyInfoResponse responseData = MyPageMyInfoResponse();
    try {
      var response;
      response = await apiCall(HttpType.post, ApiEndPoint.EP_MYPAGE_MY_INFO_SAVE, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = MyPageMyInfoResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMyInfoSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///나의 정보 프로필 사진 삭제
  Future<HeaderResponse> requestMyInfoPhotoDelete() async {
    HeaderResponse responseData = HeaderResponse();
    String url = ApiEndPoint.EP_MYPAGE_MY_INFO_PHOTO_DELETE;
    try {
      var response = await apiCall(HttpType.delete, url, apiHeader.publicApiHeader, null);

      var responseJson = Commons.returnResponse(response);
      responseData = HeaderResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMyInfoPhotoDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///나의동네 최근인증 가져오기 - 리스트
  Future<AreaAddressResponse> requestAreaAddressView() async {
    AreaAddressResponse responseData = AreaAddressResponse();
    String url = ApiEndPoint.EP_AREA_ADDRESS_VIEW;
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.publicApiHeader, null);

      var responseJson = Commons.returnResponse(response);
      responseData = AreaAddressResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestAreaAddressView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///카카오 주소 가져오기 ( gps -> address 행정동 코드)
  Future<AreaGpsAuthResponse> requestKakaoAddress(String x, String y) async {
    AreaGpsAuthResponse responseData = AreaGpsAuthResponse();
    String url = ApiEndPoint.EP_KAKAO_REGIONCODE;
    Map<String, String> header = {'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'KakaoAK e2c206154b3d4cf48472cc76ecf2c24f'};
    try {
      // var response = await apiCall(HttpType.get, url, header, {"format": 'json', "x": x, "y": y});
      // response = await dio.get('/test?id=12&name=wendu');
      // print(response.data.toString());
// Optionally the request above could also be done as
      log.d('『GGUMBI』>>> requestKakaoAddress : x : $x , y: $y, <<< ');
      var response = await dio.get(url, options: Options(headers: header), queryParameters: {/*'format': json, */ 'x': x, 'y': y});
      print(response.data.toString());

      log.d('『GGUMBI』>>> requestKakaoAddress : response: $response,  <<< ');

      var responseJson = Commons.returnResponse(response);
      // responseData = AreaGpsAuthResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestKakaoAddress',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///나의동네 인증 여부 판단하기
  Future<AreaGpsAuthResponse> requestAreaGpsAuth(AreaGpsAuthRequest data) async {
    AreaGpsAuthResponse responseData = AreaGpsAuthResponse();
    String url = ApiEndPoint.EP_AREA_GPS_AUTH;
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = AreaGpsAuthResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestAreaGpsAuth',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///나의동네 인증 주소가져오기
  Future<AreaGpsViewResponse> requestAreaGpsView(AreaGpsViewRequest data) async {
    AreaGpsViewResponse responseData = AreaGpsViewResponse();
    String url = ApiEndPoint.EP_AREA_GPS_VIEW;
    try {
      var response = await apiCall(HttpType.put, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = AreaGpsViewResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestAreaGpsAuth',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///나의동네 인증 저장하기
  Future<AreaAddressResponse> requestAreaAddressSave(AreaAddressRequest data) async {
    AreaAddressResponse responseData = AreaAddressResponse();
    String url = ApiEndPoint.EP_AREA_ADDRESS_SAVE;
    try {
      var response = await apiCall(HttpType.put, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = AreaAddressResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestAreaAddressSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///나의동네 인증 기본설정하기
  Future<AreaAddressResponse> requestAreaDefaultSet(AreaDefaultRequest data) async {
    AreaAddressResponse responseData = AreaAddressResponse();
    String url = ApiEndPoint.EP_AREA_DEFAULT_SET;
    try {
      var response = await apiCall(HttpType.patch, url, apiHeader.publicApiHeader, data);

      var responseJson = Commons.returnResponse(response);
      responseData = AreaAddressResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestAreaAddressSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///채팅방 생성
  Future<ChatRoomInitResponse> requestChatRoomInit(ChatRoomInitRequest data) async {
    ChatRoomInitResponse responseData = ChatRoomInitResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_CHAT_ROOM_INIT, apiHeader.chatApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = ChatRoomInitResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestChatRoomInit',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///채팅방 정보 가져오기
  Future<ChatRoomViewResponse> requestChatRoomView(ChatRoomViewRequest data) async {
    ChatRoomViewResponse responseData = ChatRoomViewResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_CHAT_ROOM_VIEW, apiHeader.chatApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = ChatRoomViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestChatRoomView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///채팅방 업데이트(돌봄신청서 불러오기)
  Future<ChatRoomUpdateResponse> requestChatRoomUpdate(ChatRoomUpdateRequest data) async {
    ChatRoomUpdateResponse responseData = ChatRoomUpdateResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_CHAT_ROOM_UPDATE, apiHeader.chatApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = ChatRoomUpdateResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestChatRoomUpdate',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///채팅방 삭제하기
  Future<ChatRoomDeleteResponse> requestChatRoomDelete(ChatRoomDeleteRequest data) async {
    ChatRoomDeleteResponse responseData = ChatRoomDeleteResponse();
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_CHAT_ROOM_DELETE, apiHeader.chatApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = ChatRoomDeleteResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestChatRoomDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///채팅방 리스트가져오기(주체 : linkmom, momdady, normal)
  Future<ChatMainResponse> requestChatMain(ChatMainRequest data, String url) async {
    ChatMainResponse responseData = ChatMainResponse();
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.chatApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = ChatMainResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestChatMain',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  ///채팅 대화 가져오기
  Future<ChatMessageListResponse> requestChatMessageList(ChatMessageListRequest data, String url) async {
    ChatMessageListResponse responseData = ChatMessageListResponse();
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.chatApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = ChatMessageListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestChatMessageList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///채팅 메세지 전송
  Future<ChatMessageSendResponse> requestChatMessageSend(ChatMessageSendRequest data, {Function? callBack}) async {
    ChatMessageSendResponse responseData = ChatMessageSendResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_CHAT_MSG_SEND, apiHeader.chatApiHeader, data.toFormData(), isFile: true, callBack: callBack);
      var responseJson = Commons.returnResponse(response);
      responseData = ChatMessageSendResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestChatMessageSend',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///채팅 mqtt push 상태
  Future<ChatPushOnOffStateResponse> requestChatPushOnOffState(ChatPushOnOffStateRequest data) async {
    ChatPushOnOffStateResponse responseData = ChatPushOnOffStateResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_CHAT_PUSH_ONOFF_STATE, apiHeader.chatApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = ChatPushOnOffStateResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestChatPushOnOffState',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///매칭 요청하기
  Future<MatchingInitResponse> requestMatchingInit(MatchingInitRequest data) async {
    MatchingInitResponse responseData = MatchingInitResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_MATCHING_CONTRACT_INIT, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = MatchingInitResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMatchingInit',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///재매칭 요청하기
  Future<MatchingInitResponse> requestMatchingRetry(MatchingInitRequest data) async {
    MatchingInitResponse responseData = MatchingInitResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_MATCHING_CONTRACT_RETRY, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = MatchingInitResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMatchingRetry',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///계약서조회
  Future<MatchingViewResponse> requestMatchingView(MatchingViewRequest data) async {
    MatchingViewResponse responseData = MatchingViewResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_MATCHING_CONTRACT_VIEW, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = MatchingViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMatchingView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///매칭거절
  Future<MatchingDenyResponse> requestMatchingDeny(MatchingDenyRequest data) async {
    MatchingDenyResponse responseData = MatchingDenyResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_MATCHING_CONTRACT_DENY, apiHeader.publicApiHeader, data.requestData);
      var responseJson = Commons.returnResponse(response);
      responseData = MatchingDenyResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMatchingDeny',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///매칭취소
  Future<MatchingCancelResponse> requestMatchingCancel(MatchingCancelRequest data) async {
    MatchingCancelResponse responseData = MatchingCancelResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_MATCHING_CONTRACT_CANCEL, apiHeader.publicApiHeader, data.requestData);
      var responseJson = Commons.returnResponse(response);
      responseData = MatchingCancelResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMatchingCancel',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///서명요청
  Future<MatchingSignResponse> requestMatchingSign(MatchingSignRequest data) async {
    MatchingSignResponse responseData = MatchingSignResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_MATCHING_CONTRACT_SIGN, apiHeader.publicApiHeader, data.requestData);
      var responseJson = Commons.returnResponse(response);
      responseData = MatchingSignResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMatchingSign',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///결제요청
  Future<MatchingPayResponse> requestMatchingPay(MatchingPayRequest data) async {
    MatchingPayResponse responseData = MatchingPayResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_MATCHING_CONTRACT_PAY, apiHeader.publicApiHeader, data.requestData);
      var responseJson = Commons.returnResponse(response);
      responseData = MatchingPayResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMatchingPay',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///PKU
  Future<PkuResponse> requestPku() async {
    PkuResponse responseData = PkuResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_PKU, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = PkuResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestPku',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<Response> downLoadFile(String url, {Function? callBack(int count, int total)?}) async {
    Response response = await Dio()
        .get(url,
            onReceiveProgress: callBack,
            options: Options(
                responseType: ResponseType.bytes,
                followRedirects: false,
                receiveTimeout: receiveTimeout20,
                sendTimeout: sendTimeout20,
                validateStatus: (status) {
                  return status! < 500;
                }))
        .catchError((e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'downLoadFile',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    });
    return response;
  }

  void showDownloadProgress(received, total, {Function? callBack}) {
    if (total != -1) {
      String? value = (received / total * 100).toStringAsFixed(0) + "%";
      print(value);
      callBack!(value);
    }
  }

  Future<ChildDrugSaveResponse> requestSaveDrug(ChildDrugSaveReqeust data) async {
    ChildDrugSaveResponse responseData = ChildDrugSaveResponse();
    try {
      FormData formData = FormData();
      formData = await Commons.setFormDataFiles3(ChildDrugSaveReqeust.Key_user_signature, data.user_signature, mediaType: 'png');
      formData = data.toFormData(formData);
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_MYPAGE_CHILD_DRUG_SAVE, apiHeader.publicApiHeader, formData, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = ChildDrugSaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestSaveDrug',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<ChildDrugListResponse> requestDrugList(ChildDrugListRequest data) async {
    ChildDrugListResponse responseData = ChildDrugListResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_MYPAGE_CHILD_DRUG_LIST, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = ChildDrugListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDrugList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<ChildDrugDeleteResponse> requestDrugDelete(ChildDrugDeleteRequest data) async {
    ChildDrugDeleteResponse responseData = ChildDrugDeleteResponse();
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_MYPAGE_CHILD_DRUG_DELETE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = ChildDrugDeleteResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDrugDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<SurveyListResponse> requestSurvey(SurveyListRequest data, {SurveyType type = SurveyType.mytype, String? next}) async {
    SurveyListResponse responseData = SurveyListResponse();
    try {
      String url = ApiEndPoint.EP_MYPAGE_SURVEY_LIST;
      switch (type) {
        case SurveyType.covid19:
          url = ApiEndPoint.EP_MYPAGE_SURVEY_LIST_COVID19;
          break;
        case SurveyType.stress:
          url = ApiEndPoint.EP_MYPAGE_SURVEY_LIST_STRESS;
          break;
        case SurveyType.personality:
          url = ApiEndPoint.EP_MYPAGE_SURVEY_LIST_PERSONALITY;
          break;
        default:
          break;
      }
      var response = await apiCall(HttpType.post, next ?? url, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = SurveyListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestSurvey',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<SurveyQuestionResponse> requestSurveyQuestion(SurveyQuestionRequest data, int id) async {
    SurveyQuestionResponse responseData = SurveyQuestionResponse();
    try {
      String url = ApiEndPoint.EP_MYPAGE_SURVEY_Q + '$id/';
      var response = await apiCall(HttpType.post, url, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = SurveyQuestionResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestSurveyQuestion',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<SurveyAnswerResponse> requestSurveyAnswer(SurveyAnswerRequest data) async {
    SurveyAnswerResponse responseData = SurveyAnswerResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_MYPAGE_SURVEY_ANSWER, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = SurveyAnswerResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestSurveyAnswer',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<SurveyQuestionResponse> requestCovidQuestion(SurveyQuestionRequest data) async {
    SurveyQuestionResponse responseData = SurveyQuestionResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_MYPAGE_SURVEY_COVID19_Q, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = SurveyQuestionResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestCovidQuestion',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<SurveyQuestionResponse> requestStressQuestion(SurveyQuestionRequest data) async {
    SurveyQuestionResponse responseData = SurveyQuestionResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_MYPAGE_SURVEY_STRESS_Q, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      SurveyQuestionResponse responseData = SurveyQuestionResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestStressQuestion',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<SettingsResponse> requestSettings(SettingsRequest data) async {
    SettingsResponse responseData = SettingsResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_MYPAGE_PUSH_SETTINGS, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = SettingsResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestSettings',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<AuthCenterAuthMainResponse> requestAuthCenterMain({int? id}) async {
    AuthCenterAuthMainResponse responseData = AuthCenterAuthMainResponse();
    try {
      AuthCenterAuthMainRequest request = AuthCenterAuthMainRequest();
      if (id != null) {
        request.auth_id = id;
      }
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_AUTHCENTER_MAIN, apiHeader.publicApiHeader, request);
      var responseJson = Commons.returnResponse(response);
      responseData = AuthCenterAuthMainResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestAuthCenterMain',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<AuthCenterCriminalResponse> requestSaveCriminal(AuthCenterCriminalRequest data) async {
    AuthCenterCriminalResponse responseData = AuthCenterCriminalResponse();
    try {
      FormData formData = FormData();
      var form = await data.toFormData(formData);
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_AUTHCENTER_CRIMINAL_SAVE, apiHeader.publicApiHeader, form, isFile: true);
      var responseJson = await Commons.returnResponse(response);
      responseData = AuthCenterCriminalResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestSaveCriminal',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<LinkmomWishListResponse> requestLinkmomWishList({String next = ''}) async {
    LinkmomWishListResponse responseData = LinkmomWishListResponse();
    try {
      var response = await apiCall(HttpType.patch, next.isEmpty ? ApiEndPoint.EP_LINKMOM_WISH_LIST : next, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = LinkmomWishListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestLinkmomWishList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CaresWishListResponse> requestCaresWishList({String next = ''}) async {
    CaresWishListResponse responseData = CaresWishListResponse();
    try {
      var response = await apiCall(HttpType.patch, next.isEmpty ? ApiEndPoint.EP_CARES_WISH_LIST : next, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = CaresWishListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestCaresWishList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<LinkmomWishSaveResponse> requestLinkmomWishSave(LinkmomWishSaveRequest data) async {
    LinkmomWishSaveResponse responseData = LinkmomWishSaveResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_LINKMOM_WISH_SAVE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = LinkmomWishSaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestLinkmomWishSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<LinkmomWishDeleteResponse> requestLinkmomWishDelete(LinkmomWishDeleteRequest data) async {
    LinkmomWishDeleteResponse responseData = LinkmomWishDeleteResponse();
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_LINKMOM_WISH_DELETE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = LinkmomWishDeleteResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestLinkmomWishDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CaresWishSaveResponse> requestCaresWishSave(CaresWishSaveRequest data) async {
    CaresWishSaveResponse responseData = CaresWishSaveResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_CARES_WISH_SAVE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CaresWishSaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestCaresWishSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CaresWishDeleteResponse> requestCaresWishDelete(CaresWishDeleteRequest data) async {
    CaresWishDeleteResponse responseData = CaresWishDeleteResponse();
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_CARES_WISH_DELETE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CaresWishDeleteResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestCaresWishDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<MyAccountResponse> requestAccount() async {
    MyAccountResponse responseData = MyAccountResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_MYPAGE_MY_ACCOUNT, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = MyAccountResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestAccount',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///이메일인증 채팅 또는 돌봄관리에서 결제하기클릭시 사용
  Future<MyAccountResponse> requestAccountEmailAuth() async {
    MyAccountResponse responseData = MyAccountResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_MYPAGE_MY_ACCOUNT, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = MyAccountResponse.fromJson(responseJson);
      if (responseData.getCode() == KEY_SUCCESS) {
        auth.account = responseData.getData();
        auth.indexInfo.userinfo.is_auth_email = responseData.getData().is_auth_email;
      }
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestAccountEmailAuth',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<BlockListResponse> requestBlockList() async {
    BlockListResponse responseData = BlockListResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_USER_BLOCK_LIST, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = BlockListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestBlockList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<BlockSaveResponse> requestBlockSave(BlockSaveRequest data) async {
    BlockSaveResponse responseData = BlockSaveResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_USER_BLOCK_SAVE, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = BlockSaveResponse.fromJson(responseJson);
      Commons.showToast(responseData.getMsg());
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestBlockSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<BlockDeleteResponse> requestBlockDelete(BlockDeleteRequest data) async {
    BlockDeleteResponse responseData = BlockDeleteResponse();
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_USER_BLOCK_DELETE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = BlockDeleteResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestBlockDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<BlockListResponse> requestNextBlockList(String url) async {
    BlockListResponse responseData = BlockListResponse();
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = BlockListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestNextBlockList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///내 신청서 & 필터링 (내 돌봄신청 건)
  Future<MomdadyMyCaresListResponse> requestMomdadyMyCaresList(MomdadyMyCaresListRequest data, {String? url}) async {
    MomdadyMyCaresListResponse responseData = MomdadyMyCaresListResponse();
    try {
      FormData formData = FormData();
      data.toFormData(formData);
      FormData.fromMap(data.toJson());
      var response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_MOMDADY_MYCARES_LIST, apiHeader.publicApiHeader, formData, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = MomdadyMyCaresListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMomdadyMyCaresList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///매칭 취소(구인종료)
  Future<MomdadyMyCaresStatusUpdateResponse> requestMyCaresStatusUpdate(MomdadyMyCaresStatusUpdateRequest data) async {
    MomdadyMyCaresStatusUpdateResponse responseData = MomdadyMyCaresStatusUpdateResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_MOMDADY_MYCARES_UPDATE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = MomdadyMyCaresStatusUpdateResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMyCaresStatusUpdate',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///맘대디 나에게지원한 / 내가 지원한 내역
  Future<MomdadyApplyListResponse> requestMomdadyApplyList(MomdadyApplyListRequest data) async {
    MomdadyApplyListResponse responseData = MomdadyApplyListResponse();
    try {
      FormData formData = FormData();
      data.toFormData(formData);
      FormData.fromMap(data.toJson());
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_MOMDADY_APPLY_LIST, apiHeader.publicApiHeader, formData, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = MomdadyApplyListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMomdadyApplyList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///맘대디 돌봄 진행 내역 & 필터링 (매칭 된 건)
  Future<MomdadyMatchingListResponse> requestMomdadyMatchingList(MatchingListRequest data, {String? url}) async {
    MomdadyMatchingListResponse responseData = MomdadyMatchingListResponse();
    try {
      FormData formData = FormData();
      data.toFormData(formData);
      FormData.fromMap(data.toJson());
      var response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_MOMDADY_MATCHING_LIST, apiHeader.publicApiHeader, formData, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = MomdadyMatchingListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMomdadyMatchingList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///매칭취소(구인종료) 맘대디, 링크쌤 같이 사용
  Future<HeaderResponse> requestApplyDelete(ApplyDeleteRequest data) async {
    HeaderResponse responseData = HeaderResponse();
    try {
      String url = ApiEndPoint.EP_MOMDADY_APPLY_DELETE;
      if (storageHelper.user_type == USER_TYPE.link_mom) {
        url = ApiEndPoint.EP_LINKMOM_APPLY_DELETE;
      }
      var response = await apiCall(HttpType.delete, url, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = HeaderResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestApplyDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///신고하기
  Future<CsReportResponse> requestCsReport(CsReportRequest data) async {
    CsReportResponse responseData = CsReportResponse();
    try {
      FormData formData = FormData();
      formData = await Commons.setFormDataFiles2(CsReportRequest.Key_image, data.image!);
      data.toFormData(formData);
      FormData.fromMap(data.toJson());
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_CS_REPORT, apiHeader.publicApiHeader, formData, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = CsReportResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestCsReport',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///링크쌤 지원 내역 & 필터링
  Future<LinkmomApplyListResponse> requestLinkmomApplyList(LinkmomApplyListRequest data, {String? url}) async {
    LinkmomApplyListResponse responseData = LinkmomApplyListResponse();
    try {
      FormData formData = FormData();
      data.toFormData(formData);
      FormData.fromMap(data.toJson());
      var response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_LINKMOM_APPLY_LIST, apiHeader.publicApiHeader, formData, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = LinkmomApplyListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestLinkmomApplyList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///링크쌤 돌봄 진행 내역 & 필터링
  Future<LinkmomMatchingListResponse> requestLinkmomMatchingList(MatchingListRequest data, {String? url}) async {
    LinkmomMatchingListResponse responseData = LinkmomMatchingListResponse();
    try {
      FormData formData = FormData();
      data.toFormData(formData);
      FormData.fromMap(data.toJson());
      var response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_LINKMOM_MATCHING_LIST, apiHeader.publicApiHeader, formData, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = LinkmomMatchingListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestLinkmomMatchingList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  // review save
  Future<ReviewSaveResponse> requestSaveReview(ReviewSaveRequest data) async {
    ReviewSaveResponse responseData = ReviewSaveResponse();
    try {
      FormData formData = FormData();
      data.toFormData(formData);
      FormData.fromMap(data.toJson());
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_MYPAGE_REVIEW_SAVE, apiHeader.publicApiHeader, formData, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = ReviewSaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestSaveReview',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  // review list
  Future<ReviewListResponse> requestReviewList(ReviewListRequest data, {String? url}) async {
    ReviewListResponse responseData = ReviewListResponse();
    try {
      FormData formData = FormData();
      data.toFormData(formData);
      FormData.fromMap(data.toJson());
      var response = await apiCall(HttpType.patch, url ?? ApiEndPoint.EP_MYPAGE_REVIEW_LIST, apiHeader.publicApiHeader, formData, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = ReviewListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestReviewList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  // review view
  Future<ReviewViewResponse> requestReviewView(ReviewViewRequest data) async {
    ReviewViewResponse responseData = ReviewViewResponse();
    try {
      FormData formData = FormData();
      data.toFormData(formData);
      FormData.fromMap(data.toJson());
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_MYPAGE_REVIEW_VIEW, apiHeader.publicApiHeader, formData, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = ReviewViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestReviewView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  // review list
  Future<RevieInfoResponse> requestReviewInfoList(ReviewListRequest data, {String? url}) async {
    RevieInfoResponse responseData = RevieInfoResponse();
    try {
      FormData formData = FormData();
      data.toFormData(formData);
      FormData.fromMap(data.toJson());
      var response = await apiCall(HttpType.patch, url ?? ApiEndPoint.EP_MYPAGE_REVIEW_LIST, apiHeader.publicApiHeader, formData, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = RevieInfoResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestReviewInfoList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<ReviewSaveResponse> requestReviewSave(ReviewSaveRequest data) async {
    ReviewSaveResponse responseData = ReviewSaveResponse();
    try {
      FormData formData = FormData();
      data.toFormData(formData);
      FormData.fromMap(data.toJson());
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_MYPAGE_REVIEW_SAVE, apiHeader.publicApiHeader, formData, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = ReviewSaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestReviewSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<ReviewListResponse> requestNextReivewList(String url) async {
    ReviewListResponse responseData = ReviewListResponse();
    try {
      var response = await apiCall(HttpType.post, url, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = ReviewListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestNextReivewList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<RegisterDiResponse> requestDiExists(ExistsDiRequest data) async {
    ///아이디 중복 체크
    RegisterDiResponse responseData = RegisterDiResponse();

    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_EXISTS_DI, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = RegisterDiResponse.fromJson(responseJson);

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiExists',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<MyAccountResetPwResponse> requestMyaccountResetPw(MyAccountResetPwRequest req) async {
    MyAccountResetPwResponse responseData = MyAccountResetPwResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_MYPAGE_MY_ACCOUNT_RESET_PW, apiHeader.publicApiHeader, req);
      var responseJson = Commons.returnResponse(response);
      responseData = MyAccountResetPwResponse.fromJson(responseJson);
      if (responseData.getCode() == KEY_SUCCESS) {
        auth.onUpdateUser(auth.user.id, auth.user.username, req.pwd1, auth.user.auto_login, auth.user.token_access, auth.user.token_refresh, auth.user.token_csrf, auth.user.token_csrf_expire);
      }
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestMyaccountResetPw',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<MyAccountResetResponse> requestHpChange(MyAccountResetHpRequest req) async {
    MyAccountResetResponse responseData = MyAccountResetResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_MYPAGE_MY_ACCOUNT_RESET_HP, apiHeader.publicApiHeader, req);
      var responseJson = Commons.returnResponse(response);
      responseData = MyAccountResetResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestHpChange',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareDiaryListResponse> requestDiaryList(CareDiaryListReqeust data, {String? url}) async {
    CareDiaryListResponse responseData = CareDiaryListResponse();
    try {
      var response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_CAREDIARY_LIST, apiHeader.publicApiHeader, FormData.fromMap(data.toJson()), isFile: true, receiveTimeout: receiveTimeout20, sendTimeout: sendTimeout20);
      var responseJson = Commons.returnResponse(response);
      responseData = CareDiaryListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiaryList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<DiaryViewResponse> requestDiaryView(DiaryViewRequest data) async {
    DiaryViewResponse responseData = DiaryViewResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_CAREDIARY_VIEW, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = DiaryViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiaryView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareDiaryFormResponse> requestDiaryForm(CareDiaryFormRequest data) async {
    CareDiaryFormResponse responseData = CareDiaryFormResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_CAREDIARY_FORM, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CareDiaryFormResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiaryForm',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareDiarySaveResponse> requestDiarySave(CareDiarySaveRequest data) async {
    CareDiarySaveResponse responseData = CareDiarySaveResponse();
    try {
      FormData form = FormData();
      var response;
      if (data.image != null) {
        form = await Commons.setFormDataFiles2(CareDiarySaveRequest.Key_image, data.image!);
        data.toFormData(form);
        response = await apiCall(HttpType.put, ApiEndPoint.EP_CAREDIARY_SAVE, apiHeader.publicApiHeader, form, isFile: true);
      } else {
        response = await apiCall(HttpType.put, ApiEndPoint.EP_CAREDIARY_SAVE, apiHeader.publicApiHeader, data);
      }
      var responseJson = Commons.returnResponse(response);
      responseData = CareDiarySaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiarySave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareDiaryUpdateResponse> requestDiaryUpdate(CareDiaryUpdateRequest data) async {
    CareDiaryUpdateResponse responseData = CareDiaryUpdateResponse();
    try {
      FormData form = FormData();
      if (data.image != null && data.image!.isNotEmpty) form = await Commons.setFormDataFiles2(CareDiarySaveRequest.Key_image, data.image!);
      data.toFormData(form);
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_CAREDIARY_UPDATE, apiHeader.publicApiHeader, form, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = CareDiaryUpdateResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiaryUpdate',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareDiaryChildDrugViewResponse> requestDiaryDrugView(CareDiaryChildDrugViewRequest data) async {
    CareDiaryChildDrugViewResponse responseData = CareDiaryChildDrugViewResponse();
    try {
      FormData form = FormData();
      await data.toFormData(form);
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_CAREDIARY_CHILDDRUG_VIEW, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CareDiaryChildDrugViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiaryDrugView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareDiaryChildDrugOkResponse> requestDiaryDrugOk(CareDiaryChildDrugOkRequest data) async {
    CareDiaryChildDrugOkResponse responseData = CareDiaryChildDrugOkResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_CAREDIARY_CHILDDRUG_OK, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CareDiaryChildDrugOkResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiaryDrugOk',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareDiaryLikeResponse> requestDiaryLike(CareDiaryLikeRequest data) async {
    CareDiaryLikeResponse responseData = CareDiaryLikeResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_CAREDIARY_LIKE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CareDiaryLikeResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiaryLike',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareDiaryReplySaveResponse> requestDiaryReplySave(CareDiaryReplySaveRequest data) async {
    CareDiaryReplySaveResponse responseData = CareDiaryReplySaveResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_CAREDIARY_REPLY_SAVE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CareDiaryReplySaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiaryReplySave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareDiaryReplyDeleteResponse> requestDiaryReplyDelete(CareDiaryReplyDeleteRequest data) async {
    CareDiaryReplyDeleteResponse responseData = CareDiaryReplyDeleteResponse();
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_CAREDIARY_REPLY_SAVE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CareDiaryReplyDeleteResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiaryReplyDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareDiaryClaimSaveResponse> requestDiaryClaimSave(CareDiaryClaimSaveRequest data) async {
    CareDiaryClaimSaveResponse responseData = CareDiaryClaimSaveResponse();
    try {
      var response;
      if (data.image != null) {
        FormData formData = await Commons.setFormDataFiles2(CommunitySaveRequest.Key_image, data.image!);
        data.toFormData(formData);
        response = await apiCall(HttpType.put, ApiEndPoint.EP_CAREDIARY_CLAIM_SAVE, apiHeader.publicApiHeader, formData, isFile: true);
      } else {
        response = await apiCall(HttpType.put, ApiEndPoint.EP_CAREDIARY_CLAIM_SAVE, apiHeader.publicApiHeader, data);
      }
      var responseJson = Commons.returnResponse(response);
      responseData = CareDiaryClaimSaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiaryClaimSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareDiaryClaimViewResponse> requestDiaryClaimView(CareDiaryClaimViewRequest data) async {
    CareDiaryClaimViewResponse responseData = CareDiaryClaimViewResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_CAREDIARY_CLAIM_VIEW, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CareDiaryClaimViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiaryClaimView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareDiaryClaimCancelResponse> requestDiaryClaimCancel(CareDiaryClaimCancelRequest data) async {
    CareDiaryClaimCancelResponse responseData = CareDiaryClaimCancelResponse();
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_CAREDIARY_CLARIM_CANCEL, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CareDiaryClaimCancelResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiaryClaimCancel',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<PenaltyResponse> requestPenalty({String? url}) async {
    PenaltyResponse responseData = PenaltyResponse();
    try {
      var response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_PENALTY, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = PenaltyResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestPenalty',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<PenaltyRemoveResponse> requestPenaltyRemove(PenaltyRemoveRequest data) async {
    PenaltyRemoveResponse responseData = PenaltyRemoveResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_PENALTY_REMOVE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = PenaltyRemoveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestPenaltyRemove',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<WorkTimeListResponse> requestWorkTimeList({String? url}) async {
    WorkTimeListResponse responseData = WorkTimeListResponse();
    try {
      var response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_WORKTIME_LIST, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = WorkTimeListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestWorkTimeList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<WorkTimeAllimResponse> requestWorkTimeAllim(WorkTimeAllimRequest data) async {
    WorkTimeAllimResponse responseData = WorkTimeAllimResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_WORKTIME_ALLIM, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = WorkTimeAllimResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestWorkTimeAllim',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunityListResponse> reqeustCommunityList(CommunityListRequest data) async {
    CommunityListResponse responseData = CommunityListResponse(dataList: []);
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_COMMUNITY_SHARE_LIST, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunityViewResponse> reqeustCommunityView(CommunityListViewRequest data, {String? next}) async {
    CommunityViewResponse responseData = CommunityViewResponse();
    try {
      var response = await apiCall(HttpType.post, next ?? ApiEndPoint.EP_COMMUNITY_SHARE_VIEW, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunitySaveResponse> reqeustCommunitySave(CommunitySaveRequest data) async {
    CommunitySaveResponse responseData = CommunitySaveResponse();
    try {
      FormData formData = FormData();
      formData = await Commons.setFormDataFiles2(CommunitySaveRequest.Key_image, data.image!);
      data.toFormData(formData);
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_COMMUNITY_SHARE_SAVE, apiHeader.publicApiHeader, formData, isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunitySaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunitySave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunityUpdateResponse> reqeustCommunityUpdate(CommunityUpdateRequest data) async {
    CommunityUpdateResponse responseData = CommunityUpdateResponse();
    try {
      FormData formData;
      var response;
      if (data.image != null) {
        formData = await Commons.setFormDataFiles2(CommunityUpdateRequest.Key_image, data.image!);
        data.toFormData(formData);
        response = await apiCall(HttpType.patch, ApiEndPoint.EP_COMMUNITY_SHARE_UPDATE, apiHeader.publicApiHeader, formData, isFile: true);
      } else {
        response = await apiCall(HttpType.patch, ApiEndPoint.EP_COMMUNITY_SHARE_UPDATE, apiHeader.publicApiHeader, data);
      }
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityUpdateResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityUpdate',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunityDeleteResponse> reqeustCommunityDelete(CommunityDeleteRequest data) async {
    CommunityDeleteResponse responseData = CommunityDeleteResponse();
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_COMMUNITY_SHARE_DELETE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityDeleteResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunityReplySaveResponse> reqeustCommunityReplySave(CommunityReplySaveRequest data) async {
    CommunityReplySaveResponse responseData = CommunityReplySaveResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_COMMUNITY_SHARE_REPLY_SAVE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityReplySaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityReplySave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunityDeleteResponse> reqeustCommunityReplyDel(CommunityDeleteRequest data) async {
    CommunityDeleteResponse responseData = CommunityDeleteResponse();
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_COMMUNITY_SHARE_REPLY_DELETE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityDeleteResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityReplyDel',
        '--- Exception': e,
      });
      return responseData;
    }
  }

  Future<CommunityScrapResponse> reqeustCommunityScrap(CommnunityScrapRequest data) async {
    CommunityScrapResponse responseData = CommunityScrapResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_COMMUNITY_SHARE_SCRAP, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityScrapResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityScrap',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunityDeleteResponse> reqeustCommunityScrapDelete(CommnunityScrapRequest data) async {
    CommunityDeleteResponse responseData = CommunityDeleteResponse();
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_COMMUNITY_SHARE_SCRAP_DELETE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityDeleteResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityScrapDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunityLikeResponse> reqeustCommunityLike(CommunityLikeRequest data) async {
    CommunityLikeResponse responseData = CommunityLikeResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_COMMUNITY_SHARE_LIKE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityLikeResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityLike',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunityDeleteResponse> reqeustCommunityLikeDelete(CommunityLikeRequest data) async {
    CommunityDeleteResponse responseData = CommunityDeleteResponse();
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_COMMUNITY_SHARE_LIKE_DELETE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityDeleteResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityLikeDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunityEventListResponse> reqeustCommunityEvent(CommnunityEventRequest data, {String? url}) async {
    CommunityEventListResponse responseData = CommunityEventListResponse();
    try {
      var response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_COMMUNITY_EVENT, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityEventListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityEvent',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunityEventViewResponse> reqeustCommunityEventView(CommnunityEventViewRequest data, {String? url}) async {
    CommunityEventViewResponse responseData = CommunityEventViewResponse();
    try {
      var response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_COMMUNITY_EVENT_VIEW, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityEventViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityEventView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunityReplySaveResponse> reqeustCommunityEventReplySave(CommunityEventReplySaveRequest data) async {
    CommunityReplySaveResponse responseData = CommunityReplySaveResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_COMMUNITY_EVENT_REPLY_SAVE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityReplySaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityEventReplySave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CommunityDeleteResponse> reqeustCommunityEventReplyDel(CommunityDeleteRequest data) async {
    CommunityDeleteResponse responseData = CommunityDeleteResponse();
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_COMMUNITY_EVENT_REPLY_DELETE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityDeleteResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityEventReplyDel',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<CommunityRankResponse> reqeustCommunityRankList(CommunityRankRequest data, {String? url}) async {
    CommunityRankResponse responseData = CommunityRankResponse();
    try {
      var response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_COMMUNITY_RANK_LIST, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityRankResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityRankList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<CommunityRankViewResponse> reqeustCommunityRankView(CommunityRankViewRequest data) async {
    CommunityRankViewResponse responseData = CommunityRankViewResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_COMMUNITY_RANK_VIEW, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CommunityRankViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCommunityRankView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<CsResponse> reqeustCsNotice() async {
    CsResponse responseData = CsResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_CS_NOTICE_LIST, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = CsResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCsNotice',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<CsResponse> reqeustCsFaq(CsFaqRequest data) async {
    CsResponse responseData = CsResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_CS_FAQ_LIST, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CsResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCsFaq',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<CsResponse> reqeustCsService(CsServiceRequest data) async {
    CsResponse responseData = CsResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_CS_SERVICE_LIST, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CsResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCsService',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<CsResponse> reqeustCsUserGuide(CsGuideRequest data) async {
    CsResponse responseData = CsResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_CS_USERGUIDE_LIST, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CsResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCsUserGuide',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<CsTermsListResponse> reqeustCsTermsList() async {
    CsTermsListResponse responseData = CsTermsListResponse();
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_CS_TERMS_LIST, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = CsTermsListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCsTermsList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<CsTermsViewResponse> reqeustCsTermsView(CsTermsViewRequest data) async {
    CsTermsViewResponse responseData = CsTermsViewResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_CS_TEMRS_VIEW, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CsTermsViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCsTermsView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<CsContactListResponse> reqeustCsContactList() async {
    CsContactListResponse responseData = CsContactListResponse();
    try {
      // var response = await apiCall(HttpType.post, ApiEndPoint.EP_LOGIN + 'enc/', apiHeader.basicApiHeader, data, receiveTimeout: receiveTimeout20, sendTimeout: sendTimeout20);
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_CS_CONTACT_LIST, apiHeader.publicApiHeader, null, receiveTimeout: receiveTimeout20, sendTimeout: sendTimeout20);
      // var cookies = updateCookie(response);
      var responseJson = Commons.returnResponse(response);
      responseData = CsContactListResponse.fromJson(responseJson);
      responseData.dataCookies = cookies;

      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCsContactList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<CsContactSaveResponse> reqeustCsContactSave(CsContactSaveRequest data) async {
    CsContactSaveResponse responseData = CsContactSaveResponse();
    try {
      var response;
      if (data.image != null) {
        FormData formData = await Commons.setFormDataFiles2(CsContactSaveRequest.Key_image, data.image!);
        data.toFomData(formData);
        response = await apiCall(HttpType.put, ApiEndPoint.EP_CS_CONTACT_SAVE, apiHeader.publicApiHeader, formData, isFile: true);
      } else {
        response = await apiCall(HttpType.put, ApiEndPoint.EP_CS_CONTACT_SAVE, apiHeader.publicApiHeader, data);
      }
      var responseJson = Commons.returnResponse(response);
      responseData = CsContactSaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCsContactSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<CsContactViewResponse> reqeustCsContactView(CsContactViewRequest data) async {
    CsContactViewResponse responseData = CsContactViewResponse(dataHeader: HeaderResponse(), data: ContactData(contactusImages: []));
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_CS_CONTACT_VIEW, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CsContactViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCsContactView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<MessageResponse> reqeustCsContactDelete(CsContactDeleteRequest data) async {
    MessageResponse responseData = MessageResponse(dataHeader: HeaderResponse(), dataList: []);
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_CS_CONTACT_DELETE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = MessageResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCsContactDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<CsContactSaveResponse> reqeustCsContactUpdate(CsContactUpdateRequest data) async {
    CsContactSaveResponse responseData = CsContactSaveResponse();
    try {
      var response;
      if (data.image != null) {
        FormData formData = await Commons.setFormDataFiles2(CsContactUpdateRequest.Key_image, data.image!);
        data.toFomData(formData);
        response = await apiCall(HttpType.patch, ApiEndPoint.EP_CS_CONTACT_UPDATE, apiHeader.publicApiHeader, formData, isFile: true);
      } else {
        response = await apiCall(HttpType.patch, ApiEndPoint.EP_CS_CONTACT_UPDATE, apiHeader.publicApiHeader, data);
      }
      var responseJson = Commons.returnResponse(response);
      responseData = CsContactSaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCsContactUpdate',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<LinkmomMatchingViewResponse> reqeustLinkmomMatchingView(CommonMatchingViewRequest data) async {
    late LinkmomMatchingViewResponse responseData = LinkmomMatchingViewResponse(dataHeader: HeaderResponse(), dataList: []);
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_LINKMOM_MATCHING_VIEW, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = LinkmomMatchingViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustLinkmomMatchingView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<MomdadyMatchingViewResponse> reqeustMomdadyMatchingView(CommonMatchingViewRequest data) async {
    late MomdadyMatchingViewResponse responseData = MomdadyMatchingViewResponse(dataHeader: HeaderResponse(), dataList: []);
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_MOMDADY_MATCHING_VIEw, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = MomdadyMatchingViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustMomdadyMatchingView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<NotificationListResponse> reqeustNotificationList(NotificationListRequest? data, {String? url}) async {
    late NotificationListResponse responseData = NotificationListResponse(dataHeader: HeaderResponse(), dataList: []);
    try {
      FormData formData = FormData();
      var response;
      if (data != null) {
        formData = data.toFormData(formData);
        response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_NOTIFICATION_LIST, apiHeader.publicApiHeader, formData, isFile: true);
      } else {
        response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_NOTIFICATION_LIST, apiHeader.publicApiHeader, data);
      }
      var responseJson = Commons.returnResponse(response);
      responseData = NotificationListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustNotificationList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<MessageResponse> reqeustNotificationDelete(NotificationDeleteRequest data) async {
    late MessageResponse responseData = MessageResponse(dataHeader: HeaderResponse(), dataList: []);
    try {
      var response = await apiCall(HttpType.delete, ApiEndPoint.EP_NOTIFICATION_DELETE, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = MessageResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustNotificationDelete',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///결제 요청 전
  Future<PayInitResponse> reqeustPayInit(PayInitRequest data) async {
    PayInitResponse responseData = PayInitResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_PAY_INIT, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = PayInitResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPayInit',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///캐시/포인트 사전결제
  Future<PayRrevResponse> reqeustPayRrevUsed(PayRrevRequest data) async {
    PayRrevResponse responseData = PayRrevResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_PAY_PREV_USED, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = PayRrevResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPayRrevUsed',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///캐시/포인트 사전결제 취소
  Future<PayRrevResponse> reqeustPayRrevBack(PayRrevRequest data) async {
    PayRrevResponse responseData = PayRrevResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_PAY_PREV_BACK, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = PayRrevResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPayRrevBack',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///결제요청 후 저장
  Future<PaySaveResponse> reqeustPaySave(PaySaveRequest data) async {
    PaySaveResponse responseData = PaySaveResponse();
    try {
      HttpType type = HttpType.post;
      if (data.reqdata != null) {
        //결제할 금액이 0이면 put으로 2021/10/15
        type = HttpType.put;
      }
      var response = await apiCall(type, ApiEndPoint.EP_PAY_SAVE, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = PaySaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPaySave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///결제취소 요청 전
  Future<PayCancelInitResponse> reqeustPayCancelInit(PayCancelInitRequest data) async {
    PayCancelInitResponse responseData = PayCancelInitResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_PAY_CANCEL_INIT, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = PayCancelInitResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPayCancelInit',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///결제취소 요청 저장
  Future<PayCancelSaveResponse> reqeustPayCancelSave(PayCancelSaveRequest data) async {
    PayCancelSaveResponse responseData = PayCancelSaveResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_PAY_CANCEL_SAVE, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = PayCancelSaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPayCancelSave',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  ///결제취소 내역보기
  Future<PayCancelSaveResponse> reqeustPayCancelView(PayCancelViewRequest data) async {
    PayCancelSaveResponse responseData = PayCancelSaveResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_PAY_CANCEL_VIEW, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = PayCancelSaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPayCancelView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  ///결제취소 소명하기
  Future<PayCancelExplainResponse> reqeustPayCancelExplain(PayCancelExplainRequest data) async {
    PayCancelExplainResponse responseData = PayCancelExplainResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_PAY_CANCEL_EXPLAIN, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = PayCancelExplainResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPayCancelExplain',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  ///결제취소 내역리스트
  Future<PayCancelInfoResponse> reqeustPayCacelInfo(PayInfoRequest data) async {
    PayCancelInfoResponse responseData = PayCancelInfoResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_PAY_CANCEL_INFO, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = PayCancelInfoResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPayCacelInfo',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  ///결제리스트
  Future<PayPayListResponse> reqeustPayList(PayPayListRequest data) async {
    PayPayListResponse responseData = PayPayListResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_PAY_LIST, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = PayPayListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPayList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  ///캐시리스트
  Future<PayCashListResponse> reqeustPayCashList(PayCashListRequest data) async {
    PayCashListResponse responseData = PayCashListResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_PAY_CASH_LIST, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = PayCashListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPayCashList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  ///포인트리스트
  Future<PayPointListResponse> reqeustPayPointList(PayPointListRequest data) async {
    PayPointListResponse responseData = PayPointListResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_PAY_POINT_LIST, apiHeader.publicApiHeader, data.toFormData(), isFile: true);
      var responseJson = Commons.returnResponse(response);
      responseData = PayPointListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPayPointList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<IndexResponse> requestIndex() async {
    IndexResponse responseData = IndexResponse(dataHeader: null, dataList: []);
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_HOME_INDEX, apiHeader.publicApiHeader, IndexRequest(gubun: storageHelper.user_type.index), receiveTimeout: receiveTimeout20, sendTimeout: sendTimeout20);
      var responseJson = Commons.returnResponse(response);
      responseData = IndexResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestIndex',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareiaryPaymentResponse> reqeustDiaryPayment(CarediaryPaymentRequest req) async {
    CareiaryPaymentResponse responseData = CareiaryPaymentResponse(dataHeader: null, dataList: []);
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_CAREDIARY_PAYMENT, apiHeader.publicApiHeader, req);
      var responseJson = Commons.returnResponse(response);
      responseData = CareiaryPaymentResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustDiaryPayment',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareiaryPaymentResponse> reqeustDiaryPaymentPatch(CarediaryPaymentRequest req) async {
    CareiaryPaymentResponse responseData = CareiaryPaymentResponse(dataHeader: null, dataList: []);
    try {
      var response = await apiCall(HttpType.patch, ApiEndPoint.EP_CAREDIARY_PAYMENT, apiHeader.publicApiHeader, req);
      var responseJson = Commons.returnResponse(response);
      responseData = CareiaryPaymentResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustDiaryPaymentPatch',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CareDiaryClaimSaveResponse> requestDiaryClaimSaveLinkmom(CareDiaryClaimSaveRequest data) async {
    CareDiaryClaimSaveResponse responseData = CareDiaryClaimSaveResponse();
    try {
      var response;
      if (data.image != null) {
        FormData formData = await Commons.setFormDataFiles2(CareDiaryClaimSaveRequest.Key_image, data.image!);
        data.toFormData(formData);
        response = await apiCall(HttpType.patch, ApiEndPoint.EP_CAREDIARY_CLAIM_SAVE, apiHeader.publicApiHeader, formData, isFile: true);
      } else {
        response = await apiCall(HttpType.patch, ApiEndPoint.EP_CAREDIARY_CLAIM_SAVE, apiHeader.publicApiHeader, data);
      }
      var responseJson = Commons.returnResponse(response);
      responseData = CareDiaryClaimSaveResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestDiaryClaimSaveLinkmom',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<IndexResponse> reqeustRefreshIndex() async {
    IndexResponse responseData = IndexResponse(dataHeader: null, dataList: []);
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_INDEX_REFRESH, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = IndexResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustRefreshIndex',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<PayListResponse> reqeustPaymentList(PayListRequest data, {String? url}) async {
    PayListResponse responseData = PayListResponse(dataList: []);
    try {
      var response;
      FormData formData = FormData();
      if (data.paymentFlag != null && data.paymentFlag!.isNotEmpty) {
        data.toFormData(formData);
        response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_PAYMENT_PAY, apiHeader.publicApiHeader, formData, isFile: true);
      } else {
        response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_PAYMENT_PAY, apiHeader.publicApiHeader, data);
      }

      var responseJson = Commons.returnResponse(response);
      responseData = PayListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPaymentList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CashListResponse> reqeustCashList(CashListRequest data, {String? url}) async {
    CashListResponse responseData = CashListResponse(dataList: []);
    try {
      var response;
      FormData formData = FormData();
      if (data.cashFlag != null && data.cashFlag!.isNotEmpty) {
        data.toFormData(formData);
        response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_PAYMENT_CASH, apiHeader.publicApiHeader, formData, isFile: true);
      } else {
        response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_PAYMENT_CASH, apiHeader.publicApiHeader, data);
      }

      var responseJson = Commons.returnResponse(response);
      responseData = CashListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCashList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<PointListResponse> reqeustPointList(PointListRequest data, {String? url}) async {
    PointListResponse responseData = PointListResponse(dataList: []);
    try {
      var response;
      FormData formData = FormData();
      if (data.pointFlag != null && data.pointFlag!.isNotEmpty) {
        data.toFormData(formData);
        response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_PAYMENT_POINT, apiHeader.publicApiHeader, formData, isFile: true);
      } else {
        response = await apiCall(HttpType.post, url ?? ApiEndPoint.EP_PAYMENT_POINT, apiHeader.publicApiHeader, data);
      }

      var responseJson = Commons.returnResponse(response);
      responseData = PointListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustPointList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<DisableValidateResponse> reqeustDisableValidate(DisableValidateRequest data) async {
    DisableValidateResponse responseData = DisableValidateResponse(dataList: []);
    try {
      var response;
      response = await apiCall(HttpType.post, ApiEndPoint.EP_DISABLE_VALIDATE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = DisableValidateResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustDisableValidate',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<MyAccountResetResponse> requestEmailReset(MyAccountResetEmailRequest data) async {
    MyAccountResetResponse responseData = MyAccountResetResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_MYPAGE_MY_ACCOUNT_RESET_EMAIL, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = MyAccountResetResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestEmailReset',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<WithdrawalResponse> requestWithdrawal(WithdrawalRequest data) async {
    WithdrawalResponse responseData = WithdrawalResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_WITHDRAWAL, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = WithdrawalResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestWithdrawal',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<WithdrawalListResponse> requestWithdrawalList(WithdrawalListRequest data, {String? next}) async {
    WithdrawalListResponse responseData = WithdrawalListResponse();
    try {
      var response = await apiCall(HttpType.post, next ?? ApiEndPoint.EP_WITHDRAWAL_LIST, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = WithdrawalListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestWithdrawalList',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<SettingsResponse> requestSettingsView() async {
    SettingsResponse responseData = SettingsResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_MYPAGE_PUSH_SETTINGS_VIEW, apiHeader.publicApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = SettingsResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestSettingsView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
      return responseData;
    }
  }

  Future<CsTermsViewResponse> reqeustCsUserGuideView(CsGuideRequest data) async {
    CsTermsViewResponse responseData = CsTermsViewResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_CS_USERGUIDE_VIEW, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CsTermsViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'reqeustCsUserGuideView',
        '--- Exception': e,
      });
      Commons.showExceptionError(e.toString());
    }
    return responseData;
  }

  Future<PaymentsCancelAgreeResponse> requestPaymentsCancelAgree(PaymentsCancelAgreeRequest data) async {
    PaymentsCancelAgreeResponse responseData = PaymentsCancelAgreeResponse();
    try {
      var response = await apiCall(HttpType.put, ApiEndPoint.EP_PAYMENTS_CANCEL_AGREE, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = PaymentsCancelAgreeResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'PaymentsCancelAgree',
        '--- Exception': e,
      });
      return responseData;
    }
  }

  Future<CouponListResponse> requestCouponList(CouponListRequest data) async {
    CouponListResponse responseData = CouponListResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_COUPON_LIST, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CouponListResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'CouponList',
        '--- Exception': e,
      });
      return responseData;
    }
  }

  Future<CouponViewResponse> requestCouponView(CouponViewRequest data) async {
    CouponViewResponse responseData = CouponViewResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_COUPON_VIEW, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = CouponViewResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'CouponView',
        '--- Exception': e,
      });
      return responseData;
    }
  }

  Future<AuthStateResponse> requestAuthStateView(AuthStateViewRequest data) async {
    AuthStateResponse responseData = AuthStateResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_AUTHCENTER_ETCIMG, apiHeader.publicApiHeader, data);
      var responseJson = Commons.returnResponse(response);
      responseData = AuthStateResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'AuthStateView',
        '--- Exception': e,
      });
      return responseData;
    }
  }

  Future<AuthStateResponse> requestAuthStateCriminalView() async {
    AuthStateResponse responseData = AuthStateResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_AUTHCENTER_CRIMINAL, apiHeader.publicApiHeader, AuthStateViewRequest());
      var responseJson = Commons.returnResponse(response);
      responseData = AuthStateResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestAuthStateCriminalView',
        '--- Exception': e,
      });
      return responseData;
    }
  }

  Future<AuthStateResponse> requestAuthStateDeungbonView() async {
    AuthStateResponse responseData = AuthStateResponse();
    try {
      var response = await apiCall(HttpType.post, ApiEndPoint.EP_AUTHCENTER_DEUNGBON, apiHeader.publicApiHeader, AuthStateViewRequest());
      var responseJson = Commons.returnResponse(response);
      responseData = AuthStateResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestAuthStateDeungbonView',
        '--- Exception': e,
      });
      return responseData;
    }
  }

  Future<AddrKeywordResponse> requestAddrSearchFromKeyword(String keyword, {int page = 1, int size = 15}) async {
    AddrKeywordResponse responseData = AddrKeywordResponse();
    try {
      var response = await apiCall(HttpType.get, ApiEndPoint.EP_KAKAO_ADDR_KEYWORD + '?query=$keyword&page=$page&size=$size', apiHeader.kakaoApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = AddrKeywordResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'AddrSearchFromKeyword',
        '--- Exception': e,
      });
      return responseData;
    }
  }

  Future<AddrSearchResponse> requestAddrSearch(String keyword, {int page = 1, int size = 30}) async {
    AddrSearchResponse responseData = AddrSearchResponse();
    try {
      var response = await apiCall(HttpType.get, ApiEndPoint.EP_KAKAO_ADDR + '?query=$keyword&page=$page&size=$size', apiHeader.kakaoApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = AddrSearchResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestAddrSearch',
        '--- Exception': e,
      });
      return responseData;
    }
  }

  Future<AddrCoordinateResponse> requestAddrCoordinate(double x, double y) async {
    AddrCoordinateResponse responseData = AddrCoordinateResponse();
    try {
      var response = await apiCall(HttpType.get, ApiEndPoint.EP_KAKAO_ADDR_COORDINATE + '?x=$x&y=$y', apiHeader.kakaoApiHeader, null);
      var responseJson = Commons.returnResponse(response);
      responseData = AddrCoordinateResponse.fromJson(responseJson);
      return responseData;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL PAGE Exception ---------------',
        '--- M.T Name ': 'requestAddrSearch',
        '--- Exception': e,
      });
      return responseData;
    }
  }
}

class RequestItem {
  RequestItem({this.callType = HttpType.post, this.url = '', required this.header, this.data});

  HttpType callType;
  String url;
  Map<String, String> header;
  dynamic data;

  @override
  String toString() {
    return 'requestItem{callType: $callType, url: $url, header: $header, data: $data}';
  }
}

class FileOutput extends LogOutput {
  FileOutput();

  File? file;

  @override
  void init() {
    super.init();
  }

  @override
  void output(OutputEvent event) async {
    String savePath = await Commons.findLocalPath();
    String name = '${'linkmom_log'}_${DateTime.now().year}${DateTime.now().month}${DateTime.now().day}.txt';
    file = File('$savePath/$name');
    if (file != null) {
      for (var line in event.lines) {
        await file!.writeAsString("${line.toString()}\n", mode: FileMode.writeOnlyAppend);
      }
    } else {
      for (var line in event.lines) {
        print(line);
      }
    }
  }
}
