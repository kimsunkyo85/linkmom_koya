import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class AuthCenterCriminalResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<CriminalResponseData>? dataList;

  AuthCenterCriminalResponse({
    this.dataHeader,
    this.dataList,
  });

  CriminalResponseData getData() => dataList != null && dataList!.length != 0 ? dataList![0] : CriminalResponseData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory AuthCenterCriminalResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CriminalResponseData>? datas = [];
    try {
      datas = json[Key_data].map<CriminalResponseData>((i) => CriminalResponseData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return AuthCenterCriminalResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ReqauthResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class CriminalResponseData {
  static const String Key_createdate = "createdate";
  static const String Key_id = "id";

  CriminalResponseData({
    this.createdate = '',
    this.id = 0,
  });

  @JsonKey(name: Key_createdate)
  String createdate;
  @JsonKey(name: Key_id)
  int id;

  factory CriminalResponseData.fromJson(Map<String, dynamic> json) {
    return CriminalResponseData(
      createdate: json[Key_createdate] ?? '',
      id: json[Key_id] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_createdate] = this.createdate;
    data[Key_id] = this.id;
    return data;
  }

  @override
  String toString() {
    return 'CriminalResponseData{createdate: $createdate, id: $id}';
  }
}
