import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_room_update_request.dart - 채팅 방 정보 업데이트(돌봄 내역서)
/// since        : 2021/07/15 / update:
@JsonSerializable()
class ChatRoomUpdateRequest {
  static const String Key_chatroom_id = 'chatroom_id';
  static const String Key_receiver = 'receiver';
  static const String Key_bookingcareservices = 'bookingcareservices';

  ChatRoomUpdateRequest({
    this.chatroom_id = 0,
    this.receiver = 0,
    this.bookingcareservices = 0,
  });

  @JsonKey(name: Key_chatroom_id)
  final int? chatroom_id;
  @JsonKey(name: Key_receiver)
  final int? receiver;
  @JsonKey(name: Key_bookingcareservices)
  final int? bookingcareservices;

  factory ChatRoomUpdateRequest.fromJson(Map<String, dynamic> json) {
    return ChatRoomUpdateRequest(
      chatroom_id: json[Key_chatroom_id] as int,
      receiver: json[Key_receiver] as int,
      bookingcareservices: json[Key_bookingcareservices] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_chatroom_id] = this.chatroom_id;
    data[Key_receiver] = this.receiver;
    data[Key_bookingcareservices] = this.bookingcareservices;
    return data;
  }

  FormData toFormData(FormData formData) {
    if (chatroom_id != null) {
      formData.fields.add(MapEntry(Key_chatroom_id, chatroom_id.toString()));
    }
    if (receiver != null) {
      formData.fields.add(MapEntry(Key_receiver, receiver.toString()));
    }
    if (bookingcareservices != null) {
      formData.fields.add(MapEntry(Key_bookingcareservices, bookingcareservices.toString()));
    }
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'ChatRoomUpdateRequest{chatroom_id: $chatroom_id, receiver: $receiver, bookingcareservices: $bookingcareservices}';
  }
}
