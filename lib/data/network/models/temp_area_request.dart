import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

///- All Rights Reserved. Copyright(c) 2022 GGUMBI CO., Ltd
///- Created by   : platformbiz@ggumbi.com
///- version      : 1.0.0
///- see          : temp_area_request.dart -  돌봄 장소 불러오기, 수정, 삭제 요청값
///- since        : 2022/02/22 / update:
///- [함수,메서드시]

@JsonSerializable()
class TempAreaRequest {
  static const String Key_area_id = 'area_id';
  static const String Key_temp_areaname = 'temp_areaname';
  static const String Key_temp_address = 'temp_address';
  static const String Key_temp_address_detail = 'temp_address_detail';
  static const String Key_createdate = 'createdate';

  TempAreaRequest({
    this.area_id,
    this.temp_areaname = '',
    this.temp_address = '',
    this.temp_address_detail = '',
    this.createdate,
  });

  ///id
  @JsonKey(name: Key_area_id)
  int? area_id;

  ///장소명
  @JsonKey(name: Key_temp_areaname)
  String? temp_areaname;

  ///장소 주소
  @JsonKey(name: Key_temp_address)
  String? temp_address;

  ///장소 주소 상세
  @JsonKey(name: Key_temp_address_detail)
  String? temp_address_detail;

  ///날짜(생성,수정)
  @JsonKey(name: Key_createdate)
  final String? createdate;

  factory TempAreaRequest.fromJson(Map<String, dynamic> json) {
    return TempAreaRequest(
      area_id: json[Key_area_id] as int,
      temp_areaname: json[Key_temp_areaname] ?? '',
      temp_address: json[Key_temp_address] ?? '',
      temp_address_detail: json[Key_temp_address_detail] ?? '',
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_area_id] = this.area_id;
    data[Key_temp_areaname] = this.temp_areaname;
    data[Key_temp_address] = this.temp_address;
    data[Key_temp_address_detail] = this.temp_address_detail;
    data[Key_createdate] = this.createdate;
    return data;
  }

  FormData toFormData() {
    FormData formData = FormData();
    if (area_id != null) {
      formData.fields.add(MapEntry(Key_area_id, area_id.toString()));
    }
    if (temp_areaname != null) {
      formData.fields.add(MapEntry(Key_temp_areaname, temp_areaname.toString()));
    }
    if (temp_address != null) {
      formData.fields.add(MapEntry(Key_temp_address, temp_address.toString()));
    }
    if (temp_address_detail != null) {
      formData.fields.add(MapEntry(Key_temp_address_detail, temp_address_detail.toString()));
    }
    if (createdate != null) {
      formData.fields.add(MapEntry(Key_createdate, createdate.toString()));
    }
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'TempAreaRequest{area_id: $area_id, temp_areaname: $temp_areaname, temp_address: $temp_address, temp_address_detail: $temp_address_detail, createdate: $createdate}';
  }
}
