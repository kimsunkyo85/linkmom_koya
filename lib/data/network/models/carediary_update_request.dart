import 'dart:io';

import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryUpdateRequest {
  static const String Key_id = 'id';
  static const String Key_carecontract_schedule = 'carecontract_schedule';
  static const String Key_is_servicetype = 'is_servicetype';
  static const String Key_is_childdrug = 'is_childdrug';
  static const String Key_complete_boyuk = 'complete_boyuk';
  static const String Key_complete_homecare = 'complete_homecare';
  static const String Key_complete_play = 'complete_play';
  static const String Key_content = 'content';
  static const String Key_image = 'image';

  CareDiaryUpdateRequest(
      {this.id = 0, this.carecontract_schedule = 0, this.is_servicetype = 0, this.is_childdrug = 0, this.complete_boyuk = '', this.complete_homecare = '', this.complete_play = '', this.content = '',
      this.image});

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_carecontract_schedule)
  final int carecontract_schedule;
  @JsonKey(name: Key_is_servicetype)
  final int is_servicetype;
  @JsonKey(name: Key_is_childdrug)
  final int is_childdrug;
  @JsonKey(name: Key_complete_boyuk)
  final String complete_boyuk;
  @JsonKey(name: Key_complete_homecare)
  final String complete_homecare;
  @JsonKey(name: Key_complete_play)
  final String complete_play;
  @JsonKey(name: Key_content)
  final String content;
  @JsonKey(name: Key_image)
  final List<File>? image;

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_carecontract_schedule] = this.carecontract_schedule;
    data[Key_is_servicetype] = this.is_servicetype;
    data[Key_is_childdrug] = this.is_childdrug;
    data[Key_complete_boyuk] = this.complete_boyuk;
    data[Key_complete_homecare] = this.complete_homecare;
    data[Key_complete_play] = this.complete_play;
    data[Key_content] = this.content;
    data[Key_image] = this.image;
    return data;
  }

  FormData toFormData(FormData formData) {
    formData.fields.add(MapEntry(Key_id, this.id.toString()));
    formData.fields.add(MapEntry(Key_carecontract_schedule, this.carecontract_schedule.toString()));
    formData.fields.add(MapEntry(Key_is_servicetype, this.is_servicetype.toString()));
    formData.fields.add(MapEntry(Key_is_childdrug, this.is_childdrug.toString()));
    formData.fields.add(MapEntry(Key_complete_boyuk, this.complete_boyuk.toString()));
    formData.fields.add(MapEntry(Key_complete_homecare, this.complete_homecare.toString()));
    formData.fields.add(MapEntry(Key_complete_play, this.complete_play.toString()));
    formData.fields.add(MapEntry(Key_content, this.content.toString()));
    return formData;
  }

  @override
  String toString() {
    return 'CareDiaryUpdateRequest{id: $id, carecontract_schedule: $carecontract_schedule, is_servicetype: $is_servicetype, is_childdrug: $is_childdrug, complete_boyuk: $complete_boyuk, complete_homecare: $complete_homecare, complete_play: $complete_play, content: $content, image: $image}';
  }
}
