import 'package:json_annotation/json_annotation.dart';

///#### All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///#### Created by   : platformbiz@ggumbi.com
///#### version      : 1.0.0
///#### see          : pay_cancel_info_request.dart - 돌봄취소내역 리스트 요청
///#### since        : 2021/10/19 / update:
///#### [함수,메서드시]
@JsonSerializable()
class PayInfoRequest {
  static const String Key_contract_id = 'contract_id';

  PayInfoRequest({
    required this.contract_id,
  });

  ///계약서 id
  @JsonKey(name: Key_contract_id)
  final int contract_id;

  factory PayInfoRequest.fromJson(Map<String, dynamic> json) {
    return PayInfoRequest(
      contract_id: json[Key_contract_id] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_contract_id] = this.contract_id;
    return data;
  }

  @override
  String toString() {
    return 'PayInfoRequest{contract_id: $contract_id}';
  }
}
