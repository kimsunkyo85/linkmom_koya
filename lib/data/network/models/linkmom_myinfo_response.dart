import 'package:easy_localization/easy_localization.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/utils/commons.dart';

import '../../../main.dart';
import 'data/job_myinfo_data.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : linkmom_myinfo_response.dart - 링크쌤 프로필과 링크쌤 상세 페이지와 같음.
/// since        : 2021/06/03 / update:
@JsonSerializable()
class LinkMomMyInfoResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<JobMyInfoData>? dataList;

  LinkMomMyInfoResponse({
    this.dataHeader,
    this.dataList,
  });

  JobMyInfoData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : JobMyInfoData();

  List<JobMyInfoData> getDataList() => dataList!;

  int getCode() {
    if (dataHeader == null) {
      return KEY_ERROR;
    }
    return dataHeader!.getCode();
  }

  String getMsg() {
    if (dataHeader == null) {
      return "에러_예외처리확인".tr();
    }
    return dataHeader!.getMsg();
  }

  factory LinkMomMyInfoResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<JobMyInfoData> datas = [];
    log.d('『GGUMBI』>>> fromJson : json[Key_data]: ${json[Key_data]},  <<< ');
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<JobMyInfoData>((i) => JobMyInfoData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return LinkMomMyInfoResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'JobMyInfoResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
