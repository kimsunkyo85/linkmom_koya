import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

@JsonSerializable()
class CsTermsListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<TermsListData>? dataList;

  CsTermsListResponse({this.dataHeader, this.dataList});

  TermsListData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : TermsListData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CsTermsListResponse.fromJson(Map<String, dynamic> json) {
    return CsTermsListResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => TermsListData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CsTermsListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class TermsListData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<TermsData>? results;

  TermsListData({this.count = 0, this.next = '', this.previous = '', this.results});

  factory TermsListData.fromJson(Map<String, dynamic> json) {
    return TermsListData(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] == null ? null : (json[Key_results] as List).map((e) => TermsData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    if (this.results != null) data[Key_results] = this.results!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'NoticeListData {count: $count, next: $next, previous: $previous, results: $results}';
  }
}

class TermsData {
  static const String Key_id = 'id';
  static const String Key_title = 'title';
  static const String Key_createdate = 'createdate';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_title)
  String title;
  @JsonKey(name: Key_createdate)
  String createdate;

  TermsData({this.title = '', this.id = 0, this.createdate = ''});

  factory TermsData.fromJson(Map<String, dynamic> json) {
    return TermsData(
      id: json[Key_id] as int,
      title: json[Key_title] ?? '',
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_title] = this.title;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'Results{id: $id, title: $title, createdate: $createdate}';
  }
}
