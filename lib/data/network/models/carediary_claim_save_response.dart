import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

import 'data/care_diary_claim_data.dart';

@JsonSerializable()
class CareDiaryClaimSaveResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<CareDiaryClaimData>? dataList;

  CareDiaryClaimSaveResponse({
    this.dataHeader,
    this.dataList,
  });

  CareDiaryClaimSaveResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  CareDiaryClaimData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CareDiaryClaimData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareDiaryClaimSaveResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareDiaryClaimData> datas = [];
    try {
      datas = json[Key_data].map<CareDiaryClaimData>((i) => CareDiaryClaimData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareDiaryClaimSaveResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return '_Response{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
