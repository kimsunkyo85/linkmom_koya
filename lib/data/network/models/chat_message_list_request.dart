import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_message_list_request.dart - 채팅 대화 가져오기
/// since        : 2021/07/16 / update:
@JsonSerializable()
class ChatMessageListRequest {
  static const String Key_chatroom_id = 'chatroom_id';
  static const String Key_talker_id = 'talker_id';

  ChatMessageListRequest({
    this.chatroom_id = 0,
    this.talker_id = 0,
  });

  @JsonKey(name: Key_chatroom_id)
  final int chatroom_id;
  @JsonKey(name: Key_talker_id)
  final int talker_id;

  factory ChatMessageListRequest.fromJson(Map<String, dynamic> json) {
    return ChatMessageListRequest(
      chatroom_id: json[Key_chatroom_id] as int,
      talker_id: json[Key_talker_id] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_chatroom_id] = this.chatroom_id;
    data[Key_talker_id] = this.talker_id;
    return data;
  }

  @override
  String toString() {
    return 'ChatMessageListRequest{chatroom_id: $chatroom_id, talker_id: $talker_id}';
  }
}
