import 'dart:io';

import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

@JsonSerializable()
class CareDiarySaveRequest {
  static const String Key_carecontract_schedule = 'carecontract_schedule';
  static const String Key_is_servicetype = 'is_servicetype';
  static const String Key_is_childdrug = 'is_childdrug';
  static const String Key_complete_boyuk = 'complete_boyuk';
  static const String Key_complete_homecare = 'complete_homecare';
  static const String Key_complete_play = 'complete_play';
  static const String Key_content = 'content';
  static const String Key_image = 'image';

  CareDiarySaveRequest({this.carecontract_schedule = 0, this.is_servicetype = 0, this.is_childdrug = 0, this.complete_boyuk = '', this.complete_homecare = '', this.complete_play = '', this.content = '', this.image});

  @JsonKey(name: Key_carecontract_schedule)
  final int carecontract_schedule;
  @JsonKey(name: Key_is_servicetype)
  final int is_servicetype;
  @JsonKey(name: Key_is_childdrug)
  final int is_childdrug;
  @JsonKey(name: Key_complete_boyuk)
  final String complete_boyuk;
  @JsonKey(name: Key_complete_homecare)
  final String complete_homecare;
  @JsonKey(name: Key_complete_play)
  final String complete_play;
  @JsonKey(name: Key_content)
  final String content;
  @JsonKey(name: Key_image)
  final List<File>? image;

  factory CareDiarySaveRequest.fromJson(Map<String, dynamic> json) {
    List<int> boyuk = [];
    List<int> homeCare = [];
    List<int> play = [];
    List<String> image = [];
    try {
      boyuk = List.generate(json[Key_complete_boyuk].length, (index) => json[Key_complete_boyuk][index]);
      homeCare = List.generate(json[Key_complete_homecare].length, (index) => json[Key_complete_homecare][index]);
      play = List.generate(json[Key_complete_play].length, (index) => json[Key_complete_play][index]);
      image = List.generate(json[Key_image].length, (index) => json[Key_image][index]);
    } catch (e) {
      log.e("CareDiarySaveRequest: Exception >>>>>>>>>> $e");
    }
    return CareDiarySaveRequest(
      carecontract_schedule: json[Key_carecontract_schedule] as int,
      is_servicetype: json[Key_is_servicetype] as int,
      is_childdrug: json[Key_is_childdrug] as int,
      complete_boyuk: json[Key_complete_boyuk] ?? '',
      complete_homecare: json[Key_complete_homecare] ?? '',
      complete_play: json[Key_complete_play] ?? '',
      content: json[Key_content] ?? '',
      image: json[Key_image] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    if (carecontract_schedule != 0) data[Key_carecontract_schedule] = this.carecontract_schedule;
    if (is_servicetype != 0) data[Key_is_servicetype] = this.is_servicetype;
    if (is_childdrug != 0) data[Key_is_childdrug] = this.is_childdrug;
    if (complete_boyuk.isNotEmpty) data[Key_complete_boyuk] = this.complete_boyuk;
    if (complete_homecare.isNotEmpty) data[Key_complete_homecare] = this.complete_homecare;
    if (complete_play.isNotEmpty) data[Key_complete_play] = this.complete_play;
    if (content.isNotEmpty) data[Key_content] = this.content;
    if (image != null && image!.isNotEmpty) data[Key_image] = this.image;
    return data;
  }

  FormData toFormData(FormData formData) {
    if (this.carecontract_schedule != 0) formData.fields.add(MapEntry(Key_carecontract_schedule, this.carecontract_schedule.toString()));
    if (this.is_servicetype != 0) formData.fields.add(MapEntry(Key_is_servicetype, this.is_servicetype.toString()));
    if (this.is_childdrug != 0) formData.fields.add(MapEntry(Key_is_childdrug, this.is_childdrug.toString()));
    if (this.complete_boyuk.isNotEmpty) formData.fields.add(MapEntry(Key_complete_boyuk, this.complete_boyuk.toString()));
    if (this.complete_homecare.isNotEmpty) formData.fields.add(MapEntry(Key_complete_homecare, this.complete_homecare.toString()));
    if (this.complete_play.isNotEmpty) formData.fields.add(MapEntry(Key_complete_play, this.complete_play.toString()));
    if (this.content.isNotEmpty) formData.fields.add(MapEntry(Key_content, this.content.toString()));
    return formData;
  }

  @override
  String toString() {
    return 'CareDiarySaveRequest{carecontract_schedule: $carecontract_schedule, is_servicetype: $is_servicetype, is_childdrug: $is_childdrug, complete_boyuk: $complete_boyuk, complete_homecare: $complete_homecare, complete_play: $complete_play, content: $content, image: $image}';
  }
}
