import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : job_list_request.dart - 돌봄/구직 신청 리스트 조회
/// since        : 2021/06/03 / update:
@JsonSerializable()
class JobListRequest {
  ///페이지 번호
  static const String Key_page = 'page';

  ///사용자별 보기 (user_id)
  static const String Key_userid = 'userid';

  ///시급 (최소)
  static const String Key_costmin = 'costmin';

  ///시급 (최대)
  static const String Key_costmax = 'costmax';

  ///돌봄서비스 구분(0:등원, 1:하원, 2:보육, 3:학습, 4:입주가사, 5:이유식반찬)
  static const String Key_servicetype = 'servicetype';

  ///돌봄장소 (1:우리집, 2:이웃집)
  static const String Key_possible_area = 'possible_area';

  ///요일 (0, '월'), (1, '화'), (2, '수'), (3, '목'), (4, '금'), (5, '토'), (6, '일')
  static const String Key_dayofweek = 'dayofweek';

  ///오전 (0, 1)
  static const String Key_is_am = 'is_am';

  ///오후 (0, 1)
  static const String Key_is_pm = 'is_pm';

  ///돌봄지역 1순위 - 법정동코드
  static const String Key_area_code = 'area_code';

  ///사용자 법정동코드
  static const String Key_bcode = 'bcode';

  ///사용자 행정동코드
  static const String Key_hcode = 'hcode';

  ///내 주소지 반경(내 주소지 반경 1,5,10 KM (default : 10)
  static const String Key_radius = 'radius';

  ///인증 해시 태그
  static const String Key_authtag = 'authtag';

  ///돌봄날짜순 (caredate : default) 최신등록순 (createdate), 높은 금액순 (costmax), 낮은 금액순(costmin), 맘대디묶어보기 (momdady)
  static const String Key_orderingtype = 'orderingtype';

  ///돌봄대상 돌봄대상 (0:신생아, 1:영아, 2:유아, 3:초등, 4:중등, 5:고
  static const String Key_care_ages = 'care_ages';

  ///0 : 전체, 1 : 동네인증지역, 2 : 돌봄가능지역
  static const String Key_addr_gubun = 'addr_gubun';

  ///send_code : 52001, 53001로 Push 발송 시 /
  static const String Key_is_region = 'is_region';

  JobListRequest({
    this.page = 0,
    this.userid = 0,
    this.costmin = 5000,
    this.costmax = 50000,
    this.servicetype,
    this.possible_area,
    this.dayofweek,
    this.is_am = 0,
    this.is_pm = 0,
    this.area_code,
    this.bcode = '',
    this.hcode = '',
    this.radius = 0,
    this.authtag,
    this.orderingtype = '',
    this.care_ages,
    this.area_code_data,
    this.addr_gubun = 0,
    this.is_region = 0,
  });

  JobListRequest.init() {
    costmin = 5000;
    costmax = 50000;
    servicetype = [];
    possible_area = [];
    dayofweek = [];
    // is_am = 1;
    // is_pm = 1;
    // radius = 10;
    authtag = [];
    care_ages = [];
    area_code = [];
    area_code_data = [];
    area_code_data!.add(SingleItem(''));
    area_code_data!.add(SingleItem(''));
    addr_gubun = 0;
    is_region = 0;
  }

  JobListRequest.clone(JobListRequest item)
      : this(
          page: item.page,
          userid: item.userid,
          costmin: item.costmin,
          costmax: item.costmax,
          servicetype: item.servicetype,
          possible_area: item.possible_area,
          dayofweek: item.dayofweek,
          is_am: item.is_am,
          is_pm: item.is_pm,
          area_code: item.area_code,
          bcode: item.bcode,
          hcode: item.hcode,
          radius: item.radius,
          authtag: item.authtag,
          orderingtype: item.orderingtype,
          care_ages: item.care_ages,
          area_code_data: item.area_code_data,
          addr_gubun: item.addr_gubun,
          is_region: item.is_region,
        );

  ///페이지 번호
  @JsonKey(name: Key_page)
  int? page;

  ///사용자별 보기 (user_id)
  @JsonKey(name: Key_userid)
  int? userid;

  ///시급 (최소)
  @JsonKey(name: Key_costmin)
  late int costmin;

  ///시급 (최대)
  @JsonKey(name: Key_costmax)
  late int costmax;

  ///돌봄서비스 구분(0:등원, 1:하원, 2:보육, 3:학습, 4:입주가사, 5:이유식반찬)
  @JsonKey(name: Key_servicetype)
  List<int>? servicetype;

  ///돌봄장소 (1:우리집, 2:이웃집)
  @JsonKey(name: Key_possible_area)
  List<int>? possible_area;

  ///요일 (0, '월'), (1, '화'), (2, '수'), (3, '목'), (4, '금'), (5, '토'), (6, '일')
  @JsonKey(name: Key_dayofweek)
  List<int>? dayofweek;

  ///오전 (0, 1)
  @JsonKey(name: Key_is_am)
  int? is_am;

  ///오후 (0, 1)
  @JsonKey(name: Key_is_pm)
  int? is_pm;

  ///돌봄지역 1순위 - 법정동코드, 2순
  @JsonKey(name: Key_area_code)
  List<String>? area_code;

  List<SingleItem>? area_code_data;

  ///사용자 행정동코드
  @JsonKey(name: Key_bcode)
  String bcode = '';

  ///사용자 법정동코드
  @JsonKey(name: Key_hcode)
  String hcode = '';

  ///내 주소지 반경 1,5,10 KM (default : 10)
  @JsonKey(name: Key_radius)
  int? radius;

  ///인증 HashTag : authinfo 의 hashtag key 값
  @JsonKey(name: Key_authtag)
  List<String>? authtag;

  ///돌봄날짜순 (caredate : default) 최신등록순 (createdate), 높은 금액순 (costmax), 낮은 금액순(costmin), 맘대디묶어보기 (momdady)
  @JsonKey(name: Key_orderingtype)
  String? orderingtype;

  ///돌봄대상 돌봄대상 (0:신생아, 1:영아, 2:유아, 3:초등, 4:중등, 5:고
  @JsonKey(name: Key_care_ages)
  List<int>? care_ages;

  ///0 : 전체, 1 : 동네인증지역, 2 : 돌봄가능지역
  @JsonKey(name: Key_addr_gubun)
  int? addr_gubun;

  ///send_code : 52001, 53001로 Push 발송 시 /
  @JsonKey(name: Key_is_region)
  int? is_region;

  factory JobListRequest.fromJson(Map<String, dynamic> json) {
    List<int> servicetype = json[Key_servicetype] == null ? [] : List<int>.from(json[Key_servicetype]);
    List<int> possibleArea = json[Key_possible_area] == null ? [] : List<int>.from(json[Key_possible_area]);
    List<int> dayofweek = json[Key_dayofweek] == null ? [] : List<int>.from(json[Key_dayofweek]);
    List<String> areaCode = json[Key_area_code] == null ? [] : List<String>.from(json[Key_area_code]);
    List<String> authtag = json[Key_authtag] == null ? [] : List<String>.from(json[Key_authtag]);
    List<int> careAges = json[Key_care_ages] == null ? [] : List<int>.from(json[Key_care_ages]);
    log.d('『GGUMBI』>>> fromJson : json[Key_userid].runtimeType: ${json[Key_userid] is List<dynamic>},  <<< ');
    if (json[Key_userid] is List<dynamic>) {
      log.d('『GGUMBI』>>> fromJson : json[Key_userid]: ${json[Key_userid] == ''},  <<< ');
      log.d('『GGUMBI』>>> fromJson : json[Key_userid]: ${json[Key_userid] == null},  <<< ');
      log.d('『GGUMBI』>>> fromJson : json[Key_userid]: ${json[Key_userid] == 0},  <<< ');
      var i = json[Key_userid] ?? 'null';
      log.d('『GGUMBI』>>> fromJson : json[Key_userid]: $i,  <<< ');
    }
    return JobListRequest(
      // page: json[Key_page] as int,
      userid: json[Key_userid] as int,
      costmin: json[Key_costmin] as int,
      costmax: json[Key_costmax] as int,
      servicetype: servicetype,
      possible_area: possibleArea,
      dayofweek: dayofweek,
      is_am: json[Key_is_am] as int,
      is_pm: json[Key_is_pm] as int,
      area_code: areaCode,
      bcode: json[Key_bcode] ?? '',
      hcode: json[Key_hcode] ?? '',
      radius: json[Key_radius] as int,
      authtag: authtag,
      orderingtype: json[Key_orderingtype] ?? '',
      care_ages: careAges,
      addr_gubun: json[Key_addr_gubun] as int,
      is_region: json[Key_is_region] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_userid] = this.servicetype;
    data[Key_costmin] = this.possible_area;
    data[Key_costmax] = this.costmax;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_dayofweek] = this.dayofweek;
    data[Key_is_am] = this.is_am;
    data[Key_is_pm] = this.is_pm;
    data[Key_area_code] = this.area_code;
    data[Key_bcode] = this.bcode;
    data[Key_hcode] = this.hcode;
    data[Key_radius] = this.radius;
    data[Key_authtag] = this.authtag;
    data[Key_orderingtype] = this.orderingtype;
    data[Key_care_ages] = this.care_ages;
    data[Key_addr_gubun] = this.addr_gubun;
    data[Key_is_region] = this.is_region;
    return data;
  }

  FormData toFormData() {
    FormData formData = FormData();

    // if (userid != null) {
    //   formData.fields.add(MapEntry(Key_userid, userid.toString()));
    // }
    if (costmin > 0) {
      formData.fields.add(MapEntry(Key_costmin, costmin.toString()));
    }
    if (costmax > 0) {
      formData.fields.add(MapEntry(Key_costmax, costmax.toString()));
    }
    if (servicetype != null && servicetype!.isNotEmpty) {
      servicetype!.forEach((value) {
        formData.fields.add(MapEntry(Key_servicetype, value.toString()));
      });
    }
    if (possible_area != null && possible_area!.isNotEmpty) {
      possible_area!.forEach((value) {
        formData.fields.add(MapEntry(Key_possible_area, value.toString()));
      });
    }
    if (dayofweek != null && dayofweek!.isNotEmpty) {
      dayofweek!.forEach((value) {
        formData.fields.add(MapEntry(Key_dayofweek, value.toString()));
      });
    }
    if (is_am != null) {
      formData.fields.add(MapEntry(Key_is_am, is_am.toString()));
    }
    if (is_pm != null) {
      formData.fields.add(MapEntry(Key_is_pm, is_pm.toString()));
    }
    if (area_code != null && area_code!.isNotEmpty) {
      area_code!.forEach((value) {
        formData.fields.add(MapEntry(Key_area_code, value.toString()));
      });
    }
    if (bcode.isNotEmpty) {
      formData.fields.add(MapEntry(Key_bcode, bcode.toString()));
    }
    if (hcode.isNotEmpty) {
      formData.fields.add(MapEntry(Key_hcode, hcode.toString()));
    }
    if (radius != null) {
      formData.fields.add(MapEntry(Key_radius, radius.toString()));
    }
    if (authtag != null && authtag!.isNotEmpty) {
      authtag!.forEach((value) {
        formData.fields.add(MapEntry(Key_authtag, value.toString()));
      });
    }
    if (orderingtype != null) {
      formData.fields.add(MapEntry(Key_orderingtype, orderingtype.toString()));
    }
    if (care_ages != null && care_ages!.isNotEmpty) {
      care_ages!.forEach((value) {
        formData.fields.add(MapEntry(Key_care_ages, value.toString()));
      });
    }
    if (addr_gubun != null) {
      formData.fields.add(MapEntry(Key_addr_gubun, addr_gubun.toString()));
    }
    if (is_region != null) {
      formData.fields.add(MapEntry(Key_is_region, is_region.toString()));
    }

    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  JobListRequest clone() {
    final jsonResponse = json.decode(json.encode(this));
    return JobListRequest.fromJson(jsonResponse as Map<String, dynamic>);
  }

  JobListRequest clone2(Map data) {
    return JobListRequest.fromJson(data as Map<String, dynamic>);
  }

  @override
  String toString() {
    return 'JobListRequest{page: $page, userid: $userid, costmin: $costmin, costmax: $costmax, servicetype: $servicetype, possible_area: $possible_area, dayofweek: $dayofweek, is_am: $is_am, is_pm: $is_pm, area_code: $area_code, area_code_data: $area_code_data, bcode: $bcode, hcode: $hcode, radius: $radius, authtag: $authtag, orderingtype: $orderingtype, care_ages: $care_ages, addr_gubun: $addr_gubun, is_region: $is_region}';
  }
}
