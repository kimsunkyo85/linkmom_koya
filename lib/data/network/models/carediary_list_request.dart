import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryListReqeust {
  static const String Key_gubun = 'gubun';
  static const String Key_type = 'type';
  static const String Key_start_date = 'start_date';
  static const String Key_end_date = 'end_date';
  static const String Key_pay_status = 'pay_status';
  static const String Key_servicetype = 'servicetype';
  static const String Key_is_confirm = 'is_confirm';
  static const String Key_child_id = 'child_id';

  CareDiaryListReqeust(
      {this.gubun = 0, this.type = 0, this.start_date, this.end_date, this.servicetype, this.pay_status, this.is_confirm, this.child_id});

  @JsonKey(name: Key_gubun)
  final int gubun;
  @JsonKey(name: Key_type)
  final int type;
  @JsonKey(name: Key_start_date)
  final String? start_date;
  @JsonKey(name: Key_end_date)
  final String? end_date;
  @JsonKey(name: Key_pay_status)
  final String? pay_status;
  @JsonKey(name: Key_servicetype)
  final String? servicetype;
  @JsonKey(name: Key_is_confirm)
  final int? is_confirm;
  @JsonKey(name: Key_child_id)
  final int? child_id;

  factory CareDiaryListReqeust.fromJson(Map<String, dynamic> json) {
    return CareDiaryListReqeust(
      gubun: json[Key_gubun] as int,
      type: json[Key_type] as int,
      start_date: json[Key_start_date] ?? '',
      end_date: json[Key_end_date] ?? '',
      pay_status: json[Key_pay_status] ?? '',
        servicetype: json[Key_servicetype] ?? '',
        is_confirm: json[Key_is_confirm] as int,
        child_id: json[Key_child_id] as int
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_gubun] = gubun;
    data[Key_type] = type;
    if (start_date != null) data[Key_start_date] = start_date;
    if (end_date != null) data[Key_end_date] = end_date;
    if (pay_status != null) data[Key_pay_status] = pay_status;
    if (servicetype != null) data[Key_servicetype] = servicetype;
    if (is_confirm != null) data[Key_is_confirm] = is_confirm;
    if (child_id != null) data[Key_child_id] = child_id;
    return data;
  }

  FormData toFormData(FormData formData) {
    formData.fields.add(MapEntry(Key_gubun, this.gubun.toString()));
    formData.fields.add(MapEntry(Key_type, this.type.toString()));
    if (this.start_date != null) formData.fields.add(MapEntry(Key_start_date, this.start_date.toString()));
    if (this.end_date != null) formData.fields.add(MapEntry(Key_end_date, this.end_date.toString()));
    if (this.pay_status != null) formData.fields.add(MapEntry(Key_pay_status, this.pay_status.toString()));
    if (this.servicetype != null) formData.fields.add(MapEntry(Key_servicetype, this.servicetype.toString()));
    if (this.is_confirm != null) formData.fields.add(MapEntry(Key_is_confirm, this.is_confirm.toString()));
    if (this.child_id != null) formData.fields.add(MapEntry(Key_child_id, this.child_id.toString()));
    return formData;
  }

  @override
  String toString() {
    return 'CareDiaryListReqeust{gubun: $gubun, type: $type, start_date: $start_date, end_date: $end_date, pay_status: $pay_status, servicetype: $servicetype, is_confirm: $is_confirm, child_id: $child_id}';
  }
}
