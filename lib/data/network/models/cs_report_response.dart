import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

@JsonSerializable()
class CsReportResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<CsReportData>? dataList;

  CsReportResponse({
    this.dataHeader,
    this.dataList,
  });

  CsReportData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CsReportData();

  List<CsReportData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CsReportResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CsReportData> datas = [];
    try {
      datas = json[Key_data].map<CsReportData>((i) => CsReportData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return CsReportResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AuthCenterHomeResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class CsReportData {
  static const String Key_id = 'id';
  static const String Key_notifyuser = 'notifyuser';
  static const String Key_carematching = 'carematching';
  static const String Key_chattingmessage = 'chattingmessage';
  static const String Key_notifyreason = 'notifyreason';
  static const String Key_msg = 'msg';
  static const String Key_image = 'image';
  static const String Key_createdate = 'createdate';

  CsReportData({
    this.id = 0,
    this.notifyuser = 0,
    this.carematching = 0,
    this.chattingmessage = '',
    this.notifyreason = 0,
    this.msg = '',
    this.image,
    this.createdate = '',
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_notifyuser)
  final int notifyuser;
  @JsonKey(name: Key_carematching)
  final int carematching;
  @JsonKey(name: Key_chattingmessage)
  final String chattingmessage;
  @JsonKey(name: Key_notifyreason)
  final int notifyreason;
  @JsonKey(name: Key_msg)
  final String msg;
  @JsonKey(name: Key_image)
  final List<ImageData>? image;
  @JsonKey(name: Key_createdate)
  final String createdate;

  factory CsReportData.fromJson(Map<String, dynamic> json) {
    List<ImageData> image = json[Key_image].map<ImageData>((i) => ImageData.fromJson(i)).toList();
    // List<LinkMomListResultsData> results = json[Key_results] == null ? List() : json[Key_results].map<LinkMomListResultsData>((i) => LinkMomListResultsData.fromJson(i)).toList();
    return CsReportData(
      id: json[Key_id] as int,
      notifyuser: json[Key_notifyuser] as int,
      carematching: json[Key_carematching] as int,
      chattingmessage: json[Key_chattingmessage] ?? '',
      notifyreason: json[Key_notifyreason] as int,
      msg: json[Key_msg] ?? '',
      image: image,
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_notifyuser] = this.notifyuser;
    data[Key_carematching] = this.carematching;
    data[Key_chattingmessage] = this.chattingmessage;
    data[Key_notifyreason] = this.notifyreason;
    data[Key_msg] = this.msg;
    data[Key_image] = this.image;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'ApplyNotifyData{id: $id, notifyuser: $notifyuser, carematching: $carematching, chattingmessage: $chattingmessage, notifyreason: $notifyreason, msg: $msg, image: $image, createdate: $createdate}';
  }
}

class ImageData {
  static const String Key_image = 'image';
  static const String Key_createdate = 'createdate';

  ImageData({
    this.image = '',
    this.createdate = '',
  });

  @JsonKey(name: Key_image)
  final String image;
  @JsonKey(name: Key_createdate)
  final String createdate;

  factory ImageData.fromJson(Map<String, dynamic> json) {
    return ImageData(
      image: json[Key_image] ?? '',
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_image] = this.image;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'ImageData{image: $image, createdate: $createdate}';
  }
}
