import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

import 'community_list_response.dart';
import 'mypage_myinfo_response.dart';

@JsonSerializable()
class IndexResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<IndexData> dataList;

  IndexResponse({required this.dataHeader, required this.dataList});

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.message;

  IndexData getData() => dataList.isEmpty
      ? IndexData(
          eventBanner: [],
          likeRanking: [],
          linkmominfo: Linkmominfo(carediary: []),
          momdadyinfo: Momdadyinfo(carediary: [], community: [], communityShare: [], communityReview: [], communityAll: []),
          priceRanking: [],
          reviewRanking: [],
          userinfo: Userinfo(),
          adBanner: [],
        )
      : dataList.first;

  factory IndexResponse.fromJson(Map<String, dynamic> json) {
    return IndexResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? [] : (json[Key_data] as List).map((e) => IndexData.fromJson(e)).toList(),
    );
  }

  @override
  String toString() {
    return 'IndexResponse{dataHeader:$dataHeader, dataList:$dataList}';
  }
}

@JsonSerializable()
class IndexData {
  static const String Key_userinfo = 'userinfo';
  static const String Key_momdadyinfo = 'momdadyinfo';
  static const String Key_linkmominfo = 'linkmominfo';
  static const String Key_event_banner = 'event_banner';
  static const String Key_price_ranking = 'price_ranking';
  static const String Key_like_ranking = 'like_ranking';
  static const String Key_review_ranking = 'review_ranking';
  static const String Key_ad_banner = 'ad_banner';
  static const String Key_allim = 'allim';
  static const String Key_chat = 'chat';
  static const String Key_bookingcareservices_banner = 'bookingcareservices_banner';
  static const String Key_bookingjobs_banner = 'bookingjobs_banner';

  @JsonKey(name: Key_userinfo)
  Userinfo userinfo;
  @JsonKey(name: Key_momdadyinfo)
  Momdadyinfo momdadyinfo;
  @JsonKey(name: Key_linkmominfo)
  Linkmominfo linkmominfo;
  @JsonKey(name: Key_event_banner)
  List<EventBanner> eventBanner;
  @JsonKey(name: Key_price_ranking)
  List<RankingData> priceRanking;
  @JsonKey(name: Key_like_ranking)
  List<RankingData> likeRanking;
  @JsonKey(name: Key_review_ranking)
  List<RankingData> reviewRanking;
  @JsonKey(name: Key_ad_banner)
  List<EventBanner> adBanner;
  @JsonKey(name: Key_allim)
  bool allim;
  @JsonKey(name: Key_chat)
  bool chat;
  @JsonKey(name: Key_bookingcareservices_banner)
  EventBanner? bookingcareservices_banner;
  @JsonKey(name: Key_bookingjobs_banner)
  EventBanner? bookingjobs_banner;

  IndexData({
    required this.userinfo,
    required this.momdadyinfo,
    required this.linkmominfo,
    required this.eventBanner,
    required this.priceRanking,
    required this.likeRanking,
    required this.reviewRanking,
    required this.adBanner,
    this.allim = false,
    this.chat = false,
    this.bookingcareservices_banner,
    this.bookingjobs_banner,
  });

  factory IndexData.fromJson(Map<String, dynamic> json) {
    return IndexData(
      userinfo: json[Key_userinfo] == null ? Userinfo() : Userinfo.fromJson(json[Key_userinfo]),
      momdadyinfo: json[Key_momdadyinfo] == null ? Momdadyinfo(carediary: [], community: [], communityShare: [], communityReview: [], communityAll: []) : Momdadyinfo.fromJson(json[Key_momdadyinfo]),
      linkmominfo: json[Key_linkmominfo] == null ? Linkmominfo(carediary: []) : Linkmominfo.fromJson(json[Key_linkmominfo]),
      eventBanner: json[Key_event_banner] == null ? [] : (json[Key_event_banner] as List).map((e) => EventBanner.fromJson(e)).toList(),
      priceRanking: json[Key_price_ranking] == null ? [] : (json[Key_price_ranking] as List).map((e) => RankingData.fromJson(e)).toList(),
      likeRanking: json[Key_like_ranking] == null ? [] : (json[Key_like_ranking] as List).map((e) => RankingData.fromJson(e)).toList(),
      reviewRanking: json[Key_review_ranking] == null ? [] : (json[Key_review_ranking] as List).map((e) => RankingData.fromJson(e)).toList(),
      adBanner: json[Key_ad_banner] == null ? [] : (json[Key_ad_banner] as List).map((e) => EventBanner.fromJson(e)).toList(),
      allim: json[Key_allim] as bool,
      chat: json[Key_chat] as bool,
      bookingcareservices_banner: json[Key_bookingcareservices_banner] == null ? EventBanner() : EventBanner.fromJson(json[Key_bookingcareservices_banner]),
      bookingjobs_banner: json[Key_bookingjobs_banner] == null ? EventBanner() : EventBanner.fromJson(json[Key_bookingjobs_banner]),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_userinfo] = this.userinfo.toJson();
    data[Key_momdadyinfo] = this.momdadyinfo.toJson();
    data[Key_linkmominfo] = this.linkmominfo.toJson();
    data[Key_event_banner] = this.eventBanner.map((e) => e.toJson()).toList();
    data[Key_price_ranking] = this.priceRanking.map((e) => e.toJson()).toList();
    data[Key_like_ranking] = this.likeRanking.map((e) => e.toJson()).toList();
    data[Key_review_ranking] = this.reviewRanking.map((e) => e.toJson()).toList();
    data[Key_allim] = this.allim;
    data[Key_chat] = this.chat;
    data[Key_bookingcareservices_banner] = this.bookingcareservices_banner;
    data[Key_bookingjobs_banner] = this.bookingjobs_banner;
    return data;
  }

  @override
  String toString() {
    return 'IndexData{userinfo: $userinfo, momdadyinfo: $momdadyinfo, linkmominfo: $linkmominfo, eventBanner: $eventBanner, priceRanking: $priceRanking, likeRanking: $likeRanking, reviewRanking: $reviewRanking, adBanner: $adBanner, allim: $allim, chat: $chat, bookingcareservices_banner: $bookingcareservices_banner, bookingjobs_banner: $bookingjobs_banner}';
  }
}

class RankingData {
  static const String Key_user_id = 'user_id';
  static const String Key_first_name = 'first_name';
  static const String Key_gender = 'gender';
  static const String Key_age = 'age';
  static const String Key_job = 'job';
  static const String Key_current_region_3depth = 'current_region_3depth';
  static const String Key_profileimg = 'profileimg';
  static const String Key_price_sum = 'price_sum';
  static const String Key_like_cnt = 'like_cnt';
  static const String Key_review_cnt = 'review_cnt';
  static const String Key_hcode = 'hcode';
  static const String Key_increase = 'increase';

  @JsonKey(name: Key_user_id)
  int userId;
  @JsonKey(name: Key_first_name)
  String firstName;
  @JsonKey(name: Key_gender)
  String gender;
  @JsonKey(name: Key_age)
  String age;
  @JsonKey(name: Key_job)
  String job;
  @JsonKey(name: Key_current_region_3depth)
  String currentRegion3depth;
  @JsonKey(name: Key_profileimg)
  String profileimg;
  @JsonKey(name: Key_price_sum)
  int priceSum;
  @JsonKey(name: Key_like_cnt)
  int likeCnt;
  @JsonKey(name: Key_review_cnt)
  int reviewCnt;
  @JsonKey(name: Key_hcode)
  int hcode;
  @JsonKey(name: Key_increase)
  int increase;

  RankingData({
    this.userId = 0,
    this.firstName = '',
    this.gender = '',
    this.age = '',
    this.job = '',
    this.currentRegion3depth = '',
    this.profileimg = '',
    this.priceSum = 0,
    this.likeCnt = 0,
    this.reviewCnt = 0,
    this.hcode = 0,
    this.increase = 0,
  });

  factory RankingData.fromJson(Map<String, dynamic> json) {
    return RankingData(
      userId: json[Key_user_id] as int,
      firstName: json[Key_first_name] ?? '',
      gender: json[Key_gender] ?? '',
      age: json[Key_age] ?? '',
      job: json[Key_job] ?? '',
      currentRegion3depth: json[Key_current_region_3depth] ?? '',
      profileimg: json[Key_profileimg] ?? '',
      priceSum: json[Key_price_sum] as int,
      likeCnt: json[Key_like_cnt] as int,
      reviewCnt: json[Key_review_cnt] as int,
      hcode: int.parse(json[Key_hcode] ?? 0),
      increase: json[Key_increase] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_user_id] = this.userId;
    data[Key_first_name] = this.firstName;
    data[Key_gender] = this.gender;
    data[Key_age] = this.age;
    data[Key_job] = this.job;
    data[Key_current_region_3depth] = this.currentRegion3depth;
    data[Key_profileimg] = this.profileimg;
    data[Key_price_sum] = this.priceSum;
    data[Key_like_cnt] = this.likeCnt;
    data[Key_review_cnt] = this.reviewCnt;
    data[Key_hcode] = this.hcode;
    data[Key_increase] = this.increase;
    return data;
  }

  @override
  String toString() {
    return 'RankingData{userId: $userId, firstName: $firstName, gender: $gender, age: $age, job: $job, region3Depth: $currentRegion3depth, profileimg: $profileimg, priceSum: $priceSum, likeCnt: $likeCnt, reviewCnt: $reviewCnt, increase: $increase}';
  }
}

class EventBanner {
  static const String Key_id = 'id';
  static const String Key_banner_url = 'banner_url';
  static const String Key_link_url = 'link_url';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_banner_url)
  String bannerUrl;
  @JsonKey(name: Key_link_url)
  String linkUrl;

  EventBanner({this.id = 0, this.bannerUrl = '', this.linkUrl = ''});

  factory EventBanner.fromJson(Map<String, dynamic> json) {
    return EventBanner(
      id: json[Key_id] as int,
      bannerUrl: json[Key_banner_url] ?? '',
      linkUrl: json[Key_link_url] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_banner_url] = this.bannerUrl;
    data[Key_link_url] = this.linkUrl;
    return data;
  }

  @override
  String toString() {
    return 'EventBanner{id: $id, bannerUrl: $bannerUrl, linkUrl: $linkUrl}';
  }
}

class Linkmominfo {
  static const String Key_total_caretime = 'total_caretime';
  static const String Key_total_price = 'total_price';
  static const String Key_grade_authlevel = 'grade_authlevel';
  static const String Key_grade_linkmom = 'grade_linkmom';
  static const String Key_carediary = 'carediary';
  static const String Key_is_bookingjobs = 'is_bookingjobs';

  @JsonKey(name: Key_total_caretime)
  int totalCaretime = 0;
  @JsonKey(name: Key_total_price)
  int totalPrice;
  @JsonKey(name: Key_grade_authlevel)
  int gradeAuthlevel;
  @JsonKey(name: Key_grade_linkmom)
  String gradeLinkmom;
  @JsonKey(name: Key_carediary)
  List<CareDiary> carediary;
  @JsonKey(name: Key_is_bookingjobs)
  bool isBookingjobs;

  Linkmominfo({this.totalCaretime = 0, this.totalPrice = 0, this.gradeAuthlevel = 0, this.gradeLinkmom = '', required this.carediary, this.isBookingjobs = false});

  factory Linkmominfo.fromJson(Map<String, dynamic> json) {
    return Linkmominfo(
      totalCaretime: json[Key_total_caretime] as int,
      totalPrice: json[Key_total_price] as int,
      gradeAuthlevel: json[Key_grade_authlevel] as int,
      gradeLinkmom: json[Key_grade_linkmom] ?? '',
      carediary: json[Key_carediary] == null ? [] : (json[Key_carediary] as List).map((e) => CareDiary.fromJson(e)).toList(),
      isBookingjobs: json[Key_is_bookingjobs] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_total_caretime] = this.totalCaretime;
    data[Key_total_price] = this.totalPrice;
    data[Key_grade_authlevel] = this.gradeAuthlevel;
    data[Key_grade_linkmom] = this.gradeLinkmom;
    data[Key_carediary] = this.carediary.map((e) => e.toJson()).toList();
    data[Key_is_bookingjobs] = this.isBookingjobs;
    return data;
  }

  @override
  String toString() {
    return 'Linkmominfo{totalCaretime: $totalCaretime, totalPrice: $totalPrice, gradeAuthlevel: $gradeAuthlevel, gradeLinkmom: $gradeLinkmom, carediary: $carediary, isBookingjobs: $isBookingjobs}';
  }
}

class Momdadyinfo {
  static const String Key_grade_momdady = 'grade_momdady';
  static const String Key_carediary = 'carediary';
  static const String Key_community = 'community';
  static const String Key_community_share = 'community_share';
  static const String Key_community_review = 'community_review';
  static const String Key_is_careservices = 'is_careservices';
  static const String Key_community_all = 'community_all';

  @JsonKey(name: Key_grade_momdady)
  String gradeMomdady;
  @JsonKey(name: Key_carediary)
  List<CareDiary> carediary;
  @JsonKey(name: Key_community)
  List<Community> community;
  @JsonKey(name: Key_community_share)
  List<Community> communityShare;
  @JsonKey(name: Key_community_review)
  List<Community> communityReview;
  @JsonKey(name: Key_is_careservices)
  bool isCareservice;
  @JsonKey(name: Key_community_all)
  List<Community> communityAll;

  Momdadyinfo({this.gradeMomdady = '', required this.carediary, required this.community, required this.communityShare, this.isCareservice = false, required this.communityReview, required this.communityAll});

  factory Momdadyinfo.fromJson(Map<String, dynamic> json) {
    return Momdadyinfo(
      gradeMomdady: json[Key_grade_momdady] ?? '',
      carediary: json[Key_carediary] != null && json[Key_carediary].isNotEmpty ? (json[Key_carediary] as List).map((e) => CareDiary.fromJson(e)).toList() : [],
      community: json[Key_community] != null && json[Key_community].isNotEmpty ? (json[Key_community] as List).map((e) => Community.fromJson(e)).toList() : [],
      communityShare: json[Key_community_share] != null && json[Key_community_share].isNotEmpty ? (json[Key_community_share] as List).map((e) => Community.fromJson(e)).toList() : [],
      communityReview: json[Key_community_review] != null && json[Key_community_review].isNotEmpty ? (json[Key_community_review] as List).map((e) => Community.fromJson(e)).toList() : [],
      isCareservice: json[Key_is_careservices] ?? false,
      communityAll: json[Key_community_all] != null && json[Key_community_all].isNotEmpty ? (json[Key_community_all] as List).map((e) => Community.fromJson(e)).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_grade_momdady] = this.gradeMomdady;
    data[Key_carediary] = this.carediary.map((e) => e.toJson()).toList();
    data[Key_community] = this.community.map((e) => e.toJson()).toList();
    data[Key_community_share] = this.communityShare.map((e) => e.toJson()).toList();
    data[Key_is_careservices] = this.isCareservice;
    return data;
  }

  @override
  String toString() {
    return 'Momdadyinfo{gradeMomdady: $gradeMomdady, carediary: $carediary, community: $community, communityShare: $communityShare, is_careservices: $isCareservice}';
  }
}

class Community {
  static const String Key_id = 'id';
  static const String Key_category = 'category';
  static const String Key_writer_name = 'writer_name';
  static const String Key_write_addr_name = 'write_addr_name';
  static const String Key_title = 'title';
  static const String Key_community_share_images = 'community_share_images';
  static const String Key_content = 'content';
  static const String Key_updatedate = 'updatedate';
  static const String Key_share_status = 'share_status';
  static const String Key_writer_thumbnail = 'writer_thumbnail';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_category)
  String category;
  @JsonKey(name: Key_writer_name)
  String writerName;
  @JsonKey(name: Key_write_addr_name)
  String writeAddrName;
  @JsonKey(name: Key_title)
  String title;
  @JsonKey(name: Key_community_share_images)
  List<CommunityShareImages>? communityShareImages;
  @JsonKey(name: Key_content)
  String content;
  @JsonKey(name: Key_updatedate)
  String updatedate;
  @JsonKey(name: Key_share_status)
  String shareStatus;
  @JsonKey(name: Key_writer_thumbnail)
  String writerThumnail;

  Community({this.id = 0, this.category = '', this.writerName = '', this.writeAddrName = '', this.title = '', this.communityShareImages, this.content = '', this.updatedate = '', this.shareStatus = '', this.writerThumnail = ''});

  factory Community.fromJson(Map<String, dynamic> json) {
    List<CommunityShareImages> images = [];
    try {
      images = json[Key_community_share_images] != null && json[Key_community_share_images].isNotEmpty ? json[Key_community_share_images].map<CommunityShareImages>((e) => CommunityShareImages.fromJson(e)).toList() : [];
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
    return Community(
      id: json[Key_id] as int,
      category: json[Key_category] ?? '',
      writerName: json[Key_writer_name] ?? '',
      writeAddrName: json[Key_write_addr_name] ?? '',
      title: json[Key_title] ?? '',
      communityShareImages: images,
      content: json[Key_content] ?? '',
      updatedate: json[Key_updatedate] ?? '',
      shareStatus: json[Key_share_status] ?? '',
      writerThumnail: json[Key_writer_thumbnail] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_category] = this.category;
    data[Key_writer_name] = this.writerName;
    data[Key_write_addr_name] = this.writeAddrName;
    data[Key_title] = this.title;
    data[Key_community_share_images] = this.communityShareImages;
    data[Key_content] = this.content;
    data[Key_updatedate] = this.updatedate;
    data[Key_share_status] = this.shareStatus;
    data[Key_writer_thumbnail] = this.writerThumnail;
    return data;
  }

  @override
  String toString() {
    return 'Community{id: $id, category: $category, writerName: $writerName, writeAddrName: $writeAddrName, title: $title, communityShareImage: $communityShareImages, content: $content, updatedate: $updatedate, shareStatus: $shareStatus, writerThumnail: $writerThumnail}';
  }
}

class CareDiary {
  static const String Key_start_caredate = 'start_caredate';
  static const String Key_end_caredate = 'end_caredate';
  static const String Key_pay_status = 'pay_status';
  static const String Key_servicetype = 'servicetype';
  static const String Key_child_name = 'child_name';
  static const String Key_carediary_id = 'carediary_id';
  static const String Key_is_carediary = 'is_carediary';
  static const String Key_schedule_id = 'schedule_id';
  static const String Key_linkmom_thumbnail = 'linkmom_thumbnail';
  static const String Key_pay_status_text = 'pay_status_text';
  static const String Key_linkmom_name = 'linkmom_name';

  @JsonKey(name: Key_start_caredate)
  String startCaredate;
  @JsonKey(name: Key_end_caredate)
  String endCaredate;
  @JsonKey(name: Key_pay_status)
  int payStatus;
  @JsonKey(name: Key_servicetype)
  String servicetype;
  @JsonKey(name: Key_child_name)
  String childName;
  @JsonKey(name: Key_schedule_id)
  int scheduleId;
  @JsonKey(name: Key_carediary_id)
  int carediaryId;
  @JsonKey(name: Key_is_carediary)
  bool isCarediary;
  @JsonKey(name: Key_linkmom_thumbnail)
  String linkmomThumbnail;
  @JsonKey(name: Key_pay_status_text)
  String payStatusText;
  @JsonKey(name: Key_linkmom_name)
  String linkmomName;

  CareDiary({this.startCaredate = '', this.endCaredate = '', this.payStatus = 0, this.servicetype = '', this.childName = '', this.carediaryId = 0, this.isCarediary = false, this.scheduleId = 0, this.linkmomThumbnail = '', this.payStatusText = '', this.linkmomName = ''});

  factory CareDiary.fromJson(Map<String, dynamic> json) {
    return CareDiary(
      startCaredate: json[Key_start_caredate] ?? '',
      endCaredate: json[Key_end_caredate] ?? '',
      payStatus: json[Key_pay_status] ?? 0,
      servicetype: json[Key_servicetype] ?? '',
      childName: json[Key_child_name] ?? '',
      carediaryId: json[Key_carediary_id] ?? 0,
      scheduleId: json[Key_schedule_id] ?? 0,
      isCarediary: json[Key_is_carediary] ?? false,
      linkmomThumbnail: json[Key_linkmom_thumbnail] ?? '',
      payStatusText: json[Key_pay_status_text] ?? '',
      linkmomName: json[Key_linkmom_name] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_start_caredate] = this.startCaredate;
    data[Key_end_caredate] = this.endCaredate;
    data[Key_pay_status] = this.payStatus;
    data[Key_servicetype] = this.servicetype;
    data[Key_child_name] = this.childName;
    data[Key_carediary_id] = this.carediaryId;
    data[Key_schedule_id] = this.scheduleId;
    data[Key_is_carediary] = this.isCarediary;
    data[Key_linkmom_thumbnail] = this.linkmomThumbnail;
    data[Key_pay_status_text] = this.payStatusText;
    data[Key_linkmom_name] = this.linkmomName;
    return data;
  }

  @override
  String toString() {
    return [
      'Carediary {',
      'startCaredate: $startCaredate,',
      'endCaredate: $endCaredate, ',
      'payStatus: $payStatus, ',
      'servicetype: $servicetype, ',
      'childName: $childName, ',
      'carediaryId: $carediaryId, ',
      'scheduleId: $scheduleId, ',
      'isCarediary: $isCarediary, ',
      'linkmomThumbnail: $linkmomThumbnail, ',
      'payStatusText: $payStatusText, ',
      'linkmomName: $linkmomName ',
      '}'
    ].toString();
  }
}

class Userinfo {
  static const String Key_first_name = 'first_name';
  static const String Key_gender = 'gender';
  static const String Key_age = 'age';
  static const String Key_job = 'job';
  static const String Key_profileimg = 'profileimg';
  static const String Key_is_user_momdady = 'is_user_momdady';
  static const String Key_is_user_linkmom = 'is_user_linkmom';
  static const String Key_auth_address = 'auth_address';
  static const String Key_is_auth_homestate = 'is_auth_homestate';
  static const String Key_is_auth_email = 'is_auth_email';

  @JsonKey(name: Key_first_name)
  String firstName;
  @JsonKey(name: Key_gender)
  String gender;
  @JsonKey(name: Key_age)
  int age;
  @JsonKey(name: Key_job)
  String job;
  @JsonKey(name: Key_profileimg)
  String profileimg;
  @JsonKey(name: Key_is_user_momdady)
  bool isUserMomdady;
  @JsonKey(name: Key_is_user_linkmom)
  bool isUserLinkmom;
  @JsonKey(name: Key_auth_address)
  MyInfoAddressItem? authAddress;
  @JsonKey(name: Key_is_auth_homestate)
  bool isAuthHomestate;
  @JsonKey(name: Key_is_auth_email)
  bool is_auth_email;

  Userinfo({
    this.firstName = '',
    this.gender = '',
    this.age = 0,
    this.job = '',
    this.profileimg = '',
    this.isUserMomdady = true,
    this.isUserLinkmom = false,
    this.authAddress,
    this.isAuthHomestate = false,
    this.is_auth_email = false,
  });

  factory Userinfo.fromJson(Map<String, dynamic> json) {
    return Userinfo(
      firstName: json[Key_first_name] ?? '',
      gender: json[Key_gender] ?? '',
      age: json[Key_age] as int,
      job: json[Key_job] ?? '',
      profileimg: json[Key_profileimg] ?? '',
      isUserMomdady: json[Key_is_user_momdady] ?? false,
      isUserLinkmom: json[Key_is_user_linkmom] ?? false,
      authAddress: json[Key_auth_address] != null ? MyInfoAddressItem.fromJson(json[Key_auth_address]) : MyInfoAddressItem(),
      isAuthHomestate: json[Key_is_auth_homestate] ?? false,
      is_auth_email: json[Key_is_auth_email] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_first_name] = this.firstName;
    data[Key_gender] = this.gender;
    data[Key_age] = this.age;
    data[Key_job] = this.job;
    data[Key_profileimg] = this.profileimg;
    data[Key_is_user_momdady] = this.isUserMomdady;
    data[Key_is_user_linkmom] = this.isUserLinkmom;
    data[Key_auth_address] = this.authAddress;
    data[Key_is_auth_homestate] = this.isAuthHomestate;
    data[Key_is_auth_email] = this.is_auth_email;
    return data;
  }

  @override
  String toString() {
    return 'Userinfo{firstName: $firstName, gender: $gender, age: $age, job: $job, profileimg: $profileimg, isUserMomdady: $isUserMomdady, isUserLinkmom: $isUserLinkmom, authAddress: $authAddress, isAuthHomestate: $isAuthHomestate, is_auth_email: $is_auth_email}';
  }
}
