import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///
/// Created by   : platformbiz@ggumbi.com
///
/// version      : 1.0.0
///
/// see          : pay_init_request.dart - 결제 요청 전 POST
///
/// since        : 2021/09/08 / update:
///
/// (0, '계약중'),
///
/// (11, '계약실패(거절/취소/응답없음/시간만료 등)'),
///
/// (21, '계약완료 후 취소(부분)'),
///
/// (22, '계약완료 후 취소(전체)'),
///
/// (92, '매칭성공(계약완료/결제대기중)'),
///
/// (100, '결제완료')
///
///  "msg_cd": 3700, "message": "결제 내역 전송 등을 위해 이메일 인증이 필요합니다. 이메일 인증을 해주세요.",
///
///  "msg_cd": 5000, "message": "계약이 완료 되지 않았습니다. 계약 완료 후 결제를 해주세요.",
///
///  "msg_cd": 5100, "message": "결제 완료된 계약 입니다.",
///
///  "msg_cd": 5011,"message": "이미 취소된 계약 입니다.",
///
///  "msg_cd": 4000, "message": "데이터가 없습니다.",
@JsonSerializable()
class PayInitRequest {
  static const String Key_bookingcareservices = 'bookingcareservices';
  static const String Key_carematching = 'carematching';
  static const String Key_carecontract = 'carecontract';

  PayInitRequest({
    required this.bookingcareservices,
    required this.carecontract,
    required this.carematching,
  });

  ///돌봄신청서ID (booking_id)
  @JsonKey(name: Key_bookingcareservices)
  final int bookingcareservices;

  ///매칭ID (matching_id) - 있는 경우만, 없으면 Null
  @JsonKey(name: Key_carematching)
  final int carematching;

  ///계약서번호 (contract_id)
  @JsonKey(name: Key_carecontract)
  final int carecontract;

  factory PayInitRequest.fromJson(Map<String, dynamic> json) {
    return PayInitRequest(
      bookingcareservices: json[Key_bookingcareservices] as int,
      carematching: json[Key_carematching] as int,
      carecontract: json[Key_carecontract] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bookingcareservices] = this.bookingcareservices;
    data[Key_carematching] = this.carematching;
    data[Key_carecontract] = this.carecontract;
    return data;
  }

  @override
  String toString() {
    return 'ChatMessageListRequest{bookingcareservices: $bookingcareservices, carematching: $carematching, carecontract: $carecontract}';
  }
}
