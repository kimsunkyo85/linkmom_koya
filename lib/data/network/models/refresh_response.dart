import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

@JsonSerializable()
class RefreshResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  RefreshResponse({
    this.dataHeader,
    this.dataList,
  });

  RefreshData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : RefreshData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory RefreshResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<RefreshData> datas = [];
    try {
      datas = json[Key_data].map<RefreshData>((i) => RefreshData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return RefreshResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'RefreshResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class RefreshData {
  static const String Key_access = "access";

  RefreshData({
    this.access = ''
  });

  @JsonKey(name: Key_access)
  final String access;

  factory RefreshData.fromJson(Map<String, dynamic> json) {
    return RefreshData(
      access: json[Key_access] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_access] = this.access;
    return data;
  }

  @override
  String toString() {
    return 'RefreshData{access: $access}';
  }
}
