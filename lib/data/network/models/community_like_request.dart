import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CommunityLikeRequest {
  static const String Key_share_id = 'share_id';
  @JsonKey(name: Key_share_id)
  int shareId;

  CommunityLikeRequest({this.shareId = 0});

  factory CommunityLikeRequest.fromJson(Map<String, dynamic> json) {
    return CommunityLikeRequest(shareId: json[Key_share_id]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_share_id] = this.shareId;
    return data;
  }

  @override
  String toString() {
    return 'CommunityLikeRequest: {shareId: $shareId}';
  }
}
