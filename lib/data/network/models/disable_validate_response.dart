import 'package:linkmom/data/network/models/header_response.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class DisableValidateResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<DisableData>? dataList;

  DisableValidateResponse({this.dataHeader, this.dataList});

  factory DisableValidateResponse.fromJson(Map<String, dynamic> json) {
    return DisableValidateResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? [] : (json[Key_data] as List).map((e) => DisableData.fromJson(e)).toList(),
    );
  }

  @override
  int getCode() => dataHeader!.getCode();

  @override
  String getMsg() => dataHeader!.getMsg();

  @override
  DisableData getData() => dataList!.isNotEmpty ? dataList!.first : DisableData();

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList?.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'DisableValidateResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class DisableData {
  static const String Key_account_holder = 'account_holder';
  static const String Key_link_cash = 'link_cash';
  static const String Key_link_point = 'link_point';
  static const String Key_to_give_cash = 'to_give_cash';
  static const String Key_take_cash = 'take_cash';
  static const String Key_to_give_claim = 'to_give_claim';
  static const String Key_take_claim = 'take_claim';
  static const String Key_reason = 'reason';
  static const String Key_account_bank = 'account_bank';
  static const String Key_is_care_ing = 'is_care_ing';

  @JsonKey(name: Key_account_holder)
  String accountHolder;

  /// cash
  @JsonKey(name: Key_link_cash)
  int linkCash;

  /// point
  @JsonKey(name: Key_link_point)
  int linkPoint;

  /// 정산필요금액
  @JsonKey(name: Key_to_give_cash)
  int toGiveCash;

  /// 정산받을 금액
  @JsonKey(name: Key_take_cash)
  int takeCash;

  /// momdaddy - 정산보류
  @JsonKey(name: Key_to_give_claim)
  bool toGiveClaim;

  /// linkmom - 정산보류
  @JsonKey(name: Key_take_claim)
  bool takeClaim;
  @JsonKey(name: Key_reason)
  List<DisableReason>? reason;
  @JsonKey(name: Key_account_bank)
  List<AccountBank>? accountBank;

  /// remain care schedule-> true / empty -> false
  @JsonKey(name: Key_is_care_ing)
  bool isCareIng;

  DisableData({
    this.accountHolder = '',
    this.linkCash = 0,
    this.linkPoint = 0,
    this.toGiveCash = 0,
    this.takeCash = 0,
    this.toGiveClaim = false,
    this.takeClaim = false,
    this.reason,
    this.accountBank,
    this.isCareIng = false,
  });

  factory DisableData.fromJson(Map<String, dynamic> json) {
    return DisableData(
      accountHolder: json[Key_account_holder] ?? '',
      linkCash: json[Key_link_cash] ?? 0,
      linkPoint: json[Key_link_point] ?? 0,
      toGiveCash: json[Key_to_give_cash] ?? 0,
      takeCash: json[Key_take_cash] ?? 0,
      toGiveClaim: json[Key_to_give_claim] ?? false,
      takeClaim: json[Key_take_claim] ?? false,
      reason: json[Key_reason] != null && json[Key_reason].isNotEmpty ? json[Key_reason].map<DisableReason>((e) => DisableReason.fromJson(e)).toList() : [],
      accountBank: json[Key_account_bank] != null && json[Key_account_bank].isNotEmpty ? json[Key_account_bank].map<AccountBank>((e) => AccountBank.fromJson(e)).toList() : [],
      isCareIng: json[Key_is_care_ing] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_account_holder] = this.accountHolder;
    data[Key_link_cash] = this.linkCash;
    data[Key_link_point] = this.linkPoint;
    data[Key_to_give_cash] = this.toGiveCash;
    data[Key_take_cash] = this.takeCash;
    data[Key_to_give_claim] = this.toGiveClaim;
    data[Key_take_claim] = this.takeClaim;
    if (this.reason != null) data[Key_reason] = this.reason;
    data[Key_is_care_ing] = this.isCareIng;
    return data;
  }

  @override
  String toString() {
    return 'DisableData{accountHolder: $accountHolder, linkCash: $linkCash, linkPoint: $linkPoint, toGiveCash: $toGiveCash, takeCash: $takeCash, toGiveClaim: $toGiveClaim, takeClaim: $takeClaim, reason: $reason, isCareIng: $isCareIng}';
  }
}

@JsonSerializable()
class AccountBank {
  static const String Key_bankname = 'bankname';
  static const String Key_id = 'id';

  AccountBank({this.bankname = '', this.id = ''});

  @JsonKey(name: Key_bankname)
  final String bankname;
  @JsonKey(name: Key_id)
  final String id;

  factory AccountBank.fromJson(Map<String, dynamic> json) {
    return AccountBank(
      bankname: json[Key_bankname] ?? '',
      id: json[Key_id] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_bankname] = this.bankname;
    data[Key_id] = this.id;
    return data;
  }

  @override
  String toString() {
    return 'AccountBack{bankname: $bankname, id: $id}';
  }
}

@JsonSerializable()
class DisableReason {
  static const String Key_id = 'id';
  static const String Key_text = 'text';

  DisableReason({this.id = 0, this.text = ''});

  @JsonKey(name: Key_id)
  final int id;

  @JsonKey(name: Key_text)
  final String text;

  factory DisableReason.fromJson(Map<String, dynamic> json) {
    return DisableReason(
      id: json[Key_id] ?? 0,
      text: json[Key_text] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_text] = this.text;
    return data;
  }

  @override
  String toString() {
    return 'DiasbleReason{id: $id, text: $text}';
  }
}
