import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import 'carediary_form_response.dart';
import 'data/care_schedul_info_data.dart';

@JsonSerializable()
class PayListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<PayList> dataList;

  PayListResponse({this.dataHeader, required this.dataList});

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.message;

  PayList getData() => dataList.isNotEmpty ? dataList.first : PayList(results: []);

  factory PayListResponse.fromJson(Map<String, dynamic> json) {
    return PayListResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] != null && json[Key_data].isNotEmpty ? (json[Key_data] as List).map((e) => PayList.fromJson(e)).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_data] = this.dataList.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'PaymentListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class PayList {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<PayData> results;

  PayList({this.count = 0, this.next = '', this.previous = '', required this.results});

  factory PayList.fromJson(Map<String, dynamic> json) {
    return PayList(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] == null ? [] : (json[Key_results] as List).map((e) => PayData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    if (this.results.isNotEmpty) data[Key_results] = this.results.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'PaymentList{count: $count, next: $next, previous: $previous, results: $results}';
  }
}

class PayData {
  static const String Key_payment_id = 'payment_id';
  static const String Key_imp_uid = 'imp_uid';
  static const String Key_contract_id = 'contract_id';
  static const String Key_order_id = 'order_id';
  static const String Key_pay_method = 'pay_method';
  static const String Key_pg_provider = 'pg_provider';
  static const String Key_card_name = 'card_name';
  static const String Key_card_number = 'card_number';
  static const String Key_name = 'name';
  static const String Key_product_price = 'product_price';
  static const String Key_used_cash = 'used_cash';
  static const String Key_used_point = 'used_point';
  static const String Key_used_pg = 'used_pg';
  static const String Key_paid_at = 'paid_at';
  static const String Key_status = 'status';
  static const String Key_receipt_url = 'receipt_url';
  static const String Key_is_verify = 'is_verify';
  static const String Key_paymentcancel = 'paymentcancel';
  static const String Key_care_scheduleinfo = 'care_scheduleinfo';
  static const String Key_childinfo = 'childinfo';
  static const String Key_matchinginfo = 'matchinginfo';

  @JsonKey(name: Key_payment_id)
  int paymentId;
  @JsonKey(name: Key_imp_uid)
  String impUid;
  @JsonKey(name: Key_contract_id)
  int contractId;
  @JsonKey(name: Key_order_id)
  String orderId;
  @JsonKey(name: Key_pay_method)
  String payMethod;
  @JsonKey(name: Key_pg_provider)
  String pgProvider;
  @JsonKey(name: Key_card_name)
  String cardName;
  @JsonKey(name: Key_card_number)
  String cardNumber;
  @JsonKey(name: Key_name)
  String name;
  @JsonKey(name: Key_product_price)
  int productPrice;
  @JsonKey(name: Key_used_cash)
  int usedCash;
  @JsonKey(name: Key_used_point)
  int usedPoint;
  @JsonKey(name: Key_used_pg)
  int usedPg;
  @JsonKey(name: Key_paid_at)
  String paidAt;
  @JsonKey(name: Key_status)
  String status;
  @JsonKey(name: Key_receipt_url)
  String receiptUrl;
  @JsonKey(name: Key_is_verify)
  bool isVerify;
  @JsonKey(name: Key_paymentcancel)
  List<PaymentCancel>? paymentCancel;
  @JsonKey(name: Key_care_scheduleinfo)
  CareScheduleInfoData? careScheduleinfo;
  @JsonKey(name: Key_childinfo)
  Childinfo? childinfo;
  @JsonKey(name: Key_matchinginfo)
  MatchingInfo? matchinginfo;

  PayData({
    this.paymentId = 0,
    this.impUid = '',
    this.contractId = 0,
    this.orderId = '',
    this.payMethod = '',
    this.pgProvider = '',
    this.cardName = '',
    this.cardNumber = '',
    this.name = '',
    this.productPrice = 0,
    this.usedCash = 0,
    this.usedPoint = 0,
    this.usedPg = 0,
    this.paidAt = '',
    this.status = '',
    this.receiptUrl = '',
    this.isVerify = false,
    this.paymentCancel,
    this.careScheduleinfo,
    this.childinfo,
    this.matchinginfo,
  });

  factory PayData.fromJson(Map<String, dynamic> json) {
    return PayData(
      paymentId: json[Key_payment_id] ?? 0,
      impUid: json[Key_imp_uid] ?? '',
      contractId: json[Key_contract_id] ?? 0,
      orderId: json[Key_order_id] ?? '',
      payMethod: json[Key_pay_method] ?? '',
      pgProvider: json[Key_pg_provider] ?? '',
      cardName: json[Key_card_name] ?? '',
      cardNumber: json[Key_card_number] ?? '',
      name: json[Key_name] ?? '',
      productPrice: json[Key_product_price] ?? 0,
      usedCash: json[Key_used_cash] ?? 0,
      usedPoint: json[Key_used_point] ?? 0,
      usedPg: json[Key_used_pg] ?? 0,
      paidAt: json[Key_paid_at] ?? '',
      status: json[Key_status] ?? '',
      receiptUrl: json[Key_receipt_url] ?? '',
      isVerify: json[Key_is_verify] ?? false,
      paymentCancel: json[Key_paymentcancel] == null ? [] : (json[Key_paymentcancel] as List).map((e) => PaymentCancel.fromJson(e)).toList(),
      careScheduleinfo: json[Key_care_scheduleinfo] != null ? CareScheduleInfoData.fromJson(json[Key_care_scheduleinfo]) : CareScheduleInfoData(caredate: []),
      childinfo: json[Key_childinfo] != null ? Childinfo.fromJson(json[Key_childinfo]) : Childinfo(),
      matchinginfo: json[Key_matchinginfo] != null ? MatchingInfo.fromJson(json[Key_matchinginfo]) : MatchingInfo(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_payment_id] = this.paymentId;
    data[Key_imp_uid] = this.impUid;
    data[Key_contract_id] = this.contractId;
    data[Key_order_id] = this.orderId;
    data[Key_pay_method] = this.payMethod;
    data[Key_pg_provider] = this.pgProvider;
    data[Key_card_name] = this.cardName;
    data[Key_card_number] = this.cardNumber;
    data[Key_name] = this.name;
    data[Key_product_price] = this.productPrice;
    data[Key_used_cash] = this.usedCash;
    data[Key_used_point] = this.usedPoint;
    data[Key_used_pg] = this.usedPg;
    data[Key_paid_at] = this.paidAt;
    data[Key_status] = this.status;
    data[Key_receipt_url] = this.receiptUrl;
    data[Key_is_verify] = this.isVerify;
    if (this.paymentCancel != null) data[Key_paymentcancel] = this.paymentCancel?.map((e) => e.toJson()).toList();
    if (this.careScheduleinfo != null) data[Key_care_scheduleinfo] = this.careScheduleinfo;
    if (this.childinfo != null) data[Key_childinfo] = this.childinfo;
    if (this.matchinginfo != null) data[Key_matchinginfo] = this.matchinginfo;
    return data;
  }

  @override
  String toString() {
    return 'PaymentData{paymentId $paymentId,impUid $impUid,contractId $contractId,orderId $orderId,payMethod $payMethod,pgProvider $pgProvider,cardName $cardName,cardNumber $cardNumber,name $name,productPrice $productPrice,usedCash $usedCash,usedPoint $usedPoint,usedPg $usedPg,paidAt $paidAt,status $status,receiptUrl $receiptUrl,isVerify $isVerify,cancelData $paymentCancel}';
  }
}

class PaymentCancel {
  static const String Key_cancel_id = 'cancel_id';
  static const String Key_request_cancel_price = 'request_cancel_price';
  static const String Key_refund_cash = 'refund_cash';
  static const String Key_refund_point = 'refund_point';
  static const String Key_cancelled_price = 'cancelled_price';
  static const String Key_revoked_at = 'revoked_at';
  static const String Key_createdate = 'createdate';
  static const String Key_is_refund = 'is_refund';

  @JsonKey(name: Key_cancel_id)
  int cancelId;
  @JsonKey(name: Key_request_cancel_price)
  int requestCancelPrice;
  @JsonKey(name: Key_refund_cash)
  int refundCash;
  @JsonKey(name: Key_refund_point)
  int refundPoint;
  @JsonKey(name: Key_cancelled_price)
  int cancelledPrice;
  @JsonKey(name: Key_is_refund)
  bool isRefund;
  @JsonKey(name: Key_revoked_at)
  String revokedAt;
  @JsonKey(name: Key_createdate)
  String createdate;

  PaymentCancel({this.cancelId = 0, this.requestCancelPrice = 0, this.refundCash = 0, this.refundPoint = 0, this.cancelledPrice = 0, this.isRefund = false, this.revokedAt = '', this.createdate = ''});

  factory PaymentCancel.fromJson(Map<String, dynamic> json) {
    return PaymentCancel(
      cancelId: json[Key_cancel_id] ?? 0,
      requestCancelPrice: json[Key_request_cancel_price] ?? 0,
      refundCash: json[Key_refund_cash] ?? 0,
      refundPoint: json[Key_refund_point] ?? 0,
      cancelledPrice: json[Key_cancelled_price] ?? 0,
      isRefund: json[Key_is_refund] ?? 0,
      revokedAt: json[Key_revoked_at] ?? '',
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_cancel_id] = this.cancelId;
    data[Key_request_cancel_price] = this.requestCancelPrice;
    data[Key_refund_cash] = this.refundCash;
    data[Key_refund_point] = this.refundPoint;
    data[Key_cancelled_price] = this.cancelledPrice;
    data[Key_is_refund] = this.isRefund;
    data[Key_revoked_at] = this.revokedAt;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'CancelData{cancelId: $cancelId, requestCancelPrice: $requestCancelPrice, refundCash: $refundCash, refundPoint: $refundPoint, cancelledPrice: $cancelledPrice, isRefund: $isRefund, revokedAt: $revokedAt, createdate: $createdate}';
  }
}

class MatchingInfo {
  static const String Key_matching_id = 'matching_id';
  static const String Key_momdady = 'momdady';
  static const String Key_linkmom = 'linkmom';
  static const String Key_createdate = 'createdate';
  MatchingInfo({
    this.matchingId = 0,
    this.momdady = 0,
    this.linkmom = 0,
    this.createdate = '',
  });

  final int matchingId;
  final int momdady;
  final int linkmom;
  final String createdate;

  factory MatchingInfo.fromJson(Map<String, dynamic> json) => MatchingInfo(
        matchingId: json[Key_matching_id] ?? 0,
        momdady: json[Key_momdady] ?? 0,
        linkmom: json[Key_linkmom] ?? 0,
        createdate: json[Key_createdate] ?? '',
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_matching_id] = matchingId;
    data[Key_momdady] = momdady;
    data[Key_linkmom] = linkmom;
    data[Key_createdate] = createdate;
    return data;
  }
}
