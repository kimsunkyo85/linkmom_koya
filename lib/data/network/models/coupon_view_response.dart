import 'package:json_annotation/json_annotation.dart';

import 'header_response.dart';

class CouponViewResponse {
  HeaderResponse? dataHeader;
  List<CouponViewData>? dataList;

  CouponViewResponse({this.dataHeader, this.dataList});

  CouponViewData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CouponViewData();

  int getCode() => dataHeader!.getCode();

  factory CouponViewResponse.fromJson(Map<String, dynamic> json) {
    return CouponViewResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? [] : (json[Key_data] as List).map((e) => CouponViewData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }
}

class CouponViewData {
  static const Key_id = 'id';
  static const Key_coupon_brand = 'coupon_brand';
  static const Key_coupon_brand_bi = 'coupon_brand_bi';
  static const Key_coupon_brand_site = 'coupon_brand_site';
  static const Key_coupon_name = 'coupon_name';
  static const Key_coupon_code = 'coupon_code';
  static const Key_status = 'status';
  static const Key_use_period_sdate = 'use_period_sdate';
  static const Key_use_period_edate = 'use_period_edate';
  static const Key_coupon_use_cnt_info = 'coupon_use_cnt_info';
  static const Key_coupon_info = 'coupon_info';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_coupon_brand)
  String couponBrand;
  @JsonKey(name: Key_coupon_brand_bi)
  String couponBrandBi;
  @JsonKey(name: Key_coupon_brand_site)
  String couponBrandSite;
  @JsonKey(name: Key_coupon_name)
  String couponName;
  @JsonKey(name: Key_coupon_code)
  String couponCode;
  @JsonKey(name: Key_status)
  String status;
  @JsonKey(name: Key_use_period_sdate)
  String usePeriodSdate;
  @JsonKey(name: Key_use_period_edate)
  String usePeriodEdate;
  @JsonKey(name: Key_coupon_use_cnt_info)
  String couponUseCntInfo;
  @JsonKey(name: Key_coupon_info)
  String couponInfo;

  CouponViewData(
      {this.id = 0,
      this.couponBrand = '',
      this.couponBrandBi = '',
      this.couponBrandSite = '',
      this.couponName = '',
      this.couponCode = '',
      this.status = '',
      this.usePeriodSdate = '',
      this.usePeriodEdate = '',
      this.couponUseCntInfo = '',
      this.couponInfo = ''});

  factory CouponViewData.fromJson(Map<String, dynamic> json) {
    return CouponViewData(
      id: json[Key_id] = 0,
      couponBrand: json[Key_coupon_brand] ?? '',
      couponBrandBi: json[Key_coupon_brand_bi] ?? '',
      couponBrandSite: json[Key_coupon_brand_site] ?? '',
      couponName: json[Key_coupon_name] ?? '',
      couponCode: json[Key_coupon_code] ?? '',
      status: json[Key_status] ?? '',
      usePeriodSdate: json[Key_use_period_sdate] ?? '',
      usePeriodEdate: json[Key_use_period_edate] ?? '',
      couponUseCntInfo: json[Key_coupon_use_cnt_info] ?? '',
      couponInfo: json[Key_coupon_info] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_coupon_brand] = this.couponBrand;
    data[Key_coupon_brand_bi] = this.couponBrandBi;
    data[Key_coupon_brand_site] = this.couponBrandSite;
    data[Key_coupon_name] = this.couponName;
    data[Key_coupon_code] = this.couponCode;
    data[Key_status] = this.status;
    data[Key_use_period_sdate] = this.usePeriodSdate;
    data[Key_use_period_edate] = this.usePeriodEdate;
    data[Key_coupon_use_cnt_info] = this.couponUseCntInfo;
    data[Key_coupon_info] = this.couponInfo;
    return data;
  }

  @override
  String toString() {
    return 'CouponData {${{
      'id': id,
      'couponBrand': couponBrand,
      'couponBrandBi': couponBrandBi,
      'couponBrandSite': couponBrandSite,
      'couponName': couponName,
      'couponCode': couponCode,
      'status': status,
      'usePeriodSdate': usePeriodSdate,
      'usePeriodEdate': usePeriodEdate,
      'couponUseCntInfo': couponUseCntInfo,
      'couponInfo': couponInfo,
    }}}';
  }
}
