import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

import 'temp_area_response.dart';

@JsonSerializable()
class ReqData {
  ///입력된 시급(원)
  static const String Key_cost_perhour = 'cost_perhour';

  ///"보육비(원)" → 입력된 시간(분) X 시급 / 60 * 10분
  static const String Key_cost_hoursum = 'cost_hoursum';

  ///협의가능 협의용 금액 0:협의안함, 1:협의가능
  static const String Key_cost_negotiable = 'cost_negotiable';

  ///비용합계 → 보육비+스피드매칭+긴급
  static const String Key_cost_sum = 'cost_sum';

  ///10분당 단가,  → 전체금액 합계 / 60 * 10분
  static const String Key_cost_avg = 'cost_avg';

  ///비용협의가능 0:협의안함, 1:협의가능
  static const String Key_is_negotiable = 'is_negotiable';

  ///그룹보육 동의 0:협의안함, 1:협의가능
  static const String Key_is_togetherboyuk = 'is_togetherboyuk';

  ///인증된 주소(신청서 작성시 사용)
  static const String Key_addr_code = 'addr_code';

  ///돌봄 신청 날짜
  static const String Key_careschedule = 'careschedule';

  ///등원 서비스
  static const String Key_bookinggotoschool = 'bookinggotoschool';

  ///하원 서비스
  static const String Key_bookingafterschool = 'bookingafterschool';

  ///보육 서비스
  static const String Key_bookingboyuk = 'bookingboyuk';

  ///보육 옵션
  static const String Key_bookingboyukoptions = 'bookingboyukoptions';

  ///놀이 옵션
  static const String Key_bookingplayoptions = 'bookingplayoptions';

  ///가사 옵션
  static const String Key_bookinghomecareoptions = 'bookinghomecareoptions';

  ///출발장소 선택(보육 포함)
  static const String Key_temp_area_start = 'temp_area_start';

  ///도착장소 선택(보육 포함)
  static const String Key_temp_area_end = 'temp_area_end';

  ReqData({
    this.cost_perhour = 0,
    this.cost_hoursum = 0,
    this.cost_negotiable = 0,
    this.cost_sum = 0,
    this.cost_avg = 0,
    this.is_negotiable = 0,
    this.is_togetherboyuk = 0,
    this.careschedule,
    this.bookinggotoschool,
    this.bookingboyukoptions,
    this.bookingboyuk,
    this.bookingafterschool,
    this.bookingplayoptions,
    this.bookinghomecareoptions,
    this.addr_code,
    this.temp_area_start,
    this.temp_area_end,
  });

  ReqData.init() {
    careschedule = [];
    bookingboyuk = [];
    bookinggotoschool = [];
    bookingafterschool = [];
    bookingboyukoptions = [];
    bookingplayoptions = [];
    bookinghomecareoptions = [];
    addr_code = AddrCodeData();
    temp_area_start = TempAreaData();
    temp_area_end = TempAreaData();
  }

  ///시간당 단가 (시급)
  @JsonKey(name: Key_cost_perhour)
  int cost_perhour = 0;

  ///기간 X 시급(보육비) X 신청시간
  @JsonKey(name: Key_cost_hoursum)
  int cost_hoursum = 0;

  ///협의가능 협의용 금액 0:협의안함, 1:협의가능
  @JsonKey(name: Key_cost_negotiable)
  int cost_negotiable = 0;

  ///비용합계 → 시급(보육비)+스피드매칭+긴급+cost_negotiable
  @JsonKey(name: Key_cost_sum)
  int cost_sum = 0;

  ///10분당 단가, 전체금액 합계 ((cost_perhour X 신청시간) + cost_negotiable) / 6
  @JsonKey(name: Key_cost_avg)
  int cost_avg = 0;

  ///비용협의가능 0:협의안함, 1:협의가능
  @JsonKey(name: Key_is_negotiable)
  int is_negotiable = 0;

  ///그룹보육 동의 0:협의안함, 1:협의가능
  @JsonKey(name: Key_is_togetherboyuk)
  int is_togetherboyuk = 0;

  ///돌봄 신청 날자
  @JsonKey(name: Key_careschedule)
  late List<ReqScheduleData>? careschedule;

  ///등원 서비스
  @JsonKey(name: Key_bookinggotoschool)
  late List<ReqSchoolToHomeData>? bookinggotoschool;

  ///하원 서비스
  @JsonKey(name: Key_bookingafterschool)
  late List<ReqSchoolToHomeData>? bookingafterschool;

  ///보육 서비스
  @JsonKey(name: Key_bookingboyuk)
  late List<ReqBoyukData>? bookingboyuk;

  ///보육 옵션
  @JsonKey(name: Key_bookingboyukoptions)
  late List<ReqCareOptionData>? bookingboyukoptions;

  ///놀이 옵션
  @JsonKey(name: Key_bookingplayoptions)
  late List<ReqCareOptionData>? bookingplayoptions;

  ///가사 옵션
  @JsonKey(name: Key_bookinghomecareoptions)
  late List<ReqCareOptionData>? bookinghomecareoptions;

  @JsonKey(name: Key_addr_code)
  AddrCodeData? addr_code;

  @JsonKey(name: Key_temp_area_start)
  TempAreaData? temp_area_start;

  @JsonKey(name: Key_temp_area_end)
  TempAreaData? temp_area_end;

  factory ReqData.fromJson(Map<String, dynamic> json) {
    List<ReqScheduleData> careschedule = json[Key_careschedule] == null ? [] : json[Key_careschedule].map<ReqScheduleData>((i) => ReqScheduleData.fromJson(i)).toList();
    List<ReqSchoolToHomeData> bookinggotoschool = json[Key_bookinggotoschool] == null ? [] : json[Key_bookinggotoschool].map<ReqSchoolToHomeData>((i) => ReqSchoolToHomeData.fromJson(i)).toList();
    List<ReqSchoolToHomeData> bookingafterschool = json[Key_bookingafterschool] == null ? [] : json[Key_bookingafterschool].map<ReqSchoolToHomeData>((i) => ReqSchoolToHomeData.fromJson(i)).toList();
    List<ReqBoyukData> bookingboyuk = json[Key_bookingboyuk] == null ? [] : json[Key_bookingboyuk].map<ReqBoyukData>((i) => ReqBoyukData.fromJson(i)).toList();
    List<ReqCareOptionData> bookingboyukoptions = json[Key_bookingboyukoptions] == null ? [] : json[Key_bookingboyukoptions].map<ReqCareOptionData>((i) => ReqCareOptionData.fromJson(i)).toList();
    List<ReqCareOptionData> bookingplayoptions = json[Key_bookingplayoptions] == null ? [] : json[Key_bookingplayoptions].map<ReqCareOptionData>((i) => ReqCareOptionData.fromJson(i)).toList();
    List<ReqCareOptionData> bookinghomecareoptions = json[Key_bookinghomecareoptions] == null ? [] : json[Key_bookinghomecareoptions].map<ReqCareOptionData>((i) => ReqCareOptionData.fromJson(i)).toList();
    AddrCodeData addrCode = json[Key_addr_code] == null ? AddrCodeData() : AddrCodeData.fromJson(json[Key_addr_code]);
    TempAreaData tempAreaStart = json[Key_temp_area_end] == null ? TempAreaData() : TempAreaData.fromJson(json[Key_temp_area_start]);
    TempAreaData tempAreaEnd = json[Key_temp_area_end] == null ? TempAreaData() : TempAreaData.fromJson(json[Key_temp_area_end]);
    return ReqData(
      cost_perhour: json[Key_cost_perhour] as int,
      cost_hoursum: json[Key_cost_hoursum] as int,
      cost_negotiable: json[Key_cost_negotiable] as int,
      cost_sum: json[Key_cost_sum] as int,
      cost_avg: json[Key_cost_avg] as int,
      is_negotiable: json[Key_is_negotiable] as int,
      is_togetherboyuk: json[Key_is_togetherboyuk] as int,
      careschedule: careschedule,
      bookinggotoschool: bookinggotoschool,
      bookingafterschool: bookingafterschool,
      bookingboyuk: bookingboyuk,
      bookingboyukoptions: bookingboyukoptions,
      bookingplayoptions: bookingplayoptions,
      bookinghomecareoptions: bookinghomecareoptions,
      addr_code: addrCode,
      temp_area_start: tempAreaStart,
      temp_area_end: tempAreaEnd,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_cost_perhour] = this.cost_perhour;
    data[Key_cost_hoursum] = this.cost_hoursum;
    data[Key_cost_negotiable] = this.cost_negotiable;
    data[Key_cost_sum] = this.cost_sum;
    data[Key_cost_avg] = this.cost_avg;
    data[Key_is_negotiable] = this.is_negotiable;
    data[Key_is_togetherboyuk] = this.is_togetherboyuk;
    data[Key_careschedule] = this.careschedule;
    data[Key_bookinggotoschool] = this.bookinggotoschool;
    data[Key_bookingafterschool] = this.bookingafterschool;
    data[Key_bookingboyuk] = this.bookingboyuk;
    data[Key_bookingboyukoptions] = this.bookingboyukoptions;
    data[Key_bookingplayoptions] = this.bookingplayoptions;
    data[Key_bookinghomecareoptions] = this.bookinghomecareoptions;
    data[Key_addr_code] = this.addr_code;
    data[Key_temp_area_start] = this.temp_area_start;
    data[Key_temp_area_end] = this.temp_area_end;
    return data;
  }

  ReqData clone() {
    final jsonResponse = json.decode(json.encode(this));
    return ReqData.fromJson(jsonResponse as Map<String, dynamic>);
  }

  @override
  String toString() {
    return 'ReqData{cost_perhour: $cost_perhour, cost_hoursum: $cost_hoursum, cost_negotiable: $cost_negotiable, cost_sum: $cost_sum, cost_avg: $cost_avg, is_negotiable: $is_negotiable, is_togetherboyuk: $is_togetherboyuk, careschedule: $careschedule, bookinggotoschool: $bookinggotoschool, bookingafterschool: $bookingafterschool, bookingboyuk: $bookingboyuk, bookingboyukoptions: $bookingboyukoptions, bookingplayoptions: $bookingplayoptions, bookinghomecareoptions: $bookinghomecareoptions, addr_code: $addr_code, temp_area_start: $temp_area_start, temp_area_end: $temp_area_end}';
  }
}

///스케쥴 정보
class ReqScheduleData {
  static const String Key_id = 'id';
  static const String Key_stime = 'stime';
  static const String Key_etime = 'etime';
  static const String Key_is_am = 'is_am';
  static const String Key_is_pm = 'is_pm';
  static const String Key_is_repeat = 'is_repeat';
  static const String Key_durationtime = 'durationtime';
  static const String Key_bookingdate = 'bookingdate';
  static const String Key_removedate = 'removedate';

  ReqScheduleData({
    this.id = 0,
    this.stime = '',
    this.etime = '',
    this.is_am = 0,
    this.is_pm = 0,
    this.is_repeat = 0,
    this.durationtime = 0,
    required this.bookingdate,
    this.removedate,
  });

  ///ID
  @JsonKey(name: Key_id)
  int id;

  ///시작 시간
  @JsonKey(name: Key_stime)
  String stime;

  ///종료 시간
  @JsonKey(name: Key_etime)
  String etime;

  ///오전 1, 아니면 0, 오전오후 걸치면 1
  @JsonKey(name: Key_is_am)
  int is_am;

  ///오후 1, 아니면 0, 오전오후 걸치면 1
  @JsonKey(name: Key_is_pm)
  int is_pm;

  ///반복
  @JsonKey(name: Key_is_repeat)
  int is_repeat;

  ///시간 합계(분)
  @JsonKey(name: Key_durationtime)
  int durationtime;

  ///날짜 데이터
  @JsonKey(name: Key_bookingdate)
  List<String> bookingdate;

  ///수정 날짜 데이터
  @JsonKey(name: Key_removedate)
  List<String>? removedate;

  factory ReqScheduleData.fromJson(Map<String, dynamic> json) {
    List<String> bookingdate = json[Key_bookingdate] == null ? [] : List<String>.from(json[Key_bookingdate]);
    List<String> removedate = json[Key_removedate] == null ? [] : List<String>.from(json[Key_removedate]);
    return ReqScheduleData(
      id: json[Key_id] ?? '',
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      is_am: json[Key_is_am] as int,
      is_pm: json[Key_is_pm] as int,
      is_repeat: json[Key_is_repeat] as int,
      durationtime: json[Key_durationtime] as int,
      bookingdate: bookingdate,
      removedate: removedate,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_is_am] = this.is_am;
    data[Key_is_pm] = this.is_pm;
    data[Key_is_repeat] = this.is_repeat;
    data[Key_durationtime] = this.durationtime;
    data[Key_bookingdate] = this.bookingdate;
    data[Key_removedate] = this.removedate;
    return data;
  }

  @override
  String toString() {
    return 'ReqScheduleData{id: $id, stime: $stime, etime: $etime, is_am: $is_am, is_pm: $is_pm, is_repeat: $is_repeat, durationtime: $durationtime, bookingdate: $bookingdate, removedate: $removedate}';
  }
}

///보육
class ReqBoyukData {
  static const String Key_boyuk_address = 'boyuk_address';
  static const String Key_boyuk_address_detail = 'boyuk_address_detail';
  static const String Key_boyuk_comment = 'boyuk_comment';
  static const String Key_pathroute_comment = 'pathroute_comment';
  static const String Key_nhn_area_1_id = 'nhn_area_1_id';
  static const String Key_nhn_address_1_id = 'nhn_address_1_id';
  static const String Key_nhn_area_1_address = 'nhn_area_1_address';
  static const String Key_nhn_area_2_id = 'nhn_area_2_id';
  static const String Key_nhn_address_2_id = 'nhn_address_2_id';
  static const String Key_nhn_area_2_address = 'nhn_area_2_address';
  static const String Key_nhn_radius = 'nhn_radius';
  static const String Key_nhn_comment = 'nhn_comment';

  ReqBoyukData({
    this.boyuk_address = '',
    this.boyuk_address_detail = '',
    this.boyuk_comment = '',
    this.pathroute_comment = '',
    this.nhn_area_1_id = '',
    this.nhn_address_1_id = '',
    this.nhn_area_1_address = '',
    this.nhn_area_2_id = '',
    this.nhn_address_2_id = '',
    this.nhn_area_2_address = '',
    this.nhn_radius = 0,
    this.nhn_comment = '',
  });

  @JsonKey(name: Key_boyuk_address)
  String boyuk_address;
  @JsonKey(name: Key_boyuk_address_detail)
  String boyuk_address_detail;
  @JsonKey(name: Key_boyuk_comment)
  String boyuk_comment;
  @JsonKey(name: Key_pathroute_comment)
  String pathroute_comment;
  @JsonKey(name: Key_nhn_area_1_id)
  String nhn_area_1_id;
  @JsonKey(name: Key_nhn_address_1_id)
  String nhn_address_1_id;
  @JsonKey(name: Key_nhn_area_1_address)
  String nhn_area_1_address;
  @JsonKey(name: Key_nhn_area_2_id)
  String nhn_area_2_id;
  @JsonKey(name: Key_nhn_address_2_id)
  String nhn_address_2_id;
  @JsonKey(name: Key_nhn_area_2_address)
  String nhn_area_2_address;
  @JsonKey(name: Key_nhn_radius)
  int nhn_radius;
  @JsonKey(name: Key_nhn_comment)
  String nhn_comment;

  factory ReqBoyukData.fromJson(Map<String, dynamic> json) {
    return ReqBoyukData(
      boyuk_address: json[Key_boyuk_address] ?? '',
      boyuk_address_detail: json[Key_boyuk_address_detail] ?? '',
      boyuk_comment: json[Key_boyuk_comment] ?? '',
      pathroute_comment: json[Key_pathroute_comment] ?? '',
      nhn_area_1_id: json[Key_nhn_area_1_id] ?? '',
      nhn_address_1_id: json[Key_nhn_address_1_id] ?? '',
      nhn_area_1_address: json[Key_nhn_area_1_address] ?? '',
      nhn_area_2_id: json[Key_nhn_area_2_id] ?? '',
      nhn_address_2_id: json[Key_nhn_address_2_id] ?? '',
      nhn_area_2_address: json[Key_nhn_area_2_address] ?? '',
      nhn_radius: json[Key_nhn_radius] as int,
      nhn_comment: json[Key_nhn_comment] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_boyuk_address] = this.boyuk_address;
    data[Key_boyuk_address_detail] = this.boyuk_address_detail;
    data[Key_boyuk_comment] = this.boyuk_comment;
    data[Key_pathroute_comment] = this.pathroute_comment;
    data[Key_nhn_area_1_id] = this.nhn_area_1_id;
    data[Key_nhn_address_1_id] = this.nhn_address_1_id;
    data[Key_nhn_area_1_address] = this.nhn_area_1_address;
    data[Key_nhn_area_2_id] = this.nhn_area_2_id;
    data[Key_nhn_address_2_id] = this.nhn_address_2_id;
    data[Key_nhn_area_2_address] = this.nhn_area_2_address;
    data[Key_nhn_radius] = this.nhn_radius;
    data[Key_nhn_comment] = this.nhn_comment;
    return data;
  }

  @override
  String toString() {
    return 'ReqBoyukData{boyuk_address: $boyuk_address, boyuk_address_detail: $boyuk_address_detail, boyuk_comment: $boyuk_comment, pathroute_comment: $pathroute_comment, nhn_area_1_id: $nhn_area_1_id, nhn_address_1_id: $nhn_address_1_id, nhn_area_1_address: $nhn_area_1_address, nhn_area_2_id: $nhn_area_2_id, nhn_address_2_id: $nhn_address_2_id, nhn_area_2_address: $nhn_area_2_address, nhn_radius: $nhn_radius, nhn_comment: $nhn_comment}';
  }
}

///등원/하원정보
class ReqSchoolToHomeData {
  static const String Key_product_id = 'product_id';
  static const String Key_start_address = 'start_address';
  static const String Key_start_address_detail = 'start_address_detail';
  static const String Key_start_comment = 'start_comment';
  static const String Key_pathroute_comment = 'pathroute_comment';
  static const String Key_start_time = 'start_time';
  static const String Key_end_address = 'end_address';
  static const String Key_end_address_detail = 'end_address_detail';
  static const String Key_end_time = 'end_time';
  static const String Key_end_comment = 'end_comment';
  static const String Key_nhn_area_1_id = 'nhn_area_1_id';
  static const String Key_nhn_address_1_id = 'nhn_address_1_id';
  static const String Key_nhn_area_1_address = 'nhn_area_1_address';
  static const String Key_nhn_area_2_id = 'nhn_area_2_id';
  static const String Key_nhn_address_2_id = 'nhn_address_2_id';
  static const String Key_nhn_area_2_address = 'nhn_area_2_address';
  static const String Key_nhn_radius = 'nhn_radius';
  static const String Key_nhn_comment = 'nhn_comment';

  ReqSchoolToHomeData({
    this.product_id,
    this.start_address = '',
    this.start_address_detail = '',
    this.start_time = '',
    this.start_comment = '',
    this.pathroute_comment = '',
    this.end_address = '',
    this.end_address_detail = '',
    this.end_time = '',
    this.end_comment = '',
    this.nhn_area_1_id = '',
    this.nhn_address_1_id = '',
    this.nhn_area_1_address = '',
    this.nhn_area_2_id = '',
    this.nhn_address_2_id = '',
    this.nhn_area_2_address = '',
    this.nhn_radius = 0,
    this.nhn_comment = '',
  });

  @JsonKey(name: Key_product_id)
  List<int>? product_id;
  @JsonKey(name: Key_start_address)
  String start_address;
  @JsonKey(name: Key_start_address_detail)
  String start_address_detail;
  @JsonKey(name: Key_start_time)
  String start_time;
  @JsonKey(name: Key_start_comment)
  String start_comment;
  @JsonKey(name: Key_pathroute_comment)
  String pathroute_comment;
  @JsonKey(name: Key_end_address)
  String end_address;
  @JsonKey(name: Key_end_address_detail)
  String end_address_detail;
  @JsonKey(name: Key_end_time)
  String end_time;
  @JsonKey(name: Key_end_comment)
  String end_comment;

  ///1순위 법정동코드
  @JsonKey(name: Key_nhn_area_1_id)
  String nhn_area_1_id;

  ///1순위 행정동코드
  @JsonKey(name: Key_nhn_address_1_id)
  String nhn_address_1_id;

  ///1순위 장소
  @JsonKey(name: Key_nhn_area_1_address)
  String nhn_area_1_address;

  ///2순위 법정동코드
  @JsonKey(name: Key_nhn_area_2_id)
  String nhn_area_2_id;

  ///2순위 행정동코드
  @JsonKey(name: Key_nhn_address_2_id)
  String nhn_address_2_id;

  ///2순위 장소
  @JsonKey(name: Key_nhn_area_2_address)
  String nhn_area_2_address;

  ///반경
  @JsonKey(name: Key_nhn_radius)
  int nhn_radius;

  ///추가설명
  @JsonKey(name: Key_nhn_comment)
  String nhn_comment;

  factory ReqSchoolToHomeData.fromJson(Map<String, dynamic> json) {
    List<int> productId = json[Key_product_id] == null ? [] : List<int>.from(json[Key_product_id]);
    return ReqSchoolToHomeData(
      product_id: productId,
      start_address: json[Key_start_address] ?? '',
      start_address_detail: json[Key_start_address_detail] ?? '',
      start_time: json[Key_start_time] ?? '',
      start_comment: json[Key_start_comment] ?? '',
      pathroute_comment: json[Key_pathroute_comment] ?? '',
      end_address: json[Key_end_address] ?? '',
      end_address_detail: json[Key_end_address_detail] ?? '',
      end_time: json[Key_end_time] ?? '',
      end_comment: json[Key_end_comment] ?? '',
      nhn_area_1_id: json[Key_nhn_area_1_id] ?? '',
      nhn_address_1_id: json[Key_nhn_address_1_id] ?? '',
      nhn_area_1_address: json[Key_nhn_area_1_address] ?? '',
      nhn_area_2_id: json[Key_nhn_area_2_id] ?? '',
      nhn_address_2_id: json[Key_nhn_address_2_id] ?? '',
      nhn_area_2_address: json[Key_nhn_area_2_address] ?? '',
      nhn_radius: json[Key_nhn_radius] as int,
      nhn_comment: json[Key_nhn_comment] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_product_id] = this.product_id;
    data[Key_start_address] = this.start_address;
    data[Key_start_address_detail] = this.start_address_detail;
    data[Key_start_time] = this.start_time;
    data[Key_start_comment] = this.start_comment;
    data[Key_pathroute_comment] = this.pathroute_comment;
    data[Key_end_address] = this.end_address;
    data[Key_end_address_detail] = this.end_address_detail;
    data[Key_end_time] = this.end_time;
    data[Key_end_comment] = this.end_comment;
    data[Key_nhn_area_1_id] = this.nhn_area_1_id;
    data[Key_nhn_address_1_id] = this.nhn_address_1_id;
    data[Key_nhn_area_1_address] = this.nhn_area_1_address;
    data[Key_nhn_area_2_id] = this.nhn_area_2_id;
    data[Key_nhn_address_2_id] = this.nhn_address_2_id;
    data[Key_nhn_area_2_address] = this.nhn_area_2_address;
    data[Key_nhn_radius] = this.nhn_radius;
    data[Key_nhn_comment] = this.nhn_comment;
    return data;
  }

  @override
  String toString() {
    return 'ReqSchoolToHomeData{product_id: $product_id, start_address: $start_address, start_address_detail: $start_address_detail, start_time: $start_time, start_comment: $start_comment, pathroute_comment: $pathroute_comment, end_address: $end_address, end_address_detail: $end_address_detail, end_time: $end_time, end_comment: $end_comment, nhn_area_1_id: $nhn_area_1_id, nhn_address_1_id: $nhn_address_1_id, nhn_area_1_address: $nhn_area_1_address, nhn_area_2_id: $nhn_area_2_id, nhn_address_2_id: $nhn_address_2_id, nhn_area_2_address: $nhn_area_2_address, nhn_radius: $nhn_radius, nhn_comment: $nhn_comment}';
  }
}

///보육,학습,입주가사 돌봄 정보
class ReqCareOptionData {
  static const String Key_product_id = 'product_id';
  static const String Key_product_name = 'product_name';
  static const String Key_product_cost = 'product_cost';
  static const String Key_product_comment = 'product_comment';

  ReqCareOptionData({
    this.product_id = 0,
    this.product_name = '',
    this.product_cost = 0,
    this.product_comment = '',
  });

  @JsonKey(name: Key_product_id)
  final int product_id;
  @JsonKey(name: Key_product_name)
  final String product_name;
  @JsonKey(name: Key_product_cost)
  final int product_cost;
  @JsonKey(name: Key_product_comment)
  final String product_comment;

  factory ReqCareOptionData.fromJson(Map<String, dynamic> json) {
    return ReqCareOptionData(
      product_id: json[Key_product_id] as int,
      product_name: json[Key_product_name] ?? '',
      product_cost: json[Key_product_cost] as int,
      product_comment: json[Key_product_comment] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_product_id] = this.product_id;
    data[Key_product_name] = this.product_name;
    data[Key_product_cost] = this.product_cost;
    data[Key_product_comment] = this.product_comment;
    return data;
  }

  @override
  String toString() {
    return 'ReqCareOptionData{product_id: $product_id, product_name: $product_name, product_cost: $product_cost, product_comment: $product_comment}';
  }
}

///이유식 반찬
class FoodServicesData {
  static const String Key_servicetype = 'servicetype';
  static const String Key_possible_area = 'possible_area';
  static const String Key_startdate = 'startdate';
  static const String Key_enddate = 'enddate';

  FoodServicesData({
    this.servicetype = 0,
    this.possible_area = 0,
    this.startdate = '',
    this.enddate = '',
  });

  @JsonKey(name: Key_servicetype)
  final int servicetype;
  @JsonKey(name: Key_possible_area)
  final int possible_area;
  @JsonKey(name: Key_startdate)
  final String startdate;
  @JsonKey(name: Key_enddate)
  final String enddate;

  factory FoodServicesData.fromJson(Map<String, dynamic> json) {
    return FoodServicesData(
      servicetype: json[Key_servicetype] as int,
      possible_area: json[Key_possible_area] as int,
      startdate: json[Key_startdate] ?? '',
      enddate: json[Key_enddate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_startdate] = this.startdate;
    data[Key_enddate] = this.enddate;
    return data;
  }

  @override
  String toString() {
    return 'FoodServicesData{servicetype: $servicetype, possible_area: $possible_area, startdate: $startdate, enddate: $enddate}';
  }
}

///인증된 주소(신청서 작성시 사용)
class AddrCodeData {
  ///인증된 주소(법정동코드)
  static const String Key_writed_addr_bcode = 'writed_addr_bcode';

  ///인증된 주소(행정동코드)
  static const String Key_writed_addr_hcode = 'writed_addr_hcode';

  AddrCodeData({
    this.writed_addr_bcode = '',
    this.writed_addr_hcode = '',
  });

  @JsonKey(name: Key_writed_addr_bcode)
  String writed_addr_bcode;
  @JsonKey(name: Key_writed_addr_hcode)
  String writed_addr_hcode;

  factory AddrCodeData.fromJson(Map<String, dynamic> json) {
    return AddrCodeData(
      writed_addr_bcode: json[Key_writed_addr_bcode] ?? '',
      writed_addr_hcode: json[Key_writed_addr_hcode] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_writed_addr_bcode] = this.writed_addr_bcode;
    data[Key_writed_addr_hcode] = this.writed_addr_hcode;
    return data;
  }

  @override
  String toString() {
    return 'AddrCodeData{writed_addr_bcode: $writed_addr_bcode, writed_addr_hcode: $writed_addr_hcode}';
  }
}
