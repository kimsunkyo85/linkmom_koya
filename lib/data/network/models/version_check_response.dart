import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

@JsonSerializable(explicitToJson: true)
class VersionCheckResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  VersionCheckResponse({
    this.dataHeader,
    this.dataList,
  });

  VersionCheckData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : VersionCheckData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory VersionCheckResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<VersionCheckData> datas = [];
    try {
      datas = json[Key_data].map<VersionCheckData>((i) => VersionCheckData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return VersionCheckResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'VersionCheckResponse{dataHeader: $dataHeader, dataList: $dataList, dataCookies: $dataCookies}';
  }
}

class VersionCheckData {
  static const String key_id = "id";
  static const String key_os_type = "os_type";
  static const String key_ver_major = "ver_major";
  static const String key_ver_minor = "ver_minor";
  static const String key_ver_patch = "ver_patch";
  static const String key_is_update = "is_update";
  static const String key_marketurl = "marketurl";
  static const String key_is_notice = "is_notice";
  static const String key_contents = "contents";
  static const String key_is_deploy = "is_deploy";
  static const String key_deploydate = "deploydate";
  static const String key_files = "files";
  static const String key_utils = "utils";

  VersionCheckData({
    this.id = 0,
    this.osType = '',
    this.verMajor = 0,
    this.verMinor = 0,
    this.verPatch = 0,
    this.update = 0,
    this.marketUrl = '',
    this.notice = 0,
    this.contents = '',
    this.deploy = 0,
    this.deploydate = '',
    this.files,
    this.utils,
  });

  @JsonKey(name: key_id)
  final int id;

  @JsonKey(name: key_os_type)
  final String osType;

  @JsonKey(name: key_ver_major)
  final int verMajor;

  @JsonKey(name: key_ver_minor)
  final int verMinor;

  @JsonKey(name: key_ver_patch)
  final int verPatch;

  @JsonKey(name: key_is_update)
  final int update;

  @JsonKey(name: key_marketurl)
  final String marketUrl;

  @JsonKey(name: key_is_notice)
  final int notice;

  @JsonKey(name: key_contents)
  final String contents;

  @JsonKey(name: key_is_deploy)
  final int deploy;

  @JsonKey(name: key_deploydate)
  final String deploydate;

  @JsonKey(name: key_files)
  final List<VersionCheckMenuFileData>? files;

  @JsonKey(name: key_files)
  final Utils? utils;

  factory VersionCheckData.fromJson(Map<String, dynamic> json) {
    List<VersionCheckMenuFileData> files = json[key_files] == null ? [] : json[key_files].map<VersionCheckMenuFileData>((i) => VersionCheckMenuFileData.fromJson(i)).toList();
    Utils utils = Utils.fromJson(json[key_utils] ?? {});
    return VersionCheckData(
      id: json[key_id] ?? "",
      osType: json[key_os_type] ?? "",
      verMajor: json[key_ver_major] ?? "",
      verMinor: json[key_ver_minor] ?? "",
      verPatch: json[key_ver_patch] ?? "",
      update: json[key_is_update] ?? "",
      marketUrl: json[key_marketurl] ?? "",
      notice: json[key_is_notice] ?? "",
      contents: json[key_contents] ?? "",
      deploy: json[key_is_deploy] ?? "",
      deploydate: json[key_deploydate] ?? "",
      files: files,
      utils: utils,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[VersionCheckData.key_id] = this.id;
    data[VersionCheckData.key_os_type] = this.osType;
    data[VersionCheckData.key_ver_major] = this.verMajor;
    data[VersionCheckData.key_ver_minor] = this.verMinor;
    data[VersionCheckData.key_ver_patch] = this.verPatch;
    data[VersionCheckData.key_is_update] = this.update;
    data[VersionCheckData.key_marketurl] = this.marketUrl;
    data[VersionCheckData.key_is_notice] = this.notice;
    data[VersionCheckData.key_contents] = this.contents;
    data[VersionCheckData.key_is_deploy] = this.deploy;
    data[VersionCheckData.key_deploydate] = this.deploydate;
    data[VersionCheckData.key_files] = this.files;
    return data;
  }

  @override
  String toString() {
    return 'VersionCheck{id: $id, osType: $osType, verMajor: $verMajor, verMinor: $verMinor, verPatch: $verPatch, update: $update, marketUrl: $marketUrl, notice: $notice, contents: $contents, deploy: $deploy, deploydate: $deploydate}';
  }
}

class VersionCheckMenuFileData {
  static const Key_menu_file = 'menu_file';

  VersionCheckMenuFileData({this.menu_file = ''});

  @JsonKey(name: Key_menu_file)
  String menu_file;

  factory VersionCheckMenuFileData.fromJson(Map<String, dynamic> json) {
    return VersionCheckMenuFileData(
      menu_file: json[Key_menu_file] ?? "",
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_menu_file] = this.menu_file;
    return data;
  }

  @override
  String toString() {
    return 'MenuFileData{menu_file: $menu_file}';
  }
}

@JsonSerializable()
class Utils {
  static const Key_menu_file = 'menu_file';
  static const Key_holiday = 'holiday';
  static const Key_bank = 'bank';
  static const Key_bank_file_size = 'bank_file_size';

  Utils({
    this.menu_file = '',
    this.holiday,
    this.bank = '',
    this.bank_file_size = 0,
  });

  @JsonKey(name: Key_menu_file)
  final String menu_file;
  @JsonKey(name: Key_holiday)
  final List<String>? holiday;
  @JsonKey(name: Key_bank)
  final String bank;
  @JsonKey(name: Key_bank_file_size)
  final int bank_file_size;

  factory Utils.fromJson(Map<String, dynamic> json) {
    return Utils(
      menu_file: json[Key_menu_file] ?? "",
      holiday: List<String>.from(json[Key_holiday]),
      bank: json[Key_bank] ?? "",
      bank_file_size: json[Key_bank_file_size] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_menu_file] = this.menu_file;
    data[Key_holiday] = this.holiday;
    data[Key_bank] = this.bank;
    data[Key_bank_file_size] = this.bank_file_size;
    return data;
  }

  @override
  String toString() {
    return 'Utils{menu_file: $menu_file, holiday: $holiday, bank: $bank, bank_file_size: $bank_file_size}';
  }
}
