import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryLikeRequest {
  static const String Key_carecontract_schedule = 'carecontract_schedule';

  CareDiaryLikeRequest({this.carecontract_schedule = 0});

  @JsonKey(name: Key_carecontract_schedule)
  int carecontract_schedule;

  factory CareDiaryLikeRequest.fromJson(Map<String, dynamic> json) {
    return CareDiaryLikeRequest(
        carecontract_schedule: json[Key_carecontract_schedule]);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_carecontract_schedule] = this.carecontract_schedule;
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryLikeRequest{carecontract_schedule: $carecontract_schedule}';
  }
}
