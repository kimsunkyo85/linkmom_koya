import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import 'data/linkmom_result_data.dart';

@JsonSerializable()
class LinkmomMatchingViewResponse extends HeaderResponse {
  HeaderResponse dataHeader;
  List<LinkmomResultsData> dataList;

  LinkmomMatchingViewResponse({required this.dataHeader, required this.dataList});

  LinkmomResultsData getData() => dataList.isNotEmpty ? dataList.first : LinkmomResultsData();

  int getCode() => dataHeader.getCode();

  String getMsg() => dataHeader.getMsg();

  factory LinkmomMatchingViewResponse.fromJson(Map<String, dynamic> json) {
    return LinkmomMatchingViewResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? [] : (json[Key_data] as List).map((e) => LinkmomResultsData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_data] = this.dataList;
    return data;
  }

  @override
  String toString() {
    return 'LinkmomMatchingViewResponse{dataHeader: $dataHeader, dataList:$dataList}';
  }
}
