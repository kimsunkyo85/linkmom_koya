import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class DiaryViewRequest {
  static const String Key_schedule_id = 'schedule_id';

  DiaryViewRequest({this.scheduleId = 0});

  @JsonKey(name: Key_schedule_id)
  final int scheduleId;

  factory DiaryViewRequest.fromJson(Map<String, dynamic> json) {
    return DiaryViewRequest(scheduleId: json[Key_schedule_id] as int);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_schedule_id] = this.scheduleId;
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryViewRequest{schedule_id: $scheduleId}';
  }
}
