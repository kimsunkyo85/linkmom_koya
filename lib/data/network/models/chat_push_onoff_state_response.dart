import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/chat_push_onoff_state_request.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_push_onoff_state_response.dart - 채팅 관련 메시지 push 상태 값
/// since        : 2021/07/16 / update:
@JsonSerializable()
class ChatPushOnOffStateResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ChatPushOnOffStateRequest>? dataList;

  ChatPushOnOffStateResponse({
    this.dataHeader,
    this.dataList,
  });

  ChatPushOnOffStateRequest getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ChatPushOnOffStateRequest();

  List<ChatPushOnOffStateRequest> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ChatPushOnOffStateResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ChatPushOnOffStateRequest> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<ChatPushOnOffStateRequest>((i) => ChatPushOnOffStateRequest.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return ChatPushOnOffStateResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ChatPushOnOffStateResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
