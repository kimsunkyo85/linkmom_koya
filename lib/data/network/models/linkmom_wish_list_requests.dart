import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class LinkmomWishSaveRequest {
  static const String Key_bookingjobs = 'bookingjobs';

  LinkmomWishSaveRequest({
    this.bookingjobs = 0,
  });

  @JsonKey(name: Key_bookingjobs)
  int bookingjobs;

  factory LinkmomWishSaveRequest.fromJson(Map<String, dynamic> json) {
    return LinkmomWishSaveRequest(
      bookingjobs: json[Key_bookingjobs] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bookingjobs] = this.bookingjobs;
    return data;
  }

  @override
  String toString() {
    return 'BookingWishSaveRequest{bookingjobs: $bookingjobs}';
  }
}

@JsonSerializable()
class LinkmomWishDeleteRequest {
  static const String Key_bookingjobs = 'bookingjobs';

  LinkmomWishDeleteRequest({
    this.bookingjobs = 0,
  });

  @JsonKey(name: Key_bookingjobs)
  int bookingjobs;

  factory LinkmomWishDeleteRequest.fromJson(Map<String, dynamic> json) {
    return LinkmomWishDeleteRequest(
      bookingjobs: json[Key_bookingjobs] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bookingjobs] = this.bookingjobs;
    return data;
  }

  @override
  String toString() {
    return 'BookingWishDeleteRequest{bookingjobs: $bookingjobs}';
  }
}
