import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class LinkmomWishDeleteResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  LinkmomWishDeleteResponse({
    this.dataHeader,
    this.dataList,
  });

  LinkmomWishDeleteResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  String getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ' ';

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory LinkmomWishDeleteResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<LinkmomDeleteData> datas = [];
    try {
      datas = json[Key_data].map<LinkmomDeleteData>((i) => LinkmomDeleteData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return LinkmomWishDeleteResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'LinkmomWishDeleteResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class LinkmomDeleteData {
  static const String Key_message = 'message';

  LinkmomDeleteData({
    this.message = '',
  });

  @JsonKey(name: Key_message)
  String message;

  factory LinkmomDeleteData.fromJson(Map<String, dynamic> json) {
    return LinkmomDeleteData(
      message: json[Key_message],
    );
  }
}
