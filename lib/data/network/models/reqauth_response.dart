import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';
import 'header_response.dart';

@JsonSerializable()
class ReqauthResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  ReqauthResponse({
    this.dataHeader,
    this.dataList,
  });

  ReqauthData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ReqauthData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ReqauthResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ReqauthData> datas = [];
    try {
      datas = json[Key_data].map<ReqauthData>((i) => ReqauthData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』>>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return ReqauthResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ReqauthResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class ReqauthData {
  static const String Key_send_result = "send_result";
  static const String Key_auth_number = "auth_number";

  ReqauthData({
    this.send_result = 0,
    this.auth_number = 0,
  });

  @JsonKey(name: Key_send_result)
  final int send_result;
  @JsonKey(name: Key_auth_number)
  final int auth_number;

  factory ReqauthData.fromJson(Map<String, dynamic> json) {
    return ReqauthData(
      send_result: json[Key_send_result] as int,
      auth_number: json[Key_auth_number] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_send_result] = this.send_result;
    data[Key_auth_number] = this.auth_number;
    return data;
  }

  @override
  String toString() {
    return 'ReqauthData{send_result: $send_result, auth_number: $auth_number}';
  }
}
