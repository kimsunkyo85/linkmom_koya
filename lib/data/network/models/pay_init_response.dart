import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/chat_room_data.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///
/// Created by   : platformbiz@ggumbi.com
///
/// version      : 1.0.0
///
/// see          : pay_init_response.dart - 결제 요청 전 응답 POST
///
/// since        : 2021/09/09 / update:
///
/// (0, '계약중'),
///
/// (11, '계약실패(거절/취소/응답없음/시간만료 등)'),
///
/// (21, '계약완료 후 취소(부분)'),
///
/// (22, '계약완료 후 취소(전체)'),
///
/// (92, '매칭성공(계약완료/결제대기중)'),
///
/// (100, '결제완료')
///
///  "msg_cd": 3700, "message": "결제 내역 전송 등을 위해 이메일 인증이 필요합니다. 이메일 인증을 해주세요.",
///
///  "msg_cd": 5000, "message": "계약이 완료 되지 않았습니다. 계약 완료 후 결제를 해주세요.",
///
///  "msg_cd": 5100, "message": "결제 완료된 계약 입니다.",
///
///  "msg_cd": 5011,"message": "이미 취소된 계약 입니다.",
///
///  "msg_cd": 4000, "message": "데이터가 없습니다.",
@JsonSerializable()
class PayInitResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PayInitData>? dataList;

  PayInitResponse({
    this.dataHeader,
    this.dataList,
  });

  PayInitData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PayInitData();

  List<PayInitData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory PayInitResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PayInitData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<PayInitData>((i) => PayInitData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return PayInitResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ChatRoomUpdateResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class PayInitData {
  static const String Key_contract_id = 'contract_id';
  static const String Key_bookingcareservices_info = 'bookingcareservices_info';
  static const String Key_paid_deadline = 'paid_deadline';
  static const String Key_contract_date = 'contract_date';
  static const String Key_contract_status = 'contract_status';
  static const String Key_cache_total = 'cache_total';
  static const String Key_point_total = 'point_total';
  static const String Key_order_id = 'order_id';
  static const String Key_user_info = 'user_info';

  PayInitData({
    this.contract_id = 0,
    this.bookingcareservices_info,
    this.paid_deadline = '',
    this.contract_date = '',
    this.contract_status,
    this.cache_total,
    this.point_total,
    this.order_id = '',
    this.used_cash = '',
    this.used_point = '',
    this.user_info,
  });

  PayInitData.init() {
    contract_id = 0;
    bookingcareservices_info = ChatCareInfoData();
    paid_deadline = '';
    contract_date = '';
    contract_status = 0;
    cache_total = 0;
    point_total = 0;
    order_id = '';
    used_cash = '';
    used_point = '';
    user_info = PayInitUserInfo();
  }

  @JsonKey(name: Key_contract_id)
  int? contract_id;
  @JsonKey(name: Key_bookingcareservices_info)
  ChatCareInfoData? bookingcareservices_info;
  @JsonKey(name: Key_paid_deadline)
  String? paid_deadline;
  @JsonKey(name: Key_contract_date)
  String? contract_date;
  @JsonKey(name: Key_contract_status)
  int? contract_status;
  @JsonKey(name: Key_cache_total)
  int? cache_total;
  @JsonKey(name: Key_point_total)
  int? point_total;
  @JsonKey(name: Key_order_id)
  String? order_id;
  @JsonKey(name: Key_user_info)
  PayInitUserInfo? user_info;

  ///캐시사용(결제완료에서 사용)
  String? used_cash;

  ///포인트사용(결제완료에서사용)
  String? used_point;

  factory PayInitData.fromJson(Map<String, dynamic> json) {
    ChatCareInfoData? careinfo = json[Key_bookingcareservices_info] == null ? ChatCareInfoData() : ChatCareInfoData.fromJson(json[Key_bookingcareservices_info]);
    PayInitUserInfo? userInfo = json[Key_user_info] == null ? PayInitUserInfo() : PayInitUserInfo.fromJson(json[Key_user_info]);
    return PayInitData(
      contract_id: json[Key_contract_id] as int,
      bookingcareservices_info: careinfo,
      paid_deadline: json[Key_paid_deadline] ?? '',
      contract_date: json[Key_contract_date] ?? '',
      contract_status: json[Key_contract_status] as int,
      cache_total: json[Key_cache_total] as int,
      point_total: json[Key_point_total] as int,
      order_id: json[Key_order_id] ?? '',
      user_info: userInfo,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_contract_id] = this.contract_id;
    data[Key_bookingcareservices_info] = this.bookingcareservices_info;
    data[Key_paid_deadline] = this.paid_deadline;
    data[Key_contract_date] = this.contract_date;
    data[Key_contract_status] = this.contract_status;
    data[Key_cache_total] = this.cache_total;
    data[Key_point_total] = this.point_total;
    data[Key_order_id] = this.order_id;
    data[Key_user_info] = this.user_info;
    return data;
  }

  @override
  String toString() {
    return 'PayInitData{contract_id: $contract_id, bookingcareservices_info: $bookingcareservices_info, paid_deadline: $paid_deadline, contract_date: $contract_date, contract_status: $contract_status, cache_total: $cache_total, point_total: $point_total, order_id: $order_id, user_info: $user_info, cache_used: $used_cash, point_used: $used_point}';
  }
}

class PayInitUserInfo {
  static const String Key_first_name = 'first_name';
  static const String Key_hpnumber = 'hpnumber';
  static const String Key_email = 'email';

  PayInitUserInfo({
    this.first_name = '',
    this.hpnumber = '',
    this.email = '',
  });

  @JsonKey(name: Key_first_name)
  final String? first_name;
  @JsonKey(name: Key_hpnumber)
  final String? hpnumber;
  @JsonKey(name: Key_email)
  final String? email;

  factory PayInitUserInfo.fromJson(Map<String, dynamic> json) {
    return PayInitUserInfo(
      first_name: json[Key_first_name] ?? '',
      hpnumber: json[Key_hpnumber] ?? '',
      email: json[Key_email] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_first_name] = this.first_name;
    data[Key_hpnumber] = this.hpnumber;
    data[Key_email] = this.email;
    return data;
  }

  @override
  String toString() {
    return 'PayInitUserInfo{first_name: $first_name, hpnumber: $hpnumber, email: $email}';
  }
}
