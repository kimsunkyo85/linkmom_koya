import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

import 'data/care_diary_data.dart';

@JsonSerializable()
class CareDiarySaveResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<CareDiaryData>? dataList;

  CareDiarySaveResponse({
    this.dataHeader,
    this.dataList,
  });

  CareDiarySaveResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  CareDiaryData getData() => dataList != null && dataList!.length != 0 ? dataList!.first : CareDiaryData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareDiarySaveResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareDiaryData> datas = [];
    try {
      datas = json[Key_data].map<CareDiaryData>((i) => CareDiaryData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareDiarySaveResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CareDiarySaveResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
