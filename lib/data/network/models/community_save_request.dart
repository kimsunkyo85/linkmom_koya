import 'dart:io';

import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CommunitySaveRequest {
  static const String Key_title = 'title';
  static const String Key_category = 'category';
  static const String Key_content = 'content';
  static const String Key_image = 'image';
  static const String Key_hcode = 'hcode';

  @JsonKey(name: Key_title)
  String title;
  @JsonKey(name: Key_category)
  int category;
  @JsonKey(name: Key_content)
  String content;
  @JsonKey(name: Key_image)
  List<File>? image;
  @JsonKey(name: Key_hcode)
  int hcode;

  CommunitySaveRequest({this.title = '', this.category = 0, this.content = '', this.image, this.hcode = 0});

  factory CommunitySaveRequest.fromJson(Map<String, dynamic> json) {
    return CommunitySaveRequest(
      title: json[Key_title] ?? '',
      category: json[Key_category] ?? 0,
      content: json[Key_content] ?? '',
      image: json[Key_image] ?? [],
      hcode: json[Key_hcode] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_title] = this.title;
    data[Key_category] = this.category;
    data[Key_content] = this.content;
    data[Key_image] = this.image;
    data[Key_hcode] = this.hcode;
    return data;
  }

  FormData toFormData(FormData formData) {
    formData.fields.add(MapEntry(Key_title, this.title.toString()));
    formData.fields.add(MapEntry(Key_category, this.category.toString()));
    formData.fields.add(MapEntry(Key_content, this.content.toString()));
    formData.fields.add(MapEntry(Key_hcode, this.hcode.toString()));
    return formData;
  }

  @override
  String toString() {
    return 'CommunitySaveRequest{title: $title, category: $category, content: $content, image: $image, hcode: $hcode}';
  }
}
