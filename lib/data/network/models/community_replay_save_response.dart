import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

class CommunityReplySaveResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<CommunityReplyData>? dataList;

  CommunityReplySaveResponse({this.dataHeader, this.dataList});
  CommunityReplyData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CommunityReplyData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();
  factory CommunityReplySaveResponse.fromJson(Map<String, dynamic> json) {
    return CommunityReplySaveResponse(
        dataHeader: HeaderResponse.fromJson(json),
        dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => CommunityReplyData.fromJson(e)).toList());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CommunityReplySaveResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class CommunityReplyData {
  static const String Key_id = 'id';
  static const String Key_parent = 'parent';
  static const String Key_level = 'level';
  static const String Key_reply = 'reply';
  static const String Key_writer_nickname = 'writer_nickname';
  static const String Key_profileimg = 'profileimg';
  static const String Key_is_delete = 'is_delete';
  static const String Key_updatedate = 'updatedate';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_parent)
  int parent;
  @JsonKey(name: Key_level)
  int level;
  @JsonKey(name: Key_reply)
  String reply;
  @JsonKey(name: Key_writer_nickname)
  String writerNickname;
  @JsonKey(name: Key_profileimg)
  String profileimg;
  @JsonKey(name: Key_is_delete)
  int isDelete;
  @JsonKey(name: Key_updatedate)
  String updatedate;

  CommunityReplyData({this.id = 0, this.parent = 0, this.level = 0, this.reply = '', this.writerNickname = '', this.profileimg = '', this.isDelete = 0, this.updatedate = ''});

  factory CommunityReplyData.fromJson(Map<String, dynamic> json) {
    return CommunityReplyData(
        id: json[Key_id] ?? 0,
        parent: json[Key_parent] ?? 0,
        level: json[Key_level] ?? 0,
        reply: json[Key_reply] ?? '',
        writerNickname: json[Key_writer_nickname] ?? '',
        profileimg: json[Key_profileimg] ?? '',
        isDelete: json[Key_is_delete] ?? 0,
        updatedate: json[Key_updatedate] ?? '');
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_parent] = this.parent;
    data[Key_level] = this.level;
    data[Key_reply] = this.reply;
    data[Key_writer_nickname] = this.writerNickname;
    data[Key_profileimg] = this.profileimg;
    data[Key_is_delete] = this.isDelete;
    data[Key_updatedate] = this.updatedate;
    return data;
  }

  @override
  String toString() {
    return 'CommunityReplyData{id: $id,parent: $parent,level: $level,reply: $reply,writerNickname: $writerNickname,profileimg: $profileimg,isDelete: $isDelete,updatedate: $updatedate}';
  }
}
