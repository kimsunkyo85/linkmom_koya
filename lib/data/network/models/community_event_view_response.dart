import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

class CommunityEventViewResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<EventData>? dataList;

  CommunityEventViewResponse({this.dataHeader, this.dataList});

  EventData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : EventData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CommunityEventViewResponse.fromJson(Map<String, dynamic> json) {
    return CommunityEventViewResponse(dataHeader: HeaderResponse.fromJson(json), dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => EventData.fromJson(e)).toList());
  }

  @override
  String toString() {
    return 'CommunityEventViewResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class EventData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';
  static const String Key_content = 'content';
  static const String Key_title = 'title';
  static const String Key_start_date = 'start_date';
  static const String Key_end_date = 'end_date';
  static const String Key_webview_url = 'webview_url';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<EventReply>? results;
  @JsonKey(name: Key_content)
  String content;
  @JsonKey(name: Key_title)
  String title;
  @JsonKey(name: Key_start_date)
  String startDate;
  @JsonKey(name: Key_end_date)
  String endDate;
  @JsonKey(name: Key_webview_url)
  String webviewUrl;

  EventData({
    this.count = 0,
    this.next = '',
    this.previous = '',
    this.results,
    this.content = '',
    this.title = '',
    this.startDate = '',
    this.endDate = '',
    this.webviewUrl = '',
  });

  factory EventData.fromJson(Map<String, dynamic> json) {
    return EventData(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] == null ? null : (json[Key_results] as List).map((e) => EventReply.fromJson(e)).toList(),
      content: json[Key_content] ?? '',
      title: json[Key_title] ?? '',
      startDate: json[Key_start_date] ?? '',
      endDate: json[Key_end_date] ?? '',
      webviewUrl: json[Key_webview_url] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    if (this.results != null) data[Key_results] = this.results;
    data[Key_content] = this.content;
    data[Key_title] = this.title;
    data[Key_start_date] = this.startDate;
    data[Key_end_date] = this.endDate;
    data[Key_webview_url] = this.webviewUrl;
    return data;
  }

  @override
  String toString() {
    return 'EventData{count: $count,next: $next,previous: $previous,results: $results,content: $content,title: $title, startDate: $startDate, endDate: $endDate, webviewUrl: $webviewUrl}';
  }
}

class EventReply {
  static const String Key_id = 'id';
  static const String Key_parent = 'parent';
  static const String Key_level = 'level';
  static const String Key_reply = 'reply';
  static const String Key_writer_nickname = 'writer_nickname';
  static const String Key_profileimg = 'profileimg';
  static const String Key_is_delete = 'is_delete';
  static const String Key_updatedate = 'updatedate';
  static const String Key_writer_id = 'writer_id';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_parent)
  int parent;
  @JsonKey(name: Key_level)
  int level;
  @JsonKey(name: Key_reply)
  String reply;
  @JsonKey(name: Key_writer_nickname)
  String writerNickname;
  @JsonKey(name: Key_profileimg)
  String profileimg;
  @JsonKey(name: Key_is_delete)
  int isDelete;
  @JsonKey(name: Key_updatedate)
  String updatedate;
  @JsonKey(name: Key_writer_id)
  int writerId;

  EventReply({this.id = 0, this.parent = 0, this.level = 0, this.reply = '', this.writerNickname = '', this.profileimg = '', this.isDelete = 0, this.updatedate = '', this.writerId = 0});

  factory EventReply.fromJson(Map<String, dynamic> json) {
    return EventReply(
      id: json[Key_id] ?? 0,
      parent: json[Key_parent] ?? 0,
      level: json[Key_level] ?? 0,
      reply: json[Key_reply] ?? '',
      writerNickname: json[Key_writer_nickname] ?? '',
      profileimg: json[Key_profileimg] ?? '',
      isDelete: json[Key_is_delete] ?? 0,
      updatedate: json[Key_updatedate] ?? '',
      writerId: int.tryParse(json[Key_writer_id]) ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_parent] = this.parent;
    data[Key_level] = this.level;
    data[Key_reply] = this.reply;
    data[Key_writer_nickname] = this.writerNickname;
    data[Key_profileimg] = this.profileimg;
    data[Key_is_delete] = this.isDelete;
    data[Key_updatedate] = this.updatedate;
    data[Key_writer_id] = this.writerId;
    return data;
  }

  @override
  String toString() {
    return 'Results{id: $id,parent: $parent,level: $level,reply: $reply,writerNickname: $writerNickname,profileimg: $profileimg,isDelete: $isDelete,updatedate: $updatedate,writerId: $writerId}';
  }
}
