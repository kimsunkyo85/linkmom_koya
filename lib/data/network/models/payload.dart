import 'package:json_annotation/json_annotation.dart';

part 'payload.g.dart';

@JsonSerializable()
class Payload {
  static const String key_name = "name";
  static const String key_category = "category";
  static const String key_action = "action";
  static const String key_where = "where";
  static const String key_timestamp = "timestamp";

  Payload({
    this.name = '',
    this.category = '',
    this.action = '',
    this.where = '',
    this.timestamp = '',
  });

  @JsonKey(name: key_name)
  String name;

  @JsonKey(name: key_category)
  String category;

  @JsonKey(name: key_action)
  String action;

  @JsonKey(name: key_where)
  String where;

  @JsonKey(name: key_timestamp)
  String timestamp;

  factory Payload.fromJson(Map<String, dynamic> json) => _$PayloadFromJson(json);

  Map<String, dynamic> toJson() => _$PayloadToJson(this);

  @override
  String toString() {
    return "$name $category $action $where $timestamp".toString();
  }
}
