import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

@JsonSerializable()
class AuthStateResponse {
  HeaderResponse? dataHeader;
  List<AuthState>? dataList;

  AuthStateResponse({this.dataHeader, this.dataList});

  AuthState getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : AuthState();

  List<AuthState> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory AuthStateResponse.fromJson(Map<String, dynamic> json) {
    return AuthStateResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? [] : (json[Key_data] as List).map((e) => AuthState.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.dataList != null) data[Key_data] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'AuthStateResponse { dataHeader: $dataHeader, dataList: $dataList }';
  }
}

class AuthState {
  static const String Key_id = 'id';
  static const String Key_auth_imgtype = 'auth_imgtype';
  static const String Key_status = 'status';
  static const String Key_cancel_reason = 'cancel_reason';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_auth_imgtype)
  int authImgtype;
  @JsonKey(name: Key_status)
  int status;
  @JsonKey(name: Key_cancel_reason)
  String cancelReason;

  AuthState({this.id = 0, this.authImgtype = 0, this.status = 0, this.cancelReason = ''});

  factory AuthState.fromJson(Map<String, dynamic> json) {
    return AuthState(
      id: json[Key_id] ?? 0,
      authImgtype: json[Key_auth_imgtype] ?? 0,
      status: json[Key_status] ?? 0,
      cancelReason: json[Key_cancel_reason] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_auth_imgtype] = this.authImgtype;
    data[Key_status] = this.status;
    data[Key_cancel_reason] = this.cancelReason;
    return data;
  }

  @override
  String toString() {
    return 'AuthState {id: $id, status: $status, authImgtype: $authImgtype, cancelReason: $cancelReason}';
  }
}
