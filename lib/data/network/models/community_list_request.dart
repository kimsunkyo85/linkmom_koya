import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CommunityListRequest {
  static const String Key_category = 'category';
  static const String Key_is_mine = 'is_mine';
  static const String Key_is_scrap = 'is_scrap';
  static const String Key_hcode = 'hcode';

  @JsonKey(name: Key_category)
  List<int>? category;
  @JsonKey(name: Key_is_mine)
  String isMine;
  @JsonKey(name: Key_is_scrap)
  String isScrap;
  @JsonKey(name: Key_hcode)
  String hcode;

  CommunityListRequest({this.category, this.isMine = '', this.isScrap = '', this.hcode = ''});

  factory CommunityListRequest.fromJson(Map<String, dynamic> json) {
    return CommunityListRequest(
      category: List.from(json[Key_category] ?? []),
      isMine: json[Key_is_mine] ?? '',
      isScrap: json[Key_is_scrap] ?? '',
      hcode: json[Key_hcode] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.category != null && this.category!.isNotEmpty) data[Key_category] = this.category;
    if (this.isMine.isNotEmpty) data[Key_is_mine] = this.isMine;
    if (this.isScrap.isNotEmpty) data[Key_is_scrap] = this.isScrap;
    if (this.hcode.isNotEmpty) data[Key_hcode] = this.hcode;
    return data;
  }

  FormData toFormData() {
    FormData formData = FormData();
    if (this.isMine.isNotEmpty) formData.fields.add(MapEntry(Key_is_mine, this.isMine));
    if (this.isScrap.isNotEmpty) formData.fields.add(MapEntry(Key_is_scrap, this.isScrap));
    if (this.hcode.isNotEmpty) formData.fields.add(MapEntry(Key_hcode, this.hcode));
    if (this.category != null)
      this.category!.forEach((cat) {
        formData.fields.add(MapEntry(Key_category, cat.toString()));
      });
    return formData;
  }

  @override
  String toString() {
    return "CommunityListRequest{category:$category isMine:$isMine isScrap:$isScrap hcode:$hcode}";
  }
}
