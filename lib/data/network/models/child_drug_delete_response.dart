import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';
import 'header_response.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : child_drug_response.dart - 투약 의뢰서 응답
/// since        : 5/20/21 / update:

@JsonSerializable()
class ChildDrugDeleteResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ChildDrugDeleteMsg>? dataList;

  ChildDrugDeleteResponse({
    this.dataHeader,
    this.dataList,
  });

  ChildDrugDeleteMsg getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ChildDrugDeleteMsg();

  List<ChildDrugDeleteMsg> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ChildDrugDeleteResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ChildDrugDeleteMsg> datas = [];
    try {
      datas = json[Key_data] == null ? ChildDrugDeleteMsg() : json[Key_data].map<ChildDrugDeleteMsg>((i) => ChildDrugDeleteMsg.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return ChildDrugDeleteResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ChildDrugDeleteResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class ChildDrugDeleteMsg {
  @JsonKey(name: Key_message)
  String message;

  ChildDrugDeleteMsg({
    this.message = '',
  });

  String getData() => message;

  factory ChildDrugDeleteMsg.fromJson(Map<String, dynamic> json) {
    return ChildDrugDeleteMsg(
      message: json[Key_message] ?? '',
    );
  }

  @override
  String toString() {
    return 'ChildDrugDeleteMsg{$Key_message: $message}';
  }
}
