import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:stack_trace/stack_trace.dart';

@JsonSerializable()
class FindPwResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  FindPwResponse({
    this.dataHeader,
    this.dataList,
  });

  FindPwData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : FindPwData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory FindPwResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<FindPwData> datas = [];
    try {
      datas = json[Key_data].map<FindPwData>((i) => FindPwData.fromJson(i)).toList();
    } catch (e) {
      print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::$e');
    }
    return FindPwResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'FindPwResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class FindPwData {
  static const String Key_username = "username";
  static const String Key_access = "access";

  FindPwData({
    this.username = '',
    this.access = '',
  });

  @JsonKey(name: Key_username)
  final String username;

  @JsonKey(name: Key_access)
  final String access;

  factory FindPwData.fromJson(Map<String, dynamic> json) {
    return FindPwData(
      username: json[Key_username] ?? '',
      access: json[Key_access] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_username] = this.username;
    data[Key_access] = this.access;
    return data;
  }

  @override
  String toString() {
    return 'FindPwData{username: $username, access: $access}';
  }
}
