import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

@JsonSerializable()
class LoginResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  LoginResponse({
    this.dataHeader,
    this.dataList,
  });

  LoginData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : LoginData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<LoginData> datas = [];
    try {
      datas = json[Key_data].map<LoginData>((i) => LoginData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return LoginResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'LoginResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class LoginData {
  static const String Key_access = "access";
  static const String Key_user = "user";
  static const String Key_pku = "pku";

  LoginData({
    this.access = '',
    this.user,
    this.pku = '',
  });

  @JsonKey(name: Key_access)
  final String access;
  @JsonKey(name: Key_user)
  UserData? user;
  @JsonKey(name: Key_pku)
  String pku;

  factory LoginData.fromJson(Map<String, dynamic> json) {
    UserData userData = UserData();
    try {
      userData = UserData.fromJson(json[Key_user] ?? {});
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return LoginData(
      access: json[Key_access] ?? '',
      user: userData,
      pku: json[Key_pku] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_access] = this.access;
    data[Key_user] = this.user;
    data[Key_pku] = this.pku;
    return data;
  }

  @override
  String toString() {
    return 'LoginData{access: $access, user: $user, pku: $pku}';
  }
}

class UserData {
  static const String Key_id = "id";
  static const String Key_username = "username";
  static const String Key_last_login = "last_login";
  static const String Key_penalty_10 = "penalty_10";
  static const String Key_penalty_15 = "penalty_15";
  static const String Key_is_user_momdady = "is_user_momdady";
  static const String Key_is_user_linkmom = "is_user_linkmom";

  UserData({
    this.id = 0,
    this.username = '',
    this.last_login = '',
    this.penalty_10 = '',
    this.penalty_15 = '',
    this.is_user_momdady = true,
    this.is_user_linkmom = false,
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_username)
  final String username;
  @JsonKey(name: Key_last_login)
  final String last_login;
  @JsonKey(name: Key_penalty_10)
  final String penalty_10;
  @JsonKey(name: Key_penalty_15)
  final String penalty_15;
  @JsonKey(name: Key_is_user_momdady)
  bool is_user_momdady;
  @JsonKey(name: Key_is_user_linkmom)
  bool is_user_linkmom;

  factory UserData.fromJson(Map<String, dynamic> json) {
    return UserData(
      id: json[Key_id] as int,
      username: json[Key_username] ?? '',
      last_login: json[Key_last_login] ?? '',
      penalty_10: json[Key_penalty_10] ?? '',
      penalty_15: json[Key_penalty_15] ?? '',
      is_user_momdady: json[Key_is_user_momdady] ?? true,
      is_user_linkmom: json[Key_is_user_linkmom] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_username] = this.username;
    data[Key_last_login] = this.last_login;
    data[Key_penalty_10] = this.penalty_10;
    data[Key_penalty_15] = this.penalty_15;
    data[Key_is_user_momdady] = this.is_user_momdady;
    data[Key_is_user_linkmom] = this.is_user_linkmom;
    return data;
  }

  @override
  String toString() {
    return 'UserData{id: $id, username: $username, last_login: $last_login, penalty_10: $penalty_10, penalty_15: $penalty_15, is_user_momdady: $is_user_momdady, is_user_linkmom: $is_user_linkmom}';
  }
}
