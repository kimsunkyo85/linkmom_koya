import 'package:easy_localization/easy_localization.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/utils/commons.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : header_response.dart - 공통 Response
/// since        : 1/18/21 / update:

const String Key_msg_cd = 'msg_cd';
const String Key_message = 'message';
const String Key_data = 'data';

@JsonSerializable()
class HeaderResponse {
  ///쿠키 정보가 필요한 경우 세팅
  Map<String, String> dataCookies = {};

  HeaderResponse({
    this.msg_cd = KEY_ERROR,
    this.message = ' ',
  });

  int getCode() => msg_cd;

  String getMsg() => message;

  @JsonKey(name: Key_msg_cd)
  int msg_cd;

  @JsonKey(name: Key_message)
  String message;

  factory HeaderResponse.fromJson(Map<String, dynamic> json) {
    int msgCd = KEY_ERROR;
    String message = "에러_예외처리확인".tr();
    try {
      if(json[Key_msg_cd].runtimeType == int){
        msgCd = json[Key_msg_cd] ?? KEY_ERROR;
      }else{
        msgCd = KEY_ERROR;
      }
      message = json[Key_message] ?? '';
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return HeaderResponse(
      msg_cd: msgCd,
      message: message,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_msg_cd] = this.msg_cd;
    data[Key_message] = this.message;
    return data;
  }

  @override
  String toString() {
    return 'HeaderResponse{dataCookies: $dataCookies, msg_cd: $msg_cd, message: $message}';
  }
}
