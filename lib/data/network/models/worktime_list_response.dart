import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class WorkTimeListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<WorkTimeData>? dataList;

  WorkTimeListResponse({this.dataHeader, this.dataList});

  WorkTimeData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : WorkTimeData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory WorkTimeListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<WorkTimeData> datas = [];
    try {
      datas = json[Key_data].map<WorkTimeData>((e) => WorkTimeData.fromJson(e)).toList();
    } catch (e) {
      log.e("EXCEPION >>>>>>>>>>>>>>>>>> $e");
    }
    return WorkTimeListResponse(dataHeader: dataHeader, dataList: datas);
  }
}

@JsonSerializable()
class WorkTimeData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<WorkTimeResults>? results;

  WorkTimeData({this.count = 0, this.next = '', this.previous = '', this.results});

  factory WorkTimeData.fromJson(Map<String, dynamic> json) {
    return WorkTimeData(
        count: json[Key_count] as int,
        next: json[Key_next] ?? '',
        previous: json[Key_previous] ?? '',
        results: json[Key_results] != null && json[Key_results].isNotEmpty
            ? json[Key_results].map<WorkTimeResults>((e) => WorkTimeResults.fromJson(e)).toList()
            : []);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    if (this.results != null) data[Key_results] = this.results;
    return data;
  }
}

@JsonSerializable()
class WorkTimeResults {
  static const String Key_schedule_id = 'schedule_id';
  static const String Key_pay_status = 'pay_status';
  static const String Key_start_caredate = 'start_caredate';
  static const String Key_start_allim = 'start_allim';
  static const String Key_end_caredate = 'end_caredate';
  static const String Key_end_allim = 'end_allim';
  static const String Key_child_name = 'child_name';

  @JsonKey(name: Key_schedule_id)
  int scheduleId;
  @JsonKey(name: Key_pay_status)
  String payStatus;
  @JsonKey(name: Key_start_caredate)
  String startCaredate;
  @JsonKey(name: Key_start_allim)
  String startAllim;
  @JsonKey(name: Key_end_caredate)
  String endCaredate;
  @JsonKey(name: Key_end_allim)
  String endAllim;
  @JsonKey(name: Key_child_name)
  String childName;

  WorkTimeResults({this.scheduleId = 0, this.payStatus = '', this.startCaredate = '', this.startAllim = '', this.endCaredate = '', this.endAllim = '', this.childName = ''});

  factory WorkTimeResults.fromJson(Map<String, dynamic> json) {
    return WorkTimeResults(
      scheduleId: json[Key_schedule_id] ?? 0,
      payStatus: json[Key_pay_status] ?? '',
      startCaredate: json[Key_start_caredate] ?? '',
      startAllim: json[Key_start_allim] ?? '',
      endCaredate: json[Key_end_caredate] ?? '',
      endAllim: json[Key_end_allim] ?? '',
      childName: json[Key_child_name] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_schedule_id] = this.scheduleId;
    data[Key_pay_status] = this.payStatus;
    data[Key_start_caredate] = this.startCaredate;
    data[Key_start_allim] = this.startAllim;
    data[Key_end_caredate] = this.endCaredate;
    data[Key_end_allim] = this.endAllim;
    data[Key_child_name] = this.childName;
    return data;
  }
}
