import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/chat_room_data.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_room_init_response.dart - 채팅 방 생성(방 조회)응답
/// since        : 2021/07/15 / update:
@JsonSerializable()
class ChatRoomInitResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ChatRoomData>? dataList;

  ChatRoomInitResponse({
    this.dataHeader,
    this.dataList,
  });

  ChatRoomData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ChatRoomData();

  List<ChatRoomData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ChatRoomInitResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ChatRoomData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<ChatRoomData>((i) => ChatRoomData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return ChatRoomInitResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ChatRoomInitResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
