import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class SettingsResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  SettingsResponse({
    this.dataHeader,
    this.dataList,
  });

  SettingsData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : SettingsData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory SettingsResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<SettingsData> datas = [];
    try {
      datas = json[Key_data].map<SettingsData>((i) => SettingsData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return SettingsResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ReqauthResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class SettingsData {
  static const String Key_is_push_service = "is_push_service";
  static const String Key_is_push_momdady = "is_push_momdady";
  static const String Key_is_push_linkmom = "is_push_linkmom";
  static const String Key_is_push_care = "is_push_care";
  static const String Key_is_push_job = "is_push_job";
  static const String Key_is_push_event = "is_push_event";
  static const String Key_is_auth_login = "is_auth_login";
  static const String Key_is_push_community = "is_push_community";

  SettingsData({
    this.is_push_service = true,
    this.is_push_momdady = true,
    this.is_push_linkmom = true,
    this.is_push_care = true,
    this.is_push_job = true,
    this.is_push_event = false,
    this.is_auth_login = false,
    this.is_push_community = true,
  });

  @JsonKey(name: Key_is_push_service)
  bool is_push_service;
  @JsonKey(name: Key_is_push_momdady)
  bool is_push_momdady;
  @JsonKey(name: Key_is_push_linkmom)
  bool is_push_linkmom;
  @JsonKey(name: Key_is_push_care)
  bool is_push_care;
  @JsonKey(name: Key_is_push_job)
  bool is_push_job;
  @JsonKey(name: Key_is_push_event)
  bool is_push_event;
  @JsonKey(name: Key_is_auth_login)
  bool is_auth_login;
  @JsonKey(name: Key_is_push_community)
  bool is_push_community;

  factory SettingsData.fromJson(Map<String, dynamic> json) {
    return SettingsData(
      is_push_service: json[Key_is_push_service] ?? true,
      is_push_momdady: json[Key_is_push_momdady] ?? true,
      is_push_linkmom: json[Key_is_push_linkmom] ?? true,
      is_push_care: json[Key_is_push_care] ?? true,
      is_push_job: json[Key_is_push_job] ?? true,
      is_push_event: json[Key_is_push_event] ?? false,
      is_auth_login: json[Key_is_auth_login] ?? false,
      is_push_community: json[Key_is_push_community] ?? true,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_is_push_service] = this.is_push_service;
    data[Key_is_push_momdady] = this.is_push_momdady;
    data[Key_is_push_linkmom] = this.is_push_linkmom;
    data[Key_is_push_care] = this.is_push_care;
    data[Key_is_push_job] = this.is_push_job;
    data[Key_is_push_event] = this.is_push_event;
    data[Key_is_auth_login] = this.is_auth_login;
    data[Key_is_push_community] = this.is_push_community;
    return data;
  }

  @override
  String toString() {
    return 'SettingsData{is_push_service: $is_push_service, is_push_momdady: $is_push_momdady, is_push_linkmom: $is_push_linkmom, is_push_care: $is_push_care, is_push_job: $is_push_job, is_push_event: $is_push_event, is_auth_login: $is_auth_login, is_push_community: $is_push_community}';
  }
}
