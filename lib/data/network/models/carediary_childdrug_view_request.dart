import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class CareDiaryChildDrugViewRequest {
  static const String Key_childdrug_id = 'childdrug_id';
  static const String Key_carecontract_schedule = 'carecontract_schedule';

  CareDiaryChildDrugViewRequest({this.childdrug_id = 0, this.carecontract_schedule = 0});

  @JsonKey(name: Key_childdrug_id)
  int childdrug_id;
  @JsonKey(name: Key_carecontract_schedule)
  int carecontract_schedule;

  factory CareDiaryChildDrugViewRequest.fromJson(Map<String, dynamic> json) {
    return CareDiaryChildDrugViewRequest(
      childdrug_id: json[Key_childdrug_id],
      carecontract_schedule: json[Key_carecontract_schedule],
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_childdrug_id] = this.childdrug_id;
    data[Key_carecontract_schedule] = this.carecontract_schedule;
    return data;
  }

  Future<FormData> toFormData(FormData formData) async {
    formData.fields.add(MapEntry(Key_childdrug_id, this.childdrug_id.toString()));
    formData.fields.add(MapEntry(Key_carecontract_schedule, this.carecontract_schedule.toString()));
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'CareDiaryChildDrugViewRequest{childdrug_id: $childdrug_id, carecontract_schedule: $carecontract_schedule}';
  }
}
