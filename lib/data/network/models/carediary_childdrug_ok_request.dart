import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryChildDrugOkRequest {
  static const String Key_childdruglist_id = 'childdruglist_id';
  static const String Key_schedule_id = 'schedule_id';

  CareDiaryChildDrugOkRequest({this.childdruglist_id = 0, this.schedule_id = 0});

  @JsonKey(name: Key_childdruglist_id)
  int childdruglist_id;

  @JsonKey(name: Key_schedule_id)
  int schedule_id;

  factory CareDiaryChildDrugOkRequest.fromJson(Map<String, dynamic> json) {
    return CareDiaryChildDrugOkRequest(childdruglist_id: json[Key_childdruglist_id], schedule_id: json[Key_schedule_id]);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_childdruglist_id] = this.childdruglist_id;
    data[Key_schedule_id] = this.schedule_id;
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryChildDrugOkRequest: {childdruglist_id: $childdruglist_id, schedule_id: $schedule_id}';
  }
}
