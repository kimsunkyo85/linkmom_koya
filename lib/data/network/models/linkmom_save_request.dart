import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : linkmom_save_request.dart - 구직신청
/// since        : 5/18/21 / update:
@JsonSerializable()
class LinkMomSaveRequest {
  static const String Key_reqdata = "reqdata";

  LinkMomSaveRequest({
    this.reqdata = '',
  });

  @JsonKey(name: Key_reqdata)
  String reqdata;

  factory LinkMomSaveRequest.fromJson(Map<String, dynamic> json) {
    return LinkMomSaveRequest(
      reqdata: json[Key_reqdata] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_reqdata] = this.reqdata;
    return data;
  }

  @override
  String toString() {
    return 'JobSaveRequest{reqdata: $reqdata}';
  }
}
