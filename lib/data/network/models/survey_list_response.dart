import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';
import 'header_response.dart';

@JsonSerializable()
class SurveyListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  SurveyListResponse({
    this.dataHeader,
    this.dataList,
  });

  SurveyData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : SurveyData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory SurveyListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<SurveyData> datas = [];
    try {
      datas = json[Key_data].map<SurveyData>((i) => SurveyData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return SurveyListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ReqauthResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class SurveyData {
  static const String Key_count = "count";
  static const String Key_next = "next";
  static const String Key_previous = "previous";
  static const String Key_results = "results";

  SurveyData({
    this.count = 0,
    this.next = '',
    this.previous = '',
    this.results,
  });

  @JsonKey(name: Key_count)
  int count;

  @JsonKey(name: Key_next)
  String next;

  @JsonKey(name: Key_previous)
  String previous;

  @JsonKey(name: Key_results)
  List<SurveyInfoData>? results;

  factory SurveyData.fromJson(Map<String, dynamic> json) {
    List<SurveyInfoData> datas = [];
    try {
      datas = json[Key_results].map<SurveyInfoData>((i) => SurveyInfoData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return SurveyData(count: json[Key_count] ?? 0, next: json[Key_next] ?? '', previous: json[Key_previous] ?? '', results: datas);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results;
    return data;
  }

  @override
  String toString() {
    return 'SurveyData{count: $count, next: $next, previous: $previous, results: ${results.toString()}}';
  }
}

@JsonSerializable()
class SurveyInfoData {
  static const String Key_id = "id";
  static const String Key_surveyname = "surveyname";
  static const String Key_start_date = "start_date";
  static const String Key_end_date = "end_date";
  static const String Key_answer = "answer";
  static const String Key_is_nar = "is_nar";

  SurveyInfoData({this.id = 0, this.surveyname = '', this.start_date = '', this.end_date = '', this.answer = '', this.is_nar = false});

  @JsonKey(name: Key_id)
  int id;

  @JsonKey(name: Key_surveyname)
  String surveyname;

  @JsonKey(name: Key_start_date)
  String start_date;

  @JsonKey(name: Key_end_date)
  String end_date;

  @JsonKey(name: Key_answer)
  String answer;

  @JsonKey(name: Key_is_nar)
  bool is_nar;

  factory SurveyInfoData.fromJson(Map<String, dynamic> json) {
    return SurveyInfoData(id: json[Key_id] ?? 0, surveyname: json[Key_surveyname] ?? '', start_date: json[Key_start_date] ?? '', end_date: json[Key_end_date] ?? '', answer: json[Key_answer] ?? '', is_nar: json[Key_is_nar] ?? false);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_surveyname] = this.surveyname;
    data[Key_start_date] = this.start_date;
    data[Key_end_date] = this.end_date;
    data[Key_answer] = this.answer;
    data[Key_is_nar] = this.is_nar;
    return data;
  }

  @override
  String toString() {
    return 'SurveyQuestionData{$Key_id: $id, $Key_surveyname: $surveyname, $Key_start_date: $start_date, $Key_end_date: $end_date, $Key_answer: $answer, $Key_is_nar: $is_nar}';
  }
}
