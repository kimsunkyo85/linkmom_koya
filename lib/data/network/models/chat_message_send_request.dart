import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_message_send_request.dart - 채팅 메시지 전송
/// since        : 2021/07/16 / update:
@JsonSerializable()
class ChatMessageSendRequest {
  static const String Key_chattingroom = 'chattingroom';
  static const String Key_receiver = 'receiver';
  static const String Key_msgtype = 'msgtype';
  static const String Key_msg = 'msg';
  static const String Key_image = 'image';
  static const String Key_msgdata = 'msgdata';
  static const String Key_sendtimestamp = 'sendtimestamp';

  ChatMessageSendRequest({
    this.chattingroom = 0,
    this.receiver = 0,
    this.msgtype = 0,
    this.msg = '',
    this.image,
    this.msgdata = '',
    this.sendtimestamp = '',
  });

  @JsonKey(name: Key_chattingroom)
  final int? chattingroom;
  @JsonKey(name: Key_receiver)
  final int? receiver;
  @JsonKey(name: Key_msgtype)
  final int? msgtype;
  @JsonKey(name: Key_msg)
  final String? msg;
  @JsonKey(name: Key_image)
  final File? image;
  @JsonKey(name: Key_msgdata)
  final String? msgdata;
  @JsonKey(name: Key_sendtimestamp)
  final String? sendtimestamp;

  factory ChatMessageSendRequest.fromJson(Map<String, dynamic> json) {
    return ChatMessageSendRequest(
      chattingroom: json[Key_chattingroom] as int,
      receiver: json[Key_receiver] as int,
      msgtype: json[Key_msgtype] as int,
      msg: json[Key_msg] ?? '',
      image: json[Key_image],
      msgdata: json[Key_msgdata] ?? '',
      sendtimestamp: json[Key_sendtimestamp] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_chattingroom] = this.chattingroom;
    data[Key_receiver] = this.receiver;
    data[Key_msgtype] = this.msgtype;
    data[Key_msg] = this.msg;
    data[Key_image] = this.image;
    data[Key_msgdata] = this.msgdata;
    data[Key_sendtimestamp] = this.sendtimestamp;
    return data;
  }

  Future<FormData> toFormData() async {
    FormData formData = FormData();
    if (chattingroom != null) {
      formData.fields.add(MapEntry(Key_chattingroom, chattingroom.toString()));
    }
    if (receiver != null) {
      formData.fields.add(MapEntry(Key_receiver, receiver.toString()));
    }
    if (msgtype != null) {
      formData.fields.add(MapEntry(Key_msgtype, msgtype.toString()));
    }
    if (msg != null) {
      formData.fields.add(MapEntry(Key_msg, msg.toString()));
    }
    if (image != null) {
      String fileName = image!.path.split('/').last;
      log.d('『GGUMBI』>>> setEncryptFormDataFiles : fileName: $fileName,  <<< ');
      formData.files.add(MapEntry(Key_image, await MultipartFile.fromFile(image!.path, filename: fileName, contentType: MediaType('image', 'jpeg'))));
    }
    if (msgdata != null) {
      formData.fields.add(MapEntry(Key_msgdata, msgdata.toString()));
    }
    if (sendtimestamp != null) {
      formData.fields.add(MapEntry(Key_sendtimestamp, sendtimestamp.toString()));
    }
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.files},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'ChatMessageSendRequest{chattingroom: $chattingroom, receiver: $receiver, msgtype: $msgtype, msg: $msg, image: $image, msgdata: $msgdata, sendtimestamp: $sendtimestamp}';
  }
}
