import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///
/// Created by   : platformbiz@ggumbi.com
///
/// version      : 1.0.0
///
/// see          : pay_cancel_init_request.dart - 결제취소 요청전 POST
///
/// since        : 2021/09/08 / update:
///
/// (0, '계약중'),
///
/// (11, '계약실패(거절/취소/응답없음/시간만료 등)'),
///
/// (21, '계약완료 후 취소(부분)'),
///
/// (22, '계약완료 후 취소(전체)'),
///
/// (92, '매칭성공(계약완료/결제대기중)'),
///
/// (100, '결제완료')
///
/// "msg_cd": 5400, "message": "취소 할 수 없는 결제 입니다.",
///
/// "msg_cd": 5401, "message": "이미 취소된 결제 입니다.",
///
/// "msg_cd": 4000, "message": "데이터가 없습니다.",
@JsonSerializable()
class PayCancelViewRequest {
  static const String Key_order_id = 'order_id';
  static const String Key_contract_id = 'contract_id';
  static const String Key_cancel_id = 'cancel_id';

  PayCancelViewRequest({
    this.order_id = '',
    this.contract_id = 0,
    this.cancel_id = 0,
  });

  PayCancelViewRequest.clone(PayCancelViewRequest item)
      : this(
          order_id: item.order_id,
          contract_id: item.contract_id,
          cancel_id: item.cancel_id,
        );

  ///주문번호
  @JsonKey(name: Key_order_id)
  final String? order_id;

  ///계약서 번호
  @JsonKey(name: Key_contract_id)
  final int? contract_id;

  ///취소내역(돌봄 id)
  @JsonKey(name: Key_cancel_id)
  int? cancel_id;

  factory PayCancelViewRequest.fromJson(Map<String, dynamic> json) {
    return PayCancelViewRequest(
      order_id: json[Key_order_id] ?? '',
      contract_id: json[Key_contract_id] as int,
      cancel_id: json[Key_cancel_id] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_order_id] = this.order_id;
    data[Key_contract_id] = this.contract_id;
    data[Key_cancel_id] = this.cancel_id;
    return data;
  }

  @override
  String toString() {
    return 'PayCancelViewRequest{order_id: $order_id, contract_id: $contract_id, cancel_id: $cancel_id}';
  }
}
