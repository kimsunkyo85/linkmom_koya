import 'dart:io';
import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:http_parser/http_parser.dart';

import '../../../main.dart';

@JsonSerializable()
class AuthCenterDeungbonRequest {
  static const String Key_name = 'name';
  static const String Key_birth = 'birth';
  static const String Key_bcode = 'bcode';
  static const String Key_hcode = 'hcode';
  static const String Key_region_1depth = 'region_1depth';
  static const String Key_region_2depth = 'region_2depth';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_3depth_h = 'region_3depth_h';
  static const String Key_region_4depth = 'region_4depth';
  static const String Key_deungbon_picture = 'deungbon_picture';
  static const String Key_childinfo = 'childinfo';

  AuthCenterDeungbonRequest({this.name = '', this.birth = '', this.bcode = '', this.hcode = '', this.region_1depth = '', this.region_2depth = '', this.region_3depth = '', this.region_3depth_h = '', this.region_4depth = '', this.deungbon_picture, this.childinfo});

  // user name
  @JsonKey(name: Key_name)
  String name;

  // user birth
  @JsonKey(name: Key_birth)
  String birth;

  // 법정동 코드
  @JsonKey(name: Key_bcode)
  String bcode;

  // 행정동 코드
  @JsonKey(name: Key_hcode)
  String hcode;

  // depth 1 (시/도)
  @JsonKey(name: Key_region_1depth)
  String region_1depth;

  // depth 2 (시/구)
  @JsonKey(name: Key_region_2depth)
  String region_2depth;

  // depth 3 (동/읍/면 - 하동)
  @JsonKey(name: Key_region_3depth)
  String region_3depth;

  // depth 4 (동/읍/면 - 광교2동)
  @JsonKey(name: Key_region_3depth_h)
  String region_3depth_h;

  // 상세주소
  @JsonKey(name: Key_region_4depth)
  String region_4depth;

  // 등본
  @JsonKey(name: Key_deungbon_picture)
  File? deungbon_picture;

  // child info
  @JsonKey(name: Key_childinfo)
  List<String>? childinfo = [];

  factory AuthCenterDeungbonRequest.fromJson(Map<String, dynamic> json) {
    return AuthCenterDeungbonRequest(
      name: json[Key_name] ?? 0,
      birth: json[Key_birth] ?? '',
      bcode: json[Key_bcode] ?? '',
      hcode: json[Key_hcode] ?? '',
      region_1depth: json[Key_region_1depth] ?? '',
      region_2depth: json[Key_region_2depth] ?? '',
      region_3depth: json[Key_region_3depth] ?? '',
      region_3depth_h: json[Key_region_3depth_h] ?? '',
      region_4depth: json[Key_region_4depth] ?? '',
      deungbon_picture: json[Key_deungbon_picture] ?? '',
      childinfo: json[Key_childinfo] ?? [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_name] = this.name;
    data[Key_birth] = this.birth;
    data[Key_bcode] = this.bcode;
    data[Key_hcode] = this.hcode;
    data[Key_region_1depth] = this.region_1depth;
    data[Key_region_2depth] = this.region_2depth;
    data[Key_region_3depth] = this.region_3depth;
    data[Key_region_3depth_h] = this.region_3depth_h;
    data[Key_region_4depth] = this.region_4depth;
    data[Key_deungbon_picture] = this.deungbon_picture;
    data[Key_childinfo] = this.childinfo;
    return data;
  }

  Future<FormData> toFormData(FormData formData) async {
    formData.fields.add(MapEntry(Key_name, await encryptHelper.encodeData(this.name.toString())));
    formData.fields.add(MapEntry(Key_birth, await encryptHelper.encodeData(this.birth.toString())));
    formData.fields.add(MapEntry(Key_bcode, await encryptHelper.encodeData(this.bcode.toString())));
    formData.fields.add(MapEntry(Key_hcode, await encryptHelper.encodeData(this.hcode.toString())));
    formData.fields.add(MapEntry(Key_region_1depth, await encryptHelper.encodeData(this.region_1depth.toString())));
    formData.fields.add(MapEntry(Key_region_2depth, await encryptHelper.encodeData(this.region_2depth.toString())));
    formData.fields.add(MapEntry(Key_region_3depth, await encryptHelper.encodeData(this.region_3depth.toString())));
    formData.fields.add(MapEntry(Key_region_3depth_h, await encryptHelper.encodeData(this.region_3depth_h.toString())));
    formData.fields.add(MapEntry(Key_region_4depth, await encryptHelper.encodeData(this.region_4depth.toString())));
    childinfo!.forEach((element) {
      formData.fields.add(MapEntry(Key_childinfo, element.toString()));
    });
    var file = await encryptHelper.encodeData(this.deungbon_picture);
    formData.files.add(MapEntry(Key_deungbon_picture, MultipartFile.fromString(file, filename: this.deungbon_picture!.path.split('/').last, contentType: MediaType('image', 'jpeg'))));
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'AuthCenterDeungbonRequest{name: $name, birth:$birth}';
  }
}

@JsonSerializable()
class DeungbonChildData {
  static const String Key_name = 'name';
  static const String Key_birth = 'birth';

  DeungbonChildData({
    this.name = '',
    this.birth = '',
  });

  @JsonKey(name: Key_name)
  String name;
  @JsonKey(name: Key_birth)
  String birth;

  factory DeungbonChildData.fromJson(Map<String, dynamic> json) {
    return DeungbonChildData(
      name: json[Key_name] ?? '',
      birth: json[Key_birth] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_name] = this.name;
    data[Key_birth] = this.birth;
    return data;
  }

  @override
  String toString() {
    return 'DeungbonChildData{name: $name, birth:$birth}';
  }
}
