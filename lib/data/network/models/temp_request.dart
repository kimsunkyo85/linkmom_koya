import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class TempRequest {
  static const String Key_child = 'child';
  static const String Key_servicetype = 'servicetype';
  static const String Key_depth = 'depth';

  TempRequest({
    this.child = 0,
    this.servicetype = 0,
    this.depth = 0,
  });

  @JsonKey(name: Key_child)
  int child;
  @JsonKey(name: Key_servicetype)
  int servicetype;
  @JsonKey(name: Key_depth)
  int depth;

  factory TempRequest.fromJson(Map<String, dynamic> json) {
    return TempRequest(
      child: json[Key_child] as int,
      servicetype: json[Key_servicetype] as int,
      depth: json[Key_depth] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_child] = this.child;
    data[Key_servicetype] = this.servicetype;
    data[Key_depth] = this.depth;
    // data[Key_reqdata] = this.reqdata;
    return data;
  }

  @override
  String toString() {
    return 'TempRequest{child: $child, servicetype: $servicetype, depth: $depth}';
  }
}
