import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

@JsonSerializable()
class ChildDrugSaveReqeust {
  static const String Key_child = 'child';
  static const String Key_user_signature = 'user_signature';
  static const String Key_reqdata = 'reqdata';

  ChildDrugSaveReqeust({
    this.child = 0,
    required this.user_signature,
    this.reqdata,
  });

  @JsonKey(name: Key_child)
  final int child;
  @JsonKey(name: Key_user_signature)
  final File user_signature;
  @JsonKey(name: Key_reqdata)
  final ChildDrugData? reqdata;

  factory ChildDrugSaveReqeust.fromJson(Map<String, dynamic> json) {
    return ChildDrugSaveReqeust(
      child: json[Key_child] as int,
      user_signature: json[Key_user_signature] ?? '',
      reqdata: json[Key_reqdata] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_child] = this.child;
    data[Key_reqdata] = this.reqdata;
    data[Key_user_signature] = this.user_signature.toString();
    return data;
  }

  FormData toFormData(FormData formData) {
    formData.fields.add(MapEntry(Key_child, this.child.toString()));
    formData.fields.add(MapEntry(Key_reqdata, jsonEncode(this.reqdata)));
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'ChildDrugSaveReqeust{child: $child, user_signature:$user_signature, druglist:${reqdata.toString()}}';
  }
}

@JsonSerializable()
class ChildDrugData {
  static const String Key_druglist = 'druglist';
  static const String Key_drugdate = 'drugdate';

  ChildDrugData({
    this.drugdate,
    this.druglist,
  });

  @JsonKey(name: Key_druglist)
  List<ChildDrugInfoData>? druglist;
  @JsonKey(name: Key_drugdate)
  List<String>? drugdate;

  factory ChildDrugData.fromJson(Map<String, dynamic> json) {
    List<ChildDrugInfoData> drugList = List<ChildDrugInfoData>.from(json[Key_druglist]);
    List<String> drugdate = List<String>.from(json[Key_drugdate]);
    return ChildDrugData(
      druglist: drugList,
      drugdate: drugdate,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_druglist] = this.druglist;
    data[Key_drugdate] = this.drugdate;
    return data;
  }

  @override
  String toString() {
    return 'ChildDrugData{$Key_druglist: $druglist, $Key_drugdate: $drugdate}';
  }
}

@JsonSerializable()
class ChildDrugInfoData {
  static const String Key_symptom = 'symptom';
  static const String Key_drug_time = 'drug_time';
  static const String Key_drug_timetype = 'drug_timetype';
  static const String Key_drug_type = 'drug_type';
  static const String Key_drug_keeping = 'drug_keeping';
  static const String Key_drug_repeat = 'drug_repeat';
  static const String Key_drug_amount_type0 = 'drug_amount_type0';
  static const String Key_drug_amount_type1 = 'drug_amount_type1';
  static const String Key_drug_amount_type2 = 'drug_amount_type2';
  static const String Key_drug_amount_type3 = 'drug_amount_type3';
  static const String Key_drug_reqmessage = 'drug_reqmessage';

  ChildDrugInfoData({
    this.symptom = '',
    this.drug_time = '',
    this.drug_timetype = 0,
    this.drug_type,
    this.drug_repeat = '',
    this.drug_amount_type0 = '',
    this.drug_amount_type1 = '',
    this.drug_amount_type2 = '',
    this.drug_amount_type3 = '',
    this.drug_reqmessage = '',
    this.drug_keeping = 0,
  });

  @JsonKey(name: Key_symptom)
  String symptom;
  @JsonKey(name: Key_drug_time)
  String drug_time;
  @JsonKey(name: Key_drug_timetype)
  int drug_timetype;
  @JsonKey(name: Key_drug_type)
  List<int>? drug_type;
  @JsonKey(name: Key_drug_keeping)
  int drug_keeping;
  @JsonKey(name: Key_drug_repeat)
  String drug_repeat;
  @JsonKey(name: Key_drug_amount_type0)
  String drug_amount_type0;
  @JsonKey(name: Key_drug_amount_type1)
  String drug_amount_type1;
  @JsonKey(name: Key_drug_amount_type2)
  String drug_amount_type2;
  @JsonKey(name: Key_drug_amount_type3)
  String drug_amount_type3;
  @JsonKey(name: Key_drug_reqmessage)
  String drug_reqmessage;

  factory ChildDrugInfoData.fromJson(Map<String, dynamic> json) {
    List<int> drugTypes = List<int>.from(json[Key_drug_type]);
    return ChildDrugInfoData(
      symptom: json[Key_symptom] ?? '',
      drug_time: json[Key_drug_time] ?? '',
      drug_timetype: json[Key_drug_timetype] as int,
      drug_type: drugTypes,
      drug_keeping: json[Key_drug_keeping] ?? [],
      drug_repeat: json[Key_drug_repeat] ?? [],
      drug_amount_type0: json[Key_drug_amount_type0] ?? [],
      drug_amount_type1: json[Key_drug_amount_type1] ?? [],
      drug_amount_type2: json[Key_drug_amount_type2] ?? [],
      drug_amount_type3: json[Key_drug_amount_type3] ?? [],
      drug_reqmessage: json[Key_drug_reqmessage] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_symptom] = this.symptom;
    data[Key_drug_time] = this.drug_time;
    data[Key_drug_timetype] = this.drug_timetype;
    data[Key_drug_type] = this.drug_type;
    data[Key_drug_keeping] = this.drug_keeping;
    data[Key_drug_repeat] = this.drug_repeat;
    data[Key_drug_amount_type0] = this.drug_amount_type0;
    data[Key_drug_amount_type1] = this.drug_amount_type1;
    data[Key_drug_amount_type2] = this.drug_amount_type2;
    data[Key_drug_amount_type3] = this.drug_amount_type2;
    data[Key_drug_reqmessage] = this.drug_reqmessage;
    return data;
  }

  @override
  String toString() {
    return 'ChildDrugData{symptom: $symptom,drug_time: $drug_time, drug_timetype: $drug_timetype, drug_type: $drug_type, drug_keeping: $drug_keeping, drug_repeat: $drug_repeat, drug_amount_type0: $drug_amount_type0, drug_amount_type1: $drug_amount_type1, drug_amount_type2: $drug_amount_type2, drug_amount_type3: $drug_amount_type3, drug_reqmessage: $drug_reqmessage}';
  }
}
