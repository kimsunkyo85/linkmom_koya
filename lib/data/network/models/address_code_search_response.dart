import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

@JsonSerializable()
class AddressCodeSearchResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<AreaCodeSearchData>? dataList;

  AddressCodeSearchResponse({
    this.dataHeader,
    this.dataList,
  });

  AreaCodeSearchData getData() => dataList != null && dataList!.length != 0 ? dataList!.first : AreaCodeSearchData();

  List<AreaCodeSearchData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory AddressCodeSearchResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<AreaCodeSearchData> datas = [];
    try {
      datas = json[Key_data].map<AreaCodeSearchData>((i) => AreaCodeSearchData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return AddressCodeSearchResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AuthCenterHomeResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class AreaCodeSearchData {
  static const String Key_address_code = 'address_code';
  static const String Key_address1 = 'address1';
  static const String Key_address2 = 'address2';
  static const String Key_address3 = 'address3';
  static const String Key_area_name = 'area_name';
  static const String Key_area_code = 'area_code';

  AreaCodeSearchData({
    this.address_code = '',
    this.address1 = '',
    this.address2 = '',
    this.address3 = '',
    this.area_name = '',
    this.area_code = '',
  });

  @JsonKey(name: Key_address_code)
  final String address_code;
  @JsonKey(name: Key_address1)
  final String address1;
  @JsonKey(name: Key_address2)
  final String address2;
  @JsonKey(name: Key_address3)
  final String address3;
  @JsonKey(name: Key_area_name)
  final String area_name;
  @JsonKey(name: Key_area_code)
  final String area_code;

  factory AreaCodeSearchData.fromJson(Map<String, dynamic> json) {
    return AreaCodeSearchData(
      address_code: json[Key_address_code] ?? '',
      address1: json[Key_address1] ?? '',
      address2: json[Key_address2] ?? '',
      address3: json[Key_address3] ?? '',
      area_name: json[Key_area_name] ?? '',
      area_code: json[Key_area_code] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_address_code] = this.address_code;
    data[Key_address1] = this.address1;
    data[Key_address2] = this.address2;
    data[Key_address3] = this.address3;
    data[Key_area_name] = this.area_name;
    data[Key_area_code] = this.area_code;
    return data;
  }

  @override
  String toString() {
    return 'AreaCodeSearchData{address_code: $address_code, address1: $address1, address2: $address2, address3: $address3, area_name: $area_name, area_code: $area_code}';
  }
}
