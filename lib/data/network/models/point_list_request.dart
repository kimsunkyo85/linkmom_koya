import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class PointListRequest {
  static const String Key_s_date = 's_date';
  static const String Key_e_date = 'e_date';
  static const String Key_point_flag = 'point_flag';

  @JsonKey(name: Key_s_date)
  String sDate;
  @JsonKey(name: Key_e_date)
  String eDate;
  @JsonKey(name: Key_point_flag)
  List<String>? pointFlag;

  PointListRequest({this.sDate = '', this.eDate = '', this.pointFlag});

  factory PointListRequest.fromJson(Map<String, dynamic> json) {
    return PointListRequest(
      sDate: json[Key_s_date],
      eDate: json[Key_e_date],
      pointFlag: json[Key_point_flag] == null ? null : List<String>.from(json[Key_point_flag]),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_s_date] = this.sDate;
    data[Key_e_date] = this.eDate;
    if (this.pointFlag != null) data[Key_point_flag] = this.pointFlag;
    return data;
  }

  FormData toFormData(FormData formData) {
    if (this.sDate.isNotEmpty) formData.fields.add(MapEntry(Key_s_date, sDate));
    if (this.eDate.isNotEmpty) formData.fields.add(MapEntry(Key_e_date, eDate));
    if (this.pointFlag != null && this.pointFlag!.isNotEmpty)
      pointFlag!.forEach((e) {
        formData.fields.add(MapEntry(Key_point_flag, e));
      });
    return formData;
  }

  @override
  String toString() {
    return 'PointListRequest{sDate:$sDate, eDate:$eDate, pointFlag:$pointFlag}';
  }
}
