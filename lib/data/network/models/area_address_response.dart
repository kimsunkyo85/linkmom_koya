import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/utils/string_util.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : area_address_response.dart - 동네 인증 최근 목록 가져오기(최근 값으로 인증 여부 판단하기)
/// since        : 5/7/21 / update:
@JsonSerializable()
class AreaAddressResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<AreaAddressData>? dataList;

  AreaAddressResponse({
    this.dataHeader,
    this.dataList,
  });

  AreaAddressData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : AreaAddressData();

  List<AreaAddressData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory AreaAddressResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<AreaAddressData> datas = [];
    try {
      datas = json[Key_data].map<AreaAddressData>((i) => AreaAddressData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return AreaAddressResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AreaAddressResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class AreaAddressData {
  static const String Key_address_id = 'address_id';
  static const String Key_bcode = 'bcode';
  static const String Key_hcode = 'hcode';
  static const String Key_region_1depth = 'region_1depth';
  static const String Key_region_2depth = 'region_2depth';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_3depth_h = 'region_3depth_h';
  static const String Key_region_building_name = 'region_building_name';
  static const String Key_is_gpswithaddress = 'is_gpswithaddress';
  static const String Key_is_default = 'is_default';
  static const String Key_is_affiliate = 'is_affiliate';
  static const String Key_address = 'address';

  AreaAddressData({
    this.address_id = 0,
    this.bcode = '',
    this.hcode = '',
    this.region_1depth = '',
    this.region_2depth = '',
    this.region_3depth = '',
    this.region_3depth_h = '',
    this.region_building_name = '',
    this.is_gpswithaddress = false,
    this.is_default = false,
    this.is_affiliate = false,
    this.address = '',
  });

  ///id
  @JsonKey(name: Key_address_id)
  final int address_id;

  ///법정동 코드
  @JsonKey(name: Key_bcode)
  final String bcode;

  ///행정동 코드
  @JsonKey(name: Key_hcode)
  final String hcode;

  ///경기도
  @JsonKey(name: Key_region_1depth)
  final String region_1depth;

  ///수원시 영통구
  @JsonKey(name: Key_region_2depth)
  final String region_2depth;

  ///하동
  @JsonKey(name: Key_region_3depth)
  final String region_3depth;

  ///광교2동
  @JsonKey(name: Key_region_3depth_h)
  final String region_3depth_h;

  ///제휴 빌딩 이름
  @JsonKey(name: Key_region_building_name)
  final String region_building_name;

  ///인증 여부 true, false
  @JsonKey(name: Key_is_gpswithaddress)
  final bool is_gpswithaddress;

  ///인증 기본 설정
  @JsonKey(name: Key_is_default)
  final bool is_default;

  ///제휴 인증 여부 true, false
  @JsonKey(name: Key_is_affiliate)
  final bool is_affiliate;

  ///리스트 조회시 3th -> 3th 없을경우 4th
  @JsonKey(name: Key_address)
  String address;

  factory AreaAddressData.fromJson(Map<String, dynamic> json) {
    return AreaAddressData(
      address_id: json[Key_address_id] as int,
      bcode: json[Key_bcode] ?? '',
      hcode: json[Key_hcode] ?? '',
      region_1depth: json[Key_region_1depth] ?? '',
      region_2depth: json[Key_region_2depth] ?? '',
      region_3depth: json[Key_region_3depth] ?? '',
      region_3depth_h: json[Key_region_3depth_h] ?? '',
      region_building_name: json[Key_region_building_name] ?? '',
      is_gpswithaddress: json[Key_is_gpswithaddress] ?? false,
      is_default: json[Key_is_default] ?? false,
      is_affiliate: json[Key_is_affiliate] ?? false,
      address: json[Key_address] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_address_id] = this.address_id;
    data[Key_bcode] = this.bcode;
    data[Key_hcode] = this.hcode;
    data[Key_region_1depth] = this.region_1depth;
    data[Key_region_2depth] = this.region_2depth;
    data[Key_region_3depth] = this.region_3depth;
    data[Key_region_3depth_h] = this.region_3depth_h;
    data[Key_region_building_name] = this.region_building_name;
    data[Key_is_gpswithaddress] = this.is_gpswithaddress;
    data[Key_is_default] = this.is_default;
    data[Key_is_affiliate] = this.is_affiliate;
    return data;
  }

  String getAddress() {
    String value = region_3depth;
    if (!StringUtils.validateString(region_3depth)) {
      value = region_3depth_h;
    }
    return value;
  }

  String getAddressListSearch() {
    log.d('『GGUMBI』>>> getAddressListSearch : is_affiliate: $is_affiliate,  <<< ');
    String value = getAddress();
    if (is_affiliate) {
      if (StringUtils.validateString(region_building_name)) {
        value = region_building_name;
      }
    }
    return value;
  }

  ///기본 주소 3depth 주소가 있으면 region_3depth_h
  String getAddress3depth() {
    String value = '$region_2depth $region_3depth';
    if (!StringUtils.validateString(region_3depth)) {
      value = '$region_2depth $region_3depth_h';
    }
    return value;
  }

  ///제휴 빌딩 이름 가져오기 (true = 아이파크시티, false = 수원시 권선구 권선동(아이파크시티)
  String getAddressAffiliateName({bool isBuildingName = false}) {
    String value = getAddress3depth();
    if (is_affiliate) {
      if (StringUtils.validateString(region_building_name)) {
        if (isBuildingName) {
          value = region_building_name;
        } else {
          value = '$value($region_building_name)';
        }
      }
    }
    return value;
  }

  @override
  String toString() {
    return 'AreaAddressData{address_id: $address_id, bcode: $bcode, hcode: $hcode, region_1depth: $region_1depth, region_2depth: $region_2depth, region_3depth: $region_3depth, region_3depth_h: $region_3depth_h, region_building_name: $region_building_name, is_gpswithaddress: $is_gpswithaddress, is_affiliate: $is_affiliate, is_default: $is_default, address: $address}';
  }
}
