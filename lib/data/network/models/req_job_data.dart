import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';

@JsonSerializable()
class ReqJobData {
  ///돌봄유형 (0:등원, 1:하원, 2:보육, 3:학습, 4:입주가사, 5:이유식)
  static const String Key_servicetype = 'servicetype';

  ///돌봄장소(1:우리집, 2:이웃집)
  static const String Key_possible_area = 'possible_area';

  ///그룹보육 동의 0:협의안함, 1:협의가능
  static const String Key_is_groupboyuk = 'is_groupboyuk';

  ///동시보육 동의 0:협의안함, 1:협의가능
  static const String Key_is_dongsiboyuk = 'is_dongsiboyuk';

  ///이동방법(1:도보, 2:자동차, 3:대중교통)
  static const String Key_vehicle_id = 'vehicle_id';

  ///1순위 행정동코드
  static const String Key_job_areaid_1 = 'job_areaid_1';

  ///1순위 법정동코드
  static const String Key_job_addressid_1 = 'job_addressid_1';

  ///1순위 장소
  static const String Key_job_area_address_1 = 'job_area_address_1';

  ///2순위 행정동코드
  static const String Key_job_areaid_2 = 'job_areaid_2';

  ///2순위 법정동 코드
  static const String Key_job_addressid_2 = 'job_addressid_2';

  ///2순위 장소
  static const String Key_job_area_address_2 = 'job_area_address_2';

  ///등원 돌봄 시급
  static const String Key_fee_gotoschool = 'fee_gotoschool';

  ///하원 돌봄 시급
  static const String Key_fee_afterschool = 'fee_afterschool';

  ///보육 시급
  static const String Key_fee_boyuk = 'fee_boyuk';

  ///돌봄대상 (0:신생아, 1:영아, 2:유아, 3:초등학생, 4:중학생, 5:고등학생)
  static const String Key_care_ages = 'care_ages';

  ///한줄자기소개
  static const String Key_introduce = 'introduce';

  ///돌봄방식 소개
  static const String Key_care_introduce = 'care_introduce';

  ///스케쥴 데이터
  static const String Key_careschedule = 'careschedule';

  static const String Key_caresdate_min = 'caredate_min';

  static const String Key_caresdate_max = 'caredate_max';

  ///구직중:0, 구직종료:1
  static const String Key_status = 'status';

  ReqJobData(
      {this.servicetype,
      this.possible_area,
      this.is_groupboyuk = 0,
      this.is_dongsiboyuk = 0,
      this.vehicle_id,
      this.job_areaid_1 = '',
      this.job_addressid_1 = '',
      this.job_area_address_1 = '',
      this.job_areaid_2 = '',
      this.job_addressid_2 = '',
      this.job_area_address_2 = '',
      this.fee_gotoschool = 0,
      this.fee_afterschool = 0,
      this.fee_boyuk = 0,
      this.care_ages,
      this.introduce = '',
      this.care_introduce = '',
      this.caredate_min = '',
      this.caredate_max = '',
      this.careschedule,
      this.status = 0});

  ReqJobData.init() {
    servicetype = [];
    possible_area = [];
    vehicle_id = [];
    care_ages = [];
    careschedule = [];
    fee_gotoschool = 9000;
    fee_afterschool = 9000;
    fee_boyuk = 9000;
    job_areaid_1 = '';
    job_addressid_1 = '';
    job_area_address_1 = '';
    job_areaid_2 = '';
    job_addressid_2 = '';
    job_area_address_2 = '';
    introduce = '';
    care_introduce = '';
    status = 0;
  }

  ReqJobData.clone(ReqJobData item)
      : this(
            servicetype: item.servicetype,
            possible_area: item.possible_area,
            is_groupboyuk: item.is_groupboyuk,
            is_dongsiboyuk: item.is_dongsiboyuk,
            vehicle_id: item.vehicle_id,
            job_areaid_1: item.job_areaid_1,
            job_addressid_1: item.job_addressid_1,
            job_area_address_1: item.job_area_address_1,
            job_areaid_2: item.job_areaid_2,
            job_addressid_2: item.job_addressid_2,
            job_area_address_2: item.job_area_address_2,
            fee_gotoschool: item.fee_gotoschool,
            fee_afterschool: item.fee_afterschool,
            fee_boyuk: item.fee_boyuk,
            care_ages: item.care_ages,
            introduce: item.introduce,
            care_introduce: item.care_introduce,
            careschedule: item.careschedule,
            caredate_min: item.caredate_min,
            caredate_max: item.caredate_max,
            status: item.status);

  ///돌봄유형 (0:등원, 1:하원, 2:보육, 3:학습, 4:입주가사, 5:이유식)
  @JsonKey(name: Key_servicetype)
  late List<int>? servicetype;

  ///돌봄장소(1:우리집, 2:이웃집)
  @JsonKey(name: Key_possible_area)
  late List<int>? possible_area;

  ///협의가능 협의용 금액 0:협의안함, 1:협의가능
  @JsonKey(name: Key_is_groupboyuk)
  int is_groupboyuk = 0;

  ///동시보육 동의 0:협의안함, 1:협의가능
  @JsonKey(name: Key_is_dongsiboyuk)
  int is_dongsiboyuk = 0;

  ///이동방법(1:도보, 2:자동차, 3:대중교통)
  @JsonKey(name: Key_vehicle_id)
  late List<int>? vehicle_id;

  ///1순위 법정동코드
  @JsonKey(name: Key_job_areaid_1)
  late String job_areaid_1;

  ///1순위 행정동코드
  @JsonKey(name: Key_job_addressid_1)
  late String job_addressid_1;

  ///1순위 장소
  @JsonKey(name: Key_job_area_address_1)
  late String job_area_address_1;

  ///2순위 법정동코드
  @JsonKey(name: Key_job_areaid_2)
  late String job_areaid_2;

  ///2순위 행정동코드
  @JsonKey(name: Key_job_addressid_2)
  late String job_addressid_2;

  ///2순위 장소
  @JsonKey(name: Key_job_area_address_2)
  late String job_area_address_2;

  ///등원 돌봄 시급
  @JsonKey(name: Key_fee_gotoschool)
  int fee_gotoschool = 0;

  ///하원 돌봄 시급
  @JsonKey(name: Key_fee_afterschool)
  int fee_afterschool = 0;

  ///보육 시급
  @JsonKey(name: Key_fee_boyuk)
  int fee_boyuk = 0;

  ///돌봄대상 (0:신생아, 1:영아, 2:유아, 3:초등학생, 4:중학생, 5:고등학생)
  @JsonKey(name: Key_care_ages)
  late List<int>? care_ages;

  ///한줄자기소개
  @JsonKey(name: Key_introduce)
  String introduce = '';

  ///돌봄방식소개
  @JsonKey(name: Key_care_introduce)
  String care_introduce = '';

  @JsonKey(name: Key_caresdate_min)
  String caredate_min = '';

  @JsonKey(name: Key_caresdate_max)
  String caredate_max = '';

  ///스케쥴 데이터
  @JsonKey(name: Key_careschedule)
  late List<ReqJobScheduleData>? careschedule;

  ///구직중 플래그 0:구인중, 1:종료
  @JsonKey(name: Key_status)
  late int status;

  factory ReqJobData.fromJson(Map<String, dynamic> json) {
    List<int> servicetype = json[Key_servicetype] == null ? [] : List<int>.from(json[Key_servicetype]);
    List<int> possibleArea = json[Key_possible_area] == null ? [] : List<int>.from(json[Key_possible_area]);
    List<int> vehicleId = json[Key_vehicle_id] == null ? [] : List<int>.from(json[Key_vehicle_id]);
    List<int> careAges = json[Key_care_ages] == null ? [] : List<int>.from(json[Key_care_ages]);
    List<ReqJobScheduleData> careschedule =
        json[Key_careschedule] == null ? [] : json[Key_careschedule].map<ReqJobScheduleData>((i) => ReqJobScheduleData.fromJson(i)).toList() as List<ReqJobScheduleData>;
    return ReqJobData(
      servicetype: servicetype,
      possible_area: possibleArea,
      is_groupboyuk: json[Key_is_groupboyuk] as int,
      is_dongsiboyuk: json[Key_is_dongsiboyuk] as int,
      vehicle_id: vehicleId,
      job_areaid_1: json[Key_job_areaid_1] ?? '',
      job_addressid_1: json[Key_job_addressid_1] ?? '',
      job_area_address_1: json[Key_job_area_address_1] ?? '',
      job_areaid_2: json[Key_job_areaid_2] ?? '',
      job_addressid_2: json[Key_job_addressid_2] ?? '',
      job_area_address_2: json[Key_job_area_address_2] ?? '',
      fee_gotoschool: json[Key_fee_gotoschool] as int,
      fee_afterschool: json[Key_fee_afterschool] as int,
      fee_boyuk: json[Key_fee_boyuk] as int,
      care_ages: careAges,
      introduce: json[Key_introduce] ?? '',
      care_introduce: json[Key_care_introduce] ?? '',
      caredate_min: json[Key_caresdate_min] ?? '',
      caredate_max: json[Key_caresdate_max] ?? '',
      careschedule: careschedule,
      status: json[Key_status] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_is_groupboyuk] = this.is_groupboyuk;
    data[Key_is_dongsiboyuk] = this.is_dongsiboyuk;
    data[Key_vehicle_id] = this.vehicle_id;
    data[Key_job_areaid_1] = this.job_areaid_1;
    data[Key_job_addressid_1] = this.job_addressid_1;
    data[Key_job_area_address_1] = this.job_area_address_1;
    data[Key_job_areaid_2] = this.job_areaid_2;
    data[Key_job_addressid_2] = this.job_addressid_2;
    data[Key_job_area_address_2] = this.job_area_address_2;
    data[Key_fee_gotoschool] = this.fee_gotoschool;
    data[Key_fee_afterschool] = this.fee_afterschool;
    data[Key_fee_boyuk] = this.fee_boyuk;
    data[Key_care_ages] = this.care_ages;
    data[Key_introduce] = this.introduce;
    data[Key_care_introduce] = this.care_introduce;
    data[Key_caresdate_min] = this.caredate_min;
    data[Key_caresdate_max] = this.caredate_max;
    data[Key_careschedule] = this.careschedule;
    data[Key_status] = this.status;
    return data;
  }

  ReqJobData clone() {
    final jsonResponse = json.decode(json.encode(this));
    return ReqJobData.fromJson(jsonResponse as Map<String, dynamic>);
  }

  BookingLinkMomStatus getStatus() {
    return EnumUtils.getBookingLinkMomStatus(status);
  }

  @override
  String toString() {
    return 'ReqJobData{servicetype: $servicetype, possible_area: $possible_area, is_groupboyuk: $is_groupboyuk, is_dongsiboyuk: $is_dongsiboyuk, job_areaid_1: $job_areaid_1, job_addressid_1: $job_addressid_1, job_area_address_1: $job_area_address_1, job_areaid_2: $job_areaid_2, job_addressid_2: $job_addressid_2, job_area_address_2: $job_area_address_2, fee_gotoschool: $fee_gotoschool, fee_afterschool: $fee_afterschool, fee_boyuk: $fee_boyuk, care_ages: $care_ages, introduce: $introduce, care_introduce: $care_introduce, caredate_min: $caredate_min, caredate_max: $caredate_max, careschedule: $careschedule, status: $status}';
  }
}

///스케쥴 정보(구직신청)
class ReqJobScheduleData {
  static const String Key_bookingdate = 'bookingdate';
  static const String Key_removedate = 'removedate';
  static const String Key_times = 'times';

  ReqJobScheduleData({
    this.bookingdate,
    this.removedate,
    this.times,
  });

  ///날짜 데이터
  @JsonKey(name: Key_bookingdate)
  List<String>? bookingdate;

  ///수정 날짜 데이터
  @JsonKey(name: Key_removedate)
  List<String>? removedate;

  ///수정 날짜 데이터
  @JsonKey(name: Key_times)
  List<ScheduleTimeData>? times;

  factory ReqJobScheduleData.fromJson(Map<String, dynamic> json) {
    List<String> bookingdate = json[Key_bookingdate] == null ? [] : List<String>.from(json[Key_bookingdate]);
    List<String> removedate = json[Key_removedate] == null ? [] : List<String>.from(json[Key_removedate]);
    List<ScheduleTimeData> times = json[Key_times] == null ? [] : json[Key_times].map<ScheduleTimeData>((i) => ScheduleTimeData.fromJson(i)).toList();
    return ReqJobScheduleData(
      bookingdate: bookingdate,
      removedate: removedate,
      times: times,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_bookingdate] = this.bookingdate;
    data[Key_removedate] = this.removedate;
    data[Key_times] = this.times;
    return data;
  }

  @override
  String toString() {
    return 'ReqJobScheduleData{bookingdate: $bookingdate, removedate: $removedate, times: $times}';
  }
}

///스케쥴 정보(구직신청)
class ScheduleTimeData {
  static const String Key_id = 'id';
  static const String Key_stime = 'stime';
  static const String Key_etime = 'etime';
  static const String Key_is_am = 'is_am';
  static const String Key_is_pm = 'is_pm';
  static const String Key_is_repeat = 'is_repeat';
  static const String Key_durationtime = 'durationtime';
  static const String Key_caredate_until = 'caredate_until';

  ScheduleTimeData({
    this.id = 0,
    this.stime = '',
    this.etime = '',
    this.is_am = 0,
    this.is_pm = 0,
    this.is_repeat = 0,
    this.durationtime = 0,
    this.caredate_until = '',
  });

  ///ID
  @JsonKey(name: Key_id)
  int id;

  ///시작 시간
  @JsonKey(name: Key_stime)
  String stime;

  ///종료 시간
  @JsonKey(name: Key_etime)
  String etime;

  ///오전 1, 아니면 0, 오전오후 걸치면 1
  @JsonKey(name: Key_is_am)
  int is_am;

  ///오후 1, 아니면 0, 오전오후 걸치면 1
  @JsonKey(name: Key_is_pm)
  int is_pm;

  ///반복
  @JsonKey(name: Key_is_repeat)
  int is_repeat;

  ///시간 합계(분)
  @JsonKey(name: Key_durationtime)
  int durationtime;

  @JsonKey(name: Key_caredate_until)
  String caredate_until;

  factory ScheduleTimeData.fromJson(Map<String, dynamic> json) {
    return ScheduleTimeData(
      id: json[Key_id] ?? '',
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      is_am: json[Key_is_am] as int,
      is_pm: json[Key_is_pm] as int,
      is_repeat: json[Key_is_repeat] as int,
      durationtime: json[Key_durationtime] as int,
      caredate_until: json[Key_caredate_until] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_is_am] = this.is_am;
    data[Key_is_pm] = this.is_pm;
    data[Key_is_repeat] = this.is_repeat;
    data[Key_durationtime] = this.durationtime;
    data[Key_caredate_until] = this.caredate_until;
    return data;
  }

  @override
  String toString() {
    return 'ScheduleTimeData{id: $id, stime: $stime, etime: $etime, is_am: $is_am, is_pm: $is_pm, is_repeat: $is_repeat, durationtime: $durationtime, caredate_until: $caredate_until}';
  }
}
