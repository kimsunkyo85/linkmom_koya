import 'package:json_annotation/json_annotation.dart';

import 'carediary_form_response.dart';
import 'data/care_diary_data.dart';
import 'header_response.dart';

@JsonSerializable()
class DiaryViewResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  @JsonKey(name: Key_data)
  List<DiaryViewModel>? dataList;

  DiaryViewResponse({this.dataHeader, this.dataList});

  DiaryViewModel getData() => dataList == null ? DiaryViewModel() : dataList!.first;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory DiaryViewResponse.fromJson(Map<String, dynamic> json) {
    return DiaryViewResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? [] : json[Key_data].map<DiaryViewModel>((i) => DiaryViewModel.fromJson(i)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList;
    return data;
  }

  @override
  String toString() {
    return 'DiaryViewResponse{dataHeader:$dataHeader, dataList:$dataList}';
  }
}

class DiaryViewModel {
  static const Key_matching = 'matching';
  static const String Key_carecontract_schedule = "carecontract_schedule";
  static const String Key_cost_sum = "cost_sum";
  static const String Key_pay_status = "pay_status";
  static const String Key_pay_status_text = "pay_status_text";
  static const String Key_servicetype = "servicetype";
  static const String Key_possible_area = "possible_area";
  static const String Key_stime = "stime";
  static const String Key_etime = "etime";
  static const String Key_caredate = "caredate";
  static const String Key_userinfo = "userinfo";
  static const String Key_childinfo = "childinfo";
  static const String Key_carediary_info = "carediary_info";
  static const String Key_carediary_images = "carediary_images";
  static const String Key_like_yn = "like_yn";
  static const String Key_claim_info = "claim_info";
  static const String Key_cancel_info = "cancel_info";
  static const String Key_carediary_reply_cnt = "carediary_reply_cnt";
  static const String Key_carediary_reply = "carediary_reply";
  static const Key_is_end_care = 'is_end_care';
  static const Key_is_review = 'is_review';
  static const Key_bookingcareservices = 'bookingcareservices';

  @JsonKey(name: Key_matching)
  int matching;
  @JsonKey(name: Key_carecontract_schedule)
  int carecontractSchedule;
  @JsonKey(name: Key_cost_sum)
  int costSum;
  @JsonKey(name: Key_pay_status)
  int payStatus;
  @JsonKey(name: Key_pay_status_text)
  String payStatusText;
  @JsonKey(name: Key_servicetype)
  String servicetype;
  @JsonKey(name: Key_possible_area)
  String possibleArea;
  @JsonKey(name: Key_stime)
  String stime;
  @JsonKey(name: Key_etime)
  String etime;
  @JsonKey(name: Key_caredate)
  List<String>? caredate;
  @JsonKey(name: Key_userinfo)
  Userinfo? userinfo;
  @JsonKey(name: Key_childinfo)
  Childinfo? childinfo;
  @JsonKey(name: Key_carediary_info)
  CarediaryInfo? carediaryInfo;
  @JsonKey(name: Key_carediary_images)
  List<CarediaryImages>? carediaryImages;
  @JsonKey(name: Key_like_yn)
  bool likeYn;
  @JsonKey(name: Key_claim_info)
  ClaimInfo? claimInfo;
  @JsonKey(name: Key_cancel_info)
  CancelInfo? cancelInfo;
  @JsonKey(name: Key_carediary_reply_cnt)
  int carediaryReplyCnt;
  @JsonKey(name: Key_carediary_reply)
  List<CarediaryReply>? carediaryReply;
  @JsonKey(name: Key_is_end_care)
  bool isEndCare;
  @JsonKey(name: Key_is_review)
  bool isReview;
  @JsonKey(name: Key_bookingcareservices)
  int bookingcareservices;

  DiaryViewModel({
    this.matching = 0,
    this.carecontractSchedule = 0,
    this.costSum = 0,
    this.payStatus = 0,
    this.payStatusText = '',
    this.servicetype = '',
    this.possibleArea = '',
    this.stime = '',
    this.etime = '',
    this.caredate,
    this.userinfo,
    this.childinfo,
    this.carediaryInfo,
    this.carediaryImages,
    this.likeYn = false,
    this.claimInfo,
    this.cancelInfo,
    this.carediaryReplyCnt = 0,
    this.carediaryReply,
    this.isEndCare = false,
    this.isReview = false,
    this.bookingcareservices = 0,
  });

  factory DiaryViewModel.fromJson(Map<String, dynamic> json) {
    return DiaryViewModel(
      matching: json[Key_matching] ?? 0,
      carecontractSchedule: json[Key_carecontract_schedule] ?? 0,
      costSum: json[Key_cost_sum] ?? 0,
      payStatus: json[Key_pay_status] ?? 0,
      payStatusText: json[Key_pay_status_text] ?? '',
      servicetype: json[Key_servicetype] ?? '',
      possibleArea: json[Key_possible_area] ?? '',
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      caredate: json[Key_caredate] == null ? [] : List<String>.from(json[Key_caredate] ?? {}),
      userinfo: json[Key_userinfo] == null ? Userinfo() : Userinfo.fromJson(json[Key_userinfo] ?? {}),
      childinfo: json[Key_childinfo] == null ? Childinfo() : Childinfo.fromJson(json[Key_childinfo] ?? {}),
      carediaryInfo: json[Key_carediary_info] == null ? CarediaryInfo() : CarediaryInfo.fromJson(json[Key_carediary_info] ?? {}),
      carediaryImages: json[Key_carediary_images] == null ? [] : json[Key_carediary_images].map<CarediaryImages>((r) => CarediaryImages.fromJson(r)).toList(),
      likeYn: json[Key_like_yn] ?? false,
      claimInfo: json[Key_claim_info] == null ? ClaimInfo() : ClaimInfo.fromJson(json[Key_claim_info]),
      cancelInfo: json[Key_cancel_info] == null ? CancelInfo() : CancelInfo.fromJson(json[Key_cancel_info]),
      carediaryReplyCnt: json[Key_carediary_reply_cnt] ?? 0,
      carediaryReply: json[Key_carediary_reply] == null ? CarediaryReply() : json[Key_carediary_reply].map<CarediaryReply>((r) => CarediaryReply.fromJson(r)).toList(),
      isEndCare: json[Key_is_end_care] ?? false,
      isReview: json[Key_is_review] ?? false,
      bookingcareservices: json[Key_bookingcareservices] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_matching] = this.matching;
    data[Key_carecontract_schedule] = this.carecontractSchedule;
    data[Key_cost_sum] = this.costSum;
    data[Key_pay_status] = this.payStatus;
    data[Key_pay_status_text] = this.payStatusText;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possibleArea;
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_caredate] = this.caredate;
    if (this.userinfo != null) data[Key_userinfo] = this.userinfo;
    if (this.childinfo != null) data[Key_childinfo] = this.childinfo;
    if (this.carediaryInfo != null) data[Key_carediary_info] = this.carediaryInfo;
    if (this.carediaryImages != null) data[Key_carediary_images] = this.carediaryImages;
    data[Key_like_yn] = this.likeYn;
    if (this.claimInfo != null) data[Key_claim_info] = this.claimInfo;
    if (this.cancelInfo != null) data[Key_cancel_info] = this.cancelInfo;
    data[Key_carediary_reply_cnt] = this.carediaryReplyCnt;
    if (this.carediaryReply != null) data[Key_carediary_reply] = this.carediaryReply;
    data[Key_is_end_care] = this.isEndCare;
    data[Key_is_review] = this.isReview;
    data[Key_bookingcareservices] = this.bookingcareservices;
    return data;
  }

  @override
  String toString() {
    return 'DiaryViewModel${{
      "matching": matching,
      "carecontractSchedule": carecontractSchedule,
      "costSum": costSum,
      "payStatus": payStatus,
      "payStatusText": payStatusText,
      "servicetype": servicetype,
      "possibleArea": possibleArea,
      "stime": stime,
      "etime": etime,
      "caredate": caredate,
      "userinfo": userinfo,
      "childinfo": childinfo,
      "carediaryInfo": carediaryInfo,
      "carediaryImages": carediaryImages,
      "likeYn": likeYn,
      "claimInfo": claimInfo,
      "cancelInfo": cancelInfo,
      "carediaryReplyCnt": carediaryReplyCnt,
      "carediaryReply": carediaryReply,
      "is_end_care": isEndCare,
      "is_review": isReview,
      "bookingcareservices": bookingcareservices,
    }}';
  }
}

class CancelInfo {
  static const String Key_order_id = "order_id";
  static const String Key_contract_id = "contract_id";
  static const String Key_cancel_id = "cancel_id";
  @JsonKey(name: Key_order_id)
  String orderId;
  @JsonKey(name: Key_contract_id)
  int contractId;
  @JsonKey(name: Key_cancel_id)
  int cancelId;

  CancelInfo({
    this.orderId = '',
    this.contractId = 0,
    this.cancelId = 0,
  });

  factory CancelInfo.fromJson(Map<String, dynamic> json) {
    return CancelInfo(
      orderId: json[Key_order_id] ?? '',
      contractId: json[Key_contract_id] ?? 0,
      cancelId: json[Key_cancel_id] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_order_id] = this.orderId;
    data[Key_contract_id] = this.contractId;
    data[Key_cancel_id] = this.cancelId;
    return data;
  }

  @override
  String toString() {
    return 'CancelInfo${{
      "orderId": orderId,
      "contractId": contractId,
      "cancelId": cancelId,
    }}';
  }
}

class ClaimInfo {
  static const String Key_claim_id = "claim_id";
  static const String Key_claim_after_day = "claim_after_day";
  static const String Key_claim_answer = "claim_answer";

  @JsonKey(name: Key_claim_id)
  int claimId;
  @JsonKey(name: Key_claim_after_day)
  String claimAfterDay;
  @JsonKey(name: Key_claim_answer)
  bool claimAnswer;

  ClaimInfo({
    this.claimId = 0,
    this.claimAfterDay = '',
    this.claimAnswer = false,
  });

  factory ClaimInfo.fromJson(Map<String, dynamic> json) {
    return ClaimInfo(
      claimId: json[Key_claim_id] ?? 0,
      claimAfterDay: json[Key_claim_after_day] ?? '',
      claimAnswer: json[Key_claim_answer] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_claim_id] = this.claimId;
    data[Key_claim_after_day] = this.claimAfterDay;
    data[Key_claim_answer] = this.claimAnswer;
    return data;
  }

  @override
  String toString() {
    return 'ClaimInfo${{
      "claimId": claimId,
      "claimAfterDay": claimAfterDay,
      "claimAnswer": claimAnswer,
    }}';
  }
}

class CarediaryInfo {
  static const String Key_id = "id";
  static const String Key_momdady_name = "momdady_name";
  static const String Key_linkmom_name = "linkmom_name";
  static const String Key_content = "content";
  static const String Key_is_servicetype = "is_servicetype";
  static const String Key_childdrug_id = "childdrug_id";
  static const String Key_is_childdrug = "is_childdrug";
  static const String Key_option = "option";
  static const String Key_complete_option = "complete_option";
  static const String Key_confirm_date = "confirm_date";
  static const String Key_updatedate = "updatedate";
  static const String Key_linkmom_thumbnail = 'linkmom_thumbnail';
  static const String Key_childdrug_list = 'childdrug_list';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_momdady_name)
  String momdadyName;
  @JsonKey(name: Key_linkmom_name)
  String linkmomName;
  @JsonKey(name: Key_linkmom_thumbnail)
  String linkmomThumbnail;
  @JsonKey(name: Key_content)
  String content;
  @JsonKey(name: Key_is_servicetype)
  int isServicetype;
  @JsonKey(name: Key_childdrug_id)
  int childdrugId;
  @JsonKey(name: Key_option)
  Option? option;
  @JsonKey(name: Key_complete_option)
  CompleteOption? completeOption;
  @JsonKey(name: Key_confirm_date)
  String confirmDate;
  @JsonKey(name: Key_updatedate)
  String updatedate;
  @JsonKey(name: Key_childdrug_list)
  List<ChildDrugInfo>? childdrugList;

  CarediaryInfo({
    this.id = 0,
    this.momdadyName = '',
    this.linkmomName = '',
    this.content = '',
    this.isServicetype = 0,
    this.childdrugId = 0,
    this.option,
    this.completeOption,
    this.confirmDate = '',
    this.updatedate = '',
    this.linkmomThumbnail = '',
    this.childdrugList,
  });

  factory CarediaryInfo.fromJson(Map<String, dynamic> json) {
    return CarediaryInfo(
      id: json[Key_id] ?? 0,
      momdadyName: json[Key_momdady_name] ?? '',
      linkmomName: json[Key_linkmom_name] ?? '',
      linkmomThumbnail: json[Key_linkmom_thumbnail] ?? '',
      content: json[Key_content] ?? '',
      isServicetype: json[Key_is_servicetype] ?? 0,
      childdrugId: json[Key_childdrug_id] ?? 0,
      option: json[Key_option] == null
          ? Option(
              boyukoptions: [],
              homecareoptions: [],
              playoptions: [],
            )
          : Option.fromJson(json[Key_option]),
      completeOption: json[Key_complete_option] == null
          ? CompleteOption(
              completeBoyuk: [],
              completeHomecare: [],
              completePlay: [],
            )
          : CompleteOption.fromJson(json[Key_complete_option]),
      confirmDate: json[Key_confirm_date] ?? '',
      updatedate: json[Key_updatedate] ?? '',
      childdrugList: json[Key_childdrug_list] != null ? json[Key_childdrug_list].map<ChildDrugInfo>((e) => ChildDrugInfo.fromJson(e)).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_momdady_name] = this.momdadyName;
    data[Key_linkmom_name] = this.linkmomName;
    data[Key_linkmom_thumbnail] = this.linkmomThumbnail;
    data[Key_content] = this.content;
    data[Key_is_servicetype] = this.isServicetype;
    data[Key_childdrug_id] = this.childdrugId;
    if (this.option != null) data[Key_option] = this.option;
    if (this.completeOption != null) data[Key_complete_option] = this.completeOption;
    data[Key_confirm_date] = this.confirmDate;
    data[Key_updatedate] = this.updatedate;
    data[Key_childdrug_list] = this.childdrugList;
    return data;
  }

  @override
  String toString() {
    return 'CarediaryInfo${{
      "id": id,
      "momdadyName": momdadyName,
      "linkmomName": linkmomName,
      "linkmomThumbnail": linkmomThumbnail,
      "content": content,
      "isServicetype": isServicetype,
      "childdrugId": childdrugId,
      "childdrugList": childdrugList,
      "option": option,
      "completeOption": completeOption,
      "confirmDate": confirmDate,
      "updatedate": updatedate,
    }}';
  }
}

class Childinfo {
  static const String Key_child_name = "child_name";
  static const String Key_child_age = "child_age";
  static const String Key_child_gender = "child_gender";

  @JsonKey(name: Key_child_name)
  String childName;
  @JsonKey(name: Key_child_age)
  String childAge;
  @JsonKey(name: Key_child_gender)
  String childGender;

  Childinfo({
    this.childName = '',
    this.childAge = '',
    this.childGender = '',
  });

  factory Childinfo.fromJson(Map<String, dynamic> json) {
    return Childinfo(
      childName: json[Key_child_name] ?? '',
      childAge: json[Key_child_age] ?? '',
      childGender: json[Key_child_gender] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_child_name] = this.childName;
    data[Key_child_age] = this.childAge;
    data[Key_child_gender] = this.childGender;
    return data;
  }

  @override
  String toString() {
    return 'Childinfo${{
      "childName": childName,
      "childAge": childAge,
      "childGender": childGender,
    }}';
  }
}

class Userinfo {
  static const String Key_user_id = 'user_id';
  static const String Key_first_name = 'first_name';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_4depth = 'region_4depth';
  static const String Key_profileimg = 'profileimg';

  @JsonKey(name: Key_user_id)
  int userId;
  @JsonKey(name: Key_first_name)
  String firstName;
  @JsonKey(name: Key_region_3depth)
  String region3Depth;
  @JsonKey(name: Key_region_4depth)
  String region4Depth;
  @JsonKey(name: Key_profileimg)
  String profileimg;

  Userinfo({
    this.userId = 0,
    this.firstName = '',
    this.region3Depth = '',
    this.region4Depth = '',
    this.profileimg = '',
  });

  factory Userinfo.fromJson(Map<String, dynamic> json) {
    return Userinfo(
      userId: json[Key_user_id] ?? '',
      firstName: json[Key_first_name] ?? '',
      region3Depth: json[Key_region_3depth] ?? '',
      region4Depth: json[Key_region_4depth] ?? '',
      profileimg: json[Key_profileimg] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_user_id] = this.userId;
    data[Key_first_name] = this.firstName;
    data[Key_region_3depth] = this.region3Depth;
    data[Key_region_4depth] = this.region4Depth;
    data[Key_profileimg] = this.profileimg;
    return data;
  }

  @override
  String toString() {
    return 'Userinfo${{
      "userId": userId,
      "firstName": firstName,
      "region3Depth": region3Depth,
      "region4Depth": region4Depth,
      "profileimg": profileimg,
    }}';
  }
}

class CarediaryReply {
  static const String Key_id = 'id';
  static const String Key_parent = 'parent';
  static const String Key_reply = 'reply';
  static const String Key_writer_id = 'writer_id';
  static const String Key_writer_gubun = 'writer_gubun';
  static const String Key_writer_name = 'writer_name';
  static const String Key_profileimg = 'profileimg';
  static const String Key_is_delete = 'is_delete';
  static const String Key_updatedate = 'updatedate';
  static const String Key_child = 'child';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_parent)
  int parent;
  @JsonKey(name: Key_reply)
  String reply;
  @JsonKey(name: Key_writer_gubun)
  String writerGubun;
  @JsonKey(name: Key_writer_name)
  String writerName;
  @JsonKey(name: Key_updatedate)
  String updatedate;
  @JsonKey(name: Key_child)
  List<CarediaryReply>? child;
  @JsonKey(name: Key_is_delete)
  int isDelete;
  @JsonKey(name: Key_profileimg)
  String profileimg;
  @JsonKey(name: Key_writer_id)
  int writerId;

  CarediaryReply({
    this.id = 0,
    this.parent = 0,
    this.reply = '',
    this.writerGubun = '',
    this.writerName = '',
    this.updatedate = '',
    this.child,
    this.isDelete = 0,
    this.profileimg = '',
    this.writerId = 0,
  });

  factory CarediaryReply.fromJson(Map<String, dynamic> json) {
    return CarediaryReply(
        id: json[Key_id] ?? 0,
        parent: json[Key_parent] ?? 0,
        reply: json[Key_reply] ?? '',
        writerGubun: json[Key_writer_gubun] ?? '',
        writerName: json[Key_writer_name] ?? '',
        updatedate: json[Key_updatedate] ?? '',
        child: json[Key_child] != null && json[Key_child].isNotEmpty ? json[Key_child].map<CarediaryReply>((e) => CarediaryReply.fromJson(e)).toList() : [],
        isDelete: json[Key_is_delete] ?? 0,
        profileimg: json[Key_profileimg] ?? '',
        writerId: json[Key_writer_id] ?? '');
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_parent] = this.parent;
    data[Key_reply] = this.reply;
    data[Key_writer_gubun] = this.writerGubun;
    data[Key_writer_name] = this.writerName;
    data[Key_updatedate] = this.updatedate;
    data[Key_child] = this.child;
    data[Key_is_delete] = this.isDelete;
    data[Key_profileimg] = this.profileimg;
    data[Key_writer_id] = this.writerId;
    return data;
  }

  @override
  String toString() {
    return 'CarediaryReply${{
      "id": id,
      "parent": parent,
      "reply": reply,
      "writerGubun": writerGubun,
      "writerName": writerName,
      "updatedate": updatedate,
      "child": child,
      "isDelete": isDelete,
      "profileimg": profileimg,
      "writerId": writerId,
    }}';
  }
}
