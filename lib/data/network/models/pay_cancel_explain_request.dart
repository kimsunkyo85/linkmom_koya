import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///
/// Created by   : platformbiz@ggumbi.com
///
/// version      : 1.0.0
///
/// see          : pay_cancel_explain_request.dart - 결제취소 요청 소명하기 PUT
///
/// since        : 2021/09/08 / update:
///
/// (실패)
///
/// "msg_cd": 5040, "message": "취소 할 수 없는 결제 입니다.",
///
/// "msg_cd": 5041, "message": "이미 취소된 결제 입니다.",
///
/// "msg_cd": 4000, "message": "데이터가 없습니다.",
@JsonSerializable()
class PayCancelExplainRequest {
  static const String Key_order_id = 'order_id';
  static const String Key_contract_id = 'contract_id';
  static const String Key_cancel_id = 'cancel_id';
  static const String Key_cancelexplain_msg = 'cancelexplain_msg';
  static const String Key_cancelexplain_img = 'cancelexplain_img';

  PayCancelExplainRequest({
    this.order_id,
    this.contract_id,
    this.cancel_id,
    this.cancelexplain_msg,
    this.cancelexplain_img,
  });

  ///주문번호
  @JsonKey(name: Key_order_id)
  final String? order_id;

  ///계약서 번호
  @JsonKey(name: Key_contract_id)
  final int? contract_id;

  ///취소 id
  @JsonKey(name: Key_cancel_id)
  final int? cancel_id;

  ///취소사유 - 소명하기 내용
  @JsonKey(name: Key_cancelexplain_msg)
  String? cancelexplain_msg;

  ///취소사유 - 소명하기 이미지
  @JsonKey(name: Key_cancelexplain_img)
  List<File>? cancelexplain_img;

  factory PayCancelExplainRequest.fromJson(Map<String, dynamic> json) {
    return PayCancelExplainRequest(
      order_id: json[Key_order_id] ?? '',
      contract_id: json[Key_contract_id] as int,
      cancel_id: json[Key_cancel_id] as int,
      cancelexplain_msg: json[Key_cancelexplain_msg] ?? '',
      cancelexplain_img: json[Key_cancelexplain_img],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_order_id] = this.order_id;
    data[Key_contract_id] = this.contract_id;
    data[Key_cancel_id] = this.cancel_id;
    data[Key_cancelexplain_msg] = this.cancelexplain_msg;
    data[Key_cancelexplain_img] = this.cancelexplain_img;
    return data;
  }

  Future<FormData> toFormData() async {
    FormData formData = FormData();
    if (order_id != null) {
      formData.fields.add(MapEntry(Key_order_id, order_id.toString()));
    }
    if (contract_id != null) {
      formData.fields.add(MapEntry(Key_contract_id, contract_id.toString()));
    }
    if (cancel_id != null) {
      formData.fields.add(MapEntry(Key_cancel_id, cancel_id.toString()));
    }
    if (cancelexplain_msg != null) {
      formData.fields.add(MapEntry(Key_cancelexplain_msg, cancelexplain_msg.toString()));
    }

    if (cancelexplain_img != null && cancelexplain_img!.isNotEmpty) {
      cancelexplain_img!.forEach((value) async {
        String fileName = value.path.split('/').last;
        log.d('『GGUMBI』>>> setEncryptFormDataFiles : fileName: $fileName,  <<< ');
        formData.files.add(MapEntry(Key_cancelexplain_img, await MultipartFile.fromFile(value.path, filename: fileName, contentType: MediaType('image', 'jpeg'))));
      });
    }

    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.files},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'PayCancelExplainRequest{order_id: $order_id, contract_id: $contract_id, cancel_id: $cancel_id, cancelexplain_msg: $cancelexplain_msg, cancelexplain_img: $cancelexplain_img}';
  }
}
