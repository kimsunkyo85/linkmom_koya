import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/chat_room_data.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_room_view_response.dart - 채팅 방 정보 가져오기 응답
/// since        : 2021/07/15 / update:
@JsonSerializable()
class ChatRoomViewResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ChatRoomData>? dataList;

  ChatRoomViewResponse({
    this.dataHeader,
    this.dataList,
  });

  ChatRoomData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ChatRoomData();

  List<ChatRoomData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ChatRoomViewResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ChatRoomData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<ChatRoomData>((i) => ChatRoomData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return ChatRoomViewResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ChatRoomUpdateResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
