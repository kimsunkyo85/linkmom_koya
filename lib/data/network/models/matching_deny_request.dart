import 'package:json_annotation/json_annotation.dart';

import 'data/matching_request_data.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : matching_deny_request.dart - 매칭 거절하기 요청
/// since        : 2021/08/03 / update:
@JsonSerializable()
class MatchingDenyRequest {
  MatchingRequestData? requestData;
/*///돌봄진행 상황 (매칭된 내용)
  static const String Key_receiver = 'receiver';

  ///돌봄신청서ID (booking_id)
  static const String Key_bookingcareservices = 'bookingcareservices';

  ///매칭ID (matching_id) - 있는 경우만, 없으면 Null
  static const String Key_carematching = 'carematching';

  ///채팅방번호 (chatroom_id)
  static const String Key_chattingroom = 'chattingroom';

  MatchingDenyRequest({
    this.receiver,
    this.bookingcareservices,
    this.carematching,
    this.chattingroom,
  });

  ///돌봄진행 상황 (매칭된 내용)
  @JsonKey(name: Key_receiver)
  final int receiver;

  ///돌봄신청서ID (booking_id)
  @JsonKey(name: Key_bookingcareservices)
  final int bookingcareservices;

  ///매칭ID (matching_id) - 있는 경우만, 없으면 Null
  @JsonKey(name: Key_carematching)
  final int carematching;

  ///채팅방번호 (chatroom_id)
  @JsonKey(name: Key_chattingroom)
  final int chattingroom;

  factory MatchingDenyRequest.fromJson(Map<String, dynamic> json) {
    return MatchingDenyRequest(
      receiver: json[Key_receiver] as int,
      bookingcareservices: json[Key_bookingcareservices] as int,
      carematching: json[Key_carematching] as int,
      chattingroom: json[Key_chattingroom] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_receiver] = this.receiver;
    data[Key_bookingcareservices] = this.bookingcareservices;
    data[Key_carematching] = this.carematching;
    data[Key_chattingroom] = this.chattingroom;
    return data;
  }

  Future<FormData> toFormData() async {
    FormData formData = FormData();
    if (receiver != null) {
      formData.fields.add(MapEntry(Key_receiver, receiver.toString()));
    }
    if (bookingcareservices != null) {
      formData.fields.add(MapEntry(Key_bookingcareservices, bookingcareservices.toString()));
    }
    if (carematching != null) {
      formData.fields.add(MapEntry(Key_carematching, carematching.toString()));
    }
    if (chattingroom != null) {
      formData.fields.add(MapEntry(Key_chattingroom, chattingroom.toString()));
    }
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'MatchingDenyRequest{receiver: $receiver, bookingcareservices: $bookingcareservices, carematching: $carematching, chattingroom: $chattingroom}';
  }*/
}
