import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : area_address_response.dart - 동네 인증 여부 검사
/// since        : 5/7/21 / update:
@JsonSerializable()
class AreaGpsViewResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<AreaGpsViewData>? dataList;

  AreaGpsViewResponse({
    this.dataHeader,
    this.dataList,
  });

  AreaGpsViewData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : AreaGpsViewData();

  List<AreaGpsViewData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory AreaGpsViewResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<AreaGpsViewData> datas = [];
    try {
      datas = json[Key_data].map<AreaGpsViewData>((i) => AreaGpsViewData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return AreaGpsViewResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AreaGpsViewResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class AreaGpsViewData {
  static const String Key_gps_b_code = 'gps_b_code';
  static const String Key_gps_h_code = 'gps_h_code';
  static const String Key_region_1depth = 'region_1depth';
  static const String Key_region_2depth = 'region_2depth';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_3depth_h = 'region_3depth_h';
  static const String Key_region_building_name = 'region_building_name';

  AreaGpsViewData({
    this.gps_b_code = '',
    this.gps_h_code = '',
    this.region_1depth = '',
    this.region_2depth = '',
    this.region_3depth = '',
    this.region_3depth_h = '',
    this.region_building_name = '',
  });

  ///(선택된) 법정동코드
  @JsonKey(name: Key_gps_b_code)
  final String gps_b_code;

  ///(선택된) 행정동코드
  @JsonKey(name: Key_gps_h_code)
  final String gps_h_code;

  ///(선택된) 위치 Depth1
  @JsonKey(name: Key_region_1depth)
  final String region_1depth;

  ///(선택된) 위치 Depth2
  @JsonKey(name: Key_region_2depth)
  final String region_2depth;

  ///(선택된) 위치 Depth3
  @JsonKey(name: Key_region_3depth)
  final String region_3depth;

  ///(선택된) 위치 Depth4
  @JsonKey(name: Key_region_3depth_h)
  final String region_3depth_h;

  ///제휴 빌딩 이름
  @JsonKey(name: Key_region_building_name)
  final String region_building_name;

  factory AreaGpsViewData.fromJson(Map<String, dynamic> json) {
    return AreaGpsViewData(
      gps_b_code: json[Key_gps_b_code] ?? '',
      gps_h_code: json[Key_gps_h_code] ?? '',
      region_1depth: json[Key_region_1depth] ?? '',
      region_2depth: json[Key_region_2depth] ?? '',
      region_3depth: json[Key_region_3depth] ?? '',
      region_3depth_h: json[Key_region_3depth_h] ?? '',
      region_building_name: json[Key_region_building_name] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_gps_b_code] = this.gps_b_code;
    data[Key_gps_h_code] = this.gps_h_code;
    data[Key_region_1depth] = this.region_1depth;
    data[Key_region_2depth] = this.region_2depth;
    data[Key_region_3depth] = this.region_3depth;
    data[Key_region_3depth_h] = this.region_3depth_h;
    data[Key_region_building_name] = this.region_building_name;
    return data;
  }

  @override
  String toString() {
    return 'AreaGpsViewData{gps_b_code: $gps_b_code, gps_h_code: $gps_h_code, region_1depth: $region_1depth, region_2depth: $region_2depth, region_3depth: $region_3depth, region_3depth_h: $region_3depth_h, region_building_name: $region_building_name}';
  }
}
