import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class AuthCenterHomeDeleteRequest {
  static const String Key_fileid = 'fileid';

  AuthCenterHomeDeleteRequest({
    this.fileid = '',
  });

  @JsonKey(name: Key_fileid)
  String fileid;

  factory AuthCenterHomeDeleteRequest.fromJson(Map<String, dynamic> json) {
    return AuthCenterHomeDeleteRequest(
      fileid: json[Key_fileid] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_fileid] = this.fileid;
    return data;
  }

  @override
  String toString() {
    return 'AuthCenterHomeDeleteRequest{fileid: $fileid}';
  }
}
