import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

class NotificationListRequest {
  static const String Key_pagename = 'pagename';
  static const String Key_gubun = 'gubun';

  @JsonKey(name: Key_pagename)
  int? pagename;
  @JsonKey(name: Key_gubun)
  int? gubun;

  NotificationListRequest({this.pagename, this.gubun = 0});

  factory NotificationListRequest.fromJson(Map<String, dynamic> json) {
    return NotificationListRequest(
      pagename: json[Key_pagename] as int,
      gubun: json[Key_gubun] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_pagename] = this.pagename ?? 999;
    data[Key_gubun] = this.gubun ?? 0;
    return data;
  }

  FormData toFormData(FormData formData) {
    if (this.pagename != null) formData.fields.add(MapEntry(Key_pagename, this.pagename.toString()));
    if (this.gubun != null) formData.fields.add(MapEntry(Key_gubun, this.gubun.toString()));
    return formData;
  }

  @override
  String toString() {
    return 'NotificationListRequest{pagename: $pagename, gubun: $gubun}';
  }
}
