import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class CareDiaryChildDrugOkResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  CareDiaryChildDrugOkResponse({
    this.dataHeader,
    this.dataList,
  });

  CareDiaryChildDrugOkResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  String getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ' ';

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareDiaryChildDrugOkResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareDiaryChildDrugData> datas = [];
    try {
      datas = json[Key_data].map<CareDiaryChildDrugData>((i) => CareDiaryChildDrugData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareDiaryChildDrugOkResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CareDiaryChildDrugOkResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class CareDiaryChildDrugData {
  static const String Key_message = 'message';

  CareDiaryChildDrugData({
    this.message = '',
  });

  @JsonKey(name: Key_message)
  String message;

  factory CareDiaryChildDrugData.fromJson(Map<String, dynamic> json) {
    return CareDiaryChildDrugData(
      message: json[Key_message],
    );
  }
}
