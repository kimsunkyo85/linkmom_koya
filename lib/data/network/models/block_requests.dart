import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class BlockSaveRequest {
  static const String Key_block_user = 'block_user';
  static const String Key_block_matching = 'block_matching';

  BlockSaveRequest({
    this.block_user = 0,
    this.block_matching = 0,
  });

  @JsonKey(name: Key_block_user)
  int block_user;

  @JsonKey(name: Key_block_matching)
  int block_matching = 0;

  factory BlockSaveRequest.fromJson(Map<String, dynamic> json) {
    return BlockSaveRequest(
      block_user: json[Key_block_user] as int,
      block_matching: json[Key_block_user] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_block_user] = this.block_user;
    data[Key_block_matching] = this.block_matching;
    return data;
  }

  FormData toFormData() {
    FormData formData = FormData();
    if (block_user > 0) {
      formData.fields.add(MapEntry(Key_block_user, this.block_user.toString()));
    }
    if (block_matching > 0) {
      formData.fields.add(MapEntry(Key_block_matching, this.block_matching.toString()));
    }
    return formData;
  }

  @override
  String toString() {
    return 'BlockSaveRequest{block_user: $block_user, block_matching:$block_matching}';
  }
}

@JsonSerializable()
class BlockDeleteRequest {
  static const String Key_block_user = 'block_user';

  BlockDeleteRequest({
    this.block_user = 0,
  });

  @JsonKey(name: Key_block_user)
  int block_user;

  factory BlockDeleteRequest.fromJson(Map<String, dynamic> json) {
    return BlockDeleteRequest(
      block_user: json[Key_block_user] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_block_user] = this.block_user;
    return data;
  }

  @override
  String toString() {
    return 'BlockDeleteRequest{block_user: $block_user}';
  }
}
