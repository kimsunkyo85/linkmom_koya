import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : resetpw_request.dart - 비밀번호 재설정
/// since        : 1/18/21 / update:
@JsonSerializable()
class ResetPwRequest {
  static const String Key_auth_type = "auth_type";
  static const String Key_auth_key = "auth_key";
  static const String Key_auth_number = "auth_number";
  static const String Key_username = "username";
  static const String Key_new_password = "new_password";
  static const String Key_new_password2 = "new_password2";

  ResetPwRequest({
    this.auth_type = '',
    this.auth_key = '',
    this.auth_number = '',
    this.username = '',
    this.new_password = '',
    this.new_password2 = '',
  });

  @JsonKey(name: Key_auth_type)
  String auth_type;
  @JsonKey(name: Key_auth_key)
  String auth_key;
  @JsonKey(name: Key_auth_number)
  String auth_number;
  @JsonKey(name: Key_username)
  String username;
  @JsonKey(name: Key_new_password)
  String new_password;
  @JsonKey(name: Key_new_password2)
  String new_password2;

  factory ResetPwRequest.fromJson(Map<String, dynamic> json) {
    return ResetPwRequest(
      auth_type: json[Key_auth_type] ?? '',
      auth_key: json[Key_auth_key] ?? '',
      auth_number: json[Key_auth_number] ?? '',
      username: json[Key_username] ?? '',
      new_password: json[Key_new_password] ?? '',
      new_password2: json[Key_new_password2] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_auth_type] = this.auth_type;
    data[Key_auth_key] = this.auth_key;
    data[Key_auth_number] = this.auth_number;
    data[Key_username] = this.username;
    data[Key_new_password] = this.new_password;
    data[Key_new_password2] = this.new_password2;
    return data;
  }

  @override
  String toString() {
    return 'ResetPwRequest{auth_type: $auth_type, auth_key: $auth_key, auth_number: $auth_number, username: $username, new_password: $new_password, new_password2: $new_password2}';
  }
}
