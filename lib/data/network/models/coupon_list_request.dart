import 'package:json_annotation/json_annotation.dart';

class CouponListRequest {
  static const Key_gubun = "gubun";

  @JsonKey(name: Key_gubun)
  int gubun;

  CouponListRequest({this.gubun = 0});

  factory CouponListRequest.fromJson(Map<String, dynamic> json) {
    return CouponListRequest(gubun: json[Key_gubun]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_gubun] = this.gubun;
    return data;
  }

  @override
  String toString() {
    return 'CouponListRequest{gubun : $gubun}';
  }
}
