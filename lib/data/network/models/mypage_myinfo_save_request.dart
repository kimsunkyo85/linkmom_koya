import 'dart:io';

import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/utils/commons.dart';

import '../../../main.dart';

@JsonSerializable()
class MyPageMyInfoSaveRequest {
  static const String Key_numberofchild = 'numberofchild';
  static const String Key_job = 'job';
  static const String Key_profileimg = 'profileimg';
  static const String Key_nickname = 'nickname';
  static const String Key_is_user_momdady = 'is_user_momdady';
  static const String Key_is_user_linkmom = 'is_user_linkmom';

  MyPageMyInfoSaveRequest({
    this.numberofchild = NOT_REGISTER,
    this.job = NOT_REGISTER,
    this.profileimg,
    this.nickname = '',
    this.is_user_momdady = -1,
    this.is_user_linkmom = -1,
  });

  ///아이수
  @JsonKey(name: Key_numberofchild)
  int? numberofchild;

  ///직업(번호)
  @JsonKey(name: Key_job)
  int? job;

  ///프로필 사진(변경필요시에만 업로드)
  @JsonKey(name: Key_profileimg)
  File? profileimg;

  @JsonKey(name: Key_nickname)
  String? nickname;

  ///맘대디 회원 자격 (0:미인증, 1:인증)
  @JsonKey(name: Key_is_user_momdady)
  int? is_user_momdady;

  ///링크쌤회원 자격 (0:미인증, 1:인증)
  @JsonKey(name: Key_is_user_linkmom)
  int? is_user_linkmom;

  factory MyPageMyInfoSaveRequest.fromJson(Map<String, dynamic> json) {
    return MyPageMyInfoSaveRequest(
      numberofchild: json[Key_numberofchild] as int,
      job: json[Key_job] as int,
      profileimg: (json[Key_profileimg]),
      nickname: json[Key_nickname] ?? '',
      is_user_momdady: json[Key_is_user_momdady] as int,
      is_user_linkmom: json[Key_is_user_linkmom] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_numberofchild] = this.numberofchild;
    data[Key_job] = this.job;
    data[Key_profileimg] = this.profileimg;
    data[Key_nickname] = this.nickname;
    data[Key_is_user_momdady] = this.is_user_momdady;
    data[Key_is_user_linkmom] = this.is_user_linkmom;
    return data;
  }

  Future<FormData> toFormData() async {
    FormData formData = FormData();
    if (numberofchild != null) {
      formData.fields.add(MapEntry(Key_numberofchild, numberofchild.toString()));
    }
    if (job != null) {
      formData.fields.add(MapEntry(Key_job, job.toString()));
    }
    if (nickname != null) {
      formData.fields.add(MapEntry(Key_nickname, nickname.toString()));
    }
    if (is_user_momdady != null) {
      formData.fields.add(MapEntry(Key_is_user_momdady, is_user_momdady.toString()));
    }
    if (is_user_linkmom != null) {
      formData.fields.add(MapEntry(Key_is_user_linkmom, is_user_linkmom.toString()));
    }

    if (profileimg != null) {
      await Commons.setFormDataFiles3(Key_profileimg, profileimg).then((value) {
        formData.files.add(value.files.first);
      });
      // String fileName = profileimg!.path.split('/').last;
      // log.d('『GGUMBI』>>> setEncryptFormDataFiles : fileName: $fileName,  <<< ');
      // formData.files.add(MapEntry(Key_profileimg, await MultipartFile.fromFile(profileimg!.path, filename: fileName, contentType: MediaType('image', 'jpeg'))));
    }

    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'MyPageMyInfoSaveRequest{numberofchild: $numberofchild, job: $job, profileimg: $profileimg, nickname: $nickname}';
  }
}
