import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : findid_request.dart - 아이디 찾기 (모바일, 이메일)
/// since        : 1/18/21 / update:
@JsonSerializable()
class FindIdRequest {
  static const String Key_auth_type = "auth_type";
  static const String Key_auth_key = "auth_key";
  static const String Key_auth_number = "auth_number";
  static const String Key_first_name = "first_name";

  FindIdRequest({
    this.auth_type = '',
    this.auth_key = '',
    this.auth_number = '',
    this.first_name = '',
  });

  @JsonKey(name: Key_auth_type)
  String auth_type;
  @JsonKey(name: Key_auth_key)
  String auth_key;
  @JsonKey(name: Key_auth_number)
  String auth_number;
  @JsonKey(name: Key_first_name)
  String first_name;

  factory FindIdRequest.fromJson(Map<String, dynamic> json) {
    return FindIdRequest(
      auth_type: json[Key_auth_type] ?? '',
      auth_key: json[Key_auth_key] ?? '',
      auth_number: json[Key_auth_number] ?? '',
      first_name: json[Key_first_name] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_auth_type] = this.auth_type;
    data[Key_auth_key] = this.auth_key;
    data[Key_auth_number] = this.auth_number;
    data[Key_first_name] = this.first_name;
    return data;
  }

  @override
  String toString() {
    return 'FindIdRequest{auth_type: $auth_type, auth_key: $auth_key, auth_number: $auth_number, first_name: $first_name}';
  }
}
