import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

import 'data/care_diary_data.dart';

@JsonSerializable()
class CareDiaryUpdateResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  CareDiaryUpdateResponse({this.dataHeader, this.dataList});

  @JsonKey(name: Key_data)
  List<CareDiaryData>? dataList;

  CareDiaryUpdateResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  CareDiaryData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CareDiaryData();

  List<CareDiaryData> getDataList() => dataList!;
  
  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();


  factory CareDiaryUpdateResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse header = HeaderResponse.fromJson(json);
    List<CareDiaryData> datas = [];
    try {
      log.d('『GGUMBI』>>> fromJson ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<<${json[Key_data]} ');
      datas = json[Key_data].map<CareDiaryData>((i) => CareDiaryData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareDiaryUpdateResponse(
      dataHeader: header,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CareDiaryUpdateResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
