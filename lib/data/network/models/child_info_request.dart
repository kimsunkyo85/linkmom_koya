import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : child_info_request.dart - 아이정보 저장 요청
/// since        : 5/20/21 / update:
@JsonSerializable()
class ChildInfoRequest {
  static const String Key_child_name = 'child_name';
  static const String Key_child_gender = 'child_gender';
  static const String Key_child_birth = 'child_birth';
  static const String Key_child_nursery = 'child_nursery';
  static const String Key_child_character = 'child_character';
  static const String Key_is_allergy = 'is_allergy';
  static const String Key_allergy_name = 'allergy_name';
  static const String Key_allergy_message = 'allergy_message';
  static const String Key_child_id = 'child_id';

  ChildInfoRequest({
    this.child_name = '',
    this.child_gender = 0,
    this.child_birth = '',
    this.child_nursery = 0,
    this.child_character,
    this.is_allergy = false,
    this.allergy_name = '',
    this.allergy_message = '',
    this.child_id = 0,
  });

  @JsonKey(name: Key_child_name)
  String child_name;
  @JsonKey(name: Key_child_gender)
  int child_gender;
  @JsonKey(name: Key_child_birth)
  String child_birth;
  @JsonKey(name: Key_child_nursery)
  int child_nursery;
  @JsonKey(name: Key_child_character)
  List<int>? child_character;
  @JsonKey(name: Key_is_allergy)
  bool is_allergy;
  @JsonKey(name: Key_allergy_name)
  String allergy_name;
  @JsonKey(name: Key_allergy_message)
  String allergy_message;
  @JsonKey(name: Key_child_id)
  int child_id;

  factory ChildInfoRequest.fromJson(Map<String, dynamic> json) {
    List<int> childCharacters = List<int>.from(json[Key_child_character]);
    return ChildInfoRequest(
      child_name: json[Key_child_name] ?? '',
      child_gender: json[Key_child_gender] as int,
      child_birth: json[Key_child_birth] ?? '',
      child_nursery: json[Key_child_nursery] as int,
      child_character: childCharacters,
      is_allergy: json[Key_is_allergy] ?? false,
      allergy_name: json[Key_allergy_name] ?? '',
      allergy_message: json[Key_allergy_message] ?? '',
      child_id: json[Key_child_id] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_child_name] = this.child_name;
    data[Key_child_gender] = this.child_gender;
    data[Key_child_birth] = this.child_birth;
    data[Key_child_nursery] = this.child_nursery;
    data[Key_child_character] = this.child_character;
    data[Key_is_allergy] = this.is_allergy;
    data[Key_allergy_name] = this.allergy_name;
    data[Key_allergy_message] = this.allergy_message;
    data[Key_child_id] = this.child_id;
    return data;
  }

  @override
  String toString() {
    return 'ChildInfoRequest{child_name: $child_name, child_gender: $child_gender, child_birth: $child_birth, child_nursery: $child_nursery, child_character: $child_character, is_allergy: $is_allergy, allergy_name: $allergy_name, allergy_message: $allergy_message, child_id: $child_id}';
  }
}
