import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : apply_delete_request.dart - 구인종료
/// since        : 2021/06/30 / update:
@JsonSerializable()
class ApplyDeleteRequest {
  static const String Key_matching_id = 'matching_id';

  ApplyDeleteRequest({this.matching_id = ''});

  @JsonKey(name: Key_matching_id)
  String matching_id;

  factory ApplyDeleteRequest.fromJson(Map<String, dynamic> json) {
    return ApplyDeleteRequest(
      matching_id: json[Key_matching_id] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_matching_id] = this.matching_id;
    return data;
  }

  @override
  String toString() {
    return 'ApplyDeleteRequest{matching_id: $matching_id}';
  }
}
