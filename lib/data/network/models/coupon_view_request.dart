import 'package:json_annotation/json_annotation.dart';

class CouponViewRequest {
  static const Key_id = 'id';

  @JsonKey(name: Key_id)
  int id;

  CouponViewRequest({this.id = 0});

  factory CouponViewRequest.fromJson(Map<String, dynamic> json) {
    return CouponViewRequest(
      id: json[Key_id] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    return data;
  }

  @override
  String toString() {
    return 'CouponViewRequest{id: $id}';
  }
}
