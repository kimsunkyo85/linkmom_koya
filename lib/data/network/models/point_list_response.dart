import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/data/network/models/pay_list_response.dart';

@JsonSerializable()
class PointListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<PointList> dataList;

  PointListResponse({this.dataHeader, required this.dataList});

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.message;

  PointList getData() => dataList.isNotEmpty ? dataList.first : PointList(results: []);

  factory PointListResponse.fromJson(Map<String, dynamic> json) {
    return PointListResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] != null && json[Key_data].isNotEmpty ? (json[Key_data] as List).map((e) => PointList.fromJson(e)).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_data] = this.dataList.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'PointListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class PointList {
  static const String Key_sum_point = 'sum_point';
  static const String Key_fire_point = 'fire_point';
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  @JsonKey(name: Key_sum_point)
  int sumPoint;
  @JsonKey(name: Key_fire_point)
  int firePoint;
  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<PointData> results;

  PointList({this.sumPoint = 0, this.firePoint = 0, this.count = 0, this.next = '', this.previous = '', required this.results});

  factory PointList.fromJson(Map<String, dynamic> json) {
    return PointList(
      sumPoint: json[Key_sum_point] ?? 0,
      firePoint: json[Key_fire_point] ?? 0,
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] == null ? [] : (json[Key_results] as List).map((e) => PointData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_sum_point] = this.sumPoint;
    data[Key_fire_point] = this.firePoint;
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'PointList{sumPoint: $sumPoint, firePoint: $firePoint, count: $count, next: $next, previous: $previous, results: $results}';
  }
}

class PointData {
  static const String Key_point_id = 'point_id';
  static const String Key_point_flag = 'point_flag';
  static const String Key_contract_id = 'contract_id';
  static const String Key_order_id = 'order_id';
  static const String Key_used_point = 'used_point';
  static const String Key_point_reason = 'point_reason';
  static const String Key_point_expiredate = 'point_expiredate';
  static const String Key_createdate = 'createdate';
  static const String Key_payment_info = 'payment_info';

  @JsonKey(name: Key_point_id)
  int pointId;
  @JsonKey(name: Key_point_flag)
  int pointFlag;
  @JsonKey(name: Key_contract_id)
  int contractId;
  @JsonKey(name: Key_order_id)
  String orderId;
  @JsonKey(name: Key_used_point)
  int usedPoint;
  @JsonKey(name: Key_point_reason)
  String pointReason;
  @JsonKey(name: Key_point_expiredate)
  String pointExpiredate;
  @JsonKey(name: Key_createdate)
  String createdate;
  @JsonKey(name: Key_payment_info)
  PayData paymentInfo;

  PointData({this.pointId = 0, this.pointFlag = 0, this.contractId = 0, this.orderId = '', this.usedPoint = 0, this.pointReason = '', this.pointExpiredate = '', this.createdate = '', required this.paymentInfo});

  factory PointData.fromJson(Map<String, dynamic> json) {
    return PointData(
      pointId: json[Key_point_id] ?? 0,
      pointFlag: json[Key_point_flag] ?? 0,
      contractId: json[Key_contract_id] ?? 0,
      orderId: json[Key_order_id] ?? '',
      usedPoint: json[Key_used_point] ?? 0,
      pointReason: json[Key_point_reason] ?? '',
      pointExpiredate: json[Key_point_expiredate] ?? '',
      createdate: json[Key_createdate] ?? '',
      paymentInfo: json[Key_payment_info] != null ? PayData.fromJson(json[Key_payment_info]) : PayData(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_point_id] = this.pointId;
    data[Key_point_flag] = this.pointFlag;
    data[Key_contract_id] = this.contractId;
    data[Key_order_id] = this.orderId;
    data[Key_used_point] = this.usedPoint;
    data[Key_point_reason] = this.pointReason;
    data[Key_point_expiredate] = this.pointExpiredate;
    data[Key_createdate] = this.createdate;
    data[Key_payment_info] = this.paymentInfo;
    return data;
  }

  @override
  String toString() {
    return 'PointData{pointId: $pointId, pointFlag: $pointFlag, contractId: $contractId, orderId: $orderId, usedPoint: $usedPoint, pointReason: $pointReason, pointExpiredate: $pointExpiredate, createdate: $createdate, paymentInfo: $paymentInfo}';
  }
}
