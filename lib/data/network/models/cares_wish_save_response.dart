import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class CaresWishSaveResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  CaresWishSaveResponse({
    this.dataHeader,
    dataList,
  });

  CaresWishSaveResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  CaresWishSaveData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CaresWishSaveData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CaresWishSaveResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CaresWishSaveData> datas = [];
    try {
      log.d('『GGUMBI』>>> fromJson ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<<${json[Key_data]} ');
      datas = json[Key_data].map<CaresWishSaveData>((i) => CaresWishSaveData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return CaresWishSaveResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CaresWishSaveResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class CaresWishSaveData {
  static const String Key_id = 'id';
  static const String Key_bookingcare = 'bookingcare';
  static const String Key_createdate = 'createdate';

  CaresWishSaveData({
    this.id = 0,
    this.bookingcare = 0,
    this.createdate = '',
  });

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_bookingcare)
  int bookingcare;
  @JsonKey(name: Key_createdate)
  String createdate;

  factory CaresWishSaveData.fromJson(Map<String, dynamic> json) {
    return CaresWishSaveData(
      id: json[Key_id] ?? '',
      bookingcare: json[Key_bookingcare] ?? '',
      createdate: json[Key_createdate] ?? '',
    );
  }

  @override
  String toString() {
    return 'CaresWishSaveData: {id: $id, bookingcare: $bookingcare, createdate: $createdate}';
  }
}
