import 'package:json_annotation/json_annotation.dart';

///- All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///- Created by   : platformbiz@ggumbi.com
///- version      : 1.0.0
///- see          : area_gps_view_request.dart - 동네인증 주소가져오기
///- since        : 2021/12/30 / update:
///- [함수,메서드시]
@JsonSerializable()
class AreaGpsViewRequest {
  static const String Key_latitude = 'latitude';
  static const String Key_longitude = 'longitude';

  AreaGpsViewRequest({
    this.latitude = '',
    this.longitude = '',
  });

  ///(GPS) 위도
  @JsonKey(name: Key_latitude)
  final String latitude;

  ///(GPS) 경도
  @JsonKey(name: Key_longitude)
  final String longitude;

  factory AreaGpsViewRequest.fromJson(Map<String, dynamic> json) {
    return AreaGpsViewRequest(
      latitude: json[Key_latitude] ?? '',
      longitude: json[Key_longitude] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_latitude] = this.latitude;
    data[Key_longitude] = this.longitude;
    return data;
  }

  // FormData toFormData() {
  //   FormData formData = FormData();
  //   if (StringUtils.getStringToBool(bcode)) {
  //     formData.fields.add(MapEntry(Key_bcode, bcode.toString()));
  //   }
  //   if (StringUtils.getStringToBool(hcode)) {
  //     formData.fields.add(MapEntry(Key_hcode, hcode.toString()));
  //   }
  //   if (StringUtils.getStringToBool(region_1depth)) {
  //     formData.fields.add(MapEntry(Key_region_1depth, region_1depth.toString()));
  //   }
  //   if (StringUtils.getStringToBool(region_2depth)) {
  //     formData.fields.add(MapEntry(Key_region_2depth, region_2depth.toString()));
  //   }
  //
  //   if (StringUtils.getStringToBool(region_3depth)) {
  //     formData.fields.add(MapEntry(Key_region_3depth, region_3depth.toString()));
  //   }
  //
  //   if (StringUtils.getStringToBool(region_3depth_h)) {
  //     formData.fields.add(MapEntry(Key_region_3depth_h, region_3depth_h.toString()));
  //   }
  //
  //   if (StringUtils.getStringToBool(latitude)) {
  //     formData.fields.add(MapEntry(Key_latitude, latitude.toString()));
  //   }
  //
  //   if (StringUtils.getStringToBool(longitude)) {
  //     formData.fields.add(MapEntry(Key_longitude, longitude.toString()));
  //   }
  //   return formData;
  // }

  @override
  String toString() {
    return 'AreaGpsViewRequest{latitude: $latitude, longitude: $longitude}';
  }
}
