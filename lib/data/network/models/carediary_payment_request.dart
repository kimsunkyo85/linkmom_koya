import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CarediaryPaymentRequest {
  static const String Key_schedule_id = 'schedule_id';

  @JsonKey(name: Key_schedule_id)
  int scheduleId;

  CarediaryPaymentRequest({this.scheduleId = 0});

  factory CarediaryPaymentRequest.fromJson(Map<String, dynamic> json) {
    return CarediaryPaymentRequest(
      scheduleId: json[Key_schedule_id] as int,
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_schedule_id] = this.scheduleId;
    return data;
  }

  @override
  String toString() {
    return 'CarediaryPaymentRequest{scheduleId:$scheduleId}';
  }
}
