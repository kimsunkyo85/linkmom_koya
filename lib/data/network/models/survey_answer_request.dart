import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

@JsonSerializable()
class SurveyAnswerRequest {
  static const String Key_survey = "survey";
  static const String Key_answer = "answer";

  SurveyAnswerRequest({
    this.survey = 0,
    this.answer,
  });

  @JsonKey(name: Key_survey)
  int survey;

  @JsonKey(name: Key_answer)
  List<SurveyAnswerData>? answer;

  factory SurveyAnswerRequest.fromJson(Map<String, dynamic> json) {
    List<SurveyAnswerData> datas = [];
    try {
      datas = json[Key_answer].map<SurveyAnswerData>((i) => SurveyAnswerData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    
    return SurveyAnswerRequest(
      survey: json[Key_survey] ?? 0,
      answer: datas,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_survey] = this.survey;
    data[Key_answer] = this.answer;
    return data;
  }

  @override
  String toString() {
    return 'SurveyAnswerRequest{$Key_survey: $survey, $Key_answer: ${answer.toString()}}';
  }
}

@JsonSerializable()
class SurveyAnswerData {
  static const String Key_question_seq = "question_seq";
  static const String Key_answer = "answer";

  SurveyAnswerData({
    this.question_seq = 0,
    this.answer = '',
  });

  @JsonKey(name: Key_question_seq)
  int question_seq;

  @JsonKey(name: Key_answer)
  String answer;

  factory SurveyAnswerData.fromJson(Map<String, dynamic> json) {
    return SurveyAnswerData(
      question_seq: json[Key_question_seq] ?? 0,
      answer: json[Key_answer] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_question_seq] = this.question_seq;
    data[Key_answer] = this.answer;
    return data;
  }

  @override
  String toString() {
    return 'SurveyAnswerData{$Key_question_seq: $question_seq, $Key_answer: $answer}';
  }
}
