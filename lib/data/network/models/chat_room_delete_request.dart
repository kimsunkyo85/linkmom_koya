import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_room_delete_request.dart - 채팅 방 목록 삭제
/// since        : 2021/07/15 / update:
@JsonSerializable()
class ChatRoomDeleteRequest {
  static const String Key_chatroom_id = 'chatroom_id';

  ChatRoomDeleteRequest({
    this.chatroom_id,
  });

  @JsonKey(name: Key_chatroom_id)
  final List<int>? chatroom_id;

  factory ChatRoomDeleteRequest.fromJson(Map<String, dynamic> json) {
    List<int> chatroomId = json[Key_chatroom_id] == null ? [] : List<int>.from(json[Key_chatroom_id]);
    return ChatRoomDeleteRequest(
      chatroom_id: chatroomId,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_chatroom_id] = this.chatroom_id;
    return data;
  }

  FormData toFormData() {
    FormData formData = FormData();
    if (chatroom_id != null) {
      chatroom_id!.forEach((value) {
        formData.fields.add(MapEntry(Key_chatroom_id, value.toString()));
      });
    }
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'ChatRoomDeleteRequest{chatroom_id: $chatroom_id}';
  }
}
