import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';
import 'header_response.dart';

@JsonSerializable()
class SurveyQuestionResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  SurveyQuestionResponse({
    this.dataHeader,
    this.dataList,
  });

  SurveyQuestionData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : SurveyQuestionData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory SurveyQuestionResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<SurveyQuestionData> datas = [];
    try {
      datas = json[Key_data].map<SurveyQuestionData>((i) => SurveyQuestionData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return SurveyQuestionResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ReqauthResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class SurveyQuestionData {
  static const String Key_count = "count";
  static const String Key_next = "next";
  static const String Key_previous = "previous";
  static const String Key_results = "results";

  SurveyQuestionData({
    this.count = 0,
    this.next = '',
    this.previous = '',
    this.results,
  });

  @JsonKey(name: Key_count)
  int count;

  @JsonKey(name: Key_next)
  String next;

  @JsonKey(name: Key_previous)
  String previous;

  @JsonKey(name: Key_results)
  List<SurveyQuestionInfo>? results;

  factory SurveyQuestionData.fromJson(Map<String, dynamic> json) {
    List<SurveyQuestionInfo> datas = [];
    try {
      datas = json[Key_results].map<SurveyQuestionInfo>((i) => SurveyQuestionInfo.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return SurveyQuestionData(
        count: json[Key_count] ?? 0, next: json[Key_next] ?? '', previous: json[Key_previous] ?? '', results: datas);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results;
    return data;
  }

  @override
  String toString() {
    return 'SurveyQuestionData{count: $count, next: $next, previous: $previous, results: ${results.toString()}}';
  }
}

@JsonSerializable()
class SurveyQuestionInfo {
  static const String Key_id = "id";
  static const String Key_survey = "survey";
  static const String Key_question_seq = "question_seq";
  static const String Key_question_type = "question_type";
  static const String Key_question = "question";
  static const String Key_answer1 = "answer1";
  static const String Key_answer2 = "answer2";
  static const String Key_answer3 = "answer3";
  static const String Key_answer4 = "answer4";
  static const String Key_answer5 = "answer5";

  SurveyQuestionInfo(
      {this.id = 0,
      this.survey = 0,
      this.question_seq = 0,
      this.question_type = 0,
      this.question = '',
      this.answer1 = '',
      this.answer2 = '',
      this.answer3 = '',
      this.answer4 = '',
      this.answer5 = ''});

  @JsonKey(name: Key_id)
  int id;

  @JsonKey(name: Key_survey)
  int survey;

  @JsonKey(name: Key_question_seq)
  int question_seq;

  @JsonKey(name: Key_question_type)
  int question_type;

  @JsonKey(name: Key_question)
  String question;

  @JsonKey(name: Key_answer1)
  String answer1;

  @JsonKey(name: Key_answer2)
  String answer2;

  @JsonKey(name: Key_answer3)
  String answer3;

  @JsonKey(name: Key_answer4)
  String answer4;

  @JsonKey(name: Key_answer5)
  String answer5;

  factory SurveyQuestionInfo.fromJson(Map<String, dynamic> json) {
    return SurveyQuestionInfo(
        id: json[Key_id] ?? 0,
        survey: json[Key_survey] ?? 0,
        question_seq: json[Key_question_seq] ?? 0,
        question_type: json[Key_question_type] ?? 0,
        question: json[Key_question] ?? '',
        answer1: json[Key_answer1] ?? '',
        answer2: json[Key_answer2] ?? '',
        answer3: json[Key_answer3] ?? '',
        answer4: json[Key_answer4] ?? '',
        answer5: json[Key_answer5] ?? '');
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_survey] = this.survey;
    data[Key_question_seq] = this.question_seq;
    data[Key_question_type] = this.question_type;
    data[Key_question] = this.question;
    data[Key_answer1] = this.answer1;
    data[Key_answer2] = this.answer2;
    data[Key_answer3] = this.answer3;
    data[Key_answer4] = this.answer4;
    data[Key_answer5] = this.answer5;
    return data;
  }

  @override
  String toString() {
    return 'SurveyQuestionInfo{$Key_id: $id, $Key_survey: $survey, $Key_question_seq: $question_seq, $Key_question_type: $question_type, $Key_question: $question, $Key_answer1: $answer1, $Key_answer2: $answer2, $Key_answer3: $answer3, $Key_answer4: $answer4, $Key_answer5: $answer5}';
  }
}
