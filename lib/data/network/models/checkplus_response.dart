import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

@JsonSerializable()
class CheckplusResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  CheckplusResponse({
    this.dataHeader,
    this.dataList,
  });

  NiceAuthData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : NiceAuthData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CheckplusResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<NiceAuthData> datas = [];
    try {
      datas = json[Key_data].map<NiceAuthData>((i) => NiceAuthData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return CheckplusResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CheckplusResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class NiceAuthData {
  static const String Key_plaindata = "plaindata";
  static const String Key_returnMsg = "returnMsg";
  static const String Key_ciphertime = "ciphertime";
  static const String Key_requestnumber = "requestnumber";
  static const String Key_responsenumber = "responsenumber";
  static const String Key_authtype = "authtype";
  static const String Key_name = "name";
  static const String Key_utfname = "utfname";
  static const String Key_birthdate = "birthdate";
  static const String Key_gender = "gender";
  static const String Key_nationalinfo = "nationalinfo";
  static const String Key_dupinfo = "dupinfo";
  static const String Key_conninfo = "conninfo";
  static const String Key_mobileno = "mobileno";
  static const String Key_mobileco = "mobileco";

  NiceAuthData({
    this.plaindata = '',
    this.returnMsg = '',
    this.ciphertime = '',
    this.requestnumber = '',
    this.responsenumber = '',
    this.authtype = '',
    this.name = '',
    this.utfname = '',
    this.birthdate = '',
    this.gender = '',
    this.nationalinfo = '',
    this.dupinfo = '',
    this.conninfo = '',
    this.mobileno = '',
    this.mobileco = '',
  });

  @JsonKey(name: Key_plaindata)
  String plaindata;
  @JsonKey(name: Key_returnMsg)
  String returnMsg;
  @JsonKey(name: Key_ciphertime)
  String ciphertime;
  @JsonKey(name: Key_requestnumber)
  String requestnumber;
  @JsonKey(name: Key_responsenumber)
  String responsenumber;
  @JsonKey(name: Key_authtype)
  String authtype;
  @JsonKey(name: Key_name)
  String name;
  @JsonKey(name: Key_utfname)
  String utfname;
  @JsonKey(name: Key_birthdate)
  String birthdate;
  @JsonKey(name: Key_gender)
  String gender;
  @JsonKey(name: Key_nationalinfo)
  String nationalinfo;
  @JsonKey(name: Key_dupinfo)
  String dupinfo;
  @JsonKey(name: Key_conninfo)
  String conninfo;
  @JsonKey(name: Key_mobileno)
  String mobileno;
  @JsonKey(name: Key_mobileco)
  String mobileco;

  factory NiceAuthData.fromJson(Map<String, dynamic> json) {
    return NiceAuthData(
      plaindata: json[Key_plaindata] ?? '',
      returnMsg: json[Key_returnMsg] ?? '',
      ciphertime: json[Key_ciphertime] ?? '',
      requestnumber: json[Key_requestnumber] ?? '',
      responsenumber: json[Key_responsenumber] ?? '',
      authtype: json[Key_authtype] ?? '',
      name: json[Key_name] ?? '',
      utfname: json[Key_utfname] ?? '',
      birthdate: json[Key_birthdate] ?? '',
      gender: json[Key_gender] ?? '',
      nationalinfo: json[Key_nationalinfo] ?? '',
      dupinfo: json[Key_dupinfo] ?? '',
      conninfo: json[Key_conninfo] ?? '',
      mobileno: json[Key_mobileno] ?? '',
      mobileco: json[Key_mobileco] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_plaindata] = this.plaindata;
    data[Key_returnMsg] = this.returnMsg;
    data[Key_ciphertime] = this.ciphertime;
    data[Key_requestnumber] = this.requestnumber;
    data[Key_responsenumber] = this.responsenumber;
    data[Key_authtype] = this.authtype;
    data[Key_name] = this.name;
    data[Key_utfname] = this.utfname;
    data[Key_birthdate] = this.birthdate;
    data[Key_gender] = this.gender;
    data[Key_nationalinfo] = this.nationalinfo;
    data[Key_dupinfo] = this.dupinfo;
    data[Key_conninfo] = this.conninfo;
    data[Key_mobileno] = this.mobileno;
    data[Key_mobileco] = this.mobileco;
    return data;
  }

  @override
  String toString() {
    return 'NiceAuthData{plaindata: $plaindata, returnMsg: $returnMsg, ciphertime: $ciphertime, requestnumber: $requestnumber, responsenumber: $responsenumber, authtype: $authtype, name: $name, utfname: $utfname, birthdate: $birthdate, gender: $gender, nationalinfo: $nationalinfo, dupinfo: $dupinfo, conninfo: $conninfo, mobileno: $mobileno, mobileco: $mobileco, }';
  }
}
