import 'dart:io';

import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CsContactUpdateRequest {
  static const String Key_id = 'id';
  static const String Key_type = 'type';
  static const String Key_title = 'title';
  static const String Key_content = 'content';
  static const String Key_image = 'image';
  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_type)
  int type;
  @JsonKey(name: Key_title)
  String title;
  @JsonKey(name: Key_content)
  String content;
  @JsonKey(name: Key_image)
  List<File>? image;

  CsContactUpdateRequest({this.id = 0, this.type = 0, this.title = '', this.content = '', this.image});

  factory CsContactUpdateRequest.fromJson(Map<String, dynamic> json) {
    return CsContactUpdateRequest(
      id: json[Key_id],
      type: json[Key_type],
      title: json[Key_title],
      content: json[Key_content],
      image: json[Key_image],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_type] = this.type;
    data[Key_title] = this.title;
    data[Key_content] = this.content;
    if (this.image != null) data[Key_image] = this.image;
    return data;
  }

  @override
  String toString() {
    return 'CsContactUpdateRequest{id: $id, type: $type, title: $title, content: $content, image: $image}';
  }

  FormData toFomData(FormData formData) {
    formData.fields.add(MapEntry(Key_id, this.id.toString()));
    formData.fields.add(MapEntry(Key_type, this.type.toString()));
    formData.fields.add(MapEntry(Key_title, this.title));
    formData.fields.add(MapEntry(Key_content, this.content));
    return formData;
  }
}
