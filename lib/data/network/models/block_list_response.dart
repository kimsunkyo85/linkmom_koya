import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/block_save_response.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class BlockListResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  BlockListResponse({
    this.dataHeader,
    this.dataList,
  });

  BlockListResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  BlockListData getData() => dataList != null && dataList!.length != 0 ? dataList![0] : ' ';
  
  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory BlockListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<BlockListData> datas = [];
    try {
      datas = json[Key_data].map<BlockListData>((i) => BlockListData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return BlockListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'BlockListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class BlockListData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  BlockListData({
    this.count = 0,
    this.next = '',
    this.previous = '',
    this.results,
  });

  @JsonKey(name: Key_count)
  int count;

  @JsonKey(name: Key_next)
  String next;

  @JsonKey(name: Key_previous)
  String previous;

  @JsonKey(name: Key_results)
  List<BlockUserData>? results;

  factory BlockListData.fromJson(Map<String, dynamic> json) {
    List<BlockUserData> datas = [];
    try {
      datas = json[Key_results].map<BlockUserData>((i) => BlockUserData.fromJson(i)).toList();
    } catch (e) {
    }
    return BlockListData(
      count: json[Key_count] as int,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: datas,
    );
  }
}
