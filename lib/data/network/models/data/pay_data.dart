import 'package:json_annotation/json_annotation.dart';

///시급 최소, 최대
class PayData {
  static const String Key_fee_min = 'fee_min';
  static const String Key_fee_max = 'fee_max';

  PayData({
    this.fee_min = 0,
    this.fee_max = 0,
  });

  @JsonKey(name: Key_fee_min)
  final int fee_min;
  @JsonKey(name: Key_fee_max)
  final int fee_max;

  factory PayData.fromJson(Map<String, dynamic> json) {
    return PayData(
      fee_min: json[Key_fee_min] ?? 5000,
      fee_max: json[Key_fee_max] ?? 50000,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_fee_min] = this.fee_min;
    data[Key_fee_max] = this.fee_max;
    return data;
  }

  @override
  String toString() {
    return 'PayData{fee_min: $fee_min, fee_max: $fee_max}';
  }
}
