import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class PayConractInfoData {
  static const String Key_contract_id = 'contract_id';
  static const String Key_momdady = 'momdady';
  static const String Key_momdady_first_name = 'momdady_first_name';
  static const String Key_linkmom = 'linkmom';
  static const String Key_linkmom_first_name = 'linkmom_first_name';
  static const String Key_matching_status = 'matching_status';
  static const String Key_care_status = 'care_status';
  static const String Key_contract_status = 'contract_status';

  PayConractInfoData({
    this.contract_id = 0,
    this.momdady = 0,
    this.momdady_first_name = '',
    this.linkmom = 0,
    this.linkmom_first_name = '',
    this.matching_status = 0,
    this.care_status = 0,
    this.contract_status = 0,
  });

  @JsonKey(name: Key_contract_id)
  final int? contract_id;
  @JsonKey(name: Key_momdady)
  final int? momdady;
  @JsonKey(name: Key_momdady_first_name)
  final String? momdady_first_name;
  @JsonKey(name: Key_linkmom)
  final int? linkmom;
  @JsonKey(name: Key_linkmom_first_name)
  final String? linkmom_first_name;
  @JsonKey(name: Key_matching_status)
  final int? matching_status;
  @JsonKey(name: Key_care_status)
  final int? care_status;
  @JsonKey(name: Key_contract_status)
  final int? contract_status;

  factory PayConractInfoData.fromJson(Map<String, dynamic> json) {
    return PayConractInfoData(
      contract_id: json[Key_contract_id] ?? 0,
      momdady: json[Key_momdady] ?? 0,
      momdady_first_name: json[Key_momdady_first_name] ?? '',
      linkmom: json[Key_linkmom] ?? 0,
      linkmom_first_name: json[Key_linkmom_first_name] ?? '',
      matching_status: json[Key_matching_status] ?? 0,
      care_status: json[Key_care_status] ?? 0,
      contract_status: json[Key_contract_status] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_contract_id] = this.contract_id;
    data[Key_momdady] = this.momdady;
    data[Key_momdady_first_name] = this.momdady_first_name;
    data[Key_linkmom] = this.linkmom;
    data[Key_linkmom_first_name] = this.linkmom_first_name;
    data[Key_matching_status] = this.matching_status;
    data[Key_care_status] = this.care_status;
    data[Key_contract_status] = this.contract_status;
    return data;
  }

  @override
  String toString() {
    return 'PayConractInfoData{contract_id: $contract_id, momdady: $momdady, momdady_first_name: $momdady_first_name, linkmom: $linkmom, linkmom_first_name: $linkmom_first_name, matching_status: $matching_status, care_status: $care_status, contract_status: $contract_status}';
  }
}
