import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : matching_list_request_data.dart - 돌봄 진행 내역 & 필터링 (매칭 된 건)(맘대디, 링크쌤)
/// since        : 2021/06/30 / update:
@JsonSerializable()
class MatchingListRequest {
  ///지원 주체 - 나에게지원한 : byme, 내가지원한 : tome, 둘다 선택 : bymetome
  static const String Key_bywho = 'bywho';

  ///조회 시작일자
  static const String Key_s_date = 's_date';

  ///조회 종료일자
  static const String Key_e_date = 'e_date';

  ///돌봄진행상태 (1:돌봄예정, 2:돌봄중, 3:돌봄종료, 4:돌봄취소)
  static const String Key_care_status = 'care_status';

  ///돌봄유형
  static const String Key_servicetype = 'servicetype';

  MatchingListRequest({
    this.bywho = '',
    this.s_date = '',
    this.e_date = '',
    this.care_status,
    this.servicetype,
  });

  ///지원 주체 - 나에게지원한 : byme - 내가지원한 : tome
  @JsonKey(name: Key_bywho)
  final String bywho;

  ///조회 시작일자
  @JsonKey(name: Key_s_date)
  final String s_date;

  ///조회 종료일자
  @JsonKey(name: Key_e_date)
  final String e_date;

  ///돌봄진행상태 (1:돌봄예정, 2:돌봄중, 3:돌봄종료, 4:돌봄취소)
  @JsonKey(name: Key_care_status)
  final List<int>? care_status;

  ///돌봄유형
  @JsonKey(name: Key_servicetype)
  final List<int>? servicetype;

  factory MatchingListRequest.fromJson(Map<String, dynamic> json) {
    List<int> careStatus = List<int>.from(json[Key_care_status]);
    List<int> servicetype = List<int>.from(json[Key_servicetype]);
    return MatchingListRequest(
      bywho: json[Key_bywho] ?? '',
      s_date: json[Key_s_date] ?? '',
      e_date: json[Key_e_date] ?? '',
      care_status: careStatus,
      servicetype: servicetype,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bywho] = this.bywho;
    data[Key_s_date] = this.s_date;
    data[Key_e_date] = this.e_date;
    data[Key_care_status] = this.care_status;
    data[Key_servicetype] = this.servicetype;
    return data;
  }

  FormData toFormData(FormData formData) {
    if (bywho.isNotEmpty) formData.fields.add(MapEntry(Key_bywho, bywho.toString()));
    if (s_date.isNotEmpty) formData.fields.add(MapEntry(Key_s_date, s_date.toString()));
    if (e_date.isNotEmpty) formData.fields.add(MapEntry(Key_e_date, e_date.toString()));
    if (care_status != null)
      care_status!.forEach((e) {
        formData.fields.add(MapEntry(Key_care_status, e.toString()));
    });
    if (servicetype != null)
      servicetype!.forEach((e) {
        formData.fields.add(MapEntry(Key_servicetype, e.toString()));
      });
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'MatchingListRequestData{bywho: $bywho, s_date: $s_date, e_date: $e_date, care_status: $care_status, servicetype: $servicetype}';
  }
}
