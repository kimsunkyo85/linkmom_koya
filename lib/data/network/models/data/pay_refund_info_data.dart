import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class PayRefundInfoData {
  static const String Key_request_cancel_price = 'request_cancel_price';
  static const String Key_refund_cash = 'refund_cash';
  static const String Key_refund_point = 'refund_point';
  static const String Key_expired_point = 'expired_point';
  static const String Key_cancelled_price = 'cancelled_price';

  PayRefundInfoData({
    this.request_cancel_price = 0,
    this.refund_cash = 0,
    this.refund_point = 0,
    this.expired_point = 0,
    this.cancelled_price = 0,
  });

  @JsonKey(name: Key_request_cancel_price)
  final int? request_cancel_price;
  @JsonKey(name: Key_refund_cash)
  final int? refund_cash;
  @JsonKey(name: Key_refund_point)
  final int? refund_point;
  @JsonKey(name: Key_expired_point)
  final int? expired_point;
  @JsonKey(name: Key_cancelled_price)
  final int? cancelled_price;

  factory PayRefundInfoData.fromJson(Map<String, dynamic> json) {
    return PayRefundInfoData(
      request_cancel_price: json[Key_request_cancel_price] ?? 0,
      refund_cash: json[Key_refund_cash] ?? 0,
      refund_point: json[Key_refund_point] ?? 0,
      expired_point: json[Key_expired_point] ?? 0,
      cancelled_price: json[Key_cancelled_price] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_request_cancel_price] = this.request_cancel_price;
    data[Key_refund_cash] = this.refund_cash;
    data[Key_refund_point] = this.refund_point;
    data[Key_expired_point] = this.expired_point;
    data[Key_cancelled_price] = this.cancelled_price;
    return data;
  }

  @override
  String toString() {
    return 'PayRefundInfoData{request_cancel_price: $request_cancel_price, refund_cash: $refund_cash, refund_point: $refund_point, expired_point: $expired_point, cancelled_price: $cancelled_price}';
  }
}
