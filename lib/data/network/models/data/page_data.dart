import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/data/chat_main_result_data.dart';

import '../../../../main.dart';
import '../chat_main_response.dart';
import '../chat_message_list_response.dart';
import '../linkmom_apply_list_response.dart';
import '../linkmom_matching_list_response.dart';
import '../linkmomd_matching_view_response.dart';
import '../momdady_apply_list_response.dart';
import '../momdady_matching_list_response.dart';
import '../momdady_matching_view_response.dart';
import '../momdady_mycares_list_response.dart';
import '../pay_cash_list_response.dart';
import '../pay_pay_list_response.dart';
import '../pay_point_list_response.dart';
import 'chat_send_data.dart';
import 'linkmom_result_data.dart';
import 'momdady_result_data.dart';

@JsonSerializable()
class PageData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  PageData({
    this.count = 0,
    this.next = '',
    this.previous = '',
    required this.results,
  });

  @JsonKey(name: Key_count)
  int count;

  @JsonKey(name: Key_next)
  String next;

  @JsonKey(name: Key_previous)
  String previous;

  @JsonKey(name: Key_results)
  List<dynamic> results;

  factory PageData.fromJson(Map<String, dynamic> json, String responseName) {
    List<dynamic> datas = [];
    try {
      if (responseName == '$MomdadyMyCaresListResponse') {
        datas = json[Key_results] == null ? [] : json[Key_results].map<MyCareResultsData>((i) => MyCareResultsData.fromJson(i)).toList();
      } else if (responseName == '$MomdadyApplyListResponse') {
        datas = json[Key_results] == null ? [] : json[Key_results].map<MomdadyResultsData>((i) => MomdadyResultsData.fromJson(i)).toList();
      } else if (responseName == '$MomdadyMatchingListResponse') {
        datas = json[Key_results] == null ? [] : json[Key_results].map<MomdadyResultsData>((i) => MomdadyResultsData.fromJson(i)).toList();
      } else if (responseName == '$LinkmomApplyListResponse') {
        datas = json[Key_results] == null ? [] : json[Key_results].map<LinkmomResultsData>((i) => LinkmomResultsData.fromJson(i)).toList();
      } else if (responseName == '$LinkmomMatchingListResponse') {
        datas = json[Key_results] == null ? [] : json[Key_results].map<LinkmomResultsData>((i) => LinkmomResultsData.fromJson(i)).toList();
      } else if (responseName == '$ChatMainResponse') {
        datas = json[Key_results] == null ? [] : json[Key_results].map<ChatMainResultsData>((i) => ChatMainResultsData.fromJson(i)).toList();
      } else if (responseName == '$ChatMessageListResponse') {
        datas = json[Key_results] == null ? [] : json[Key_results].map<ChatSendData>((i) => ChatSendData.fromJson(i)).toList();
      } else if (responseName == '$PayPayListResponse') {
        datas = json[Key_results] == null ? [] : json[Key_results].map<PayPayListData>((i) => PayPayListData.fromJson(i)).toList();
      } else if (responseName == '$PayCashListResponse') {
        datas = json[Key_results] == null ? [] : json[Key_results].map<PayCashListData>((i) => PayCashListData.fromJson(i)).toList();
      } else if (responseName == '$PayPointListResponse') {
        datas = json[Key_results] == null ? [] : json[Key_results].map<PayPointListData>((i) => PayPointListData.fromJson(i)).toList();
      } else if (responseName == '$MomdadyMatchingViewResponse') {
        datas = json[Key_results] == null ? [] : json[Key_results].map<MomdadyResultsData>((i) => MomdadyResultsData.fromJson(i)).toList();
      } else if (responseName == '$LinkmomMatchingViewResponse') {
        datas = json[Key_results] == null ? [] : json[Key_results].map<MomdadyResultsData>((i) => LinkmomResultsData.fromJson(i)).toList();
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return PageData(
      count: json[Key_count] as int,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: datas,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results;
    return data;
  }

  @override
  String toString() {
    return 'PageData{count: $count, next: $next, previous: $previous, results: $results}';
  }
}
