import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/string_util.dart';

import '../req_job_data.dart';
import 'auth_info_data.dart';
import 'review_info_data.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : job_myinfo_data.dart - 링크쌤 프로필, 링크쌤 상세 데이터
/// since        : 2021/06/03 / update:
@JsonSerializable()
class JobMyInfoData {
  ///나의정보
  static const String Key_userinfo = 'userinfo';

  ///인증정보
  static const String Key_authinfo = 'authinfo';

  ///돌봄유형 (0:등원, 1:하원, 2:보육, 3:학습, 4:입주가사, 5:이유식)
  static const String Key_servicetype = 'servicetype';

  ///돌봄장소(1:우리집, 2:이웃집)
  static const String Key_possible_area = 'possible_area';

  ///그룹보육
  static const String Key_is_groupboyuk = 'is_groupboyuk';

  ///동시보육
  static const String Key_is_dongsiboyuk = 'is_dongsiboyuk';

  ///이동방법(1:도보, 2:자동차, 3:대중교통)
  static const String Key_vehicle_id = 'vehicle_id';

  ///1순위 법정동 코드
  static const String Key_job_areaid_1 = 'job_areaid_1';

  ///1순위 행정동 코드
  static const String Key_job_addressid_1 = 'job_addressid_1';

  ///1순위 주소
  static const String Key_job_area_address_1 = 'job_area_address_1';

  ///2순위 법정동 코드
  static const String Key_job_areaid_2 = 'job_areaid_2';

  ///2순위 행정동 코드
  static const String Key_job_addressid_2 = 'job_addressid_2';

  ///2순위 주소
  static const String Key_job_area_address_2 = 'job_area_address_2';

  ///등원 시급
  static const String Key_fee_gotoschool = 'fee_gotoschool';

  ///하원 시급
  static const String Key_fee_afterschool = 'fee_afterschool';

  ///보육 시급
  static const String Key_fee_boyuk = 'fee_boyuk';

  ///돌봄대상 (0:신생아, 1:영아, 2:유아, 3:초등학생, 4:중학생, 5:고등학생)
  static const String Key_care_ages = 'care_ages';

  ///한줄자기소개
  static const String Key_introduce = 'introduce';

  ///돌봄방식 소개
  static const String Key_care_introduce = 'care_introduce';

  ///스케줄 시작 날짜
  static const String Key_caredate_min = "caredate_min";

  ///스케줄 종료 날짜
  static const String Key_caredate_max = "caredate_max";

  ///스케쥴 데이터
  static const String Key_careschedule = 'careschedule';

  ///구직중:0, 구직종료:1
  static const String Key_status = 'status';

  JobMyInfoData({
    this.userinfo,
    this.authinfo,
    this.servicetype,
    this.possible_area,
    this.is_groupboyuk = false,
    this.is_dongsiboyuk = false,
    this.vehicle_id,
    this.job_areaid_1 = '',
    this.job_addressid_1 = '',
    this.job_area_address_1 = '',
    this.job_areaid_2 = '',
    this.job_addressid_2 = '',
    this.job_area_address_2 = '',
    this.fee_gotoschool = 0,
    this.fee_afterschool = 0,
    this.fee_boyuk = 0,
    this.care_ages,
    this.introduce = '',
    this.care_introduce = '',
    this.caredate_min = '',
    this.caredate_max = '',
    this.careschedule,
    this.status = 0,
  });

  @JsonKey(name: Key_userinfo)
  MyInfoData? userinfo;
  @JsonKey(name: Key_authinfo)
  AuthInfoData? authinfo;
  @JsonKey(name: Key_servicetype)
  List<int>? servicetype;
  @JsonKey(name: Key_possible_area)
  List<int>? possible_area;
  @JsonKey(name: Key_is_groupboyuk)
  final bool is_groupboyuk;
  @JsonKey(name: Key_is_dongsiboyuk)
  final bool is_dongsiboyuk;
  @JsonKey(name: Key_vehicle_id)
  List<int>? vehicle_id;
  @JsonKey(name: Key_job_areaid_1)
  final String job_areaid_1;
  @JsonKey(name: Key_job_addressid_1)
  final String job_addressid_1;
  @JsonKey(name: Key_job_area_address_1)
  final String job_area_address_1;
  @JsonKey(name: Key_job_areaid_2)
  final String job_areaid_2;
  @JsonKey(name: Key_job_addressid_2)
  final String job_addressid_2;
  @JsonKey(name: Key_job_area_address_2)
  final String job_area_address_2;
  @JsonKey(name: Key_fee_gotoschool)
  int fee_gotoschool;
  @JsonKey(name: Key_fee_afterschool)
  int fee_afterschool;
  @JsonKey(name: Key_fee_boyuk)
  int fee_boyuk;
  @JsonKey(name: Key_care_ages)
  List<int>? care_ages;
  @JsonKey(name: Key_introduce)
  String introduce;

  ///돌봄방식소개
  @JsonKey(name: Key_care_introduce)
  String care_introduce = '';

  @JsonKey(name: Key_caredate_min)
  String caredate_min;
  @JsonKey(name: Key_caredate_max)
  String caredate_max;

  @JsonKey(name: Key_careschedule)
  List<ReqJobScheduleData>? careschedule;

  ///구직중:0, 구직종료:1
  @JsonKey(name: Key_status)
  int status;

  factory JobMyInfoData.fromJson(Map<String, dynamic> json) {
    MyInfoData userInfoData = MyInfoData.fromJson(json[Key_userinfo]);
    AuthInfoData authInfoData = AuthInfoData.fromJson(json[Key_authinfo]);
    List<int> servicetype = json[Key_servicetype] == null ? [] : List<int>.from(json[Key_servicetype]);
    List<int> possibleArea = json[Key_possible_area] == null ? [] : List<int>.from(json[Key_possible_area]);
    List<int> vehicleId = json[Key_vehicle_id] == null ? [] : List<int>.from(json[Key_vehicle_id]);
    List<int> careAges = json[Key_care_ages] == null ? [] : List<int>.from(json[Key_care_ages]);
    List<ReqJobScheduleData> bookingjobs = json[Key_careschedule] == null ? [] : json[Key_careschedule].map<ReqJobScheduleData>((i) => ReqJobScheduleData.fromJson(i)).toList() as List<ReqJobScheduleData>;
    return JobMyInfoData(
      userinfo: userInfoData,
      authinfo: authInfoData,
      servicetype: servicetype,
      possible_area: possibleArea,
      is_groupboyuk: json[Key_is_groupboyuk] as bool,
      is_dongsiboyuk: json[Key_is_dongsiboyuk] as bool,
      vehicle_id: vehicleId,
      job_areaid_1: json[Key_job_areaid_1] ?? '',
      job_addressid_1: json[Key_job_addressid_1] ?? '',
      job_area_address_1: json[Key_job_area_address_1] ?? '',
      job_areaid_2: json[Key_job_areaid_2] ?? '',
      job_addressid_2: json[Key_job_addressid_2] ?? '',
      job_area_address_2: json[Key_job_area_address_2] ?? '',
      fee_gotoschool: json[Key_fee_gotoschool] as int,
      fee_afterschool: json[Key_fee_afterschool] as int,
      fee_boyuk: json[Key_fee_boyuk] as int,
      care_ages: careAges,
      introduce: json[Key_introduce] ?? '',
      care_introduce: json[Key_care_introduce] ?? '',
      caredate_min: json[Key_caredate_min] ?? '',
      caredate_max: json[Key_caredate_max] ?? '',
      careschedule: bookingjobs,
      status: json[Key_status] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_userinfo] = this.userinfo;
    data[Key_authinfo] = this.authinfo;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_is_groupboyuk] = this.is_groupboyuk;
    data[Key_is_dongsiboyuk] = this.is_dongsiboyuk;
    data[Key_vehicle_id] = this.vehicle_id;
    data[Key_job_areaid_1] = this.job_areaid_1;
    data[Key_job_addressid_1] = this.job_addressid_1;
    data[Key_job_area_address_1] = this.job_area_address_1;
    data[Key_job_areaid_2] = this.job_areaid_2;
    data[Key_job_addressid_2] = this.job_addressid_2;
    data[Key_job_area_address_2] = this.job_area_address_2;
    data[Key_fee_gotoschool] = this.fee_gotoschool;
    data[Key_fee_afterschool] = this.fee_afterschool;
    data[Key_fee_boyuk] = this.fee_boyuk;
    data[Key_care_ages] = this.care_ages;
    data[Key_introduce] = this.introduce;
    data[Key_care_introduce] = this.care_introduce;
    data[Key_caredate_min] = this.caredate_min;
    data[Key_caredate_max] = this.caredate_max;
    data[Key_careschedule] = this.careschedule;
    data[Key_status] = this.status;
    return data;
  }

  BookingLinkMomStatus getStatus() {
    return EnumUtils.getBookingLinkMomStatus(status);
  }

  @override
  String toString() {
    return 'JobMyInfoData{userinfo: $userinfo, authinfo: $authinfo, servicetype: $servicetype, possible_area: $possible_area, is_groupboyuk: $is_groupboyuk, is_dongsiboyuk: $is_dongsiboyuk, vehicle_id: $vehicle_id, job_areaid_1: $job_areaid_1, job_addressid_1: $job_addressid_1, job_area_address_1: $job_area_address_1, job_areaid_2: $job_areaid_2, job_addressid_2: $job_addressid_2, job_area_address_2: $job_area_address_2, fee_gotoschool: $fee_gotoschool, fee_afterschool: $fee_afterschool, fee_boyuk: $fee_boyuk, care_ages: $care_ages, introduce: $introduce, care_introduce: $care_introduce, caredate_min: $caredate_min, caredate_max: $caredate_max, careschedule: $careschedule, status: $status}';
  }
}

///나의정보
class MyInfoData {
  static const String Key_user_id = 'user_id';
  static const String Key_first_name = 'first_name';
  static const String Key_gender = 'gender';
  static const String Key_age = 'age';
  static const String Key_numberofchild = 'numberofchild';
  static const String Key_job = 'job';
  static const String Key_profileimg = 'profileimg';
  static const String Key_is_user_momdady = 'is_user_momdady';
  static const String Key_is_user_linkmom = 'is_user_linkmom';
  static const String Key_grade_linkmom = 'grade_linkmom';
  static const String Key_grade_linkmom_txt = 'grade_linkmom_txt';
  static const String Key_region_2depth = 'region_2depth';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_4depth = 'region_4depth';
  static const String Key_is_affiliate = 'is_affiliate';
  static const String Key_is_follower = 'is_follower';
  static const String Key_reviewinfo = 'reviewinfo';

  MyInfoData({
    this.user_id = 0,
    this.first_name = '',
    this.gender = '',
    this.age = '',
    this.numberofchild = '',
    this.job = '',
    this.profileimg = '',
    this.is_user_momdady = false,
    this.is_user_linkmom = false,
    this.grade_linkmom = 0,
    this.grade_linkmom_txt = '',
    this.region_2depth = '',
    this.region_3depth = '',
    this.region_4depth = '',
    this.is_affiliate = false,
    this.is_follower = false,
    this.reviewinfo,
  });

  @JsonKey(name: Key_user_id)
  final int user_id;
  @JsonKey(name: Key_first_name)
  final String first_name;
  @JsonKey(name: Key_gender)
  final String gender;
  @JsonKey(name: Key_age)
  final String age;
  @JsonKey(name: Key_numberofchild)
  final String numberofchild;
  @JsonKey(name: Key_job)
  final String job;
  @JsonKey(name: Key_profileimg)
  final String profileimg;
  @JsonKey(name: Key_is_user_momdady)
  final bool is_user_momdady;
  @JsonKey(name: Key_is_user_linkmom)
  final bool is_user_linkmom;
  @JsonKey(name: Key_grade_linkmom)
  final int grade_linkmom;
  @JsonKey(name: Key_grade_linkmom_txt)
  final String grade_linkmom_txt;
  @JsonKey(name: Key_region_2depth)
  final String region_2depth;
  @JsonKey(name: Key_region_3depth)
  final String region_3depth;
  @JsonKey(name: Key_region_4depth)
  final String region_4depth;

  ///제휴 인증 여부 true, false
  @JsonKey(name: Key_is_affiliate)
  final bool is_affiliate;
  @JsonKey(name: Key_is_follower)
  final bool is_follower;

  ///리뷰,후기 데이터
  @JsonKey(name: Key_reviewinfo)
  ReViewInfoData? reviewinfo;

  factory MyInfoData.fromJson(Map<String, dynamic> json) {
    ReViewInfoData reViewInfoData = ReViewInfoData.fromJson(json[Key_reviewinfo]);
    return MyInfoData(
      user_id: json[Key_user_id] ?? 0,
      first_name: json[Key_first_name] ?? '',
      gender: json[Key_gender] ?? '',
      age: json[Key_age] ?? '',
      numberofchild: json[Key_numberofchild] ?? '',
      job: json[Key_job] ?? '',
      profileimg: json[Key_profileimg] ?? '',
      is_user_momdady: json[Key_is_user_momdady] ?? false,
      is_user_linkmom: json[Key_is_user_linkmom] ?? false,
      grade_linkmom: json[Key_grade_linkmom] as int,
      grade_linkmom_txt: json[Key_grade_linkmom_txt] ?? '',
      region_2depth: json[Key_region_2depth] ?? '',
      region_3depth: json[Key_region_3depth] ?? '',
      region_4depth: json[Key_region_4depth] ?? '',
      is_affiliate: json[Key_is_affiliate] ?? false,
      is_follower: json[Key_is_follower] ?? false,
      reviewinfo: reViewInfoData,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_user_id] = this.user_id;
    data[Key_first_name] = this.first_name;
    data[Key_gender] = this.gender;
    data[Key_age] = this.age;
    data[Key_numberofchild] = this.numberofchild;
    data[Key_job] = this.job;
    data[Key_profileimg] = this.profileimg;
    data[Key_is_user_momdady] = this.is_user_momdady;
    data[Key_is_user_linkmom] = this.is_user_linkmom;
    data[Key_grade_linkmom] = this.grade_linkmom;
    data[Key_grade_linkmom_txt] = this.grade_linkmom_txt;
    data[Key_region_2depth] = this.region_2depth;
    data[Key_region_3depth] = this.region_3depth;
    data[Key_region_4depth] = this.region_4depth;
    data[Key_is_affiliate] = this.is_affiliate;
    data[Key_is_follower] = this.is_follower;
    data[Key_reviewinfo] = this.reviewinfo;
    return data;
  }

  String getAddress() {
    return StringUtils.getAddressParser(region_3depth, region_4depth);
  }

  @override
  String toString() {
    return 'MyInfoData{user_id: $user_id, first_name: $first_name, gender: $gender, age: $age, numberofchild: $numberofchild, job: $job, profileimg: $profileimg, is_user_momdady: $is_user_momdady, is_user_linkmom: $is_user_linkmom, grade_linkmom: $grade_linkmom, grade_linkmom_txt: $grade_linkmom_txt, region_2depth: $region_2depth, region_3depth: $region_3depth, region_4depth: $region_4depth, is_affiliate: $is_affiliate, is_follower: $is_follower, reviewinfo: $reviewinfo}';
  }
}
