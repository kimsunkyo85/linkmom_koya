import 'package:json_annotation/json_annotation.dart';

import 'chat_room_data.dart';
import 'chat_send_data.dart';
import 'pay_care_diary_info_data.dart';
import 'pay_payment_info_data.dart';
import 'pay_refund_info_data.dart';

///결제취소저장, 소명하기 같이 사용
class PayCanceSaveExplainData {
  static const String Key_cancel_id = 'cancel_id';
  static const String Key_is_totalcancel = 'is_totalcancel';
  static const String Key_is_refund = 'is_refund';
  static const String Key_revoked_at = 'revoked_at';
  static const String Key_cancel_req_date = 'cancel_req_date';
  static const String Key_cancel_usertype = 'cancel_usertype';
  static const String Key_cancel_usertype_txt = 'cancel_usertype_txt';
  static const String Key_user_id = 'user_id';
  static const String Key_order_id = 'order_id';
  static const String Key_bookingcareservices_info = 'bookingcareservices_info';
  static const String Key_payment_info = 'payment_info';
  static const String Key_refund_info = 'refund_info';
  static const String Key_contract_info = 'contract_info';
  static const String Key_carediary_info = 'carediary_info';
  static const String Key_cancelreason = 'cancelreason';
  static const String Key_cancelreason_txt = 'cancelreason_txt';
  static const String Key_cancelreason_msg = 'cancelreason_msg';
  static const String Key_cancelreason_img = 'cancelreason_img';
  static const String Key_cancelexplain_deadline = 'cancelexplain_deadline';
  static const String Key_cancelexplain_msg = 'cancelexplain_msg';
  static const String Key_cancelexplain_img = 'cancelexplain_img';
  static const String Key_is_cancelexplain = 'is_cancelexplain';
  static const String Key_manager_msg = 'manager_msg';
  static const String Key_is_cancelreason_agree = 'is_cancelreason_agree';

  PayCanceSaveExplainData({
    this.cancel_id,
    this.is_totalcancel,
    this.is_refund,
    this.revoked_at,
    this.cancel_req_date,
    this.cancel_usertype,
    this.cancel_usertype_txt,
    this.user_id,
    this.order_id,
    this.bookingcareservices_info,
    this.contract_info,
    this.payment_info,
    this.refund_info,
    this.carediary_info,
    this.cancelreason,
    this.cancelreason_txt,
    this.cancelreason_msg,
    this.cancelreason_img,
    this.cancelexplain_deadline,
    this.cancelexplain_msg,
    this.cancelexplain_img,
    this.is_cancelexplain,
    this.manager_msg,
    this.is_cancelreason_agree = false,
  });

  PayCanceSaveExplainData.clone(PayCanceSaveExplainData item)
      : this(
          cancel_id: item.cancel_id,
          is_totalcancel: item.is_totalcancel,
          is_refund: item.is_refund,
          revoked_at: item.revoked_at,
          cancel_req_date: item.cancel_req_date,
          cancel_usertype: item.cancel_usertype,
          cancel_usertype_txt: item.cancel_usertype_txt,
          user_id: item.user_id,
          order_id: item.order_id,
          bookingcareservices_info: item.bookingcareservices_info,
          contract_info: item.contract_info,
          payment_info: item.payment_info,
          refund_info: item.refund_info,
          carediary_info: item.carediary_info,
          cancelreason: item.cancelreason,
          cancelreason_txt: item.cancelreason_txt,
          cancelreason_msg: item.cancelreason_msg,
          cancelreason_img: item.cancelreason_img,
          cancelexplain_deadline: item.cancelexplain_deadline,
          cancelexplain_msg: item.cancelexplain_msg,
          cancelexplain_img: item.cancelexplain_img,
          is_cancelexplain: item.is_cancelexplain,
          manager_msg: item.manager_msg,
          is_cancelreason_agree: item.is_cancelreason_agree,
        );

  @JsonKey(name: Key_cancel_id)
  final int? cancel_id;
  @JsonKey(name: Key_is_totalcancel)
  final bool? is_totalcancel;
  @JsonKey(name: Key_is_refund)
  final bool? is_refund;
  @JsonKey(name: Key_revoked_at)
  final String? revoked_at;
  @JsonKey(name: Key_cancel_req_date)
  final String? cancel_req_date;
  @JsonKey(name: Key_cancel_usertype)
  final int? cancel_usertype;
  @JsonKey(name: Key_cancel_usertype_txt)
  final String? cancel_usertype_txt;
  @JsonKey(name: Key_user_id)
  final int? user_id;
  @JsonKey(name: Key_order_id)
  final String? order_id;
  @JsonKey(name: Key_bookingcareservices_info)
  final ChatCareInfoData? bookingcareservices_info;
  @JsonKey(name: Key_contract_info)
  final ContractData? contract_info;
  @JsonKey(name: Key_payment_info)
  final PayMentInfoData? payment_info;
  @JsonKey(name: Key_refund_info)
  final PayRefundInfoData? refund_info;
  @JsonKey(name: Key_carediary_info)
  final List<PayCarediaryInfoData>? carediary_info;
  @JsonKey(name: Key_cancelreason)
  final int? cancelreason;
  @JsonKey(name: Key_cancelreason_txt)
  final String? cancelreason_txt;
  @JsonKey(name: Key_cancelreason_msg)
  final String? cancelreason_msg;
  @JsonKey(name: Key_cancelreason_img)
  final List<CancelImgData>? cancelreason_img;
  @JsonKey(name: Key_cancelexplain_deadline)
  final String? cancelexplain_deadline;
  @JsonKey(name: Key_cancelexplain_msg)
  final String? cancelexplain_msg;
  @JsonKey(name: Key_cancelexplain_img)
  final List<CancelImgData>? cancelexplain_img;
  @JsonKey(name: Key_is_cancelexplain)
  final bool? is_cancelexplain;
  @JsonKey(name: Key_manager_msg)
  final String? manager_msg;
  @JsonKey(name: Key_is_cancelreason_agree)
  final bool is_cancelreason_agree;

  factory PayCanceSaveExplainData.fromJson(Map<String, dynamic> json) {
    ChatCareInfoData? careinfo = json[Key_bookingcareservices_info] == null ? ChatCareInfoData() : ChatCareInfoData.fromJson(json[Key_bookingcareservices_info]);
    PayMentInfoData? paymentInfo = json[Key_payment_info] == null ? PayMentInfoData() : PayMentInfoData.fromJson(json[Key_payment_info]);
    PayRefundInfoData? refundInfo = json[Key_refund_info] == null ? PayRefundInfoData() : PayRefundInfoData.fromJson(json[Key_refund_info]);
    ContractData? contractInfo = json[Key_contract_info] == null ? ContractData() : ContractData.fromJson(json[Key_contract_info]);
    List<PayCarediaryInfoData>? carediaryInfoData = json[Key_carediary_info] == null ? [] : json[Key_carediary_info].map<PayCarediaryInfoData>((i) => PayCarediaryInfoData.fromJson(i)).toList();
    List<CancelImgData> cancelreasonImg = json[Key_cancelreason_img] == null ? [] : json[Key_cancelreason_img].map<CancelImgData>((i) => CancelImgData.fromJson(i)).toList(); //List<CancelImgData>.from(json[Key_cancelreason_img]);
    List<CancelImgData> cancelexplainImg = json[Key_cancelexplain_img] == null ? [] : json[Key_cancelexplain_img].map<CancelImgData>((i) => CancelImgData.fromJson(i)).toList(); //List<CancelImgData>.from(json[Key_cancelexplain_img]);
    return PayCanceSaveExplainData(
      cancel_id: json[Key_cancel_id] as int,
      is_totalcancel: json[Key_is_totalcancel] ?? false,
      is_refund: json[Key_is_refund] ?? false,
      revoked_at: json[Key_revoked_at] ?? '',
      cancel_req_date: json[Key_cancel_req_date] ?? '',
      cancel_usertype: json[Key_cancel_usertype] as int,
      cancel_usertype_txt: json[Key_cancel_usertype_txt] ?? '',
      user_id: json[Key_user_id] as int,
      order_id: json[Key_order_id] ?? '',
      bookingcareservices_info: careinfo,
      payment_info: paymentInfo,
      refund_info: refundInfo,
      contract_info: contractInfo,
      carediary_info: carediaryInfoData,
      cancelreason: json[Key_cancelreason] as int,
      cancelreason_txt: json[Key_cancelreason_txt] ?? '',
      cancelreason_msg: json[Key_cancelreason_msg] ?? '',
      cancelreason_img: cancelreasonImg,
      cancelexplain_deadline: json[Key_cancelexplain_deadline] ?? '',
      cancelexplain_msg: json[Key_cancelexplain_msg] ?? '',
      cancelexplain_img: cancelexplainImg,
      is_cancelexplain: json[Key_is_cancelexplain] ?? false,
      manager_msg: json[Key_manager_msg] ?? '',
      is_cancelreason_agree: json[Key_is_cancelreason_agree] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_cancel_id] = this.cancel_id;
    data[Key_is_totalcancel] = this.is_totalcancel;
    data[Key_is_refund] = this.is_refund;
    data[Key_revoked_at] = this.revoked_at;
    data[Key_cancel_req_date] = this.cancel_req_date;
    data[Key_cancel_usertype] = this.cancel_usertype;
    data[Key_cancel_usertype_txt] = this.cancel_usertype_txt;
    data[Key_user_id] = this.user_id;
    data[Key_order_id] = this.order_id;
    data[Key_bookingcareservices_info] = this.bookingcareservices_info;
    data[Key_payment_info] = this.payment_info;
    data[Key_refund_info] = this.refund_info;
    data[Key_contract_info] = this.contract_info;
    data[Key_carediary_info] = this.carediary_info;
    data[Key_cancelreason] = this.cancelreason;
    data[Key_cancelreason_txt] = this.cancelreason_txt;
    data[Key_cancelreason_msg] = this.cancelreason_msg;
    data[Key_cancelreason_img] = this.cancelreason_img;
    data[Key_cancelexplain_deadline] = this.cancelexplain_deadline;
    data[Key_cancelexplain_msg] = this.cancelexplain_msg;
    data[Key_cancelexplain_img] = this.cancelexplain_img;
    data[Key_is_cancelexplain] = this.is_cancelexplain;
    data[Key_manager_msg] = this.manager_msg;
    data[Key_is_cancelreason_agree] = this.is_cancelreason_agree;
    return data;
  }

  @override
  String toString() {
    return 'PayCanceSaveExplainData{cancel_id: $cancel_id, is_totalcancel: $is_totalcancel, is_refund: $is_refund, revoked_at: $revoked_at, cancel_req_date: $cancel_req_date, cancel_usertype: $cancel_usertype, cancel_usertype_txt: $cancel_usertype_txt, user_id: $user_id, order_id: $order_id, bookingcareservices_info: $bookingcareservices_info, contract_info: $contract_info, payment_info: $payment_info, refund_info: $refund_info, carediary_info: $carediary_info, cancelreason: $cancelreason, cancelreason_txt: $cancelreason_txt, cancelreason_msg: $cancelreason_msg, cancelreason_img: $cancelreason_img, cancelexplain_deadline: $cancelexplain_deadline, cancelexplain_msg: $cancelexplain_msg, cancelexplain_img: $cancelexplain_img, is_cancelexplain: $is_cancelexplain, manager_msg: $manager_msg, is_cancelreason_agree: $is_cancelreason_agree}';
  }
}

class CancelImgData {
  static const String Key_id = 'id';
  static const String Key_image = 'image';
  static const String Key_createdate = 'createdate';

  CancelImgData({
    this.id,
    this.image,
    this.createdate,
  });

  @JsonKey(name: Key_id)
  final int? id;
  @JsonKey(name: Key_image)
  final String? image;
  @JsonKey(name: Key_createdate)
  final String? createdate;

  factory CancelImgData.fromJson(Map<String, dynamic> json) {
    return CancelImgData(
      id: json[Key_id] as int,
      image: json[Key_image] ?? '',
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_image] = this.image;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'CancelImgData{id: $id, image: $image, createdate: $createdate}';
  }
}
