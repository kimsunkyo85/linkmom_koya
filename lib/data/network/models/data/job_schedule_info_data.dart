import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class JobScheduleInfo {
  static const String Key_job_id = 'job_id';
  static const String Key_fee_min = 'fee_min';
  static const String Key_fee_max = 'fee_max';
  static const String Key_servicetype = 'servicetype';
  static const String Key_possible_area = 'possible_area';
  static const String Key_caredate_min = 'caredate_min';
  static const String Key_caredate_max = 'caredate_max';
  static const String Key_careschedule_cnt = 'careschedule_cnt';
  static const String Key_introduce = 'introduce';

  JobScheduleInfo({
    this.job_id = 0,
    this.fee_min = 0,
    this.fee_max = 0,
    this.servicetype = '',
    this.possible_area = '',
    this.caredate_min = '',
    this.caredate_max = '',
    this.careschedule_cnt = 0,
    this.introduce = '',
  });

  @JsonKey(name: Key_job_id)
  int job_id;
  @JsonKey(name: Key_fee_min)
  int fee_min;
  @JsonKey(name: Key_fee_max)
  int fee_max;
  @JsonKey(name: Key_servicetype)
  String servicetype;
  @JsonKey(name: Key_possible_area)
  String possible_area;
  @JsonKey(name: Key_caredate_min)
  String caredate_min;
  @JsonKey(name: Key_caredate_max)
  String caredate_max;
  @JsonKey(name: Key_careschedule_cnt)
  int careschedule_cnt;
  @JsonKey(name: Key_introduce)
  String introduce;

  factory JobScheduleInfo.fromJson(Map<String, dynamic> json) {
    return JobScheduleInfo(
      job_id: json[Key_job_id] as int,
      fee_min: json[Key_fee_min] as int,
      fee_max: json[Key_fee_max] as int,
      servicetype: json[Key_servicetype],
      possible_area: json[Key_possible_area],
      caredate_min: json[Key_caredate_min],
      caredate_max: json[Key_caredate_max],
      careschedule_cnt: json[Key_careschedule_cnt] as int,
      introduce: json[Key_introduce],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_job_id] = this.job_id;
    data[Key_fee_min] = this.fee_min;
    data[Key_fee_max] = this.fee_max;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_caredate_min] = this.caredate_min;
    data[Key_caredate_max] = this.caredate_max;
    data[Key_careschedule_cnt] = this.careschedule_cnt;
    data[Key_introduce] = this.introduce;
    return data;
  }

  @override
  String toString() {
    return 'JobScheduleInfo{job_id: $job_id, fee_min: $fee_min, fee_max: $fee_max, servicetype: $servicetype, possible_area: $possible_area, caredate_min: $caredate_min, caredate_max: $caredate_max, careschedule_cnt: $careschedule_cnt, introduce: $introduce}';
  }
}
