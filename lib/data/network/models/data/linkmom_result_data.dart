import 'package:json_annotation/json_annotation.dart';

import '../../../../main.dart';
import 'auth_info_data.dart';
import 'bywho_data.dart';
import 'care_child_info_data.dart';
import 'care_schedul_info_data.dart';
import 'user_info_data.dart';

@JsonSerializable()
class LinkmomResultsData {
  static const String Key_matching_id = 'matching_id';
  static const String Key_bywho = 'bywho';
  static const String Key_matching_status = 'matching_status';
  static const String Key_matching_status_date = 'matching_status_date';
  static const String Key_care_status = 'care_status';
  static const String Key_care_status_date = 'care_status_date';
  static const String Key_paid_deadline = 'paid_deadline';
  static const String Key_userinfo = 'userinfo';
  static const String Key_authinfo = 'authinfo';
  static const String Key_childinfo = 'childinfo';
  static const String Key_care_scheduleinfo = 'care_scheduleinfo';
  static const String Key_chatting = 'chatting';
  static const String Key_order_id = 'order_id';
  static const String Key_contract_id = 'contract_id';
  static const String Key_rematching = 'rematching';

  LinkmomResultsData({
    this.matching_id = 0,
    this.bywho,
    this.matching_status = 0,
    this.matching_status_date = '',
    this.care_status = 0,
    this.care_status_date = '',
    this.paid_deadline = '',
    this.userinfo,
    this.authinfo,
    this.childinfo,
    this.care_scheduleinfo,
    this.chatting,
    this.order_id = '',
    this.contract_id = 0,
    this.rematching = false,
  });

  @JsonKey(name: Key_matching_id)
  final int matching_id;
  @JsonKey(name: Key_bywho)
  final ByWhoData? bywho;
  @JsonKey(name: Key_matching_status)
  int matching_status;
  @JsonKey(name: Key_matching_status_date)
  final String matching_status_date;
  @JsonKey(name: Key_care_status)
  final int care_status;
  @JsonKey(name: Key_care_status_date)
  final String care_status_date;
  @JsonKey(name: Key_paid_deadline)
  String paid_deadline;
  @JsonKey(name: Key_userinfo)
  final UserInfoData? userinfo;
  @JsonKey(name: Key_authinfo)
  final AuthInfoData? authinfo;
  @JsonKey(name: Key_childinfo)
  final CareChildInfoData? childinfo;
  @JsonKey(name: Key_care_scheduleinfo)
  final CareScheduleInfoData? care_scheduleinfo;
  @JsonKey(name: Key_chatting)
  final ChattingData? chatting;
  @JsonKey(name: Key_order_id)
  final String order_id;
  @JsonKey(name: Key_contract_id)
  final int contract_id;
  @JsonKey(name: Key_rematching)
  bool rematching;

  factory LinkmomResultsData.fromJson(Map<String, dynamic> json) {
    ByWhoData bywho = ByWhoData();
    UserInfoData userinfo = UserInfoData();
    AuthInfoData authinfo = AuthInfoData();
    CareChildInfoData childinfo = CareChildInfoData();
    CareScheduleInfoData careScheduleinfo = CareScheduleInfoData(caredate: []);
    ChattingData chatting = ChattingData();
    try {
      bywho = ByWhoData.fromJson(json[Key_bywho]);
      userinfo = UserInfoData.fromJson(json[Key_userinfo]);
      authinfo = AuthInfoData.fromJson(json[Key_userinfo]);
      childinfo = CareChildInfoData.fromJson(json[Key_childinfo]);
      careScheduleinfo = CareScheduleInfoData.fromJson(json[Key_care_scheduleinfo]);
      chatting = ChattingData.fromJson(json[Key_chatting]);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return LinkmomResultsData(
      matching_id: json[Key_matching_id] as int,
      bywho: bywho,
      matching_status: json[Key_matching_status] as int,
      matching_status_date: json[Key_matching_status_date] ?? '',
      care_status: json[Key_care_status] as int,
      care_status_date: json[Key_care_status_date] ?? '',
      paid_deadline: json[Key_paid_deadline] ?? '',
      userinfo: userinfo,
      authinfo: authinfo,
      childinfo: childinfo,
      care_scheduleinfo: careScheduleinfo,
      chatting: chatting,
      order_id: json[Key_order_id] ?? '',
      contract_id: json[Key_contract_id] ?? 0,
      rematching: json[Key_rematching] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_matching_id] = this.matching_id;
    data[Key_bywho] = this.bywho;
    data[Key_matching_status] = this.matching_status;
    data[Key_matching_status_date] = this.matching_status_date;
    data[Key_care_status] = this.care_status;
    data[Key_care_status_date] = this.care_status_date;
    data[Key_paid_deadline] = this.paid_deadline;
    data[Key_userinfo] = this.userinfo;
    data[Key_authinfo] = this.authinfo;
    data[Key_childinfo] = this.childinfo;
    data[Key_care_scheduleinfo] = this.care_scheduleinfo;
    data[Key_chatting] = this.chatting;
    data[Key_contract_id] = this.contract_id;
    data[Key_order_id] = this.order_id;
    data[Key_rematching] = this.rematching;
    return data;
  }

  @override
  String toString() {
    return 'LinkmomResultsData{matching_id: $matching_id, bywho: $bywho, matching_status: $matching_status, matching_status_date: $matching_status_date, care_status: $care_status, care_status_date: $care_status_date, paid_deadline: $paid_deadline, userinfo: $userinfo, authinfo: $authinfo, childinfo: $childinfo, care_scheduleinfo: $care_scheduleinfo, chatting: $chatting, contract_id: $contract_id, order_id: $order_id, rematching: $rematching}';
  }
}

class ChattingData {
  static const String Key_chatroom_id = 'chatroom_id';
  static const String Key_chattingmessage_cnt = 'chattingmessage_cnt';
  
  @JsonKey(name: Key_chatroom_id)
  int chatroomId;
  @JsonKey(name: Key_chattingmessage_cnt)
  int chattingmessageCnt;

  ChattingData({this.chatroomId = 0, this.chattingmessageCnt = 0});

  factory ChattingData.fromJson(Map<String, dynamic> json) {
    return ChattingData(
      chatroomId: json[Key_chatroom_id],
      chattingmessageCnt: json[Key_chattingmessage_cnt],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_chatroom_id] = this.chatroomId;
    data[Key_chattingmessage_cnt] = this.chattingmessageCnt;
    return data;
  }

  @override
  String toString() {
    return 'ChattingData {chatroom_id: $chatroomId, chattingmessage_cnt: $chattingmessageCnt}';
  }
}
