import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/utils/string_util.dart';

import 'review_info_data.dart';

///나의정보
class UserInfoData {
  static const String Key_user_id = 'user_id';
  static const String Key_first_name = 'first_name';
  static const String Key_region_2depth = 'region_2depth';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_4depth = 'region_4depth';
  static const String Key_is_affiliate = 'is_affiliate';
  static const String Key_profileimg = 'profileimg';
  static const String Key_is_follower = 'is_follower';
  static const String Key_reviewinfo = 'reviewinfo';
  static const String Key_id = 'id';

  UserInfoData({
    this.user_id = 0,
    this.first_name = '',
    this.region_2depth = '',
    this.region_3depth = '',
    this.region_4depth = '',
    this.is_affiliate = false,
    this.profileimg = '',
    this.is_follower = false,
    this.reviewinfo,
    this.id = 0,
  });

  @JsonKey(name: Key_user_id)
  final int user_id;
  @JsonKey(name: Key_first_name)
  final String first_name;
  @JsonKey(name: Key_region_2depth)
  final String region_2depth;
  @JsonKey(name: Key_region_3depth)
  final String region_3depth;
  @JsonKey(name: Key_region_4depth)
  final String region_4depth;

  ///제휴 인증 여부 true, false
  @JsonKey(name: Key_is_affiliate)
  final bool is_affiliate;
  @JsonKey(name: Key_profileimg)
  final String profileimg;
  @JsonKey(name: Key_is_follower)
  bool is_follower;
  @JsonKey(name: Key_reviewinfo)
  final ReViewInfoData? reviewinfo;

  /// Use matching list, view
  @JsonKey(name: Key_id)
  final int id;

  factory UserInfoData.fromJson(Map<String, dynamic> json) {
    ReViewInfoData reViewInfoData = ReViewInfoData.fromJson(json[Key_reviewinfo]);
    return UserInfoData(
      id: json[Key_id] ?? 0,
      user_id: json[Key_user_id] as int,
      first_name: json[Key_first_name] ?? '',
      region_2depth: json[Key_region_2depth] ?? '',
      region_3depth: json[Key_region_3depth] ?? '',
      region_4depth: json[Key_region_4depth] ?? '',
      is_affiliate: json[Key_is_affiliate] ?? false,
      profileimg: json[Key_profileimg] ?? '',
      is_follower: json[Key_is_follower] ?? false,
      reviewinfo: reViewInfoData,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_user_id] = this.user_id;
    data[Key_id] = this.id;
    data[Key_first_name] = this.first_name;
    data[Key_region_2depth] = this.region_2depth;
    data[Key_region_3depth] = this.region_3depth;
    data[Key_region_4depth] = this.region_4depth;
    data[Key_is_affiliate] = this.is_affiliate;
    data[Key_profileimg] = this.profileimg;
    data[Key_is_follower] = this.is_follower;
    data[Key_reviewinfo] = this.reviewinfo;
    return data;
  }

  String getAddress() {
    return StringUtils.getAddressParser(region_3depth, region_4depth);
  }

  @override
  String toString() {
    return 'UserInfoData{user_id: $user_id, first_name: $first_name, region_2depth: $region_2depth, region_3depth: $region_3depth, region_4depth: $region_4depth, is_affiliate: $is_affiliate, profileimg: $profileimg, is_follower: $is_follower, reviewinfo: $reviewinfo, id: $id}';
  }
}
