import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryClaimData {
  static const String Key_carediary = 'carediary';
  static const String Key_gubun = 'gubun';
  static const String Key_type = 'type';
  static const String Key_content1 = 'content1';
  static const String Key_content2 = 'content2';
  static const String Key_carediary_claim_images1 = 'carediary_claim_images1';
  static const String Key_carediary_claim_images2 = 'carediary_claim_images2';
  static const String Key_createdate_after24 = 'createdate_after24';
  static const String Key_id = 'id';
  static const String Key_answer = 'answer';
  static const String Key_linkmom_name = 'linkmom_name';
  static const String Key_momdady_name = 'momdady_name';
  static const String Key_schedule_id = 'schedule';

  @JsonKey(name: Key_carediary)
  int carediary;
  @JsonKey(name: Key_gubun)
  int gubun;
  @JsonKey(name: Key_type)
  int type;
  @JsonKey(name: Key_content1)
  String content1;
  @JsonKey(name: Key_content2)
  String content2;
  @JsonKey(name: Key_carediary_claim_images1)
  List<CarediaryClaimImages>? carediaryClaimImages1;
  @JsonKey(name: Key_carediary_claim_images2)
  List<CarediaryClaimImages>? carediaryClaimImages2;
  @JsonKey(name: Key_createdate_after24)
  String createdateAfter24;
  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_answer)
  String answer;
  @JsonKey(name: Key_linkmom_name)
  String linkmomName;
  @JsonKey(name: Key_momdady_name)
  String momdadyName;
  @JsonKey(name: Key_schedule_id)
  int schedule;

  CareDiaryClaimData({
    this.carediary = 0,
    this.gubun = 0,
    this.type = 0,
    this.content1 = '',
    this.content2 = '',
    this.carediaryClaimImages1,
    this.carediaryClaimImages2,
    this.createdateAfter24 = '',
    this.id = 0,
    this.answer = '',
    this.linkmomName = '',
    this.momdadyName = '',
    this.schedule = 0,
  });

  factory CareDiaryClaimData.fromJson(Map<String, dynamic> json) {
    return CareDiaryClaimData(
      carediary: json[Key_carediary] as int,
      gubun: json[Key_gubun] as int,
      type: json[Key_type] as int,
      content1: json[Key_content1] ?? '',
      content2: json[Key_content2] ?? '',
      carediaryClaimImages1: json[Key_carediary_claim_images1] != null && json[Key_carediary_claim_images1].isNotEmpty ? json[Key_carediary_claim_images1].map<CarediaryClaimImages>((e) => CarediaryClaimImages.fromJson(e)).toList() : [],
      carediaryClaimImages2: json[Key_carediary_claim_images2] != null && json[Key_carediary_claim_images2].isNotEmpty ? json[Key_carediary_claim_images2].map<CarediaryClaimImages>((e) => CarediaryClaimImages.fromJson(e)).toList() : [],
      createdateAfter24: json[Key_createdate_after24] ?? '',
      id: json[Key_id] as int,
      answer: json[Key_answer] ?? '',
      linkmomName: json[Key_linkmom_name] ?? '',
      momdadyName: json[Key_momdady_name] ?? '',
      schedule: json[Key_schedule_id] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_carediary] = this.carediary;
    data[Key_gubun] = this.gubun;
    data[Key_type] = this.type;
    data[Key_content1] = this.content1;
    data[Key_content2] = this.content2;
    if (this.carediaryClaimImages1 != null) data[Key_carediary_claim_images1] = this.carediaryClaimImages1!.map((e) => e.toJson()).toList();
    if (this.carediaryClaimImages2 != null) data[Key_carediary_claim_images2] = this.carediaryClaimImages2!.map((e) => e.toJson()).toList();
    data[Key_createdate_after24] = this.createdateAfter24;
    data[Key_id] = this.id;
    data[Key_answer] = this.answer;
    data[Key_linkmom_name] = this.linkmomName;
    data[Key_momdady_name] = this.momdadyName;
    data[Key_schedule_id] = this.schedule;
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryClaimData{carediary: $carediary, gubun: $gubun, type: $type, content1: $content1, content2: $content2, carediaryClaimImages1: $carediaryClaimImages1, carediaryClaimImages2: $carediaryClaimImages2,  answer: $answer, linkmomName: $linkmomName, momdadyName: $momdadyName, scheduleId: $schedule}';
  }
}

@JsonSerializable()
class CarediaryClaimImages {
  static const String Key_image = 'image';
  static const String Key_image_thumbnail = 'image_thumbnail';

  @JsonKey(name: Key_image)
  String image;
  @JsonKey(name: Key_image_thumbnail)
  String imageThumbnail;

  CarediaryClaimImages({this.image = '', this.imageThumbnail = ''});

  factory CarediaryClaimImages.fromJson(Map<String, dynamic> json) {
    return CarediaryClaimImages(
      image: json[Key_image] ?? '',
      imageThumbnail: json[Key_image_thumbnail] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_image] = this.image;
    data[Key_image_thumbnail] = this.imageThumbnail;
    return data;
  }

  @override
  String toString() {
    return 'CarediaryClaimImages{image:$image, imageThumbnail:$imageThumbnail}';
  }
}
