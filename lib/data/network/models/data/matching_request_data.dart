import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/main.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : matching_deny_request.dart - 매칭/거절 4개, 계약서/서명/결제 3개 요청 데이터
/// since        : 2021/08/03 / update:
@JsonSerializable()
class MatchingRequestData {
  ///돌봄진행 상황 (매칭된 내용)
  static const String Key_receiver = 'receiver';

  ///돌봄신청서ID (booking_id)
  static const String Key_bookingcareservices = 'bookingcareservices';

  ///매칭ID (matching_id) - 있는 경우만, 없으면 Null
  static const String Key_carematching = 'carematching';

  ///채팅방번호 (chatroom_id)
  static const String Key_chattingroom = 'chattingroom';

  MatchingRequestData({
    this.receiver = 0,
    this.bookingcareservices = 0,
    this.carematching = 0,
    this.chattingroom = 0,
  });

  ///돌봄진행 상황 (매칭된 내용)
  @JsonKey(name: Key_receiver)
  final int receiver;

  ///돌봄신청서ID (booking_id)
  @JsonKey(name: Key_bookingcareservices)
  final int bookingcareservices;

  ///매칭ID (matching_id) - 있는 경우만, 없으면 Null
  @JsonKey(name: Key_carematching)
  final int carematching;

  ///채팅방번호 (chatroom_id)
  @JsonKey(name: Key_chattingroom)
  final int chattingroom;

  factory MatchingRequestData.fromJson(Map<String, dynamic> json) {
    if (json[Key_chattingroom] == null) {
      return MatchingRequestData(
        receiver: json[Key_receiver] as int,
        bookingcareservices: json[Key_bookingcareservices] as int,
        carematching: json[Key_carematching] as int,
      );
    } else {
      return MatchingRequestData(
        receiver: json[Key_receiver] as int,
        bookingcareservices: json[Key_bookingcareservices] as int,
        carematching: json[Key_carematching] as int,
        chattingroom: json[Key_chattingroom] as int,
      );
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (receiver != 0) data[Key_receiver] = this.receiver;
    if (bookingcareservices != 0) data[Key_bookingcareservices] = this.bookingcareservices;
    if (carematching != 0) data[Key_carematching] = this.carematching;
    if (chattingroom != 0) data[Key_chattingroom] = this.chattingroom;
    return data;
  }

  Future<FormData> toFormData() async {
    FormData formData = FormData();
    if (receiver != 0) {
      formData.fields.add(MapEntry(Key_receiver, receiver.toString()));
    }
    if (bookingcareservices != 0) {
      formData.fields.add(MapEntry(Key_bookingcareservices, bookingcareservices.toString()));
    }
    if (carematching != 0) {
      formData.fields.add(MapEntry(Key_carematching, carematching.toString()));
    }
    if (chattingroom != 0) {
      formData.fields.add(MapEntry(Key_chattingroom, chattingroom.toString()));
    }
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'MatchingRequestData{receiver: $receiver, bookingcareservices: $bookingcareservices, carematching: $carematching, chattingroom: $chattingroom}';
  }
}
