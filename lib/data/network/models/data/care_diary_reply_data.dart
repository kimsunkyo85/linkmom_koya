import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryReplyData {
  static const String Key_carediary = 'carediary';
  static const String Key_writer_gubun = 'writer_gubun';
  static const String Key_reply = 'reply';
  static const String Key_parent = 'parent';

  @JsonKey(name: Key_carediary)
  int carediary;
  @JsonKey(name: Key_writer_gubun)
  int writerGubun;
  @JsonKey(name: Key_reply)
  String reply;
  @JsonKey(name: Key_parent)
  int parent;

  CareDiaryReplyData({this.carediary = 0, this.writerGubun = 0, this.reply = '', this.parent = 0});

  factory CareDiaryReplyData.fromJson(Map<String, dynamic> json) {
    return CareDiaryReplyData(
      carediary: json[Key_carediary],
      writerGubun: json[Key_writer_gubun],
      reply: json[Key_reply],
      parent: json[Key_parent],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_carediary] = this.carediary;
    data[Key_writer_gubun] = this.writerGubun;
    data[Key_reply] = this.reply;
    data[Key_parent] = this.parent;
    return data;
  }
}
