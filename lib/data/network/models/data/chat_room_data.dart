import 'package:json_annotation/json_annotation.dart';

import 'care_child_info_data.dart';

@JsonSerializable()
class ChatRoomData {
  static const String Key_chatroom_id = 'chatroom_id';
  static const String Key_chattingtype = 'chattingtype';
  static const String Key_talker_info = 'talker_info';
  static const String Key_bookingcareservices = 'bookingcareservices';
  static const String Key_bookingcareservices_info = 'bookingcareservices_info';
  static const String Key_carematching = 'carematching';
  static const String Key_receiver = 'receiver';
  static const String Key_createdate = 'createdate';

  ChatRoomData({
    this.chatroom_id = 0,
    this.chattingtype = 0,
    this.talker_info,
    this.bookingcareservices = 0,
    this.bookingcareservices_info,
    this.carematching,
    this.receiver = 0,
    this.createdate = '',
  });

  @JsonKey(name: Key_chatroom_id)
  final int chatroom_id;
  @JsonKey(name: Key_chattingtype)
  final int chattingtype;
  @JsonKey(name: Key_talker_info)
  final ChatMessageInfoData? talker_info;
  @JsonKey(name: Key_bookingcareservices)
  final int bookingcareservices;
  @JsonKey(name: Key_bookingcareservices_info)
  final ChatBookingCareServicesInfoData? bookingcareservices_info;
  @JsonKey(name: Key_carematching)
  final ChatCareMatchingData? carematching;
  @JsonKey(name: Key_receiver)
  final int receiver;
  @JsonKey(name: Key_createdate)
  final String createdate;

  factory ChatRoomData.fromJson(Map<String, dynamic> json) {
    ChatMessageInfoData? talkerInfo = json[Key_talker_info] == null ? ChatMessageInfoData() : ChatMessageInfoData.fromJson(json[Key_talker_info]);
    ChatBookingCareServicesInfoData? bookingcareservicesInfo = json[Key_bookingcareservices_info] == null ? ChatBookingCareServicesInfoData() : ChatBookingCareServicesInfoData.fromJson(json[Key_bookingcareservices_info]);
    ChatCareMatchingData? carematching = json[Key_carematching] == null ? ChatCareMatchingData() : ChatCareMatchingData.fromJson(json[Key_carematching]);
    return ChatRoomData(
      chatroom_id: json[Key_chatroom_id] as int,
      chattingtype: json[Key_chattingtype] as int,
      talker_info: talkerInfo,
      bookingcareservices: json[Key_bookingcareservices] as int,
      bookingcareservices_info: bookingcareservicesInfo,
      carematching: carematching,
      receiver: json[Key_receiver] as int,
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_chatroom_id] = this.chatroom_id;
    data[Key_chattingtype] = this.chattingtype;
    data[Key_talker_info] = this.talker_info;
    data[Key_bookingcareservices] = this.bookingcareservices;
    data[Key_bookingcareservices_info] = this.bookingcareservices_info;
    data[Key_carematching] = this.carematching;
    data[Key_receiver] = this.receiver;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'ChatRoomData{chatroom_id: $chatroom_id, chattingtype: $chattingtype, talker_info: $talker_info, bookingcareservices: $bookingcareservices, bookingcareservices_info: $bookingcareservices_info, carematching: $carematching, receiver: $receiver, createdate: $createdate}';
  }
}

///매칭 정보
class ChatBookingCareServicesInfoData {
  static const String Key_careinfo = 'careinfo';
  static const String Key_userinfo = 'userinfo';
  static const String Key_childinfo = 'childinfo';

  ChatBookingCareServicesInfoData({
    this.careinfo,
    this.userinfo,
    this.childinfo,
  });

  @JsonKey(name: Key_careinfo)
  final ChatCareInfoData? careinfo;

  // final CareScheduleInfoData careinfo;
  @JsonKey(name: Key_userinfo)
  final ChatUserInfoData? userinfo;
  @JsonKey(name: Key_childinfo)
  final CareChildInfoData? childinfo;

  factory ChatBookingCareServicesInfoData.fromJson(Map<String, dynamic> json) {
    ChatCareInfoData? careinfo = json[Key_careinfo] == null ? null : ChatCareInfoData.fromJson(json[Key_careinfo]);
    // CareScheduleInfoData careinfo = json[Key_careinfo] == null ? null : CareScheduleInfoData.fromJson(json[Key_careinfo]);
    ChatUserInfoData? userinfo = json[Key_userinfo] == null ? null : ChatUserInfoData.fromJson(json[Key_userinfo]);
    CareChildInfoData? childinfo = json[Key_childinfo] == null ? null : CareChildInfoData.fromJson(json[Key_childinfo]);
    return ChatBookingCareServicesInfoData(
      careinfo: careinfo,
      userinfo: userinfo,
      childinfo: childinfo,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_careinfo] = this.careinfo;
    data[Key_userinfo] = this.userinfo;
    data[Key_childinfo] = this.childinfo;
    return data;
  }

  @override
  String toString() {
    return 'ChatBookingCareServicesInfoData{careinfo: $careinfo, userinfo: $userinfo, childinfo: $childinfo}';
  }
}

///케어 정보
class ChatCareInfoData {
  static const String Key_booking_id = 'booking_id';
  static const String Key_booking_status = 'booking_status';
  static const String Key_servicetype = 'servicetype';
  static const String Key_cost_sum = 'cost_sum';
  static const String Key_stime = 'stime';
  static const String Key_etime = 'etime';
  static const String Key_caredate = 'caredate';
  static const String Key_possible_area = 'possible_area';
  static const String Key_cost_perhour = 'cost_perhour';
  static const String Key_durationtime = 'durationtime';
  static const String Key_product_name = 'product_name';

  ChatCareInfoData.init() {
    booking_id = 0;
    booking_status = 0;
    servicetype = 0;
    cost_sum = 0;
    stime = '';
    etime = '';
    caredate = [];
    possible_area = 0;
    cost_perhour = 0;
    durationtime = 0;
    product_name = '';
  }

  ChatCareInfoData({
    this.booking_id = 0,
    this.booking_status = 0,
    this.servicetype = 0,
    this.cost_sum = 0,
    this.stime = '',
    this.etime = '',
    this.caredate,
    this.possible_area = 0,
    this.cost_perhour = 0,
    this.durationtime = 0,
    this.product_name = '',
  });

  @JsonKey(name: Key_booking_id)
  late int booking_id;
  @JsonKey(name: Key_booking_status)
  late int booking_status;
  @JsonKey(name: Key_servicetype)
  late int servicetype;
  @JsonKey(name: Key_cost_sum)
  late int cost_sum;
  @JsonKey(name: Key_stime)
  late String stime;
  @JsonKey(name: Key_etime)
  late String etime;
  @JsonKey(name: Key_caredate)
  late List<String>? caredate;
  @JsonKey(name: Key_possible_area)
  late int possible_area;

  ///시급
  @JsonKey(name: Key_cost_perhour)
  int? cost_perhour;

  ///돌봄 총 신청시간(분)
  @JsonKey(name: Key_durationtime)
  int? durationtime;

  ///상품명
  @JsonKey(name: Key_product_name)
  String? product_name;

  factory ChatCareInfoData.fromJson(Map<String, dynamic> json) {
    List<String> caredate = json[Key_caredate] == null ? [] : List<String>.from(json[Key_caredate]);
    return ChatCareInfoData(
      booking_id: json[Key_booking_id] as int,
      booking_status: json[Key_booking_status] as int,
      servicetype: json[Key_servicetype] as int,
      cost_sum: json[Key_cost_sum] as int,
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      caredate: caredate,
      possible_area: json[Key_possible_area] as int,
      cost_perhour: json[Key_cost_perhour] as int,
      durationtime: json[Key_durationtime] as int,
      product_name: json[Key_product_name] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_booking_id] = this.booking_id;
    data[Key_booking_status] = this.booking_status;
    data[Key_servicetype] = this.servicetype;
    data[Key_cost_sum] = this.cost_sum;
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_caredate] = this.caredate;
    data[Key_possible_area] = this.possible_area;
    data[Key_cost_perhour] = this.cost_perhour;
    data[Key_durationtime] = this.durationtime;
    data[Key_product_name] = this.product_name;
    return data;
  }

  @override
  String toString() {
    return 'ChatCareInfoData{booking_id: $booking_id, booking_status: $booking_status, servicetype: $servicetype, cost_sum: $cost_sum, stime: $stime, etime: $etime, caredate: $caredate, possible_area: $possible_area, cost_perhour: $cost_perhour, durationtime: $durationtime, product_name: $product_name}';
  }
}

///사용자 정보
class ChatUserInfoData {
  static const String Key_id = 'id';
  static const String Key_first_name = 'first_name';
  static const String Key_profileimg = 'profileimg';
  static const String Key_region_2depth = 'region_2depth';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_4depth = 'region_4depth';

  ChatUserInfoData({
    this.id = 0,
    this.first_name = '',
    this.profileimg = '',
    this.region_2depth = '',
    this.region_3depth = '',
    this.region_4depth = '',
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_first_name)
  final String first_name;
  @JsonKey(name: Key_profileimg)
  final String profileimg;
  @JsonKey(name: Key_region_2depth)
  final String region_2depth;
  @JsonKey(name: Key_region_3depth)
  final String region_3depth;
  @JsonKey(name: Key_region_4depth)
  final String region_4depth;

  factory ChatUserInfoData.fromJson(Map<String, dynamic> json) {
    return ChatUserInfoData(
      id: json[Key_id] as int,
      first_name: json[Key_first_name] ?? '',
      profileimg: json[Key_profileimg] ?? '',
      region_2depth: json[Key_region_2depth] ?? '',
      region_3depth: json[Key_region_3depth] ?? '',
      region_4depth: json[Key_region_4depth] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_first_name] = this.first_name;
    data[Key_profileimg] = this.profileimg;
    data[Key_region_2depth] = this.region_2depth;
    data[Key_region_3depth] = this.region_3depth;
    data[Key_region_4depth] = this.region_4depth;
    return data;
  }

  @override
  String toString() {
    return 'ChatUserInfoData{id: $id, first_name: $first_name, profileimg: $profileimg, region_2depth: $region_2depth, region_3depth: $region_3depth, region_4depth: $region_4depth}';
  }
}

///매칭 데이터
class ChatCareMatchingData {
  static const String Key_matching_id = 'matching_id';
  static const String Key_matching_status = 'matching_status';
  static const String Key_care_status = 'care_status';
  static const String Key_paid_deadline = 'paid_deadline';
  static const String Key_contract_id = 'contract_id';
  static const String Key_order_id = 'order_id';
  static const String Key_momdady = 'momdady';
  static const String Key_linkmom = 'linkmom';

  ChatCareMatchingData({
    this.matching_id = 0,
    this.matching_status = 0,
    this.care_status = 0,
    this.paid_deadline = '',
    this.contract_id = 0,
    this.order_id = '',
    this.momdady = 0,
    this.linkmom = 0,
  });

  @JsonKey(name: Key_matching_id)
  final int matching_id;
  @JsonKey(name: Key_matching_status)
  final int matching_status;
  @JsonKey(name: Key_care_status)
  final int care_status;
  @JsonKey(name: Key_paid_deadline)
  final String paid_deadline;
  @JsonKey(name: Key_contract_id)
  int contract_id;
  @JsonKey(name: Key_order_id)
  String order_id;
  @JsonKey(name: Key_momdady)
  final int momdady;
  @JsonKey(name: Key_linkmom)
  final int linkmom;

  factory ChatCareMatchingData.fromJson(Map<String, dynamic> json) {
    return ChatCareMatchingData(
      matching_id: json[Key_matching_id] as int,
      matching_status: json[Key_matching_status] as int,
      care_status: json[Key_care_status] as int,
      paid_deadline: json[Key_paid_deadline] ?? '',
      contract_id: json[Key_contract_id] as int,
      order_id: json[Key_order_id] ?? '',
      momdady: json[Key_momdady] as int,
      linkmom: json[Key_linkmom] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_matching_id] = this.matching_id;
    data[Key_matching_status] = this.matching_status;
    data[Key_care_status] = this.care_status;
    data[Key_paid_deadline] = this.paid_deadline;
    data[Key_order_id] = this.order_id;
    data[Key_momdady] = this.momdady;
    data[Key_linkmom] = this.linkmom;
    return data;
  }

  @override
  String toString() {
    return 'ChatCareMatchingData{matching_id: $matching_id, matching_status: $matching_status, care_status: $care_status, paid_deadline: $paid_deadline, contract_id: $contract_id, order_id: $order_id, momdady: $momdady, linkmom: $linkmom}';
  }
}

///보낸/받은 메시지 정보(이름, 프로필 이미지) sender_info, receiver_info
class ChatMessageInfoData {
  static const String Key_id = 'id';
  static const String Key_first_name = 'first_name';
  static const String Key_profileimg = 'profileimg';
  static const String Key_talker_matching_mode = 'talker_matching_mode';
  static const String Key_is_user_momdady = 'is_user_momdady';
  static const String Key_is_user_linkmom = 'is_user_linkmom';
  static const String Key_is_active = 'is_active';
  static const String Key_is_block = 'is_block';

  ChatMessageInfoData({
    this.id = 0,
    this.first_name = '',
    this.profileimg = '',
    this.talker_matching_mode = '',
    this.is_user_momdady = false,
    this.is_user_linkmom = false,
    this.is_active = true,
    this.is_block = false,
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_first_name)
  final String first_name;
  @JsonKey(name: Key_profileimg)
  final String profileimg;
  @JsonKey(name: Key_talker_matching_mode)
  final String talker_matching_mode;
  @JsonKey(name: Key_is_user_momdady)
  final bool is_user_momdady;
  @JsonKey(name: Key_is_user_linkmom)
  final bool is_user_linkmom;
  @JsonKey(name: Key_is_active)
  final bool is_active;
  @JsonKey(name: Key_is_block)
  bool is_block;

  factory ChatMessageInfoData.fromJson(Map<String, dynamic> json) {
    return ChatMessageInfoData(
      id: json[Key_id] as int,
      first_name: json[Key_first_name] ?? '',
      profileimg: json[Key_profileimg] ?? '',
      talker_matching_mode: json[Key_talker_matching_mode] ?? '',
      is_user_momdady: json[Key_is_user_momdady] as bool,
      is_user_linkmom: json[Key_is_user_linkmom] as bool,
      is_active: json[Key_is_active] as bool,
      is_block: json[Key_is_block] as bool,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_first_name] = this.first_name;
    data[Key_profileimg] = this.profileimg;
    data[Key_talker_matching_mode] = this.talker_matching_mode;
    data[Key_is_user_momdady] = this.is_user_momdady;
    data[Key_is_user_linkmom] = this.is_user_linkmom;
    data[Key_is_active] = this.is_active;
    data[Key_is_block] = this.is_block;
    return data;
  }

  @override
  String toString() {
    return 'ChatMessageInfoData{id: $id, first_name: $first_name, profileimg: $profileimg, talker_matching_mode: $talker_matching_mode, is_user_momdady: $is_user_momdady, is_user_linkmom: $is_user_linkmom, is_active: $is_active, is_block: $is_block}';
  }
}
