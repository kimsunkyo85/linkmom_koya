import 'package:json_annotation/json_annotation.dart';

class ByWhoData {
  static const String Key_is_byme = 'is_byme';
  static const String Key_is_tome = 'is_tome';

  ByWhoData({
    this.is_byme = false,
    this.is_tome = false,
  });

  @JsonKey(name: Key_is_byme)
  final bool is_byme;
  @JsonKey(name: Key_is_tome)
  final bool is_tome;

  factory ByWhoData.fromJson(Map<String, dynamic> json) {
    return ByWhoData(
      is_byme: json[Key_is_byme] as bool,
      is_tome: json[Key_is_tome] as bool,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_is_byme] = this.is_byme;
    data[Key_is_tome] = this.is_tome;
    return data;
  }

  @override
  String toString() {
    return 'ByWhoData{is_byme: $is_byme, is_tome: $is_tome}';
  }
}
