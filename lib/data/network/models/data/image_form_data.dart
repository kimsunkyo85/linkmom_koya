import 'dart:io';

class ImageFormData {
  String key;
  File file;
  ImageFormData(this.key, this.file);
}
