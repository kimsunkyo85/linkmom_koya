import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/emoji/emoji.dart';

import '../../../../main.dart';
import '../momdady_mycares_list_response.dart';

@JsonSerializable()
class ChatSendData {
  static const String Key_id = 'id';
  static const String Key_chattingroom = 'chattingroom';
  static const String Key_sender = 'sender';
  static const String Key_receiver = 'receiver';
  static const String Key_msgtype = 'msgtype';
  static const String Key_title = 'title';
  static const String Key_msg = 'msg';
  static const String Key_image = 'image';
  static const String Key_image_thumbnail = 'image_thumbnail';
  static const String Key_msgdata = 'msgdata';
  static const String Key_sendtime = 'sendtime';
  static const String Key_sendtimestamp = 'sendtimestamp';
  static const String Key_is_receive = 'is_receive';
  static const String Key_pagename = 'pagename';
  static const String Key_chattingtype = 'chattingtype';
  static const String Key_is_emoji = 'is_emoji';

  ChatSendData({
    this.id = 0,
    this.chattingroom = 0,
    this.sender = 0,
    this.receiver = 0,
    this.msgtype,
    this.title = '',
    this.msg = '',
    this.msgdata,
    this.image = '',
    this.image_thumbnail = '',
    this.sendtime = '',
    this.sendtimestamp = 0,
    this.is_receive = false,
    this.pagename = 999,
    this.chattingtype = 0,
    this.is_emoji = false,
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_chattingroom)
  int chattingroom;
  @JsonKey(name: Key_sender)
  final int sender;
  @JsonKey(name: Key_receiver)
  final int receiver;
  @JsonKey(name: Key_msgtype)
  final ChatMsgType? msgtype;
  @JsonKey(name: Key_title)
  String title;
  @JsonKey(name: Key_msg)
  String msg;
  @JsonKey(name: Key_image)
  String image;
  @JsonKey(name: Key_image_thumbnail)
  String image_thumbnail;
  @JsonKey(name: Key_msgdata)
  dynamic msgdata;
  @JsonKey(name: Key_sendtime)
  String sendtime;
  @JsonKey(name: Key_sendtimestamp)
  int sendtimestamp;
  @JsonKey(name: Key_is_receive)
  bool is_receive;
  @JsonKey(name: Key_pagename)
  int pagename;
  @JsonKey(name: Key_chattingtype)
  int chattingtype;
  @JsonKey(name: Key_is_emoji)
  final bool is_emoji;

  factory ChatSendData.fromJson(Map<String, dynamic> json, {bool isMsgData = true}) {
    ChatMsgType msgtype;
    int id = 0;
    if (json[Key_msgtype].runtimeType == int) {
      msgtype = ChatMsgType.values.where((element) => element.value == json[Key_msgtype]).last;
    } else {
      //firebase수신후, mqtt로 보내고, 채팅 화면으로 보낼때 오류수정 추가
      try {
        String type = json[Key_msgtype] ?? '0';
        msgtype = ChatMsgType.values.where((value) => value.value.toString() == type || value.valueName == type).last;
      } catch (e) {
        msgtype = ChatMsgType.text;
        log.e({'EXCEPTION': e});
      }
    }
    if (json[Key_id] is String) {
      id = int.tryParse(json[Key_id]) ?? 0;
    } else {
      id = json[Key_id] as int;
    }
    var msgdata = isMsgData ? getMsgData(msgtype, json) : null;
    if (msgdata is ContractData) {
      //2022/05/17 돌봄신청서 수정시 작업 처리, 일반 msgtype으로 수신받아 rematching이 True이면 강제로 매칭정보(시스템 메시지로) 돌려준다.
      //그렇지 않을 경우 업데이트를 안한 사용자의 경우 화면이 제대로 표시 되지 않는 문제 발생.
      if (msgdata.rematching) {
        msgtype = ChatMsgType.matching;
      }
    }

    bool isEmoji = json[Key_is_emoji] ?? false;
    String msg = json[Key_msg] ?? '';
    try {
      if (isEmoji) {
        msg = EmojiParser().emojify(msg);
      }
    } catch (e) {
      log.e({'EXCEPTION': e});
    }
    return ChatSendData(
      id: id,
      chattingroom: json[Key_chattingroom] as int,
      sender: json[Key_sender] as int,
      receiver: json[Key_receiver] as int,
      msgtype: msgtype,
      title: json[Key_title] ?? '',
      msg: msg,
      image: json[Key_image] ?? '',
      image_thumbnail: json[Key_image_thumbnail] ?? '',
      msgdata: msgdata == null ? {} : msgdata,
      sendtime: json[Key_sendtime] ?? '',
      sendtimestamp: json[Key_sendtimestamp] as int,
      is_receive: json[Key_is_receive] ?? false,
      pagename: json[Key_pagename] as int,
      chattingtype: json[Key_chattingtype] as int,
      is_emoji: isEmoji,
    );
  }

  static dynamic getMsgData(ChatMsgType msgtype, Map<String, dynamic> json) {
    var msgdata;
    if (json[Key_msgdata] == null) {
      return msgdata;
    }
    switch (msgtype) {
      case ChatMsgType.text:
      case ChatMsgType.image:
        msgdata = ChatInfoData.fromJson(json[Key_msgdata]);
        if (msgdata.rematching) {
          msgdata = ContractData.fromJson(json[Key_msgdata]);
        }
        break;
      case ChatMsgType.matching: //계약서 내용
        msgdata = ContractData.fromJson(json[Key_msgdata]);
        break;
      case ChatMsgType.careInfo: //돌봄신청서
        msgdata = MyCareResultsData.fromJson(json[Key_msgdata]);
        break;
      default:
    }
    return msgdata;
  }

  ///푸시로 응답시 처리
  factory ChatSendData.fromMap(String data) {
    Map<String, dynamic> _json = json.decode(data);
    ChatMsgType msgtype = getChatMsgTypeInt(_json[Key_msgtype]);
    var msgdata = getMsgData(msgtype, _json);
    if (msgdata is ContractData) {
      //2022/05/17 돌봄신청서 수정시 작업 처리, 일반 msgtype으로 수신받아 rematching이 True이면 강제로 매칭정보(시스템 메시지로) 돌려준다.
      //그렇지 않을 경우 업데이트를 안한 사용자의 경우 화면이 제대로 표시 되지 않는 문제 발생.
      if (msgdata.rematching) {
        msgtype = ChatMsgType.matching;
      }
    }
    bool isEmoji = _json[Key_is_emoji] ?? false;
    String msg = _json[Key_msg] ?? '';
    try {
      if (isEmoji) {
        msg = EmojiParser().emojify(msg);
      }
    } catch (e) {}

    return ChatSendData(
      id: _json[Key_id] as int,
      chattingroom: _json[Key_chattingroom] as int,
      sender: _json[Key_sender] as int,
      receiver: _json[Key_receiver] as int,
      msgtype: msgtype,
      title: _json[Key_title] ?? '',
      msg: msg,
      image: _json[Key_image] ?? '',
      image_thumbnail: _json[Key_image_thumbnail] ?? '',
      msgdata: msgdata == null ? {} : msgdata,
      sendtime: _json[Key_sendtime] ?? '',
      sendtimestamp: _json[Key_sendtimestamp] as int,
      is_receive: _json[Key_is_receive] ?? false,
      pagename: _json[Key_pagename] ?? '',
      chattingtype: _json[Key_chattingtype] as int,
      is_emoji: isEmoji,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_chattingroom] = this.chattingroom;
    data[Key_sender] = this.sender;
    data[Key_receiver] = this.receiver;
    data[Key_msgtype] = this.msgtype.toString();
    data[Key_msg] = this.msg;
    data[Key_image] = this.image;
    data[Key_msgdata] = this.msgdata;
    data[Key_sendtime] = this.sendtime;
    data[Key_sendtimestamp] = this.sendtimestamp;
    data[Key_is_receive] = this.is_receive;
    data[Key_pagename] = this.pagename;
    data[Key_chattingtype] = this.chattingtype;
    data[Key_is_emoji] = this.is_emoji;
    return data;
  }

  @override
  String toString() {
    return 'ChatSendData{id: $id, chattingroom: $chattingroom, sender: $sender, receiver: $receiver, msgtype: $msgtype, title: $title, msg: $msg, image: $image, image_thumbnail: $image_thumbnail, msgdata: $msgdata, sendtime: $sendtime, sendtimestamp: $sendtimestamp, is_receive: $is_receive, pagename: $pagename, chattingtype: $chattingtype, is_emoji: $is_emoji}';
  }
}

///계약서 정보 데이터
class ContractData {
  static const String Key_contract_id = 'contract_id';
  static const String Key_order_id = 'order_id';
  static const String Key_chattingroom = 'chattingroom';
  static const String Key_momdady = 'momdady';
  static const String Key_momdady_first_name = 'momdady_first_name';
  static const String Key_linkmom = 'linkmom';
  static const String Key_linkmom_first_name = 'linkmom_first_name';
  static const String Key_matching_status = 'matching_status';
  static const String Key_care_status = 'care_status';
  static const String Key_deadline = 'deadline';
  static const String Key_cancel_id = 'cancel_id';
  static const String Key_cancel_usertype = 'cancel_usertype';
  static const String Key_cancelreason = 'cancelreason';
  static const String Key_is_cancelexplain = 'is_cancelexplain';
  static const String Key_is_totalcancel = 'is_totalcancel';
  static const String Key_popup_code = 'popup_code';
  static const String Key_contract_status = 'contract_status';
  static const String Key_rematching = 'rematching';

  ContractData({
    this.contract_id = 0,
    this.order_id = '',
    this.chattingroom = 0,
    this.momdady = 0,
    this.momdady_first_name = '',
    this.linkmom = 0,
    this.linkmom_first_name = '',
    this.matching_status = 0,
    this.care_status = 0,
    this.deadline = '',
    this.cancel_id = 0,
    this.cancel_usertype = 0,
    this.cancelreason = 0,
    this.isLinkMom = false,
    this.is_cancelexplain = false,
    this.is_totalcancel = false,
    this.popup_code = 0,
    this.contract_status = 0,
    this.rematching = false,
  });

  ContractData.clone(ContractData item)
      : this(
          contract_id: item.contract_id,
          order_id: item.order_id,
          chattingroom: item.chattingroom,
          momdady: item.momdady,
          momdady_first_name: item.momdady_first_name,
          linkmom: item.linkmom,
          linkmom_first_name: item.linkmom_first_name,
          matching_status: item.matching_status,
          care_status: item.care_status,
          deadline: item.deadline,
          cancel_id: item.cancel_id,
          cancel_usertype: item.cancel_usertype,
          cancelreason: item.cancelreason,
          isLinkMom: item.isLinkMom,
          is_cancelexplain: item.is_cancelexplain,
          is_totalcancel: item.is_totalcancel,
          popup_code: item.popup_code,
          contract_status: item.contract_status,
        );

  @JsonKey(name: Key_contract_id)
  int contract_id;
  @JsonKey(name: Key_order_id)
  String order_id;
  @JsonKey(name: Key_chattingroom)
  final int chattingroom;
  @JsonKey(name: Key_momdady)
  final int momdady;
  @JsonKey(name: Key_momdady_first_name)
  String momdady_first_name;
  @JsonKey(name: Key_linkmom)
  final int linkmom;
  @JsonKey(name: Key_linkmom_first_name)
  String linkmom_first_name;
  @JsonKey(name: Key_matching_status)
  int matching_status;
  @JsonKey(name: Key_care_status)
  int care_status;
  @JsonKey(name: Key_deadline)
  String deadline;
  @JsonKey(name: Key_cancel_id)
  int? cancel_id;
  @JsonKey(name: Key_cancel_usertype)
  int? cancel_usertype;
  @JsonKey(name: Key_cancelreason)
  int? cancelreason;
  @JsonKey(name: Key_is_cancelexplain)
  bool? is_cancelexplain;
  @JsonKey(name: Key_is_totalcancel)
  bool? is_totalcancel;
  @JsonKey(name: Key_popup_code)
  int popup_code;
  @JsonKey(name: Key_contract_status)
  int contract_status;
  bool isLinkMom = false;
  @JsonKey(name: Key_rematching)
  bool rematching;

  factory ContractData.fromJson(Map<String, dynamic> json) {
    return ContractData(
      contract_id: json[Key_contract_id] ?? 0,
      order_id: json[Key_order_id] ?? '',
      chattingroom: json[Key_chattingroom] ?? 0,
      momdady: json[Key_momdady] ?? 0,
      momdady_first_name: json[Key_momdady_first_name] ?? '',
      linkmom: json[Key_linkmom] ?? 0,
      linkmom_first_name: json[Key_linkmom_first_name] ?? '',
      matching_status: json[Key_matching_status] ?? 0,
      care_status: json[Key_care_status] ?? 0,
      deadline: json[Key_deadline] ?? '',
      cancel_id: json[Key_cancel_id] ?? 0,
      cancel_usertype: json[Key_cancel_usertype] ?? 0,
      cancelreason: json[Key_cancelreason] ?? 0,
      is_cancelexplain: json[Key_is_cancelexplain] ?? false,
      is_totalcancel: json[Key_is_totalcancel] ?? false,
      popup_code: json[Key_popup_code] ?? 0,
      contract_status: json[Key_contract_status] ?? 0,
      rematching: json[Key_rematching] ?? false,
    );
  }

  // ///푸시로 응답시 처리
  // factory ContractData.fromMap(String data) {
  //   log.d('『GGUMBI』>>> fromMap 1: json: $data,   <<< ');
  //   Map<String, dynamic> _json = json.decode(data);
  //   ChatMsgType msgtype = getChatMsgTypeInt(_json[Key_msgtype]);
  //
  //   return ContractData(
  //     id: _json[Key_id] as int,
  //     chattingroom: _json[Key_chattingroom] as int,
  //     sender: _json[Key_sender] as int,
  //     receiver: _json[Key_receiver] as int,
  //     msgtype: msgtype,
  //     msg: _json[Key_msg] ?? '',
  //     image: _json[Key_image] ?? '',
  //     msgdata: _json[Key_msgdata] ?? '',
  //     sendtime: _json[Key_sendtime] ?? '',
  //     sendtimestamp: _json[Key_sendtimestamp] as int,
  //     is_receive: _json[Key_is_receive] as bool,
  //     pagename: _json[Key_pagename] ?? '',
  //   );
  // }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_contract_id] = this.contract_id;
    data[Key_order_id] = this.order_id;
    data[Key_chattingroom] = this.chattingroom;
    data[Key_momdady] = this.momdady;
    data[Key_momdady_first_name] = this.momdady_first_name;
    data[Key_linkmom] = this.linkmom;
    data[Key_linkmom_first_name] = this.linkmom_first_name;
    data[Key_matching_status] = this.matching_status;
    data[Key_care_status] = this.care_status;
    data[Key_deadline] = this.deadline;
    data[Key_cancel_id] = this.cancel_id;
    data[Key_cancel_usertype] = this.cancel_usertype;
    data[Key_cancelreason] = this.cancelreason;
    data[Key_is_cancelexplain] = this.is_cancelexplain;
    data[Key_is_totalcancel] = this.is_totalcancel;
    data[Key_popup_code] = this.popup_code;
    data[Key_contract_status] = this.contract_status;
    data[Key_rematching] = this.rematching;
    return data;
  }

  @override
  String toString() {
    return 'ContractData{contract_id: $contract_id, order_id: $order_id, chattingroom: $chattingroom, momdady: $momdady, momdady_first_name: $momdady_first_name, linkmom: $linkmom, linkmom_first_name: $linkmom_first_name, matching_status: $matching_status, care_status: $care_status, deadline: $deadline, cancel_id: $cancel_id, cancel_usertype: $cancel_usertype, cancelreason: $cancelreason, is_cancelexplain: $is_cancelexplain, is_totalcancel: $is_totalcancel, popup_code: $popup_code, contract_status: $contract_status, isLinkMom: $isLinkMom, rematching: $rematching}';
  }
}

///링크쌤, 맘대디 정보
class ChatInfoData {
  static const String Key_momdady = 'momdady';
  static const String Key_linkmom = 'linkmom';
  static const String Key_rematching = 'rematching';

  ChatInfoData({
    this.momdady = 0,
    this.linkmom = 0,
    this.rematching = false,
  });

  @JsonKey(name: Key_momdady)
  final int momdady;
  @JsonKey(name: Key_linkmom)
  final int linkmom;
  @JsonKey(name: Key_rematching)
  final bool rematching;

  factory ChatInfoData.fromJson(Map<String, dynamic> json) {
    return ChatInfoData(
      momdady: json[Key_momdady] as int,
      linkmom: json[Key_linkmom] as int,
      rematching: json[Key_rematching] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_momdady] = this.momdady;
    data[Key_linkmom] = this.linkmom;
    data[Key_rematching] = this.rematching;
    return data;
  }

  @override
  String toString() {
    return 'ChatInfoData{momdady: $momdady, linkmom: $linkmom, rematching: $rematching}';
  }
}

///링크쌤, 맘대디 정보
class ChatFPData {
  static const String Key_pagename = 'pagename';
  static const String Key_chattingroom = 'chattingroom';
  static const String Key_sendtime = 'sendtime';
  static const String Key_sendtimestamp = 'sendtimestamp';

  ChatFPData({
    this.pagename = 0,
    this.chattingroom = 0,
    this.sendtime = '',
    this.sendtimestamp = 0,
  });

  @JsonKey(name: Key_pagename)
  final int pagename;
  @JsonKey(name: Key_chattingroom)
  final int chattingroom;
  @JsonKey(name: Key_sendtime)
  final String sendtime;
  @JsonKey(name: Key_sendtimestamp)
  final int sendtimestamp;

  factory ChatFPData.fromJson(Map<String, dynamic> json) {
    return ChatFPData(
      pagename: json[Key_pagename] as int,
      chattingroom: json[Key_chattingroom] as int,
      sendtime: json[Key_sendtime] ?? '',
      sendtimestamp: json[Key_sendtimestamp] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_pagename] = this.pagename;
    data[Key_chattingroom] = this.chattingroom;
    data[Key_sendtime] = this.sendtime;
    data[Key_sendtimestamp] = this.sendtimestamp;
    return data;
  }

  @override
  String toString() {
    return 'ChatFPData{pagename: $pagename, chattingroom: $chattingroom, sendtime: $sendtime, sendtimestamp: $sendtimestamp}';
  }
}
