import 'package:json_annotation/json_annotation.dart';

import 'chat_send_data.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : matching_deny_response.dart - 매칭 거절/취소하기 응답
/// since        : 2021/08/03 / update:
/// (0, '매칭중(대화중)'), (11, '매칭실패(링크쌤 응답없음)'), (12, '매칭실패(맘대디 응답없음)'), (13, '매칭실패(링크쌤 거절)'), (14, '매칭실패(맘대디 거절)'), (21, '매칭취소(링크쌤 계약취소)'), (22, '매칭취소(맘대디 계약취소)'), (23, '매칭취소(결제시간만료)'), (91, '매칭대기(링크쌤 계약대기)'), (92, '매칭대기(맘대디 계약대기)'), (99, '매칭성공(계약완료/결제대기중)') (100, '결제완료') (0, '없음(매칭상태 확인)'), (1, '돌봄예정'), (2, '돌봄 중'), (3, '돌봄종료(정상종료)'), (4, '돌봄취소(부분종료&일부취소)'), (5, '돌봄취소(전체취소)') (0, '계약중'), (11, '계약실패(거절/취소/응답없음/시간만료 등)'), (21, '계약완료 후 취소(부분)'), (22, '계약완료 후 취소(전체)'), (99, '매칭성공(계약완료/결제대기중)'), (100, '결제완료')
@JsonSerializable()
class MatchingCancelDenyData {
  static const String Key_id = 'id';
  static const String Key_bookingcareservices = 'bookingcareservices';
  static const String Key_carematching = 'carematching';
  static const String Key_carematching_info = 'carematching_info';
  static const String Key_paid_deadline = 'paid_deadline';
  static const String Key_contract_date = 'contract_date';
  static const String Key_contract_status = 'contract_status';
  static const String Key_createdate = 'createdate';
  static const String Key_updatedate = 'updatedate';
  static const String Key_msgdata = 'msgdata';

  MatchingCancelDenyData({
    this.id = 0,
    this.bookingcareservices = 0,
    this.carematching = 0,
    this.carematching_info,
    this.paid_deadline = '',
    this.contract_date = '',
    this.contract_status = 0,
    this.createdate = '',
    this.updatedate = '',
    this.msgdata,
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_bookingcareservices)
  final int bookingcareservices;
  @JsonKey(name: Key_carematching)
  final int carematching;
  @JsonKey(name: Key_carematching_info)
  final CareMatchingInfoData? carematching_info;
  @JsonKey(name: Key_paid_deadline)
  final String paid_deadline;
  @JsonKey(name: Key_contract_date)
  final String contract_date;
  @JsonKey(name: Key_contract_status)
  final int contract_status;
  @JsonKey(name: Key_createdate)
  final String createdate;
  @JsonKey(name: Key_updatedate)
  final String updatedate;
  @JsonKey(name: Key_msgdata)
  final ContractData? msgdata;

  factory MatchingCancelDenyData.fromJson(Map<String, dynamic> json) {
    CareMatchingInfoData carematchingInfo = CareMatchingInfoData.fromJson(json[Key_carematching_info]);
    ContractData msgdata = json[Key_msgdata] == null ? ContractData() : ContractData.fromJson(json[Key_msgdata]);
    return MatchingCancelDenyData(
      id: json[Key_id] as int,
      bookingcareservices: json[Key_bookingcareservices] as int,
      carematching: json[Key_carematching] as int,
      carematching_info: carematchingInfo,
      paid_deadline: json[Key_paid_deadline] ?? '',
      contract_date: json[Key_contract_date] ?? '',
      contract_status: json[Key_contract_status] as int,
      createdate: json[Key_createdate] ?? '',
      updatedate: json[Key_updatedate] ?? '',
      msgdata: msgdata,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_bookingcareservices] = this.bookingcareservices;
    data[Key_carematching] = this.carematching;
    data[Key_carematching_info] = this.carematching_info;
    data[Key_paid_deadline] = this.paid_deadline;
    data[Key_contract_date] = this.contract_date;
    data[Key_contract_status] = this.contract_status;
    data[Key_createdate] = this.createdate;
    data[Key_updatedate] = this.updatedate;
    data[Key_msgdata] = this.msgdata;
    return data;
  }

  @override
  String toString() {
    return 'MatchingCancelDenyData{id: $id, bookingcareservices: $bookingcareservices, carematching: $carematching, carematching_info: $carematching_info, paid_deadline: $paid_deadline, contract_date: $contract_date, contract_status: $contract_status, createdate: $createdate, updatedate: $updatedate, msgdata: $msgdata}';
  }
}

class CareMatchingInfoData {
  static const String Key_matching_id = 'matching_id';
  static const String Key_matching_status = 'matching_status';
  static const String Key_matching_status_date = 'matching_status_date';
  static const String Key_care_status = 'care_status';
  static const String Key_care_status_date = 'care_status_date';

  CareMatchingInfoData({
    this.matching_id = 0,
    this.matching_status = 0,
    this.matching_status_date = '',
    this.care_status = 0,
    this.care_status_date = '',
  });

  @JsonKey(name: Key_matching_id)
  final int matching_id;
  @JsonKey(name: Key_matching_status)
  final int matching_status;
  @JsonKey(name: Key_matching_status_date)
  final String matching_status_date;
  @JsonKey(name: Key_care_status)
  final int care_status;
  @JsonKey(name: Key_care_status_date)
  final String care_status_date;

  factory CareMatchingInfoData.fromJson(Map<String, dynamic> json) {
    return CareMatchingInfoData(
      matching_id: json[Key_matching_id] as int,
      matching_status: json[Key_matching_status] as int,
      matching_status_date: json[Key_matching_status_date] ?? '',
      care_status: json[Key_care_status] as int,
      care_status_date: json[Key_care_status_date] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_matching_id] = this.matching_id;
    data[Key_matching_status] = this.matching_status;
    data[Key_matching_status_date] = this.matching_status_date;
    data[Key_care_status] = this.care_status;
    data[Key_care_status_date] = this.care_status_date;
    return data;
  }

  @override
  String toString() {
    return 'CareMatchingInfoData{matching_id: $matching_id, matching_status: $matching_status, matching_status_date: $matching_status_date, care_status: $care_status, care_status_date: $care_status_date}';
  }
}
