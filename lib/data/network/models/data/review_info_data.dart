import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : review_info_data.dart - 리뷰, 후기(링크쌤 프로필, 링크쌤 리스트에서 사용)
/// since        : 2021/06/03 / update:
@JsonSerializable()
class ReViewInfoData {
  static const String Key_favoite_cnt_linkmom = 'favoite_cnt_linkmom';
  static const String Key_review_cnt_linkmom = 'review_cnt_linkmom';

  ReViewInfoData({
    this.favoite_cnt_linkmom = 0,
    this.review_cnt_linkmom = 0,
  });

  @JsonKey(name: Key_favoite_cnt_linkmom)
  final int favoite_cnt_linkmom;
  @JsonKey(name: Key_review_cnt_linkmom)
  final int review_cnt_linkmom;

  factory ReViewInfoData.fromJson(Map<String, dynamic> json) {
    return ReViewInfoData(
      favoite_cnt_linkmom: json[Key_favoite_cnt_linkmom] as int,
      review_cnt_linkmom: json[Key_review_cnt_linkmom] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_favoite_cnt_linkmom] = this.favoite_cnt_linkmom;
    data[Key_review_cnt_linkmom] = this.review_cnt_linkmom;
    return data;
  }

  @override
  String toString() {
    return 'ReViewInfoData{favoite_cnt_linkmom: $favoite_cnt_linkmom, review_cnt_linkmom: $review_cnt_linkmom}';
  }
}
