import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class PayMentInfoData {
  static const String Key_payment_id = 'payment_id';
  static const String Key_createdate = 'createdate';
  static const String Key_product_price = 'product_price';
  static const String Key_used_point = 'used_point';
  static const String Key_used_cash = 'used_cash';
  static const String Key_used_pg = 'used_pg';
  static const String Key_pay_method = 'pay_method';
  static const String Key_pg_provider = 'pg_provider';
  static const String Key_card_name = 'card_name';
  static const String Key_card_number = 'card_number';

  PayMentInfoData({
    this.payment_id = 0,
    this.createdate = '',
    this.product_price = 0,
    this.used_point = 0,
    this.used_cash = 0,
    this.used_pg = 0,
    this.pay_method = '',
    this.pg_provider = '',
    this.card_name = '',
    this.card_number = '',
  });

  @JsonKey(name: Key_payment_id)
  final int payment_id;
  @JsonKey(name: Key_createdate)
  final String createdate;
  @JsonKey(name: Key_product_price)
  int product_price;
  @JsonKey(name: Key_used_point)
  final int used_point;
  @JsonKey(name: Key_used_cash)
  final int used_cash;
  @JsonKey(name: Key_used_pg)
  final int used_pg;
  @JsonKey(name: Key_pay_method)
  final String pay_method;
  @JsonKey(name: Key_pg_provider)
  final String pg_provider;
  @JsonKey(name: Key_card_name)
  final String card_name;
  @JsonKey(name: Key_card_number)
  final String card_number;

  factory PayMentInfoData.fromJson(Map<String, dynamic> json) {
    return PayMentInfoData(
      payment_id: json[Key_payment_id] as int,
      createdate: json[Key_createdate] ?? '',
      product_price: json[Key_product_price] as int,
      used_point: json[Key_used_point] as int,
      used_cash: json[Key_used_cash] as int,
      used_pg: json[Key_used_pg] as int,
      pay_method: json[Key_pay_method] ?? '',
      pg_provider: json[Key_pg_provider] ?? '',
      card_name: json[Key_card_name] ?? '',
      card_number: json[Key_card_number] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_payment_id] = this.payment_id;
    data[Key_createdate] = this.createdate;
    data[Key_product_price] = this.product_price;
    data[Key_used_point] = this.used_point;
    data[Key_used_cash] = this.used_cash;
    data[Key_used_pg] = this.used_pg;
    data[Key_pay_method] = this.pay_method;
    data[Key_pg_provider] = this.pg_provider;
    data[Key_card_name] = this.card_name;
    data[Key_card_number] = this.card_number;
    return data;
  }

  ///결제금액 + 포인트 + 캐시 = 최종금액
  int calcuPrice() {
    return used_pg + used_point + used_cash;
  }

  @override
  String toString() {
    return 'PayMentInfoData{payment_id: $payment_id, createdate: $createdate, product_price: $product_price, used_point: $used_point, used_cash: $used_cash, used_pg: $used_pg, pay_method: $pay_method, pg_provider: $pg_provider, card_name: $card_name, card_number: $card_number}';
  }
}
