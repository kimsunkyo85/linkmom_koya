import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class PayCarediaryInfoData {
  static const String Key_carediary_id = 'carediary_id';
  static const String Key_start_caredate = 'start_caredate';
  static const String Key_end_caredate = 'end_caredate';
  static const String Key_pay_status = 'pay_status';
  static const String Key_pay_status_txt = 'pay_status_txt';
  static const String Key_pay_expect = 'pay_expect';
  static const String Key_momdady_pay_rate = 'momdady_pay_rate';
  static const String Key_momdady_charge = 'momdady_charge';
  static const String Key_linkmom_pay_rate = 'linkmom_pay_rate';
  static const String Key_linkmom_charge = 'linkmom_charge';
  static const String Key_linkmom_penalty = 'linkmom_penalty';

  PayCarediaryInfoData({
    this.carediary_id = 0,
    this.start_caredate = '',
    this.end_caredate = '',
    this.pay_status = 0,
    this.pay_status_txt = '',
    this.pay_expect = 0,
    this.momdady_pay_rate = 0,
    this.momdady_charge = 0,
    this.linkmom_pay_rate = 0,
    this.linkmom_charge = 0,
    this.linkmom_penalty = 0,
    this.isCheck = false,
  });

  @JsonKey(name: Key_carediary_id)
  final int? carediary_id;
  @JsonKey(name: Key_start_caredate)
  final String? start_caredate;
  @JsonKey(name: Key_end_caredate)
  final String? end_caredate;
  @JsonKey(name: Key_pay_status)
  final int? pay_status;
  @JsonKey(name: Key_pay_status_txt)
  final String? pay_status_txt;
  @JsonKey(name: Key_pay_expect)
  final int? pay_expect;
  @JsonKey(name: Key_momdady_pay_rate)
  final int? momdady_pay_rate;
  @JsonKey(name: Key_momdady_charge)
  final int? momdady_charge;
  @JsonKey(name: Key_linkmom_pay_rate)
  final int? linkmom_pay_rate;
  @JsonKey(name: Key_linkmom_charge)
  final int? linkmom_charge;
  @JsonKey(name: Key_linkmom_penalty)
  final int? linkmom_penalty;

  bool? isCheck = false;

  factory PayCarediaryInfoData.fromJson(Map<String, dynamic> json) {
    return PayCarediaryInfoData(
      carediary_id: json[Key_carediary_id] as int,
      start_caredate: json[Key_start_caredate] ?? '',
      end_caredate: json[Key_end_caredate] ?? '',
      pay_status: json[Key_pay_status] as int,
      pay_status_txt: json[Key_pay_status_txt] ?? '',
      pay_expect: json[Key_pay_expect] as int,
      momdady_pay_rate: json[Key_momdady_pay_rate] as int,
      momdady_charge: json[Key_momdady_charge] as int,
      linkmom_pay_rate: json[Key_linkmom_pay_rate] as int,
      linkmom_charge: json[Key_linkmom_charge] as int,
      linkmom_penalty: json[Key_linkmom_penalty] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_carediary_id] = this.carediary_id;
    data[Key_start_caredate] = this.start_caredate;
    data[Key_end_caredate] = this.end_caredate;
    data[Key_pay_status] = this.pay_status;
    data[Key_pay_status_txt] = this.pay_status_txt;
    data[Key_pay_expect] = this.pay_expect;
    data[Key_momdady_pay_rate] = this.momdady_pay_rate;
    data[Key_momdady_charge] = this.momdady_charge;
    data[Key_linkmom_pay_rate] = this.linkmom_pay_rate;
    data[Key_linkmom_charge] = this.linkmom_charge;
    data[Key_linkmom_penalty] = this.linkmom_penalty;
    return data;
  }

  @override
  String toString() {
    return 'PayCarediaryInfoData{carediary_id: $carediary_id, start_caredate: $start_caredate, end_caredate: $end_caredate, pay_status: $pay_status, pay_status_txt: $pay_status_txt, pay_expect: $pay_expect, momdady_pay_rate: $momdady_pay_rate, momdady_charge: $momdady_charge, linkmom_pay_rate: $linkmom_pay_rate, linkmom_charge: $linkmom_charge, linkmom_penalty: $linkmom_penalty, isCheck: $isCheck}';
  }
}
