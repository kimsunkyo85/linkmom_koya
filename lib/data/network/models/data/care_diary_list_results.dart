import 'package:json_annotation/json_annotation.dart';

import '../carediary_view_response.dart';

@JsonSerializable()
class CareDiaryListResults {
  static const String Key_pay_status = 'pay_status';
  static const String Key_pay_status_text = 'pay_status_text';
  static const String Key_schedule_info = 'schedule_info';
  static const String Key_like_yn = 'like_yn';
  static const String Key_carediary_info = 'carediary_info';
  static const String Key_is_childdrug = 'is_childdrug';
  static const String Key_is_servicetype = 'is_servicetype';
  static const String Key_is_options = 'is_options';
  static const Key_cancel_info = 'cancel_info';

  @JsonKey(name: Key_pay_status)
  int payStatus;
  @JsonKey(name: Key_pay_status_text)
  String payStatusText;
  @JsonKey(name: Key_schedule_info)
  ScheduleInfo? scheduleInfo;
  @JsonKey(name: Key_like_yn)
  bool likeYn;
  @JsonKey(name: Key_carediary_info)
  CareDiaryInfo? carediaryInfo;
  @JsonKey(name: Key_is_childdrug)
  bool isChilddrug;
  @JsonKey(name: Key_is_servicetype)
  bool isServicetype;
  @JsonKey(name: Key_is_options)
  bool isOptions;
  @JsonKey(name: Key_cancel_info)
  CancelInfo? cancelinfo;

  CareDiaryListResults({this.payStatus = 0, this.payStatusText = '', this.scheduleInfo, this.likeYn = false, this.carediaryInfo, this.isChilddrug = false, this.isOptions = false, this.isServicetype = false, this.cancelinfo});

  factory CareDiaryListResults.fromJson(Map<String, dynamic> json) {
    return CareDiaryListResults(
      payStatus: json[Key_pay_status] ?? 0,
      payStatusText: json[Key_pay_status_text] ?? '',
      scheduleInfo: json[Key_schedule_info] == null ? ScheduleInfo() : ScheduleInfo.fromJson(json[Key_schedule_info]),
      likeYn: json[Key_like_yn] ?? false,
      carediaryInfo: json[Key_carediary_info] != null && json[Key_carediary_info].isNotEmpty ? CareDiaryInfo.fromJson(json[Key_carediary_info]) : CareDiaryInfo(),
      isChilddrug: json[Key_is_childdrug] ?? false,
      isServicetype: json[Key_is_servicetype] ?? false,
      isOptions: json[Key_is_options] ?? false,
      cancelinfo: json[Key_cancel_info] != null ? CancelInfo.fromJson(json[Key_cancel_info]) : CancelInfo(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_pay_status] = this.payStatus;
    data[Key_pay_status_text] = this.payStatusText;
    data[Key_schedule_info] = this.scheduleInfo!.toJson();
    data[Key_like_yn] = this.likeYn;
    data[Key_carediary_info] = this.carediaryInfo!.toJson();
    data[Key_is_childdrug] = this.isChilddrug;
    data[Key_is_servicetype] = this.isServicetype;
    data[Key_is_options] = this.isOptions;
    data[Key_cancel_info] = this.cancelinfo;
    return data;
  }

  @override
  String toString() {
    return 'Results{payStatus: $payStatus, payStatusText: $payStatusText, scheduleInfo: $scheduleInfo, likeYn: $likeYn, carediaryInfo: $carediaryInfo, isChilddrug: $isChilddrug isChilddrug: $isChilddrug isServicetype: $isServicetype isOptions: $isOptions, cancelinfo: $cancelinfo}';
  }
}

@JsonSerializable()
class CareDiaryInfo {
  static const String Key_id = 'id';
  static const String Key_content = 'content';
  static const String Key_thumbnail = 'thumbnail';
  static const String Key_reply_cnt = 'reply_cnt';
  static const String Key_momdady_confirm = 'momdady_confirm';
  static const String Key_updatedate = 'updatedate';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_content)
  String content;
  @JsonKey(name: Key_thumbnail)
  String thumbnail;
  @JsonKey(name: Key_reply_cnt)
  int replyCnt;
  @JsonKey(name: Key_momdady_confirm)
  bool momdadyConfirm;
  @JsonKey(name: Key_updatedate)
  String updatedate;

  CareDiaryInfo({this.id = 0, this.content = '', this.thumbnail = '', this.replyCnt = 0, this.momdadyConfirm = true, this.updatedate = ''});

  factory CareDiaryInfo.fromJson(Map<String, dynamic> json) {
    return CareDiaryInfo(
      id: json[Key_id] ?? 0,
      content: json[Key_content] ?? '',
      thumbnail: json[Key_thumbnail] ?? '',
      replyCnt: json[Key_reply_cnt] ?? 0,
      momdadyConfirm: json[Key_momdady_confirm] ?? true,
      updatedate: json[Key_updatedate] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_content] = this.content;
    data[Key_thumbnail] = this.thumbnail;
    data[Key_reply_cnt] = this.replyCnt;
    data[Key_momdady_confirm] = this.momdadyConfirm;
    data[Key_updatedate] = this.updatedate;
    return data;
  }

  @override
  String toString() {
    return 'CarediaryInfo{id: $id, content: $content, thumbnail: $thumbnail, replyCnt: $replyCnt, momdadyConfirm: $momdadyConfirm, updatedate: $updatedate}';
  }
}

@JsonSerializable()
class ScheduleInfo {
  static const String Key_schedule_id = 'schedule_id';
  static const String Key_servicetype = 'servicetype';
  static const String Key_start_caredate = 'start_caredate';
  static const String Key_end_caredate = 'end_caredate';
  static const String Key_child_name = 'child_name';
  static const String Key_momdady_name = 'momdady_name';
  static const String Key_linkmom_name = 'linkmom_name';

  @JsonKey(name: Key_schedule_id)
  int scheduleId;
  @JsonKey(name: Key_servicetype)
  String servicetype;
  @JsonKey(name: Key_start_caredate)
  String startCaredate;
  @JsonKey(name: Key_end_caredate)
  String endCaredate;
  @JsonKey(name: Key_child_name)
  String childName;
  @JsonKey(name: Key_momdady_name)
  String momdadyName;
  @JsonKey(name: Key_linkmom_name)
  String linkmomName;

  ScheduleInfo({this.scheduleId = 0, this.servicetype = '', this.startCaredate = '', this.endCaredate = '', this.childName = '', this.momdadyName = '', this.linkmomName = ''});

  factory ScheduleInfo.fromJson(Map<String, dynamic> json) {
    return ScheduleInfo(
      scheduleId: json[Key_schedule_id] as int,
      servicetype: json[Key_servicetype] ?? '',
      startCaredate: json[Key_start_caredate] ?? '',
      endCaredate: json[Key_end_caredate] ?? '',
      childName: json[Key_child_name] ?? '',
      momdadyName: json[Key_momdady_name] ?? '',
      linkmomName: json[Key_linkmom_name] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_schedule_id] = this.scheduleId;
    data[Key_servicetype] = this.servicetype;
    data[Key_start_caredate] = this.startCaredate;
    data[Key_end_caredate] = this.endCaredate;
    data[Key_child_name] = this.childName;
    data[Key_momdady_name] = this.momdadyName;
    data[Key_linkmom_name] = this.linkmomName;
    return data;
  }

  @override
  String toString() {
    return 'ScheduleInfo{scheduleId: $scheduleId,servicetype: $servicetype,startCaredate: $startCaredate,endCaredate: $endCaredate,childName: $childName,momdadyName: $momdadyName,linkmomName: $linkmomName}';
  }
}
