import 'package:json_annotation/json_annotation.dart';

import 'chat_room_data.dart';

@JsonSerializable()
class ChatMainResultsData {
  static const String Key_id = 'id';
  static const String Key_chatroom_id = 'chatroom_id';
  static const String Key_chattingtype = 'chattingtype';
  static const String Key_talker_info = 'talker_info';
  static const String Key_msgtype = 'msgtype';
  static const String Key_msg = 'msg';
  static const String Key_bookingcareservices_info = 'bookingcareservices_info';
  static const String Key_image = 'image';
  static const String Key_sendtime = 'sendtime';
  static const String Key_sendtimestamp = 'sendtimestamp';
  static const String Key_is_receive = 'is_receive';

  //리스트화면에서 사용
  static const String Key_isDelete = 'isDelete';

  ChatMainResultsData({
    this.id = 0,
    this.chatroom_id = 0,
    this.chattingtype = 0,
    this.talker_info,
    this.msgtype = 0,
    this.msg = '',
    this.bookingcareservices_info,
    this.image = '',
    this.sendtime = '',
    this.sendtimestamp = 0,
    this.is_receive = false,
    this.isDelete = false,
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_chatroom_id)
  final int chatroom_id;
  @JsonKey(name: Key_chattingtype)
  final int chattingtype;
  @JsonKey(name: Key_talker_info)
  final ChatMessageInfoData? talker_info;
  @JsonKey(name: Key_msgtype)
  final int msgtype;
  @JsonKey(name: Key_msg)
  String msg;
  @JsonKey(name: Key_bookingcareservices_info)
  final ChatBookingCareServicesInfoData? bookingcareservices_info;
  @JsonKey(name: Key_image)
  String image;
  @JsonKey(name: Key_sendtime)
  String sendtime;
  @JsonKey(name: Key_sendtimestamp)
  int sendtimestamp;
  @JsonKey(name: Key_is_receive)
  bool is_receive;

  bool isDelete;

  factory ChatMainResultsData.fromJson(Map<String, dynamic> json) {
    ChatMessageInfoData? talkerInfo = json[Key_talker_info] == null ? null : ChatMessageInfoData.fromJson(json[Key_talker_info]);
    // log.d('『GGUMBI』>>> fromJson : json[Key_bookingcareservices_info]: ${json[Key_bookingcareservices_info]},  <<< ');
    ChatBookingCareServicesInfoData? bookingcareservicesInfo = json[Key_bookingcareservices_info] == null ? null : ChatBookingCareServicesInfoData.fromJson(json[Key_bookingcareservices_info]);
    return ChatMainResultsData(
      id: json[Key_id] as int,
      chatroom_id: json[Key_chatroom_id] as int,
      chattingtype: json[Key_chattingtype] as int,
      talker_info: talkerInfo,
      msgtype: json[Key_msgtype] as int,
      msg: json[Key_msg] ?? '',
      bookingcareservices_info: bookingcareservicesInfo,
      image: json[Key_image] ?? '',
      sendtime: json[Key_sendtime] ?? '',
      sendtimestamp: json[Key_sendtimestamp] as int,
      is_receive: json[Key_is_receive] as bool,
      isDelete: json[Key_isDelete] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_chatroom_id] = this.chatroom_id;
    data[Key_chattingtype] = this.chattingtype;
    data[Key_talker_info] = this.talker_info;
    data[Key_msgtype] = this.msgtype;
    data[Key_msg] = this.msg;
    data[Key_bookingcareservices_info] = this.bookingcareservices_info;
    data[Key_image] = this.image;
    data[Key_sendtime] = this.sendtime;
    data[Key_sendtimestamp] = this.sendtimestamp;
    data[Key_is_receive] = this.is_receive;
    data[Key_isDelete] = this.isDelete;
    return data;
  }

  @override
  String toString() {
    return 'ChatMainResultsData{id: $id, chatroom_id: $chatroom_id, chattingtype: $chattingtype, talker_info: $talker_info, msgtype: $msgtype, msg: $msg, bookingcareservices_info: $bookingcareservices_info, image: $image, sendtime: $sendtime, sendtimestamp: $sendtimestamp, is_receive: $is_receive, isDelete: $isDelete}';
  }
}

/*class TalkerInfoData {
  static const String Key_first_name = 'first_name';
  static const String Key_profileimg = 'profileimg';

  TalkerInfoData({
    this.first_name,
    this.profileimg,
  });

  @JsonKey(name: Key_first_name)
  final String first_name;
  @JsonKey(name: Key_profileimg)
  final String profileimg;

  factory TalkerInfoData.fromJson(Map<String, dynamic> json) {
    return TalkerInfoData(
      first_name: json[Key_first_name] ?? '',
      profileimg: json[Key_profileimg] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_first_name] = this.first_name;
    data[Key_profileimg] = this.profileimg;
    return data;
  }

  @override
  String toString() {
    return 'TalkerInfoData{first_name: $first_name, profileimg: $profileimg}';
  }
}*/
