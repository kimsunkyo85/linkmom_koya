import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class PayRefundInfoInitData {
  static const String Key_refund_point = 'refund_point';
  static const String Key_expired_point = 'expired_point';
  static const String Key_refund_cash = 'refund_cash';

  PayRefundInfoInitData({
    this.refund_point = 0,
    this.expired_point = 0,
    this.refund_cash = 0,
  });

  ///환불가능 포인트
  @JsonKey(name: Key_refund_point)
  final int? refund_point;

  ///만료 포인트
  @JsonKey(name: Key_expired_point)
  final int? expired_point;

  ///환불가능 캐시
  @JsonKey(name: Key_refund_cash)
  final int? refund_cash;

  factory PayRefundInfoInitData.fromJson(Map<String, dynamic> json) {
    return PayRefundInfoInitData(
      refund_point: json[Key_refund_point] ?? 0,
      expired_point: json[Key_expired_point] ?? 0,
      refund_cash: json[Key_refund_cash] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_refund_point] = this.refund_point;
    data[Key_expired_point] = this.expired_point;
    data[Key_refund_cash] = this.refund_cash;
    return data;
  }

  @override
  String toString() {
    return 'PayRefundInfoInitData{refund_point: $refund_point, expired_point: $expired_point, refund_cash: $refund_cash}';
  }
}
