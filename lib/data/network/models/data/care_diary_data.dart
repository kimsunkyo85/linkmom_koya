import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryData {
  static const String Key_id = "id";
  static const String Key_carecontract_schedule = "carecontract_schedule";
  static const String Key_is_servicetype = "is_servicetype";
  static const String Key_complete_boyuk = "complete_boyuk";
  static const String Key_complete_homecare = "complete_homecare";
  static const String Key_complete_play = "complete_play";
  static const String Key_content = "content";
  static const String Key_carediary_images = "carediary_images";

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_carecontract_schedule)
  int carecontractSchedule;
  @JsonKey(name: Key_is_servicetype)
  int isServicetype;
  @JsonKey(name: Key_complete_boyuk)
  List<int>? completeBoyuk;
  @JsonKey(name: Key_complete_homecare)
  List<int>? completeHomecare;
  @JsonKey(name: Key_complete_play)
  List<int>? completePlay;
  @JsonKey(name: Key_content)
  String content;
  @JsonKey(name: Key_carediary_images)
  List<CarediaryImages>? carediaryImages;

  CareDiaryData({this.id = 0, this.carecontractSchedule = 0, this.isServicetype = 0, this.completeBoyuk, this.completeHomecare, this.completePlay, this.content = '', this.carediaryImages});

  factory CareDiaryData.fromJson(Map<String, dynamic> json) {
    return CareDiaryData(
      id: json[Key_id] as int,
      carecontractSchedule: json[Key_carecontract_schedule] ?? 0,
      isServicetype: json[Key_is_servicetype] ?? 0,
      completeBoyuk: json[Key_complete_boyuk] != null ? json[Key_complete_boyuk].split(',').map<int>((e) => int.parse(e)).toList() : [],
      completeHomecare: json[Key_complete_homecare] != null ? json[Key_complete_homecare].split(',').map<int>((e) => int.parse(e)).toList() : [],
      completePlay: json[Key_complete_play] != null ? json[Key_complete_play].split(',').map<int>((e) => int.parse(e)).toList() : [],
      content: json[Key_content] ?? '',
      carediaryImages: json[Key_carediary_images] != null ? json[Key_carediary_images].map<CarediaryImages>((e) => CarediaryImages.fromJson(e)).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_carecontract_schedule] = this.carecontractSchedule;
    data[Key_is_servicetype] = this.isServicetype;
    data[Key_complete_boyuk] = this.completeBoyuk;
    data[Key_complete_homecare] = this.completeHomecare;
    data[Key_complete_play] = this.completePlay;
    data[Key_content] = this.content;
    data[Key_carediary_images] = this.carediaryImages;
    return data;
  }
}

@JsonSerializable()
class CarediaryImages {
  static const String Key_images = 'images';
  
  String images;

  CarediaryImages({this.images = ''});

  factory CarediaryImages.fromJson(Map<String, dynamic> json) {
    return CarediaryImages(images: json[Key_images] ?? '');
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.images;
    return data;
  }

  @override
  String toString() {
    return 'CarediaryImages{image:$images}';
  }
}
