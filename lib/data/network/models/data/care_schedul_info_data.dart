import 'package:json_annotation/json_annotation.dart';

import '../../../../main.dart';

@JsonSerializable()
class CareScheduleInfoData {
  static const String Key_booking_id = 'booking_id';
  static const String Key_booking_status = 'booking_status';
  static const String Key_servicetype = 'servicetype';
  static const String Key_cost_sum = 'cost_sum';
  static const String Key_stime = 'stime';
  static const String Key_etime = 'etime';
  static const String Key_caredate = 'caredate';
  static const String Key_possible_area = 'possible_area';
  static const String Key_status = 'status';

  CareScheduleInfoData({
    this.booking_id = 0,
    this.booking_status = 0,
    this.servicetype = '',
    this.cost_sum = 0,
    this.stime = '',
    this.etime = '',
    required this.caredate,
    this.possible_area = '',
    this.status = ''
  });

  @JsonKey(name: Key_booking_id)
  int booking_id;
  @JsonKey(name: Key_booking_status)
  int booking_status;
  @JsonKey(name: Key_servicetype)
  String servicetype;
  @JsonKey(name: Key_cost_sum)
  int cost_sum;
  @JsonKey(name: Key_stime)
  String stime;
  @JsonKey(name: Key_etime)
  String etime;
  @JsonKey(name: Key_caredate)
  List<String> caredate;
  @JsonKey(name: Key_possible_area)
  String possible_area;
  @JsonKey(name: Key_status)
  String status;

  factory CareScheduleInfoData.fromJson(Map<String, dynamic> json) {
    List<String> datas = [];
    try {
      datas = List<String>.from(json[Key_caredate]);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareScheduleInfoData(
      booking_id: json[Key_booking_id] ?? 0,
      booking_status: json[Key_booking_status] ?? 0,
      servicetype: json[Key_servicetype] ?? '',
      cost_sum: json[Key_cost_sum] ?? 0,
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      caredate: datas,
      possible_area: json[Key_possible_area] ?? '',
      status: json[Key_status] ?? ''
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_booking_id] = this.booking_id;
    data[Key_booking_status] = this.booking_status;
    data[Key_servicetype] = this.servicetype;
    data[Key_cost_sum] = this.cost_sum;
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_caredate] = this.caredate;
    data[Key_possible_area] = this.possible_area;
    return data;
  }

  @override
  String toString() {
    return 'CareScheduleInfoData{booking_id: $booking_id, booking_status: $booking_status, servicetype: $servicetype, cost_sum: $cost_sum, stime: $stime, etime: $etime, caredate: $caredate, possible_area: $possible_area}';
  }
}
