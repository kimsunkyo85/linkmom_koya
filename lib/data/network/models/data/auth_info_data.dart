import 'package:json_annotation/json_annotation.dart';

///인증정보 데이터
class AuthInfoData {
  static const String Key_auth_id = 'auth_id';
  static const String Key_grade_authlevel = 'grade_authlevel';
  static const String Key_hashtag_cctv = 'hashtag_cctv';
  static const String Key_hashtag_anmal = 'hashtag_anmal';
  static const String Key_hashtag_codiv_vaccine = 'hashtag_codiv_vaccine';
  static const String Key_hashtag_deungbon = 'hashtag_deungbon';
  static const String Key_hashtag_criminal = 'hashtag_criminal';
  static const String Key_hashtag_personality = 'hashtag_personality';
  static const String Key_hashtag_healthy = 'hashtag_healthy';
  static const String Key_hashtag_career_2 = 'hashtag_career_2';
  static const String Key_hashtag_education = 'hashtag_education';
  static const String Key_hashtag_career_1 = 'hashtag_career_1';
  static const String Key_hashtag_graduated = 'hashtag_graduated';
  static const String Key_hashtag_babysiter = 'hashtag_babysiter';

  AuthInfoData({
    this.auth_id = 0,
    this.grade_authlevel = 0,
    this.hashtag_cctv = '',
    this.hashtag_anmal = '',
    this.hashtag_codiv_vaccine = '',
    this.hashtag_deungbon = '',
    this.hashtag_criminal = '',
    this.hashtag_personality = '',
    this.hashtag_healthy = '',
    this.hashtag_career_2 = '',
    this.hashtag_education = '',
    this.hashtag_career_1 = '',
    this.hashtag_graduated = '',
    this.hashtag_babysiter = '',
  });

  @JsonKey(name: Key_auth_id)
  int auth_id;
  @JsonKey(name: Key_grade_authlevel)
  int grade_authlevel;
  final String hashtag_cctv;
  @JsonKey(name: Key_hashtag_anmal)
  final String hashtag_anmal;
  @JsonKey(name: Key_hashtag_codiv_vaccine)
  final String hashtag_codiv_vaccine;
  @JsonKey(name: Key_hashtag_deungbon)
  final String hashtag_deungbon;
  @JsonKey(name: Key_hashtag_criminal)
  final String hashtag_criminal;
  @JsonKey(name: Key_hashtag_personality)
  final String hashtag_personality;
  @JsonKey(name: Key_hashtag_healthy)
  final String hashtag_healthy;
  @JsonKey(name: Key_hashtag_career_2)
  final String hashtag_career_2;
  @JsonKey(name: Key_hashtag_education)
  final String hashtag_education;
  @JsonKey(name: Key_hashtag_career_1)
  final String hashtag_career_1;
  @JsonKey(name: Key_hashtag_graduated)
  final String hashtag_graduated;
  @JsonKey(name: Key_hashtag_babysiter)
  final String hashtag_babysiter;

  factory AuthInfoData.fromJson(Map<String, dynamic> json) {
    return AuthInfoData(
      auth_id: json[Key_auth_id] as int,
      grade_authlevel: json[Key_grade_authlevel] as int,
      hashtag_cctv: json[Key_hashtag_cctv] ?? '',
      hashtag_anmal: json[Key_hashtag_anmal] ?? '',
      hashtag_codiv_vaccine: json[Key_hashtag_codiv_vaccine] ?? '',
      hashtag_deungbon: json[Key_hashtag_deungbon] ?? '',
      hashtag_criminal: json[Key_hashtag_criminal] ?? '',
      hashtag_personality: json[Key_hashtag_personality] ?? '',
      hashtag_healthy: json[Key_hashtag_healthy] ?? '',
      hashtag_career_2: json[Key_hashtag_career_2] ?? '',
      hashtag_education: json[Key_hashtag_education] ?? '',
      hashtag_career_1: json[Key_hashtag_career_1] ?? '',
      hashtag_graduated: json[Key_hashtag_graduated] ?? '',
      hashtag_babysiter: json[Key_hashtag_babysiter] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_auth_id] = this.auth_id;
    data[Key_grade_authlevel] = this.grade_authlevel;
    data[Key_hashtag_cctv] = this.hashtag_cctv;
    data[Key_hashtag_anmal] = this.hashtag_anmal;
    data[Key_hashtag_codiv_vaccine] = this.hashtag_codiv_vaccine;
    data[Key_hashtag_deungbon] = this.hashtag_deungbon;
    data[Key_hashtag_criminal] = this.hashtag_criminal;
    data[Key_hashtag_personality] = this.hashtag_personality;
    data[Key_hashtag_healthy] = this.hashtag_healthy;
    data[Key_hashtag_career_2] = this.hashtag_career_2;
    data[Key_hashtag_education] = this.hashtag_education;
    data[Key_hashtag_career_1] = this.hashtag_career_1;
    data[Key_hashtag_graduated] = this.hashtag_graduated;
    data[Key_hashtag_babysiter] = this.hashtag_babysiter;
    return data;
  }

  @override
  String toString() {
    return 'AuthInfoData{auth_id: $auth_id, grade_authlevel: $grade_authlevel, hashtag_cctv: $hashtag_cctv, hashtag_anmal: $hashtag_anmal, hashtag_codiv_vaccine: $hashtag_codiv_vaccine, hashtag_deungbon: $hashtag_deungbon, hashtag_criminal: $hashtag_criminal, hashtag_personality: $hashtag_personality, hashtag_healthy: $hashtag_healthy, hashtag_career_2: $hashtag_career_2, hashtag_education: $hashtag_education, hashtag_career_1: $hashtag_career_1, hashtag_graduated: $hashtag_graduated, hashtag_babysiter: $hashtag_babysiter}';
  }
}
