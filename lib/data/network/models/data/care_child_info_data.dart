import 'package:json_annotation/json_annotation.dart';

///아이정보
class CareChildInfoData {
  static const String Key_child_name = 'child_name';
  static const String Key_child_age = 'child_age';
  static const String Key_child_gender = 'child_gender';

  CareChildInfoData({
    this.child_name = '',
    this.child_age = '',
    this.child_gender = '',
  });

  @JsonKey(name: Key_child_name)
  final String child_name;
  @JsonKey(name: Key_child_age)
  final String child_age;
  @JsonKey(name: Key_child_gender)
  final String child_gender;

  factory CareChildInfoData.fromJson(Map<String, dynamic> json) {
    return CareChildInfoData(
      child_name: json[Key_child_name] ?? '',
      child_age: json[Key_child_age] ?? '',
      child_gender: json[Key_child_gender] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_child_name] = this.child_name;
    data[Key_child_age] = this.child_age;
    data[Key_child_gender] = this.child_gender;
    return data;
  }

  @override
  String toString() {
    return 'CareChildInfoData{child_name: $child_name, child_age: $child_age, child_gender: $child_gender}';
  }
}
