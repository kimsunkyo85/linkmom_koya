import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : matching_init_view_data.dart - 서명요청/결제요청 응답 데이터
/// since        : 2021/08/03 / update:
class MatchingSignPayData {
  static const String Key_message = 'message';

  MatchingSignPayData({
    this.message = '',
  });

  @JsonKey(name: Key_message)
  final String message;

  factory MatchingSignPayData.fromJson(Map<String, dynamic> json) {
    return MatchingSignPayData(
      message: json[Key_message] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_message] = this.message;
    return data;
  }

  @override
  String toString() {
    return 'MatchingSignPayData{message: $message}';
  }
}
