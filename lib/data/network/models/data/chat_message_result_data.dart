import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/manager/enum_manager.dart';

import 'chat_room_data.dart';

@JsonSerializable()
class ChatMessageResultsData {
  static const String Key_id = 'id';
  static const String Key_chattingroom = 'chattingroom';
  static const String Key_sender_info = 'sender_info';
  static const String Key_receiver_info = 'receiver_info';
  static const String Key_msgtype = 'msgtype';
  static const String Key_msg = 'msg';
  static const String Key_image = 'image';
  static const String Key_sendtime = 'sendtime';
  static const String Key_sendtimestamp = 'sendtimestamp';
  static const String Key_is_receive = 'is_receive';

  ChatMessageResultsData({
    this.id = 0,
    this.chattingroom = 0,
    this.sender_info,
    this.receiver_info,
    this.msgtype,
    this.msg = '',
    this.image = '',
    this.sendtime = '',
    this.sendtimestamp = 0,
    this.is_receive = '',
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_chattingroom)
  final int chattingroom;
  @JsonKey(name: Key_sender_info)
  final ChatMessageInfoData? sender_info;
  @JsonKey(name: Key_receiver_info)
  final ChatMessageInfoData? receiver_info;
  @JsonKey(name: Key_msgtype)
  final ChatMsgType? msgtype;
  @JsonKey(name: Key_msg)
  final String msg;
  @JsonKey(name: Key_image)
  final String image;
  @JsonKey(name: Key_sendtime)
  final String sendtime;
  @JsonKey(name: Key_sendtimestamp)
  final int sendtimestamp;
  @JsonKey(name: Key_is_receive)
  final String is_receive;

  factory ChatMessageResultsData.fromJson(Map<String, dynamic> json) {
    ChatMessageInfoData senderInfo = ChatMessageInfoData.fromJson(json[Key_sender_info]);
    ChatMessageInfoData receiverInfo = ChatMessageInfoData.fromJson(json[Key_receiver_info]);
    ChatMsgType msgtype = ChatMsgType.values.where((element) => element.value == json[Key_msgtype] as int).last;

    return ChatMessageResultsData(
      id: json[Key_id] as int,
      chattingroom: json[Key_chattingroom] as int,
      sender_info: senderInfo,
      receiver_info: receiverInfo,
      msgtype: msgtype,
      msg: json[Key_msg] ?? '',
      image: json[Key_image] ?? '',
      sendtime: json[Key_sendtime] ?? '',
      sendtimestamp: json[Key_sendtimestamp] as int,
      is_receive: json[Key_is_receive] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_chattingroom] = this.chattingroom;
    data[Key_sender_info] = this.sender_info;
    data[Key_receiver_info] = this.receiver_info;
    data[Key_msgtype] = this.msgtype;
    data[Key_msg] = this.msg;
    data[Key_image] = this.image;
    data[Key_sendtime] = this.sendtime;
    data[Key_sendtimestamp] = this.sendtimestamp;
    data[Key_is_receive] = this.is_receive;
    return data;
  }

  @override
  String toString() {
    return 'ChatMessageResultsData{id: $id, chattingroom: $chattingroom, sender_info: $sender_info, receiver_info: $receiver_info, msgtype: $msgtype, msg: $msg, image: $image, sendtime: $sendtime, sendtimestamp: $sendtimestamp, is_receive: $is_receive}';
  }
}
