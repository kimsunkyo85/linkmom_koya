import 'package:json_annotation/json_annotation.dart';

import '../../../../main.dart';
import 'auth_info_data.dart';
import 'care_child_info_data.dart';
import 'care_schedul_info_data.dart';
import 'job_schedule_info_data.dart';
import 'linkmom_result_data.dart';
import 'user_info_data.dart';

@JsonSerializable()
class MomdadyResultsData {
  static const String Key_matching_id = 'matching_id';
  static const String Key_matching_status = 'matching_status';
  static const String Key_matching_status_date = 'matching_status_date';
  static const String Key_care_status = 'care_status';
  static const String Key_care_status_date = 'care_status_date';
  static const String Key_paid_deadline = 'paid_deadline';
  static const String Key_contract_id = 'contract_id';
  static const String Key_userinfo = 'userinfo';
  static const String Key_authinfo = 'authinfo';
  static const String Key_childinfo = 'childinfo';
  static const String Key_care_scheduleinfo = 'care_scheduleinfo';
  static const String Key_job_scheduleinfo = 'job_scheduleinfo';
  static const String Key_chatting = 'chatting';
  static const String Key_order_id = 'order_id';
  static const String Key_rematching = 'rematching';

  MomdadyResultsData({
    this.matching_id = 0,
    this.matching_status = 0,
    this.matching_status_date = '',
    this.care_status = 0,
    this.care_status_date = '',
    this.paid_deadline = '',
    this.contract_id = 0,
    this.userinfo,
    this.authinfo,
    this.childinfo,
    this.care_scheduleinfo,
    this.job_scheduleinfo,
    this.chatting,
    this.order_id = '',
    this.rematching = false,
  });

  @JsonKey(name: Key_matching_id)
  int matching_id;
  @JsonKey(name: Key_matching_status)
  int matching_status;
  @JsonKey(name: Key_matching_status_date)
  String matching_status_date;
  @JsonKey(name: Key_care_status)
  int care_status;
  @JsonKey(name: Key_care_status_date)
  String care_status_date;
  @JsonKey(name: Key_paid_deadline)
  String paid_deadline;
  @JsonKey(name: Key_contract_id)
  int contract_id;
  @JsonKey(name: Key_rematching)
  bool rematching;

  @JsonKey(name: Key_userinfo)
  UserInfoData? userinfo;
  @JsonKey(name: Key_authinfo)
  AuthInfoData? authinfo;
  @JsonKey(name: Key_childinfo)
  CareChildInfoData? childinfo;
  @JsonKey(name: Key_care_scheduleinfo)
  CareScheduleInfoData? care_scheduleinfo;
  @JsonKey(name: Key_job_scheduleinfo)
  JobScheduleInfo? job_scheduleinfo;
  @JsonKey(name: Key_chatting)
  ChattingData? chatting;
  @JsonKey(name: Key_order_id)
  String order_id;

  factory MomdadyResultsData.fromJson(Map<String, dynamic> json) {
    UserInfoData userinfo = UserInfoData();
    AuthInfoData authinfo = AuthInfoData();
    CareChildInfoData childinfo = CareChildInfoData();
    CareScheduleInfoData careSchedule = CareScheduleInfoData(caredate: []);
    JobScheduleInfo jobSchedule = JobScheduleInfo();
    ChattingData chatting = ChattingData();
    try {
      userinfo = UserInfoData.fromJson(json[Key_userinfo]);
      authinfo = AuthInfoData.fromJson(json[Key_authinfo]);
      childinfo = CareChildInfoData.fromJson(json[Key_childinfo]);
      careSchedule = CareScheduleInfoData.fromJson(json[Key_care_scheduleinfo]);
      jobSchedule = JobScheduleInfo.fromJson(json[Key_job_scheduleinfo]);
      chatting = ChattingData.fromJson(json[Key_chatting]);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return MomdadyResultsData(
      matching_id: json[Key_matching_id] as int,
      matching_status: json[Key_matching_status] as int,
      matching_status_date: json[Key_matching_status_date] ?? '',
      care_status: json[Key_care_status] as int,
      care_status_date: json[Key_care_status_date] ?? '',
      paid_deadline: json[Key_paid_deadline] ?? '',
      contract_id: json[Key_contract_id] as int,
      userinfo: userinfo,
      authinfo: authinfo,
      childinfo: childinfo,
      care_scheduleinfo: careSchedule,
      job_scheduleinfo: jobSchedule,
      chatting: chatting,
      order_id: json[Key_order_id] ?? '',
      rematching: json[Key_rematching] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_matching_id] = this.matching_id;
    data[Key_matching_status] = this.matching_status;
    data[Key_matching_status_date] = this.matching_status_date;
    data[Key_care_status] = this.care_status;
    data[Key_care_status_date] = this.care_status_date;
    data[Key_paid_deadline] = this.paid_deadline;
    data[Key_contract_id] = this.contract_id;
    data[Key_userinfo] = this.userinfo;
    data[Key_authinfo] = this.authinfo;
    data[Key_childinfo] = this.childinfo;
    data[Key_care_scheduleinfo] = this.care_scheduleinfo;
    data[Key_job_scheduleinfo] = this.job_scheduleinfo;
    data[Key_chatting] = this.chatting;
    data[Key_order_id] = this.order_id;
    data[Key_rematching] = this.rematching;
    return data;
  }

  @override
  String toString() {
    return 'MomdadyResultsData{matching_id: $matching_id, matching_status: $matching_status, matching_status_date: $matching_status_date, care_status: $care_status, care_status_date: $care_status_date, paid_deadline: $paid_deadline, contract_id: $contract_id, userinfo: $userinfo, authinfo: $authinfo, childinfo: $childinfo, care_scheduleinfo: $care_scheduleinfo, job_scheduleinfo: $job_scheduleinfo, chatting: $chatting, order_id: $order_id, rematching: $rematching}';
  }
}
