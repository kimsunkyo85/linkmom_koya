import 'package:json_annotation/json_annotation.dart';

import '../care_view_response.dart';
import 'auth_info_data.dart';
import 'chat_send_data.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : matching_init_view_data.dart - 매칭 데이터 응답(매칭요청 및 계약서 확인)
/// since        : 2021/08/03 / update:
class MatchingInitViewData {
  static const String Key_bookingcareservices_info = 'bookingcareservices_info';
  static const String Key_contract_info = 'contract_info';
  static const String Key_msgdata = 'msgdata';
  static const String Key_contract_id = 'contract_id';
  static const String Key_carematching = 'carematching';

  MatchingInitViewData({
    this.bookingcareservices_info,
    this.contract_info,
    this.msgdata,
    this.contract_id,
    this.carematching,
  });

  @JsonKey(name: Key_bookingcareservices_info)
  final CareViewData? bookingcareservices_info;
  @JsonKey(name: Key_contract_info)
  final ContractInfoData? contract_info;
  @JsonKey(name: Key_msgdata)
  final ContractData? msgdata;
  @JsonKey(name: Key_contract_id)
  final int? contract_id;
  @JsonKey(name: Key_carematching)
  final int? carematching;

  factory MatchingInitViewData.fromJson(Map<String, dynamic> json) {
    CareViewData bookingcareservicesInfo = json[Key_bookingcareservices_info] == null
        ? CareViewData(
            authinfo: AuthInfoData(),
            careboyuk: CareViewBoyukData(),
            caredate: [],
            careoptions: CareViewOptionData(),
            careschool: CareViewSchoolData(),
            childinfo: CareViewChildInfoData(),
            userinfo: CareViewUserInfoData(),
          )
        : CareViewData.fromJson(json[Key_bookingcareservices_info]);
    ContractInfoData linkmomInfo = json[Key_contract_info] == null ? ContractInfoData() : ContractInfoData.fromJson(json[Key_contract_info]);
    ContractData msgdata = json[Key_msgdata] == null ? ContractData() : ContractData.fromJson(json[Key_msgdata]);
    return MatchingInitViewData(
      bookingcareservices_info: bookingcareservicesInfo,
      contract_info: linkmomInfo,
      msgdata: msgdata,
      contract_id: json[Key_contract_id] as int,
      carematching: json[Key_carematching] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bookingcareservices_info] = this.bookingcareservices_info;
    data[Key_contract_info] = this.contract_info;
    data[Key_msgdata] = this.msgdata;
    data[Key_contract_id] = this.contract_id;
    data[Key_carematching] = this.carematching;
    return data;
  }

  @override
  String toString() {
    return 'MatchingInitViewData{bookingcareservices_info: $bookingcareservices_info, contract_info: $contract_info, msgdata: $msgdata, contract_id: $contract_id, carematching: $carematching}';
  }
}

class ContractInfoData {
  static const String Key_contract_id = 'contract_id';
  static const String Key_contract_date = 'contract_date';
  static const String Key_contract_status = 'contract_status';
  static const String Key_momdady_signature = 'momdady_signature';
  static const String Key_momdady_signdate = 'momdady_signdate';
  static const String Key_momdady_first_name = 'momdady_first_name';
  static const String Key_linkmom_signature = 'linkmom_signature';
  static const String Key_linkmom_signdate = 'linkmom_signdate';
  static const String Key_linkmom_first_name = 'linkmom_first_name';
  static const String Key_linkmom_address = 'linkmom_address';
  static const String Key_linkmom_address_detail = 'linkmom_address_detail';
  static const String Key_child_name = 'child_name';
  static const String Key_start_address = 'start_address';
  static const String Key_arrive_address = 'arrive_address';
  static const String Key_care_address = 'care_address';
  static const String Key_pathroute_comment = 'pathroute_comment';

  ContractInfoData({
    this.contract_id = 0,
    this.contract_date = '',
    this.contract_status = 0,
    this.momdady_signature = '',
    this.momdady_signdate = '',
    this.momdady_first_name = '',
    this.linkmom_signature = '',
    this.linkmom_signdate = '',
    this.linkmom_first_name = '',
    this.linkmom_address = '',
    this.linkmom_address_detail = '',
    this.child_name = '',
    this.start_address,
    this.arrive_address,
    this.care_address,
    this.pathroute_comment = '',
  });

  @JsonKey(name: Key_contract_id)
  final int contract_id;
  @JsonKey(name: Key_contract_date)
  final String contract_date;
  @JsonKey(name: Key_contract_status)
  final int contract_status;
  @JsonKey(name: Key_momdady_signature)
  final String momdady_signature;
  @JsonKey(name: Key_momdady_signdate)
  final String momdady_signdate;
  @JsonKey(name: Key_momdady_first_name)
  final String momdady_first_name;
  @JsonKey(name: Key_linkmom_signature)
  final String linkmom_signature;
  @JsonKey(name: Key_linkmom_signdate)
  final String linkmom_signdate;
  @JsonKey(name: Key_linkmom_first_name)
  final String linkmom_first_name;
  @JsonKey(name: Key_linkmom_address)
  String linkmom_address;
  @JsonKey(name: Key_linkmom_address_detail)
  String linkmom_address_detail;
  @JsonKey(name: Key_child_name)
  final String child_name;
  @JsonKey(name: Key_start_address)
  final String? start_address;
  @JsonKey(name: Key_arrive_address)
  final String? arrive_address;
  @JsonKey(name: Key_care_address)
  final String? care_address;
  @JsonKey(name: Key_pathroute_comment)
  final String? pathroute_comment;

  factory ContractInfoData.fromJson(Map<String, dynamic> json) {
    return ContractInfoData(
      contract_id: json[Key_contract_id] as int,
      contract_date: json[Key_contract_date] ?? '',
      contract_status: json[Key_contract_status] as int,
      momdady_signature: json[Key_momdady_signature] ?? '',
      momdady_signdate: json[Key_momdady_signdate] ?? '',
      momdady_first_name: json[Key_momdady_first_name] ?? '',
      linkmom_signature: json[Key_linkmom_signature] ?? '',
      linkmom_signdate: json[Key_linkmom_signdate] ?? '',
      linkmom_first_name: json[Key_linkmom_first_name] ?? '',
      linkmom_address: json[Key_linkmom_address] ?? '',
      linkmom_address_detail: json[Key_linkmom_address_detail] ?? '',
      child_name: json[Key_child_name] ?? '',
      start_address: json[Key_start_address],
      arrive_address: json[Key_arrive_address],
      care_address: json[Key_care_address],
      pathroute_comment: json[Key_pathroute_comment],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_contract_id] = this.contract_id;
    data[Key_contract_date] = this.contract_date;
    data[Key_contract_status] = this.contract_status;
    data[Key_momdady_signature] = this.momdady_signature;
    data[Key_momdady_signdate] = this.momdady_signdate;
    data[Key_momdady_first_name] = this.momdady_first_name;
    data[Key_linkmom_signature] = this.linkmom_signature;
    data[Key_linkmom_signdate] = this.linkmom_signdate;
    data[Key_linkmom_first_name] = this.linkmom_first_name;
    data[Key_linkmom_address] = this.linkmom_address;
    data[Key_linkmom_address_detail] = this.linkmom_address_detail;
    data[Key_child_name] = this.child_name;
    data[Key_start_address] = this.start_address;
    data[Key_arrive_address] = this.arrive_address;
    data[Key_care_address] = this.care_address;
    data[Key_pathroute_comment] = this.pathroute_comment;
    return data;
  }

  @override
  String toString() {
    return 'ContractInfoData{contract_id: $contract_id, contract_date: $contract_date, contract_status: $contract_status, momdady_signature: $momdady_signature, momdady_signdate: $momdady_signdate, momdady_first_name: $momdady_first_name, linkmom_signature: $linkmom_signature, linkmom_signdate: $linkmom_signdate, linkmom_first_name: $linkmom_first_name, linkmom_address: $linkmom_address, linkmom_address_detail: $linkmom_address_detail, child_name: $child_name, start_address: $start_address, arrive_address: $arrive_address, care_address: $care_address, pathroute_comment: $pathroute_comment}';
  }
}
