import 'package:json_annotation/json_annotation.dart';

part 'token_refresh.g.dart';

@JsonSerializable()
class TokenRefresh {
  static const String key_refresh = "refresh";

  TokenRefresh({
    this.refresh = ''
  });

  @JsonKey(name: key_refresh)
  final String refresh;

  factory TokenRefresh.fromJson(Map<String, dynamic> json) => _$TokenFromJson(json);

  Map<String, dynamic> toJson() => _$TokenToJson(this);

  @override
  String toString() {
    return "$refresh".toString();
  }
}
