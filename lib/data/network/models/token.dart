import 'package:json_annotation/json_annotation.dart';

part 'token.g.dart';

@JsonSerializable()
class Token {
  static const String key_username = "username";
  static const String key_password = "password";

  Token({
    this.username = '',
    this.password = '',
  });

  @JsonKey(name: key_username)
  final String username;

  @JsonKey(name: key_password)
  final String password;

  factory Token.fromJson(Map<String, dynamic> json) => _$TokenFromJson(json);

  Map<String, dynamic> toJson() => _$TokenToJson(this);

  @override
  String toString() {
    return "$username $password".toString();
  }
}
