import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/care_list_response.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class CaresWishListResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  CaresWishListResponse({
    this.dataHeader,
    this.dataList,
  });

  CaresWishListResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  CareListData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CareListData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CaresWishListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareListData> datas = [];
    try {
      log.d('『GGUMBI』>>> fromJson ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<<${json[Key_data]} ');
      datas = json[Key_data].map<CareListData>((i) => CareListData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return CaresWishListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CaresWishListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
