part of 'payload.dart';

Payload _$PayloadFromJson(Map<String, dynamic> json) {
  return Payload(
    name: json[Payload.key_name] as String,
    category: json[Payload.key_category] as String,
    action: json[Payload.key_action] as String,
    where: json[Payload.key_where] as String,
    timestamp: json[Payload.key_timestamp] as String,
  );
}

Map<String, dynamic> _$PayloadToJson(Payload instance) => <String, dynamic>{
      Payload.key_name: instance.name,
      Payload.key_category: instance.category,
      Payload.key_action: instance.action,
      Payload.key_where: instance.where,
      Payload.key_timestamp: instance.timestamp,
    };
