import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CommnunityScrapRequest {
  static const String Key_share_id = 'share_id';
  @JsonKey(name: Key_share_id)
  int shareId;

  CommnunityScrapRequest({this.shareId = 0});

  factory CommnunityScrapRequest.fromJson(Map<String, dynamic> json) {
    return CommnunityScrapRequest(shareId: json[Key_share_id]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_share_id] = this.shareId;
    return data;
  }

  @override
  String toString() {
    return 'CommnunityScrapRequest{shareId:$shareId}';
  }
}
