import 'package:json_annotation/json_annotation.dart';

class ChildDrugDeleteRequest {
  static const String Key_child = 'child';
  static const String Key_drug_id = 'drug_id';

  ChildDrugDeleteRequest({
    this.child = 0,
    this.drug_id = 0,
  });

  @JsonKey(name: Key_child)
  final int child;
  @JsonKey(name: Key_drug_id)
  final int drug_id;

  factory ChildDrugDeleteRequest.fromJson(Map<String, dynamic> json) {
    return ChildDrugDeleteRequest(
      child: json[Key_child] as int,
      drug_id: json[Key_drug_id] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_child] = this.child;
    data[Key_drug_id] = this.drug_id.toString();
    return data;
  }

  @override
  String toString() {
    return 'ChildDrugSaveReqeust{child: $child, drug_id:$drug_id}';
  }
}
