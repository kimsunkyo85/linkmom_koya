import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

class NotificationDeleteRequest {
  static const String Key_pagename = 'pagename';
  static const String Key_gubun = 'gubun';
  static const String Key_id = 'id';

  @JsonKey(name: Key_pagename)
  int pagename;
  @JsonKey(name: Key_gubun)
  int gubun;
  @JsonKey(name: Key_id)
  List<int> id;

  NotificationDeleteRequest({this.pagename = 0, this.gubun = 0, required this.id});

  factory NotificationDeleteRequest.fromJson(Map<String, dynamic> json) {
    return NotificationDeleteRequest(
      pagename: json[Key_pagename] as int,
      gubun: json[Key_gubun] as int,
      id: json[Key_id] == null ? [] : List<int>.from(json[Key_id]),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_pagename] = this.pagename;
    data[Key_gubun] = this.gubun;
    data[Key_id] = this.id;
    return data;
  }

  FormData toFormData() {
    FormData formData = FormData();
    formData.fields.add(MapEntry(Key_pagename, this.pagename.toString()));
    formData.fields.add(MapEntry(Key_gubun, this.gubun.toString()));
    id.forEach((e) {
      formData.fields.add(MapEntry(Key_id, e.toString()));
    });
    return formData;
  }

  @override
  String toString() {
    return 'NotificationDeleteRequest{pagename: $pagename, gubun: $gubun, id: $id}';
  }
}
