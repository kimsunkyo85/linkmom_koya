import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class CaresWishDeleteResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  CaresWishDeleteResponse({
    this.dataHeader,
    this.dataList,
  });

  CaresWishDeleteResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  String getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ' ';

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CaresWishDeleteResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CaresDeleteData> datas = [];
    try {
      datas = json[Key_data].map<CaresDeleteData>((i) => CaresDeleteData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CaresWishDeleteResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CaresWishDeleteResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class CaresDeleteData {
  static const String Key_message = 'message';

  CaresDeleteData({
    this.message = '',
  });

  @JsonKey(name: Key_message)
  String message;

  factory CaresDeleteData.fromJson(Map<String, dynamic> json) {
    return CaresDeleteData(
      message: json[Key_message],
    );
  }
}
