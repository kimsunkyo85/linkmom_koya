import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

class PenaltyRemoveRequest {
  static const String Key_count = 'count';
  static const String Key_penalty = 'penalty';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_penalty)
  int penalty;

  PenaltyRemoveRequest({this.count = 0, this.penalty = 0});

  factory PenaltyRemoveRequest.fromJson(Map<String, dynamic> json) {
    return PenaltyRemoveRequest(
      count: json[Key_count] as int,
      penalty: json[Key_penalty] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_penalty] = this.penalty;
    return data;
  }

  FormData toFormData() {
    FormData formData = FormData();
    formData.fields.add(MapEntry(Key_count, this.count.toString()));
    formData.fields.add(MapEntry(Key_penalty, this.penalty.toString()));
    return formData;
  }

  @override
  String toString() {
    return "PanaltyRemoveRequest {count: $count, penalty: $penalty } ";
  }
}
