import 'package:json_annotation/json_annotation.dart';

///- All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///- Created by   : platformbiz@ggumbi.com
///- version      : 1.0.0
///- see          : index_request.dart - 메인인덱스 요청
///- since        : 2021/12/01 / update:
///- [함수,메서드시]
@JsonSerializable()
class IndexRequest {
  static const String Key_gubun = 'gubun';

  IndexRequest({
    this.gubun = 0,
  });

  ///맘대디-0, 링크쌤-1
  @JsonKey(name: Key_gubun)
  final int gubun;

  factory IndexRequest.fromJson(Map<String, dynamic> json) {
    return IndexRequest(
      gubun: json[Key_gubun] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_gubun] = this.gubun;
    return data;
  }

  @override
  String toString() {
    return 'IndexRequest{gubun: $gubun}';
  }
}
