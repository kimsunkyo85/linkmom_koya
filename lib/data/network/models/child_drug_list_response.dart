import 'dart:io';

import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/child_drug_response.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

@JsonSerializable()
class ChildDrugListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ChildDrugReponseData>? dataList;

  ChildDrugListResponse({
    this.dataHeader,
    this.dataList,
  });

  ChildDrugReponseData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ChildDrugReponseData();

  List<ChildDrugReponseData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ChildDrugListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ChildDrugReponseData> datas = [];
    log.d('『GGUMBI』>>> fromJson : json[Key_data]: ${json[Key_data]},  <<< ');
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<ChildDrugReponseData>((i) => ChildDrugReponseData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return ChildDrugListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ChildDrugListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class ChildDrugListData {
  static const String Key_id = 'id';
  static const String Key_child = 'child';
  static const String Key_child_name = 'child_name';
  static const String Key_drug_date = 'drug_date';
  static const String Key_user_signature = 'user_signature';
  static const String Key_druglists = 'druglists';
  static const String Key_createdate = 'createdate';

  ChildDrugListData({this.id = 0, this.child = 0, this.child_name = '', this.drug_date = '', this.user_signature, this.druglists, this.createdate = ''});

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_child)
  int child;
  @JsonKey(name: Key_child_name)
  String child_name;
  @JsonKey(name: Key_drug_date)
  String drug_date;
  @JsonKey(name: Key_user_signature)
  File? user_signature;
  @JsonKey(name: Key_druglists)
  List<ChildDrugLists>? druglists;
  @JsonKey(name: Key_createdate)
  String createdate;

  factory ChildDrugListData.fromJson(Map<String, dynamic> json) {
    List<ChildDrugLists> datas = [];
    log.d('『GGUMBI』>>> fromJson : json[Key_data]: ${json[Key_data]},  <<< ');
    try {
      datas = json[Key_druglists] == null ? [] : json[Key_druglists].map<ChildDrugListData>((i) => ChildDrugListData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }

    return ChildDrugListData(
      id: json[Key_id] as int,
      child: json[Key_child] as int,
      child_name: json[Key_child_name] as String,
      drug_date: json[Key_drug_date] as String,
      user_signature: json[Key_user_signature] as File,
      druglists: datas,
      createdate: json[Key_createdate] as String);
  }

  @override
  String toString() {
    return 'ChildDrugListData{$Key_id: $id, $Key_child: $child, child_name: $child_name, drug_date: $drug_date, user_signature: $user_signature, druglists: $druglists, createdate: $createdate }';
  }
}

@JsonSerializable()
class ChildDrugLists {
  static const String Key_drug_id = 'drug_id';
  static const String Key_symptom = 'symptom';
  static const String Key_drug_time = 'drug_time';
  static const String Key_drug_timetype = 'drug_timetype';
  static const String Key_drug_type = 'drug_type';
  static const String Key_drug_keeping = 'drug_keeping';
  static const String Key_drug_repeat = 'drug_repeat';
  static const String Key_drug_amount_type0 = 'drug_amount_type0';
  static const String Key_drug_amount_type1 = 'drug_amount_type1';
  static const String Key_drug_amount_type2 = 'drug_amount_type2';
  static const String Key_drug_amount_type3 = 'drug_amount_type3';
  static const String Key_drug_reqmessage = 'drug_reqmessage';
  static const String Key_is_drug = 'is_drug';
  static const String Key_createdate = 'createdate';

  ChildDrugLists({this.drug_id = 0, this.symptom = '', this.drug_time = '', this.drug_timetype = 0, this.drug_type, this.drug_keeping, this.drug_repeat, this.drug_amount_type0, this.drug_amount_type1, this.drug_amount_type2, this.drug_amount_type3, this.drug_reqmessage = '', this.is_drug = false, this.createdate = '',});

  @JsonKey(name: Key_drug_id)
  int drug_id;
  @JsonKey(name: Key_symptom)
  String symptom;
  @JsonKey(name: Key_drug_time)
  String drug_time;
  @JsonKey(name: Key_drug_timetype)
  int drug_timetype;
  @JsonKey(name: Key_drug_type)
  List<int>? drug_type;
  @JsonKey(name: Key_drug_keeping)
  List<int>? drug_keeping;
  @JsonKey(name: Key_drug_repeat)
  List<String>? drug_repeat;
  @JsonKey(name: Key_drug_amount_type0)
  List<String>? drug_amount_type0;
  @JsonKey(name: Key_drug_amount_type1)
  List<String>? drug_amount_type1;
  @JsonKey(name: Key_drug_amount_type2)
  List<String>? drug_amount_type2;
  @JsonKey(name: Key_drug_amount_type3)
  List<String>? drug_amount_type3;
  @JsonKey(name: Key_drug_reqmessage)
  String drug_reqmessage;
  @JsonKey(name: Key_is_drug)
  bool is_drug;
  @JsonKey(name: Key_createdate)
  String createdate;

  factory ChildDrugLists.fromJson(Map<String, dynamic> json) {
    List<int> drugTypes = List<int>.from(json[Key_drug_type]);
    return ChildDrugLists(
      drug_id: json[Key_drug_id] ?? '',
      symptom: json[Key_symptom] ?? '',
      drug_time: json[Key_drug_time] ?? '',
      drug_timetype: json[Key_drug_timetype] as int,
      drug_type: drugTypes,
      drug_keeping: json[Key_drug_keeping] ?? [],
      drug_repeat: json[Key_drug_repeat] ?? [],
      drug_amount_type0: json[Key_drug_amount_type0] ?? [],
      drug_amount_type1: json[Key_drug_amount_type1] ?? [],
      drug_amount_type2: json[Key_drug_amount_type2] ?? [],
      drug_amount_type3: json[Key_drug_amount_type3] ?? [],
      drug_reqmessage: json[Key_drug_reqmessage] ?? '',
      is_drug: json[Key_is_drug] ?? false,
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_drug_id] = this.drug_id;
    data[Key_symptom] = this.symptom;
    data[Key_drug_time] = this.drug_time;
    data[Key_drug_timetype] = this.drug_timetype;
    data[Key_drug_type] = this.drug_type;
    data[Key_drug_keeping] = this.drug_keeping;
    data[Key_drug_repeat] = this.drug_repeat;
    data[Key_drug_amount_type0] = this.drug_amount_type0;
    data[Key_drug_amount_type1] = this.drug_amount_type1;
    data[Key_drug_amount_type2] = this.drug_amount_type2;
    data[Key_drug_amount_type3] = this.drug_amount_type2;
    data[Key_drug_reqmessage] = this.drug_reqmessage;
    data[Key_is_drug] = this.is_drug;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'ChildDrugLists{$Key_drug_id: $drug_id, $Key_symptom: $symptom, drug_time: $drug_time, drug_timetype: $drug_timetype drug_type: $drug_type, drug_keeping: $drug_keeping, drug_repeat: $drug_repeat, drug_amount_type0: $drug_amount_type0, drug_amount_type1: $drug_amount_type1, drug_amount_type2: $drug_amount_type2, drug_amount_type3: $drug_amount_type3, drug_reqmessage: $drug_reqmessage, is_drug: $is_drug, createdate: $createdate}';
  }
}
