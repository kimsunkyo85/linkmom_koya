import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CashListRequest {
  static const String Key_s_date = 's_date';
  static const String Key_e_date = 'e_date';
  static const String Key_cash_flag = 'cash_flag';

  @JsonKey(name: Key_s_date)
  String sDate;
  @JsonKey(name: Key_e_date)
  String eDate;
  @JsonKey(name: Key_cash_flag)
  List<String>? cashFlag;

  CashListRequest({this.sDate = '', this.eDate = '', this.cashFlag});

  factory CashListRequest.fromJson(Map<String, dynamic> json) {
    return CashListRequest(
      sDate: json[Key_s_date] ?? '',
      eDate: json[Key_e_date] ?? '',
      cashFlag: json[Key_cash_flag] == null ? null : List<String>.from(json[Key_cash_flag]),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_s_date] = this.sDate;
    data[Key_e_date] = this.eDate;
    if (this.cashFlag != null) data[Key_cash_flag] = this.cashFlag;
    return data;
  }

  FormData toFormData(FormData formData) {
    if (this.sDate.isNotEmpty) formData.fields.add(MapEntry(Key_s_date, sDate));
    if (this.eDate.isNotEmpty) formData.fields.add(MapEntry(Key_e_date, eDate));
    if (this.cashFlag != null && this.cashFlag!.isNotEmpty)
      cashFlag!.forEach((e) {
        formData.fields.add(MapEntry(Key_cash_flag, e));
      });
    return formData;
  }

  @override
  String toString() {
    return 'CashListRequest{sDate:$sDate, eDate:$eDate, cashFlag:$cashFlag}';
  }
}
