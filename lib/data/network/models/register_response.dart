import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/register_request.dart';
import 'package:linkmom/utils/commons.dart';

import '../../../main.dart';
import 'header_response.dart';

@JsonSerializable()
class RegisterResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList = [];

  RegisterResponse({
    this.dataHeader,
    this.dataList,
  });

  ///성공시에만 사용할것
  RegisterData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : RegisterData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory RegisterResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<dynamic> datas = [];
    try {
      if (dataHeader.msg_cd == KEY_SUCCESS) {
        datas = json[Key_data].map<RegisterData>((i) => RegisterData.fromJson(i)).toList() as List<RegisterData>;
      } else {
        datas = json[Key_data].map<RegisterRequest>((i) => RegisterRequest.fromJson(i)).toList() as List<RegisterRequest>;
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return RegisterResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'RegisterResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class RegisterData {
  static const String Key_username = 'username';
  static const String Key_first_name = 'first_name';

  RegisterData({
    this.username = '',
    this.first_name = '',
  });

  @JsonKey(name: Key_username)
  final String username;
  @JsonKey(name: Key_first_name)
  final String first_name;

  factory RegisterData.fromJson(Map<String, dynamic> json) {
    return RegisterData(
      username: json[Key_username] ?? '',
      first_name: json[Key_first_name] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_username] = this.username;
    data[Key_first_name] = this.first_name;
    return data;
  }

  @override
  String toString() {
    return 'RegisterData{username: $username, first_name: $first_name}';
  }
}
