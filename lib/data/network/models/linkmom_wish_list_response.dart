import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/data/network/models/linkmom_list_response.dart';
import 'package:linkmom/main.dart';

class LinkmomWishListResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  LinkmomWishListResponse({
    this.dataHeader,
    this.dataList,
  });

  LinkmomWishListResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  LinkMomListData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : LinkMomListData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory LinkmomWishListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<LinkMomListData> datas = [];
    try {
      log.d('『GGUMBI』>>> fromJson ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<<${json[Key_data]} ');
      datas = json[Key_data].map<LinkMomListData>((i) => LinkMomListData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return LinkmomWishListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'LinkmomWishListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
