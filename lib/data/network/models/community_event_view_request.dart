import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CommnunityEventViewRequest {
  static const String Key_id = 'id';
  @JsonKey(name: Key_id)
  int id;

  CommnunityEventViewRequest({this.id = 0});

  factory CommnunityEventViewRequest.fromJson(Map<String, dynamic> json) {
    return CommnunityEventViewRequest(id: json[Key_id]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    return data;
  }

  @override
  String toString() {
    return 'CommnunityEventViewRequest{id: $id}';
  }
}
