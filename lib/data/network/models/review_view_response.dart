import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/data/network/models/review_list_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class ReviewViewResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ReviewViewData>? dataList;

  ReviewViewResponse({
    this.dataHeader,
    this.dataList,
  });

  ReviewViewData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ReviewViewData();

  List<ReviewViewData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ReviewViewResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ReviewViewData> datas = [];
    try {
      datas = json[Key_data].map<ReviewViewData>((i) => ReviewViewData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return ReviewViewResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }
}

@JsonSerializable()
class ReviewViewData {
  static const String Key_matching = 'matching';
  static const String Key_review = 'review';
  static const String Key_momdady_name = 'momdady_name';
  static const String Key_linkmom_name = 'linkmom_name';
  static const String Key_profileimg = 'profileimg';
  static const String Key_childinfo = 'childinfo';
  static const String Key_carescheduleinfo = 'carescheduleinfo';
  static const String Key_reviewinfo = 'reviewinfo';

  ReviewViewData({this.matching = 0, this.review, this.momdady_name = '', this.linkmom_name = '', this.profileimg = '', this.childinfo = '', this.carescheduleinfo, this.reviewinfo});

  @JsonKey(name: Key_matching)
  int matching;

  @JsonKey(name: Key_review)
  List<int>? review;

  @JsonKey(name: Key_momdady_name)
  String momdady_name;

  @JsonKey(name: Key_linkmom_name)
  String linkmom_name;

  @JsonKey(name: Key_profileimg)
  String profileimg;

  @JsonKey(name: Key_childinfo)
  String childinfo;

  @JsonKey(name: Key_carescheduleinfo)
  CareScheduleInfo? carescheduleinfo;

  @JsonKey(name: Key_reviewinfo)
  List<ReviewInfo>? reviewinfo;

  factory ReviewViewData.fromJson(Map<String, dynamic> json) {
    List<int> list = [];
    List<ReviewInfo> datas = [];
    CareScheduleInfo cares = CareScheduleInfo();
    try {
      datas = json[Key_reviewinfo].map<ReviewInfo>((i) => ReviewInfo.fromJson(i)).toList();
      cares = CareScheduleInfo.fromJson(json[Key_carescheduleinfo]);
      list = List<int>.from(json[Key_review] ?? []);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return ReviewViewData(
      matching: json[Key_matching] ?? '',
      review: list,
      momdady_name: json[Key_momdady_name] ?? '',
      linkmom_name: json[Key_linkmom_name] ?? '',
      profileimg: json[Key_profileimg] ?? '',
      childinfo: json[Key_childinfo] ?? '',
      carescheduleinfo: cares,
      reviewinfo: datas,
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_matching] = this.matching;
    data[Key_review] = this.review;
    data[Key_momdady_name] = this.momdady_name;
    data[Key_linkmom_name] = this.linkmom_name;
    data[Key_profileimg] = this.profileimg;
    data[Key_childinfo] = this.childinfo;
    data[Key_carescheduleinfo] = this.carescheduleinfo;
    data[Key_reviewinfo] = this.reviewinfo;
    return data;
  }

  @override
  String toString() {
    return "ReviewViewData{$Key_matching: $matching,$Key_review: $review,$Key_momdady_name: $momdady_name,$Key_linkmom_name: $linkmom_name,$Key_profileimg: $profileimg,$Key_childinfo: $childinfo,$Key_carescheduleinfo: $carescheduleinfo,$Key_reviewinfo: $reviewinfo}";
  }
}

@JsonSerializable()
class ReviewInfo {
  static const String Key_review = 'review';
  static const String Key_review_text = 'review_text';

  ReviewInfo({this.review = 0, this.review_text = ''});

  @JsonKey(name: Key_review)
  int review = 0;
  @JsonKey(name: Key_review_text)
  String review_text = '';

  factory ReviewInfo.fromJson(Map<String, dynamic> json) {
    return ReviewInfo(
      review: json[Key_review] as int,
      review_text: json[Key_review_text] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_review] = this.review;
    data[Key_review_text] = this.review_text;
    return data;
  }

  @override
  String toString() {
    return "ReviewInfo{ $Key_review: $review, $Key_review_text: $review}";
  }
}

@JsonSerializable()
class ReviewUserInfo {
  static const String Key_name = 'name';
  static const String Key_grade = 'grade';
  static const String Key_profileimg = 'profileimg';
  static const String Key_review_level = 'review_level';
  static const String Key_favorite_level = 'favorite_level';

  @JsonKey(name: Key_name)
  String name;
  @JsonKey(name: Key_grade)
  String grade;
  @JsonKey(name: Key_profileimg)
  String profileimg;
  @JsonKey(name: Key_review_level)
  int reviewLevel;
  @JsonKey(name: Key_favorite_level)
  int favoriteLevel;

  ReviewUserInfo({this.name = '', this.grade = '', this.profileimg = '', this.reviewLevel = 0, this.favoriteLevel = 0});

  factory ReviewUserInfo.fromJson(Map<String, dynamic> json) {
    try {
      return ReviewUserInfo(
        name: json[Key_name] ?? '',
        grade: json[Key_grade] ?? '',
        profileimg: json[Key_profileimg] ?? '',
        reviewLevel: json[Key_review_level] ?? 0,
        favoriteLevel: json[Key_favorite_level] ?? 0,
      );
    } catch (e) {
      return ReviewUserInfo();
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_name] = this.name;
    data[Key_grade] = this.grade;
    data[Key_profileimg] = this.profileimg;
    data[Key_review_level] = this.reviewLevel;
    data[Key_favorite_level] = this.favoriteLevel;
    return data;
  }

  @override
  String toString() {
    return "ReviewUserInfo{'name: $name','grade: $grade','profileimg: $profileimg','reviewLevel: $reviewLevel','favoriteLevel: $favoriteLevel'}";
  }
}
