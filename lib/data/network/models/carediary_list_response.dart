import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/main.dart';

import 'data/care_diary_list_results.dart';
import 'header_response.dart';

@JsonSerializable()
class CareDiaryListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<CareDiaryListData>? dataList;

  CareDiaryListResponse({
    this.dataHeader,
    this.dataList,
  });

  CareDiaryListResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  CareDiaryListData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CareDiaryListData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareDiaryListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareDiaryListData> datas = [];
    try {
      datas = json[Key_data].map<CareDiaryListData>((i) => CareDiaryListData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareDiaryListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CareDiaryListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class CareDiaryListData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<CareDiaryListResults>? results;

  CareDiaryListData({this.count = 0, this.next = '', this.previous = '', this.results});

  factory CareDiaryListData.fromJson(Map<String, dynamic> json) {
    return CareDiaryListData(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] == null ? null : (json[Key_results] as List).map((e) => CareDiaryListResults.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    if (this.results != null) data[Key_results] = this.results!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryListData{count: $count, next: $next, previous: $previous, results: $results}';
  }
}
