import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class WithdrawalRequest {
  static const String Key_account_user = 'account_user';
  static const String Key_bank_name = 'bank_name';
  static const String Key_bank_account = 'bank_account';
  static const String Key_withdrawal_cash = 'withdrawal_cash';

  @JsonKey(name: Key_account_user)
  String accountUser;

  /// NOTE: bank code
  @JsonKey(name: Key_bank_name)
  String bankName;
  @JsonKey(name: Key_bank_account)
  String bankAccount;
  @JsonKey(name: Key_withdrawal_cash)
  int withdrawalCash;

  WithdrawalRequest({this.accountUser = '', this.bankName = '', this.bankAccount = '', this.withdrawalCash = 0});

  factory WithdrawalRequest.fromJson(Map<String, dynamic> json) {
    return WithdrawalRequest(
      accountUser: json[Key_account_user] ?? '',
      bankName: json[Key_bank_name] ?? '',
      bankAccount: json[Key_bank_account] ?? '',
      withdrawalCash: json[Key_withdrawal_cash] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_account_user] = this.accountUser;
    data[Key_bank_name] = this.bankName;
    data[Key_bank_account] = this.bankAccount;
    data[Key_withdrawal_cash] = this.withdrawalCash;
    return data;
  }

  @override
  String toString() {
    return 'WithdrawalRequest{account_user: $accountUser,bank_name: $bankName,bank_account: $bankAccount, withdrawal_cash: $withdrawalCash}';
  }
}
