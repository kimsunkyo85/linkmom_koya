import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class CareiaryPaymentResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<DiaryPaymentData> dataList;

  CareiaryPaymentResponse({
    this.dataHeader,
    required this.dataList,
  });

  DiaryPaymentData getData() => dataList.isNotEmpty ? dataList.first : DiaryPaymentData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareiaryPaymentResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<DiaryPaymentData> datas = [];
    try {
      datas = json[Key_data].map<DiaryPaymentData>((i) => DiaryPaymentData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareiaryPaymentResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'DiaryPaymentResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class DiaryPaymentData {
  static const String Key_schedule_id = 'schedule_id';
  static const String Key_pay_status = 'pay_status';
  static const String Key_pay_status_text = 'pay_status_text';
  static const String Key_servicetype = 'servicetype';
  static const String Key_catedate = 'catedate';
  static const String Key_stime = 'stime';
  static const String Key_etime = 'etime';
  static const String Key_cost_perhour = 'cost_perhour';
  static const String Key_paid_price = 'paid_price';
  static const String Key_pay_expect = 'pay_expect';
  static const String Key_commission = 'commission';
  static const String Key_paid_charge = 'paid_charge';
  static const String Key_pay_updatedate = 'pay_updatedate';
  static const String Key_penalty_expect = 'penalty_expect';
  static const Key_is_end_care = 'is_end_care';
  static const Key_is_review = 'is_review';

  @JsonKey(name: Key_schedule_id)
  int scheduleId;
  @JsonKey(name: Key_pay_status)
  int payStatus;
  @JsonKey(name: Key_pay_status_text)
  String payStatusText;
  @JsonKey(name: Key_servicetype)
  String servicetype;
  @JsonKey(name: Key_catedate)
  String caredate;
  @JsonKey(name: Key_stime)
  String stime;
  @JsonKey(name: Key_etime)
  String etime;
  @JsonKey(name: Key_cost_perhour)
  int costPerhour;
  @JsonKey(name: Key_paid_price)
  int paidPrice;
  @JsonKey(name: Key_commission)
  int commission;
  @JsonKey(name: Key_paid_charge)
  int paidCharge;
  @JsonKey(name: Key_pay_updatedate)
  String payUpdatedate;
  @JsonKey(name: Key_penalty_expect)
  int penaltyExpect;
  @JsonKey(name: Key_pay_expect)
  int payExpect;
  @JsonKey(name: Key_is_end_care)
  bool isEndCare;
  @JsonKey(name: Key_is_review)
  bool isReview;

  DiaryPaymentData({
    this.scheduleId = 0,
    this.payStatus = 0,
    this.payStatusText = '',
    this.servicetype = '',
    this.caredate = '',
    this.stime = '',
    this.etime = '',
    this.costPerhour = 0,
    this.paidPrice = 0,
    this.commission = 0,
    this.paidCharge = 0,
    this.payUpdatedate = '',
    this.penaltyExpect = 0,
    this.payExpect = 0,
    this.isEndCare = false,
    this.isReview = false,
  });

  factory DiaryPaymentData.fromJson(Map<String, dynamic> json) {
    return DiaryPaymentData(
      scheduleId: json[Key_schedule_id] ?? 0,
      payStatus: json[Key_pay_status] ?? 0,
      payStatusText: json[Key_pay_status_text] ?? '',
      servicetype: json[Key_servicetype] ?? '',
      caredate: json[Key_catedate] ?? '',
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      costPerhour: json[Key_cost_perhour] ?? 0,
      paidPrice: json[Key_paid_price] ?? 0,
      commission: json[Key_commission] ?? 0,
      paidCharge: json[Key_paid_charge] ?? 0,
      payUpdatedate: json[Key_pay_updatedate] ?? '',
      penaltyExpect: json[Key_penalty_expect] ?? 0,
      payExpect: json[Key_pay_expect] ?? 0,
      isEndCare: json[Key_is_end_care] ?? false,
      isReview: json[Key_is_review] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_schedule_id] = this.scheduleId;
    data[Key_pay_status] = this.payStatus;
    data[Key_pay_status_text] = this.payStatusText;
    data[Key_servicetype] = this.servicetype;
    data[Key_catedate] = this.caredate;
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_cost_perhour] = this.costPerhour;
    data[Key_paid_price] = this.paidPrice;
    data[Key_commission] = this.commission;
    data[Key_paid_charge] = this.paidCharge;
    data[Key_pay_updatedate] = this.payUpdatedate;
    data[Key_penalty_expect] = this.penaltyExpect;
    data[Key_pay_expect] = this.payExpect;
    data[Key_is_end_care] = this.isEndCare;
    data[Key_is_review] = this.isReview;
    return data;
  }

  @override
  String toString() {
    return 'DiaryPaymentData{schedule_id: $scheduleId, pay_status: $payStatus, pay_status_text: $payStatusText, servicetype: $servicetype, catedate: $caredate, stime: $stime, etime: $etime, cost_perhour: $costPerhour, paid_price: $paidPrice, commission: $commission, paid_charge: $paidCharge, pay_updatedate: $payUpdatedate, penaltyExpect: $penaltyExpect, isEndCare: $isEndCare, isReview: $isReview}';
  }
}
