import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';


@JsonSerializable()
class CareDiaryChildDrugViewResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<CareDiaryDrugData>? dataList;

  CareDiaryChildDrugViewResponse({
    this.dataHeader,
    this.dataList,
  });

  CareDiaryChildDrugViewResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  CareDiaryDrugData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CareDiaryDrugData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareDiaryChildDrugViewResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareDiaryDrugData> datas = [];
    try {
      datas = json[Key_data] != null && json[Key_data].isNotEmpty ? [CareDiaryDrugData.fromJson(json[Key_data])] : [];
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareDiaryChildDrugViewResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CareDiaryChildDrugViewResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class CareDiaryDrugData {
  static const String Key_child_name = 'child_name';
  static const String Key_drug_date = 'drug_date';
  static const String Key_user_signature = 'user_signature';
  static const String Key_childdrug_list = 'childdrug_list';

  @JsonKey(name: Key_child_name)
  String childName;
  @JsonKey(name: Key_drug_date)
  String drugDate;
  @JsonKey(name: Key_user_signature)
  String userSignature;
  @JsonKey(name: Key_childdrug_list)
  List<ChildDrugList>? childdrugList;

  CareDiaryDrugData({this.childName = '', this.drugDate = '', this.userSignature = '', this.childdrugList});

  factory CareDiaryDrugData.fromJson(Map<String, dynamic> json) {
    return CareDiaryDrugData(
      childName: json[Key_child_name],
      drugDate: json[Key_drug_date],
      userSignature: json[Key_user_signature],
      childdrugList: json[Key_childdrug_list] == null ? null : (json[Key_childdrug_list] as List).map((e) => ChildDrugList.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_child_name] = this.childName;
    data[Key_drug_date] = this.drugDate;
    data[Key_user_signature] = this.userSignature;
    if (this.childdrugList != null) data[Key_childdrug_list] = this.childdrugList!.map((e) => e.toJson()).toList();
    return data;
  }
}

@JsonSerializable()
class ChildDrugList {
  static const String Key_childdruglist_id = 'childdruglist_id';
  static const String Key_symptom = 'symptom';
  static const String Key_drug_time = 'drug_time';
  static const String Key_drug_timetype = 'drug_timetype';
  static const String Key_drug_type = 'drug_type';
  static const String Key_drug_amount_type0 = 'drug_amount_type0';
  static const String Key_drug_amount_type1 = 'drug_amount_type1';
  static const String Key_drug_amount_type2 = 'drug_amount_type2';
  static const String Key_drug_amount_type3 = 'drug_amount_type3';
  static const String Key_drug_repeat = 'drug_repeat';
  static const String Key_drug_keeping = 'drug_keeping';
  static const String Key_drug_reqmessage = 'drug_reqmessage';
  static const String Key_createdate = 'createdate';
  static const String Key_is_drug = 'is_drug';

  ChildDrugList({
    this.childdruglist_id = 0,
    this.symptom = '',
    this.drug_time = '',
    this.drug_timetype = '',
    required this.drug_type,
    this.drug_amount_type0 = '',
    this.drug_amount_type1 = '',
    this.drug_amount_type2 = '',
    this.drug_amount_type3 = '',
    this.drug_repeat = '',
    this.drug_keeping = 0,
    this.drug_reqmessage = '',
    this.createdate = '',
    this.is_drug = false,
  });

  @JsonKey(name: Key_childdruglist_id)
  int childdruglist_id;
  @JsonKey(name: Key_symptom)
  String symptom;
  @JsonKey(name: Key_drug_time)
  String drug_time;
  @JsonKey(name: Key_drug_timetype)
  String drug_timetype;
  @JsonKey(name: Key_drug_type)
  List<String> drug_type;
  @JsonKey(name: Key_drug_amount_type0)
  String drug_amount_type0;
  @JsonKey(name: Key_drug_amount_type1)
  String drug_amount_type1;
  @JsonKey(name: Key_drug_amount_type2)
  String drug_amount_type2;
  @JsonKey(name: Key_drug_amount_type3)
  String drug_amount_type3;
  @JsonKey(name: Key_drug_repeat)
  String drug_repeat;
  @JsonKey(name: Key_drug_keeping)
  int drug_keeping;
  @JsonKey(name: Key_drug_reqmessage)
  String drug_reqmessage;
  @JsonKey(name: Key_createdate)
  String createdate;
  @JsonKey(name: Key_is_drug)
  bool is_drug;

  factory ChildDrugList.fromJson(Map<String, dynamic> json) {
    return ChildDrugList(
      childdruglist_id: json[Key_childdruglist_id] as int,
      symptom: json[Key_symptom] ?? '',
      drug_time: json[Key_drug_time] ?? '',
      drug_type: json[Key_drug_type] != null && json[Key_drug_type].isNotEmpty ? json[Key_drug_type].replaceAll(' ', '').split(',').toList() : '',
      drug_timetype: json[Key_drug_timetype] ?? '',
      drug_amount_type0: json[Key_drug_amount_type0] ?? '',
      drug_amount_type1: json[Key_drug_amount_type1] ?? '',
      drug_amount_type2: json[Key_drug_amount_type2] ?? '',
      drug_amount_type3: json[Key_drug_amount_type3] ?? '',
      drug_repeat: json[Key_drug_repeat] ?? '',
      drug_keeping: json[Key_drug_keeping] as int,
      drug_reqmessage: json[Key_drug_reqmessage] ?? '',
      createdate: json[Key_createdate] ?? '',
      is_drug: json[Key_is_drug] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_childdruglist_id] = this.childdruglist_id;
    data[Key_symptom] = this.symptom;
    data[Key_drug_time] = this.drug_time;
    data[Key_drug_timetype] = this.drug_timetype;
    data[Key_drug_type] = this.drug_type;
    data[Key_drug_amount_type0] = this.drug_amount_type0;
    data[Key_drug_amount_type1] = this.drug_amount_type1;
    data[Key_drug_amount_type2] = this.drug_amount_type2;
    data[Key_drug_amount_type3] = this.drug_amount_type3;
    data[Key_drug_repeat] = this.drug_repeat;
    data[Key_drug_keeping] = this.drug_keeping;
    data[Key_drug_reqmessage] = this.drug_reqmessage;
    data[Key_createdate] = this.createdate;
    data[Key_is_drug] = this.is_drug;
    return data;
  }

  @override
  String toString() {
    return 'ChildDrugList{drug_id: $childdruglist_id, symptom: $symptom, drug_time: $drug_time, drug_timetype: $drug_timetype, drug_type: $drug_type, drug_amount_type0: $drug_amount_type0, drug_amount_type1: $drug_amount_type1, drug_amount_type2: $drug_amount_type2, drug_amount_type3: $drug_amount_type3, drug_repeat: $drug_repeat, drug_keeping: $drug_keeping, drug_reqmessage: $drug_reqmessage, createdate: $createdate, is_drug: $is_drug}';
  }
}
