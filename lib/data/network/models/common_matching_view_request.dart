class CommonMatchingViewRequest {
  static const String Key_matching_id = 'matching_id';
  int matchingId;

  CommonMatchingViewRequest({this.matchingId = 0});

  factory CommonMatchingViewRequest.fromJson(Map<String, dynamic> json) {
    return CommonMatchingViewRequest(matchingId: json[Key_matching_id] as int);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_matching_id] = this.matchingId;
    return data;
  }

  @override
  String toString() {
    return 'CommonMatchingViewRequest{matchingId: $matchingId}';
  }
}
