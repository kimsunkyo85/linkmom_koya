import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/data/pay_contract_info_data.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/manager/enum_manager.dart';

import '../../../main.dart';
import 'data/chat_room_data.dart';
import 'data/pay_care_diary_info_data.dart';
import 'data/pay_payment_info_data.dart';
import 'data/pay_refund_info_init_data.dart';

///- All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///- Created by   : platformbiz@ggumbi.com
///- version      : 1.0.0
///- see          : pay_cancel_init_response.dart - 결제취소 요청전
///- since        : 2021/09/09 / update:
///
///> <pay_status><br/>
/// (0, '정산 예정'),<br/>
/// (1, '정산 완료'),<br/>
/// (2, '정산 보류'),<br/>
/// (10, '돌봄취소(보상) 예정'),<br/>
/// (11, '돌봄취소(보상) 완료'),<br/>
/// (20, '돌봄취소(벌점부과) 예정'),<br/>
/// (21, '돌봄취소(벌점부과) 완료'),<br/>
/// <contract_status><br/>
/// (0, '계약중'),<br/>
/// (11, '계약실패(거절/취소/응답없음/시간만료 등)'),<br/>
/// (21, '계약완료 후 취소(부분)'),<br/>
/// (22, '계약완료 후 취소(전체)'),<br/>
/// (99, '매칭성공(계약완료/결제대기중)'),<br/>
/// (100, '결제완료')<br/>
/// <맘대디측 사유><br/>
/// (10, '[맘대디 사유] 계약중, 거절하기'),<br/>
/// (11, '[맘대디 사유] 돌봄 필요한 일정이 바뀌었어요.'),<br/>
/// (12, '[맘대디 사유] 링크쌤과 돌봄성향이 맞지 않아요.'),<br/>
/// (13, '[맘대디 사유] 질병, 사고 등으로 인해 서비스 이용이 어려워요.'),<br/>
/// (14, '[맘대디 사유] 더 이상 돌봄이 필요 없어 졌어요.'),<br/>
/// (15, '[맘대디 사유] 맘대디가 돌봄 당일 미리 연락없이 아이를 맡기지 않았어요.'),<br/>
/// (16, '[맘대디 사유] 맘대디가 취소를 요청해 왔어요.'),<br/>
/// (19, '[맘대디 사유] 기타')<br/>
/// <링크쌤측 사유><br/>
/// (20, '[링크쌤 사유] 계약중, 거절하기'),<br/>
/// (21, '[링크쌤 사유] 돌봄 활동 가능한 일정이 바뀌었어요.'),<br/>
/// (22, '[링크쌤 사유] 맘대디와 돌봄성향이 맞지 않아요.'),<br/>
/// (23, '[링크쌤 사유] 질병, 사고 등으로 인해 서비스 제공이 어려워요.'),<br/>
/// (24, '[링크쌤 사유] 당분간 돌봄 활동을 하지 않을 생각이에요.'),<br/>
/// (25, '[링크쌤 사유] 링크쌤이 돌봄 당일 연락없이 출근하지 않았어요.'),<br/>
/// (26, '[링크쌤 사유] 링크쌤이 취소를 요청해 왔어요.'),<br/>
/// (29, '[링크쌤 사유] 기타')<br/>
/// (실패)<br/>
///"msg_cd": 5400 - "message": "취소 할 수 없는 결제 입니다.",<br/>
///"msg_cd": 5401, - "message": "이미 취소된 결제 입니다.",<br/>
///"msg_cd": 4000, - "message": "데이터가 없습니다.",<br/>

@JsonSerializable()
class PayCancelInitResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PayCancelInitData>? dataList;

  PayCancelInitResponse({
    this.dataHeader,
    this.dataList,
  });

  PayCancelInitData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PayCancelInitData();

  List<PayCancelInitData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory PayCancelInitResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PayCancelInitData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<PayCancelInitData>((i) => PayCancelInitData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return PayCancelInitResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'PayCancelInitResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class PayCancelInitData {
  static const String Key_momdady = 'momdady';
  static const String Key_linkmom = 'linkmom';
  static const String Key_contract_id = 'contract_id';
  static const String Key_order_id = 'order_id';
  static const String Key_bookingcareservices_info = 'bookingcareservices_info';
  static const String Key_payment_info = 'payment_info';
  static const String Key_refund_info = 'refund_info';
  static const String Key_carediary_info = 'carediary_info';
  static const String Key_contract_info = 'contract_info';

  PayCancelInitData({
    this.momdady,
    this.linkmom,
    this.contract_id,
    this.order_id,
    this.bookingcareservices_info,
    this.payment_info,
    this.refund_info,
    this.carediary_info,
    this.isAll = false,
    this.priceTotal = 0,
    this.stickerTotal = 0,
    this.contract_info,
  });

  PayCancelInitData.clone(PayCancelInitData item)
      : this(
          momdady: item.momdady,
          linkmom: item.linkmom,
          contract_id: item.contract_id,
          order_id: item.order_id,
          bookingcareservices_info: item.bookingcareservices_info,
          payment_info: item.payment_info,
          refund_info: item.refund_info,
          carediary_info: item.carediary_info,
          isAll: item.isAll,
          priceTotal: item.priceTotal,
          stickerTotal: item.stickerTotal,
          contract_info: item.contract_info,
        );

  @JsonKey(name: Key_momdady)
  final int? momdady;
  @JsonKey(name: Key_linkmom)
  final int? linkmom;
  @JsonKey(name: Key_contract_id)
  final int? contract_id;
  @JsonKey(name: Key_order_id)
  final String? order_id;
  @JsonKey(name: Key_bookingcareservices_info)
  final ChatCareInfoData? bookingcareservices_info;
  @JsonKey(name: Key_payment_info)
  final PayMentInfoData? payment_info;
  @JsonKey(name: Key_refund_info)
  final PayRefundInfoInitData? refund_info;
  @JsonKey(name: Key_carediary_info)
  final List<PayCarediaryInfoData>? carediary_info;
  @JsonKey(name: Key_contract_info)
  final PayConractInfoData? contract_info;

  bool? isAll = false;
  int? priceTotal = 0;
  int? stickerTotal = 0;

  factory PayCancelInitData.fromJson(Map<String, dynamic> json) {
    ChatCareInfoData? careinfo = json[Key_bookingcareservices_info] == null ? ChatCareInfoData() : ChatCareInfoData.fromJson(json[Key_bookingcareservices_info]);
    PayMentInfoData? paymentInfo = json[Key_payment_info] == null ? PayMentInfoData() : PayMentInfoData.fromJson(json[Key_payment_info]);
    PayRefundInfoInitData? refundInfo = json[Key_refund_info] == null ? PayRefundInfoInitData() : PayRefundInfoInitData.fromJson(json[Key_refund_info]);
    List<PayCarediaryInfoData>? carediaryInfoData = json[Key_carediary_info] == null ? [] : json[Key_carediary_info].map<PayCarediaryInfoData>((i) => PayCarediaryInfoData.fromJson(i)).toList();
    PayConractInfoData? contractInfo = json[Key_contract_info] == null ? PayConractInfoData() : PayConractInfoData.fromJson(json[Key_contract_info]);
    return PayCancelInitData(
      momdady: json[Key_momdady] ?? '',
      linkmom: json[Key_linkmom] ?? '',
      contract_id: json[Key_contract_id] ?? '',
      order_id: json[Key_order_id] ?? '',
      bookingcareservices_info: careinfo,
      payment_info: paymentInfo,
      refund_info: refundInfo,
      carediary_info: carediaryInfoData,
      contract_info: contractInfo,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_momdady] = this.momdady;
    data[Key_linkmom] = this.linkmom;
    data[Key_contract_id] = this.contract_id;
    data[Key_order_id] = this.order_id;
    data[Key_bookingcareservices_info] = this.bookingcareservices_info;
    data[Key_payment_info] = this.payment_info;
    data[Key_refund_info] = this.refund_info;
    data[Key_carediary_info] = this.carediary_info;
    data[Key_contract_info] = this.contract_info;
    return data;
  }

  ///취소날짜를 선택한 취소 id
  List<int> getCareDiarys() {
    List<int> list = [];
    carediary_info!.forEach((e) {
      if (e.isCheck!) {
        list.add(e.carediary_id!);
      }
    });
    return list;
  }

  ///전체 환불 금액
  int getPriceTotal(PayCancelInitData data, PayViewType viewType, bool isLinkMom) {
    int totalPrice = 0;
    int sticker = 0;
    data.carediary_info!.forEach((element) {
      if (viewType == PayViewType.cancel_init) {
        if (element.isCheck!) {
          if (isLinkMom) {
            totalPrice = totalPrice + element.linkmom_charge!;
          } else {
            totalPrice = totalPrice + element.momdady_charge!;
          }
          sticker = sticker + element.linkmom_penalty!;
        }
      } else {
        //환불 내역 합계 금액
        if (element.pay_status! > PayStatus.delayed.value) {
          if (isLinkMom) {
            totalPrice = totalPrice + element.linkmom_charge!;
          } else {
            totalPrice = totalPrice + element.momdady_charge!;
          }
          sticker = sticker + element.linkmom_penalty!;
        }
      }
    });
    data.priceTotal = totalPrice;
    data.stickerTotal = sticker;
    return totalPrice;
  }

  @override
  String toString() {
    return 'PayCancelInitData{momdady: $momdady, linkmom: $linkmom, contract_id: $contract_id, order_id: $order_id, bookingcareservices_info: $bookingcareservices_info, payment_info: $payment_info, refund_info: $refund_info, carediary_info: $carediary_info, contract_info: $contract_info, isAll: $isAll, priceTotal: $priceTotal, stickerTotal: $stickerTotal}';
  }
}
