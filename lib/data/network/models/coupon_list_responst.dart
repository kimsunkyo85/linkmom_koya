import 'package:json_annotation/json_annotation.dart';

import 'header_response.dart';

class CouponListResponse {
  HeaderResponse? dataHeader;
  List<CouponList>? dataList;

  CouponListResponse({this.dataHeader, this.dataList});

  CouponList getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CouponList();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CouponListResponse.fromJson(Map<String, dynamic> json) {
    return CouponListResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => CouponList.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null && this.dataList!.isNotEmpty) data["data"] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CouponListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class CouponList {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<CouponListData>? results;

  CouponList({this.count = 0, this.next = '', this.previous = '', this.results});

  factory CouponList.fromJson(Map<String, dynamic> json) {
    return CouponList(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] == null ? null : (json[Key_results] as List).map((e) => CouponListData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    if (this.results != null) data[Key_results] = this.results!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CouponListData {count: $count, next: $next, previous: $previous, results: $results}';
  }
}

class CouponListData {
  static const Key_id = 'id';
  static const Key_coupon_brand = 'coupon_brand';
  static const Key_coupon_brand_bi = 'coupon_brand_bi';
  static const Key_coupon_name = 'coupon_name';
  static const Key_coupon_code = 'coupon_code';
  static const Key_status = 'status';
  static const Key_use_period_sdate = 'use_period_sdate';
  static const Key_use_period_edate = 'use_period_edate';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_coupon_brand)
  String couponBrand;
  @JsonKey(name: Key_coupon_brand_bi)
  String couponBrandBi;
  @JsonKey(name: Key_coupon_name)
  String couponName;
  @JsonKey(name: Key_coupon_code)
  String couponCode;
  @JsonKey(name: Key_status)
  String status;
  @JsonKey(name: Key_use_period_sdate)
  String usePeriodSdate;
  @JsonKey(name: Key_use_period_edate)
  String usePeriodEdate;

  CouponListData({this.id = 0, this.couponBrand = '', this.couponBrandBi = '', this.couponName = '', this.couponCode = '', this.status = '', this.usePeriodSdate = '', this.usePeriodEdate = ''});

  factory CouponListData.fromJson(Map<String, dynamic> json) {
    return CouponListData(
      id: json[Key_id] ?? 0,
      couponBrand: json[Key_coupon_brand] ?? '',
      couponBrandBi: json[Key_coupon_brand_bi] ?? '',
      couponName: json[Key_coupon_name] ?? '',
      couponCode: json[Key_coupon_code] ?? '',
      status: json[Key_status] ?? '',
      usePeriodSdate: json[Key_use_period_sdate] ?? '',
      usePeriodEdate: json[Key_use_period_edate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_coupon_brand] = this.couponBrand;
    data[Key_coupon_brand_bi] = this.couponBrandBi;
    data[Key_coupon_name] = this.couponName;
    data[Key_coupon_code] = this.couponCode;
    data[Key_status] = this.status;
    data[Key_use_period_sdate] = this.usePeriodSdate;
    data[Key_use_period_edate] = this.usePeriodEdate;
    return data;
  }

  @override
  String toString() {
    return 'CouponData {${{
      'id': id,
      'couponBrand': couponBrand,
      'couponBrandBi': couponBrandBi,
      'couponName': couponName,
      'couponCode': couponCode,
      'status': status,
      'usePeriodSdate': usePeriodSdate,
      'usePeriodEdate': usePeriodEdate,
    }}}';
  }
}
