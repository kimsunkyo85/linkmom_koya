import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : area_address_response.dart - 동네 인증 여부 검사
/// since        : 5/7/21 / update:
@JsonSerializable()
class AreaGpsAuthResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<AreaGpsAuthData>? dataList;

  AreaGpsAuthResponse({
    this.dataHeader,
    this.dataList,
  });

  AreaGpsAuthData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : AreaGpsAuthData();

  List<AreaGpsAuthData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory AreaGpsAuthResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<AreaGpsAuthData> datas = [];
    try {
      datas = json[Key_data].map<AreaGpsAuthData>((i) => AreaGpsAuthData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return AreaGpsAuthResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AreaAddressResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class AreaGpsAuthData {
  static const String Key_bcode = 'bcode';
  static const String Key_hcode = 'hcode';
  static const String Key_region_1depth = 'region_1depth';
  static const String Key_region_2depth = 'region_2depth';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_3depth_h = 'region_3depth_h';
  static const String Key_is_gpswithaddress = 'is_gpswithaddress';

  AreaGpsAuthData({
    this.bcode = '',
    this.hcode = '',
    this.region_1depth = '',
    this.region_2depth = '',
    this.region_3depth = '',
    this.region_3depth_h = '',
    this.is_gpswithaddress = false,
  });

  ///(선택된) 법정동코드
  @JsonKey(name: Key_bcode)
  final String bcode;

  ///(선택된) 행정동코드
  @JsonKey(name: Key_hcode)
  final String hcode;

  ///(선택된) 위치 Depth1
  @JsonKey(name: Key_region_1depth)
  final String region_1depth;

  ///(선택된) 위치 Depth2
  @JsonKey(name: Key_region_2depth)
  final String region_2depth;

  ///(선택된) 위치 Depth3
  @JsonKey(name: Key_region_3depth)
  final String region_3depth;

  ///(선택된) 위치 Depth4
  @JsonKey(name: Key_region_3depth_h)
  final String region_3depth_h;

  ///인증성공여부 (1:성공, 0:실패)
  @JsonKey(name: Key_is_gpswithaddress)
  final bool is_gpswithaddress;

  factory AreaGpsAuthData.fromJson(Map<String, dynamic> json) {
    return AreaGpsAuthData(
      bcode: json[Key_bcode] ?? '',
      hcode: json[Key_hcode] ?? '',
      region_1depth: json[Key_region_1depth] ?? '',
      region_2depth: json[Key_region_2depth] ?? '',
      region_3depth: json[Key_region_3depth] ?? '',
      region_3depth_h: json[Key_region_3depth_h] ?? '',
      is_gpswithaddress: json[Key_is_gpswithaddress] as bool,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bcode] = this.bcode;
    data[Key_hcode] = this.hcode;
    data[Key_region_1depth] = this.region_1depth;
    data[Key_region_2depth] = this.region_2depth;
    data[Key_region_3depth] = this.region_3depth;
    data[Key_region_3depth_h] = this.region_3depth_h;
    data[Key_is_gpswithaddress] = this.is_gpswithaddress;
    return data;
  }

  @override
  String toString() {
    return 'AreaGpsAuthData{bcode: $bcode, hcode: $hcode, region_1depth: $region_1depth, region_2depth: $region_2depth, region_3depth: $region_3depth, region_3depth_h: $region_3depth_h, is_gpswithaddress: $is_gpswithaddress}';
  }
}
