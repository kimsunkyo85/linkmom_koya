import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import 'data/momdady_result_data.dart';

@JsonSerializable()
class MomdadyMatchingViewResponse extends HeaderResponse {
  HeaderResponse dataHeader;
  List<MomdadyResultsData> dataList;

  MomdadyMatchingViewResponse({required this.dataHeader, required this.dataList});

  MomdadyResultsData getData() => dataList.isNotEmpty ? dataList.first : MomdadyResultsData();

  int getCode() => dataHeader.getCode();

  String getMsg() => dataHeader.getMsg();

  factory MomdadyMatchingViewResponse.fromJson(Map<String, dynamic> json) {
    return MomdadyMatchingViewResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? [] : (json[Key_data] as List).map((e) => MomdadyResultsData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_data] = this.dataList;
    return data;
  }

  @override
  String toString() {
    return 'MomdadyMatchingViewResponse{dataHeader: $dataHeader, dataList:$dataList}';
  }
}
