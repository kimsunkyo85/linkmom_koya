import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class AuthCenterCriminalRequest {
  static const String Key_name = "name";
  static const String Key_jumin = "jumin";
  static const String Key_hpnum = "hpnum";
  static const String Key_agreement_picture = "agreement_picture";

  AuthCenterCriminalRequest({
    this.name = '',
    this.jumin = '',
    this.hpnum = '',
    this.agreement_picture,
  });

  @JsonKey(name: Key_name)
  String name;
  @JsonKey(name: Key_jumin)
  String jumin;
  @JsonKey(name: Key_hpnum)
  String hpnum;
  @JsonKey(name: Key_agreement_picture)
  File? agreement_picture;

  factory AuthCenterCriminalRequest.fromJson(Map<String, dynamic> json) {
    return AuthCenterCriminalRequest(
      name: json[Key_name] ?? '',
      jumin: json[Key_jumin] ?? '',
      hpnum: json[Key_hpnum] ?? '',
      agreement_picture: json[Key_agreement_picture],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_name] = this.name;
    data[Key_jumin] = this.jumin;
    data[Key_hpnum] = this.hpnum;
    data[Key_agreement_picture] = this.agreement_picture;
    return data;
  }

  Future<FormData> toFormData(FormData formData) async {
    formData.fields.add(MapEntry(Key_name, await encryptHelper.encodeData(this.name.toString())));
    formData.fields.add(MapEntry(Key_jumin, await encryptHelper.encodeData(this.jumin.toString())));
    formData.fields.add(MapEntry(Key_hpnum, await encryptHelper.encodeData(this.hpnum.toString())));
    var file = await encryptHelper.encodeData(this.agreement_picture);
    formData.files.add(MapEntry(Key_agreement_picture, MultipartFile.fromString(file, filename: this.agreement_picture!.path.split('/').last, contentType: MediaType('image', 'jpeg'))));
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'CriminalRequest{name: $name, jumin: $jumin, hpnum: $hpnum, agreement_picture: $agreement_picture}';
  }
}
