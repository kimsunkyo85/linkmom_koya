import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

@JsonSerializable()
class MyAccountResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<MyAccountData>? dataList;

  MyAccountResponse({
    this.dataHeader,
    this.dataList,
  });

  MyAccountData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : MyAccountData();

  List<MyAccountData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory MyAccountResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<MyAccountData> datas = [];
    try {
      datas = json[Key_data].map<MyAccountData>((i) => MyAccountData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return MyAccountResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'MyAccountResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class MyAccountData {
  static const String Key_username = 'username';
  static const String Key_first_name = 'first_name';
  static const String Key_hpnumber = 'hpnumber';
  static const String Key_email = 'email';
  static const String Key_is_auth_email = 'is_auth_email';
  static const String Key_is_auth_email_send = 'is_auth_email_send';
  static const String Key_authinfo = 'authinfo';

  MyAccountData({
    this.username = '',
    this.first_name = '',
    this.hpnumber = '',
    this.email = '',
    this.is_auth_email = false,
    this.is_auth_email_send = false,
    this.authinfo,
  });

  MyAccountData.init() {
    this.username = '';
    this.first_name = '';
    this.hpnumber = '';
    this.email = '';
    this.is_auth_email = false;
    this.is_auth_email_send = false;
    this.authinfo = AuthInfo();
  }

  @JsonKey(name: Key_username)
  late String username;

  @JsonKey(name: Key_first_name)
  late String first_name;

  @JsonKey(name: Key_hpnumber)
  late String hpnumber;

  @JsonKey(name: Key_email)
  late String email;

  @JsonKey(name: Key_is_auth_email)
  late bool is_auth_email;

  @JsonKey(name: Key_is_auth_email_send)
  late bool is_auth_email_send;

  @JsonKey(name: Key_authinfo)
  late AuthInfo? authinfo;

  factory MyAccountData.fromJson(Map<String, dynamic> json) {
    AuthInfo authInfo = AuthInfo();
    try {
      authInfo = AuthInfo.fromJson(json[Key_authinfo]);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return MyAccountData(
      username: json[Key_username] ?? '',
      first_name: json[Key_first_name] ?? '',
      hpnumber: json[Key_hpnumber] ?? '',
      email: json[Key_email] ?? '',
      is_auth_email: json[Key_is_auth_email] ?? false,
      is_auth_email_send: json[Key_is_auth_email_send] ?? false,
      authinfo: authInfo,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_username] = this.username;
    data[Key_first_name] = this.first_name;
    data[Key_hpnumber] = this.hpnumber;
    data[Key_email] = this.email;
    data[Key_is_auth_email] = this.is_auth_email;
    data[Key_is_auth_email_send] = this.is_auth_email_send;
    data[Key_authinfo] = this.authinfo;
    return data;
  }

  @override
  String toString() {
    return 'MyAccountData{username: $username, first_name: $first_name, hpnumber: $hpnumber, email: $email, is_auth_email: $is_auth_email, is_auth_email_send: $is_auth_email_send, authinfo: $authinfo}';
  }
}

@JsonSerializable()
class AuthInfo {
  static const String Key_auth_id = 'auth_id';
  static const String Key_grade_authlevel = 'grade_authlevel';
  static const String Key_favorite_level = 'favorite_level';
  static const String Key_auth_date_hp = 'auth_date_hp';
  static const String Key_auth_date_address = 'auth_date_address';
  static const String Key_auth_date_homestate = 'auth_date_homestate';
  static const String Key_auth_date_deungbon = 'auth_date_deungbon';
  static const String Key_auth_date_healthy = 'auth_date_healthy';
  static const String Key_auth_date_career = 'auth_date_career';
  static const String Key_auth_date_graduated = 'auth_date_graduated';
  static const String Key_auth_date_education = 'auth_date_education';
  static const String Key_auth_date_personality = 'auth_date_personality';
  static const String Key_auth_date_covid = 'auth_date_covid';
  static const String Key_auth_date_stress = 'auth_date_stress';
  static const String Key_auth_date_criminal = 'auth_date_criminal';

  AuthInfo({
    this.auth_id = 0,
    this.grade_authlevel = 0,
    this.favorite_level = 0,
    this.auth_date_hp = '',
    this.auth_date_address = '',
    this.auth_date_homestate = '',
    this.auth_date_deungbon = '',
    this.auth_date_healthy = '',
    this.auth_date_career = '',
    this.auth_date_graduated = '',
    this.auth_date_education = '',
    this.auth_date_personality = '',
    this.auth_date_covid = '',
    this.auth_date_stress = '',
    this.auth_date_criminal = '',
  });

  @JsonKey(name: Key_auth_id)
  int auth_id;
  @JsonKey(name: Key_grade_authlevel)
  int grade_authlevel;
  @JsonKey(name: Key_favorite_level)
  int favorite_level;
  @JsonKey(name: Key_auth_date_hp)
  String auth_date_hp;
  @JsonKey(name: Key_auth_date_address)
  String auth_date_address;
  @JsonKey(name: Key_auth_date_homestate)
  String auth_date_homestate;
  @JsonKey(name: Key_auth_date_deungbon)
  String auth_date_deungbon;
  @JsonKey(name: Key_auth_date_healthy)
  String auth_date_healthy;
  @JsonKey(name: Key_auth_date_career)
  String auth_date_career;
  @JsonKey(name: Key_auth_date_graduated)
  String auth_date_graduated;
  @JsonKey(name: Key_auth_date_education)
  String auth_date_education;
  @JsonKey(name: Key_auth_date_personality)
  String auth_date_personality;
  @JsonKey(name: Key_auth_date_covid)
  String auth_date_covid;
  @JsonKey(name: Key_auth_date_stress)
  String auth_date_stress;
  @JsonKey(name: Key_auth_date_criminal)
  String auth_date_criminal;

  factory AuthInfo.fromJson(Map<String, dynamic> json) {
    return AuthInfo(
      auth_id: json[Key_auth_id] ?? 0,
      grade_authlevel: json[Key_grade_authlevel] ?? 0,
      favorite_level: json[Key_favorite_level] ?? 0,
      auth_date_hp: json[Key_auth_date_hp] ?? '',
      auth_date_address: json[Key_auth_date_address] ?? '',
      auth_date_homestate: json[Key_auth_date_homestate] ?? '',
      auth_date_deungbon: json[Key_auth_date_deungbon] ?? '',
      auth_date_healthy: json[Key_auth_date_healthy] ?? '',
      auth_date_career: json[Key_auth_date_career] ?? '',
      auth_date_graduated: json[Key_auth_date_graduated] ?? '',
      auth_date_education: json[Key_auth_date_education] ?? '',
      auth_date_personality: json[Key_auth_date_personality] ?? '',
      auth_date_covid: json[Key_auth_date_covid] ?? '',
      auth_date_stress: json[Key_auth_date_stress] ?? '',
      auth_date_criminal: json[Key_auth_date_criminal] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_auth_id] = this.auth_id;
    data[Key_grade_authlevel] = this.grade_authlevel;
    data[Key_favorite_level] = this.favorite_level;
    data[Key_auth_date_hp] = this.auth_date_hp;
    data[Key_auth_date_address] = this.auth_date_address;
    data[Key_auth_date_homestate] = this.auth_date_homestate;
    data[Key_auth_date_deungbon] = this.auth_date_deungbon;
    data[Key_auth_date_healthy] = this.auth_date_healthy;
    data[Key_auth_date_career] = this.auth_date_career;
    data[Key_auth_date_graduated] = this.auth_date_graduated;
    data[Key_auth_date_education] = this.auth_date_education;
    data[Key_auth_date_personality] = this.auth_date_personality;
    data[Key_auth_date_covid] = this.auth_date_covid;
    data[Key_auth_date_stress] = this.auth_date_stress;
    data[Key_auth_date_criminal] = this.auth_date_criminal;
    return data;
  }

  @override
  String toString() {
    return 'Authinfo{auth_id: $auth_id, grade_authlevel: $grade_authlevel, favorite_level: $favorite_level, auth_date_hp: $auth_date_hp, auth_date_address: $auth_date_address, auth_date_homestate: $auth_date_homestate, auth_date_deungbon: $auth_date_deungbon, auth_date_healthy: $auth_date_healthy, auth_date_career: $auth_date_career, auth_date_graduated: $auth_date_graduated, auth_date_education: $auth_date_education, auth_date_personality: $auth_date_personality, auth_date_covid: $auth_date_covid, auth_date_stress: $auth_date_stress, auth_date_criminal: $auth_date_criminal}';
  }
}

// }
