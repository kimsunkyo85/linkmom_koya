import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/main.dart';

import 'data/care_diary_reply_data.dart';
import 'header_response.dart';

@JsonSerializable()
class CareDiaryReplySaveResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  CareDiaryReplySaveResponse({
    this.dataHeader,
    this.dataList,
  });

  CareDiaryReplySaveResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  CareDiaryReplyData getData() => dataList!.first;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareDiaryReplySaveResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareDiaryReplyData> datas = [];
    try {
      datas = json[Key_data].map<CareDiaryReplyData>((i) => CareDiaryReplyData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareDiaryReplySaveResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CareDiaryReplySaveResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
