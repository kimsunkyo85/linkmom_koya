import 'community_list_response.dart';
import 'header_response.dart';

class CommunityUpdateResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<CommunityBoardData>? dataList;

  CommunityUpdateResponse({this.dataHeader, this.dataList});
  
  CommunityBoardData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CommunityBoardData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CommunityUpdateResponse.fromJson(Map<String, dynamic> json) {
    return CommunityUpdateResponse(
        dataHeader: HeaderResponse.fromJson(json),
        dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => CommunityBoardData.fromJson(e)).toList());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CommunityUpdateResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
