import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

///- All Rights Reserved. Copyright(c) 2022 GGUMBI CO., Ltd
///- Created by   : platformbiz@ggumbi.com
///- version      : 1.0.0
///- see          : temp_area_response.dart - 돌봄 장소 불러오기, 수정, 삭제 응답값
///- since        : 2022/02/22 / update:
///- [함수,메서드시]
@JsonSerializable()
class TempAreaResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<TempAreaData>? dataList;

  TempAreaResponse({
    this.dataHeader,
    this.dataList,
  });

  TempAreaData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : TempAreaData();

  List<TempAreaData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory TempAreaResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<TempAreaData> datas = [];
    try {
      datas = json[Key_data].map<TempAreaData>((i) => TempAreaData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return TempAreaResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'TempAreaResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class TempAreaData {
  static const String Key_area_id = 'area_id';
  static const String Key_temp_areaname = 'temp_areaname';
  static const String Key_temp_address = 'temp_address';
  static const String Key_temp_address_detail = 'temp_address_detail';
  static const String Key_createdate = 'createdate';

  TempAreaData({
    this.area_id,
    this.temp_areaname = '',
    this.temp_address = '',
    this.temp_address_detail = '',
    this.createdate,
  });

  ///id
  @JsonKey(name: Key_area_id)
  int? area_id;

  ///장소명
  @JsonKey(name: Key_temp_areaname)
  String temp_areaname;

  ///장소 주소
  @JsonKey(name: Key_temp_address)
  String temp_address;

  ///장소 주소 상세
  @JsonKey(name: Key_temp_address_detail)
  String temp_address_detail;

  ///날짜(생성,수정)
  @JsonKey(name: Key_createdate)
  String? createdate;

  bool isAddress = false;

  factory TempAreaData.fromJson(Map<String, dynamic> json) {
    return TempAreaData(
      area_id: json[Key_area_id] as int,
      temp_areaname: json[Key_temp_areaname] ?? '',
      temp_address: json[Key_temp_address] ?? '',
      temp_address_detail: json[Key_temp_address_detail] ?? '',
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_area_id] = this.area_id;
    data[Key_temp_areaname] = this.temp_areaname;
    data[Key_temp_address] = this.temp_address;
    data[Key_temp_address_detail] = this.temp_address_detail;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'TempAreaData{area_id: $area_id, temp_areaname: $temp_areaname, temp_address: $temp_address, temp_address_detail: $temp_address_detail, createdate: $createdate, isAddress: $isAddress}';
  }
}
