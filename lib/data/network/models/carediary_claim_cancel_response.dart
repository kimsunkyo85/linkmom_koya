import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/data/care_diary_claim_data.dart';
import 'package:linkmom/main.dart';

import 'header_response.dart';

@JsonSerializable()
class CareDiaryClaimCancelResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<CancelInfo>? dataList;

  CareDiaryClaimCancelResponse({
    this.dataHeader,
    this.dataList,
  });

  CareDiaryClaimCancelResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  CancelInfo getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CancelInfo();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareDiaryClaimCancelResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CancelInfo> datas = [];
    try {
      datas = json[Key_data].map<CancelInfo>((i) => CancelInfo.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareDiaryClaimCancelResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CareDiaryClaimCancelResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
class CancelInfo {
  static const String Key_id = 'id';
  static const String Key_carediary = 'carediary';
  static const String Key_type = 'type';
  static const String Key_content1 = 'content1';
  static const String Key_content2 = 'content2';
  static const String Key_createdate_after24 = 'createdate_after24';
  static const String Key_answer = 'answer';
  static const String Key_carediary_claim_images1 = 'carediary_claim_images1';
  static const String Key_carediary_claim_images2 = 'carediary_claim_images2';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_carediary)
  int carediary;
  @JsonKey(name: Key_type)
  int type;
  @JsonKey(name: Key_content1)
  String content1;
  @JsonKey(name: Key_content2)
  String content2;
  @JsonKey(name: Key_createdate_after24)
  String createdateAfter24;
  @JsonKey(name: Key_answer)
  String answer;
  @JsonKey(name: Key_carediary_claim_images1)
  List<dynamic>? carediaryClaimImages1;
  @JsonKey(name: Key_carediary_claim_images2)
  List<dynamic>? carediaryClaimImages2;

  CancelInfo({this.id = 0, this.carediary = 0, this.type = 0, this.content1 = '', this.content2 = '', this.createdateAfter24 = '', this.answer = '', this.carediaryClaimImages1, this.carediaryClaimImages2});

  factory CancelInfo.fromJson(Map<String, dynamic> json) {
    return CancelInfo(
      id: json[Key_id] ?? 0,
      carediary: json[Key_carediary] ?? 0,
      type: json[Key_type] ?? 0,
      content1: json[Key_content1] ?? '',
      content2: json[Key_content2] ?? '',
      createdateAfter24: json[Key_createdate_after24] ?? '',
      answer: json[Key_answer] ?? '',
      carediaryClaimImages1: json[Key_carediary_claim_images1] != null ? json[Key_carediary_claim_images1].map((e) => CarediaryClaimImages.fromJson(json[Key_carediary_claim_images1])).toList() : [],
      carediaryClaimImages2: json[Key_carediary_claim_images2] != null ? json[Key_carediary_claim_images2].map((e) => CarediaryClaimImages.fromJson(json[Key_carediary_claim_images2])).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_carediary] = this.carediary;
    data[Key_type] = this.type;
    data[Key_content1] = this.content1;
    data[Key_content2] = this.content2;
    data[Key_createdate_after24] = this.createdateAfter24;
    data[Key_answer] = this.answer;
    if (this.carediaryClaimImages1 != null) data[Key_carediary_claim_images1] = this.carediaryClaimImages1;
    if (this.carediaryClaimImages2 != null) data[Key_carediary_claim_images2] = this.carediaryClaimImages2;
    return data;
  }
}
