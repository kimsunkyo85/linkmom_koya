import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class PayListRequest {
  static const String Key_s_date = 's_date';
  static const String Key_e_date = 'e_date';
  static const String Key_payment_flag = 'payment_flag';

  @JsonKey(name: Key_s_date)
  String sDate;
  @JsonKey(name: Key_e_date)
  String eDate;
  @JsonKey(name: Key_payment_flag)
  List<String>? paymentFlag;

  PayListRequest({this.sDate = '', this.eDate = '', this.paymentFlag});

  factory PayListRequest.fromJson(Map<String, dynamic> json) {
    return PayListRequest(
      sDate: json[Key_s_date] ?? '',
      eDate: json[Key_e_date] ?? '',
      paymentFlag: json[Key_payment_flag] == null ? [] : List<String>.from(json[Key_payment_flag]),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_s_date] = this.sDate;
    data[Key_e_date] = this.eDate;
    if (this.paymentFlag != null) data[Key_payment_flag] = this.paymentFlag;
    return data;
  }


  FormData toFormData(FormData formData) {
    if (this.sDate.isNotEmpty) formData.fields.add(MapEntry(Key_s_date, sDate));
    if (this.eDate.isNotEmpty) formData.fields.add(MapEntry(Key_e_date, eDate));
    if (this.paymentFlag != null && this.paymentFlag!.isNotEmpty)
      paymentFlag!.forEach((e) {
        formData.fields.add(MapEntry(Key_payment_flag, e));
      });
    return formData;
  }

  @override
  String toString() {
    return 'PayListRequest{sDate:$sDate, eDate:$eDate, paymentFlag:$paymentFlag}';
  }
}
