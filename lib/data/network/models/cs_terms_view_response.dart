import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

@JsonSerializable()
class CsTermsViewResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<TermsViewData>? dataList;

  CsTermsViewResponse({this.dataHeader, this.dataList});

  TermsViewData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : TermsViewData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CsTermsViewResponse.fromJson(Map<String, dynamic> json) {
    return CsTermsViewResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => TermsViewData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CsTermsViewResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class TermsViewData {
  static const String Key_id = 'id';
  static const String Key_title = 'title';
  static const String Key_content = 'content';
  static const String Key_createdate = 'createdate';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_title)
  String title;
  @JsonKey(name: Key_content)
  String content;
  @JsonKey(name: Key_createdate)
  String createdate;

  TermsViewData({this.title = '', this.id = 0, this.createdate = '', this.content = ''});

  factory TermsViewData.fromJson(Map<String, dynamic> json) {
    return TermsViewData(
      id: json[Key_id] as int,
      title: json[Key_title] ?? '',
      content: json[Key_content] ?? '',
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_title] = this.title;
    data[Key_content] = this.content;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'Results{id: $id, title: $title, createdate: $createdate}';
  }
}
