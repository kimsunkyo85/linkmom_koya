import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/data/network/models/pay_list_response.dart';

@JsonSerializable()
class CashListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<CashList> dataList;

  CashListResponse({this.dataHeader, required this.dataList});

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.message;

  CashList getData() => dataList.isNotEmpty ? dataList.first : CashList(results: []);

  factory CashListResponse.fromJson(Map<String, dynamic> json) {
    return CashListResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] != null && json[Key_data].isNotEmpty ? (json[Key_data] as List).map((e) => CashList.fromJson(e)).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_data] = this.dataList.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CashListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class CashList {
  static const String Key_sum_cash = 'sum_cash';
  static const String Key_fire_cash = 'fire_cash';
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  @JsonKey(name: Key_sum_cash)
  int sumCash;
  @JsonKey(name: Key_fire_cash)
  int fireCash;
  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<CashData> results;

  CashList({this.sumCash = 0, this.fireCash = 0, this.count = 0, this.next = '', this.previous = '', required this.results});

  factory CashList.fromJson(Map<String, dynamic> json) {
    return CashList(
      sumCash: json[Key_sum_cash] ?? 0,
      fireCash: json[Key_fire_cash] ?? 0,
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] == null ? [] : (json[Key_results] as List).map((e) => CashData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_sum_cash] = this.sumCash;
    data[Key_fire_cash] = this.fireCash;
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CashList{sumCash $sumCash, fireCash $fireCash, count $count, next $next, previous $previous, results $results}';
  }
}

class CashData {
  static const String Key_cash_id = 'cash_id';
  static const String Key_cash_flag = 'cash_flag';
  static const String Key_contract_id = 'contract_id';
  static const String Key_order_id = 'order_id';
  static const String Key_used_cash = 'used_cash';
  static const String Key_cash_reason = 'cash_reason';
  static const String Key_cash_expiredate = 'cash_expiredate';
  static const String Key_createdate = 'createdate';
  static const String Key_payment_info = 'payment_info';
  static const String Key_schedule_paid = 'schedule_paid';

  @JsonKey(name: Key_cash_id)
  int cashId;
  @JsonKey(name: Key_cash_flag)
  int cashFlag;
  @JsonKey(name: Key_contract_id)
  int contractId;
  @JsonKey(name: Key_order_id)
  String orderId;
  @JsonKey(name: Key_used_cash)
  int usedCash;
  @JsonKey(name: Key_cash_reason)
  String cashReason;
  @JsonKey(name: Key_cash_expiredate)
  String cashExpiredate;
  @JsonKey(name: Key_createdate)
  String createdate;
  @JsonKey(name: Key_payment_info)
  PayData paymentInfo;
  @JsonKey(name: Key_schedule_paid)
  SchedulePaid? schedulePaid;

  CashData({
    this.cashId = 0,
    this.cashFlag = 0,
    this.contractId = 0,
    this.orderId = '',
    this.usedCash = 0,
    this.cashReason = '',
    this.cashExpiredate = '',
    this.createdate = '',
    required this.paymentInfo,
    this.schedulePaid,
  });

  factory CashData.fromJson(Map<String, dynamic> json) {
    return CashData(
      cashId: json[Key_cash_id] ?? 0,
      cashFlag: json[Key_cash_flag] ?? 0,
      contractId: json[Key_contract_id] ?? 0,
      orderId: json[Key_order_id] ?? '',
      usedCash: json[Key_used_cash] ?? 0,
      cashReason: json[Key_cash_reason] ?? '',
      cashExpiredate: json[Key_cash_expiredate] ?? '',
      createdate: json[Key_createdate] ?? '',
      paymentInfo: json[Key_payment_info] == null ? PayData() : PayData.fromJson(json[Key_payment_info]),
      schedulePaid: json[Key_schedule_paid] == null ? SchedulePaid() : SchedulePaid.fromJson(json[Key_schedule_paid]),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_cash_id] = this.cashId;
    data[Key_cash_flag] = this.cashFlag;
    data[Key_contract_id] = this.contractId;
    data[Key_order_id] = this.orderId;
    data[Key_used_cash] = this.usedCash;
    data[Key_cash_reason] = this.cashReason;
    data[Key_cash_expiredate] = this.cashExpiredate;
    data[Key_createdate] = this.createdate;
    data[Key_payment_info] = this.paymentInfo;
    data[Key_schedule_paid] = this.schedulePaid;
    return data;
  }

  @override
  String toString() {
    return 'Results{cashId: $cashId, cashFlag: $cashFlag, contractId: $contractId, orderId: $orderId, usedCash: $usedCash, cashReason: $cashReason, cashExpiredate: $cashExpiredate, createdate: $createdate, paymentInfo: $paymentInfo, schedulePaid: $schedulePaid}';
  }
}

class SchedulePaid {
  static const String Key_schedule_id = 'schedule_id';
  static const String Key_start_caredate = 'start_caredate';
  static const String Key_end_caredate = 'end_caredate';
  static const String Key_pay_status = 'pay_status';
  static const String Key_paid_price = 'paid_price';
  static const String Key_commission_fee = 'commission_fee';
  static const String Key_paid_charge = 'paid_charge';
  static const String Key_pay_updatedate = 'pay_updatedate';

  @JsonKey(name: Key_schedule_id)
  int scheduleId;
  @JsonKey(name: Key_start_caredate)
  String startCaredate;
  @JsonKey(name: Key_end_caredate)
  String endCaredate;
  @JsonKey(name: Key_pay_status)
  int payStatus;
  @JsonKey(name: Key_paid_price)
  int paidPrice;
  @JsonKey(name: Key_commission_fee)
  int commissionFee;
  @JsonKey(name: Key_paid_charge)
  int paidCharge;
  @JsonKey(name: Key_pay_updatedate)
  String payUpdatedate;

  SchedulePaid({
    this.scheduleId = 0,
    this.payStatus = 0,
    this.startCaredate = '',
    this.endCaredate = '',
    this.paidPrice = 0,
    this.commissionFee = 0,
    this.paidCharge = 0,
    this.payUpdatedate = '',
  });

  factory SchedulePaid.fromJson(Map<String, dynamic> json) {
    return SchedulePaid(
      scheduleId: json[Key_schedule_id] ?? 0,
      payStatus: json[Key_paid_price] ?? 0,
      startCaredate: json[Key_start_caredate] ?? '',
      endCaredate: json[Key_end_caredate] ?? '',
      paidPrice: json[Key_paid_price] ?? 0,
      commissionFee: json[Key_commission_fee] ?? 0,
      paidCharge: json[Key_paid_charge] ?? 0,
      payUpdatedate: json[Key_pay_updatedate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_schedule_id] = this.scheduleId;
    data[Key_paid_price] = this.payStatus;
    data[Key_start_caredate] = this.startCaredate;
    data[Key_end_caredate] = this.endCaredate;
    data[Key_paid_price] = this.paidPrice;
    data[Key_commission_fee] = this.commissionFee;
    data[Key_paid_charge] = this.paidCharge;
    data[Key_pay_updatedate] = this.payUpdatedate;
    return data;
  }

  @override
  String toString() {
    return 'SchedulePaid{schedule_id: $scheduleId, pay_status: $payStatus, stime: $startCaredate, etime: $endCaredate, paid_price: $paidPrice, commission: $commissionFee, paid_charge: $paidCharge, pay_updatedate: $payUpdatedate}';
  }
}
