import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/view/main/payment/model/custom_data.dart';

///#### All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///#### Created by   : platformbiz@ggumbi.com
///#### version      : 1.0.0
///#### see          : pay_save_request.dart - 결제요청후 저장 PUT
///#### since        : 2021/09/08 / update:
///- 6000 - 이미 결제된 내역 입니다.
///- 6592 - 주문번호가 잘못되었습니다.
///- 6593 - 결제가 완료되지 않았습니다.
///- 3000 - 등록에 실패하였습니다.
///- (실패)
/// 6599|결제정보 누락. 확인 후 다시 결제해주세요.

@JsonSerializable()
class PaySaveRequest {
  static const String Key_imp_uid = 'imp_uid';
  static const String Key_merchant_uid = 'merchant_uid';
  static const String Key_status = 'status';
  static const String Key_reqdata = 'reqdata';

  PaySaveRequest({
    this.imp_uid,
    this.merchant_uid,
    this.status,
    this.reqdata,
  });

  ///아임포트 결제번호
  @JsonKey(name: Key_imp_uid)
  final String? imp_uid;

  ///주문번호
  @JsonKey(name: Key_merchant_uid)
  final String? merchant_uid;

  ///결제상태 (완료시-paid, 실패-ready)
  @JsonKey(name: Key_status)
  final String? status;

  ///결제 금액이 0인 경우 데이터 세팅
  @JsonKey(name: Key_reqdata)
  final String? reqdata;

  factory PaySaveRequest.fromJson(Map<String, dynamic> json) {
    return PaySaveRequest(
      imp_uid: json[Key_imp_uid] ?? '',
      merchant_uid: json[Key_merchant_uid] ?? '',
      status: json[Key_status] ?? '',
      reqdata: json[Key_reqdata] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_imp_uid] = this.imp_uid;
    data[Key_merchant_uid] = this.merchant_uid;
    data[Key_status] = this.status;
    data[Key_reqdata] = this.reqdata;
    return data;
  }

  Future<FormData> toFormData() async {
    FormData formData = FormData();
    if (imp_uid != null) {
      formData.fields.add(MapEntry(Key_imp_uid, imp_uid.toString()));
    }
    if (merchant_uid != null) {
      formData.fields.add(MapEntry(Key_merchant_uid, merchant_uid.toString()));
    }
    if (status != null) {
      formData.fields.add(MapEntry(Key_status, status.toString()));
    }
    if (reqdata != null) {
      formData.fields.add(MapEntry(Key_reqdata, reqdata.toString()));
    }
    return formData;
  }

  @override
  String toString() {
    return 'PaySaveRequest{imp_uid: $imp_uid, merchant_uid: $merchant_uid, status: $status, reqdata: $reqdata}';
  }
}

class PaySaveReqData {
  static const String Key_amount = 'amount';
  static const String Key_custom_data = 'custom_data';
  static const String Key_imp_uid = 'imp_uid';
  static const String Key_merchant_uid = 'merchant_uid';
  static const String Key_name = 'name';
  static const String Key_paid_at = 'paid_at';
  static const String Key_pay_method = 'pay_method';
  static const String Key_status = 'status';

  PaySaveReqData({
    this.amount = 0,
    this.custom_data,
    this.imp_uid = '',
    this.merchant_uid = '',
    this.name = '',
    this.paid_at = '',
    this.pay_method = '',
    this.status = '',
  });

  ///결제금액
  @JsonKey(name: Key_amount)
  final int? amount;

  ///PG사 Custom Data
  @JsonKey(name: Key_custom_data)
  final CustomData? custom_data;

  ///결제번호
  @JsonKey(name: Key_imp_uid)
  final String? imp_uid;

  ///결제번호
  @JsonKey(name: Key_merchant_uid)
  final String? merchant_uid;

  ///주문명
  @JsonKey(name: Key_name)
  final String? name;

  ///결제일시
  @JsonKey(name: Key_paid_at)
  final String? paid_at;

  ///결제수단
  @JsonKey(name: Key_pay_method)
  final String? pay_method;

  ///결제여부
  @JsonKey(name: Key_status)
  final String? status;

  factory PaySaveReqData.fromJson(Map<String, dynamic> json) {
    CustomData? customData = json[Key_custom_data] == null ? CustomData() : CustomData.fromJson(json[Key_custom_data]);
    return PaySaveReqData(
      amount: json[Key_amount] as int,
      custom_data: customData,
      imp_uid: json[Key_imp_uid] ?? '',
      merchant_uid: json[Key_merchant_uid] ?? '',
      name: json[Key_name] ?? '',
      paid_at: json[Key_paid_at] ?? '',
      pay_method: json[Key_pay_method] ?? '',
      status: json[Key_status] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_amount] = this.amount;
    data[Key_custom_data] = this.custom_data;
    data[Key_imp_uid] = this.imp_uid;
    data[Key_merchant_uid] = this.merchant_uid;
    data[Key_name] = this.name;
    data[Key_paid_at] = this.paid_at;
    data[Key_pay_method] = this.pay_method;
    data[Key_status] = this.status;
    return data;
  }

  @override
  String toString() {
    return 'PaySaveReqData{amount: $amount, custom_data: $custom_data, imp_uid: $imp_uid, merchant_uid: $merchant_uid, name: $name, paid_at: $paid_at, pay_method: $pay_method, status: $status}';
  }
}
