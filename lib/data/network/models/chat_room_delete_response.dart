import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/chat_room_data.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_room_delete_response.dart - 채팅 방 리스트 삭제 응답
/// since        : 2021/07/15 / update:
@JsonSerializable()
class ChatRoomDeleteResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ChatRoomDeleteData>? dataList;

  ChatRoomDeleteResponse({
    this.dataHeader,
    this.dataList,
  });

  ChatRoomDeleteData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ChatRoomDeleteData();

  List<ChatRoomDeleteData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ChatRoomDeleteResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ChatRoomDeleteData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<ChatRoomDeleteData>((i) => ChatRoomDeleteData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return ChatRoomDeleteResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ChatRoomInitResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class ChatRoomDeleteData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  ChatRoomDeleteData({
    this.count = 0,
    this.next = '',
    this.previous = '',
    this.results,
  });

  @JsonKey(name: Key_count)
  final int count;
  @JsonKey(name: Key_next)
  final String next;
  @JsonKey(name: Key_previous)
  final String previous;
  @JsonKey(name: Key_results)
  final List<ChatRoomDeleteResultsData>? results;

  factory ChatRoomDeleteData.fromJson(Map<String, dynamic> json) {
    List<ChatRoomDeleteResultsData> datas = json[Key_results].map<ChatRoomDeleteResultsData>((i) => ChatRoomDeleteResultsData.fromJson(i)).toList();
    return ChatRoomDeleteData(
      count: json[Key_count] as int,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: datas,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results;
    return data;
  }

  @override
  String toString() {
    return 'ChatRoomDeleteData{count: $count, next: $next, previous: $previous, results: $results}';
  }
}

///삭제 데이터 정보
class ChatRoomDeleteResultsData {
  static const String Key_id = 'id';
  static const String Key_chattingroom = 'chattingroom';
  static const String Key_sender = 'sender';
  static const String Key_sender_info = 'sender_info';
  static const String Key_receiver = 'receiver';
  static const String Key_receiver_info = 'receiver_info';
  static const String Key_msg = 'msg';
  static const String Key_image = 'image';
  static const String Key_sendtime = 'sendtime';
  static const String Key_sendtimestamp = 'sendtimestamp';
  static const String Key_is_receive = 'is_receive';

  ChatRoomDeleteResultsData({
    this.id = 0,
    this.chattingroom = 0,
    this.sender = 0,
    this.sender_info,
    this.receiver = 0,
    this.receiver_info,
    this.msg = '',
    this.image = '',
    this.sendtime = '',
    this.sendtimestamp = '',
    this.is_receive = '',
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_chattingroom)
  final int chattingroom;
  @JsonKey(name: Key_sender)
  final int sender;
  @JsonKey(name: Key_sender_info)
  final ChatMessageInfoData? sender_info;
  @JsonKey(name: Key_receiver)
  final int receiver;
  @JsonKey(name: Key_receiver_info)
  final ChatMessageInfoData? receiver_info;
  @JsonKey(name: Key_msg)
  final String msg;
  @JsonKey(name: Key_image)
  final String image;
  @JsonKey(name: Key_sendtime)
  final String sendtime;
  @JsonKey(name: Key_sendtimestamp)
  final String sendtimestamp;
  @JsonKey(name: Key_is_receive)
  final String is_receive;

  factory ChatRoomDeleteResultsData.fromJson(Map<String, dynamic> json) {
    ChatMessageInfoData senderInfo = ChatMessageInfoData.fromJson(json[Key_sender_info]);
    ChatMessageInfoData receiverInfo = ChatMessageInfoData.fromJson(json[Key_receiver_info]);
    return ChatRoomDeleteResultsData(
      id: json[Key_id] as int,
      chattingroom: json[Key_chattingroom] as int,
      sender: json[Key_sender] as int,
      sender_info: senderInfo,
      receiver: json[Key_receiver] as int,
      receiver_info: receiverInfo,
      msg: json[Key_msg] ?? '',
      image: json[Key_image] ?? '',
      sendtime: json[Key_sendtime] ?? '',
      sendtimestamp: json[Key_sendtimestamp] ?? '',
      is_receive: json[Key_is_receive] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_chattingroom] = this.chattingroom;
    data[Key_sender] = this.sender;
    data[Key_sender_info] = this.sender_info;
    data[Key_receiver] = this.receiver;
    data[Key_receiver_info] = this.receiver_info;
    data[Key_msg] = this.msg;
    data[Key_image] = this.image;
    data[Key_sendtime] = this.sendtime;
    data[Key_sendtimestamp] = this.sendtimestamp;
    data[Key_is_receive] = this.is_receive;
    return data;
  }

  @override
  String toString() {
    return 'ChatRoomDeleteResultsData{id: $id, chattingroom: $chattingroom, sender: $sender, sender_info: $sender_info, receiver: $receiver, receiver_info: $receiver_info, msg: $msg, image: $image, sendtime: $sendtime, sendtimestamp: $sendtimestamp, is_receive: $is_receive}';
  }
}
