import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class ChildInfoDeleteRequest {
  static const String Key_child_id = 'child_id';

  ChildInfoDeleteRequest({
    this.child_id = 0
  });

  @JsonKey(name: Key_child_id)
  int child_id;

  factory ChildInfoDeleteRequest.fromJson(Map<String, dynamic> json) {
    return ChildInfoDeleteRequest(
      child_id: json[Key_child_id] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_child_id] = this.child_id;
    return data;
  }

  @override
  String toString() {
    return 'ChildInfoRequest{child_id: $child_id}';
  }
}
