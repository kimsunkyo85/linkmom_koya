import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryFormRequest {
  static const String Key_schedule_id = 'schedule_id';

  CareDiaryFormRequest({this.schedule_id = 0});

  @JsonKey(name: Key_schedule_id)
  final int schedule_id;

  factory CareDiaryFormRequest.fromJson(Map<String, dynamic> json) {
    return CareDiaryFormRequest(
      schedule_id: json[Key_schedule_id] as int,
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_schedule_id] = this.schedule_id;
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryFormRequest{schedule_id:$schedule_id}';
  }
}
