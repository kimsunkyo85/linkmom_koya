import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

@JsonSerializable()
class CsContactTypeResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<List<int>>? dataList;

  CsContactTypeResponse({this.dataHeader, this.dataList});

  factory CsContactTypeResponse.fromJson(Map<String, dynamic> json) {
    return CsContactTypeResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? null : List<List<int>>.from(json[Key_data]),
    );
  }

  int getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first.first : 0;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  @override
  String toString() {
    return 'CsContactTypeResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
