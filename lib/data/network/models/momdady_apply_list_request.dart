import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : momdady_apply_list_request.dart - 나에게지원한 / 내가 지원한 내역
/// since        : 2021/06/30 / update:
@JsonSerializable()
class MomdadyApplyListRequest {
  ///지원 주체 - 나에게지원한 : byme - 내가지원한 : tome
  static const String Key_bywho = 'bywho';

  ///구인상태 (0: 구인중, 1:구인종료)
  static const String Key_booking_id = 'booking_id';

  MomdadyApplyListRequest({
    this.bywho = '',
    this.booking_id = 0,
  });

  @JsonKey(name: Key_bywho)
  final String bywho;

  @JsonKey(name: Key_booking_id)
  final int booking_id;

  factory MomdadyApplyListRequest.fromJson(Map<String, dynamic> json) {
    return MomdadyApplyListRequest(
      bywho: json[Key_bywho] ?? '',
      booking_id: json[Key_booking_id] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bywho] = this.bywho;
    data[Key_booking_id] = this.booking_id;
    return data;
  }

  FormData toFormData(FormData formData) {
    formData.fields.add(MapEntry(Key_bywho, bywho.toString()));
    formData.fields.add(MapEntry(Key_booking_id, this.booking_id.toString()));
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'MomdadyApplyListRequest{bywho: $bywho, booking_id: $booking_id}';
  }
}
