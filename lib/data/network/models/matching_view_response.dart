import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

import 'data/matching_init_view_data.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : matching_view_response.dart - 계약서 조회 응답
/// since        : 2021/08/03 / update:
/// (0, '매칭중(대화중)'), (11, '매칭실패(링크쌤 응답없음)'), (12, '매칭실패(맘대디 응답없음)'), (13, '매칭실패(링크쌤 거절)'), (14, '매칭실패(맘대디 거절)'), (21, '매칭취소(링크쌤 계약취소)'), (22, '매칭취소(맘대디 계약취소)'), (23, '매칭취소(결제시간만료)'), (91, '매칭대기(링크쌤 계약대기)'), (92, '매칭대기(맘대디 계약대기)'), (99, '매칭성공(계약완료/결제대기중)') (100, '결제완료') (0, '없음(매칭상태 확인)'), (1, '돌봄예정'), (2, '돌봄 중'), (3, '돌봄종료(정상종료)'), (4, '돌봄취소(부분종료&일부취소)'), (5, '돌봄취소(전체취소)') (0, '계약중'), (11, '계약실패(거절/취소/응답없음/시간만료 등)'), (21, '계약완료 후 취소(부분)'), (22, '계약완료 후 취소(전체)'), (99, '매칭성공(계약완료/결제대기중)'), (100, '결제완료')
@JsonSerializable()
class MatchingViewResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<MatchingInitViewData>? dataList;

  MatchingViewResponse({
    this.dataHeader,
    this.dataList,
  });

  MatchingInitViewData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : MatchingInitViewData();

  List<MatchingInitViewData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  bool isData() => dataList!.isNotEmpty;

  factory MatchingViewResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<MatchingInitViewData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<MatchingInitViewData>((i) => MatchingInitViewData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return MatchingViewResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'MatchingViewResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
