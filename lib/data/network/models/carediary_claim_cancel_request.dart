import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryClaimCancelRequest {
  static const String Key_claim_id = 'claim_id';

  CareDiaryClaimCancelRequest({this.claim_id = 0});

  @JsonKey(name: Key_claim_id)
  final int claim_id;

  factory CareDiaryClaimCancelRequest.fromJson(Map<String, dynamic> json) {
    return CareDiaryClaimCancelRequest(claim_id: json[Key_claim_id]);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_claim_id] = this.claim_id;
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryClaimCancelRequest{claim_id: $claim_id}';
  }
}
