import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/page_data.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_main_response.dart - 채팅 방 리스트 조회하기(링크쌤, 맘대디, 일반채팅)
/// since        : 2021/07/16 / update:
@JsonSerializable()
class ChatMainResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PageData>? dataList;

  ChatMainResponse({
    this.dataHeader,
    this.dataList,
  });

  PageData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PageData(results: []);

  List<PageData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ChatMainResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PageData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<PageData>((i) => PageData.fromJson(i, '$ChatMainResponse')).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return ChatMainResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ChatRoomUpdateResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
