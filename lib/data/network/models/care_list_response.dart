import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/utils/string_util.dart';

import '../../../main.dart';
import 'data/care_child_info_data.dart';

@JsonSerializable()
class CareListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<CareListData>? dataList;

  CareListResponse({
    this.dataHeader,
    this.dataList,
  });

  CareListData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CareListData();

  List<CareListData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareListData> datas = [];
    try {
      datas = json[Key_data].map<CareListData>((i) => CareListData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return CareListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CareListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class CareListData {
  ///리스트 갯수
  static const String Key_count = 'count';

  ///다음 페이지 url
  static const String Key_next = 'next';

  ///이전 페이지 url
  static const String Key_previous = 'previous';

  ///돌봄신청 데이터
  static const String Key_results = 'results';

  CareListData({
    this.count = 0,
    this.next = '',
    this.previous = '',
    this.results,
  });

  ///리스트 갯수
  @JsonKey(name: Key_count)
  int count;

  ///다음 페이지 url
  @JsonKey(name: Key_next)
  String next;

  ///이전 페이지 url
  @JsonKey(name: Key_previous)
  String previous;

  ///돌봄신청 데이터
  @JsonKey(name: Key_results)
  List<CareListResultsData>? results;

  factory CareListData.fromJson(Map<String, dynamic> json) {
    List<CareListResultsData> results = json[Key_results] == null ? [] : json[Key_results].map<CareListResultsData>((i) => CareListResultsData.fromJson(i)).toList();
    return CareListData(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: results,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results;
    return data;
  }

  @override
  String toString() {
    return 'CareListData{count: $count, next: $next, previous: $previous, results: $results}';
  }
}

///돌봄신청 리스트 아이템 데이터
class CareListResultsData {
  static const String Key_booking_id = 'booking_id';
  static const String Key_cost_sum = 'cost_sum';
  static const String Key_servicetype = 'servicetype';
  static const String Key_possible_area = 'possible_area';
  static const String Key_stime = 'stime';
  static const String Key_etime = 'etime';
  static const String Key_caredate = 'caredate';
  static const String Key_userinfo = 'userinfo';
  static const String Key_childinfo = 'childinfo';

  CareListResultsData({
    this.booking_id = 0,
    this.cost_sum = 0,
    this.servicetype = '',
    this.possible_area = '',
    this.stime = '',
    this.etime = '',
    required this.caredate,
    this.userinfo,
    this.childinfo,
  });

  @JsonKey(name: Key_booking_id)
  final int booking_id;
  @JsonKey(name: Key_cost_sum)
  final int cost_sum;
  @JsonKey(name: Key_servicetype)
  final String servicetype;
  @JsonKey(name: Key_possible_area)
  final String possible_area;
  @JsonKey(name: Key_stime)
  final String stime;
  @JsonKey(name: Key_etime)
  final String etime;
  @JsonKey(name: Key_caredate)
  final List<String> caredate;
  @JsonKey(name: Key_userinfo)
  final CareUserInfoData? userinfo;
  @JsonKey(name: Key_childinfo)
  final CareChildInfoData? childinfo;

  factory CareListResultsData.fromJson(Map<String, dynamic> json) {
    List<String> caredate = json[Key_caredate] == null ? [] : List<String>.from(json[Key_caredate]);
    CareUserInfoData userinfo = CareUserInfoData.fromJson(json[Key_userinfo]);
    CareChildInfoData childinfo = CareChildInfoData.fromJson(json[Key_childinfo]);
    return CareListResultsData(
      booking_id: json[Key_booking_id] as int,
      cost_sum: json[Key_cost_sum] as int,
      servicetype: json[Key_servicetype] ?? '',
      possible_area: json[Key_possible_area] ?? '',
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      caredate: caredate,
      userinfo: userinfo,
      childinfo: childinfo,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_booking_id] = this.booking_id;
    data[Key_cost_sum] = this.cost_sum;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_caredate] = this.caredate;
    data[Key_userinfo] = this.userinfo;
    data[Key_childinfo] = this.childinfo;
    return data;
  }

  @override
  String toString() {
    return 'CareListResultsData{booking_id: $booking_id, cost_sum: $cost_sum, servicetype: $servicetype, possible_area: $possible_area, stime: $stime, etime: $etime, caredate: $caredate, userinfo: $userinfo, childinfo: $childinfo}';
  }
}

///나의정보
class CareUserInfoData {
  static const String Key_user_id = 'user_id';
  static const String Key_first_name = 'first_name';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_4depth = 'region_4depth';
  static const String Key_is_affiliate = 'is_affiliate';
  static const String Key_profileimg = 'profileimg';
  static const String Key_is_follower = 'is_follower';

  CareUserInfoData({
    this.user_id = 0,
    this.first_name = '',
    this.region_3depth = '',
    this.region_4depth = '',
    this.is_affiliate = false,
    this.profileimg = '',
    this.is_follower = false,
  });

  @JsonKey(name: Key_user_id)
  final int user_id;
  @JsonKey(name: Key_first_name)
  final String first_name;
  @JsonKey(name: Key_region_3depth)
  final String region_3depth;
  @JsonKey(name: Key_region_4depth)
  final String region_4depth;

  ///제휴 인증 여부 true, false
  @JsonKey(name: Key_is_affiliate)
  final bool is_affiliate;
  @JsonKey(name: Key_profileimg)
  final String profileimg;
  @JsonKey(name: Key_is_follower)
  bool is_follower;

  factory CareUserInfoData.fromJson(Map<String, dynamic> json) {
    return CareUserInfoData(
      user_id: json[Key_user_id] as int,
      first_name: json[Key_first_name] ?? '',
      region_3depth: json[Key_region_3depth] ?? '',
      region_4depth: json[Key_region_4depth] ?? '',
      is_affiliate: json[Key_is_affiliate] ?? false,
      profileimg: json[Key_profileimg] ?? '',
      is_follower: json[Key_is_follower] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_user_id] = this.user_id;
    data[Key_first_name] = this.first_name;
    data[Key_region_3depth] = this.region_3depth;
    data[Key_region_4depth] = this.region_4depth;
    data[Key_is_affiliate] = this.is_affiliate;
    data[Key_profileimg] = this.profileimg;
    data[Key_is_follower] = this.is_follower;
    return data;
  }

  String getAddress() {
    return StringUtils.getAddressParser(region_3depth, region_4depth);
  }

  @override
  String toString() {
    return 'CareUserInfoData{user_id: $user_id, first_name: $first_name, region_3depth: $region_3depth, region_4depth: $region_4depth, is_affiliate: $is_affiliate, profileimg: $profileimg, is_follower: $is_follower}';
  }
}
