import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

@JsonSerializable()
class CsResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<NoticeListData>? dataList;

  CsResponse({this.dataHeader, this.dataList});

  NoticeListData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : NoticeListData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CsResponse.fromJson(Map<String, dynamic> json) {
    return CsResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => NoticeListData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CsNoticeResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class NoticeListData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<NoticeResult>? results;

  NoticeListData({this.count = 0, this.next = '', this.previous = '', this.results});

  factory NoticeListData.fromJson(Map<String, dynamic> json) {
    return NoticeListData(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] == null ? null : (json[Key_results] as List).map((e) => NoticeResult.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    if (this.results != null) data[Key_results] = this.results!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'NoticeListData {count: $count, next: $next, previous: $previous, results: $results}';
  }
}

class NoticeResult {
  static const String Key_title = 'title';
  static const String Key_content = 'content';
  static const String Key_createdate = 'createdate';

  @JsonKey(name: Key_title)
  String title;
  @JsonKey(name: Key_content)
  String content;
  @JsonKey(name: Key_createdate)
  String createdate;

  NoticeResult({this.title = '', this.content = '', this.createdate = ''});

  factory NoticeResult.fromJson(Map<String, dynamic> json) {
    return NoticeResult(
      title: json[Key_title] ?? '',
      content: json[Key_content] ?? '',
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_title] = this.title;
    data[Key_content] = this.content;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'Results{title: $title, content: $content, createdate: $createdate}';
  }
}
