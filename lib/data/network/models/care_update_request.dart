import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : care_update_request.dart - 돌봄신청 수정
/// since        : 4/13/21 / update:
@JsonSerializable()
class CareUpdateRequest {
  ///아이정보
  static const String Key_child = 'child';

  ///돌봄유형(등원:0, 하원:1, 보육:2, 학습:3, 입주가사:4, 이유식:5)
  static const String Key_servicetype = 'servicetype';

  ///돌봄장소(우리집:1, 이웃집:2)
  static const String Key_possible_area = 'possible_area';

  ///예약번호 ID
  static const String Key_booking_id = 'booking_id';

  ///돌봄이동방법, 추가서비스 등
  static const String Key_reqdata = 'reqdata';

  CareUpdateRequest({
    this.child = 0,
    this.servicetype = 0,
    this.possible_area = 0,
    this.booking_id = 0,
    this.reqdata = '',
  });

  ///아이정보
  @JsonKey(name: Key_child)
  int child;

  ///돌봄유형(등원:0, 하원:1, 보육:2, 학습:3, 입주가사:4, 이유식:5)
  @JsonKey(name: Key_servicetype)
  int servicetype;

  ///돌봄장소(우리집:1, 이웃집:2)
  @JsonKey(name: Key_possible_area)
  int possible_area;

  ///돌봄이동방법, 추가서비스 등
  @JsonKey(name: Key_booking_id)
  int booking_id;

  ///돌봄이동방법, 추가서비스 등
  @JsonKey(name: Key_reqdata)
  String? reqdata;

  factory CareUpdateRequest.fromJson(Map<String, dynamic> json) {
    return CareUpdateRequest(
      child: json[Key_child] as int,
      servicetype: json[Key_servicetype] as int,
      possible_area: json[Key_possible_area] as int,
      booking_id: json[Key_booking_id] as int,
      reqdata: json[Key_reqdata] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_child] = this.child;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_booking_id] = this.booking_id;
    data[Key_reqdata] = this.reqdata;
    return data;
  }

  @override
  String toString() {
    return 'CareUpdateRequest{child: $child, servicetype: $servicetype, possible_area: $possible_area, booking_id: $booking_id, reqdata: $reqdata}';
  }
}
