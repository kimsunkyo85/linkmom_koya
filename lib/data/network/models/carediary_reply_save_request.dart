import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryReplySaveRequest {
  static const String Key_carediary = 'carediary';
  static const String Key_writer_gubun = 'writer_gubun';
  static const String Key_reply = 'reply';
  static const String Key_parent = 'parent';

  CareDiaryReplySaveRequest(
      {this.carediary = 0, this.writer_gubun = 0, this.reply = '', this.parent = 0});

  @JsonKey(name: Key_carediary)
  final int carediary;
  @JsonKey(name: Key_writer_gubun)
  final int writer_gubun;
  @JsonKey(name: Key_reply)
  final String reply;
  @JsonKey(name: Key_parent)
  final int parent;

  factory CareDiaryReplySaveRequest.fromJson(Map<String, dynamic> json) {
    return CareDiaryReplySaveRequest(
      carediary: json[Key_carediary] ?? 0,
      writer_gubun: json[Key_writer_gubun] ?? 0,
      reply: json[Key_reply] ?? '',
      parent: json[Key_parent] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_carediary] = this.carediary;
    data[Key_writer_gubun] = this.writer_gubun;
    data[Key_reply] = this.reply;
    if (this.parent != 0) data[Key_parent] = this.parent;
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryReplySaveRequest{carediary: $carediary writer_gubun: $writer_gubun reply: $reply parent: $parent}';
  }
}
