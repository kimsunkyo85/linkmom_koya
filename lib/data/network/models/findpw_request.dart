import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : findpw_request.dart - 비밀번호 찾기 (모바일, 이메일)
/// since        : 1/18/21 / update:
@JsonSerializable()
class FindPwRequest {
  static const String Key_nice_di = "nice_di";
  static const String Key_username = "username";

  FindPwRequest({
    this.nice_di = '',
    this.username = '',
  });

  @JsonKey(name: Key_nice_di)
  String nice_di;
  @JsonKey(name: Key_username)
  String username;

  factory FindPwRequest.fromJson(Map<String, dynamic> json) {
    return FindPwRequest(
      nice_di: json[Key_nice_di] ?? '',
      username: json[Key_username] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_nice_di] = this.nice_di;
    data[Key_username] = this.username;
    return data;
  }

  @override
  String toString() {
    return 'FindPwRequest{nice_di: $nice_di, username: $username}';
  }
}
