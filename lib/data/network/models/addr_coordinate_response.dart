import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class AddrCoordinateResponse {
  static const String Key_meta = "meta";
  static const String Key_documents = "documents";

  @JsonKey(name: Key_meta)
  Meta? meta;
  @JsonKey(name: Key_documents)
  List<Documents>? documents;

  AddrCoordinateResponse({this.meta, this.documents});

  AddrCoordinateResponse.fromJson(Map<String, dynamic> json) {
    this.meta = json[Key_meta] == null ? Meta() : Meta.fromJson(json[Key_meta]);
    this.documents = json[Key_documents] == null ? [] : (json[Key_documents] as List).map((e) => Documents.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.meta != null) data[Key_meta] = this.meta!.toJson();
    if (this.documents != null) data[Key_documents] = this.documents!.map((e) => e.toJson()).toList();
    return data;
  }
}

class Documents {
  static const String Key_region_type = 'region_type';
  static const String Key_code = 'code';
  static const String Key_address_name = 'address_name';
  static const String Key_region_1depth_name = 'region_1depth_name';
  static const String Key_region_2depth_name = 'region_2depth_name';
  static const String Key_region_3depth_name = 'region_3depth_name';
  static const String Key_region_4depth_name = 'region_4depth_name';
  static const String Key_x = 'x';
  static const String Key_y = 'y';

  @JsonKey(name: Key_region_type)
  String regionType;
  @JsonKey(name: Key_code)
  String code;
  @JsonKey(name: Key_address_name)
  String addressName;
  @JsonKey(name: Key_region_1depth_name)
  String region1DepthName;
  @JsonKey(name: Key_region_2depth_name)
  String region2DepthName;
  @JsonKey(name: Key_region_3depth_name)
  String region3DepthName;
  @JsonKey(name: Key_region_4depth_name)
  String region4DepthName;
  @JsonKey(name: Key_x)
  double x;
  @JsonKey(name: Key_y)
  double y;

  Documents({this.regionType = '', this.code = '', this.addressName = '', this.region1DepthName = '', this.region2DepthName = '', this.region3DepthName = '', this.region4DepthName = '', this.x = 0, this.y = 0});

  factory Documents.fromJson(Map<String, dynamic> json) {
    return Documents(
      regionType: json[Key_region_type] ?? '',
      code: json[Key_code] ?? '',
      addressName: json[Key_address_name] ?? '',
      region1DepthName: json[Key_region_1depth_name] ?? '',
      region2DepthName: json[Key_region_2depth_name] ?? '',
      region3DepthName: json[Key_region_3depth_name] ?? '',
      region4DepthName: json[Key_region_4depth_name] ?? '',
      x: json[Key_x] ?? 0,
      y: json[Key_y] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_region_type] = this.regionType;
    data[Key_code] = this.code;
    data[Key_address_name] = this.addressName;
    data[Key_region_1depth_name] = this.region1DepthName;
    data[Key_region_2depth_name] = this.region2DepthName;
    data[Key_region_3depth_name] = this.region3DepthName;
    data[Key_region_4depth_name] = this.region4DepthName;
    data[Key_x] = this.x;
    data[Key_y] = this.y;

    return data;
  }
}

class Meta {
  static const String Key_total_count = "total_count";
  @JsonKey(name: Key_total_count)
  int totalCount;

  Meta({this.totalCount = 0});

  factory Meta.fromJson(Map<String, dynamic> json) {
    return Meta(
      totalCount: json[Key_total_count] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_total_count] = this.totalCount;
    return data;
  }
}
