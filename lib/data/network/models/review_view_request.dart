import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class ReviewViewRequest {
  static const String Key_matching = 'matching';
  static const String Key_gubun = 'gubun';

  ReviewViewRequest({this.matching = 0, this.gubun = 0});

  @JsonKey(name: Key_matching)
  int matching;
  @JsonKey(name: Key_gubun)
  int gubun;

  factory ReviewViewRequest.fromJson(Map<String, dynamic> json) {
    return ReviewViewRequest();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_matching] = this.matching;
    data[Key_gubun] = this.gubun;
    return data;
  }

  FormData toFormData(FormData form) {
    form.fields.add(MapEntry(Key_matching, this.matching.toString()));
    form.fields.add(MapEntry(Key_gubun, this.gubun.toString()));
    return form;
  }

  @override
  String toString() {
    return 'ReviewViewRequest{$Key_matching:$matching, $Key_gubun:$gubun}';
  }
}
