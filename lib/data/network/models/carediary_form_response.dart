import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/main.dart';

import 'data/care_diary_data.dart';
import 'diary_view_response.dart';
import 'header_response.dart';

@JsonSerializable()
class CareDiaryFormResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<CareDiaryFormData>? dataList;

  CareDiaryFormResponse({
    this.dataHeader,
    this.dataList,
  });

  CareDiaryFormResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  CareDiaryFormData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CareDiaryFormData(caredate: []);

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareDiaryFormResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareDiaryFormData> datas = [];
    try {
      datas = json[Key_data].map<CareDiaryFormData>((i) => CareDiaryFormData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareDiaryFormResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CareDiaryFormResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class CareDiaryFormData {
  static const String Key_carecontract_schedule = 'carecontract_schedule';
  static const String Key_cost_sum = 'cost_sum';
  static const String Key_pay_status = 'pay_status';
  static const String Key_servicetype = 'servicetype';
  static const String Key_possible_area = 'possible_area';
  static const String Key_stime = 'stime';
  static const String Key_etime = 'etime';
  static const String Key_caredate = 'caredate';
  static const String Key_userinfo = 'userinfo';
  static const String Key_childinfo = 'childinfo';
  static const String Key_carediary_info = 'carediary_info';
  static const String Key_carediary_images = "carediary_images";
  static const String Key_claim_info = "claim_info";
  static const String Key_bookingcareservices = "bookingcareservices";
  static const String Key_matching = "matching";

  @JsonKey(name: Key_carecontract_schedule)
  int carecontractSchedule;
  @JsonKey(name: Key_cost_sum)
  int costSum;
  @JsonKey(name: Key_pay_status)
  String payStatus;
  @JsonKey(name: Key_servicetype)
  String servicetype;
  @JsonKey(name: Key_possible_area)
  String possibleArea;
  @JsonKey(name: Key_stime)
  String stime;
  @JsonKey(name: Key_etime)
  String etime;
  @JsonKey(name: Key_caredate)
  List<String> caredate;
  @JsonKey(name: Key_userinfo)
  Userinfo? userinfo;
  @JsonKey(name: Key_childinfo)
  Childinfo? childinfo;
  @JsonKey(name: Key_carediary_info)
  CarediaryInfo? carediaryInfo;
  @JsonKey(name: Key_carediary_images)
  List<CarediaryImages>? carediaryImages;
  @JsonKey(name: Key_claim_info)
  ClaimInfo? claimInfo;
  @JsonKey(name: Key_bookingcareservices)
  int bookingcareservices;
  @JsonKey(name: Key_matching)
  int matching;

  CareDiaryFormData({
    this.carecontractSchedule = 0,
    this.costSum = 0,
    this.payStatus = '',
    this.servicetype = '',
    this.possibleArea = '',
    this.stime = '',
    this.etime = '',
    required this.caredate,
    this.userinfo,
    this.childinfo,
    this.carediaryInfo,
    this.carediaryImages,
    this.claimInfo,
    this.bookingcareservices = 0,
    this.matching = 0,
  });

  factory CareDiaryFormData.fromJson(Map<String, dynamic> json) {
    return CareDiaryFormData(
      carecontractSchedule: json[Key_carecontract_schedule] ?? 0,
      costSum: json[Key_cost_sum] ?? 0,
      payStatus: json[Key_pay_status] ?? '',
      servicetype: json[Key_servicetype] ?? '',
      possibleArea: json[Key_possible_area] ?? '',
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      caredate: List.from(json[Key_caredate] ?? []),
      userinfo: json[Key_userinfo] != null ? Userinfo.fromJson(json[Key_userinfo]) : Userinfo(),
      childinfo: json[Key_childinfo] != null ? Childinfo.fromJson(json[Key_childinfo]) : Childinfo(),
      carediaryInfo: json[Key_carediary_info] != null ? CarediaryInfo.fromJson(json[Key_carediary_info]) : CarediaryInfo(),
      carediaryImages: json[Key_carediary_images] != null ? json[Key_carediary_images].map<CarediaryImages>((e) => CarediaryImages.fromJson(e)).toList() : [],
      claimInfo: json[Key_claim_info] == null ? ClaimInfo() : ClaimInfo.fromJson(json[Key_claim_info]),
      bookingcareservices: json[Key_bookingcareservices] ?? 0,
      matching: json[Key_matching] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_carecontract_schedule] = this.carecontractSchedule;
    data[Key_cost_sum] = this.costSum;
    data[Key_pay_status] = this.payStatus;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possibleArea;
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_caredate] = this.caredate;
    data[Key_userinfo] = this.userinfo;
    data[Key_childinfo] = this.childinfo;
    data[Key_carediary_info] = this.carediaryInfo;
    data[Key_carediary_images] = this.carediaryImages;
    data[Key_claim_info] = this.claimInfo;
    data[Key_bookingcareservices] = this.bookingcareservices;
    data[Key_matching] = this.matching;
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryFormData${{
      Key_carecontract_schedule: this.carecontractSchedule,
      Key_cost_sum: this.costSum,
      Key_pay_status: this.payStatus,
      Key_servicetype: this.servicetype,
      Key_possible_area: this.possibleArea,
      Key_stime: this.stime,
      Key_etime: this.etime,
      Key_caredate: this.caredate,
      Key_userinfo: this.userinfo,
      Key_childinfo: this.childinfo,
      Key_carediary_info: this.carediaryInfo,
      Key_carediary_images: this.carediaryImages,
      Key_claim_info: this.claimInfo,
      Key_bookingcareservices: this.bookingcareservices,
      Key_matching: this.matching,
    }}';
  }
}

@JsonSerializable()
class CarediaryInfo {
  static const String Key_id = 'id';
  static const String Key_content = 'content';
  static const String Key_is_servicetype = 'is_servicetype';
  static const String Key_is_childdrug = 'is_childdrug';
  static const String Key_option = 'option';
  static const String Key_complete_option = 'complete_option';
  static const String Key_confirm_date = 'confirm_date';
  static const String Key_updatedate = 'updatedate';
  static const String Key_childdrug_list = 'childdrug_list';
  static const String Key_childdrug_id = 'childdrug_id';
  static const String Key_momdady_name = 'momdady_name';
  static const String Key_linkmom_name = 'linkmom_name';
  static const String Key_linkmom_thumbnail = 'linkmom_thumbnail';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_content)
  String content;
  @JsonKey(name: Key_is_servicetype)
  int isServicetype;
  @JsonKey(name: Key_option)
  Option? option;
  @JsonKey(name: Key_complete_option)
  CompleteOption? completeOption;
  @JsonKey(name: Key_confirm_date)
  String confirmDate;
  @JsonKey(name: Key_updatedate)
  String updatedate;
  @JsonKey(name: Key_childdrug_list)
  List<ChildDrugInfo>? childdrugList;
  @JsonKey(name: Key_childdrug_id)
  int childdrugId;
  @JsonKey(name: Key_momdady_name)
  String momdadyName;
  @JsonKey(name: Key_linkmom_name)
  String linkmomName;
  @JsonKey(name: Key_linkmom_thumbnail)
  String linkmomThumbnail;

  CarediaryInfo(
      {this.id = 0,
      this.content = '',
      this.isServicetype = 0,
      this.childdrugList,
      this.childdrugId = 0,
      this.option,
      this.completeOption,
      this.confirmDate = '',
      this.updatedate = '',
      this.linkmomName = '',
      this.momdadyName = '',
      this.linkmomThumbnail = ''});

  factory CarediaryInfo.fromJson(Map<String, dynamic> json) {
    return CarediaryInfo(
      id: json[Key_id] ?? 0,
      isServicetype: json[Key_is_servicetype] ?? 0,
      option: json[Key_option] != null ? Option.fromJson(json[Key_option]) : Option(boyukoptions: [], homecareoptions: [], playoptions: []),
      completeOption: json[Key_complete_option] != null ? CompleteOption.fromJson(json[Key_complete_option]) : CompleteOption(completeBoyuk: [], completeHomecare: [], completePlay: []),
      confirmDate: json[Key_confirm_date] ?? "",
      updatedate: json[Key_updatedate] ?? "",
      content: json[Key_content] ?? "",
      childdrugList: json[Key_childdrug_list] != null ? json[Key_childdrug_list].map<ChildDrugInfo>((e) => ChildDrugInfo.fromJson(e)).toList() : [],
      childdrugId: json[Key_childdrug_id] ?? 0,
      momdadyName: json[Key_momdady_name] ?? '',
      linkmomName: json[Key_linkmom_name] ?? '',
      linkmomThumbnail: json[Key_linkmom_thumbnail] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_is_servicetype] = this.isServicetype;
    data[Key_option] = this.option;
    data[Key_complete_option] = this.completeOption;
    data[Key_confirm_date] = this.confirmDate;
    data[Key_updatedate] = this.updatedate;
    data[Key_childdrug_list] = this.childdrugList;
    data[Key_childdrug_id] = this.childdrugId;
    data[Key_momdady_name] = this.momdadyName;
    data[Key_linkmom_name] = this.linkmomName;
    data[Key_linkmom_thumbnail] = this.linkmomThumbnail;
    return data;
  }

  @override
  String toString() {
    return 'CarediaryInfo{id: $id, isServicetype: $isServicetype, option: $option, completeOption: $completeOption, confirmDate: $confirmDate, updatedate: $updatedate, childdrugId: $childdrugList, childdrugId: $childdrugId, momdadyName: $momdadyName, linkmomName: $linkmomName, linkmomThumbnail: $linkmomThumbnail}';
  }
}

@JsonSerializable()
class CompleteOption {
  static const String Key_complete_boyuk = 'complete_boyuk';
  static const String Key_complete_homecare = 'complete_homecare';
  static const String Key_complete_play = 'complete_play';

  @JsonKey(name: Key_complete_boyuk)
  List<int> completeBoyuk;
  @JsonKey(name: Key_complete_homecare)
  List<int> completeHomecare;
  @JsonKey(name: Key_complete_play)
  List<int> completePlay;

  CompleteOption({required this.completeBoyuk, required this.completeHomecare, required this.completePlay});

  factory CompleteOption.fromJson(Map<String, dynamic> json) {
    List<dynamic> care = json[Key_complete_boyuk] != null && json[Key_complete_boyuk].isNotEmpty && json[Key_complete_boyuk] != 'null' ? json[Key_complete_boyuk].split(',').map((e) => int.parse(e)).toList() : [];
    List<dynamic> home = json[Key_complete_homecare] != null && json[Key_complete_homecare].isNotEmpty && json[Key_complete_homecare] != 'null' ? json[Key_complete_homecare].split(',').map((e) => int.parse(e)).toList() : [];
    List<dynamic> play = json[Key_complete_play] != null && json[Key_complete_play].isNotEmpty && json[Key_complete_play] != 'null' ? json[Key_complete_play].split(',').map((e) => int.parse(e)).toList() : [];
    return CompleteOption(
      completeBoyuk: List.from(care),
      completeHomecare: List.from(home),
      completePlay: List.from(play),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_complete_boyuk] = this.completeBoyuk;
    data[Key_complete_homecare] = this.completeHomecare;
    data[Key_complete_play] = this.completePlay;
    return data;
  }

  @override
  String toString() {
    return 'CompleteOption${{
      Key_complete_boyuk: this.completeBoyuk,
      Key_complete_homecare: this.completeHomecare,
      Key_complete_play: this.completePlay,
    }}';
  }
}

@JsonSerializable()
class Option {
  static const String Key_boyukoptions = 'boyukoptions';
  static const String Key_homecareoptions = 'homecareoptions';
  static const String Key_playoptions = 'playoptions';

  @JsonKey(name: Key_boyukoptions)
  List<int> boyukoptions;
  @JsonKey(name: Key_homecareoptions)
  List<int> homecareoptions;
  @JsonKey(name: Key_playoptions)
  List<int> playoptions;

  Option({required this.boyukoptions, required this.homecareoptions, required this.playoptions});

  factory Option.fromJson(Map<String, dynamic> json) {
    List<dynamic> care = json[Key_boyukoptions] != null && json[Key_boyukoptions].isNotEmpty ? json[Key_boyukoptions].split(',').map((e) => int.parse(e)).toList() : [];
    List<dynamic> home = json[Key_homecareoptions] != null && json[Key_homecareoptions].isNotEmpty ? json[Key_homecareoptions].split(',').map((e) => int.parse(e)).toList() : [];
    List<dynamic> play = json[Key_playoptions] != null && json[Key_playoptions].isNotEmpty ? json[Key_playoptions].split(',').map((e) => int.parse(e)).toList() : [];
    return Option(
      boyukoptions: List.from(care),
      homecareoptions: List.from(home),
      playoptions: List.from(play),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_boyukoptions] = this.boyukoptions;
    data[Key_homecareoptions] = this.homecareoptions;
    data[Key_playoptions] = this.playoptions;
    return data;
  }

  @override
  String toString() {
    return 'Option${{
      Key_boyukoptions: this.boyukoptions,
      Key_homecareoptions: this.homecareoptions,
      Key_playoptions: this.playoptions,
    }}';
  }
}

@JsonSerializable()
class Childinfo {
  static const String Key_child_name = 'child_name';
  static const String Key_child_age = 'child_age';
  static const String Key_child_gender = 'child_gender';

  @JsonKey(name: Key_child_name)
  String childName;
  @JsonKey(name: Key_child_age)
  String childAge;
  @JsonKey(name: Key_child_gender)
  String childGender;

  Childinfo({this.childName = '', this.childAge = '', this.childGender = ''});

  factory Childinfo.fromJson(Map<String, dynamic> json) {
    return Childinfo(
      childName: json[Key_child_name] ?? "",
      childAge: json[Key_child_age] ?? "",
      childGender: json[Key_child_gender] ?? "",
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_child_name] = this.childName;
    data[Key_child_age] = this.childAge;
    data[Key_child_gender] = this.childGender;
    return data;
  }

  @override
  String toString() {
    return "Childinfo${{
      Key_child_name: this.childName,
      Key_child_age: this.childAge,
      Key_child_gender: this.childGender,
    }}";
  }
}

@JsonSerializable()
class Userinfo {
  static const String Key_user_id = 'user_id';
  static const String Key_first_name = 'first_name';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_4depth = 'region_4depth';
  static const String Key_profileimg = 'profileimg';

  @JsonKey(name: Key_user_id)
  int userId;
  @JsonKey(name: Key_first_name)
  String firstName;
  @JsonKey(name: Key_region_3depth)
  String region3Depth;
  @JsonKey(name: Key_region_4depth)
  String region4Depth;
  @JsonKey(name: Key_profileimg)
  String profileimg;

  Userinfo({this.userId = 0, this.firstName = '', this.region3Depth = '', this.region4Depth = '', this.profileimg = ''});

  factory Userinfo.fromJson(Map<String, dynamic> json) {
    return Userinfo(
      userId: json[Key_user_id] ?? "",
      firstName: json[Key_first_name] ?? "",
      region3Depth: json[Key_region_3depth] ?? "",
      region4Depth: json[Key_region_4depth] ?? "",
      profileimg: json[Key_profileimg] ?? "",
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_user_id] = this.userId;
    data[Key_first_name] = this.firstName;
    data[Key_region_3depth] = this.region3Depth;
    data[Key_region_4depth] = this.region4Depth;
    data[Key_profileimg] = this.profileimg;
    return data;
  }
}

class ChildDrugInfo {
  static const String Key_id = "id";
  static const String Key_is_drug = "is_drug";

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_is_drug)
  bool isDrug;

  ChildDrugInfo({this.id = 0, this.isDrug = false});

  factory ChildDrugInfo.fromJson(Map<String, dynamic> json) {
    return ChildDrugInfo(
      id: json[Key_id] ?? 0,
      isDrug: json[Key_is_drug] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_is_drug] = this.isDrug;
    return data;
  }
}
