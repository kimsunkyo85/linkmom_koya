import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/utils/string_util.dart';

import '../../../main.dart';

@JsonSerializable()
class AreaCodeSearchResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<AreaCodeSearchData>? dataList;

  AreaCodeSearchResponse({
    this.dataHeader,
    this.dataList,
  });

  AreaCodeSearchData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : AreaCodeSearchData();

  List<AreaCodeSearchData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory AreaCodeSearchResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<AreaCodeSearchData> datas = [];
    try {
      datas = json[Key_data].map<AreaCodeSearchData>((i) => AreaCodeSearchData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return AreaCodeSearchResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AuthCenterHomeResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class AreaCodeSearchData {
  static const String Key_bcode = 'bcode';
  static const String Key_hcode = 'hcode';
  static const String Key_address1 = 'address1';
  static const String Key_address2 = 'address2';
  static const String Key_address3 = 'address3';
  static const String Key_area_name = 'area_name';

  AreaCodeSearchData({
    this.bcode = '',
    this.hcode = '',
    this.address1 = '',
    this.address2 = '',
    this.address3 = '',
    this.area_name = '',
  });

  @JsonKey(name: Key_bcode)
  final String bcode;
  @JsonKey(name: Key_hcode)
  final String hcode;
  @JsonKey(name: Key_address1)
  final String address1;
  @JsonKey(name: Key_address2)
  final String address2;
  @JsonKey(name: Key_address3)
  final String address3;
  @JsonKey(name: Key_area_name)
  final String area_name;

  factory AreaCodeSearchData.fromJson(Map<String, dynamic> json) {
    return AreaCodeSearchData(
      bcode: json[Key_bcode] ?? '',
      hcode: json[Key_hcode] ?? '',
      address1: json[Key_address1] ?? '',
      address2: json[Key_address2] ?? '',
      address3: json[Key_address3] ?? '',
      area_name: json[Key_area_name] ?? '',
    );
  }

  // TODO: GGUMBI 2022/06/23 - DB쪽에서 내려줄때 반대로 되어있음, 행정동/법정동 -> address3 (광교2동): area_name (하동) 클라이언트에서 반대로 세팅 Search_Util -> AreaAddressData 에서 반대로 세팅하도록 되어있음 getAreaData
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bcode] = this.bcode;
    data[Key_hcode] = this.hcode;
    data[Key_address1] = this.address1;
    data[Key_address2] = this.address2;
    data[Key_address3] = this.address3;
    data[Key_area_name] = this.area_name;
    return data;
  }

  String getAddress3depth() {
    String value = '$address1 $address2 $area_name';
    if (address2 == area_name) {
      value = '$address1 $area_name';
    }
    if (!StringUtils.validateString(area_name)) {
      value = '$address1 $address2 $address3';
    }
    return value;
  }

  String getAddress() {
    String value = area_name;
    if (!StringUtils.validateString(area_name)) {
      value = address3;
    }
    return value;
  }

  @override
  String toString() {
    return 'AreaCodeSearchData{bcode: $bcode, hcode: $hcode, address1: $address1, address2: $address2, address3: $address3, area_name: $area_name}';
  }
}
