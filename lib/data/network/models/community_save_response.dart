import 'community_list_response.dart';
import 'header_response.dart';

class CommunitySaveResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<CommunityBoardData>? dataList;

  CommunitySaveResponse({this.dataHeader, this.dataList});
  CommunityBoardData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CommunityBoardData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();
  factory CommunitySaveResponse.fromJson(Map<String, dynamic> json) {
    return CommunitySaveResponse(
        dataHeader: HeaderResponse.fromJson(json),
        dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => CommunityBoardData.fromJson(e)).toList());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CommunitySaveResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
