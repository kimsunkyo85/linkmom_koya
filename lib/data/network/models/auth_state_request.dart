import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class AuthStateViewRequest {
  static const String Key_gubun = "gubun";

  @JsonKey(name: Key_gubun)
  int gubun;

  AuthStateViewRequest({this.gubun = 0});

  factory AuthStateViewRequest.fromJson(Map<String, dynamic> json) {
    return AuthStateViewRequest(gubun: json[Key_gubun] ?? 0);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_gubun] = this.gubun;
    return data;
  }
}
