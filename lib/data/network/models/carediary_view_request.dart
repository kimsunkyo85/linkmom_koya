import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryViewRequest {
  static const String Key_schedule_id = 'schedule_id';

  CareDiaryViewRequest({this.schedule_id = 0});

  @JsonKey(name: Key_schedule_id)
  final int schedule_id;

  factory CareDiaryViewRequest.fromJson(Map<String, dynamic> json) {
    return CareDiaryViewRequest(schedule_id: json[Key_schedule_id] as int);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_schedule_id] = this.schedule_id;
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryViewRequest{schedule_id: $schedule_id}';
  }
}
