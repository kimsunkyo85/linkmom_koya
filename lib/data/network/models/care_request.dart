import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

///- All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
///-  Created by   : platformbiz@ggumbi.com
///-  version      : 1.0.0
///-  see          : care_request.dart - 돌봄신청 저장
///-  since        : 4/13/21 / update:
@JsonSerializable()
class CareRequest {
  ///아이정보
  static const String Key_child = 'child';

  ///돌봄유형(등원:0, 하원:1, 보육:2, 학습:3, 입주가사:4, 이유식:5)
  static const String Key_servicetype = 'servicetype';

  ///돌봄장소(우리집:1, 이웃집:2)
  static const String Key_possible_area = 'possible_area';

  ///돌봄이동방법, 추가서비스 등
  static const String Key_reqdata = 'reqdata';

  CareRequest({
    this.child = 0,
    this.servicetype = 0,
    this.possible_area = 1,
    this.reqdata = '',
  });

  ///아이정보
  @JsonKey(name: Key_child)
  int child;

  ///돌봄유형(등원:0, 하원:1, 보육:2, 학습:3, 입주가사:4, 이유식:5)
  @JsonKey(name: Key_servicetype)
  int servicetype;

  ///돌봄장소(우리집:1, 이웃집:2)
  @JsonKey(name: Key_possible_area)
  int possible_area;

  ///돌봄이동방법, 추가서비스 등
  @JsonKey(name: Key_reqdata)
  // ReqData reqdata;
  String reqdata;

  factory CareRequest.fromJson(Map<String, dynamic> json) {
    return CareRequest(
      child: json[Key_child] as int,
      servicetype: json[Key_servicetype] as int,
      possible_area: json[Key_possible_area] as int,
      reqdata: json[Key_reqdata] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_child] = this.child;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_reqdata] = this.reqdata;
    return data;
  }

  CareRequest clone() {
    final jsonResponse = json.decode(json.encode(this));
    return CareRequest.fromJson(jsonResponse as Map<String, dynamic>);
  }

  @override
  String toString() {
    return 'CaresRequest{child: $child, servicetype: $servicetype, possible_area: $possible_area, reqdata: $reqdata}';
  }
}
