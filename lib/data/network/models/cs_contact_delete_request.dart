import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CsContactDeleteRequest {
  static const String Key_id = 'id';

  @JsonKey(name: Key_id)
  int id;

  CsContactDeleteRequest({this.id = 0});

  factory CsContactDeleteRequest.fromJson(Map<String, dynamic> json) {
    return CsContactDeleteRequest(
      id: json[Key_id],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    return data;
  }

  @override
  String toString() {
    return 'CsContactDeleteRequest{id: $id}';
  }
}
