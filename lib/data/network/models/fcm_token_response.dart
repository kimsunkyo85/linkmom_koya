import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : fcm_token_response.dart - Firebase FCM Token 등록 응답
/// since        : 2021/07/15 / update:
@JsonSerializable()
class FcmTokenResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  FcmTokenResponse({
    this.dataHeader,
    this.dataList,
  });

  FcmTokenData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : FcmTokenData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory FcmTokenResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<FcmTokenData> datas = [];
    try {
      datas = json[Key_data].map<FcmTokenData>((i) => FcmTokenData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return FcmTokenResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'FindIdResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class FcmTokenData {
  static const String Key_username = "username";

  FcmTokenData({
    this.username = ''
  });

  @JsonKey(name: Key_username)
  final String username;

  factory FcmTokenData.fromJson(Map<String, dynamic> json) {
    return FcmTokenData(
      username: json[Key_username] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_username] = this.username;
    return data;
  }

  @override
  String toString() {
    return 'FcmTokenData{username: $username}';
  }
}
