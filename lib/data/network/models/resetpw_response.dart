import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';
import 'header_response.dart';

@JsonSerializable()
class ResetPwResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  ResetPwResponse({
    this.dataHeader,
    this.dataList,
  });

  ResetPwData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ResetPwData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ResetPwResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ResetPwData> datas = [];
    try {
      datas = json[Key_data].map<ResetPwData>((i) => ResetPwData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return ResetPwResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ReqauthResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class ResetPwData {
  static const String Key_username = "username";

  ResetPwData({
    this.username = 0
  });

  @JsonKey(name: Key_username)
  final int username;

  factory ResetPwData.fromJson(Map<String, dynamic> json) {
    return ResetPwData(
      username: json[Key_username] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_username] = this.username;
    return data;
  }

  @override
  String toString() {
    return 'ResetPwData{username: $username}';
  }
}
