import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CsFaqRequest {
  static const String Key_gubun = 'gubun';
  static const String Key_type = 'type';

  @JsonKey(name: Key_gubun)
  int gubun;
  @JsonKey(name: Key_type)
  int type;

  CsFaqRequest({this.gubun = 0, this.type = 0});

  factory CsFaqRequest.fromJson(Map<String, dynamic> json) {
    return CsFaqRequest(
      gubun: json[Key_gubun],
      type: json[Key_type],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_gubun] = this.gubun;
    data[Key_type] = this.type;
    return data;
  }

  @override
  String toString() {
    return 'CsFaqRequest{gubun: $gubun, type: $type}';
  }
}
