import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : reqauth_request.dart - 이메일찾기(아이디,패스워드)
/// since        : 1/18/21 / update:
@JsonSerializable()
class ReqauthRequest {
  static const String Key_auth_type = "auth_type";
  static const String Key_auth_key = "auth_key";
  static const String Key_req_screen = "req_screen";

  ReqauthRequest({
    this.auth_type = '',
    this.auth_key = '',
    this.req_screen = 0,
  });

  @JsonKey(name: Key_auth_type)
  final String auth_type;

  @JsonKey(name: Key_auth_key)
  final String auth_key;

  @JsonKey(name: Key_req_screen)
  final int req_screen;

  factory ReqauthRequest.fromJson(Map<String, dynamic> json) {
    return ReqauthRequest(
      auth_type: json[Key_auth_type] ?? '',
      auth_key: json[Key_auth_key] ?? '',
      req_screen: json[Key_req_screen] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_auth_type] = this.auth_type;
    data[Key_auth_key] = this.auth_key;
    data[Key_req_screen] = this.req_screen;
    return data;
  }

  @override
  String toString() {
    return 'ReqauthRequest{auth_type: $auth_type, auth_key: $auth_key, req_screen: $req_screen}';
  }
}
