import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class RegisterRequest {
  static const String Key_username = "username";
  static const String Key_password = "password";
  static const String Key_password2 = "password2";
  static const String Key_email = "email";
  static const String Key_first_name = "first_name";
  static const String Key_gender = "gender";
  static const String Key_birth = "birth";
  static const String Key_nationalinfo = "nationalinfo";
  static const String Key_telco = "telco";
  static const String Key_hpnumber = "hpnumber";
  static const String Key_nice_ci = "nice_ci";
  static const String Key_nice_di = "nice_di";
  static const String Key_aggement_serviceterms = "aggement_serviceterms";
  static const String Key_aggement_personal = "aggement_personal";
  static const String Key_aggement_otheruser = "aggement_otheruser";
  static const String Key_aggement_gps = "aggement_gps";
  static const String Key_aggement_telco = "aggement_telco";
  static const String Key_aggement_refund = "aggement_refund";
  static const String Key_aggement_event = "aggement_event";
  static const String Key_join_path = "join_path";
  static const String Key_invite_code = "invite_code";

  RegisterRequest({
    this.username = '',
    this.password = '',
    this.password2 = '',
    this.email = '',
    this.first_name = '',
    this.gender = '',
    this.birth = '',
    this.nationalinfo = '',
    this.telco = '',
    this.hpnumber = '',
    this.nice_ci = '',
    this.nice_di = '',
    this.aggement_serviceterms = '',
    this.aggement_personal = '',
    this.aggement_otheruser = '',
    this.aggement_telco = '',
    this.aggement_gps = '',
    this.aggement_refund = '',
    this.aggement_event = '',
    this.joinPath = 0,
    this.inviteCode = '',
  });

  @JsonKey(name: Key_username)
  String username;
  @JsonKey(name: Key_password)
  String password;
  @JsonKey(name: Key_password2)
  String password2;
  @JsonKey(name: Key_email)
  String email;
  @JsonKey(name: Key_first_name)
  String first_name;
  @JsonKey(name: Key_gender)
  String gender;
  @JsonKey(name: Key_birth)
  String birth;
  @JsonKey(name: Key_nationalinfo)
  String nationalinfo;
  @JsonKey(name: Key_telco)
  String telco;
  @JsonKey(name: Key_hpnumber)
  String hpnumber;
  @JsonKey(name: Key_nice_ci)
  String nice_ci;
  @JsonKey(name: Key_nice_di)
  String nice_di;
  @JsonKey(name: Key_aggement_serviceterms)
  String aggement_serviceterms;
  @JsonKey(name: Key_aggement_personal)
  String aggement_personal;
  @JsonKey(name: Key_aggement_otheruser)
  String aggement_otheruser;
  @JsonKey(name: Key_aggement_gps)
  String aggement_gps;
  @JsonKey(name: Key_aggement_telco)
  String aggement_telco;
  @JsonKey(name: Key_aggement_refund)
  String aggement_refund;
  @JsonKey(name: Key_aggement_event)
  String aggement_event;
  @JsonKey(name: Key_join_path)
  int joinPath;
  @JsonKey(name: Key_invite_code)
  String inviteCode;

  factory RegisterRequest.fromJson(Map<String, dynamic> json) {
    return RegisterRequest(
      username: json[Key_username] ?? '',
      password: json[Key_password] ?? '',
      password2: json[Key_password2] ?? '',
      email: json[Key_email] ?? '',
      first_name: json[Key_first_name] ?? '',
      gender: json[Key_gender] ?? '',
      birth: json[Key_birth] ?? '',
      nationalinfo: json[Key_nationalinfo] ?? '',
      telco: json[Key_telco] ?? '',
      hpnumber: json[Key_hpnumber] ?? '',
      nice_ci: json[Key_nice_ci] ?? '',
      nice_di: json[Key_nice_di] ?? '',
      aggement_serviceterms: json[Key_aggement_serviceterms] ?? '',
      aggement_personal: json[Key_aggement_personal] ?? '',
      aggement_otheruser: json[Key_aggement_otheruser] ?? '',
      aggement_gps: json[Key_aggement_gps] ?? '',
      aggement_telco: json[Key_aggement_telco] ?? '',
      aggement_refund: json[Key_aggement_refund] ?? '',
      aggement_event: json[Key_aggement_event] ?? '',
      joinPath: json[Key_join_path] ?? '',
      inviteCode: json[Key_invite_code] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_username] = this.username;
    data[Key_password] = this.password;
    data[Key_password2] = this.password2;
    data[Key_email] = this.email;
    data[Key_first_name] = this.first_name;
    data[Key_gender] = this.gender;
    data[Key_birth] = this.birth;
    data[Key_nationalinfo] = this.nationalinfo;
    data[Key_telco] = this.telco;
    data[Key_hpnumber] = this.hpnumber;
    data[Key_nice_ci] = this.nice_ci;
    data[Key_nice_di] = this.nice_di;
    data[Key_aggement_serviceterms] = this.aggement_serviceterms;
    data[Key_aggement_personal] = this.aggement_personal;
    data[Key_aggement_otheruser] = this.aggement_otheruser;
    data[Key_aggement_gps] = this.aggement_gps;
    data[Key_aggement_telco] = this.aggement_telco;
    data[Key_aggement_refund] = this.aggement_refund;
    data[Key_aggement_event] = this.aggement_event;
    data[Key_join_path] = this.joinPath;
    data[Key_invite_code] = this.inviteCode;
    return data;
  }

  @override
  String toString() {
    return 'RegisterRequest{username: $username, password: $password, password2: $password2, email: $email, first_name: $first_name, gender: $gender, birth: $birth, nationalinfo: $nationalinfo, telco: $telco, hpnumber: $hpnumber, nice_ci: $nice_ci, nice_di: $nice_di, aggement_serviceterms: $aggement_serviceterms, aggement_personal: $aggement_personal, aggement_otheruser: $aggement_otheruser, aggement_telco: $aggement_telco, aggement_gps: $aggement_gps, aggement_refund: $aggement_refund, aggement_event: $aggement_event, joinPath: $joinPath, inviteCode: $inviteCode˜}';
  }
}
