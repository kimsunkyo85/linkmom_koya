import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class BlockDeleteResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  BlockDeleteResponse({
    this.dataHeader,
    this.dataList,
  });

  BlockDeleteResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  String getData() => dataList != null && dataList!.length != 0 ? dataList![0] : ' ';

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory BlockDeleteResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<BlockDeleteData> datas = [];
    try {
      datas = json[Key_data].map<BlockDeleteData>((i) => BlockDeleteData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return BlockDeleteResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'BlockDeleteResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class BlockDeleteData {
  static const String Key_message = 'message';

  BlockDeleteData({
    this.message = '',
  });

  @JsonKey(name: Key_message)
  String message;

  factory BlockDeleteData.fromJson(Map<String, dynamic> json) {
    return BlockDeleteData(
      message: json[Key_message],
    );
  }
}
