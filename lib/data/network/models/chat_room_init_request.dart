import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_room_init_request.dart - 채팅 방 생성하기
/// since        : 2021/07/15 / update:
@JsonSerializable()
class ChatRoomInitRequest {
  static const String Key_receiver = 'receiver';
  static const String Key_bookingcareservices = 'bookingcareservices';

  ChatRoomInitRequest({
    this.receiver,
    this.bookingcareservices,
  });

  @JsonKey(name: Key_receiver)
  final int? receiver;
  @JsonKey(name: Key_bookingcareservices)
  final int? bookingcareservices;

  factory ChatRoomInitRequest.fromJson(Map<String, dynamic> json) {
    return ChatRoomInitRequest(
      receiver: json[Key_receiver] as int,
      bookingcareservices: json[Key_bookingcareservices] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_receiver] = this.receiver;
    data[Key_bookingcareservices] = this.bookingcareservices;
    return data;
  }

  FormData toFormData(FormData formData) {
    if (receiver != null) {
      formData.fields.add(MapEntry(Key_receiver, receiver.toString()));
    }
    if (bookingcareservices != null) {
      formData.fields.add(MapEntry(Key_bookingcareservices, bookingcareservices.toString()));
    }
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'ChatRoomInitRequest{receiver: $receiver, bookingcareservices: $bookingcareservices}';
  }
}
