import 'header_response.dart';
import 'message_response.dart';

class CommunityDeleteResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<MessageData>? dataList;

  CommunityDeleteResponse({this.dataHeader, this.dataList});

  factory CommunityDeleteResponse.fromJson(Map<String, dynamic> json) {
    return CommunityDeleteResponse(dataHeader: HeaderResponse.fromJson(json), dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => MessageData.fromJson(e)).toList());
  }
  
  MessageData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : MessageData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CommunityDeleteResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
