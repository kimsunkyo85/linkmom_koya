import 'dart:io';

import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : momdady_apply_list_request.dart - 신고하기
/// since        : 2021/06/30 / update:
/// <신고사유 번호> (0, '협의된 업무 불이행'),
/// (1, '약속된 근무시간 불이행'),
/// (2, '돌봄 시작 48시간 이내 일방적 예약 취소'),
/// (3, '돌봄 당일 No-show (예약 불이행/연락두절)'),
/// (4, '협의되지 않은 추가비용 요구'),
/// (5, '링크맘 외의 거래, 결제유도 (보상 포인트 지급)'),
/// (6, '폭언'),
/// (7, '폭행'),
/// (8, '광고/홍보'),
/// (9, '기타 (신고항목 추가요청)'),
/// (10, '링크맘 외의 거래 유도 (보상 포인트 지급)')
@JsonSerializable()
class CsReportRequest {
  ///신고당하는 사람 ID
  static const String Key_reported_user = 'reported_user';

  ///돌봄매칭 ID
  static const String Key_carematching = 'carematching';

  ///채팅 ID
  static const String Key_chattingroom = 'chattingroom';

  ///신고사유 번호
  static const String Key_reason = 'reason';

  ///신고 내용
  static const String Key_msg = 'msg';

  ///신고 이미지 (최대 5장)
  static const String Key_image = 'image';

  /// Community
  static const String Key_communityshare = 'communityshare';

  /// Community
  static const String Key_communitysharereply = 'communitysharereply';

  /// Community
  static const String Key_communityeventreply = 'communityeventreply';

  CsReportRequest({
    this.notifyuser = 0,
    this.carematching = 0,
    this.chattingroom = 0,
    this.reason = 0,
    this.msg = '',
    this.image,
    this.communityshare = 0,
    this.communitysharereply = 0,
    this.communityeventreply = 0,
  });

  ///신고당하는 사람 ID
  @JsonKey(name: Key_reported_user)
  int notifyuser;

  ///돌봄매칭 ID
  @JsonKey(name: Key_carematching)
  int carematching;

  ///채팅 ID
  @JsonKey(name: Key_chattingroom)
  int chattingroom;

  ///신고사유 번호
  @JsonKey(name: Key_reason)
  int reason;

  ///신고 내용
  @JsonKey(name: Key_msg)
  String msg;

  ///신고 이미지 (최대 5장)
  @JsonKey(name: Key_image)
  List<File>? image;

  /// community
  @JsonKey(name: Key_communityshare)
  int communityshare;

  /// community reply
  @JsonKey(name: Key_communitysharereply)
  int communitysharereply;

  /// community event reply
  @JsonKey(name: Key_communityeventreply)
  int communityeventreply;

  factory CsReportRequest.fromJson(Map<String, dynamic> json) {
    return CsReportRequest(
      notifyuser: json[Key_reported_user] as int,
      carematching: json[Key_carematching] as int,
      chattingroom: json[Key_chattingroom] as int,
      reason: json[Key_reason] as int,
      msg: json[Key_msg] ?? '',
      image: json[Key_image] ?? '',
      communityshare: json[Key_communityshare] ?? 0,
      communitysharereply: json[Key_communitysharereply] ?? 0,
      communityeventreply: json[Key_communityeventreply] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_reported_user] = this.notifyuser;
    data[Key_carematching] = this.carematching;
    data[Key_chattingroom] = this.chattingroom;
    data[Key_reason] = this.reason;
    data[Key_msg] = this.msg;
    data[Key_image] = this.image;
    data[Key_communityshare] = this.communityshare;
    data[Key_communitysharereply] = this.communitysharereply;
    data[Key_communityeventreply] = this.communityeventreply;

    return data;
  }

  FormData toFormData(FormData formData) {
    formData.fields.add(MapEntry(Key_reported_user, notifyuser.toString()));
    if (carematching > 0) formData.fields.add(MapEntry(Key_carematching, carematching.toString()));
    if (chattingroom > 0) formData.fields.add(MapEntry(Key_chattingroom, chattingroom.toString()));
    formData.fields.add(MapEntry(Key_reason, reason.toString()));
    formData.fields.add(MapEntry(Key_msg, msg.toString()));
    if (communityshare > 0) formData.fields.add(MapEntry(Key_communityshare, communityshare.toString()));
    if (communitysharereply > 0) formData.fields.add(MapEntry(Key_communitysharereply, communitysharereply.toString()));
    if (communityeventreply > 0) formData.fields.add(MapEntry(Key_communityeventreply, communityeventreply.toString()));
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'ApplyNotifyRequest{notifyuser: $notifyuser, carematching: $carematching, chattingroom: $chattingroom, notifyreason: $reason, msg: $msg, image: $image, communityshare: $communityshare, communitysharereply: $communitysharereply, communityeventreply: $communityeventreply}';
  }
}
