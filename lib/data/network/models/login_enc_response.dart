import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/utils/commons.dart';

import '../../../main.dart';
import 'login_response.dart';

@JsonSerializable()
class LoginEncResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<LoginData>? dataList;

  LoginEncResponse({
    this.dataHeader,
    this.dataList,
  });

  LoginData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : LoginData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory LoginEncResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<LoginData> datas = [];
    try {
      //응답코드가 205인 경우 복호화를 한다.
      if (dataHeader.getCode() == KEY_205_SUCCESS_ENCRYPTION) {
        datas.add(LoginData.fromJson(encryptHelper.decrypt(json[Key_data][0])));
      } else {
        datas = json[Key_data].map<LoginData>((i) => LoginData.fromJson(i)).toList();
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return LoginEncResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'LoginResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
