import 'dart:io';

import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

@JsonSerializable()
class AuthCenterHomeResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<HomeStateData>? dataList;

  AuthCenterHomeResponse({
    this.dataHeader,
    this.dataList,
  });

  AuthCenterHomeResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  HomeStateData getData() => dataList != null && dataList!.length != 0 ? dataList![0] : HomeStateData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory AuthCenterHomeResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<HomeStateData> datas = [];
    try {
      log.d('『GGUMBI』>>> fromJson ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<<${json[Key_data]} ');
      datas = json[Key_data].map<HomeStateData>((i) => HomeStateData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return AuthCenterHomeResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AuthCenterHomeResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class HomeStateData {
  static const String Key_id = 'id';
  static const String Key_username = 'username';
  static const String Key_ourhome_type = 'ourhome_type';
  static const String Key_ourhome_cctv = 'ourhome_cctv';
  static const String Key_ourhome_animal = 'ourhome_animal';
  static const String Key_ourhome_picture1 = 'ourhome_picture1';
  static const String Key_ourhome_picture2 = 'ourhome_picture2';
  static const String Key_nbhhome_type = 'nbhhome_type';
  static const String Key_nbhhome_cctv = 'nbhhome_cctv';
  static const String Key_nbhhome_animal = 'nbhhome_animal';
  static const String Key_nbhhome_picture = 'nbhhome_picture';

  HomeStateData({
    this.id = 0,
    this.username = '',
    this.ourhome_type = 0,
    this.ourhome_cctv = 0,
    this.ourhome_animal,
    this.ourhome_picture1 = '',
    this.ourhome_picture2 = '',
    this.nbhhome_type,
    this.nbhhome_cctv = 0,
    this.nbhhome_animal = 0,
    this.nbhhome_picture = 0,
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_username)
  final String username;
  @JsonKey(name: Key_ourhome_type)
  final int ourhome_type;
  @JsonKey(name: Key_ourhome_cctv)
  final int ourhome_cctv;
  @JsonKey(name: Key_ourhome_animal)
  final List<int>? ourhome_animal;
  @JsonKey(name: Key_ourhome_picture1)
  String ourhome_picture1;
  @JsonKey(name: Key_ourhome_picture2)
  String ourhome_picture2;
  @JsonKey(name: Key_nbhhome_type)
  final List<int>? nbhhome_type;
  @JsonKey(name: Key_nbhhome_cctv)
  final int nbhhome_cctv;
  @JsonKey(name: Key_nbhhome_animal)
  final int nbhhome_animal;
  @JsonKey(name: Key_nbhhome_picture)
  final int nbhhome_picture;

  // List<HomeStateImageData> images;
  File? file1;
  File? file2;

  factory HomeStateData.fromJson(Map<String, dynamic> json) {
    List<int> animals = List<int>.from(json[Key_ourhome_animal]);
    List<int> hometypes = List<int>.from(json[Key_nbhhome_type]);
    String ourhomePicture1 = json[Key_ourhome_picture1] as String;
    String ourhomePicture2 = json[Key_ourhome_picture2] as String;

    return HomeStateData(
      id: json[Key_id] as int,
      username: json[Key_username] as String,
      ourhome_type: json[Key_ourhome_type] as int,
      ourhome_cctv: json[Key_ourhome_cctv] as int,
      ourhome_animal: animals,
      ourhome_picture1: ourhomePicture1,
      ourhome_picture2: ourhomePicture2,
      nbhhome_type: hometypes,
      nbhhome_cctv: json[Key_nbhhome_cctv] as int,
      nbhhome_animal: json[Key_nbhhome_animal] as int,
      nbhhome_picture: json[Key_nbhhome_picture] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_username] = this.username;
    data[Key_ourhome_type] = this.ourhome_type;
    data[Key_ourhome_cctv] = this.ourhome_cctv;
    data[Key_ourhome_animal] = this.ourhome_animal;
    data[Key_ourhome_picture1] = this.ourhome_picture1;
    data[Key_ourhome_picture2] = this.ourhome_picture2;
    data[Key_nbhhome_type] = this.nbhhome_type;
    data[Key_nbhhome_cctv] = this.nbhhome_cctv;
    data[Key_nbhhome_animal] = this.nbhhome_animal;
    data[Key_nbhhome_picture] = this.nbhhome_picture;
    return data;
  }

  @override
  String toString() {
    return 'HomeStateData{id: $id, username: $username, ourhome_type: $ourhome_type, ourhome_cctv: $ourhome_cctv, ourhome_animal: $ourhome_animal, ourhome_picture1: $ourhome_picture1, ourhome_picture2: $ourhome_picture2, nbhhome_type: $nbhhome_type, nbhhome_cctv: $nbhhome_cctv, nbhhome_animal: $nbhhome_animal, nbhhome_picture: $nbhhome_picture}';
  }
}

@JsonSerializable()
class HomeStateImageData {
  static const String Key_id = 'id';
  static const String Key_filename = 'filename';
  static const String Key_ourhome_picture = 'ourhome_picture';

  HomeStateImageData({
    this.id = 0,
    this.filename = '',
    this.ourhome_picture = '',
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_filename)
  final String filename;
  @JsonKey(name: Key_ourhome_picture)
  final String ourhome_picture;

  factory HomeStateImageData.fromJson(Map<String, dynamic> json) {
    log.d('『GGUMBI』>>> fromJson : json11111: $json,  <<< ');
    return HomeStateImageData(
      id: json[Key_id] as int,
      filename: json[Key_filename] as String,
      ourhome_picture: json[Key_ourhome_picture] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_filename] = this.filename;
    data[Key_ourhome_picture] = this.ourhome_picture;
    return data;
  }

  @override
  String toString() {
    return 'HomeStateImageData{id: $id, filename: $filename, ourhome_picture: $ourhome_picture}';
  }
}
