import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryClaimViewRequest {
  static const String Key_claim_id = 'claim_id';

  CareDiaryClaimViewRequest({this.claim_id = 0});

  @JsonKey(name: Key_claim_id)
  final int claim_id;

  factory CareDiaryClaimViewRequest.fromJson(Map<String, dynamic> json) {
    return CareDiaryClaimViewRequest(claim_id: json[Key_claim_id]);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_claim_id] = this.claim_id;
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryClaimViewRequest{claim_id: $claim_id}';
  }
}
