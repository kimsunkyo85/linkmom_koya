part of 'token.dart';

Token _$TokenFromJson(Map<String, dynamic> json) {
  return Token(
    username: json[Token.key_username] as String,
    password: json[Token.key_password] as String,
  );
}

Map<String, dynamic> _$TokenToJson(Token instance) => <String, dynamic>{
      Token.key_username: instance.username,
      Token.key_password: instance.password,
    };
