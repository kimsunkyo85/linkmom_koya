import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';


@JsonSerializable()
class MomdadyMyCaresStatusUpdateResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<StatusUpdateResultsData>? dataList;

  MomdadyMyCaresStatusUpdateResponse({
    this.dataHeader,
    this.dataList,
  });

  StatusUpdateResultsData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : StatusUpdateResultsData();

  List<StatusUpdateResultsData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  bool isData() => dataList!.isNotEmpty;

  factory MomdadyMyCaresStatusUpdateResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<StatusUpdateResultsData> datas = [];
    try {
      datas = json[Key_data].map<StatusUpdateResultsData>((i) => StatusUpdateResultsData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return MomdadyMyCaresStatusUpdateResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'MycaresMatchListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class StatusUpdateResultsData {
  static const String Key_id = 'id';
  static const String Key_status = 'status';

  StatusUpdateResultsData({
    this.id = 0,
    this.status = 0,
  });

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_status)
  int status;

  factory StatusUpdateResultsData.fromJson(Map<String, dynamic> json) {
    return StatusUpdateResultsData(
      id: json[Key_id] as int,
      status: json[Key_status] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_status] = this.status;
    return data;
  }

  @override
  String toString() {
    return 'StatusUpdateResultsData{id: $id, status: $status}';
  }
}
