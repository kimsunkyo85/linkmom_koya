part of 'token_refresh.dart';

TokenRefresh _$TokenFromJson(Map<String, dynamic> json) {
  return TokenRefresh(
    refresh: json[TokenRefresh.key_refresh] as String,
  );
}

Map<String, dynamic> _$TokenToJson(TokenRefresh instance) => <String, dynamic>{
      'refresh': instance.refresh,
    };
