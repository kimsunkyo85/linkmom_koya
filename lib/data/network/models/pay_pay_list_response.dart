import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/page_data.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///
/// Created by   : platformbiz@ggumbi.com
///
/// version      : 1.0.0
///
/// see          : pay_pay_list_response.dart - 결제리스트 POST
///
/// since        : 2021/09/09 / update:
///
/// (0, '결제'),
///
/// (11, '결제취소')
@JsonSerializable()
class PayPayListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PageData>? dataList;

  PayPayListResponse({
    this.dataHeader,
    this.dataList,
  });

  PageData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PageData(results: []);

  List<PageData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory PayPayListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PageData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<PageData>((i) => PageData.fromJson(i, '$PayPayListResponse')).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return PayPayListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'PayPayListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class PayPayListData {
  static const String Key_payment_flag = 'payment_flag';
  static const String Key_receipt_id = 'receipt_id';
  static const String Key_contract_id = 'contract_id';
  static const String Key_order_id = 'order_id';
  static const String Key_name = 'name';
  static const String Key_purchased_at = 'purchased_at';
  static const String Key_price = 'price';
  static const String Key_used_cash = 'used_cash';
  static const String Key_used_point = 'used_point';
  static const String Key_method = 'method';
  static const String Key_payment_data = 'payment_data';
  static const String Key_receipt_url = 'receipt_url';
  static const String Key_cash_receipts = 'cash_receipts';
  static const String Key_cancel_data = 'cancel_data';

  PayPayListData({
    this.payment_flag,
    this.receipt_id,
    this.contract_id,
    this.order_id,
    this.name,
    this.purchased_at,
    this.price,
    this.used_cash,
    this.used_point,
    this.method,
    this.payment_data,
    this.receipt_url,
    this.cash_receipts,
    this.cancel_data,
  });

  @JsonKey(name: Key_payment_flag)
  final int? payment_flag;
  @JsonKey(name: Key_receipt_id)
  final String? receipt_id;
  @JsonKey(name: Key_contract_id)
  final int? contract_id;
  @JsonKey(name: Key_order_id)
  final String? order_id;
  @JsonKey(name: Key_name)
  final String? name;
  @JsonKey(name: Key_purchased_at)
  final String? purchased_at;
  @JsonKey(name: Key_price)
  final int? price;
  @JsonKey(name: Key_used_cash)
  final int? used_cash;
  @JsonKey(name: Key_used_point)
  final int? used_point;
  @JsonKey(name: Key_method)
  final String? method;
  @JsonKey(name: Key_payment_data)
  final PaymentData? payment_data;
  @JsonKey(name: Key_receipt_url)
  final String? receipt_url;
  @JsonKey(name: Key_cash_receipts)
  final String? cash_receipts;
  @JsonKey(name: Key_cancel_data)
  final String? cancel_data;

  factory PayPayListData.fromJson(Map<String, dynamic> json) {
    PaymentData? paymentData = json[Key_payment_data] == null ? null : PaymentData.fromJson(json[Key_payment_data]);
    return PayPayListData(
      payment_flag: json[Key_payment_flag] as int,
      receipt_id: json[Key_receipt_id] ?? '',
      contract_id: json[Key_contract_id] as int,
      order_id: json[Key_order_id] ?? '',
      name: json[Key_name] ?? '',
      purchased_at: json[Key_purchased_at] ?? '',
      price: json[Key_price] as int,
      used_cash: json[Key_used_cash] as int,
      used_point: json[Key_used_point] as int,
      method: json[Key_method] ?? '',
      payment_data: paymentData,
      receipt_url: json[Key_receipt_url] ?? '',
      cash_receipts: json[Key_cash_receipts] ?? '',
      cancel_data: json[Key_cancel_data] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_payment_flag] = this.payment_flag;
    data[Key_receipt_id] = this.receipt_id;
    data[Key_contract_id] = this.contract_id;
    data[Key_order_id] = this.order_id;
    data[Key_name] = this.name;
    data[Key_purchased_at] = this.purchased_at;
    data[Key_price] = this.price;
    data[Key_used_cash] = this.used_cash;
    data[Key_used_point] = this.used_point;
    data[Key_method] = this.method;
    data[Key_payment_data] = this.payment_data;
    data[Key_receipt_url] = this.receipt_url;
    data[Key_cash_receipts] = this.cash_receipts;
    data[Key_cancel_data] = this.cancel_data;
    return data;
  }

  @override
  String toString() {
    return 'PayPayListData{payment_flag: $payment_flag, receipt_id: $receipt_id, contract_id: $contract_id, order_id: $order_id, name: $name, purchased_at: $purchased_at, price: $price, used_cash: $used_cash, used_point: $used_point, method: $method, payment_data: $payment_data, receipt_url: $receipt_url, cash_receipts: $cash_receipts, cancel_data: $cancel_data}';
  }
}

class PaymentData {
  static const String Key_card_name = 'card_name';
  static const String Key_card_no = 'card_no';

  PaymentData({
    this.card_name,
    this.card_no,
  });

  @JsonKey(name: Key_card_name)
  final String? card_name;
  @JsonKey(name: Key_card_no)
  final String? card_no;

  factory PaymentData.fromJson(Map<String, dynamic> json) {
    return PaymentData(
      card_name: json[Key_card_name] ?? '',
      card_no: json[Key_card_no] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_card_name] = this.card_name;
    data[Key_card_no] = this.card_no;
    return data;
  }

  @override
  String toString() {
    return 'PaymentData{card_name: $card_name, card_no: $card_no}';
  }
}

class CancelData {
  static const String Key_request_cancel_price = 'request_cancel_price';
  static const String Key_cancelled_price = 'cancelled_price';
  static const String Key_cancelled_tax_free = 'cancelled_tax_free';
  static const String Key_remain_price = 'remain_price';
  static const String Key_remain_tax_free = 'remain_tax_free';
  static const String Key_revoked_at = 'revoked_at';

  CancelData({
    this.request_cancel_price,
    this.cancelled_price,
    this.cancelled_tax_free,
    this.remain_price,
    this.remain_tax_free,
    this.revoked_at,
  });

  @JsonKey(name: Key_request_cancel_price)
  final int? request_cancel_price;
  @JsonKey(name: Key_cancelled_price)
  final int? cancelled_price;
  @JsonKey(name: Key_cancelled_tax_free)
  final int? cancelled_tax_free;
  @JsonKey(name: Key_remain_price)
  final int? remain_price;
  @JsonKey(name: Key_remain_tax_free)
  final int? remain_tax_free;
  @JsonKey(name: Key_revoked_at)
  final String? revoked_at;

  factory CancelData.fromJson(Map<String, dynamic> json) {
    return CancelData(
      request_cancel_price: json[Key_request_cancel_price] as int,
      cancelled_price: json[Key_cancelled_price] as int,
      cancelled_tax_free: json[Key_cancelled_tax_free] as int,
      remain_price: json[Key_remain_price] as int,
      remain_tax_free: json[Key_remain_tax_free] as int,
      revoked_at: json[Key_revoked_at] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_request_cancel_price] = this.request_cancel_price;
    data[Key_cancelled_price] = this.cancelled_price;
    data[Key_cancelled_tax_free] = this.cancelled_tax_free;
    data[Key_remain_price] = this.remain_price;
    data[Key_remain_tax_free] = this.remain_tax_free;
    data[Key_revoked_at] = this.revoked_at;
    return data;
  }

  @override
  String toString() {
    return 'PayPayListResponse{request_cancel_price: $request_cancel_price, cancelled_price: $cancelled_price, cancelled_tax_free: $cancelled_tax_free, remain_price: $remain_price, remain_tax_free: $remain_tax_free, revoked_at: $revoked_at}';
  }
}
