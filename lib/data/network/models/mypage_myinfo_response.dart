import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/utils/string_util.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : mypage_myinfo_response.dart - 나의 정보 가져오기
/// since        : 5/10/21 / update:
@JsonSerializable()
class MyPageMyInfoResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<MyInfoData>? dataList;

  MyPageMyInfoResponse({
    this.dataHeader,
    this.dataList,
  });

  MyInfoData getData() => dataList!.first;

  List<MyInfoData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory MyPageMyInfoResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<MyInfoData> datas = [];
    try {
      datas = json[Key_data].map<MyInfoData>((i) => MyInfoData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return MyPageMyInfoResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AuthCenterHomeResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class MyInfoData {
  static const String Key_first_name = 'first_name';
  static const String Key_nickname = 'nickname';
  static const String Key_gender = 'gender';
  static const String Key_age = 'age';
  static const String Key_numberofchild = 'numberofchild';
  static const String Key_job = 'job';
  static const String Key_profileimg = 'profileimg';
  static const String Key_is_user_momdady = 'is_user_momdady';
  static const String Key_is_user_linkmom = 'is_user_linkmom';
  static const String Key_grade_momdady = 'grade_momdady';
  static const String Key_grade_linkmom = 'grade_linkmom';
  static const String Key_grade_momdady_txt = 'grade_momdady_txt';
  static const String Key_grade_linkmom_txt = 'grade_linkmom_txt';
  static const String Key_auth_address = 'auth_address';
  static const String Key_reviewinfo = 'reviewinfo';
  static const String Key_mypage_info_cnt = 'mypage_info_cnt';
  static const String Key_is_auth_homestate = 'is_auth_homestate';
  static const String Key_bookingjobs_ing = 'bookingjobs_ing';
  static const String Key_profileimg_original = 'profileimg_original';

  MyInfoData({
    this.first_name = '',
    this.nickname = '',
    this.gender = 1,
    this.age = 1,
    this.numberofchild = 99,
    this.job = 99,
    this.profileimg = '',
    this.is_user_momdady = true,
    this.is_user_linkmom = false,
    this.grade_momdady = 0,
    this.grade_linkmom = 0,
    this.grade_momdady_txt = '',
    this.grade_linkmom_txt = '',
    this.auth_address,
    this.reviewinfo,
    this.mypageInfoCnt,
    this.isAuthHomestate = false,
    this.bookingjobs_ing = true,
    this.profileimg_original = '',
  });

  MyInfoData.init() {
    this.first_name = '';
    this.nickname = '';
    this.gender = 1;
    this.age = 1;
    this.numberofchild = 99;
    this.job = 99;
    this.profileimg = '';
    this.is_user_momdady = true;
    this.is_user_linkmom = false;
    this.grade_momdady = 0;
    this.grade_linkmom = 0;
    this.grade_momdady_txt = '';
    this.grade_linkmom_txt = '';
    this.auth_address = MyInfoAddressItem();
    this.reviewinfo = MyInfoLikeReViewItem();
    this.mypageInfoCnt = MypageInfoCnt();
    this.isAuthHomestate = false;
    this.bookingjobs_ing = true;
    this.profileimg_original = '';
  }

  ///이름
  @JsonKey(name: Key_first_name)
  late String first_name;

  ///성별 (남자:1, 여자:2)
  @JsonKey(name: Key_gender)
  late int gender;

  ///나이
  @JsonKey(name: Key_age)
  late int age;

  ///아이수 (미등록 : 99, 그 외 아이 숫자 0, 1, 2, 3)
  @JsonKey(name: Key_numberofchild)
  late int numberofchild;

  ///직업 (0, u'전업 맘/대디'), (1, u'워킹 맘/대디'), (2, u'출산휴가/육아휴직'), (3, u'프리랜서파트타임'), (4, u'자영업'), (5, u'전문직/강사'), (6, u'대학생'), (7, u'미취업자/취준생'), (8, u'회사원'), (9, u'기타'), (99, u'미등록')
  @JsonKey(name: Key_job)
  late int job;

  ///사진
  @JsonKey(name: Key_profileimg)
  late String profileimg;

  ///맘대디 회원 자격 (0:미인증, 1:인증)
  @JsonKey(name: Key_is_user_momdady)
  late bool is_user_momdady;

  ///링크쌤 회원 자격 (0:미인증, 1:인증)
  @JsonKey(name: Key_is_user_linkmom)
  late bool is_user_linkmom;

  ///부모 등급(맘대디)
  @JsonKey(name: Key_grade_momdady)
  late int grade_momdady;

  ///링크쌤등급
  @JsonKey(name: Key_grade_linkmom)
  late int grade_linkmom;

  ///부모 등급(맘대디)
  @JsonKey(name: Key_grade_momdady_txt)
  late String grade_momdady_txt;

  ///링크쌤등급
  @JsonKey(name: Key_grade_linkmom_txt)
  late String grade_linkmom_txt;

  ///기본 주소 데이터
  @JsonKey(name: Key_auth_address)
  late MyInfoAddressItem? auth_address;

  ///좋아요, 리뷰 데이터
  @JsonKey(name: Key_reviewinfo)
  late MyInfoLikeReViewItem? reviewinfo;

  @JsonKey(name: Key_nickname)
  late String nickname;

  @JsonKey(name: Key_nickname)
  MypageInfoCnt? mypageInfoCnt;

  @JsonKey(name: Key_is_auth_homestate)
  late bool isAuthHomestate;

  @JsonKey(name: Key_bookingjobs_ing)
  late bool bookingjobs_ing;

  @JsonKey(name: Key_profileimg_original)
  late String profileimg_original;

  factory MyInfoData.fromJson(Map<String, dynamic> json) {
    MyInfoAddressItem addressItem = json[Key_auth_address] != null ? MyInfoAddressItem.fromJson(json[Key_auth_address]) : MyInfoAddressItem();
    MyInfoLikeReViewItem reviewinfo = json[Key_reviewinfo] != null ? MyInfoLikeReViewItem.fromJson(json[Key_reviewinfo]) : MyInfoLikeReViewItem();
    return MyInfoData(
      first_name: json[Key_first_name] ?? '',
      gender: json[Key_gender] as int,
      age: json[Key_age] as int,
      numberofchild: json[Key_numberofchild] as int,
      job: json[Key_job] as int,
      profileimg: json[Key_profileimg] ?? '',
      is_user_momdady: json[Key_is_user_momdady] as bool,
      is_user_linkmom: json[Key_is_user_linkmom] as bool,
      grade_momdady: json[Key_grade_momdady] as int,
      grade_linkmom: json[Key_grade_linkmom] as int,
      grade_momdady_txt: json[Key_grade_momdady_txt] ?? '',
      grade_linkmom_txt: json[Key_grade_linkmom_txt] ?? '',
      auth_address: addressItem,
      reviewinfo: reviewinfo,
      nickname: json[Key_nickname] ?? '',
      mypageInfoCnt: json[Key_mypage_info_cnt] != null ? MypageInfoCnt.fromJson(json[Key_mypage_info_cnt]) : MypageInfoCnt(),
      isAuthHomestate: json[Key_is_auth_homestate] ?? false,
      bookingjobs_ing: json[Key_bookingjobs_ing] ?? true,
      profileimg_original: json[Key_profileimg_original] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_first_name] = this.first_name;
    data[Key_gender] = this.gender;
    data[Key_age] = this.age;
    data[Key_numberofchild] = this.numberofchild;
    data[Key_job] = this.job;
    data[Key_profileimg] = this.profileimg;
    data[Key_is_user_momdady] = this.is_user_momdady;
    data[Key_is_user_linkmom] = this.is_user_linkmom;
    data[Key_grade_momdady] = this.grade_momdady;
    data[Key_grade_linkmom] = this.grade_linkmom;
    data[Key_grade_momdady_txt] = this.grade_momdady_txt;
    data[Key_grade_linkmom_txt] = this.grade_linkmom_txt;
    data[Key_auth_address] = this.auth_address;
    data[Key_reviewinfo] = this.reviewinfo;
    data[Key_nickname] = this.nickname;
    data[Key_mypage_info_cnt] = this.mypageInfoCnt;
    data[Key_is_auth_homestate] = this.isAuthHomestate;
    data[Key_bookingjobs_ing] = this.bookingjobs_ing;
    data[Key_profileimg_original] = this.profileimg_original;
    return data;
  }

  @override
  String toString() {
    return 'MyInfoData{first_name: $first_name, gender: $gender, age: $age, numberofchild: $numberofchild, job: $job, profileimg: $profileimg, is_user_momdady: $is_user_momdady, is_user_linkmom: $is_user_linkmom, grade_momdady: $grade_momdady, grade_linkmom: $grade_linkmom, grade_momdady_txt: $grade_momdady_txt, grade_linkmom_txt: $grade_linkmom_txt, auth_address: $auth_address, reviewinfo: $reviewinfo, nickname: $nickname, mypageInfoCnt: $mypageInfoCnt, isAuthHomestate: $isAuthHomestate, bookingjobs_ing: $bookingjobs_ing, profileimg_original: $profileimg_original}';
  }
}

@JsonSerializable()
class MyInfoAddressItem {
  static const String Key_address_name = 'address_name';
  static const String Key_region_1depth = 'region_1depth';
  static const String Key_region_2depth = 'region_2depth';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_4depth = 'region_4depth';
  static const String Key_region_building_name = 'region_building_name';
  static const String Key_is_gpswithaddress = 'is_gpswithaddress';
  static const String Key_is_affiliate = 'is_affiliate';

  MyInfoAddressItem({
    this.region_1depth = '',
    this.region_2depth = '',
    this.region_3depth = '',
    this.region_4depth = '',
    this.region_building_name = '',
    this.is_gpswithaddress = false,
    this.is_affiliate = false,
  });

  ///기본 주소(합친거)
  @JsonKey(name: Key_address_name)
  String _address_name = '';

  @JsonKey(name: Key_region_1depth)
  late String region_1depth;
  @JsonKey(name: Key_region_2depth)
  late String region_2depth;
  @JsonKey(name: Key_region_3depth)
  late String region_3depth;
  @JsonKey(name: Key_region_4depth)
  late String region_4depth;

  ///제휴 빌딩 이름
  @JsonKey(name: Key_region_building_name)
  final String region_building_name;

  ///인증성공여부 (1:성공, 0:실패)
  @JsonKey(name: Key_is_gpswithaddress)
  late bool is_gpswithaddress;

  ///제휴 인증 여부 true, false
  @JsonKey(name: Key_is_affiliate)
  final bool is_affiliate;

  factory MyInfoAddressItem.fromJson(Map<String, dynamic> json) {
    return MyInfoAddressItem(
      region_1depth: json[Key_region_1depth] ?? '',
      region_2depth: json[Key_region_2depth] ?? '',
      region_3depth: json[Key_region_3depth] ?? '',
      region_4depth: json[Key_region_4depth] ?? '',
      region_building_name: json[Key_region_building_name] ?? '',
      is_gpswithaddress: json[Key_is_gpswithaddress] ?? false,
      is_affiliate: json[Key_is_affiliate] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_region_1depth] = this.region_1depth;
    data[Key_region_2depth] = this.region_2depth;
    data[Key_region_3depth] = this.region_3depth;
    data[Key_region_4depth] = this.region_4depth;
    data[Key_region_building_name] = this.region_building_name;
    data[Key_is_gpswithaddress] = this.is_gpswithaddress;
    data[Key_is_affiliate] = this.is_affiliate;
    return data;
  }

  String getAddressName() {
    _address_name = StringUtils.getAddressParser(region_3depth, region_4depth, region2th: region_2depth);
    return _address_name;
  }

  ///맘대디,링크쌤 리스트 조회시 3th이름으로 하여, 만약 null인 경우 4th로 사용한다.
  String getLocationName() {
    return StringUtils.validateString(region_3depth) ? region_3depth : region_4depth;
  }

  @override
  String toString() {
    return 'MyInfoAddressItem{_address_name: $_address_name, region_1depth: $region_1depth, region_2depth: $region_2depth, region_3depth: $region_3depth, region_4depth: $region_4depth, region_building_name: $region_building_name, is_gpswithaddress: $is_gpswithaddress, is_affiliate: $is_affiliate}';
  }
}

@JsonSerializable()
class MyInfoLikeReViewItem {
  static const String Key_favoite_cnt_momdady = 'favoite_cnt_momdady';
  static const String Key_favoite_cnt_linkmom = 'favoite_cnt_linkmom';
  static const String Key_review_cnt_momdady = 'review_cnt_momdady';
  static const String Key_review_cnt_linkmom = 'review_cnt_linkmom';

  MyInfoLikeReViewItem({
    this.favoite_cnt_momdady = 0,
    this.favoite_cnt_linkmom = 0,
    this.review_cnt_momdady = 0,
    this.review_cnt_linkmom = 0,
  });

  @JsonKey(name: Key_favoite_cnt_momdady)
  int favoite_cnt_momdady;
  @JsonKey(name: Key_favoite_cnt_linkmom)
  int favoite_cnt_linkmom;
  @JsonKey(name: Key_review_cnt_momdady)
  int review_cnt_momdady;
  @JsonKey(name: Key_review_cnt_linkmom)
  int review_cnt_linkmom;

  factory MyInfoLikeReViewItem.fromJson(Map<String, dynamic> json) {
    return MyInfoLikeReViewItem(
      favoite_cnt_momdady: json[Key_favoite_cnt_momdady] as int,
      favoite_cnt_linkmom: json[Key_favoite_cnt_linkmom] as int,
      review_cnt_momdady: json[Key_review_cnt_momdady] as int,
      review_cnt_linkmom: json[Key_review_cnt_linkmom] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_favoite_cnt_momdady] = this.favoite_cnt_momdady;
    data[Key_favoite_cnt_linkmom] = this.favoite_cnt_linkmom;
    data[Key_review_cnt_momdady] = this.review_cnt_momdady;
    data[Key_review_cnt_linkmom] = this.review_cnt_linkmom;
    return data;
  }

  @override
  String toString() {
    return 'MyInfoLikeReViewItem{favoite_cnt_momdady: $favoite_cnt_momdady, favoite_cnt_linkmom: $favoite_cnt_linkmom, review_cnt_momdady: $review_cnt_momdady, review_cnt_linkmom: $review_cnt_linkmom}';
  }
}

class MypageInfoCnt {
  static const String Key_auth_cnt = 'auth_cnt';
  static const String Key_review_cnt_momdady = 'review_cnt_momdady';
  static const String Key_total_care_time = 'total_care_time';
  static const String Key_review_cnt_linkmom = 'review_cnt_linkmom';
  static const String Key_favoite_cnt_linkmom = 'favoite_cnt_linkmom';
  static const String Key_link_cash = 'link_cash';
  static const String Key_link_point = 'link_point';
  static const String Key_total_income_cash = 'total_income_cash';

  @JsonKey(name: Key_auth_cnt)
  int authCnt;
  @JsonKey(name: Key_review_cnt_momdady)
  int reviewCntMomdady;
  @JsonKey(name: Key_total_care_time)
  int totalCareTime;
  @JsonKey(name: Key_review_cnt_linkmom)
  int reviewCntLinkmom;
  @JsonKey(name: Key_favoite_cnt_linkmom)
  int favoiteCntLinkmom;
  @JsonKey(name: Key_link_cash)
  int linkCash;
  @JsonKey(name: Key_link_point)
  int linkPoint;
  @JsonKey(name: Key_total_income_cash)
  int totalIncomeCash;

  MypageInfoCnt({
    this.authCnt = 0,
    this.reviewCntMomdady = 0,
    this.totalCareTime = 0,
    this.reviewCntLinkmom = 0,
    this.favoiteCntLinkmom = 0,
    this.linkCash = 0,
    this.linkPoint = 0,
    this.totalIncomeCash = 0,
  });

  factory MypageInfoCnt.fromJson(Map<String, dynamic> json) {
    return MypageInfoCnt(
      authCnt: json[Key_auth_cnt] ?? 0,
      reviewCntMomdady: json[Key_review_cnt_momdady] ?? 0,
      totalCareTime: json[Key_total_care_time] ?? 0,
      reviewCntLinkmom: json[Key_review_cnt_linkmom] ?? 0,
      favoiteCntLinkmom: json[Key_favoite_cnt_linkmom] ?? 0,
      linkCash: json[Key_link_cash] ?? 0,
      linkPoint: json[Key_link_point] ?? 0,
      totalIncomeCash: json[Key_total_income_cash] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_auth_cnt] = this.authCnt;
    data[Key_review_cnt_momdady] = this.reviewCntMomdady;
    data[Key_total_care_time] = this.totalCareTime;
    data[Key_review_cnt_linkmom] = this.reviewCntLinkmom;
    data[Key_favoite_cnt_linkmom] = this.favoiteCntLinkmom;
    data[Key_link_cash] = this.linkCash;
    data[Key_link_point] = this.linkPoint;
    data[Key_total_income_cash] = this.totalIncomeCash;
    return data;
  }

  @override
  String toString() {
    return 'MypageInfoCnt{authCnt: $authCnt, reviewCntMomdady: $reviewCntMomdady, totalCareTime: $totalCareTime, reviewCntLinkmom: $reviewCntLinkmom, favoiteCntLinkmom: $favoiteCntLinkmom, linkCash: $linkCash, linkPoint: $linkPoint, totalIncomeCash: $totalIncomeCash}';
  }
}
