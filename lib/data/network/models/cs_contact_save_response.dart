import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import 'cs_contact_list_response.dart';

@JsonSerializable()
class CsContactSaveResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<ContactData>? dataList;

  CsContactSaveResponse({this.dataHeader, this.dataList});

  ContactData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ContactData(contactusImages: []);

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CsContactSaveResponse.fromJson(Map<String, dynamic> json) {
    return CsContactSaveResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => ContactData.fromJson(e)).toList(),
    );
  }

  @override
  String toString() {
    return 'CsContactSaveResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
