import 'package:json_annotation/json_annotation.dart';

class CareScheduleRequest {
  static const String Key_careschedule = 'careschedule';

  CareScheduleRequest({
    this.careschedule,
  });

  @JsonKey(name: Key_careschedule)
  final ScheduleData? careschedule;

  factory CareScheduleRequest.fromJson(Map<String, dynamic> json) {
    List<ScheduleData> scheduleData = json[Key_careschedule] == null ? [] : json[Key_careschedule].map<ScheduleData>((i) => ScheduleData.fromJson(i));
    return CareScheduleRequest(
      careschedule: json[Key_careschedule] ?? scheduleData,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_careschedule] = this.careschedule;
    return data;
  }

  @override
  String toString() {
    return 'CareScheduleData{$Key_careschedule: $careschedule}}';
  }
}

@JsonSerializable()
class ScheduleData {
  static const String Key_bookingdate = 'bookingdate';
  static const String Key_removedate = 'removedate';
  static const String Key_times = 'times';

  ScheduleData({this.bookingdate, this.removedate, this.times});

  @JsonKey(name: Key_bookingdate)
  final List<String>? bookingdate;
  @JsonKey(name: Key_removedate)
  final List<String>? removedate;
  @JsonKey(name: Key_times)
  final List<TimesData>? times;

  factory ScheduleData.fromJson(Map<String, dynamic> json) {
    List<String> booking = List<String>.from(json[Key_bookingdate]);
    List<String> remove = List<String>.from(json[Key_removedate]);
    List<TimesData> time = [];
    try {
      time = json[Key_times].map<TimesData>((i) => TimesData.fromJson(i)).toList();
    } catch (e) {}
    return ScheduleData(
      bookingdate: booking,
      removedate: remove,
      times: time,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bookingdate] = this.bookingdate;
    data[Key_removedate] = this.removedate;
    data[Key_times] = this.times;
    return data;
  }

  @override
  String toString() {
    return 'ChildDrugData{$Key_bookingdate: $bookingdate, $Key_removedate: $removedate, $Key_times: $times}';
  }
}

@JsonSerializable()
class TimesData {
  static const String Key_stime = 'stime';
  static const String Key_etime = 'etime';
  static const String Key_is_am = 'is_am';
  static const String Key_is_pm = 'is_pm';
  static const String Key_is_repeat = 'is_repeat';
  static const String Key_durationtime = 'durationtime';
  static const String Key_caredate_until = 'caredate_until';

  TimesData({
    this.stime = '',
    this.etime = '',
    this.is_am = 0,
    this.is_pm = 0,
    this.is_repeat = 0,
    this.durationtime = 0,
    this.caredate_until = '',
  });

  @JsonKey(name: Key_stime)
  String stime;
  @JsonKey(name: Key_etime)
  String etime;
  @JsonKey(name: Key_is_am)
  int is_am;
  @JsonKey(name: Key_is_pm)
  int is_pm;
  @JsonKey(name: Key_is_repeat)
  int is_repeat;
  @JsonKey(name: Key_durationtime)
  int durationtime;
  @JsonKey(name: Key_caredate_until)
  String caredate_until;

  factory TimesData.fromJson(Map<String, dynamic> json) {
    return TimesData(
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      is_am: json[Key_is_am] ?? 0,
      is_pm: json[Key_is_pm] ?? 0,
      is_repeat: json[Key_is_repeat] ?? 0,
      durationtime: json[Key_durationtime] ?? 0,
      caredate_until: json[Key_caredate_until] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_is_am] = this.is_am;
    data[Key_is_pm] = this.is_pm;
    data[Key_is_repeat] = this.is_repeat;
    data[Key_durationtime] = this.durationtime;
    data[Key_caredate_until] = this.caredate_until;
    return data;
  }

  @override
  String toString() {
    return 'ChildDrugData{$Key_stime: $stime, $Key_etime: $etime, $Key_is_am: $is_am, $Key_is_pm: $is_pm, $Key_is_repeat: $is_repeat, $Key_durationtime: $durationtime, $Key_caredate_until: $caredate_until}';
  }
}
