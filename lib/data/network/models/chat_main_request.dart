import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_message_list_request.dart - 채팅 목록 가져오기
/// since        : 2021/07/16 / update:
@JsonSerializable()
class ChatMainRequest {
  static const String Key_bywho = 'bywho';

  ChatMainRequest({
    this.bywho = '',
  });

  @JsonKey(name: Key_bywho)
  final String bywho;

  factory ChatMainRequest.fromJson(Map<String, dynamic> json) {
    return ChatMainRequest(
      bywho: json[Key_bywho] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bywho] = this.bywho;
    return data;
  }

  @override
  String toString() {
    return 'ChatMainRequest{bywho: $bywho}';
  }
}
