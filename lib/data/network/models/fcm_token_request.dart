import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : fcm_token_request.dart - Firebase FCM Token 등록
/// since        : 2021/07/15 / update:
@JsonSerializable()
class FcmTokenRequest {
  static const String Key_fcm_token = 'fcm_token';

  FcmTokenRequest({
    this.fcm_token = '',
  });

  @JsonKey(name: Key_fcm_token)
  String fcm_token;

  factory FcmTokenRequest.fromJson(Map<String, dynamic> json) {
    return FcmTokenRequest(
      fcm_token: json[Key_fcm_token] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_fcm_token] = this.fcm_token;
    return data;
  }

  @override
  String toString() {
    return 'FcmTokenRequest{fcm_token: $fcm_token}';
  }
}
