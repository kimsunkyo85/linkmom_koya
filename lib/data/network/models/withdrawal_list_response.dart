import 'header_response.dart';
import 'withdrawal_response.dart';
import 'package:json_annotation/json_annotation.dart';

class WithdrawalListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<WithdrawalListData>? dataList;

  WithdrawalListResponse({this.dataHeader, this.dataList});

  factory WithdrawalListResponse.fromJson(Map<String, dynamic> json) {
    return WithdrawalListResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] != null ? (json[Key_data] as List).map((e) => WithdrawalListData.fromJson(e)).toList() : [],
    );
  }

  WithdrawalListData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : WithdrawalListData(results: []);

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList;
    return data;
  }

  @override
  String toString() {
    return 'WithdrawalListResponse{dataHeader: $dataHeader,dataList: $dataList}';
  }
}

class WithdrawalListData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<WithdrawalData> results;

  WithdrawalListData({this.count = 0, this.next = '', this.previous = '', required this.results});

  factory WithdrawalListData.fromJson(Map<String, dynamic> json) {
    return WithdrawalListData(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] != null ? (json[Key_results] as List).map((e) => WithdrawalData.fromJson(e)).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results;
    return data;
  }
}
