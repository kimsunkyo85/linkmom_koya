import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/main.dart';

import 'header_response.dart';

@JsonSerializable()
class CareDiaryLikeResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  CareDiaryLikeResponse({
    this.dataHeader,
    this.dataList,
  });

  CareDiaryLikeResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  CareDiaryLikeData getData() => dataList != null && dataList!.length != 0 ? dataList!.first : CareDiaryLikeData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareDiaryLikeResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareDiaryLikeData> datas = [];
    try {
      datas = json[Key_data].map<CareDiaryLikeData>((i) => CareDiaryLikeData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareDiaryLikeResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CareDiaryLikeResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class CareDiaryLikeData {
  static const String Key_carecontract_schedule = 'carecontract_schedule';
  int carecontractSchedule;

  CareDiaryLikeData({this.carecontractSchedule = 0});

  factory CareDiaryLikeData.fromJson(Map<String, dynamic> json) {
    return CareDiaryLikeData(carecontractSchedule: json[Key_carecontract_schedule] as int);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_carecontract_schedule] = this.carecontractSchedule;
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryLikeData{ carecontractSchedule:$carecontractSchedule }';
  }
}
