import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CsContactViewRequest {
  static const String Key_id = 'id';

  @JsonKey(name: Key_id)
  int id;

  CsContactViewRequest({this.id = 0});

  factory CsContactViewRequest.fromJson(Map<String, dynamic> json) {
    return CsContactViewRequest(
      id: json[Key_id],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    return data;
  }

  @override
  String toString() {
    return 'CsContactViewRequest{id: $id}';
  }
}
