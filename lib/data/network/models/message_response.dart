import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

class MessageResponse extends HeaderResponse {
  HeaderResponse dataHeader;
  List<MessageData> dataList;

  MessageResponse({required this.dataHeader, required this.dataList});

  MessageData getData() => dataList.isNotEmpty ? dataList.first : MessageData();

  int getCode() => dataHeader.getCode();

  String getMsg() => dataHeader.getMsg();

  factory MessageResponse.fromJson(Map<String, dynamic> json) {
    return MessageResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? [] : (json[Key_data] as List).map((e) => MessageData.fromJson(e)).toList(),
    );
  }

  @override
  String toString() {
    return 'MessageResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class MessageData {
  static const String Key_message = 'message';
  @JsonKey(name: Key_message)
  String message;

  MessageData({this.message = ''});

  factory MessageData.fromJson(Map<String, dynamic> json) {
    return MessageData(message: json[Key_message]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_message] = this.message;
    return data;
  }
}
