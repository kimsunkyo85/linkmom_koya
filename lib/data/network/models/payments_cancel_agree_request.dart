import 'package:json_annotation/json_annotation.dart';

class PaymentsCancelAgreeRequest {
  static const String Key_order_id = 'order_id';
  static const String Key_cancel_id = 'cancel_id';

  @JsonKey(name: Key_order_id)
  final String orderId;

  @JsonKey(name: Key_cancel_id)
  final int cancelId;

  PaymentsCancelAgreeRequest({this.orderId = '', this.cancelId = 0});

  factory PaymentsCancelAgreeRequest.fromJson(Map<String, dynamic> json) {
    return PaymentsCancelAgreeRequest(
      orderId: json[Key_order_id] ?? '',
      cancelId: json[Key_cancel_id] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_order_id] = this.orderId;
    data[Key_cancel_id] = this.cancelId;
    return data;
  }

  @override
  String toString() {
    return 'PaymentsCancelRequest{orderId: $orderId, cancelId: $cancelId}';
  }
}
