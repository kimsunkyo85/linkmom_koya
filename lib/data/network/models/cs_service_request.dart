import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CsServiceRequest {
  static const String Key_place = 'place';

  @JsonKey(name: Key_place)
  int place;

  CsServiceRequest({this.place = 0});

  factory CsServiceRequest.fromJson(Map<String, dynamic> json) {
    return CsServiceRequest(
      place: json[Key_place],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_place] = this.place;
    return data;
  }

  @override
  String toString() {
    return 'CsServiceRequest{place: $place}';
  }
}
