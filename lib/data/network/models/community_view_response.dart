import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import 'community_list_response.dart';

class CommunityViewResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<CommunityViewData>? dataList;

  CommunityViewResponse({this.dataHeader, this.dataList});

  CommunityViewData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CommunityViewData(communityShare: CommunityShare(), communityShareImages: [], results: [], userinfo: CommunityUserInfo());

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CommunityViewResponse.fromJson(Map<String, dynamic> json) {
    return CommunityViewResponse(
        dataHeader: HeaderResponse.fromJson(json),
        dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => CommunityViewData.fromJson(e)).toList());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CommunityViewResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class CommunityViewData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';
  static const String Key_userinfo = 'userinfo';
  static const String Key_community_share = 'community_share';
  static const String Key_community_share_images = 'community_share_images';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<Reply> results;
  @JsonKey(name: Key_userinfo)
  CommunityUserInfo userinfo;
  @JsonKey(name: Key_community_share)
  CommunityShare communityShare;
  @JsonKey(name: Key_community_share_images)
  List<CommunityShareImages> communityShareImages;

  CommunityViewData({
    this.count = 0,
    this.next = '',
    this.previous = '',
    required this.results,
    required this.userinfo,
    required this.communityShare,
    required this.communityShareImages,
  });

  factory CommunityViewData.fromJson(Map<String, dynamic> json) {
    return CommunityViewData(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] != null ? (json[Key_results] as List).map((e) => Reply.fromJson(e)).toList() : [],
      userinfo: json[Key_userinfo] == null ? CommunityUserInfo() : CommunityUserInfo.fromJson(json[Key_userinfo]),
      communityShare: json[Key_community_share] == null ? CommunityShare() : CommunityShare.fromJson(json[Key_community_share]),
      communityShareImages: json[Key_community_share_images] != null && json[Key_community_share_images].isNotEmpty ? json[Key_community_share_images].map<CommunityShareImages>((e) => CommunityShareImages.fromJson(e)).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results.map((e) => e.toJson()).toList();
    data[Key_userinfo] = this.userinfo.toJson();
    data[Key_community_share] = this.communityShare.toJson();
    data[Key_community_share_images] = this.communityShareImages;
    return data;
  }

  @override
  String toString() {
    return 'CommunityViewData{count: $count,next: $next,previous: $previous,results: $results}';
  }
}

class Reply {
  static const String Key_id = 'id';
  static const String Key_parent = 'parent';
  static const String Key_level = 'level';
  static const String Key_reply = 'reply';
  static const String Key_writer_nickname = 'writer_nickname';
  static const String Key_profileimg = 'profileimg';
  static const String Key_is_delete = 'is_delete';
  static const String Key_updatedate = 'updatedate';
  static const String Key_writer_id = 'writer_id';
  static const String Key_is_block = 'is_block';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_parent)
  int parent;
  @JsonKey(name: Key_level)
  int level;
  @JsonKey(name: Key_reply)
  String reply;
  @JsonKey(name: Key_writer_nickname)
  String writerNickname;
  @JsonKey(name: Key_profileimg)
  String profileimg;
  @JsonKey(name: Key_is_delete)
  int isDelete;
  @JsonKey(name: Key_updatedate)
  String updatedate;
  @JsonKey(name: Key_writer_id)
  int writerId;
  @JsonKey(name: Key_is_block)
  bool isBlock;

  Reply({this.id = 0, this.parent = 0, this.level = 0, this.reply = '', this.writerNickname = '', this.profileimg = '', this.isDelete = 0, this.updatedate = '', this.writerId = 0, this.isBlock = false});

  factory Reply.fromJson(Map<String, dynamic> json) {
    return Reply(
      id: json[Key_id] ?? 0,
      parent: json[Key_parent] ?? 0,
      level: json[Key_level] ?? '',
      reply: json[Key_reply] ?? '',
      writerNickname: json[Key_writer_nickname] ?? '',
      profileimg: json[Key_profileimg] ?? '',
      isDelete: json[Key_is_delete] ?? 0,
      updatedate: json[Key_updatedate] ?? '',
      writerId: int.tryParse(json[Key_writer_id]) ?? 0,
      isBlock: json[Key_is_block] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_parent] = this.parent;
    data[Key_level] = this.level;
    data[Key_reply] = this.reply;
    data[Key_writer_nickname] = this.writerNickname;
    data[Key_profileimg] = this.profileimg;
    data[Key_is_delete] = this.isDelete;
    data[Key_updatedate] = this.updatedate;
    data[Key_writer_id] = this.writerId;
    data[Key_is_block] = this.isBlock;
    return data;
  }

  @override
  String toString() {
    return 'Results{id: $id, parent: $parent, level: $level, reply: $reply, writerNickname: $writerNickname, profileimg: $profileimg, isDelete: $isDelete, updatedate: $updatedate, writerId: $writerId, isBlock: $isBlock}';
  }
}

class CommunityShareImages {
  static const String Key_images = 'images';
  static const String Key_images_thumbnail = 'images_thumbnail';

  @JsonKey(name: Key_images)
  String images;
  @JsonKey(name: Key_images)
  String imagesThumbnail;

  CommunityShareImages({this.images = '', this.imagesThumbnail = ''});

  factory CommunityShareImages.fromJson(Map<String, dynamic> json) {
    return CommunityShareImages(
      images: json[Key_images] ?? '',
      imagesThumbnail: json[Key_images_thumbnail] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_images] = this.images;
    data[Key_images_thumbnail] = this.imagesThumbnail;
    return data;
  }

  @override
  String toString() {
    return 'CommunityShareImages{image:$images, imagesThumbnail:$imagesThumbnail}';
  }
}
