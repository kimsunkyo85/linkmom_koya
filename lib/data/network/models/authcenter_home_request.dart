import 'dart:io';

import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

@JsonSerializable()
class AuthCenterHomeRequest {
  static const String Key_ourhome_type = 'ourhome_type';
  static const String Key_ourhome_cctv = 'ourhome_cctv';
  static const String Key_ourhome_animal = 'ourhome_animal';
  static const String Key_ourhome_picture1 = 'ourhome_picture1';
  static const String Key_ourhome_picture2 = 'ourhome_picture2';
  static const String Key_nbhhome_type = 'nbhhome_type';
  static const String Key_nbhhome_cctv = 'nbhhome_cctv';
  static const String Key_nbhhome_animal = 'nbhhome_animal';
  static const String Key_nbhhome_picture = 'nbhhome_picture';

  AuthCenterHomeRequest({
    this.ourhome_type = 0,
    this.ourhome_cctv = 0,
    this.ourhome_animal,
    this.ourhome_picture1,
    this.ourhome_picture2,
    this.nbhhome_type,
    this.nbhhome_cctv = 0,
    this.nbhhome_animal = 0,
    this.nbhhome_picture = 0,
  });

  @JsonKey(name: Key_ourhome_type)
  int ourhome_type;
  @JsonKey(name: Key_ourhome_cctv)
  int ourhome_cctv;
  @JsonKey(name: Key_ourhome_animal)
  List<int>? ourhome_animal = List.generate(5, (index) => 0);
  @JsonKey(name: Key_ourhome_picture1)
  File? ourhome_picture1;
  @JsonKey(name: Key_ourhome_picture2)
  File? ourhome_picture2;
  @JsonKey(name: Key_ourhome_animal)
  List<int>? nbhhome_type = List.generate(5, (index) => 0);
  @JsonKey(name: Key_nbhhome_type)
  int nbhhome_cctv;
  @JsonKey(name: Key_nbhhome_animal)
  int nbhhome_animal;
  @JsonKey(name: Key_nbhhome_picture)
  int nbhhome_picture;

  factory AuthCenterHomeRequest.fromJson(Map<String, dynamic> json) {
    List<int> animals = List<int>.from(json[Key_ourhome_animal]);
    List<int> hometypes = List<int>.from(json[Key_nbhhome_type]);
    return AuthCenterHomeRequest(
      ourhome_type: json[Key_ourhome_type] as int,
      ourhome_cctv: json[Key_ourhome_cctv] as int,
      ourhome_animal: animals,
      ourhome_picture1: (json[Key_ourhome_picture1]),
      ourhome_picture2: (json[Key_ourhome_picture2]),
      nbhhome_type: hometypes,
      nbhhome_cctv: json[Key_nbhhome_cctv] as int,
      nbhhome_animal: json[Key_nbhhome_animal] as int,
      nbhhome_picture: json[Key_nbhhome_picture] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_ourhome_type] = this.ourhome_type;
    data[Key_ourhome_cctv] = this.ourhome_cctv;
    data[Key_ourhome_animal] = this.ourhome_animal;
    data[Key_ourhome_picture1] = this.ourhome_picture1;
    data[Key_ourhome_picture2] = this.ourhome_picture2;
    data[Key_nbhhome_type] = this.nbhhome_type;
    data[Key_nbhhome_cctv] = this.nbhhome_cctv;
    data[Key_nbhhome_animal] = this.nbhhome_animal;
    data[Key_nbhhome_picture] = this.nbhhome_picture;
    return data;
  }

  FormData toFormData(FormData formData) {
    List<int> animals = List<int>.from(ourhome_animal ?? []);
    List<int> hometypes = List<int>.from(nbhhome_type ?? []);
    log.d('『GGUMBI』>>> toFormData : animals: $animals,  <<< ');
    log.d('『GGUMBI』>>> toFormData : animals: ${animals.toString()},  <<< ');
    log.d('『GGUMBI』>>> toFormData : : ${toString()},  <<< ');

    formData.fields.add(MapEntry(Key_ourhome_type, this.ourhome_type.toString()));
    formData.fields.add(MapEntry(Key_ourhome_cctv, this.ourhome_cctv.toString()));
    animals.forEach((value) {
      formData.fields.add(MapEntry(Key_ourhome_animal, value.toString()));
    });
    // formData.fields.add(MapEntry(Key_ourhome_animal, animals.toString()));
    hometypes.forEach((value) {
      formData.fields.add(MapEntry(Key_nbhhome_type, value.toString()));
    });
    // formData.fields.add(MapEntry(Key_nbhhome_type, hometypes.toString()));
    formData.fields.add(MapEntry(Key_nbhhome_cctv, this.nbhhome_cctv.toString()));
    formData.fields.add(MapEntry(Key_nbhhome_animal, this.nbhhome_animal.toString()));
    formData.fields.add(MapEntry(Key_nbhhome_picture, this.nbhhome_picture.toString()));
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'AuthCenterHomeRequest{ourhome_type: $ourhome_type, ourhome_cctv: $ourhome_cctv, ourhome_animal: $ourhome_animal, ourhome_picture1: $ourhome_picture1, ourhome_picture2: $ourhome_picture2, nbhhome_type: $nbhhome_type, nbhhome_cctv: $nbhhome_cctv, nbhhome_animal: $nbhhome_animal, nbhhome_picture: $nbhhome_picture}';
  }
}
