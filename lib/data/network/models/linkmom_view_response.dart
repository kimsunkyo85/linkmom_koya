import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/job_myinfo_data.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : linkmom_view_response.dart - 링크쌤 프로필과 링크쌤 상세 페이지와 같음.
/// since        : 2021/06/03 / update:
@JsonSerializable()
class LinkMomViewResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<JobMyInfoData>? dataList;

  LinkMomViewResponse({
    this.dataHeader,
    this.dataList,
  });

  JobMyInfoData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : JobMyInfoData();

  List<JobMyInfoData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  bool isData() => dataList!.isNotEmpty;

  factory LinkMomViewResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<JobMyInfoData> datas = [];
    try {
      datas = json[Key_data].map<JobMyInfoData>((i) => JobMyInfoData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return LinkMomViewResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'LinkMomViewResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
