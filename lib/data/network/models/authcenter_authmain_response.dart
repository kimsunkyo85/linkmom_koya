import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class AuthCenterAuthMainResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  AuthMainData? dataList;

  AuthCenterAuthMainResponse({
    this.dataHeader,
    this.dataList,
  });

  AuthMainData getData() => dataList != null ? dataList! : AuthMainData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory AuthCenterAuthMainResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    AuthMainData datas = AuthMainData();
    try {
      datas = json[Key_data] != null && json[Key_data].isNotEmpty ? AuthMainData.fromJson(json[Key_data]) : AuthMainData();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return AuthCenterAuthMainResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ReqauthResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class AuthMainData {
  static const String Key_id = "id";
  static const String Key_auth_date_hp = "auth_date_hp";
  static const String Key_auth_date_address = "auth_date_address";
  static const String Key_auth_date_homestate = "auth_date_homestate";
  static const String Key_auth_date_deungbon = "auth_date_deungbon";
  static const String Key_auth_date_healthy = "auth_date_healthy";
  static const String Key_auth_date_career = "auth_date_career";
  static const String Key_auth_date_graduated = "auth_date_graduated";
  static const String Key_auth_date_education = "auth_date_education";
  static const String Key_auth_date_personality = "auth_date_personality";
  static const String Key_auth_date_covid = "auth_date_covid";
  static const String Key_auth_date_stress = "auth_date_stress";
  static const String Key_auth_date_criminal = "auth_date_criminal";
  static const String Key_userhomestat = "userhomestat";

  AuthMainData({
    this.id = 0,
    this.auth_date_hp = '',
    this.auth_date_address = '',
    this.auth_date_homestate = '',
    this.auth_date_deungbon = '',
    this.auth_date_healthy = '',
    this.auth_date_career = '',
    this.auth_date_graduated = '',
    this.auth_date_education = '',
    this.auth_date_personality = '',
    this.auth_date_covid = '',
    this.auth_date_stress = '',
    this.auth_date_criminal = '',
    this.userhomestat,
  });

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_auth_date_hp)
  String auth_date_hp;
  @JsonKey(name: Key_auth_date_address)
  String auth_date_address;
  @JsonKey(name: Key_auth_date_homestate)
  String auth_date_homestate;
  @JsonKey(name: Key_auth_date_deungbon)
  String auth_date_deungbon;
  @JsonKey(name: Key_auth_date_healthy)
  String auth_date_healthy;
  @JsonKey(name: Key_auth_date_career)
  String auth_date_career;
  @JsonKey(name: Key_auth_date_graduated)
  String auth_date_graduated;
  @JsonKey(name: Key_auth_date_education)
  String auth_date_education;
  @JsonKey(name: Key_auth_date_personality)
  String auth_date_personality;
  @JsonKey(name: Key_auth_date_covid)
  String auth_date_covid;
  @JsonKey(name: Key_auth_date_stress)
  String auth_date_stress;
  @JsonKey(name: Key_auth_date_criminal)
  String auth_date_criminal;
  @JsonKey(name: Key_userhomestat)
  UserHomeStateData? userhomestat;

  factory AuthMainData.fromJson(Map<String, dynamic> json) {
    UserHomeStateData homeState = UserHomeStateData();
    homeState = [json[Key_userhomestat]].map<UserHomeStateData>((i) => UserHomeStateData.fromJson(i)).first;
    return AuthMainData(
      id: json[Key_id] ?? 0,
      auth_date_hp: json[Key_auth_date_hp] ?? '',
      auth_date_address: json[Key_auth_date_address] ?? '',
      auth_date_homestate: json[Key_auth_date_homestate] ?? '',
      auth_date_deungbon: json[Key_auth_date_deungbon] ?? '',
      auth_date_healthy: json[Key_auth_date_healthy] ?? '',
      auth_date_career: json[Key_auth_date_career] ?? '',
      auth_date_graduated: json[Key_auth_date_graduated] ?? '',
      auth_date_education: json[Key_auth_date_education] ?? '',
      auth_date_personality: json[Key_auth_date_personality] ?? '',
      auth_date_covid: json[Key_auth_date_covid] ?? '',
      auth_date_stress: json[Key_auth_date_stress] ?? '',
      auth_date_criminal: json[Key_auth_date_criminal] ?? '',
      userhomestat: homeState,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_auth_date_hp] = this.auth_date_hp;
    data[Key_auth_date_address] = this.auth_date_address;
    data[Key_auth_date_homestate] = this.auth_date_homestate;
    data[Key_auth_date_deungbon] = this.auth_date_deungbon;
    data[Key_auth_date_healthy] = this.auth_date_healthy;
    data[Key_auth_date_career] = this.auth_date_career;
    data[Key_auth_date_graduated] = this.auth_date_graduated;
    data[Key_auth_date_education] = this.auth_date_education;
    data[Key_auth_date_personality] = this.auth_date_personality;
    data[Key_auth_date_covid] = this.auth_date_covid;
    data[Key_auth_date_stress] = this.auth_date_stress;
    data[Key_auth_date_criminal] = this.auth_date_criminal;
    data[Key_userhomestat] = this.userhomestat;
    return data;
  }

  @override
  String toString() {
    return 'AuthMainData{id: $id, }';
  }
}

class UserHomeStateData {
  static const String Key_ourhome_type = 'ourhome_type';
  static const String Key_ourhome_cctv = 'ourhome_cctv';
  static const String Key_ourhome_animal = 'ourhome_animal';
  static const String Key_ourhome_picture1 = 'ourhome_picture1';
  static const String Key_ourhome_picture2 = 'ourhome_picture2';
  static const String Key_ourhome_picture1_thumbnail = 'ourhome_picture1_thumbnail';
  static const String Key_ourhome_picture2_thumbnail = 'ourhome_picture2_thumbnail';
  static const String Key_nbhhome_type = 'nbhhome_type';
  static const String Key_nbhhome_cctv = 'nbhhome_cctv';
  static const String Key_nbhhome_animal = 'nbhhome_animal';
  static const String Key_nbhhome_picture = 'nbhhome_picture';

  UserHomeStateData({
    this.ourhome_type = 0,
    this.ourhome_cctv = 0,
    this.ourhome_animal,
    this.ourhome_picture1 = '',
    this.ourhome_picture2 = '',
    this.ourhome_picture1_thumbnail = '',
    this.ourhome_picture2_thumbnail = '',
    this.nbhhome_type,
    this.nbhhome_cctv = 0,
    this.nbhhome_animal = 0,
    this.nbhhome_picture = 0,
  });

  @JsonKey(name: Key_ourhome_type)
  final int ourhome_type;
  @JsonKey(name: Key_ourhome_cctv)
  final int ourhome_cctv;
  @JsonKey(name: Key_ourhome_animal)
  final List<int>? ourhome_animal;
  @JsonKey(name: Key_ourhome_picture1)
  final String? ourhome_picture1;
  @JsonKey(name: Key_ourhome_picture2)
  final String? ourhome_picture2;
  @JsonKey(name: Key_ourhome_picture1_thumbnail)
  final String? ourhome_picture1_thumbnail;
  @JsonKey(name: Key_ourhome_picture2_thumbnail)
  final String? ourhome_picture2_thumbnail;
  @JsonKey(name: Key_nbhhome_type)
  final List<int>? nbhhome_type;
  @JsonKey(name: Key_nbhhome_cctv)
  final int nbhhome_cctv;
  @JsonKey(name: Key_nbhhome_animal)
  final int nbhhome_animal;
  @JsonKey(name: Key_nbhhome_picture)
  final int nbhhome_picture;

  factory UserHomeStateData.fromJson(Map<String, dynamic> json) {
    return UserHomeStateData(
      ourhome_type: json[Key_ourhome_type] ?? 0,
      ourhome_cctv: json[Key_ourhome_cctv] ?? 0,
      ourhome_animal: json[Key_ourhome_animal] != null ? json[Key_ourhome_animal].map<int>((e) => int.tryParse(e) ?? 0).toList() : [],
      ourhome_picture1: json[Key_ourhome_picture1] ?? '',
      ourhome_picture2: json[Key_ourhome_picture2] ?? '',
      ourhome_picture1_thumbnail: json[Key_ourhome_picture1_thumbnail] ?? '',
      ourhome_picture2_thumbnail: json[Key_ourhome_picture2_thumbnail] ?? '',
      nbhhome_type: json[Key_nbhhome_type] != null ? json[Key_nbhhome_type].map<int>((e) => int.tryParse(e) ?? 0).toList() : [],
      nbhhome_cctv: json[Key_nbhhome_cctv] ?? 0,
      nbhhome_animal: json[Key_nbhhome_animal] ?? 0,
      nbhhome_picture: json[Key_nbhhome_picture] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_ourhome_type] = this.ourhome_type;
    data[Key_ourhome_cctv] = this.ourhome_cctv;
    data[Key_ourhome_animal] = this.ourhome_animal;
    data[Key_ourhome_picture1] = this.ourhome_picture1;
    data[Key_ourhome_picture2] = this.ourhome_picture2;
    data[Key_ourhome_picture1_thumbnail] = this.ourhome_picture1_thumbnail;
    data[Key_ourhome_picture2_thumbnail] = this.ourhome_picture2_thumbnail;
    data[Key_nbhhome_type] = this.nbhhome_type;
    data[Key_nbhhome_cctv] = this.nbhhome_cctv;
    data[Key_nbhhome_animal] = this.nbhhome_animal;
    data[Key_nbhhome_picture] = this.nbhhome_picture;
    return data;
  }

  @override
  String toString() {
    return 'UserHomeStateData${{
      'ourhome_type': ourhome_type,
      'ourhome_cctv': ourhome_cctv,
      'ourhome_animal': ourhome_animal,
      'ourhome_picture1': ourhome_picture1,
      'ourhome_picture2': ourhome_picture2,
      'ourhome_picture1_thumbnail': ourhome_picture1_thumbnail,
      'ourhome_picture2_thumbnail': ourhome_picture2_thumbnail,
      'nbhhome_type': nbhhome_type,
      'nbhhome_cctv': nbhhome_cctv,
      'nbhhome_animal': nbhhome_animal,
      'nbhhome_picture': nbhhome_picture,
    }}';
  }
}
