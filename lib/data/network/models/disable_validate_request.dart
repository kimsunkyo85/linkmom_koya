import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class DisableValidateRequest {
  static const String Key_password = 'password';

  DisableValidateRequest({this.password = ''});

  @JsonKey(name: Key_password)
  final String password;

  factory DisableValidateRequest.fromJson(Map<String, dynamic> json) {
    return DisableValidateRequest(
      password: json[Key_password] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_password] = this.password;
    return data;
  }

  @override
  String toString() {
    return 'DisableValidateRequest{password: $password}';
  }
}
