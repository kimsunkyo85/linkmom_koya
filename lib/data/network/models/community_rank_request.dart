import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CommunityRankRequest {
  static const String Key_gubun = 'gubun';
  static const String Key_hcode = 'hcode';

  @JsonKey(name: Key_gubun)
  int gubun;
  @JsonKey(name: Key_hcode)
  String hcode;

  CommunityRankRequest({this.gubun = 0, this.hcode = ''});

  factory CommunityRankRequest.fromJson(Map<String, dynamic> json) {
    return CommunityRankRequest(
      gubun: json[Key_gubun] ?? 0,
      hcode: json[Key_hcode] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_gubun] = this.gubun;
    if (this.hcode.isNotEmpty) data[Key_hcode] = this.hcode;
    return data;
  }

  @override
  String toString() {
    return 'CommunityRankRequest{gubun: $gubun, hcode: $hcode}';
  }
}
