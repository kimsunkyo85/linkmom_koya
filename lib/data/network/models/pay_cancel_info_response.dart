import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

///#### All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///#### Created by   : platformbiz@ggumbi.com
///#### version      : 1.0.0
///#### see          : pay_cancel_info_response.dart - 돌봄취소내역리스트 응답
///#### since        : 2021/10/19 / update:
///#### [함수,메서드시]
@JsonSerializable()
class PayCancelInfoResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PayInfoData>? dataList;

  PayCancelInfoResponse({
    this.dataHeader,
    this.dataList,
  });

  PayInfoData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PayInfoData();

  List<PayInfoData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory PayCancelInfoResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PayInfoData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<PayInfoData>((i) => PayInfoData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return PayCancelInfoResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'PayCancelInitResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class PayInfoData {
  static const String Key_momdady = 'momdady';
  static const String Key_linkmom = 'linkmom';
  static const String Key_care_scheduleinfo = 'care_scheduleinfo';
  static const String Key_cancel_info = 'cancel_info';

  PayInfoData({
    this.momdady,
    this.linkmom,
    this.care_scheduleinfo,
    this.cancel_info,
  });

  PayInfoData.init({
    this.momdady = 0,
    this.linkmom = 0,
    this.care_scheduleinfo,
    this.cancel_info,
  }) {
    care_scheduleinfo = PayInfoCareScheduleData(caredate: []);
    cancel_info = [];
  }

  @JsonKey(name: Key_momdady)
  int? momdady;
  @JsonKey(name: Key_linkmom)
  int? linkmom;
  @JsonKey(name: Key_care_scheduleinfo)
  PayInfoCareScheduleData? care_scheduleinfo;
  @JsonKey(name: Key_cancel_info)
  List<PayInfoCancelData>? cancel_info;

  factory PayInfoData.fromJson(Map<String, dynamic> json) {
    PayInfoCareScheduleData careScheduleinfo = json[Key_care_scheduleinfo] == null ? PayInfoCareScheduleData() : PayInfoCareScheduleData.fromJson(json[Key_care_scheduleinfo]);
    // List<PayInfoCancelData> cancel_info = json[Key_cancel_info] == null ? PayInfoCancelData() : PayInfoCancelData.fromJson(json[Key_cancel_info]);
    List<PayInfoCancelData>? cancelInfo = json[Key_cancel_info] == null ? [] : json[Key_cancel_info].map<PayInfoCancelData>((i) => PayInfoCancelData.fromJson(i)).toList();
    return PayInfoData(
      momdady: json[Key_momdady] ?? 0,
      linkmom: json[Key_linkmom] ?? 0,
      care_scheduleinfo: careScheduleinfo,
      cancel_info: cancelInfo,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_momdady] = this.momdady;
    data[Key_linkmom] = this.linkmom;
    data[Key_care_scheduleinfo] = this.care_scheduleinfo;
    data[Key_cancel_info] = this.cancel_info;
    return data;
  }

  @override
  String toString() {
    return 'PayInfoData{momdady: $momdady, linkmom: $linkmom, care_scheduleinfo: $care_scheduleinfo, cancel_info: $cancel_info}';
  }
}

class PayInfoCareScheduleData {
  static const String Key_booking_id = 'booking_id';
  static const String Key_booking_status = 'booking_status';
  static const String Key_servicetype = 'servicetype';
  static const String Key_cost_perhour = 'cost_perhour';
  static const String Key_cost_sum = 'cost_sum';
  static const String Key_stime = 'stime';
  static const String Key_etime = 'etime';
  static const String Key_durationtime = 'durationtime';
  static const String Key_caredate = 'caredate';
  static const String Key_possible_area = 'possible_area';

  PayInfoCareScheduleData({
    this.booking_id = 0,
    this.booking_status = '',
    this.servicetype = '',
    this.cost_perhour = 0,
    this.cost_sum = 0,
    this.stime = '',
    this.etime = '',
    this.durationtime = 0,
    this.caredate,
    this.possible_area = '',
  });

  @JsonKey(name: Key_booking_id)
  final int booking_id;
  @JsonKey(name: Key_booking_status)
  final String booking_status;
  @JsonKey(name: Key_servicetype)
  final String servicetype;
  @JsonKey(name: Key_cost_perhour)
  final int cost_perhour;
  @JsonKey(name: Key_cost_sum)
  final int cost_sum;
  @JsonKey(name: Key_stime)
  final String stime;
  @JsonKey(name: Key_etime)
  final String etime;
  @JsonKey(name: Key_durationtime)
  final int durationtime;
  @JsonKey(name: Key_caredate)
  final List<String>? caredate;
  @JsonKey(name: Key_possible_area)
  final String possible_area;

  factory PayInfoCareScheduleData.fromJson(Map<String, dynamic> json) {
    List<String> caredate = json[Key_caredate] == null ? [] : List<String>.from(json[Key_caredate]);
    return PayInfoCareScheduleData(
      booking_id: json[Key_booking_id] as int,
      booking_status: json[Key_booking_status] ?? '',
      servicetype: json[Key_servicetype] ?? '',
      cost_perhour: json[Key_cost_perhour] as int,
      cost_sum: json[Key_cost_sum] as int,
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      durationtime: json[Key_durationtime] as int,
      caredate: caredate,
      possible_area: json[Key_possible_area] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_booking_id] = this.booking_id;
    data[Key_booking_status] = this.booking_status;
    data[Key_servicetype] = this.servicetype;
    data[Key_cost_perhour] = this.cost_perhour;
    data[Key_cost_sum] = this.cost_sum;
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_durationtime] = this.durationtime;
    data[Key_caredate] = this.caredate;
    data[Key_possible_area] = this.possible_area;
    return data;
  }

  @override
  String toString() {
    return 'PayInfoCareScheduleData{booking_id: $booking_id, booking_status: $booking_status, servicetype: $servicetype, cost_perhour: $cost_perhour, cost_sum: $cost_sum, stime: $stime, etime: $etime, durationtime: $durationtime, caredate: $caredate, possible_area: $possible_area}';
  }
}

class PayInfoCancelData {
  static const String Key_cancel_id = 'cancel_id';
  static const String Key_order_id = 'order_id';
  static const String Key_cancel_usertype = 'cancel_usertype';
  static const String Key_cancel_request_date = 'cancel_request_date';
  static const String Key_is_refund = 'is_refund';
  static const String Key_revoked_at = 'revoked_at';
  static const String Key_request_cancel_price = 'request_cancel_price';
  static const String Key_pay_expect = 'pay_expect';
  static const String Key_cancelled_price = 'cancelled_price';
  static const String Key_linkmom_paid_price = 'linkmom_paid_price';
  static const String Key_penalty_expect = 'penalty_expect';
  static const String Key_cancelreason = 'cancelreason';
  static const String Key_start_caredate = 'start_caredate';
  static const String Key_end_caredate = 'end_caredate';
  static const String Key_other_schedule_cnt = 'other_schedule_cnt';
  static const String Key_cancelexplain_deadline = 'cancelexplain_deadline';

  PayInfoCancelData({
    this.cancel_id = 0,
    this.order_id = '',
    this.cancel_usertype = 1,
    this.cancel_request_date = '',
    this.is_refund = false,
    this.revoked_at = '',
    this.request_cancel_price = 0,
    this.pay_expect = 0,
    this.cancelled_price = 0,
    this.linkmom_paid_price = 0,
    this.penalty_expect = 0,
    this.cancelreason = 0,
    this.start_caredate = '',
    this.end_caredate = '',
    this.other_schedule_cnt = 0,
    this.cancelexplain_deadline = '',
  });

  @JsonKey(name: Key_cancel_id)
  final int cancel_id;
  @JsonKey(name: Key_order_id)
  final String order_id;
  @JsonKey(name: Key_cancel_usertype)
  final int cancel_usertype;
  @JsonKey(name: Key_cancel_request_date)
  final String cancel_request_date;
  @JsonKey(name: Key_is_refund)
  final bool is_refund;
  @JsonKey(name: Key_revoked_at)
  final String revoked_at;
  @JsonKey(name: Key_request_cancel_price)
  final int request_cancel_price;
  @JsonKey(name: Key_pay_expect)
  final int pay_expect;
  @JsonKey(name: Key_cancelled_price)
  final int cancelled_price;
  @JsonKey(name: Key_linkmom_paid_price)
  final int linkmom_paid_price;
  @JsonKey(name: Key_penalty_expect)
  final int penalty_expect;
  @JsonKey(name: Key_cancelreason)
  final int cancelreason;
  @JsonKey(name: Key_start_caredate)
  final String start_caredate;
  @JsonKey(name: Key_end_caredate)
  final String end_caredate;
  @JsonKey(name: Key_other_schedule_cnt)
  final int other_schedule_cnt;
  @JsonKey(name: Key_cancelexplain_deadline)
  final String cancelexplain_deadline;

  factory PayInfoCancelData.fromJson(Map<String, dynamic> json) {
    return PayInfoCancelData(
      cancel_id: json[Key_cancel_id] as int,
      order_id: json[Key_order_id] ?? '',
      cancel_usertype: json[Key_cancel_usertype] as int,
      cancel_request_date: json[Key_cancel_request_date] ?? '',
      is_refund: json[Key_is_refund] as bool,
      revoked_at: json[Key_revoked_at] ?? '',
      request_cancel_price: json[Key_request_cancel_price] as int,
      pay_expect: json[Key_pay_expect] as int,
      cancelled_price: json[Key_cancelled_price] as int,
      linkmom_paid_price: json[Key_linkmom_paid_price] as int,
      penalty_expect: json[Key_penalty_expect] as int,
      cancelreason: json[Key_cancelreason] as int,
      start_caredate: json[Key_start_caredate] ?? '',
      end_caredate: json[Key_end_caredate] ?? '',
      other_schedule_cnt: json[Key_other_schedule_cnt] as int,
      cancelexplain_deadline: json[Key_cancelexplain_deadline] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_cancel_id] = this.cancel_id;
    data[Key_order_id] = this.order_id;
    data[Key_cancel_usertype] = this.cancel_usertype;
    data[Key_cancel_request_date] = this.cancel_request_date;
    data[Key_is_refund] = this.is_refund;
    data[Key_revoked_at] = this.revoked_at;
    data[Key_request_cancel_price] = this.request_cancel_price;
    data[Key_pay_expect] = this.pay_expect;
    data[Key_cancelled_price] = this.cancelled_price;
    data[Key_linkmom_paid_price] = this.linkmom_paid_price;
    data[Key_penalty_expect] = this.penalty_expect;
    data[Key_cancelreason] = this.cancelreason;
    data[Key_start_caredate] = this.start_caredate;
    data[Key_end_caredate] = this.end_caredate;
    data[Key_other_schedule_cnt] = this.other_schedule_cnt;
    data[Key_cancelexplain_deadline] = this.cancelexplain_deadline;
    return data;
  }

  @override
  String toString() {
    return 'PayInfoCancelData{cancel_id: $cancel_id, order_id: $order_id, cancel_usertype: $cancel_usertype, cancel_request_date: $cancel_request_date, is_refund: $is_refund, revoked_at: $revoked_at, request_cancel_price: $request_cancel_price, pay_expect: $pay_expect, cancelled_price: $cancelled_price, linkmom_paid_price: $linkmom_paid_price, penalty_expect: $penalty_expect, cancelreason: $cancelreason, start_caredate: $start_caredate, end_caredate: $end_caredate, other_schedule_cnt: $other_schedule_cnt, cancelexplain_deadline: $cancelexplain_deadline}';
  }
}
