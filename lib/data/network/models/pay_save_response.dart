import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

///#### All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///#### Created by   : platformbiz@ggumbi.com
///#### version      : 1.0.0
///#### see          : pay_save_response.dart - 결제요청후 저장 PUT
///#### since        : 2021/09/09 / update:
///- 6000 - 이미 결제된 내역 입니다.
///- 6592 - 주문번호가 잘못되었습니다.
///- 6593 - 결제가 완료되지 않았습니다.
///- 3000 - 등록에 실패하였습니다.
///- (실패)
///- 6599|결제정보 누락. 확인 후 다시 결제해주세요.
@JsonSerializable()
class PaySaveResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PaySaveData>? dataList;

  PaySaveResponse({
    this.dataHeader,
    this.dataList,
  });

  PaySaveData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PaySaveData();

  List<PaySaveData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory PaySaveResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PaySaveData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<PaySaveData>((i) => PaySaveData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return PaySaveResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'PaySaveResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class PaySaveData {
  static const String Key_payment_id = 'payment_id';
  static const String Key_imp_uid = 'imp_uid';
  static const String Key_order_id = 'order_id';
  static const String Key_pay_method = 'pay_method';
  static const String Key_pg_provider = 'pg_provider';
  static const String Key_card_name = 'card_name';
  static const String Key_card_number = 'card_number';
  static const String Key_name = 'name';
  static const String Key_product_price = 'product_price';
  static const String Key_price = 'price';
  static const String Key_used_cash = 'used_cash';
  static const String Key_used_point = 'used_point';
  static const String Key_paid_at = 'paid_at';
  static const String Key_status = 'status';
  static const String Key_receipt_url = 'receipt_url';
  static const String Key_is_verify = 'is_verify';

  PaySaveData({
    this.payment_id = 0,
    this.imp_uid = '',
    this.order_id = '',
    this.pay_method = '',
    this.pg_provider = '',
    this.card_name = '',
    this.card_number = '',
    this.name = '',
    this.product_price = 0,
    this.price = 0,
    this.used_cash = 0,
    this.used_point = 0,
    this.paid_at = '',
    this.status = '',
    this.receipt_url = '',
    this.is_verify = true,
  });

  @JsonKey(name: Key_payment_id)
  final int payment_id;
  @JsonKey(name: Key_imp_uid)
  final String imp_uid;
  @JsonKey(name: Key_order_id)
  final String order_id;
  @JsonKey(name: Key_pay_method)
  final String pay_method;
  @JsonKey(name: Key_pg_provider)
  final String pg_provider;
  @JsonKey(name: Key_card_name)
  final String card_name;
  @JsonKey(name: Key_card_number)
  final String card_number;
  @JsonKey(name: Key_name)
  final String name;
  @JsonKey(name: Key_product_price)
  final int product_price;
  @JsonKey(name: Key_price)
  final int price;
  @JsonKey(name: Key_used_cash)
  int used_cash;
  @JsonKey(name: Key_used_point)
  int used_point;
  @JsonKey(name: Key_paid_at)
  String paid_at;
  @JsonKey(name: Key_status)
  final String status;
  @JsonKey(name: Key_receipt_url)
  final String receipt_url;
  @JsonKey(name: Key_is_verify)
  final bool is_verify;

  factory PaySaveData.fromJson(Map<String, dynamic> json) {
    return PaySaveData(
      payment_id: json[Key_payment_id] as int,
      imp_uid: json[Key_imp_uid] ?? '',
      order_id: json[Key_order_id] ?? '',
      pay_method: json[Key_pay_method] ?? '',
      pg_provider: json[Key_pg_provider] ?? '',
      card_name: json[Key_card_name] ?? '',
      card_number: json[Key_card_number] ?? '',
      name: json[Key_name] ?? '',
      product_price: json[Key_product_price] ?? '',
      price: json[Key_price] as int,
      used_cash: json[Key_used_cash] as int,
      used_point: json[Key_used_point] as int,
      paid_at: json[Key_paid_at] ?? '',
      status: json[Key_status] ?? '',
      receipt_url: json[Key_receipt_url] ?? '',
      is_verify: json[Key_is_verify] as bool,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_payment_id] = this.payment_id;
    data[Key_imp_uid] = this.imp_uid;
    data[Key_order_id] = this.order_id;
    data[Key_pay_method] = this.pay_method;
    data[Key_pg_provider] = this.pg_provider;
    data[Key_card_name] = this.card_name;
    data[Key_card_number] = this.card_number;
    data[Key_name] = this.name;
    data[Key_product_price] = this.product_price;
    data[Key_price] = this.price;
    data[Key_used_cash] = this.used_cash;
    data[Key_used_point] = this.used_point;
    data[Key_paid_at] = this.paid_at;
    data[Key_status] = this.status;
    data[Key_receipt_url] = this.receipt_url;
    data[Key_is_verify] = this.is_verify;
    return data;
  }

  @override
  String toString() {
    return 'PaySaveData{payment_id: $payment_id, imp_uid: $imp_uid, order_id: $order_id, pay_method: $pay_method, pg_provider: $pg_provider, card_name: $card_name, card_number: $card_number, name: $name, product_price: $product_price, price: $price, used_cash: $used_cash, used_point: $used_point, paid_at: $paid_at, status: $status, receipt_url: $receipt_url, is_verify: $is_verify}';
  }
}
