import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/main.dart';

import 'data/page_data.dart';
import 'header_response.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : linkmom_apply_list_response.dart - 지원 내역 & 필터링
/// since        : 2021/06/30 / update:
@JsonSerializable()
class LinkmomApplyListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PageData>? dataList;

  LinkmomApplyListResponse({
    this.dataHeader,
    this.dataList,
  });

  PageData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PageData(results: []);

  List<PageData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  bool isData() => dataList!.isNotEmpty;

  factory LinkmomApplyListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PageData> datas = [];
    try {
      datas = json[Key_data].map<PageData>((i) => PageData.fromJson(i, '$LinkmomApplyListResponse')).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return LinkmomApplyListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'MomdadyMatchingListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
