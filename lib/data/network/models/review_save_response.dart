import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class ReviewSaveResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ReviewSaveData>? dataList;

  ReviewSaveResponse({
    this.dataHeader,
    this.dataList,
  });

  ReviewSaveData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ReviewSaveData();

  List<ReviewSaveData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ReviewSaveResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ReviewSaveData> datas = [];
    try {
      datas = json[Key_data].map<ReviewSaveData>((i) => ReviewSaveData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return ReviewSaveResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }
}

@JsonSerializable()
class ReviewSaveData {
  static const String Key_matching = 'matching';
  static const String Key_gubun = 'gubun';
  static const String Key_review = 'review';

  ReviewSaveData({this.matching = 0, this.gubun = 0, this.review});

  @JsonKey(name: Key_matching)
  int matching;
  @JsonKey(name: Key_gubun)
  int gubun;
  @JsonKey(name: Key_review)
  List<int>? review;

  factory ReviewSaveData.fromJson(Map<String, dynamic> json) {
    List<int> review = List<int>.from(json[Key_review]);
    return ReviewSaveData(
      matching: json[Key_matching],
      gubun: json[Key_gubun],
      review: review,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_matching] = this.matching;
    data[Key_gubun] = this.gubun;
    data[Key_review] = this.review;
    return data;
  }

  @override
  String toString() {
    return 'ReviewSaveData{$Key_matching:$matching, $Key_gubun:$gubun, $Key_review:${review.toString()}}';
  }
}
