import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

@JsonSerializable()
class AuthCenterDeungbonResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<DeungbonData>? dataList;

  AuthCenterDeungbonResponse({
    this.dataHeader,
    this.dataList,
  });

  DeungbonData getData() => dataList != null && dataList!.length != 0 ? dataList![0] : DeungbonData();

  List<DeungbonData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory AuthCenterDeungbonResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<DeungbonData> datas = [];
    try {
      datas = json[Key_data].map<DeungbonData>((i) => DeungbonData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return AuthCenterDeungbonResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AuthCenterHomeResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}


@JsonSerializable()
class DeungbonData {
  static const String Key_id = 'id';
  static const String Key_createdate = 'createdate';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_createdate)
  String createdate;

  DeungbonData({
    this.id = 0,
    this.createdate = '',
  });

  factory DeungbonData.fromJson(Map<String, dynamic> json) {
    return DeungbonData(
      id: json[Key_id] as int,
      createdate: json[Key_createdate] ?? '',
    );
  }

  @override
  String toString() {
    return 'AuthCenterHomeResponse{id: $id, createdate: $createdate}';
  }
}

