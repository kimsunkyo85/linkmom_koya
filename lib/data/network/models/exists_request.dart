import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : exists_request.dart - 아이디 중복 체크, not Response
/// since        : 1/18/21 / update:
@JsonSerializable()
class ExistsRequest {
  static const String Key_username = "username";

  ExistsRequest({
    this.username = ''
  });

  @JsonKey(name: Key_username)
  final String username;

  factory ExistsRequest.fromJson(Map<String, dynamic> json) {
    return ExistsRequest(
      username: json[Key_username] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_username] = this.username;
    return data;
  }

  @override
  String toString() {
    return 'ExistsRequest{username: $username}';
  }
}

@JsonSerializable()
class ExistsDiRequest {
  static const String Key_nice_di = "nice_di";

  ExistsDiRequest({
    this.nice_di = ''
  });

  @JsonKey(name: Key_nice_di)
  final String nice_di;

  factory ExistsDiRequest.fromJson(Map<String, dynamic> json) {
    return ExistsDiRequest(
      nice_di: json[Key_nice_di] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_nice_di] = this.nice_di;
    return data;
  }

  @override
  String toString() {
    return 'ExistsDiRequest{nice_di: $nice_di}';
  }
}
