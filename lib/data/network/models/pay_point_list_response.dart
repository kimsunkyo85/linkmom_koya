import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/page_data.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///
/// Created by   : platformbiz@ggumbi.com
///
/// version      : 1.0.0
///
/// see          : pay_point_list_response.dart - 결제리스트 POST
///
/// since        : 2021/09/09 / update:
///
/// (10, '이벤트포인트(+)'),
///
/// (11, '포인트환불(+)'),
///
/// (20, '돌봄요금결제(-)'),
///
/// (99, '포인트소멸 (-)')
@JsonSerializable()
class PayPointListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PageData>? dataList;

  PayPointListResponse({
    this.dataHeader,
    this.dataList,
  });

  PageData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PageData(results: []);

  List<PageData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory PayPointListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PageData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<PageData>((i) => PageData.fromJson(i, '$PayPointListResponse')).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return PayPointListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'PayPayListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class PayPointListData {
  static const String Key_point_flag = 'point_flag';
  static const String Key_contract_id = 'contract_id';
  static const String Key_used_point = 'used_point';
  static const String Key_point_reason = 'point_reason';
  static const String Key_point_expiredate = 'point_expiredate';
  static const String Key_createdate = 'createdate';

  PayPointListData({
    this.point_flag,
    this.contract_id,
    this.used_point,
    this.point_reason,
    this.point_expiredate,
    this.createdate,
  });

  @JsonKey(name: Key_point_flag)
  final int? point_flag;
  @JsonKey(name: Key_contract_id)
  final int? contract_id;
  @JsonKey(name: Key_used_point)
  final int? used_point;
  @JsonKey(name: Key_point_reason)
  final String? point_reason;
  @JsonKey(name: Key_point_expiredate)
  final String? point_expiredate;
  @JsonKey(name: Key_createdate)
  final String? createdate;

  factory PayPointListData.fromJson(Map<String, dynamic> json) {
    return PayPointListData(
      point_flag: json[Key_point_flag] as int,
      contract_id: json[Key_contract_id] as int,
      used_point: json[Key_used_point] as int,
      point_reason: json[Key_point_reason] ?? '',
      point_expiredate: json[Key_point_expiredate] ?? '',
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_point_flag] = this.point_flag;
    data[Key_contract_id] = this.contract_id;
    data[Key_used_point] = this.used_point;
    data[Key_point_reason] = this.point_reason;
    data[Key_point_expiredate] = this.point_expiredate;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'PayPointListResponse{point_flag: $point_flag, contract_id: $contract_id, used_point: $used_point, point_reason: $point_reason, point_expiredate: $point_expiredate, createdate: $createdate}';
  }
}
