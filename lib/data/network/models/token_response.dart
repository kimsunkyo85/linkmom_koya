class TokenResponse {
  static const String key_refresh = "refresh";
  static const String key_access = "access";
  final String refresh;
  final String access;

  TokenResponse({this.refresh = '', this.access = ''});

  factory TokenResponse.fromJson(Map<String, dynamic> json) {
    return TokenResponse(
      refresh: json[key_refresh] ?? "",
      access: json[key_access] ?? "",
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[key_refresh] = this.refresh;
    data[key_access] = this.access;
    return data;
  }
}
