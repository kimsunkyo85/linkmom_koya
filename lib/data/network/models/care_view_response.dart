import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/utils/string_util.dart';

import '../../../main.dart';
import 'data/auth_info_data.dart';
import 'req_data.dart';

@JsonSerializable()
class CareViewResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<CareViewData>? dataList;

  CareViewResponse({
    this.dataHeader,
    this.dataList,
  });

  CareViewData getData() => dataList != null && dataList!.isNotEmpty
      ? dataList!.first
      : CareViewData(
          authinfo: AuthInfoData(),
          careboyuk: CareViewBoyukData(),
          caredate: [],
          careoptions: CareViewOptionData(),
          careschool: CareViewSchoolData(),
          childinfo: CareViewChildInfoData(),
          userinfo: CareViewUserInfoData(),
        );

  List<CareViewData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  bool isData() => dataList!.isNotEmpty;

  factory CareViewResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareViewData> datas = [];
    try {
      datas = json[Key_data].map<CareViewData>((i) => CareViewData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareViewResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CareViewResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class CareViewData {
  static const String Key_booking_id = 'booking_id';
  static const String Key_cost_sum = 'cost_sum';
  static const String Key_cost_avg = 'cost_avg';
  static const String Key_is_negotiable = 'is_negotiable';
  static const String Key_is_togetherboyuk = 'is_togetherboyuk';
  static const String Key_status = 'status';
  static const String Key_servicetype = 'servicetype';
  static const String Key_possible_area = 'possible_area';
  static const String Key_userinfo = 'userinfo';
  static const String Key_userhomestat = 'userhomestat';
  static const String Key_authinfo = 'authinfo';
  static const String Key_childinfo = 'childinfo';
  static const String Key_stime = 'stime';
  static const String Key_etime = 'etime';
  static const String Key_caredate = 'caredate';
  static const String Key_careschool = 'careschool';
  static const String Key_careboyuk = 'careboyuk';
  static const String Key_careoptions = 'careoptions';
  static const String Key_reqdata = 'reqdata';

  CareViewData({
    this.booking_id = 0,
    this.cost_sum = 0,
    this.cost_avg = 0,
    this.is_negotiable = false,
    this.is_togetherboyuk = false,
    this.status = 0,
    this.servicetype = 0,
    this.possible_area = 0,
    this.userinfo,
    this.authinfo,
    this.childinfo,
    this.stime = '',
    this.etime = '',
    this.caredate,
    this.careschool,
    this.careboyuk,
    this.careoptions,
    this.reqdata,
  });

  @JsonKey(name: Key_booking_id)
  int booking_id;
  @JsonKey(name: Key_cost_sum)
  int cost_sum;
  @JsonKey(name: Key_cost_avg)
  int cost_avg;
  @JsonKey(name: Key_is_negotiable)
  bool is_negotiable;
  @JsonKey(name: Key_servicetype)
  bool is_togetherboyuk;
  @JsonKey(name: Key_status)
  int status;
  @JsonKey(name: Key_is_togetherboyuk)
  int servicetype;
  @JsonKey(name: Key_possible_area)
  int possible_area;
  @JsonKey(name: Key_userinfo)
  CareViewUserInfoData? userinfo;
  @JsonKey(name: Key_userhomestat)
  @JsonKey(name: Key_authinfo)
  AuthInfoData? authinfo;
  @JsonKey(name: Key_childinfo)
  CareViewChildInfoData? childinfo;
  @JsonKey(name: Key_stime)
  String stime;
  @JsonKey(name: Key_etime)
  String etime;
  @JsonKey(name: Key_caredate)
  List<String>? caredate;
  @JsonKey(name: Key_careschool)
  CareViewSchoolData? careschool;
  @JsonKey(name: Key_careboyuk)
  CareViewBoyukData? careboyuk;
  @JsonKey(name: Key_careoptions)
  CareViewOptionData? careoptions;
  @JsonKey(name: Key_reqdata)
  ReqData? reqdata;

  factory CareViewData.fromJson(Map<String, dynamic> json) {
    CareViewUserInfoData userinfo = CareViewUserInfoData.fromJson(json[Key_userinfo]);
    AuthInfoData authinfo = AuthInfoData.fromJson(json[Key_authinfo]);
    CareViewChildInfoData childinfo = CareViewChildInfoData.fromJson(json[Key_childinfo]);
    List<String> caredate = json[Key_caredate] == null ? [] : List<String>.from(json[Key_caredate]);
    CareViewSchoolData careschool = CareViewSchoolData.fromJson(json[Key_careschool]);
    CareViewBoyukData careboyuk = CareViewBoyukData.fromJson(json[Key_careboyuk]);
    CareViewOptionData careoptions = CareViewOptionData.fromJson(json[Key_careoptions]);
    ReqData reqdata = ReqData();
    try {
      reqdata = json[Key_reqdata] == null ? ReqData() : ReqData.fromJson(jsonDecode(json[Key_reqdata]));
    } catch (e) {
      reqdata = ReqData();
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareViewData(
      booking_id: json[Key_booking_id] as int,
      cost_sum: json[Key_cost_sum] as int,
      cost_avg: json[Key_cost_avg] as int,
      is_negotiable: json[Key_is_negotiable] as bool,
      is_togetherboyuk: json[Key_is_togetherboyuk] as bool,
      status: json[Key_status] as int,
      servicetype: json[Key_servicetype] as int,
      possible_area: json[Key_possible_area] as int,
      userinfo: userinfo,
      authinfo: authinfo,
      childinfo: childinfo,
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      caredate: caredate,
      careschool: careschool,
      careboyuk: careboyuk,
      careoptions: careoptions,
      reqdata: reqdata,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_booking_id] = this.booking_id;
    data[Key_cost_sum] = this.cost_sum;
    data[Key_cost_avg] = this.cost_avg;
    data[Key_is_negotiable] = this.is_negotiable;
    data[Key_is_togetherboyuk] = this.is_togetherboyuk;
    data[Key_status] = this.status;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_userinfo] = this.userinfo;
    // data[Key_userhomestat] = this.userhomestat;
    data[Key_authinfo] = this.authinfo;
    data[Key_childinfo] = this.childinfo;
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_caredate] = this.caredate;
    data[Key_careschool] = this.careschool;
    data[Key_careboyuk] = this.careboyuk;
    data[Key_careoptions] = this.careoptions;
    data[Key_reqdata] = this.reqdata;
    return data;
  }

  @override
  String toString() {
    return 'CareViewData{booking_id: $booking_id, cost_sum: $cost_sum, cost_avg: $cost_avg, is_negotiable: $is_negotiable, is_togetherboyuk: $is_togetherboyuk, status: $status, servicetype: $servicetype, possible_area: $possible_area, userinfo: $userinfo, authinfo: $authinfo, childinfo: $childinfo, stime: $stime, etime: $etime, caredate: $caredate, careschool: $careschool, careboyuk: $careboyuk, careoptions: $careoptions, reqdata: $reqdata}';
  }
}

///나의정보
class CareViewUserInfoData {
  static const String Key_user_id = 'user_id';
  static const String Key_first_name = 'first_name';
  static const String Key_region_2depth = 'region_2depth';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_4depth = 'region_4depth';
  static const String Key_is_affiliate = 'is_affiliate';
  static const String Key_profileimg = 'profileimg';
  static const String Key_is_follower = 'is_follower';

  CareViewUserInfoData({
    this.user_id = 0,
    this.first_name = '',
    this.region_2depth = '',
    this.region_3depth = '',
    this.region_4depth = '',
    this.is_affiliate = false,
    this.profileimg = '',
    this.is_follower = false,
  });

  @JsonKey(name: Key_user_id)
  final int user_id;
  @JsonKey(name: Key_first_name)
  final String first_name;
  @JsonKey(name: Key_region_2depth)
  final String region_2depth;
  @JsonKey(name: Key_region_3depth)
  final String region_3depth;
  @JsonKey(name: Key_region_4depth)
  final String region_4depth;

  ///제휴 인증 여부 true, false
  @JsonKey(name: Key_is_affiliate)
  final bool is_affiliate;
  @JsonKey(name: Key_profileimg)
  final String profileimg;
  @JsonKey(name: Key_is_follower)
  bool is_follower;

  factory CareViewUserInfoData.fromJson(Map<String, dynamic> json) {
    return CareViewUserInfoData(
      user_id: json[Key_user_id] as int,
      first_name: json[Key_first_name] ?? '',
      region_2depth: json[Key_region_2depth] ?? '',
      region_3depth: json[Key_region_3depth] ?? '',
      region_4depth: json[Key_region_4depth] ?? '',
      is_affiliate: json[Key_is_affiliate] ?? false,
      profileimg: json[Key_profileimg] ?? '',
      is_follower: json[Key_is_follower] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_user_id] = this.user_id;
    data[Key_first_name] = this.first_name;
    data[Key_region_2depth] = this.region_2depth;
    data[Key_region_3depth] = this.region_3depth;
    data[Key_region_4depth] = this.region_4depth;
    data[Key_is_affiliate] = this.is_affiliate;
    data[Key_profileimg] = this.profileimg;
    data[Key_is_follower] = this.is_follower;
    return data;
  }

  String getAddress() {
    return StringUtils.getAddressParser(region_3depth, region_4depth);
  }

  @override
  String toString() {
    return 'CareViewUserInfoData{user_id: $user_id, first_name: $first_name, region_2depth: $region_2depth, region_3depth: $region_3depth, region_4depth: $region_4depth, is_affiliate: $is_affiliate, profileimg: $profileimg, is_follower: $is_follower}';
  }
}

///아이정보
class CareViewChildInfoData {
  static const String Key_child_id = 'child_id';
  static const String Key_child_name = 'child_name';
  static const String Key_child_age = 'child_age';
  static const String Key_child_gender = 'child_gender';
  static const String Key_child_character = 'child_character';
  static const String Key_is_allergy = 'is_allergy';
  static const String Key_allergy_name = 'allergy_name';
  static const String Key_allergy_message = 'allergy_message';

  CareViewChildInfoData({
    this.child_id = 0,
    this.child_name = '',
    this.child_age = '',
    this.child_gender = '',
    this.child_character = '',
    this.is_allergy = false,
    this.allergy_name = '',
    this.allergy_message = '',
  });

  @JsonKey(name: Key_child_id)
  int child_id;
  @JsonKey(name: Key_child_name)
  String child_name;
  @JsonKey(name: Key_child_age)
  String child_age;
  @JsonKey(name: Key_child_gender)
  String child_gender;
  @JsonKey(name: Key_child_character)
  String child_character;
  @JsonKey(name: Key_is_allergy)
  bool is_allergy;
  @JsonKey(name: Key_allergy_name)
  String allergy_name;
  @JsonKey(name: Key_allergy_message)
  String allergy_message;

  factory CareViewChildInfoData.fromJson(Map<String, dynamic> json) {
    return CareViewChildInfoData(
      child_id: json[Key_child_id] as int,
      child_name: json[Key_child_name] ?? '',
      child_age: json[Key_child_age] ?? '',
      child_gender: json[Key_child_gender] ?? '',
      child_character: json[Key_child_character] ?? '',
      is_allergy: json[Key_is_allergy] ?? false,
      allergy_name: json[Key_allergy_name] ?? '',
      allergy_message: json[Key_allergy_message] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_child_id] = this.child_id;
    data[Key_child_name] = this.child_name;
    data[Key_child_age] = this.child_age;
    data[Key_child_gender] = this.child_gender;
    data[Key_child_character] = this.child_character;
    data[Key_is_allergy] = this.is_allergy;
    data[Key_allergy_name] = this.allergy_name;
    data[Key_allergy_message] = this.allergy_message;
    return data;
  }

  @override
  String toString() {
    return 'CareViewChildInfoData{child_id: $child_id, child_name: $child_name, child_age: $child_age, child_gender: $child_gender, child_character: $child_character, is_allergy: $is_allergy, allergy_name: $allergy_name, allergy_message: $allergy_message}';
  }
}

///등/하원데이터
class CareViewSchoolData {
  static const String Key_vehicle_id = 'vehicle_id';
  static const String Key_start_address = 'start_address';
  static const String Key_start_time = 'start_time';
  static const String Key_start_comment = 'start_comment';
  static const String Key_pathroute_comment = 'pathroute_comment';
  static const String Key_nhn_area_1_address = 'nhn_area_1_address';
  static const String Key_nhn_area_2_address = 'nhn_area_2_address';
  static const String Key_nhn_comment = 'nhn_comment';
  static const String Key_end_address = 'end_address';
  static const String Key_end_time = 'end_time';
  static const String Key_end_comment = 'end_comment';

  CareViewSchoolData({
    this.vehicle_id,
    this.start_address = '',
    this.start_time = '',
    this.start_comment = '',
    this.pathroute_comment = '',
    this.nhn_area_1_address = '',
    this.nhn_area_2_address = '',
    this.nhn_comment = '',
    this.end_address = '',
    this.end_time = '',
    this.end_comment = '',
  });

  @JsonKey(name: Key_vehicle_id)
  List<int>? vehicle_id;
  @JsonKey(name: Key_start_address)
  String start_address;
  @JsonKey(name: Key_start_time)
  String start_time;
  @JsonKey(name: Key_start_comment)
  String start_comment;
  @JsonKey(name: Key_pathroute_comment)
  String pathroute_comment;
  @JsonKey(name: Key_nhn_area_1_address)
  String nhn_area_1_address;
  @JsonKey(name: Key_nhn_area_2_address)
  String nhn_area_2_address;
  @JsonKey(name: Key_nhn_comment)
  String nhn_comment;
  @JsonKey(name: Key_end_address)
  String end_address;
  @JsonKey(name: Key_end_time)
  String end_time;
  @JsonKey(name: Key_end_comment)
  String end_comment;

  factory CareViewSchoolData.fromJson(Map<String, dynamic> json) {
    List<int> vehicleId = json[Key_vehicle_id] == null ? [] : List<int>.from(json[Key_vehicle_id]);
    return CareViewSchoolData(
      vehicle_id: vehicleId,
      start_address: json[Key_start_address] ?? '',
      start_time: json[Key_start_time] ?? '',
      start_comment: json[Key_start_comment] ?? '',
      pathroute_comment: json[Key_pathroute_comment] ?? '',
      nhn_area_1_address: json[Key_nhn_area_1_address] ?? '',
      nhn_area_2_address: json[Key_nhn_area_2_address] ?? '',
      nhn_comment: json[Key_nhn_comment] ?? '',
      end_address: json[Key_end_address] ?? '',
      end_time: json[Key_end_time] ?? '',
      end_comment: json[Key_end_comment] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_vehicle_id] = this.vehicle_id;
    data[Key_start_address] = this.start_address;
    data[Key_start_time] = this.start_time;
    data[Key_start_comment] = this.start_comment;
    data[Key_pathroute_comment] = this.pathroute_comment;
    data[Key_nhn_area_1_address] = this.nhn_area_1_address;
    data[Key_nhn_area_2_address] = this.nhn_area_2_address;
    data[Key_nhn_comment] = this.nhn_comment;
    data[Key_end_address] = this.end_address;
    data[Key_end_time] = this.end_time;
    data[Key_end_comment] = this.end_comment;
    return data;
  }

  @override
  String toString() {
    return 'CareViewSchoolData{vehicle_id: $vehicle_id, start_address: $start_address, start_time: $start_time, start_comment: $start_comment, pathroute_comment: $pathroute_comment, nhn_area_1_address: $nhn_area_1_address, nhn_area_2_address: $nhn_area_2_address, nhn_comment: $nhn_comment, end_address: $end_address, end_time: $end_time, end_comment: $end_comment}';
  }
}

///보육 데이터
class CareViewBoyukData {
  static const String Key_boyuk_address = 'boyuk_address';
  static const String Key_nhn_area_1_address = 'nhn_area_1_address';
  static const String Key_nhn_area_2_address = 'nhn_area_2_address';
  static const String Key_nhn_comment = 'nhn_comment';
  static const String Key_pathroute_comment = 'pathroute_comment';

  CareViewBoyukData({
    this.boyuk_address = '',
    this.nhn_area_1_address = '',
    this.nhn_area_2_address = '',
    this.nhn_comment = '',
    this.pathroute_comment = '',
  });

  @JsonKey(name: Key_boyuk_address)
  String boyuk_address;
  @JsonKey(name: Key_nhn_area_1_address)
  String nhn_area_1_address;
  @JsonKey(name: Key_nhn_area_2_address)
  String nhn_area_2_address;
  @JsonKey(name: Key_nhn_comment)
  String nhn_comment;
  @JsonKey(name: Key_pathroute_comment)
  String pathroute_comment;

  factory CareViewBoyukData.fromJson(Map<String, dynamic> json) {
    return CareViewBoyukData(
      boyuk_address: json[Key_boyuk_address] ?? '',
      nhn_area_1_address: json[Key_nhn_area_1_address] ?? '',
      nhn_area_2_address: json[Key_nhn_area_2_address] ?? '',
      nhn_comment: json[Key_nhn_comment] ?? '',
      pathroute_comment: json[Key_pathroute_comment] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_boyuk_address] = this.boyuk_address;
    data[Key_nhn_area_1_address] = this.nhn_area_1_address;
    data[Key_nhn_area_2_address] = this.nhn_area_2_address;
    data[Key_nhn_comment] = this.nhn_comment;
    data[Key_pathroute_comment] = this.pathroute_comment;
    return data;
  }

  @override
  String toString() {
    return 'CareViewBoyukData{boyuk_address: $boyuk_address, nhn_area_1_address: $nhn_area_1_address, nhn_area_2_address: $nhn_area_2_address, nhn_comment: $nhn_comment, pathroute_comment: $pathroute_comment}';
  }
}

///서비스 데이터
class CareViewOptionData {
  static const String Key_bookingboyukoptions_product = 'bookingboyukoptions_product';
  static const String Key_bookinghomecareoptions_product = 'bookinghomecareoptions_product';
  static const String Key_bookingplayoptions_product = 'bookingplayoptions_product';

  CareViewOptionData({
    this.bookingboyukoptions_product,
    this.bookinghomecareoptions_product,
    this.bookingplayoptions_product,
  });

  @JsonKey(name: Key_bookingboyukoptions_product)
  final List<int>? bookingboyukoptions_product;
  @JsonKey(name: Key_bookinghomecareoptions_product)
  final List<int>? bookinghomecareoptions_product;
  @JsonKey(name: Key_bookingplayoptions_product)
  final List<int>? bookingplayoptions_product;

  factory CareViewOptionData.fromJson(Map<String, dynamic> json) {
    List<int> bookingboyukoptionsProduct = json[Key_bookingboyukoptions_product] == null ? [] : List<int>.from(json[Key_bookingboyukoptions_product]);
    List<int> bookinghomecareoptionsProduct = json[Key_bookinghomecareoptions_product] == null ? [] : List<int>.from(json[Key_bookinghomecareoptions_product]);
    List<int> bookingplayoptionsProduct = json[Key_bookingplayoptions_product] == null ? [] : List<int>.from(json[Key_bookingplayoptions_product]);
    return CareViewOptionData(
      bookingboyukoptions_product: bookingboyukoptionsProduct,
      bookinghomecareoptions_product: bookinghomecareoptionsProduct,
      bookingplayoptions_product: bookingplayoptionsProduct,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_bookingboyukoptions_product] = this.bookingboyukoptions_product;
    data[Key_bookinghomecareoptions_product] = this.bookinghomecareoptions_product;
    data[Key_bookingplayoptions_product] = this.bookingplayoptions_product;
    return data;
  }

  @override
  String toString() {
    return 'CareViewOptionData{bookingboyukoptions_product: $bookingboyukoptions_product, bookinghomecareoptions_product: $bookinghomecareoptions_product, bookingplayoptions_product: $bookingplayoptions_product}';
  }
}
