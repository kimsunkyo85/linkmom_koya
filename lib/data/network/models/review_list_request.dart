import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class ReviewListRequest {
  static const String Key_type = 'type';
  static const String Key_gubun = 'gubun';
  static const String Key_user_id = 'user_id';

  ReviewListRequest({this.type = 0, this.gubun = 0, this.userId = 0});

  @JsonKey(name: Key_type)
  int type;
  @JsonKey(name: Key_gubun)
  int gubun;
  @JsonKey(name: Key_user_id)
  int userId;

  factory ReviewListRequest.fromJson(Map<String, dynamic> json) {
    return ReviewListRequest(
      type: json[Key_type] ?? 0,
      gubun: json[Key_gubun] ?? 0,
      userId: json[Key_user_id] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_type] = this.type;
    data[Key_gubun] = this.gubun;
    data[Key_user_id] = this.userId;
    return data;
  }

  FormData toFormData(FormData form) {
    form.fields.add(MapEntry(Key_type, this.type.toString()));
    form.fields.add(MapEntry(Key_gubun, this.gubun.toString()));
    if (this.userId != 0) form.fields.add(MapEntry(Key_user_id, this.userId.toString()));
    return form;
  }

  @override
  String toString() {
    return 'ReviewListRequest{$Key_type:$type, $Key_gubun:$gubun, $Key_user_id: $userId}';
  }
}
