import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class SurveyQuestionRequest {
  static const String Key_page = "page";

  SurveyQuestionRequest({
    this.page = 0
  });

  @JsonKey(name: Key_page)
  int page;

  factory SurveyQuestionRequest.fromJson(Map<String, dynamic> json) {
    return SurveyQuestionRequest(
      page: json[Key_page] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_page] = this.page;
    return data;
  }

  @override
  String toString() {
    return 'SurveyQuestionRequest{page: $page}';
  }
}
