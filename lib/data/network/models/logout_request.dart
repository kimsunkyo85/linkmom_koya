import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : logout_request.dart - logout to header response
/// since        : 1/18/21 / update:
class LogoutRequest {
  static const String Key_fcm_token = "fcm_token";

  LogoutRequest({
    this.fcm_token = '',
  });

  @JsonKey(name: Key_fcm_token)
  final String fcm_token;

  factory LogoutRequest.fromJson(Map<String, dynamic> json) {
    return LogoutRequest(
      fcm_token: json[Key_fcm_token] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_fcm_token] = this.fcm_token;
    return data;
  }

  @override
  String toString() {
    return 'LogoutRequest{fcm_token: $fcm_token}';
  }
}
