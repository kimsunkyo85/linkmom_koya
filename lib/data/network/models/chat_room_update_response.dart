import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/chat_room_data.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_room_update_response.dart - 채팅 방 정보 업데이트 응답
/// since        : 2021/07/15 / update:
@JsonSerializable()
class ChatRoomUpdateResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ChatRoomData>? dataList;

  ChatRoomUpdateResponse({
    this.dataHeader,
    this.dataList,
  });

  ChatRoomData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ChatRoomData();

  List<ChatRoomData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ChatRoomUpdateResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ChatRoomData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<ChatRoomData>((i) => ChatRoomData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return ChatRoomUpdateResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ChatRoomUpdateResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
//
// @JsonSerializable()
// class ChatRoomUpdateData {
//   static const String Key_chatroom_id = 'chatroom_id';
//   static const String Key_chattingtype = 'chattingtype';
//   static const String Key_bookingcareservices = 'bookingcareservices';
//   static const String Key_bookingcareservices_info = 'bookingcareservices_info';
//   static const String Key_carematching = 'carematching';
//   static const String Key_sender = 'sender';
//   static const String Key_sender_info = 'sender_info';
//   static const String Key_receiver = 'receiver';
//   static const String Key_receiver_info = 'receiver_info';
//   static const String Key_createdate = 'createdate';
//
//   ChatRoomUpdateData({
//     this.chatroom_id,
//     this.chattingtype,
//     this.bookingcareservices,
//     this.bookingcareservices_info,
//     this.carematching,
//     this.sender,
//     this.sender_info,
//     this.receiver,
//     this.receiver_info,
//     this.createdate,
//   });
//
//   @JsonKey(name: Key_chatroom_id)
//   final int chatroom_id;
//   @JsonKey(name: Key_chattingtype)
//   final String chattingtype;
//   @JsonKey(name: Key_bookingcareservices)
//   final int bookingcareservices;
//   @JsonKey(name: Key_bookingcareservices_info)
//   final ChatUpdateBookingCareServicesInfoData bookingcareservices_info;
//   @JsonKey(name: Key_carematching)
//   final ChatUpdateCareMatchingData carematching;
//   @JsonKey(name: Key_sender)
//   final int sender;
//   @JsonKey(name: Key_sender_info)
//   final ChatUpdateMessageInfoData sender_info;
//   @JsonKey(name: Key_receiver)
//   final int receiver;
//   @JsonKey(name: Key_receiver_info)
//   final ChatUpdateMessageInfoData receiver_info;
//   @JsonKey(name: Key_createdate)
//   final String createdate;
//
//   factory ChatRoomUpdateData.fromJson(Map<String, dynamic> json) {
//     ChatUpdateBookingCareServicesInfoData bookingcareservices_info = ChatUpdateBookingCareServicesInfoData.fromJson(json[Key_bookingcareservices]);
//     ChatUpdateCareMatchingData carematching = ChatUpdateCareMatchingData.fromJson(json[Key_carematching]);
//     ChatUpdateMessageInfoData sender_info = ChatUpdateMessageInfoData.fromJson(json[Key_sender_info]);
//     ChatUpdateMessageInfoData receiver_info = ChatUpdateMessageInfoData.fromJson(json[Key_receiver_info]);
//     // try {
//     //   bookingcareservices_info = BookingCareServicesInfoData.fromJson(json[Key_bookingcareservices]);
//     //   carematching = CareMatchingData.fromJson(json[Key_carematching]);
//     //   sender_info = MessageInfoData.fromJson(json[Key_sender_info]);
//     //   receiver_info = MessageInfoData.fromJson(json[Key_receiver_info]);
//     // } catch (e) {
//     //   log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
//     // }
//     return ChatRoomUpdateData(
//       chatroom_id: json[Key_chatroom_id] as int,
//       chattingtype: json[Key_chattingtype] ?? '',
//       bookingcareservices: json[Key_bookingcareservices] as int,
//       bookingcareservices_info: bookingcareservices_info,
//       carematching: carematching,
//       sender: json[Key_sender] as int,
//       sender_info: sender_info,
//       receiver: json[Key_receiver] as int,
//       receiver_info: receiver_info,
//       createdate: json[Key_createdate] ?? '',
//     );
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = Map<String, dynamic>();
//     data[Key_chatroom_id] = this.chatroom_id;
//     data[Key_chattingtype] = this.chattingtype;
//     data[Key_bookingcareservices] = this.bookingcareservices;
//     data[Key_bookingcareservices_info] = this.bookingcareservices_info;
//     data[Key_carematching] = this.carematching;
//     data[Key_sender] = this.sender;
//     data[Key_sender_info] = this.sender_info;
//     data[Key_receiver] = this.receiver;
//     data[Key_receiver_info] = this.receiver_info;
//     data[Key_createdate] = this.createdate;
//     return data;
//   }
//
//   @override
//   String toString() {
//     return 'ChatRoomInitData{chatroom_id: $chatroom_id, chattingtype: $chattingtype, bookingcareservices: $bookingcareservices, bookingcareservices_info: $bookingcareservices_info, carematching: $carematching, sender: $sender, sender_info: $sender_info, receiver: $receiver, receiver_info: $receiver_info, createdate: $createdate}';
//   }
// }
//
// ///매칭 정보
// class ChatUpdateBookingCareServicesInfoData {
//   static const String Key_careinfo = 'careinfo';
//   static const String Key_userinfo = 'userinfo';
//   static const String Key_childinfo = 'childinfo';
//
//   ChatUpdateBookingCareServicesInfoData({
//     this.careinfo,
//     this.userinfo,
//     this.childinfo,
//   });
//
//   @JsonKey(name: Key_careinfo)
//   final ChatUpdateCareInfoData careinfo;
//   @JsonKey(name: Key_userinfo)
//   final ChatUpdateUserInfoData userinfo;
//   @JsonKey(name: Key_childinfo)
//   final ChatUpdateChildInfoData childinfo;
//
//   factory ChatUpdateBookingCareServicesInfoData.fromJson(Map<String, dynamic> json) {
//     ChatUpdateCareInfoData careinfo = ChatUpdateCareInfoData.fromJson(json[Key_careinfo]);
//     ChatUpdateUserInfoData userinfo = ChatUpdateUserInfoData.fromJson(json[Key_userinfo]);
//     ChatUpdateChildInfoData childinfo = ChatUpdateChildInfoData.fromJson(json[Key_childinfo]);
//     return ChatUpdateBookingCareServicesInfoData(
//       careinfo: careinfo,
//       userinfo: userinfo,
//       childinfo: childinfo,
//     );
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data[Key_careinfo] = this.careinfo;
//     data[Key_userinfo] = this.userinfo;
//     data[Key_childinfo] = this.childinfo;
//     return data;
//   }
//
//   @override
//   String toString() {
//     return 'BookingCareServicesInfoData{careinfo: $careinfo, userinfo: $userinfo, childinfo: $childinfo}';
//   }
// }
//
// ///케어 정보
// class ChatUpdateCareInfoData {
//   static const String Key_booking_id = 'booking_id';
//   static const String Key_booking_status = 'booking_status';
//   static const String Key_servicetype = 'servicetype';
//   static const String Key_cost_sum = 'cost_sum';
//   static const String Key_stime = 'stime';
//   static const String Key_etime = 'etime';
//   static const String Key_caredate = 'caredate';
//   static const String Key_possible_area = 'possible_area';
//
//   ChatUpdateCareInfoData({
//     this.booking_id,
//     this.booking_status,
//     this.servicetype,
//     this.cost_sum,
//     this.stime,
//     this.etime,
//     this.caredate,
//     this.possible_area,
//   });
//
//   @JsonKey(name: Key_booking_id)
//   final int booking_id;
//   @JsonKey(name: Key_booking_status)
//   final String booking_status;
//   @JsonKey(name: Key_servicetype)
//   final String servicetype;
//   @JsonKey(name: Key_cost_sum)
//   final int cost_sum;
//   @JsonKey(name: Key_stime)
//   final String stime;
//   @JsonKey(name: Key_etime)
//   final String etime;
//   @JsonKey(name: Key_caredate)
//   final List<String> caredate;
//   @JsonKey(name: Key_possible_area)
//   final String possible_area;
//
//   factory ChatUpdateCareInfoData.fromJson(Map<String, dynamic> json) {
//     List<String> caredate = json[Key_caredate] == null ? List() : List<String>.from(json[Key_caredate]);
//     return ChatUpdateCareInfoData(
//       booking_id: json[Key_booking_id] as int,
//       booking_status: json[Key_booking_status] ?? '',
//       servicetype: json[Key_servicetype] ?? '',
//       cost_sum: json[Key_cost_sum] as int,
//       stime: json[Key_stime] ?? '',
//       etime: json[Key_etime] ?? '',
//       caredate: caredate,
//       possible_area: json[Key_possible_area] ?? '',
//     );
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data[Key_booking_id] = this.booking_id;
//     data[Key_booking_status] = this.booking_status;
//     data[Key_servicetype] = this.servicetype;
//     data[Key_cost_sum] = this.cost_sum;
//     data[Key_stime] = this.stime;
//     data[Key_etime] = this.etime;
//     data[Key_caredate] = this.caredate;
//     data[Key_possible_area] = this.possible_area;
//     return data;
//   }
//
//   @override
//   String toString() {
//     return 'CareInfoData{booking_id: $booking_id, booking_status: $booking_status, servicetype: $servicetype, cost_sum: $cost_sum, stime: $stime, etime: $etime, caredate: $caredate, possible_area: $possible_area}';
//   }
// }
//
// ///사용자 정보
// class ChatUpdateUserInfoData {
//   static const String Key_id = 'id';
//   static const String Key_first_name = 'first_name';
//   static const String Key_profileimg = 'profileimg';
//   static const String Key_region_2depth = 'region_2depth';
//   static const String Key_region_3depth = 'region_3depth';
//   static const String Key_region_4depth = 'region_4depth';
//
//   ChatUpdateUserInfoData({
//     this.id,
//     this.first_name,
//     this.profileimg,
//     this.region_2depth,
//     this.region_3depth,
//     this.region_4depth,
//   });
//
//   @JsonKey(name: Key_id)
//   final int id;
//   @JsonKey(name: Key_first_name)
//   final String first_name;
//   @JsonKey(name: Key_profileimg)
//   final String profileimg;
//   @JsonKey(name: Key_region_2depth)
//   final String region_2depth;
//   @JsonKey(name: Key_region_3depth)
//   final String region_3depth;
//   @JsonKey(name: Key_region_4depth)
//   final String region_4depth;
//
//   factory ChatUpdateUserInfoData.fromJson(Map<String, dynamic> json) {
//     return ChatUpdateUserInfoData(
//       id: json[Key_id] as int,
//       first_name: json[Key_first_name] ?? '',
//       profileimg: json[Key_profileimg] ?? '',
//       region_2depth: json[Key_region_2depth] ?? '',
//       region_3depth: json[Key_region_3depth] ?? '',
//       region_4depth: json[Key_region_4depth] ?? '',
//     );
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data[Key_id] = this.id;
//     data[Key_first_name] = this.first_name;
//     data[Key_profileimg] = this.profileimg;
//     data[Key_region_2depth] = this.region_2depth;
//     data[Key_region_3depth] = this.region_3depth;
//     data[Key_region_4depth] = this.region_4depth;
//     return data;
//   }
//
//   @override
//   String toString() {
//     return 'UserInfoData{id: $id, first_name: $first_name, profileimg: $profileimg, region_2depth: $region_2depth, region_3depth: $region_3depth, region_4depth: $region_4depth}';
//   }
// }
//
// ///사용자 정보
// class ChatUpdateChildInfoData {
//   static const String Key_child_name = 'child_name';
//   static const String Key_child_age = 'child_age';
//   static const String Key_child_gender = 'child_gender';
//
//   ChatUpdateChildInfoData({
//     this.child_name,
//     this.child_age,
//     this.child_gender,
//   });
//
//   @JsonKey(name: Key_child_name)
//   final String child_name;
//   @JsonKey(name: Key_child_age)
//   final String child_age;
//   @JsonKey(name: Key_child_gender)
//   final String child_gender;
//
//   factory ChatUpdateChildInfoData.fromJson(Map<String, dynamic> json) {
//     return ChatUpdateChildInfoData(
//       child_name: json[Key_child_name] ?? '',
//       child_age: json[Key_child_age] ?? '',
//       child_gender: json[Key_child_gender] ?? '',
//     );
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data[Key_child_name] = this.child_name;
//     data[Key_child_age] = this.child_age;
//     data[Key_child_gender] = this.child_gender;
//     return data;
//   }
//
//   @override
//   String toString() {
//     return 'ChildInfoData{child_name: $child_name, child_age: $child_age, child_gender: $child_gender}';
//   }
// }
//
// ///매칭 데이터
// class ChatUpdateCareMatchingData {
//   static const String Key_matching_id = 'matching_id';
//   static const String Key_matching_status = 'matching_status';
//   static const String Key_care_status = 'care_status';
//   static const String Key_paid_deadline = 'paid_deadline';
//   static const String Key_momdady = 'momdady';
//   static const String Key_linkmom = 'linkmom';
//
//   ChatUpdateCareMatchingData({
//     this.matching_id,
//     this.matching_status,
//     this.care_status,
//     this.paid_deadline,
//     this.momdady,
//     this.linkmom,
//   });
//
//   @JsonKey(name: Key_matching_id)
//   final int matching_id;
//   @JsonKey(name: Key_matching_status)
//   final int matching_status;
//   @JsonKey(name: Key_care_status)
//   final int care_status;
//   @JsonKey(name: Key_paid_deadline)
//   final String paid_deadline;
//   @JsonKey(name: Key_momdady)
//   final int momdady;
//   @JsonKey(name: Key_linkmom)
//   final int linkmom;
//
//   factory ChatUpdateCareMatchingData.fromJson(Map<String, dynamic> json) {
//     return ChatUpdateCareMatchingData(
//       matching_id: json[Key_matching_id] as int,
//       matching_status: json[Key_matching_status] as int,
//       care_status: json[Key_care_status] as int,
//       paid_deadline: json[Key_paid_deadline] ?? '',
//       momdady: json[Key_momdady] as int,
//       linkmom: json[Key_linkmom] as int,
//     );
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data[Key_matching_id] = this.matching_id;
//     data[Key_matching_status] = this.matching_status;
//     data[Key_care_status] = this.care_status;
//     data[Key_paid_deadline] = this.paid_deadline;
//     data[Key_momdady] = this.momdady;
//     data[Key_linkmom] = this.linkmom;
//     return data;
//   }
//
//   @override
//   String toString() {
//     return 'CareMatchingData{matching_id: $matching_id, matching_status: $matching_status, care_status: $care_status, paid_deadline: $paid_deadline, momdady: $momdady, linkmom: $linkmom}';
//   }
// }
//
// ///보낸/받은 메시지 정보(이름, 프로필 이미지)
// class ChatUpdateMessageInfoData {
//   static const String Key_id = 'id';
//   static const String Key_first_name = 'first_name';
//   static const String Key_profileimg = 'profileimg';
//
//   ChatUpdateMessageInfoData({
//     this.id,
//     this.first_name,
//     this.profileimg,
//   });
//
//   @JsonKey(name: Key_id)
//   final int id;
//   @JsonKey(name: Key_first_name)
//   final String first_name;
//   @JsonKey(name: Key_profileimg)
//   final String profileimg;
//
//   factory ChatUpdateMessageInfoData.fromJson(Map<String, dynamic> json) {
//     return ChatUpdateMessageInfoData(
//       id: json[Key_id] as int,
//       first_name: json[Key_first_name] ?? '',
//       profileimg: json[Key_profileimg] ?? '',
//     );
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data[Key_first_name] = this.first_name;
//     data[Key_profileimg] = this.profileimg;
//     return data;
//   }
//
//   @override
//   String toString() {
//     return 'SenderInfoData{first_name: $first_name, profileimg: $profileimg}';
//   }
// }
