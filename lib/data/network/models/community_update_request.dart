import 'dart:io';

import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/manager/enum_manager.dart';

@JsonSerializable()
class CommunityUpdateRequest {
  static const String Key_id = 'id';
  static const String Key_title = 'title';
  static const String Key_category = 'category';
  static const String Key_share_status = 'share_status';
  static const String Key_content = 'content';
  static const String Key_image = 'image';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_title)
  String title;
  @JsonKey(name: Key_category)
  int category;
  @JsonKey(name: Key_share_status)
  int shareStatus;
  @JsonKey(name: Key_content)
  String content;
  @JsonKey(name: Key_image)
  List<File>? image;

  CommunityUpdateRequest({this.id = 0, this.title = '', this.category = 0, this.shareStatus = 0, this.content = '', this.image});

  factory CommunityUpdateRequest.fromJson(Map<String, dynamic> json) {
    return CommunityUpdateRequest(
      id: json[Key_id] ?? 0,
      title: json[Key_title] ?? '',
      category: json[Key_category] ?? 0,
      shareStatus: json[Key_share_status] ?? 0,
      content: json[Key_content] ?? '',
      image: json[Key_image],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    if (this.title.isNotEmpty) data[Key_title] = this.title;
    data[Key_category] = this.category;
    if (this.category == Category.give.index) data[Key_share_status] = this.shareStatus;
    if (this.content.isNotEmpty) data[Key_content] = this.content;
    if (this.image != null) data[Key_image] = this.image;
    return data;
  }

  FormData toFormData(FormData formData) {
    formData.fields.add(MapEntry(Key_id, this.id.toString()));
    formData.fields.add(MapEntry(Key_title, this.title.toString()));
    formData.fields.add(MapEntry(Key_category, this.category.toString()));
    formData.fields.add(MapEntry(Key_content, this.content.toString()));
    if (this.category == Category.give.index) formData.fields.add(MapEntry(Key_share_status, this.shareStatus.toString()));
    return formData;
  }

  @override
  String toString() {
    return 'CommunityUpdateRequest{ id: $id, title: $title, category: $category, share_status: $shareStatus, content: $content, image: $image}';
  }
}
