import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class AuthCenterAuthMainRequest {
  static const String Key_auth_id = "auth_id";

  AuthCenterAuthMainRequest({
    this.auth_id = 0,
  });

  @JsonKey(name: Key_auth_id)
  int auth_id;

  factory AuthCenterAuthMainRequest.fromJson(Map<String, dynamic> json) {
    return AuthCenterAuthMainRequest(auth_id: json[Key_auth_id] ?? 0);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_auth_id] = this.auth_id;
    return data;
  }

  @override
  String toString() {
    return 'AuthCenterAuthMainRequest{auth_id: $auth_id}';
  }
}
