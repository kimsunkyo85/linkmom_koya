import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class TempSaveRequest {
  static const String Key_child = 'child';
  static const String Key_servicetype = 'servicetype';
  static const String Key_possible_area = 'possible_area';
  static const String Key_depth = 'depth';
  static const String Key_reqdata = 'reqdata';

  TempSaveRequest({
    this.child = 0,
    this.servicetype = 0,
    this.possible_area = 1,
    this.depth = 99,
    this.reqdata = '',
  });

  @JsonKey(name: Key_child)
  int child;
  @JsonKey(name: Key_servicetype)
  int servicetype;
  @JsonKey(name: Key_possible_area)
  int possible_area;
  @JsonKey(name: Key_depth)
  int depth;
  @JsonKey(name: Key_reqdata)
  String reqdata;

  factory TempSaveRequest.fromJson(Map<String, dynamic> json) {
    return TempSaveRequest(
      child: json[Key_child] as int,
      servicetype: json[Key_servicetype] as int,
      possible_area: json[Key_possible_area] as int,
      depth: json[Key_depth] as int,
      reqdata: json[Key_reqdata],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_child] = this.child;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_depth] = this.depth;
    data[Key_reqdata] = this.reqdata;
    return data;
  }

  @override
  String toString() {
    return 'TempSaveRequest{child: $child, servicetype: $servicetype, possible_area: $possible_area, depth: $depth, reqdata: $reqdata}';
  }
}
