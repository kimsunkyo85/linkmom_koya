import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

@JsonSerializable()
class CommunityListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<CommunityListData> dataList;

  CommunityListResponse({this.dataHeader, required this.dataList});
  CommunityListData getData() => dataList.isNotEmpty ? dataList.first : CommunityListData(results: []);

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();
  factory CommunityListResponse.fromJson(Map<String, dynamic> json) {
    return CommunityListResponse(dataHeader: HeaderResponse.fromJson(json), dataList: json[Key_data] != null ? (json[Key_data] as List).map((e) => CommunityListData.fromJson(e)).toList() : []);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_data] = this.dataList.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CommunityListResponse{dataHeader:$dataHeader, dataList:$dataList}';
  }
}

class CommunityListData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';
  static const String Key_region = 'region';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<CommunityBoardData> results;
  @JsonKey(name: Key_region)
  List<Region>? region;

  CommunityListData({this.count = 0, this.next = '', this.previous = '', required this.results, this.region});

  factory CommunityListData.fromJson(Map<String, dynamic> json) {
    return CommunityListData(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] != null ? (json[Key_results] as List).map((e) => CommunityBoardData.fromJson(e)).toList() : [],
      region: json[Key_region] != null ? (json[Key_region] as List).map((e) => Region.fromJson(e)).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results.map((e) => e.toJson()).toList();
    if (this.region != null) data[Key_region] = this.region!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CommunityListData{count: $count, next: $next, previous: $previous, results: $results, region: $region}';
  }
}

class Region {
  static const String Key_region_2depth = 'region_2depth';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_3depth_h = 'region_3depth_h';
  static const String Key_hcode = 'hcode';

  @JsonKey(name: Key_region_2depth)
  String region2Depth;
  @JsonKey(name: Key_region_3depth)
  String region3Depth;
  /// 행정동 이름
  @JsonKey(name: Key_region_3depth_h)
  String region3DepthH;
  @JsonKey(name: Key_hcode)
  int hcode;

  Region({this.region2Depth = '', this.region3Depth = '', this.hcode = 0, this.region3DepthH = ''});

  factory Region.fromJson(Map<String, dynamic> json) {
    return Region(
      region2Depth: json[Key_region_2depth] ?? '',
      region3Depth: json[Key_region_3depth] ?? '',
      region3DepthH: json[Key_region_3depth_h] ?? '',
      hcode: int.parse(json[Key_hcode] ?? 0),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_region_2depth] = this.region2Depth;
    data[Key_region_3depth] = this.region3Depth;
    data[Key_region_3depth_h] = this.region3DepthH;
    data[Key_hcode] = this.hcode;
    return data;
  }

  @override
  String toString() {
    return 'Region{region2Depth: $region2Depth, region3Depth: $region3Depth, region4Depth: $region3DepthH, hcode: $hcode}';
  }
}

class CommunityBoardData {
  static const String Key_userinfo = 'userinfo';
  static const String Key_community_share = 'community_share';
  static const String Key_community_share_images = 'community_share_images';

  @JsonKey(name: Key_userinfo)
  CommunityUserInfo? userinfo;
  @JsonKey(name: Key_community_share)
  CommunityShare? communityShare;
  @JsonKey(name: Key_community_share_images)
  List<CommunityShareImages>? communityShareImages;

  CommunityBoardData({this.userinfo, this.communityShare, this.communityShareImages});

  factory CommunityBoardData.fromJson(Map<String, dynamic> json) {
    return CommunityBoardData(
      userinfo: json[Key_userinfo] == null ? CommunityUserInfo() : CommunityUserInfo.fromJson(json[Key_userinfo]),
      communityShare: json[Key_community_share] == null ? CommunityShare() : CommunityShare.fromJson(json[Key_community_share]),
      communityShareImages: json[Key_community_share_images] != null && json[Key_community_share_images].isNotEmpty ? json[Key_community_share_images].map<CommunityShareImages>((e) => CommunityShareImages.fromJson(e)).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userinfo != null) data[Key_userinfo] = this.userinfo!;
    if (this.communityShare != null) data[Key_community_share] = this.communityShare!;
    if (this.communityShareImages != null) data[Key_community_share_images] = this.communityShareImages;
    return data;
  }

  @override
  String toString() {
    return 'CommunityBoardData{userinfo: $userinfo, communityShare: $communityShare, communityShareImage: $communityShareImages}';
  }
}

class CommunityShareImages {
  static const String Key_images = 'images';
  static const String Key_image = 'image';
  static const String Key_image_thumbnail = 'image_thumbnail';
  static const String Key_images_thumbnail = 'images_thumbnail';

  /// NOTE: use community view/home index/index refresh response
  @JsonKey(name: Key_images)
  String images;

  /// NOTE: use community view/home index/index refresh response
  @JsonKey(name: Key_images_thumbnail)
  String imagesThumbnail;

  /// NOTE: use community list/save/update response
  @JsonKey(name: Key_image)
  String image;

  /// NOTE: use community list/save/update response
  @JsonKey(name: Key_image_thumbnail)
  String imageThumbnail;

  CommunityShareImages({this.image = '', this.images = '', this.imagesThumbnail = '', this.imageThumbnail = ''});

  factory CommunityShareImages.fromJson(Map<String, dynamic> json) {
    return CommunityShareImages(
      image: json[Key_image] ?? '',
      imageThumbnail: json[Key_image_thumbnail] ?? '',
      images: json[Key_images] ?? '',
      imagesThumbnail: json[Key_images_thumbnail] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_images] = this.images;
    data[Key_image] = this.image;
    data[Key_images_thumbnail] = this.imagesThumbnail;
    data[Key_image_thumbnail] = this.imageThumbnail;
    return data;
  }

  @override
  String toString() {
    return 'CommunityShareImage{images:$images, image:$image, imagesThumbnail:$imagesThumbnail, imageThumbnail: $imageThumbnail}';
  }
}

class CommunityShare {
  static const String Key_id = 'id';
  static const String Key_title = 'title';
  static const String Key_content = 'content';
  static const String Key_category = 'category';
  static const String Key_share_status = 'share_status';
  static const String Key_is_scrap = 'is_scrap';
  static const String Key_reply_cnt = 'reply_cnt';
  static const String Key_like_cnt = 'like_cnt';
  static const String Key_updatedate = 'updatedate';
  static const String Key_is_like = 'is_like';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_title)
  String title;
  @JsonKey(name: Key_content)
  String content;
  @JsonKey(name: Key_category)
  String category;
  @JsonKey(name: Key_share_status)
  String shareStatus;
  @JsonKey(name: Key_is_scrap)
  bool isScrap;
  @JsonKey(name: Key_reply_cnt)
  int replyCnt;
  @JsonKey(name: Key_like_cnt)
  int likeCnt;
  @JsonKey(name: Key_updatedate)
  String updatedate;
  @JsonKey(name: Key_is_like)
  bool isLike;

  CommunityShare({this.id = 0, this.title = '', this.content = '', this.category = '', this.shareStatus = '', this.isScrap = false, this.replyCnt = 0, this.likeCnt = 0, this.updatedate = '', this.isLike = false});

  factory CommunityShare.fromJson(Map<String, dynamic> json) {
    return CommunityShare(
      id: json[Key_id] as int,
      title: json[Key_title] ?? '',
      content: json[Key_content] ?? '',
      category: json[Key_category] ?? '',
      shareStatus: json[Key_share_status] ?? '',
      isScrap: json[Key_is_scrap] ?? false,
      replyCnt: json[Key_reply_cnt] ?? '',
      likeCnt: json[Key_like_cnt] ?? '',
      updatedate: json[Key_updatedate] ?? '',
      isLike: json[Key_is_like] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_title] = this.title;
    data[Key_content] = this.content;
    data[Key_category] = this.category;
    data[Key_share_status] = this.shareStatus;
    data[Key_is_scrap] = this.isScrap;
    data[Key_reply_cnt] = this.replyCnt;
    data[Key_like_cnt] = this.likeCnt;
    data[Key_updatedate] = this.updatedate;
    data[Key_is_like] = this.isLike;
    return data;
  }

  @override
  String toString() {
    return 'CommunityShare{id: $id, title: $title, content: $content, category: $category, shareStatus: $shareStatus, isScrap: $isScrap, replyCnt: $replyCnt, likeCnt: $likeCnt, updatedate: $updatedate, isLike: $isLike}';
  }
}

class CommunityUserInfo {
  static const String Key_user_id = 'user_id';
  static const String Key_nickname = 'nickname';
  static const String Key_region_2depth = 'region_2depth';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_profileimg = 'profileimg';

  @JsonKey(name: Key_user_id)
  int userId;
  @JsonKey(name: Key_nickname)
  String nickname;
  @JsonKey(name: Key_region_2depth)
  String region2Depth;
  @JsonKey(name: Key_region_3depth)
  String region3Depth;
  @JsonKey(name: Key_profileimg)
  String profileimg;

  CommunityUserInfo({this.userId = 0, this.nickname = '', this.region2Depth = '', this.region3Depth = '', this.profileimg = ''});

  factory CommunityUserInfo.fromJson(Map<String, dynamic> json) {
    return CommunityUserInfo(
      userId: json[Key_user_id] ?? 0,
      nickname: json[Key_nickname] ?? '',
      region2Depth: json[Key_region_2depth] ?? '',
      region3Depth: json[Key_region_3depth] ?? '',
      profileimg: json[Key_profileimg] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_user_id] = this.userId;
    data[Key_nickname] = this.nickname;
    data[Key_region_2depth] = this.region2Depth;
    data[Key_region_3depth] = this.region3Depth;
    data[Key_profileimg] = this.profileimg;
    return data;
  }

  @override
  String toString() {
    return 'Userinfo{userId: $userId, nickname: $nickname, region2Depth: $region2Depth, region3Depth: $region3Depth, profileimg: $profileimg}';
  }
}
