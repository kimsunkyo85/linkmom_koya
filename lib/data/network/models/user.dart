import 'package:json_annotation/json_annotation.dart';

import 'mypage_myinfo_response.dart';

@JsonSerializable()
class User {
  static const String Key_id = 'id';
  static const String Key_username = 'username';
  static const String Key_password = 'password';
  static const String Key_momdaddy = 'momdaddy';
  static const String Key_linkmom = 'linkmom';
  static const String Key_auto_login = 'auto_login';
  static const String Key_token_access = 'token_access';
  static const String Key_token_refresh = 'token_refresh';
  static const String Key_token_csrf = 'token_csrf';
  static const String Key_token_access_expire = 'access_expire';
  static const String Key_token_refresh_expire = 'refresh_expire';
  static const String Key_token_csrf_expire = 'csrf_expire';
  static const String Key_token_fcm = 'token_fcm';
  static const String Key_my_info_data = 'my_info_data';
  static const String Key_aes_key = 'aes_key';

  User({
    this.id = 0,
    this.username = '',
    this.password = '',
    this.momdaddy = -1,
    this.linkmom = -1,
    this.auto_login = false,
    this.token_access = '',
    this.token_refresh = '',
    this.token_csrf = '',
    this.token_access_expire = '',
    this.token_refresh_expire = '',
    this.token_csrf_expire = '',
    this.token_fcm = '',
    this.my_info_data,
    this.aesKey = '',
  });

  void setClear(String? csrfToken, String? csrfExpire) {
    this.id = 0;
    this.username = '';
    this.password = '';
    this.momdaddy = -1;
    this.linkmom = -1;
    this.auto_login = false;
    this.token_access = '';
    this.token_refresh = '';
    this.token_csrf = csrfToken ?? '';
    this.token_access_expire = '';
    this.token_refresh_expire = '';
    this.token_csrf_expire = csrfExpire ?? '';
    this.token_fcm = '';
    this.my_info_data = MyInfoData.init();
    this.aesKey = '';

    ///로그아웃시 갱신
  }

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_username)
  String username;
  @JsonKey(name: Key_password)
  String password;
  @JsonKey(name: Key_momdaddy)
  int momdaddy;
  @JsonKey(name: Key_linkmom)
  int linkmom;
  @JsonKey(name: Key_auto_login)
  bool auto_login;
  @JsonKey(name: Key_token_access)
  String token_access;
  @JsonKey(name: Key_token_refresh)
  String token_refresh;
  @JsonKey(name: Key_token_csrf)
  String token_csrf;
  @JsonKey(name: Key_token_access_expire)
  String token_access_expire;
  @JsonKey(name: Key_token_refresh_expire)
  String token_refresh_expire;
  @JsonKey(name: Key_token_csrf_expire)
  String token_csrf_expire;
  @JsonKey(name: Key_token_fcm)
  String token_fcm;
  @JsonKey(name: Key_my_info_data)
  MyInfoData? my_info_data;
  @JsonKey(name: Key_aes_key)
  String aesKey;

  factory User.fromJson(Map<String, dynamic> json) {
    MyInfoData myInfoData = json[Key_my_info_data] == null ? MyInfoData.init() : MyInfoData.fromJson(json[Key_my_info_data]);
    return User(
      id: json[Key_id] as int,
      username: json[Key_username] ?? '',
      password: json[Key_password] ?? '',
      momdaddy: json[Key_momdaddy] ?? -1,
      linkmom: json[Key_linkmom] ?? -1,
      auto_login: json[Key_auto_login] as bool,
      token_access: json[Key_token_access] ?? '',
      token_refresh: json[Key_token_refresh] ?? '',
      token_csrf: json[Key_token_csrf] ?? '',
      token_access_expire: json[Key_token_access_expire] ?? '',
      token_refresh_expire: json[Key_token_refresh_expire] ?? '',
      token_csrf_expire: json[Key_token_csrf_expire] ?? '',
      token_fcm: json[Key_token_fcm] ?? '',
      my_info_data: myInfoData,
      aesKey: json[Key_aes_key] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_username] = this.username;
    data[Key_password] = this.password;
    data[Key_momdaddy] = this.momdaddy;
    data[Key_linkmom] = this.linkmom;
    data[Key_auto_login] = this.auto_login;
    data[Key_token_access] = this.token_access;
    data[Key_token_refresh] = this.token_refresh;
    data[Key_token_csrf] = this.token_csrf;
    data[Key_token_access_expire] = this.token_access_expire;
    data[Key_token_refresh_expire] = this.token_refresh_expire;
    data[Key_token_csrf_expire] = this.token_csrf_expire;
    data[Key_token_fcm] = this.token_fcm;
    data[Key_my_info_data] = this.my_info_data;
    data[Key_aes_key] = this.aesKey;
    return data;
  }

  @override
  String toString() {
    return 'User{id: $id, username: $username, password: $password, momdaddy: $momdaddy, linkmom: $linkmom, auto_login: $auto_login, token_access: $token_access, token_refresh: $token_refresh, token_csrf: $token_csrf, token_access_expire: $token_access_expire, token_refresh_expire: $token_refresh_expire, token_csrf_expire: $token_csrf_expire, token_fcm: $token_fcm, my_info_data: $my_info_data, aesKey: $aesKey}';
  }
}
