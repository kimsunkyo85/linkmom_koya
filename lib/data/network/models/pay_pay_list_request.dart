import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///
/// Created by   : platformbiz@ggumbi.com
///
/// version      : 1.0.0
///
/// see          : pay_pay_list_request.dart - 결제리스트 POST
///
/// since        : 2021/09/08 / update:
///
/// (0, '결제'),
///
/// (11, '결제취소')
///
/// (실패) "msg_cd": 4000,"message": "데이터가 없습니다.",

@JsonSerializable()
class PayPayListRequest {
  static const String Key_s_date = 's_date';
  static const String Key_e_date = 'e_date';
  static const String Key_payment_flag = 'payment_flag';

  PayPayListRequest({
    this.s_date,
    this.e_date,
    this.payment_flag,
  });

  ///결제조회 시작일자 (YYYYMMDD)
  @JsonKey(name: Key_s_date)
  final String? s_date;

  ///결제조회 종료일자 (YYYYMMDD)
  @JsonKey(name: Key_e_date)
  final String? e_date;

  ///결제조회 구분 (전체선택 : Null)
  @JsonKey(name: Key_payment_flag)
  final List<int>? payment_flag;

  factory PayPayListRequest.fromJson(Map<String, dynamic> json) {
    List<int> paymentFlag = json[Key_payment_flag] == null ? [] : List<int>.from(json[Key_payment_flag]);
    return PayPayListRequest(
      s_date: json[Key_s_date] ?? '',
      e_date: json[Key_e_date] ?? '',
      payment_flag: paymentFlag,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_s_date] = this.s_date;
    data[Key_e_date] = this.e_date;
    data[Key_payment_flag] = this.payment_flag;
    return data;
  }

  Future<FormData> toFormData() async {
    FormData formData = FormData();
    if (s_date != null) {
      formData.fields.add(MapEntry(Key_s_date, s_date.toString()));
    }
    if (e_date != null) {
      formData.fields.add(MapEntry(Key_e_date, e_date.toString()));
    }
    if (payment_flag != null) {
      payment_flag!.forEach((value) {
        formData.fields.add(MapEntry(Key_payment_flag, value.toString()));
      });
    }
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.files},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'PayPayListRequest{s_date: $s_date, e_date: $e_date, payment_flag: $payment_flag}';
  }
}
