import 'package:json_annotation/json_annotation.dart';

import 'header_response.dart';

@JsonSerializable()
class CommunityLikeResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<CommunityLikeData>? dataList;

  CommunityLikeResponse({this.dataHeader, this.dataList});
  CommunityLikeData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CommunityLikeData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();
  factory CommunityLikeResponse.fromJson(Map<String, dynamic> json) {
    return CommunityLikeResponse(dataHeader: HeaderResponse.fromJson(json), dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => CommunityLikeData.fromJson(e)).toList());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CommunityLikeResponse{dataHeader:$dataHeader, dataList:$dataList}';
  }
}

class CommunityLikeData {
  static const String Key_share_id = 'share_id';
  @JsonKey(name: Key_share_id)
  String sharedId;

  CommunityLikeData({this.sharedId = ''});

  factory CommunityLikeData.fromJson(Map<String, dynamic> json) {
    return CommunityLikeData(sharedId: json[Key_share_id]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_share_id] = this.sharedId;
    return data;
  }

  @override
  String toString() {
    return 'CommunityLikeData{sharedId:$sharedId}';
  }
}
