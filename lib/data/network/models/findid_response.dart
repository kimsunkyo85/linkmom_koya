import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

@JsonSerializable()
class FindIdResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  FindIdResponse({
    this.dataHeader,
    this.dataList,
  });

  FindIdData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : FindIdData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory FindIdResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<FindIdData> datas = [];
    try {
      datas = json[Key_data].map<FindIdData>((i) => FindIdData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return FindIdResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'FindIdResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class FindIdData {
  static const String Key_username = "username";

  FindIdData({
    this.username = ''
  });

  @JsonKey(name: Key_username)
  final String username;

  factory FindIdData.fromJson(Map<String, dynamic> json) {
    return FindIdData(
      username: json[Key_username] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_username] = this.username;
    return data;
  }

  @override
  String toString() {
    return 'LoginData{username: $username}';
  }
}
