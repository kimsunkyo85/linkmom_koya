import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : area_code_search_request.dart - 주소 위치검색 (행정구역) - 돌봄신청시
/// since        : 4/22/21 / update:
@JsonSerializable()
class AreaCodeSearchRequest {
  ///검색어
  static const String Key_area = 'area';

  AreaCodeSearchRequest({
    this.area = '',
  });

  ///검색어
  @JsonKey(name: Key_area)
  String area;

  factory AreaCodeSearchRequest.fromJson(Map<String, dynamic> json) {
    return AreaCodeSearchRequest(
      area: json[Key_area] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_area] = this.area;
    return data;
  }

  @override
  String toString() {
    return 'CareRequest{area: $area}';
  }
}
