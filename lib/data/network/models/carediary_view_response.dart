import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

import 'carediary_form_response.dart';

@JsonSerializable()
class CareDiaryViewResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<CareDiayViewData>? dataList;

  CareDiaryViewResponse({
    this.dataHeader,
    this.dataList,
  });

  CareDiaryViewResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  CareDiayViewData getData() => dataList!.isNotEmpty ? dataList!.first : CareDiayViewData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareDiaryViewResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareDiayViewData> datas = [];
    try {
      datas = json[Key_data].map<CareDiayViewData>((i) => CareDiayViewData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareDiaryViewResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CareDiaryViewResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class CareDiayViewData {
  static const Key_carecontract_schedule = 'carecontract_schedule';
  static const Key_cost_sum = 'cost_sum';
  static const Key_pay_status = 'pay_status';
  static const Key_pay_status_text = 'pay_status_text';
  static const Key_servicetype = 'servicetype';
  static const Key_possible_area = 'possible_area';
  static const Key_stime = 'stime';
  static const Key_etime = 'etime';
  static const Key_caredate = 'caredate';
  static const Key_userinfo = 'userinfo';
  static const Key_childinfo = 'childinfo';
  static const Key_carediary_info = 'carediary_info';
  static const Key_carediary_images = 'carediary_images';
  static const Key_like_yn = 'like_yn';
  static const Key_carediary_reply_cnt = 'carediary_reply_cnt';
  static const Key_carediary_reply = 'carediary_reply';
  static const Key_cancel_info = 'cancel_info';
  static const Key_claim_info = 'claim_info';
  static const Key_is_end_care = 'is_end_care';
  static const Key_matching = 'matching';
  static const Key_is_review = 'is_review';

  @JsonKey(name: Key_matching)
  int matching;
  @JsonKey(name: Key_carecontract_schedule)
  int carecontractSchedule;
  @JsonKey(name: Key_cost_sum)
  int costSum;
  @JsonKey(name: Key_pay_status)
  int payStatus;
  @JsonKey(name: Key_pay_status_text)
  String payStatusText;
  @JsonKey(name: Key_servicetype)
  String servicetype;
  @JsonKey(name: Key_possible_area)
  String possibleArea;
  @JsonKey(name: Key_stime)
  String stime;
  @JsonKey(name: Key_etime)
  String etime;
  @JsonKey(name: Key_caredate)
  List<String>? caredate;
  @JsonKey(name: Key_userinfo)
  Userinfo? userinfo;
  @JsonKey(name: Key_childinfo)
  Childinfo? childinfo;
  @JsonKey(name: Key_carediary_info)
  CarediaryInfo? carediaryInfo;
  @JsonKey(name: Key_carediary_images)
  List<CarediaryImage>? carediaryImages;
  @JsonKey(name: Key_like_yn)
  bool likeYn;
  @JsonKey(name: Key_carediary_reply_cnt)
  int carediaryReplyCnt;
  @JsonKey(name: Key_carediary_reply)
  List<CarediaryReply>? carediaryReply;
  @JsonKey(name: Key_cancel_info)
  CancelInfo? cancelinfo;
  @JsonKey(name: Key_claim_info)
  ClaimInfo? claimInfo;
  @JsonKey(name: Key_is_end_care)
  bool isEndCare;
  @JsonKey(name: Key_is_review)
  bool isReview;

  CareDiayViewData({
    this.carecontractSchedule = 0,
    this.costSum = 0,
    this.payStatus = 0,
    this.payStatusText = '',
    this.servicetype = '',
    this.possibleArea = '',
    this.stime = '',
    this.etime = '',
    this.caredate,
    this.userinfo,
    this.childinfo,
    this.carediaryInfo,
    this.carediaryImages,
    this.likeYn = false,
    this.carediaryReplyCnt = 0,
    this.carediaryReply,
    this.cancelinfo,
    this.claimInfo,
    this.isEndCare = false,
    this.matching = 0,
    this.isReview = false,
  });

  factory CareDiayViewData.fromJson(Map<String, dynamic> json) {
    return CareDiayViewData(
      matching: json[Key_matching] ?? 0,
      carecontractSchedule: json[Key_carecontract_schedule] as int,
      costSum: json[Key_cost_sum] ?? 0,
      payStatus: json[Key_pay_status] ?? 0,
      payStatusText: json[Key_pay_status_text] ?? '',
      servicetype: json[Key_servicetype] ?? '',
      possibleArea: json[Key_possible_area] ?? '',
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      caredate: json[Key_caredate] != null ? List<String>.from(json[Key_caredate]) : [],
      userinfo: json[Key_userinfo] != null ? Userinfo.fromJson(json[Key_userinfo]) : Userinfo(),
      childinfo: json[Key_childinfo] != null ? Childinfo.fromJson(json[Key_childinfo]) : Childinfo(),
      carediaryInfo: json[Key_carediary_info] != null ? CarediaryInfo.fromJson(json[Key_carediary_info]) : CarediaryInfo(),
      carediaryImages: json[Key_carediary_images] == null ? [] : (json[Key_carediary_images] as List).map((e) => CarediaryImage.fromJson(e)).toList(),
      likeYn: json[Key_like_yn] ?? false,
      carediaryReplyCnt: json[Key_carediary_reply_cnt] ?? 0,
      carediaryReply: json[Key_carediary_reply] == null ? [] : (json[Key_carediary_reply] as List).map((e) => CarediaryReply.fromJson(e)).toList(),
      cancelinfo: json[Key_cancel_info] != null ? CancelInfo.fromJson(json[Key_cancel_info]) : CancelInfo(),
      claimInfo: json[Key_claim_info] != null ? ClaimInfo.fromJson(json[Key_claim_info]) : ClaimInfo(),
      isEndCare: json[Key_is_end_care] ?? false,
      isReview: json[Key_is_review] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_matching] = this.matching;
    data[Key_carecontract_schedule] = this.carecontractSchedule;
    data[Key_cost_sum] = this.costSum;
    data[Key_pay_status] = this.payStatus;
    data[Key_pay_status_text] = this.payStatusText;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possibleArea;
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_caredate] = this.caredate;
    data[Key_userinfo] = this.userinfo;
    data[Key_childinfo] = this.childinfo;
    data[Key_carediary_info] = this.carediaryInfo;
    data[Key_carediary_images] = this.carediaryImages;
    data[Key_like_yn] = this.likeYn;
    data[Key_carediary_reply_cnt] = this.carediaryReplyCnt;
    data[Key_carediary_reply] = this.carediaryReply;
    data[Key_cancel_info] = this.cancelinfo;
    data[Key_claim_info] = this.claimInfo;
    data[Key_is_end_care] = this.isEndCare;
    data[Key_is_review] = this.isReview;
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryviewData{matching: $matching, carecontractSchedule: $carecontractSchedule, costSum: $costSum, payStatus: $payStatus, servicetype: $servicetype, possibleArea: $possibleArea, stime: $stime, etime: $etime, caredate: $caredate, userinfo: $userinfo, childinfo: $childinfo, carediaryInfo: $carediaryInfo, carediaryImages: $carediaryImages, likeYn: $likeYn, carediaryReplyCnt: $carediaryReplyCnt, carediaryReply: $carediaryReply, claimInfo: $claimInfo, cancelinfo: $cancelinfo, isEndCare: $isEndCare, isReview: $isReview}';
  }
}

class CarediaryReply {
  static const String Key_id = 'id';
  static const String Key_parent = 'parent';
  static const String Key_reply = 'reply';
  static const String Key_writer_gubun = 'writer_gubun';
  static const String Key_writer_name = 'writer_name';
  static const String Key_writer_id = 'writer_id';
  static const String Key_updatedate = 'updatedate';
  static const String Key_child = 'child';
  static const String Key_is_delete = 'is_delete';
  static const String Key_profileimg = 'profileimg';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_parent)
  int parent;
  @JsonKey(name: Key_reply)
  String reply;
  @JsonKey(name: Key_writer_gubun)
  String writerGubun;
  @JsonKey(name: Key_writer_name)
  String writerName;
  @JsonKey(name: Key_updatedate)
  String updatedate;
  @JsonKey(name: Key_child)
  List<CarediaryReply>? child;
  @JsonKey(name: Key_is_delete)
  int is_delete;
  @JsonKey(name: Key_profileimg)
  String profileimg;
  @JsonKey(name: Key_writer_id)
  int writerId;

  CarediaryReply({this.id = 0, this.parent = 0, this.reply = '', this.writerGubun = '', this.writerName = '', this.updatedate = '', this.child, this.is_delete = 0, this.profileimg = '', this.writerId = 0});

  factory CarediaryReply.fromJson(Map<String, dynamic> json) {
    return CarediaryReply(
        id: json[Key_id] as int,
        parent: json[Key_parent] as int,
        reply: json[Key_reply],
        writerGubun: json[Key_writer_gubun] ?? '',
        writerName: json[Key_writer_name] ?? '',
        updatedate: json[Key_updatedate] ?? '',
        child: json[Key_child] != null && json[Key_child].isNotEmpty ? json[Key_child].map<CarediaryReply>((e) => CarediaryReply.fromJson(e)).toList() : [],
        is_delete: json[Key_is_delete] as int,
        profileimg: json[Key_profileimg] ?? '',
        writerId: json[Key_writer_id] ?? '');
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_parent] = this.parent;
    data[Key_reply] = this.reply;
    data[Key_writer_gubun] = this.writerGubun;
    data[Key_writer_name] = this.writerName;
    data[Key_updatedate] = this.updatedate;
    data[Key_child] = this.child;
    data[Key_is_delete] = this.is_delete;
    data[Key_profileimg] = this.profileimg;
    data[Key_writer_id] = this.writerId;
    return data;
  }
}

class CarediaryImage {
  static const String Key_image = 'image';

  @JsonKey(name: Key_image)
  String image;

  CarediaryImage({this.image = ''});

  factory CarediaryImage.fromJson(Map<String, dynamic> json) {
    return CarediaryImage(image: json[Key_image] ?? '');
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_image] = this.image;
    return data;
  }
}

class CancelInfo {
  static const String Key_order_id = 'order_id';
  static const String Key_contract_id = 'contract_id';
  static const String Key_cancel_id = 'cancel_id';

  @JsonKey(name: Key_order_id)
  String orderId;
  @JsonKey(name: Key_contract_id)
  int contractId;
  @JsonKey(name: Key_cancel_id)
  int cancelId;

  CancelInfo({this.orderId = '', this.contractId = 0, this.cancelId = 0});

  factory CancelInfo.fromJson(Map<String, dynamic> json) {
    return CancelInfo(
      orderId: json[Key_order_id] ?? '',
      contractId: json[Key_contract_id] ?? 0,
      cancelId: json[Key_cancel_id] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_order_id] = this.orderId;
    data[Key_contract_id] = this.contractId;
    data[Key_cancel_id] = this.cancelId;
    return data;
  }

  @override
  String toString() {
    return 'CancelInfo{orderId: $orderId, contractId: $contractId, cancelId: $cancelId}';
  }
}

@JsonSerializable()
class ClaimInfo {
  static const String Key_claim_id = 'claim_id';
  static const String Key_claim_after_day = 'claim_after_day';
  static const String Key_claim_answer = 'claim_answer';

  @JsonKey(name: Key_claim_id)
  int claimId;
  @JsonKey(name: Key_claim_after_day)
  String claimAfterDay;
  @JsonKey(name: Key_claim_answer)
  bool claimAnswer;

  ClaimInfo({this.claimId = 0, this.claimAfterDay = '', this.claimAnswer = false});

  factory ClaimInfo.fromJson(Map<String, dynamic> json) {
    return ClaimInfo(
      claimId: json[Key_claim_id] ?? 0,
      claimAfterDay: json[Key_claim_after_day] ?? '',
      claimAnswer: json[Key_claim_answer] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_claim_id] = this.claimId;
    data[Key_claim_after_day] = this.claimAfterDay;
    data[Key_claim_answer] = this.claimAnswer;
    return data;
  }

  @override
  String toString() {
    return 'ClaimInfo{claimId: $claimId, claimAfterDay: $claimAfterDay, claimAnswer: $claimAnswer}';
  }
}
