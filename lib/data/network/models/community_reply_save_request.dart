import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CommunityReplySaveRequest {
  static const String Key_share_id = 'share_id';
  static const String Key_reply = 'reply';
  static const String Key_parent = 'parent';

  @JsonKey(name: Key_share_id)
  int shareId;
  @JsonKey(name: Key_reply)
  String reply;
  @JsonKey(name: Key_parent)
  int parent;

  CommunityReplySaveRequest({this.shareId = 0, this.reply = '', this.parent = 0});

  factory CommunityReplySaveRequest.fromJson(Map<String, dynamic> json) {
    return CommunityReplySaveRequest(
      shareId: json[Key_share_id] ?? 0,
      reply: json[Key_reply] ?? '',
      parent: json[Key_parent] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_share_id] = this.shareId;
    data[Key_reply] = this.reply;
    if (this.parent > 0) data[Key_parent] = this.parent;
    return data;
  }

  @override
  String toString() {
    return 'CommunityReplySaveRequest{shareId: $shareId,reply: $reply,parent: $parent}';
  }
}
