import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class RefreshRequest {
  static const String Key_refreshtoken = "refreshtoken";
  static const String Key_csrftoken = "csrftoken";

  RefreshRequest({
    this.csrftoken = '',
    this.refreshtoken = '',
  });

  @JsonKey(name: Key_csrftoken)
  String csrftoken;
  @JsonKey(name: Key_refreshtoken)
  String refreshtoken;

  factory RefreshRequest.fromJson(Map<String, dynamic> json) {
    return RefreshRequest(
      csrftoken: json[Key_csrftoken] ?? '',
      refreshtoken: json[Key_refreshtoken] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_csrftoken] = this.csrftoken;
    data[Key_refreshtoken] = this.refreshtoken;
    return data;
  }

  @override
  String toString() {
    return 'RefreshRequest{csrftoken: $csrftoken, refreshtoken: $refreshtoken}';
  }
}
