import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class MyAccountResetEmailRequest {
  static const String Key_email = 'email';

  MyAccountResetEmailRequest({
    this.email = '',
  });

  @JsonKey(name: Key_email)
  String email;

  factory MyAccountResetEmailRequest.fromJson(Map<String, dynamic> json) {
    return MyAccountResetEmailRequest(
      email: json[Key_email] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_email] = this.email;
    return data;
  }

  @override
  String toString() {
    return 'MyAccountResetEmailRequest{$Key_email: $email}';
  }
}

@JsonSerializable()
class MyAccountResetPwRequest {
  static const String Key_old_pwd = 'old_pwd';
  static const String Key_pwd1 = 'pwd1';
  static const String Key_pwd2 = 'pwd2';

  MyAccountResetPwRequest({
    this.pwd1 = '',
    this.pwd2 = '',
    this.old_pwd = '',
  });

  @JsonKey(name: Key_pwd2)
  String pwd1;

  @JsonKey(name: Key_pwd2)
  String pwd2;

  @JsonKey(name: Key_old_pwd)
  String old_pwd;

  factory MyAccountResetPwRequest.fromJson(Map<String, dynamic> json) {
    return MyAccountResetPwRequest(
      pwd1: json[Key_pwd1] ?? '',
      pwd2: json[Key_pwd2] ?? '',
      old_pwd: json[Key_old_pwd] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_pwd1] = this.pwd1;
    data[Key_pwd2] = this.pwd2;
    data[Key_old_pwd] = this.old_pwd;
    return data;
  }

  @override
  String toString() {
    return 'MyAccountResetPwRequest{$Key_pwd1: $pwd1, $Key_pwd2: $pwd2, $Key_old_pwd: $old_pwd}';
  }
}

@JsonSerializable()
class MyAccountResetHpRequest {
  static const String Key_hpnum = 'hpnum';
  static const String Key_nice_di = 'nice_di';
  static const String Key_telco = 'telco';
  static const String Key_nationalinfo = 'nationalinfo';
  MyAccountResetHpRequest({
    this.hpnum = '',
    this.nice_di = '',
    this.telco = '',
    this.nationalinfo = '',
  });

  @JsonKey(name: Key_hpnum)
  String hpnum;

  @JsonKey(name: Key_nice_di)
  String nice_di;

  @JsonKey(name: Key_telco)
  String telco;

  @JsonKey(name: Key_nationalinfo)
  String nationalinfo;

  factory MyAccountResetHpRequest.fromJson(Map<String, dynamic> json) {
    return MyAccountResetHpRequest(
      hpnum: json[Key_hpnum] ?? '',
      nice_di: json[Key_nice_di] ?? '',
      telco: json[Key_telco] ?? '',
      nationalinfo: json[Key_nationalinfo] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_hpnum] = this.hpnum;
    data[Key_nice_di] = this.nice_di;
    data[Key_telco] = this.telco;
    data[Key_nationalinfo] = this.nationalinfo;
    return data;
  }

  @override
  String toString() {
    return 'MyAccountResetHpRequest{$Key_hpnum: $hpnum, $Key_nice_di:$nice_di, $Key_telco: $telco, $Key_nationalinfo: $nationalinfo}';
  }
}
