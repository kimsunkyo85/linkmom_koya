import 'package:linkmom/main.dart';

import 'header_response.dart';
import 'package:json_annotation/json_annotation.dart';

class PaymentsCancelAgreeResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<CancelReasonData>? dataList;

  PaymentsCancelAgreeResponse({this.dataHeader, this.dataList});

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  CancelReasonData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CancelReasonData();

  factory PaymentsCancelAgreeResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CancelReasonData> datas = [];
    try {
      datas = json[Key_data] != null ? json[Key_data].map((c) => CancelReasonData.fromJson(c)).toList() : [];
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return PaymentsCancelAgreeResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'PaymentsCancelReasonResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class CancelReasonData {
  static const String Key_cancel_id = 'cancel_id';
  static const String Key_is_cancelreason_agree = 'is_cancelreason_agree';

  @JsonKey(name: Key_cancel_id)
  int cancelId;

  @JsonKey(name: Key_is_cancelreason_agree)
  bool isCancelreasonAgree;

  CancelReasonData({this.cancelId = 0, this.isCancelreasonAgree = false});

  factory CancelReasonData.fromJson(Map<String, dynamic> json) {
    return CancelReasonData(
      cancelId: json[Key_cancel_id] ?? 0,
      isCancelreasonAgree: json[Key_is_cancelreason_agree] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_cancel_id] = this.cancelId;
    data[Key_is_cancelreason_agree] = this.isCancelreasonAgree;
    return data;
  }

  @override
  String toString() {
    return 'CancelReasonData{cancelId: $cancelId, isCancelreasonAgree: $isCancelreasonAgree}';
  }
}
