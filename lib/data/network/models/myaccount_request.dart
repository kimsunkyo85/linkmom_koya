
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class MyAccountRequest {

  MyAccountRequest();

  factory MyAccountRequest.fromJson(Map<String, dynamic> json) {
    return MyAccountRequest();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    return data;
  }

  @override
  String toString() {
    return 'MyAccountRequest{isEmptyBody}';
  }
}
