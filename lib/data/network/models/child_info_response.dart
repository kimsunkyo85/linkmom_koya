import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : child_info_response.dart - 아이정보 응답
/// since        : 5/20/21 / update:
@JsonSerializable()
class ChildInfoResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ChildInfoData>? dataList;

  ChildInfoResponse({
    this.dataHeader,
    this.dataList,
  });

  ChildInfoData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ChildInfoData();

  List<ChildInfoData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ChildInfoResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ChildInfoData> datas = [];
    try {
      datas = json[Key_data].map<ChildInfoData>((i) => ChildInfoData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return ChildInfoResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AuthCenterHomeResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class ChildInfoData {
  static const String Key_id = 'id';
  static const String Key_child_name = 'child_name';
  static const String Key_child_gender = 'child_gender';
  static const String Key_child_birth = 'child_birth';
  static const String Key_child_age = 'child_age';
  static const String Key_child_nursery = 'child_nursery';
  static const String Key_child_character = 'child_character';
  static const String Key_is_allergy = 'is_allergy';
  static const String Key_allergy_name = 'allergy_name';
  static const String Key_allergy_message = 'allergy_message';

  ChildInfoData({
    this.id = 0,
    this.child_name = '',
    this.child_gender = 0,
    this.child_birth = '',
    this.child_age = 0,
    this.child_nursery = 0,
    this.child_character,
    this.is_allergy = false,
    this.allergy_name = '',
    this.allergy_message = '',
  });

  ChildInfoData.clone(ChildInfoData childInfoData)
      : this(
          id: childInfoData.id,
          child_name: childInfoData.child_name,
          child_gender: childInfoData.child_gender,
          child_birth: childInfoData.child_birth,
          child_age: childInfoData.child_age,
          child_nursery: childInfoData.child_nursery,
          child_character: childInfoData.child_character,
          is_allergy: childInfoData.is_allergy,
          allergy_name: childInfoData.allergy_name,
          allergy_message: childInfoData.allergy_message,
        );

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_child_name)
  String child_name;
  @JsonKey(name: Key_child_gender)
  int child_gender;
  @JsonKey(name: Key_child_birth)
  String child_birth;
  @JsonKey(name: Key_child_age)
  int child_age;
  @JsonKey(name: Key_child_nursery)
  int child_nursery;
  @JsonKey(name: Key_child_character)
  List<int>? child_character;
  @JsonKey(name: Key_is_allergy)
  bool is_allergy;
  @JsonKey(name: Key_allergy_name)
  String allergy_name;
  @JsonKey(name: Key_allergy_message)
  String allergy_message;

  factory ChildInfoData.fromJson(Map<String, dynamic> json) {
    List<int> childCharacters = List<int>.from(json[Key_child_character]);
    return ChildInfoData(
      id: json[Key_id] as int,
      child_name: json[Key_child_name] ?? '',
      child_gender: json[Key_child_gender] as int,
      child_birth: json[Key_child_birth] ?? '',
      child_age: json[Key_child_age] as int,
      child_nursery: json[Key_child_nursery] as int,
      child_character: childCharacters,
      is_allergy: json[Key_is_allergy] ?? false,
      allergy_name: json[Key_allergy_name] ?? '',
      allergy_message: json[Key_allergy_message] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_child_name] = this.child_name;
    data[Key_child_gender] = this.child_gender;
    data[Key_child_birth] = this.child_birth;
    data[Key_child_age] = this.child_age;
    data[Key_child_nursery] = this.child_nursery;
    data[Key_child_character] = this.child_character;
    data[Key_is_allergy] = this.is_allergy;
    data[Key_allergy_name] = this.allergy_name;
    data[Key_allergy_message] = this.allergy_message;
    return data;
  }

  ///int to String
  List<String> getCharaters() {
    List<String> charaters = [];
    if (child_character!.isNotEmpty) {
      child_character!.forEach((e) {
        charaters.add(EnumUtils.getChildCharacter(e).string);
      });
    }
    return charaters;
  }

  ///int to String
  static List<String> getCharatersInt(List<int> values) {
    List<String> charaters = [];
    if (values.isNotEmpty) {
      values.forEach((e) {
        charaters.add(EnumUtils.getChildCharacter(e).string);
      });
    }
    return charaters;
  }

  ///String to int
  static List<int> getCharatersString(List<String> values) {
    List<int> charaters = [];
    if (values.isNotEmpty) {
      values.forEach((e) {
        charaters.add(EnumUtils.getChildCharacterString(e).index);
      });
    }
    return charaters;
  }

  @override
  String toString() {
    return 'ChildInfoData{id: $id, child_name: $child_name, child_gender: $child_gender, child_birth: $child_birth, child_age: $child_age, child_nursery: $child_nursery, child_character: $child_character, is_allergy: $is_allergy, allergy_name: $allergy_name, allergy_message: $allergy_message}';
  }
}
