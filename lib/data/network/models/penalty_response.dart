import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class PenaltyResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PenaltyData>? dataList;

  PenaltyResponse({this.dataHeader, this.dataList});

  PenaltyResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  PenaltyData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PenaltyData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory PenaltyResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PenaltyData>? datas = [];
    try {
      datas = json[Key_data] == null ? null : (json[Key_data] as List).map((e) => PenaltyData.fromJson(e)).toList();
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>>>>> $e");
    }
    return PenaltyResponse(dataHeader: dataHeader, dataList: datas);
  }
}

class PenaltyData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';
  static const String Key_penalty_count = 'penalty_count';
  static const String Key_like_count = 'like_count';
  static const String Key_momdady_review_count = 'momdady_review_count';
  static const String Key_linkmom_review_count = 'linkmom_review_count';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<Results>? results;
  @JsonKey(name: Key_penalty_count)
  int penaltyCount;
  @JsonKey(name: Key_like_count)
  int likeCount;
  @JsonKey(name: Key_momdady_review_count)
  int momdadyReviewCount;
  @JsonKey(name: Key_linkmom_review_count)
  int linkmomReviewCount;

  PenaltyData({this.count = 0, this.next = '', this.previous = '', this.results, this.penaltyCount = 0, this.likeCount = 0, this.momdadyReviewCount = 0, this.linkmomReviewCount = 0});

  factory PenaltyData.fromJson(Map<String, dynamic> json) {
    return PenaltyData(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] = json[Key_results]!.map<Results>((e) => Results.fromJson(e)).toList(),
      penaltyCount: json[Key_penalty_count] ?? 0,
      likeCount: json[Key_like_count] ?? 0,
      momdadyReviewCount: json[Key_momdady_review_count] ?? 0,
      linkmomReviewCount: json[Key_linkmom_review_count] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    if (this.results != null) data[Key_results] = this.results!.map((e) => e.toJson()).toList();
    data[Key_penalty_count] = this.penaltyCount;
    data[Key_like_count] = this.likeCount;
    data[Key_momdady_review_count] = this.momdadyReviewCount;
    data[Key_linkmom_review_count] = this.linkmomReviewCount;
    return data;
  }

  @override
  String toString() {
    return "PanaltyData {count: $count, next: $next, previous: $previous, results: $results, penaltyCount: $penaltyCount, likeCount: $likeCount, momdadyReviewCount: $momdadyReviewCount, linkmomReviewCoun: $linkmomReviewCount}";
  }
}

class Results {
  static const String Key_count = 'count';
  static const String Key_penalty_text = 'penalty_text';
  static const String Key_createdate = 'createdate';
  static const String Key_answer = 'answer';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_penalty_text)
  String penaltyText;
  @JsonKey(name: Key_createdate)
  String createdate;
  @JsonKey(name: Key_answer)
  String answer;

  Results({this.count = 0, this.penaltyText = '', this.createdate = '', this.answer = ''});

  factory Results.fromJson(Map<String, dynamic> json) {
    return Results(
      count: json[Key_count] as int,
      penaltyText: json[Key_penalty_text] ?? '',
      createdate: json[Key_createdate] ?? '',
      answer: json[Key_answer] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_penalty_text] = this.penaltyText;
    data[Key_createdate] = this.createdate;
    data[Key_answer] = this.answer;
    return data;
  }

  @override
  String toString() {
    return 'Results{count: $count, penalty_text: $penaltyText, createdate: $createdate, answer: $answer}';
  }
}
