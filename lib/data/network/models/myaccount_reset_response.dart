import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

@JsonSerializable()
class MyAccountResetResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<AccountResetMessage>? dataList;

  MyAccountResetResponse({
    this.dataHeader,
    this.dataList,
  });

  AccountResetMessage getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : AccountResetMessage();

  List<AccountResetMessage> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory MyAccountResetResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<AccountResetMessage> datas = [];
    try {
      datas = json[Key_data].map<AccountResetMessage>((i) => AccountResetMessage.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return MyAccountResetResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'MyAccountResetResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class AccountResetMessage {
  static const String Key_message = 'message';

  AccountResetMessage({
    this.message = '',
  });

  AccountResetMessage.init() {
    this.message = '';
  }

  @JsonKey(name: Key_message)
  late String message;

  factory AccountResetMessage.fromJson(Map<String, dynamic> json) {
    return AccountResetMessage(
      message: json[Key_message],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> message = Map<String, dynamic>();
    message[Key_data] = this.message;
    return message;
  }

  @override
  String toString() {
    return 'AccountResetMessage{$Key_message: $message}';
  }
}

class MyAccountResetPwResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ResetPwData>? dataList;

  MyAccountResetPwResponse({
    this.dataHeader,
    this.dataList,
  });

  ResetPwData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ResetPwData();

  List<ResetPwData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory MyAccountResetPwResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ResetPwData> datas = [];
    try {
      datas = json[Key_data].map<ResetPwData>((i) => ResetPwData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return MyAccountResetPwResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'MyAccountResetPwResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class ResetPwData {
  static const String Key_old_pwd = 'old_pwd1';
  static const String Key_pwd1 = 'pwd1';
  static const String Key_pwd2 = 'pwd2';
  static const String Key_message = 'message';

  ResetPwData({
    this.pwd1 = '',
    this.pwd2 = '',
    this.old_pwd = '',
    this.message = '',
  });

  @JsonKey(name: Key_pwd2)
  String pwd1;

  @JsonKey(name: Key_pwd2)
  String pwd2;

  @JsonKey(name: Key_old_pwd)
  String old_pwd;

  @JsonKey(name: Key_message)
  String message;

  factory ResetPwData.fromJson(Map<String, dynamic> json) {
    return ResetPwData(
      pwd1: json[Key_pwd1] ?? '',
      pwd2: json[Key_pwd2] ?? '',
      old_pwd: json[Key_old_pwd] ?? '',
      message: json[Key_message] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_pwd1] = this.pwd1;
    data[Key_pwd2] = this.pwd2;
    data[Key_old_pwd] = this.old_pwd;
    data[Key_message] = this.message;
    return data;
  }

  @override
  String toString() {
    return 'ResetPwData{$Key_pwd1: $pwd1, $Key_pwd2: $pwd2, $Key_old_pwd: $old_pwd, Key_message: $message}';
  }
}
