import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/pay_cancel_save_explain_data.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///
/// Created by   : platformbiz@ggumbi.com
///
/// version      : 1.0.0
///
/// see          : pay_cancel_save_response.dart - 결제취소 요청 저장
///
/// since        : 2021/09/09 / update:
///
/// (0, '계약중'),
///
/// (11, '계약실패(거절/취소/응답없음/시간만료 등)'),
///
/// (21, '계약완료 후 취소(부분)'),
///
/// (22, '계약완료 후 취소(전체)'),
///
/// (92, '매칭성공(계약완료/결제대기중)'),
///
/// (100, '결제완료')
///
/// "msg_cd": 5400, "message": "취소 할 수 없는 결제 입니다.",
///
/// "msg_cd": 5401, "message": "이미 취소된 결제 입니다.",
///
/// "msg_cd": 4000, "message": "데이터가 없습니다.",

class PayCancelSaveResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PayCanceSaveExplainData>? dataList;

  PayCancelSaveResponse({
    this.dataHeader,
    this.dataList,
  });

  PayCanceSaveExplainData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PayCanceSaveExplainData();

  List<PayCanceSaveExplainData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory PayCancelSaveResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PayCanceSaveExplainData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<PayCanceSaveExplainData>((i) => PayCanceSaveExplainData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return PayCancelSaveResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ChatRoomUpdateResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
