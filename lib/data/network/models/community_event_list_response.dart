import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

class CommunityEventListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<EventListData>? dataList;

  CommunityEventListResponse({this.dataHeader, this.dataList});
  EventListData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : EventListData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();
  factory CommunityEventListResponse.fromJson(Map<String, dynamic> json) {
    return CommunityEventListResponse(dataHeader: HeaderResponse.fromJson(json), dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => EventListData.fromJson(e)).toList());
  }

  @override
  String toString() {
    return 'CommunityEventListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class EventListData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<EventInfo>? results;

  EventListData({this.count = 0, this.next = '', this.previous = '', this.results});

  factory EventListData.fromJson(Map<String, dynamic> json) {
    return EventListData(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] != null ? (json[Key_results] as List).map((e) => EventInfo.fromJson(e)).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_count] = this.next;
    data[Key_previous] = this.previous;
    if (this.results != null) data[Key_results] = this.results!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'EventListData{count: $count,next: $next,previous: $previous,results: $results}';
  }
}

class EventInfo {
  static const String Key_id = 'id';
  static const String Key_title = 'title';
  static const String Key_banner = 'banner';
  static const String Key_start_date = 'start_date';
  static const String Key_end_date = 'end_date';
  static const String Key_image = 'image';
  static const String Key_is_reply = 'is_reply';
  static const String Key_webview_url = 'webview_url';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_title)
  String title;
  @JsonKey(name: Key_banner)
  String banner;
  @JsonKey(name: Key_start_date)
  String startDate;
  @JsonKey(name: Key_end_date)
  String endDate;
  @JsonKey(name: Key_image)
  String image;
  @JsonKey(name: Key_is_reply)
  bool isReply;
  @JsonKey(name: Key_webview_url)
  String webviewUrl;

  EventInfo({this.id = 0, this.title = '', this.banner = '', this.startDate = '', this.endDate = '', this.image = '', this.isReply = false, this.webviewUrl = ''});

  factory EventInfo.fromJson(Map<String, dynamic> json) {
    return EventInfo(
      id: json[Key_id] ?? 0,
      title: json[Key_title] ?? '',
      banner: json[Key_banner] ?? '',
      startDate: json[Key_start_date] ?? '',
      endDate: json[Key_end_date] ?? '',
      image: json[Key_image] ?? '',
      isReply: json[Key_is_reply] ?? false,
      webviewUrl: json[Key_webview_url] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_title] = this.title;
    data[Key_banner] = this.banner;
    data[Key_start_date] = this.startDate;
    data[Key_end_date] = this.endDate;
    data[Key_image] = this.image;
    data[Key_is_reply] = this.isReply;
    data[Key_webview_url] = this.webviewUrl;
    return data;
  }

  @override
  String toString() {
    return 'Results{id:$id, title:$title, banner:$banner, startDate:$startDate, endDate:$endDate, image: $image, isReply: $isReply, webviewUrl: $webviewUrl}';
  }
}
