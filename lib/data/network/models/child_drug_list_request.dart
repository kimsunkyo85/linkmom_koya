import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class ChildDrugListRequest {
  static const String Key_child = 'child';

  ChildDrugListRequest({
    this.child = 0
  });

  @JsonKey(name: Key_child)
  int child;

  factory ChildDrugListRequest.fromJson(Map<String, dynamic> json) {
    return ChildDrugListRequest(
      child: json[Key_child] as int
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_child] = this.child;
    return data;
  }

  @override
  String toString() {
    return 'ChildDrugListRequest{$Key_child: $child}';
  }
}
