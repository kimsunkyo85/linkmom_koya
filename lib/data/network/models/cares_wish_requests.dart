import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CaresWishSaveRequest {
  static const String Key_bookingcare = 'bookingcare';

  CaresWishSaveRequest({
    this.bookingcare = 0,
  });

  @JsonKey(name: Key_bookingcare)
  int bookingcare;

  factory CaresWishSaveRequest.fromJson(Map<String, dynamic> json) {
    return CaresWishSaveRequest(
      bookingcare: json[Key_bookingcare] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bookingcare] = this.bookingcare;
    return data;
  }

  @override
  String toString() {
    return 'CaresWishSaveRequest{bookingcare: $bookingcare}';
  }
}

@JsonSerializable()
class CaresWishDeleteRequest {
  static const String Key_bookingcare = 'bookingcare';

  CaresWishDeleteRequest({
    this.bookingcare = 0,
  });

  @JsonKey(name: Key_bookingcare)
  int bookingcare;

  factory CaresWishDeleteRequest.fromJson(Map<String, dynamic> json) {
    return CaresWishDeleteRequest(
      bookingcare: json[Key_bookingcare] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bookingcare] = this.bookingcare;
    return data;
  }

  @override
  String toString() {
    return 'CaresWishDeleteRequest{bookingcare: $bookingcare}';
  }
}
