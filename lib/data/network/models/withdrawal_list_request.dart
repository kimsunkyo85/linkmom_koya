import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class WithdrawalListRequest {
  static const String Key_start_date = 'start_date';
  static const String Key_end_date = 'end_date';
  static const String Key_is_withdrawal = 'is_withdrawal';

  @JsonKey(name: Key_start_date)
  String startDate;
  @JsonKey(name: Key_end_date)
  String endDate;
  @JsonKey(name: Key_is_withdrawal)
  int? isWithdrawal;

  WithdrawalListRequest({
    this.startDate = '',
    this.endDate = '',
    this.isWithdrawal,
  });

  factory WithdrawalListRequest.fromJson(Map<String, dynamic> json) {
    return WithdrawalListRequest(
      startDate: json[Key_start_date] ?? '',
      endDate: json[Key_end_date] ?? '',
      isWithdrawal: json[Key_is_withdrawal] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_start_date] = this.startDate;
    data[Key_end_date] = this.endDate;
    if (isWithdrawal != null) data[Key_is_withdrawal] = this.isWithdrawal;
    return data;
  }

  FormData toFormData() {
    FormData formData = FormData();
    formData.fields.add(MapEntry(Key_start_date, startDate.toString()));
    formData.fields.add(MapEntry(Key_end_date, endDate.toString()));
    formData.fields.add(MapEntry(Key_is_withdrawal, isWithdrawal.toString()));
    return formData;
  }

  @override
  String toString() {
    return 'WithdrawalListRequest{startDate: $startDate, endDate: $endDate, isWithdrawal: $isWithdrawal}';
  }
}
