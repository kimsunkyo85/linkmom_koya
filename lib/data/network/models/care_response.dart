import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'req_data.dart';

@JsonSerializable()
class CareResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<CareData>? dataList;

  CareResponse({
    this.dataHeader,
    this.dataList,
  });

  CareData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CareData();

  List<CareData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareData> datas = [];
    try {
      datas = json[Key_data].map<CareData>((i) => CareData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AuthCenterHomeResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class CareData {
  static const String Key_id = 'id';
  static const String Key_child = 'child';
  static const String Key_servicetype = 'servicetype';
  static const String Key_possible_area = 'possible_area';
  static const String Key_cost_perhour = 'cost_perhour';
  static const String Key_cost_hoursum = 'cost_hoursum';
  static const String Key_cost_negotiable = 'cost_negotiable';
  static const String Key_cost_sum = 'cost_sum';
  static const String Key_cost_avg = 'cost_avg';
  static const String Key_is_negotiable = 'is_negotiable';
  static const String Key_is_togetherboyuk = 'is_togetherboyuk';
  static const String Key_is_view = 'is_view';
  static const String Key_status = 'status';
  static const String Key_createdate = 'createdate';

  ///돌봄 신청 날짜
  static const String Key_careschedule = 'careschedule';

  ///등원 서비스
  static const String Key_bookinggotoschool = 'bookinggotoschool';

  ///하원 서비스
  static const String Key_bookingafterschool = 'bookingafterschool';

  ///보육 서비스
  static const String Key_bookingboyuk = 'bookingpathroute';

  ///보육 옵션
  static const String Key_bookingboyukoptions = 'bookingboyukoptions';

  ///놀이 옵션
  static const String Key_bookingplayoptions = 'bookingplayoptions';

  ///가사 옵션
  static const String Key_bookinghomecareoptions = 'bookinghomecareoptions';
  static const String Key_reqdata = 'reqdata';

  CareData({
    this.id = 0,
    this.child = 0,
    this.servicetype = 0,
    this.possible_area = 0,
    this.cost_perhour = 0,
    this.cost_hoursum = 0,
    this.cost_negotiable = 0,
    this.cost_sum = 0,
    this.cost_avg = 0,
    this.is_negotiable = false,
    this.is_togetherboyuk = false,
    this.is_view = false,
    this.status = 0,
    this.createdate = '',
    this.careschedule,
    this.bookinggotoschool,
    this.bookingboyukoptions,
    this.bookingboyuk,
    this.bookingafterschool,
    this.bookingplayoptions,
    this.bookinghomecareoptions,
    this.reqdata,
  });

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_child)
  int child;
  @JsonKey(name: Key_servicetype)
  int servicetype;
  @JsonKey(name: Key_possible_area)
  int possible_area;
  @JsonKey(name: Key_cost_perhour)
  int cost_perhour;
  @JsonKey(name: Key_cost_hoursum)
  int cost_hoursum;
  @JsonKey(name: Key_cost_negotiable)
  int cost_negotiable;
  @JsonKey(name: Key_cost_sum)
  int cost_sum;
  @JsonKey(name: Key_cost_avg)
  int cost_avg;
  @JsonKey(name: Key_is_negotiable)
  bool is_negotiable;
  @JsonKey(name: Key_is_togetherboyuk)
  bool is_togetherboyuk;
  @JsonKey(name: Key_is_view)
  bool is_view;
  @JsonKey(name: Key_status)
  int status;
  @JsonKey(name: Key_createdate)
  String createdate;

  ///돌봄 신청 날자
  @JsonKey(name: Key_careschedule)
  CareScheduleData? careschedule;

  ///등원 서비스
  @JsonKey(name: Key_bookinggotoschool)
  CareSchoolToHomeData? bookinggotoschool;

  ///하원 서비스
  @JsonKey(name: Key_bookingafterschool)
  CareSchoolToHomeData? bookingafterschool;

  ///보육 서비스
  @JsonKey(name: Key_bookingboyuk)
  CareBoyukData? bookingboyuk;

  ///보육 옵션
  @JsonKey(name: Key_bookingboyukoptions)
  CareOptionData? bookingboyukoptions;

  ///놀이 옵션
  @JsonKey(name: Key_bookingplayoptions)
  CareOptionData? bookingplayoptions;

  ///가사 옵션
  @JsonKey(name: Key_bookinghomecareoptions)
  CareOptionData? bookinghomecareoptions;

  @JsonKey(name: Key_reqdata)
  final ReqData? reqdata;

  factory CareData.fromJson(Map<String, dynamic> json) {
    CareScheduleData careschedule = CareScheduleData.fromJson(json[Key_careschedule]);
    CareSchoolToHomeData bookinggotoschool = CareSchoolToHomeData.fromJson(json[Key_bookinggotoschool]);
    CareSchoolToHomeData bookingafterschool = CareSchoolToHomeData.fromJson(json[Key_bookingafterschool]);
    CareBoyukData bookingboyuk = CareBoyukData.fromJson(json[Key_bookingboyuk]);
    CareOptionData bookingboyukoptions = CareOptionData.fromJson(json[Key_bookingboyukoptions]);
    CareOptionData bookingplayoptions = CareOptionData.fromJson(json[Key_bookingplayoptions]);
    CareOptionData bookinghomecareoptions = CareOptionData.fromJson(json[Key_bookinghomecareoptions]);
    ReqData reqdata = ReqData();
    try {
      reqdata = json[Key_reqdata] == null ? ReqData() : ReqData.fromJson(jsonDecode(json[Key_reqdata]));
    } catch (e) {
      reqdata = ReqData();
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareData(
      id: json[Key_id] as int,
      child: json[Key_child] as int,
      servicetype: json[Key_servicetype] as int,
      possible_area: json[Key_possible_area] as int,
      cost_perhour: json[Key_cost_perhour] as int,
      cost_hoursum: json[Key_cost_hoursum] as int,
      cost_negotiable: json[Key_cost_negotiable] as int,
      cost_sum: json[Key_cost_sum] as int,
      cost_avg: json[Key_cost_avg] as int,
      is_negotiable: json[Key_is_negotiable] as bool,
      is_togetherboyuk: json[Key_is_togetherboyuk] as bool,
      is_view: json[Key_is_view] as bool,
      status: json[Key_status] as int,
      createdate: json[Key_createdate] ?? '',
      careschedule: careschedule,
      bookinggotoschool: bookinggotoschool,
      bookingafterschool: bookingafterschool,
      bookingboyuk: bookingboyuk,
      bookingboyukoptions: bookingboyukoptions,
      bookingplayoptions: bookingplayoptions,
      bookinghomecareoptions: bookinghomecareoptions,
      reqdata: reqdata,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_child] = this.child;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_cost_perhour] = this.cost_perhour;
    data[Key_cost_hoursum] = this.cost_hoursum;
    data[Key_cost_negotiable] = this.cost_negotiable;
    data[Key_cost_sum] = this.cost_sum;
    data[Key_cost_avg] = this.cost_avg;
    data[Key_is_negotiable] = this.is_negotiable;
    data[Key_is_togetherboyuk] = this.is_togetherboyuk;
    data[Key_is_view] = this.is_view;
    data[Key_status] = this.status;
    data[Key_createdate] = this.createdate;

    data[Key_careschedule] = this.careschedule;
    data[Key_bookinggotoschool] = this.bookinggotoschool;
    data[Key_bookingafterschool] = this.bookingafterschool;
    data[Key_bookingboyuk] = this.bookingboyuk;
    data[Key_bookingboyukoptions] = this.bookingboyukoptions;
    data[Key_bookingplayoptions] = this.bookingplayoptions;
    data[Key_bookinghomecareoptions] = this.bookinghomecareoptions;
    data[Key_reqdata] = this.reqdata;
    return data;
  }

  @override
  String toString() {
    return 'CareData{id: $id, child: $child, servicetype: $servicetype, possible_area: $possible_area, cost_perhour: $cost_perhour, cost_hoursum: $cost_hoursum, cost_negotiable: $cost_negotiable, cost_sum: $cost_sum, cost_avg: $cost_avg, is_negotiable: $is_negotiable, is_togetherboyuk: $is_togetherboyuk, is_view: $is_view, status: $status, createdate: $createdate, careschedule: $careschedule, bookinggotoschool: $bookinggotoschool, bookingafterschool: $bookingafterschool, bookingboyuk: $bookingboyuk, bookingboyukoptions: $bookingboyukoptions, bookingplayoptions: $bookingplayoptions, bookinghomecareoptions: $bookinghomecareoptions, reqdata: $reqdata}';
  }
}

///스케쥴 정보
class CareScheduleData {
  static const String Key_id = 'id';
  static const String Key_stime = 'stime';
  static const String Key_etime = 'etime';
  static const String Key_is_am = 'is_am';
  static const String Key_is_pm = 'is_pm';
  static const String Key_is_repeat = 'is_repeat';
  static const String Key_durationtime = 'durationtime';
  static const String Key_caredate = 'caredate';

  CareScheduleData({
    this.id = 0,
    this.stime = '',
    this.etime = '',
    this.is_am = false,
    this.is_pm = false,
    this.is_repeat = false,
    this.durationtime = 0,
    this.caredate,
  });

  @JsonKey(name: Key_id)
  int id;

  ///시작 시간
  @JsonKey(name: Key_stime)
  String stime;

  ///종료 시간
  @JsonKey(name: Key_etime)
  String etime;

  ///오전 1, 아니면 0, 오전오후 걸치면 1
  @JsonKey(name: Key_is_am)
  bool is_am;

  ///오후 1, 아니면 0, 오전오후 걸치면 1
  @JsonKey(name: Key_is_pm)
  bool is_pm;

  ///반복
  @JsonKey(name: Key_is_repeat)
  bool is_repeat;

  ///시간 합계(분)
  @JsonKey(name: Key_durationtime)
  int durationtime;

  ///날짜 데이터
  @JsonKey(name: Key_caredate)
  List<String>? caredate;

  factory CareScheduleData.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return CareScheduleData();
    }
    List<String> caredate = json[Key_caredate] == null ? [] : List<String>.from(json[Key_caredate]);
    return CareScheduleData(
      id: json[Key_id] as int,
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      is_am: json[Key_is_am] as bool,
      is_pm: json[Key_is_pm] as bool,
      is_repeat: json[Key_is_repeat] as bool,
      durationtime: json[Key_durationtime] as int,
      caredate: caredate,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_is_am] = this.is_am;
    data[Key_is_pm] = this.is_pm;
    data[Key_is_repeat] = this.is_repeat;
    data[Key_durationtime] = this.durationtime;
    data[Key_caredate] = this.caredate;
    return data;
  }

  @override
  String toString() {
    return 'CareScheduleData{id: $id, stime: $stime, etime: $etime, is_am: $is_am, is_pm: $is_pm, durationtime: $durationtime, repeat: $is_repeat, caredate: $caredate}';
  }
}

///보육
class CareBoyukData {
  static const String Key_id = 'id';
  static const String Key_boyuk_address = 'boyuk_address';
  static const String Key_boyuk_address_detail = 'boyuk_address_detail';
  static const String Key_boyuk_comment = 'boyuk_comment';
  static const String Key_pathroute_comment = 'pathroute_comment';
  static const String Key_nhn_area_1_id = 'nhn_area_1_id';
  static const String Key_nhn_address_1_id = 'nhn_address_1_id';
  static const String Key_nhn_area_1_address = 'nhn_area_1_address';
  static const String Key_nhn_area_2_id = 'nhn_area_2_id';
  static const String Key_nhn_address_2_id = 'nhn_address_2_id';
  static const String Key_nhn_area_2_address = 'nhn_area_2_address';
  static const String Key_nhn_radius = 'nhn_radius';
  static const String Key_nhn_comment = 'nhn_comment';

  CareBoyukData({
    this.id = 0,
    this.boyuk_address = '',
    this.boyuk_address_detail = '',
    this.boyuk_comment = '',
    this.pathroute_comment = '',
    this.nhn_area_1_id = '',
    this.nhn_address_1_id = '',
    this.nhn_area_1_address = '',
    this.nhn_area_2_id = '',
    this.nhn_address_2_id = '',
    this.nhn_area_2_address = '',
    this.nhn_radius = 0,
    this.nhn_comment = '',
  });

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_boyuk_address)
  String boyuk_address;
  @JsonKey(name: Key_boyuk_address_detail)
  String boyuk_address_detail;
  @JsonKey(name: Key_boyuk_comment)
  String boyuk_comment;
  @JsonKey(name: Key_pathroute_comment)
  String pathroute_comment;
  @JsonKey(name: Key_nhn_area_1_id)
  String nhn_area_1_id;
  @JsonKey(name: Key_nhn_area_1_address)
  String nhn_area_1_address;
  @JsonKey(name: Key_nhn_address_1_id)
  String nhn_address_1_id;
  @JsonKey(name: Key_nhn_area_2_id)
  String nhn_area_2_id;
  @JsonKey(name: Key_nhn_address_2_id)
  String nhn_address_2_id;
  @JsonKey(name: Key_nhn_area_2_address)
  String nhn_area_2_address;
  @JsonKey(name: Key_nhn_radius)
  int nhn_radius;
  @JsonKey(name: Key_nhn_comment)
  String nhn_comment;

  factory CareBoyukData.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return CareBoyukData();
    }
    return CareBoyukData(
      id: json[Key_id] as int,
      boyuk_address: json[Key_boyuk_address] ?? '',
      boyuk_address_detail: json[Key_boyuk_address_detail] ?? '',
      boyuk_comment: json[Key_boyuk_comment] ?? '',
      pathroute_comment: json[Key_pathroute_comment] ?? '',
      nhn_area_1_id: json[Key_nhn_area_1_id] ?? '',
      nhn_address_1_id: json[Key_nhn_address_1_id] ?? '',
      nhn_area_1_address: json[Key_nhn_area_1_address] ?? '',
      nhn_area_2_id: json[Key_nhn_area_2_id] ?? '',
      nhn_address_2_id: json[Key_nhn_address_2_id] ?? '',
      nhn_area_2_address: json[Key_nhn_area_2_address] ?? '',
      nhn_radius: json[Key_nhn_radius] as int,
      nhn_comment: json[Key_nhn_comment] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_boyuk_address] = this.boyuk_address;
    data[Key_boyuk_address_detail] = this.boyuk_address_detail;
    data[Key_boyuk_comment] = this.boyuk_comment;
    data[Key_pathroute_comment] = this.pathroute_comment;
    data[Key_nhn_area_1_id] = this.nhn_area_1_id;
    data[Key_nhn_address_1_id] = this.nhn_address_1_id;
    data[Key_nhn_area_1_address] = this.nhn_area_1_address;
    data[Key_nhn_area_2_id] = this.nhn_area_2_id;
    data[Key_nhn_address_2_id] = this.nhn_address_2_id;
    data[Key_nhn_area_2_address] = this.nhn_area_2_address;
    data[Key_nhn_radius] = this.nhn_radius;
    data[Key_nhn_comment] = this.nhn_comment;
    return data;
  }

  @override
  String toString() {
    return 'CareBoyukData{id: $id, boyuk_address: $boyuk_address, boyuk_address_detail: $boyuk_address_detail, boyuk_comment: $boyuk_comment, pathroute_comment: $pathroute_comment, nhn_area_1_id: $nhn_area_1_id, nhn_area_1_address: $nhn_area_1_address, nhn_address_1_id: $nhn_address_1_id, nhn_area_2_id: $nhn_area_2_id, nhn_address_2_id: $nhn_address_2_id, nhn_area_2_address: $nhn_area_2_address, nhn_radius: $nhn_radius, nhn_comment: $nhn_comment}';
  }
}

///등원/하원정보
class CareSchoolToHomeData {
  static const String Key_id = 'id';
  static const String Key_product_id = 'product_id';
  static const String Key_start_address = 'start_address';
  static const String Key_start_address_detail = 'start_address_detail';
  static const String Key_start_comment = 'start_comment';
  static const String Key_pathroute_comment = 'pathroute_comment';
  static const String Key_start_time = 'start_time';
  static const String Key_end_address = 'end_address';
  static const String Key_end_address_detail = 'end_address_detail';
  static const String Key_end_time = 'end_time';
  static const String Key_end_comment = 'end_comment';
  static const String Key_nhn_area_1_id = 'nhn_area_1_id';
  static const String Key_nhn_address_1_id = 'nhn_address_1_id';
  static const String Key_nhn_area_1_address = 'nhn_area_1_address';
  static const String Key_nhn_area_2_id = 'nhn_area_2_id';
  static const String Key_nhn_address_2_id = 'nhn_address_2_id';
  static const String Key_nhn_area_2_address = 'nhn_area_2_address';
  static const String Key_nhn_radius = 'nhn_radius';
  static const String Key_nhn_comment = 'nhn_comment';

  CareSchoolToHomeData({
    this.id = 0,
    this.product_id,
    this.start_address = '',
    this.start_address_detail = '',
    this.start_time = '',
    this.start_comment = '',
    this.pathroute_comment = '',
    this.end_address = '',
    this.end_address_detail = '',
    this.end_time = '',
    this.end_comment = '',
    this.nhn_area_1_id = '',
    this.nhn_address_1_id = '',
    this.nhn_area_1_address = '',
    this.nhn_area_2_id = '',
    this.nhn_address_2_id = '',
    this.nhn_area_2_address = '',
    this.nhn_radius = 0,
    this.nhn_comment = '',
  });

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_product_id)
  List<int>? product_id;
  @JsonKey(name: Key_start_address)
  String start_address;
  @JsonKey(name: Key_start_address_detail)
  String start_address_detail;
  @JsonKey(name: Key_start_time)
  String start_time;
  @JsonKey(name: Key_start_comment)
  String start_comment;
  @JsonKey(name: Key_pathroute_comment)
  String pathroute_comment;
  @JsonKey(name: Key_end_address)
  String end_address;
  @JsonKey(name: Key_end_address_detail)
  String end_address_detail;
  @JsonKey(name: Key_end_time)
  String end_time;
  @JsonKey(name: Key_end_comment)
  String end_comment;

  ///1순위
  @JsonKey(name: Key_nhn_area_1_id)
  String nhn_area_1_id;

  @JsonKey(name: Key_nhn_address_1_id)
  String nhn_address_1_id;

  ///1순위 장소
  @JsonKey(name: Key_nhn_area_1_address)
  String nhn_area_1_address;

  ///2순위
  @JsonKey(name: Key_nhn_area_2_id)
  String nhn_area_2_id;

  @JsonKey(name: Key_nhn_address_2_id)
  String nhn_address_2_id;

  ///2순위 장소
  @JsonKey(name: Key_nhn_area_2_address)
  String nhn_area_2_address;

  ///반경
  @JsonKey(name: Key_nhn_radius)
  int nhn_radius;

  ///추가설명
  @JsonKey(name: Key_nhn_comment)
  String nhn_comment;

  factory CareSchoolToHomeData.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return CareSchoolToHomeData();
    }
    List<int> productId = json[Key_product_id] == null ? [] : List<int>.from(json[Key_product_id]);
    return CareSchoolToHomeData(
      id: json[Key_id] as int,
      product_id: productId,
      start_address: json[Key_start_address] ?? '',
      start_address_detail: json[Key_start_address_detail] ?? '',
      start_time: json[Key_start_time] ?? '',
      start_comment: json[Key_start_comment] ?? '',
      pathroute_comment: json[Key_pathroute_comment] ?? '',
      end_address: json[Key_end_address] ?? '',
      end_address_detail: json[Key_end_address_detail] ?? '',
      end_time: json[Key_end_time] ?? '',
      end_comment: json[Key_end_comment] ?? '',
      nhn_area_1_id: json[Key_nhn_area_1_id] ?? '',
      nhn_address_1_id: json[Key_nhn_address_1_id] ?? '',
      nhn_area_1_address: json[Key_nhn_area_1_address] ?? '',
      nhn_area_2_id: json[Key_nhn_area_2_id] ?? '',
      nhn_address_2_id: json[Key_nhn_address_2_id] ?? '',
      nhn_area_2_address: json[Key_nhn_area_2_address] ?? '',
      nhn_radius: json[Key_nhn_radius] as int,
      nhn_comment: json[Key_nhn_comment] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_product_id] = this.product_id;
    data[Key_start_address] = this.start_address;
    data[Key_start_address_detail] = this.start_address_detail;
    data[Key_start_time] = this.start_time;
    data[Key_start_comment] = this.start_comment;
    data[Key_pathroute_comment] = this.pathroute_comment;
    data[Key_end_address] = this.end_address;
    data[Key_end_address_detail] = this.end_address_detail;
    data[Key_end_time] = this.end_time;
    data[Key_end_comment] = this.end_comment;
    data[Key_nhn_area_1_id] = this.nhn_area_1_id;
    data[Key_nhn_address_1_id] = this.nhn_address_1_id;
    data[Key_nhn_area_1_address] = this.nhn_area_1_address;
    data[Key_nhn_area_2_id] = this.nhn_area_2_id;
    data[Key_nhn_address_2_id] = this.nhn_address_2_id;
    data[Key_nhn_area_2_address] = this.nhn_area_2_address;
    data[Key_nhn_radius] = this.nhn_radius;
    data[Key_nhn_comment] = this.nhn_comment;
    return data;
  }

  @override
  String toString() {
    return 'CareSchoolToHomeData{id: $id, product_id: $product_id, start_address: $start_address, start_address_detail: $start_address_detail, start_time: $start_time, start_comment: $start_comment, pathroute_comment: $pathroute_comment, end_address: $end_address, end_address_detail: $end_address_detail, end_time: $end_time, end_comment: $end_comment, nhn_area_1_id: $nhn_area_1_id, nhn_address_1_id: $nhn_address_1_id, nhn_area_1_address: $nhn_area_1_address, nhn_area_2_id: $nhn_area_2_id, nhn_address_2_id: $nhn_address_2_id, nhn_area_2_address: $nhn_area_2_address, nhn_radius: $nhn_radius, nhn_comment: $nhn_comment}';
  }
}

///보육,학습,입주가사 돌봄 정보
class CareOptionData {
  static const String Key_id = 'id';
  static const String Key_reqdata = 'reqdata';

  CareOptionData({
    this.id = 0,
    this.reqdata,
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_reqdata)
  final List<CareOptionDataItem>? reqdata;

  factory CareOptionData.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return CareOptionData();
    }
    List<CareOptionDataItem> reqdata = json[Key_reqdata] == null ? [] : json[Key_reqdata].map<CareOptionDataItem>((i) => CareOptionDataItem.fromJson(i)).toList();
    return CareOptionData(
      id: json[Key_id] as int,
      reqdata: reqdata,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_reqdata] = this.reqdata;
    return data;
  }

  @override
  String toString() {
    return 'CareOptionData{id: $id, reqdata: $reqdata}';
  }
}

///보육,학습,입주가사 돌봄 정보
class CareOptionDataItem {
  static const String Key_product_id = 'product_id';
  static const String Key_product_name = 'product_name';
  static const String Key_product_cost = 'product_cost';
  static const String Key_product_comment = 'product_comment';

  CareOptionDataItem({
    this.product_id = 0,
    this.product_name = '',
    this.product_cost = 0,
    this.product_comment = '',
  });

  @JsonKey(name: Key_product_id)
  final int product_id;
  @JsonKey(name: Key_product_name)
  final String product_name;
  @JsonKey(name: Key_product_cost)
  final int product_cost;
  @JsonKey(name: Key_product_comment)
  final String product_comment;

  factory CareOptionDataItem.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return CareOptionDataItem();
    }
    return CareOptionDataItem(
      product_id: json[Key_product_id] as int,
      product_name: json[Key_product_name] ?? '',
      product_cost: json[Key_product_cost] as int,
      product_comment: json[Key_product_comment] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_product_id] = this.product_id;
    data[Key_product_name] = this.product_name;
    data[Key_product_cost] = this.product_cost;
    data[Key_product_comment] = this.product_comment;
    return data;
  }

  @override
  String toString() {
    return 'CareOptionDataItem{product_id: $product_id, product_name: $product_name, product_cost: $product_cost, product_comment: $product_comment}';
  }
}

///이유식 반찬
class FoodServicesData {
  static const String Key_id = 'id';
  static const String Key_servicetype = 'servicetype';
  static const String Key_possible_area = 'possible_area';
  static const String Key_startdate = 'startdate';
  static const String Key_enddate = 'enddate';

  FoodServicesData({
    this.id = 0,
    this.servicetype = 0,
    this.possible_area = 0,
    this.startdate = '',
    this.enddate = '',
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_servicetype)
  final int servicetype;
  @JsonKey(name: Key_possible_area)
  final int possible_area;
  @JsonKey(name: Key_startdate)
  final String startdate;
  @JsonKey(name: Key_enddate)
  final String enddate;

  factory FoodServicesData.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return FoodServicesData();
    }
    return FoodServicesData(
      id: json[Key_id] as int,
      servicetype: json[Key_servicetype] as int,
      possible_area: json[Key_possible_area] as int,
      startdate: json[Key_startdate] ?? '',
      enddate: json[Key_enddate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_startdate] = this.startdate;
    data[Key_enddate] = this.enddate;
    return data;
  }

  @override
  String toString() {
    return 'FoodServicesData{id: $id, servicetype: $servicetype, possible_area: $possible_area, startdate: $startdate, enddate: $enddate}';
  }
}
