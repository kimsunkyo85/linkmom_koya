import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/main.dart';

import 'header_response.dart';

@JsonSerializable()
class CommunityRankViewResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<RankData>? dataList;

  CommunityRankViewResponse({this.dataHeader, this.dataList});

  RankData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : RankData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CommunityRankViewResponse.fromJson(Map<String, dynamic> json) {
    return CommunityRankViewResponse(dataHeader: HeaderResponse.fromJson(json), dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => RankData.fromJson(e)).toList());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_data] = this.dataList;
    return data;
  }

  @override
  String toString() {
    return 'CommunityRankViewResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class RankData {
  static const String Key_userinfo = 'userinfo';
  static const String Key_rankinginfo = 'rankinginfo';
  static const String Key_careinfo = 'careinfo';
  static const String Key_reviewinfo = 'reviewinfo';

  @JsonKey(name: Key_userinfo)
  Userinfo? userinfo;
  @JsonKey(name: Key_rankinginfo)
  Rankinginfo? rankinginfo;
  @JsonKey(name: Key_careinfo)
  Careinfo? careinfo;
  @JsonKey(name: Key_reviewinfo)
  List<RankReviewInfo>? reviewinfo = [];

  RankData({this.userinfo, this.rankinginfo, this.careinfo, this.reviewinfo});

  factory RankData.fromJson(Map<String, dynamic> json) {
    List<RankReviewInfo> list = [];
    late Userinfo user = Userinfo();
    late Rankinginfo rank = Rankinginfo();
    late Careinfo care = Careinfo();
    try {
      list = json[Key_reviewinfo] != null && json[Key_reviewinfo].isNotEmpty ? json[Key_reviewinfo].map<RankReviewInfo>((e) => RankReviewInfo.fromJson(e)).toList() : [];
      user = Userinfo.fromJson(json[Key_userinfo]);
      rank = Rankinginfo.fromJson(json[Key_rankinginfo]);
      care = Careinfo.fromJson(json[Key_careinfo]);
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>> $e");
    }
    return RankData(
      userinfo: user,
      rankinginfo: rank,
      careinfo: care,
      reviewinfo: list,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_userinfo] = this.userinfo;
    data[Key_rankinginfo] = this.rankinginfo;
    data[Key_careinfo] = this.careinfo;
    data[Key_reviewinfo] = this.reviewinfo;
    return data;
  }

  @override
  String toString() {
    return 'RankData{userinfo: $userinfo, rankinginfo: $rankinginfo, careinfo: $careinfo, reviewinfo: $reviewinfo}';
  }
}

class Rankinginfo {
  static const String Key_price_sum = 'price_sum';
  static const String Key_like_cnt = 'like_cnt';
  static const String Key_review_cnt = 'review_cnt';

  @JsonKey(name: Key_price_sum)
  int priceSum;
  @JsonKey(name: Key_like_cnt)
  int likeCnt;
  @JsonKey(name: Key_review_cnt)
  int reviewCnt;

  Rankinginfo({this.priceSum = 0, this.likeCnt = 0, this.reviewCnt = 0});

  factory Rankinginfo.fromJson(Map<String, dynamic> json) {
    return Rankinginfo(
      priceSum: json[Key_price_sum] as int,
      likeCnt: json[Key_like_cnt] as int,
      reviewCnt: json[Key_review_cnt] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_price_sum] = this.priceSum;
    data[Key_like_cnt] = this.likeCnt;
    data[Key_review_cnt] = this.reviewCnt;
    return data;
  }

  @override
  String toString() {
    return 'Rankinginfo{priceSum: $priceSum, likeCnt: $likeCnt, reviewCnt: $reviewCnt}';
  }
}

class Userinfo {
  static const String Key_user_id = 'user_id';
  static const String Key_first_name = 'first_name';
  static const String Key_gender = 'gender';
  static const String Key_age = 'age';
  static const String Key_job = 'job';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_profileimg = 'profileimg';

  @JsonKey(name: Key_user_id)
  int userId;
  @JsonKey(name: Key_first_name)
  String firstName;
  @JsonKey(name: Key_gender)
  String gender;
  @JsonKey(name: Key_age)
  String age;
  @JsonKey(name: Key_job)
  String job;
  @JsonKey(name: Key_region_3depth)
  String region3Depth;
  @JsonKey(name: Key_profileimg)
  String profileimg;

  Userinfo({this.userId = 0, this.firstName = '', this.gender = '', this.age = '', this.job = '', this.region3Depth = '', this.profileimg = ''});

  factory Userinfo.fromJson(Map<String, dynamic> json) {
    return Userinfo(
      userId: json[Key_user_id] as int,
      firstName: json[Key_first_name] ?? '',
      gender: json[Key_gender] ?? '',
      age: json[Key_age] ?? '',
      job: json[Key_job] ?? '',
      region3Depth: json[Key_region_3depth] ?? '',
      profileimg: json[Key_profileimg] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_user_id] = this.userId;
    data[Key_first_name] = this.firstName;
    data[Key_gender] = this.gender;
    data[Key_age] = this.age;
    data[Key_job] = this.job;
    data[Key_region_3depth] = this.region3Depth;
    data[Key_profileimg] = this.profileimg;
    return data;
  }

  @override
  String toString() {
    return 'Userinfo{userId: $userId, firstName: $firstName, gender: $gender, age: $age, job: $job, region3Depth: $region3Depth, profileimg: $profileimg}';
  }
}

class Careinfo {
  static const String Key_total_caretime = 'total_caretime';
  static const String Key_servicetype = 'servicetype';
  static const String Key_grade_linkmom = 'grade_linkmom';
  static const String Key_grade_authlevel = 'grade_authlevel';
  static const String Key_hashtag_cctv = 'hashtag_cctv';
  static const String Key_hashtag_anmal = 'hashtag_anmal';
  static const String Key_hashtag_codiv_vaccine = 'hashtag_codiv_vaccine';
  static const String Key_hashtag_deungbon = 'hashtag_deungbon';
  static const String Key_hashtag_criminal = 'hashtag_criminal';
  static const String Key_hashtag_personality = 'hashtag_personality';
  static const String Key_hashtag_healthy = 'hashtag_healthy';
  static const String Key_hashtag_career_2 = 'hashtag_career_2';
  static const String Key_hashtag_education = 'hashtag_education';
  static const String Key_hashtag_career_1 = 'hashtag_career_1';
  static const String Key_hashtag_graduated = 'hashtag_graduated';
  static const String Key_hashtag_babysiter = 'hashtag_babysiter';
  static const String Key_auth_id = 'auth_id';

  @JsonKey(name: Key_total_caretime)
  int totalCaretime;
  @JsonKey(name: Key_servicetype)
  List<String>? servicetype;
  @JsonKey(name: Key_grade_linkmom)
  String gradeLinkmom;
  @JsonKey(name: Key_grade_authlevel)
  int gradeAuthlevel;
  @JsonKey(name: Key_hashtag_cctv)
  bool hashtagCctv;
  @JsonKey(name: Key_hashtag_anmal)
  bool hashtagAnmal;
  @JsonKey(name: Key_hashtag_codiv_vaccine)
  bool hashtagCodivVaccine;
  @JsonKey(name: Key_hashtag_deungbon)
  bool hashtagDeungbon;
  @JsonKey(name: Key_hashtag_criminal)
  bool hashtagCriminal;
  @JsonKey(name: Key_hashtag_personality)
  bool hashtagPersonality;
  @JsonKey(name: Key_hashtag_healthy)
  bool hashtagHealthy;
  @JsonKey(name: Key_hashtag_career_2)
  bool hashtagCareer2;
  @JsonKey(name: Key_hashtag_education)
  bool hashtagEducation;
  @JsonKey(name: Key_hashtag_career_1)
  bool hashtagCareer1;
  @JsonKey(name: Key_hashtag_graduated)
  bool hashtagGraduated;
  @JsonKey(name: Key_hashtag_babysiter)
  bool hashtagBabysiter;
  @JsonKey(name: Key_auth_id)
  int authId;

  Careinfo({
    this.totalCaretime = 0,
    this.servicetype,
    this.gradeLinkmom = '',
    this.gradeAuthlevel = 0,
    this.hashtagCctv = false,
    this.hashtagAnmal = false,
    this.hashtagCodivVaccine = false,
    this.hashtagDeungbon = false,
    this.hashtagCriminal = false,
    this.hashtagPersonality = false,
    this.hashtagHealthy = false,
    this.hashtagCareer2 = false,
    this.hashtagEducation = false,
    this.hashtagCareer1 = false,
    this.hashtagGraduated = false,
    this.hashtagBabysiter = false,
    this.authId = 0,
  });

  factory Careinfo.fromJson(Map<String, dynamic> json) {
    return Careinfo(
      totalCaretime: json[Key_total_caretime] as int,
      servicetype: json[Key_servicetype] != null ? json[Key_servicetype].replaceAll(' ', '').split(',').toList() : [],
      gradeLinkmom: json[Key_grade_linkmom] ?? '',
      gradeAuthlevel: json[Key_grade_authlevel] as int,
      hashtagCctv: json[Key_hashtag_cctv] ?? false,
      hashtagAnmal: json[Key_hashtag_anmal] ?? false,
      hashtagCodivVaccine: json[Key_hashtag_codiv_vaccine] ?? false,
      hashtagDeungbon: json[Key_hashtag_deungbon] ?? false,
      hashtagCriminal: json[Key_hashtag_criminal] ?? false,
      hashtagPersonality: json[Key_hashtag_personality] ?? false,
      hashtagHealthy: json[Key_hashtag_healthy] ?? false,
      hashtagCareer2: json[Key_hashtag_career_2] ?? false,
      hashtagEducation: json[Key_hashtag_education] ?? false,
      hashtagCareer1: json[Key_hashtag_career_1] ?? false,
      hashtagGraduated: json[Key_hashtag_graduated] ?? false,
      hashtagBabysiter: json[Key_hashtag_babysiter] ?? false,
      authId: json[Key_auth_id] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_total_caretime] = this.totalCaretime;
    data[Key_servicetype] = this.servicetype;
    data[Key_grade_linkmom] = this.gradeLinkmom;
    data[Key_grade_authlevel] = this.gradeAuthlevel;
    data[Key_hashtag_cctv] = this.hashtagCctv;
    data[Key_hashtag_anmal] = this.hashtagAnmal;
    data[Key_hashtag_codiv_vaccine] = this.hashtagCodivVaccine;
    data[Key_hashtag_deungbon] = this.hashtagDeungbon;
    data[Key_hashtag_criminal] = this.hashtagCriminal;
    data[Key_hashtag_personality] = this.hashtagPersonality;
    data[Key_hashtag_healthy] = this.hashtagHealthy;
    data[Key_hashtag_career_2] = this.hashtagCareer2;
    data[Key_hashtag_education] = this.hashtagEducation;
    data[Key_hashtag_career_1] = this.hashtagCareer1;
    data[Key_hashtag_graduated] = this.hashtagGraduated;
    data[Key_hashtag_babysiter] = this.hashtagBabysiter;
    data[Key_auth_id] = this.authId;
    return data;
  }

  @override
  String toString() {
    return 'Careinfo{totalCaretime: $totalCaretime, servicetype: $servicetype, gradeLinkmom: $gradeLinkmom, gradeAuthlevel: $gradeAuthlevel, hashtagCctv: $hashtagCctv, hashtagAnmal: $hashtagAnmal, hashtagCodivVaccine: $hashtagCodivVaccine, hashtagDeungbon: $hashtagDeungbon, hashtagCriminal: $hashtagCriminal, hashtagPersonality: $hashtagPersonality, hashtagHealthy: $hashtagHealthy, hashtagCareer2: $hashtagCareer2, hashtagEducation: $hashtagEducation, hashtagCareer1: $hashtagCareer1, hashtagGraduated: $hashtagGraduated, hashtagBabysiter: $hashtagBabysiter: authId: $authId}';
  }
}

class RankReviewInfo {
  static const String Key_review_text = 'review_text';
  static const String Key_count = 'count';
  static const String Key_review = 'review';

  @JsonKey(name: Key_review_text)
  String reviewText;
  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_review)
  int review;

  RankReviewInfo({this.reviewText = '', this.count = 0, this.review = 0});

  factory RankReviewInfo.fromJson(Map<String, dynamic> json) {
    return RankReviewInfo(reviewText: json[Key_review_text] ?? '', count: json[Key_count] as int, review: json[Key_review] as int);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_review_text] = this.reviewText;
    data[Key_count] = this.count;
    data[Key_review] = this.review;
    return data;
  }

  @override
  String toString() {
    return 'RankReviewInfo{reviewText: $reviewText, count: $count, review: $review}';
  }
}
