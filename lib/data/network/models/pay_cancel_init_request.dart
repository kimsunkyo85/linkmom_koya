import 'package:json_annotation/json_annotation.dart';

///- All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///- Created by   : platformbiz@ggumbi.com
///- version      : 1.0.0
///- see          : pay_cancel_init_request.dart - 결제취소 요청전 POST
///- since        : 2021/09/08 / update:
///
///> (0, '계약중').  (11, '계약실패(거절/취소/응답없음/시간만료 등)'),<br/>
/// (21, '계약완료 후 취소(부분)'),<br/>
/// (22, '계약완료 후 취소(전체)'),<br/>
/// (92, '매칭성공(계약완료/결제대기중)'),<br/>
/// (100, '결제완료')<br/>
/// "msg_cd": 5400, "message": "취소 할 수 없는 결제 입니다.",<br/>
/// "msg_cd": 5401, "message": "이미 취소된 결제 입니다.",<br/>
/// "msg_cd": 4000, "message": "데이터가 없습니다.",<br/>
@JsonSerializable()
class PayCancelInitRequest {
  static const String Key_order_id = 'order_id';
  static const String Key_contract_id = 'contract_id';
  static const String Key_cancelreason = 'cancelreason';
  static const String Key_cancelreason_msg = 'cancelreason_msg';

  PayCancelInitRequest({
    this.order_id = '',
    this.contract_id = 0,
    this.cancelreason = 0,
    this.cancelreason_msg = '',
  });

  PayCancelInitRequest.clone(PayCancelInitRequest item)
      : this(
          order_id: item.order_id,
          contract_id: item.contract_id,
          cancelreason: item.cancelreason,
          cancelreason_msg: item.cancelreason_msg,
        );

  ///주문번호
  @JsonKey(name: Key_order_id)
  final String? order_id;

  ///계약서 번호
  @JsonKey(name: Key_contract_id)
  final int? contract_id;

  ///취소사유 No<br/>
  /// <맘대디측 사유><br/>
  /// (10, '[맘대디 사유] 계약중, 거절하기'),<br/>
  /// (11, '[맘대디 사유] 돌봄 필요한 일정이 바뀌었어요.'),<br/>
  /// (12, '[맘대디 사유] 링크쌤과 돌봄성향이 맞지 않아요.'),<br/>
  /// (13, '[맘대디 사유] 질병, 사고 등으로 인해 서비스 이용이 어려워요.'),<br/>
  /// (14, '[맘대디 사유] 더 이상 돌봄이 필요 없어 졌어요.'),<br/>
  /// (15, '[맘대디 사유] 맘대디가 돌봄 당일 미리 연락없이 아이를 맡기지 않았어요.'),<br/>
  /// (16, '[맘대디 사유] 맘대디가 취소를 요청해 왔어요.'),<br/>
  /// (19, '[맘대디 사유] 기타')<br/>
  /// <링크쌤측 사유><br/>
  /// (20, '[링크쌤 사유] 계약중, 거절하기'),<br/>
  /// (21, '[링크쌤 사유] 돌봄 활동 가능한 일정이 바뀌었어요.'),<br/>
  /// (22, '[링크쌤 사유] 맘대디와 돌봄성향이 맞지 않아요.'),<br/>
  /// (23, '[링크쌤 사유] 질병, 사고 등으로 인해 서비스 제공이 어려워요.'),<br/>
  /// (24, '[링크쌤 사유] 당분간 돌봄 활동을 하지 않을 생각이에요.')<br/>
  /// (25, '[링크쌤 사유] 링크쌤이 돌봄 당일 연락없이 출근하지 않았어요.'),<br/>
  /// (26, '[링크쌤 사유] 링크쌤이 취소를 요청해 왔어요.'),<br/>
  /// (29, '[링크쌤 사유] 기타')<br/>
  @JsonKey(name: Key_cancelreason)
  int? cancelreason;

  ///취소사유 내용
  @JsonKey(name: Key_cancelreason_msg)
  String? cancelreason_msg;

  factory PayCancelInitRequest.fromJson(Map<String, dynamic> json) {
    return PayCancelInitRequest(
      order_id: json[Key_order_id] ?? '',
      contract_id: json[Key_contract_id] as int,
      cancelreason: json[Key_cancelreason] as int,
      cancelreason_msg: json[Key_cancelreason_msg] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_order_id] = this.order_id;
    data[Key_contract_id] = this.contract_id;
    data[Key_cancelreason] = this.cancelreason;
    data[Key_cancelreason_msg] = this.cancelreason_msg;
    return data;
  }

  @override
  String toString() {
    return 'PayCancelInitRequest{order_id: $order_id, contract_id: $contract_id, cancelreason: $cancelreason, cancelreason_msg: $cancelreason_msg}';
  }
}
