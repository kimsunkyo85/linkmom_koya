import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : area_address_request.dart - 동네 인증 여부 검사
/// since        : 5/7/21 / update:
@JsonSerializable()
class AreaGpsAuthRequest {
  static const String Key_bcode = 'bcode';
  static const String Key_hcode = 'hcode';
  static const String Key_region_1depth = 'region_1depth';
  static const String Key_region_2depth = 'region_2depth';
  static const String Key_region_3depth = 'region_3depth';
  static const String Key_region_3depth_h = 'region_3depth_h';
  static const String Key_latitude = 'latitude';
  static const String Key_longitude = 'longitude';

  AreaGpsAuthRequest({
    this.bcode = '',
    this.hcode = '',
    this.region_1depth = '',
    this.region_2depth = '',
    this.region_3depth = '',
    this.region_3depth_h = '',
    this.latitude = '',
    this.longitude = '',
  });

  ///(선택된) 법정동코드
  @JsonKey(name: Key_bcode)
  final String bcode;

  ///(선택된) 행정동코드
  @JsonKey(name: Key_hcode)
  final String hcode;

  ///(선택된) 위치 Depth1
  @JsonKey(name: Key_region_1depth)
  final String region_1depth;

  ///(선택된) 위치 Depth2
  @JsonKey(name: Key_region_2depth)
  final String region_2depth;

  ///(선택된) 위치 Depth3
  @JsonKey(name: Key_region_3depth)
  final String region_3depth;

  ///(선택된) 위치 Depth4
  @JsonKey(name: Key_region_3depth_h)
  final String region_3depth_h;

  ///(GPS) 위도
  @JsonKey(name: Key_latitude)
  final String latitude;

  ///(GPS) 경도
  @JsonKey(name: Key_longitude)
  final String longitude;

  factory AreaGpsAuthRequest.fromJson(Map<String, dynamic> json) {
    return AreaGpsAuthRequest(
      bcode: json[Key_bcode] ?? '',
      hcode: json[Key_hcode] ?? '',
      region_1depth: json[Key_region_1depth] ?? '',
      region_2depth: json[Key_region_2depth] ?? '',
      region_3depth: json[Key_region_3depth] ?? '',
      region_3depth_h: json[Key_region_3depth_h] ?? '',
      latitude: json[Key_latitude] ?? '',
      longitude: json[Key_longitude] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bcode] = this.bcode;
    data[Key_hcode] = this.hcode;
    data[Key_region_1depth] = this.region_1depth;
    data[Key_region_2depth] = this.region_2depth;
    data[Key_region_3depth] = this.region_3depth;
    data[Key_region_3depth_h] = this.region_3depth_h;
    data[Key_latitude] = this.latitude;
    data[Key_longitude] = this.longitude;
    return data;
  }

  @override
  String toString() {
    return 'AreaGpsAuthRequest{bcode: $bcode, hcode: $hcode, region_1depth: $region_1depth, region_2depth: $region_2depth, region_3depth: $region_3depth, region_3depth_h: $region_3depth_h, latitude: $latitude, longitude: $longitude}';
  }
}
