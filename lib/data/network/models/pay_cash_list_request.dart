import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///
/// Created by   : platformbiz@ggumbi.com
///
/// version      : 1.0.0
///
/// see          : pay_cash_list_request.dart - 캐시리스트 POST
///
/// since        : 2021/09/08 / update:
///
///(10, '캐시소득(+)'),
///
///(11, '돌봄취소(환불)(+)'),
///
///(20, '돌봄요금결제(-)'),
///
///(21, '캐시인출(-)'),
///
///(99, '캐시소멸(-)')

@JsonSerializable()
class PayCashListRequest {
  static const String Key_s_date = 's_date';
  static const String Key_e_date = 'e_date';
  static const String Key_cash_flag = 'cash_flag';

  PayCashListRequest({
    this.s_date,
    this.e_date,
    this.cash_flag,
  });

  ///결제조회 시작일자 (YYYYMMDD)
  @JsonKey(name: Key_s_date)
  final String? s_date;

  ///결제조회 종료일자 (YYYYMMDD)
  @JsonKey(name: Key_e_date)
  final String? e_date;

  ///캐시조회 구분 (전체조회 NULL)
  @JsonKey(name: Key_cash_flag)
  final List<int>? cash_flag;

  factory PayCashListRequest.fromJson(Map<String, dynamic> json) {
    List<int> cashFlag = json[Key_cash_flag] == null ? [] : List<int>.from(json[Key_cash_flag]);
    return PayCashListRequest(
      s_date: json[Key_s_date] ?? '',
      e_date: json[Key_e_date] ?? '',
      cash_flag: cashFlag,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_s_date] = this.s_date;
    data[Key_e_date] = this.e_date;
    data[Key_cash_flag] = this.cash_flag;
    return data;
  }

  Future<FormData> toFormData() async {
    FormData formData = FormData();
    if (s_date != null) {
      formData.fields.add(MapEntry(Key_s_date, s_date.toString()));
    }
    if (e_date != null) {
      formData.fields.add(MapEntry(Key_e_date, e_date.toString()));
    }
    if (cash_flag != null) {
      cash_flag!.forEach((value) {
        formData.fields.add(MapEntry(Key_cash_flag, value.toString()));
      });
    }
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.files},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'PayCashListRequest{s_date: $s_date, e_date: $e_date, cash_flag: $cash_flag}';
  }
}
