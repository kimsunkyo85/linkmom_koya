import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CsGuideRequest {
  static const String Key_gubun = 'gubun';

  @JsonKey(name: Key_gubun)
  int gubun;

  CsGuideRequest({this.gubun = 0});

  factory CsGuideRequest.fromJson(Map<String, dynamic> json) {
    return CsGuideRequest(
      gubun: json[Key_gubun],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_gubun] = this.gubun;
    return data;
  }

  @override
  String toString() {
    return 'CsGuideRequest{gubun: $gubun}';
  }
}
