
import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

@JsonSerializable()
class AuthcenterImgSaveResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ImgSaveData>? dataList;

  AuthcenterImgSaveResponse({
    this.dataHeader,
    this.dataList,
  });

  ImgSaveData getData() => dataList!.first;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory AuthcenterImgSaveResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ImgSaveData> datas = [];
    try {
      datas = json[Key_data].map<ImgSaveData>((i) => ImgSaveData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return AuthcenterImgSaveResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AuthcenterImgSaveResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class ImgSaveData {
  static const String Key_id = 'id';
  static const String Key_auth_imgtype = 'auth_imgtype';
  static const String Key_createdate = 'createdate';

  ImgSaveData({
    this.id = 0, this.auth_imgtype = 0, this.createdate = ''
  });

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_auth_imgtype)
  int auth_imgtype;
  @JsonKey(name: Key_createdate)
  String createdate;

  factory ImgSaveData.fromJson(Map<String, dynamic> json) {
    return ImgSaveData(
      id: json[Key_id] ?? 0,
      auth_imgtype: json[Key_auth_imgtype] ?? 0,
      createdate: json[Key_createdate] ?? '',
    );
  }
}
