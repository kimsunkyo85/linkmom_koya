import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : momdady_mycares_status_update_request.dart - 구인 종료 요청
/// since        : 2021/06/30 / update:
@JsonSerializable()
class MomdadyMyCaresStatusUpdateRequest {
  static const String Key_booking_id = 'booking_id';
  static const String Key_status = 'status';

  MomdadyMyCaresStatusUpdateRequest({this.booking_id = 0, this.status = 0});

  @JsonKey(name: Key_booking_id)
  int booking_id;

  @JsonKey(name: Key_status)
  int status;

  factory MomdadyMyCaresStatusUpdateRequest.fromJson(Map<String, dynamic> json) {
    return MomdadyMyCaresStatusUpdateRequest(
      booking_id: json[Key_booking_id] as int,
      status: json[Key_status] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_booking_id] = this.booking_id;
    data[Key_status] = this.status;
    return data;
  }

  @override
  String toString() {
    return 'MomdadyMyCaresStatusUpdateRequest{booking_id: $booking_id, status: $status}';
  }
}
