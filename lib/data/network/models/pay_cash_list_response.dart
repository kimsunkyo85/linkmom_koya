import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/page_data.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///
/// Created by   : platformbiz@ggumbi.com
///
/// version      : 1.0.0
///
/// see          : pay_pay_list_response.dart - 결제리스트 POST
///
/// since        : 2021/09/09 / update:
///
/// (10, '캐시소득(+)'),
///
/// (11, '돌봄취소(환불)(+)'),
///
/// (20, '돌봄요금결제(-)'),
///
/// (21, '캐시인출(-)'),
///
/// (99, '캐시소멸(-)')
@JsonSerializable()
class PayCashListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PageData>? dataList;

  PayCashListResponse({
    this.dataHeader,
    this.dataList,
  });

  PageData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PageData(results: []);

  List<PageData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory PayCashListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PageData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<PageData>((i) => PageData.fromJson(i, '$PayCashListResponse')).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return PayCashListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'PayPayListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class PayCashListData {
  static const String Key_cash_flag = 'cash_flag';
  static const String Key_contract_id = 'contract_id';
  static const String Key_used_cash = 'used_cash';
  static const String Key_cash_reason = 'cash_reason';
  static const String Key_cash_expiredate = 'cash_expiredate';
  static const String Key_createdate = 'createdate';

  PayCashListData({
    this.cash_flag,
    this.contract_id,
    this.used_cash,
    this.cash_reason,
    this.cash_expiredate,
    this.createdate,
  });

  @JsonKey(name: Key_cash_flag)
  final int? cash_flag;
  @JsonKey(name: Key_contract_id)
  final int? contract_id;
  @JsonKey(name: Key_used_cash)
  final int? used_cash;
  @JsonKey(name: Key_cash_reason)
  final String? cash_reason;
  @JsonKey(name: Key_cash_expiredate)
  final String? cash_expiredate;
  @JsonKey(name: Key_createdate)
  final String? createdate;

  factory PayCashListData.fromJson(Map<String, dynamic> json) {
    return PayCashListData(
      cash_flag: json[Key_cash_flag] as int,
      contract_id: json[Key_contract_id] as int,
      used_cash: json[Key_used_cash] as int,
      cash_reason: json[Key_cash_reason] ?? '',
      cash_expiredate: json[Key_cash_expiredate] ?? '',
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_cash_flag] = this.cash_flag;
    data[Key_contract_id] = this.contract_id;
    data[Key_used_cash] = this.used_cash;
    data[Key_cash_reason] = this.cash_reason;
    data[Key_cash_expiredate] = this.cash_expiredate;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'PayCashListData{cash_flag: $cash_flag, contract_id: $contract_id, used_cash: $used_cash, cash_reason: $cash_reason, cash_expiredate: $cash_expiredate, createdate: $createdate}';
  }
}
