import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/main.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : matching_init_request.dart - 매칭요청 및 수락하기 (계약서 생성 & 싸인) 조회
/// since        : 2021/08/03 / update:
@JsonSerializable()
class MatchingInitRequest {
  ///돌봄진행 상황 (매칭된 내용)
  static const String Key_receiver = 'receiver';

  ///돌봄신청서ID (booking_id)
  static const String Key_bookingcareservices = 'bookingcareservices';

  ///매칭ID (matching_id) - 있는 경우만, 없으면 Null
  static const String Key_carematching = 'carematching';

  ///싸인 정보
  static const String Key_signature = 'signature';

  ///채팅방번호 (chatroom_id)
  static const String Key_chattingroom = 'chattingroom';

  ///링크쌤 주소
  static const String Key_linkmom_address = 'linkmom_address';

  ///링크쌤 주소 상세
  static const String Key_linkmom_address_detail = 'linkmom_address_detail';

  MatchingInitRequest({
    this.receiver = 0,
    this.bookingcareservices = 0,
    this.carematching = 0,
    this.signature,
    this.chattingroom = 0,
    this.matchingStatus = 0,
    this.linkmom_address = '',
    this.linkmom_address_detail = '',
  });

  ///돌봄진행 상황 (매칭된 내용)
  @JsonKey(name: Key_receiver)
  final int? receiver;

  ///돌봄신청서ID (booking_id)
  @JsonKey(name: Key_bookingcareservices)
  final int? bookingcareservices;

  ///매칭ID (matching_id) - 있는 경우만, 없으면 Null
  @JsonKey(name: Key_carematching)
  final int? carematching;

  ///싸인 정보
  @JsonKey(name: Key_signature)
  final File? signature;

  ///채팅방번호 (chatroom_id)
  @JsonKey(name: Key_chattingroom)
  final int? chattingroom;

  ///링크쌤 주소
  @JsonKey(name: Key_linkmom_address)
  final String? linkmom_address;

  ///링크쌤 주소 상세
  @JsonKey(name: Key_linkmom_address_detail)
  final String? linkmom_address_detail;

  ///맘대디프로필에서만 사용.
  int matchingStatus = -1;

  factory MatchingInitRequest.fromJson(Map<String, dynamic> json) {
    return MatchingInitRequest(
      receiver: json[Key_receiver] as int,
      bookingcareservices: json[Key_bookingcareservices] as int,
      carematching: json[Key_carematching] as int,
      signature: json[Key_signature],
      chattingroom: json[Key_chattingroom] as int,
      linkmom_address: json[Key_linkmom_address] ?? '',
      linkmom_address_detail: json[Key_linkmom_address_detail] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_receiver] = this.receiver;
    data[Key_bookingcareservices] = this.bookingcareservices;
    data[Key_carematching] = this.carematching;
    data[Key_signature] = this.signature;
    data[Key_chattingroom] = this.chattingroom;
    data[Key_linkmom_address] = this.linkmom_address;
    data[Key_linkmom_address_detail] = this.linkmom_address_detail;
    return data;
  }

  Future<FormData> toFormData() async {
    FormData formData = FormData();
    if (receiver != null) {
      formData.fields.add(MapEntry(Key_receiver, receiver.toString()));
    }
    if (bookingcareservices != null) {
      formData.fields.add(MapEntry(Key_bookingcareservices, bookingcareservices.toString()));
    }
    if (carematching != null) {
      formData.fields.add(MapEntry(Key_carematching, carematching.toString()));
    }
    if (signature != null) {
      String fileName = signature!.path.split('/').last;
      log.d('『GGUMBI』>>> setEncryptFormDataFiles : fileName: $fileName,  <<< ');
      formData.files.add(MapEntry(Key_signature, await MultipartFile.fromFile(signature!.path, filename: fileName, contentType: MediaType('image', 'jpeg'))));
    }
    if (chattingroom != null) {
      formData.fields.add(MapEntry(Key_chattingroom, chattingroom.toString()));
    }
    if (linkmom_address != null) {
      formData.fields.add(MapEntry(Key_linkmom_address, linkmom_address.toString()));
    }
    if (linkmom_address_detail != null) {
      formData.fields.add(MapEntry(Key_linkmom_address_detail, linkmom_address_detail.toString()));
    }
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'MatchingInitRequest{receiver: $receiver, bookingcareservices: $bookingcareservices, carematching: $carematching, signature: $signature, chattingroom: $chattingroom, linkmom_address: $linkmom_address, linkmom_address_detail: $linkmom_address_detail, matchingStatus: $matchingStatus}';
  }
}
