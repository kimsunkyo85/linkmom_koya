import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CommunityEventReplySaveRequest {
  static const String Key_event_id = 'event_id';
  static const String Key_reply = 'reply';
  static const String Key_parent = 'parent';

  @JsonKey(name: Key_event_id)
  int eventId;
  @JsonKey(name: Key_reply)
  String reply;
  @JsonKey(name: Key_parent)
  int? parent;

  CommunityEventReplySaveRequest({this.eventId = 0, this.reply = '', this.parent});

  factory CommunityEventReplySaveRequest.fromJson(Map<String, dynamic> json) {
    return CommunityEventReplySaveRequest(
      eventId: json[Key_event_id] ?? 0,
      reply: json[Key_reply] ?? 0,
      parent: json[Key_parent] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_event_id] = this.eventId;
    data[Key_reply] = this.reply;
    if (this.parent != 0) data[Key_parent] = this.parent;
    return data;
  }

  @override
  String toString() {
    return 'CommunityEventReplySaveRequest{eventId: $eventId, reply: $reply, parent: $parent}';
  }
}
