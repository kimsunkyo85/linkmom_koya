import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';
import 'header_response.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : child_drug_response.dart - 투약 의뢰서 응답
/// since        : 5/20/21 / update:

@JsonSerializable()
class ChildDrugSaveResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ChildDrugReponseData>? dataList;

  ChildDrugSaveResponse({
    this.dataHeader,
    this.dataList,
  });

  ChildDrugReponseData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ChildDrugReponseData();

  List<ChildDrugReponseData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ChildDrugSaveResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ChildDrugReponseData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<ChildDrugReponseData>((i) => ChildDrugReponseData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return ChildDrugSaveResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AuthCenterHomeResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class ChildDrugReponseData {
  static const String Key_id = 'id';
  static const String Key_child = 'child';
  static const String Key_child_name = 'child_name';
  static const String Key_drug_date = 'drug_date';
  static const String Key_user_signature = 'user_signature';
  static const String Key_druglists = 'druglists';
  static const String Key_createdate = 'createdate';

  ChildDrugReponseData({
    this.id = 0,
    this.child = 0,
    this.child_name = '',
    this.drug_date = '',
    this.user_signature = '',
    this.druglists,
    this.createdate = '',
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_child)
  final int child;
  @JsonKey(name: Key_child_name)
  final String child_name;
  @JsonKey(name: Key_drug_date)
  final String drug_date;
  @JsonKey(name: Key_user_signature)
  final String user_signature;
  @JsonKey(name: Key_druglists)
  final List<ChildDrugResData>? druglists;
  @JsonKey(name: Key_createdate)
  final String createdate;

  factory ChildDrugReponseData.fromJson(Map<String, dynamic> json) {
    List<ChildDrugResData> drugLists = json[Key_druglists].map<ChildDrugResData>((i) => ChildDrugResData.fromJson(i)).toList();
    return ChildDrugReponseData(
      id: json[Key_id] as int,
      child: json[Key_child] as int,
      child_name: json[Key_child_name] ?? '',
      drug_date: json[Key_drug_date],
      user_signature: json[Key_user_signature] ?? '',
      druglists: drugLists,
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_child] = this.child;
    data[Key_child_name] = this.child_name;
    data[Key_drug_date] = this.drug_date;
    data[Key_user_signature] = this.user_signature;
    data[Key_druglists] = this.druglists;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'ChildDrugData{id: $id, child: $child, child_name: $child_name, drug_date: $drug_date, user_signature: $user_signature, druglists: $druglists}';
  }
}

class ChildDrugResData {
  static const String Key_drug_id = 'drug_id';
  static const String Key_symptom = 'symptom';
  static const String Key_drug_time = 'drug_time';
  static const String Key_drug_timetype = 'drug_timetype';
  static const String Key_drug_type = 'drug_type';
  static const String Key_drug_amount_type0 = 'drug_amount_type0';
  static const String Key_drug_amount_type1 = 'drug_amount_type1';
  static const String Key_drug_amount_type2 = 'drug_amount_type2';
  static const String Key_drug_amount_type3 = 'drug_amount_type3';
  static const String Key_drug_repeat = 'drug_repeat';
  static const String Key_drug_keeping = 'drug_keeping';
  static const String Key_drug_reqmessage = 'drug_reqmessage';
  static const String Key_createdate = 'createdate';
  static const String Key_is_drug = 'is_drug';

  ChildDrugResData({
    this.drug_id = 0,
    this.symptom = '',
    this.drug_time = '',
    this.drug_timetype = 0,
    this.drug_type,
    this.drug_amount_type0 = '',
    this.drug_amount_type1 = '',
    this.drug_amount_type2 = '',
    this.drug_amount_type3 = '',
    this.drug_repeat = '',
    this.drug_keeping = 0,
    this.drug_reqmessage = '',
    this.createdate = '',
    this.is_drug = false,
  });

  @JsonKey(name: Key_drug_id)
  int drug_id;
  @JsonKey(name: Key_symptom)
  String symptom;
  @JsonKey(name: Key_drug_time)
  String drug_time;
  @JsonKey(name: Key_drug_timetype)
  int drug_timetype;
  @JsonKey(name: Key_drug_type)
  List<int>? drug_type;
  @JsonKey(name: Key_drug_amount_type0)
  String drug_amount_type0;
  @JsonKey(name: Key_drug_amount_type1)
  String drug_amount_type1;
  @JsonKey(name: Key_drug_amount_type2)
  String drug_amount_type2;
  @JsonKey(name: Key_drug_amount_type3)
  String drug_amount_type3;
  @JsonKey(name: Key_drug_repeat)
  String drug_repeat;
  @JsonKey(name: Key_drug_keeping)
  int drug_keeping;
  @JsonKey(name: Key_drug_reqmessage)
  String drug_reqmessage;
  @JsonKey(name: Key_createdate)
  String createdate;
  @JsonKey(name: Key_is_drug)
  bool is_drug;


  factory ChildDrugResData.fromJson(Map<String, dynamic> json) {
    List<int> drugType = List<int>.from(json[Key_drug_type].map((e) => int.parse(e))).toList();
    return ChildDrugResData(
      drug_id: json[Key_drug_id] as int,
      symptom: json[Key_symptom] ?? '',
      drug_time: json[Key_drug_time] ?? '',
      drug_type: drugType,
      drug_timetype: json[Key_drug_timetype] as int,
      drug_amount_type0: json[Key_drug_amount_type0] ?? '',
      drug_amount_type1: json[Key_drug_amount_type1] ?? '',
      drug_amount_type2: json[Key_drug_amount_type2] ?? '',
      drug_amount_type3: json[Key_drug_amount_type3] ?? '',
      drug_repeat: json[Key_drug_repeat] ?? '',
      drug_keeping: json[Key_drug_keeping] as int,
      drug_reqmessage: json[Key_drug_reqmessage] ?? '',
      createdate: json[Key_createdate] ?? '',
      is_drug: json[Key_is_drug] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_drug_id] = this.drug_id;
    data[Key_symptom] = this.symptom;
    data[Key_drug_time] = this.drug_time;
    data[Key_drug_timetype] = this.drug_timetype;
    data[Key_drug_type] = this.drug_type;
    data[Key_drug_amount_type0] = this.drug_amount_type0;
    data[Key_drug_amount_type1] = this.drug_amount_type1;
    data[Key_drug_amount_type2] = this.drug_amount_type2;
    data[Key_drug_amount_type3] = this.drug_amount_type3;
    data[Key_drug_repeat] = this.drug_repeat;
    data[Key_drug_keeping] = this.drug_keeping;
    data[Key_drug_reqmessage] = this.drug_reqmessage;
    data[Key_createdate] = this.createdate;
    data[Key_is_drug] = this.is_drug;
    return data;
  }

  @override
  String toString() {
    return 'ChildDrugResData{drug_id: $drug_id, symptom: $symptom, drug_time: $drug_time, drug_timetype: $drug_timetype, drug_type: $drug_type, drug_amount_type0: $drug_amount_type0, drug_amount_type1: $drug_amount_type1, drug_amount_type2: $drug_amount_type2, drug_amount_type3: $drug_amount_type3, drug_repeat: $drug_repeat, drug_keeping: $drug_keeping, drug_reqmessage: $drug_reqmessage, createdate: $createdate, is_drug: $is_drug}';
  }
}
