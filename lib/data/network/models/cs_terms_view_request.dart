import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CsTermsViewRequest {
  static const String Key_id = 'id';

  @JsonKey(name: Key_id)
  int id;

  CsTermsViewRequest({this.id = 0});

  factory CsTermsViewRequest.fromJson(Map<String, dynamic> json) {
    return CsTermsViewRequest(
      id: json[Key_id],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    return data;
  }

  @override
  String toString() {
    return 'CsTermsViewRequest{id: $id}';
  }
}
