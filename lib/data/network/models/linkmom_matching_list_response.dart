import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/data/page_data.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class LinkmomMatchingListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PageData>? dataList;

  LinkmomMatchingListResponse({
    this.dataHeader,
    this.dataList,
  });

  PageData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PageData(results: []);

  List<PageData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  bool isData() => dataList!.isNotEmpty;

  factory LinkmomMatchingListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PageData> datas = [];
    try {
      datas = json[Key_data].map<PageData>((i) => PageData.fromJson(i, '$LinkmomMatchingListResponse')).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return LinkmomMatchingListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'LinkmomMatchingListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
