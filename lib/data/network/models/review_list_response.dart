import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/data/network/models/review_view_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class ReviewListResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ReviewListData>? dataList;

  ReviewListResponse({
    this.dataHeader,
    this.dataList,
  });

  ReviewListData getData() => dataList != null && dataList!.length != 0 ? dataList!.first : ReviewListData();

  List<ReviewListData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ReviewListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ReviewListData> datas = [];
    try {
      datas = json[Key_data].map<ReviewListData>((i) => ReviewListData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return ReviewListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }
}

@JsonSerializable()
class ReviewListData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  ReviewListData({this.count = 0, this.next = '', this.previous = '', this.results});

  @JsonKey(name: Key_count)
  int count = 0;
  @JsonKey(name: Key_next)
  String next = '';
  @JsonKey(name: Key_previous)
  String previous = '';
  @JsonKey(name: Key_results)
  List<ReviewResultsData>? results = [];

  factory ReviewListData.fromJson(Map<String, dynamic> json) {
    List<ReviewResultsData> datas = [];
    try {
      datas = json[Key_results].map<ReviewResultsData>((i) => ReviewResultsData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return ReviewListData(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: datas,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results;
    return data;
  }

  @override
  String toString() {
    return 'ReviewListData{ $Key_count:$count $Key_next:$next $Key_previous:$previous $Key_results:${results.toString()}}';
  }
}

@JsonSerializable()
class ReviewResultsData {
  static const String Key_matching = 'matching';
  static const String Key_momdady_name = 'momdady_name';
  static const String Key_linkmom_name = 'linkmom_name';
  static const String Key_profileimg = 'profileimg';
  static const String Key_childinfo = 'childinfo';
  static const String Key_carescheduleinfo = 'carescheduleinfo';
  static const String Key_reviewinfo = 'reviewinfo';

  ReviewResultsData(
      {this.matching = 0, this.momdady_name = '', this.linkmom_name = '', this.profileimg = '', this.childinfo = '',
      this.carescheduleinfo,
      this.reviewinfo});

  @JsonKey(name: Key_matching)
  int matching;
  @JsonKey(name: Key_momdady_name)
  String momdady_name;
  @JsonKey(name: Key_linkmom_name)
  String linkmom_name;
  @JsonKey(name: Key_profileimg)
  String profileimg;
  @JsonKey(name: Key_childinfo)
  String childinfo;
  @JsonKey(name: Key_carescheduleinfo)
  CareScheduleInfo? carescheduleinfo;
  @JsonKey(name: Key_reviewinfo)
  List<ReviewInfo>? reviewinfo;

  factory ReviewResultsData.fromJson(Map<String, dynamic> json) {
    CareScheduleInfo datas = CareScheduleInfo();
    List<ReviewInfo> reivews = [];
    try {
      datas = CareScheduleInfo.fromJson(json[Key_carescheduleinfo]);
      // reivews = json[Key_reviewinfo].map<ReviewInfo>((i) => ReviewInfo.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return ReviewResultsData(
      matching: json[Key_matching] as int,
      momdady_name: json[Key_momdady_name] ?? '',
      linkmom_name: json[Key_linkmom_name] ?? '',
      profileimg: json[Key_profileimg] ?? '',
      childinfo: json[Key_childinfo] ?? '',
      carescheduleinfo: datas,
      reviewinfo: reivews,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_matching] = this.matching;
    data[Key_momdady_name] = this.momdady_name;
    data[Key_linkmom_name] = this.linkmom_name;
    data[Key_profileimg] = this.profileimg;
    data[Key_childinfo] = this.childinfo;
    data[Key_carescheduleinfo] = this.carescheduleinfo;
    data[Key_reviewinfo] = this.reviewinfo;
    return data;
  }

  @override
  String toString() {
    return 'ReviewResultsData {$Key_matching:$matching, $Key_momdady_name:$momdady_name, $Key_linkmom_name:$linkmom_name, profileimg: $profileimg, childinfo: $childinfo, carescheduleinfo: $carescheduleinfo, reviewinfo: $reviewinfo}';
  }
}

@JsonSerializable()
class CareScheduleInfo {
  static const String Key_stime = 'stime';
  static const String Key_etime = 'etime';
  static const String Key_caredate = 'caredate';
  static const String Key_servicetype = 'servicetype';
  static const String Key_servicetype_text = 'servicetype_text';
  static const String Key_amount = 'amount';

  CareScheduleInfo({this.stime = '', this.etime = '', this.caredate, this.amount = 0, this.servicetype = 0, this.servicetype_text = ''});

  @JsonKey(name: Key_stime)
  String stime;
  @JsonKey(name: Key_etime)
  String etime;
  @JsonKey(name: Key_caredate)
  List<String>? caredate;
  @JsonKey(name: Key_amount)
  int amount;
  @JsonKey(name: Key_servicetype)
  int servicetype;
  @JsonKey(name: Key_servicetype_text)
  String servicetype_text;

  factory CareScheduleInfo.fromJson(Map<String, dynamic> json) {
    List<String> list = [];
    list = List<String>.from(json[Key_caredate]);
    return CareScheduleInfo(
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      caredate: list,
      amount: json[Key_amount] ?? 0,
      servicetype: json[Key_servicetype] as int,
      servicetype_text: json[Key_servicetype_text] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_caredate] = this.caredate;
    return data;
  }

  @override
  String toString() {
    return 'CareScheduleInfo{stime: $stime, etime: $etime, caredate: $caredate, amount: $amount, servicetype: $servicetype, servicetype_text: $servicetype_text}';
  }
}
