import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///
/// Created by   : platformbiz@ggumbi.com
///
/// version      : 1.0.0
///
/// see          : pay_prev_response.dart - 캐시/포인트 사전결제 응답 PUT
///
/// since        : 2021/09/09 / update: pay/prev/used == back 같이 사용 (캐시/포인트 사용전 결제,취소)
///
///  "msg_cd": 6000, "message": "이미 결제된 내역 입니다.",
///
///  "msg_cd": 6001, "message": "캐시가 부족합니다. 캐시 금액을 확인해주세요.",
///
///  "msg_cd": 6002, "message": "포인트가 부족합니다. 포인트를 확인해주세요.",
///
///  "msg_cd": 3000,"message": "등록에 실패하였습니다.",
@JsonSerializable()
class PayRrevResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PayPrevUsedData>? dataList;

  PayRrevResponse({
    this.dataHeader,
    this.dataList,
  });

  PayPrevUsedData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PayPrevUsedData();

  List<PayPrevUsedData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory PayRrevResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PayPrevUsedData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<PayPrevUsedData>((i) => PayPrevUsedData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return PayRrevResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ChatRoomUpdateResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class PayPrevUsedData {
  static const String Key_order_id = 'order_id';
  static const String Key_used_cash = 'used_cash';
  static const String Key_used_point = 'used_point';
  static const String Key_total_fee = 'total_fee';
  static const String Key_left_fee = 'left_fee';

  PayPrevUsedData({
    this.order_id,
    this.used_cash,
    this.used_point,
    this.total_fee,
    this.left_fee,
  });

  @JsonKey(name: Key_order_id)
  final String? order_id;
  @JsonKey(name: Key_used_cash)
  final int? used_cash;
  @JsonKey(name: Key_used_point)
  final int? used_point;
  @JsonKey(name: Key_total_fee)
  final int? total_fee;
  @JsonKey(name: Key_left_fee)
  final int? left_fee;

  factory PayPrevUsedData.fromJson(Map<String, dynamic> json) {
    return PayPrevUsedData(
      order_id: json[Key_order_id] ?? '',
      used_cash: json[Key_used_cash] ?? 0,
      used_point: json[Key_used_point] ?? 0,
      total_fee: json[Key_total_fee] ?? 0,
      left_fee: json[Key_left_fee] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_order_id] = this.order_id;
    data[Key_used_cash] = this.used_cash;
    data[Key_used_point] = this.used_point;
    data[Key_total_fee] = this.total_fee;
    data[Key_left_fee] = this.left_fee;
    return data;
  }

  @override
  String toString() {
    return 'PayRrevResponse{order_id: $order_id, used_cash: $used_cash, used_point: $used_point, total_fee: $total_fee, left_fee: $left_fee}';
  }
}
