import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class LoginRequest {
  static const String Key_username = "username";
  static const String Key_password = "password";
  static const String Key_is_user_linkmom = "is_user_linkmom";
  static const String Key_is_user_momdady = "is_user_momdady";

  LoginRequest({
    this.username = '',
    this.password = '',
    this.is_user_linkmom = -1,
    this.is_user_momdady = -1,
  });

  @JsonKey(name: Key_username)
  final String username;
  @JsonKey(name: Key_password)
  final String password;
  @JsonKey(name: Key_is_user_linkmom)
  final int is_user_linkmom;
  @JsonKey(name: Key_is_user_momdady)
  final int is_user_momdady;

  factory LoginRequest.fromJson(Map<String, dynamic> json) {
    return LoginRequest(
      username: json[Key_username] ?? '',
      password: json[Key_password] ?? '',
      is_user_linkmom: json[Key_is_user_linkmom] ?? -1,
      is_user_momdady: json[Key_is_user_momdady] ?? -1,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_username] = this.username;
    data[Key_password] = this.password;
    data[Key_is_user_linkmom] = this.is_user_linkmom;
    data[Key_is_user_momdady] = this.is_user_momdady;
    return data;
  }

  @override
  String toString() {
    return 'LoginRequest{username: $username, password: $password, is_user_linkmom: $is_user_linkmom, is_user_momdady: $is_user_momdady}';
  }
}
