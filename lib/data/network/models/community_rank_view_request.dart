import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CommunityRankViewRequest {
  static const String Key_gubun = 'gubun';
  static const String Key_user_id = 'user_id';
  static const String Key_hcode = 'hcode';

  @JsonKey(name: Key_gubun)
  int gubun;
  @JsonKey(name: Key_user_id)
  int user_id;
  @JsonKey(name: Key_hcode)
  int hcode;

  CommunityRankViewRequest({this.gubun = 0, this.user_id = 0, this.hcode = 0});

  factory CommunityRankViewRequest.fromJson(Map<String, dynamic> json) {
    return CommunityRankViewRequest(
      gubun: json[Key_gubun] ?? 0,
      user_id: json[Key_user_id] ?? 0,
      hcode: json[Key_hcode] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_gubun] = this.gubun.toString();
    data[Key_user_id] = this.user_id.toString();
    data[Key_hcode] = this.hcode.toString();
    return data;
  }

  @override
  String toString() {
    return 'CommunityRankViewRequest{gubun: $gubun, user_id: $user_id, hcode: $hcode}';
  }
}
