import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_push_onoff_state_request.dart - 채팅 관련 메시지 push 상태 값
/// since        : 2021/07/16 / update:
@JsonSerializable()
class ChatPushOnOffStateRequest {
  static const String Key_state = 'state';

  ChatPushOnOffStateRequest({
    this.state = '',
  });

  ///PUSH 수신 가능 상태 (ON / OFF)
  @JsonKey(name: Key_state)
  final String state;

  factory ChatPushOnOffStateRequest.fromJson(Map<String, dynamic> json) {
    return ChatPushOnOffStateRequest(
      state: json[Key_state] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_state] = this.state;
    return data;
  }

  @override
  String toString() {
    return 'ChatPushOnoffStateRequest{state: $state}';
  }
}
