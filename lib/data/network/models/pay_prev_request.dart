import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

///#### All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///#### version      : 1.0.0
///#### see          : pay_prev_request.dart - 캐시/포인트 사전결제 요청 PUT
///#### since        : 2021/09/08 / update: pay/prev/used == back 같이 사용 (캐시/포인트 사용전 결제,취소)
///- "msg_cd": 6000, "message": "이미 결제된 내역 입니다.",
///- "msg_cd": 6001, "message": "캐시가 부족합니다. 캐시 금액을 확인해주세요.",
///- "msg_cd": 6002, "message": "포인트가 부족합니다. 포인트를 확인해주세요.",
///- "msg_cd": 3000,"message": "등록에 실패하였습니다.",
@JsonSerializable()
class PayRrevRequest {
  static const String Key_order_id = 'order_id';
  static const String Key_total_fee = 'total_fee';
  static const String Key_used_cash = 'used_cash';
  static const String Key_used_point = 'used_point';

  PayRrevRequest({
    this.order_id = '',
    this.total_fee = 0,
    this.used_cash = 0,
    this.used_point = 0,
  });

  ///주문번호
  @JsonKey(name: Key_order_id)
  final String? order_id;

  ///총 결제해야할 금액
  @JsonKey(name: Key_total_fee)
  final int? total_fee;

  ///사용할 캐시
  @JsonKey(name: Key_used_cash)
  final int? used_cash;

  ///사용할 포인트
  @JsonKey(name: Key_used_point)
  final int? used_point;

  factory PayRrevRequest.fromJson(Map<String, dynamic> json) {
    return PayRrevRequest(
      order_id: json[Key_order_id] ?? '',
      total_fee: json[Key_total_fee] as int,
      used_cash: json[Key_used_cash] as int,
      used_point: json[Key_used_point] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_order_id] = this.order_id;
    data[Key_total_fee] = this.total_fee;
    data[Key_used_cash] = this.used_cash;
    data[Key_used_point] = this.used_point;
    return data;
  }

  FormData toFormData() {
    FormData formData = FormData();
    if (order_id != null) {
      formData.fields.add(MapEntry(Key_order_id, order_id.toString()));
    }
    if (total_fee != null) {
      formData.fields.add(MapEntry(Key_total_fee, total_fee.toString()));
    }
    if (used_cash != null) {
      formData.fields.add(MapEntry(Key_used_cash, used_cash.toString()));
    }
    if (used_point != null) {
      formData.fields.add(MapEntry(Key_used_point, used_point.toString()));
    }
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'PayRrevRequest{order_id: $order_id, total_fee: $total_fee, used_cash: $used_cash, used_point: $used_point}';
  }
}
