import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

import 'data/matching_sign_pay_data.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : matching_pay_response.dart -결제요청 응답
/// since        : 2021/08/03 / update:
@JsonSerializable()
class MatchingPayResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<MatchingSignPayData>? dataList;

  MatchingPayResponse({
    this.dataHeader,
    this.dataList,
  });

  MatchingSignPayData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : MatchingSignPayData();

  List<MatchingSignPayData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  bool isData() => dataList!.isNotEmpty;

  factory MatchingPayResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<MatchingSignPayData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<MatchingSignPayData>((i) => MatchingSignPayData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return MatchingPayResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'MatchingPayResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
