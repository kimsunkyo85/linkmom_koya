import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

import 'penalty_response.dart';

@JsonSerializable()
class PenaltyRemoveResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<Results>? dataList;

  PenaltyRemoveResponse({this.dataHeader, this.dataList});

  Results getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : Results();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory PenaltyRemoveResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<Results> datas = [];
    try {
      datas = json[Key_data].map<Results>((e) => Results.fromJson(e)).toList();
    } catch (e) {
      log.e("EXCEPION >>>>>>>>>>>>>>>>>> $e");
    }
    return PenaltyRemoveResponse(dataHeader: dataHeader, dataList: datas);
  }
}
