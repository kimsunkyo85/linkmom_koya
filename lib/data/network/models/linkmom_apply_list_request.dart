import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : linkmom_apply_list_request.dart - 지원 내역 & 필터링
/// since        : 2021/06/30 / update:
@JsonSerializable()
class LinkmomApplyListRequest {
  ///지원 주체 - 나에게지원한 : byme, 내가지원한 : tome, 둘다 선택 : bymetome
  static const String Key_bywho = 'bywho';

  ///조회 시작일자
  static const String Key_s_date = 's_date';

  ///조회 종료일자
  static const String Key_e_date = 'e_date';

  ///구인상태 (0: 구인중, 1:구인종료)
  static const String Key_status = 'status';

  ///돌봄유형
  static const String Key_servicetype = 'servicetype';

  LinkmomApplyListRequest({
    this.bywho = '',
    this.s_date,
    this.e_date,
    this.status,
    this.servicetype,
  });

  ///지원 주체 - 나에게지원한 : byme - 내가지원한 : tome
  @JsonKey(name: Key_bywho)
  final String bywho;

  ///조회 시작일자
  @JsonKey(name: Key_s_date)
  final String? s_date;

  ///조회 종료일자
  @JsonKey(name: Key_e_date)
  final String? e_date;

  ///구인상태 (0: 구인중, 1:구인종료)
  @JsonKey(name: Key_status)
  final List<int>? status;

  ///돌봄유형
  @JsonKey(name: Key_status)
  final List<int>? servicetype;

  factory LinkmomApplyListRequest.fromJson(Map<String, dynamic> json) {
    List<int> status = List<int>.from(json[Key_status]);
    List<int> servicetype = List<int>.from(json[Key_servicetype]);
    return LinkmomApplyListRequest(
      bywho: json[Key_bywho] ?? '',
      s_date: json[Key_s_date] ?? '',
      e_date: json[Key_e_date] ?? '',
      status: status,
      servicetype: servicetype,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_bywho] = this.bywho;
    if (this.s_date != null) data[Key_s_date] = this.s_date;
    if (this.e_date != null) data[Key_e_date] = this.e_date;
    data[Key_status] = this.status;
    data[Key_servicetype] = this.servicetype;
    return data;
  }

  FormData toFormData(FormData formData) {
    formData.fields.add(MapEntry(Key_bywho, bywho.toString()));
    if (s_date != null) formData.fields.add(MapEntry(Key_s_date, s_date.toString()));
    if (e_date != null) formData.fields.add(MapEntry(Key_e_date, e_date.toString()));
    if (status != null)
      status!.forEach((e) {
      formData.fields.add(MapEntry(Key_status, e.toString()));
    });
    if (servicetype != null)
      servicetype!.forEach((e) {
        formData.fields.add(MapEntry(Key_servicetype, e.toString()));
      });
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'LinkmomApplyListRequest{bywho: $bywho, status: $status, s_date: $s_date, e_date: $e_date, servicetype: $servicetype}';
  }
}
