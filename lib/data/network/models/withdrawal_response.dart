import 'package:linkmom/data/network/models/header_response.dart';
import 'package:json_annotation/json_annotation.dart';

class WithdrawalResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<WithdrawalData>? dataList;

  WithdrawalResponse({this.dataHeader, this.dataList});

  factory WithdrawalResponse.fromJson(Map<String, dynamic> json) {
    return WithdrawalResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] != null ? (json[Key_data] as List).map((e) => WithdrawalData.fromJson(e)).toList() : [],
    );
  }

  WithdrawalData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : WithdrawalData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList;
    return data;
  }

  @override
  String toString() {
    return 'Withdrawalresponse{dataHeader: $dataHeader,dataList: $dataList}';
  }
}

class WithdrawalData {
  static const String Key_withdrawal_cash = 'withdrawal_cash';
  static const String Key_bank_name = 'bank_name';
  static const String Key_bank_name_text = 'bank_name_text';
  static const String Key_bank_account = 'bank_account';
  static const String Key_account_user = 'account_user';
  static const String Key_createdate = 'createdate';
  static const String Key_withdrawal_date = 'withdrawal_date';
  static const String Key_is_withdrawal = 'is_withdrawal';

  @JsonKey(name: Key_withdrawal_cash)
  int withdrawalCash;
  /// NOTE: bank code
  @JsonKey(name: Key_bank_name)
  String bankName;
  @JsonKey(name: Key_bank_name_text)
  String bankNameText;
  @JsonKey(name: Key_bank_account)
  String bankAccount;
  @JsonKey(name: Key_account_user)
  String accountUser;
  @JsonKey(name: Key_createdate)
  String createdate;
  @JsonKey(name: Key_withdrawal_date)
  String withdrawalDate;
  @JsonKey(name: Key_is_withdrawal)
  bool isWithdrawal;

  WithdrawalData({
    this.withdrawalCash = 0,
    this.bankName = '',
    this.bankNameText = '',
    this.bankAccount = '',
    this.accountUser = '',
    this.createdate = '',
    this.withdrawalDate = '',
    this.isWithdrawal = false,
  });

  factory WithdrawalData.fromJson(Map<String, dynamic> json) {
    return WithdrawalData(
      withdrawalCash: json[Key_withdrawal_cash] ?? 0,
      bankName: json[Key_bank_name] ?? '',
      bankNameText: json[Key_bank_name_text] ?? '',
      bankAccount: json[Key_bank_account] ?? '',
      accountUser: json[Key_account_user] ?? '',
      createdate: json[Key_createdate] ?? '',
      withdrawalDate: json[Key_withdrawal_date] ?? '',
      isWithdrawal: json[Key_is_withdrawal] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_withdrawal_cash] = this.withdrawalCash;
    data[Key_bank_name] = this.bankName;
    data[Key_bank_name_text] = this.bankNameText;
    data[Key_bank_account] = this.bankAccount;
    data[Key_account_user] = this.accountUser;
    data[Key_createdate] = this.createdate;
    data[Key_withdrawal_date] = this.withdrawalDate;
    data[Key_is_withdrawal] = this.isWithdrawal;
    return data;
  }

  @override
  String toString() {
    return 'WithdrawalData{withdrawalCash: $withdrawalCash, bankName: $bankName, bankNameText: $bankNameText, bankAccount: $bankAccount, accountUser: $accountUser, createdate: $createdate, withdrawalDate: $withdrawalDate, isWithdrawal: $isWithdrawal}';
  }
}
