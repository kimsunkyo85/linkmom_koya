import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

class NotificationListResponse extends HeaderResponse {
  HeaderResponse dataHeader;
  List<NotificationResultsData> dataList;

  NotificationListResponse({required this.dataHeader, required this.dataList});

  NotificationResultsData getData() => dataList.isEmpty ? NotificationResultsData(results: []) : dataList.first;

  int getCode() => dataHeader.getCode();

  String getMsg() => dataHeader.getMsg();

  factory NotificationListResponse.fromJson(Map<String, dynamic> json) {
    return NotificationListResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? [] : (json[Key_data] as List).map((e) => NotificationResultsData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_data] = this.dataList.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'NotificationListResponse{dataHeader:$dataHeader, dataList:$dataList}';
  }
}

class NotificationResultsData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<NotificationData> results;

  NotificationResultsData({this.count = 0, this.next = '', this.previous = '', required this.results});

  factory NotificationResultsData.fromJson(Map<String, dynamic> json) {
    return NotificationResultsData(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] == null ? [] : (json[Key_results] as List).map((e) => NotificationData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'NotificationResultsData{count: $count, next: $next, previous: $previous, results: $results}';
  }
}

class NotificationData {
  static const String Key_id = 'id';
  static const String Key_pagename = 'pagename';
  static const String Key_next_id = 'next_id';
  static const String Key_message = 'message';
  static const String Key_is_receive = 'is_receive';
  static const String Key_senddate = 'senddate';
  static const String Key_msgdata = 'msgdata';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_pagename)
  int pagename;
  @JsonKey(name: Key_next_id)
  int nextId;
  @JsonKey(name: Key_message)
  String message;
  @JsonKey(name: Key_is_receive)
  bool isReceive;
  @JsonKey(name: Key_senddate)
  String senddate;
  @JsonKey(name: Key_msgdata)
  NotificationMsgData? msgData;

  NotificationData({this.id = 0, this.pagename = 0, this.nextId = 0, this.message = '', this.isReceive = false, this.senddate = '', this.msgData});

  factory NotificationData.fromJson(Map<String, dynamic> json) {
    return NotificationData(
      id: json[Key_id] as int,
      pagename: json[Key_pagename] as int,
      nextId: json[Key_next_id] as int,
      message: json[Key_message] ?? '',
      isReceive: json[Key_is_receive] ?? false,
      senddate: json[Key_senddate] ?? '',
      msgData: json[Key_msgdata] != null ? NotificationMsgData.fromJson(json[Key_msgdata]) : NotificationMsgData(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_pagename] = this.pagename;
    data[Key_next_id] = this.nextId;
    data[Key_message] = this.message;
    data[Key_is_receive] = this.isReceive;
    data[Key_senddate] = this.senddate;
    data[Key_msgdata] = this.msgData;
    return data;
  }

  @override
  String toString() {
    return 'NotificationData{id: $id, pagename: $pagename, next_id: $nextId, message: $message, is_receive: $isReceive, senddate: $senddate, msgData: $msgData}';
  }
}

class NotificationMsgData {
  static const String Key_contract_id = 'contract_id';
  static const String Key_chattingroom = 'chattingroom';
  static const String Key_momdady = 'momdady';
  static const String Key_momdady_first_name = 'momdady_first_name';
  static const String Key_linkmom = 'linkmom';
  static const String Key_linkmom_first_name = 'linkmom_first_name';
  static const String Key_matching_status = 'matching_status';
  static const String Key_care_status = 'care_status';
  static const String Key_deadline = 'deadline';
  static const String Key_order_id = 'order_id';
  static const String Key_next_id = 'next_id';
  static const String Key_bywho = 'bywho';
  static const String Key_allim = 'allim';
  static const String Key_send_code = 'send_code';
  static const String Key_popup_code = 'popup_code';
  static const String Key_redirect_url = 'redirect_url';

  @JsonKey(name: Key_contract_id)
  int contractId;
  @JsonKey(name: Key_chattingroom)
  int chattingroom;
  @JsonKey(name: Key_momdady)
  int momdady;
  @JsonKey(name: Key_momdady_first_name)
  String momdadyFirstName;
  @JsonKey(name: Key_linkmom)
  int linkmom;
  @JsonKey(name: Key_linkmom_first_name)
  String linkmomFirstName;
  @JsonKey(name: Key_matching_status)
  int matchingStatus;
  @JsonKey(name: Key_care_status)
  int careStatus;
  @JsonKey(name: Key_deadline)
  String deadline;
  @JsonKey(name: Key_order_id)
  String orderId;
  @JsonKey(name: Key_next_id)
  int nextId;
  @JsonKey(name: Key_bywho)
  String bywho;
  @JsonKey(name: Key_allim)
  bool allim;
  @JsonKey(name: Key_send_code)
  int sendCode;
  @JsonKey(name: Key_popup_code)
  int popupCode;
  @JsonKey(name: Key_redirect_url)
  String redirect_url;

  NotificationMsgData({
    this.contractId = 0,
    this.chattingroom = 0,
    this.momdady = 0,
    this.momdadyFirstName = '',
    this.linkmom = 0,
    this.linkmomFirstName = '',
    this.matchingStatus = 0,
    this.careStatus = 0,
    this.deadline = '',
    this.orderId = '',
    this.nextId = 0,
    this.bywho = '',
    this.allim = false,
    this.sendCode = 0,
    this.popupCode = 0,
    this.redirect_url = '',
  });

  factory NotificationMsgData.fromJson(Map<String, dynamic> json) {
    return NotificationMsgData(
      contractId: json[Key_contract_id] ?? 0,
      chattingroom: json[Key_chattingroom] ?? 0,
      momdady: json[Key_momdady] ?? 0,
      momdadyFirstName: json[Key_momdady_first_name] ?? '',
      linkmom: json[Key_linkmom] ?? 0,
      linkmomFirstName: json[Key_linkmom_first_name] ?? '',
      matchingStatus: json[Key_matching_status] ?? 0,
      careStatus: json[Key_care_status] ?? 0,
      deadline: json[Key_deadline] ?? '',
      orderId: json[Key_order_id] ?? '',
      nextId: json[Key_next_id] ?? 0,
      bywho: json[Key_bywho] ?? '',
      allim: json[Key_allim] ?? false,
      sendCode: json[Key_send_code] ?? 0,
      popupCode: json[Key_popup_code] ?? 0,
      redirect_url: json[Key_redirect_url] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_contract_id] = this.contractId;
    data[Key_chattingroom] = this.chattingroom;
    data[Key_momdady] = this.momdady;
    data[Key_momdady_first_name] = this.momdadyFirstName;
    data[Key_linkmom] = this.linkmom;
    data[Key_linkmom_first_name] = this.linkmomFirstName;
    data[Key_matching_status] = this.matchingStatus;
    data[Key_care_status] = this.careStatus;
    data[Key_deadline] = this.deadline;
    data[Key_order_id] = this.orderId;
    data[Key_next_id] = this.nextId;
    data[Key_bywho] = this.bywho;
    data[Key_allim] = this.allim;
    data[Key_send_code] = this.sendCode;
    data[Key_popup_code] = this.popupCode;
    data[Key_redirect_url] = this.redirect_url;
    return data;
  }

  @override
  String toString() {
    return 'NotificationMsgData{contractId: $contractId, chattingroom: $chattingroom, momdady: $momdady, momdadyFirstName: $momdadyFirstName, linkmom: $linkmom, linkmomFirstName: $linkmomFirstName, matchingStatus: $matchingStatus, careStatus: $careStatus, deadline: $deadline, orderId: $orderId, nextId: $nextId, bywho: $bywho, allim: $allim, sendCode: $sendCode, popupCode: $popupCode, redirect_url: $redirect_url}';
  }
}
