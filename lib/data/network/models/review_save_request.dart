import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class ReviewSaveRequest {
  static const String Key_matching = 'matching';
  static const String Key_gubun = 'gubun';
  static const String Key_review = 'review';

  ReviewSaveRequest({this.matching = 0, this.gubun = 0, this.review});

  @JsonKey(name: Key_matching)
  int matching;
  @JsonKey(name: Key_gubun)
  int gubun;
  @JsonKey(name: Key_review)
  List<int>? review;

  factory ReviewSaveRequest.fromJson(Map<String, dynamic> json) {
    return ReviewSaveRequest(
      matching: json[Key_matching],
      gubun: json[Key_gubun],
      review: json[Key_review]
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_matching] = this.matching;
    data[Key_gubun] = this.gubun;
    data[Key_review] = this.review;
    return data;
  }

  FormData toFormData(FormData form) {
    form.fields.add(MapEntry(Key_matching, this.matching.toString()));
    form.fields.add(MapEntry(Key_gubun, this.gubun.toString()));
    review!.forEach((element) {
      form.fields.add(MapEntry(Key_review, element.toString()));
    });
    return form;
  }

  @override
  String toString() {
    return 'ReviewSaveRequest{$Key_matching:$matching, $Key_gubun:$gubun, $Key_review:${review.toString()}}';
  }
}
