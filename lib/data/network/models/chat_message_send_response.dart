import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/chat_send_data.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : chat_message_send_response.dart - 메시지 전송 응답
/// since        : 2021/07/16 / update:
@JsonSerializable()
class ChatMessageSendResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ChatSendData>? dataList;

  ChatMessageSendResponse({
    this.dataHeader,
    this.dataList,
  });

  ChatSendData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ChatSendData();

  List<ChatSendData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory ChatMessageSendResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ChatSendData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<ChatSendData>((i) => ChatSendData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return ChatMessageSendResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ChatMessageSendResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
