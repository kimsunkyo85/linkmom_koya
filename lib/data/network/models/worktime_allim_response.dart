import 'package:linkmom/data/network/models/worktime_list_response.dart';
import 'package:linkmom/main.dart';

import 'header_response.dart';

class WorkTimeAllimResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<WorkTimeResults>? dataList;

  WorkTimeAllimResponse({this.dataHeader, this.dataList});

  WorkTimeResults getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : WorkTimeResults();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory WorkTimeAllimResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<WorkTimeResults> datas = [];
    try {
      datas = json[Key_data].map<WorkTimeResults>((e) => WorkTimeResults.fromJson(e)).toList();
    } catch (e) {
      log.e("EXCEPION >>>>>>>>>>>>>>>>>> $e");
    }
    return WorkTimeAllimResponse(dataHeader: dataHeader, dataList: datas);
  }
}
