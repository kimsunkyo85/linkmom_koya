import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/auth_info_data.dart';
import 'data/pay_data.dart';
import 'data/user_info_data.dart';

@JsonSerializable()
class LinkMomListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<LinkMomListData>? dataList;

  LinkMomListResponse({
    this.dataHeader,
    this.dataList,
  });

  LinkMomListData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : LinkMomListData();

  List<LinkMomListData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  bool isData() => dataList!.isNotEmpty;

  factory LinkMomListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<LinkMomListData> datas = [];
    try {
      datas = json[Key_data].map<LinkMomListData>((i) => LinkMomListData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return LinkMomListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'LinkMomListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class LinkMomListData {
  ///리스트 갯수
  static const String Key_count = 'count';

  ///다음 페이지 url
  static const String Key_next = 'next';

  ///이전 페이지 url
  static const String Key_previous = 'previous';

  ///돌봄신청 데이터
  static const String Key_results = 'results';

  LinkMomListData({
    this.count = 0,
    this.next = '',
    this.previous = '',
    this.results,
  });

  ///리스트 갯수
  @JsonKey(name: Key_count)
  int count;

  ///다음 페이지 url
  @JsonKey(name: Key_next)
  String next;

  ///이전 페이지 url
  @JsonKey(name: Key_previous)
  String previous;

  ///링크쌤 신청 데이터
  @JsonKey(name: Key_results)
  List<LinkMomListResultsData>? results;

  factory LinkMomListData.fromJson(Map<String, dynamic> json) {
    List<LinkMomListResultsData> results = json[Key_results] == null ? [] : json[Key_results].map<LinkMomListResultsData>((i) => LinkMomListResultsData.fromJson(i)).toList();
    return LinkMomListData(
      count: json[Key_count] as int,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: results,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results;
    return data;
  }

  @override
  String toString() {
    return 'LinkMomListResponse{count: $count, next: $next, previous: $previous, results: $results}';
  }
}

///돌봄신청 리스트 아이템 데이터
class LinkMomListResultsData {
  static const String Key_userinfo = 'userinfo';
  static const String Key_authinfo = 'authinfo';
  static const String Key_job_id = 'job_id';
  static const String Key_servicetype = 'servicetype';
  static const String Key_possible_area = 'possible_area';
  static const String Key_introduce = 'introduce';
  static const String Key_fee_range = 'fee_range';
  static const String Key_caredate_min = 'caredate_min';
  static const String Key_caredate_max = 'caredate_max';
  static const String Key_careschedule_cnt = 'careschedule_cnt';
  static const String Key_penalty_5 = 'penalty_5';

  LinkMomListResultsData({
    this.userinfo,
    this.authinfo,
    this.job_id = 0,
    this.servicetype = '',
    this.possible_area = '',
    this.introduce = '',
    this.fee_range,
    this.caredate_min = '',
    this.caredate_max = '',
    this.careschedule_cnt = 0,
    this.penalty_5 = false,
  });

  @JsonKey(name: Key_userinfo)
  final UserInfoData? userinfo;
  @JsonKey(name: Key_authinfo)
  final AuthInfoData? authinfo;
  @JsonKey(name: Key_job_id)
  final int job_id;
  @JsonKey(name: Key_servicetype)
  final String servicetype;
  @JsonKey(name: Key_possible_area)
  final String possible_area;
  @JsonKey(name: Key_introduce)
  final String introduce;
  @JsonKey(name: Key_fee_range)
  final PayData? fee_range;
  @JsonKey(name: Key_caredate_min)
  final String caredate_min;
  @JsonKey(name: Key_caredate_max)
  final String caredate_max;
  @JsonKey(name: Key_careschedule_cnt)
  final int careschedule_cnt;
  @JsonKey(name: Key_penalty_5)
  final bool penalty_5;

  factory LinkMomListResultsData.fromJson(Map<String, dynamic> json) {
    UserInfoData userinfo = UserInfoData.fromJson(json[Key_userinfo]);
    AuthInfoData authinfo = AuthInfoData.fromJson(json[Key_authinfo]);
    PayData feeRange = PayData.fromJson(json[Key_fee_range]);
    return LinkMomListResultsData(
      userinfo: userinfo,
      authinfo: authinfo,
      job_id: json[Key_job_id] as int,
      servicetype: json[Key_servicetype] ?? '',
      possible_area: json[Key_possible_area] ?? '',
      introduce: json[Key_introduce] ?? '',
      fee_range: feeRange,
      caredate_min: json[Key_caredate_min] ?? '',
      caredate_max: json[Key_caredate_max] ?? '',
      careschedule_cnt: json[Key_careschedule_cnt] as int,
      penalty_5: json[Key_penalty_5] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_userinfo] = this.userinfo;
    data[Key_job_id] = this.job_id;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_introduce] = this.introduce;
    data[Key_fee_range] = this.fee_range;
    data[Key_caredate_min] = this.caredate_min;
    data[Key_caredate_max] = this.caredate_max;
    data[Key_careschedule_cnt] = this.careschedule_cnt;
    data[Key_penalty_5] = this.penalty_5;
    return data;
  }

  @override
  String toString() {
    return 'LinkMomListResultsData{userinfo: $userinfo, job_id: $job_id, servicetype: $servicetype, possible_area: $possible_area, introduce: $introduce, fee_range: $fee_range, caredate_min: $caredate_min, caredate_max: $caredate_max, careschedule_cnt: $careschedule_cnt, penalty_5: $penalty_5}';
  }
}
