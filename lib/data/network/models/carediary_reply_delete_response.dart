import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/main.dart';

import 'header_response.dart';

@JsonSerializable()
class CareDiaryReplyDeleteResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  CareDiaryReplyDeleteResponse({
    this.dataHeader,
    this.dataList,
  });

  CareDiaryReplyDeleteResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  String getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ' ';

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CareDiaryReplyDeleteResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<CareDiaryMessageData> datas = [];
    try {
      datas = json[Key_data].map<CareDiaryMessageData>((i) => CareDiaryMessageData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return CareDiaryReplyDeleteResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'CareDiaryReplyDeleteResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class CareDiaryMessageData {
  static const String Key_message = 'message';

  CareDiaryMessageData({
    this.message = ''
  });

  @JsonKey(name: Key_message)
  String message;

  factory CareDiaryMessageData.fromJson(Map<String, dynamic> json) {
    return CareDiaryMessageData(
      message: json[Key_message],
    );
  }
}
