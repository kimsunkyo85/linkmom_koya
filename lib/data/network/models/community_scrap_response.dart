import 'package:json_annotation/json_annotation.dart';

import 'header_response.dart';

@JsonSerializable()
class CommunityScrapResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<CommunityScrapData>? dataList;

  CommunityScrapResponse({this.dataHeader, this.dataList});
  CommunityScrapData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : CommunityScrapData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();
  factory CommunityScrapResponse.fromJson(Map<String, dynamic> json) {
    return CommunityScrapResponse(
        dataHeader: HeaderResponse.fromJson(json),
        dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => CommunityScrapData.fromJson(e)).toList());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'CommunityScrapResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class CommunityScrapData {
  static const String Key_share_id = 'share_id';
  @JsonKey(name: Key_share_id)
  String shareId;

  CommunityScrapData({this.shareId = ''});

  factory CommunityScrapData.fromJson(Map<String, dynamic> json) {
    return CommunityScrapData(shareId: json[Key_share_id]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_share_id] = this.shareId;
    return data;
  }

  @override
  String toString() {
    return 'CommunityScrapData{shareId:$shareId}';
  }
}
