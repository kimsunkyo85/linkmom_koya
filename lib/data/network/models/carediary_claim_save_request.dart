import 'dart:io';

import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryClaimSaveRequest {
  static const String Key_schedule = 'schedule';
  static const String Key_type = 'type';
  static const String Key_content1 = 'content1';
  static const String Key_content2 = 'content2';
  static const String Key_image = 'image';

  CareDiaryClaimSaveRequest({
    this.schedule = 0,
    this.type,
    this.content1 = '',
    this.content2 = '',
    this.image,
  });

  @JsonKey(name: Key_schedule)
  final int schedule;
  @JsonKey(name: Key_type)
  final int? type;
  @JsonKey(name: Key_content1)
  final String content1;
  @JsonKey(name: Key_content2)
  final String content2;
  @JsonKey(name: Key_image)
  final List<File>? image;

  factory CareDiaryClaimSaveRequest.fromJson(Map<String, dynamic> json) {
    return CareDiaryClaimSaveRequest(
      schedule: json[Key_schedule] ?? 0,
      type: json[Key_type] ?? 0,
      content1: json[Key_content1] ?? '',
      content2: json[Key_content2] ?? '',
      image: json[Key_image] ?? [],
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_schedule] = this.schedule;
    data[Key_type] = this.type;
    if (this.content1.isNotEmpty) data[Key_content1] = this.content1;
    if (this.content2.isNotEmpty) data[Key_content2] = this.content2;
    if (this.image != null) data[Key_image] = this.image;
    return data;
  }

  FormData toFormData(FormData formData) {
    formData.fields.add(MapEntry(Key_schedule, this.schedule.toString()));
    if (this.type != null) formData.fields.add(MapEntry(Key_type, this.type.toString()));
    if (this.content1.isNotEmpty) formData.fields.add(MapEntry(Key_content1, this.content1.toString()));
    if (this.content2.isNotEmpty) formData.fields.add(MapEntry(Key_content2, this.content2.toString()));
    return formData;
  }
  @override
  String toString() {
    return 'CareDiaryClaimSaveRequest{carediary: $schedule,type: $type,content: $content1,content2: $content2,image: $image}';
  }
}
