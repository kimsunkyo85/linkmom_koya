import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class SettingsRequest {
  static const String Key_is_push_service = "is_push_service";
  static const String Key_is_push_momdady = "is_push_momdady";
  static const String Key_is_push_linkmom = "is_push_linkmom";
  static const String Key_is_push_care = "is_push_care";
  static const String Key_is_push_job = "is_push_job";
  static const String Key_is_push_event = "is_push_event";
  static const String Key_is_auth_login = "is_auth_login";
  static const String Key_is_push_community = "is_push_community";

  SettingsRequest({
    this.is_push_service = 0,
    this.is_push_momdady = 0,
    this.is_push_linkmom = 0,
    this.is_push_care = 1,
    this.is_push_job = 1,
    this.is_push_event = 0,
    this.is_auth_login = 0,
    this.is_push_community = 0,
  });

  @JsonKey(name: Key_is_push_service)
  int is_push_service;
  @JsonKey(name: Key_is_push_momdady)
  int is_push_momdady;
  @JsonKey(name: Key_is_push_linkmom)
  int is_push_linkmom;
  @JsonKey(name: Key_is_push_care)
  int is_push_care;
  @JsonKey(name: Key_is_push_job)
  int is_push_job;
  @JsonKey(name: Key_is_push_event)
  int is_push_event;
  @JsonKey(name: Key_is_auth_login)
  int is_auth_login;
  @JsonKey(name: Key_is_push_community)
  int is_push_community;

  factory SettingsRequest.fromJson(Map<String, dynamic> json) {
    return SettingsRequest(
      is_push_service: json[Key_is_push_service] ?? 0,
      is_push_momdady: json[Key_is_push_momdady] ?? 0,
      is_push_linkmom: json[Key_is_push_linkmom] ?? 0,
      is_push_care: json[Key_is_push_care] ?? 0,
      is_push_job: json[Key_is_push_job] ?? 0,
      is_push_event: json[Key_is_push_event] ?? 0,
      is_auth_login: json[Key_is_auth_login] ?? 0,
      is_push_community: json[Key_is_push_community] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_is_push_service] = this.is_push_service;
    data[Key_is_push_momdady] = this.is_push_momdady;
    data[Key_is_push_linkmom] = this.is_push_linkmom;
    data[Key_is_push_care] = this.is_push_care;
    data[Key_is_push_job] = this.is_push_job;
    data[Key_is_push_event] = this.is_push_event;
    data[Key_is_auth_login] = this.is_auth_login;
    data[Key_is_push_community] = this.is_push_community;
    return data;
  }

  @override
  String toString() {
    return 'SettingsRequest{is_push_service: $is_push_service, is_push_momdady: $is_push_momdady, is_push_linkmom: $is_push_linkmom, is_push_care: $is_push_care, is_push_job: $is_push_job, is_push_event: $is_push_event, is_auth_login: $is_auth_login, is_push_community: $is_push_community}';
  }
}
