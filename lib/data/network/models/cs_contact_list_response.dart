import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

@JsonSerializable()
class CsContactListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;
  List<ListData>? dataList;

  CsContactListResponse({this.dataHeader, this.dataList});

  ListData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : ListData(results: []);

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory CsContactListResponse.fromJson(Map<String, dynamic> json) {
    return CsContactListResponse(
      dataHeader: HeaderResponse.fromJson(json),
      dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => ListData.fromJson(e)).toList(),
    );
  }

  @override
  String toString() {
    return 'CsContactListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class ListData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<ContactData> results;

  ListData({this.count = 0, this.next = '', this.previous = '', required this.results});

  factory ListData.fromJson(Map<String, dynamic> json) {
    return ListData(
      count: json[Key_count] as int,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] == null ? [] : (json[Key_results] as List).map((e) => ContactData.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    data[Key_results] = this.results.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'Data{count: $count, next: $next, previous: $previous, results: $results}';
  }
}

class ContactData {
  static const String Key_id = 'id';
  static const String Key_username = 'username';
  static const String Key_type = 'type';
  static const String Key_typetext = 'typetext';
  static const String Key_title = 'title';
  static const String Key_content = 'content';
  static const String Key_createdate = 'createdate';
  static const String Key_answerid = 'answerid';
  static const String Key_answer = 'answer';
  static const String Key_answerdate = 'answerdate';
  static const String Key_contactus_images = 'contactus_images';

  @JsonKey(name: Key_id)
  int id;
  @JsonKey(name: Key_username)
  String username;
  @JsonKey(name: Key_type)
  int type;
  @JsonKey(name: Key_typetext)
  String typetext;
  @JsonKey(name: Key_title)
  String title;
  @JsonKey(name: Key_content)
  String content;
  @JsonKey(name: Key_createdate)
  String createdate;
  @JsonKey(name: Key_answerid)
  String answerid;
  @JsonKey(name: Key_answer)
  String answer;
  @JsonKey(name: Key_answerdate)
  String answerdate;
  @JsonKey(name: Key_contactus_images)
  List<ContactusImages> contactusImages;

  ContactData({this.id = 0, this.username = '', this.type = 0, this.typetext = '', this.title = '', this.content = '', this.createdate = '', this.answerid = '', this.answer = '', this.answerdate = '', required this.contactusImages});

  factory ContactData.fromJson(Map<String, dynamic> json) {
    return ContactData(
      id: json[Key_id] as int,
      username: json[Key_username] ?? '',
      type: json[Key_type] as int,
      typetext: json[Key_typetext] ?? '',
      title: json[Key_title] ?? '',
      content: json[Key_content] ?? '',
      createdate: json[Key_createdate] ?? '',
      answerid: json[Key_answerid] ?? '',
      answer: json[Key_answer] ?? '',
      answerdate: json[Key_answerdate] ?? '',
      contactusImages: json[Key_contactus_images] == null ? [] : (json[Key_contactus_images] as List).map((e) => ContactusImages.fromJson(e)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_username] = this.username;
    data[Key_type] = this.type;
    data[Key_typetext] = this.typetext;
    data[Key_title] = this.title;
    data[Key_content] = this.content;
    data[Key_createdate] = this.createdate;
    data[Key_answerid] = this.answerid;
    data[Key_answer] = this.answer;
    data[Key_answerdate] = this.answerdate;
    data[Key_contactus_images] = this.contactusImages.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'ContactData{id: $id, username: $username, type: $type, typetext: $typetext, title: $title, content: $content, createdate: $createdate, answerid: $answerid, answer: $answer, answerdate: $answerdate, contactusImages: $contactusImages}';
  }
}

class ContactusImages {
  static const String Key_image = 'image';

  @JsonKey(name: Key_image)
  String image;

  ContactusImages({this.image = ''});

  factory ContactusImages.fromJson(Map<String, dynamic> json) {
    return ContactusImages(image: json[Key_image] ?? '');
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_image] = this.image;
    return data;
  }

  @override
  String toString() {
    return 'ContactusImages{image: $image}';
  }
}
