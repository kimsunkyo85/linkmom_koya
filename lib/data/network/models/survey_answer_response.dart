import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';
import 'header_response.dart';

@JsonSerializable()
class SurveyAnswerResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  SurveyAnswerResponse({
    this.dataHeader,
    this.dataList,
  });

  SurveyAnswerInfo getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : SurveyAnswerInfo();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory SurveyAnswerResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<SurveyAnswerInfo> datas = [];
    try {
      datas = json[Key_data].map<SurveyAnswerInfo>((i) => SurveyAnswerInfo.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return SurveyAnswerResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ReqauthResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class SurveyAnswerInfo {
  static const String Key_survey = "survey";
  static const String Key_id = "id";
  static const String Key_createdate = "createdate";

  SurveyAnswerInfo({
    this.survey = 0,
    this.id = 0,
    this.createdate = '',
  });

  @JsonKey(name: Key_survey)
  int survey;

  @JsonKey(name: Key_id)
  int id;

  @JsonKey(name: Key_createdate)
  String createdate;

  factory SurveyAnswerInfo.fromJson(Map<String, dynamic> json) {
    return SurveyAnswerInfo(
      survey: json[Key_survey] ?? 0, 
      id: json[Key_id] ?? 0, 
      createdate: json[Key_createdate] ?? ''
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_survey] = this.survey;
    data[Key_id] = this.id;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'SurveyAnswerInfo{survey: $survey, id: $id, createdate: $createdate}';
  }
}
