import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class BlockSaveResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  BlockSaveResponse({
    this.dataHeader,
    dataList,
  });

  BlockSaveResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  BlockUserData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : BlockUserData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory BlockSaveResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<BlockUserData> datas = [];
    try {
      log.d('『GGUMBI』>>> fromJson ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<<${json[Key_data]} ');
      datas = json[Key_data].map<BlockUserData>((i) => BlockUserData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return BlockSaveResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'BlockSaveResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class BlockUserData {
  static const String Key_block_user = 'block_user';
  static const String Key_block_user_name = 'block_user_name';
  static const String Key_block_user_profileimg = 'block_user_profileimg';
  static const String Key_block_matching = 'block_matching';

  BlockUserData({
    this.block_user = 0,
    this.block_user_name = '',
    this.block_user_profileimg = '',
    this.block_matching = 0,
  });

  @JsonKey(name: Key_block_user)
  int block_user;

  @JsonKey(name: Key_block_user_name)
  String block_user_name;

  @JsonKey(name: Key_block_user_profileimg)
  String block_user_profileimg;

  @JsonKey(name: Key_block_matching)
  int block_matching;

  factory BlockUserData.fromJson(Map<String, dynamic> json) {
    return BlockUserData(
      block_user: json[Key_block_user] as int,
      block_user_name: json[Key_block_user_name] ?? '',
      block_user_profileimg: json[Key_block_user_profileimg] ?? '',
      block_matching: json[Key_block_matching] as int,
    );
  }

  @override
  String toString() {
    return 'BlockSaveData: {block_user: $block_user, block_user_name: $block_user_name, block_user_profileimg: $block_user_profileimg, block_matching: $block_matching}';
  }
}
