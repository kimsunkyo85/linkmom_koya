import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

import 'data/matching_cancel_deny_data.dart';

/// All Rights Reserved. Copyright(c) 2021 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : matching_deny_response.dart - 매칭 거절하기 응답
/// since        : 2021/08/03 / update:
/// (0, '매칭중(대화중)'), (11, '매칭실패(링크쌤 응답없음)'), (12, '매칭실패(맘대디 응답없음)'), (13, '매칭실패(링크쌤 거절)'), (14, '매칭실패(맘대디 거절)'), (21, '매칭취소(링크쌤 계약취소)'), (22, '매칭취소(맘대디 계약취소)'), (23, '매칭취소(결제시간만료)'), (91, '매칭대기(링크쌤 계약대기)'), (92, '매칭대기(맘대디 계약대기)'), (99, '매칭성공(계약완료/결제대기중)') (100, '결제완료') (0, '없음(매칭상태 확인)'), (1, '돌봄예정'), (2, '돌봄 중'), (3, '돌봄종료(정상종료)'), (4, '돌봄취소(부분종료&일부취소)'), (5, '돌봄취소(전체취소)') (0, '계약중'), (11, '계약실패(거절/취소/응답없음/시간만료 등)'), (21, '계약완료 후 취소(부분)'), (22, '계약완료 후 취소(전체)'), (99, '매칭성공(계약완료/결제대기중)'), (100, '결제완료')
@JsonSerializable()
class MatchingDenyResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<MatchingCancelDenyData>? dataList;

  MatchingDenyResponse({
    this.dataHeader,
    this.dataList,
  });

  MatchingCancelDenyData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : MatchingCancelDenyData();

  List<MatchingCancelDenyData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  bool isData() => dataList!.isNotEmpty;

  factory MatchingDenyResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<MatchingCancelDenyData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<MatchingCancelDenyData>((i) => MatchingCancelDenyData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return MatchingDenyResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'MatchingDenyResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
/*
  MatchingDenyData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : MatchingDenyData();

  List<MatchingDenyData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  bool isData() => dataList!.isNotEmpty;

  factory MatchingDenyResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<MatchingDenyData> datas = [];
    try {
      datas = json[Key_data] == null ? List() : json[Key_data].map<MatchingDenyData>((i) => MatchingDenyData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return MatchingDenyResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'MatchingDenyResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class MatchingDenyData {
  static const String Key_id = 'id';
  static const String Key_bookingcareservices = 'bookingcareservices';
  static const String Key_carematching = 'carematching';
  static const String Key_carematching_info = 'carematching_info';
  static const String Key_paid_deadline = 'paid_deadline';
  static const String Key_contract_date = 'contract_date';
  static const String Key_contract_status = 'contract_status';
  static const String Key_createdate = 'createdate';
  static const String Key_updatedate = 'updatedate';

  MatchingDenyData({
    this.id,
    this.bookingcareservices,
    this.carematching,
    this.carematching_info,
    this.paid_deadline,
    this.contract_date,
    this.contract_status,
    this.createdate,
    this.updatedate,
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_bookingcareservices)
  final int bookingcareservices;
  @JsonKey(name: Key_carematching)
  final int carematching;
  @JsonKey(name: Key_carematching_info)
  final CareMatchingInfoData carematching_info;
  @JsonKey(name: Key_paid_deadline)
  final String paid_deadline;
  @JsonKey(name: Key_contract_date)
  final String contract_date;
  @JsonKey(name: Key_contract_status)
  final int contract_status;
  @JsonKey(name: Key_createdate)
  final String createdate;
  @JsonKey(name: Key_updatedate)
  final String updatedate;

  factory MatchingDenyData.fromJson(Map<String, dynamic> json) {
    CareMatchingInfoData carematching_info = CareMatchingInfoData.fromJson(json[Key_carematching_info]);
    return MatchingDenyData(
      id: json[Key_id] as int,
      bookingcareservices: json[Key_bookingcareservices] as int,
      carematching: json[Key_carematching] as int,
      carematching_info: carematching_info,
      paid_deadline: json[Key_paid_deadline] ?? '',
      contract_date: json[Key_contract_date] ?? '',
      contract_status: json[Key_contract_status] as int,
      createdate: json[Key_createdate] ?? '',
      updatedate: json[Key_updatedate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_bookingcareservices] = this.bookingcareservices;
    data[Key_carematching] = this.carematching;
    data[Key_carematching_info] = this.carematching_info;
    data[Key_paid_deadline] = this.paid_deadline;
    data[Key_contract_date] = this.contract_date;
    data[Key_contract_status] = this.contract_status;
    data[Key_createdate] = this.createdate;
    data[Key_updatedate] = this.updatedate;
    return data;
  }

  @override
  String toString() {
    return 'MatchingDenyData{id: $id, bookingcareservices: $bookingcareservices, carematching: $carematching, carematching_info: $carematching_info, paid_deadline: $paid_deadline, contract_date: $contract_date, contract_status: $contract_status, createdate: $createdate, updatedate: $updatedate}';
  }
}*/
