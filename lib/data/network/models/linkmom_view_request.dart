import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : job_list_request.dart - 링크쌤 신청 리스트 상세 조회
/// since        : 2021/06/03 / update:
@JsonSerializable()
class LinkMomViewRequest {
  ///돌봄 신청 아이디
  static const String Key_job_id = 'job_id';

  LinkMomViewRequest({
    this.job_id = 0,
  });

  ///페이지 번호
  @JsonKey(name: Key_job_id)
  final int job_id;

  factory LinkMomViewRequest.fromJson(Map<String, dynamic> json) {
    return LinkMomViewRequest(
      job_id: json[Key_job_id] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_job_id] = this.job_id;
    return data;
  }

  @override
  String toString() {
    return 'LinkMomViewRequest{job_id: $job_id}';
  }
}
