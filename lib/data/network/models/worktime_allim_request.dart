import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class WorkTimeAllimRequest {
  static const String Key_schedule_id = 'schedule_id';
  static const String Key_gubun = 'gubun';
  @JsonKey(name: Key_schedule_id)
  int scheduleId;
  @JsonKey(name: Key_gubun)
  int gubun;

  WorkTimeAllimRequest({this.scheduleId = 0, this.gubun = 0});

  factory WorkTimeAllimRequest.fromJson(Map<String, dynamic> json) {
    return WorkTimeAllimRequest(
      scheduleId: json[Key_schedule_id] ?? 0,
      gubun: json[Key_gubun] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_schedule_id] = this.scheduleId;
    data[Key_gubun] = this.gubun;
    return data;
  }

  FormData toFormData() {
    FormData formData = FormData();
    formData.fields.add(MapEntry(Key_schedule_id, this.scheduleId.toString()));
    formData.fields.add(MapEntry(Key_gubun, this.gubun.toString()));
    return formData;
  }

  @override
  String toString() {
    return 'WorkTimeAllimRequest{scheduleId: $scheduleId, gubun: $gubun}';
  }
}
