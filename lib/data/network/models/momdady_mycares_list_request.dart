import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : momdady_mycares_list_request.dart - 내 신청서 & 필터링 (내 돌봄신청 건)
/// since        : 2021/06/30 / update:
@JsonSerializable()
class MomdadyMyCaresListRequest {
  ///조회 시작일자
  static const String Key_s_date = 's_date';

  ///조회 종료일자
  static const String Key_e_date = 'e_date';

  ///돌봄신청 상태 (0:구인중, 1:구인종료)
  static const String Key_status = 'status';

  ///돌봄유형
  static const String Key_servicetype = 'servicetype';

  MomdadyMyCaresListRequest({
    this.s_date = '',
    this.e_date = '',
    this.status,
    this.servicetype,
  });

  ///채팅에서 내 신청서 목록 불러오기시 사용 (s_date:-1, e_date:-1, status:0)
  MomdadyMyCaresListRequest.reqeustChat() {
    s_date = '-1';
    e_date = '-1';
    status = [0];
    servicetype = [];
  }

  ///조회 시작일자
  @JsonKey(name: Key_s_date)
  late String s_date;

  ///조회 종료일자
  @JsonKey(name: Key_e_date)
  late String e_date;

  ///돌봄신청 상태 (0:구인중, 1:구인종료)
  @JsonKey(name: Key_status)
  late List<int>? status = [];

  ///돌봄유형
  @JsonKey(name: Key_status)
  late final List<int>? servicetype;

  factory MomdadyMyCaresListRequest.fromJson(Map<String, dynamic> json) {
    List<int> list = List<int>.from(json[Key_status]);
    List<int> servicetype = List<int>.from(json[Key_servicetype]);
    return MomdadyMyCaresListRequest(
      s_date: json[Key_s_date] ?? '',
      e_date: json[Key_e_date] ?? '',
      status: list,
      servicetype: servicetype,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_s_date] = this.s_date;
    data[Key_e_date] = this.e_date;
    data[Key_status] = this.status;
    data[Key_servicetype] = this.servicetype;
    return data;
  }

  FormData toFormData(FormData formData) {
    formData.fields.add(MapEntry(Key_s_date, s_date.toString()));
    formData.fields.add(MapEntry(Key_e_date, e_date.toString()));
    if (status != null)
    status!.forEach((element) {
      formData.fields.add(MapEntry(Key_status, element.toString()));
    });
    if (servicetype != null)
      servicetype!.forEach((e) {
        formData.fields.add(MapEntry(Key_servicetype, e.toString()));
      });
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'MyCaresListRequest{s_date: $s_date, e_date: $e_date, status: $status, servicetype: $servicetype}';
  }
}
