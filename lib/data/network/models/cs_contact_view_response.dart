import 'cs_contact_list_response.dart';
import 'header_response.dart';

class CsContactViewResponse extends HeaderResponse {
  HeaderResponse dataHeader;
  ContactData data;

  CsContactViewResponse({required this.dataHeader, required this.data});

  ContactData getData() => data;

  int getCode() => dataHeader.getCode();

  String getMsg() => dataHeader.getMsg();

  factory CsContactViewResponse.fromJson(Map<String, dynamic> json) {
    return CsContactViewResponse(
      dataHeader: HeaderResponse.fromJson(json),
      data: ContactData.fromJson(json[Key_data] ?? ''),
    );
  }

  @override
  String toString() {
    return 'CsContactViewResponse{dataHeader: $dataHeader, data: $data}';
  }
}
