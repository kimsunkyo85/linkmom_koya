import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'req_data.dart';

@JsonSerializable()
class TempResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<TempData>? dataList;

  TempResponse({
    this.dataHeader,
    this.dataList,
  });

  TempResponse.clone(TempResponse data)
      : this(
          dataHeader: data.dataHeader,
          dataList: data.dataList,
        );

  TempData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : TempData();

  List<TempData> getDataList() => dataList ?? [];

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory TempResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<TempData> datas = [];
    try {
      datas = json[Key_data].map<TempData>((i) => TempData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return TempResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  TempResponse clone() {
    final jsonResponse = json.decode(json.encode(this));
    return TempResponse.fromJson(jsonResponse as Map<String, dynamic>);
  }

  @override
  String toString() {
    return 'TempResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class TempData {
  static const String Key_child = 'child';
  static const String Key_servicetype = 'servicetype';
  static const String Key_possible_area = 'possible_area';
  static const String Key_depth = 'depth';
  static const String Key_reqdata = 'reqdata';
  static const String Key_createdate = 'createdate';

  TempData({
    this.child = 0,
    this.servicetype = 0,
    this.possible_area = 0,
    this.depth = 0,
    this.reqdata,
    this.createdate = '',
  });

  // TempData.clone(TempData data )
  //     : this(
  //   child: data.child,
  //   servicetype: data.servicetype.,
  //   possible_area: data.possible_area,
  //   depth: data.depth,
  //   reqdata: data.reqdata,
  //   createdate: data.createdate,
  // );

  @JsonKey(name: Key_child)
  int child;
  @JsonKey(name: Key_servicetype)
  int servicetype;
  @JsonKey(name: Key_possible_area)
  int possible_area;
  @JsonKey(name: Key_depth)
  int depth;
  @JsonKey(name: Key_reqdata)
  ReqData? reqdata;
  @JsonKey(name: Key_createdate)
  String createdate;

  factory TempData.fromJson(Map<String, dynamic> json) {
    ReqData reqdata;
    try {
      reqdata = json[Key_reqdata] == "null" ? ReqData(careschedule: []) : ReqData.fromJson(jsonDecode(json[Key_reqdata]));
    } catch (e) {
      reqdata = ReqData();
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }

    return TempData(
      child: json[Key_child] as int,
      servicetype: json[Key_servicetype] as int,
      possible_area: json[Key_possible_area] as int,
      depth: json[Key_depth] as int,
      reqdata: reqdata,
      createdate: json[Key_createdate] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_child] = this.child;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_depth] = this.depth;
    data[Key_reqdata] = this.reqdata;
    data[Key_createdate] = this.createdate;
    return data;
  }

  @override
  String toString() {
    return 'TempData{child: $child, servicetype: $servicetype, possible_area: $possible_area, depth: $depth, reqdata: $reqdata, createdate: $createdate}';
  }
}
