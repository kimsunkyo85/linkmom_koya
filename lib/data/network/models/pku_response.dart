import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class PkuResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  PkuResponse({
    this.dataHeader,
    this.dataList,
  });

  PkuResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  PkuData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PkuData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory PkuResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PkuData> datas = [];
    try {
      datas = json[Key_data].map<PkuData>((i) => PkuData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return PkuResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'PkuResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class PkuData {
  static const String Key_pku = 'pku';

  PkuData({
    this.pku = '',
  });

  @JsonKey(name: Key_pku)
  String pku;

  factory PkuData.fromJson(Map<String, dynamic> json) {
    return PkuData(
      pku: json[Key_pku] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_pku] = this.pku;
    return data;
  }

  @override
  String toString() {
    return 'PkuData{pku: $pku}';
  }
}
