import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

import 'data/care_child_info_data.dart';
import 'data/page_data.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : momdady_mycares_list_response.dart - 내 신청서 & 필터링 (내 돌봄신청 건)
/// since        : 2021/06/30 / update:
@JsonSerializable()
class MomdadyMyCaresListResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PageData>? dataList;

  MomdadyMyCaresListResponse({
    this.dataHeader,
    this.dataList,
  });

  PageData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PageData(results: []);

  List<PageData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  bool isData() => dataList!.isNotEmpty;

  factory MomdadyMyCaresListResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PageData> datas = [];
    try {
      datas = json[Key_data].map<PageData>((i) => PageData.fromJson(i, '$MomdadyMyCaresListResponse')).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return MomdadyMyCaresListResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'MomdadyMyCaresListResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class MyCareResultsData {
  static const String Key_booking_id = 'booking_id';
  static const String Key_servicetype = 'servicetype';
  static const String Key_possible_area = 'possible_area';
  static const String Key_cost_sum = 'cost_sum';
  static const String Key_status = 'status';
  static const String Key_carescheduleinfo = 'carescheduleinfo';
  static const String Key_carematching_cnt = 'carematching_cnt';
  static const String Key_childinfo = 'childinfo';
  static const String Key_booking_status = 'booking_status';

  MyCareResultsData({
    this.booking_id = 0,
    this.servicetype = '',
    this.possible_area = '',
    this.cost_sum = 0,
    this.status = '',
    this.carescheduleinfo,
    this.carematching_cnt,
    this.childinfo,
    this.booking_status = 0,
  });

  @JsonKey(name: Key_booking_id)
  int booking_id;

  @JsonKey(name: Key_servicetype)
  String servicetype;

  @JsonKey(name: Key_possible_area)
  String possible_area;

  @JsonKey(name: Key_cost_sum)
  int cost_sum;

  @JsonKey(name: Key_status)
  String status;

  @JsonKey(name: Key_carescheduleinfo)
  CareScheduleInfo? carescheduleinfo;

  @JsonKey(name: Key_carematching_cnt)
  CareMathingCount? carematching_cnt;

  @JsonKey(name: Key_childinfo)
  CareChildInfoData? childinfo;

  @JsonKey(name: Key_booking_status)
  int booking_status;

  ///채팅 돌봄신청서 불어오기만 사용
  bool isSelected = false;

  factory MyCareResultsData.fromJson(Map<String, dynamic> json) {
    CareScheduleInfo infoList = CareScheduleInfo();
    CareMathingCount countList = CareMathingCount();
    CareChildInfoData childinfo = CareChildInfoData();
    try {
      infoList = CareScheduleInfo.fromJson(json[Key_carescheduleinfo]);
      countList = CareMathingCount.fromJson(json[Key_carematching_cnt]);
      childinfo = CareChildInfoData.fromJson(json[Key_childinfo]);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return MyCareResultsData(
        booking_id: json[Key_booking_id] as int,
        servicetype: json[Key_servicetype] ?? '',
        possible_area: json[Key_possible_area] ?? '',
        cost_sum: json[Key_cost_sum] as int,
        status: json[Key_status] ?? '',
        carescheduleinfo: infoList,
        carematching_cnt: countList,
        childinfo: childinfo,
        booking_status: json[Key_booking_status] ?? 0);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_booking_id] = this.booking_id;
    data[Key_servicetype] = this.servicetype;
    data[Key_possible_area] = this.possible_area;
    data[Key_cost_sum] = this.cost_sum;
    data[Key_status] = this.status;
    data[Key_carescheduleinfo] = this.carescheduleinfo;
    data[Key_carematching_cnt] = this.carematching_cnt;
    data[Key_childinfo] = this.childinfo;
    data[Key_booking_status] = this.booking_status;
    return data;
  }

  @override
  String toString() {
    return 'MyCareResultsData{booking_id: $booking_id, servicetype: $servicetype, possible_area: $possible_area, cost_sum: $cost_sum, status: $status, carescheduleinfo: $carescheduleinfo, carematching_cnt: $carematching_cnt, childinfo: $childinfo, isSelected: $isSelected, booking_status: $booking_status}';
  }
}

@JsonSerializable()
class CareScheduleInfo {
  static const String Key_stime = 'stime';
  static const String Key_etime = 'etime';
  static const String Key_caredate = 'caredate';

  CareScheduleInfo({this.stime = '', this.etime = '', this.caredate});

  @JsonKey(name: Key_stime)
  String stime;

  @JsonKey(name: Key_etime)
  String etime;

  @JsonKey(name: Key_caredate)
  List<String>? caredate;

  factory CareScheduleInfo.fromJson(Map<String, dynamic> json) {
    List<String> list = [];
    list = List<String>.from(json[Key_caredate]);
    return CareScheduleInfo(
      stime: json[Key_stime] ?? '',
      etime: json[Key_etime] ?? '',
      caredate: list,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_stime] = this.stime;
    data[Key_etime] = this.etime;
    data[Key_caredate] = this.caredate;
    return data;
  }

  @override
  String toString() {
    return 'CareScheduleInfo{stime: $stime, etime: $etime, caredate: $caredate}';
  }
}

@JsonSerializable()
class CareMathingCount {
  static const String Key_tome_cnt = 'tome_cnt';
  static const String Key_byme_cnt = 'byme_cnt';

  CareMathingCount({this.tome_cnt = 0, this.byme_cnt = 0});

  @JsonKey(name: Key_tome_cnt)
  int tome_cnt;

  @JsonKey(name: Key_byme_cnt)
  int byme_cnt;

  factory CareMathingCount.fromJson(Map<String, dynamic> json) {
    return CareMathingCount(
      tome_cnt: json[Key_tome_cnt] as int,
      byme_cnt: json[Key_byme_cnt] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_tome_cnt] = this.tome_cnt;
    data[Key_byme_cnt] = this.byme_cnt;
    return data;
  }

  @override
  String toString() {
    return 'CareScheduleInfo{tome_cnt: $tome_cnt, byme_cnt: $byme_cnt}';
  }
}
