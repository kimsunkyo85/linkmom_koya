import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CommnunityEventRequest {
  static const String Key_status = 'status';
  @JsonKey(name: Key_status)
  String status;

  CommnunityEventRequest({this.status = ''});

  factory CommnunityEventRequest.fromJson(Map<String, dynamic> json) {
    return CommnunityEventRequest(status: json[Key_status] ?? '');
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_status] = this.status;
    return data;
  }

  @override
  String toString() {
    return 'CommnunityEventRequest{ status: $status }';
  }
}
