import 'package:linkmom/main.dart';

import '../../../utils/string_util.dart';
import 'addr_keyword_response.dart';

class AddrSearchResponse {
  List<Documents>? documents;
  Meta? meta;

  AddrSearchResponse({this.documents, this.meta});

  factory AddrSearchResponse.fromJson(Map<String, dynamic> json) {
    try {
      return AddrSearchResponse(
        documents: json["documents"] == null ? [] : (json["documents"] as List).map((e) => Documents.fromJson(e)).toList(),
        meta: json["meta"] == null ? Meta() : Meta.fromJson(json["meta"]),
      );
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- type ---------------',
        '--- DATA     ': e,
      });
      return AddrSearchResponse();
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.documents != null) data["documents"] = this.documents!.map((e) => e.toJson()).toList();
    if (this.meta != null) data["meta"] = this.meta!.toJson();
    return data;
  }

  @override
  String toString() {
    return 'AddrSearchResponse{documents: $documents, meta: $meta}';
  }
}

class Documents {
  Address? address;
  String addressName;
  String addressType;
  RoadAddress? roadAddress;
  String x;
  String y;

  Documents({this.address, this.addressName = '', this.addressType = '', this.roadAddress, this.x = '', this.y = ''});

  factory Documents.fromJson(Map<String, dynamic> json) {
    try {
      return Documents(
        address: json["address"] == null ? Address() : Address.fromJson(json["address"]),
        addressName: json["address_name"] ?? '',
        addressType: json["address_type"] ?? '',
        roadAddress: json["road_address"] == null ? RoadAddress() : RoadAddress.fromJson(json["road_address"]),
        x: json["x"] ?? '',
        y: json["y"] ?? '',
      );
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- type ---------------',
        '--- DATA     ': e,
      });
      return Documents();
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.address != null) data["address"] = this.address!.toJson();
    data["address_name"] = this.addressName;
    data["address_type"] = this.addressType;
    data["road_address"] = this.roadAddress;
    data["x"] = this.x;
    data["y"] = this.y;
    return data;
  }

  ///돌봄신청서 작성준, 도로명 주소가 없으면, 동으로 리턴
  String getAddress() {
    try {
      if (roadAddress!.getAddrWithBuilding.isNotEmpty) {
        return roadAddress!.getAddrWithBuilding;
      } else {
        return address!.getAddress3depth();
      }
    } catch (e) {
      return '';
    }
  }

  @override
  String toString() {
    return 'Documents{address: $address, addressName: $addressName, addressType: $addressType, roadAddress: $roadAddress, x: $x, y: $y}';
  }
}

class Address {
  String addressName;
  String bCode;
  String hCode;
  String mainAddressNo;
  String mountainYn;
  String region1DepthName;
  String region2DepthName;
  String region3DepthHName;
  String region3DepthName;
  String subAddressNo;
  String x;
  String y;

  Address({this.addressName = '', this.bCode = '', this.hCode = '', this.mainAddressNo = '', this.mountainYn = '', this.region1DepthName = '', this.region2DepthName = '', this.region3DepthHName = '', this.region3DepthName = '', this.subAddressNo = '', this.x = '', this.y = ''});

  factory Address.fromJson(Map<String, dynamic> json) {
    try {
      return Address(
        addressName: json["address_name"] ?? '',
        bCode: json["b_code"] ?? '',
        hCode: json["h_code"] ?? '',
        mainAddressNo: json["main_address_no"] ?? '',
        mountainYn: json["mountain_yn"] ?? '',
        region1DepthName: json["region_1depth_name"] ?? '',
        region2DepthName: json["region_2depth_name"] ?? '',
        region3DepthHName: json["region_3depth_h_name"] ?? '',
        region3DepthName: json["region_3depth_name"] ?? '',
        subAddressNo: json["sub_address_no"] ?? '',
        x: json["x"] ?? '',
        y: json["y"] ?? '',
      );
    } catch (e) {
      return Address();
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["address_name"] = this.addressName;
    data["b_code"] = this.bCode;
    data["h_code"] = this.hCode;
    data["main_address_no"] = this.mainAddressNo;
    data["mountain_yn"] = this.mountainYn;
    data["region_1depth_name"] = this.region1DepthName;
    data["region_2depth_name"] = this.region2DepthName;
    data["region_3depth_h_name"] = this.region3DepthHName;
    data["region_3depth_name"] = this.region3DepthName;
    data["sub_address_no"] = this.subAddressNo;
    data["x"] = this.x;
    data["y"] = this.y;
    return data;
  }

  ///원하는 링크쌤 위치는 동으로 한다. 1순위, 2순위
  String getAddress3depth() {
    String value = '$region1DepthName $region2DepthName $region3DepthName';
    if (region2DepthName == region3DepthName) {
      value = '$region1DepthName $region3DepthName';
    }
    if (!StringUtils.validateString(region3DepthName)) {
      value = '$region1DepthName $region2DepthName';
    }
    return value;
  }

  @override
  String toString() {
    return 'Address{addressName: $addressName, bCode: $bCode, hCode: $hCode, mainAddressNo: $mainAddressNo, mountainYn: $mountainYn, region1DepthName: $region1DepthName, region2DepthName: $region2DepthName, region3DepthHName: $region3DepthHName, region3DepthName: $region3DepthName, subAddressNo: $subAddressNo, x: $x, y: $y}';
  }
}

class RoadAddress {
  String addressName;
  String buildingName;
  String mainBuildingNo;
  String roadName;
  String subBuildingNo;
  String region1DepthName;
  String region2DepthName;
  String region3DepthName;
  String undergroundYn;
  String x;
  String y;
  String zoneNo;

  RoadAddress({this.addressName = '', this.buildingName = '', this.mainBuildingNo = '', this.roadName = '', this.subBuildingNo = '', this.region1DepthName = '', this.region2DepthName = '', this.region3DepthName = '', this.undergroundYn = '', this.x = '', this.y = '', this.zoneNo = ''});

  factory RoadAddress.fromJson(Map<String, dynamic> json) {
    try {
      return RoadAddress(
        addressName: json["address_name"] ?? '',
        buildingName: json["building_name"] ?? '',
        mainBuildingNo: json["main_building_no"] ?? '',
        roadName: json["main_address_no"] ?? '',
        subBuildingNo: json["sub_building_no"] ?? '',
        region1DepthName: json["region_1depth_name"] ?? '',
        region2DepthName: json["region_2depth_name"] ?? '',
        region3DepthName: json["region_3depth_name"] ?? '',
        undergroundYn: json["underground_yn"] ?? '',
        x: json["x"] ?? '',
        y: json["y"] ?? '',
        zoneNo: json["zone_no"] ?? '',
      );
    } catch (e) {
      return RoadAddress();
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["address_name"] = this.addressName;
    data["b_code"] = this.buildingName;
    data["h_code"] = this.mainBuildingNo;
    data["main_address_no"] = this.roadName;
    data["mountain_yn"] = this.subBuildingNo;
    data["region_1depth_name"] = this.region1DepthName;
    data["region_2depth_name"] = this.region2DepthName;
    data["region_3depth_name"] = this.region3DepthName;
    data["sub_address_no"] = this.undergroundYn;
    data["x"] = this.x;
    data["y"] = this.y;
    data["zone_no"] = this.zoneNo;
    return data;
  }

  /// 도로명 +(빌딩이름)
  String get getAddrWithBuilding => this.addressName + '${this.buildingName.isNotEmpty ? ' (' + this.buildingName + ')' : ''}';

  @override
  String toString() {
    return 'RoadAddress{addressName: $addressName, buildingName: $buildingName, mainBuildingNo: $mainBuildingNo, roadName: $roadName, subBuildingNo: $subBuildingNo, region1DepthName: $region1DepthName, region2DepthName: $region2DepthName, region3DepthName: $region3DepthName, undergroundYn: $undergroundYn, x: $x, y: $y, zoneNo: $zoneNo}';
  }
}
