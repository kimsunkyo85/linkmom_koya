import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../../../main.dart';
import 'data/pay_cancel_save_explain_data.dart';

/// All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///
/// Created by   : platformbiz@ggumbi.com
///
/// version      : 1.0.0
///
/// see          : pay_cancel_explain_response.dart - 결제취소 소명하기
///
/// since        : 2021/09/09 / update:
///
/// "msg_cd": 5040, "message": "취소 할 수 없는 결제 입니다.",
///
/// "msg_cd": 5041, "message": "이미 취소된 결제 입니다.",
///
/// "msg_cd": 4000, "message": "데이터가 없습니다.",

class PayCancelExplainResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<PayCanceSaveExplainData>? dataList;

  PayCancelExplainResponse({
    this.dataHeader,
    this.dataList,
  });

  PayCanceSaveExplainData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : PayCanceSaveExplainData();

  List<PayCanceSaveExplainData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory PayCancelExplainResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<PayCanceSaveExplainData> datas = [];
    try {
      datas = json[Key_data] == null ? [] : json[Key_data].map<PayCanceSaveExplainData>((i) => PayCanceSaveExplainData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return PayCancelExplainResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'ChatRoomUpdateResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}
