import 'package:json_annotation/json_annotation.dart';

///- All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///- Created by   : platformbiz@ggumbi.com
///- version      : 1.0.0
///- see          : area_default_request.dart - 기본 동네 인증 설정저장하기
///- since        : 2022/01/04 / update:
///- [함수,메서드시]
@JsonSerializable()
class AreaDefaultRequest {
  static const String Key_address_id = 'address_id';

  AreaDefaultRequest({
    this.address_id = 0,
  });

  ///기본설정 주소 ID (인증된 주소 ID : address_id)
  @JsonKey(name: Key_address_id)
  final int address_id;

  factory AreaDefaultRequest.fromJson(Map<String, dynamic> json) {
    return AreaDefaultRequest(
      address_id: json[Key_address_id] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_address_id] = this.address_id;
    return data;
  }

  @override
  String toString() {
    return 'AreaDefaultRequest{address_id: $address_id}';
  }
}
