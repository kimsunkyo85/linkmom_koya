import 'package:json_annotation/json_annotation.dart';

import 'header_response.dart';

@JsonSerializable()
class RegisterDiResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<DiData>? dataList;

  RegisterDiResponse({this.dataHeader, this.dataList});

  ///성공시에만 사용할것
  DiData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : DiData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();
  factory RegisterDiResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse header = HeaderResponse.fromJson(json);
    List<DiData> data = json[Key_data] == null ? [] : (json[Key_data] as List).map((e) => DiData.fromJson(e)).toList();

    return RegisterDiResponse(
      dataHeader: header,
      dataList: data,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dataList != null) data[Key_data] = this.dataList!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'RegisterDiResponse {${{
      'dataHeader': dataHeader,
      'dataList': dataList,
    }}}';
  }
}

class DiData {
  static const String Key_is_rejoin = 'is_rejoin';

  @JsonKey(name: Key_is_rejoin)
  bool isRejoin;

  DiData({this.isRejoin = false});

  factory DiData.fromJson(Map<String, dynamic> json) {
    return DiData(isRejoin: json[Key_is_rejoin] ?? false);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_is_rejoin] = this.isRejoin;
    return data;
  }

  @override
  String toString() {
    return 'DiData {${{'isRejoin': isRejoin}}}';
  }
}
