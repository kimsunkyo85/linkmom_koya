import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:json_annotation/json_annotation.dart';

///- All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///- Created by   : platformbiz@ggumbi.com
///- version      : 1.0.0
///- see          : pay_cancel_save_request.dart - 결제취소 요청 저장 PUT
///- since        : 2021/09/08 / update:
///
///> (실패)<br/>
/// "msg_cd": 5040, "message": "취소 할 수 없는 결제 입니다.",<br/>
/// "msg_cd": 5041, "message": "이미 취소된 결제 입니다.",<br/>
/// "msg_cd": 4000, "message": "데이터가 없습니다.",<br/>
@JsonSerializable()
class PayCancelSaveRequest {
  static const String Key_order_id = 'order_id';
  static const String Key_contract_id = 'contract_id';
  static const String Key_request_cancel_price = 'request_cancel_price';
  static const String Key_cancelreason = 'cancelreason';
  static const String Key_cancelreason_msg = 'cancelreason_msg';
  static const String Key_cancelreason_img = 'cancelreason_img';
  static const String Key_carediary_id = 'carediary_id';

  PayCancelSaveRequest({
    this.order_id,
    this.contract_id,
    this.request_cancel_price,
    this.cancelreason,
    this.cancelreason_msg,
    this.cancelreason_img,
    this.carediary_id,
  });

  PayCancelSaveRequest.clone(PayCancelSaveRequest item)
      : this(
          order_id: item.order_id,
          contract_id: item.contract_id,
          request_cancel_price: item.request_cancel_price,
          cancelreason: item.cancelreason,
          cancelreason_msg: item.cancelreason_msg,
          cancelreason_img: item.cancelreason_img,
          carediary_id: item.carediary_id,
        );

  ///주문번호
  @JsonKey(name: Key_order_id)
  final String? order_id;

  ///계약서 번호
  @JsonKey(name: Key_contract_id)
  final int? contract_id;

  ///취소 요청 금액
  @JsonKey(name: Key_request_cancel_price)
  final int? request_cancel_price;

  ///취소사유
  @JsonKey(name: Key_cancelreason)
  int? cancelreason;

  ///취소사유내용
  @JsonKey(name: Key_cancelreason_msg)
  String? cancelreason_msg;

  ///취소사유이미지
  @JsonKey(name: Key_cancelreason_img)
  List<File>? cancelreason_img;

  ///돌봄일기
  @JsonKey(name: Key_carediary_id)
  final List<int>? carediary_id;

  factory PayCancelSaveRequest.fromJson(Map<String, dynamic> json) {
    List<int> carediaryId = json[Key_carediary_id] == null ? [] : List<int>.from(json[Key_carediary_id]);
    return PayCancelSaveRequest(
      order_id: json[Key_order_id] ?? '',
      contract_id: json[Key_contract_id] as int,
      request_cancel_price: json[Key_request_cancel_price] as int,
      cancelreason: json[Key_cancelreason] as int,
      cancelreason_msg: json[Key_cancelreason_msg] ?? '',
      cancelreason_img: json[Key_cancelreason_img],
      carediary_id: carediaryId,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_order_id] = this.order_id;
    data[Key_contract_id] = this.contract_id;
    data[Key_request_cancel_price] = this.request_cancel_price;
    data[Key_cancelreason] = this.cancelreason;
    data[Key_cancelreason_msg] = this.cancelreason_msg;
    data[Key_cancelreason_img] = this.cancelreason_img;
    data[Key_carediary_id] = this.carediary_id;
    return data;
  }

  Future<FormData> toFormData() async {
    FormData formData = FormData();
    if (order_id != null) {
      formData.fields.add(MapEntry(Key_order_id, order_id.toString()));
    }
    if (contract_id != null) {
      formData.fields.add(MapEntry(Key_contract_id, contract_id.toString()));
    }
    if (request_cancel_price != null) {
      formData.fields.add(MapEntry(Key_request_cancel_price, request_cancel_price.toString()));
    }
    if (cancelreason != null) {
      formData.fields.add(MapEntry(Key_cancelreason, cancelreason.toString()));
    }
    if (cancelreason_msg != null) {
      formData.fields.add(MapEntry(Key_cancelreason_msg, cancelreason_msg.toString()));
    }
    if (cancelreason_img != null && cancelreason_img!.isNotEmpty) {
      cancelreason_img!.forEach((value) async {
        String fileName = value.path.split('/').last;
        formData.files.add(MapEntry(Key_cancelreason_img, await MultipartFile.fromFile(value.path, filename: fileName, contentType: MediaType('image', 'jpeg'))));
      });
    }

    if (carediary_id != null && carediary_id!.isNotEmpty) {
      carediary_id!.forEach((value) {
        formData.fields.add(MapEntry(Key_carediary_id, value.toString()));
      });
    }
    return formData;
  }

  @override
  String toString() {
    return 'PayCanceSaveRequest{order_id: $order_id, contract_id: $contract_id, request_cancel_price: $request_cancel_price, cancelreason: $cancelreason, cancelreason_msg: $cancelreason_msg, cancelreason_img: $cancelreason_img}';
  }
}
