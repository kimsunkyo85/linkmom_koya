import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class LinkmomWishSaveResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  LinkmomWishSaveResponse({
    this.dataHeader,
    dataList,
  });

  LinkmomWishSaveResponse.init() {
    dataHeader = HeaderResponse();
    dataList = [];
  }

  LinkmomWishSaveData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : LinkmomWishSaveData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory LinkmomWishSaveResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<LinkmomWishSaveData> datas = [];
    try {
      log.d('『GGUMBI』>>> fromJson ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<<${json[Key_data]} ');
      datas = json[Key_data].map<LinkmomWishSaveData>((i) => LinkmomWishSaveData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return LinkmomWishSaveResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'LinkmomWishSaveResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class LinkmomWishSaveData {
  static const String Key_id = 'id';
  static const String Key_bookingcare = 'bookingcare';
  static const String Key_createdate = 'createdate';

  LinkmomWishSaveData({
    this.id = 0,
    this.bookingcare = 0,
    this.createdate = '',
  });

  @JsonKey(name: Key_id)
  int id;

  @JsonKey(name: Key_bookingcare)
  int bookingcare;
  
  @JsonKey(name: Key_createdate)
  String createdate;

  factory LinkmomWishSaveData.fromJson(Map<String, dynamic> json) {
    return LinkmomWishSaveData(
      id: json[Key_id] as int,
      bookingcare: json[Key_bookingcare] as int,
      createdate: json[Key_createdate] ?? '',
    );
  }

  @override
  String toString() {
    return 'BookingSaveData: {id: $id, bookingcare: $bookingcare, createdate: $createdate}';
  }
}
