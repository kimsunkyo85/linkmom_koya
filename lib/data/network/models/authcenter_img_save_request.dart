import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../../main.dart';

class AuthcenterImgSaveRequest {
  static const String Key_auth_imgtype = 'auth_imgtype';
  static const String Key_auth_picture = 'auth_picture';

  AuthcenterImgSaveRequest({
    this.auth_imgtype = 0,
    this.auth_picture,
  });

  @JsonKey(name: Key_auth_imgtype)
  final int auth_imgtype;
  @JsonKey(name: Key_auth_picture)
  final File? auth_picture;

  factory AuthcenterImgSaveRequest.fromJson(Map<String, dynamic> json) {
    return AuthcenterImgSaveRequest(
      auth_imgtype: json[Key_auth_imgtype] as int,
      auth_picture: json[Key_auth_picture],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_auth_imgtype] = this.auth_imgtype;
    data[Key_auth_picture] = this.auth_picture;
    return data;
  }

  Future<FormData> toFormData(FormData formData, {String? encImg}) async {
    formData.fields.add(MapEntry(Key_auth_imgtype, this.auth_imgtype.toString()));
    if (encImg != null) {
      formData.files.add(MapEntry(Key_auth_picture, MultipartFile.fromString(encImg, filename: DateTime.now().toIso8601String(), contentType: MediaType('image', 'jpeg'))));
    } else {
      var file = await encryptHelper.encodeData(this.auth_picture);
      formData.files.add(MapEntry(Key_auth_picture, MultipartFile.fromString(file, filename: this.auth_picture!.path.split('/').last, contentType: MediaType('image', 'jpeg'))));
    }
    log.d('『GGUMBI』>>> toFormData : formData.fields: ${formData.fields},  <<< ');
    return formData;
  }

  @override
  String toString() {
    return 'AuthImageUploadRequest{auth_imgtype: $auth_imgtype, auth_picture:$auth_picture}';
  }
}
