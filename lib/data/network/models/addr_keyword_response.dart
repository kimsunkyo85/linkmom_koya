import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class AddrKeywordResponse {
  @JsonKey(name: 'documents')
  List<KeywordDocuments>? documents;
  @JsonKey(name: 'meta')
  Meta? meta;

  AddrKeywordResponse({this.documents, this.meta});

  factory AddrKeywordResponse.fromJson(Map<String, dynamic> json) {
    return AddrKeywordResponse(
      documents: json["documents"] == null ? [] : (json["documents"] as List).map((e) => KeywordDocuments.fromJson(e)).toList(),
      meta: json["meta"] == null ? Meta() : Meta.fromJson(json["meta"]),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.documents != null) data["documents"] = this.documents!.map((e) => e.toJson()).toList();
    if (this.meta != null) data["meta"] = this.meta!.toJson();
    return data;
  }
}

class Meta {
  bool isEnd;
  int pageableCount;
  SameName? sameName;
  int totalCount;

  Meta({this.isEnd = true, this.pageableCount = 0, this.sameName, this.totalCount = 0});

  factory Meta.fromJson(Map<String, dynamic> json) {
    return Meta(
      isEnd: json["is_end"] ?? true,
      pageableCount: json["pageable_count"] ?? 0,
      sameName: json["same_name"] == null ? SameName() : SameName.fromJson(json["same_name"]),
      totalCount: json["total_count"] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["is_end"] = this.isEnd;
    data["pageable_count"] = this.pageableCount;
    if (this.sameName != null) data["same_name"] = this.sameName!.toJson();
    data["total_count"] = this.totalCount;
    return data;
  }
}

class SameName {
  String keyword;
  List<String>? region;
  String selectedRegion;

  SameName({this.keyword = '', this.region, this.selectedRegion = ''});

  factory SameName.fromJson(Map<String, dynamic> json) {
    return SameName(
      keyword: json["keyword"],
      region: json["region"] != null ? List<String>.from(json["region"]) : [],
      selectedRegion: json["selected_region"],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["keyword"] = this.keyword;
    if (this.region != null) data["region"] = this.region;
    data["selected_region"] = this.selectedRegion;
    return data;
  }
}

class KeywordDocuments {
  String addressName;
  String categoryGroupCode;
  String categoryGroupName;
  String categoryName;
  String distance;
  String id;
  String phone;
  String placeName;
  String placeUrl;
  String roadAddressName;
  String x;
  String y;

  KeywordDocuments(
      {this.addressName = '',
      this.categoryGroupCode = '',
      this.categoryGroupName = '',
      this.categoryName = '',
      this.distance = '',
      this.id = '',
      this.phone = '',
      this.placeName = '',
      this.placeUrl = '',
      this.roadAddressName = '',
      this.x = '',
      this.y = ''});

  factory KeywordDocuments.fromJson(Map<String, dynamic> json) {
    return KeywordDocuments(
      addressName: json["address_name"] ?? '',
      categoryGroupCode: json["category_group_code"] ?? '',
      categoryGroupName: json["category_group_name"] ?? '',
      categoryName: json["category_name"] ?? '',
      distance: json["distance"] ?? '',
      id: json["id"] ?? '',
      phone: json["phone"] ?? '',
      placeName: json["place_name"] ?? '',
      placeUrl: json["place_url"] ?? '',
      roadAddressName: json["road_address_name"] ?? '',
      x: json["x"] ?? '',
      y: json["y"] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["address_name"] = this.addressName;
    data["category_group_code"] = this.categoryGroupCode;
    data["category_group_name"] = this.categoryGroupName;
    data["category_name"] = this.categoryName;
    data["distance"] = this.distance;
    data["id"] = this.id;
    data["phone"] = this.phone;
    data["place_name"] = this.placeName;
    data["place_url"] = this.placeUrl;
    data["road_address_name"] = this.roadAddressName;
    data["x"] = this.x;
    data["y"] = this.y;
    return data;
  }
}
