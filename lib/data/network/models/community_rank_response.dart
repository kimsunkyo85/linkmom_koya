import 'package:json_annotation/json_annotation.dart';

import 'community_list_response.dart';
import 'header_response.dart';

@JsonSerializable()
class CommunityRankResponse {
  HeaderResponse? dataHeader;
  List<RankListData>? dataList;

  CommunityRankResponse({this.dataHeader, this.dataList});

  factory CommunityRankResponse.fromJson(Map<String, dynamic> json) {
    return CommunityRankResponse(dataHeader: HeaderResponse.fromJson(json), dataList: json[Key_data] == null ? null : (json[Key_data] as List).map((e) => RankListData.fromJson(e)).toList());
  }
  RankListData getData() => dataList != null && dataList!.isNotEmpty ? dataList!.first : RankListData();

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_data] = this.dataList;
    return data;
  }

  @override
  String toString() {
    return 'CommunityRankResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

class RankListData {
  static const String Key_count = 'count';
  static const String Key_next = 'next';
  static const String Key_previous = 'previous';
  static const String Key_results = 'results';
  static const String Key_region = 'region';

  @JsonKey(name: Key_count)
  int count;
  @JsonKey(name: Key_next)
  String next;
  @JsonKey(name: Key_previous)
  String previous;
  @JsonKey(name: Key_results)
  List<RankResults>? results;
  @JsonKey(name: Key_region)
  List<Region>? region;

  RankListData({this.count = 0, this.next = '', this.previous = '', this.results, this.region});

  factory RankListData.fromJson(Map<String, dynamic> json) {
    return RankListData(
      count: json[Key_count] ?? 0,
      next: json[Key_next] ?? '',
      previous: json[Key_previous] ?? '',
      results: json[Key_results] != null && json[Key_results].isNotEmpty ? (json[Key_results] as List).map((e) => RankResults.fromJson(e)).toList() : [],
      region: json[Key_region] != null && json[Key_region].isNotEmpty ? (json[Key_region] as List).map((e) => Region.fromJson(e)).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_count] = this.count;
    data[Key_next] = this.next;
    data[Key_previous] = this.previous;
    if (this.results != null) data[Key_results] = this.results!.map((e) => e.toJson()).toList();
    if (this.region != null) data[Key_region] = this.region!.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  String toString() {
    return 'RankListData{count: $count, next: $next, previous: $previous, results:  $results, region: $region}';
  }
}

class RankResults {
  static const String Key_userinfo = 'userinfo';
  static const String Key_rankinginfo = 'rankinginfo';

  @JsonKey(name: Key_userinfo)
  Userinfo? userinfo;
  @JsonKey(name: Key_rankinginfo)
  Rankinginfo? rankinginfo;

  RankResults({this.userinfo, this.rankinginfo});

  factory RankResults.fromJson(Map<String, dynamic> json) {
    return RankResults(
      userinfo: json[Key_userinfo] == null ? Userinfo() : Userinfo.fromJson(json[Key_userinfo]),
      rankinginfo: json[Key_rankinginfo] == null ? Rankinginfo() : Rankinginfo.fromJson(json[Key_rankinginfo]),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userinfo != null) data[Key_userinfo] = this.userinfo!.toJson();
    if (this.rankinginfo != null) data[Key_rankinginfo] = this.rankinginfo!.toJson();
    return data;
  }

  @override
  String toString() {
    return 'Results{userinfo: $userinfo, rankinginfo: $rankinginfo}';
  }
}

class Rankinginfo {
  static const String Key_price_sum = 'price_sum';
  static const String Key_like_cnt = 'like_cnt';
  static const String Key_review_cnt = 'review_cnt';
  static const String Key_increase = 'increase';

  @JsonKey(name: Key_price_sum)
  int priceSum;
  @JsonKey(name: Key_like_cnt)
  int likeCnt;
  @JsonKey(name: Key_review_cnt)
  int reviewCnt;
  @JsonKey(name: Key_increase)
  int increase;

  Rankinginfo({this.priceSum = 0, this.likeCnt = 0, this.reviewCnt = 0, this.increase = 0});

  factory Rankinginfo.fromJson(Map<String, dynamic> json) {
    return Rankinginfo(
      priceSum: json[Key_price_sum] ?? 0,
      likeCnt: json[Key_like_cnt] ?? 0,
      reviewCnt: json[Key_review_cnt] ?? 0,
      increase: json[Key_increase] ?? 0,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_price_sum] = this.priceSum;
    data[Key_like_cnt] = this.likeCnt;
    data[Key_review_cnt] = this.reviewCnt;
    data[Key_increase] = this.increase;
    return data;
  }

  @override
  String toString() {
    return 'Rankinginfo{priceSum: $priceSum, likeCnt: $likeCnt, reviewCnt: $reviewCnt, increase: $increase}';
  }
}

class Userinfo {
  static const String Key_user_id = 'user_id';
  static const String Key_first_name = 'first_name';
  static const String Key_gender = 'gender';
  static const String Key_age = 'age';
  static const String Key_job = 'job';
  static const String Key_current_region_3depth = 'current_region_3depth';
  static const String Key_profileimg = 'profileimg';
  static const String Key_hcode = 'hcode';

  @JsonKey(name: Key_user_id)
  int userId;
  @JsonKey(name: Key_first_name)
  String firstName;
  @JsonKey(name: Key_gender)
  String gender;
  @JsonKey(name: Key_age)
  String age;
  @JsonKey(name: Key_job)
  String job;
  @JsonKey(name: Key_current_region_3depth)
  String currentRegion3depth;
  @JsonKey(name: Key_profileimg)
  String profileimg;
  @JsonKey(name: Key_hcode)
  int hcode;

  Userinfo({this.userId = 0, this.firstName = '', this.gender = '', this.age = '', this.job = '', this.currentRegion3depth = '', this.profileimg = '', this.hcode = 0});

  factory Userinfo.fromJson(Map<String, dynamic> json) {
    return Userinfo(
      userId: json[Key_user_id] ?? 0,
      firstName: json[Key_first_name] ?? '',
      gender: json[Key_gender] ?? '',
      age: json[Key_age] ?? '',
      job: json[Key_job] ?? '',
      currentRegion3depth: json[Key_current_region_3depth] ?? '',
      profileimg: json[Key_profileimg] ?? '',
      hcode: int.parse(json[Key_hcode] ?? 0),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_user_id] = this.userId;
    data[Key_first_name] = this.firstName;
    data[Key_gender] = this.gender;
    data[Key_age] = this.age;
    data[Key_job] = this.job;
    data[Key_current_region_3depth] = this.currentRegion3depth;
    data[Key_profileimg] = this.profileimg;
    data[Key_hcode] = this.hcode;
    return data;
  }

  @override
  String toString() {
    return 'Userinfo{userId: $userId, firstName: $firstName, gender: $gender, age: $age, job: $job, region3Depth: $currentRegion3depth, profileimg: $profileimg, hcode: $hcode}';
  }
}
