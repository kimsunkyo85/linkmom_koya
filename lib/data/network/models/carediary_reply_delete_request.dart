import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CareDiaryReplyDeleteRequest {
  static const String Key_id = 'id';

  CareDiaryReplyDeleteRequest({this.id = 0});

  @JsonKey(name: Key_id)
  final int id;

  factory CareDiaryReplyDeleteRequest.fromJson(Map<String, dynamic> json) {
    return CareDiaryReplyDeleteRequest(id: json[Key_id]);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    return data;
  }

  @override
  String toString() {
    return 'CareDiaryDeleteRequest{id: $id}';
  }
}
