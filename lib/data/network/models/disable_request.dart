import 'package:dio/dio.dart';
import 'package:json_annotation/json_annotation.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : disable_request.dart - 회원탈퇴
/// since        : 1/18/21 / update:
@JsonSerializable()
class DisableRequest {
  static const String Key_account = 'account';
  static const String Key_reason = 'reason';
  static const String Key_other_reason = 'other_reason';
  static const String Key_account_bank = 'account_bank';

  DisableRequest({this.account = '', this.reason, this.other_reason = '', this.account_bank = ''});

  @JsonKey(name: Key_account)
  String account;
  @JsonKey(name: Key_reason)
  List<int>? reason;
  @JsonKey(name: Key_other_reason)
  String other_reason;
  @JsonKey(name: Key_account_bank)
  String account_bank;

  factory DisableRequest.fromJson(Map<String, dynamic> json) {
    return DisableRequest(
      account: json[Key_account] ?? '',
      reason: json[Key_reason] != null && json[Key_reason].isNotEmpty ? List.from(json[Key_reason]) : [],
      other_reason: json[Key_other_reason] ?? '',
      account_bank: json[Key_account_bank] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_account] = this.account;
    data[Key_reason] = this.reason;
    if (this.other_reason.isNotEmpty) data[Key_other_reason] = this.other_reason;
    data[Key_account_bank] = this.account_bank;
    return data;
  }

  FormData toFormData(FormData formData) {
    if (this.account.isNotEmpty) formData.fields.add(MapEntry(Key_account, this.account));
    if (this.account_bank.isNotEmpty) formData.fields.add(MapEntry(Key_account_bank, this.account_bank));
    this.reason!.forEach((e) {
      formData.fields.add(MapEntry(Key_reason, e.toString()));
    });
    if (this.other_reason.isNotEmpty) formData.fields.add(MapEntry(Key_other_reason, this.other_reason));
    return formData;
  }

  @override
  String toString() {
    return 'DisableRequest{account: $account, reason: $reason, other_reason: $other_reason, account_bank: $account_bank}';
  }
}
