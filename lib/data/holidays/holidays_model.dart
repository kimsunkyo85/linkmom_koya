import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class Holiday {
  static const String Key_version = 'version';
  static const String Key_holiday = 'holiday';

  Holiday({required this.version, required this.holiday});

  @JsonKey(name: Key_version)
  String version;

  @JsonKey(name: Key_holiday)
  List<EventData> holiday;

  factory Holiday.fromJson(Map<String, dynamic> json) {
    List<EventData> datas = [];
    try {
      datas = json[Key_holiday].map<EventData>((e) => EventData.fromJson(e)).toList();
    } catch (e) {}
    return Holiday(version: json[Key_version] ?? '', holiday: datas);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_version] = this.version;
    data[Key_holiday] = this.holiday;
    return data;
  }

  @override
  String toString() {
    return 'Holiday{version: $version, holiday:$holiday}';
  }
}

class EventData {
  static const String Key_dateName = 'dateName';
  static const String Key_date = 'date';

  EventData({this.dateName = '', this.date = ''});

  @JsonKey(name: Key_dateName)
  String dateName;

  @JsonKey(name: Key_date)
  String date;

  factory EventData.fromJson(Map<String, dynamic> json) {
    return EventData(dateName: json[Key_dateName] ?? '', date: json[Key_date] ?? '');
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_dateName] = this.dateName;
    data[Key_date] = this.date;
    return data;
  }

  @override
  String toString() {
    return 'EventData{dateName: $dateName, date: $date}';
  }
}

class CalendarEvents {
  static const String Key_date = 'date';
  static const String Key_dateNum = 'dateNum';

  CalendarEvents({this.dateNum, this.date});

  @JsonKey(name: Key_dateNum)
  List<int>? dateNum;

  @JsonKey(name: Key_date)
  DateTime? date;

  factory CalendarEvents.fromJson(Map<String, dynamic> json) {
    List<int> datas = [];
    DateTime date = DateTime.now();
    try {
      datas.add(0);
      date = DateFormat().parse(json[Key_date]);
    } catch (e) {}
    return CalendarEvents(date: date, dateNum: datas);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_date] = this.date;
    data[Key_dateNum] = this.dateNum;
    return data;
  }
}
