import 'dart:convert';
import 'dart:io';

import 'package:linkmom/data/holidays/holidays_model.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';

class HolidaysHelper {
  static Map<DateTime, List<int>> _holidays = Map<DateTime, List<int>>();

  static Future<void> loadHolidays() async {
    try {
      if (storageHelper.holiday.isNotEmpty) {
        String path = await Commons.findLocalPath();
        await _load(path, storageHelper.holiday.first);
        await _load(path, storageHelper.holiday.last);
        log.d("Holiday is loaded: ${_holidays.isNotEmpty}");
      }
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
  }

  static Map<DateTime, List<int>> get getHolidays {
    if (_holidays.isEmpty) loadHolidays();
    return _holidays;
  }

  static Future<void> _load(String path, String fileName) async {
    File file = File('$path/$fileName');
    if (file.isAbsolute) {
      var data = await file.readAsString();
      var b = Holiday.fromJson(json.decode(data));
      log.d("$fileName is ${b.holiday.isNotEmpty}");
      b.holiday.forEach((element) {
        String y = element.date.substring(0, 4);
        String m = element.date.substring(4, 6);
        String d = element.date.substring(6, 8);
        DateTime key = StringUtils.parseYMD('$y-$m-$d');
        _holidays[key] = [0];
      });
    }
  }
}
