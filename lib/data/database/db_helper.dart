import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../../main.dart';
import 'model/chat_model.dart';

final String TABLE_CHAT = 'Chat';

final int VERSION = 1;

class DBHelper {
  DBHelper._();

  static final DBHelper _db = DBHelper._();

  factory DBHelper() => _db;

  static late Database _database;

  String PRIMARY_KEY = '${Chat.Key_id} = ?';

  Future<Database> get database async {
    log.d('『GGUMBI』>>> database : _database: $_database,  <<< ');

    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'LinkMom.db');
    log.d('『GGUMBI』>>> initDB : path: $path,  <<< ');

    return await openDatabase(path, version: VERSION, onCreate: (db, version) async {
      await db.execute('CREATE TABLE $TABLE_CHAT (${Chat.Key_id} INTEGER PRIMARY KEY, ${Chat.Key_name} TEXT, ${Chat.Key_fromName} TEXT, ${Chat.Key_type} INTEGER, ${Chat.Key_isRead} BIT, ${Chat.Key_content} TEXT, ${Chat.Key_fromImg} TEXT, ${Chat.Key_dateTime} TEXT)');
      // await db.execute(
      //     '''
      //     CREATE TABLE $TABLE_CHAT ${Chat.Key_id} INTEGER PRIMARY KEY,
      //     ${Chat.Key_idTo} TEXT,
      //     ${Chat.Key_idFrom} TEXT,
      //     ${Chat.Key_type} INTEGER,
      //     ${Chat.Key_isRead} BIT,
      //     ${Chat.Key_content} TEXT,
      //     ${Chat.Key_img} TEXT,
      //     ${Chat.Key_time} TEXT
      //     ''');
      // db.execute('CREATE TABLE tasks(생성할 column 이름 자료형)');
    }, onUpgrade: (db, oldVersion, newVersion) {
      if (newVersion == 2) {
        db.execute('CREATE TABLE ${TABLE_CHAT}2 (${Chat.Key_id} INTEGER PRIMARY KEY, ${Chat.Key_name} TEXT, ${Chat.Key_fromName} TEXT, ${Chat.Key_type} INTEGER, ${Chat.Key_isRead} BIT, ${Chat.Key_content} TEXT, ${Chat.Key_fromImg} TEXT, ${Chat.Key_dateTime} TEXT)');
      }
      log.d('『GGUMBI』>>> initDB : oldVersion: $oldVersion, newVersion: $newVersion, <<< ');
    });
  }

  ///Insert
  insertData(Chat chat) async {
    final db = await database;
    var res = await db.insert(TABLE_CHAT, chat.toJson());
    log.d('『GGUMBI』>>> insertData : res: $res,  <<< ');
    return res;
  }

  ///Read
  getChat(int id) async {
    final db = await database;
    var res = await db.query(TABLE_CHAT, where: PRIMARY_KEY, whereArgs: [id]);
    log.d('『GGUMBI』>>> getChat : res: $res,  <<< ');
    return res.isNotEmpty ? Chat.fromJson(res.first) : Null;
  }

  ///Read All
  Future<List<Chat>> getAllChats() async {
    final db = await database;
    log.d('『GGUMBI』>>> getAllChats : db: ${await db.query('${TABLE_CHAT}2')},  <<< ');
    var res = await db.query(TABLE_CHAT);
    log.d('『GGUMBI』>>> getAllChats : res: $res,  <<< ');
    List<Chat> list = res.isNotEmpty ? res.map((c) => Chat.fromJson(c)).toList() : [];
    log.d('『GGUMBI』>>> getAllChats : list: ${list.length},  <<< ');
    return list;
  }

  ///Update
  updateChat(Chat chat) async {
    final db = await database;
    var res = await db.update(TABLE_CHAT, chat.toJson(), where: PRIMARY_KEY, whereArgs: [chat.id]);
    log.d('『GGUMBI』>>> updateChat : res: $res,  <<< ');
    return res;
  }

  ///Delete
  deleteChat(int id) async {
    final db = await database;
    var res = await db.delete(TABLE_CHAT, where: PRIMARY_KEY, whereArgs: [id]);
    log.d('『GGUMBI』>>> deleteChat : res: $res,  <<< ');
    return res;
  }

  ///Delete All
  deleteAllChats() async {
    final db = await database;
    db.rawDelete('DELETE FROM $TABLE_CHAT');
  }
}
