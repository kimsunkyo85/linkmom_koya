//lib/sqlite/models/chat_model.dart

import 'package:linkmom/manager/enum_manager.dart';

class Chat {
  static const String Key_id = 'id';
  static const String Key_name = 'name';
  static const String Key_fromName = 'fromName';
  static const String Key_fromImg = 'fromImg';
  static const String Key_type = 'type';
  static const String Key_content = 'content';
  static const String Key_isRead = 'isRead';
  static const String Key_dateTime = 'dateTime';
  static const String Key_isDelete = 'isDelete';

  final int id;
  final String name; //primary key
  final String fromName;
  final String fromImg;
  final ChatMsgType type;
  final bool isRead;
  final String content;
  final String dateTime;
  bool isDelete;

  Chat({
    this.id = 0,
    this.name = '',
    this.fromName = '',
    this.fromImg = '',
    this.type = ChatMsgType.text,
    this.isRead = false,
    this.content = '',
    this.dateTime = '',
    this.isDelete = false,
  });

  factory Chat.fromJson(Map<String, dynamic> json) {
    bool isRead = json[Key_isRead] == 0 ? false : true;
    bool isDelete = json[Key_isDelete] == 0 ? false : true;
    ChatMsgType type = ChatMsgType.text;
    if (json[Key_type] == 0) {
      type = ChatMsgType.text;
    } else if (json[Key_type] == 1) {
      type = ChatMsgType.image;
    } else if (json[Key_type] == 2) {
      type = ChatMsgType.matching;
    } else if (json[Key_type] == 3) {
      type = ChatMsgType.careInfo;
    } else {
      type = ChatMsgType.noData;
    }
    return Chat(
      id: json[Key_id] as int,
      name: json[Key_name] ?? '',
      fromName: json[Key_fromName] ?? '',
      fromImg: json[Key_fromImg] ?? '',
      type: type,
      isRead: isRead,
      content: json[Key_content] ?? '',
      dateTime: json[Key_dateTime] ?? '',
      isDelete: isDelete,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_name] = this.name;
    data[Key_fromName] = this.fromName;
    data[Key_fromImg] = this.fromImg;
    data[Key_type] = this.type;
    data[Key_isRead] = this.isRead;
    data[Key_content] = this.content;
    data[Key_dateTime] = this.dateTime;
    data[Key_isDelete] = this.isDelete;
    return data;
  }

  @override
  String toString() {
    return 'Chat{id: $id, name: $name, fromName: $fromName, fromImg: $fromImg, type: $type, isRead: $isRead, content: $content, dateTime: $dateTime, isDelete: $isDelete}';
  }
}
