import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:encrypt/encrypt.dart';
import 'package:linkmom/data/network/api_helper.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:pointycastle/asymmetric/api.dart';

import '../../main.dart';

// final aesKey = '4dhsEfZN3qeW0h0y';
// final aesKey = Key.fromSecureRandom(16);
// final aseKey = Key('4dhsEfZN3qeW0h0y');//테스트용
// final aseKey = Key(bytes);

class EncryptHelper {
  static final parser = RSAKeyParser();
  static const String KEY_PUBLIC = "public.pem";
  static const String KEY_PRIVATE = "private.pem";

  var encrypterRSA;

  late Key aesKey;
  var encrypterAES;

  ///RSA 키 생성 [Commons.doLoginFlow]에서 생성한다.
  createRSAKey() async {
    log.d('『GGUMBI』>>> createRSAKey : publicKeyString:  <<< ');
    String savePath = '${await Commons.findLocalPath()}/${storageHelper.pku}';
    File file = File(savePath);
    String publicKeyString = '';
    //파일이있으면...없으면 요청한다... API 추가시 재작업~!
    if (file.isAbsolute) {
      try {
        publicKeyString = await file.readAsString();
      } catch (e) {
        log.e('『GGUMBI』 Exception >>> encodeData : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      }
    }

    ///public key get
    try {
      RSAAsymmetricKey key = parser.parse(publicKeyString);
      RSAPublicKey publicKey = RSAPublicKey(key.modulus!, key.exponent!);
      encrypterRSA = Encrypter(RSA(publicKey: publicKey, encoding: RSAEncoding.OAEP));
    } catch (e) {
      log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  /// RSA 암호화 (data type = File or Map)
  Future<String> encodeData(dynamic data) async {
    if (encrypterRSA == null) await createRSAKey();
    List<int> dataList = [];
    if (data is File) {
      dataList = await data.readAsBytes();
    } else if (data is Map || data is List) {
      final jsonStr = jsonEncode(data);
      dataList = utf8.encode(jsonStr);
    } else {
      dataList = utf8.encode(data);
    }

    //데이터 전체 크기
    int inputLen = dataList.length;
    //342 byte cut
    int maxLen = 342;
    //encode data
    List<int> totalBytes = [];
    for (var i = 0; i < inputLen; i += maxLen) {
      int endLen = inputLen - i;
      log.d('『GGUMBI』>>> encodeLong2 : endLen: $i, $inputLen, $endLen, ${i + endLen}, ${i + maxLen} <<< ');
      List<int> item;
      if (endLen > maxLen) {
        item = dataList.sublist(i, i + maxLen);
      } else {
        item = dataList.sublist(i, i + endLen);
      }
      totalBytes.addAll(encrypterRSA.encryptBytes(item).bytes);
    }
    String encodeData = base64.encode(totalBytes);
    log.d('『GGUMBI』>>> encodeLong : encodeLong: $encodeData,  <<< ');

    return encodeData;
  }

  ///AES 키 생성하기 [ApiHelper.requestLogin]
  String crateKey() {
    try {
      String randomString = createCryptoRandomString();
      log.d('『GGUMBI』>>> crateKey : key: $randomString,  <<< ');
      aesKey = Key.fromUtf8(randomString);
      encrypterAES = Encrypter(AES(aesKey, mode: AESMode.cbc));
      log.d('『GGUMBI』>>> crateKey : aesKey: ${aesKey.base64}, ${utf8.decode(aesKey.bytes)} <<< ');
      return utf8.decode(aesKey.bytes);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return '';
  }

  String createCryptoRandomString([int length = 16]) {
    Random _random = Random.secure();
    // var values = List<int>.generate(length, (i) => _random.nextInt(256));
    // return utf8.decode(values);
    // String randomString = String.fromCharCodes(List.generate(16, (index)=> random.nextInt(33) + 89));
    const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1!2@3#45%6^7&8*9(0)-_=+[],.;:';
    return List.generate(length, (index) => _chars[_random.nextInt(_chars.length)]).join();
  }

  ///AES 암호화
  Future<dynamic> encrypt(dynamic data) async {
    List<int> dataList = [];
    if (data is File) {
      dataList = await data.readAsBytes();
    } else if (data is Map || data is List) {
      final jsonStr = jsonEncode(data);
      dataList = utf8.encode(jsonStr);
    } else {
      dataList = utf8.encode(data);
    }
    IV iv = IV.fromBase64(base64.encode(dataList.sublist(0, 16)));
    final encrypted = encrypterAES.encrypt(base64.encode(dataList), iv: iv);
    return encrypted;
  }

  ///AES 복호화
  dynamic decrypt(dynamic data) {
    // # 첫 16바이트(block_size=BS) 를 잘라 iv를 만들고, 그 뒤를 복호화 하고자 하는 메세지로 잘라 만든다.
    // iv = enc[:self.BS]
    // encrypted_msg = enc[self.BS:]
    // cipher = AES.new(self.encrypt_key, AES.MODE_CBC, iv)
    // return self.unpad(cipher.decrypt(encrypted_msg)).decode('utf-8')
    //암호화된 메세지 자체가 (iv + 메세지)로 되어있기 때문에 앞에서 부터 BLOCK_SIZE 만큼 잘라내어 iv를 만들고
    //나머지 부분을 잘라내서 복호화한다.
    // Uint8List ivBytes = List.copyRange(msgBytes, 0, msgBytes, 0, 16);

    //test 데이터
    // data =
    //     "3Z8loO986SuoodeqDsh4F-EVgc4W3MsJV6WCogZpUEmw62UpSCGqFXxIXbrzmyT3OtoHlTSCOZUc40h76rS8JCI33yqi_NAb7NRQF6iDqgV9iSao7Q4hjjAGsR2Lxl1xtDrsi4H0iw77qsAgGcU-Q0CfYb69C4GWw_YvaiHPTnCyj2o8C_EBO6ElA8gLaMAGlsxd7tUOp3E6dTnwGoVXagptljrcc2f2QuNBPNs1NB6hiMdAOblMffBc5go0IHLSjz8c-fw8j0OsK76QIHsILFvlzretq4PtRAHbuf_s0hSUwXVV1uiq_kZ8vKFp1xJF6OxQueOGNjm8hcZWsMHtrVLrESOuTQ5B4x-yawLfrzuP_ZUXASzPCvpGm3Xv-aDI36EG0q5JhQPWtVf53hUamXq7obPywmsJNBRobqHUBZKXzwKXSkzi8TepDKZ1vuCJpfixIGgwdr5oH0dGzmRBgO3-VEgre2CHkc4voN-P_q5Tg8scJb9JiUhFVHYI3BPDvKWlngphEfU95qSK7P1oYL4HDzaT6vCxqjkZ-XvsF0BHSz3gsw51b-jjzGshdMBTsS1Z53ZSOnpVuuUZlpPeYrlw2df9FB5ODvpk0GOYB7X9_aaYJOh6rTZTHMauvuxoMqFeurrT5AF4dO-RBv2s68Mz53_l5f82SoN05t4s2N8ueHREnJ5Ef1aXhLzYqHYoUfKBXYDHYhOBr2lzm0jG8W71UZzmGNiWCVcFHA_7bgvNYqsGfFcG201cupBxMQrImLlhQaUQCmQCU_N5rFaRQ0yGcEcn97htkMOkgU4ki-2hhVQMF8AZXOj22ys-W_aXkwfzId0Y0XJ-gGHfLhXAT3Ds29LMoiApdWqImA256dGqfXDjTjuuit14mLgkzz3NKjtaIM4oQzzesHJ9jDEV9uYVooLt29XtV5KVRE5aUlgOkrNHSyfhoJoD7EVMhxk1d49JolyQv4_WESl0bC_2WstSTNm_IkyGIwuo_UgZzxtdHfNDMoUZbiDYKk7xgxLlmnUNruDCkIwe96Uch63B-1e1p8vPX30F32UxQBTTSOvSInGLuxcGZQJuCnBOqKmXPLpg0TJoJScjyqMOV7704d1Ebb2h4QUcpyUnkpZ4hM96vJAL-acs83HBOfegor9y";
    Map<String, dynamic> decryptData = {};
    var decrypted;
    try {
      var ivBytes = base64.decode(data);
      IV iv = IV.fromBase64(base64.encode(ivBytes.sublist(0, 16)));
      var inputData = base64.encode(ivBytes.sublist(iv.bytes.length, ivBytes.length));
      decrypted = encrypterAES.decrypt64(inputData, iv: iv);
      decrypted = jsonDecode(decrypted);
      log.d('『GGUMBI』>>> decrypt : data 2 : $decrypted,  <<< ');
      decryptData = decrypted;
      return decryptData;
    } catch (e) {
      log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      // NOTE: server many handled
      // if (decrypted is list) => return decrypted.
      // please refer to Commons.catchApi -> KEY_205_SUCCESS_ENCRYPTION
      return decryptData.isEmpty ? decrypted : decryptData;
    }
  }
}
