import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/storage/model/flag_data.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';

class Flags {
  static const String KEY_DIARY_PAY_POPUP = "KEY_DIARY_PAY_POPUP";
  static const String KEY_DIARY_CLAIM_DATE_POPUP = "KEY_DIARY_CLAIM_POPUP";
  static const String KEY_DIARY_CLAIM_ADMIN_RESP = "KEY_DIARY_CLAIM_ADMIN_RESP";
  static const String KEY_COVID_CHECK_ALERT = "KEY_COVID_CHECK_ALERT";
  static const String KEY_SHOW_LOGIN_TUTORIAL = "KEY_SHOW_LOGIN_TUTORIAL";
  static const String KEY_SHOW_LOGIN_INTRO = "KEY_SHOW_LOGIN_INTRO";
  static const String KEY_SHOW_CHAT_ALERT_NORMAL = "KEY_SHOW_CHAT_ALERT_NORMAL";
  static const String KEY_SHOW_CHAT_ALERT_LINKMOM = "KEY_SHOW_CHAT_ALERT_LINKMOM";
  static const String KEY_SHOW_CHAT_ALERT_MOMDADDY = "KEY_SHOW_CHAT_ALERT_MOMDADDY";
  static const String KEY_DIARY_L = "KEY_DIARY_L";
  static const String KEY_DIARY_M_BEFORE_WRITE = "KEY_DIARY_M_BEFORE_WRITE";
  static const String KEY_DIARY_M_AFTER_WRITE = "KEY_DIARY_M_AFTER_WRITE";
  static const String KEY_COMMUNITY_BOOKMARK = "KEY_COMMUNITY_BOOKMARK";
  static const String KEY_PUSH_MOMDADY = "KEY_PUSH_MOMDADY";
  static const String KEY_PUSH_LINKMOM = "KEY_PUSH_LINKMOM";
  static const String KEY_DEEPLINK = "KEY_DEEPLINK";

  static List<String> _flagCached = [];

  static void showDialog(BuildContext context, String key, String message, {int id = 0, Function(DialogAction)? onClickAction, bool showNo = true}) {
    try {
      if (!getFlag(key, id: id).isShown) {
        storageHelper.flags.state!.firstWhere((e) => e.name == key && e.id == id).isShown = true;
        showNormalDlg(
          context: context,
          message: message,
          color: Commons.getColor(),
          onClickAction: onClickAction,
          btnLeft: onClickAction != null ? "취소".tr() : '',
          btnRight: onClickAction != null ? "확인".tr() : '',
        );
      }
    } catch (e) {
      addFlag(key, id: id, isShown: true);
      showNormalDlg(
        context: context,
        message: message,
        color: Commons.getColor(),
        onClickAction: onClickAction,
        btnLeft: onClickAction != null ? "취소".tr() : '',
        btnRight: onClickAction != null ? "확인".tr() : '',
      );
    } finally {
      updateFlags();
    }
  }

  static StateFlag getFlag(String key, {int id = 0}) {
    return storageHelper.flags.state!.lastWhere((e) => e.name == key && e.id == id, orElse: () => StateFlag(key, id: id, isShown: false));
  }

  static bool addFlag(String key, {bool isShown = false, int id = 0}) {
    if (getFlag(key, id: id).isShown != isShown) {
      removeFlag(key, id: id);
    }
    storageHelper.setFlag(StateFlag(key, id: id, isShown: isShown));
    return true;
  }

  static bool removeFlag(String key, {int id = 0}) {
    try {
      storageHelper.flags.state!.removeWhere((e) => e.name == key && e.id == id);
      updateFlags();
      return true;
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- REMOVE FLAG ---------------',
        '--- DATA     ': e,
      });
      return false;
    }
  }

  static void updateFlags() => storageHelper.setFlags(storageHelper.flags);

  static void action(BuildContext context, String key, Function dialog, {int id = 0}) {
    try {
      StateFlag flag = getFlag(key, id: id);
      if (!flag.isShown) {
        dialog();
        if (hasFlag(flag)) {
          storageHelper.flags.state!.lastWhere((e) => e.name == flag.name && e.id == flag.id).isShown = true;
        } else {
          addFlag(key, id: id, isShown: true);
        }
      }
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- REMOVE FLAG ---------------',
        '--- DATA     ': e,
      });
      addFlag(key, id: id, isShown: false);
    } finally {
      updateFlags();
    }
  }

  static bool hasFlag(StateFlag flag) {
    StateFlag? state;
    try {
      state = storageHelper.flags.state!.lastWhere((e) => e.name == flag.name && e.id == flag.id);
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- REMOVE FLAG ---------------',
        '--- DATA     ': e,
      });
      state = null;
    }
    return state != null;
  }

  static bool isShowingCachedFlag(String key) {
    return !_flagCached.contains(key);
  }

  static void shownCachedFlag(String key) => _flagCached.add(key);
  
  static void removeCachedFlag(String key) => _flagCached.remove(key);

  static void showCachedFlag(String key, String msg) {
    if (!_flagCached.contains(key))
      showNormalDlg(
        message: msg,
        onClickAction: (a) => {if (a == DialogAction.yes) _flagCached.add(key)},
      );
  }
}
