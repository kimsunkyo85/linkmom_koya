import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:linkmom/data/network/models/settings_response.dart';
import 'package:linkmom/data/network/models/user.dart';
import 'package:linkmom/data/network/models/version_check_response.dart';
import 'package:linkmom/data/network/models/withdrawal_response.dart';
import 'package:linkmom/data/storage/model/flag_data.dart';
import 'package:linkmom/data/storage/model/menu_file.dart';
import 'package:linkmom/data/storage/storage_manager.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';

import '../../main.dart';
import 'flag_manage.dart';
import 'model/bank_model.dart';
import 'model/menu_file_data.dart';

class StorageHelper extends ChangeNotifier {
  final _storage = FlutterSecureStorage();

  bool _useBio = false;

  bool get isBioSetup => _useBio;

  void setBioSetup(bool value) async {
    _useBio = value;
    await _storage.write(key: KEY_SP_USER_BIO, value: value.toString()).then((storage) {
      // log.d('『GGUMBI』>>> setBioSetup : _useBio: $_useBio,  <<< ');
    });
    notifyListeners();
  }

  Future<bool> getBioSetup() async {
    try {
      await _storage.read(key: KEY_SP_USER_BIO).then((value) {
        _useBio = StringUtils.getStringToBool(value);
        // log.d('『GGUMBI』>>> getBioSetup : _useBio: $_useBio,  <<< ');
      });
    } catch (e) {
      log.e('『GGUMBI』>>> getBioSetup : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      return _useBio;
    }
    notifyListeners();
    return _useBio;
  }

  User _user = User();

  User get user => _user;

  Future<User> getUser() async {
    try {
      String? data = await _storage.read(key: KEY_SP_USER_DATA);
      // log.d('『GGUMBI』>>> getUser : _data: $data,  <<< ');
      _user = data == null ? User() : User.fromJson(json.decode(data));
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getUser : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return User();
    }
    notifyListeners();
    return _user;
  }

  void setUser(User data) async {
    _user = data;

    // log.d('『GGUMBI』>>> setUser : data: $data,  <<< ');
    var _save = json.encode(_user.toJson());
    await _storage.write(key: KEY_SP_USER_DATA, value: _save).then((value) {
      // log.d('『GGUMBI』>>> setUser : _user: $_user,  <<< ');
    });
    notifyListeners();
  }

  Future<User> setUserClear({bool isUpdate = true}) async {
    try {
      await _storage.write(key: KEY_SP_USER_DATA, value: null).then((value) {
        _user = User();
        // log.d('『GGUMBI』>>> setUserClear : _user: $_user,  <<< ');
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> setUserClear : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return _user;
    }
    if (isUpdate) notifyListeners();
    return _user;
  }

  USER_TYPE _user_type = USER_TYPE.mom_daddy;

  USER_TYPE get user_type => _user_type;

  bool _isUserType = false;

  bool get isUserType => _isUserType;

  void setUserType(USER_TYPE data, {Function? callBack}) async {
    // log.d('『GGUMBI』>>> setUserType : data: $data,  <<< ');
    _user_type = data;
    _user = auth.user;
    _isUserType = true;
    await _storage.write(key: KEY_SP_USER_TYPE, value: data.toString()).then((value) {
      // log.d('『GGUMBI』>>> setUserType : _user_type: $_user_type,  <<< ');
      if (callBack != null) {
        callBack(_user_type);
      }
    });
    notifyListeners();
  }

  Future<USER_TYPE> getUserType() async {
    try {
      await _storage.read(key: KEY_SP_USER_TYPE).then((value) {
        _user_type = StringUtils.validateUserType(value ?? USER_TYPE.mom_daddy.toString());
        // log.d('『GGUMBI』>>> getUserType : _user_type: $_user_type,  <<< ');
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getUserType : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return USER_TYPE.mom_daddy;
    }
    return _user_type;
  }

  String _csrfExpire = "";

  String get csrfExpire => _csrfExpire;

  ///CSRF Token 만료시간
  void setCsrfExpire(DateTime data) async {
    _csrfExpire = data.toString();
    // log.d('『GGUMBI』>>> setCsrfExpire : _csrfExpire: $_csrfExpire,  <<< ');
    await _storage.write(key: KEY_SP_CSRF_EXPIRE, value: data.toString()).then((value) {
      // log.d('『GGUMBI』>>> setCsrfExpire : data: $data,  <<< ');
    });
    notifyListeners();
  }

  ///CSRF Token 만료시간
  Future<String> getCsrfExpire() async {
    try {
      await _storage.read(key: KEY_SP_CSRF_EXPIRE).then((value) {
        _csrfExpire = value!;
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getCsrfExpire : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return _csrfExpire;
    }
    notifyListeners();
    return _csrfExpire;
  }

  int _userMomddady = AGREE_int;

  int get userParents => _userMomddady;

  ///부모님 회원
  void setMomdady(bool data) async {
    _userMomddady = StringUtils.getBoolToInt(data);
    await _storage.write(key: KEY_SP_USER_PARENTS, value: _userMomddady.toString()).then((value) {
      // log.d('『GGUMBI』>>> setParents : data: $data,  <<< ');
      auth.user.momdaddy = _userMomddady;
      auth.setUser(auth.user);
    });
    notifyListeners();
  }

  ///부모님 회원
  Future<int> getMomdady() async {
    try {
      await _storage.read(key: KEY_SP_USER_PARENTS).then((value) {
        // log.d('『GGUMBI』>>> getParents : value.toString(): ${value.toString()},  <<< ');
        if (value == null || value.length == 0) {
          value = AGREE;
        }
        _userMomddady = int.parse(value);
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getParents : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return _userMomddady;
    }
    notifyListeners();
    return _userMomddady;
  }

  int _userLinkmom = DISAAGREE_int;

  int get userLinkmom => _userLinkmom;

  ///돌보미 회원
  void setLinkmom(bool data) async {
    _userLinkmom = StringUtils.getBoolToInt(data);
    await _storage.write(key: KEY_SP_USER_DOLBOMI, value: _userLinkmom.toString()).then((value) {
      // log.d('『GGUMBI』>>> setDolbomi : data: $data,  <<< ');
      auth.user.linkmom = _userLinkmom;
      auth.setUser(auth.user);
    });
    notifyListeners();
  }

  ///돌보미 회원
  Future<int> getLinkmom() async {
    try {
      await _storage.read(key: KEY_SP_USER_DOLBOMI).then((value) {
        // log.d('『GGUMBI』>>> getDolbomi : value.toString(): ${value.toString()},  <<< ');
        if (value == null || value.length == 0) {
          value = DISAGREE;
        }
        _userLinkmom = int.parse(value);
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getDolbomi : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return _userLinkmom;
    }
    notifyListeners();
    return _userLinkmom;
  }

  MenuFile _menu_file = MenuFile();

  MenuFile get menu_file => _menu_file;

  void setMenuFile(MenuFile data) async {
    _menu_file = data;
    // log.d('『GGUMBI』>>> setMenuFile : data: $data,  <<< ');
    var _save = json.encode(_menu_file.toJson());
    await _storage.write(key: KEY_SP_MENU_FILE, value: _save).then((value) {
      // log.d('『GGUMBI』>>> setMenuFile : setMenuFile: $_menu_file,  <<< ');
    });
    notifyListeners();
  }

  Future<MenuFile> getMenuFile() async {
    try {
      String? data = await _storage.read(key: KEY_SP_MENU_FILE);
      // log.d('『GGUMBI』>>> getMenuFile : _data: $data,  <<< ');
      _menu_file = data == null ? MenuFile() : MenuFile.fromJson(json.decode(data));
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getMenuFile : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return MenuFile();
    }
    notifyListeners();
    return _menu_file;
  }

  MenuFileData _menu_file_data = MenuFileData();

  MenuFileData get menu_file_data => _menu_file_data;

  void setMenuFileData(MenuFileData data) async {
    _menu_file_data = data;
    // log.d('『GGUMBI』>>> setMenuFileData : data: $data,  <<< ');
    var _save = json.encode(_menu_file_data.toJson());
    await _storage.write(key: KEY_SP_MENU_FILE_DATA, value: _save).then((value) {
      log.d('『GGUMBI』>>> setMenuFileData : _user: ${_save.isNotEmpty},  <<< ');
    });
    notifyListeners();
  }

  /// get menuFile from local storage or keyChain
  Future<MenuFileData> getMenuFileData() async {
    try {
      if (menu_file.name.isEmpty) await getMenuFile();
      String? data = await _storage.read(key: KEY_SP_MENU_FILE_DATA);
      // log.d('『GGUMBI』>>> getMenuFileData : _data: $data,  <<< ');
      _menu_file_data = data == null ? MenuFileData() : MenuFileData.fromJson(json.decode(data));

      if (data == null) {
        String savePath = await Commons.findLocalPath();
        String name = '${menu_file.version}.${menu_file.name}';
        File file = File('$savePath/$name');
        data = await file.readAsString();
        _menu_file_data = MenuFileData.fromJson(json.decode(data));
        setMenuFileData(_menu_file_data);
      }

      // log.d('『GGUMBI』>>> getMenuFileData : _data: $_menu_file_data,  <<< ');
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getMenuFileData : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      return MenuFileData();
    }
    notifyListeners();
    return _menu_file_data;
  }

  List<AddressListItem> _addressData = [];

  List<AddressListItem> get addressData => _addressData;

  void setAddressData(List<AddressListItem> data) async {
    _addressData = data;
    // log.d('『GGUMBI』>>> setAddressDataa : data: $data,  <<< ');
    var _save = AddressListItem.encode(_addressData);
    await _storage.write(key: KEY_SP_ADDRESS_DATA, value: _save).then((value) {
      // log.d('『GGUMBI』>>> setAddressDataa : _user: $_addressData,  <<< ');
    });
    notifyListeners();
  }

  Future<List<AddressListItem>> getAddressData() async {
    try {
      String? data = await _storage.read(key: KEY_SP_ADDRESS_DATA);
      // log.d('『GGUMBI』>>> getAddressData : _data: $data,  <<< ');
      _addressData = data == null ? [] : AddressListItem.decode(data);
      // log.d('『GGUMBI』>>> getAddressData : _addressData.length: ${_addressData.length},  <<< ');
      // log.d('『GGUMBI』>>> getAddressData : _addressData: ${_addressData},  <<< ');
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getAddressData : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return [];
    }
    notifyListeners();
    return _addressData;
  }

  int _addressVersion = 0;

  int get addressVersion => _addressVersion;

  void setAddressVersion(int version) async {
    _addressVersion = version;
    log.d('『GGUMBI』>>> setAddressVersion : data: $version,  <<< ');
    var _save = json.encode(jsonEncode(_addressVersion));
    await _storage.write(key: KEY_SP_ADDRESS_VERSION, value: _save).then((value) {
      log.d('『GGUMBI』>>> setAddressVersion : version: $_addressVersion,  <<< ');
    });
    notifyListeners();
  }

  Future<int> getAddressVersion() async {
    try {
      String? data = await _storage.read(key: KEY_SP_ADDRESS_VERSION);
      log.d('『GGUMBI』>>> getAddressVersion : _data: $data,  <<< ');

      if (data == null) {
        return _addressVersion;
      }

      var _data = jsonDecode(data);
      if (StringUtils.validateString(_data)) {
        _addressVersion = int.parse(_data);
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getAddressVersion : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return _addressVersion;
    }
    notifyListeners();
    return _addressVersion;
  }

  late SettingsData _settings;

  void setSettings(SettingsData settings) async {
    _settings = settings;
    auth.setSettings = settings;
    log.d('『GGUMBI』>>> setSettings : data: $settings,  <<< ');
    var _save = json.encode(_settings.toJson());
    await _storage.write(key: KEY_SP_SETTINGS_VALUE, value: _save).then((value) {
      log.d('『GGUMBI』>>> setSettings : settings: $_settings,  <<< ');
    });
    notifyListeners();
  }

  Future<SettingsData> getSettings() async {
    try {
      String? data = await _storage.read(key: KEY_SP_SETTINGS_VALUE);
      log.d('『GGUMBI』>>> getSettings : _data: $data,  <<< ');
      _settings = data == null ? SettingsData() : SettingsData.fromJson(json.decode(data));
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getSettings : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return SettingsData();
    }
    notifyListeners();
    return _settings;
  }

  String _pkuName = '';

  void setPkuName(String data) async {
    _pkuName = data;
    log.d('『GGUMBI』>>> setPkuName : data: $data,  <<< ');
    await _storage.write(key: KEY_SP_PKU_NAME, value: data).then((value) {
      log.d('『GGUMBI』>>> setPkuName : setMenuFile: $_pkuName,  <<< ');
    });
    notifyListeners();
  }

  Future<String> getPkuName() async {
    try {
      String? data = await _storage.read(key: KEY_SP_PKU_NAME);
      log.d('『GGUMBI』>>> getPkuName : _data: $data,  <<< ');
      _pkuName = data ?? '';
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getPkuName : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return '';
    }
    notifyListeners();
    return _pkuName;
  }

  String get pku => _pkuName;

  List<String> _holidays = [];

  void setHolidayName(List<String> data) async {
    _holidays = data;
    log.d('『GGUMBI』>>> setHolidayName : data: $data,  <<< ');
    var _save = json.encode(_holidays);
    await _storage.write(key: KEY_SP_HOLIDAYS_NAME, value: _save).then((value) {
      log.d('『GGUMBI』>>> setHolidayName : setHolidayName: $_holidays,  <<< ');
    });
    notifyListeners();
  }

  Future<List<String>> getHolidayName() async {
    try {
      String? data = await _storage.read(key: KEY_SP_HOLIDAYS_NAME);
      log.d('『GGUMBI』>>> getHolidayName : _data: $data,  <<< ');
      _holidays = List.from(json.decode(data!));
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getHolidayName : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      return [];
    }
    notifyListeners();
    return _holidays;
  }

  List<String> get holiday => _holidays;

  bool _penalty = false;
  DateTime _penaltyDate = DateTime.now();

  void setPenaltyDate(String data) async {
    _penalty = data.isNotEmpty;
    log.d('『GGUMBI』>>> setPaneltyState : data: $data,  <<< ');
    await _storage.write(key: KEY_SP_PANELTY_STATE, value: data).then((value) {
      log.d('『GGUMBI』>>> setPaneltyState : $data,  <<< ');
    });
    notifyListeners();
  }

  Future<DateTime> getPenaltyDate() async {
    try {
      String data = await _storage.read(key: KEY_SP_PANELTY_STATE) ?? '';
      log.d('『GGUMBI』>>> getPaneltyState : _data: $data,  <<< ');
      _penaltyDate = StringUtils.formatYMDHMS(data);
      _penalty = _penaltyDate.isAfter(DateTime.now());
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getPaneltyState : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      return DateTime.now();
    }
    notifyListeners();
    return _penaltyDate;
  }

  bool get penalty => _penalty;

  DateTime get penaltyDate => _penaltyDate;

  FlagData _flag = FlagData();

  FlagData get flags => this._flag;

  Future<FlagData> getFlags() async {
    try {
      String data = await _storage.read(key: KEY_FLAG) ?? '';
      log.d('『GGUMBI』>>> getFlags : data: $data,  <<< ');
      _flag = FlagData.fromJson(json.decode(data));
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getFlags : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      _flag = FlagData();
    }
    notifyListeners();
    return _flag;
  }

  void setFlag(StateFlag flag) async {
    if (_flag.state == null) _flag.state = [];
    _flag.state!.add(flag);
    setFlags(_flag);
  }

  void setPenalty(PenaltyFlag flag) async {
    if (_flag.penalty != null && flag == _flag.penalty) return;
    _flag.penalty = flag;
    setFlags(_flag);
  }

  Future<void> setFlags(FlagData flag) async {
    _flag = flag;
    await _storage.write(key: KEY_FLAG, value: json.encode(_flag.toJson()));
    notifyListeners();
  }

  WithdrawalData _bank = WithdrawalData();

  WithdrawalData get accountInfo => _bank;

  Future<void> setAccount(WithdrawalData account) async {
    _bank = account;
    log.d('『GGUMBI』>>> setAccount : account: $account,  <<< ');
    await _storage.write(key: KEY_BANK_ACCOUNT, value: json.encode(_bank.toJson()));
    notifyListeners();
  }

  /// For Withdrawal
  Future<WithdrawalData> getAccount() async {
    try {
      String data = await _storage.read(key: KEY_BANK_ACCOUNT) ?? '';
      log.d('『GGUMBI』>>> getAccount : data: $data,  <<< ');
      _bank = WithdrawalData.fromJson(json.decode(data));
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getAccount : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    notifyListeners();
    return _bank;
  }

  List<BankModel> _bankList = [];
  int _bankListLength = 0;

  List<BankModel> get bankList => _bankList;

  int get bankListLegnth => _bankListLength;

  String _bankPath = '';

  Future<void> setBankList(String path, List<int> data) async {
    try {
      File file = File(path);
      var raf = file.openSync(mode: FileMode.write);
      raf.writeFromSync(data);
      String list = await file.readAsString();
      _bankList = BankList.decode(list);
      setBankPath(path);
      log.i("setBankList: ${file.path}");
      await raf.close();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> BankList : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    notifyListeners();
  }

  /// For bankList
  Future<List<BankModel>> getBankList(String path) async {
    try {
      String savePath = await Commons.findLocalPath();
      String name = path.split('/').last;
      File file = File('$savePath/$name');
      String list = await file.readAsString();
      _bankListLength = file.lengthSync();
      _bankList = BankList.decode(list);
      log.i("getBankList: ${_bankList.isNotEmpty}");
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> BankList : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      _bankList = [];
      _bankListLength = 0;
    }
    notifyListeners();
    return _bankList;
  }

  Future<List<BankModel>> getBankListWithoutPath() async {
    if (_bankPath.isEmpty) await getBankPath();
    return getBankList(_bankPath);
  }

  Future<String> getBankPath() async {
    try {
      String data = await _storage.read(key: KEY_BANK_PATH) ?? '';
      log.d('『GGUMBI』>>> getBankPath : data: $data,  <<< ');
      _bankPath = data;
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getBankPath : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    notifyListeners();
    return _bankPath;
  }

  Future<void> setBankPath(String path) async {
    log.d('『GGUMBI』>>> setBankPath : path: $path,  <<< ');
    _bankPath = path;
    await _storage.write(key: KEY_BANK_PATH, value: path);
    notifyListeners();
  }

  Future<void> removeAll() async {
    try {
      FlagData flag = FlagData.clone(await getFlags());
      String path = await Commons.findLocalPath();
      var pkuName = await getPkuName();
      await getBankList(_bankPath);
      var length = _bankListLength;
      await removeFile('$path/$pkuName');
      _flag.state = [];
      await _storage.deleteAll();

      /// NOTE: copy to new storage
      setBankPath(_bankPath).then((value) => getBankList(_bankPath));
      saveTutorialFlags(flag.state);
      saveChatFlags(flag.state);
      savePushFlags(flag.state);
      setHolidayName(_holidays);
      _bankListLength = length;
      setUserType(user_type);
      setModeCnt(modeCnt);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> removeAll $e');
      await _storage.deleteAll();
    }
    notifyListeners();
  }

  Future<void> saveTutorialFlags(List<StateFlag>? flag) async {
    if (flag != null) {
      StateFlag intro = flag.singleWhere((e) => e.name == Flags.KEY_SHOW_LOGIN_INTRO, orElse: () => StateFlag(Flags.KEY_SHOW_LOGIN_INTRO, isShown: false));
      StateFlag tutorial = flag.singleWhere((e) => e.name == Flags.KEY_SHOW_LOGIN_TUTORIAL, orElse: () => StateFlag(Flags.KEY_SHOW_LOGIN_TUTORIAL, isShown: false));

      Flags.addFlag(intro.name, id: intro.id, isShown: intro.isShown);
      Flags.addFlag(tutorial.name, id: tutorial.id, isShown: tutorial.isShown);
    }
  }

  Future<void> saveChatFlags(List<StateFlag>? flag) async {
    if (flag != null) {
      StateFlag chatNormal = flag.singleWhere((e) => e.name == Flags.KEY_SHOW_CHAT_ALERT_NORMAL, orElse: () => StateFlag(Flags.KEY_SHOW_CHAT_ALERT_NORMAL, isShown: false));
      StateFlag chatLinkMom = flag.singleWhere((e) => e.name == Flags.KEY_SHOW_CHAT_ALERT_LINKMOM, orElse: () => StateFlag(Flags.KEY_SHOW_CHAT_ALERT_LINKMOM, isShown: false));
      StateFlag chatMomDady = flag.singleWhere((e) => e.name == Flags.KEY_SHOW_CHAT_ALERT_MOMDADDY, orElse: () => StateFlag(Flags.KEY_SHOW_CHAT_ALERT_MOMDADDY, isShown: false));

      Flags.addFlag(chatNormal.name, id: chatNormal.id, isShown: chatNormal.isShown);
      Flags.addFlag(chatLinkMom.name, id: chatLinkMom.id, isShown: chatLinkMom.isShown);
      Flags.addFlag(chatMomDady.name, id: chatMomDady.id, isShown: chatMomDady.isShown);
    }
  }

  Future<void> savePushFlags(List<StateFlag>? flag) async {
    if (flag != null) {
      StateFlag pushMomdady = flag.singleWhere((e) => e.name == Flags.KEY_PUSH_MOMDADY, orElse: () => StateFlag(Flags.KEY_PUSH_MOMDADY, isShown: false));
      StateFlag pushLinkMom = flag.singleWhere((e) => e.name == Flags.KEY_PUSH_LINKMOM, orElse: () => StateFlag(Flags.KEY_PUSH_LINKMOM, isShown: false));

      Flags.addFlag(pushMomdady.name, id: pushMomdady.id, isShown: pushMomdady.isShown);
      Flags.addFlag(pushLinkMom.name, id: pushLinkMom.id, isShown: pushLinkMom.isShown);
    }
  }

  VersionCheckData _appVersion = VersionCheckData();

  VersionCheckData get appVersion => _appVersion;

  Future<VersionCheckData> getAppVersion() async {
    try {
      String data = await _storage.read(key: KEY_SP_APPVERSION) ?? '';
      log.d('『GGUMBI』>>> getFlags : data: $data,  <<< ');
      _appVersion = VersionCheckData.fromJson(json.decode(data));
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getAppVersion : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      return VersionCheckData();
    }
    notifyListeners();
    return _appVersion;
  }

  void setAppVersion(VersionCheckData version) async {
    log.d('『GGUMBI』>>> setAppVersion : flag: $version,  <<< ');
    _appVersion = version;
    await _storage.write(key: KEY_SP_APPVERSION, value: json.encode(_appVersion.toJson()));
    notifyListeners();
  }

  Future<bool> removeFile(String path) async {
    try {
      log.d({'removeFile | DELETE REQUEST': '$path'});
      if (StringUtils.validateString(path) && path.contains('.')) {
        File file = File(path);
        await file.delete();
        log.d({'removeFile | DELETED': '$path'});
        return Future.value(true);
      } else {
        log.d({"removeFile | CAN'T DELETE": '$path'});
        return Future.value(false);
      }
    } catch (e) {
      log.e({
        'removeFile': 'Exception',
        "ERROR": e.toString(),
        "CAN'T DELETE": '$path',
      });
      return Future.value(false);
    }
  }

  int _modeCnt = 0;

  int get modeCnt => _modeCnt;

  Future<int> getModeCnt() async {
    try {
      String? data = await _storage.read(key: KEY_SP_MODE_LINKMOM);
      // log.d('『GGUMBI』>>> getUser : _data: $data,  <<< ');
      _modeCnt = data == null ? 0 : int.parse(data);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getUser : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return _modeCnt;
    }
    return _modeCnt;
  }

  void setModeCnt(int modeCnt) async {
    _modeCnt = modeCnt;
    // log.d('『GGUMBI』>>> setModeCnt : modeCnt: $modeCnt,  <<< ');
    var _save = modeCnt.toString();
    await _storage.write(key: KEY_SP_MODE_LINKMOM, value: _save).then((value) {
      // log.d('『GGUMBI』>>> setModeCnt : modeCnt: $modeCnt,  <<< ');
    });
  }

  bool _isDebug = true;

  bool get isDebug => _isDebug;

  Future<bool> getDebug() async {
    try {
      String? data = await _storage.read(key: KEY_SP_IS_DEBUG);
      _isDebug = data == null ? true : StringUtils.getStringToBool(data);
    } catch (e) {
      return _isDebug;
    }
    return _isDebug;
  }

  void setDebug(bool isDebug) async {
    _isDebug = isDebug;
    var _save = _isDebug.toString();
    await _storage.write(key: KEY_SP_IS_DEBUG, value: _save).then((value) {});
  }
}
