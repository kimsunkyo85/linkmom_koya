import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class BankModel {
  static const String Key_code = 'code';
  static const String Key_name = 'name';

  @JsonKey(name: Key_code)
  String code;
  @JsonKey(name: Key_name)
  String name;

  BankModel({this.code = '', this.name = ''});

  factory BankModel.fromJson(Map<String, dynamic> json) {
    return BankModel(
      code: json[Key_code] ?? '',
      name: json[Key_name] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_code] = this.code;
    data[Key_name] = this.name;
    return data;
  }

  @override
  String toString() {
    return 'BankModel{code: $code, name: $name}';
  }
}

class BankList {
  static String encode(List<BankModel> models) {
    return json.encode(models.map((e) => e.toJson()).toList());
  }

  static List<BankModel> decode(String models) {
    return (json.decode(models) as List).map((e) => BankModel.fromJson(e)).toList();
  }

  
}
