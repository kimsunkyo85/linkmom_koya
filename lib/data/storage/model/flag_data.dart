import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class FlagData {
  static const String Key_penalty = 'penalty';
  static const String Key_states = 'StateFlag';
  PenaltyFlag? penalty;
  List<StateFlag>? state;

  FlagData({this.penalty, this.state}) {
    if (penalty == null) penalty = PenaltyFlag();
    if (state == null) state = [];
  }

  FlagData.clone(FlagData item)
      : this(
          penalty: item.penalty,
          state: item.state,
        );

  @override
  String toString() {
    return 'FlagData {penalty: $penalty, StateFlag: $state}';
  }

  factory FlagData.fromJson(Map<String, dynamic> json) {
    return FlagData(
      penalty: json[Key_penalty] != null ? PenaltyFlag.fromJson(json[Key_penalty]) : PenaltyFlag(),
      state: json[Key_states] != null ? json[Key_states].map<StateFlag>((e) => StateFlag.fromJson(e)).toList() : [],
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = {};
    data[Key_penalty] = this.penalty;
    data[Key_states] = this.state;
    return data;
  }
}

class PenaltyFlag {
  static const String Key_first = 'first';
  static const String Key_second = 'second';
  static const String Key_third = 'third';

  bool first;
  bool second;
  bool third;

  PenaltyFlag({this.first = false, this.second = false, this.third = false});

  factory PenaltyFlag.fromJson(Map<String, dynamic> json) {
    return PenaltyFlag(
      first: json[Key_first] ?? false,
      second: json[Key_second] ?? false,
      third: json[Key_third] ?? false,
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = {};
    data[Key_first] = this.first;
    data[Key_second] = this.second;
    data[Key_third] = this.third;
    return data;
  }

  @override
  String toString() {
    return 'PenaltyFlag {first: $first, second: $second, third: $third}';
  }
}

class StateFlag {
  static const String Key_name = 'name';
  static const String Key_id = 'id';
  static const String Key_isShown = 'isShown';

  String name;
  int id;
  bool isShown;

  StateFlag(this.name, {this.id = 0, this.isShown = false});

  factory StateFlag.fromJson(Map<String, dynamic> json) {
    return StateFlag(json[Key_name] ?? '', id: json[Key_id] ?? 0, isShown: json[Key_isShown] ?? false);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = {};
    data[Key_name] = this.name;
    data[Key_id] = this.id;
    data[Key_isShown] = this.isShown;
    return data;
  }

  @override
  String toString() {
    return 'StateFlag{name: $name, id: $id, isShown: $isShown}';
  }
}
