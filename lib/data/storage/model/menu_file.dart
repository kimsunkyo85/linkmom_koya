import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class MenuFile {
  static const String Key_name = 'name';
  static const String Key_version = 'version';

  MenuFile({
    this.name = '',
    this.version = '',
  });

  @JsonKey(name: Key_name)
  String name;
  @JsonKey(name: Key_version)
  String version;

  factory MenuFile.fromJson(Map<String, dynamic> json) {
    return MenuFile(
      name: json[Key_name] ?? '',
      version: json[Key_version] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_name] = this.name;
    data[Key_version] = this.version;
    return data;
  }

  @override
  String toString() {
    return 'MenuFile{name: $name, version:$version}';
  }
}
