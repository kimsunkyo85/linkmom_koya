import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class VersionData {
  static const String Key_version = 'version';
  static const String Key_code = 'code';
  static const String Key_os = 'os';
  static const String Key_major = 'major';
  static const String Key_minor = 'minor';
  static const String Key_patch = 'patch';
  static const String Key_unique = 'unique';

  VersionData({
    this.versionName = '',
    this.versionCode = '',
    this.os = '',
    this.major = 1,
    this.minor = 0,
    this.patch = 0,
    this.unique = '',
  });

  @JsonKey(name: Key_version)
  String versionName;
  @JsonKey(name: Key_code)
  String versionCode;
  @JsonKey(name: Key_os)
  String os;
  @JsonKey(name: Key_major)
  int major;
  @JsonKey(name: Key_minor)
  int minor;
  @JsonKey(name: Key_patch)
  int patch;
  @JsonKey(name: Key_unique)
  String unique;

  factory VersionData.fromJson(Map<String, dynamic> json) {
    String versionName = json[Key_version] ?? '';
    int major = 1;
    int minor = 0;
    int patch = 0;
    if (versionName.length != 0) {
      var paser = versionName.split(".");
      if (paser.isNotEmpty) {
        major = int.parse(paser[0]);
        minor = int.parse(paser[1]);
        patch = int.parse(paser[2]);
      }
    }
    return VersionData(
      versionName: versionName,
      versionCode: json[Key_code] ?? '',
      os: json[Key_os] ?? '',
      major: major,
      minor: minor,
      patch: patch,
      unique: json[Key_unique] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_version] = this.versionName;
    data[Key_code] = this.versionCode;
    data[Key_os] = this.os;
    data[Key_major] = this.major;
    data[Key_minor] = this.minor;
    data[Key_patch] = this.patch;
    data[Key_unique] = this.unique;
    return data;
  }

  String getVersion() {
    return '$major.$minor.$patch';
  }

  @override
  String toString() {
    return 'VersionData{versionName: $versionName, versionCode: $versionCode, os: $os, major: $major, minor: $minor, patch: $patch, unique: $unique}';
  }
}
