import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class AddressData {
  static const String Key_version = 'version';
  static const String Key_data = 'data';

  AddressData({
    this.version = 1,
    this.data,
  });

  int version;
  List<AddressDataItem>? data;

  factory AddressData.fromJson(Map<String, dynamic> json) {
    List<AddressDataItem> data = json[Key_data].map<AddressDataItem>((i) => AddressDataItem.fromJson(i)).toList();
    return AddressData(
      version: json[Key_version] as int,
      data: data,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_version] = this.version;
    data[Key_data] = this.data;
    return data;
  }

  @override
  String toString() {
    return 'AddressData{version: $version, data: $data}';
  }
}

@JsonSerializable()
class AddressDataItem {
  static const String Key_address1_id = 'address1_id';
  static const String Key_address1 = 'address1';
  static const String Key_address2_id = 'address2_id';
  static const String Key_address2 = 'address2';

  AddressDataItem({
    this.address1_id = '',
    this.address1 = '',
    this.address2_id = '',
    this.address2 = '',
  });

  @JsonKey(name: Key_address1_id)
  final String address1_id;
  @JsonKey(name: Key_address1)
  final String address1;
  @JsonKey(name: Key_address2_id)
  final String address2_id;
  @JsonKey(name: Key_address2)
  final String address2;

  factory AddressDataItem.fromJson(Map<String, dynamic> json) {
    return AddressDataItem(
      address1_id: json[Key_address1_id] ?? '',
      address1: json[Key_address1] ?? '',
      address2_id: json[Key_address2_id] ?? '',
      address2: json[Key_address2] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_address1_id] = this.address1_id;
    data[Key_address1] = this.address1;
    data[Key_address2_id] = this.address2_id;
    data[Key_address2] = this.address2;
    return data;
  }

  @override
  String toString() {
    return 'AddressDataItem{address1_id: $address1_id, address1: $address1, address2_id: $address2_id, address2: $address2}';
  }
}
