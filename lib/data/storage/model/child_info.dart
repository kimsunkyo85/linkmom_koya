import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';

@JsonSerializable()
class ChildInfo {
  static const String Key_step = 'step';
  static const String Key_name = 'name';
  static const String Key_age = 'age';
  static const String Key_nursery = 'nursery';
  static const String Key_gender = 'gender';
  static const String Key_character = 'character';
  static const String Key_is_allergy = 'is_allergy';
  static const String Key_allergy_name = 'allergy_name';
  static const String Key_allergy_message = 'allergy_message';
  static const String Key_show = 'show';
  static const String Key_show_child = 'show_child';

  ChildInfo({
    this.step = 1,
    this.name = '',
    this.age = 0,
    this.nursery = 0,
    this.gender = 1,
    this.character,
    this.is_allergy = false,
    this.allergy_name = '',
    this.allergy_message = '',
    this.isShow = true,
    this.isShowChild = false,
  });

  ChildInfo.clone(ChildInfo childInfo)
      : this(
          step: childInfo.step,
          name: childInfo.name,
          age: childInfo.age,
          nursery: childInfo.nursery,
          gender: childInfo.gender,
          character: childInfo.character,
          is_allergy: childInfo.is_allergy,
          allergy_name: childInfo.allergy_name,
          allergy_message: childInfo.allergy_message,
        );

  @JsonKey(name: Key_step)
  int step;
  @JsonKey(name: Key_name)
  String? name;
  @JsonKey(name: Key_age)
  int? age;
  @JsonKey(name: Key_nursery)
  int? nursery;
  @JsonKey(name: Key_gender)
  int? gender;
  @JsonKey(name: Key_character)
  List<int>? character;
  @JsonKey(name: Key_is_allergy)
  bool is_allergy;
  @JsonKey(name: Key_allergy_name)
  String allergy_name;
  @JsonKey(name: Key_allergy_message)
  String allergy_message;
  @JsonKey(name: Key_show)
  bool isShow;
  @JsonKey(name: Key_show_child)
  bool isShowChild;

  factory ChildInfo.fromJson(Map<String, dynamic> json) {
    List<int> characters = List<int>.from(json[Key_character]);
    return ChildInfo(
      step: json[Key_step] as int,
      name: json[Key_name] ?? '',
      age: json[Key_age] as int,
      nursery: json[Key_nursery] as int,
      gender: json[Key_gender] as int,
      character: characters,
      is_allergy: json[Key_is_allergy] ?? false,
      allergy_name: json[Key_allergy_name] ?? '',
      allergy_message: json[Key_allergy_message] ?? '',
      isShow: json[Key_show] as bool,
      isShowChild: json[Key_show_child] as bool,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_step] = this.step;
    data[Key_name] = this.name;
    data[Key_age] = this.age;
    data[Key_nursery] = this.nursery;
    data[Key_gender] = this.gender;
    data[Key_character] = this.character;
    data[Key_is_allergy] = this.is_allergy;
    data[Key_allergy_name] = this.allergy_name;
    data[Key_allergy_message] = this.allergy_message;
    data[Key_show] = this.isShow;
    data[Key_show_child] = this.isShowChild;
    return data;
  }

  String getStringAppend(List<int> values) {
    StringBuffer stringBuffer = StringBuffer();
    if (values.isNotEmpty) {
      values.asMap().forEach((index, value) {
        stringBuffer.write(EnumUtils.getChildCharacter(value).string);
        if (index != values.length - 1) {
          stringBuffer.write(", ");
        }
      });
    }
    return stringBuffer.toString();
  }

  @override
  String toString() {
    return 'ChildInfo{step: $step, name: $name, age: $age, nursery: $nursery, gender: $gender, character: $character, is_allergy: $is_allergy, allergy_name: $allergy_name, allergy_message: $allergy_message, isShow: $isShow, isShowChild: $isShowChild}';
  }
}
