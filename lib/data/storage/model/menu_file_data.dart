import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class MenuFileData {
  static const String Key_services = 'services';
  static const String Key_caretypes = 'caretypes';
  static const String Key_products = 'products';

  MenuFileData({
    this.services,
    this.caretypes,
    this.products,
  });

  @JsonKey(name: Key_services)
  final List<dynamic>? services;
  @JsonKey(name: Key_caretypes)
  final List<dynamic>? caretypes;
  @JsonKey(name: Key_products)
  final List<dynamic>? products;

  factory MenuFileData.fromJson(Map<String, dynamic> json) {
    List<ServicesData> servicesData = json[Key_services] == null ? [] : json[Key_services].map<ServicesData>((i) => ServicesData.fromJson(i)).toList();
    List<CareTypesData> careTypeData = json[Key_caretypes] == null ? [] : json[Key_caretypes].map<CareTypesData>((i) => CareTypesData.fromJson(i)).toList();
    List<ProductData> productData = json[Key_products] == null ? [] : json[Key_products].map<ProductData>((i) => ProductData.fromJson(i)).toList();
    return MenuFileData(
      services: servicesData,
      caretypes: careTypeData,
      products: productData,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_services] = this.services;
    data[Key_caretypes] = this.caretypes;
    data[Key_products] = this.products;
    return data;
  }

  @override
  String toString() {
    return 'MenuFileData{services: $services, caretypes: $caretypes, products: $products}';
  }
}

@JsonSerializable()
class ServicesData {
  static const String Key_servicetype_no = 'servicetype_no';
  static const String Key_area_no = 'area_no';
  static const String Key_transports = 'transports';
  static const String Key_boyuks = 'boyuks';
  static const String Key_plays = 'plays';
  static const String Key_homecares = 'homecares';

  ServicesData({
    this.servicetype_no = 0,
    this.area_no = 0,
    this.transports,
    this.boyuks,
    this.plays,
    this.homecares,
  });

  ///"서비스 종류 구분 : (0, '등원돌봄'), (1, '하원돌봄'), (2, '보육돌봄'), (3, '학습돌봄'), (4, '입주가사'), (5, '이유식반찬')",
  @JsonKey(name: Key_servicetype_no)
  final int servicetype_no;

  ///(1, '우리집'), (2, '이웃집')",
  @JsonKey(name: Key_area_no)
  final int area_no;

  ///이동수단
  @JsonKey(name: Key_transports)
  final List<CareServiceItem>? transports;

  ///보육 서비스
  @JsonKey(name: Key_boyuks)
  final List<CareServiceItem>? boyuks;

  ///놀이 서비스
  @JsonKey(name: Key_plays)
  final List<CareServiceItem>? plays;

  ///가사 서비스
  @JsonKey(name: Key_homecares)
  final List<CareServiceItem>? homecares;

  factory ServicesData.fromJson(Map<String, dynamic> json) {
    List<CareServiceItem> _transports = json[Key_transports] == null ? [] : json[Key_transports].map<CareServiceItem>((i) => CareServiceItem.fromJson(i)).toList();
    List<CareServiceItem> _boyuks = json[Key_boyuks] == null ? [] : json[Key_boyuks].map<CareServiceItem>((i) => CareServiceItem.fromJson(i)).toList();
    List<CareServiceItem> _plays = json[Key_plays] == null ? [] : json[Key_plays].map<CareServiceItem>((i) => CareServiceItem.fromJson(i)).toList();
    List<CareServiceItem> _homecares = json[Key_homecares] == null ? [] : json[Key_homecares].map<CareServiceItem>((i) => CareServiceItem.fromJson(i)).toList();
    return ServicesData(
      servicetype_no: json[Key_servicetype_no] as int,
      area_no: json[Key_area_no] as int,
      transports: _transports,
      boyuks: _boyuks,
      plays: _plays,
      homecares: _homecares,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_servicetype_no] = this.servicetype_no;
    data[Key_area_no] = this.area_no;
    data[Key_transports] = this.transports;
    data[Key_boyuks] = this.boyuks;
    data[Key_plays] = this.plays;
    data[Key_homecares] = this.homecares;
    return data;
  }

  @override
  String toString() {
    return 'ServicesData{servicetype_no: $servicetype_no, area_no: $area_no, transports: $transports, boyuks: $boyuks, plays: $plays, homecares: $homecares}';
  }
}

@JsonSerializable()
class ServicesDataItem {
  static const String Key_service_id = 'service_id';
  static const String Key_menu = 'menu';
  static const String Key_careservice = 'careservice';
  static const String Key_nextservice = 'nextservice';

  ServicesDataItem({
    this.service_id = 0,
    this.menu = '',
    this.careservice,
    this.nextservice,
  });

  ///"서비스ID",
  @JsonKey(name: Key_service_id)
  final int service_id;

  ///"메뉴명",
  @JsonKey(name: Key_menu)
  final String menu;

  ///"기본돌봄 서비스 그룹"
  @JsonKey(name: Key_careservice)
  final List<CareServiceItem>? careservice;

  ///"다음서비스 번호",
  @JsonKey(name: Key_nextservice)
  final List<NextServiceItem>? nextservice;

  factory ServicesDataItem.fromJson(Map<String, dynamic> json) {
    List<CareServiceItem> careItem = json[Key_careservice] == null ? [] : json[Key_careservice].map<CareServiceItem>((i) => CareServiceItem.fromJson(i)).toList();
    List<NextServiceItem> serviceItem = json[Key_nextservice] == null ? [] : json[Key_nextservice].map<NextServiceItem>((i) => NextServiceItem.fromJson(i)).toList();
    return ServicesDataItem(
      service_id: json[Key_service_id] as int,
      menu: json[Key_menu] ?? '',
      careservice: careItem,
      nextservice: serviceItem,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_service_id] = this.service_id;
    data[Key_menu] = this.menu;
    data[Key_careservice] = this.careservice;
    data[Key_nextservice] = this.nextservice;
    return data;
  }

  @override
  String toString() {
    return 'ServicesDataItem{service_id: $service_id, menu: $menu, careservice: $careservice, nextservice: $nextservice}';
  }
}

@JsonSerializable()
class CareServiceItem {
  static const String Key_caretype_no = 'caretype_no';
  static const String Key_basic_product = 'basic_product';
  static const String Key_add_product = 'add_product';

  CareServiceItem({
    this.caretype_no = 0,
    this.basic_product,
    this.add_product,
  });

  ///caretypes 번호"
  @JsonKey(name: Key_caretype_no)
  final int caretype_no;

  ///기본돌봄 상품번호 List"
  @JsonKey(name: Key_basic_product)
  final List<int>? basic_product;

  ///유료돌봄 상품번호 List"
  @JsonKey(name: Key_add_product)
  final List<int>? add_product;

  factory CareServiceItem.fromJson(Map<String, dynamic> json) {
    List<int> basicProductItem = json[Key_basic_product] == null ? [] : List<int>.from(json[Key_basic_product]);
    List<int> addProductItem = json[Key_add_product] == null ? [] : List<int>.from(json[Key_add_product]);
    return CareServiceItem(
      caretype_no: json[Key_caretype_no] as int,
      basic_product: basicProductItem,
      add_product: addProductItem,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_caretype_no] = this.caretype_no;
    data[Key_basic_product] = this.basic_product;
    data[Key_add_product] = this.add_product;
    return data;
  }

  @override
  String toString() {
    return 'CareServiceItem{caretype_no: $caretype_no, basic_product: $basic_product, add_product: $add_product}';
  }
}

@JsonSerializable()
class NextServiceItem {
  static const String Key_nextservice_id = 'nextservice_id';

  NextServiceItem({
    this.nextservice_id = 0,
  });

  @JsonKey(name: Key_nextservice_id)
  final int nextservice_id;

  factory NextServiceItem.fromJson(Map<String, dynamic> json) {
    return NextServiceItem(
      nextservice_id: json[Key_nextservice_id] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_nextservice_id] = this.nextservice_id;
    return data;
  }

  @override
  String toString() {
    return 'NextServiceItem{nextservice_id: $nextservice_id}';
  }
}

@JsonSerializable()
class ProductData {
  static const String Key_product_id = 'product_id';
  static const String Key_caretype = 'caretype';
  static const String Key_product_data = 'product_data';

  ProductData({
    this.product_id = 0,
    this.caretype = 0,
    this.product_data,
  });

  ///돌봄서비스 번호",
  @JsonKey(name: Key_product_id)
  final int product_id;

  ///케어서비스 타입
  @JsonKey(name: Key_caretype)
  final int caretype;

  ///"돌봄서비스 속성"
  @JsonKey(name: Key_product_data)
  final ProductDataItem? product_data;

  factory ProductData.fromJson(Map<String, dynamic> json) {
    ProductDataItem productDataItem = ProductDataItem.fromJson(json[Key_product_data]);
    return ProductData(
      product_id: json[Key_product_id] as int,
      caretype: json[Key_caretype] as int,
      product_data: productDataItem,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_product_id] = this.product_id;
    data[Key_caretype] = this.caretype;
    data[Key_product_data] = this.product_data;
    return data;
  }

  @override
  String toString() {
    return 'ProductData{product_id: $product_id, caretype: $caretype, product_data: $product_data}';
  }
}

@JsonSerializable()
class CareTypesData {
  static const String Key_type_id = 'type_id';
  static const String Key_type_name = 'type_name';

  CareTypesData({
    this.type_id = 0,
    this.type_name = '',
  });

  ///유형별 번호"
  @JsonKey(name: Key_type_id)
  final int type_id;

  ///유형별 이름 (위생...)
  @JsonKey(name: Key_type_name)
  final String type_name;

  factory CareTypesData.fromJson(Map<String, dynamic> json) {
    return CareTypesData(
      type_id: json[Key_type_id] as int,
      type_name: json[Key_type_name] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_type_id] = this.type_id;
    data[Key_type_name] = this.type_name;
    return data;
  }

  @override
  String toString() {
    return 'CareTypesData{type_id: $type_id, type_name: $type_name}';
  }
}

@JsonSerializable()
class ProductDataItem {
  static const String Key_possible_area = 'possible_area';
  static const String Key_name = 'name';
  static const String Key_s_age = 's_age';
  static const String Key_e_age = 'e_age';
  static const String Key_b_minutes = 'b_minutes';
  static const String Key_b_cost = 'b_cost';
  static const String Key_infotype = 'infotype';
  static const String Key_ourhome_parents_message = 'ourhome_parents_message';
  static const String Key_ourhome_dobomi_message = 'ourhome_dobomi_message';
  static const String Key_nbrhome_parents_message = 'nbrhome_parents_message';
  static const String Key_nbrrhome_dobomi_message = 'nbrrhome_dobomi_message';

  ProductDataItem({
    this.possible_area = 0,
    this.name = '',
    this.s_age = 0,
    this.e_age = 0,
    this.b_minutes = 0,
    this.b_cost = 0,
    this.infotype = '',
    this.ourhome_parents_message = '',
    this.ourhome_dobomi_message = '',
    this.nbrhome_parents_message = '',
    this.nbrrhome_dobomi_message = '',
  });

  ///"서비스 가능 지역 구분번호 : (0, '모두가능'), (1, '우리집'), (2, '이웃집')",
  @JsonKey(name: Key_possible_area)
  final int possible_area;

  ///"서비스명",
  @JsonKey(name: Key_name)
  final String name;

  ///"제공 가능나이 (시작)",
  @JsonKey(name: Key_s_age)
  final int s_age;

  ///"제공 가능나이 (종료)",
  @JsonKey(name: Key_e_age)
  final int e_age;

  ///최소 필요시간(분)
  @JsonKey(name: Key_b_minutes)
  final int b_minutes;

  ///"유료돌봄 추가 가격",
  @JsonKey(name: Key_b_cost)
  final int b_cost;

  ///"안내문구 노출 방법 : ('infotxt', '일반 TEXT 안내'), ('inputbox', '입력창 내 안내')",
  @JsonKey(name: Key_infotype)
  final String infotype;

  ///"우리집: 부모님 안내문구",
  @JsonKey(name: Key_ourhome_parents_message)
  final String ourhome_parents_message;

  ///"우리집: 돌보미 안내문구",
  @JsonKey(name: Key_ourhome_dobomi_message)
  final String ourhome_dobomi_message;

  /// "이웃집: 부모님 안내문구",
  @JsonKey(name: Key_nbrhome_parents_message)
  final String nbrhome_parents_message;

  ///"이웃집: 돌보미 안내문구"
  @JsonKey(name: Key_nbrrhome_dobomi_message)
  final String nbrrhome_dobomi_message;

  factory ProductDataItem.fromJson(Map<String, dynamic> json) {
    return ProductDataItem(
      possible_area: json[Key_possible_area] as int,
      name: json[Key_name] ?? '',
      s_age: json[Key_s_age] as int,
      e_age: json[Key_e_age] as int,
      b_minutes: json[Key_b_minutes] as int,
      b_cost: json[Key_b_cost] as int,
      infotype: json[Key_infotype] ?? '',
      ourhome_parents_message: json[Key_ourhome_parents_message] ?? '',
      ourhome_dobomi_message: json[Key_ourhome_dobomi_message] ?? '',
      nbrhome_parents_message: json[Key_nbrhome_parents_message] ?? '',
      nbrrhome_dobomi_message: json[Key_nbrrhome_dobomi_message] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_possible_area] = this.possible_area;
    data[Key_name] = this.name;
    data[Key_s_age] = this.s_age;
    data[Key_e_age] = this.e_age;
    data[Key_b_minutes] = this.b_minutes;
    data[Key_b_cost] = this.b_cost;
    data[Key_infotype] = this.infotype;
    data[Key_ourhome_parents_message] = this.ourhome_parents_message;
    data[Key_ourhome_dobomi_message] = this.ourhome_dobomi_message;
    data[Key_nbrhome_parents_message] = this.nbrhome_parents_message;
    data[Key_nbrrhome_dobomi_message] = this.nbrrhome_dobomi_message;
    return data;
  }

  @override
  String toString() {
    return 'ProductDataItem{possible_area: $possible_area, name: $name, s_age: $s_age, e_age: $e_age, b_minutes: $b_minutes, b_cost: $b_cost, infotype: $infotype, ourhome_parents_message: $ourhome_parents_message, ourhome_dobomi_message: $ourhome_dobomi_message, nbrhome_parents_message: $nbrhome_parents_message, nbrrhome_dobomi_message: $nbrrhome_dobomi_message}';
  }
}

@JsonSerializable()
class ServiceItem {
  static const String Key_cares = 'cares';
  static const String Key_products = 'products';

  ServiceItem({
    this.cares,
    this.products,
  });

  ServiceItem.init() {
    // cares = [];
    cares = CareTypesData();
    products = [];
  }

  // List<CareTypesData> cares;
  CareTypesData? cares;
  List<ProductData>? products;

  factory ServiceItem.fromJson(Map<String, dynamic> json) {
    // List<CareTypesData> cares = json[Key_cares] == null ? List() : List<CareTypesData>.from(json[Key_cares]);
    CareTypesData cares = CareTypesData.fromJson(json[Key_cares]);
    List<ProductData> products = json[Key_products] == null ? [] : List<ProductData>.from(json[Key_products]);
    return ServiceItem(
      cares: cares,
      products: products,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_cares] = this.cares;
    data[Key_products] = this.products;
    return data;
  }

  @override
  String toString() {
    return 'ServiceItem{cares: $cares, products: $products}';
  }
}
