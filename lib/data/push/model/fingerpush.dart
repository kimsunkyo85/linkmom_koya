import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class FingerPushData {
  static const String Key_msgTag = 'msgTag';
  static const String Key_code = 'code';
  static const String Key_time = 'time';
  static const String Key_beRcvCnt = 'beRcvCnt';
  static const String Key_badge = 'badge';
  static const String Key_sound = 'sound';
  static const String Key_title = 'title';
  static const String Key_message = 'message';
  static const String Key_weblink = 'weblink';
  static const String Key_labelCode = 'labelCode';
  static const String Key_img = 'img';
  static const String Key_src = 'src';
  static const String Key_imgUrl = 'imgUrl';

  FingerPushData({
    this.msgTag = '',
    this.code = '',
    this.time = '',
    this.beRcvCnt = '',
    this.badge = '',
    this.sound = '',
    this.title = '',
    this.message = '',
    this.weblink = '',
    this.labelCode = '',
    this.img = '',
    this.src = '',
    this.imgUrl = '',
  });

  @JsonKey(name: Key_msgTag)
  final String msgTag;
  @JsonKey(name: Key_code)
  final String code;
  @JsonKey(name: Key_time)
  final String time;
  @JsonKey(name: Key_beRcvCnt)
  final String beRcvCnt;
  @JsonKey(name: Key_badge)
  final String badge;
  @JsonKey(name: Key_sound)
  final String sound;
  @JsonKey(name: Key_title)
  final String title;
  @JsonKey(name: Key_message)
  final String message;
  @JsonKey(name: Key_weblink)
  final String weblink;
  @JsonKey(name: Key_labelCode)
  final String labelCode;
  @JsonKey(name: Key_img)
  final String img;
  @JsonKey(name: Key_src)
  final String src;
  @JsonKey(name: Key_imgUrl)
  final String imgUrl;

  factory FingerPushData.fromJson(Map<String, dynamic> json) {
    return FingerPushData(
      msgTag: json[Key_msgTag] ?? '',
      code: json[Key_code] ?? '',
      time: json[Key_time] ?? '',
      beRcvCnt: json[Key_beRcvCnt] ?? '',
      badge: json[Key_badge] ?? '',
      sound: json[Key_sound] ?? '',
      title: json[Key_title] ?? '',
      message: json[Key_message] ?? '',
      weblink: json[Key_weblink] ?? '',
      labelCode: json[Key_labelCode] ?? '',
      img: json[Key_img] ?? '',
      src: json[Key_src] ?? '',
      imgUrl: json[Key_imgUrl] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_msgTag] = this.msgTag;
    data[Key_code] = this.code;
    data[Key_time] = this.time;
    data[Key_beRcvCnt] = this.beRcvCnt;
    data[Key_badge] = this.badge;
    data[Key_sound] = this.sound;
    data[Key_title] = this.title;
    data[Key_weblink] = this.weblink;
    data[Key_labelCode] = this.labelCode;
    data[Key_img] = this.img;
    data[Key_src] = this.src;
    data[Key_imgUrl] = this.imgUrl;
    return data;
  }

  @override
  String toString() {
    return 'FingerPushData{msgTag: $msgTag, code: $code, time: $time, beRcvCnt: $beRcvCnt, badge: $badge, sound: $sound, title: $title, message: $message, weblink: $weblink, labelCode: $labelCode, img: $img, src: $src, imgUrl: $imgUrl}';
  }
}
