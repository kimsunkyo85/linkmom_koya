import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/data/chat_send_data.dart';
import 'package:linkmom/data/network/models/notification_list_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_utils.dart';

@JsonSerializable()
class LinkmomPushData extends ChangeNotifier {
  LinkmomPushData? _pushData;

  LinkmomPushData get pushData => _pushData ?? LinkmomPushData();

  void setData(LinkmomPushData data) {
    this._pushData = data;
    notifyListeners();
  }

  void setDataClear() {
    _pushData = LinkmomPushData();
  }

  //{
  //   "data": "{\"msg\":\"고러\",\"image\":\"\",\"is_receive\":false,\"receiver\":4,\"sender\":94,\"sendtime\":\"2021-07-22 18:21:50\",\"msgtype\":0,\"sendtimestamp\":20210722182150,\"chattingroom\":4}",
  //   "data.title": "[링크쌤]채팅",
  //   "data.message": "고러",
  //   "pagename": "0"
  // }
  static const String Key_body = 'body';
  static const String Key_code = 'code';
  static const String Key_time = 'time';
  static const String Key_click_action = 'click_action';
  static const String Key_badge = 'badge';
  static const String Key_sound = 'sound';
  static const String Key_title = 'title';
  static const String Key_message = 'message';
  static const String Key_data = 'data';
  static const String Key_data_title = 'data.title';
  static const String Key_data_message = 'data.message';
  static const String Key_src = 'src';
  static const String Key_pagename = 'pagename';
  static const String Key_chattingtype = 'chattingtype';
  static const String Key_send_code = 'send_code';
  static const String Key_weblink = 'weblink';
  static const String Key_bywho = 'bywho';

  LinkmomPushData({
    this.body = '',
    this.code = '',
    this.time = '',
    this.click_action = '',
    this.badge = '',
    this.sound = '',
    this.title = '',
    this.message = '',
    this.chatSendData,
    this.data_title = '',
    this.data_message = '',
    this.src = '',
    this.pagename = 0,
    this.chattingtype = 0,
    this.sendCode = '',
    this.weblink = '',
    this.msgData,
    this.bywho = '',
  });

  @JsonKey(name: Key_body)
  final String body;
  @JsonKey(name: Key_code)
  final String code;
  @JsonKey(name: Key_time)
  final String time;
  @JsonKey(name: Key_click_action)
  final String click_action;
  @JsonKey(name: Key_badge)
  final String badge;
  @JsonKey(name: Key_sound)
  final String sound;
  @JsonKey(name: Key_title)
  final String title;
  @JsonKey(name: Key_message)
  final String message;
  @JsonKey(name: Key_data)
  ChatSendData? chatSendData;
  @JsonKey(name: Key_data_title)
  final String data_title;
  @JsonKey(name: Key_data_message)
  final String data_message;
  @JsonKey(name: Key_src)
  final String src;
  @JsonKey(name: Key_pagename)
  final int pagename;
  @JsonKey(name: Key_chattingtype)
  final int chattingtype;
  @JsonKey(name: Key_send_code)
  final String sendCode;
  @JsonKey(name: Key_weblink)
  final String weblink;
  @JsonKey(name: Key_data)
  final NotificationMsgData? msgData;
  @JsonKey(name: Key_bywho)
  final String bywho;

  factory LinkmomPushData.fromJson(Map data, {String fingerPushAlias = ''}) {
    ChatSendData chatSendData = ChatSendData();
    bool isMsgData = false;
    int pagename = 999;
    NotificationMsgData msgData = NotificationMsgData();
    try {
      pagename = int.parse(data[Key_pagename] ?? data[fingerPushAlias + Key_pagename] ?? '999');
      isMsgData = EnumUtils.getNotificationTypeMsgData(pagename);
      chatSendData = ChatSendData.fromJson(json.decode(data[Key_data] ?? ''), isMsgData: isMsgData);
      if (data[Key_data] != null) msgData = NotificationMsgData.fromJson(json.decode(data[Key_data] ?? '')[NotificationData.Key_msgdata]);
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- ERROR     ': e,
      });
    }
    // log.d('『GGUMBI』>>> fromJson : data: $data,  <<< ');
    // log.d('『GGUMBI』>>> fromJson : msgData: $msgData,  <<< ');
    // log.d('『GGUMBI』>>> fromJson : chatSendData: $chatSendData,  <<< ');
    return LinkmomPushData(
      body: data[fingerPushAlias + Key_body] ?? '',
      code: data[fingerPushAlias + Key_code] ?? '',
      time: data[fingerPushAlias + Key_time] ?? '',
      click_action: data[fingerPushAlias + Key_click_action] ?? '',
      badge: data[fingerPushAlias + Key_badge] ?? '',
      sound: data[fingerPushAlias + Key_sound] ?? '',
      title: data[Key_title] ?? '',
      message: data[Key_message] ?? '',
      chatSendData: chatSendData,
      data_title: data[fingerPushAlias + Key_data_title] ?? '',
      data_message: data[fingerPushAlias + Key_data_message] ?? '',
      src: data[fingerPushAlias + Key_src] ?? '',
      pagename: pagename,
      chattingtype: data[fingerPushAlias + Key_chattingtype] as int,
      sendCode: data[fingerPushAlias + Key_send_code] ?? '',
      weblink: data[fingerPushAlias + Key_weblink] ?? '',
      msgData: msgData,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_body] = this.body;
    data[Key_code] = this.code;
    data[Key_time] = this.time;
    data[Key_click_action] = this.click_action;
    data[Key_badge] = this.badge;
    data[Key_sound] = this.sound;
    data[Key_title] = this.title;
    data[Key_data] = this.chatSendData;
    if (msgData != null) data[Key_data] = this.msgData;
    data[Key_data_title] = this.data_title;
    data[Key_data_message] = this.data_message;
    data[Key_src] = this.src;
    data[Key_pagename] = this.pagename;
    data[Key_chattingtype] = this.chattingtype;
    data[Key_send_code] = this.sendCode;
    data[Key_weblink] = this.weblink;
    return data;
  }

  @override
  String toString() {
    return 'LinkmomPushData{_pushData: $_pushData, body: $body, code: $code, time: $time, click_action: $click_action, badge: $badge, sound: $sound, title: $title, message: $message, data: $chatSendData, data_title: $data_title, data_message: $data_message, src: $src, pagename: $pagename, chattingtype: $chattingtype, detail: $sendCode, weblink: $weblink, msgData: $msgData}';
  }
}
