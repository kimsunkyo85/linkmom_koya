import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:linkmom/data/providers/model/agreement.dart';

class AgreementModel extends ChangeNotifier {
  int index = 0;

  AgreementModel();

  Agreement _header = Agreement(title: '');
  Agreement _footer = Agreement(title: '');
  final List<Agreement> _lists = [];

  get header => _header;

  get footer => _footer;

  UnmodifiableListView<Agreement> get lists => UnmodifiableListView(_lists);

  // UnmodifiableListView<Agreement> get incompleteTasks => UnmodifiableListView(_lists.where((agreement) => !agreement.completed));
  //
  // UnmodifiableListView<Agreement> get completedTasks => UnmodifiableListView(_lists.where((agreement) => agreement.completed));

  void clear() {
    if (_lists.isNotEmpty) {
      _lists.clear();
    }
  }

  void addAll(List<Agreement> list) {
    if (list.isNotEmpty) {
      clear();
    }
    _lists.addAll(list);
  }

  void createAgrrement(Agreement agreement) {
    this._header = agreement;
  }

  void addAgrrement(Agreement agreement) {
    _lists.add(agreement);
    notifyListeners();
  }

  void toggleList(Agreement agreement) {
    if (agreement.isItem) {
      index = _lists.indexOf(agreement);
      agreement.index = index;
      if (_lists[agreement.index].completed) {
        _lists[agreement.index].completed = false;
      } else {
        _lists[agreement.index].completed = true;
      }
      _lists[agreement.index].toggleCompleted();

      bool isAll = false;
      _lists.forEach((element) {
        if (!element.completed) {
          isAll = true;
        }
      });

      _header.completed = !isAll;
    } else {
      if (agreement.completed) {
        agreement.completed = false;
      } else {
        agreement.completed = true;
      }
      agreement.toggleCompleted();

      if (agreement.isVisible) {
        agreement.isVisible = false;
      } else {
        agreement.isVisible = true;
      }
      agreement.toggleVisible();

      _lists.forEach((element) {
        element.completed = agreement.completed;
      });
    }
    notifyListeners();
  }

  int itemClick(Agreement agreement) {
    index = _lists.indexOf(agreement);
    agreement.index = index;
    toggleList(agreement);
    return agreement.index;
  }

  void deleteList(Agreement agreement) {
    _lists.remove(agreement);
    notifyListeners();
  }
}
