import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:linkmom/data/network/models/authcenter_home_response.dart';
import 'package:linkmom/main.dart';

class ImageModel extends ChangeNotifier {
  ImageModel();

  late List<HomeStateImageData> _imageData = [];

  List<HomeStateImageData> get imgaeData => _imageData;

  late List<File> _images = [];

  List<File> get images => _images;

  Future<List<File>> setImages(List<HomeStateImageData> datas) async {
    _imageData = [];
    _imageData = datas;

    _images = [];
    final documentDirectory = await getApplicationDocumentsDirectory();
    log.d('『GGUMBI』>>> setImages : _imageData.length: ${_imageData.length},  <<< ');

    _imageData.asMap().forEach((index, value) async {
      var response = await Dio().get(value.ourhome_picture, options: Options(responseType: ResponseType.bytes));
      final file = File(p.join(documentDirectory.path, value.filename));
      file.writeAsBytesSync(response.data);
      _images.add(file);

      // await Future.forEach(_imageData, (value) async {
      //   var response = await Dio().get(value.ourhome_picture, options: Options(responseType: ResponseType.bytes));
      //   final file = File(p.join(documentDirectory.path, value.filename));
      //   file.writeAsBytesSync(response.data);
      //   _images.add(file);
      //   log.d('『GGUMBI』>>> setImages : _imageData[index].ourhome_picture: $_images,  <<< ');
      //   notifyListeners();
      // });
      log.d('『GGUMBI』>>> setImages : _imageData[index].ourhome_picture: $index : , $_images,  <<< ');
      if (index == _imageData.length - 1) {
        notifyListeners();
        log.d('『GGUMBI』>>> setImages ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
        return Future.value(_images);
      } else {
        return Future.value(_images);
      }
    });
    return _images;
  }
}
