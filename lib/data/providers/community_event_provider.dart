import 'package:flutter/material.dart';
import 'package:linkmom/data/network/api_header.dart';
import 'package:linkmom/data/network/models/community_event_list_response.dart';
import 'package:linkmom/data/network/models/community_event_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';

class CommunityEventProvider extends ChangeNotifier {
  List<EventListData> _eventList = [];
  CommnunityEventRequest _request = CommnunityEventRequest();

  bool _showLoading = false;

  bool hasNext(int tab) => _eventList[tab].next.isNotEmpty;

  bool get showLoading => _showLoading;

  List<EventListData> get getDatas => this._eventList;
  EventListData getData(int index) => this._eventList[index];

  void fetch() {
    _requestEventList();
  }

  void refresh(int tab) {
    _requestEvent(tab);
  }

  void next(int tab) {
    if (_eventList[tab].next.isNotEmpty) {
      apiHelper.apiCall(HttpType.post, this._eventList[tab].next, ApiHeader().publicApiHeader, _request).then((response) {
        var responseJson = Commons.returnResponse(response);
        CommunityEventListResponse responseData = CommunityEventListResponse.fromJson(responseJson);
        if (responseData.getCode() == KEY_SUCCESS) {
          this._eventList[tab].next = responseData.getData().next;
          this._eventList[tab].results!.addAll(responseData.getData().results ?? []);
        } else {
          this._eventList[tab].next = '';
        }
        notifyListeners();
      }).catchError((e) {
        log.e({
          '--- TITLE    ': '--------------- EXCEPTION ---------------',
          '--- DATA     ': e,
        });
        notifyListeners();
      });
    }
  }

  Future<void> _requestEvent(int tab, {String url = ''}) async {
    try {
      CommnunityEventRequest req = CommnunityEventRequest(status: tab.toString());
      await apiHelper.reqeustCommunityEvent(req, url: url.isNotEmpty ? url : null).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _eventList[tab] = response.getData();
        } else {
          _eventList[tab].results = [];
          _eventList[tab].count = 0;
          _eventList[tab].next = '';
        }
        notifyListeners();
      }).catchError((e) {
        log.e({
          '--- TITLE    ': '--------------- EXCEPTION ---------------',
          '--- DATA     ': e,
        });
        notifyListeners();
      });
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>> $e");
    }
  }

  Future<void> _requestEventList() async {
    try {
      if (_eventList.isEmpty) _eventList = List.generate(2, (index) => EventListData(results: []));
      Future.forEach([0, 1], (int tab) => _requestEvent(tab));
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>> $e");
    }
  }

  @override
  void addListener(VoidCallback listener) {
    this.removeListener(listener);
    super.addListener(listener);
  }
}
