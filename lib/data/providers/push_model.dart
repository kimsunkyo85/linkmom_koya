import 'package:flutter/material.dart';

class PushModel extends ChangeNotifier {
  PushModel();

  ///알림 Badge
  get badgeNotify => _badgeNotify;
  int _badgeNotify = 0;

  ///채팅 Badge
  get badgeChat => _badgeChat;
  int _badgeChat = 0;

  setBadgeNotify({bool isUpdate = false}) {
    _badgeNotify++;
    if (isUpdate) {
      notifyListeners();
    }
  }

  setBadgeNotifyClear({bool isUpdate = false}) {
    _badgeNotify = 0;
    if (isUpdate) {
      notifyListeners();
    }
  }

  setBadgeChat({bool isUpdate = false}) {
    _badgeChat++;
    if (isUpdate) {
      notifyListeners();
    }
  }

  setBadgeChatClear({bool isUpdate = false}) {
    _badgeChat = 0;

    if (isUpdate) {
      notifyListeners();
    }
  }

  setBadgeAllClear({bool isUpdate = false}) {
    setBadgeNotifyClear();
    setBadgeChatClear();
  }

  bool isUpdate() {
    // log.d('『GGUMBI』>>> build : _pushProvider : : $_badgeChat,  $_badgeNotify <<< ');
    if (_badgeChat > 0 || _badgeNotify > 0) {
      return true;
    }
    return false;
  }

  @override
  String toString() {
    return 'PushModel{_badgeNotify: $_badgeNotify, _badgeChat: $_badgeChat}';
  }
//
// get isUpdate => _isUpdate;
// bool _isUpdate = false;
//
// void setUpdate(bool isSelect) {
//   _isUpdate = isSelect;
//   notifyListeners();
// }

}
