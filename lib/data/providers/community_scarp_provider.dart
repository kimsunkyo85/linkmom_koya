import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/api_header.dart';
import 'package:linkmom/data/network/models/community_list_request.dart';
import 'package:linkmom/data/network/models/community_list_response.dart';
import 'package:linkmom/data/network/models/community_scrap_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';


class CommunityScrapProvider extends ChangeNotifier {
  CommunityListData _communityList = CommunityListData(results: [], region: []);
  CommunityListRequest _request = CommunityListRequest();
  bool _showLoading = false;

  Region _all = Region(region2Depth: "전국".tr(), region3Depth: "전국".tr(), hcode: 0);
  Region _default = Region(region2Depth: '수원시 영통구', region3Depth: '광교2동', hcode: 4111700000);

  List<CommunityBoardData> get getDatas => this._communityList.results;
  List<Region> get getRegions => this._communityList.region ?? [];

  bool get showLoading => this._showLoading;
  bool get hasNext => this._communityList.next.isNotEmpty;

  bool _isDoNext = false;

  int _count = 0;
  int get count => this._count;

  @override
  void notifyListeners() {
    _showLoading = false;
    super.notifyListeners();
  }

  void _requestCommunityList({
    List<int>? category,
    String isMine = '',
    String isScrap = '',
    String hcode = '',
  }) {
    try {
      _showLoading = true;
      if (!_communityList.region!.contains(_all)) _communityList.region!.add(_all);
      _request = CommunityListRequest(category: category, isMine: isMine, isScrap: isScrap, hcode: hcode);
      apiHelper.reqeustCommunityList(_request).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          this._count = response.getData().count;
          this._communityList.results = response.getData().results;
          this._communityList.next = response.getData().next;
          response.getData().region!.forEach((region) {
            if (this._communityList.region!.where((reg) => reg.region2Depth == region.region2Depth).isEmpty) {
              this._communityList.region!.add(region);
            }
          });
        } else {
          this._communityList.region = [_all, _default];
        }
        notifyListeners();
      });
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
      notifyListeners();
    }
  }

  void fetch({
    List<int>? category,
    String isMine = '',
    String isScrap = '',
    String hcode = '',
  }) {
    _requestCommunityList(category: category ?? [], isMine: isMine, isScrap: isScrap, hcode: hcode);
  }

  void refresh() {
    _requestCommunityList(
      category: _request.category,
      isMine: _request.isMine,
      isScrap: _request.isScrap,
      hcode: _request.hcode,
    );
  }

  void next() {
    if (_isDoNext) return;
    if (_communityList.next.isNotEmpty) {
      _isDoNext = true;
      apiHelper.apiCall(HttpType.post, this._communityList.next, ApiHeader().publicApiHeader, _request).then((response) {
        _isDoNext = false;
        var responseJson = Commons.returnResponse(response);
        CommunityListResponse responseData = CommunityListResponse.fromJson(responseJson);
        if (responseData.getCode() == KEY_SUCCESS) {
          this._communityList.next = responseData.getData().next;
          this._communityList.results.addAll(responseData.getData().results);
        } else {
          this._communityList.next = '';
        }
        notifyListeners();
      }).catchError((e) {
        log.e({
          '--- TITLE    ': '--------------- EXCEPTION ---------------',
          '--- DATA     ': e,
        });
        notifyListeners();
      });
    }
  }

  void bookmark(int id, bool isScrap) {
    try {
      if (isScrap) {
        apiHelper.reqeustCommunityScrapDelete(CommnunityScrapRequest(shareId: id)).then((response) => notifyListeners());
      } else {
        apiHelper.reqeustCommunityScrap(CommnunityScrapRequest(shareId: id)).then((response) => notifyListeners());
      }
    } catch (e) {
      notifyListeners();
    }
  }
}
