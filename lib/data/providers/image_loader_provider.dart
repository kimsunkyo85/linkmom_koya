import 'dart:io';

import 'package:flutter/material.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/utils/commons.dart';

class ImageLoader with ChangeNotifier {
  List<File> _images = [];
  Function? _update;

  @override
  void notifyListeners() {
    super.notifyListeners();
    if (_update != null) this._update!();
  }

  void loadImage(List<String> urls, BuildContext context) {
    _images.clear();
    Future.forEach(urls, (String url) async {
      File image = await Commons.networkToFile(context, url);
      _images.add(image);
      log.d({
        'ImageLoader': 'add image',
        "count": "${_images.length} / ${urls.length}",
      });
      notifyListeners();
    });
  }

  List<File> get getImages => this._images;

  set setImages(List<File> images) {
    this._images = images;
    notifyListeners();
  }

  void clear() {
    this._images.clear();
    notifyListeners();
  }

  set setUpdateCallback(Function callback) => this._update = callback;
}
