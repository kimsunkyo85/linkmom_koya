import 'package:flutter/material.dart';

class LoadingModel extends ChangeNotifier {
  LoadingModel();

  bool _isLoading = false;

  get isLoading => _isLoading;

  void showLoading() {
    _isLoading = true;
    notifyListeners();
  }

  void hideLoading() {
    _isLoading = false;
    notifyListeners();
  }
}
