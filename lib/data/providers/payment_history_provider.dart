import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/cash_list_response.dart';
import 'package:linkmom/data/network/models/pay_list_response.dart';
import 'package:linkmom/data/network/models/point_list_response.dart';

class PaymentHistoryProvider extends ChangeNotifier {
  PayList _payList = PayList(results: []);
  CashList _cashList = CashList(results: []);
  PointList _pointList = PointList(results: []);

  PayList get pay => this._payList;
  CashList get cash => this._cashList;
  PointList get point => this._pointList;

  bool get payHasNext => this._payList.next.isNotEmpty;
  bool get cashHasNext => this._cashList.next.isNotEmpty;
  bool get pointHasNext => this._pointList.next.isNotEmpty;

  set setPay(PayList list) {
    this._payList = list;
    notifyListeners();
  }

  set setCash(CashList list) {
    this._cashList = list;
    notifyListeners();
  }

  set setPoint(PointList list) {
    this._pointList = list;
    notifyListeners();
  }

  void appendPay(PayList list) {
    this._payList.results.addAll(list.results);
    this._payList.next = list.next;
    this._payList.previous = list.previous;
    notifyListeners();
  }

  void appendCash(CashList list) {
    this._cashList.results.addAll(list.results);
    this._cashList.next = list.next;
    this._cashList.previous = list.previous;
    notifyListeners();
  }

  void appendPoint(PointList list) {
    this._pointList.results.addAll(list.results);
    this._pointList.next = list.next;
    this._pointList.previous = list.previous;
    notifyListeners();
  }
}
