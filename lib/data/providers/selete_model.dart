import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';

class SelectModel extends ChangeNotifier {
  SelectModel();

  get isSelect => _isSelect;
  bool _isSelect = false;

  get action => _action;
  DialogAction _action = DialogAction.no;

  get totalTime => _totalTime;
  int _totalTime = 0;

  void setSelect(bool isSelect) {
    setAction(DialogAction.update);
    _isSelect = isSelect;
    notifyListeners();
  }

  void setAction(DialogAction action) {
    _action = action;
  }

  void setTotalTime({int totalTime = 0}) {
    _totalTime = totalTime;
    notifyListeners();
  }

  void setTotalTimeReset() {
    _totalTime = 0;
  }
}
