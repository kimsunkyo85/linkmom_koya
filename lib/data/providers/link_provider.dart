import 'package:flutter/material.dart';
import 'package:linkmom/data/storage/flag_manage.dart';
import 'package:linkmom/utils/route_action.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';

class LinkProvider extends ChangeNotifier {
  String? _link;
  BuildContext? _context;
  Map<String, Function> _queue = {};

  // get Link
  String get getLink => this._link ?? '';

  bool isValid(String url) {
    if (url.isEmpty) return false;
    String routeScheme = 'linkmom:///';
    String clearUrl = url.replaceAll(routeScheme, '').replaceAll(routeLink, '');
    List<String> routes = clearUrl.split('/').where((e) => e.isNotEmpty).toList();
    routes.toSet();
    if (routes.isNotEmpty && routes.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  void received(BuildContext context, String link) {
    this._link = '';
    this._context = context;
    log.d('DeeplinkProvider: received() $context $link');
    this._queue.clear();
    this._queue.putIfAbsent(link, () => run);
    start(link);
    notifyListeners();
  }

  void run(String link) {
    log.d('DeeplinkProvider: run $link');
    if (isValid(link)) {
      if (link.contains(RouteAction.linkEvent) && !auth.getSettings.is_auth_login && !Commons.isLogin) {
        moveOnLink(link);
      } else if (!Commons.isLogin) {
        Flags.shownCachedFlag(Flags.KEY_DEEPLINK);
        log.d('DeeplinkProvider: run() not loginned');
        this._link = link;
        Commons.pageClear(this._context, routeSplash);
      } else {
        moveOnLink(link);
      }
    } else {
      log.d('DeeplinkProvider: run() $link is not valid');
    }
  }

  void moveOnLink(String link) {
    this._queue.clear();
    linkClear();
    RouteAction.onLink(this._context!, link);
  }

  void start(String link) {
    log.d('DeeplinkProvider: start() with timer $link');
    Future.delayed(Duration(milliseconds: 500), () {
      log.d('DeeplinkProvider: excute timer $link ${this._queue[link]}');
      if (this._queue[link] != null) {
        this._queue[link]!(link);
      }
    });
  }

  bool linkClear() {
    this._link = null;
    Flags.removeCachedFlag(Flags.KEY_DEEPLINK);
    notifyListeners();
    return this._link == null;
  }

  @override
  void notifyListeners() {
    super.notifyListeners();
  }
}
