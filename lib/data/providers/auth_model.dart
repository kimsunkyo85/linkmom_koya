import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/api_header.dart';
import 'package:linkmom/data/network/api_helper.dart';
import 'package:linkmom/data/network/models/child_info_response.dart';
import 'package:linkmom/data/network/models/index_response.dart';
import 'package:linkmom/data/network/models/login_request.dart';
import 'package:linkmom/data/network/models/logout_request.dart';
import 'package:linkmom/data/network/models/myaccount_response.dart';
import 'package:linkmom/data/network/models/mypage_myinfo_response.dart';
import 'package:linkmom/data/network/models/refresh_request.dart';
import 'package:linkmom/data/network/models/settings_response.dart';
import 'package:linkmom/data/network/models/user.dart';
import 'package:linkmom/data/network/models/version_check_response.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';

import '../../main.dart';
import '../../route_name.dart';

class AuthModel extends ChangeNotifier {
  final _apiHeader = ApiHeader();
  SettingsData _settings = SettingsData();

  set setSettings(SettingsData settings) => _settings = settings;

  SettingsData get getSettings => _settings;

  set setAutoLogin(bool value) => _settings.is_auth_login = value;

  bool get getAutoLogin => _settings.is_auth_login;

  bool _useBio = false;

  bool get isBioSetup => _useBio;

  void setBioSetup(bool value) async {
    _useBio = value;
    storageHelper.setBioSetup(value);
    notifyListeners();
  }

  User _user = User();

  User get user => _user;

  void setUser(User value) async {
    _user = value;
    storageHelper.setUser(value);
    notifyListeners();
  }

  VersionCheckData _version = VersionCheckData();

  void setTokenCsrf(String? value) async {
    log.d('『GGUMBI』>>> setTokenCsrf : value: $value, _user: $_user, <<< ');
    _user.token_csrf = value!;
    _apiHeader.setTokenCSRF(value);
    storageHelper.setUser(_user);
  }

  void loadSettings() async {
    try {
      _useBio = await storageHelper.getBioSetup();
      _user = await storageHelper.getUser();
      log.d('『GGUMBI』>>> loadSettings : *************** DataLoad STart ***************\n _user: $_user,<<< ');
      if (_user.auto_login) {
        if (!kIsWeb && _useBio) {
          // if (await biometrics()) {
          //   _user = _user;
          // }
        } else {
          _user = _user;
        }
      }
    } catch (e) {
      log.e('『GGUMBI』>>> loadSettings : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    notifyListeners();
  }

  ///토큰 갱신시 사용 할것 추후
  List<RequestItem> getTokenApiList() {
    List<RequestItem> callList = [];
    RefreshRequest refreshRequest = RefreshRequest(refreshtoken: _user.token_refresh, csrftoken: _user.token_csrf);
    //토큰 갱신
    _apiHeader.setTokenRefresh(_user.token_csrf, _user.token_refresh);
    callList.add(RequestItem(
      callType: HttpType.post,
      url: ApiEndPoint.EP_TOKEN_REFRESH,
      header: _apiHeader.publicApiHeader,
      data: refreshRequest,
    ));
    LoginRequest loginData = LoginRequest(
      username: auth.user.username,
      password: auth.user.password,
      is_user_momdady: auth.user.momdaddy,
      is_user_linkmom: auth.user.linkmom,
    );
    //로그인
    callList.add(RequestItem(
      callType: HttpType.post,
      url: ApiEndPoint.EP_LOGIN,
      header: _apiHeader.publicApiHeader,
      data: loginData,
    ));

    return callList;
  }

  onUpdateUser(
    int id,
    String username,
    String password,
    bool isAutoLogin,
    String? accessToken,
    String? refreshToken,
    String? csrfToken,
    String? csrfTokenExpire, {
    MyInfoData? myinfoData,
    String aesKey = '',
  }) async {
    try {
      int _momdaddy = await storageHelper.getMomdady();
      int _linkmom = await storageHelper.getLinkmom();

      String AccessExpire = JwtDecoder.getExpirationDate(accessToken ?? '').toString();
      String RefreshExpire = JwtDecoder.getExpirationDate(refreshToken ?? '').toString();
      bool isAccessExpire = JwtDecoder.isExpired(accessToken ?? '');
      bool isRefreshExpire = JwtDecoder.isExpired(refreshToken ?? '');
      bool isCsrfhExpire = StringUtils.validateString(csrfTokenExpire ?? '') ? Commons.isExpired(Commons.parseCookieDate(csrfTokenExpire ?? '')) : false;

      log.d({
        '_access_expire': AccessExpire,
        '_refresh_expire': RefreshExpire,
        'csrfTokenExpire': csrfTokenExpire,
        'isAccessExpite': isAccessExpire,
        'isRefreshExpite': isRefreshExpire,
        'isCsrfhExpire': isCsrfhExpire,
        '===============================': '',
        'id': id,
        'username': username,
        'password': password,
        '_momdaddy': _momdaddy,
        '_linkmom': _linkmom,
        'isAutoLogin': isAutoLogin,
        'accessToken': accessToken,
        'refreshToken': refreshToken,
        'csrfToken': csrfToken,
        '_access_expire': AccessExpire,
        '_refresh_expire': RefreshExpire,
        'csfrTokenExpire': csrfTokenExpire,
      });

      _user = User(
          id: id,
          username: username,
          password: password,
          auto_login: isAutoLogin,
          momdaddy: _momdaddy,
          linkmom: _linkmom,
          token_access: accessToken ?? '',
          token_refresh: refreshToken ?? '',
          token_csrf: csrfToken ?? '',
          token_access_expire: AccessExpire,
          token_refresh_expire: RefreshExpire,
          token_csrf_expire: csrfTokenExpire ?? '',
          my_info_data: myinfoData ?? this.getMyInfoData,
          aesKey: aesKey);
      storageHelper.setUser(_user);
      _apiHeader.setTokenCSRF(csrfToken ?? '');
      _apiHeader.setTokenAccess(accessToken ?? '');
      storageHelper.getMenuFileData();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> onUpdateUser : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e $aesKey');
      return;
    }
  }

  ChildInfo _childInfo = ChildInfo();

  ChildInfo get childInfo => _childInfo;

  ChildInfoData _childInfoItemData = ChildInfoData();

  ChildInfoData get childInfoItemData => _childInfoItemData;

  onUpdateChildInfo(int step, String name, int age, int nursery, int gender, List<int> character, {ChildInfoData? data}) async {
    log.i({
      'step': step,
      'name': name,
      'age': age,
      'nursery': nursery,
      'gender': gender,
      'character': character,
      'data': data,
    });

    _childInfoItemData = ChildInfoData.clone(data ?? ChildInfoData());

    _childInfo = ChildInfo(
      step: step,
      name: name,
      age: age,
      nursery: nursery,
      gender: gender,
      character: character,
      is_allergy: _childInfoItemData.is_allergy,
      allergy_name: _childInfoItemData.allergy_name,
      allergy_message: _childInfoItemData.allergy_message,
    );
  }

  ///Token 갱신 요청
  Future<bool> requestToken() async {
    RefreshRequest data = RefreshRequest(refreshtoken: _user.token_refresh, csrftoken: _user.token_csrf);
    bool isExpire = false;
    await apiHelper.requestRefresh(data).then((response) {
      //통신 받는 onRequest에서 호출한다.
      if (response.getCode() != KEY_SUCCESS) {
        isExpire = true;
        return isExpire;
      }
      String? csrfToken = response.dataCookies[HEADER_KEY_CSRFTOKEN];
      String? csrfTokenExpire = response.dataCookies[HEADER_KEY_CSRFTOKEN_EXPIRE];
      String? refreshToken = response.dataCookies[HEADER_KEY_REFRESH_TOKEN];

      ///csrf값이 없으면 기존 값으로 대처한다.
      if (!StringUtils.validateString(csrfToken)) {
        csrfToken = _user.token_csrf;
        csrfTokenExpire = _user.token_csrf_expire;
      }

      if (!StringUtils.validateString(refreshToken)) {
        refreshToken = _user.token_refresh;
      }

      onUpdateUser(
        _user.id,
        _user.username,
        _user.password,
        _user.auto_login,
        response.getData().access,
        refreshToken,
        csrfToken,
        csrfTokenExpire,
      );
    });
    return isExpire;
  }

  ///로그아웃 요청
  Future<void> requestLogout(BuildContext context) async {
    apiHelper.requestLogout(LogoutRequest(fcm_token: auth.user.token_fcm)).then((response) {
      _user.setClear(response.dataCookies[HEADER_KEY_CSRFTOKEN], response.dataCookies[HEADER_KEY_CSRFTOKEN_EXPIRE]);
      setUserAllClear(context: context);
    }).catchError((e) {
      _user.setClear('', '');
      setUserAllClear(context: context);
    });
    return;
  }

  setUserAllClear({BuildContext? context}) {
    ///2021/11/09 로그아웃시 해당 id
    pushFinger.removeId();
    _apiHeader.setTokenClear();
    storageHelper.removeAll();
    storageHelper.setUserClear(isUpdate: false).then((value) {
      if (context != null) {
        Commons.pageClear(context, routeLogin);
      }
    });
  }

  MyAccountData account = MyAccountData();

  set setAccount(MyAccountData accountData) => account = accountData;

  get getAccount => account;

  set setEmail(String email) => account.email = email;

  get getEmail => account.email;

  set setEmailAuth(bool authState) => account.is_auth_email = authState;

  get getEmailAuth => account.is_auth_email;

  set setHpNumber(String number) => account.hpnumber = number;

  get getHpNumber => account.hpnumber;

  set setMyInfoData(MyInfoData _myInfoData) => user.my_info_data;

  MyInfoData get getMyInfoData => user.my_info_data ?? MyInfoData.init();

  get version => '${_version.verMajor}.${StringUtils.getNumberAddZero(_version.verMinor)}.${StringUtils.getNumberAddZero(_version.verPatch)}';

  VersionCheckData get getVersion => _version;

  set setVersion(VersionCheckData version) => _version = version;

  IndexData _index =
      IndexData(
    eventBanner: [],
    likeRanking: [],
    linkmominfo: Linkmominfo(carediary: []),
    momdadyinfo: Momdadyinfo(carediary: [], community: [], communityShare: [], communityReview: [], communityAll: []),
    priceRanking: [],
    reviewRanking: [],
    userinfo: Userinfo(),
    adBanner: [],
  );

  IndexData get indexInfo => _index;

  set setIndexInfo(IndexData info) => _index = info;
}
