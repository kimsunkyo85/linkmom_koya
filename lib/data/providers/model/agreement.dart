import 'package:linkmom/manager/enum_manager.dart';

class Agreement {
  late int index;
  late String title;
  late Terms id;
  late bool completed;
  late bool isItem;
  late bool isVisible;

  Agreement({required this.title, this.id = Terms.privacy, this.completed = false, this.isItem = true, this.isVisible = false, Function onClickAction(DialogAction action)?});

  void toggleCompleted() {
    completed = !completed;
  }

  void toggleVisible() {
    isVisible = !isVisible;
  }
}
