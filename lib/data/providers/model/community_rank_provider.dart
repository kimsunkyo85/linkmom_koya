import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/community_list_response.dart';
import 'package:linkmom/data/network/models/community_rank_request.dart';
import 'package:linkmom/data/network/models/community_rank_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';

class CommunityRankProvider extends ChangeNotifier {
  bool _showLoading = false;
  Region _all = Region(region2Depth: "전국".tr(), region3Depth: "전국".tr(), hcode: 0);
  Region _default = Region(region2Depth: '수원시 영통구', region3Depth: '하동', hcode: 4111761000);

  List<RankListData> _rankListData = [RankListData(region: [], results: []), RankListData(region: [], results: []), RankListData(region: [], results: [])];
  CommunityRankRequest _request = CommunityRankRequest();

  List<RankResults> getDatas(int index) => this._rankListData[index].results ?? [];
  List<RankListData> get getDataList => this._rankListData;
  List<Region> get getRegions => this._rankListData.first.region ?? [];

  bool get showLoading => this._showLoading;
  bool hasNext(int index) => this._rankListData[index].next.isNotEmpty;

  @override
  void notifyListeners() {
    _showLoading = false;
    super.notifyListeners();
  }

  void fetch({
    int? hcode,
  }) {
    if (this._rankListData.first.region!.isEmpty) this._rankListData.first.region = [_all];
    _requestAllRankList(hcode);
  }

  void refresh(
    int index, {
    int? hcode,
  }) {
    _requestRank(
      index,
      hcode: hcode,
    );
  }

  Future _requestAllRankList(int? hcode) async {
    try {
      this._showLoading = true;
      Future.forEach(RankType.values, (RankType type) => _requestRank(type.index, hcode: hcode));
    } catch (e) {
      _rankListData.first.region = [_all, _default];
      notifyListeners();
    }
  }

  Future<void> _requestRank(int index, {int? hcode, String url = ''}) async {
    _request = CommunityRankRequest(gubun: index, hcode: (hcode ?? _all.hcode).toString());
    await apiHelper.reqeustCommunityRankList(_request, url: url.isEmpty ? null : url).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        if (url.isEmpty) {
          this._rankListData[index].results = response.getData().results ?? [];
        } else {
          if (this._rankListData[index].results == null) this._rankListData[index].results = [];
          this._rankListData[index].results!.addAll(response.getData().results ?? []);
        }
        this._rankListData[index].next = response.getData().next;
        if (index == 0)
          response.getData().region!.forEach((region) {
            if (this._rankListData[index].region!.where((reg) => reg.region2Depth == region.region2Depth).isEmpty) {
              this._rankListData[index].region!.add(region);
            }
          });
      } else {
        this._rankListData[index].region = [_all, _default];
      }
      notifyListeners();
    }).catchError((e) {
      notifyListeners();
    });
  }

  void next(int index) {
    _requestRank(index, url: _rankListData[index].next, hcode: int.tryParse(_request.hcode) ?? _all.hcode);
  }

  @override
  void addListener(VoidCallback listener) {
    this.removeListener(listener);
    super.addListener(listener);
  }
}
