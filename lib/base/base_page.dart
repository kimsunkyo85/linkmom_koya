import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/register_request.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

class BasePage extends BaseStateful {
  RegisterRequest registerData = RegisterRequest();

  ///이용약관에서 넘어 온 값
  BasePage({required this.registerData});

  RegisterPageState createState() => RegisterPageState();
}

class RegisterPageState extends BaseStatefulState<BasePage> {
  ///회원가입 입력폼 에러 체크 플래그
  bool isDebug = false;

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      Scaffold(
        key: data.scaffoldKey,
        appBar: appBar("정보입력".tr()),
        body: Container(
          padding: padding_20, //전체 패딩 좌우
          child: Column(
            children: [
              Expanded(
                child: lScrollView(
                  padding: padding_0,
                  child: Container(
                    child: Stack(
                      children: <Widget>[
                        Container(
                          child: Column(
                            children: <Widget>[
                              lText('기본 페이지'),
                              sb_h_20,
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              _confirmButton(),
            ],
          ),
        ),
      ),
      showText: false,
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn("확인".tr(), isEnabled: flag.isConfirm, onClickAction: () {
      data.registerItem.registerErrors = RegisterRequest();
    });
  }
}
