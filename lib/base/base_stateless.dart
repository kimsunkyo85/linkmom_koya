import 'package:flutter/material.dart';

import '../manager/data_manager.dart';
import '../utils/view_util.dart';

class BaseStateless extends StatelessWidget {
  BaseStateless({Key? key});

  ViewFlag flag = ViewFlag();
  DataManager data = DataManager();

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
