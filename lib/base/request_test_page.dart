import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/req_data.dart';
import 'package:linkmom/data/network/models/temp_request.dart';
import 'package:linkmom/data/network/models/temp_save_request.dart';
import 'package:linkmom/data/network/models/token_response.dart';
import 'package:linkmom/data/storage/storage_help.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:stack_trace/stack_trace.dart';

import '../data/network/models/authcenter_home_request.dart';
import '../main.dart';
import '../view/image/image_page.dart';

MqttServerClient client = MqttServerClient.withPort('192.168.0.142', '60', 1883);

class RequestTestPage extends BaseStateful {
  @override
  _RequestTestPageState createState() => _RequestTestPageState();
}

class _RequestTestPageState extends BaseStatefulState<RequestTestPage> {
  late TokenResponse refresh;
  bool isLoding = false;
  String text = "마이페이지";
  int segmentedControlValue = 0;
  List<File> _image = [];

  // StorageHelper helper = StorageHelper();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: isLoding,
        // demo of some additional parameters
        opacity: 0.5,
        dismissible: false,
        progressIndicator: CircularProgressIndicator(),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              btnTokenFirebase(),
              btnMqttConnect(),
              btnMqttDisconnect(),
              btnMqttSend(),
              // segmentedBtn(helper),
              imageBtn(),
              authCenterHomeSave(),
              authCenterHomeView(),
              authCenterHomeDelete(),
              imageUpload(),
              MaterialButton(
                child: lText('임시저장가져오기'),
                onPressed: () {
                  apiHelper.requestTempView(TempRequest(child: 13, servicetype: 0, depth: 0));
                  // Commons.pagePushLeft(context, fn: () => ScreenUtilPage());
                },
              ),
              MaterialButton(
                child: lText('임시저장'),
                onPressed: () {
                  TempSaveRequest datas = TempSaveRequest(child: 13, servicetype: 0, depth: 0);
                  ReqData reqdata = ReqData(careschedule: []);
                  reqdata = ReqData(careschedule: []);
                  ReqScheduleData item1 = ReqScheduleData(stime: "2021-02-08 09:00:00", etime: "2021-02-09 11:00:00", bookingdate: []);
                  reqdata.careschedule!.add(item1);
                  ReqSchoolToHomeData item2 = ReqSchoolToHomeData(
                    start_address: "서울시 종로구 00번지",
                    start_comment: "승강장에서 출발",
                    start_time: "09시",
                    end_address: "제주시 제주읍 99번지",
                    end_comment: "안전하게 도착",
                    end_time: "10시 30분까지",
                  );
                  reqdata.bookinggotoschool!.add(item2);

                  item2 = ReqSchoolToHomeData(start_address: "서울시 종로구 00번지", start_comment: "승강장에서 출발", start_time: "09시", end_address: "제주시 제주읍 99번지", end_comment: "안전하게 도착", end_time: "10시 30분까지");
                  reqdata.bookingafterschool!.add(item2);

                  ReqCareOptionData item3 = ReqCareOptionData(product_id: 5, product_name: "양치/세수/손발", product_cost: 2500, product_comment: "치약은 00버전으로");
                  item3 = ReqCareOptionData(product_id: 8, product_name: "드라이기", product_cost: 2500, product_comment: "드라이기는 차가운 바람으로");
                  reqdata.bookingboyukoptions!.add(item3);

                  item3 = ReqCareOptionData(product_id: 35, product_name: "(학습지도)온라인수업", product_cost: 4000, product_comment: "온라인수업 사이트 안내");
                  reqdata.bookingplayoptions!.add(item3);
                  item3 = ReqCareOptionData(product_id: 36, product_name: "(학습지도)학원숙제", product_cost: 4000, product_comment: "학원 숙제:수학1쪽");
                  reqdata.bookingplayoptions!.add(item3);

                  item3 = ReqCareOptionData(product_id: 5, product_name: "양치/세수/손발", product_cost: 2500, product_comment: "치약은 00버전으로");
                  item3 = ReqCareOptionData(product_id: 11, product_name: "아침식사", product_cost: 4000, product_comment: "아침은 Bread");
                  item3 = ReqCareOptionData(product_id: 18, product_name: "모든 설거지", product_cost: 4000, product_comment: "설거지 후,식기세척기로 건조해주세요.");
                  reqdata.bookinghomecareoptions!.add(item3);
                  datas.reqdata = jsonEncode(reqdata);

                  // datas.reqdata = MapEntry("test", 'test1').toString();
                  // var jsonString = '{"test": "test"}';
                  // datas.reqdata = jsonString;
                  apiHelper.requestTempSave(datas);
                  // Commons.pagePushLeft(context, fn: () => ScreenUtilPage());
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget btnTokenFirebase() {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('파이어베이스', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () => {},
        ),
      ),
    );
  }

  Widget btnMqttConnect() {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('MQTT Connect', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () => {
            connect(),
          },
        ),
      ),
    );
  }

  Widget btnMqttDisconnect() {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('MQTT DisConnect', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () => {
            client.disconnect(),
          },
        ),
      ),
    );
  }

  Widget btnMqttSend() {
    const pubTopic = 'test';
    final MqttClientPayloadBuilder? builder = MqttClientPayloadBuilder();
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('MQTT Send', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () => {
            // client.subscribe('test', MqttQos.atLeastOnce),
            builder!.addString('Hello MQTT Flutter'),
            client.publishMessage(pubTopic, MqttQos.atLeastOnce, builder.payload!),
          },
        ),
      ),
    );
  }

  Widget segmentedBtn(StorageHelper helper) {
    return Center(
      child: CupertinoSlidingSegmentedControl(
          groupValue: segmentedControlValue,
          backgroundColor: Colors.blue.shade200,
          children: <int, Widget>{0: lText('수요자'), 1: lText('공급자')},
          onValueChanged: (value) {
            setState(() {
              segmentedControlValue = int.parse(value.toString());
              // helper.loadUserType();
              if (value == 0) {
                helper.setUserType(USER_TYPE.mom_daddy);
              } else if (value == 1) {
                helper.setUserType(USER_TYPE.link_mom);
              }
            });
          }),
    );
  }

  Widget fingerPushBtn() {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('FingerPush', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () => {
            // Navigator.push(context, MaterialPageRoute(builder: (context) => BodyLayout())),
            // Navigator.push(context, MaterialPageRoute(builder: (context) => BodyLayout())),
          },
        ),
      ),
    );
  }

  Widget imageBtn() {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('Image', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () async {
            File image = await Navigator.push(context, MaterialPageRoute(builder: (context) => ImagePage()));
            _image.add(image);
            log.d('『GGUMBI』>>> imageBtn : _image: $_image,  <<< ');
            log.d('『GGUMBI』>>> imageBtn : _image: $image, ${image.path.split('/').last} <<< ');
          },
        ),
      ),
    );
  }

  Widget authCenterHomeSave() {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('Home State save', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () async {
            AuthCenterHomeRequest data = AuthCenterHomeRequest(
                ourhome_type: HomeType.apartment.index, ourhome_cctv: CctvType.yes.index, ourhome_animal: [AnimalType.cat.index], nbhhome_type: [HomeType.apartment.index], nbhhome_cctv: CctvType.yes.index, nbhhome_animal: AnimalType.cat.index, nbhhome_picture: FineType.ok.index);
            // AuthCenterHomeRequest(ourhome_type: HomeType.apartment.index, ourhome_cctv: CctvType.yes.index, ourhome_animal: AnimalType.cat.index, nbhhome_type: HomeType.apartment.index, nbhhome_cctv: CctvType.yes.index, nbhhome_animal: AnimalType.cat.index, nbhhome_picture: byteData);
            // log.d('『GGUMBI』>>> authCenterHomeSave : : ${File(_image.path).readAsStringSync()},  <<< ');
            // data.fileName = _image.path.split('/').last;
            // apiHelper.requestHomeStateView(data, HttpType.post, ApiEndPoint.EP_AUTH_CENTER_HOME_SAVE).then((response) {
            //   log.i({
            //     '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
            //     '--- response ': response.toString(),
            //   });
            // });
          },
        ),
      ),
    );
  }

  Widget authCenterHomeView() {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('Home State view', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () {
            AuthCenterHomeRequest data = AuthCenterHomeRequest();
            // apiHelper.requestHomeStateView(data, HttpType.get, ApiEndPoint.EP_AUTH_CENTER_HOME_VIEW).then((response) {
            //   log.i({
            //     '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
            //     '--- response ': response.toString(),
            //   });
            // });
          },
        ),
      ),
    );
  }

  Widget authCenterHomeDelete() {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('Home State delete', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () {
            AuthCenterHomeRequest data = AuthCenterHomeRequest();
            // apiHelper.requestHomeStateView(data, HttpType.delete, ApiEndPoint.EP_AUTH_CENTER_HOME_DELETE).then((response) {
            //   log.i({
            //     '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
            //     '--- response ': response.toString(),
            //   });
            // });
          },
        ),
      ),
    );
  }

  Widget imageUpload() {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('ImageUpLoad', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () {
            // Commons.pagePushLeft(context,fn: () => DownloadTestPage(title: 'download', platform: platform));
            // AuthCenterHomeImageRequest data = AuthCenterHomeImageRequest(ourhome_picture: _image, homestate: 1);
            // apiHelper.requestHomeStateImage(data, HttpType.post, 'http://192.168.0.112:8000/tests/img/save/').then((response) {
            //   log.i({
            //     '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
            //     '--- response ': response.toString(),
            //   });
            // });
          },
        ),
      ),
    );
  }

  Future<MqttServerClient> connect() async {
    // "192.168.0.142", 1883, 60

    client.logging(on: true);
    client.onConnected = onConnected;
    client.onDisconnected = onDisconnected;
    client.onUnsubscribed = onUnsubscribed;
    client.onSubscribed = onSubscribed;
    client.onSubscribeFail = onSubscribeFail;
    client.pongCallback = pong;

    // final connMessage = MqttConnectMessage().authenticateAs('username', 'password').keepAliveFor(60).withWillTopic('willtopic').withWillMessage('Will message').startClean().withWillQos(MqttQos.atLeastOnce);
    // client.connectionMessage = connMessage;

    try {
      await client.connect();
      print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::');
    } catch (e) {
      print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::$e');
      client.disconnect();
    }

    client.updates?.listen((dynamic c) {
      final MqttPublishMessage message = c[0].payload;
      final payload = MqttPublishPayload.bytesToStringAsString(message.payload.message);

      print('Received message:$payload from topic: ${c[0].topic}>');
    });

    return client;
  }

  // connection succeeded
  void onConnected() {
    print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::');
    client.subscribe('test', MqttQos.atLeastOnce);
  }

// unconnected
  void onDisconnected() {
    print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::');
  }

// subscribe to topic succeeded
  void onSubscribed(String topic) {
    print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::$topic');
  }

// subscribe to topic failed
  void onSubscribeFail(String topic) {
    print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::');
  }

// unsubscribe succeeded
  void onUnsubscribed(String? topic) {
    print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::');
  }

// PING response received
  void pong() {
    print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::');
  }
}
