import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';

import '../main.dart';

@JsonSerializable()
class BasicResponse extends HeaderResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<dynamic>? dataList;

  BasicResponse({
    required this.dataHeader,
    required this.dataList,
  });

  BaseData getData() => dataList![0];

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory BasicResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<BaseData> datas = [];
    try {
      log.d('『GGUMBI』>>> fromJson ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<<${json[Key_data]} ');
      datas = json[Key_data].map<BaseData>((i) => BaseData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return BasicResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }

  @override
  String toString() {
    return 'AuthCenterHomeResponse{dataHeader: $dataHeader, dataList: $dataList}';
  }
}

@JsonSerializable()
class BaseData {
  static const String Key_id = 'id';
  static const String Key_username = 'username';
  static const String Key_ourhome_cctv = 'ourhome_cctv';
  static const String Key_ourhome_animal = 'ourhome_animal';
  static const String Key_homestateimg = 'homestateimg';

  BaseData({
    this.id = 0,
    this.username = '',
    this.ourhome_animal,
    this.homestateimg,
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_username)
  final String username;
  @JsonKey(name: Key_ourhome_animal)
  final List<int>? ourhome_animal;
  @JsonKey(name: Key_homestateimg)
  final List<BaseItemData>? homestateimg;

  factory BaseData.fromJson(Map<String, dynamic> json) {
    List<int> animals = List<int>.from(json[Key_ourhome_animal]);
    List<BaseItemData> imges = json[Key_homestateimg] == null ? [] : json[Key_homestateimg].map<BaseItemData>((i) => BaseItemData.fromJson(i)).toList();

    return BaseData(
      id: json[Key_id] as int,
      username: json[Key_username] as String,
      ourhome_animal: animals,
      homestateimg: imges,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_username] = this.username;
    data[Key_ourhome_animal] = this.ourhome_animal;
    data[Key_homestateimg] = this.homestateimg;
    return data;
  }

  @override
  String toString() {
    return 'BaseData{id: $id, username: $username, ourhome_animal: $ourhome_animal, homestateimg: $homestateimg}';
  }
}

@JsonSerializable()
class BaseItemData {
  static const String Key_id = 'id';
  static const String Key_filename = 'filename';
  static const String Key_ourhome_picture = 'ourhome_picture';

  BaseItemData({
    this.id = 0,
    this.filename = '',
    this.ourhome_picture = '',
  });

  @JsonKey(name: Key_id)
  final int id;
  @JsonKey(name: Key_filename)
  final String filename;
  @JsonKey(name: Key_ourhome_picture)
  final String ourhome_picture;

  factory BaseItemData.fromJson(Map<String, dynamic> json) {
    log.d('『GGUMBI』>>> fromJson : json11111: $json,  <<< ');
    return BaseItemData(
      id: json[Key_id] as int,
      filename: json[Key_filename] as String,
      ourhome_picture: json[Key_ourhome_picture] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_id] = this.id;
    data[Key_filename] = this.filename;
    data[Key_ourhome_picture] = this.ourhome_picture;
    return data;
  }

  @override
  String toString() {
    return 'BaseItemData{id: $id, filename: $filename, ourhome_picture: $ourhome_picture}';
  }
}
