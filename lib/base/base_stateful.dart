import 'package:flutter/material.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/view_util.dart';

import '../main.dart';

class BaseStateful extends StatefulWidget {
  @override
  BaseStatefulState createState() => BaseStatefulState();
}

class BaseStatefulState<T extends BaseStateful> extends State<T> with RouteAware implements BaseFuntion {
  ViewFlag flag = ViewFlag();
  DataManager data = DataManager();
  ViewMode _viewMode = ViewMode();

  // ///화면 타입(기본 신청화면)
  // ViewType viewType = ViewType.apply;
  // ///아이템 타입(아이등록/수정/삭제 등등)
  // ItemType itemType = ItemType.add;

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  @override
  void initState() {
    super.initState();
    selectModel.setAction(DialogAction.no);
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant T oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context)!);
  }

  @override
  void didPush() {}

  @override
  void didPop() {}

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    // log.d('『GGUMBI』>>> didChangeAppLifecycleState : state: ${state},  <<< ');
    if (state == AppLifecycleState.resumed) {
      onResume();
    } else if (state == AppLifecycleState.inactive) {
      onInactive();
    } else if (state == AppLifecycleState.paused) {
      onPaused();
    } else if (state == AppLifecycleState.detached) {
      onDetached();
    }
  }

  @override
  onResume() {}

  @override
  onPaused() {}

  @override
  onDetached() {}

  @override
  onInactive() {}

  @override
  void onUpDate() {
    if (this.mounted) {
      setState(() {});
    }
  }

  @override
  void onData(DataManager _data) {}

  @override
  onViewMode(ViewMode _viewMode) {
    if (_viewMode == null) {
      _viewMode = ViewMode();
    }
    this._viewMode = _viewMode;
    setViewType(_viewMode.viewType);
    setItemType(_viewMode.itemType);
    log.d('『GGUMBI』>>> onViewMode : viewType: ${_viewMode.viewType}, itemType: ${_viewMode.itemType},  <<< ');
  }

  @override
  void onDataPage(dynamic _data) {}

  @override
  void onConfirmBtn() {}

  ///수정모드 확인
  bool isModifyMode() {
    return getViewType() == ViewType.modify;
  }

  @override
  getViewMode() {
    return _viewMode;
  }

  @override
  setItemType(ItemType _itemType) {
    this._viewMode.itemType = _itemType;
  }

  @override
  setViewType(ViewType _viewType) {
    this._viewMode.viewType = _viewType;
  }

  @override
  getItemType() {
    return _viewMode.itemType;
  }

  @override
  getViewType() {
    return _viewMode.viewType;
  }

  ///보육이 아닐 경우는 중복가능 문구를 보여준다.
  bool isBoyuk(ServiceType serviceType) {
    return Commons.isBoyuk(serviceType);
  }
}

abstract class BaseFuntion {
  onUpDate();

  onData(DataManager _data);

  onViewMode(ViewMode _viewMode);

  onDataPage(dynamic _data);

  onConfirmBtn();

  ViewMode getViewMode();

  setViewType(ViewType _viewType);

  ViewType getViewType();

  setItemType(ItemType _itemType);

  ItemType getItemType();

  onInactive();

  onPaused();

  onResume();

  onDetached();
}
