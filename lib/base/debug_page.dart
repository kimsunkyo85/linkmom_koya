
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/pay_cancel_explain_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_init_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_save_request.dart';
import 'package:linkmom/data/network/models/pay_cash_list_request.dart';
import 'package:linkmom/data/network/models/pay_init_request.dart';
import 'package:linkmom/data/network/models/pay_pay_list_request.dart';
import 'package:linkmom/data/network/models/pay_point_list_request.dart';
import 'package:linkmom/data/network/models/pay_prev_request.dart';
import 'package:linkmom/data/network/models/pay_save_request.dart';
import 'package:linkmom/data/network/models/token_response.dart';
import 'package:linkmom/data/storage/storage_help.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:stack_trace/stack_trace.dart';

import '../main.dart';
import '../manager/enum_manager.dart';
import '../utils/style/linkmom_style.dart';

MqttServerClient client = MqttServerClient.withPort('192.168.0.142', '60', 1883);

class DebugPage extends StatefulWidget {
  @override
  _DebugPageState createState() => _DebugPageState();
}

class _DebugPageState extends State<DebugPage> {
  TokenResponse? refresh;
  bool isLoding = false;
  String text = "마이페이지";
  int segmentedControlValue = 0;

  // StorageHelper helper = StorageHelper();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: isLoding,
        // demo of some additional parameters
        opacity: 0.5,
        dismissible: false,
        progressIndicator: CircularProgressIndicator(),
        child: ListView(
          children: [
            ListTile(
              title: lText("결제 요청 전"),
              onTap: () {
                apiHelper.reqeustPayInit(PayInitRequest(bookingcareservices: 55, carematching: 14, carecontract: 129)).then((response) {
                  log.i({
                    '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
                    '--- response ': response,
                  });
                });
              },
            ),
            ListTile(
              title: lText("캐시/포인트 사전결제"),
              onTap: () {
                apiHelper.reqeustPayRrevUsed(PayRrevRequest(order_id: "care-20210824-55-14-129", total_fee: 300000, used_cash: 1000, used_point: 3000)).then((response) {
                  log.i({
                    '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
                    '--- response ': response,
                  });
                });
              },
            ),
            ListTile(
              title: lText("캐시/포인트 사전결제 취소"),
              onTap: () {
                apiHelper.reqeustPayRrevBack(PayRrevRequest(order_id: "care-20210824-55-14-129", total_fee: 300000, used_cash: 1000, used_point: 3000)).then((response) {
                  log.i({
                    '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
                    '--- response ': response,
                  });
                });
              },
            ),
            ListTile(
              title: lText("결제요청 후 저장"),
              onTap: () {
                apiHelper.reqeustPaySave(PaySaveRequest(imp_uid: '', status: 'paid', merchant_uid: '')).then((response) {
                  log.i({
                    '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
                    '--- response ': response,
                  });
                });
              },
            ),
            ListTile(
              title: lText("결제취소 요청 전"),
              onTap: () {
                apiHelper.reqeustPayCancelInit(PayCancelInitRequest(order_id: "care-20210824-55-14-129", contract_id: 129)).then((response) {
                  log.i({
                    '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
                    '--- response ': response,
                  });
                });
              },
            ),
            ListTile(
              title: lText("결제취소 요청 저장"),
              onTap: () {
                apiHelper
                    .reqeustPayCancelSave(PayCancelSaveRequest(
                  order_id: "care-20210824-55-14-129",
                  contract_id: 129,
                  request_cancel_price: 300000,
                  cancelreason: 11,
                  cancelreason_msg: "취소내용",
                ))
                    .then((response) {
                  log.i({
                    '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
                    '--- response ': response,
                  });
                });
              },
            ),
            ListTile(
              title: lText("결제취소 소명하기"),
              onTap: () {
                apiHelper
                    .reqeustPayCancelExplain(PayCancelExplainRequest(
                  order_id: "care-20210824-55-14-129",
                  contract_id: 129,
                  cancelexplain_msg: "취소내용",
                ))
                    .then((response) {
                  log.i({
                    '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
                    '--- response ': response,
                  });
                });
              },
            ),
            ListTile(
              title: lText("결제리스트"),
              onTap: () {
                apiHelper
                    .reqeustPayList(PayPayListRequest(
                  s_date: "20210801",
                  e_date: "20210831",
                ))
                    .then((response) {
                  log.i({
                    '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
                    '--- response ': response,
                  });
                });
              },
            ),
            ListTile(
              title: lText("캐시리스트"),
              onTap: () {
                apiHelper
                    .reqeustPayCashList(PayCashListRequest(
                  s_date: "20210801",
                  e_date: "20210831",
                ))
                    .then((response) {
                  log.i({
                    '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
                    '--- response ': response,
                  });
                });
              },
            ),
            ListTile(
              title: lText("포인트리스트"),
              onTap: () {
                apiHelper
                    .reqeustPayPointList(PayPointListRequest(
                  s_date: "20210801",
                  e_date: "20210831",
                ))
                    .then((response) {
                  log.i({
                    '--- TITLE    ': '--------------- CALL PAGE RESPONSE END ---------------',
                    '--- response ': response,
                  });
                });
              },
            ),
          ],
        ),
        // child: lScrollView(
        //   child: Column(
        //     children: <Widget>[
        //       sb_h_20,
        //       btnTokenFirebase(),
        //       sb_h_20,
        //       btnMqttConnect(),
        //       sb_h_20,
        //       btnMqttDisconnect(),
        //       sb_h_20,
        //       btnMqttSend(),
        //       sb_h_20,
        //       segmentedBtn(helper),
        //       sb_h_20,
        //       fingerPushBtn(),
        //       sb_h_20,
        //       // SizedBox(
        //       //   height: 65,
        //       //   child: Container(
        //       //     child: BodyLayout(),
        //       //   ),
        //       // ),
        //       //
        //       // Center(
        //       //   child: Text(
        //       //     text,
        //       //     style: TextStyle(fontSize: 10),
        //       //   ),
        //       // ),
        //     ],
        //   ),
        // ),
      ),
    );
  }

  Widget btnTokenFirebase() {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('파이어베이스', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () => {},
        ),
      ),
    );
  }

  Widget btnMqttConnect() {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('MQTT Connect', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () => {
            connect(),
          },
        ),
      ),
    );
  }

  Widget btnMqttDisconnect() {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('MQTT DisConnect', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () => {
            client.disconnect(),
          },
        ),
      ),
    );
  }

  Widget btnMqttSend() {
    const pubTopic = 'test';
    final builder = MqttClientPayloadBuilder();
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('MQTT Send', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () => {
            // client.subscribe('test', MqttQos.atLeastOnce),
            builder.addString('Hello MQTT Flutter'),
            client.publishMessage(pubTopic, MqttQos.atLeastOnce, builder.payload!),
          },
        ),
      ),
    );
  }

  Widget segmentedBtn(StorageHelper helper) {
    return Center(
      child: CupertinoSlidingSegmentedControl(
          groupValue: segmentedControlValue,
          backgroundColor: Colors.blue.shade200,
          children: <int, Widget>{0: lText('수요자'), 1: lText('공급자')},
          onValueChanged: (value) {
            setState(() {
              segmentedControlValue = int.parse(value.toString());
              // helper.loadUserType();
              if (value == 0) {
                helper.setUserType(USER_TYPE.mom_daddy);
              } else if (value == 1) {
                helper.setUserType(USER_TYPE.link_mom);
              }
            });
          }),
    );
  }

  Widget fingerPushBtn() {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: RaisedButton(
          child: lText('FingerPush', style: stLoginBtn),
          color: color_main,
          highlightColor: color_main,
          onPressed: () => {
            // Navigator.push(context, MaterialPageRoute(builder: (context) => BodyLayout())),
            // Navigator.push(context, MaterialPageRoute(builder: (context) => BodyLayout())),
          },
        ),
      ),
    );
  }

  Future<MqttServerClient> connect() async {
    // "192.168.0.142", 1883, 60

    client.logging(on: true);
    client.onConnected = onConnected;
    client.onDisconnected = onDisconnected;
    client.onUnsubscribed = onUnsubscribed;
    client.onSubscribed = onSubscribed;
    client.onSubscribeFail = onSubscribeFail;
    client.pongCallback = pong;

    // final connMessage = MqttConnectMessage().authenticateAs('username', 'password').keepAliveFor(60).withWillTopic('willtopic').withWillMessage('Will message').startClean().withWillQos(MqttQos.atLeastOnce);
    // client.connectionMessage = connMessage;

    try {
      await client.connect();
      print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::');
    } catch (e) {
      print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::$e');
      client.disconnect();
    }

    client.updates?.listen((dynamic c) {
      final MqttPublishMessage message = c[0].payload;
      final payload = MqttPublishPayload.bytesToStringAsString(message.payload.message);

      print('Received message:$payload from topic: ${c[0].topic}>');
    });

    return client;
  }

  // connection succeeded
  void onConnected() {
    print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::');
    client.subscribe('test', MqttQos.atLeastOnce);
  }

// unconnected
  void onDisconnected() {
    print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::');
  }

// subscribe to topic succeeded
  void onSubscribed(String topic) {
    print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::$topic');
  }

// subscribe to topic failed
  void onSubscribeFail(String topic) {
    print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::');
  }

// unsubscribe succeeded
  void onUnsubscribed(String? topic) {
    print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::');
  }

// PING response received
  void pong() {
    print('ggumbi:' + Trace.current().frames[0].location + ' ' + (Trace.current().frames[0].member ?? '') + '::');
  }
}

///추후 파일 라운로드시 사용할것 주석 처리함
// class DownloadTestPage extends StatefulWidget with WidgetsBindingObserver {
//   final TargetPlatform platform;
//
//   DownloadTestPage({Key key, this.title, this.platform}) : super(key: key);
//
//   final String title;
//
//   @override
//   _DownloadTestPageState createState() => new _DownloadTestPageState();
// }
//
// class _DownloadTestPageState extends State<DownloadTestPage> {
//   final _documents = [
//     {'name': 'Learning Android Studio', 'link': 'http://barbra-coco.dyndns.org/student/learning_android_studio.pdf'},
//     {'name': 'Android Programming Cookbook', 'link': 'http://enos.itcollege.ee/~jpoial/allalaadimised/reading/Android-Programming-Cookbook.pdf'},
//     {'name': 'iOS Programming Guide', 'link': 'http://darwinlogic.com/uploads/education/iOS_Programming_Guide.pdf'},
//     {'name': 'Objective-C Programming (Pre-Course Workbook', 'link': 'https://www.bignerdranch.com/documents/objective-c-prereading-assignment.pdf'},
//   ];
//
//   final _images = [
//     {'name': 'Arches National Park', 'link': 'https://upload.wikimedia.org/wikipedia/commons/6/60/The_Organ_at_Arches_National_Park_Utah_Corrected.jpg'},
//     {'name': 'Canyonlands National Park', 'link': 'https://upload.wikimedia.org/wikipedia/commons/7/78/Canyonlands_National_Park%E2%80%A6Needles_area_%286294480744%29.jpg'},
//     {'name': 'Death Valley National Park', 'link': 'https://upload.wikimedia.org/wikipedia/commons/b/b2/Sand_Dunes_in_Death_Valley_National_Park.jpg'},
//     {'name': 'Gates of the Arctic National Park and Preserve', 'link': 'https://upload.wikimedia.org/wikipedia/commons/e/e4/GatesofArctic.jpg'}
//   ];
//
//   final _videos = [
//     {'name': 'Big Buck Bunny', 'link': 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4'},
//     {'name': 'Elephant Dream', 'link': 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4'}
//   ];
//
//   List<_TaskInfo> _tasks;
//   List<_ItemHolder> _items;
//   bool _isLoading;
//   bool _permissionReady;
//   String _localPath;
//   ReceivePort _port = ReceivePort();
//
//   @override
//   void initState() {
//     super.initState();
//
//     _bindBackgroundIsolate();
//
//     FlutterDownloader.registerCallback(downloadCallback);
//
//     _isLoading = true;
//     _permissionReady = false;
//
//     _prepare();
//   }
//
//   @override
//   void dispose() {
//     _unbindBackgroundIsolate();
//     super.dispose();
//   }
//
//   void _bindBackgroundIsolate() {
//     bool isSuccess = IsolateNameServer.registerPortWithName(_port.sendPort, 'downloader_send_port');
//     if (!isSuccess) {
//       _unbindBackgroundIsolate();
//       _bindBackgroundIsolate();
//       return;
//     }
//     _port.listen((dynamic data) {
//       if (debug) {
//         log.d('『GGUMBI』>>> _bindBackgroundIsolate : data: $data,  <<< ');
//       }
//       String id = data[0];
//       DownloadTaskStatus status = data[1];
//       int progress = data[2];
//
//       if (_tasks != null && _tasks.isNotEmpty) {
//         final task = _tasks.firstWhere((task) => task.taskId == id);
//         if (task != null) {
//           setState(() {
//             task.status = status;
//             task.progress = progress;
//           });
//         }
//       }
//     });
//   }
//
//   void _unbindBackgroundIsolate() {
//     IsolateNameServer.removePortNameMapping('downloader_send_port');
//   }
//
//   static void downloadCallback(String id, DownloadTaskStatus status, int progress) {
//     if (debug) {
//       log.d('『GGUMBI』>>> downloadCallback : id: $id, \nstatus: $status,\nprogress: $progress, <<< ');
//     }
//     final SendPort send = IsolateNameServer.lookupPortByName('downloader_send_port');
//     send.send([id, status, progress]);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//       appBar: new AppBar(
//         title: new Text(widget.title),
//       ),
//       body: Builder(
//           builder: (context) => _isLoading
//               ? new Center(
//                   child: new CircularProgressIndicator(),
//                 )
//               : _permissionReady
//                   ? _buildDownloadList()
//                   : _buildNoPermissionWarning()),
//     );
//   }
//
//   Widget _buildDownloadList() => Container(
//         child: ListView(
//           padding: const EdgeInsets.symmetric(vertical: 16.0),
//           children: _items
//               .map((item) => item.task == null
//                   ? _buildListSection(item.name)
//                   : DownloadItem(
//                       data: item,
//                       onItemClick: (task) {
//                         _openDownloadedFile(task).then((success) {
//                           if (!success) {
//                             Scaffold.of(context).showSnackBar(SnackBar(content: Text('Cannot open this file')));
//                           }
//                         });
//                       },
//                       onAtionClick: (task) {
//                         if (task.status == DownloadTaskStatus.undefined) {
//                           _requestDownload(task);
//                         } else if (task.status == DownloadTaskStatus.running) {
//                           _pauseDownload(task);
//                         } else if (task.status == DownloadTaskStatus.paused) {
//                           _resumeDownload(task);
//                         } else if (task.status == DownloadTaskStatus.complete) {
//                           _delete(task);
//                         } else if (task.status == DownloadTaskStatus.failed) {
//                           _retryDownload(task);
//                         }
//                       },
//                     ))
//               .toList(),
//         ),
//       );
//
//   Widget _buildListSection(String title) => Container(
//         padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
//         child: Text(
//           title,
//           style: TextStyle(fontWeight: FontWeight.bold, color: Colors.blue, fontSize: 18.0),
//         ),
//       );
//
//   Widget _buildNoPermissionWarning() => Container(
//         child: Center(
//           child: Column(
//             mainAxisSize: MainAxisSize.min,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               Padding(
//                 padding: const EdgeInsets.symmetric(horizontal: 24.0),
//                 child: Text(
//                   'Please grant accessing storage permission to continue -_-',
//                   textAlign: TextAlign.center,
//                   style: TextStyle(color: Colors.blueGrey, fontSize: 18.0),
//                 ),
//               ),
//               SizedBox(
//                 height: 32.0,
//               ),
//               FlatButton(
//                   onPressed: () {
//                     _checkPermission().then((hasGranted) {
//                       setState(() {
//                         _permissionReady = hasGranted;
//                       });
//                     });
//                   },
//                   child: Text(
//                     'Retry',
//                     style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 20.0),
//                   ))
//             ],
//           ),
//         ),
//       );
//
//   void _requestDownload(_TaskInfo task) async {
//     task.taskId = await FlutterDownloader.enqueue(url: task.link, headers: {"auth": "test_for_sql_encoding"}, savedDir: _localPath, showNotification: true, openFileFromNotification: true);
//     log.d('『GGUMBI』>>> _retryDownload : task: ${task.name}, \ntask: ${task.taskId}, \ntask: ${task.link}, <<< ');
//   }
//
//   void _cancelDownload(_TaskInfo task) async {
//     await FlutterDownloader.cancel(taskId: task.taskId);
//   }
//
//   void _pauseDownload(_TaskInfo task) async {
//     await FlutterDownloader.pause(taskId: task.taskId);
//   }
//
//   void _resumeDownload(_TaskInfo task) async {
//     String newTaskId = await FlutterDownloader.resume(taskId: task.taskId);
//     task.taskId = newTaskId;
//   }
//
//   void _retryDownload(_TaskInfo task) async {
//     log.d('『GGUMBI』>>> _retryDownload : task: ${task.name}, \ntask: ${task.taskId}, \ntask: ${task.link}, <<< ');
//     String newTaskId = await FlutterDownloader.retry(taskId: task.taskId);
//     task.taskId = newTaskId;
//   }
//
//   Future<bool> _openDownloadedFile(_TaskInfo task) {
//     return FlutterDownloader.open(taskId: task.taskId);
//   }
//
//   void _delete(_TaskInfo task) async {
//     await FlutterDownloader.remove(taskId: task.taskId, shouldDeleteContent: true);
//     await _prepare();
//     setState(() {});
//   }
//
//   Future<bool> _checkPermission() async {
//     if (widget.platform == TargetPlatform.android) {
//       final status = await Permission.storage.status;
//       if (status != PermissionStatus.granted) {
//         final result = await Permission.storage.request();
//         if (result == PermissionStatus.granted) {
//           return true;
//         }
//       } else {
//         return true;
//       }
//     } else {
//       return true;
//     }
//     return false;
//   }
//
//   Future<Null> _prepare() async {
//     final tasks = await FlutterDownloader.loadTasks();
//
//     int count = 0;
//     _tasks = [];
//     _items = [];
//
//     _tasks.addAll(_documents.map((document) => _TaskInfo(name: document['name'], link: document['link'])));
//
//     _items.add(_ItemHolder(name: 'Documents'));
//     for (int i = count; i < _tasks.length; i++) {
//       _items.add(_ItemHolder(name: _tasks[i].name, task: _tasks[i]));
//       count++;
//     }
//
//     _tasks.addAll(_images.map((image) => _TaskInfo(name: image['name'], link: image['link'])));
//
//     _items.add(_ItemHolder(name: 'Images'));
//     for (int i = count; i < _tasks.length; i++) {
//       _items.add(_ItemHolder(name: _tasks[i].name, task: _tasks[i]));
//       count++;
//     }
//
//     _tasks.addAll(_videos.map((video) => _TaskInfo(name: video['name'], link: video['link'])));
//
//     _items.add(_ItemHolder(name: 'Videos'));
//     for (int i = count; i < _tasks.length; i++) {
//       _items.add(_ItemHolder(name: _tasks[i].name, task: _tasks[i]));
//       count++;
//     }
//
//     tasks?.forEach((task) {
//       for (_TaskInfo info in _tasks) {
//         if (info.link == task.url) {
//           info.taskId = task.taskId;
//           info.status = task.status;
//           info.progress = task.progress;
//         }
//       }
//     });
//
//     _permissionReady = await _checkPermission();
//
//     _localPath = (await _findLocalPath()) + Platform.pathSeparator + 'Download';
//     log.d('『GGUMBI』>>> _prepare : _localPath: $_localPath,  <<< ');
//
//     final savedDir = Directory(_localPath);
//     bool hasExisted = await savedDir.exists();
//     if (!hasExisted) {
//       savedDir.create();
//     }
//
//     setState(() {
//       _isLoading = false;
//     });
//   }
//
//   Future<String> _findLocalPath() async {
//     final directory = widget.platform == TargetPlatform.android ? await getExternalStorageDirectory() : await getApplicationDocumentsDirectory();
//     return directory.path;
//   }
// }
//
// class DownloadItem extends StatelessWidget {
//   final _ItemHolder data;
//   final Function(_TaskInfo) onItemClick;
//   final Function(_TaskInfo) onAtionClick;
//
//   DownloadItem({this.data, this.onItemClick, this.onAtionClick});
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: const EdgeInsets.only(left: 16.0, right: 8.0),
//       child: lInkWell(
//         onTap: data.task.status == DownloadTaskStatus.complete
//             ? () {
//                 onItemClick(data.task);
//               }
//             : null,
//         child: Stack(
//           children: <Widget>[
//             Container(
//               width: double.infinity,
//               height: 64.0,
//               child: Row(
//                 crossAxisAlignment: CrossAxisAlignment.center,
//                 children: <Widget>[
//                   Expanded(
//                     child: Text(
//                       data.name,
//                       maxLines: 1,
//                       softWrap: true,
//                       overflow: TextOverflow.ellipsis,
//                     ),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(left: 8.0),
//                     child: _buildActionForTask(data.task),
//                   ),
//                 ],
//               ),
//             ),
//             data.task.status == DownloadTaskStatus.running || data.task.status == DownloadTaskStatus.paused
//                 ? Positioned(
//                     left: 0.0,
//                     right: 0.0,
//                     bottom: 0.0,
//                     child: LinearProgressIndicator(
//                       value: data.task.progress / 100,
//                     ),
//                   )
//                 : Container()
//           ].where((child) => child != null).toList(),
//         ),
//       ),
//     );
//   }
//
//   Widget _buildActionForTask(_TaskInfo task) {
//     if (task.status == DownloadTaskStatus.undefined) {
//       return RawMaterialButton(
//         onPressed: () {
//           onAtionClick(task);
//         },
//         child: Icon(Icons.file_download),
//         shape: CircleBorder(),
//         constraints: BoxConstraints(minHeight: 32.0, minWidth: 32.0),
//       );
//     } else if (task.status == DownloadTaskStatus.running) {
//       return RawMaterialButton(
//         onPressed: () {
//           onAtionClick(task);
//         },
//         child: Icon(
//           Icons.pause,
//           color: Colors.red,
//         ),
//         shape: CircleBorder(),
//         constraints: BoxConstraints(minHeight: 32.0, minWidth: 32.0),
//       );
//     } else if (task.status == DownloadTaskStatus.paused) {
//       return RawMaterialButton(
//         onPressed: () {
//           onAtionClick(task);
//         },
//         child: Icon(
//           Icons.play_arrow,
//           color: Colors.green,
//         ),
//         shape: CircleBorder(),
//         constraints: BoxConstraints(minHeight: 32.0, minWidth: 32.0),
//       );
//     } else if (task.status == DownloadTaskStatus.complete) {
//       return Row(
//         mainAxisSize: MainAxisSize.min,
//         mainAxisAlignment: MainAxisAlignment.end,
//         children: [
//           Text(
//             'Ready',
//             style: TextStyle(color: Colors.green),
//           ),
//           RawMaterialButton(
//             onPressed: () {
//               onAtionClick(task);
//             },
//             child: Icon(
//               Icons.delete_forever,
//               color: Colors.red,
//             ),
//             shape: CircleBorder(),
//             constraints: BoxConstraints(minHeight: 32.0, minWidth: 32.0),
//           )
//         ],
//       );
//     } else if (task.status == DownloadTaskStatus.canceled) {
//       return Text('Canceled', style: TextStyle(color: Colors.red));
//     } else if (task.status == DownloadTaskStatus.failed) {
//       return Row(
//         mainAxisSize: MainAxisSize.min,
//         mainAxisAlignment: MainAxisAlignment.end,
//         children: [
//           Text('Failed', style: TextStyle(color: Colors.red)),
//           RawMaterialButton(
//             onPressed: () {
//               onAtionClick(task);
//             },
//             child: Icon(
//               Icons.refresh,
//               color: Colors.green,
//             ),
//             shape: CircleBorder(),
//             constraints: BoxConstraints(minHeight: 32.0, minWidth: 32.0),
//           )
//         ],
//       );
//     } else if (task.status == DownloadTaskStatus.enqueued) {
//       return Text('Pending', style: TextStyle(color: Colors.orange));
//     } else {
//       return null;
//     }
//   }
// }
//
// class _TaskInfo {
//   final String name;
//   final String link;
//
//   String taskId;
//   int progress = 0;
//   DownloadTaskStatus status = DownloadTaskStatus.undefined;
//
//   _TaskInfo({this.name, this.link});
// }
//
// class _ItemHolder {
//   final String name;
//   final _TaskInfo task;
//
//   _ItemHolder({this.name, this.task});
// }

class Gender {
  String name;
  IconData icon;
  bool isSelected;

  Gender(this.name, this.icon, this.isSelected);
}

class GenderSelector extends StatefulWidget {
  @override
  _GenderSelectorState createState() => _GenderSelectorState();
}

class _GenderSelectorState extends State<GenderSelector> {
  List<Gender> genders = [];

  @override
  void initState() {
    super.initState();
    genders.add(new Gender("Male", Icons.account_circle_rounded, false));
    genders.add(new Gender("Female", Icons.account_box, false));
    genders.add(new Gender("Others", Icons.notifications, false));
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        shrinkWrap: false,
        itemCount: genders.length,
        itemBuilder: (context, index) {
          return Material(
            child: lInkWell(
                splashColor: Colors.pinkAccent,
                onTap: () {
                  setState(() {
                    genders.forEach((gender) => gender.isSelected = false);
                    genders[index].isSelected = true;
                  });
                },
                child: CustomRadio(genders[index])),
          );
        });
  }
}

class CustomRadio extends StatelessWidget {
  Gender _gender;

  CustomRadio(this._gender);

  @override
  Widget build(BuildContext context) {
    return Card(
        color: _gender.isSelected ? Color(0xFF3B4257) : color_white,
        child: Container(
          height: 80,
          width: 80,
          alignment: Alignment.center,
          margin: EdgeInsets.all(5.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                _gender.icon,
                color: _gender.isSelected ? color_white : Colors.grey,
                size: 40,
              ),
              sb_h_10,
              Text(
                _gender.name,
                style: TextStyle(color: _gender.isSelected ? color_white : Colors.grey),
              )
            ],
          ),
        ));
  }
}

// class ScreenUtilPage extends StatefulWidget {
//   const ScreenUtilPage();
//
//   @override
//   _ScreenUtilPageState createState() => _ScreenUtilPageState();
// }
//
// class _ScreenUtilPageState extends State<ScreenUtilPage> {
//   @override
//   Widget build(BuildContext context) {
//     //Set the fit size (fill in the screen size of the device in the design) If the design is based on the size of the iPhone6 ​​(iPhone6 ​​750*1334)
//
//     printScreenInformation();
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("ScreenUtil"),
//       ),
//       body: SingleChildScrollView(
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: <Widget>[
//             Row(
//               children: <Widget>[
//                 // Using Extensions
//                 Container(
//                   padding: EdgeInsets.all(10.w),
//                   width: 0.5.sw,
//                   height: 200.h,
//                   color: Colors.red,
//                   child: Text(
//                     'My actual width: ${0.5.sw}dp \n\n'
//                         'My actual height: ${200.h}dp',
//                     style: TextStyle(
//                       color: color_white,
//                       fontSize: 12.sp,
//                     ),
//                   ),
//                 ),
//                 // Without using Extensions
//                 Container(
//                   padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
//                   width: ScreenUtil().setWidth(180),
//                   height: ScreenUtil().setHeight(200),
//                   color: Colors.blue,
//                   child: Text(
//                     'My design draft width: 180dp\n\n'
//                         'My design draft height: 200dp',
//                     style: TextStyle(
//                       color: color_white,
//                       fontSize: ScreenUtil().setSp(12),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//             Container(
//               padding: EdgeInsets.all(ScreenUtil().setWidth(10)),
//               width: 100.r,
//               height: 100.r,
//               color: Colors.green,
//               child: Text('I am a square with a side length of 100',
//                 style: TextStyle(
//                   color: color_white,
//                   fontSize: ScreenUtil().setSp(12),
//                 ),
//               ),
//             ),
//             Text('Device width:${ScreenUtil().screenWidth}dp'),
//             Text('Device height:${ScreenUtil().screenHeight}dp'),
//             Text('Device pixel density:${ScreenUtil().pixelRatio}'),
//             Text('Bottom safe zone distance:${ScreenUtil().bottomBarHeight}dp'),
//             Text('Status bar height:${ScreenUtil().statusBarHeight}dp'),
//             Text(
//               'The ratio of actual width to UI design:${ScreenUtil().scaleWidth}',
//               textAlign: TextAlign.center,
//             ),
//             Text(
//               'The ratio of actual height to UI design:${ScreenUtil().scaleHeight}',
//               textAlign: TextAlign.center,
//             ),
//             SizedBox(
//               height: 50.h,
//             ),
//             Text('System font scaling factor:${ScreenUtil().textScaleFactor}'),
//             Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: <Widget>[
//                 Container(
//                   height: 32.h,
//                   child: Text(
//                     'My font size is 16sp on the design draft and will not change with the system.',
//                     style: TextStyle(
//                       color: color_black,
//                       fontSize: 16.sp,
//                     ),
//                   ),
//                 ),
//                 Text(
//                   'My font size is 16sp on the design draft and will change with the system.',
//                   style: TextStyle(
//                     color: color_black,
//                     fontSize: 16.ssp,
//                   ),
//                 ),
//               ],
//             )
//           ],
//         ),
//       ),
//     );
//   }
//
//   void printScreenInformation() {
//     print('Device width dp:${1.sw}dp');
//     print('Device height dp:${1.sh}dp');
//     print('Device pixel density:${ScreenUtil().pixelRatio}');
//     print('Bottom safe zone distance dp:${ScreenUtil().bottomBarHeight}dp');
//     print('Status bar height dp:${ScreenUtil().statusBarHeight}dp');
//     print('The ratio of actual width to UI design:${ScreenUtil().scaleWidth}');
//     print('The ratio of actual height to UI design:${ScreenUtil().scaleHeight}');
//     print('System font scaling:${ScreenUtil().textScaleFactor}');
//     print('0.5 times the screen width:${0.5.sw}dp');
//     print('0.5 times the screen height:${0.5.sh}dp');
//   }
// }
