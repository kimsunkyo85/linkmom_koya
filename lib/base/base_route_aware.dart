import 'package:flutter/material.dart';
import 'package:linkmom/main.dart';

class BaseRouteAware extends RouteObserver<ModalRoute<dynamic>> {
  List<String> _routes = [];
  List<String> get routes => this._routes;

  @override
  void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {
    super.didPush(route, previousRoute);
    if (route is ModalRoute) {
      _routes.add(route.settings.name ?? '');
    }
  }

  @override
  void didReplace({Route<dynamic>? newRoute, Route<dynamic>? oldRoute}) {
    super.didReplace(newRoute: newRoute, oldRoute: oldRoute);
    if (newRoute is ModalRoute) {
      _routes.remove(oldRoute!.settings.name ?? '');
      _routes.add(newRoute.settings.name ?? '');
    }
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) {
    super.didPop(route, previousRoute);
    if (previousRoute is ModalRoute && route is ModalRoute) {
      _routes.remove(route.settings.name ?? '');
    }
  }

  @override
  void subscribe(RouteAware routeAware, ModalRoute route) {
    super.subscribe(routeAware, route);
    log.d('routes: $_routes');
  }

  @override
  void unsubscribe(RouteAware routeAware) {
    super.unsubscribe(routeAware);
    log.d('routes: $_routes');
  }

  void popRoutes(Route? route) {
    if (route != null) {
      _routes.remove(route.settings.name);
    } else {
      _routes.removeAt(0);
    }
  }

  void clearWithoutMain() {
    String first = _routes.first;
    clearAll();
    _routes.add(first);
  }

  void clearAll() {
    _routes.clear();
  }
}
