// Copyright 2021 linkmom
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'package:linkmom/data/network/models/req_data.dart';
import 'package:linkmom/utils/string_util.dart';

class CareSchedule extends ReqScheduleData {
  DateTime sTime = StringUtils.convertNow(hour: 8);
  DateTime eTime = StringUtils.convertNow(hour: 8, min: 30);
  CareSchedule({
    required List<String> bookingdate,
    int id = 0,
    String stime = '',
    String etime = '',
    int isAm = 0,
    int isPm = 0,
    int isRepeat = 0,
    int durationtime = 0,
    List<String>? removedate,
  }) : super(
          bookingdate: bookingdate,
          id: id,
          stime: stime,
          etime: etime,
          is_am: isAm,
          is_pm: isPm,
          is_repeat: isRepeat,
          durationtime: durationtime,
        );

  ReqScheduleData get getSchedule => this;
  DateTime get getStime => this.sTime;
  DateTime get getEtime => this.eTime;
  List<DateTime> _times = [];

  void addSchedule(List<DateTime> schedules) {
    schedules.sort();
    schedules.forEach((date) {
      this.bookingdate.add(StringUtils.formatYMD(date));
    });
    this._times.addAll(schedules);
    this._times.sort();
  }

  void clearSchedule() {
    this.bookingdate.clear();
    this._times.clear();
  }

  void setTime(DateTime start, DateTime end) {
    this.stime = StringUtils.formatHD(start);
    this.sTime = StringUtils.convertNow(hour: start.hour, min: start.minute);
    this.etime = StringUtils.formatHD(end);
    this.eTime = StringUtils.convertNow(hour: end.hour, min: end.minute);
  }

  void addDate(DateTime date) {
    this.bookingdate.add(StringUtils.formatYMD(date));
    this.bookingdate.sort();
  }

  void removeDate(DateTime date) {
    this.bookingdate.remove(StringUtils.formatYMD(date));
    this.bookingdate.sort();
  }

  void setSchedule(ReqScheduleData schedule) {
    schedule.bookingdate.sort();
    this.bookingdate = schedule.bookingdate;
    this.id = schedule.id;
    DateTime start = StringUtils.parseHD(schedule.stime);
    this.stime = schedule.stime;
    this.sTime = StringUtils.convertNow(hour: start.hour, min: start.minute);
    DateTime end = StringUtils.parseHD(schedule.etime);
    this.etime = schedule.etime;
    this.eTime = StringUtils.convertNow(hour: end.hour, min: end.minute);
    this.is_am = schedule.is_am;
    this.is_pm = schedule.is_pm;
    this.is_repeat = schedule.is_repeat;
    this.durationtime = schedule.durationtime;
    if (schedule.removedate != null) schedule.removedate!.sort();
    this.removedate = schedule.removedate;
    this._times = this.bookingdate.map((e) => StringUtils.parseYMD(e)).toList();
    this._times.sort();
  }

  Map<DateTime, List<int>> asMap() {
    Map<DateTime, List<int>> map = {};
    this.bookingdate.sort();
    this.bookingdate.forEach((date) {
      map.putIfAbsent(StringUtils.parseYMD(date), () => [0]);
    });
    return map;
  }

  List<DateTime> get schedules => this._times;
}
