import 'package:flutter/material.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

import 'calendar_range_view.dart';

class DateRangeSelecter extends StatefulWidget {
  final Function(DateTime date, List<int> events) selectCallback;
  final Function? onCalendarChanged;
  final Duration? duration;
  List<DateTime>? event;
  Selecter mode;
  final List<Color> color;
  final int initDate;
  DateTime? endDate;
  DateTime? startDate;
  DateRangeSelecter({
    required this.selectCallback,
    this.duration,
    this.event,
    required this.mode,
    this.onCalendarChanged,
    this.color = colorList,
    this.initDate = 0,
    this.endDate,
    this.startDate,
  });
  _DateRangeSelecterState createState() => _DateRangeSelecterState();
}

class _DateRangeSelecterState extends State<DateRangeSelecter> {

  @override
  void initState() {
    super.initState();
    if (widget.duration == null) widget.startDate = DateTime.now().add(Duration(days: -365));
  }

  @override
  Widget build(BuildContext context) {
    return lScrollView(
      padding: padding_0,
      child: CalendarRangeView(
        duration: widget.duration,
        selected: (date, events) {
          widget.selectCallback(date, events);
        },
        type: storageHelper.user_type,
        startDate: widget.startDate,
        events: {},
      ),
    );
  }
}
