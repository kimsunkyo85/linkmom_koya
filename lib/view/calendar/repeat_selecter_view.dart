import 'package:flutter/material.dart';
import 'package:linkmom/data/holidays/holidays_helper.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/calendar/calendar_view.dart';

import 'schedule_controller.dart';

class RepeatSelecter extends CalendarView {
  final bool Function(DateTime date, List<int> events) selectCallback;
  final Function? onCalendarChanged;
  final Duration? duration;
  final Map<DateTime, List<int>>? event;
  final List<Color>? colors;
  final Selecter mode;
  final Function? onEventUpdate, onAlertAction;
  final int end;
  final Function(ScheduleController)? onCreateCalendar;

  RepeatSelecter({
    this.onEventUpdate,
    required this.selectCallback,
    this.duration,
    this.event,
    this.colors,
    this.mode = Selecter.view,
    this.onCalendarChanged,
    this.end = 30,
    this.onAlertAction,
    this.onCreateCalendar,
  }) : super(
          selected: selectCallback,
          type: USER_TYPE.mom_daddy,
          height: 40,
          eventLimit: 1,
          colors: colors ?? [color_main, color_main],
          events: event ?? {},
          duration: duration,
          onCalendarChanged: onCalendarChanged,
          holidays: HolidaysHelper.getHolidays,
          selectable: true,
          onCreateCalendar: onCreateCalendar,
        );
  _DateSelecterState createState() => _DateSelecterState(this.mode, this.onEventUpdate!, this.end, this.onAlertAction!);
}

class _DateSelecterState extends CalendarViewState<RepeatSelecter> {
  Selecter mode;
  late Function onEventUpdate, onAlertAction;
  int end = 0;

  _DateSelecterState(this.mode, this.onEventUpdate, this.end, this.onAlertAction);

  @override
  void initState() {
    super.initState();
  }

  @override
  void updateUnit(List<DateTime> dates, {int id = -1}) {
    super.updateUnit(dates, id: 0);
  }

  @override
  void updateEvent(DateTime date, List<int> events) {
    setState(() {
      DateTime last = widget.events.keys.last;
      List<DateTime> list = [];
      if (last.isBefore(date)) {
        // NOTE: get last weekdays
        List<DateTime> lastRepeat = widget.events.keys.where((e) => e.isAfter(last.add(Duration(days: -6)))).toList();
        int count = (last.difference(date).inDays / 7).ceil().abs();
        lastRepeat.forEach((day) {
          List<DateTime> gen = List.generate((count).round(), (counter) => day.add(Duration(days: (counter + 1) * 7)));
          list.addAll(gen);
        });
        list.sort();
        super.calendarCtrl.eventer.clear();
        list.forEach((e) {
          super.calendarCtrl.eventer.append(e, event: 0);
        });
        if (list.isNotEmpty) last = list.last;
      } else {
        widget.events.removeWhere((day, event) => day.isAfter(date));
        super.calendarCtrl.eventer.getSchedule.removeWhere((day, event) => day.isAfter(date));
        last = widget.events.keys.last;
      }
      log.d('|| updated 1$list');
      log.d('|| updated 2${widget.events.keys.toList()}');
      log.d('|| updated 3${super.calendarCtrl.eventer.getSchedule.keys.toList()}');
      list.addAll(widget.events.keys.toList());
      if (widget.events.keys.toList().length > end) {
        date = last;
        onAlertAction(DialogAction.delete);
      }
      onEventUpdate(list);
      widget.selected(last, events);
    });
  }

  @override
  Widget build(BuildContext context) {
    return super.build(context);
  }

  @override
  Widget selectBuilder(BuildContext context, DateTime date, List<int> events, Color color, {bool isToday = false, int id = -1}) {
    if (events.isNotEmpty) {
      if (selectDate[date] != null) {
        id = selectDate[date]!.last;
      }
      return Container(
          height: widget.height,
          alignment: Alignment.topCenter,
          color: Commons.getColor().withAlpha(52),
          padding: padding_01_T,
          child: Column(
            children: [
              Container(
                  width: 22,
                  height: 22,
                  alignment: Alignment.center,
                  decoration: isToday ? BoxDecoration(shape: BoxShape.circle, border: Border.all(color: color)) : null,
                  child: lText('${date.day}', style: st_13(textColor: color), textAlign: TextAlign.center)),
            ],
          ));
    } else {
      return Container(
          height: widget.height,
          alignment: Alignment.topCenter,
          color: Commons.getColor().withAlpha(52),
          padding: padding_01_T,
          child: Column(
            children: [
              Container(
                  width: 22,
                  height: 22,
                  alignment: Alignment.center,
                  decoration: isToday ? BoxDecoration(shape: BoxShape.circle, border: Border.all(color: color)) : null,
                  child: lText('${date.day}', style: st_13(textColor: color), textAlign: TextAlign.center)),
              if (this.mode == Selecter.repeat) Row(mainAxisAlignment: MainAxisAlignment.center, children: events.map((e) => buildUnit(date, id: e)).toList()),
            ],
          ));
    }
  }
}
