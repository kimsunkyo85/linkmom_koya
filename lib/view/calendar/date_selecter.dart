import 'package:flutter/material.dart';
import 'package:linkmom/data/holidays/holidays_helper.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/calendar/calendar_view.dart';

class DateSelecter extends CalendarView {
  final bool Function(DateTime date, List<int> events) selectCallback;
  final Function? onCalendarChanged;
  final Duration? duration;
  final List<DateTime>? event;
  final List<Color>? colors;
  final Selecter mode;
  final Function? onEventUpdate, onAlertAction;
  final int end;

  DateSelecter({
    this.onEventUpdate,
    required this.selectCallback,
    this.duration,
    this.event,
    this.colors,
    this.mode = Selecter.view,
    this.onCalendarChanged,
    this.end = 30,
    this.onAlertAction,
  }) : super(
            selected: selectCallback,
            type: USER_TYPE.mom_daddy,
            height: 40,
            eventLimit: 1,
            colors: colors ?? [color_main, color_main],
            events: {},
            duration: duration,
            onCalendarChanged: onCalendarChanged,
            holidays: HolidaysHelper.getHolidays,
            selectable: true) {
    if (this.event != null) {
      this.event!.forEach((e) {
        super.events.putIfAbsent(e, () => [0]);
      });
    }
  }
  _DateSelecterState createState() => _DateSelecterState(this.mode, this.onEventUpdate!, this.end, this.onAlertAction!);
}

class _DateSelecterState extends CalendarViewState<DateSelecter> {
  Selecter mode;
  late Function onEventUpdate, onAlertAction;
  int end = 0;

  _DateSelecterState(this.mode, this.onEventUpdate, this.end, this.onAlertAction);

  @override
  void initState() {
    super.initState();
  }

  @override
  void updateUnit(List<DateTime> dates, {int id = -1}) {
    super.updateUnit(dates, id: 0);
  }

  @override
  void updateEvent(DateTime date, List<int> events) {
    setState(() {
      if (this.mode == Selecter.view) {
        // drug dateSelecter
        if (widget.startDate == null) {
          // widget.startDate = date;
          // widget.duration = Duration(days: 4);
        }
        if (events.contains(0)) {
          events.remove(0);
        } else {
          events.add(0);
        }
        super.updateEvent(date, events);
      } else {
        // repeat
        Map<DateTime, List<int>> revoke = {};
        revoke.addAll(widget.events);
        super.selectDate.clear();
        if (widget.events.containsKey(date)) {
          widget.events.removeWhere((element, events) => element.isAfter(date));
          super.updateEvent(date, events);
        } else {
          // update repeat schedule.
          DateTime last = widget.events.keys.toList().last;
          List<DateTime> list = [];
          if (widget.events.keys.last.isBefore(date)) {
            // NOTE: get last weekdays
            List<DateTime> lastRepeat = widget.events.keys.where((e) => e.isAfter(last.add(Duration(days: -6)))).toList();
            int count = (last.difference(date).inDays / 7).ceil().abs();
            lastRepeat.forEach((day) {
              List<DateTime> gen = List.generate((count).round(), (counter) => day.add(Duration(days: (counter + 1) * 7)));
              list.addAll(gen);
            });
            list.sort();
            list.forEach((e) {
              widget.events.update(e, (value) => [0], ifAbsent: () => [0]);
            });
          } else {
            widget.events.removeWhere((d, e) => d.isAfter(date));
          }
        }
        if (widget.events.keys.toList().length > end) {
          // widget.events = revoke;
          date = widget.events.keys.last;
          onAlertAction(DialogAction.delete);
        }
        onEventUpdate(widget.events.keys.toList());
        // super.updateEvents(widget.events);
        events.add(0);
      }
      widget.selected(date, events);
    });
  }

  @override
  Widget build(BuildContext context) {
    return super.build(context);
  }

  @override
  Widget selectBuilder(BuildContext context, DateTime date, List<int> events, Color color, {bool isToday = false, int id = -1}) {
    if (events.isNotEmpty) {
      int id = 0;
      if (selectDate[date] != null) {
        id = selectDate[date]!.last;
      }
      return Container(
          height: widget.height,
          alignment: Alignment.topCenter,
          color: Commons.getColor().withAlpha(52),
          padding: padding_01_T,
          child: Column(
            children: [
              Container(
                  width: 22,
                  height: 22,
                  alignment: Alignment.center,
                  decoration: isToday ? BoxDecoration(shape: BoxShape.circle, border: Border.all(color: color)) : null,
                  child: lText('${date.day}', style: st_13(textColor: color), textAlign: TextAlign.center)),
            ],
          ));
    } else {
      return Container(
          height: widget.height,
          alignment: Alignment.topCenter,
          color: Commons.getColor().withAlpha(52),
          padding: padding_01_T,
          child: Column(
            children: [
              Container(
                  width: 22,
                  height: 22,
                  alignment: Alignment.center,
                  decoration: isToday ? BoxDecoration(shape: BoxShape.circle, border: Border.all(color: color)) : null,
                  child: lText('${date.day}', style: st_13(textColor: color), textAlign: TextAlign.center)),
              if (this.mode == Selecter.repeat) Row(mainAxisAlignment: MainAxisAlignment.center, children: events.map((e) => buildUnit(date, id: e)).toList()),
            ],
          ));
    }
  }
}
