class ScheduleController {
  Scheduler eventer = Scheduler();
  Scheduler selecter = Scheduler();

  void saveEvent() {
    this.selecter.getSchedule.forEach((date, event) {
      event = event.toSet().toList();
      if (event.isNotEmpty) this.eventer.append(date, events: event);
    });
    this.selecter.clear();
  }
}

class Scheduler {
  int _index = 0;
  Map<DateTime, List<int>> _schedule = {};

  bool get isNotEmpty => this._schedule.values.where((element) => element.isNotEmpty).isNotEmpty;
  bool get isEmpty => this._schedule.values.where((element) => element.isNotEmpty).isEmpty;

  bool hasDay(DateTime date) => this._schedule[date] != null && this._schedule[date]!.isNotEmpty;

  set setIndex(index) => this._index = index;
  int get getIndex => this._index;

  int get length => this._schedule.length;

  /// append
  void append(DateTime date, {int? event, List<int>? events}) {
    if (this._schedule[date] == null) this._schedule[date] = [];
    if (events == null) {
      this._schedule[date]!.add(event ?? 0);
    } else {
      this._schedule[date]!.addAll(events);
    }
    this._schedule[date] = this._schedule[date]!.toSet().toList();
    if (this._schedule[date] != null) this._schedule[date]!.sort();
  }

  /// remove
  void remove(DateTime date, {int? index}) {
    if (this._schedule[date] != null) {
      if (index == null) {
        this._schedule.remove(date);
      } else {
        if (this._schedule[date]!.length < 2) {
          this._schedule.remove(date);
        } else {
          this._schedule[date]!.remove(index);
        }
      }
    }
    if (this._schedule[date] != null) this._schedule[date]!.sort();
  }

  /// clear
  void clear({int? id}) {
    if (id != null) {
      this._schedule.forEach((key, value) {
        if (value.contains(id)) value.remove(id);
      });
    } else {
      this._schedule.clear();
    }
  }

  /// get
  Map<DateTime, List<int>> get getSchedule => this._schedule;

  /// import
  void imports(Map<DateTime, List<int>> events) {
    _schedule.addAll(events);
  }

  void toggle(DateTime date, List<int> events) {
    if (this._schedule[date] != null && this._schedule[date]!.isNotEmpty) {
      if (events.isEmpty) {
        this._schedule.remove(date);
      } else {
        this._schedule[date] = events;
      }
      if (this._schedule[date] == null) this._schedule[date] = [];
      this._schedule[date] = this._schedule[date]!.toSet().toList();
      this._schedule[date]!.sort();
    } else {
      this.append(date, events: events);
    }
  }

  List<DateTime> getDates(int id) {
    List<DateTime> list = [];
    this._schedule.forEach((date, events) {
      if (events.contains(id)) list.add(date);
    });
    return list;
  }

  bool containId(int id) {
    return this._schedule.values.where((list) => list.contains(id)).isNotEmpty;
  }
}
