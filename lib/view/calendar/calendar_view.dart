// Copyright 2021 linkmom
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:jiffy/jiffy.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:table_calendar/table_calendar.dart';

import 'schedule_controller.dart';

class CalendarView extends StatefulWidget {
  final USER_TYPE? type;
  final Map<DateTime, List<int>> events;
  final Map<DateTime, List<int>> holidays;
  final bool Function(DateTime, List<int>) selected;
  final String unavailableMsg;
  final Duration? duration;
  final int eventLimit;
  final List<Color>? colors;
  final double height;
  final DateTime? startDate, endDate;
  final Function? onCalendarChanged;
  final bool selectable;
  final Function(ScheduleController)? onCreateCalendar;

  CalendarView({
    required this.events,
    required this.selected,
    required this.holidays,
    this.type,
    this.unavailableMsg = '',
    this.duration,
    this.eventLimit = 0,
    this.colors,
    this.height = 70,
    this.startDate,
    this.onCalendarChanged,
    this.selectable = false,
    this.endDate,
    this.onCreateCalendar,
  });

  @override
  CalendarViewState createState() => CalendarViewState();
}

class CalendarViewState<T extends CalendarView> extends State<T> with TickerProviderStateMixin {
  Map<DateTime, List<int>> _newEvents = {};
  Map<DateTime, List<int>> selectDate = {};
  late PageController _calendarPageCtrl;
  DateTime _focused = DateTime.now();
  ScheduleController calendarCtrl = ScheduleController();

  @override
  void initState() {
    super.initState();
    _calendarPageCtrl = PageController();
    if (widget.onCreateCalendar != null) widget.onCreateCalendar!(calendarCtrl);
  }

  @override
  Widget build(BuildContext context) {
    initializeDateFormatting(LOCALE, null);
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Container(
          padding: padding_10_LR,
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Row(mainAxisAlignment: MainAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: [
              lText(DateFormat().addPattern('yyyy.MM').format(_focused), style: stRegisterCompleteSelect),
              TextButton(
                onPressed: () => _moveToToday(),
                child: Container(padding: EdgeInsets.only(left: 9, right: 9, top: 6, bottom: 6), decoration: BoxDecoration(border: Border.all(color: color_222222), borderRadius: BorderRadius.circular(16)), child: lText("오늘".tr(), style: st_b_10())),
              ),
            ]),
            Row(mainAxisAlignment: MainAxisAlignment.end, mainAxisSize: MainAxisSize.min, children: [
              lInkWell(child: Lcons.nav_left(color: color_222222), onTap: () => _previousPage()),
              sb_w_10,
              lInkWell(child: Lcons.nav_right(color: color_222222), onTap: () => _nextPage()),
            ])
          ])),
      sb_h_15,
      TableCalendar(
        rowHeight: widget.height,
        locale: 'ko_KR',
        weekendDays: [DateTime.sunday, DateTime.saturday],
        focusedDay: _focused,
        firstDay: widget.startDate ?? DateTime.now().add(Duration(days: -365)),
        lastDay: widget.endDate ?? (widget.startDate == null ? DateTime.now().add(widget.duration ?? Duration(days: 365)) : widget.startDate!.add(widget.duration ?? Duration(days: 365 * 1))),
        calendarFormat: CalendarFormat.month,
        eventLoader: (date) {
          List<int> loader = widget.events[date] ?? this.calendarCtrl.eventer.getSchedule[date] ?? [];
          if (loader.isEmpty) {
            DateTime time = Jiffy(date).startOf(Units.DAY).dateTime;
            loader = widget.events[time] ?? this.calendarCtrl.eventer.getSchedule[time] ?? [];
          }
          return loader;
        },
        startingDayOfWeek: StartingDayOfWeek.sunday,
        availableGestures: AvailableGestures.horizontalSwipe,
        daysOfWeekStyle: DaysOfWeekStyle(weekendStyle: st_13(textColor: color_ff3b30), weekdayStyle: st_b_13()),
        headerStyle: HeaderStyle(formatButtonVisible: false),
        headerVisible: false,
        calendarBuilders: CalendarBuilders(
          defaultBuilder: (ctx, date, focused) => _dateBuilder(ctx, date, calendarCtrl.eventer.getSchedule[date] ?? [], color_222222),
          todayBuilder: (ctx, date, focused) => _todayBuilder(ctx, date, calendarCtrl.eventer.getSchedule[date] ?? []),
          selectedBuilder: (ctx, date, focused) {
            DateTime now = Jiffy(DateTime.now().toLocal()).startOf(Units.DAY).dateTime;
            if (calendarCtrl.eventer.getSchedule[date] == null) {
              if (calendarCtrl.selecter.getSchedule[date] == null) calendarCtrl.selecter.getSchedule[date] = [];
              return selectBuilder(
                ctx,
                date,
                calendarCtrl.selecter.getSchedule[date] ?? [],
                date.weekday > DateTime.friday ? color_ff3b30 : color_222222,
                isToday: Jiffy(date.toLocal()).startOf(Units.DAY).dateTime == now,
              );
            } else {
              return selectBuilder(
                ctx,
                date,
                calendarCtrl.eventer.getSchedule[date] ?? [],
                date.weekday > DateTime.friday ? color_ff3b30 : color_222222,
                isToday: Jiffy(date.toLocal()).startOf(Units.DAY).dateTime == now,
              );
            }
          },
          outsideBuilder: (ctx, date, focused) => _dateBuilder(
            ctx,
            date,
            calendarCtrl.eventer.getSchedule[date] ?? [],
            color_dedede,
          ),
          holidayBuilder: (ctx, date, focused) => _dateBuilder(
            ctx,
            date,
            calendarCtrl.eventer.getSchedule[date] ?? [],
            focused.month != date.month ? color_dedede : color_ff3b30,
          ),
          disabledBuilder: (ctx, date, focused) => _dateBuilder(ctx, date, calendarCtrl.eventer.getSchedule[date] ?? [], color_dedede),
          markerBuilder: (ctx, date, events) => _markerBuilder(events.map((e) => int.parse(e.toString())).toList(), date),
        ),
        holidayPredicate: (day) {
          if (DateTime.sunday == day.weekday || DateTime.saturday == day.weekday) {
            return true;
          } else if (widget.holidays.isNotEmpty) {
            return widget.holidays.containsKey(day.toLocal().add(day.toLocal().timeZoneOffset * -1));
          } else {
            return false;
          }
        },
        selectedDayPredicate: (day) => calendarCtrl.selecter.hasDay(day),
        onDaySelected: (selected, focused) {
          if (widget.selectable) updateEvent(selected, calendarCtrl.selecter.getSchedule[selected] ?? []);
        },
        onPageChanged: (focused) {
          setState(() => _focused = focused); // title update
          if (widget.onCalendarChanged != null) widget.onCalendarChanged!.call(focused);
        },
        onCalendarCreated: (ctrl) => _calendarPageCtrl = ctrl,
      )
    ]);
  }

  void removeUnit(int id) {
    if (widget.type == USER_TYPE.mom_daddy) id = 0;
    _newEvents.forEach((date, value) {
      value.remove(id);
    });
    setState(() => {});
  }

  void updateUnit(List<DateTime> dates, {int id = -1}) {
    setState(() {
      _newEvents.forEach((date, value) {
        if (!dates.contains(date)) _newEvents[date]!.remove(id);
      });
      dates.forEach((date) {
        DateTime time = Jiffy(date.toLocal()).startOf(Units.DAY).dateTime;
        _newEvents.update(time, (value) {
          value.add(id);
          return value.toSet().toList();
        }, ifAbsent: () => [id]);
      });
      calendarCtrl.selecter.getSchedule.clear();
    });
  }

  List<DateTime> changeUnit(List<DateTime> oldList, int id) {
    List<DateTime> dates = [];
    calendarCtrl.selecter.getSchedule.forEach((date, value) {
      date = Jiffy(date.toLocal()).startOf(Units.DAY).dateTime;
      if (_newEvents.containsKey(date) && _newEvents[date]!.contains(id)) {
        // select O, recent O -> remove
        _newEvents[date]!.remove(id);
      } else {
        // select O, recent X -> Add
        _newEvents.update(date, (value) {
          value.add(id);
          return value;
        }, ifAbsent: () => [id]);
      }
    });
    _newEvents.forEach((date, value) {
      if (value.contains(id)) {
        dates.add(date);
      }
    });
    calendarCtrl.selecter.getSchedule.clear();
    setState(() => {});
    return dates;
  }

  void updateEvent(DateTime date, List<int> events) {
    DateTime local = Jiffy(date).startOf(Units.DAY).dateTime;
    setState(() {
      bool ret = widget.selected(date, events);
      if (!ret) return;
      if (widget.events[date] != null) {
        events.addAll(widget.events[date] ?? []);
        events = events.toSet().toList();
      }
      if (widget.type == USER_TYPE.mom_daddy) {
        if (calendarCtrl.eventer.getSchedule[date] == null && calendarCtrl.eventer.getSchedule[local] == null) {
          calendarCtrl.selecter.toggle(date, events);
        } else {
          calendarCtrl.eventer.remove(date);
          calendarCtrl.eventer.remove(local);
        }
      } else {
        if (calendarCtrl.eventer.getSchedule[date] == null && calendarCtrl.eventer.getSchedule[local] == null) {
          calendarCtrl.selecter.toggle(date, events);
        } else {
          if (!calendarCtrl.eventer.containId(events.last)) {
            calendarCtrl.selecter.toggle(date, events);
          } else {
            if ((calendarCtrl.eventer.getSchedule[date] ?? calendarCtrl.eventer.getSchedule[local] ?? []).contains(events.last)) {
              calendarCtrl.eventer.getSchedule[date]!.remove(events.last);
              events.remove(events.last);
            }
            events.addAll(calendarCtrl.eventer.getSchedule[date] ?? calendarCtrl.eventer.getSchedule[local] ?? []);
            calendarCtrl.eventer.toggle(date, events);
          }
        }
      }
    });
  }

  Widget buildUnit(DateTime date, {int id = -1}) {
    if (id != -1) {
      return lDot(color: widget.colors![id], size: 6, padding: padding_01_LR);
    } else {
      return Container();
    }
  }

  Widget _markerBuilder(List<int> events, DateTime date) {
    return events.isNotEmpty
        ? Container(
            padding: padding_15_TB,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: events.map((e) => buildUnit(date, id: e)).toList(),
            ))
        : Container();
  }

  void _nextPage() {
    setState(() => _calendarPageCtrl.nextPage(duration: Duration(milliseconds: 100), curve: Curves.ease));
  }

  void _previousPage() {
    setState(() => _calendarPageCtrl.previousPage(duration: Duration(milliseconds: 100), curve: Curves.ease));
  }

  void _moveToToday() {
    setState(() => _focused = DateTime.now());
  }

  Widget _todayBuilder(BuildContext context, DateTime date, List<int> events) {
    return _dateBuilder(context, date, events, color_222222, isToday: true);
  }

  Widget _dateBuilder(BuildContext context, DateTime date, List<int> events, Color color, {bool isToday = false}) {
    return Container(
      height: widget.height,
      alignment: Alignment.topCenter,
      color: Colors.white,
      padding: padding_01_T,
      child: Column(
        children: [
          Container(
            width: 22,
            height: 22,
            alignment: Alignment.center,
            decoration: isToday ? BoxDecoration(shape: BoxShape.circle, border: Border.all(color: color)) : null,
            child: lText('${date.day}', style: st_13(textColor: color), textAlign: TextAlign.center),
          ),
        ],
      ),
    );
  }

  Widget selectBuilder(BuildContext context, DateTime date, List<int> events, Color color, {bool isToday = false, int id = -1}) {
    Color bg = _getColor(date);
    return Container(
      height: widget.height,
      alignment: Alignment.topCenter,
      color: bg,
      padding: padding_01_T,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 22,
            height: 22,
            alignment: Alignment.center,
            decoration: isToday ? BoxDecoration(shape: BoxShape.circle, border: Border.all(color: color)) : null,
            child: lText('${date.day}', style: st_13(textColor: color), textAlign: TextAlign.center),
          ),
        ],
      ),
    );
  }

  Widget createUnit(DateTime date, int id, Color color) {
    return lDot(color: color, size: 6, padding: padding_01_LR);
  }

  // set updateSelectable(bool selectable) => widget.selectable = selectable;

  Color _getColor(DateTime time) {
    if (calendarCtrl.selecter.getSchedule[time] != null && calendarCtrl.selecter.getSchedule[time]!.isNotEmpty) {
      bool ret = false;
      if (_newEvents.isNotEmpty) {
        _newEvents.forEach((key, value) {
          if (value.contains(calendarCtrl.selecter.getSchedule[time]!.last)) {
            ret = true;
          }
        });
        return ret ? widget.colors![calendarCtrl.selecter.getSchedule[time]!.last].withAlpha(52) : Commons.getColor().withAlpha(52);
      } else {
        return Commons.getColor().withAlpha(52);
      }
    } else {
      return Colors.white;
    }
  }

  void moveFocus(DateTime date) {
    if (this.mounted) {
      setState(() => _focused = date);
    }
  }

  DateTime _toLocal(DateTime time, DateTime origin) {
    if (origin.isUtc) time = Jiffy(time.toLocal()).startOf(Units.DAY).dateTime;
    return time;
  }
}

class TimeBlock extends StatefulWidget {
  List<DateTime> date;
  Function? editDateFun, deleteDateFun;
  Function(bool) onRepeat;
  Function editTimeFun;
  Function? deleteTimeFun, addTimeFun;
  Color color;
  DateTime startTime, endTime;
  int id, index;
  bool isRepeat = false, showTitle = true, isFirstUnit = true, showDeleteBtn = false;
  String timeDesc = '';

  TimeBlock(
      {required this.date,
      required this.editDateFun,
      this.addTimeFun,
      required this.color,
      this.deleteDateFun,
      required this.startTime,
      required this.endTime,
      required this.onRepeat,
      required this.editTimeFun,
      required this.deleteTimeFun,
      this.id = 0,
      required this.isRepeat,
      this.index = 0,
      required this.showTitle,
      this.isFirstUnit = false,
      required this.timeDesc,
      required this.showDeleteBtn}) {
    date.sort();
  }

  void updateUi(bool show) {
    this.isFirstUnit = show;
  }

  @override
  _TimeBlockState createState() => _TimeBlockState();
}

class _TimeBlockState extends State<TimeBlock> {
  bool _editMode = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _addTimeBlock(
      widget.date,
      widget.editDateFun ?? () => {},
      widget.addTimeFun ?? () => {},
      widget.color,
      widget.deleteDateFun ?? () => {},
      widget.startTime,
      widget.endTime,
      widget.onRepeat,
      widget.editTimeFun,
      widget.deleteTimeFun!,
      id: widget.id,
      index: widget.index,
      isRepeat: widget.isRepeat,
      showTitle: widget.showTitle,
      timeDesc: widget.timeDesc,
      showDeleteBtn: widget.showDeleteBtn,
    );
  }

  Widget _addTimeBlock(List<DateTime> date, Function editDateFun, Function addTimeFun, Color color, Function deleteDateFun, DateTime startTime, DateTime endTime, Function onRepeat, Function editTimeFun, Function deleteTimeFun,
      {int id = 0, bool isRepeat = false, bool showTitle = true, int index = 0, String timeDesc = '', bool showDeleteBtn = false}) {
    DateFormat _koMonthDay = DateFormat().addPattern("MM${"월".tr()} dd${"일".tr()}");
    DateFormat _koYearMonthDay = DateFormat().addPattern("MM${"월".tr()} dd${"일".tr()}");
    late String lastDate, firstDate, year = '';
    bool duplicateCheck = false;
    format(Duration d) => d.toString().split(':');
    List<String> time = format(endTime.difference(startTime));

    if (date.isEmpty) {
      firstDate = '';
      lastDate = '';
    } else if (date.last.year > date.first.year) {
      lastDate = _koYearMonthDay.format(date.last);
      firstDate = _koMonthDay.format(date.first);
      year = DateFormat().addPattern("(yy${"년".tr()})").format(date.last);
    } else {
      duplicateCheck = date.first == date.last;
      lastDate = _koMonthDay.format(date.last);
      firstDate = _koMonthDay.format(date.first);
    }

    timeDesc = timeDesc.isEmpty ? "(최대 30일)".tr() : timeDesc;

    return Container(
        child: Column(children: [
      showTitle
          ? Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Row(mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                lText(firstDate, style: st_16(textColor: duplicateCheck && _editMode ? color : color_222222, fontWeight: FontWeight.bold)),
                duplicateCheck ? Container() : lText(" - ", style: st_b_16(fontWeight: FontWeight.bold)),
                duplicateCheck
                    ? Container()
                    : Row(
                        children: [
                          year.isNotEmpty ? lText(year, style: st_16(textColor: _editMode ? color : Commons.getColor(), fontWeight: FontWeight.bold)) : Container(),
                          lText(lastDate, style: st_16(textColor: year.isNotEmpty ? color : color_222222, fontWeight: FontWeight.bold)),
                        ],
                      ),
                sb_w_08,
                EditTextButton(
                    callback: (repeat, state, callback) {
                      editDateFun(repeat, state, callback);
                    },
                    enableTitle: "저장".tr(),
                    disableTitle: "날짜수정".tr(),
                    isRepeat: isRepeat,
                    checked: _editMode),
              ]),
            ])
          : Container(),
      Column(children: [
        sb_h_05,
        Container(
            padding: padding_20,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: color_dedede, width: 1),
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
            child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, mainAxisSize: MainAxisSize.min, children: [
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                  Container(
                      width: 12,
                      height: 12,
                      decoration: BoxDecoration(
                        color: color,
                        shape: BoxShape.circle,
                      )),
                  lInkWell(
                      onTap: () {
                        editTimeFun();
                      },
                      child: Row(children: [
                        sb_w_05,
                        lText("${DateFormat.Hm().format(startTime)} - ${DateFormat.Hm().format(endTime)}", style: st_b_16(fontWeight: FontWeight.w700)),
                        sb_w_05,
                        lText("(${time[0]}${"시간".tr()} ${time[1]}${"분".tr()})", style: st_b_14(textColor: color_545454, fontWeight: FontWeight.w500)),
                      ])),
                ]),
                showDeleteBtn
                    ? SizedBox(
                        width: 30,
                        height: 20,
                        child: IconButton(
                            padding: padding_0,
                            icon: lText("삭제".tr(), style: st_14(fontWeight: FontWeight.w500, textColor: color_b2b2b2)),
                            onPressed: () {
                              deleteTimeFun(widget.isFirstUnit);
                            }),
                      )
                    : Container(),
              ]),
              sb_h_15,
              lIconText(
                title: "${"매주반복".tr()} $timeDesc",
                onClick: () => setState(() {
                  isRepeat = !isRepeat;
                  onRepeat(isRepeat);
                }),
                textStyle: st_14(textColor: color_999999, fontWeight: FontWeight.w500),
                icon: Lcons.checkbox(isEnabled: isRepeat, color: Commons.getColor(), disableColor: color_cecece),
              )
            ]))
      ])
    ]));
  }
}

class EditTextButton extends StatefulWidget {
  final String enableTitle, disableTitle;
  final Color? enableColor, disableColor;
  bool checked = false;
  bool isRepeat = false;
  final Function? callback;

  EditTextButton({this.enableTitle = '', this.disableTitle = '', this.enableColor, this.disableColor, this.checked = false, this.isRepeat = false, this.callback});

  @override
  _EditTextButtonSatte createState() => _EditTextButtonSatte();
}

class _EditTextButtonSatte extends State<EditTextButton> {
  bool checked = false;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
        padding: padding_0,
        onPressed: () {
          checked = !checked;
          widget.callback!(widget.isRepeat, checked, (state) {
            setState(() {
              widget.checked = state;
              checked = state;
            });
          });
        },
        child: Container(
            decoration: BoxDecoration(
                color: (widget.isRepeat
                    ? color_b2b2b2.withAlpha(52)
                    : checked
                        ? Commons.getColor()
                        : Commons.getColor().withAlpha(52)),
                borderRadius: BorderRadius.circular(26)),
            padding: EdgeInsets.only(top: 6, bottom: 6, left: 20, right: 20),
            child: Row(mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.center, children: [
              lText(
                  widget.isRepeat
                      ? widget.disableTitle
                      : checked
                          ? widget.enableTitle
                          : widget.disableTitle,
                  style: st_12(textColor: (widget.isRepeat ? color_b2b2b2 : (checked ? Colors.white : Commons.getColor())))),
              if (widget.isRepeat && checked) Lcons.check(color: Colors.white, size: 12, isEnabled: true),
            ])));
  }
}
