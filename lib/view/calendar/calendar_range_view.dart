// Copyright 2021 linkmom
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/calendar/schedule_controller.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:table_calendar/table_calendar.dart';

class CalendarRangeView extends StatefulWidget {
  final USER_TYPE type;
  final Map<DateTime, List<int>>? events;
  final Function selected;
  final String unavailableMsg;
  final Duration? duration;
  final DateTime? startDate;
  final int eventLimit;

  CalendarRangeView({required this.type, this.events, required this.selected, this.unavailableMsg = ' ', this.duration, this.eventLimit = 0, this.startDate});

  @override
  _CalendarRangeViewState createState() => _CalendarRangeViewState();
}

class _CalendarRangeViewState extends State<CalendarRangeView> with TickerProviderStateMixin {
  late PageController _calendarController;
  bool _lastPrevious = true;
  late Function selectCallback;
  double _height = 45;
  DateTime _focusedDay = DateTime.now();
  ScheduleController _schedCtrl = ScheduleController();
  List<DateTime> _range = [];

  @override
  void initState() {
    super.initState();
    _calendarController = PageController();
    selectCallback = widget.selected;
  }

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Container(
          margin: padding_10_LR,
          child: Row(children: [
            Row(mainAxisAlignment: MainAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: [
              lText(DateFormat().addPattern('yyyy.MM').format(_focusedDay), style: stRegisterCompleteSelect),
              sb_w_10,
              lTextBtn(
                "오늘".tr(),
                style: st_b_10(),
                border: Border.all(color: color_222222),
                radius: 16,
                bg: Colors.white,
                padding: EdgeInsets.only(left: 9, right: 9, top: 5, bottom: 5),
                onClickEvent: () => _moveToToday(),
              ),
            ]),
            Expanded(child: Container(), flex: 1),
            Row(mainAxisAlignment: MainAxisAlignment.end, mainAxisSize: MainAxisSize.min, children: [
              IconButton(
                  visualDensity: VisualDensity(horizontal: VisualDensity.minimumDensity),
                  padding: padding_0,
                  icon: Lcons.nav_left(color: _lastPrevious ? color_cecece : color_222222),
                  onPressed: () {
                    if (!_lastPrevious) _previousPage();
                  }),
              sb_w_10,
              IconButton(
                  visualDensity: VisualDensity(horizontal: VisualDensity.minimumDensity),
                  padding: padding_0,
                  icon: Lcons.nav_right(color: color_222222),
                  onPressed: () {
                    _nextPage();
                  }),
            ])
          ])),
      sb_h_15,
      _createCalendar((context, date, _) {}),
    ]);
  }

  void removeUnit(int id) {
    if (widget.type == USER_TYPE.mom_daddy) id = 0;
    _schedCtrl.selecter.getSchedule.forEach((date, value) {
      value.remove(id);
    });
    setState(() {});
  }

  void updateUnit(List<DateTime> dates, {int id = -1}) {
    _schedCtrl.selecter.getSchedule.forEach((date, value) {
      if (!dates.contains(date)) {
        _schedCtrl.selecter.getSchedule[date]!.remove(id);
      }
    });
    dates.forEach((date) {
      _schedCtrl.selecter.getSchedule.update(date, (value) {
        value.add(id);
        value.sort();
        return value.toSet().toList();
      }, ifAbsent: () => [id]);
    });
    _schedCtrl.selecter.getSchedule.clear();
    setState(() {});
  }

  List<DateTime> changeUnit(List<DateTime> oldList, int id) {
    List<DateTime> dates = [];
    _schedCtrl.selecter.getSchedule.forEach((date, value) {
      if (_schedCtrl.selecter.getSchedule.containsKey(date) && _schedCtrl.selecter.getSchedule[date]!.contains(id)) {
        // select O, recent O -> remove
        _schedCtrl.selecter.getSchedule[date]!.remove(id);
      } else {
        // select O, recent X -> Add
        _schedCtrl.selecter.getSchedule.update(date, (value) {
          value.add(id);
          return value;
        }, ifAbsent: () => [id]);
      }
    });
    _schedCtrl.selecter.getSchedule.forEach((date, value) {
      if (value.contains(id)) {
        dates.add(date);
      }
    });
    _schedCtrl.selecter.getSchedule.clear();
    setState(() {});
    return dates;
  }

  Widget _createUnit(DateTime date, int id, Color color) {
    return Container(alignment: Alignment.center, width: 7, height: st_15().fontSize, margin: EdgeInsets.all(1), decoration: BoxDecoration(color: color, shape: BoxShape.circle));
  }

  Widget _createCalendar(Function selectedDay) {
    return TableCalendar(
      rowHeight: _height,
      locale: 'ko-KR',
      focusedDay: _focusedDay,
      firstDay: widget.startDate ?? DateTime.now(),
      lastDay: (widget.startDate ?? DateTime.now()).add(widget.duration ?? Duration(days: 365 * 2)),
      calendarFormat: CalendarFormat.month,
      startingDayOfWeek: StartingDayOfWeek.sunday,
      availableGestures: AvailableGestures.horizontalSwipe,
      calendarStyle: CalendarStyle(
        outsideDaysVisible: true,
      ),
      daysOfWeekStyle: DaysOfWeekStyle(weekendStyle: st_13(textColor: color_ff3b30), weekdayStyle: st_b_13(fontWeight: FontWeight.w400)),
      headerStyle: HeaderStyle(
        formatButtonVisible: false,
      ),
      headerVisible: false,
      calendarBuilders: CalendarBuilders(
        defaultBuilder: (context, date, events) => _dateBuilder(context, date, [], color_222222),
        todayBuilder: (context, date, events) => _todayBuilder(context, date, []),
        disabledBuilder: (context, date, events) => _dateBuilder(context, date, [], color_dedede),
        outsideBuilder: (context, date, events) => _dateBuilder(context, date, [], color_dedede),
        selectedBuilder: (context, date, events) => _selectBuilder(context, date, _schedCtrl.selecter.getSchedule[date] ?? [], color_222222, _isToday(date)),
      ),
      onDaySelected: (date, focused) {
        _updateEvent(date, _schedCtrl.selecter.getSchedule[date] ?? []);
      },
      selectedDayPredicate: (date) => _schedCtrl.selecter.getSchedule[date] != null,
      onPageChanged: (focused) {
        setState(() {
          if (focused.month < DateTime.now().add(Duration(days: -90)).month) {
            _lastPrevious = true;
          } else {
            _lastPrevious = false;
          }
          _focusedDay = focused;
        }); // title update
      },
      onCalendarCreated: (ctrl) => _calendarController = ctrl,
    );
  }

  void _updateEvent(DateTime date, List<int> events) {
    setState(() {
      if (_range.contains(date)) {
        _schedCtrl.selecter.remove(date);
        _range.remove(date);
      } else {
        if (_range.length > 1) {
          _schedCtrl.selecter.clear();
          _range.clear();
        } 
        _schedCtrl.selecter.append(date);
        _range.add(date);
      }
      _range.sort();
      selectCallback(date, events);
    });
  }

  List<Widget> _buildUnit(List<int> events, DateTime date) {
    List<Widget> children = [];
    for (int event in events) {
      children.add(_createUnit(date, event, Commons.getColor()));
      if (children.length == (widget.eventLimit)) break;
    }
    return [Row(mainAxisAlignment: MainAxisAlignment.center, children: children)];
  }

  void _nextPage() {
    setState(() {
      _lastPrevious = false;
      _calendarController.nextPage(duration: Duration(milliseconds: 100), curve: Curves.ease);
    });
  }

  void _previousPage() {
    setState(() {
      _calendarController.previousPage(duration: Duration(milliseconds: 100), curve: Curves.ease);
      if (_focusedDay.month <= DateTime.now().month) _lastPrevious = true;
    });
  }

  void _moveToToday() {
    setState(() {
      _focusedDay = DateTime.now();
    });
  }

  Widget _todayBuilder(BuildContext context, DateTime date, List<int> events) {
    return _dateBuilder(context, date, events, color_222222, isToday: true);
  }

  Widget _dateBuilder(BuildContext context, DateTime date, List<int> events, Color color, {bool isToday = false}) {
    Color bg = Colors.transparent;
    if (_range.isNotEmpty && (date.isAfter(_range.first) && date.isBefore(_range.last))) bg = Commons.getColor().withOpacity(0.16);
    return Container(
      height: _height,
      alignment: Alignment.center,
      color: bg,
      padding: padding_04_T,
      child: Container(
          width: 22,
          height: 22,
          alignment: Alignment.center,
          decoration: isToday ? BoxDecoration(shape: BoxShape.circle, border: Border.all(color: color)) : null,
          child: lText('${date.day}', style: st_13(textColor: color), textAlign: TextAlign.center)),
    );
  }

  Widget _selectBuilder(BuildContext context, DateTime date, List<int> events, Color color, bool isToday) {
    return Container(
      height: _height,
      alignment: Alignment.topCenter,
      color: Commons.getColor().withOpacity(0.32),
      padding: padding_01_T,
      child: _dateBuilder(context, date, events, color, isToday: isToday),
    );
  }

  bool _isToday(DateTime date) {
    DateTime now = DateTime.now();
    return now.year == date.year && now.month == date.month && now.day == date.day;
  }
}
