// Copyright 2021 linkmom
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/req_job_data.dart';
import 'package:linkmom/utils/string_util.dart';

class JobSchedule extends ReqJobScheduleData {
  JobSchedule({
    required List<String> bookingdate,
    List<String>? removedate,
    List<ScheduleTimeData>? times,
  }) : super(
          bookingdate: bookingdate,
          removedate: removedate,
          times: times,
        );

  List<DateTime> _dates = [];
  List<DateTimeRange> _times = [];

  ReqJobScheduleData get getSchedule => this;

  List<String> get bookingDate {
    if (this.bookingdate == null) this.bookingdate = [];
    return this.bookingdate!;
  }

  List<ScheduleTimeData> get workTimes {
    if (this.times == null) this.times = [];
    return this.times!;
  }

  List<String> get removeDate {
    if (this.removedate == null) this.removedate = [];
    return this.removedate!;
  }

  List<DateTime> get getDates => this._dates;
  List<DateTimeRange> get getTimes => this._times;

  void addSchedule(List<DateTime> schedules) {
    schedules.sort();
    schedules.forEach((date) {
      String name = StringUtils.formatYMD(date);
      if (!bookingDate.contains(name)) this.bookingDate.add(name);
      if (!this._dates.contains(date)) this._dates.add(date);
    });
  }

  void clearSchedule() {
    bookingDate.clear();
    this._dates.clear();
  }

  void addTime({
    int id = 0,
    required DateTime stime,
    required DateTime etime,
    int isRepeat = 0,
    int durationtime = 0,
    String caredateUntil = '',
  }) {
    workTimes.add(ScheduleTimeData(
      id: id,
      stime: StringUtils.formatHD(stime),
      etime: StringUtils.formatHD(etime),
      is_am: isAm(stime, etime),
      is_pm: isPm(stime, etime),
      is_repeat: isRepeat,
      durationtime: durationtime,
      caredate_until: caredateUntil,
    ));
    stime = StringUtils.convertNow(hour: stime.hour, min: stime.minute);
    etime = StringUtils.convertNow(hour: etime.hour, min: etime.minute);
    this._times.add(DateTimeRange(start: stime, end: etime));
  }

  void updateTime(int id, {DateTime? stime, DateTime? etime, bool isRepeat = false}) {
    DateTime temp = DateTime.now();
    if (stime != null && etime != null) {
      if (stime.isAfter(etime)) {
        temp = stime;
        stime = etime;
        etime = temp;
      }

      workTimes[id].stime = StringUtils.formatHD(stime);
      workTimes[id].etime = StringUtils.formatHD(etime);
      workTimes[id].is_am = isAm(stime, etime);
      workTimes[id].is_pm = isPm(stime, etime);
      workTimes[id].durationtime = stime.difference(etime).inMinutes.abs();
      this._times[id] = DateTimeRange(start: stime, end: etime);
    }
    workTimes[id].is_repeat = isRepeat ? 1 : 0;
  }

  void updateDate(int id, DateTime date) {
    workTimes[id].caredate_until = StringUtils.formatYMD(date);
  }

  void removeTimes(int id) {
    workTimes.removeAt(id);
  }

  void setSchedule(ReqJobScheduleData schedule) {
    schedule.bookingdate!.sort();
    this.bookingdate = schedule.bookingdate;
    workTimes.addAll(schedule.times ?? []);
    this.removedate = schedule.removedate;
    this._dates = bookingDate.map((e) => StringUtils.parseYMD(e)).toList();
    this._times = workTimes.map((e) {
      DateTime start = StringUtils.parseHD(e.stime);
      DateTime end = StringUtils.parseHD(e.etime);
      return DateTimeRange(
        start: StringUtils.convertNow(hour: start.hour, min: start.minute),
        end: StringUtils.convertNow(hour: end.hour, min: end.minute),
      );
    }).toList();
  }

  Map<DateTime, List<int>> asMap() {
    Map<DateTime, List<int>> map = {};
    bookingDate.forEach((date) {
      map.putIfAbsent(StringUtils.parseYMD(date), () => [0]);
    });
    return map;
  }

  List<DateTime> get schedules => this._dates;

  int isAm(DateTime stime, DateTime etime) {
    if (stime.hour < 12 || etime.hour < 12) {
      return 1;
    } else {
      return 0;
    }
  }

  int isPm(DateTime stime, DateTime etime) {
    if (stime.hour > 11 || etime.hour > 11) {
      return 1;
    } else {
      return 0;
    }
  }

  void remove(DateTime date) {
    if (this._dates.contains(date)) {
      this._dates.remove(date);
      this.bookingDate.remove(StringUtils.formatYMD(date));
    }
  }
}
