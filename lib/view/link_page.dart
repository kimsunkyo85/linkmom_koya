import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:linkmom/data/providers/link_provider.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/route_action.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:provider/provider.dart';


class DeepLinkingPage extends StatefulWidget {
  final String url;

  DeepLinkingPage({this.url = ''});

  @override
  State<DeepLinkingPage> createState() => _DeepLinkingPageState();
}

class _DeepLinkingPageState extends State<DeepLinkingPage> {
  late LinkProvider _provider;

  @override
  void initState() {
    super.initState();
    _provider = context.read<LinkProvider>();
    if (_provider.getLink.isNotEmpty) {
      _provider.received(context, _provider.getLink);
    } else {
      _provider.received(context, widget.url);
    }
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Future.delayed(Duration(seconds: 2), () {
        if (!this.mounted) Commons.pagePop(context);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
      appBar: appBar("", onBack: () => Commons.pageClear(context, routeSplash)),
      body: LoadingIndicator(showText: false, background: Colors.white),
    );
  }
}

class DeepLinkManager {
  static const String channel = 'deeplink';

  DeepLinkManager(BuildContext context) {
    startUri().then((d) => _onRedirected(d.toString()));
    stream.receiveBroadcastStream().listen((d) => _onRedirected(d));
    platform.setMethodCallHandler((call) async {
      log.d({'${call.method}': '${call.arguments.toString()}'});
      String url = call.arguments.toString();
      if (call.method == 'deeplink' && url.isNotEmpty) {
        if (Commons.isLogin) {
          RouteAction.onLink(context, url);
        } else {
          showNormalDlg(message: "로그인후확인가능합니다".tr(), onClickAction: (a) => {if (a == DialogAction.yes) Commons.pageClear(context, routeLogin)});
        }
      }
    });
  }

  static const stream = const EventChannel(channel);

  static const platform = const MethodChannel(channel);

  StreamController<String> _stateController = StreamController();

  Stream<String> get state => _stateController.stream;

  Sink<String> get stateSink => _stateController.sink;

  _onRedirected(String uri) => stateSink.add(uri);

  Future<Object> startUri() async {
    try {
      return DeepLinkManager.platform.invokeMethod(channel);
    } on PlatformException catch (e) {
      return "Failed to Invoke: '${e.message}'.";
    }
  }
}

