import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

class TypeSelecter extends StatefulWidget {
  final Widget? modalHeader;
  final Widget? modalBottom;
  final Widget? header;
  final Widget? bottom;
  final String title;
  final String btnText;
  final int maxSelect;
  final List<SelecterModel> models;
  final Function(List<dynamic>) onSelectedData;
  final ViewType viewType;

  TypeSelecter({this.modalHeader, this.modalBottom, this.title = '', this.btnText = '', this.maxSelect = 1, required this.models, this.header, this.bottom, required this.onSelectedData, required this.viewType}) {
    assert((title.isNotEmpty && modalHeader == null) || title.isEmpty && modalHeader != null);
  }

  @override
  _TypeSelecterState createState() => _TypeSelecterState();
}

class _TypeSelecterState extends State<TypeSelecter> {
  List<dynamic> get getSelectedData => widget.models.where((e) => e.selected).map((e) => e.data).toList();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding_20,
      decoration: BoxDecoration(border: Border.all(color: color_dedede), borderRadius: BorderRadius.circular(20)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          widget.header ??
              Container(
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                lText(widget.title, style: st_b_14(textColor: color_545454, fontWeight: FontWeight.w500)),
                lInkWell(
                  onTap: () {
                    setState(() {
                      showModal();
                    });
                  },
                  child: widget.viewType == ViewType.view ? Center() : Container(height: 28, width: 28, alignment: Alignment.center, decoration: BoxDecoration(color: color_eeeeee, borderRadius: BorderRadius.circular(100)), child: Lcons.add()),
                ),
              ])),
          sb_h_20,
          widget.bottom ??
              Wrap(
                  alignment: WrapAlignment.start,
                  children: widget.models
                      .where((e) => e.selected)
                      .map((e) => Container(
                          margin: padding_10_RB,
                          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                          decoration: BoxDecoration(border: Border.all(color: Commons.getColor()), color: Colors.white, borderRadius: BorderRadius.circular(32)),
                          child: lInkWell(
                              child: Row(mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                lText(e.title, style: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
                                widget.viewType == ViewType.view
                                    ? Center()
                                    : Row(
                                        children: [
                                          sb_w_05,
                                          Lcons.nav_close(size: 18, color: color_c4c4c4),
                                        ],
                                      ),
                              ]),
                              onTap: widget.viewType == ViewType.view
                                  ? null
                                  : () {
                                      setState(() {
                                        e.selected = false;
                                        widget.onSelectedData(getSelectedData);
                                      });
                                    })))
                      .toList())
        ],
      ),
    );
  }

  void showModal() {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        builder: (ctx) {
          return StatefulBuilder(
            builder: (context, state) {
              return Container(
                padding: padding_30,
                height: 320,
                child: Column(
                  children: [
                    widget.modalHeader ??
                        Container(
                            padding: padding_30_B,
                            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.center, children: [
                              Row(crossAxisAlignment: CrossAxisAlignment.end, children: [
                                lText(widget.title, style: st_16(textColor: color_545454, fontWeight: FontWeight.w700)),
                                sb_w_04,
                                lText("아이등록_아이성향_조건".tr(), style: st_13(textColor: color_ff3b30, fontWeight: FontWeight.w400)),
                              ]),
                              lInkWell(
                                  onTap: () {
                                    setState(() {
                                      Navigator.pop(context);
                                    });
                                  },
                                  child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                                    lText("확인".tr(), style: st_16(textColor: color_545454, fontWeight: FontWeight.w500)),
                                    Lcons.check(color: color_545454, size: 15, isEnabled: true),
                                  ])),
                            ])),
                    widget.modalBottom ??
                        Container(
                            width: double.infinity,
                            height: 200,
                            child: GridView.builder(
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: 5),
                                itemCount: widget.models.length,
                                itemBuilder: (ctx, i) {
                                  return lInkWell(
                                      onTap: widget.viewType == ViewType.view
                                          ? null
                                          : () {
                                              state(() {
                                                widget.models[i].selected = !widget.models[i].selected;
                                                if (widget.models.where((e) => e.selected).length > 3) {
                                                  widget.models[i].selected = false;
                                                }
                                                widget.onSelectedData(getSelectedData);
                                              });
                                            },
                                      child: Container(
                                          child: Row(children: [
                                        Lcons.checkbox(color: Commons.getColor(), disableColor: color_cecece, size: 24, isEnabled: widget.models[i].selected),
                                        sb_w_05,
                                        lText(widget.models[i].title, style: st_b_14(textColor: color_545454)),
                                      ])));
                                }))
                  ],
                ),
              );
            },
          );
        });
  }
}

class SelecterModel {
  bool selected;
  final String title;
  final dynamic data;

  SelecterModel(this.title, this.data, {this.selected = false});
}
