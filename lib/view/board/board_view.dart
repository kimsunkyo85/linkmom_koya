import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/image/image_add_page.dart';
import 'package:linkmom/view/image/image_add_view.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/chat/full_photo.dart';

class ReplyData {
  final int id;
  final int? parent;
  final String writer;
  final String message;
  final DateTime writtenTime;
  final String? profile;
  final List<ReplyData>? reReply;
  final Map<Widget, Function>? replyMenu;
  final bool isMine;
  final bool isWriter;
  final bool isDeleted;
  final bool showMenu;
  int? level;

  ReplyData({
    required this.id,
    this.parent,
    required this.writer,
    required this.message,
    required this.writtenTime,
    this.reReply,
    this.profile,
    this.replyMenu,
    this.isMine = false,
    this.isWriter = false,
    this.isDeleted = false,
    this.level = 0,
    this.showMenu = true,
  });
}

class BoardProfile extends StatefulWidget {
  final String author;
  final String writtenTime;
  final String? profileImg;
  final String location;
  final bool showCommunityAvatar;
  final Widget? rightBtn;
  final BoxDecoration? decoration;
  final EdgeInsets? margin, padding;

  BoardProfile({
    this.author = "",
    this.writtenTime = "",
    this.profileImg,
    this.location = "",
    this.showCommunityAvatar = false,
    this.rightBtn,
    this.decoration,
    this.margin,
    this.padding,
  });

  @override
  BoardProfileState createState() => BoardProfileState();
}

class BoardProfileState<T extends BoardProfile> extends State<T> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: widget.padding ?? padding_10_TB,
        decoration: widget.decoration,
        margin: widget.margin,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                lProfileAvatar(widget.profileImg, radius: 20, fromCommunity: widget.showCommunityAvatar),
                sb_w_10,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    lText(widget.author, style: st_b_15(fontWeight: FontWeight.w700)),
                    widget.location.isEmpty
                        ? lText(widget.writtenTime, style: st_13(textColor: color_b2b2b2))
                        : Row(children: [
                            lText(widget.writtenTime, style: st_13(textColor: color_b2b2b2)),
                            Container(
                              padding: padding_10_LR,
                              height: 14,
                              child: lDividerVertical(color: color_dedede, thickness: 1),
                            ),
                            lIconText(icon: Lcons.location(color: color_999999, size: 15), title: widget.location, padding: padding_0, isExpanded: false, textStyle: st_13(textColor: color_999999))
                          ])
                  ],
                )
              ],
            ),
            if (widget.rightBtn != null) widget.rightBtn!,
          ],
        ));
  }
}

class BoardBody extends StatelessWidget {
  final String text;
  final Widget? hint;
  final List<String>? images;
  final ViewType viewType;
  final EdgeInsets padding;

  BoardBody({this.text = '', this.images, this.hint, this.viewType = ViewType.summary, this.padding = padding_20});

  PageController _controller = PageController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: padding,
          alignment: Alignment.topLeft,
          child: text.isNotEmpty ? lText(text, style: st_b_15(), overflow: TextOverflow.visible) : hint,
        ),
        if (text.isNotEmpty && text.length < 50) AspectRatio(aspectRatio: Commons.ratio(15)),
        if (images != null && images!.isNotEmpty)
          viewType == ViewType.summary
              ? Container(
                  padding: padding_20,
                  child: imageThumbnailView(context, images ?? [], origin: images),
                )
              : Container(
                  child: Column(
                    children: images!
                        .map((e) => lInkWell(
                            onTap: () => Commons.nextPage(context, fn: () => FullPhotoPage(index: images!.indexOf(e), urlList: images)),
                            child: Container(
                              margin: padding_10_T,
                              child: CachedNetworkImage(
                                imageUrl: e,
                                placeholder: (ctx, provider) => Padding(
                                  padding: padding_20,
                                  child: CircularProgressIndicator(color: color_eeeeee),
                                ),
                                imageBuilder: (ctx, provider) => Container(
                                  decoration: BoxDecoration(border: Border.all(color: color_eeeeee)),
                                  child: Image(image: provider),
                                ),
                              ),
                            )))
                        .toList(),
                  ),
                ),
      ],
    );
  }
}

class BoardReadView extends StatefulWidget {
  final BoardProfile? profile;
  final Widget? title;
  final Widget? body;
  final Widget? footer, topper;
  List<ReplyData>? reply;
  int replyLength;
  final bool showReply;
  final Function? saveReplyAction;
  final Function? onNextReply;
  final EdgeInsets? padding;
  final bool fromComm;

  BoardReadView({this.profile, this.body, this.reply, this.footer, this.topper, this.replyLength = 0, this.saveReplyAction, this.showReply = false, this.title, this.padding, this.onNextReply, this.fromComm = false}) {
    if (this.reply == null) this.reply = [];
  }

  @override
  BoardReadViewState createState() => BoardReadViewState();
}

class BoardReadViewState<T extends BoardReadView> extends State<T> {
  int? _focusedId = 0;
  FocusNode _replyFocus = FocusNode();
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Wrap(
      children: [
        Column(mainAxisSize: MainAxisSize.min, children: [
          Container(
            padding: widget.padding ?? padding_20_LR,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (widget.profile != null) widget.profile!,
                if (widget.topper != null) widget.topper!,
                if (widget.title != null) widget.title!,
                if (widget.body != null) widget.body!,
                if (widget.footer != null) widget.footer!,
              ],
            ),
          ),
          if (widget.showReply)
            ReplyView(
                isComm: widget.fromComm,
                reply: widget.reply!,
                replyLength: widget.replyLength,
                addAnswer: (parent) {
                  _focusedId = parent;
                  _replyFocus.requestFocus();
                }),
        ]),
        if (widget.showReply)
          Container(
              padding: EdgeInsets.fromLTRB(15, 10, 15, 0),
              color: color_eeeeee.withOpacity(0.52),
              alignment: Alignment.centerLeft,
              child: ConstrainedBox(
                constraints: BoxConstraints(maxHeight: 150, minHeight: 85),
                child: TextField(
                  focusNode: _replyFocus,
                  textAlign: TextAlign.left,
                  textInputAction: TextInputAction.done,
                  controller: _controller,
                  maxLines: null,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(15, 10, 5, 10),
                    isDense: true,
                    fillColor: Colors.white,
                    filled: true,
                    suffixIcon: TextButton(
                        onPressed: () {
                          widget.saveReplyAction!(_focusedId, _controller.text);
                          _controller.text = "";
                          _focusedId = 0;
                          focusClear(context);
                        },
                        child: lText("등록".tr(), style: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.w500))),
                    border: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(24)),
                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(24)),
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(24)),
                    hintText: "댓글을남겨주세요".tr(),
                    hintStyle: st_14(textColor: color_cecece),
                  ),
                  onChanged: (value) {
                    setState(() {});
                  },
                ),
              ))
      ],
    ));
  }
}

class BoardWriteView extends StatefulWidget {
  final String? hint;
  final TextEditingController? controller;
  final EdgeInsets? padding;
  final bool showCount;
  final Widget? hintWidget, topper;
  final Function update;
  final Function? addImageAction;
  final bool useImage;
  final bool leftCount;
  List<File>? image = [];
  final int imageLength;
  final int? bodyLength;
  final FocusNode? focus;
  final Function(FocusNode)? onCreate;

  BoardWriteView({
    this.hint,
    this.controller,
    this.image,
    this.padding,
    this.showCount = true,
    this.hintWidget,
    required this.update,
    this.useImage = true,
    this.leftCount = true,
    this.addImageAction,
    this.imageLength = 5,
    this.bodyLength,
    this.onCreate,
    this.topper,
    this.focus,
  });

  @override
  BoardWriteViewState createState() => BoardWriteViewState();
}

class BoardWriteViewState extends State<BoardWriteView> {
  TextEditingController _controller = TextEditingController();
  FocusNode _focus = FocusNode();

  @override
  void initState() {
    super.initState();
    if (widget.focus != null) {
      _focus = widget.focus!;
    }
    _focus.addListener(() {
      setState(() {});
    });
    if (widget.onCreate != null) widget.onCreate!(_focus);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: widget.padding ?? padding_20,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (widget.topper != null) widget.topper!,
          if (widget.hintWidget != null && !_focus.hasFocus && (widget.controller ?? _controller).text.isEmpty)
            lInkWell(
              onTap: () => _focus.requestFocus(),
              child: widget.hintWidget!,
            ),
          Container(
            child: TextField(
              controller: widget.controller ?? _controller,
              style: st_b_15(),
              focusNode: _focus,
              decoration: InputDecoration(
                fillColor: Colors.white,
                hintMaxLines: _focus.hasFocus ? 1 : 10,
                hintText: _focus.hasFocus ? "" : widget.hint,
                hintStyle: st_15(textColor: color_b2b2b2, height: 1.5),
                border: UnderlineInputBorder(borderSide: BorderSide.none),
                filled: true,
                isDense: true,
                contentPadding: padding_20_TB,
              ),
              maxLines: null,
              maxLength: widget.bodyLength,
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              buildCounter: (_, {currentLength = 0, maxLength, isFocused = false}) => Container(
                child: Container(
                    height: 15,
                    alignment: widget.leftCount ? Alignment.centerLeft : Alignment.centerRight,
                    child: widget.showCount
                        ? TextHighlight(
                            text: StringUtils.formatPay(currentLength) + "/" + StringUtils.formatPay(maxLength ?? 1000),
                            term: "/" + StringUtils.formatPay(maxLength),
                            textStyle: st_b_14(),
                            textStyleHighlight: st_14(textColor: color_b2b2b2),
                          )
                        : Container()),
              ),
              onChanged: (v) {
                widget.update();
              },
            ),
          ),
          if (widget.useImage)
            Container(
              height: 120,
              padding: padding_20_T,
              child: ImageAddView(
                max: widget.imageLength,
                data: widget.image ?? [],
                onItemClick: (image, index) {
                  if (image is ImageItem) {
                    widget.image!.remove(image.file);
                  } else {
                    widget.image = image;
                  }
                  if (widget.addImageAction != null) widget.addImageAction!(widget.image);
                  widget.update();
                },
              ),
            ),
        ],
      ),
    );
  }
}

class ReplyView extends StatelessWidget {
  List<ReplyData>? reply;
  int replyLength;
  final Function? addAnswer;
  final bool isComm;
  final EdgeInsets? padding;

  ReplyView({this.reply, this.addAnswer, this.replyLength = 0, this.isComm = false, this.padding});

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: Column(
          children: [
            if (!isComm) lDivider(color: color_dedede.withAlpha(81), thickness: 8.0),
            Container(
                padding: padding ?? padding_20,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    if (!isComm)
                      Container(
                        padding: padding_20_TB,
                        margin: padding_05_B,
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(color: color_eeeeee)),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            lText("댓글".tr(), style: st_15(textColor: color_999999, fontWeight: FontWeight.w500)),
                            Container(padding: padding_05_LR, child: Lcons.chat(color: color_999999, size: 20)),
                            lText("$replyLength", style: st_15(textColor: color_999999, fontWeight: FontWeight.w500)),
                          ],
                        ),
                      ),
                    Container(
                        padding: padding_05_TB,
                        child: reply!.isNotEmpty
                            ? Column(children: reply!.map((e) => ReplyList(e, (parent) => addAnswer!(parent), depth: e.level ?? 0, isComm: isComm, showMenu: e.showMenu)).toList())
                            : Padding(
                                padding: padding_30,
                                child: lText("첫번째댓글을남겨주세요".tr(), style: st_16(textColor: color_cecece, fontWeight: FontWeight.w700)),
                              ))
                  ],
                ))
          ],
        ));
  }
}

class ReplyList extends StatelessWidget {
  final ReplyData reply;
  final Function addAnswer;
  final int depth;
  final bool isComm;
  final bool showMenu;

  ReplyList(this.reply, this.addAnswer, {this.depth = 0, this.isComm = false, this.showMenu = true});

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        ReplyWidget(reply, width: Commons.width(context, 0.65), addAnswer: (parent) => addAnswer(parent), isComm: isComm, showMenu: showMenu),
        if (reply.reReply != null)
          Container(
              child: Column(
            children: reply.reReply!.map((e) => ReplyList(e, (parent) => addAnswer(parent), depth: e.level ?? 0)).toList(),
          )),
      ],
    ));
  }
}

class ReplyWidget extends StatelessWidget {
  final ReplyData reply;
  final double width;
  final Map<Widget, Function>? menu;
  final Function? addAnswer;
  final bool showMenu;
  final bool isComm;

  ReplyWidget(this.reply, {this.width = double.infinity, this.menu, this.addAnswer, this.showMenu = true, this.isComm = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: padding_07_TB,
      padding: (reply.level ?? 0) > 0 ? padding_30_L : padding_0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(padding: padding_10_R, child: lProfileAvatar(reply.profile, radius: 15, fromCommunity: isComm)),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Row(children: [
                  lText(reply.writer, style: st_b_15(fontWeight: FontWeight.w700)),
                  sb_w_05,
                  if (reply.isWriter) lTextBtn('${"작성자".tr()}', padding: EdgeInsets.fromLTRB(5, 2, 5, 2), radius: 4, style: st_10(textColor: color_999999), border: Border.all(color: color_999999), bg: Colors.white),
                ]),
                Container(
                  width: width - 10,
                  padding: padding_04_TB,
                  child: reply.isDeleted ? lText("삭제된메세지입니다".tr(), style: st_15(textColor: color_545454), overflow: TextOverflow.clip) : lText(reply.message, style: st_b_15(), overflow: TextOverflow.clip),
                ),
                Row(
                  children: [
                    lText('${StringUtils.toReplyDate(reply.writtenTime)}', style: st_12(fontWeight: FontWeight.w500, textColor: color_b2b2b2)),
                    sb_w_05,
                    lDot(color: color_b2b2b2, size: 2),
                    lInkWell(
                      onTap: () => addAnswer!(reply.id),
                      child: lText("답글달기".tr(), style: st_12(fontWeight: FontWeight.w500, textColor: color_b2b2b2)),
                    ),
                  ],
                )
              ]),
            ],
          ),
          if (showMenu && reply.replyMenu!.isNotEmpty)
            lInkWell(
              onTap: () => showMenuModal(context, reply.replyMenu!),
              child: Lcons.more(color: color_cecece),
            )
        ],
      ),
    );
  }
}

class BoardPreview extends StatefulWidget {
  final String body;
  final BoardProfile? title;
  final List<String>? images;
  final List<String>? thumbnail;
  final String bodyTitle;
  final Widget? topper;
  final Widget? footer;
  final bool showMenu;
  final BoardLikeMenus? likes;
  final Function? onMenuClick;

  BoardPreview({this.images, this.bodyTitle = '', this.topper, this.footer, this.body = '', this.title, this.showMenu = true, this.likes, this.onMenuClick, this.thumbnail});

  @override
  State<BoardPreview> createState() => _BoardPreview();
}

class _BoardPreview extends State<BoardPreview> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding_20,
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee, width: 8))),
      child: Column(children: [
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          widget.title!,
          if (widget.showMenu)
            lInkWell(
              onTap: () {
                if (widget.onMenuClick != null) widget.onMenuClick!();
              },
              child: Padding(
                padding: padding_10_LB,
                child: Lcons.more(color: color_999999, size: 23),
              ),
            )
        ]),
        Padding(
          padding: padding_15_B,
          child: lDivider(color: color_eeeeee),
        ),
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          if (widget.topper != null) widget.topper!,
          sb_h_10,
          lText(widget.bodyTitle, style: st_b_18(fontWeight: FontWeight.w700), maxLines: 1, overflow: TextOverflow.ellipsis),
          sb_h_10,
          lText(widget.body, style: st_b_15(), maxLines: 2, overflow: TextOverflow.ellipsis),
          sb_h_20,
          if (widget.images != null && widget.images!.isNotEmpty)
            Container(
                width: double.infinity,
                height: 90,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: widget.thumbnail!
                      .map((e) => FittedBox(
                            fit: BoxFit.cover,
                            child: Container(
                              margin: padding_10_R,
                              width: 90,
                              height: 90,
                              child: lInkWell(
                                  onTap: () => Commons.nextPage(context, fn: () => FullPhotoPage(index: widget.thumbnail!.indexOf(e), urlList: widget.images)),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(8),
                                    child: e.isNotEmpty ? Hero(tag: e, child: CachedNetworkImage(imageUrl: e, fit: BoxFit.cover)) : Container(),
                                  )),
                            ),
                          ))
                      .toList(),
                )),
          if (widget.footer != null) widget.footer!,
          Padding(padding: padding_20_T, child: widget.likes),
        ]),
      ]),
    );
  }
}

class BoardLikeMenus extends StatefulWidget {
  final int like;
  final int reply;
  final bool isLike;
  final bool isBookMark;
  final Function? likeOnClick;
  final Function? bookmarkOnClick;
  final Function? onReplyClick;

  BoardLikeMenus({this.like = 0, this.reply = 0, this.isBookMark = false, this.isLike = false, this.likeOnClick, this.bookmarkOnClick, this.onReplyClick});

  @override
  _BoardLikeMenusState createState() => _BoardLikeMenusState();
}

class _BoardLikeMenusState extends State<BoardLikeMenus> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Row(
        children: [
          lIconText(
              icon: Lcons.heart(isEnabled: widget.isLike, color: color_f8859a, disableColor: color_c4c4c4, size: 25),
              title: widget.like.toString(),
              isExpanded: false,
              textStyle: st_17(textColor: color_b2b2b2, fontWeight: FontWeight.w500),
              onClick: () {
                setState(() {
                  if (widget.likeOnClick != null) widget.likeOnClick!();
                });
              }),
          sb_w_10,
          lIconText(
            icon: Lcons.chat(color: color_b2b2b2, size: 23),
            title: widget.reply.toString(),
            isExpanded: false,
            textStyle: st_17(textColor: color_b2b2b2, fontWeight: FontWeight.w500),
            onClick: widget.onReplyClick,
          ),
        ],
      ),
      lInkWell(
          onTap: () {
            setState(() {
              if (widget.bookmarkOnClick != null) widget.bookmarkOnClick!();
            });
          },
          child: Lcons.scrap(
            size: 25,
            isEnabled: widget.isBookMark,
            disableColor: color_b2b2b2,
            color: color_ffbb56,
          )),
    ]));
  }
}

class EventBanner extends StatelessWidget {
  final String title;
  String bannerUrl;
  final String fromTo;
  final Function? onClickAction;

  EventBanner({this.title = '', this.bannerUrl = '', this.fromTo = '', this.onClickAction});

  @override
  Widget build(BuildContext context) {
    return lInkWell(
        onTap: () => onClickAction!(),
        child: Container(
            margin: padding_08_B,
            child: Column(children: [
              Container(
                height: 160,
                child: CachedNetworkImage(
                  imageUrl: bannerUrl,
                  fit: BoxFit.cover,
                  imageBuilder: (ctx, imgProvider) => imageBuilder(imgProvider, width: 1000.0),
                  placeholder: (ctx, url) => placeholder(),
                  errorWidget: (ctx, url, error) {
                    bannerUrl = '';
                    return lText("이미지를표시할수없습니다".tr(), style: st_12(textColor: color_cecece));
                  },
                ),
              ),
              Container(
                  padding: padding_20,
                  decoration: BoxDecoration(color: Colors.white),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    lText(title, style: st_14(textColor: color_545454, fontWeight: FontWeight.w500)),
                    lText(fromTo, style: st_14(textColor: color_999999, fontWeight: FontWeight.w500)),
                  ]))
            ])));
  }
}

class ReplyListView extends StatelessWidget {
  const ReplyListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class BoardViewer extends StatefulWidget {
  final BoardProfile? profile;
  final Widget? title;
  final Widget? body;
  final Widget? footer, topper, dock;
  List<ReplyData>? reply;
  int replyLength;
  final bool showReply;
  final Function? saveReplyAction;
  final Function? onNextReply;
  final EdgeInsets? padding, replyPadding;
  final bool fromComm, toReply;
  final ScrollPhysics? physics;
  final Function? onNextReq, onRefresh;

  BoardViewer({
    this.profile,
    this.body,
    this.reply,
    this.footer,
    this.topper,
    this.replyLength = 0,
    this.saveReplyAction,
    this.showReply = false,
    this.title,
    this.padding,
    this.replyPadding,
    this.onNextReply,
    this.fromComm = false,
    this.onNextReq,
    this.onRefresh,
    this.dock,
    this.physics,
    this.toReply = false,
  }) {
    if (this.reply == null) this.reply = [];
  }

  @override
  BoardViewerState createState() => BoardViewerState();
}

class BoardViewerState<T extends BoardViewer> extends State<T> {
  late FocusNode _replyFocus;
  int _focusedId = 0;
  late TextEditingController _controller;
  ScrollController _scroll = ScrollController();
  ScrollController _topScroll = ScrollController();
  bool _toReply = false;
  GlobalKey _key = GlobalKey();

  //2022/06/13 WebView 처리 추가
  ScrollPhysics? physics;

  @override
  void initState() {
    super.initState();
    _toReply = widget.toReply;
    _controller = TextEditingController();
    _replyFocus = FocusNode();
    physics = widget.physics ?? AlwaysScrollableScrollPhysics(parent: ClampingScrollPhysics());
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (_toReply) {
        Future.delayed(Duration(milliseconds: 100)).then((value) {
          _toReply = false;
          Scrollable.ensureVisible(_key.currentContext!, duration: Duration(milliseconds: 100), curve: Curves.easeOutBack);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return RefreshView(
        onRefresh: widget.onRefresh ?? () => {},
        onNext: widget.onNextReq ?? () => {},
        child: Column(
          children: [
            Expanded(
              child: lScrollView(
                padding: padding_0,
                physics: physics,
                controller: _topScroll,
                child: Container(
                  color: Colors.white,
                  child: Column(
                    children: [
                      Padding(
                        padding: widget.padding ?? padding_20,
                        child: Column(children: [
                          if (widget.profile != null) widget.profile!,
                          if (widget.topper != null) widget.topper!,
                          if (widget.title != null) widget.title!,
                          if (widget.body != null) widget.body!,
                        ]),
                      ),
                      if (widget.showReply) lDivider(height: 8.0, thickness: 8, color: color_dedede.withAlpha(32)),
                      if (widget.showReply)
                        Padding(
                          padding: widget.padding ?? padding_20,
                          child: Column(
                            children: [
                              if (widget.footer != null) Container(key: _key, child: widget.footer!),
                              if (widget.showReply)
                                ReplyView(
                                    padding: widget.replyPadding ?? padding_0,
                                    isComm: widget.fromComm,
                                    reply: widget.reply!,
                                    replyLength: widget.replyLength,
                                    addAnswer: (parent) {
                                      _focusedId = parent;
                                      _replyFocus.requestFocus();
                                    }),
                            ],
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            ),
            if (widget.showReply)
              Container(
                child: Column(
                  children: [
                    if (widget.dock != null)
                      lInkWell(
                        onTap: () {
                          if (widget.footer != null) Scrollable.ensureVisible(_key.currentContext!, duration: Duration(milliseconds: 1000), curve: Curves.easeInOutCirc);
                        },
                        child: widget.dock!,
                      ),
                    Container(
                      padding: EdgeInsets.fromLTRB(15, 10, 15, 15),
                      color: color_f4f4f4,
                      alignment: Alignment.centerLeft,
                      child: ConstrainedBox(
                        constraints: BoxConstraints(maxHeight: kBottomNavigationBarHeight, minHeight: kBottomNavigationBarHeight),
                        child: TextField(
                          focusNode: _replyFocus,
                          textAlign: TextAlign.left,
                          textInputAction: TextInputAction.done,
                          controller: _controller,
                          maxLines: null,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.fromLTRB(15, 10, 5, 10),
                            isDense: true,
                            fillColor: Colors.white,
                            filled: true,
                            suffixIcon: TextButton(
                                onPressed: () async {
                                  focusClear(context);
                                  await widget.saveReplyAction!(_focusedId, _controller.text);
                                  if (_scroll.positions.isNotEmpty) _scroll.animateTo(_scroll.position.maxScrollExtent, duration: Duration(milliseconds: 500), curve: Curves.easeOutBack);
                                  _focusedId = 0;
                                  setState(() {
                                    _controller.text = "";
                                  });
                                },
                                child: lText("등록".tr(), style: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.w500))),
                            border: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(24)),
                            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(24)),
                            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(24)),
                            hintText: "댓글을남겨주세요".tr(),
                            hintStyle: st_14(textColor: color_cecece),
                          ),
                          onChanged: (value) {
                            setState(() {});
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
          ],
        ));
  }
}
