// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/style/color_style.dart';

class Lcons extends StatefulWidget {
  final String name;
  final double size;
  final Color? color;
  final Color? disableColor;
  bool isEnabled;

  Lcons(this.name, {this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : super();

  @override
  State<Lcons> createState() => _LconsState();

  static String _path = 'assets/icons/';

  static String _academic_bg_certification_active = '${_path}academic_bg_certification_active.svg';

  Lcons.academic_bg_certification({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _academic_bg_certification_active;

  static String _add = '${_path}add.svg';

  Lcons.add({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _add;

  static String _add_round = '${_path}add_round.svg';

  Lcons.add_round({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _add_round;

  static String _announcement = '${_path}announcement.svg';

  Lcons.announcement({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _announcement;

  static String _art = '${_path}art.svg';

  Lcons.art({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _art;

  static String _athletic = '${_path}athletic.svg';

  Lcons.athletic({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _athletic;

  static String _babybottle = '${_path}babybottle.svg';

  Lcons.babybottle({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _babybottle;

  static String _babydish = '${_path}babydish.svg';

  Lcons.babydish({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _babydish;

  static String _babyfood = '${_path}babyfood.svg';

  Lcons.babyfood({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _babyfood;

  static String _babylaundry = '${_path}babylaundry.svg';

  Lcons.babylaundry({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _babylaundry;

  static String _babyroom = '${_path}babyroom.svg';

  Lcons.babyroom({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _babyroom;

  static String _badge_active = '${_path}badge_active.svg';
  static String _badge_inactive = '${_path}badge_inactive.svg';

  Lcons.badge({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _badge_active : _badge_inactive;

  static String _badge_certification_active = '${_path}badge_certification_active.svg';

  Lcons.badge_certification({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _badge_certification_active;

  static String _bag = '${_path}bag.svg';

  Lcons.bag({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _bag;

  static String _bedroom = '${_path}bedroom.svg';

  Lcons.bedroom({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _bedroom;

  static String _bicycle = '${_path}bicycle.svg';

  Lcons.bicycle({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _bicycle;

  static String _book_guide = '${_path}book_guide.svg';

  Lcons.book_guide({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _book_guide;

  static String _bookmark_active = '${_path}bookmark_active.svg';
  static String _bookmark_inactive = '${_path}bookmark_inactive.svg';

  Lcons.bookmark({this.size = 20.0, this.color = color_ffbb56, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _bookmark_active : _bookmark_inactive;

  static String _care_completed = '${_path}care_completed.svg';

  Lcons.care_completed({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _care_completed;

  static String _care_edit = '${_path}care_edit.svg';

  Lcons.care_edit({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _care_edit;

  static String _care = '${_path}care_active.svg';

  Lcons.care({this.size = 20.0, this.color = color_cecece, this.disableColor = color_cecece, this.isEnabled = false}) : this.name = _care;

  static String _care_diary = '${_path}care_diary.svg';
  static String _linkmom_care_diary = '${_path}linkmom_care_diary.svg';

  Lcons.care_diary({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _linkmom_care_diary : _care_diary;

  static String _career_certification_active = '${_path}career_certification_active.svg';

  Lcons.career_certification_active({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _career_certification_active;

  static String _certified_center = '${_path}certified_center.svg';

  Lcons.certified_center({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _certified_center;

  static String _change = '${_path}change.svg';

  Lcons.change({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _change;

  static String _chat = '${_path}chat.svg';

  Lcons.chat({this.size = 20.0, this.color = color_222222, this.disableColor, this.isEnabled = true}) : this.name = _chat;

  static String _check_box_active = '${_path}check_box_active.svg';
  static String _check_box_inactive = '${_path}check_box_inactive.svg';

  Lcons.checkbox({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _check_box_active : _check_box_inactive;

  static String _check_circle_outline = '${_path}check_circle_outline.svg';

  Lcons.check_circle_outline({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _check_circle_outline;

  static String _check_circle = '${_path}check_circle.svg';

  Lcons.check_circle({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _check_circle;

  static String _check = '${_path}check.svg';

  Lcons.check({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _check;

  static String _child_care = '${_path}child_care.svg';

  Lcons.child_care({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _child_care;

  static String _cinactive_are = '${_path}cinactive_are.svg';

  Lcons.cinactive_are({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _cinactive_are;

  static String _clay = '${_path}clay.svg';

  Lcons.clay({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _clay;

  static String _commuting_to_work = '${_path}commuting_to_work.svg';

  Lcons.commuting_to_work({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _commuting_to_work;

  static String _complete = '${_path}complete.svg';

  Lcons.complete({this.size = 20.0, this.color = color_545454, this.disableColor, this.isEnabled = true}) : this.name = _complete;

  static String _completed = '${_path}completed.svg';

  Lcons.completed({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _completed;

  static String _contact = '${_path}contact.svg';

  Lcons.contact({this.size = 27.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _contact;

  static String _copy_certification_active = '${_path}copy_certification_active.svg';

  Lcons.copy_certification_active({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _copy_certification_active;

  static String _copy = '${_path}copy.svg';

  Lcons.copy({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _copy;

  static String _covid_self_test = '${_path}covid_self_test.svg';

  Lcons.covid_self_test({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _covid_self_test;

  static String _create = '${_path}create.svg';

  Lcons.create({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _create;

  static String _criminal_certification_active = '${_path}criminal_certification_active.svg';

  Lcons.criminal_certification_active({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _criminal_certification_active;

  static String _diaper = '${_path}diaper.svg';

  Lcons.diaper({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _diaper;

  static String _diary_like_active = '${_path}diary_like_active.svg';
  static String _diary_like_inactive = '${_path}diary_like_inactive.svg';

  Lcons.diary_like({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _diary_like_active : _diary_like_inactive;

  static String _dishes = '${_path}dishes.svg';

  Lcons.dishes({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _dishes;

  static String _dry = '${_path}dry.svg';

  Lcons.dry({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _dry;

  static String _dryer = '${_path}dryer.svg';

  Lcons.dryer({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _dryer;

  static String _dung = '${_path}dung.svg';

  Lcons.dung({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _dung;

  static String _edit_house = '${_path}edit_house.svg';

  Lcons.edit_house({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _edit_house;

  static String _edit = '${_path}edit.svg';

  Lcons.edit({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _edit;

  static String _education_certification_active = '${_path}education_certification_active.svg';

  Lcons.education_certification_active({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _education_certification_active;

  static String _error_outline = '${_path}error_outline.svg';

  Lcons.error_outline({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _error_outline;

  static String _error = '${_path}error.svg';

  Lcons.error({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _error;

  static String _fail = '${_path}fail.svg';

  Lcons.fail({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _fail;

  static String _family_environment_active = '${_path}family_environment_active.svg';

  Lcons.family_environment_active({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _family_environment_active;

  static String _favorite = '${_path}favorite.svg';

  Lcons.favorite({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _favorite;

  static String _filter = '${_path}filter.svg';

  Lcons.filter({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _filter;

  static String _go_home_active = '${_path}go_home_active.svg';
  static String _go_home_inactive = '${_path}go_home_inactive.svg';

  Lcons.go_home({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _go_home_active : _go_home_inactive;

  static String _go_school_active = '${_path}go_school_active.svg';
  static String _go_school_inactive = '${_path}go_school_inactive.svg';

  Lcons.go_school({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _go_school_active : _go_school_inactive;

  static String _guide = '${_path}guide.svg';

  Lcons.guide({this.size = 27.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _guide;

  static String _hand = '${_path}hand.svg';

  Lcons.hand({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _hand;

  static String _health_certification_active = '${_path}health_certification_active.svg';

  Lcons.health_certification_active({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _health_certification_active;

  static String _home_linkmom = '${_path}home_linkmom.svg';
  static String _home_momdaddy = '${_path}home_momdaddy.svg';

  Lcons.mypage({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _home_linkmom : _home_momdaddy;

  static String _home = '${_path}home.svg';

  Lcons.home({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _home;

  static String _homework = '${_path}homework.svg';

  Lcons.homework({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _homework;

  static String _housework = '${_path}housework.svg';

  Lcons.housework({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _housework;

  static String _indeterminate_check_box_active = '${_path}indeterminate_check_box_active.svg';

  Lcons.indeterminate_check_box_active({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _indeterminate_check_box_active;

  static String _laundry = '${_path}laundry.svg';

  Lcons.laundry({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _laundry;

  static String _linkmom_like_list = '${_path}linkmom_like_list.svg';
  static String _momdaddy_like_list = '${_path}momdaddy_like_list.svg';

  Lcons.like_list({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _linkmom_like_list : _momdaddy_like_list;

  static String _like = '${_path}like.svg';

  Lcons.like({this.size = 20.0, this.color = color_b2b2b2, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _heart : _like;

  static String _link = '${_path}link.svg';

  Lcons.link({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _link;

  static String _living = '${_path}living.svg';

  Lcons.living({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _living;

  static String _local_certification_active = '${_path}local_certification_active.svg';

  Lcons.local_certification_active({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _local_certification_active;

  static String _lotion = '${_path}lotion.svg';

  Lcons.lotion({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _lotion;

  static String _made = '${_path}made.svg';

  Lcons.made({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _made;

  static String _matching_success = '${_path}matching_success.svg';

  Lcons.matching_success({this.size = 26.0, this.color = Colors.white, this.disableColor, this.isEnabled = false}) : this.name = _matching_success;

  static String _meal = '${_path}meal.svg';

  Lcons.meal({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _meal;

  static String _medication_form = '${_path}medication_form.svg';

  Lcons.medication_form({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _medication_form;

  static String _medication_form02 = '${_path}medication_form02.svg';

  Lcons.medication_form02({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _medication_form02;

  static String _milk = '${_path}milk.svg';

  Lcons.milk({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _milk;

  static String _my_child = '${_path}my_child.svg';

  Lcons.my_child({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _my_child;

  static String _my_house_1_active = '${_path}my_house_1_active.svg';

  Lcons.my_house_1_active({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _my_house_1_active;

  static String _my_house_active = '${_path}my_house_active.svg';
  static String _my_house_inactive = '${_path}my_house_inactive.svg';

  Lcons.my_house({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _my_house_active : _my_house_inactive;

  static String _nav_care_schedule = '${_path}nav_care_schedule.svg';

  Lcons.nav_care_schedule({this.size = 25.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _nav_care_schedule;

  static String _nav_chat = '${_path}nav_chat.svg';

  Lcons.nav_chat({this.size = 25.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _nav_chat;

  static String _nav_community = '${_path}nav_community.svg';

  Lcons.nav_community({this.size = 25.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _nav_community;

  static String _nav_home = '${_path}nav_home.svg';

  Lcons.nav_home({this.size = 25.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _nav_home;

  static String _nav_linkmom_search = '${_path}nav_linkmom_search.svg';

  Lcons.nav_linkmom_search({this.size = 25.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _nav_linkmom_search;

  static String _neighbor_house_1 = '${_path}neighbor_house_1.svg';

  Lcons.neighbor_house_1({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _neighbor_house_1;

  static String _neighbor_house_active = '${_path}neighbor_house_active.svg';
  static String _neighbor_house_inactive = '${_path}neighbor_house_inactive.svg';

  Lcons.neighbor_house({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _neighbor_house_active : _neighbor_house_inactive;

  static String _noti_care = '${_path}noti_care.svg';

  Lcons.noti_care({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _noti_care;

  static String _noti_community = '${_path}noti_community.svg';

  Lcons.noti_community({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _noti_community;

  static String _noti_cs = '${_path}noti_cs.svg';

  Lcons.noti_cs({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _noti_cs;

  static String _noti_event = '${_path}noti_event.svg';

  Lcons.noti_event({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _noti_event;

  static String _noti_mypage = '${_path}noti_mypage.svg';

  Lcons.noti_mypage({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _noti_mypage;

  static String _notification_important = '${_path}notification_important.svg';

  Lcons.notification_important({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _notification_important;

  static String _notification = '${_path}notification.svg';

  Lcons.notification({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _notification;

  static String _notifications_active = '${_path}notifications_active.svg';
  static String _notifications_inactive = '${_path}notifications_inactive.svg';

  Lcons.notifications({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _notifications_active : _notifications_inactive;

  static String _online_class = '${_path}online_class.svg';

  Lcons.online_class({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _online_class;

  static String _origami = '${_path}origami.svg';

  Lcons.origami({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _origami;

  static String _page_arrow = '${_path}page_arrow.svg';

  Lcons.page_arrow({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _page_arrow;

  static String _payment_completed = '${_path}payment_completed.svg';

  Lcons.payment_completed({this.size = 26.0, this.color = Colors.white, this.disableColor, this.isEnabled = false}) : this.name = _payment_completed;

  static String _payment_details = '${_path}momdaddy_payment_details.svg';
  static String _linkmom_payment_details = '${_path}linkmom_payment_details.svg';

  Lcons.payment_details({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _linkmom_payment_details : _payment_details;

  static String _person = '${_path}person.svg';

  Lcons.person({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _person;

  static String _personality_test = '${_path}personality_test.svg';

  Lcons.personality_test({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _personality_test;

  static String _play = '${_path}play.svg';

  Lcons.play({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _play;

  static String _playground = '${_path}playground.svg';

  Lcons.playground({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _playground;

  static String _porch = '${_path}porch.svg';

  Lcons.porch({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _porch;

  static String _qna = '${_path}qna.svg';

  Lcons.qna({this.size = 27.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _qna;

  static String _quickboard = '${_path}quickboard.svg';

  Lcons.quickboard({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _quickboard;

  static String _radio_button_active = '${_path}radio_button_active.svg';
  static String _radio_button_inactive = '${_path}radio_button_inactive.svg';

  Lcons.radio_button({this.size = 20.0, this.color, this.disableColor = color_eeeeee, this.isEnabled = false}) : this.name = isEnabled ? _radio_button_active : _radio_button_inactive;

  static String _rank_1 = '${_path}rank_1.svg';

  Lcons.rank_1({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _rank_1;

  static String _rank_2 = '${_path}rank_2.svg';

  Lcons.rank_2({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _rank_2;

  static String _rank_3 = '${_path}rank_3.svg';

  Lcons.rank_3({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _rank_3;

  static String _reading = '${_path}reading.svg';

  Lcons.reading({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _reading;

  static String _rectangle_582 = '${_path}rectangle_582.svg';

  Lcons.rectangle_582({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _rectangle_582;

  static String _review_management = '${_path}review_management.svg';
  static String _linkmom_review_management = '${_path}linkmom_review_management.svg';

  Lcons.review_management({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _linkmom_review_management : _review_management;

  static String _search = '${_path}search.svg';

  Lcons.search({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _search;

  static String _self_certification_active = '${_path}self_certification_active.svg';

  Lcons.self_certification_active({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _self_certification_active;

  static String _setting = '${_path}setting.svg';

  Lcons.setting({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _setting;

  static String _settings = '${_path}settings.svg';

  Lcons.settings({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _settings;

  static String _share = '${_path}share.svg';

  Lcons.share({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _share;

  static String _shower = '${_path}shower.svg';

  Lcons.shower({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _shower;

  static String _snack = '${_path}snack.svg';

  Lcons.snack({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _snack;

  static String _star_active = '${_path}star_active.svg';
  static String _star_inactive = '${_path}star_inactive.svg';

  Lcons.star({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _star_active : _star_inactive;

  static String _stree_test = '${_path}stress_test.svg';

  Lcons.stress_test({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _stree_test;

  static String _supplies = '${_path}supplies.svg';

  Lcons.supplies({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _supplies;

  static String _table = '${_path}table.svg';

  Lcons.table({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _table;

  static String _taking_care = '${_path}taking_care.svg';

  Lcons.taking_care({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _taking_care;

  static String _visibility = '${_path}visibility.svg';
  static String _visibility_off = '${_path}visibility_off.svg';

  Lcons.visibility({this.size = 24.0, this.color = color_cecece, this.disableColor = color_cecece, this.isEnabled = true}) : this.name = isEnabled ? _visibility : _visibility_off;

  static String _waiting_match = '${_path}waiting_match.svg';

  Lcons.waiting_match({this.size = 26.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _waiting_match;

  static String _waiting_payment = '${_path}waiting_payment.svg';

  Lcons.waiting_payment({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _waiting_payment;

  static String _wash = '${_path}wash.svg';

  Lcons.wash({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _wash;

  static String _cash = '${_path}cash.svg';
  static String _cash_inactive = '${_path}cash_inactive.svg';

  Lcons.cash({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _cash : _cash_inactive;

  static String _point = '${_path}point.svg';

  Lcons.point({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _point;

  static String _total_income = '${_path}total_income.svg';

  Lcons.total_income({this.size = 20.0, this.color = color_222222, this.disableColor, this.isEnabled = false}) : this.name = _total_income;

  static String _total_time = '${_path}total_time.svg';

  Lcons.total_time({this.size = 20.0, this.color = color_222222, this.disableColor, this.isEnabled = false}) : this.name = _total_time;

  static String _review = '${_path}review.svg';

  Lcons.review({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _review;

  static String _heart = '${_path}heart.svg';
  static String _heart_inactive = '${_path}heart_inactive.svg';

  Lcons.heart({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = isEnabled ? _heart : _heart_inactive;

  static String _warning_sticker = '${_path}warning_sticker.svg';

  Lcons.warning_sticker({this.size = 20.0, this.color = Colors.white, this.disableColor, this.isEnabled = false}) : this.name = _warning_sticker;

  static String _stress_low = '${_path}stress_low.svg';

  Lcons.stress_low({this.size = 52.0, this.color = color_9edd6d, this.disableColor, this.isEnabled = true}) : this.name = _stress_low;

  static String _stress_mid = '${_path}stress_mid.svg';

  Lcons.stress_mid({this.size = 52.0, this.color = color_faa139, this.disableColor, this.isEnabled = true}) : this.name = _stress_mid;

  static String _stress_high = '${_path}stress_high.svg';

  Lcons.stress_high({this.size = 52.0, this.color = color_ff3b30, this.disableColor, this.isEnabled = true}) : this.name = _stress_high;

  static String _boy = '${_path}boy.svg';

  Lcons.boy({this.size = 40.0, this.color = color_dedede, this.disableColor, this.isEnabled = true}) : this.name = _boy;

  static String _girl = '${_path}girl.svg';

  Lcons.girl({this.size = 40.0, this.color = color_dedede, this.disableColor, this.isEnabled = true}) : this.name = _girl;

  static String _nursery = '${_path}nursery.svg';

  Lcons.nursery({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _nursery;

  static String _auth_bg_active = '${_path}auth_bg_active.svg';
  static String _auth_bg_inactive = '${_path}auth_bg_inactive.svg';

  Lcons.auth_bg({this.size = 80.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _auth_bg_active : _auth_bg_inactive;

  static String _care_ready = '${_path}care_ready.svg';

  Lcons.care_ready({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _care_ready;

  static String _won = '${_path}won.svg';

  Lcons.won({this.size = 20.0, this.color = color_linkmom, this.disableColor = color_main, this.isEnabled = false}) : this.name = _won;

  static String _location = '${_path}location.svg';

  Lcons.location({this.size = 20.0, this.color = color_545454, this.disableColor = color_999999, this.isEnabled = true}) : this.name = _location;

  static String _review_cnt_bg = '${_path}review_cnt_bg.svg';

  Lcons.review_cnt_bg({this.size = 40.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _review_cnt_bg;

  static String _nav_close = '${_path}nav_close.svg';

  Lcons.nav_close({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _nav_close;

  static String _nav_left = '${_path}nav_left.svg';

  Lcons.nav_left({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _nav_left;

  static String _info = '${_path}info.svg';

  Lcons.info({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _info;

  static String _info_t = '${_path}info_t.svg';

  Lcons.info_t({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _info_t;

  static String _auth_bg_momdady = '${_path}auth_bg_momdady.svg';

  Lcons.auth_count({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _auth_bg_active : _auth_bg_momdady;

  static String _clear = '${_path}clear.svg';

  Lcons.clear({this.size = 24.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _clear;

  static String _more = '${_path}more.svg';

  Lcons.more({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _more;

  static String _survey_require = '${_path}survey_require.svg';

  Lcons.survey_require({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _survey_require;

  static String _survey_sub_1 = '${_path}survey_sub_1.svg';

  Lcons.survey_sub_1({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _survey_sub_1;

  static String _survey_sub_2 = '${_path}survey_sub_2.svg';

  Lcons.survey_sub_2({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _survey_sub_2;

  static String _survey_sub_3 = '${_path}survey_sub_3.svg';

  Lcons.survey_sub_3({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _survey_sub_3;

  static String _camera = '${_path}camera.svg';

  Lcons.camera({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _camera;

  static String _fill_arrow = '${_path}fill_arrow.svg';

  Lcons.fill_arrow({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _fill_arrow;

  static String _send = '${_path}send.svg';

  Lcons.send({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _send;

  static String _chat_bubble_center = '${_path}chat_bubble_center.svg';

  Lcons.chat_bubble_center({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _chat_bubble_center;

  static String _open_checkbox = '${_path}open_checkbox.svg';

  Lcons.open_checkbox({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _open_checkbox;

  static String _rereply = '${_path}rereply.svg';

  Lcons.rereply({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _rereply;

  static String _calendar = '${_path}calendar.svg';

  Lcons.calendar({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _calendar;

  static String _radio_button_checked = '${_path}radio_button_checked.svg';
  static String _radio_button_unchecked = '${_path}radio_button_unchecked.svg';

  Lcons.radio({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _radio_button_checked : _radio_button_unchecked;

  static String _slash = '${_path}slash.svg';

  Lcons.slash({this.size = 7.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _slash;

  static String _icon_review_linkmom = '${_path}icon_review_linkmom.svg';
  static String _icon_review_momdaddy = '${_path}icon_review_momdaddy.svg';

  Lcons.review_info({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _icon_review_linkmom : _icon_review_momdaddy;

  static String _diary_like_linkmom = '${_path}diary_like_linkmom.svg';
  static String _diary_like_momdaddy = '${_path}diary_like_momdaddy.svg';

  Lcons.diary_like_type({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _diary_like_linkmom : _diary_like_momdaddy;

  static String _top = '${_path}top.svg';

  Lcons.top({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _top;

  static String _down = '${_path}down.svg';

  Lcons.down({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _down;

  static String _loading_person = '${_path}loading_person.svg';

  Lcons.loading_person({this.size = 45.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _loading_person;

  static String _loading_round = '${_path}loading_round.svg';

  Lcons.loading_round({this.size = 66.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _loading_round;

  static String _loading_bar = '${_path}loading_bar.svg';

  Lcons.loading_bar({this.size = 66.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _loading_bar;

  static String _empty = '${_path}empty.svg';

  Lcons.empty({this.size = 100.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _empty;

  static String _nav_bottom = '${_path}nav_bottom.svg';

  Lcons.nav_bottom({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _nav_bottom;

  static String _nav_right = '${_path}nav_right.svg';

  Lcons.nav_right({this.size = 20.0, this.color, this.disableColor, this.isEnabled = true}) : this.name = _nav_right;

  static String _upload = '${_path}upload.svg';

  Lcons.upload({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _upload;

  static String _nav_top = '${_path}nav_top.svg';

  Lcons.nav_top({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _nav_top;

  static String _bottom_more_arrow = '${_path}bottom_more_arrow.svg';

  Lcons.bottom_more_arrow({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _bottom_more_arrow;

  static String _icon_arrow_down = '${_path}icon_arrow_down.svg';

  Lcons.icon_arrow_down({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _icon_arrow_down;

  static String _handsign = '${_path}handsign.svg';

  Lcons.handsign({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _handsign;

  static String _tutorial_arrow = '${_path}tutorial_arrow.svg';

  Lcons.tutorial_arrow({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _tutorial_arrow;

  static String _my_location = '${_path}my_location.svg';

  Lcons.my_location({this.size = 22.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _my_location;

  static String _auth_self_bg_inactive = '${_path}auth_self_bg_inactive.svg';
  static String _auth_self_bg_active = '${_path}auth_self_bg_active.svg';

  Lcons.auth_self_bg({this.size = 80.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _auth_self_bg_active : _auth_self_bg_inactive;

  static String _polygon = '${_path}polygon.svg';

  Lcons.polygon({this.size = 80.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _polygon;

  static String _scrap_active = '${_path}scrap_active.svg';
  static String _scrap_inactive = '${_path}scrap_inactive.svg';

  Lcons.scrap({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = isEnabled ? _scrap_active : _scrap_inactive;

  static String _menu = '${_path}menu.svg';

  Lcons.menu({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _menu;

  static String _edit_not_available = '${_path}edit_not_available.svg';

  Lcons.edit_not_available({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _edit_not_available;

  static String _drug = '${_path}drug.svg';

  Lcons.drug({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _drug;

  static String _mode_mypage = '${_path}mode_mypage.svg';

  Lcons.mode_mypage({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _mode_mypage;

  static String _home_v = '${_path}home_v.svg';

  Lcons.home_v({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _home_v;

  static String _affiliate_ipark = '${_path}affiliate_ipark.svg';

  Lcons.affiliate_ipark({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _affiliate_ipark;

  static String _bg_white_active = '${_path}bg_white_active.svg';

  Lcons.check_bg_white_active({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _bg_white_active;

  static String _no_data_chat = '${_path}no_data_chat.svg';

  Lcons.no_data_chat({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _no_data_chat;

  static String _no_data_community = '${_path}no_data_community.svg';

  Lcons.no_data_community({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _no_data_community;

  static String _no_data_default = '${_path}no_data_default.svg';

  Lcons.no_data_default({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _no_data_default;

  static String _no_data_event = '${_path}no_data_event.svg';

  Lcons.no_data_event({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _no_data_event;

  static String _no_data_linkssam = '${_path}no_data_linkssam.svg';

  Lcons.no_data_linkssam({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _no_data_linkssam;

  static String _no_data_momdaddy = '${_path}no_data_momdaddy.svg';

  Lcons.no_data_momdaddy({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _no_data_momdaddy;

  static String _no_data_ranking = '${_path}no_data_ranking.svg';

  Lcons.no_data_ranking({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _no_data_ranking;

  static String _key_down = '${_path}key_down.svg';

  Lcons.key_down({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _key_down;

  static String _photo_add = '${_path}photo_add.svg';

  Lcons.photo_add({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _photo_add;

  static String _delete = '${_path}delete.svg';
  
  Lcons.delete({this.size = 20.0, this.color, this.disableColor, this.isEnabled = false}) : this.name = _delete;

  /*----------------------- UTILS -----------------------*/

  static Widget serviceType(ServiceType type, {Color color = Colors.white, Color disableColor = color_dedede, bool isEnabled = false, double size = 20.0}) {
    switch (type) {
      case ServiceType.serviceType_0:
        return Lcons.go_home(color: color, disableColor: disableColor, isEnabled: isEnabled, size: size);
      case ServiceType.serviceType_1:
        return Lcons.go_school(color: color, disableColor: disableColor, isEnabled: isEnabled, size: size);
      case ServiceType.serviceType_2:
        return Lcons.care(color: color, disableColor: disableColor, isEnabled: isEnabled, size: size);
      case ServiceType.serviceType_3:
      case ServiceType.serviceType_4:
      case ServiceType.serviceType_5:
      default:
        return Lcons.care(color: color, disableColor: disableColor, isEnabled: isEnabled, size: size);
    }
  }

  static Widget getNotiIcons(NotificationType type) {
    switch (type) {
      case NotificationType.care:
      case NotificationType.jobs:
      case NotificationType.diary:
      case NotificationType.chatCareManage:
        return Lcons.noti_care(color: Colors.white, isEnabled: true);
      case NotificationType.communityBoard:
      case NotificationType.community:
        return Lcons.noti_community(color: Colors.white, isEnabled: true);
      case NotificationType.mypage:
      case NotificationType.mypagePenalty:
      case NotificationType.mypageReview:
      case NotificationType.mypageAuthcenter:
      case NotificationType.mypageExpiredPoint:
      case NotificationType.pay:
        return Lcons.noti_mypage(color: Colors.white, isEnabled: true);
      case NotificationType.event:
        return Lcons.noti_event(color: Colors.white, isEnabled: true);
      case NotificationType.cs:
      case NotificationType.csNotice:
      case NotificationType.csContact:
        return Lcons.noti_cs(color: Colors.white, isEnabled: true);
      default:
        return Lcons.noti_care(color: Colors.white, isEnabled: true);
    }
  }

  static Lcons getServiceIcon(int service, int type, {double size = 20}) {
    switch (service) {
      case 21:
        {
          switch (type) {
            case 5:

              /// 양치/세수
              return Lcons.wash(size: size);
            case 8:

              /// 드라이기
              return Lcons.dryer(size: size);
            case 10:

              /// 환복
              return Lcons.change(size: size);
            case 43:

              /// 기저귀 교체
              return Lcons.diaper(size: size);
            case 44:

              /// 손/발 씻기
              return Lcons.hand(size: size);
            case 48:

              /// 샤워/목욕
              return Lcons.shower(size: size);
            case 49:

              /// 배변 씻기기
              return Lcons.dung(size: size);
            case 50:

              /// 로션
              return Lcons.lotion(size: size);
            default:
              return Lcons.wash(size: size);
          }
        }
      case 22:
        {
          switch (type) {
            case 11:

              /// 식사
              return Lcons.meal(size: size);
            case 14:

              /// 이유식
              return Lcons.babyfood(size: size);
            case 15:

              /// 분유
              return Lcons.milk(size: size);
            case 51:

              /// 간식
              return Lcons.snack(size: size);
            default:
              return Lcons.meal(size: size);
          }
        }
      case 23:
        {
          switch (type) {
            case 9:

              /// 책가방 확인
              return Lcons.bag(size: size);
            case 24:

              /// 알림장 확인
              return Lcons.announcement(size: size);
            case 25:

              /// 준비물 확인
              return Lcons.supplies(size: size);
            default:
              return Lcons.bag(size: size);
          }
        }
      case 31:
        {
          switch (type) {
            case 26:

              /// 찰흙놀이
              return Lcons.clay(size: size);
            case 28:

              /// 종이접기/
              return Lcons.origami(size: size);
            case 29:

              /// 미술놀이/
              return Lcons.art(size: size);
            case 45:

              /// 만들기/
              return Lcons.made(size: size);
            default:
              return Lcons.clay(size: size);
          }
        }
      case 32:
        {
          switch (type) {
            case 31:

              /// 놀이터/산책/
              return Lcons.playground(size: size);
            case 32:

              /// 퀵보드/
              return Lcons.quickboard(size: size);
            case 33:

              /// 자전거/
              return Lcons.bicycle(size: size);
            case 58:

              /// 체육놀이/
              return Lcons.athletic(size: size);
            default:
              return Lcons.playground(size: size);
          }
        }
      case 33:
        {
          switch (type) {
            case 35:

              /// 온라인수업/
              return Lcons.online_class(size: size);
            case 37:

              /// 숙제검사/
              return Lcons.homework(size: size);
            case 38:

              /// 독서지도/
              return Lcons.book_guide(size: size);
            case 39:

              /// 책읽기/
              return Lcons.reading(size: size);
            default:
              return Lcons.online_class(size: size);
          }
        }
      case 41:
        {
          switch (type) {
            case 20:

              /// 아이방 청소/
              return Lcons.babyroom(size: size);
            case 21:

              /// 안방 청소/
              return Lcons.bedroom(size: size);
            case 22:

              /// 거실 청소/
              return Lcons.living(size: size);
            case 53:

              /// 현관 청소/
              return Lcons.porch(size: size);
            default:
              return Lcons.babyroom(size: size);
          }
        }
      case 42:
        {
          switch (type) {
            case 17:

              /// 아이식기 세척/
              return Lcons.babydish(size: size);
            case 18:

              /// 모든 설거지/
              return Lcons.dishes(size: size);
            case 19:

              /// 젖병세척/
              return Lcons.babybottle(size: size);
            case 65:

              /// 식탁정리/
              return Lcons.table(size: size);
            default:
              return Lcons.babydish(size: size);
          }
        }
      case 43:
        {
          switch (type) {
            case 56:

              /// 아이빨래/
              return Lcons.babylaundry(size: size);
            case 60:

              /// 빨래건조/
              return Lcons.dry(size: size);
            case 62:

              /// 빨래정리/
              return Lcons.laundry(size: size);
            default:
              return Lcons.babylaundry(size: size);
          }
        }
      default:
        return Lcons.wash(size: size);
    }
  }

  static Lcons getGender(int type, {Color? color, double size = 20, bool isEnabled = true}) {
    if (type == 1) {
      return Lcons.boy(color: color, size: size, isEnabled: isEnabled);
    } else {
      return Lcons.girl(color: color, size: size, isEnabled: isEnabled);
    }
  }

  static Lcons getAuthType(int type, {Color color = color_b579c8, double size = 40, bool isEnabled = false}) {
    switch (type) {
      case 0:
        return Lcons.self_certification_active(color: Colors.white, disableColor: color, size: size, isEnabled: isEnabled);
      case 1:
        return Lcons.local_certification_active(color: Colors.white, disableColor: color, size: size, isEnabled: isEnabled);
      case 2:
        return Lcons.family_environment_active(color: Colors.white, disableColor: color, size: size, isEnabled: isEnabled);
      case 3:
        return Lcons.criminal_certification_active(color: Colors.white, disableColor: color, size: size, isEnabled: isEnabled);
      case 4:
        return Lcons.health_certification_active(color: Colors.white, disableColor: color, size: size, isEnabled: isEnabled);
      case 5:
        return Lcons.copy_certification_active(color: Colors.white, disableColor: color, size: size, isEnabled: isEnabled);
      case 6:
        return Lcons.career_certification_active(color: Colors.white, disableColor: color, size: size, isEnabled: isEnabled);
      case 7:
        return Lcons.academic_bg_certification(color: Colors.white, disableColor: color, size: size, isEnabled: isEnabled);
      case 8:
        return Lcons.education_certification_active(color: Colors.white, disableColor: color, size: size, isEnabled: isEnabled);
      case 9:
        return Lcons.covid_self_test(color: Colors.white, disableColor: color, size: size, isEnabled: isEnabled);
      case 10:
        return Lcons.stress_test(color: Colors.white, disableColor: color, size: size, isEnabled: isEnabled);
      default:
        return Lcons.self_certification_active(color: Colors.white, disableColor: color, size: size, isEnabled: isEnabled);
    }
  }

  static Lcons getSubSurvey(int index, {Color color = color_b579c8, double size = 30}) {
    if (index == 0 || index % 3 == 0) {
      return Lcons.survey_sub_1(color: color, size: size, isEnabled: true);
    } else if (index == 1 || index % 4 == 0) {
      return Lcons.survey_sub_2(color: color, size: size, isEnabled: true);
    } else {
      return Lcons.survey_sub_3(color: color, size: size, isEnabled: true);
    }
  }

  static Lcons upDown(bool isEnabled) {
    return isEnabled ? Lcons.nav_top() : Lcons.nav_bottom();
  }

  static Widget polygonBg(bool isLinkmom, {double size = 60}) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
            decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [
              BoxShadow(
                blurRadius: 10,
                offset: Offset(0, 2),
                color: color_727272.withOpacity(0.42),
                spreadRadius: -10,
              )
            ]),
            child: Lcons.polygon(
              disableColor: Colors.white,
              size: size + 10,
            )),
        Container(
            decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [
              BoxShadow(
                blurRadius: 10,
                offset: Offset(0, 2),
                color: color_727272.withOpacity(0.42),
                spreadRadius: -10,
              )
            ]),
            child: Lcons.polygon(
              color: color_linkmom,
              disableColor: color_main,
              size: size,
              isEnabled: isLinkmom,
            )),
      ],
    );
  }
}

class _LconsState extends State<Lcons> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.size,
      child: SvgPicture.asset(widget.name, width: widget.size, height: widget.size, color: widget.isEnabled ? widget.color : widget.disableColor),
    );
  }
}
