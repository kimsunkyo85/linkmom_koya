import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/manager.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:timer_builder/timer_builder.dart';

import '../main.dart';

bool _isShowing(bool state, String pw1, String pw2, Function onUpDate, ViewFlag flag) {
  if (!state && pw1.isNotEmpty) {
    return _passwordConfirmCheck(pw1, pw2, onUpDate, flag);
  }
  return false;
}

bool _passwordConfirmCheck(String pw1, String pw2, Function onUpDate, ViewFlag flag) {
  if (pw1 == pw2) {
    flag.enablePasswordConfirm(fn: () => onUpDate());
    return true;
  } else {
    flag.disablePasswordConfirm(fn: () => onUpDate());
    return false;
  }
}

String? _validatorMobile(String? value) {
  if (StringUtils.validateString(value)) {
    // return data.registerItem.registerErrors.hpnumber;
  }
  return StringUtils.validateMobile(value);
}

bool _styleCheck(String value) {
  return (value == "직접입력".tr() || value == "이메일선택".tr());
}

String? _validateAuth(String? value) {
  if ((value ?? '').length == 6) {
    return null;
  } else {
    return "에러_인증번호확인".tr();
  }
}

bool isMobileConfirm(DataManager data) {
  return data.registerItem.mobileConfirm = StringUtils.validateMobile(data.tcMobile.text) == null ? true : false;
}

bool isNameValidate(DataManager data) {
  return data.registerItem.nameConfirm = StringUtils.validateName(data.tcName.text) == null ? true : false;
}

class EmailAddressSelecter extends StatefulWidget {
  final DataManager data;
  final ViewFlag flag;
  final String? message;
  final bool isValidMail;
  final Function onChanged;
  final Function onEditingComplete;

  EmailAddressSelecter({
    required this.data,
    this.message,
    this.isValidMail = false,
    required this.onChanged,
    required this.onEditingComplete,
    required this.flag,
  });

  @override
  _EmailAddressSelecterState createState() => _EmailAddressSelecterState();
}

class _EmailAddressSelecterState extends State<EmailAddressSelecter> {
  FocusNode _focusEmailID = FocusNode();
  String? _emailValidMsg = '';

  @override
  Widget build(BuildContext context) {
    return Focus(
        focusNode: _focusEmailID,
        onFocusChange: (f) => setState(() => widget.onChanged(_getId())),
        child: Form(
          key: widget.data.formId,
          child: Container(
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Expanded(
                flex: 3,
                child: TextFormField(
                  controller: widget.data.tcId,
                  style: stLogin,
                  cursorColor: color_00cfb5,
                  textInputAction: TextInputAction.done,
                  keyboardType: TextInputType.emailAddress,
                  autofillHints: [AutofillHints.email],
                  decoration: underLineDecoration("아이디_힌트".tr(), counterText: '', focusColor: Colors.transparent, enableColor: Colors.transparent),
                  onChanged: (value) {
                    setState(() {
                      if (value.contains('@')) widget.data.dropValue = "직접입력".tr();
                      _emailValidMsg = _validatorId(_getId());
                      widget.onChanged(value);
                    });
                  },
                  onEditingComplete: () {
                    setState(() {
                      _emailValidMsg = _validatorId(_getId());
                      widget.onEditingComplete(widget.data.tcId.text);
                      focusClear(context);
                    });
                  },
                ),
              ),
              widget.flag.isSelectEmail
                  ? Expanded(
                      flex: 1,
                      child: Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        height: size_40, // sync underline heigth
                        alignment: Alignment.center,
                        child: lText(
                          '@',
                          textAlign: TextAlign.center,
                          style: stRegisterHint,
                        ),
                      ))
                  : Container(),
              Expanded(
                  flex: 2,
                  child: DropdownButton<String>(
                    value: widget.data.dropValue,
                    icon: Lcons.nav_bottom(color: color_b2b2b2),
                    underline: Container(),
                    // remove underline
                    style: stRegisterHint,
                    isExpanded: true,
                    onChanged: (value) {
                      setState(() {
                        widget.data.dropValue = value!;
                        if (value == "직접입력".tr()) {
                          widget.flag.disableSelectEmail(fn: () => setState(() {}));
                        } else {
                          widget.flag.enableSelectEmail(fn: () => setState(() {}));
                          int index = widget.data.tcId.text.indexOf('@');
                          if (index != 0) widget.data.tcId.text = widget.data.tcId.text.substring(0, index);
                        }
                        log.d('『GGUMBI』>>> idEmailField : value: $value,  <<< ');
                        _emailValidMsg = _validatorId(_getId());
                        widget.onChanged(_getId());
                      });
                    },
                    items: widget.data.emails.map<DropdownMenuItem<String>>((
                      String value,
                    ) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: lText(value, style: (_styleCheck(value) ? stRegisterHint : stLogin)),
                      );
                    }).toList(),
                  )),
            ]),
            Divider(color: _focusEmailID.hasFocus ? color_main : color_dedede, height: 3, thickness: _focusEmailID.hasFocus ? 2 : 1),
            sb_h_03,
            lText(widget.message ?? _emailValidMsg ?? ' ', style: widget.isValidMail ? st_b_13(textColor: color_00cfb5) : st_b_13(textColor: color_ff3b30)),
          ])),
        ));
  }

  String? _validatorId(String? value) {
    if (value == null) return "에러_아이디_확인3".tr();
    value = _getId();
    return StringUtils.validateEmail(value);
  }

  ///이메일 직접입력 및 이메일 선택시 조합하기
  String _getId() {
    String id;
    if (widget.flag.isSelectEmail) {
      id = StringUtils.getEmail(widget.data.tcId.text, widget.data.dropValue);
    } else {
      id = widget.data.tcId.text;
    }
    log.d('『GGUMBI』>>> _getId : id: $id,  <<< ');
    return id;
  }
}

class PasswordField extends StatefulWidget {
  final Function? onPressed;
  final Function onEditingComplete;
  final Function onChanged;
  final String? Function(String?)? validate;
  final Color focusedColor;
  final String? hint;
  final DataManager data;

  final ViewFlag flag;

  PasswordField({this.onPressed, required this.onEditingComplete, required this.onChanged, this.focusedColor = color_main, required this.data, required this.flag, this.hint, this.validate});

  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  FocusNode _focusPw1 = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Focus(
        focusNode: _focusPw1,
        onFocusChange: (f) => setState(() => widget.onChanged(widget.data.tcPassword.text)),
        child: Form(
            key: widget.data.formPw,
            child: TextFormField(
              controller: widget.data.tcPassword,
              obscureText: widget.flag.isShowPassword,
              validator: widget.validate ?? _validatePw,
              style: stLogin,
              cursorColor: color_00cfb5,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.text,
              maxLength: maxPasswordLength,
              autofillHints: [AutofillHints.newPassword],
              decoration: underlineDecorationMultiIcon(widget.hint ?? "비밀번호_힌트".tr(),
                  focusColor: widget.focusedColor,
                  icons: [
                    if (_focusPw1.hasFocus) lInkWell(child: Lcons.clear(), onTap: () => widget.data.tcPassword.clear()),
                    if (_isShowing(_focusPw1.hasFocus, widget.data.tcPassword.text, widget.data.tcPasswordConfirm.text, () => setState(() => {}), widget.flag)) Lcons.check(color: widget.focusedColor, isEnabled: true),
                    sb_w_10,
                    if (_focusPw1.hasFocus || widget.data.tcPassword.text.isNotEmpty)
                      lInkWell(
                          child: Lcons.visibility(isEnabled: widget.flag.isShowPassword),
                          onTap: () => widget.flag.toggleShowPassword(false,
                              fn: () => setState(() {
                                    if (widget.onPressed != null) widget.onPressed!();
                                  })))
                  ],
                  focus: _focusPw1.hasFocus),
              onSaved: (val) => widget.data.tcPassword.text = val!,
              onEditingComplete: () {
                setState(() {
                  widget.data.formPw.currentState!.validate();
                  widget.onEditingComplete(widget.data.tcPassword.text);
                  focusNextTextField(context);
                });
              },
              onChanged: (value) {
                setState(() {
                  widget.data.formPw.currentState!.validate();
                  widget.onChanged(value);
                });
              },
            )));
  }

  String? _validatePw(String? value) {
    if (value!.isEmpty) return null;
    if (StringUtils.isValidatePw(value)) {
      return null;
    } else {
      return "비밀번호안내".tr();
    }
  }
}

class PasswordConfirmField extends StatefulWidget {
  final Function? onPressed;
  final Function onEditingComplete;
  final Function(String) onChanged;
  final String? Function(String?)? validate;
  final Color focusedColor;
  final String? hint;
  final DataManager data;

  final ViewFlag flag;

  PasswordConfirmField({this.onPressed, required this.onEditingComplete, required this.onChanged, this.focusedColor = color_main, required this.data, required this.flag, this.hint, this.validate});

  @override
  _PasswordConfirmFieldState createState() => _PasswordConfirmFieldState();
}

class _PasswordConfirmFieldState extends State<PasswordConfirmField> {
  FocusNode _focusPw2 = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Focus(
        focusNode: _focusPw2,
        onFocusChange: (f) => setState(() => widget.onChanged(widget.data.tcPasswordConfirm.text)),
        child: Form(
            key: widget.data.formPw2,
            child: TextFormField(
              controller: widget.data.tcPasswordConfirm,
              obscureText: widget.flag.isShowPasswordConfirm,
              validator: widget.validate ?? _validate,
              style: stLogin,
              cursorColor: color_00cfb5,
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.text,
              maxLength: maxPasswordLength,
              autofillHints: [AutofillHints.newPassword],
              decoration: underlineDecorationMultiIcon(widget.hint ?? "비밀번호_재입력_힌트".tr(),
                  focusColor: widget.focusedColor,
                  icons: [
                    if (_focusPw2.hasFocus) lInkWell(child: Lcons.clear(), onTap: () => widget.data.tcPasswordConfirm.clear()),
                    _isShowing(_focusPw2.hasFocus, widget.data.tcPassword.text, widget.data.tcPasswordConfirm.text, () => setState(() => {}), widget.flag) ? Lcons.check(color: widget.focusedColor, isEnabled: true) : Container(),
                    sb_w_10,
                    if (_focusPw2.hasFocus || widget.data.tcPasswordConfirm.text.isNotEmpty)
                      lInkWell(
                          child: Lcons.visibility(isEnabled: widget.flag.isShowPasswordConfirm),
                          onTap: () => widget.flag.toggleShowPassword(true,
                              fn: () => setState(() {
                                    if (widget.onPressed != null) widget.onPressed!();
                                  }))),
                  ],
                  focus: _focusPw2.hasFocus),
              onSaved: (val) => widget.data.tcPasswordConfirm.text = val!,
              onEditingComplete: () {
                setState(() {
                  widget.data.registerItem.passwordReConfirm = widget.data.formPw2.currentState!.validate();
                  focusClear(context);
                  widget.onEditingComplete(widget.data.tcPasswordConfirm.text);
                });
              },
              onChanged: (value) {
                setState(() {
                  widget.data.registerItem.passwordReConfirm = widget.data.formPw2.currentState!.validate();
                  widget.onChanged(value);
                });
              },
            )));
  }

  String? _validate(String? pw2) {
    return StringUtils.validatePwConfirm(widget.data.tcPassword.text, pw2 ?? '');
  }
}

class NameField extends StatefulWidget {
  final Function onChanged;
  final Function onEditingComplete;
  final int locate;
  final Color focusColor;
  final String? hint;
  final bool lastField;

  final DataManager data;
  final ViewType viewType;

  NameField({required this.onChanged, required this.onEditingComplete, this.locate = 0, this.focusColor = color_main, this.hint, this.lastField = false, required this.data, this.viewType = ViewType.apply});

  @override
  _NameFieldState createState() => _NameFieldState();
}

class _NameFieldState extends State<NameField> {
  FocusNode _focusName = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Focus(
        focusNode: _focusName,
        onFocusChange: (f) => setState(() => widget.onChanged(widget.data.tcName.text)),
        child: Form(
          key: widget.data.formName,
          child: TextFormField(
            cursorColor: color_00cfb5,
            controller: widget.data.tcName,
            validator: StringUtils.validateName,
            style: st_b_16(textColor: widget.viewType == ViewType.view ? color_dedede : color_222222),
            textInputAction: widget.lastField ? TextInputAction.done : TextInputAction.next,
            keyboardType: TextInputType.text,
            enabled: widget.viewType == ViewType.view ? false : true,
            decoration: underLineDecoration(
              widget.hint ?? "이름입력힌트".tr(),
              icon: Lcons.clear(),
              fn: () => widget.data.tcName.clear(),
              focus: _focusName.hasFocus,
              focusColor: widget.focusColor,
            ),
            onSaved: (val) => widget.data.name = val!,
            onChanged: (value) {
              widget.data.registerItem.nameConfirm = widget.data.formName.currentState!.validate();
              widget.onChanged(value);
            },
            onEditingComplete: () {
              widget.data.registerItem.nameConfirm = widget.data.formName.currentState!.validate();
              widget.onEditingComplete(widget.data.tcName.text);
              widget.lastField ? focusClear(context) : focusNextTextField(context);
            },
          ),
        ));
  }
}

class MobileField extends StatefulWidget {
  final Function onChanged;
  final Function onEditingComplete;
  final int locate;
  final Function onClick;

  final ViewFlag flag;

  final DataManager data;

  MobileField({required this.onChanged, required this.onEditingComplete, this.locate = 0, required this.onClick, required this.flag, required this.data});

  @override
  _MobileFieldState createState() => _MobileFieldState();
}

class _MobileFieldState extends State<MobileField> {
  FocusNode _focusMobile = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Focus(
        focusNode: _focusMobile,
        onFocusChange: (f) => setState(() => widget.onChanged(widget.data.tcMobile.text)),
        child: Form(
          key: widget.data.formMobile,
          child: Container(
            child: Row(
              children: [
                Flexible(
                  child: TextFormField(
                      cursorColor: color_00cfb5,
                      controller: widget.data.tcMobile,
                      validator: _validatorMobile,
                      style: stLogin,
                      inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      maxLength: maxMobileLength,
                      decoration: underLineDecoration("휴대폰번호힌트".tr(), focusColor: color_main, focus: _focusMobile.hasFocus),
                      onSaved: (val) => widget.data.mobile = val!,
                      onChanged: (value) {
                        widget.data.registerItem.mobileConfirm = widget.data.formMobile.currentState!.validate();
                        widget.onChanged(value);
                      },
                      onEditingComplete: () {
                        widget.data.registerItem.mobileConfirm = widget.data.formMobile.currentState!.validate();
                        widget.onEditingComplete(widget.data.tcMobile.text);
                        focusNextTextField(context);
                      }),
                ),
                sb_w_08,
                if (widget.locate == 0)
                  Container(
                      padding: padding_20_B,
                      child: lInkWell(
                          child: Container(
                            height: 44,
                            width: 90,
                            alignment: Alignment.center,
                            child: lText(widget.data.findItem.isMobileTimer ? "재전송".tr() : "인증번호".tr(), style: defaultStyle(fontSize: 14, color: isMobileConfirm(widget.data) && isNameValidate(widget.data) ? color_main : color_dedede)),
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(32), border: Border.all(color: isMobileConfirm(widget.data) && isNameValidate(widget.data) ? color_main : color_dedede)),
                          ),
                          onTap: () {
                            if (isMobileConfirm(widget.data) && isNameValidate(widget.data)) {
                              widget.data.resetAlert();
                              widget.data.tcAuth.clear();
                              widget.flag.disableConfirm();
                              widget.onClick();
                            }
                          })),
              ],
            ),
          ),
        ));
  }
}

class AuthCodeTimerField extends StatefulWidget {
  final bool focus;
  final bool lastField;
  final Function onChanged;
  final Function onEditingComplete;

  final DataManager data;

  AuthCodeTimerField({Key? key, required this.onChanged, required this.onEditingComplete, this.focus = false, this.lastField = true, required this.data}) : super(key: key);

  @override
  _AuthCodeTimerFieldState createState() => _AuthCodeTimerFieldState();
}

class _AuthCodeTimerFieldState extends State<AuthCodeTimerField> {
  FocusNode _focusAuth = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Positioned(
        top: size_45,
        width: widget.data.width(context, 1),
        child: Divider(color: _focusAuth.hasFocus ? color_00cfb5 : color_dedede, height: 3, thickness: _focusAuth.hasFocus ? 2 : 1),
      ),
      Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Flexible(
            flex: 5,
            child: Focus(
                focusNode: _focusAuth,
                onFocusChange: (f) => setState(() => widget.onChanged(widget.data.tcAuth.text)),
                child: Form(
                  key: widget.data.formAuth,
                  child: TextFormField(
                      controller: widget.data.tcAuth,
                      cursorColor: color_00cfb5,
                      validator: _validateAuth,
                      maxLength: maxAuthLength,
                      keyboardType: TextInputType.number,
                      textInputAction: widget.lastField ? TextInputAction.done : TextInputAction.next,
                      decoration: underLineDecoration("인증번호입력".tr(), focusColor: Colors.transparent, enableColor: Colors.transparent),
                      // not showing
                      onChanged: (value) {
                        setState(() {
                          widget.onChanged(value);
                          widget.data.registerItem.authConfirm = widget.data.formAuth.currentState!.validate();
                        });
                      },
                      onEditingComplete: () {
                        setState(() {
                          widget.onEditingComplete(widget.data.tcAuth.text);
                          widget.data.registerItem.authConfirm = widget.data.formAuth.currentState!.validate();
                          if (!widget.lastField)
                            focusNextTextField(context);
                          else
                            focusClear(context);
                        });
                      }),
                ))),
        Flexible(
            flex: 1,
            child: Column(
              children: [
                Container(
                  margin: padding_14_B,
                  padding: padding_12_B,
                  alignment: Alignment.topCenter,
                  child: !widget.data.findItem.isMobileTimer
                      ? lText('00:00', style: TextStyle(fontSize: st_b_16().fontSize, color: color_b2b2b2))
                      : TimerBuilder.periodic(
                          Duration(seconds: 1),
                          alignment: Duration.zero,
                          builder: (context) {
                            // This function will be called every second until the alert time
                            var now = DateTime.now();
                            var remaining = widget.data.alert.difference(now);
                            String timer = "";
                            TextStyle style = st_b_16();
                            if (now.compareTo(widget.data.alert) >= 0) {
                              timer = '00:00';
                              style = TextStyle(fontSize: st_b_16().fontSize, color: color_b2b2b2);
                            } else {
                              timer = StringUtils.formatDuration(remaining);
                              style = TextStyle(fontSize: st_b_16().fontSize, color: color_ff3b30);
                            }
                            return lText(timer, style: style);
                          },
                        ),
                ),
              ],
            )),
      ])
    ]);
  }
}

class IdField extends StatefulWidget {
  final Function onChanged;
  final Function onEditingComplete;

  final DataManager data;

  IdField({required this.onChanged, required this.onEditingComplete, required this.data});

  @override
  _IdFieldState createState() => _IdFieldState();
}

class _IdFieldState extends State<IdField> {
  @override
  void initState() {
    super.initState();
    widget.data.registerItem.idConfirm = false;
  }

  FocusNode _focusEmail = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Focus(
        focusNode: _focusEmail,
        onFocusChange: (f) => setState(() => widget.onChanged(widget.data.tcId.text)),
        child: Form(
          key: widget.data.formId,
          child: TextFormField(
            cursorColor: color_00cfb5,
            controller: widget.data.tcId,
            validator: StringUtils.validateEmail,
            style: stLogin,
            autofillHints: [AutofillHints.username],
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.emailAddress,
            decoration: underLineDecoration("아이디이메일".tr(), icon: Lcons.clear(size: 30), fn: () => widget.data.tcId.clear(), focus: _focusEmail.hasFocus),
            onSaved: (val) => widget.data.id = val!,
            onEditingComplete: () {
              setState(() {
                if (widget.data.tcId.text.isNotEmpty) widget.data.registerItem.idConfirm = widget.data.formId.currentState!.validate();
                widget.onEditingComplete(widget.data.tcId.text);
                focusClear(context);
              });
            },
            onChanged: (value) {
              setState(() {
                if (widget.data.tcId.text.isNotEmpty) widget.data.registerItem.idConfirm = widget.data.formId.currentState!.validate();
                widget.onChanged(value);
              });
            },
          ),
        ));
  }
}

class Login extends StatefulWidget {
  final Function onIdChanged;
  final Function onPwChanged;
  final Function onIdEditingComplete;
  final Function onPwEditingComplete;

  final DataManager data;

  final ViewFlag flag;

  Login({required this.onIdChanged, required this.onPwChanged, required this.onIdEditingComplete, required this.onPwEditingComplete, required this.data, required this.flag});

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late FocusNode _idFNode, _pwFNode;
  Color _idColor = color_dedede;
  Color _pwColor = color_dedede;
  bool _toggleShowPassword = false;

  @override
  void initState() {
    super.initState();
    _idFNode = FocusNode();
    _pwFNode = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return AutofillGroup(
      child: Column(children: [
        Focus(
            onFocusChange: (f) => setState(() {
                  if (f || widget.data.tcId.text.isNotEmpty) {
                    _idColor = color_00cfb5;
                  } else {
                    _idColor = color_b2b2b2;
                  }
                }),
            child: Form(
              key: widget.data.formId,
              child: TextFormField(
                focusNode: _idFNode,
                cursorColor: color_00cfb5,
                key: Key(KEY_ID),
                controller: widget.data.tcId,
                validator: StringUtils.validateEmail,
                style: stLogin,
                autofillHints: [AutofillHints.username],
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.emailAddress,
                decoration: lineDecorationExceptHint(
                  "아이디이메일".tr(),
                  () => widget.data.tcId.clear(),
                  _idColor,
                  _idFNode.hasFocus,
                ),
                onSaved: (val) => widget.data.id = val!,
                onEditingComplete: () => setState(() {
                  if (widget.data.tcId.text.isNotEmpty) widget.data.registerItem.idConfirm = widget.data.formId.currentState!.validate();
                  widget.onIdEditingComplete(widget.data.tcId.text);
                  focusNextTextField(context);
                }),
                onChanged: (value) => setState(() {
                  if (widget.data.tcId.text.isNotEmpty) widget.data.registerItem.idConfirm = widget.data.formId.currentState!.validate();
                  widget.onIdChanged(value);
                }),
              ),
            )),
        sb_h_15,
        Focus(
            onFocusChange: (f) => setState(() {
                  if (f || widget.data.tcPassword.text.isNotEmpty) {
                    _pwColor = color_00cfb5;
                  } else {
                    _pwColor = color_b2b2b2;
                  }
                  if (!f) widget.data.formPw.currentState!.validate();
                }),
            child: Form(
              key: widget.data.formPw,
              child: TextFormField(
                focusNode: _pwFNode,
                cursorColor: color_00cfb5,
                key: Key(KEY_PASSWORD),
                autofillHints: [AutofillHints.password],
                controller: widget.data.tcPassword,
                obscureText: widget.flag.isShowPassword,
                validator: StringUtils.confirmPw,
                style: stLogin,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.text,
                decoration: lineDecorationPwExceptHint(
                  "비밀번호".tr(),
                  widget.flag.isShowPassword,
                  () => widget.flag.toggleShowPassword(false, fn: () => setState(() => {})),
                  () => widget.data.tcPassword.clear(),
                  _pwColor,
                  _pwFNode.hasFocus,
                ),
                onSaved: (val) => widget.data.password = val!,
                onEditingComplete: () => setState(() {
                  if (widget.data.tcPassword.text.isNotEmpty) widget.data.registerItem.passwordConfirm = widget.data.formPw.currentState!.validate();
                  widget.onPwEditingComplete(widget.data.tcPassword.text);
                  focusClear(context);
                }),
                onChanged: (value) => setState(() {
                  if (widget.data.tcPassword.text.isNotEmpty) widget.data.registerItem.passwordConfirm = widget.data.formPw.currentState!.validate();
                  widget.onPwChanged(value);
                }),
              ),
            )),
      ]),
    );
  }
}
