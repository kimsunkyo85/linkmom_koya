import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/utils/view_util.dart';

import '../../main.dart';
import '../lcons.dart';
import '../main/chat/full_photo.dart';

class ImageAddView extends StatefulWidget {
  ImageAddView({
    this.title = '',
    this.max = 2,
    this.data,
    this.listHeight = 78,
    this.onItemClick,
    this.onCheck,
  });

  final String title;
  final int max;
  final List<File>? data;
  final double listHeight;
  final Function? onItemClick;
  final Function? onCheck;

  ImageAddViewState createState() => ImageAddViewState();
}

class ImageAddViewState extends State<ImageAddView> {
  bool isSkip = false;
  final picker = ImagePicker();
  double fileSize = 0;

  @override
  void initState() {
    super.initState();
    getImageSize(isDlg: false);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.listHeight + 11,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          lInkWell(
            child: Container(
              margin: EdgeInsets.fromLTRB(0, 10, 8, 0),
              width: widget.listHeight,
              height: widget.listHeight,
              decoration: decorationChildInfo(color: color_white, borderColor: color_dfe3e4),
              child: Padding(
                padding: padding_10,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Lcons.photo_add(size: 24),
                    TextHighlight(
                      text: '${widget.data?.length ?? 0}/${widget.max}',
                      term: '/${widget.max}',
                      textStyle: st_b_13(fontWeight: FontWeight.w700),
                      textStyleHighlight: st_13(textColor: color_b2b2b2, fontWeight: FontWeight.w700),
                    ),
                    lText('${fileSize.toStringAsFixed(2)}M', style: st_b_10(textColor: color_b2b2b2)),
                  ],
                ),
              ),
            ),
            onTap: () {
              focusClear(context);
              if ((widget.data?.length ?? 0) < widget.max) {
                showDlgCamera(
                  btn1: "사진선택".tr(),
                  btn2: "사진찍기".tr(),
                  onClickAction: (action) {
                    log.d('『GGUMBI』>>> build : action: $action,  <<< ');
                    if (action == DialogAction.gallery) {
                      getImage(ImageSource.gallery);
                    } else if (action == DialogAction.camera) {
                      getImage(ImageSource.camera);
                    }
                  },
                );
              } else {
                showNormalDlg(
                  message: "이미지최대안내_추가".tr(),
                );
              }
            },
          ),
          Expanded(
            child: ListView.builder(
                itemCount: widget.data?.length ?? 0,
                scrollDirection: Axis.horizontal,
                physics: const AlwaysScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        lInkWell(
                          onTap: () {
                            if (widget.data?.isNotEmpty ?? false) {
                              Commons.nextPage(context, fn: () => FullPhotoPage(index: index, fileList: widget.data!));
                            }
                          },
                          child: Container(
                            margin: padding_08_LTR,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(6),
                              child: Image.file(
                                widget.data![index],
                                frameBuilder: (context, child, frame, wasSynchronouslyLoaded) {
                                  if (frame != null || wasSynchronouslyLoaded) {
                                    return child;
                                  }
                                  return Container(
                                    width: widget.listHeight,
                                    height: widget.listHeight,
                                    child: lLoadingIndicator(),
                                  );
                                },
                                fit: BoxFit.cover,
                                height: widget.listHeight,
                                width: widget.listHeight,
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          right: 0,
                          top: 0,
                          child: lInkWell(
                              onTap: () {
                                setState(() {
                                  if (widget.onItemClick != null) {
                                    widget.data!.removeAt(index);
                                    widget.onItemClick!(widget.data!, index);
                                    getImageSize(isDlg: false);
                                  }
                                });
                              },
                              child: Container(child: Lcons.delete(size: 22))),
                        ),
                      ],
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  ///갤러리 또는 이미지 파일 가져오기
  Future<void> getImage(ImageSource type) async {
    switch (type) {
      case ImageSource.gallery:
        List<XFile>? imageFileList = await picker.pickMultiImage();
        setState(() {
          if (imageFileList != null) {
            imageFileList.forEach((img) {
              File file = Commons.replaceFileName(File(img.path));
              if (widget.data!.length < widget.max) {
                widget.data!.add(file);
              }
            });
            getImageSize();
            if (imageFileList.length > widget.max) {
              showNormalDlg(message: "${"최대".tr()}${widget.max}${"이미지최대안내".tr()}");
            }
          } else {
            log.d('『GGUMBI』>>> getImage : No Image Selected  <<< ');
          }
        });
        break;
      case ImageSource.camera:
        XFile? imageFile = await picker.pickImage(source: type);
        setState(() {
          if (imageFile != null) {
            File file = Commons.replaceFileName(File(imageFile.path));
            if (widget.data!.length < widget.max) {
              widget.data!.add(file);
            } else {}
          } else {
            log.d('『GGUMBI』>>> getImage : No Image Selected  <<< ');
          }
        });
        break;
      default:
    }
  }

  getImageSize({bool isDlg = true}) async {
    fileSize = 0;
    if (widget.data!.length > 0) {
      widget.data!.forEach((value) {
        final bytes = value.readAsBytesSync().lengthInBytes;
        final kb = bytes / 1024;
        final mb = kb / 1024;
        fileSize = fileSize + mb;
      });
      // 사진 총 합계가 25 넘을경우 안내
      if (fileSize > 25 && isDlg) {
        showNormalDlg(message: "에러_413".tr());
      }
    }
  }
}
