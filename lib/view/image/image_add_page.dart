import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:linkmom/data/network/models/authcenter_home_request.dart';
import 'package:linkmom/data/network/models/authcenter_home_response.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/utils/view_util.dart';

import '../../main.dart';
import '../lcons.dart';
import '../main/chat/full_photo.dart';

class ImageAdd extends StatefulWidget {
  ImageAdd({
    this.title = '',
    this.max = 2,
    this.data = '',
    this.listHeight = 78,
    this.onItemClick,
    this.onCheck,
  });

  final String? title;
  final int max;
  final dynamic data;
  final double listHeight;
  final Function? onItemClick;
  final Function? onCheck;

  ImageAddState createState() => ImageAddState();
}

class ImageAddState extends State<ImageAdd> {
  String? _title;
  dynamic data;
  late double listHeight;
  late int max;
  bool isSkip = false;
  File? _image;
  final picker = ImagePicker();
  List<ImageItem> _images = [];
  IosDeviceInfo? _info;

  @override
  void initState() {
    super.initState();
    _title = widget.title ?? '';
    max = widget.max;
    listHeight = widget.listHeight;
    log.d('『GGUMBI』>>> initState imageADD : $data, ${data is AuthCenterHomeResponse} <<< ');
    log.d('『GGUMBI』>>> initState : _images : $_images,  <<< ');
    Commons.onIosInfo().then((value) => _info = value);
  }

  @override
  void didUpdateWidget(covariant ImageAdd oldWidget) {
    super.didUpdateWidget(oldWidget);
    data = widget.data;
    if (data is AuthCenterHomeResponse) {
      if (data.getData() != null && _images.isEmpty) {
        if (StringUtils.validateString(data.getData().ourhome_picture1)) {
          _images.add(ImageItem(
            key: AuthCenterHomeRequest.Key_ourhome_picture1,
            file: data.getData().file1,
            imageUrl: data.getData().ourhome_picture1,
            viewType: ViewType.view,
          ));
        }

        if (StringUtils.validateString(data.getData().ourhome_picture2)) {
          _images.add(ImageItem(
            key: AuthCenterHomeRequest.Key_ourhome_picture2,
            file: data.getData().file2,
            imageUrl: data.getData().ourhome_picture2,
            viewType: ViewType.view,
          ));
        }
      }
    } else if (data is List<File>) {
      var images = data as List<File>;
      images.forEach((value) {
        _images.add(ImageItem(
          file: value,
          viewType: ViewType.add,
        ));
      });
    } else if (data is List<ImageItem>) {
      _images = data as List<ImageItem>;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: listHeight + 22,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          InkWell(
            child: Container(
              height: listHeight,
              width: listHeight,
              margin: padding_05_R,
              decoration: decorationChildInfo(color: color_white, borderColor: color_dfe3e4),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Lcons.photo_add(size: 24),
                  sb_h_05,
                  TextHighlight(
                    text: '${_images.length}/$max',
                    term: '/$max',
                    textStyle: st_b_13(fontWeight: FontWeight.w700),
                    textStyleHighlight: st_13(textColor: color_b2b2b2, fontWeight: FontWeight.w700),
                  ),
                ],
              ),
            ),
            onTap: () {
              if (_images.length < max) {
                String btn2 = "사진찍기".tr();
                if (_info!.model!.toLowerCase().contains('ipad')) btn2 = '';
                showDlgCamera(
                  btn1: "사진선택".tr(),
                  btn2: btn2,
                  onClickAction: (action) {
                    log.d('『GGUMBI』>>> build : action: $action,  <<< ');
                    focusClear(context);
                    if (_images.length < max) {
                      if (action == DialogAction.gallery) {
                        getImage(ImageSource.gallery);
                      } else if (action == DialogAction.camera) {
                        getImage(ImageSource.camera);
                      }
                    }
                  },
                );
              } else {
                Commons.showToast("이미지최대안내_추가".tr());
              }
            },
          ),
          Expanded(
            child: ListView.builder(
                padding: padding_0,
                itemCount: _images.length,
                scrollDirection: Axis.horizontal,
                physics: const AlwaysScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    height: listHeight + 11,
                    width: listHeight + 11,
                    margin: padding_06_L,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        lInkWell(
                          onTap: () {
                            if (_images.length != 0) {
                              Commons.nextPage(
                                context,
                                fn: () => _images[index].file == null
                                    ? FullPhotoPage(index: index, urlList: List<String>.generate(_images.length, (index) => _images[index].imageUrl!))
                                    : FullPhotoPage(
                                        index: index,
                                        fileList: List<File>.generate(_images.length, (index) => _images[index].file!),
                                      ),
                              );
                            }
                          },
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(6),
                            child: _images[index].viewType == ViewType.add
                                ? Image.file(
                                    _images[index].file!,
                                    frameBuilder: (context, child, frame, wasSynchronouslyLoaded) {
                                      if (frame != null || wasSynchronouslyLoaded) {
                                        return child;
                                      }
                                      return lLoadingIndicator();
                                    },
                                    fit: BoxFit.cover,
                                    height: listHeight,
                                    width: listHeight,
                                  )
                                : Image.network(
                                    _images[index].imageThumbnail ?? _images[index].imageUrl ?? '',
                                    loadingBuilder: (context, child, loadingProgress) {
                                      if (loadingProgress == null) return child;
                                      return lLoadingIndicator();
                                    },
                                    fit: BoxFit.cover,
                                    height: listHeight,
                                    width: listHeight,
                                  ),
                          ),
                        ),
                        Positioned(
                          right: 0,
                          top: 2,
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                if (widget.onItemClick != null) {
                                  widget.onItemClick!(_images[index], index);
                                  _images.removeAt(index);
                                }
                              });
                            },
                            child: Lcons.delete(size: 22),
                          ),
                        ),
                      ],
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }

  ///갤러리 또는 이미지 파일 가져오기
  getImage(ImageSource type) async {
    try {
      //집안환겨 인증에서 사용
      if (data is AuthCenterHomeResponse) {
        final pickedFile = await picker.pickImage(source: type);
        if (pickedFile != null) {
          _image = Commons.replaceFileName(File(pickedFile.path));
          if (_images.length < max) {
            String key = AuthCenterHomeRequest.Key_ourhome_picture1;
            _images.forEach((value) {
              if (value.key == AuthCenterHomeRequest.Key_ourhome_picture1) {
                key = AuthCenterHomeRequest.Key_ourhome_picture2;
              } else if (value.key == AuthCenterHomeRequest.Key_ourhome_picture2) {
                key = AuthCenterHomeRequest.Key_ourhome_picture1;
              }
            });
            _images.add(ImageItem(key: key, file: _image, viewType: ViewType.add));
          } else {
            Commons.showToast("이미지최대안내_추가".tr());
          }
        }
      } else {
        //그외 멀티 사진 추가로 변경
        if (type == ImageSource.gallery) {
          List<XFile>? _imageFileList = [];
          final pickedFileList = await picker.pickMultiImage();
          _imageFileList = pickedFileList;
          _imageFileList!.asMap().forEach((index, value) {
            log.d('『GGUMBI』>>> getImage : index: $index, $max, ${index > max} <<< ');
            if (index < max) {
              _image = Commons.replaceFileName(File(value.path));
              _images.add(ImageItem(key: '', file: _image, viewType: ViewType.add));
            }
          });
          if (_imageFileList.length > max) {
            Commons.showToast('$max${"개".tr()} ${"까지".tr()} ${"환경인증_선택해주세요".tr()}');
          }
        } else {
          final pickedFile = await picker.pickImage(source: type);
          if (pickedFile != null) {
            _image = Commons.replaceFileName(File(pickedFile.path));
            if (_images.length < max) {
              String key = AuthCenterHomeRequest.Key_ourhome_picture1;
              _images.forEach((value) {
                if (value.key == AuthCenterHomeRequest.Key_ourhome_picture1) {
                  key = AuthCenterHomeRequest.Key_ourhome_picture2;
                } else if (value.key == AuthCenterHomeRequest.Key_ourhome_picture2) {
                  key = AuthCenterHomeRequest.Key_ourhome_picture1;
                }
              });
              _images.add(ImageItem(key: key, file: _image, viewType: ViewType.add));
            }
          }
        }
      }

      setState(() {});
      if (widget.onItemClick != null) {
        widget.onItemClick!(_images, -1);
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }
}

class ImageItem {
  ImageItem({
    this.key,
    this.filename,
    this.imageUrl,
    this.imageThumbnail,
    this.file,
    this.viewType,
  });

  String? key;
  String? filename;
  String? imageUrl;
  String? imageThumbnail;
  File? file;
  ViewType? viewType;

  @override
  String toString() {
    return 'ImageItem{key: $key, filename: $filename, imageUrl: $imageUrl, file: $file, viewType: $viewType, imageThumbnail: $imageThumbnail}';
  }
}
