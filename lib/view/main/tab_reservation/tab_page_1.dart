import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/chuck_categories.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/main/tab_rank_review/tab_rank.dart';
import 'package:linkmom/view/main/tab_rank_review/tab_review.dart';

import '../../../base/base_stateless.dart';

class TabPage1 extends BaseStateless {
  TabPage1({Key? key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          sb_h_20,
          viewEvent(),
          sb_h_20,
          SizedBox(
            height: height_list,
            child: viewRankReview(),
          ),
          sb_h_20,
        ],
      ),
    );
  }

  Widget viewEvent() {
    return CarouselSlider(
      options: CarouselOptions(
        height: 150,
        autoPlay: true,
      ),
      items: dummyItems.map((url) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(horizontal: 5.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: Image.network(
                  url,
                  fit: BoxFit.cover,
                ),
              ),
            );
          },
        );
      }).toList(),
    );
  }

  /*Widget viewReservation(BuildContext context) {
    chuckCategories = Provider.of<ApiHelper>(context, listen: true).chuckCategories;
    print('ggumbi:' + Trace.current().frames[0].location + ' ' + Trace.current().frames[0].member + '::$chuckCategories');
    return FutureBuilder<ChuckCategories>(
      future: Provider.of<ApiHelper>(context, listen: true).fetchChuckyCategories(),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return lText(
              'Fetch opportunity data',
              textAlign: TextAlign.center,
            );
          case ConnectionState.active:
            return lText('');
          case ConnectionState.waiting:
            return Commons.chuckyLoading("Fetching Chucky Categories...");
          case ConnectionState.done:
            if (snapshot.hasError) {
              return Error(
                errorMessage: "Error retrieving chucky categories.",
              );
            } else {
              chuckCategories = snapshot.data;
              print('ggumbi:' + Trace.current().frames[0].location + ' ' + Trace.current().frames[0].member + '::$snapshot');
              print('ggumbi:' + Trace.current().frames[0].location + ' ' + Trace.current().frames[0].member + '::$chuckCategories');
              return CategoryList(categoryList: chuckCategories);
            }
        }
        return Commons.chuckyLoading("Fetching Chucky Categories...");
      },
    );
  }*/

  Widget viewRankReview() {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        // backgroundColor: Commons.mainAppColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(kToolbarHeight),
          // TabBar 구현. 각 컨텐트를 호출할 탭들을 등록
          child: Container(
            height: 50.0,
            child: TabBar(
              indicatorColor: Colors.blue,
              unselectedLabelColor: Colors.grey,
              labelColor: Colors.blue,
              tabs: [
                Tab(
                  text: "우리집돌봄".tr(),
                ),
                Tab(
                  text: "이웃집돌봄".tr(),
                ),
              ],
            ),
          ),
        ),
        // TabVarView 구현. 각 탭에 해당하는 컨텐트 구성
        body: TabBarView(
          children: [
            TabRank(title: "우리집돌봄".tr()),
            TabReview(title: "이웃집돌봄".tr()),
            // Icon(Icons.directions_transit),
            // Icon(Icons.directions_bike),
          ],
        ),
      ),
    );
  }
}

class CategoryList extends StatelessWidget {
  final ChuckCategories categoryList;

  const CategoryList({Key? key, required this.categoryList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        return Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 0.0,
              vertical: 0.5,
            ),
            child: lInkWell(
                onTap: () {},
                child: SizedBox(
                  height: 65,
                  child: Container(
                    color: color_tileBackground,
                    alignment: Alignment.center,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(4, 0, 0, 0),
                      child: lText(
                        categoryList.categories![index],
                        style: TextStyle(color: color_black, fontSize: 20, fontFamily: 'Roboto'),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                )));
      },
      itemCount: categoryList.categories!.length,
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
    );
  }
}

final dummyItems = [
  'https://dreambcokr.cdn.smart-img.com/ggumbi/2020/banner/main/09/200918_online_befe_main_pc.jpg',
  'https://dreambcokr.cdn.smart-img.com/ggumbi/2020/banner/main/06/200618_super_giant_323x230_main_pc.jpg',
  'https://dreambcokr.cdn.smart-img.com/ggumbi/2020/event/06/200513-A_topper_main_pc.jpg',
];
