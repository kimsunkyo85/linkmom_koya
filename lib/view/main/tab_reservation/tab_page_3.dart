import 'package:flutter/material.dart';

import '../../../base/base_stateless.dart';

class TabPage3 extends BaseStateless {
  TabPage3({Key? key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        title,
        style: TextStyle(fontSize: 40),
      ),
    );
  }
}
