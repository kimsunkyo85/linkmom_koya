import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cs_report_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/compleat_view_page.dart';

import 'notify_page.dart';

class CareNotifyPage extends BaseStateful {
  final int userId;
  final int matchingId;
  final int? chatId;
  final String? userName, desc;
  final List<NotifyReason>? reasons;

  CareNotifyPage({Key? key, required this.userId, required this.matchingId, this.chatId, this.reasons, this.userName, this.desc});

  @override
  _CareNotifyPagetate createState() => _CareNotifyPagetate();
}

class _CareNotifyPagetate extends BaseStatefulState<CareNotifyPage> {
  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar("신고하기".tr(), hide: false),
        body: NotifyPage(
          reasons: widget.reasons ?? [],
          userName: widget.userName,
          desc: widget.desc,
          noticeMessage: List.generate(2, (index) => "예약관리_신고하기_${index + 1}".tr()),
          reasonAlert: (type) {
            if (type == NotifyReason.linkmomOutOfPlatform.value || type == NotifyReason.momdadyOutOfPlatform.value) {
              showNormalDlg(
                  context: context,
                  color: Commons.getColor(),
                  messageWidget: Container(
                    child: Column(
                      children: [
                        lText("신고하기_타플랫폼요구안내_1".tr(), style: st_14(textColor: color_545454), textAlign: TextAlign.center),
                        TextHighlight(
                          text: "신고하기_타플랫폼요구안내_2".tr(),
                          term: "만포인트".tr(),
                          textStyle: st_14(textColor: color_545454),
                          textStyleHighlight: st_14(textColor: color_545454, fontWeight: FontWeight.w700),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ));
            }
          },
          requestNotice: (selected, detail, image) => _requestNotify(selected, detail, image),
        ),
      ),
    );
  }

  void _requestNotify(int selected, String detail, List<File>? image) {
    try {
      CsReportRequest req = CsReportRequest(
        notifyuser: widget.userId,
        carematching: widget.matchingId,
        image: image,
        reason: selected,
        msg: detail,
      );
      if (widget.chatId != null) {
        req.chattingroom = widget.chatId ?? 0;
      }
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestCsReport(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.pageReplace(context, routeCompleteView, arguments: CompleteViewPage(data: data, title: "신고완료".tr(), resultMessage: "신고접수가완료되었습니다".tr()));
        } else {
          showNormalDlg(context: context, message: response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}
