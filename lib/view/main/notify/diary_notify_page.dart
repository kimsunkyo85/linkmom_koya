import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/carediary_claim_cancel_request.dart';
import 'package:linkmom/data/network/models/carediary_claim_save_request.dart';
import 'package:linkmom/data/network/models/carediary_save_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/compleat_view_page.dart';
import 'package:linkmom/view/lcons.dart';

import 'notify_page.dart';

class DiaryNotifyPage extends BaseStateful {
  final int id;

  DiaryNotifyPage({Key? key, required this.id});

  @override
  _DiaryNotifyPagetate createState() => _DiaryNotifyPagetate();
}

class _DiaryNotifyPagetate extends BaseStatefulState<DiaryNotifyPage> {
  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar("정산보류요청".tr(), hide: false),
        body: NotifyPage(
          notifyTitle: "정산_정산보류_선택".tr(),
          reasons: DiaryClaim.values,
          hint: "정산_정산보류_힌트".tr(),
          noticeTitle: "",
          noticeMessage: [
            "정산_정산보류_전송안내".tr(),
            "정산_정산보류_안내1".tr(),
            "정산_정산보류_안내2".tr(),
          ],
          reasonAlert: (type) {},
          btnText: "요청하기".tr(),
          requestNotice: (selected, detail, image) => _requestNotify(selected, detail, image),
        ),
      ),
    );
  }

  void _requestNotify(int selected, String detail, List<File>? image) {
    try {
      showNormalDlg(
        context: context,
        message: "정산보류_안내".tr(),
        btnRight: "예".tr(),
        btnLeft: "아니오".tr(),
        onClickAction: (a) {
          if (a == DialogAction.yes) {
            _requestClaim(widget.id, selected, image ?? [], detail);
          }
        },
      );
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestClaim(int id, int index, List<File> image, String body) {
    try {
      CareDiaryClaimSaveRequest req = CareDiaryClaimSaveRequest(
        schedule: id,
        type: index,
        content1: body,
        image: image,
      );
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestDiaryClaimSave(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.page(context, routeCompleteView,
              arguments: CompleteViewPage(
                data: data,
                title: "접수완료".tr(),
                resultMessage: "정산_정산보류_접수안내".tr(),
                footer: Container(
                  child: lTextBtn(
                    "정산보류요청취소".tr(),
                    style: st_16(textColor: color_main),
                    rightIcon: Lcons.nav_right(color: color_main),
                    bg: Colors.transparent,
                    onClickEvent: () => _requestClaimCancel(response.getData().id),
                  ),
                ),
                finishAction: () => Commons.pagePopUntilFirst(context),
                btnColor: Commons.getColor(),
              ));
        } else {
          showNormalDlg(message: response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>>> $e");
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _saveDiary(int id, int index, List<File> image, String body) {
    try {
      CareDiarySaveRequest req = CareDiarySaveRequest(carecontract_schedule: id);
      apiHelper.requestDiarySave(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _requestClaim(response.getData().id, index, image, body);
        } else {
          showNormalDlg(message: "잠시후다시시도해주세요".tr(), onClickAction: (a) => {if (a == DialogAction.yes) Commons.pagePop(context, data: true)});
        }
      });
    } catch (e) {
      log.e("EXCEPTION : >>>>>>>>>>>>>>> $e");
    }
  }

  void _requestClaimCancel(int id) {
    try {
      CareDiaryClaimCancelRequest req = CareDiaryClaimCancelRequest(claim_id: id);
      apiHelper.requestDiaryClaimCancel(req).then((response) async {
        if (response.getCode() == KEY_SUCCESS) {
          var ret = await Commons.page(context, routeCompleteView,
              arguments: CompleteViewPage(
                data: data,
                title: "접수완료".tr(),
                resultMessage: "정산_정산보류_취소".tr(),
                btnColor: Commons.getColor(),
                finishAction: () => Commons.pageToMain(context, routeHome),
              ));
          if (ret != null) Commons.pagePop(context);
        }
      });
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>> $e");
    }
  }
}
