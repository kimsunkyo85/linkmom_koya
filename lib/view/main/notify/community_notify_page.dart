import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cs_report_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/compleat_view_page.dart';

import 'notify_page.dart';

class CommunityNotifyPage extends BaseStateful {
  final int userId;
  final int boardId;
  final int? replyId;
  final int? eventReplyId;
  final String? userName;
  CommunityNotifyPage({Key? key, required this.userId, required this.boardId, this.replyId, this.eventReplyId, this.userName});

  @override
  _CommunityNotifyPageState createState() => _CommunityNotifyPageState();
}

class _CommunityNotifyPageState extends BaseStatefulState<CommunityNotifyPage> {
  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar("신고하기".tr(), hide: false),
        body: NotifyPage(
          userName: widget.userName,
          reasons: [
            NotifyReason.communityInappropriate,
            NotifyReason.communityAd,
            NotifyReason.communityInsult,
            NotifyReason.communityPlastered,
            NotifyReason.communityPrivacy,
            NotifyReason.other,
          ],
          hint: "커뮤니티_신고_힌트".tr(),
          noticeMessage: List.generate(5, (index) => "커뮤니티_신고하기_${index + 1}".tr()),
          requestNotice: (selected, detail, image) => _doNotify(selected, detail, image),
        ),
      ),
    );
  }

  void _doNotify(int selected, String detail, List<File>? image) {
    showNormalDlg(
        message: "커뮤니티_신고_안내".tr(),
        btnLeft: "취소".tr(),
        onClickAction: (a) {
          if (a == DialogAction.yes) _requestNotify(selected, detail, image);
        });
  }

  void _requestNotify(int selected, String detail, List<File>? image) {
    try {
      CsReportRequest req = CsReportRequest(
        notifyuser: widget.userId,
        communityshare: widget.boardId,
        communitysharereply: widget.replyId ?? 0,
        communityeventreply: widget.eventReplyId ?? 0,
        image: image,
        reason: selected,
        msg: detail,
      );

      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestCsReport(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.pageReplace(context, routeCompleteView, arguments: CompleteViewPage(data: data, title: "신고완료".tr(), resultMessage: "신고접수가완료되었습니다".tr()));
        } else {
          showNormalDlg(context: context, message: response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}
