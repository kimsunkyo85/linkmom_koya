import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/cs_report_request.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:provider/provider.dart';

import '../../../data/providers/image_loader_provider.dart';
import '../../image/image_add_view.dart';

class NotifyModel {
  final String? title;
  final String? subTitle;
  final Widget? footer;
  final String? btnText;
  final String? appbarTitle;
  final String? hint;
  final Function? onClickAction;
  final String? textFieldTitle;
  final CsReportRequest? req;
  final List<NotifyReason>? reasons;
  final List<DiaryClaim>? claims;

  NotifyModel({
    this.title,
    this.subTitle,
    this.footer,
    this.btnText,
    this.appbarTitle,
    this.hint,
    this.onClickAction,
    this.textFieldTitle,
    this.req,
    this.reasons,
    this.claims,
  });
}

class NotifyPage extends StatefulWidget {
  final String? title;
  final String? notifyTitle;
  final String? noticeTitle;
  final List<String> noticeMessage;
  final Widget? noticeWidget;
  final Function requestNotice;
  final List<dynamic> reasons;
  final Function? reasonAlert;
  final String? hint;
  final String? textFieldTitle;
  final String? btnText, userName, desc;

  NotifyPage({
    this.noticeTitle,
    required this.noticeMessage,
    this.noticeWidget,
    required this.requestNotice,
    required this.reasons,
    this.title,
    this.notifyTitle,
    this.reasonAlert,
    this.hint,
    this.textFieldTitle,
    this.btnText,
    this.userName,
    this.desc,
  });

  @override
  _NotifyPageState createState() => _NotifyPageState();
}

class _NotifyPageState extends State<NotifyPage> {
  int _selected = -1;
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    context.read<ImageLoader>().loadImage([], context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
              child: lScrollView(
                  padding: padding_20,
                  child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                    lText(widget.notifyTitle ?? (widget.userName != null ? "${widget.userName}${"님을".tr()} ${"신고하는사유를선택해주세요".tr()}" : "신고사유를선택해주세요".tr()), style: st_b_18(fontWeight: FontWeight.w700)),
                    sb_h_05,
                    lText(widget.desc ?? '', style: st_14(textColor: color_999999)),
                    sb_h_20,
                    Column(
                        children: widget.reasons.map((reason) {
                      return StatefulBuilder(builder: (ctx, state) {
                        String name = '';
                        int reasonValue = 0;
                        if (reason is NotifyReason) {
                          name = reason.reason;
                          reasonValue = reason.value;
                        } else if (reason is DiaryClaim) {
                          name = reason.reason;
                          reasonValue = reason.value;
                        }
                        return Row(children: [
                          Radio(
                              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                              visualDensity: VisualDensity(horizontal: -2, vertical: -2),
                              activeColor: color_222222,
                              value: reasonValue,
                              groupValue: _selected,
                              onChanged: (value) {
                                setState(() {
                                  _selected = value as int;
                                  if (widget.reasonAlert != null) widget.reasonAlert!(value);
                                });
                              }),
                          lText(name, style: st_b_14()),
                        ]);
                      });
                    }).toList()),
                    Container(
                      padding: padding_10_TB,
                      child: lText(widget.textFieldTitle ?? ' ', style: st_b_18(fontWeight: FontWeight.w700)),
                    ),
                    Container(
                      margin: padding_20_B,
                      decoration: BoxDecoration(color: Colors.white, border: Border.all(color: color_dedede), borderRadius: BorderRadius.circular(6)),
                      padding: padding_10,
                      child: TextField(
                        controller: _controller,
                        keyboardType: TextInputType.multiline,
                        maxLines: 4,
                        decoration: InputDecoration(
                            hintMaxLines: 4,
                            // for hint overflow
                            contentPadding: padding_0,
                            hintText: widget.hint ?? "신고하기_힌트".tr(),
                            hintStyle: st_14(textColor: color_b2b2b2),
                            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                            border: OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent))),
                        maxLength: 1000,
                        onChanged: (value) => setState(() => {}),
                        buildCounter: (_, {required currentLength, required isFocused, maxLength}) => Padding(
                          padding: padding_25_L,
                          child: Container(
                              alignment: Alignment.centerRight,
                              child: lText(
                                currentLength.toString() + "/" + StringUtils.formatPay(maxLength!),
                                style: st_15(textColor: color_b2b2b2),
                              )),
                        ),
                      ),
                    ),
                    Container(
                        padding: padding_05_TB,
                        child: ChangeNotifierProvider<ImageLoader>(
                          create: (_) => ImageLoader(),
                          builder: (ctx, child) => Container(
                            child: ImageAddView(
                              max: 5,
                              data: context.read<ImageLoader>().getImages,
                              onItemClick: (image, index) {
                                context.read<ImageLoader>().setImages = image;
                                onConfirmBtn();
                              },
                            ),
                          ),
                        )),
                    Container(
                        padding: padding_20_TB,
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                          lText(widget.noticeTitle ?? "신고전유의사항".tr(), style: st_b_16(fontWeight: FontWeight.w700)),
                          sb_h_15,
                          Column(
                              children: widget.noticeMessage
                                  .map(
                                    (e) => lDingbatText(
                                      text: e,
                                      dingbatSize: 2,
                                      dingbatColor: color_545454,
                                      style: st_14(textColor: color_545454),
                                      width: Commons.width(context, 0.8),
                                      padding: padding_04_B,
                                    ),
                                  )
                                  .toList()),
                          if (widget.noticeWidget != null) widget.noticeWidget!,
                        ])),
                  ]))),
          lBtn(widget.btnText ?? "신고하기".tr(), isEnabled: onConfirmBtn(), onClickAction: () => widget.requestNotice(_selected, _controller.text, context.read<ImageLoader>().getImages))
        ],
      ),
    );
  }

  bool onConfirmBtn() {
    if (widget.reasons.first is NotifyReason && _selected == NotifyReason.other.value && _controller.text.isEmpty) {
      return false;
    } else if (widget.reasons.first is DiaryClaim && _selected == DiaryClaim.other.value && _controller.text.isEmpty) {
      return false;
    } else if (_selected < 0) {
      return false;
    } else {
      return true;
    }
  }
}
