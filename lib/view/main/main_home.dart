import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:linkmom/data/providers/push_model.dart';
import 'package:linkmom/data/storage/flag_manage.dart';
import 'package:linkmom/data/storage/storage_help.dart';
import 'package:linkmom/view/link_page.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/manager/firebase_analytics_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/services/connectivity_service.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/home_provider.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_list/momdady_list_page.dart';
import 'package:linkmom/view/main/main_page.dart';
import 'package:linkmom/view/main/matchings/linkmom_matching_page.dart';
import 'package:linkmom/view/main/mypage/mypage.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import '../../base/base_stateful.dart';
import '../../main.dart';
import 'chat/chat_page.dart';
import 'community/community_main_page.dart';
import 'job_list/linkmom_list_page.dart';
import 'matchings/momdady_matching_page.dart';
import 'notification_center_page.dart';

class MainHome extends BaseStateful {
  ///tab index
  final int tabIndex;
  final int? pageTabIndex;
  final int sendCode;
  final ViewType view;

  MainHome({this.tabIndex = 0, this.pageTabIndex, this.view = ViewType.view, this.sendCode = 0});

  @override
  _MainHomeState createState() => _MainHomeState(tabIndex: tabIndex, sendCode: sendCode);
}

class _MainHomeState extends BaseStatefulState<MainHome> {
  ///tab index
  late int tabIndex = MenuType.home.index;
  int sendCode = 0;

  _MainHomeState({this.tabIndex = 0, this.sendCode = 0});

  String title = "appName".tr();
  Widget? titleWidget = image(imagePath: IMG_LOGO, width: 200, height: 30);
  var tokens = '';

  List<Widget> _pages_0 = [];
  List<Widget> _pages_1 = [];

  int duration = 5; //초 종료 홈으로 이동후 2초로 수정

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  DateTime pre_backpress = DateTime.now();

  @override
  void initState() {
    super.initState();
    DeepLinkManager(this.context);
    tabIndex = widget.tabIndex;
    _pages_0 = [MainPage(), CommunityMainPage(tabIndex: widget.pageTabIndex), MomdadyMatchingPage(), LinkMomListPage(sendCode: sendCode), ChatPage()];
    _pages_1 = [MainPage(), CommunityMainPage(tabIndex: widget.pageTabIndex), LinkmomMatchingPage(), MomDadyListPage(sendCode: sendCode), ChatPage()];
    setTabs();
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    if (auth.user.username.isEmpty) return;

    checkPermission();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Future.delayed(Duration(milliseconds: 100), () {
        log.d('『GGUMBI』>>> initState mainhome : 1: ${Flags.isShowingCachedFlag(Flags.KEY_DEEPLINK)},  <<< ');
        if (!Flags.isShowingCachedFlag(Flags.KEY_DEEPLINK)) {
          log.d('『GGUMBI』>>> initState mainhome : 2: ${Flags.isShowingCachedFlag(Flags.KEY_DEEPLINK)},  <<< ');
          return Commons.page(context, routeLink);
        }
      });
    });
  }

  ///모드 전환시 탭 화면 설정
  List<Widget> getTabList(int? index) {
    return Commons.isLinkMom() ? [MainPage(), CommunityMainPage(tabIndex: index), LinkmomMatchingPage(), MomDadyListPage(sendCode: sendCode), ChatPage()] : [MainPage(), CommunityMainPage(tabIndex: index), MomdadyMatchingPage(), LinkMomListPage(sendCode: sendCode), ChatPage()];
  }

  Future<void> checkPermission() async {
    if (!await Commons.checkPermission()) {
      showNormalDlg(
          message: "사진권한".tr(),
          onClickAction: (action) {
            if (DialogAction.yes == action) {
              openAppSettings();
            }
          });
    }
  }

  Future onDidReceiveLocalNotification(int id, String title, String body, String payload) async {
    // log.i({
    //   'id': id,
    //   'title': title,
    //   'body': body,
    //   'payload': payload,
    // });
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Ok'),
            onPressed: () async {
              // Navigator.of(context, rootNavigator: true).pop();
              // await Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     // builder: (context) => SecondScreen(payload),
              //   ),
              // );
            },
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    mqttManager.client.disconnect();
    pushProvider.setBadgeAllClear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _storageHelper = Provider.of<StorageHelper>(context, listen: true); //맘대디,링크쌤 전환시 업데이트로 사용
    var _network = Provider.of<NetworkProvider>(context, listen: true);
    var _provider = Provider.of<HomeNavigationProvider>(context, listen: true);
    var _pushProvider = Provider.of<PushModel>(context, listen: true);
    int? subIndex = _provider.subIndex;

    if (_provider.isMove) {
      tabIndex = _provider.currentIndex;
      setTabs();
      _provider.setHomeDataClear();
      onUpDate();
    } else {
      setTabs();
      onUpDate();
    }

    //2022/06/22
    if (_pushProvider.isUpdate()) {
      onUpDate();
    }

    return WillPopScope(
      onWillPop: () async {
        // NOTE: close the mypage
        if (_scaffoldKey.currentState!.isDrawerOpen) {
          _scaffoldKey.currentState!.openEndDrawer();
          return false;
        }

        if (tabIndex != TAB_HOME) {
          setState(() {
            tabIndex = TAB_HOME;
            setTabs();
          });
          return false;
        }

        final timegap = DateTime.now().difference(pre_backpress);
        final cantExit = timegap >= Duration(seconds: duration);
        pre_backpress = DateTime.now();
        if (cantExit) {
          Commons.showSnackBar(context, "앱종료_메시지".tr());
          return false;
        } else {
          log.d('『GGUMBI』>>> didChangeAppLifecycleState : state mainHome  <<< ');
          try {
            mqttManager.remove();
          } catch (e) {
            log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
          }
          Commons.appFinish();
          return true;
        }
      },
      child: lScaffold(
        key: _scaffoldKey,
        appBar: mainAppBar(pushProvider.badgeNotify, title: title, titleWidget: titleWidget, onBadge: () {
          pushProvider.setBadgeNotifyClear();
          onUpDate();
          lLoginCheck(context, () {
            Commons.page(context, routeNotiCenter, arguments: NotiCenterPage(data: data));
            FbaManager.addLogEvent(viewName: routeHome, type: FbaLogType.menu, parameters: {FbaLogType.click.name: "알림".tr()});
          });
        },
            hide: tabIndex == TAB_COMMUNITY && tabIndex == TAB_SCHEDULE,
            leading: lInkWell(
              onTap: () => openDrawerMyPage(),
              child: Container(padding: EdgeInsets.fromLTRB(5, 12, 0, 12), child: Lcons.mypage(color: Commons.getColor(), disableColor: Commons.getColor(), isEnabled: Commons.isLinkMom())),
            ),
            // leading: lInkWell(
            //   onTap: () => openDrawerMyPage(),
            //   child: Container(
            //       padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
            //       child: Row(
            //         children: [
            //           Lcons.mode_mypage(
            //             color: Commons.getColor(),
            //             disableColor: Commons.getColor(),
            //             isEnabled: Commons.isLinkMom(),
            //             size: 28,
            //           ),
            //           FlutterSwitch(
            //             width: 55,
            //             height: 22,
            //             padding: 2.0,
            //             toggleColor: color_white,
            //             activeColor: color_linkmom,
            //             inactiveColor: color_momdady,
            //             toggleSize: 20,
            //             value: Commons.isLinkMom(),
            //             showOnOff: true,
            //             valueFontSize: 9,
            //             activeText: "링크쌤".tr(),
            //             activeTextColor: color_white,
            //             inactiveText: "맘대디".tr(),
            //             inactiveTextColor: color_white,
            //             onToggle: (_) => openDrawerMyPage(),
            //           )
            //         ],
            //       )),
            // ),
            icon: Lcons.notification(color: color_222222, size: 30)),
        body: IndexedStack(
          index: tabIndex,
          children: getTabList(subIndex),
        ),
        drawer: MyPage(data: data),
        //왼쪽 상단 메뉴
        bottomNavigationBar: _tabNavigationBar(_provider),
        floatingActionButton: testModeView(),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      ),
    );
  }

  openDrawerMyPage() {
    _scaffoldKey.currentState!.openDrawer();
    FbaManager.addLogEvent(viewName: routeHome, type: FbaLogType.menu, parameters: {FbaLogType.click.name: "마이페이지".tr()});
  }

  Widget _tabNavigationBar(HomeNavigationProvider provider) {
    Provider.of<PushModel>(context, listen: true); //Badge처리용
    return BottomNavigationBar(
      selectedLabelStyle: st_11(fontWeight: FontWeight.w700),
      unselectedLabelStyle: st_11(),
      backgroundColor: Colors.white,
      items: [
        BottomNavigationBarItem(label: "홈".tr(), icon: Lcons.nav_home(isEnabled: tabIndex == TAB_HOME, color: Commons.getColor())),
        BottomNavigationBarItem(label: "커뮤니티".tr(), icon: Lcons.nav_community(isEnabled: tabIndex == TAB_COMMUNITY, color: Commons.getColor())),
        BottomNavigationBarItem(label: Commons.isLinkMom() ? "근무관리".tr() : "돌봄관리".tr(), icon: Lcons.nav_care_schedule(isEnabled: tabIndex == TAB_SCHEDULE, color: Commons.getColor())),
        BottomNavigationBarItem(label: Commons.isLinkMom() ? "맘대디찾기".tr() : "링크쌤찾기".tr(), icon: Lcons.nav_linkmom_search(isEnabled: tabIndex == TAB_LIST, color: Commons.getColor())),
        BottomNavigationBarItem(
          label: "채팅".tr(),
          icon: Stack(
            alignment: Alignment.topRight,
            children: [
              Center(
                child: Lcons.nav_chat(isEnabled: tabIndex == TAB_CHAT, color: Commons.getColor()),
              ),
              lBadge(pushProvider.badgeChat, isIcon: false, top: 0.0, end: 20.0),
            ],
          ),
        ),
      ],
      onTap: (index) {
        FbaManager.addLogEvent(viewName: routeHome, type: FbaLogType.menu, parameters: {FbaLogType.click.name: EnumUtils.getMenuType(index).name});
        if (index == TAB_COMMUNITY || index == TAB_LIST) {
          if (!Commons.isGpsWithAddressCheck(context)) {
            return;
          }
        }
        Commons.penaltyCheck(() {
          setState(() {
            tabIndex = index;
            provider.currentIndex = index;
            setTabs();
          });
        });
      },
      currentIndex: tabIndex,
      selectedItemColor: Commons.getColor(),
      unselectedItemColor: color_808080,
      type: BottomNavigationBarType.fixed,
      // 타입에 따라 확대 모드로 가능하다.
    );
  }

  ///디버깅 테스트
  var testMode = 0;

  void setTabs() {
    titleWidget = null;
    duration = 5;
    switch (tabIndex) {
      case TAB_HOME:
        title = '';
        duration = 2;
        if (Commons.isDebugMode) {
          titleWidget = InkWell(
            onLongPress: () {
              Commons.isDebugViewMode = !Commons.isDebugViewMode;
              onUpDate();
            },
            onDoubleTap: () {
              Commons.setTestMode(callBack: () async {
                pushFinger.removeId();
                storageHelper.setUserType(USER_TYPE.mom_daddy);
                await auth.requestLogout(context);
              });
            },
            onTap: () {
              testMode++;
              if (testMode > 10) {
                Commons.setTestMode(callBack: () async {
                  pushFinger.removeId();
                  storageHelper.setUserType(USER_TYPE.mom_daddy);
                  await auth.requestLogout(context);
                });
                testMode = 0;
              } else if (testMode > 5) {
                Commons.showSnackBar(context, '서버 선택 10번 클릭!! : (${testMode.toString()})');
              }
            },
            child: Stack(children: [
              image(imagePath: IMG_LOGO, width: double.infinity, height: 30),
            ]),
          );
        } else {
          if (kDebugMode && !Commons.isDebugMode) {
            titleWidget = InkWell(
              onLongPress: () async {
                storageHelper.setDebug(true);
                Commons.isDebugMode = true;
                Commons.showToast('디버그 모드 ${await storageHelper.getDebug()}');
                onUpDate();
              },
              child: Stack(children: [
                image(imagePath: IMG_LOGO, width: double.infinity, height: 30),
              ]),
            );
          } else {
            titleWidget = image(imagePath: IMG_LOGO, width: double.infinity, height: 30);
          }
        }
        break;
      case TAB_COMMUNITY:
        title = "커뮤니티".tr();
        break;
      case TAB_SCHEDULE:
        if (Commons.isLinkMom()) {
          title = "근무관리".tr();
        } else {
          title = "돌봄관리".tr();
        }
        break;
      case TAB_LIST:
        if (Commons.isLinkMom()) {
          title = "맘대디찾기".tr();
        } else {
          title = "링크쌤찾기".tr();
        }
        break;
      case TAB_CHAT:
        title = "채팅".tr();
        pushProvider.setBadgeChatClear();
        break;
    }
  }
}
