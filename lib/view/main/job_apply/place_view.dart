import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:linkmom/data/network/models/addr_search_response.dart';
import 'package:linkmom/data/network/models/temp_area_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_movetype_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/picker/date_picker.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/lcons.dart';

import '../../../main.dart';
import 'temp_area/temp_area_page.dart';
import 'temp_area/temp_area_view_page.dart';

class PlaceAction {
  MoveViewType? viewType;
  String? address;
  dynamic? data;

  PlaceAction({this.viewType, this.address, this.data});

  @override
  String toString() {
    return 'PlaceAction{viewType: $viewType, address: $address, data: $data}';
  }
}

///아이와 만나는 장소, 출발시간, 장소설명, 도착장소, 추가설명
Widget placeView(BuildContext context, SingleItem item, DataManager data, ServiceType serviceType, PossibleArea areaType, {Function? callBack}) {
  log.d('『GGUMBI』>>> placeView : data.content: ${item.content},  \ncallBack: $callBack,<<< ');
  log.d('『GGUMBI』>>> placeView : data.jobItem.startTime: ${data.jobItem.startTime},  <<< ');
  log.d('『GGUMBI』>>> placeView : data.jobItem.endTime: ${data.jobItem.endTime},  <<< ');
  bool isStart = isStartEnable(serviceType, areaType);
  bool isEnd = isEndEnable(serviceType, areaType);
  bool isStartAddress = StringUtils.validateString(data.tcPlaceStartDetail.text);
  bool isEndAddress = StringUtils.validateString(data.tcPlaceEndDetail.text);
  bool isStartSelect = StringUtils.validateString(data.jobItem.temp_area_start.temp_areaname);
  bool isEndSelect = StringUtils.validateString(data.jobItem.temp_area_end.temp_areaname);
  bool isAdd = data.jobItem.tempAreaLength < 10;
  isStartAddress = isAdd && isStartAddress && !isStartSelect ? true : false;
  isEndAddress = isAdd && isEndAddress && !isEndSelect ? true : false;
  String startAddressTitle = getStartAddressTitle(serviceType, areaType);
  return Column(
    children: [
      //출발장소
      Padding(
        padding: padding_20_LR,
        child: Column(
          children: [
            Row(
              children: [
                lTextIcon(
                  title: startAddressTitle,
                  rightIcon: !isStart ? null : requiredText(),
                ),
                sb_w_10,
                if (isStart)
                  lBtnWrap(
                    isStartSelect ? "장소변경".tr() : "장소선택".tr(),
                    height: 30,
                    padding: padding_10_LR,
                    textStyle: st_b_14(textColor: color_999999, fontWeight: FontWeight.w500),
                    btnEnableColor: color_eeeeee.withOpacity(0.42),
                    sideEnableColor: color_dedede.withOpacity(0.42),
                    onClickAction: () async {
                      var result = await Commons.page(context, routeTempAreaView, arguments: TempAreaViewPage(viewMode: ViewMode(viewType: ViewType.apply), tempAreaData: data.jobItem.temp_area_start));
                      if (result != null && result is TempAreaResultData) {
                        var _data = result.data as TempAreaData;
                        data.jobItem.temp_area_start = _data;
                        data.tcPlaceStart.text = _data.temp_address;
                        data.tcPlaceStartDetail.text = _data.temp_address_detail;
                        data.jobItem.tempAreaLength = result.tempAreaLength;
                        callBack!(PlaceAction(viewType: MoveViewType.startSelect, data: _data));
                      } else if (result != null && result is List<TempAreaData>) {
                        data.jobItem.tempAreaLength = result.length;
                        callBack!(PlaceAction(viewType: MoveViewType.startUpdate, data: result));
                      }
                    },
                  ),
              ],
            ),
            isStartSelect
                ? Column(
                    children: [
                      sb_h_10,
                      Row(
                        children: [
                          Lcons.location(color: Commons.getColor()),
                          Expanded(
                              child: lAutoSizeText(
                            data.jobItem.temp_area_start.temp_areaname,
                            style: st_b_16(textColor: Commons.getColor(), fontWeight: FontWeight.bold),
                          )),
                        ],
                      ),
                    ],
                  )
                : Center(),
            Form(
              key: data.formPlaceStart,
              child: TextFormField(
                textAlignVertical: TextAlignVertical.center,
                controller: data.tcPlaceStart,
                style: stLogin,
                readOnly: true,
                // validator: isStart ? StringUtils.validateAddress : null,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.emailAddress,
                decoration: underLineDecoration(
                  getStartAddressTitleHint(serviceType, areaType),
                  hintStyle: st_b_16(textColor: isStart ? color_aaaaaa : color_030303),
                  icon: isStart ? Lcons.search(disableColor: color_222222) : lText(''),
                  suffixIconConstraints: true,
                  isFn: isStart ? true : false,
                  focus: true,
                  width: 1.0,
                  focusColor: color_dbdbdb,
                  counterText: '',
                ),
                onTap: isStart
                    ? () async {
                        Documents resultData = await Commons.page(context, routeAddressSearch);
                        data.tcPlaceStart.text = resultData.getAddress();
                        data.tcPlaceStartDetail.text = '';
                        callBack!(PlaceAction(viewType: MoveViewType.start, address: data.tcPlaceStart.text));
                      }
                    : null,
                onChanged: (value) {
                  if (callBack != null) {
                    callBack();
                  }
                },
                onEditingComplete: () {
                  if (callBack != null) {
                    callBack();
                  }
                  focusClear(context);
                },
              ),
            ),
            if (isStart) sb_h_05,
            //주소 상세
            if (_showDetailChecker(serviceType, areaType, true))
              InkWell(
                onTap: isStartSelect
                    ? () {
                        if (isStartSelect) {
                          Commons.showToast("상세주소_입력_안내".tr(), toastLength: Toast.LENGTH_LONG);
                        }
                      }
                    : null,
                child: Container(
                  margin: padding_20_B,
                  child: Form(
                    key: data.formPlaceStartDetail,
                    child: TextFormField(
                      enabled: !isStartSelect,
                      controller: data.tcPlaceStartDetail,
                      style: stLogin,
                      validator: isStart ? StringUtils.validateDetailAddress : null,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.emailAddress,
                      decoration: underLineDecoration(
                        "장소_상세_힌트".tr(),
                        focus: true,
                        counterText: '',
                        suffix: isStartAddress
                            ? Padding(
                                padding: padding_10_L,
                                child: lTextBtn("장소추가".tr(), rightIcon: Lcons.add(disableColor: Commons.getColor(), size: 20), padding: padding_0, bg: color_transparent),
                              )
                            : null,
                        suffixIconConstraints: true,
                        isFn: isStartAddress ? true : false,
                        fn: isStartAddress
                            ? () async {
                                TempAreaData tempAreaData = TempAreaData(temp_address: data.tcPlaceStart.text, temp_address_detail: data.tcPlaceStartDetail.text);
                                var result = await Commons.page(context, routeTempArea, arguments: TempAreaPage(viewMode: ViewMode(viewType: ViewType.apply), tempAreaData: tempAreaData));
                                if (result != null && result is TempAreaData) {
                                  var _data = result;
                                  data.jobItem.temp_area_start = _data;
                                  data.tcPlaceStart.text = _data.temp_address;
                                  data.tcPlaceStartDetail.text = _data.temp_address_detail;
                                  callBack!(PlaceAction(viewType: MoveViewType.startSelect, data: _data));
                                }
                              }
                            : null,
                      ),
                      onChanged: (value) {
                        data.formPlaceStartDetail.currentState!.validate();
                        callBack!(PlaceAction(viewType: MoveViewType.startDetail, data: data.tcPlaceStartDetail.text));
                      },
                      onEditingComplete: () {
                        data.formPlaceStartDetail.currentState!.validate();
                        focusClear(context);
                        callBack!(PlaceAction(viewType: MoveViewType.startDetail, data: data.tcPlaceStartDetail.text));
                      },
                    ),
                  ),
                ),
              ),
            //돌봄 유형이 보육이 아니면 보여준다.
            if (!Commons.isBoyuk(serviceType))
              Padding(
                padding: isStart ? padding_0 : padding_20_T, //돌봄장소가 이웃인 경우, 출발장소를 링크쌤집 주소로 직접등록할 경우
                child: Column(
                  children: [
                    lTextIcon(
                      title: "출발시간".tr(),
                      rightIcon: requiredText(),
                    ),
                    Form(
                      key: data.formPlaceStartTime,
                      child: TextFormField(
                        controller: data.tcPlaceStartTime,
                        style: stLogin,
                        readOnly: true,
                        // validator: StringUtils.validateDetailAddress,
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.emailAddress,
                        decoration: underLineDecoration(getStartTimeHint(areaType), focusColor: color_dbdbdb, width: 1.0),
                        onTap: () {
                          showPickerDate(
                            context,
                            valueTime: data.jobItem.startTime,
                            endTime: data.jobItem.endTime,
                            minValue: StringUtils.getTimeToDateTime(data.jobItem.reqdata.careschedule!.first.stime),
                            maxValue: StringUtils.getTimeToDateTime(data.jobItem.reqdata.careschedule!.first.etime),
                            onClick: (dataTime) {
                              DateTime time = dataTime as DateTime;
                              data.tcPlaceStartTime.text = StringUtils.getTimeToString(time);
                              callBack!(PlaceAction(viewType: MoveViewType.startTime, data: dataTime));
                            },
                          );
                        },
                        onEditingComplete: () {
                          if (callBack != null) {
                            callBack();
                          }
                          focusClear(context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            if (Commons.isBoyuk(serviceType)) sb_h_20,
            //장소설명
            lIconText(
              title: "추가설명".tr(),
            ),
            TextFormField(
              controller: data.tcPlaceStartComment,
              style: stLogin,
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.emailAddress,
              decoration: underLineDecoration(!Commons.isBoyuk(serviceType) ? "출발추가설명_힌트".tr() : "보육추가설명_힌트".tr(), focus: true, counterText: ''),
              onChanged: (value) {
                callBack!(PlaceAction(viewType: MoveViewType.startMsg, data: data.tcPlaceStartComment.text));
              },
              onEditingComplete: () {
                focusClear(context);
                callBack!(PlaceAction(viewType: MoveViewType.startMsg, data: data.tcPlaceStartComment.text));
              },
            ),
          ],
        ),
      ),

      sb_h_20,
      lDivider(color: color_e5e5ea.withOpacity(0.38), thickness: 8.0, padding: padding_20_TB),

      ///경유지 (하원, 보육시 사용)
      if (serviceType == ServiceType.serviceType_1 || Commons.isBoyuk(serviceType))
        Column(
          children: [
            //도착해야하는 장소
            Padding(
              padding: padding_20_LR,
              child: Column(
                children: [
                  lIconText(
                      icon: Lcons.check_circle(isEnabled: data.isSelect, color: Commons.getColor(), disableColor: color_cecece, size: 25),
                      title: "경유지장소".tr(),
                      onClick: () {
                        data.isSelect = !data.isSelect;
                        callBack!(PlaceAction(viewType: MoveViewType.check));
                      }),
                  sb_h_10,
                  Form(
                    key: data.formPlacePathRoute,
                    child: TextFormField(
                      textAlignVertical: TextAlignVertical.center,
                      controller: data.tcPlacePathRoute,
                      maxLines: 3,
                      style: st_14(textColor: data.isSelect ? color_222222 : color_dedede),
                      enabled: data.isSelect,
                      validator: StringUtils.validateAddress,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.emailAddress,
                      decoration: bgDecoration("경유지장소_힌트".tr(), bgColor: color_white, textColor: data.isSelect ? color_222222 : color_b2b2b2, isLine: true, lineColor: color_dedede, radius: 6),
                      onChanged: (value) {
                        callBack!(PlaceAction(viewType: MoveViewType.routeAdd, data: data.tcPlacePathRoute.text));
                      },
                      onEditingComplete: () {
                        focusClear(context);
                        callBack!(PlaceAction(viewType: MoveViewType.routeAdd, data: data.tcPlacePathRoute.text));
                      },
                    ),
                  ),
                ],
              ),
            ),

            //돌봄 유형이 보육이 아니면 보여준다.
            if (!Commons.isBoyuk(serviceType))
              Column(
                children: [
                  sb_h_10,
                  lDivider(color: color_e5e5ea.withOpacity(0.38), thickness: 8.0, padding: padding_20_TB),
                  sb_h_20,
                ],
              ),
          ],
        ),

      //돌봄 유형이 보육이 아니면 보여준다.
      if (!Commons.isBoyuk(serviceType))
        //도착해야하는 장소
        Padding(
          padding: padding_20_LR,
          child: Column(
            children: [
              Row(
                children: [
                  lTextIcon(
                    title: "도착장소".tr(),
                    rightIcon: !isEnd ? null : requiredText(),
                  ),
                  sb_w_10,
                  if (isEnd)
                    lBtnWrap(
                      isEndSelect ? "장소변경".tr() : "장소선택".tr(),
                      height: 30,
                      padding: padding_10_LR,
                      textStyle: st_b_14(textColor: color_999999, fontWeight: FontWeight.w500),
                      btnEnableColor: color_eeeeee.withOpacity(0.42),
                      sideEnableColor: color_dedede.withOpacity(0.42),
                      onClickAction: () async {
                        var result = await Commons.page(context, routeTempAreaView, arguments: TempAreaViewPage(viewMode: ViewMode(viewType: ViewType.apply), tempAreaData: data.jobItem.temp_area_end));
                        if (result != null && result is TempAreaResultData) {
                          var _data = result.data as TempAreaData;
                          data.jobItem.temp_area_end = _data;
                          data.tcPlaceEnd.text = _data.temp_address;
                          data.tcPlaceEndDetail.text = _data.temp_address_detail;
                          data.jobItem.tempAreaLength = result.tempAreaLength;
                          callBack!(PlaceAction(viewType: MoveViewType.endSelect, data: _data));
                        } else if (result != null && result is List<TempAreaData>) {
                          data.jobItem.tempAreaLength = result.length;
                          callBack!(PlaceAction(viewType: MoveViewType.endUpdate, data: result));
                        }
                      },
                    ),
                ],
              ),

              isEndSelect
                  ? Column(
                      children: [
                        sb_h_10,
                        Row(
                          children: [
                            Lcons.location(color: Commons.getColor()),
                            Expanded(
                                child: lAutoSizeText(
                              data.jobItem.temp_area_end.temp_areaname,
                              style: st_b_16(textColor: Commons.getColor(), fontWeight: FontWeight.bold),
                            )),
                          ],
                        ),
                      ],
                    )
                  : Center(),

              Form(
                key: data.formPlaceEnd,
                child: TextFormField(
                  textAlignVertical: TextAlignVertical.center,
                  controller: data.tcPlaceEnd,
                  style: stLogin,
                  readOnly: true,
                  // validator: isEnd ? StringUtils.validateAddress : null,
                  textInputAction: TextInputAction.done,
                  keyboardType: TextInputType.emailAddress,
                  decoration: underLineDecoration(
                    getEndAddressTitleHint(serviceType, areaType),
                    hintStyle: st_16(textColor: isEnd ? color_aaaaaa : color_030303),
                    icon: isEnd ? Lcons.search(disableColor: color_222222) : lText(''),
                    suffixIconConstraints: true,
                    isFn: isEnd ? true : false,
                    focus: true,
                    width: 1.0,
                    focusColor: color_dbdbdb,
                    counterText: '',
                  ),
                  onTap: isEnd
                      ? () async {
                          Documents resultData = await Commons.page(context, routeAddressSearch);
                          data.tcPlaceEnd.text = resultData.getAddress();
                          data.tcPlaceEndDetail.text = '';
                          callBack!(PlaceAction(viewType: MoveViewType.end, address: data.tcPlaceEnd.text));
                        }
                      : null,
                  onChanged: (value) {
                    if (callBack != null) {
                      callBack();
                    }
                  },
                  onEditingComplete: () {
                    if (callBack != null) {
                      callBack();
                    }
                    focusClear(context);
                  },
                ),
              ),
              if (isEnd) sb_h_05,
              if (_showDetailChecker(serviceType, areaType, false))
                InkWell(
                  onTap: isEndSelect
                      ? () {
                          if (isEndSelect) {
                            Commons.showToast("상세주소_입력_안내".tr(), toastLength: Toast.LENGTH_LONG);
                          }
                        }
                      : null,
                  child: Container(
                    margin: padding_20_B,
                    child: Form(
                      key: data.formPlaceEndDetail,
                      child: TextFormField(
                        enabled: !isEndSelect,
                        controller: data.tcPlaceEndDetail,
                        style: stLogin,
                        validator: isEnd ? StringUtils.validateDetailAddress : null,
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.emailAddress,
                        decoration: underLineDecoration(
                          "장소_상세_힌트".tr(),
                          focus: true,
                          counterText: '',
                          suffix: isEndAddress
                              ? Padding(
                                  padding: padding_10_L,
                                  child: lTextBtn("장소추가".tr(), rightIcon: Lcons.add(disableColor: Commons.getColor(), size: 20), padding: padding_0, bg: color_transparent),
                                )
                              : null,
                          suffixIconConstraints: true,
                          isFn: isEndAddress ? true : false,
                          fn: isEndAddress
                              ? () async {
                                  TempAreaData tempAreaData = TempAreaData(temp_address: data.tcPlaceEnd.text, temp_address_detail: data.tcPlaceEndDetail.text);
                                  var result = await Commons.page(context, routeTempArea, arguments: TempAreaPage(viewMode: ViewMode(viewType: ViewType.apply), tempAreaData: tempAreaData));
                                  if (result != null && result is TempAreaData) {
                                    var _data = result;
                                    data.jobItem.temp_area_end = _data;
                                    data.tcPlaceEnd.text = _data.temp_address;
                                    data.tcPlaceEndDetail.text = _data.temp_address_detail;
                                    callBack!(PlaceAction(viewType: MoveViewType.endSelect, data: _data));
                                  }
                                }
                              : null,
                        ),
                        onChanged: (value) {
                          data.formPlaceEndDetail.currentState!.validate();
                          callBack!(PlaceAction(viewType: MoveViewType.endDetail, data: data.tcPlaceEndDetail.text));
                        },
                        onEditingComplete: () {
                          data.formPlaceEndDetail.currentState!.validate();
                          focusClear(context);
                          callBack!(PlaceAction(viewType: MoveViewType.endDetail, data: data.tcPlaceEndDetail.text));
                        },
                      ),
                    ),
                  ),
                ),
              Padding(
                padding: isEnd ? padding_0 : padding_20_T,
                child: Column(
                  children: [
                    lTextIcon(
                      title: "도착시간".tr(),
                      rightIcon: requiredText(),
                    ),
                    Form(
                      key: data.formPlaceEndTime,
                      child: TextFormField(
                        controller: data.tcPlaceEndTime,
                        style: stLogin,
                        readOnly: true,
                        // validator: StringUtils.validateDetailAddress,
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.emailAddress,
                        decoration: underLineDecoration(getEndTimeHint(areaType), focusColor: color_dbdbdb, width: 1.0),
                        onTap: () {
                          showPickerDate(
                            context,
                            valueTime: data.jobItem.endTime,
                            startTime: data.jobItem.startTime,
                            minValue: StringUtils.getTimeToDateTime(data.jobItem.reqdata.careschedule!.first.stime),
                            maxValue: StringUtils.getTimeToDateTime(data.jobItem.reqdata.careschedule!.first.etime),
                            onClick: (dataTime) {
                              DateTime time = dataTime as DateTime;
                              data.tcPlaceEndTime.text = StringUtils.getTimeToString(time);
                              callBack!(PlaceAction(viewType: MoveViewType.endTime, data: dataTime));
                            },
                          );
                        },
                        onEditingComplete: () {
                          if (callBack != null) {
                            callBack();
                          }
                          focusClear(context);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              //장소설명
              lIconText(
                title: "추가설명".tr(),
              ),
              TextFormField(
                controller: data.tcPlaceEndComment,
                style: stLogin,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.emailAddress,
                decoration: underLineDecoration("도착추가설명_힌트".tr(), focus: true, counterText: ''),
                onChanged: (value) {
                  callBack!(PlaceAction(viewType: MoveViewType.endMsg, data: data.tcPlaceEndComment.text));
                },
                onEditingComplete: () {
                  focusClear(context);
                  callBack!(PlaceAction(viewType: MoveViewType.endMsg, data: data.tcPlaceEndComment.text));
                },
              ),
            ],
          ),
        ),

      ///이웃집일 경우 표시
      if (areaType == PossibleArea.link_mom)
        Column(
          children: [
            sb_h_30,
            lDivider(color: color_d4d4d4, thickness: 15.0, padding: padding_20_TB),
            sb_h_20,
            Padding(
              padding: padding_20_LR,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  lTextIcon(
                    title: "원하는링크쌤집위치".tr(),
                    rightIcon: requiredText(),
                  ),
                  sb_h_10,
                  lText("1순위".tr(), style: st_b_14(fontWeight: FontWeight.bold)),
                  Form(
                    key: data.formHomeLocation1,
                    child: TextFormField(
                      textAlignVertical: TextAlignVertical.center,
                      controller: data.tcHomeLocation1,
                      style: stLogin,
                      readOnly: true,
                      // validator: StringUtils.validateDetailAddress,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.emailAddress,
                      decoration: underLineDecoration(
                        "주소선택_힌트".tr(),
                        icon: Lcons.search(disableColor: color_222222),
                        focus: true,
                        focusColor: color_dbdbdb,
                        width: 1.0,
                      ),
                      onTap: () async {
                        Documents resultData = await Commons.nextPage(context, routeName: routeAddressSearch);
                        if (resultData.address != null) {
                          data.tcHomeLocation1.text = resultData.address!.getAddress3depth();
                        }
                        callBack!(PlaceAction(viewType: MoveViewType.location1, address: data.tcHomeLocation1.text, data: resultData));
                      },
                      onEditingComplete: () {
                        if (callBack != null) {
                          callBack();
                        }
                        focusClear(context);
                      },
                    ),
                  ),
                  lText("2순위".tr(), style: st_b_14(fontWeight: FontWeight.bold)),
                  Form(
                    key: data.formHomeLocation2,
                    child: TextFormField(
                      controller: data.tcHomeLocation2,
                      textAlignVertical: TextAlignVertical.center,
                      style: stLogin,
                      readOnly: true,
                      // validator: StringUtils.validateDetailAddress,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.emailAddress,
                      decoration: underLineDecoration(
                        "주소선택_힌트".tr(),
                        icon: Lcons.search(disableColor: color_222222),
                        focus: true,
                        focusColor: color_dbdbdb,
                        width: 1.0,
                      ),
                      onTap: () async {
                        Documents resultData = await Commons.nextPage(context, routeName: routeAddressSearch);
                        if (resultData.address != null) {
                          data.tcHomeLocation2.text = resultData.address!.getAddress3depth();
                        }
                        callBack!(PlaceAction(viewType: MoveViewType.location2, address: data.tcHomeLocation2.text, data: resultData));
                      },
                      onEditingComplete: () {
                        if (callBack != null) {
                          callBack();
                        }
                        focusClear(context);
                      },
                    ),
                  ),
                  sb_h_30,
                  lIconText(
                    // icon: iconClock(),
                    title: "내위치에서링크쌤집거리".tr(),
                  ),
                  sb_h_20,
                  ListMoveTypeSelector(
                    data: data.jobItem.lsJobLocation,
                    count: 3,
                    isOverlap: false,
                    margin: padding_10_LR,
                    onItemClick: (value) {
                      log.d('『GGUMBI』>>> build ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<<  $value');
                      SingleItem _item = value as SingleItem;
                      callBack!(PlaceAction(viewType: MoveViewType.locationType, address: data.tcHomeLocation1.text, data: _item));
                      // //선택하였다가 비활성화시 제일 마지막 아이템으로 보여준다.
                      //   data.jobItem.lsJobMoveType.forEach((value) {
                      //     if (value.isSelected) {
                      //       _item = value;
                      //     }
                      //
                      //     callBack(PlaceAction(viewType: MoveViewType.location2, address: data.tcHomeLocation1.text, data: locations));
                      //   });
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
    ],
  );
}

bool isBoyukLinkMom(ServiceType serviceType, PossibleArea area) {
  return Commons.isBoyuk(serviceType) && area == PossibleArea.link_mom;
}

///출발장소 타이틀
String getStartAddressTitle(ServiceType serviceType, PossibleArea area) {
  String title = "출발장소".tr();
  //보육
  if (Commons.isBoyuk(serviceType)) {
    title = "돌봄주소".tr();
    //등원,하원,학습
    if (area == PossibleArea.mom_daddy) {
      title = "돌봄주소".tr();
    } else if (area == PossibleArea.link_mom) {
      title = "돌봄장소_이웃집".tr();
    }

    //등원,하원,학습
  } else {
    if (area == PossibleArea.mom_daddy) {
      title = "출발장소".tr();
    } else if (area == PossibleArea.link_mom) {
      // title = "이웃집출발장소".tr();
      title = "출발장소".tr();
    } else if (area == PossibleArea.link_mom) {
      // title = "이웃집출발장소".tr();
      title = "출발장소".tr();
    }
  }

  return title;
}

///출발장소 힌트
String getStartAddressTitleHint(ServiceType serviceType, PossibleArea area) {
  String title = "장소_힌트".tr();
  if (isStartEnable(serviceType, area)) {
    title = "장소_힌트".tr();
  } else {
    title = "이웃집출발장소_안내".tr();
  }
  return title;
}

///출발 검색 위젯 설정
bool isStartEnable(ServiceType serviceType, PossibleArea area) {
  bool isEnable = true;
  if (area == PossibleArea.mom_daddy || serviceType == ServiceType.serviceType_1 && area == PossibleArea.link_mom) {
    isEnable = true;
  } else {
    isEnable = false;
  }
  return isEnable;
}

///출발 시간 힌트
String getStartTimeHint(PossibleArea area) {
  String title = "출발시간_힌트".tr();
  // String title = "출발시간_우리집_힌트".tr();
  // if (area == PossibleArea.ourhome.index) {
  //   title = "출발시간_우리집_힌트".tr();
  // } else if (area == PossibleArea.nbhhome.index) {
  //   title = "출발시간_이웃집_힌트".tr();
  // }
  return title;
}

///도착장소 힌트 (하원+이웃집)일 경우 다르게
String getEndAddressTitleHint(ServiceType serviceType, PossibleArea area) {
  String title = "도착장소_힌트".tr();
  if (!isEndEnable(serviceType, area)) {
    title = "이웃집출발장소_안내".tr();
  } else {
    title = "도착장소_힌트".tr();
  }
  return title;
}

///도착 검색 위젯 설정
bool isEndEnable(ServiceType serviceType, PossibleArea area) {
  log.d('『GGUMBI』>>> isEndEnable : serviceType: $serviceType, area: $area, <<< ');
  bool isEnable = true;
  if (serviceType == ServiceType.serviceType_1 && area == PossibleArea.link_mom) {
    isEnable = false;
  } else {
    isEnable = true;
  }
  return isEnable;
}

///도착 시간 힌트
String getEndTimeHint(PossibleArea area) {
  String title = "도착시간_힌트".tr();
  if (area == PossibleArea.mom_daddy) {
    title = "도착시간_힌트".tr();
  } else if (area == PossibleArea.link_mom) {
    title = "도착시간_힌트".tr();
  }
  return title;
}

bool _showDetailChecker(ServiceType serviceType, PossibleArea areaType, bool isArrive) {
  if (areaType == PossibleArea.mom_daddy) return true;
  switch (serviceType) {
    case ServiceType.serviceType_0:
      return isArrive ? areaType != PossibleArea.link_mom : areaType == PossibleArea.link_mom;
    case ServiceType.serviceType_1:
      return isArrive ? areaType == PossibleArea.link_mom : areaType != PossibleArea.link_mom;
    case ServiceType.serviceType_2:
      return areaType != PossibleArea.link_mom;
    default:
      return true;
  }
}

bool isConfirm(DataManager data, ServiceType serviceType, PossibleArea areaType) {
  bool isConfirm = false;
  bool isMoveType = false;
  data.jobItem.lsJobMoveType.asMap().forEach((index, value) {
    if (value.isSelected) {
      isMoveType = true;
    }
  });
  if (Commons.isBoyuk(serviceType)) {
    bool isLocation = false;
    if (StringUtils.validateString(data.tcHomeLocation1.text) || StringUtils.validateString(data.tcHomeLocation2.text)) {
      isLocation = true;
    }

    bool isStart = false;
    if (StringUtils.validateString(data.tcPlaceStart.text)) {
      isStart = true;
    }
    bool isStartDetail = false;
    if (StringUtils.validateString(data.tcPlaceStartDetail.text)) {
      isStartDetail = true;
    }

    //보육+우리집일 경우는 무시
    if (areaType == PossibleArea.mom_daddy) {
      isLocation = true;
    } else if (areaType == PossibleArea.link_mom) {
      isStart = true;
      isStartDetail = true;
    }

    if (isLocation && isStart && isStartDetail) {
      isConfirm = true;
    } else {
      isConfirm = false;
    }
  } else {
    //등원 우리집일 경우, 출발지와 상세 입력이 필수
    if (areaType == PossibleArea.mom_daddy) {
      bool isStart = false;
      if (StringUtils.validateString(data.tcPlaceStart.text)) {
        isStart = true;
      }

      bool isStartDetail = false;
      if (StringUtils.validateString(data.tcPlaceStartDetail.text)) {
        isStartDetail = true;
      }

      bool isStartTime = false;
      if (StringUtils.validateString(data.tcPlaceStartTime.text)) {
        isStartTime = true;
      }

      bool isEnd = false;
      if (StringUtils.validateString(data.tcPlaceEnd.text)) {
        isEnd = true;
      }

      bool isEndDetail = false;
      if (StringUtils.validateString(data.tcPlaceEndDetail.text)) {
        isEndDetail = true;
      }

      bool isEndTime = false;
      if (StringUtils.validateString(data.tcPlaceEndTime.text)) {
        isEndTime = true;
      }

      if (isMoveType && isStart && isStartDetail && isStartTime && isEnd && isEndDetail && isEndTime) {
        isConfirm = true;
      } else {
        isConfirm = false;
      }

      //이웃집 경우, 1순위 또는 2순위가 있으면 ok
    } else if (areaType == PossibleArea.link_mom) {
      bool isLocation = false;
      if (StringUtils.validateString(data.tcHomeLocation1.text) || StringUtils.validateString(data.tcHomeLocation2.text)) {
        isLocation = true;
      }

      bool isStartTime = false;
      if (StringUtils.validateString(data.tcPlaceStartTime.text)) {
        isStartTime = true;
      }

      bool isEnd = false;
      if (StringUtils.validateString(data.tcPlaceEnd.text)) {
        isEnd = true;
      }
      bool isEndDetail = false;
      if (StringUtils.validateString(data.tcPlaceEndDetail.text)) {
        isEndDetail = true;
      }
      //하원+이웃집일 경우는 무시
      if (serviceType == ServiceType.serviceType_1 && areaType == PossibleArea.link_mom) {
        isEnd = true;
        isEndDetail = true;
      }

      bool isEndTime = false;
      if (StringUtils.validateString(data.tcPlaceEndTime.text)) {
        isEndTime = true;
      }

      if (isMoveType && isLocation && isStartTime && isEnd && isEndDetail && isEndTime) {
        isConfirm = true;
      } else {
        isConfirm = false;
      }
    }
  }

  return isConfirm;
}
