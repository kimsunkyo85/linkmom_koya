import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/area_code_search_request.dart';
import 'package:linkmom/data/network/models/area_code_search_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_address_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../../../main.dart';

///주소 검색(행정동, 시도,시군구, 읍면동)
class AddressPage extends BaseStateful {
  @override
  _AddressPageState createState() => _AddressPageState();

  AddressPage({this.data, this.viewMode, this.sidoCode, this.sigunguCode});

  final DataManager? data;
  final ViewMode? viewMode;
  final String? sidoCode;
  final String? sigunguCode;
}

class _AddressPageState extends BaseStatefulState<AddressPage> {
  String sidoCode = '';
  String sigunguCode = '';
  String dong = '';

  @override
  initState() {
    super.initState();

    log.d('『GGUMBI』>>> initState :     storageHelper.addressData: ${storageHelper.addressData.length} ${storageHelper.addressData},  <<< ');
    if (storageHelper.addressData.isEmpty) {
      storageHelper.getAddressData().then((value) {
        value.forEach((value) {
          data.addressItem.lsAddress1.add(SingleItem(value.address1, content: value.address1_id));
          onUpDate();
        });
      });
    } else {
      storageHelper.addressData.forEach((value) {
        data.addressItem.lsAddress1.add(SingleItem(value.address1, content: value.address1_id));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        resizeToAvoidBottomPadding: false,
        appBar: appBar("위치검색".tr(), isClose: false),
        body: lContainer(
          child: Column(
            children: [
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    lDivider(color: color_eeeeee),
                    Expanded(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(
                            flex: 20,
                            child: Column(
                              children: [
                                sb_h_20,
                                lText("시/도".tr(), style: st_b_15()),
                                sb_h_20,
                                lDivider(color: color_dedede),
                                Expanded(
                                  child: ListAddressSelector(
                                    data: data.addressItem.lsAddress1,
                                    enableColor: Commons.getColor(),
                                    bgColor: Commons.getColorBg(),
                                    onItemClick: (value) {
                                      SingleItem item = value as SingleItem;
                                      data.addressItem.lsAddress2.clear();
                                      data.addressItem.lsAddress3.clear();
                                      data.addressItem.codeSearchData = null;
                                      storageHelper.addressData.forEach((valueData) {
                                        if (item.name == valueData.address1) {
                                          valueData.data!.forEach((value) {
                                            if (StringUtils.validateString(value.address2)) {
                                              sidoCode = item.content; //코드 입력
                                              sigunguCode = '';
                                              data.addressItem.lsAddress2.add(SingleItem(value.address2, content: value.address2_id));
                                            }
                                          });
                                        }
                                      });
                                      onConfirmBtn();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                          lDividerVertical(color: color_dedede, thickness: 1),
                          Expanded(
                            flex: 35,
                            child: Column(
                              children: [
                                sb_h_20,
                                lText("시/구/군".tr(), style: st_b_15()),
                                sb_h_20,
                                lDivider(color: color_dedede),
                                Expanded(
                                  child: ListAddressSelector(
                                    data: data.addressItem.lsAddress2,
                                    alignment: Alignment.centerLeft,
                                    padding: padding_10_LR,
                                    enableColor: Commons.getColor(),
                                    bgColor: Commons.getColorBg(),
                                    onItemClick: (value) {
                                      SingleItem item = value as SingleItem;
                                      sigunguCode = item.content; //코드 입력
                                      data.addressItem.codeSearchData = null;
                                      _requestCodeSearch('$sidoCode/$sigunguCode');
                                      onConfirmBtn();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                          lDividerVertical(color: color_dedede, thickness: 1),
                          Expanded(
                            flex: 45,
                            child: Column(
                              children: [
                                sb_h_20,
                                lText("동/읍/면".tr(), style: st_b_15()),
                                sb_h_20,
                                lDivider(color: color_dedede),
                                Expanded(
                                  child: ListAddressSelector(
                                    data: data.addressItem.lsAddress3,
                                    padding: padding_10_LR,
                                    alignment: Alignment.centerLeft,
                                    enableColor: Commons.getColor(),
                                    bgColor: Commons.getColorBg(),
                                    onItemClick: (value) {
                                      // log.d('『GGUMBI』>>> build : data: $value,  <<< ');
                                      // log.d('『GGUMBI』>>> build : value as SingleItem: ${value as SingleItem},  <<< ');
                                      SingleItem item = value as SingleItem;
                                      data.addressItem.codeSearchData = item.data as AreaCodeSearchData;
                                      log.d('『GGUMBI』>>> build : item.data as AreaCodeSearchData: ${item.data as AreaCodeSearchData},  <<< ');
                                      log.d('『GGUMBI』>>> build : data: $data,  ${data.addressItem.codeSearchData}<<< ');
                                      onConfirmBtn();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              _confirmButton(),
            ],
          ),
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn("확인".tr(), isEnabled: flag.isConfirm, onClickAction: () {
      setViewType(ViewType.apply);

      // datas.reqdata = MapEntry("test", 'test1').toString();
      // var jsonString = '{"test": "test"}';
      // datas.reqdata = jsonString;
      // datas.reqdata = jsonEncode(reqdata);
      // apiHelper.requestTempSave(data.jobItem.request).then((value) => Commons.page(context, routeParents2, arguments: ParentsStep2Page(data: data)));
      log.d('『GGUMBI』>>> _confirmButton : data.addressItem.codeSearchData: ${data.addressItem.codeSearchData},  <<< ');
      Commons.pagePop(context, data: data);
    });
  }

  @override
  void onConfirmBtn() {
    bool isSido = false;
    if (StringUtils.validateString(sidoCode)) {
      isSido = true;
    }

    bool isSigungu = false;
    if (StringUtils.validateString(sigunguCode)) {
      isSigungu = true;
    }

    log.d('『GGUMBI』>>> onConfirmBtn : data.addressItem.codeSearchData: ${data.addressItem.codeSearchData},  <<< ');

    ///5/17/21 시군구까지 선택하더라도, 행정동까지 선택하게끔 처리 -> 실제 운영에서 동이 아닌 시,구,읍까지만 열어준다.
    // if(storageHelper.user_type == USER_TYPE.link_mom){
    //   data.addressItem.codeSearchData = AreaCodeSearchData();
    // }

    ///행정동 3개까지 선택하기
    if (isSido && isSigungu && data.addressItem.codeSearchData != null) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  ///행정동검색하기(위치검색 1순위, 2순위)
  _requestCodeSearch(String code) {
    // flag.enableLoading(fn: () => onUpDate());//로딩 다이얼로그 사용시 주석 제거
    apiHelper.requestAreaCodeSearch(code, data: AreaCodeSearchRequest(area: "")).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        data.addressItem.lsAddress3.clear();
        response.getDataList().forEach((value) {
          String name = value.address3;
          if (!StringUtils.validateString(name)) {
            name = "전체".tr();
          } else {
            if (StringUtils.validateString(value.area_name)) {
              name = '$name(${value.area_name})';
            }
          }
          data.addressItem.lsAddress3.add(SingleItem(name, content: value.hcode, data: value));
          // if (StringUtils.validateString(value.address3)) {
          //   String name = value.address3;
          //   if (StringUtils.validateString(value.area_name)) {
          //     name = '$name(${value.area_name})';
          //   }
          //   data.addressItem.lsAddress3.add(SingleItem(name, content: value.area_code, data: value));
          // }
        });
        onUpDate();
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }
}
