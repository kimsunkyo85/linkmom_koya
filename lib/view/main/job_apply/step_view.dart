import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

///진행상황, 아이정보 뷰 돌봄신청 컨텐츠 제목(필요한 돌봄 유형을 선택해주세요), Step 화면
Widget StepView({int? valueStep, EdgeInsets margin = stepMargin, EdgeInsetsGeometry padding = padding_10}) {
  String step = "${"step".tr()} $valueStep";
  return lBtnStep(step, margin: margin);
}
