import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_1_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/linkmom_profile_page.dart';

import '../../../../route_name.dart';
import 'guide_view.dart';

class GuideLinkMomPage extends BaseStateful {
  GuideLinkMomPage({this.viewMode});

  @override
  _GuideLinkMomPageState createState() => _GuideLinkMomPageState();

  final ViewMode? viewMode;
}

class _GuideLinkMomPageState extends BaseStatefulState<GuideLinkMomPage> {
  GuideType _guidType = GuideType.guid_0;
  List<SingleItem> _guidList = [];
  CarouselControllerImpl _ctrl = CarouselControllerImpl();

  @override
  void initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());

    _guidList.add(SingleItem(GuideType.guid_0.name, content: GuideType.guid_0.content, data: GuideType.guid_0, isSelected: true, image: IMG_GUIDE_LINKMOM_START));
    _guidList.add(SingleItem(GuideType.guid_1.name, content: GuideType.guid_1.content, data: GuideType.guid_1, image: IMG_GUIDE_LINKMOM_CARE));
    _guidList.add(SingleItem(GuideType.guid_2.name, content: GuideType.guid_2.content, data: GuideType.guid_2, values: [
      SingleItem(IMG_GUIDE_LINKMOM_MATCHING_1),
      SingleItem(IMG_GUIDE_LINKMOM_MATCHING_2),
      SingleItem(IMG_GUIDE_LINKMOM_MATCHING_3),
      SingleItem(IMG_GUIDE_LINKMOM_MATCHING_4),
      SingleItem(IMG_GUIDE_LINKMOM_MATCHING_5),
    ]));
    _guidList.add(SingleItem(GuideType.guid_3.name, content: GuideType.guid_3.content, data: GuideType.guid_3, values: [
      SingleItem(IMG_GUIDE_LINKMOM_DIARY_1),
      SingleItem(IMG_GUIDE_LINKMOM_DIARY_2),
      SingleItem(IMG_GUIDE_LINKMOM_DIARY_3),
      SingleItem(IMG_GUIDE_LINKMOM_DIARY_4),
    ]));
    _guidList.add(SingleItem(GuideType.guid_4.name, content: GuideType.guid_4.content, data: GuideType.guid_4, values: [
      SingleItem(IMG_GUIDE_LINKMOM_PAY),
    ]));
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        appBar: appBar("링크쌤가이드".tr()),
        body: Column(
          children: [
            Expanded(
              child: CustomScrollView(
                slivers: [
                  SliverAppBar(
                    leading: Center(),
                    backgroundColor: color_transparent,
                    shadowColor: color_transparent,
                    collapsedHeight: 90,
                    expandedHeight: 90,
                    // floating: true,
                    // pinned: true,
                    flexibleSpace: Column(
                      children: [
                        sb_h_05,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            tabTitleView(_guidList[0], () => setState(() => _guidType = selectItem(_guidList, _guidList[0].data, _ctrl))),
                            tabTitleView(_guidList[1], () => setState(() => _guidType = selectItem(_guidList, _guidList[1].data, _ctrl))),
                          ],
                        ),
                        sb_h_10,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            tabTitleView(_guidList[2], () => setState(() => _guidType = selectItem(_guidList, _guidList[2].data, _ctrl))),
                            tabTitleView(_guidList[3], () => setState(() => _guidType = selectItem(_guidList, _guidList[3].data, _ctrl))),
                            tabTitleView(_guidList[4], () => setState(() => _guidType = selectItem(_guidList, _guidList[4].data, _ctrl))),
                          ],
                        ),
                      ],
                    ),
                  ),
                  buildView(_guidList, _ctrl, _guidType, () => onUpDate()),
                ],
              ),
            ),
            if (getViewType() != ViewType.view)
              Container(
                padding: Platform.isIOS ? padding_10_B : padding_0,
                color: Commons.getColor(),
                child: lBtn(getViewType() == ViewType.modify ? "링크쌤지원서수정하기".tr() : "링크쌤즉시지원하기".tr(), margin: padding_0, borderRadius: 0, height: 70, style: st_18(fontWeight: FontWeight.bold), onClickAction: () async {
                  if (getViewType() == ViewType.apply) {
                    Commons.page(context, routeCare1, arguments: CareStep1Page(data: data));
                  } else if (getViewType() == ViewType.modify) {
                    var result = await Commons.page(context, routeLinkMomProfile, arguments: LinkMomProfilePage(data: data, viewMode: getViewMode()));
                    Commons.pagePop(context, data: result);
                  }
                }),
              ),
          ],
        ));
  }
}
