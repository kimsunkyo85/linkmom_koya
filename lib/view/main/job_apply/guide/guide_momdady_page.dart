import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/mypage/child_info/child_info_page.dart';

import '../../../../route_name.dart';
import 'guide_view.dart';

class GuideMomDadyPage extends BaseStateful {
  GuideMomDadyPage({this.viewMode});

  @override
  _GuideMomDadyPageState createState() => _GuideMomDadyPageState();
  final ViewMode? viewMode;
}

class _GuideMomDadyPageState extends BaseStatefulState<GuideMomDadyPage> {
  GuideType _guidType = GuideType.guid_0;
  List<SingleItem> _guidList = [];
  CarouselControllerImpl _ctrl = CarouselControllerImpl();

  @override
  void initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    _guidList.add(SingleItem(GuideType.guid_0.name, content: GuideType.guid_0.content, data: GuideType.guid_0, isSelected: true, image: IMG_GUIDE_MOMDADY_START));
    _guidList.add(SingleItem(GuideType.guid_1.name, content: GuideType.guid_1.content, data: GuideType.guid_1, values: [
      SingleItem(IMG_GUIDE_MOMDADY_MATCHING_1),
      SingleItem(IMG_GUIDE_MOMDADY_MATCHING_2),
      SingleItem(IMG_GUIDE_MOMDADY_MATCHING_3),
      SingleItem(IMG_GUIDE_MOMDADY_MATCHING_4),
      SingleItem(IMG_GUIDE_MOMDADY_MATCHING_5),
    ]));
    _guidList.add(SingleItem(GuideType.guid_2.name, content: GuideType.guid_2.content, data: GuideType.guid_2, values: [
      SingleItem(IMG_GUIDE_MOMDADY_CHAT_1),
      SingleItem(IMG_GUIDE_MOMDADY_CHAT_2),
      SingleItem(IMG_GUIDE_MOMDADY_CHAT_3),
      SingleItem(IMG_GUIDE_MOMDADY_CHAT_4),
      SingleItem(IMG_GUIDE_MOMDADY_CHAT_5),
    ]));
    _guidList.add(SingleItem(GuideType.guid_3.name, content: GuideType.guid_3.content, data: GuideType.guid_3, values: [
      SingleItem(IMG_GUIDE_MOMDADY_DIARY),
    ]));
    _guidList.add(SingleItem(GuideType.guid_4.name, content: GuideType.guid_4.content, data: GuideType.guid_4, values: [
      SingleItem(IMG_GUIDE_MOMDADY_PAY_1),
      SingleItem(IMG_GUIDE_MOMDADY_PAY_2),
    ]));
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        appBar: appBar("맘대디가이드".tr()),
        body: Column(
          children: [
            Expanded(
              child: CustomScrollView(
                slivers: [
                  SliverAppBar(
                    leading: Center(),
                    backgroundColor: color_transparent,
                    shadowColor: color_transparent,
                    collapsedHeight: 90,
                    expandedHeight: 90,
                    // floating: true,
                    // pinned: true,
                    flexibleSpace: Column(
                      children: [
                        sb_h_05,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            tabTitleView(_guidList[0], () => setState(() => _guidType = selectItem(_guidList, _guidList[0].data, _ctrl))),
                            tabTitleView(_guidList[1], () => setState(() => _guidType = selectItem(_guidList, _guidList[1].data, _ctrl))),
                          ],
                        ),
                        sb_h_10,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            tabTitleView(_guidList[2], () => setState(() => _guidType = selectItem(_guidList, _guidList[2].data, _ctrl))),
                            tabTitleView(_guidList[3], () => setState(() => _guidType = selectItem(_guidList, _guidList[3].data, _ctrl))),
                            tabTitleView(_guidList[4], () => setState(() => _guidType = selectItem(_guidList, _guidList[4].data, _ctrl))),
                          ],
                        ),
                      ],
                    ),
                  ),
                  buildView(_guidList, _ctrl, _guidType, () => onUpDate()),
                ],
              ),
            ),
            if (getViewType() != ViewType.view)
              Container(
                padding: Platform.isIOS ? padding_10_B : padding_0,
                color: Commons.getColor(),
                child: lBtn("돌봄신청서작성하기".tr(), margin: padding_0, borderRadius: 0, height: 70, style: st_18(fontWeight: FontWeight.bold), onClickAction: () {
                  Commons.page(context, routeChildInfo, arguments: ChildInfoPage(data: data));
                }),
              ),
          ],
        ));
  }
}
