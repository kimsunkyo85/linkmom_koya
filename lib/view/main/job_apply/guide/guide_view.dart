import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/svg_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

///가이드
Widget guidView({
  required List<SingleItem> data,
  required GuideType type,
  required CarouselControllerImpl carouselCtrl,
  Function? onPageChanged,
}) {
  SingleItem item = data[type.value];
  bool isIndicator = item.values != null && item.values!.length > 1;
  bool isData = item.values != null;
  return Column(
    children: [
      _titleView(item.content),
      if (isIndicator)
        Column(
          children: [
            AnimatedSmoothIndicator(
              count: item.values!.length,
              effect: ExpandingDotsEffect(
                spacing: 6,
                activeDotColor: Commons.getColor(),
                dotColor: color_dedede,
                dotWidth: 6,
                dotHeight: 6,
                radius: 8,
              ),
              activeIndex: item.type,
            ),
            sb_h_20,
          ],
        ),
      isData
          ? Padding(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 20),
              child: CarouselSlider(
                carouselController: carouselCtrl,
                items: item.values!
                    .map(
                      (e) => Container(
                        margin: padding_02_LR,
                        child: Image.asset(
                          e.name,
                          alignment: Alignment.topCenter,
                          // fit: BoxFit.fitWidth,
                        ),
                      ),
                    )
                    .toList(),
                options: CarouselOptions(
                  viewportFraction: 1,
                  aspectRatio: 0.5,
                  enableInfiniteScroll: false,
                  onPageChanged: (index, reason) {
                    item.type = index;
                    onPageChanged!();
                  },
                ),
              ),
            )
          : Padding(
              padding: EdgeInsets.fromLTRB(40, 0, 40, 20),
              child: Image.asset(item.image),
            ),
    ],
  );
}

Widget _titleView(String title) {
  return Padding(
    padding: EdgeInsets.fromLTRB(20, 30, 20, 35),
    child: lText(
      title,
      fontWeight: FontWeight.bold,
      color: Commons.getColor(),
      fontSize: 26,
      textAlign: TextAlign.center,
    ),
  );
}

Widget buildView(List<SingleItem> listData, CarouselControllerImpl carouselCtrl, GuideType type, Function fn) => SliverToBoxAdapter(
      child: guidView(data: listData, type: type, carouselCtrl: carouselCtrl, onPageChanged: () => fn()),
    );

Widget tabTitleView(SingleItem item, Function fn) {
  double height = 35;
  GuideType type = item.data;
  return type.value > 1
       ? InkWell(
          onTap: () => fn(),
          child: Padding(
            padding: padding_05_LR,
            child: Stack(
              alignment: Alignment.center,
              children: [
                svgImage(
                  imagePath: item.isSelected ? SVG_BG_PENTAGON_ACTIVE : SVG_BG_PENTAGON_INACTIVE,
                  height: height,
                  color: !Commons.isLinkMom()
                      ? item.isSelected
                          ? Commons.getColor()
                          : null
                      : null,
                ),
                lText(
                  item.name,
                  style: st_13(textColor: item.isSelected ? color_white : color_747474, fontWeight: item.isSelected ? FontWeight.bold : FontWeight.w500),
                ),
              ],
            ),
          ),
        )
      : lBtnWrap(
          item.name,
          textStyle: st_13(textColor: item.isSelected ? color_white : color_747474, fontWeight: item.isSelected ? FontWeight.bold : FontWeight.w500),
          sideEnableColor: item.isSelected ? color_transparent : color_eeeeee,
          sideColor: color_transparent,
          btnEnableColor: item.isSelected ? Commons.getColor() : color_f8f8f8,
          animationDuration: 0,
          padding: padding_12_LR,
          margin: padding_05_LR,
          height: height,
          onClickAction: fn,
        );
}

GuideType selectItem(List<SingleItem> data, GuideType type, CarouselControllerImpl ctrl) {
  data.asMap().forEach((index, value) {
    if (index == type.value) {
      value.isSelected = true;
      if (ctrl.ready) {
        try {
          ctrl.jumpToPage(value.type);
        } catch (e) {}
      }
    } else {
      value.isSelected = false;
    }
  });
  return EnumUtils.getGuideType(type.value);
}
