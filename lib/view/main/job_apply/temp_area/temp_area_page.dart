import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/addr_search_response.dart';
import 'package:linkmom/data/network/models/temp_area_request.dart';
import 'package:linkmom/data/network/models/temp_area_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../../../lcons.dart';

class TempAreaPage extends BaseStateful {
  @override
  _TempAreaPageState createState() => _TempAreaPageState();

  TempAreaPage({this.data, this.viewMode, this.tempAreaData});

  final DataManager? data;
  final ViewMode? viewMode;
  final TempAreaData? tempAreaData;
}

class _TempAreaPageState extends BaseStatefulState<TempAreaPage> {
  var formAddress = GlobalKey<FormState>();
  var formAddressDetail = GlobalKey<FormState>();
  var formAreaName = GlobalKey<FormState>();

  TextEditingController tcAddress = TextEditingController(text: "");
  TextEditingController tcAddressDetail = TextEditingController(text: "");
  TextEditingController tcAreaName = TextEditingController(text: "");

  TempAreaRequest _tempAreaRequest = TempAreaRequest();

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
    onDataPage(widget.tempAreaData);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        resizeToAvoidBottomPadding: false,
        appBar: appBar(isModifyMode() ? "장소수정".tr() : "장소추가".tr(), isClose: false, hide: false),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      padding: padding_Item,
                      child: Column(
                        children: [
                          Form(
                            key: formAddress,
                            child: TextFormField(
                              textAlignVertical: TextAlignVertical.center,
                              controller: tcAddress,
                              style: stLogin,
                              readOnly: true,
                              // validator: isStart ? StringUtils.validateAddress : null,
                              textInputAction: TextInputAction.done,
                              keyboardType: TextInputType.emailAddress,
                              decoration: underLineDecoration(
                                "주소를선택해주세요.".tr(),
                                hintStyle: st_b_16(textColor: color_aaaaaa),
                                icon: Lcons.search(disableColor: color_222222),
                                isFn: true,
                                focus: true,
                                width: 1.0,
                                focusColor: color_dbdbdb,
                                counterText: '',
                                contentPadding: padding_05_LR,
                              ),
                              onTap: () async {
                                Documents resultData = await Commons.nextPage(context, routeName: routeAddressSearch);
                                tcAddress.text = resultData.getAddress();
                                tcAddressDetail.text = '';
                              },
                              onChanged: (value) {
                                onConfirmBtn();
                              },
                              onEditingComplete: () {
                                focusClear(context);
                                onConfirmBtn();
                              },
                            ),
                          ),
                          sb_h_05,
                          //주소 상세
                          Container(
                            margin: padding_30_B,
                            child: Form(
                              key: formAddressDetail,
                              child: TextFormField(
                                controller: tcAddressDetail,
                                style: stLogin,
                                validator: null,
                                textInputAction: TextInputAction.done,
                                keyboardType: TextInputType.emailAddress,
                                decoration: underLineDecoration(
                                  "장소_상세_힌트".tr(),
                                  focus: true,
                                  focusColor: Commons.getColor(),
                                  counterText: '',
                                  contentPadding: padding_05_LR,
                                ),
                                onChanged: (value) {
                                  formAddressDetail.currentState!.validate();
                                  onConfirmBtn();
                                },
                                onEditingComplete: () {
                                  formAddressDetail.currentState!.validate();
                                  focusClear(context);
                                  onConfirmBtn();
                                },
                              ),
                            ),
                          ),

                          sb_h_10,
                          lTextIcon(title: "장소명".tr(), textStyle: st_18(textColor: Commons.getColor(), fontWeight: FontWeight.bold), leftIcon: Lcons.location(color: Commons.getColor())),
                          sb_h_05,
                          //주소 상세
                          Container(
                            margin: padding_30_B,
                            child: Form(
                              key: formAreaName,
                              child: TextFormField(
                                controller: tcAreaName,
                                style: stLogin,
                                validator: null,
                                textInputAction: TextInputAction.done,
                                keyboardType: TextInputType.emailAddress,
                                maxLength: 10,
                                decoration: underLineDecoration(
                                  "장소명_힌트".tr(),
                                  focus: true,
                                  focusColor: Commons.getColor(),
                                  maxLength: 10,
                                  contentPadding: padding_05_LR,
                                ),
                                onChanged: (value) {
                                  formAreaName.currentState!.validate();
                                  onConfirmBtn();
                                },
                                onEditingComplete: () {
                                  formAreaName.currentState!.validate();
                                  focusClear(context);
                                  onConfirmBtn();
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  _confirmButton(),
                ],
              ),
            ),
          ],
        ),
      ),
      isOnlyLoading: true,
    );
  }

  Widget _confirmButton() {
    return lBtn(isModifyMode() ? "수정하기".tr() : "저장하기".tr(), isEnabled: flag.isConfirm, onClickAction: () {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestTempAreaSave(_tempAreaRequest).then((response) async {
        if (response.getCode() == KEY_SUCCESS) {
          if (isModifyMode()) {
            response.getData().area_id = _tempAreaRequest.area_id!;
          }
          Commons.pagePop(context, data: response.getData());
        } else {
          Commons.showSnackBar(context, response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      });
    }, btnColor: Commons.getColor());
  }

  @override
  void onConfirmBtn() {
    bool isAddress = StringUtils.validateString(tcAddress.text);
    bool isAddressDetail = StringUtils.validateString(tcAddressDetail.text);
    bool isAreaName = StringUtils.validateString(tcAreaName.text);

    _tempAreaRequest.temp_address = tcAddress.text;
    _tempAreaRequest.temp_address_detail = tcAddressDetail.text;
    _tempAreaRequest.temp_areaname = tcAreaName.text;

    if (isAddress && isAddressDetail && isAreaName) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  @override
  void onDataPage(_data) {
    if (_data is TempAreaData) {
      _tempAreaRequest.area_id = _data.area_id;
      _tempAreaRequest.temp_address = _data.temp_address;
      _tempAreaRequest.temp_address_detail = _data.temp_address_detail;
      _tempAreaRequest.temp_areaname = _data.temp_areaname;

      log.d('『GGUMBI』>>> onDataPage : _data: $_data,  <<< ');
      tcAddress.text = _data.temp_address;
      tcAddressDetail.text = _data.temp_address_detail;
      tcAreaName.text = _data.temp_areaname;
    }
    onConfirmBtn();
  }
}
