import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/temp_area_request.dart';
import 'package:linkmom/data/network/models/temp_area_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../../../../route_name.dart';
import '../../../lcons.dart';
import 'temp_area_page.dart';

class TempAreaViewPage extends BaseStateful {
  @override
  _TempAreaViewPageState createState() => _TempAreaViewPageState();

  TempAreaViewPage({this.data, this.viewMode, this.tempAreaData});

  final DataManager? data;
  final ViewMode? viewMode;
  final TempAreaData? tempAreaData;
}

class _TempAreaViewPageState extends BaseStatefulState<TempAreaViewPage> {
  TempAreaResponse _tempAreaResponse = TempAreaResponse(dataList: []);

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
    _requestTempAreaView();
  }

  ///재조회
  Future<Null> _onRefresh() async {
    // _visible = false;
    _requestTempAreaView();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        resizeToAvoidBottomPadding: false,
        appBar: appBar(
          isModifyMode() ? "장소관리".tr() : "장소선택".tr(),
          isClose: false,
          hide: false,
          onBack: () => Commons.pagePop(context, data: _tempAreaResponse.getDataList()),
        ),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _tempAreaResponse.getDataList().isNotEmpty
                      ? Padding(
                          padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                          // padding: padding_20,
                          child: lText("장소추가하기_안내".tr(), style: st_15(textColor: color_aaaaaa)),
                        )
                      : Center(),
                  Expanded(
                    child: _tempAreaResponse.getDataList().isNotEmpty
                        ? LinkmomRefresh(
                            onRefresh: _onRefresh,
                            padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                            child: Column(
                              children: _tempAreaResponse.getDataList().map((e) => _areaViewItem(e)).toList(),
                            ),
                          )
                        : flag.isLoading
                            ? Center()
                            : _emptyView(),
                  ),
                  lBtn("장소추가하기".tr(),
                      style: st_b_16(textColor: flag.isConfirm ? color_222222 : color_eeeeee),
                      borderRadius: 0,
                      margin: Platform.isIOS ? EdgeInsets.fromLTRB(0, 5, 0, 15) : padding_05_TB,
                      isEnabled: flag.isConfirm,
                      disabledTextColor: color_eeeeee,
                      highLightBg: color_transparent,
                      highListColor: color_aaaaaa,
                      icon: Container(
                          padding: padding_10,
                          child: Lcons.add_round(
                            disableColor: flag.isConfirm ? Commons.getColor() : color_eeeeee,
                          )), onClickAction: () async {
                    var result = await Commons.page(context, routeTempArea, arguments: TempAreaPage(viewMode: ViewMode(viewType: ViewType.apply)));
                    if (result != null) {
                      _tempAreaResponse.getDataList().insert(0, result);
                      onConfirmBtn();
                    }
                  }, btnColor: color_white, disabledColor: color_transparent),
                ],
              ),
            ),
          ],
        ),
      ),
      isOnlyLoading: true,
    );
  }

  Widget _areaViewItem(TempAreaData _data) {
    bool isSelect = false;
    if (widget.tempAreaData != null) {
      if (widget.tempAreaData!.area_id == _data.area_id) {
        isSelect = true;
      }
    }
    return InkWell(
      onTap: () {
        //메뉴에서 온 경우는 아무런 처리를 하지 않는다.
        if (!isModifyMode()) {
          Commons.pagePop(context, data: TempAreaResultData(data: _data, tempAreaLength: _tempAreaResponse.getDataList().length));
        }
      },
      child: Container(
        decoration: decorationRound(borderColor: isSelect ? Commons.getColor() : color_eeeeee, radius: 20, width: isSelect ? 2 : 1),
        padding: padding_20,
        margin: padding_07_TB,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            lTextIcon(
              leftIcon: Lcons.location(color: Commons.getColor()),
              title: _data.temp_areaname,
              textStyle: st_b_16(textColor: Commons.getColor(), fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(5, 10, 10, 5),
              child: lText(
                '${_data.temp_address} ${_data.temp_address_detail}',
                style: st_b_14(),
                maxLines: 3,
              ),
            ),
            sb_h_10,
            Row(
              children: [
                lBtnWrap(
                  "수정".tr(),
                  height: 35,
                  textStyle: st_b_14(textColor: color_999999, fontWeight: FontWeight.w500),
                  borderRadius: 16,
                  btnEnableColor: color_eeeeee.withOpacity(0.42),
                  sideEnableColor: color_dedede.withOpacity(0.42),
                  onClickAction: () async {
                    var result = await Commons.page(context, routeTempArea, arguments: TempAreaPage(viewMode: ViewMode(viewType: ViewType.modify), tempAreaData: _data));
                    if (result != null) {
                      if (result is TempAreaData) {
                        _tempAreaResponse.getDataList().forEach((value) {
                          if (value.area_id == result.area_id) {
                            value.area_id = result.area_id;
                            value.temp_address = result.temp_address;
                            value.temp_address_detail = result.temp_address_detail;
                            value.temp_areaname = result.temp_areaname;
                            onUpDate();
                          }
                        });
                      }
                    }
                  },
                ),
                Row(
                  children: [
                    sb_w_10,
                    lBtnWrap(
                      "삭제".tr(),
                      height: 35,
                      textStyle: st_b_14(textColor: color_999999, fontWeight: FontWeight.w500),
                      borderRadius: 16,
                      btnEnableColor: color_eeeeee.withOpacity(0.42),
                      sideEnableColor: color_dedede.withOpacity(0.42),
                      onClickAction: () {
                        showNormalDlg(
                          message: "정말삭제하시겠습니까".tr(),
                          btnLeft: "아니오".tr(),
                          btnRight: "네".tr(),
                          onClickAction: (action) {
                            if (action == DialogAction.yes) {
                              flag.enableLoading(fn: () => onUpDate());
                              apiHelper.requestTempAreaDelete(TempAreaRequest(area_id: _data.area_id)).then((response) {
                                if (response.getCode() == KEY_SUCCESS) {
                                  try {
                                    _tempAreaResponse.getDataList().remove(_data);
                                  } catch (e) {
                                    _requestTempAreaView();
                                  }
                                  onConfirmBtn();
                                } else {
                                  Commons.showSnackBar(context, response.getMsg());
                                }
                                flag.disableLoading(fn: () => onUpDate());
                              });
                            }
                          },
                        );
                      },
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _emptyView() {
    return Container(
      width: widthFull(context),
      height: heightFull(context),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Lcons.local_certification_active(size: 64, disableColor: color_b2b2b2),
          sb_h_10,
          Padding(
            padding: padding_10_TB,
            child: lText(
              "등록된장소가없어요".tr(),
              style: st_b_18(textColor: color_999999, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
          lText(
            "등록된장소가없어요_안내".tr(),
            style: st_b_14(textColor: color_999999),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  ///돌봄장소 가져오기 요청
  void _requestTempAreaView() async {
    flag.enableLoading(fn: () => onUpDate());
    await apiHelper.requestTempAreaView().then((response) {
      flag.disableLoading(fn: () => onUpDate());
      _tempAreaResponse.getDataList().clear();
      if (response.getCode() == KEY_SUCCESS) {
        onDataPage(response);
      }
      onConfirmBtn();
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  @override
  void onDataPage(_data) {
    if (_data is TempAreaResponse) {
      _tempAreaResponse = _data;
    }
  }

  @override
  void onConfirmBtn() {
    bool isConfirmCheck = _tempAreaResponse.getDataList().length < 10;
    if (isConfirmCheck) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }
}

class TempAreaResultData {
  TempAreaResultData({
    this.data,
    this.tempAreaLength = 0,
  });

  final TempAreaData? data;
  final int tempAreaLength;
}
