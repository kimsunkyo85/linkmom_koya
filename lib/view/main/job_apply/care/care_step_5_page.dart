import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/req_job_data.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/grid_care_target_select.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/lcons.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../address_page.dart';
import '../content_view.dart';
import '../step_view.dart';
import 'care_step_6_page.dart';

class CareStep5Page extends BaseStateful {
  @override
  _CareStep5PageState createState() => _CareStep5PageState();

  CareStep5Page({this.data, this.viewMode});

  final DataManager? data;
  final ViewMode? viewMode;
}

class _CareStep5PageState extends BaseStatefulState<CareStep5Page> {
  double goToSchoolPayValue = 9000.0;
  double afterSchoolPayValue = 9000.0;
  double boyukPayValue = 9000.0;

  int flex = 3;
  int flex2 = 7;

  ///주소 최대 라인
  int maxLineAddress = 3;

  ///경유지 최대 라인
  int maxLinePath = 4;

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 5;

    ///수정일때는 STEP을 보여주지 않는다.
    if (getViewType() == ViewType.modify) {
      data.jobItem.childInfo.isShow = false;
    }

    data.jobItem.requestJob = _data.jobItem.requestJob.clone();
    data.jobItem.jobData = _data.jobItem.jobData;
    log.d('『GGUMBI』>>> onData : data.jobItem.requestJob: ${data.jobItem.requestJob},  <<< ');
    onDataPage(data.jobItem.requestJob);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        resizeToAvoidBottomInset: true,
        key: data.scaffoldKey,
        appBar: jobAppBar(
          "희망근무조건".tr(),
          // isBack: getViewType() == ViewType.modify ? false : true,
          isClose: getViewType() == ViewType.apply ? true : false,
          hide: getViewType() == ViewType.modify ? false : true,
          onClick: () {
            if (getViewType() == ViewType.modify) {
              Commons.pagePop(context);
            } else {
              Commons.showCloseDlg();
            }
          },
        ),
        context: context,
        body: lContainer(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step, stepCnt: 6),
              Expanded(
                child: Column(
                  children: [
                    Expanded(
                      child: lScrollView(
                        padding: padding_Item_TB,
                        child: Container(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              if (data.jobItem.childInfo.isShow) StepView(valueStep: data.jobItem.childInfo.step),
                              if (!data.jobItem.childInfo.isShow) sb_h_20,
                              Padding(
                                padding: padding_Item_LR,
                                child: InkWell(
                                  onTap: () {
                                    if (!Commons.isDebugMode) return;
                                    // data.tcHomeLocation1.text = '경기 수원시 영통구';
                                    // data.jobItem.requestJob.job_area_address_1 = data.tcHomeLocation1.text;
                                    // data.jobItem.requestJob.job_addressid_1 = '4111700000';
                                    // data.jobItem.requestJob.job_areaid_1 = '4111700000';

                                    data.tcHomeLocation2.text = '수원시 영통구 광교1동';
                                    data.jobItem.requestJob.job_area_address_2 = data.tcHomeLocation2.text;
                                    data.jobItem.requestJob.job_addressid_2 = '4111700000';
                                    data.jobItem.requestJob.job_areaid_2 = '4111700000';

                                    data.jobItem.lsJobCareTargetType[0].isSelected = true;
                                    data.jobItem.lsJobCareTargetType[1].isSelected = true;
                                    data.jobItem.lsJobCareTargetType[2].isSelected = true;

                                    data.tcJob.text = '안녕하세요. 링크쌤입니다. ^^*';

                                    onConfirmBtn();
                                  },
                                  child: contentView(data: ContentData(content: "희망근무조건선택".tr())),
                                ),
                              ),

                              //희망 지역 1순위, 2순위
                              Padding(
                                padding: padding_Item_LR,
                                child: _hopeAreaView(),
                              ),

                              lDivider20(),

                              //희망 급여 등원, 하원
                              Padding(
                                padding: padding_Item_LR,
                                child: _hopePayView(),
                              ),

                              lDivider20(),

                              //돌봄대상
                              Padding(
                                padding: padding_Item_LR,
                                child: _targetCareView(),
                              ),

                              lDivider20(),

                              //한줄 자기 소개
                              Padding(
                                padding: padding_Item_LR,
                                child: _myAboutView(),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    _confirmButton(),
                  ],
                ),
              ),
              // wdDivider(),
            ],
          ),
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn(Commons.bottomBtnTitle(getViewType()), isEnabled: flag.isConfirm, onClickAction: () {
      if (getViewType() == ViewType.modify) {
        Commons.pagePop(context, data: data.jobItem.requestJob);
      } else {
        Commons.page(context, routeCare6, arguments: CareStep6Page(data: data));
      }
    });
  }

  @override
  void onConfirmBtn() {
    //희망지역
    bool isHopeArea = false;
    //1순위
    if (StringUtils.validateString(data.tcHomeLocation1.text)) {
      isHopeArea = true;
    } else {
      //2순위 2순위만 있을 경우 1순위로 변경해서 서버로 보낸다.
      if (StringUtils.validateString(data.tcHomeLocation2.text)) {
        isHopeArea = true;
        data.jobItem.requestJob.job_area_address_1 = data.tcHomeLocation2.text;
        data.jobItem.requestJob.job_addressid_1 = data.jobItem.requestJob.job_addressid_2;
        data.jobItem.requestJob.job_areaid_1 = data.jobItem.requestJob.job_areaid_2;

        data.jobItem.requestJob.job_area_address_2 = "";
        data.jobItem.requestJob.job_addressid_2 = "";
        data.jobItem.requestJob.job_areaid_2 = "";
      }
    }

    //돌봄대상
    bool isCareTarget = false;
    data.jobItem.requestJob.care_ages!.clear();
    data.jobItem.lsJobCareTargetType.asMap().forEach((index, value) {
      if (value.isSelected) {
        isCareTarget = true;
        data.jobItem.requestJob.care_ages!.add(value.type);
      }
    });

    //시급
    data.jobItem.requestJob.fee_gotoschool = goToSchoolPayValue.toInt();
    data.jobItem.requestJob.fee_afterschool = afterSchoolPayValue.toInt();
    data.jobItem.requestJob.fee_boyuk = boyukPayValue.toInt();

    //한줄 자기소개
    data.jobItem.requestJob.introduce = data.tcJob.text;

    //돌봄방식 소개
    data.jobItem.requestJob.care_introduce = data.tcJobCare.text;

    log.d('『GGUMBI』>>> onConfirmBtn : isHopeArea $isHopeArea, $isCareTarget  <<< ');
    if (isHopeArea && isCareTarget /*&& isAbout*/) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  @override
  void onDataPage(_data) {
    if (_data is ReqJobData) {
      var item = _data;

      if (_data == null) {
        onConfirmBtn();
        return;
      }

      //1순위
      data.tcHomeLocation1.text = item.job_area_address_1;
      //2순위
      data.tcHomeLocation2.text = item.job_area_address_2;

      //등원시급
      goToSchoolPayValue = item.fee_gotoschool.toDouble();
      //하원시급
      afterSchoolPayValue = item.fee_afterschool.toDouble();
      //보육시급
      boyukPayValue = item.fee_boyuk.toDouble();

      //돌봄대상
      data.jobItem.lsJobCareTargetType.asMap().forEach((index, value) {
        item.care_ages!.forEach((item) {
          if (value.type == item) {
            value.isSelected = true;
          }
        });
      });

      data.tcJob.text = item.introduce;
      data.tcJobCare.text = item.care_introduce;
    }
    onConfirmBtn();
  }

  ///희망 지역 1순위, 2순위
  Widget _hopeAreaView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        sb_h_20,
        contentView(
            data: ContentData(
          content: "희망지역".tr(),
          contentStyle: st_b_18(fontWeight: FontWeight.bold),
          rightWidget: lText("*", style: st_b_16(textColor: color_ff3b30)),
        )),
        sb_h_10,
        lText("1순위".tr(), style: st_b_14(fontWeight: FontWeight.bold)),
        Form(
          key: data.formHomeLocation1,
          child: TextFormField(
            textAlignVertical: TextAlignVertical.center,
            controller: data.tcHomeLocation1,
            style: stLogin,
            readOnly: true,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.emailAddress,
            decoration: underLineDecoration(
              "주소선택_힌트".tr(),
              icon: Lcons.search(disableColor: color_222222),
              focus: true,
              focusColor: color_dbdbdb,
              width: 1.0,
            ),
            onTap: () async {
              log.d('『GGUMBI』>>> placeView ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ $data <<< ');
              DataManager dataManager = await Commons.nextPage(context, routeName: routeAddress, arguments: AddressPage(data: data));
              log.d('『GGUMBI』>>> placeView : dataManager: ${dataManager.addressItem.codeSearchData},  <<< ');

              if (dataManager.addressItem.codeSearchData != null) {
                data.tcHomeLocation1.text = dataManager.addressItem.codeSearchData!.getAddress3depth();
                data.jobItem.requestJob.job_area_address_1 = data.tcHomeLocation1.text;
                data.jobItem.requestJob.job_areaid_1 = dataManager.addressItem.codeSearchData!.bcode;
                data.jobItem.requestJob.job_addressid_1 = dataManager.addressItem.codeSearchData!.hcode;
              }
              onConfirmBtn();
              log.d('『GGUMBI』>>> placeView : data.addressItem.codeSearchData: ${dataManager.addressItem.codeSearchData},  <<< ');
            },
            onEditingComplete: () {
              focusClear(context);
            },
          ),
        ),
        lText("2순위".tr(), style: st_b_14(fontWeight: FontWeight.bold)),
        Form(
          key: data.formHomeLocation2,
          child: TextFormField(
            controller: data.tcHomeLocation2,
            textAlignVertical: TextAlignVertical.center,
            style: stLogin,
            readOnly: true,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.emailAddress,
            decoration: underLineDecoration(
              "주소선택_힌트".tr(),
              icon: Lcons.search(disableColor: color_222222),
              focus: true,
              focusColor: color_dbdbdb,
              width: 1.0,
            ),
            onTap: () async {
              log.d('『GGUMBI』>>> placeView ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
              DataManager dataManager = await Commons.nextPage(context, routeName: routeAddress, arguments: AddressPage(data: data));

              if (dataManager.addressItem.codeSearchData != null) {
                data.tcHomeLocation2.text = dataManager.addressItem.codeSearchData!.getAddress3depth();
                data.jobItem.requestJob.job_area_address_2 = data.tcHomeLocation2.text;
                data.jobItem.requestJob.job_areaid_2 = dataManager.addressItem.codeSearchData!.bcode;
                data.jobItem.requestJob.job_addressid_2 = dataManager.addressItem.codeSearchData!.hcode;
              }
              onConfirmBtn();
              log.d('『GGUMBI』>>> placeView : data.addressItem.codeSearchData: ${dataManager.addressItem.codeSearchData},  <<< ');
            },
            onEditingComplete: () {
              focusClear(context);
            },
          ),
        ),
      ],
    );
  }

  ///시급,스피드매칭 화면
  Widget _hopePayView() {
    return lContainer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          contentView(
              data: ContentData(
            content: "희망시급".tr(),
            contentStyle: st_b_18(fontWeight: FontWeight.bold),
            rightWidget: lText("*", style: st_b_16(textColor: color_ff3b30)),
          )),
          sb_h_10,
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              lText("등원".tr(), style: st_b_16(fontWeight: FontWeight.w500)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  lText("시급".tr(), style: st_15(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
                  Padding(
                    padding: padding_05_L,
                    child: lText(StringUtils.formatPay(goToSchoolPayValue.round()), style: st_20(textColor: Commons.getColor(), fontWeight: FontWeight.bold)),
                  ),
                  lText("원".tr(), style: st_15(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
                ],
              ),
              SliderTheme(
                data: lSliderThemeData(
                  context,
                  activeTrackColor: Commons.getColor(),
                  thumbColor: Commons.getColor(),
                ),
                child: Slider(
                    value: goToSchoolPayValue,
                    min: 5000.0,
                    max: 50000.0,
                    divisions: 45,
                    autofocus: false,
                    label: StringUtils.formatPay(goToSchoolPayValue.round()),
                    onChanged: (double value) {
                      setState(() {
                        goToSchoolPayValue = value.roundToDouble();
                        data.jobItem.requestJob.fee_gotoschool = goToSchoolPayValue.toInt();
                      });
                    }),
              ),
              Row(
                children: [
                  Expanded(child: lText('${"최소".tr()} 5,000${"원".tr()}', style: st_14(textColor: color_b2b2b2))),
                  Expanded(child: lText('${"최대".tr()} 50,000${"원".tr()}', style: st_14(textColor: color_b2b2b2), textAlign: TextAlign.right)),
                ],
              ),
            ],
          ),
          sb_h_15,
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              lText("하원".tr(), style: st_b_16(fontWeight: FontWeight.w500)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  lText("시급".tr(), style: st_15(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
                  Padding(
                    padding: padding_05_L,
                    child: lText(StringUtils.formatPay(afterSchoolPayValue.round()), style: st_20(textColor: Commons.getColor(), fontWeight: FontWeight.bold)),
                  ),
                  lText("원".tr(), style: st_15(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
                ],
              ),
              SliderTheme(
                data: lSliderThemeData(
                  context,
                  activeTrackColor: Commons.getColor(),
                  thumbColor: Commons.getColor(),
                ),
                child: Slider(
                    value: afterSchoolPayValue,
                    min: 5000.0,
                    max: 50000.0,
                    divisions: 45,
                    autofocus: false,
                    label: StringUtils.formatPay(afterSchoolPayValue.round()),
                    onChanged: (double value) {
                      setState(() {
                        afterSchoolPayValue = value.roundToDouble();
                        data.jobItem.requestJob.fee_afterschool = afterSchoolPayValue.toInt();
                      });
                    }),
              ),
              Row(
                children: [
                  Expanded(child: lText('${"최소".tr()} 5,000${"원".tr()}', style: st_14(textColor: color_b2b2b2))),
                  Expanded(child: lText('${"최대".tr()} 50,000${"원".tr()}', style: st_14(textColor: color_b2b2b2), textAlign: TextAlign.right)),
                ],
              ),
            ],
          ),
          sb_h_15,
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              lText("보육".tr(), style: st_b_16(fontWeight: FontWeight.w500)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  lText("시급".tr(), style: st_15(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
                  Padding(
                    padding: padding_05_L,
                    child: lText(StringUtils.formatPay(boyukPayValue.round()), style: st_20(textColor: Commons.getColor(), fontWeight: FontWeight.bold)),
                  ),
                  lText("원".tr(), style: st_15(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
                ],
              ),
              SliderTheme(
                data: lSliderThemeData(
                  context,
                  activeTrackColor: Commons.getColor(),
                  thumbColor: Commons.getColor(),
                ),
                child: Slider(
                    value: boyukPayValue,
                    min: 5000.0,
                    max: 50000.0,
                    divisions: 45,
                    autofocus: false,
                    label: StringUtils.formatPay(boyukPayValue.round()),
                    onChanged: (double value) {
                      setState(() {
                        boyukPayValue = value.roundToDouble();
                        data.jobItem.requestJob.fee_boyuk = boyukPayValue.toInt();
                      });
                    }),
              ),
              Row(
                children: [
                  Expanded(child: lText('${"최소".tr()} 5,000${"원".tr()}', style: st_14(textColor: color_b2b2b2))),
                  Expanded(child: lText('${"최대".tr()} 50,000${"원".tr()}', style: st_14(textColor: color_b2b2b2), textAlign: TextAlign.right)),
                ],
              ),
            ],
          ),
          sb_h_15,
        ],
      ),
    );
  }

  ///돌봄대상
  Widget _targetCareView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        contentView(
            data: ContentData(
          content: "돌봄대상".tr(),
          contentStyle: st_b_18(fontWeight: FontWeight.bold),
          rightWidget: lText("*", style: st_b_16(textColor: color_ff3b30)),
        )),
        sb_h_10,
        GridCareTargetSelector(
          data: data.jobItem.lsJobCareTargetType,
          count: 2,
          user_type: storageHelper.user_type,
          margin: padding_05_LR,
          padding: padding_moveTypeLinkMom,
          borderRadius: 32,
          enableColor: Commons.getColor(),
          onItemClick: (value) {
            onConfirmBtn();
          },
        ),
      ],
    );
  }

  ///한줄 자기소개
  Widget _myAboutView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        contentView(
            data: ContentData(
          content: "한줄자기소개".tr(),
          contentStyle: st_b_18(fontWeight: FontWeight.bold),
        )),
        sb_h_10,
        Form(
          key: data.formJob,
          child: TextFormField(
            textAlignVertical: TextAlignVertical.center,
            controller: data.tcJob,
            maxLines: 1,
            maxLength: 30,
            style: st_b_16(),
            validator: StringUtils.validateAddress,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.emailAddress,
            decoration: bgDecoration("한줄자기소개_힌트".tr(), bgColor: color_white, textColor: color_b2b2b2, isLine: true, maxLength: 30),
            onChanged: (value) {
              onConfirmBtn();
            },
            onEditingComplete: () {
              focusClear(context);
              onConfirmBtn();
            },
          ),
        ),
        contentView(
            data: ContentData(
          content: "돌봄방식".tr(),
          contentStyle: st_b_18(fontWeight: FontWeight.bold),
        )),
        sb_h_10,
        Form(
          key: data.formJobCare,
          child: TextFormField(
            textAlignVertical: TextAlignVertical.center,
            controller: data.tcJobCare,
            maxLines: 10,
            maxLength: 1000,
            style: st_b_16(),
            validator: StringUtils.validateAddress,
            textInputAction: TextInputAction.newline,
            keyboardType: TextInputType.multiline,
            decoration: bgDecoration("돌봄방식_힌트".tr(), bgColor: color_white, textColor: color_b2b2b2, isLine: true, maxLength: 1000),
            onChanged: (value) {
              onConfirmBtn();
            },
            onEditingComplete: () {
              focusClear(context);
              onConfirmBtn();
            },
          ),
        ),
      ],
    );
  }

  /// 등원,하원,보육...서비스 타입
  String getServiceTypeToString(int serviceType) {
    String type = '';
    if (serviceType == ServiceType.serviceType_0.index) {
      type = "등원돌봄".tr();
    } else if (serviceType == ServiceType.serviceType_1.index) {
      type = "하원돌봄".tr();
    } else if (serviceType == ServiceType.serviceType_2.index) {
      type = "보육돌봄".tr();
    } else if (serviceType == ServiceType.serviceType_3.index) {
      type = "학습볼봄".tr();
    } else if (serviceType == ServiceType.serviceType_5.index) {
      type = "이유식반찬".tr();
    }
    log.d('『GGUMBI』>>> getServiceTypeToString : type: $type, $serviceType <<< ');
    return type;
  }

  /// 보육장소 우리집,이웃집
  String getAreaToString(int areaType) {
    String type = '';
    if (areaType == PossibleArea.mom_daddy.value) {
      type = '${"돌봄장소".tr()} : ${"우리집".tr()}';
    } else if (areaType == PossibleArea.link_mom.value) {
      type = '${"돌봄장소".tr()} : ${"이웃집".tr()}';
    }
    log.d('『GGUMBI』>>> getServiceTypeToString : type: $type, $areaType <<< ');
    return type;
  }

  /// 출발해야하는시간
  String getStartTimeToString(int serviceType) {
    String type = '';
    if (serviceType == ServiceType.serviceType_0.index) {
      type = "등원볼봄".tr();
    } else if (serviceType == ServiceType.serviceType_1.index) {
      type = "하원볼봄".tr();
    } else if (serviceType == ServiceType.serviceType_2.index) {
      type = "보육돌봄".tr();
    } else if (serviceType == ServiceType.serviceType_3.index) {
      type = "학습볼봄".tr();
    } else if (serviceType == ServiceType.serviceType_5.index) {
      type = "이유식반찬".tr();
    }
    log.d('『GGUMBI』>>> getStartTimeToString : type: $type, $serviceType <<< ');
    return type;
  }

  /// 출발장소 제목
  String getStartAddressTitle(int serviceType) {
    String type = "출발장소".tr();
    if (serviceType == ServiceType.serviceType_0.index) {
    } else if (serviceType == ServiceType.serviceType_1.index) {
    } else if (serviceType == ServiceType.serviceType_2.index) {
      // type = "돌봄장소".tr();
      type = ''; //아무런 표시하지 않는다.
    } else if (serviceType == ServiceType.serviceType_3.index) {
    } else if (serviceType == ServiceType.serviceType_5.index) {}
    log.d('『GGUMBI』>>> getStartAddressTitle : type: $type, $serviceType <<< ');
    return type;
  }

  ///출발시간
  String getStartTime(int serviceType) {
    String type = '';
    if (serviceType == ServiceType.serviceType_0.index) {
      type = data.jobItem.reqdata.bookinggotoschool![0].start_time;
    } else if (serviceType == ServiceType.serviceType_1.index) {
      type = data.jobItem.reqdata.bookingafterschool![0].start_time;
    } else if (serviceType == ServiceType.serviceType_2.index) {
    } else if (serviceType == ServiceType.serviceType_3.index) {
    } else if (serviceType == ServiceType.serviceType_5.index) {}
    log.d('『GGUMBI』>>> getStartTime : type: $type, $serviceType <<< ');
    type = StringUtils.getStringToTime(type);
    return type;
    // if (possible_area == PossibleArea.ourhome.index) {
    //   data.jobItem.request.reqdata.bookinggotoschool.
    //   type = "등원볼봄".tr();
    // } else if (possible_area == PossibleArea.ourhome.index) {
    //   type = "하원볼봄".tr();
    // }
  }

  ///원하는 이웃집 위치 1순위, 2순위
  String getLocation(int serviceType, int possibleArea, int location) {
    String value = '';
    log.d('『GGUMBI』>>> getLocation : data.jobItem.reqdata.bookinggotoschool: ${data.jobItem.reqdata.bookinggotoschool},  <<< ');
    if (serviceType == ServiceType.serviceType_0.index) {
      //등원이고, 이웃집
      if (possibleArea == PossibleArea.link_mom.value) {
        if (location == 2) {
          value = data.jobItem.reqdata.bookinggotoschool![0].nhn_area_2_address;
        } else {
          value = data.jobItem.reqdata.bookinggotoschool![0].nhn_area_1_address;
        }
      }
    } else if (serviceType == ServiceType.serviceType_1.index) {
      //등원이고, 이웃집
      if (possibleArea == PossibleArea.link_mom.value) {
        if (location == 2) {
          value = data.jobItem.reqdata.bookingafterschool![0].nhn_area_2_address;
        } else {
          value = data.jobItem.reqdata.bookingafterschool![0].nhn_area_1_address;
        }
      }
    } else if (serviceType == ServiceType.serviceType_2.index) {
      //등원이고, 이웃집
      if (possibleArea == PossibleArea.link_mom.value) {
        if (location == 2) {
          value = data.jobItem.reqdata.bookingboyuk![0].nhn_area_2_address;
        } else {
          value = data.jobItem.reqdata.bookingboyuk![0].nhn_area_1_address;
        }
      }
    } else if (serviceType == ServiceType.serviceType_3.index) {
    } else if (serviceType == ServiceType.serviceType_5.index) {}

    log.d('『GGUMBI』>>> getStartAddress : type: $value, $serviceType, $possibleArea, $location <<< ');
    return value;
  }
}
