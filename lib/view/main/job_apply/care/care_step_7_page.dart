import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:linkmom/data/network/models/index_response.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/firebase_analytics_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_menu_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/route_action.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/auth_center/auth_center_page.dart';
import 'package:linkmom/view/main/community/community_event_page.dart';

import '../../../../base/base_stateful.dart';
import '../../../link_page.dart';
import '../../../../main.dart';
import '../../../../utils/string_util.dart';
import '../../../../utils/style/color_style.dart';

class CareStep7Page extends BaseStateful {
  @override
  _CareStep7PageState createState() => _CareStep7PageState();

  CareStep7Page({this.data, this.viewMode});

  final DataManager? data;
  final ViewMode? viewMode;
}

class _CareStep7PageState extends BaseStatefulState<CareStep7Page> {
  SingleItem _item = SingleItem('');
  List<Picker> pickes = [];

  var startTime = [0, 0];
  var endTime = [0, 0];

  EventBanner _eventBanner = EventBanner();

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
    _eventBanner = auth.indexInfo.bookingjobs_banner!;
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 7;
    data.jobItem.childInfo.isShow = false;
    data.jobItem.requestJob = _data.jobItem.requestJob.clone();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          key: data.scaffoldKey,
          resizeToAvoidBottomPadding: false,
          appBar: appBar(
            "등록완료".tr(),
            isBack: false,
            isClose: false,
            hide: false,
          ),
          body: lContainer(
            child: Column(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      _completeView(),
                      sb_h_20,
                      Expanded(
                        child: lScrollView(
                          child: _addView(),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: padding_20,
                  child: lText("링크쌤지원서알림설정_안내_2".tr(), style: st_12(textColor: color_999999)),
                ),
                if (StringUtils.validateString(_eventBanner.bannerUrl))
                  InkWell(
                    onTap: () {
                      if (_eventBanner.linkUrl.isEmpty) {
                        Commons.pageToMain(context, routeCommunityEvent, arguments: CommunityEventPage(id: _eventBanner.id));
                      } else if (_eventBanner.linkUrl.startsWith(RouteAction.routeScheme)) {
                        RouteAction.onLink(context, _eventBanner.linkUrl);
                      } else {
                        Commons.lLaunchUrl(_eventBanner.linkUrl);
                      }
                      FbaManager.addLogEvent(viewName: routeCare7, type: FbaLogType.event, parameters: {FbaLogType.click.name: _eventBanner.id, EventBanner.Key_link_url: _eventBanner.linkUrl});
                    },
                    child: AspectRatio(
                      aspectRatio: Commons.getEventBannerHeight(),
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        imageUrl: _eventBanner.bannerUrl,
                        imageBuilder: (ctx, imgProvider) => imageBuilder(imgProvider, width: widthFull(context)),
                        placeholder: (ctx, url) => placeholder(),
                        errorWidget: (ctx, url, error) => errorMsgWidget(),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///돌봄 신청이 완료되었습니다.
  Widget _completeView() {
    return lContainer(
      alignment: Alignment.center,
      height: heightFull(context) * 0.3,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          sb_h_30,
          Lcons.complete(size: 60, color: Commons.getColor()),
          sb_h_20,
          lAutoSizeText(
            "링크쌤등록이완료되었습니다".tr(),
            style: st_b_20(fontWeight: FontWeight.bold),
          ),
          sb_h_10,
          TextHighlight(
            text: "링크쌤_등록완료".tr(),
            textStyle: st_b_14(),
            textAlign: TextAlign.center,
            term: "링크쌤_등록완료_힌트".tr(),
            textStyleHighlight: st_14(textColor: Commons.getColor()),
          ),
          sb_h_03,
          TextHighlight(
            text: "링크쌤_등록완료_2".tr(),
            textStyle: st_b_14(),
            textAlign: TextAlign.center,
            term: "링크쌤_등록완료_2_힌트".tr(),
            textStyleHighlight: st_14(textColor: Commons.getColor()),
          ),
        ],
      ),
    );
  }

  ///추가가 필요한 돌봄이 있나요?
  Widget _addView() {
    return ListMenuSelector(
      data: data.jobItem.lsViewType,
      count: 2,
      padding: padding_05,
      margin: padding_07,
      onItemClick: (value) {
        _item = value as SingleItem;
        _goPage();
      },
    );
  }

  ///확인 버튼 클릭시 이벤트
  _goPage() {
    String routeName = routeHome;
    var arguments;
    //2021/12/08 구직신청서를 작성시 먼저 변경을 해준다.
    auth.indexInfo.userinfo.isUserLinkmom = true;
    auth.indexInfo.linkmominfo.isBookingjobs = true;
    FbaManager.addLogEvent(viewName: routeCare7, type: FbaLogType.menu, parameters: {FbaLogType.click.name: _item.name});

    MoveMenuType type = _item.data as MoveMenuType;
    if (type == MoveMenuType.menu_0) {
      return Commons.pageToMainTabMove(context, tabIndex: TAB_LIST);
    } else if (type == MoveMenuType.menu_1) {
      return Commons.pageToMainTabMove(context, tabIndex: TAB_SCHEDULE);
    } else if (type == MoveMenuType.menu_2) {
      routeName = routeAuthCenter;
      arguments = AuthCenterPage(data: data);
      return Commons.pageToMain(context, routeName, arguments: arguments);
    } else if (type == MoveMenuType.menu_3) {
      return Commons.pageToMainTabMove(context, tabIndex: TAB_HOME);
    }
  }
}
