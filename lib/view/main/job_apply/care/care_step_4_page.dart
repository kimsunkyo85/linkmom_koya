import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:linkmom/data/holidays/holidays_helper.dart';
import 'package:linkmom/data/network/models/req_job_data.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/calendar/calendar_view.dart';
import 'package:linkmom/view/calendar/linkmom_calendar.dart';
import 'package:linkmom/view/calendar/schedule_controller.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../content_view.dart';
import '../step_view.dart';
import 'care_step_5_page.dart';

class CareStep4Page extends BaseStateful {
  @override
  _CareStep4PageState createState() => _CareStep4PageState();

  CareStep4Page({this.data, this.viewMode});

  final DataManager? data;
  final ViewMode? viewMode;
}

class _CareStep4PageState extends BaseStatefulState<CareStep4Page> {
  List<JobSchedule> _schedule = [];
  ScheduleController _scheCtrl = ScheduleController();
  PanelController _panelController = PanelController();

  bool _editMode = false;
  int _scheduleId = 0;

  Widget _panelArrow = Lcons.top(color: color_cecece);

  late PageController _pageController;

  String endDate = '';

  @override
  void initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
    _pageController = PageController(
      initialPage: _schedule.length < 2 ? 0 : 5,
      keepPage: false,
      viewportFraction: 0.9,
    );
    _pageController.addListener(() {
      if (_pageController.page! > _schedule.length - 1) {
        _movePage(_schedule.length - 1);
      }
    });
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data = _data;
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 4;

    ///수정일때는 STEP을 보여주지 않는다.
    if (getViewType() == ViewType.modify) {
      data.jobItem.childInfo.isShow = false;
    }

    data.jobItem.requestJob = _data.jobItem.requestJob.clone();
    data.jobItem.jobData = _data.jobItem.jobData;

    log.d('『GGUMBI』>>> onDataPage : data.jobItem.requestJob: ${data.jobItem.requestJob},  <<< ');
    _schedule = List.generate(data.jobItem.requestJob.careschedule!.length, (index) => JobSchedule(bookingdate: []));
    for (var i = 0; i < data.jobItem.requestJob.careschedule!.length; i++) {
      _schedule[i].setSchedule(data.jobItem.requestJob.careschedule![i]);
    }
    onDataPage(_data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        key: data.scaffoldKey,
        appBar: jobAppBar(
          "날짜시간".tr(),
          isClose: getViewType() == ViewType.apply ? true : false,
          hide: getViewType() == ViewType.modify ? false : true,
          onClick: () {
            if (getViewType() == ViewType.modify) {
              Commons.pagePop(context);
            } else {
              //스케줄 데이터 값 저장
              Commons.showSaveTempDlg(data: data);
            }
          },
        ),
        body: lContainer(
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            Expanded(
                child: SlidingUpPanel(
              controller: _panelController,
              minHeight: 90,
              maxHeight: 200,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(24.0), topRight: Radius.circular(24.0)),
              body: Column(children: [
                Expanded(
                    child: ListView(children: [
                  if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step, stepCnt: 6),
                  Container(
                      margin: padding_Item,
                      child: Column(
                        children: [
                          if (data.jobItem.childInfo.isShow) StepView(valueStep: data.jobItem.childInfo.step),
                          if (!data.jobItem.childInfo.isShow) sb_h_20,
                          contentView(data: ContentData(content: "시간선택요청".tr())),
                        ],
                      )),
                  Padding(
                    padding: padding_10,
                    child: CalendarView(
                      events: {},
                      selected: selectDate,
                      selectable: true,
                      holidays: HolidaysHelper.getHolidays,
                      height: 50,
                      eventLimit: 365,
                      startDate: DateTime.now(),
                      duration: Duration(days: 395),
                      type: USER_TYPE.link_mom,
                      onCreateCalendar: (ctrl) {
                        _scheCtrl = ctrl;
                        _appendEvent();
                      },
                      colors: colorList,
                    ),
                  ),
                  Container(height: Commons.height(context, 0.6)),
                ]))
              ]),
              header: Column(
                children: [
                  Container(
                    height: 50,
                    width: Commons.width(context, 1),
                    alignment: Alignment.center,
                    child: IconButton(
                      icon: _panelArrow,
                      onPressed: () {
                        if (_panelController.isPanelClosed) {
                          _openPanel();
                        } else {
                          _closePanel();
                        }
                      },
                    ),
                  ),
                  if (_schedule.isNotEmpty)
                    Container(
                      width: Commons.width(context, 1),
                      padding: padding_20_LR,
                      child: titleBlock(
                        isShowing: !_isNotAbAdd,
                        startTime: _schedule[_getPage()].getDates.first,
                        endTime: _schedule[_getPage()].getDates.last,
                        date: _schedule[_getPage()].getDates,
                        timeDesc: "",
                        color: colorList[_getPage()],
                        isRepeat: _schedule[_getPage()].workTimes.where((time) => time.is_repeat == 1).isNotEmpty,
                        editDateFun: (repeat, state, callback) {
                          if (repeat) {
                            _showdlg("매주반복해제".tr());
                          } else {
                            _editMode = state;
                            if (!state) {
                              if (!_scheCtrl.selecter.containId(_getPage()) && !_scheCtrl.eventer.containId(_getPage())) {
                                _showdlg("날짜선택_에러".tr());
                                _updateId();
                                callback(true);
                                _editMode = true;
                              } else if (!_isAvailable(_getPage())) {
                                _showdlg("시간추가_에러_중복".tr());
                                _updateId();
                                callback(true);
                                _editMode = true;
                              } else {
                                _scheCtrl.saveEvent();
                                _schedule[_getPage()].clearSchedule();
                                _schedule[_getPage()].addSchedule(_scheCtrl.eventer.getDates(_getPage()));
                                callback(state);
                              }
                            } else {
                              if (_isAvailable(_getPage())) {
                                _schedule[_getPage()].clearSchedule();
                                _schedule[_getPage()].addSchedule(_scheCtrl.eventer.getDates(_getPage()));
                                callback(true);
                                _updateId();
                                _closePanel();
                              } else {
                                _editMode = false;
                                callback(false);
                                _showdlg("일정중복안내".tr(), callback: (a) => _openPanel());
                              }
                            }
                          }
                          onConfirmBtn();
                        },
                      ),
                    ),
                ],
              ),
              panel: Container(
                margin: EdgeInsets.only(top: 100, bottom: 10),
                child: PageView(
                  controller: _pageController,
                  scrollDirection: Axis.horizontal,
                  physics: _editMode || _scheCtrl.selecter.isNotEmpty ? NeverScrollableScrollPhysics() : null,
                  onPageChanged: (idx) {
                    _movePage(idx);
                  },
                  children: _schedule.map((schedule) {
                    int scheId = _schedule.indexOf(schedule);
                    return ListView(
                      shrinkWrap: true,
                      children: _schedule[scheId].workTimes.map((time) {
                        int timeId = _schedule[scheId].workTimes.indexOf(time);
                        return Container(
                          margin: padding_10_LRB,
                          child: TimeBlock(
                            deleteTimeFun: (isFirst) {
                              if (isFirst) {
                                _showDeleteMsg("삭제확인_메세지".tr(), ok: () {
                                  _removeSchedule(scheId);
                                });
                              } else {
                                _showDeleteMsg("삭제확인_타이틀".tr(), ok: () {
                                  _schedule[scheId].removeTimes(timeId);
                                  onConfirmBtn();
                                });
                              }
                            },
                            onRepeat: (value) {
                              if (_editMode || _scheCtrl.selecter.isNotEmpty) return;
                              List<DateTime> updated = _updateRepeatSchedule(true, scheId);
                              if (_isEnableRepeat(schedule.getDates)) {
                                if (value) {
                                  _closePanel();
                                  _showRepeatSelecter(updated, scheId, timeId);
                                } else {
                                  updated.forEach((date) {
                                    _scheCtrl.eventer.remove(date, index: scheId);
                                  });
                                  time.caredate_until = StringUtils.formatYMD(schedule.getDates.last);
                                  time.is_repeat = 0;
                                }
                              } else {
                                _showdlg("매주반복_에러".tr());
                              }
                              onConfirmBtn();
                            },
                            editTimeFun: () {
                              if (_editMode || _scheCtrl.selecter.isNotEmpty) return;
                              _scheduleId = _getPage();
                              _showTimePicker(
                                true,
                                scheId,
                                stime: StringUtils.convertNowFromString(time.stime),
                                etime: StringUtils.convertNowFromString(time.etime),
                                index: timeId,
                              );
                              onConfirmBtn();
                            },
                            editDateFun: (repeat, state, callback) {
                              // Nothing
                            },
                            addTimeFun: (index, state, callback) {
                              if (state) {
                                if (_schedule[index].workTimes.length < 3) {
                                  _showTimePicker(false, timeId);
                                } else {
                                  _showdlg("시간추가_에러_초과".tr());
                                }
                                callback(false);
                              }
                              onConfirmBtn();
                            },
                            color: colorList[scheId],
                            isRepeat: time.is_repeat == 1,
                            date: schedule.getDates,
                            startTime: StringUtils.parseHD(time.stime),
                            endTime: StringUtils.parseHD(time.etime),
                            showTitle: false,
                            index: timeId,
                            isFirstUnit: schedule.workTimes.length < 2,
                            timeDesc: "(${"매주반복안내_하이라이트2_구직".tr()})",
                            showDeleteBtn: !_editMode && _scheCtrl.selecter.isEmpty,
                          ),
                        );
                      }).toList(),
                    );
                  }).toList(),
                ),
              ),
            )),
            _pageIndicator(),
            if (!_editMode) _timeSelecter(),
            _confirmButton(),
          ]),
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn(getViewType() == ViewType.modify ? "저장".tr() : "다음".tr(), isEnabled: flag.isConfirm && !_editMode && _scheCtrl.selecter.isEmpty, onClickAction: () {
      data.jobItem.requestJob.careschedule = _schedule.map((e) => e.getSchedule).toList();
      DateTime? first, last = DateTime(0, 0, 1);
      data.jobItem.totalTime = 0;
      data.jobItem.requestJob.careschedule!.forEach((element) {
        element.times!.forEach((e) {
          data.jobItem.totalTime += e.durationtime;
        });
      });
      _schedule.forEach((schedule) {
        if (first == null) first = schedule.getDates.first;
        if (first!.isAfter(schedule.getDates.first)) first = schedule.getDates.first;
        if (last!.isBefore(schedule.getDates.last)) last = schedule.getDates.last;
        List<ScheduleTimeData> untilLast = schedule.times!.where((element) => last!.isBefore(StringUtils.parseYMD(element.caredate_until))).toSet().toList();
        if (untilLast.isNotEmpty) {
          untilLast.forEach((element) {
            DateTime time = StringUtils.parseYMD(element.caredate_until);
            if (last!.isBefore(time)) last = time;
          });
        }
      });
      data.jobItem.requestJob.caredate_min = StringUtils.formatYMD(first!);
      data.jobItem.requestJob.caredate_max = StringUtils.formatYMD(last!);
      data.jobItem.calcuTime = data.jobItem.totalTime;
      if (getViewType() == ViewType.modify) {
        Commons.pagePop(context, data: data.jobItem.requestJob);
      } else {
        Commons.page(context, routeCare5, arguments: CareStep5Page(data: data));
      }
    });
  }

  @override
  void onConfirmBtn() {
    if (_schedule.length > 0) {
      flag.enableConfirm(fn: onUpDate);
    } else {
      flag.disableConfirm(fn: onUpDate);
    }
  }

  @override
  void onDataPage(_data) {
    onConfirmBtn();
  }

  Widget _timeSelecter() {
    bool isEnabled = _scheCtrl.selecter.isEmpty && !_editMode;
    if (_scheCtrl.eventer.isEmpty) isEnabled = _scheCtrl.selecter.isNotEmpty;
    return lRoundContainer(
        padding: padding_20_LR,
        alignment: Alignment.center,
        bgColor: Colors.white,
        child: lInkWell(
          onTap: () {
            try {
              if (!isEnabled) return;
              if (_scheCtrl.selecter.isNotEmpty) {
                _showTimePicker(false, _getPage(append: !_editMode));
              } else if (_schedule.isNotEmpty && _schedule[_getPage()].workTimes.isNotEmpty) {
                if (_schedule[_getPage()].workTimes.length < 3) {
                  _showTimePicker(false, _getPage());
                } else {
                  _showdlg("시간추가_에러_초과".tr());
                }
              } else {
                _closePanel();
                _showdlg("날짜선택요청".tr());
              }
            } catch (e) {
              _showdlg("날짜선택요청".tr());
            }
            onUpDate();
          },
          child: lRoundContainer(
            padding: padding_18_TB,
            bgColor: Colors.white,
            borderColor: isEnabled ? Commons.getColor() : color_dedede,
            borderWidth: 1,
            alignment: Alignment.center,
            child: Row(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: [
              lText("시간추가".tr(), style: st_14(textColor: isEnabled ? color_222222 : color_b2b2b2, fontWeight: FontWeight.w500)),
              Lcons.add(
                color: Commons.getColor(),
                disableColor: color_dedede,
                size: 16,
                isEnabled: isEnabled,
              )
            ]),
          ),
        ));
  }

  void _showTimePicker(bool isEdit, int id, {DateTime? stime, DateTime? etime, int? index}) {
    showScheduleTimePicker(
      context,
      (DialogAction a, DateTime start, DateTime end) {
        switch (a) {
          case DialogAction.close:
            {
              if (!isEdit && _schedule[id].getTimes.isEmpty) {
                _scheCtrl.eventer.clear(id: id);
                _schedule.removeAt(id);
              }
            }
            break;
          case DialogAction.update:
            {
              // NOTE: ignore
            }
            break;
          case DialogAction.confirm:
            {
              if (isEdit) {
                _schedule[id].updateTime(index ?? 0, stime: start, etime: end, isRepeat: _schedule[id].getSchedule.times![index ?? 0].is_repeat == 1);
              } else {
                if (_schedule.length - 1 < id) _schedule.add(JobSchedule(bookingdate: []));
                _appendSchedule(id);
                DateTime temp = DateTime.now();
                if (start.isAfter(end)) {
                  temp = start;
                  start = end;
                  end = temp;
                }
                if (_isAvailable(id, start: start, end: end)) {
                  int duration = start.difference(end).inMinutes.abs();
                  if (duration > 0) {
                    _schedule[id].addTime(
                      stime: start,
                      etime: end,
                      id: id,
                      durationtime: start.difference(end).inMinutes.abs(),
                      caredateUntil: StringUtils.formatYMD(_schedule[id].getDates.last),
                    );
                    _openPanel();
                    _updateId();
                  } else {
                    _showdlg("시간확인".tr());
                  }
                } else {
                  _showdlg("시간추가_에러_중복".tr(),
                      callback: (a) => {
                            if (a == DialogAction.yes) {_showTimePicker(isEdit, id, stime: start, etime: end)}
                          });
                }
              }
              _movePage(_schedule.length - 1);
              onConfirmBtn();
            }
            break;
          default:
        }
        onUpDate();
      },
      btnStr: isEdit ? "수정".tr() : "추가".tr(),
      start: stime,
      end: etime,
    );
  }

  bool _isAvailable(int id, {DateTime? start, DateTime? end}) {
    bool isAvailable = true;
    bool containDate = _scheCtrl.eventer.getDates(id).where((date) {
      bool isContain = false;
      _schedule.forEach((schedule) {
        int idx = _schedule.indexOf(schedule);
        if (idx != id) {
          isContain = schedule.getDates.contains(date) || schedule.getDates.contains(Jiffy(date).startOf(Units.DAY).dateTime);
        }
      });
      return isContain;
    }).isNotEmpty;
    if (containDate) {
      if (start != null && end != null) {
        _schedule.forEach((schedule) {
          bool ret = _isBetween(DateTimeRange(start: start, end: end), schedule.getTimes);
          if (!ret) isAvailable = ret;
        });
      } else {
        List<DateTimeRange> differ = _schedule[id].getTimes;
        _schedule.forEach((schedule) {
          int idx = _schedule.indexOf(schedule);
          if (idx != id) {
            differ.forEach((time) {
              bool ret = _isBetween(time, schedule.getTimes);
              if (!ret) isAvailable = ret;
            });
          }
        });
      }
      return isAvailable;
    } else {
      if (start != null && end != null && _schedule[id].getTimes.isNotEmpty) {
        isAvailable = _isBetween(DateTimeRange(start: start, end: end), _schedule[id].getTimes);
      }
      return isAvailable;
    }
  }

  void _removeSchedule(int id, {int? index}) {
    if (index == null) {
      _schedule.removeAt(id);
      _scheCtrl.eventer.clear();
      _schedule.forEach((schedule) {
        int id = _schedule.indexOf(schedule);
        log.d({'schedule': id});
        schedule.getDates.forEach((date) {
          log.d({'date': date});
          _scheCtrl.eventer.append(date, event: id);
        });
      });
      _movePage(_schedule.length - 1);
    } else {
      _removeTime(id, index);
    }
    if (_schedule.isEmpty) {
      _scheduleId = 0;
      _closePanel();
    } else {
      _scheduleId = _schedule.length;
    }
    onConfirmBtn();
  }

  void _removeTime(int id, int index) {
    _schedule[id].removeTimes(index);
  }

  bool selectDate(DateTime date, List<dynamic> events) {
    if (_isNotAbAdd && !_editMode) return false;
    if (events.contains(_getPage(append: !_editMode))) {
      events.remove(_getPage(append: !_editMode));
    } else {
      if (date.isUtc) date = date.toLocal().add(date.toLocal().timeZoneOffset * -1);
      int idx = _getPage(append: !_editMode);
      events.add(idx);
    }
    onUpDate();
    return true;
  }

  int _getPage({bool append = false}) {
    try {
      int page = _pageController.page!.toInt();
      if (_schedule.length - 1 < page) return _schedule.length - 1 < 0 ? 0 : _schedule.length - 1;
      if (_schedule.isNotEmpty && append) page++;
      return page;
    } catch (e) {
      return 0;
    }
  }

  void _updateId() {
    _scheduleId = _schedule.length;
    if (_scheduleId > 4) {
      _scheduleId = _getPage();
    }
    onUpDate();
  }

  bool _repeatChecker(List<DateTime> date) {
    date.sort();
    return date.last.weekday <= DateTime.sunday && date.last.difference(date.first) < Duration(days: 7);
  }

  List<DateTime> _updateRepeatSchedule(bool clear, int id, {DateTime? endDate}) {
    List<DateTime> list = [];
    int size = 52; // 1 year
    if (_repeatChecker(_schedule[id].getDates)) {
      _schedule[id].getDates.forEach((schedule) {
        if (endDate != null) size = (endDate.add(Duration(days: 1)).difference(schedule).inDays / 7).ceil();
        list.addAll(List.generate(size, (counter) => schedule.add(Duration(days: counter * 7))));
      });
    }
    list.sort();
    onUpDate();
    return list;
  }

  void _showDeleteMsg(String message, {Function? cancel, Function? ok}) {
    showNormalDlg(
      btnLeft: "취소".tr(),
      btnRight: "삭제".tr(),
      message: message,
      onClickAction: (action) {
        switch (action) {
          case DialogAction.yes:
            if (ok != null) ok();
            break;
          case DialogAction.no:
            if (cancel != null) cancel();
            break;
          default:
        }
      },
    );
  }

  void _showRepeatSelecter(List<DateTime> list, int id, int index) {
    _closePanel();
    int weekdays = list.where((e) => e.isAfter(list.last.add(Duration(days: -6)))).toList().length;
    Map<DateTime, List<int>> events = {};
    list.forEach((date) => events.putIfAbsent(date, () => [0]));
    showRepeatSelecter(
      context,
      list.last,
      (a, updated) {
        switch (a) {
          case DialogAction.update:
            {
              // NOTE: ignore
            }
            break;
          case DialogAction.close:
            {
              // NOTE: ignore
            }
            break;
          case DialogAction.fail:
            {
              Commons.showToast("최대1년까지선택할수있습니다".tr());
              onUpDate();
            }
            break;
          case DialogAction.confirm:
            {
              updated.sort();
              if (_isRepeatAvailable(id, index, updated)) {
                updated.forEach((date) {
                  _scheCtrl.eventer.append(date, event: id);
                });
                _schedule[id].times![index].is_repeat = 1;
                _schedule[id].times![index].caredate_until = StringUtils.formatYMD(updated.last);
                _closePanel();
              } else {
                _showdlg("시간추가_에러_중복".tr());
                _showRepeatSelecter(list, id, index);
              }
              onUpDate();
            }
            break;
          default:
        }
      },
      originEvents: events,
      end: (52 * weekdays).round(),
      index: id,
      info: "매주반복안내_구직".tr(),
      infoHighlight: "매주반복안내_하이라이트2_구직".tr(),
    );
  }

  void _showdlg(String message, {Function(DialogAction)? callback}) {
    showNormalDlg(message: message, onClickAction: (a) => {if (callback != null) callback(a)});
  }

  void _closePanel() {
    _panelArrow = Lcons.nav_top(color: color_cecece);
    _panelController.close();
    onUpDate();
  }

  void _openPanel() {
    if (_schedule.isEmpty) return;
    _panelArrow = Lcons.nav_bottom(color: color_cecece);
    _panelController.open();
    onUpDate();
  }

  Widget _pageIndicator() {
    if (_schedule.length > 1) {
      return Container(
          margin: padding_10,
          alignment: Alignment.topCenter,
          child: SmoothPageIndicator(
              effect: WormEffect(
                spacing: 6,
                dotHeight: 7,
                dotWidth: 7,
                dotColor: color_cecece,
                activeDotColor: Commons.getColor(),
              ),
              onDotClicked: (idx) => _movePage(idx),
              controller: _pageController,
              count: _schedule.length));
    } else {
      return Container();
    }
  }

  Widget titleBlock({required DateTime startTime, required DateTime endTime, List<DateTime>? date, String? timeDesc, Color color = color_222222, Function? editDateFun, bool isRepeat = false, Function? deleteDateFun, bool isShowing = true}) {
    DateFormat _koMonthDay = DateFormat().addPattern("MM${"월".tr()} dd${"일".tr()}");
    DateFormat _koYearMonthDay = DateFormat().addPattern("MM${"월".tr()} dd${"일".tr()}");
    late String lastDate, firstDate, year = '';

    if (date == null || date.isEmpty) {
      firstDate = '';
      lastDate = '';
    } else if (endTime.year > startTime.year) {
      lastDate = _koYearMonthDay.format(endTime);
      firstDate = _koMonthDay.format(startTime);
      year = DateFormat().addPattern("(yy${"년".tr()})").format(endTime);
    } else {
      lastDate = _koMonthDay.format(endTime);
      firstDate = _koMonthDay.format(startTime);
    }

    timeDesc = timeDesc ?? "최대1년".tr();
    lastDate = year.isEmpty && firstDate == lastDate ? '' : lastDate;
    lastDate = year + lastDate;

    return Row(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Container(
          width: data.width(context, 0.4),
          child: LAutoSizeHightLightText(
            '$firstDate ${lastDate.isNotEmpty ? "-" : ""} $lastDate',
            term: year,
            style: st_16(textColor: year.isEmpty ? color : color_222222, fontWeight: FontWeight.bold),
            highlightStyle: st_16(textColor: _editMode ? color : Commons.getColor(), fontWeight: FontWeight.bold),
            textAlign: TextAlign.left,
          )),
      Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            if (_editMode || _scheCtrl.selecter.isEmpty)
              EditTextButton(
                  callback: (repeat, state, callback) {
                    editDateFun!(repeat, state, callback);
                  },
                  enableTitle: "저장".tr(),
                  disableTitle: "날짜수정".tr(),
                  isRepeat: isRepeat,
                  checked: _editMode),
            sb_w_08,
            if (isShowing && !_editMode)
              lInkWell(
                  onTap: () {
                    try {
                      if (_scheCtrl.selecter.isNotEmpty) {
                        _showTimePicker(false, _getPage() + 1);
                      } else {
                        _closePanel();
                        _showdlg("날짜선택요청".tr());
                      }
                    } catch (e) {
                      _showdlg("날짜선택요청".tr());
                    }
                    onUpDate();
                  },
                  child: Container(
                    height: 28,
                    width: 28,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(color: color_c4c4c4.withAlpha(52), borderRadius: BorderRadius.circular(100)),
                    child: Lcons.add(color: color_999999, size: 20),
                  )),
          ],
        ),
      ),
    ]);
  }

  bool _isRepeatAvailable(int id, int index, List<DateTime> list) {
    bool enable = true;
    if (_schedule.length > 0) {
      // _jobDate.forEach((schedule, value) {
      //   DateTime sTime = _jobDate[id]!.sTime[index];
      //   DateTime eTime = _jobDate[id]!.eTime[index];
      //   try {
      //     if (schedule != id) {
      //       if (value.dateTime.where((element) => list.where((e) => e.isAtSameMomentAs(element)).isNotEmpty).isNotEmpty) {
      //         if (value.sTime.contains(sTime) || value.eTime.contains(eTime)) {
      //           enable = false;
      //         }
      //         for (var i = 0; i < value.sTime.length; i++) {
      //           DateTime start = value.sTime[i];
      //           DateTime end = value.eTime[i];
      //           if (start.isBefore(eTime) && end.isAfter(sTime)) {
      //             enable = false;
      //           }
      //         }
      //       }
      //     }
      //   } catch (e) {
      //     return;
      //   }
      // });
    }
    return enable;
  }

  bool _isBetween(DateTimeRange newTime, List<DateTimeRange> oldTime) {
    try {
      if (oldTime.isEmpty) return true;
      return oldTime.where((range) {
        if (range.start.year < 2021) {
          DateTime start = Jiffy(DateTime.now()).startOf(Units.DAY).dateTime;
          DateTime end = Jiffy(DateTime.now()).startOf(Units.DAY).dateTime;
          range = DateTimeRange(start: start.add(Duration(hours: range.start.hour, minutes: range.start.minute)), end: end.add(Duration(hours: range.end.hour, minutes: range.end.minute)));
        }
        bool ret = ((range.start.isBefore(newTime.start) || range.start.isAtSameMomentAs(newTime.start)) && // start to start
                (range.end.isAfter(newTime.end) || range.end.isAtSameMomentAs(newTime.end))) // end to end
            ||
            ((range.start.isBefore(newTime.end) || range.start.isAtSameMomentAs(newTime.end)) && // start to end
                (range.end.isAfter(newTime.start) || range.end.isAtSameMomentAs(newTime.start))); // end to start
        return ret;
      }).isEmpty;
    } catch (e) {
      return true;
    }
  }

  Map<DateTime, List<int>> _toMaps() {
    Map<DateTime, List<int>> map = {};
    _schedule.forEach((sched) {
      int id = _schedule.indexOf(sched);
      sched.schedules.forEach((date) {
        map.update(StringUtils.getUtcFromDate(date), (ids) {
          ids.add(id);
          ids.toSet();
          return ids;
        }, ifAbsent: () => [id]);
      });
    });
    return map;
  }

  void _appendSchedule(int id) {
    if (_schedule.length - 1 < id) _schedule.add(JobSchedule(bookingdate: []));
    if (_schedule[id].bookingDate.isEmpty) {
      _scheCtrl.saveEvent();
      List<DateTime> schedule = _scheCtrl.eventer.getDates(id);
      if (schedule.isNotEmpty) {
        _schedule[id].addSchedule(schedule);
      }
    }
  }

  bool _isEnableRepeat(List<DateTime> date) {
    date.sort();
    return date.last.weekday < DateTime.sunday && date.last.difference(date.first) < Duration(days: 7);
  }

  bool get _isNotAbAdd => _schedule.length > 4;

  void _appendEvent() {
    _scheCtrl.eventer.imports(_toMaps());
    Future.delayed(Duration(milliseconds: 100), () => onConfirmBtn());
  }

  void _movePage(int page) {
    _pageController.animateToPage(page, curve: Curves.ease, duration: Duration(milliseconds: 100)).then((value) => onUpDate());
  }
}
