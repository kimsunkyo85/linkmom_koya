import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/req_job_data.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_dailog/dlg_info_view.dart';
import 'package:linkmom/utils/listview/list_type_area_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../content_view.dart';
import '../step_view.dart';
import 'care_step_3_page.dart';

class CareStep2Page extends BaseStateful {
  @override
  _CareStep2PageState createState() => _CareStep2PageState();

  CareStep2Page({this.data, this.viewMode, this.requestJob});

  final DataManager? data;
  final ViewMode? viewMode;
  final ReqJobData? requestJob;
}

class _CareStep2PageState extends BaseStatefulState<CareStep2Page> {
  late ReqJobData requestJob;

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 2;

    ///수정일때는 STEP을 보여주지 않는다.
    if (getViewType() == ViewType.modify) {
      data.jobItem.childInfo.isShow = false;
    }

    data.jobItem.requestJob = _data.jobItem.requestJob.clone();
    data.jobItem.makeData();
    data.jobItem.jobData = Commons.getJobTransportsData();

    log.d('『GGUMBI』>>> onDataPage : data.jobItem.requestJob: ${data.jobItem.requestJob},  <<< ');
    onDataPage(data.jobItem.requestJob);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        resizeToAvoidBottomPadding: false,
        appBar: jobAppBar(
          "보육장소".tr(),
          // isBack: getViewType() == ViewType.modify ? false : true,
          isClose: getViewType() == ViewType.apply ? true : false,
          hide: getViewType() == ViewType.modify ? false : true,
          onClick: () {
            if (getViewType() == ViewType.modify) {
              Commons.pagePop(context, data: requestJob);
            } else {
              Commons.showCloseDlg();
            }
          },
        ),
        body: lContainer(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step, stepCnt: 6),
              Expanded(
                child: Column(
                  children: [
                    Expanded(
                      child: lScrollView(
                        child: Container(
                          width: double.infinity,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              if (data.jobItem.childInfo.isShow) StepView(valueStep: data.jobItem.childInfo.step),
                              if (!data.jobItem.childInfo.isShow) sb_h_20,
                              contentView(
                                  data: ContentData(content: "돌봄장소_선택_구직".tr(), icon: Lcons.info().name, isOverLapRight: true, iconColor: Commons.getColor()),
                                  onClick: () {
                                    showNormalDlg(messageWidget: dlgInfoView(title: "돌봄유형".tr(), data: data.jobItem.lsJobArea));
                                  }),
                              sb_h_20,
                              ListTypeAreaSelector(
                                data: data.jobItem.lsJobArea,
                                isOverlap: true,
                                onItemClick: (value) {
                                  log.d('『GGUMBI』>>> build : data: $value,  <<< ');
                                  // log.d('『GGUMBI』>>> build : value as SingleItem: ${value as SingleItem},  <<< ');
                                  onConfirmBtn();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    _confirmButton(),
                  ],
                ),
              ),
              // wdDivider(),
            ],
          ),
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn(Commons.bottomBtnTitle(getViewType()), isEnabled: flag.isConfirm, onClickAction: () {
      if (getViewType() == ViewType.modify) {
        Commons.pagePop(context, data: data.jobItem.requestJob);
      } else {
        Commons.page(context, routeCare3, arguments: CareStep3Page(data: data));
      }
    });
  }

  @override
  void onConfirmBtn() {
    bool isHome = false;
    data.jobItem.requestJob.possible_area!.clear();
    data.jobItem.lsJobArea.forEach((value) {
      SingleItem item = value;
      if (item.isSelected) {
        isHome = true;
        data.jobItem.requestJob.possible_area!.add(item.type);

        if (item.type == PossibleArea.mom_daddy.value) {
          data.jobItem.requestJob.is_groupboyuk = StringUtils.getBoolToInt(item.values![0].isSelected);
        } else {
          data.jobItem.requestJob.is_dongsiboyuk = StringUtils.getBoolToInt(item.values![0].isSelected);
        }
      } else {
        ///링크쌤집, 맘대집 선택 해제시 데이터만 사용안함으로 두고, 화면 데이터는 그대로 유지한다.
        if (item.type == PossibleArea.mom_daddy.value) {
          data.jobItem.requestJob.is_groupboyuk = 0;
        } else {
          data.jobItem.requestJob.is_dongsiboyuk = 0;
        }
      }
    });

    log.d('『GGUMBI』>>> onConfirmBtn : data.jobItem.requestJob.: ${data.jobItem.requestJob},  <<< ');

    if (isHome) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  @override
  void onDataPage(_data) {
    if (_data is ReqJobData) {
      var item = _data;
      log.d('『GGUMBI』>>> onDataPage : item: $item,  <<< ');
      if (_data == null) {
        onConfirmBtn();
        return;
      }

      log.d('『GGUMBI』>>> onDataPage : item: ${item.possible_area}, ${data.jobItem.requestJob.is_groupboyuk}, ${data.jobItem.requestJob.is_dongsiboyuk} <<< ');
      //링크쌤집, 맘대디집
      data.jobItem.lsJobArea.asMap().forEach((index, value) {
        item.possible_area!.forEach((item) {
          if (value.type == item) {
            value.isSelected = true;
            if (item == PossibleArea.mom_daddy.value) {
              value.values![0].isSelected = StringUtils.getIntToBool(data.jobItem.requestJob.is_groupboyuk);
            } else {
              value.values![0].isSelected = StringUtils.getIntToBool(data.jobItem.requestJob.is_dongsiboyuk);
            }
          }
        });
      });
    } else {}
    onConfirmBtn();
  }
}
