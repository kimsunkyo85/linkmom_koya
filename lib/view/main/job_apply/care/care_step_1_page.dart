import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/req_job_data.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_dailog/dlg_info_view.dart';
import 'package:linkmom/utils/listview/list_type_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/content_view.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../step_view.dart';
import 'care_step_2_page.dart';

class CareStep1Page extends BaseStateful {
  @override
  _CareStep1PageState createState() => _CareStep1PageState();

  CareStep1Page({this.data, this.viewMode});

  final DataManager? data;
  final ViewMode? viewMode;
}

class _CareStep1PageState extends BaseStatefulState<CareStep1Page> {
  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);

    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 1;

    ///수정일때는 STEP을 보여주지 않는다.
    if (getViewType() == ViewType.modify) {
      data.jobItem.childInfo.isShow = false;
      data.jobItem.requestJob = _data.jobItem.requestJob.clone();
    } else {
      data.jobItem.requestJob = ReqJobData.init();
    }
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.requestJob: ${data.jobItem.requestJob},  <<< ');
    onDataPage(data.jobItem.requestJob);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        resizeToAvoidBottomPadding: false,
        appBar: jobAppBar(
          "돌봄유형".tr(),
          // isBack: getViewType() == ViewType.modify ? false : true,
          isClose: getViewType() == ViewType.apply ? true : false,
          hide: getViewType() == ViewType.modify ? false : true,
          onClick: () {
            if (getViewType() == ViewType.modify) {
              Commons.pagePop(context);
            } else {
              Commons.showCloseDlg();
            }
          },
        ),
        body: lContainer(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step, stepCnt: 6),
              Expanded(
                child: Column(
                  children: [
                    Expanded(
                      child: lScrollView(
                        child: Container(
                          width: double.infinity,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              if (data.jobItem.childInfo.isShow) StepView(valueStep: data.jobItem.childInfo.step),
                              if (!data.jobItem.childInfo.isShow) sb_h_20,
                              contentView(
                                  data: ContentData(content: "돌봄유형_안내메시지".tr(), icon: Lcons.info().name, isOverLapRight: true, iconColor: color_linkmom),
                                  onClick: () {
                                    showNormalDlg(messageWidget: dlgInfoView(title: "돌봄유형".tr(), data: data.jobItem.lsJobType));
                                  }),
                              sb_h_20,
                              ListTypeSelector(
                                data: data.jobItem.lsJobType,
                                isOverlap: true,
                                onItemClick: (value) {
                                  SingleItem item = value as SingleItem;

                                  ///서비스 타입 (등원,하원,보육,학습...)
                                  if (item.type == ServiceType.serviceType_3.index) {
                                    showNormalDlg(message: "서비스준비중".tr());
                                    item.isSelected = false;
                                    onConfirmBtn();
                                    return;
                                  }
                                  onConfirmBtn();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    _confirmButton(),
                  ],
                ),
              ),
              // wdDivider(),
            ],
          ),
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn(Commons.bottomBtnTitle(getViewType()), isEnabled: flag.isConfirm, onClickAction: () {
      if (getViewType() == ViewType.modify) {
        Commons.pagePop(context, data: data.jobItem.requestJob);
      } else {
        Commons.page(context, routeCare2, arguments: CareStep2Page(data: data));
      }
    });
  }

  @override
  void onConfirmBtn() {
    bool isHome = false;
    data.jobItem.requestJob.servicetype!.clear();
    data.jobItem.lsJobType.forEach((value) {
      SingleItem item = value;
      if (item.isSelected) {
        isHome = true;
        data.jobItem.requestJob.servicetype!.add(item.type);
      }
    });

    if (isHome) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  @override
  void onDataPage(_data) {
    if (_data is ReqJobData) {
      var item = _data;

      if (_data == null) {
        onConfirmBtn();
        return;
      }
      log.d('『GGUMBI』>>> onDataPage : item.servicetype: ${item.servicetype},  <<< ');

      //돌봄유형 등원, 하원, 보육, 학습, 입주가사, 이유식
      data.jobItem.lsJobType.asMap().forEach((index, value) {
        log.d('『GGUMBI』>>> onDataPage : index: ${value.type},  <<< ');
        item.servicetype!.forEach((item) {
          log.d('『GGUMBI』>>> onDataPage : index item: $item,: ${value.type} ,  <<< ');
          if (value.type == item) {
            value.isSelected = true;
          }
        });
      });
    }
    onConfirmBtn();
  }
}
