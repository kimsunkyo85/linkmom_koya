import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/linkmom_save_request.dart';
import 'package:linkmom/data/network/models/req_job_data.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/listview/list_area_view.dart';
import 'package:linkmom/utils/listview/list_cate_type_view.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_4_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/linkmom_schedule_page.dart';
import 'package:linkmom/view/main/job_apply/title_value_view.dart';

import '../../../../base/base_stateful.dart';
import '../../../../data/storage/flag_manage.dart';
import '../../../../main.dart';
import '../../../../route_name.dart';
import '../../../../utils/modal_bottom_sheet/push_modal_bottom.dart';
import '../content_view.dart';
import '../step_view.dart';
import 'care_step_1_page.dart';
import 'care_step_2_page.dart';
import 'care_step_3_page.dart';
import 'care_step_5_page.dart';
import 'care_step_7_page.dart';

class CareStep6Page extends BaseStateful {
  @override
  _CareStep6PageState createState() => _CareStep6PageState();

  CareStep6Page({this.data, this.viewMode});

  final DataManager? data;
  final ViewMode? viewMode;
}

class _CareStep6PageState extends BaseStatefulState<CareStep6Page> {
  int flex = 3;
  int flex2 = 7;

  ///주소 최대 라인
  int maxLineAddress = 3;

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 6;
    data.jobItem.requestJob = _data.jobItem.requestJob.clone();
    log.d('『GGUMBI』>>> initState : data.jobItem.request.requestJob: ${data.jobItem.requestJob},  <<< ');
    log.d('『GGUMBI』>>> initState : data.jobItem.request.requestJob.careschedule: ${data.jobItem.requestJob.careschedule!.length},  <<< ');
    log.d('『GGUMBI』>>> initState : data.jobItem.request.requestJob.careschedule: ${data.jobItem.requestJob.careschedule},  <<< ');
    onDataPage(data.jobItem.requestJob);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      ///하드웨어 back key 막기
      onWillPop: () => Future.value(false),
      child: lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          appBar: jobAppBar(
            "등록내역".tr(),
            isBack: false,
            hide: true,
            onClick: () {
              Commons.showCloseDlg();
            },
          ),
          body: lContainer(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step, stepCnt: 6),
                Expanded(
                  child: Column(
                    children: [
                      Expanded(
                        child: lScrollView(
                          padding: padding_Item_TB,
                          child: Container(
                            child: Column(
                              children: [
                                //Step
                                Padding(
                                  padding: padding_20_LR,
                                  child: StepView(valueStep: data.jobItem.childInfo.step),
                                ),
                                //돌봄유형
                                Padding(
                                  padding: padding_20_LR,
                                  child: _careTypeView(),
                                ),
                                lDivider20(),
                                //보육장소
                                Padding(
                                  padding: padding_20_LR,
                                  child: _areaView(),
                                ),
                                lDivider20(),
                                //이동방법
                                Padding(
                                  padding: padding_20_LR,
                                  child: _moveView(),
                                ),
                                lDivider20(),
                                //날짜,시간
                                Padding(
                                  padding: padding_20_LR,
                                  child: _dateView(),
                                ),
                                lDivider20(),
                                //희망근무조건
                                Padding(
                                  padding: padding_20_LR,
                                  child: _locationView(),
                                ),
                                // _groupView(),
                              ],
                            ),
                          ),
                        ),
                      ),
                      _confirmButton(),
                    ],
                  ),
                ),
                // wdDivider(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn("링크쌤신청등록".tr(), onClickAction: () async {
      flag.enableLoading(fn: () => onUpDate());
      await apiHelper.requestLinkMomSave(LinkMomSaveRequest(reqdata: jsonEncode(data.jobItem.requestJob))).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          if (Flags.getFlag(Flags.KEY_PUSH_LINKMOM).isShown) {
            Commons.page(context, routeCare7, arguments: CareStep7Page(data: data));
          } else {
            Flags.addFlag(Flags.KEY_PUSH_LINKMOM, isShown: true);
            showPushBottomSheet(context, onClick: (action) {
              if (DialogAction.confirm == action) {
                Commons.page(context, routeCare7, arguments: CareStep7Page(data: data));
              }
            });
          }
        } else {
          showNormalDlg(message: response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    });
  }

  @override
  void onConfirmBtn() {
    log.d('『GGUMBI』>>> onConfirmBtn ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
    flag.enableConfirm(fn: () => onUpDate());
  }

  @override
  void onDataPage(_data) {
    if (_data is ReqJobData) {
      var _item = _data;

      if (_data == null) {
        onConfirmBtn();
        return;
      }

      log.d('『GGUMBI』>>> onDataPage : item: $_item,  <<< ');
      log.d('『GGUMBI』>>> onDataPage : item: ${data.jobItem.lsJobType.length},  <<< ');
      log.d('『GGUMBI』>>> onDataPage : data.jobItem.lsJobCareData: ${data.jobItem.lsJobCareData},  <<< ');
      //돌봄유형 데이터 만들기
      data.jobItem.lsJobCareData.clear();
      data.jobItem.lsJobCareData.addAll(data.jobItem.lsJobType.map((item) => SingleItem.clone(item)).toList());
      // data.jobItem.lsJobCareData.addAll(data.jobItem.lsJobType);
      log.d('『GGUMBI』>>> onDataPage : data.jobItem.lsJobCareData: ${data.jobItem.lsJobCareData[0]},  <<< ');
      log.d('『GGUMBI』>>> onDataPage : data.jobItem.lsJobCareData: ${data.jobItem.lsJobCareData[1]},  <<< ');
      log.d('『GGUMBI』>>> onDataPage : data.jobItem.lsJobCareData: ${data.jobItem.lsJobCareData[2]},  <<< ');
      _item.servicetype!.forEach((value) {
        data.jobItem.lsJobCareData.forEach((item) {
          if (value == item.type) {
            item.isSelected = true;
          }
          // log.d('『GGUMBI』>>> onDataPage : value: $item,  <<< ');
        });
      });
      log.d('『GGUMBI』>>> onDataPage : data.jobItem.lsJobCareData: ${data.jobItem.lsJobCareData[0]},  <<< ');
      log.d('『GGUMBI』>>> onDataPage : data.jobItem.lsJobCareData: ${data.jobItem.lsJobCareData[1]},  <<< ');
      log.d('『GGUMBI』>>> onDataPage : data.jobItem.lsJobCareData: ${data.jobItem.lsJobCareData[2]},  <<< ');

      //보육장소 데이터 만들기
      data.jobItem.makeData();
      data.jobItem.lsJobAreaData.clear();
      data.jobItem.lsJobAreaData.addAll(data.jobItem.lsJobArea.map((item) => SingleItem.clone(item)).toList());
      _item.possible_area!.forEach((value) {
        data.jobItem.lsJobAreaData.forEach((item) {
          if (value == item.type) {
            item.isSelected = true;
            if (data.jobItem.requestJob.is_groupboyuk == 1 && item.type == PossibleArea.mom_daddy.value) {
              item.values![0].isSelected = true;
            } else if (data.jobItem.requestJob.is_dongsiboyuk == 1 && item.type == PossibleArea.link_mom.value) {
              item.values![0].isSelected = true;
            }
          }
        });
      });

      //희망급여 데이터 만들기
      data.jobItem.lsJobPayData.clear();
      data.jobItem.lsJobPayData.add(SingleItem("등원".tr(), content: data.jobItem.requestJob.fee_gotoschool.toString()));
      data.jobItem.lsJobPayData.add(SingleItem("하원".tr(), content: data.jobItem.requestJob.fee_afterschool.toString()));
      data.jobItem.lsJobPayData.add(SingleItem("보육".tr(), content: data.jobItem.requestJob.fee_boyuk.toString()));

      //돌봄대상 데이터 만들기
      data.jobItem.lsJobCareTargetData.clear();
      _item.care_ages!.asMap().forEach((index, value) {
        data.jobItem.lsJobCareTargetType.forEach((item) {
          if (value == item.type) {
            if (index == _item.care_ages!.length - 1) {
              item.isSelected = true;
            }
            data.jobItem.lsJobCareTargetData.add(item);
          }
        });
      });

      //한줄자기소개
      data.tcJob.text = _item.introduce;
      data.tcJobCare.text = _item.care_introduce;
    } else {}
    onConfirmBtn();
  }

  ///돌봄유형
  Widget _careTypeView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        contentView(
          data: ContentData(content: "돌봄유형".tr(), isModify: true, modifyStyle: st_15(textColor: Commons.getColor())),
          onClick: () async {
            log.d('『GGUMBI』>>> _dateView : 날짜 시간 수정 이벤트  <<< ');
            var _data = await Commons.page(context, routeCare1,
                arguments: CareStep1Page(
                  data: data,
                  viewMode: ViewMode(viewType: ViewType.modify),
                ));
            log.d('『GGUMBI』>>> _moveView : _data: $_data,  <<< ');
            if (_data != null) {
              data.jobItem.requestJob = _data;
              onDataPage(data.jobItem.requestJob);
            }
          },
        ),
        sb_h_10,
        Padding(
          padding: padding_30_LR,
          child: ListCareTypeView(
            data: data.jobItem.lsJobCareData,
            childAspectRatio: 1.0,
            count: 3,
            imageHeight: 45,
            imageWidth: 45,
          ),
        ),
      ],
    );
  }

  ///보육장소
  Widget _areaView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        contentView(
          data: ContentData(content: "보육장소".tr(), isModify: true, modifyStyle: st_15(textColor: Commons.getColor())),
          onClick: () async {
            log.d('『GGUMBI』>>> _dateView : 날짜 시간 수정 이벤트  <<< ');
            var _data = await Commons.page(context, routeCare2,
                arguments: CareStep2Page(
                  data: data,
                  viewMode: ViewMode(viewType: ViewType.modify),
                ));
            log.d('『GGUMBI』>>> _moveView : _data: $_data,  <<< ');
            log.d('『GGUMBI』>>> _areaView : data.jobItem.requestJob: ${data.jobItem.requestJob},  <<< ');
            if (_data != null) {
              data.jobItem.requestJob = _data;
              onDataPage(data.jobItem.requestJob);
            }
          },
        ),
        ListAreaView(
          data: data.jobItem.lsJobAreaData,
          count: 2,
          imageHeight: 45,
          imageWidth: 45,
        ),
      ],
    );
  }

  ///이동방법
  Widget _moveView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        contentView(
            data: ContentData(
              content: "이동방법".tr(),
              isModify: true,
              modifyStyle: st_15(textColor: Commons.getColor()),
              rightWidget: Row(
                children: [
                  sb_w_20,
                  lText(getMoveType(), style: st_b_16()),
                ],
              ),
            ),
            onClick: () async {
              log.d('『GGUMBI』>>> _dateView : 날짜 시간 수정 이벤트  <<< ');
              data.jobItem.jobData = Commons.getJobData(data.jobItem.request.servicetype, data.jobItem.request.possible_area);
              var _data = await Commons.page(context, routeCare3,
                  arguments: CareStep3Page(
                    data: data,
                    viewMode: ViewMode(viewType: ViewType.modify),
                  ));
              log.d('『GGUMBI』>>> _moveView : _data: $_data,  <<< ');
              log.d('『GGUMBI』>>> _areaView : data.jobItem.requestJob: ${data.jobItem.requestJob},  <<< ');
              if (_data != null) {
                data.jobItem.requestJob = _data;
                onDataPage(data.jobItem.requestJob);
              }
            }),
      ],
    );
  }

  ///날짜/시간
  Widget _dateView() {
    return Container(
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              contentView(
                  data: ContentData(content: "날짜시간".tr(), isModify: true, modifyStyle: st_15(textColor: Commons.getColor())),
                  onClick: () async {
                    log.d('『GGUMBI』>>> _dateView : 날짜 시간 수정 이벤트  <<< ');
                    var _data = await Commons.page(context, routeCare4,
                        arguments: CareStep4Page(
                          data: data,
                          viewMode: ViewMode(viewType: ViewType.modify),
                        ));
                    if (_data != null) {
                      data.jobItem.requestJob = _data;
                      onDataPage(data.jobItem.requestJob);
                    }
                    log.d('『GGUMBI』>>> _dateView : _data: $_data,  <<< ');
                  }),
              sb_h_15,
              titleValueView(
                "돌봄날짜".tr(),
                valueRight: getJobDate(),
                bottomAlign: Alignment.bottomLeft,
                flex: 1,
                flex2: 3,
              ),
              sb_h_15,
              lInkWell(
                onTap: () {
                  log.d('『GGUMBI』>>> _dateView : : ,  <<< ');
                  Commons.page(context, routeLinkmomSchedule, arguments: LinkmomSchedulePage(data: data.jobItem.requestJob.careschedule!));
                },
                child: lRoundContainer(
                  bgColor: color_eeeeee,
                  padding: padding_15,
                  child: lText(
                    "근무날짜확인".tr(),
                    style: st_b_16(fontWeight: FontWeight.w500),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  ///희망지역
  Widget _locationView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        contentView(
            data: ContentData(content: "희망근무조건".tr(), isModify: true, modifyStyle: st_15(textColor: Commons.getColor())),
            onClick: () async {
              log.d('『GGUMBI』>>> _dateView : 날짜 시간 수정 이벤트  <<< ');
              var _data = await Commons.page(context, routeCare5,
                  arguments: CareStep5Page(
                    data: data,
                    viewMode: ViewMode(viewType: ViewType.modify),
                  ));
              if (_data != null) {
                data.jobItem.requestJob = _data;
                onDataPage(data.jobItem.requestJob);
              }
              log.d('『GGUMBI』>>> _dateView : _data: $_data,  <<< ');
            }),
        Column(
          children: [
            sb_h_15,
            titleValueView(
              "희망지역".tr(),
              maxLine: 2,
              rightWidget: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Row(
                    children: [
                      lText("1순위".tr(), style: st_b_16(fontWeight: FontWeight.bold)),
                      sb_w_05,
                      Expanded(
                          child: lAutoSizeText(
                        data.jobItem.requestJob.job_area_address_1,
                        maxLines: maxLineAddress,
                        style: st_b_16(),
                      )),
                    ],
                  ),
                  sb_h_05,
                  Row(
                    children: [
                      lText("2순위".tr(), style: st_b_16(fontWeight: FontWeight.bold)),
                      sb_w_05,
                      Expanded(
                          child: lAutoSizeText(
                        data.jobItem.requestJob.job_area_address_2,
                        maxLines: maxLineAddress,
                        style: st_b_16(),
                      )),
                    ],
                  )
                ],
              ),
              flex: flex,
              flex2: 8,
            ),
          ],
        ),

        // lDivider(color: color_d2d2d2, padding: padding_20_TB, thickness: 1.0),
        Column(
          children: [
            titleValueView(
              "희망시급".tr(),
              flex: flex,
              flex2: 8,
              maxLineRight: maxLineAddress,
              padding: padding_0,
              rightWidget: Column(
                  children: data.jobItem.lsJobPayData
                      .map(
                        (e) => Padding(
                          padding: padding_05_B,
                          child: Row(
                            children: [
                              lText(
                                '${e.name} ${"돌봄".tr()}',
                                style: st_b_16(fontWeight: FontWeight.w500),
                              ),
                              sb_w_10,
                              /*Lcons(
                                e.icon!.name,
                                color: Commons.getColor(),
                                isEnabled: true,
                              ),
                              sb_w_05,*/
                              lText(
                                '${"시급".tr()} ${StringUtils.formatPay(int.parse(e.content))}${"원".tr()}',
                                style: st_b_16(fontWeight: FontWeight.bold, textColor: Commons.getColor()),
                              ),
                            ],
                          ),
                        ),
                      )
                      .toList()),
            ),
          ],
        ),
        lDivider(color: color_d2d2d2, padding: padding_15_TB, thickness: 1.0),
        Column(
          children: [
            titleValueView(
              "돌봄대상".tr(),
              flex: flex,
              flex2: 8,
              maxLineRight: maxLineAddress,
              padding: padding_0,
              rightWidget: Column(
                  children: data.jobItem.lsJobCareTargetData
                      .map(
                        (e) => Padding(
                          padding: e.isSelected ? padding_0 : padding_05_B,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              lText(
                                e.name,
                                style: st_b_16(fontWeight: FontWeight.w500),
                              ),
                              sb_w_03,
                              lText(
                                e.content,
                                style: st_b_14(),
                              ),
                            ],
                          ),
                        ),
                      )
                      .toList()),
            ),
          ],
        ),
        lDivider(color: color_d2d2d2, padding: padding_15_TB, thickness: 1.0),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            titleValueView("한줄자기소개".tr()),
            Padding(
              padding: padding_05_B,
              child: lText(data.jobItem.requestJob.introduce),
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            titleValueView("돌봄방식".tr()),
            Padding(
              padding: padding_05_B,
              child: lText(data.jobItem.requestJob.care_introduce),
            ),
          ],
        ),
      ],
    );
  }

  ///근무날짜
  String getJobDate() {
    String type = '';
    try {
      String minDate = data.jobItem.requestJob.caredate_min;
      String maxDate = data.jobItem.requestJob.caredate_max;
      if (StringUtils.validateString(maxDate)) {
        type = '${StringUtils.getStringToDate(minDate)} ~ ${StringUtils.getStringToDate(maxDate)}';
      } else {
        type = StringUtils.getStringToDate(minDate);
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getCareDate : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }

    return type;
  }

  ///이동수단
  String getMoveType() {
    StringBuffer st = StringBuffer();
    List<int> lsMove = [];
    lsMove = data.jobItem.requestJob.vehicle_id!;

    log.d('『GGUMBI』>>> getMoveType : lsMove: $lsMove,  <<< ');

    lsMove.forEach((value) {
      if (value == MoveType.waking.index) {
        //도보
        st.write('${"도보".tr()}/');
      } else if (value == MoveType.car.index) {
        //자차
        st.write('${"자동차".tr()}/');
      } else if (value == MoveType.transport.index) {
        //대중교통
        st.write('${"대중교통".tr()}/');
      }
    });

    String value = st.toString();
    if (value.length != 0 && value.contains("/")) {
      value = value.substring(0, value.toString().length - 1);
    }
    return value;
  }
}
