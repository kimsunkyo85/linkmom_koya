import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/req_job_data.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/data/storage/model/menu_file_data.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_movetype_select.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../content_view.dart';
import '../step_view.dart';
import 'care_step_4_page.dart';

class CareStep3Page extends BaseStateful {
  @override
  _CareStep3PageState createState() => _CareStep3PageState();

  CareStep3Page({this.data, this.viewMode, this.requestJob});

  final DataManager? data;
  final ViewMode? viewMode;
  final ReqJobData? requestJob;
}

class _CareStep3PageState extends BaseStatefulState<CareStep3Page> {
  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 3;

    ///수정일때는 STEP을 보여주지 않는다.
    if (getViewType() == ViewType.modify) {
      data.jobItem.childInfo.isShow = false;
    }
    data.jobItem.requestJob = _data.jobItem.requestJob.clone();
    data.jobItem.jobData = _data.jobItem.jobData; //도보,자차,대중교통 데이터
    log.d('『GGUMBI』>>> onData : data.jobItem.requestJob: ${data.jobItem.requestJob},  <<< ');
    _setMakeData();
    onDataPage(data.jobItem.requestJob);
  }

  ///도보,자동차,대중교통 아이템 데이터 만들기
  _setMakeData() {
    var transportsData = data.jobItem.jobData![ServicesData.Key_transports];
    log.d('transportsData: $transportsData,  <<< ');
    List<ServiceItem> serviceDatas = data.jobItem.jobData![ServicesData.Key_transports] as List<ServiceItem>;
    log.d('『GGUMBI』>>> _setMakeData : itmes: $serviceDatas,  <<< ');

    ///도보 - 1, 자동차 - 2, 대중교통 - 3
    data.jobItem.lsJobMoveType = Commons.makeTransports(serviceDatas);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: jobAppBar(
          "이동방법".tr(),
          // isBack: getViewType() == ViewType.modify ? false : true,
          isClose: getViewType() == ViewType.apply ? true : false,
          hide: getViewType() == ViewType.modify ? false : true,
          onClick: () {
            if (getViewType() == ViewType.modify) {
              Commons.pagePop(context);
            } else {
              Commons.showCloseDlg();
            }
          },
        ),
        body: lContainer(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step, stepCnt: 6),
              Expanded(
                child: Column(
                  children: [
                    Expanded(
                      child: lScrollView(
                        padding: padding_Item_TB,
                        child: Column(children: [
                          Padding(
                            padding: padding_20_LR,
                            child: Column(
                              children: [
                                if (data.jobItem.childInfo.isShow) StepView(valueStep: data.jobItem.childInfo.step),
                                if (!data.jobItem.childInfo.isShow) sb_h_20,
                                contentView(data: ContentData(content: "이동_메시지".tr(), isOverLapRight: true), onClick: () {}),
                                //보육일 경운엔 숨김
                                if (data.jobItem.request.servicetype != ServiceType.serviceType_2.index)
                                  Column(
                                    children: [
                                      sb_h_10,
                                      ListMoveTypeSelector(
                                        data: data.jobItem.lsJobMoveType,
                                        user_type: storageHelper.user_type,
                                        padding: padding_moveTypeLinkMom,
                                        borderRadius: 20,
                                        onItemClick: (value) {
                                          onConfirmBtn();
                                        },
                                      ),
                                    ],
                                  ),
                              ],
                            ),
                          ),
                        ]),
                      ),
                    ),
                    _confirmButton(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn(Commons.bottomBtnTitle(getViewType()), isEnabled: flag.isConfirm, onClickAction: () {
      if (getViewType() == ViewType.modify) {
        Commons.pagePop(context, data: data.jobItem.requestJob);
      } else {
        Commons.page(context, routeCare4, arguments: CareStep4Page(data: data));
      }
    });
  }

  @override
  void onConfirmBtn() {
    bool isMoveType = false;
    data.jobItem.requestJob.vehicle_id!.clear();
    data.jobItem.lsJobMoveType.asMap().forEach((index, value) {
      if (value.isSelected) {
        isMoveType = true;
        data.jobItem.requestJob.vehicle_id!.add(value.type);
      }
    });

    log.d('『GGUMBI』>>> onConfirmBtn : data.jobItem.requestJob.: ${data.jobItem.requestJob},  <<< ');

    if (isMoveType) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  @override
  void onDataPage(_data) {
    if (_data is ReqJobData) {
      var item = _data;

      if (_data == null) {
        onConfirmBtn();
        return;
      }

      log.d('『GGUMBI』>>> onDataPage : : ${data.jobItem.requestJob},  <<< ');

      //링크쌤집, 맘대디집
      data.jobItem.lsJobMoveType.asMap().forEach((index, value) {
        item.vehicle_id!.forEach((item) {
          if (value.type == item) {
            value.isSelected = true;
          }
        });
      });
    }

    onConfirmBtn();
  }
}
