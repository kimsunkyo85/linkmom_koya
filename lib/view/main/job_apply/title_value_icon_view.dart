import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

///제목,아이콘,체크박스, 드럽다운
Widget titleValueIconView(DataManager data, title, {String? value, int price = 9000, /*Color color = color_767676, */ FontWeight fontWeight = FontWeight.normal, TextAlign? textAlign, int maxLine = 1, bool isSelect = false, Function? onClick}) {
  return Padding(
    padding: padding_05_B,
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          flex: 2,
          child: Row(
            children: [
              lText(
                title,
                style: st_b_18(fontWeight: FontWeight.bold),
              ),
              sb_w_05,
              lInkWell(
                  onTap: () {
                    onClick!(DialogAction.info);
                  },
                  child: Lcons.info(color: color_main, isEnabled: true)),
            ],
          ),
        ),
        sb_w_10,
        if (StringUtils.validateString(value))
          Expanded(
              flex: 3,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // InkWell(onTap: onClick, child: iconInfo()),
                  lInkWell(
                    onTap: () {
                      onClick!(DialogAction.toggle);
                    },
                    child: Row(
                      children: [
                        Lcons.check_circle(isEnabled: isSelect, color: Commons.getColor(), disableColor: color_eeeeee),
                        sb_w_05,
                        lAutoSizeText(
                          value!,
                          style: st_b_16(fontWeight: fontWeight),
                          maxLines: maxLine,
                          textAlign: textAlign,
                        ),
                      ],
                    ),
                  ),
                  sb_w_10,
                  Expanded(
                    child: lInkWell(
                      onTap: isSelect
                          ? () {
                              onClick!(DialogAction.select);
                            }
                          : null,
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          lRoundContainer(
                            bgColor: isSelect ? color_main : color_f8f8fa,
                            padding: padding_10,
                            alignment: Alignment.center,
                            child: Padding(
                              padding: padding_15_R,
                              child: lAutoSizeText(
                                '${StringUtils.formatPay(price)}${"원".tr()}',
                                style: st_15(textColor: isSelect ? color_white : color_aaaaaa),
                                textAlign: TextAlign.center,
                                maxLines: 1,
                              ),
                            ),
                          ),
                          Container(
                            padding: padding_05_R,
                            alignment: Alignment.centerRight,
                            // child: iconInfo(),
                            child: Lcons.nav_bottom(isEnabled: isSelect, color: Colors.white, disableColor: color_aaaaaa)
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )),
      ],
    ),
  );
}
