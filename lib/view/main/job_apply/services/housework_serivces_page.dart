import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/listview/list_service_select.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';

class HouseWorkServicesPage extends BaseStateful {
  @override
  _HouseWorkServicesPagePageState createState() => _HouseWorkServicesPagePageState();

  HouseWorkServicesPage({this.data, this.viewMode});

  final DataManager? data;
  final ViewMode? viewMode;
}

class _HouseWorkServicesPagePageState extends BaseStatefulState<HouseWorkServicesPage> with AutomaticKeepAliveClientMixin<HouseWorkServicesPage> {
  @override
  initState() {
    super.initState();
    log.d('『GGUMBI』>>> onData : data.serviceItem.infos[servicesItem.cares.type_name]: ${widget.data},  <<< ');
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.isShow = false;
    data.jobItem.request = _data.jobItem.request;
    data.jobItem.reqdata = _data.jobItem.reqdata;
    data.serviceItem.infos = _data.serviceItem.infos;
    data.serviceItem.lsBoyuk = _data.serviceItem.lsBoyuk;
    data.serviceItem.lsHomeCare = _data.serviceItem.lsHomeCare;
    data.serviceItem.lsPlay = _data.serviceItem.lsPlay;
    data.jobItem.tempResponse = _data.jobItem.tempResponse;
    data.jobItem.totalTime = _data.jobItem.totalTime;
    data.jobItem.calcuTime = _data.jobItem.calcuTime;
    onDataPage(_data);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: data.serviceItem.lsHomeCare.isEmpty
                ? Center(
                    child: Padding(
                      padding: padding_30,
                      child: lText(getInfoText(), style: st_b_16(), textAlign: TextAlign.center),
                    ),
                  )
                : lScrollView(
                    child: Container(
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          _listView(),
                          if (data.serviceItem.lsHomeCare.isNotEmpty) _commentView(),
                        ],
                      ),
                    ),
                  ),
          ),
        ],
      ),
    );
  }

  @override
  void onConfirmBtn() {
    flag.enableConfirm(fn: () => onUpDate());
  }

  @override
  void onDataPage(_data) {
    var item = _data as DataManager;
    log.d('『GGUMBI』>>> onDataPage : item: $item,  <<< ');

    if (_data == null) {
      return;
    }

    onUpDate();
  }

  ///청소, 주방, 빨래
  Widget _listView() {
    return ListServiceSelector(
      data: data.serviceItem.lsHomeCare,
      dataManager: data,
      viewMode: getViewMode(),
      onItemClick: (action, value) {
        log.d('『GGUMBI』>>> build : data: $action,  $value <<< ');
        onConfirmBtn();
      },
    );
  }

  ///청소, 주방, 빨래 추가설명
  Widget _commentView() {
    return lRoundContainer(
      margin: padding_10_TB,
      alignment: Alignment.center,
      bgColor: color_80dbdbdb,
      child: lText('${"필요한돌봄_추가내용".tr()}\n${"ex)쓰레기버리기등".tr()}', textAlign: TextAlign.center, style: st_b_14()),
    );
  }

  String getInfoText() {
    String value = "서비스제공안내문구".tr();
    //등원이고, 이웃집 일 경우
    if (data.jobItem.request.servicetype == ServiceType.serviceType_0.index && data.jobItem.request.possible_area == PossibleArea.link_mom.value) {
      value = '${"등원돌봄가사안내".tr()}';
    } else {
      value = '${"가사서비스".tr()} $value';
    }
    return value;
  }

  ///탭 유지 코드
  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
