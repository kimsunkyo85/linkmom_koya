import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/main/mypage/child_info/child_drug_list_page.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../../../lcons.dart';

class DrugServicesPage extends BaseStateful {
  @override
  _DrugServicesPageState createState() => _DrugServicesPageState();

  DrugServicesPage({this.data, this.viewMode, this.matchingStatus = 0, this.childInfo});

  final DataManager? data;
  final ViewMode? viewMode;
  final int? matchingStatus;
  final ChildInfoItem? childInfo;
}

class _DrugServicesPageState extends BaseStatefulState<DrugServicesPage> with AutomaticKeepAliveClientMixin<DrugServicesPage> {
  bool isPay = false;

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());

    isPay = widget.matchingStatus! >= MatchingStatus.paid.value;
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.isShow = false;
    data.jobItem.request = _data.jobItem.request;
    data.jobItem.reqdata = _data.jobItem.reqdata;
    data.serviceItem.infos = _data.serviceItem.infos;
    data.serviceItem.lsBoyuk = _data.serviceItem.lsBoyuk;
    data.serviceItem.lsHomeCare = _data.serviceItem.lsHomeCare;
    data.serviceItem.lsPlay = _data.serviceItem.lsPlay;
    data.jobItem.tempResponse = _data.jobItem.tempResponse;
    data.jobItem.totalTime = _data.jobItem.totalTime;
    data.jobItem.calcuTime = _data.jobItem.calcuTime;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: lScrollView(
              child: Column(
                children: [Commons.isLinkMom() ? _infoLinkmom() : _infoMomdady()],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void onConfirmBtn() {
    flag.enableConfirm(fn: () => onUpDate());
  }

  Widget _infoMomdady() {
    return Container(
      width: widthFull(context),
      child: Column(
        children: [
          Padding(
            padding: padding_15_TB,
            child: TextHighlight(
              text: isPay ? "투약의뢰_안내2".tr() : "투약의뢰_안내1".tr(),
              term: isPay ? "투약의뢰_안내2_힌트".tr() : "투약의뢰_안내1_힌트".tr(),
              textStyle: st_b_14(),
              textStyleHighlight: st_14(textColor: Commons.getColor()),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: padding_10_TB,
            child: Image.asset(
              IMG_DRUG_MOMDADY,
              width: data.width(context, 0.65),
            ),
          ),
          if (isPay && widget.childInfo != null)
            lBtn("투약의뢰서바로가기".tr(), margin: padding_20_B, onClickAction: () {
              Commons.page(context, routeChildDrugList, arguments: ChildDrugListPage(data: data, child: widget.childInfo));
            }),
        ],
      ),
    );
  }

  Widget _infoLinkmom() {
    return Container(
      width: widthFull(context),
      child: Column(
        children: [
          Padding(
            padding: padding_15_TB,
            child: TextHighlight(
              text: isPay ? "투약의뢰_안내4".tr() : "투약의뢰_안내3".tr(),
              term: isPay ? "투약의뢰_안내4_힌트".tr() : "투약의뢰_안내3_힌트".tr(),
              textStyle: st_b_14(),
              textStyleHighlight: st_14(textColor: Commons.getColor()),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: padding_10_T,
            child: Image.asset(IMG_DRUG_LINKMOM),
          ),
          lTextIcon(
            align: MainAxisAlignment.center,
            leftIcon: Lcons.info_t(disableColor: Commons.getColor()),
            paddingLeft: padding_03_R,
            title: "투약시주의사항".tr(),
            textStyle: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.bold),
          ),
          Padding(
            padding: padding_10_T,
            child: lText("투약시주의사항_안내".tr(), textAlign: TextAlign.center),
          ),
          if (isPay)
            lBtn("돌봄일기바로가기".tr(), margin: padding_20_TB, onClickAction: () {
              Commons.page(context, routeLinkmomDiary);
            }),
        ],
      ),
    );
  }

  ///탭 유지 코드
  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
