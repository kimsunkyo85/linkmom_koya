import 'dart:convert';
import 'dart:math';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/data/job_myinfo_data.dart';
import 'package:linkmom/data/network/models/linkmom_myinfo_response.dart';
import 'package:linkmom/data/network/models/linkmom_save_request.dart';
import 'package:linkmom/data/network/models/linkmom_view_request.dart';
import 'package:linkmom/data/network/models/linkmom_view_response.dart';
import 'package:linkmom/data/network/models/linkmom_wish_list_requests.dart';
import 'package:linkmom/data/network/models/momdady_mycares_list_response.dart';
import 'package:linkmom/data/network/models/mypage_myinfo_response.dart';
import 'package:linkmom/data/network/models/req_job_data.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_area_view.dart';
import 'package:linkmom/utils/listview/list_cate_type_view.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/auth_center/auth_center_page.dart';
import 'package:linkmom/view/main/chat/chat_care_info_page.dart';
import 'package:linkmom/view/main/chat/chat_room_page.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_1_page.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_2_page.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_3_page.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_4_page.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_5_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/kbinsure_view.dart';
import 'package:linkmom/view/main/job_apply/job_profile/linkmom_my_info_view.dart';
import 'package:linkmom/view/main/job_apply/job_profile/linkmom_schedule_page.dart';
import 'package:linkmom/view/main/job_apply/title_value_view.dart';
import 'package:linkmom/view/main/job_list/location_view.dart';
import 'package:linkmom/view/main/mypage/myinfo_view.dart';
import 'package:linkmom/view/main/mypage/review/linkmom_review_page.dart';
import 'package:linkmom/view/main/mypage/review/review_view_page.dart';

import '../../../../base/base_stateful.dart';
import '../../../../data/storage/flag_manage.dart';
import '../../../../main.dart';
import '../../../../route_name.dart';
import '../../../../utils/modal_bottom_sheet/push_modal_bottom.dart';
import '../content_view.dart';
import 'chip_view.dart';
import 'listview_child_info_view.dart';

///- All Rights Reserved. Copyright(c) 2022 GGUMBI CO., Ltd
///- Created by   : platformbiz@ggumbi.com
///- version      : 1.0.0
///- see          : linkmom_profile_page.dart - 링크쌤지원서 & 프로필
///- since        : 2022/02/08 / update:
///- [함수,메서드시]
class LinkMomProfilePage extends BaseStateful {
  @override
  _LinkMomProfilePageState createState() => _LinkMomProfilePageState();

  LinkMomProfilePage({
    this.data,
    this.viewMode,
    this.jobId,
    this.first_name,
    this.btnType = BottomBtnType.normal,
  });

  final DataManager? data;
  final ViewMode? viewMode;
  final int? jobId;
  final String? first_name;
  final BottomBtnType btnType;
}

class _LinkMomProfilePageState extends BaseStatefulState<LinkMomProfilePage> {
  int flex = 3;
  int flex2 = 7;

  ///주소 최대 라인
  int maxLineAddress = 3;

  late LinkMomMyInfoResponse responseData;
  JobMyInfoData? _myInfoData;

  late int jobId;
  late TextStyle _modityStyle;
  late BottomBtnType btnType;
  bool isMine = false;

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
    btnType = widget.btnType;
    if (data.jobItem.requestJob.possible_area == null) {
      data.jobItem.requestJob = ReqJobData();
    }
    if (getViewType() == ViewType.view) {
      if (widget.jobId != null) {
        jobId = widget.jobId!;
        _requestJobProfile(jobId);
      }
      //내 프로필 화면 볼때(수정)
    } else {
      _requestJobMyInfo();
    }
    _modityStyle = st_15(textColor: Commons.getColor());
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);

    if (getViewType() == ViewType.view) {
      if (widget.jobId != null) {
        jobId = widget.jobId!;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: jobAppBar(
          "링크쌤지원서".tr(),
          isBack: true,
          isClose: false,
          onClick: () {
            Commons.showCloseDlg();
          },
        ),
        body: _myInfoData == null
            ? flag.isLoading
                ? Center()
                : Center(child: lEmptyView())
            : lContainer(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          Expanded(
                            child: lScrollView(
                              padding: padding_20_B,
                              child: Container(
                                child: Column(
                                  children: <Widget>[
                                    _myInfoView(),
                                    lDivider20(),
                                    //돌봄유형
                                    Padding(
                                      padding: padding_20_LR,
                                      child: _careTypeView(),
                                    ),
                                    lDivider20(),
                                    //보육장소
                                    Padding(
                                      padding: padding_20_LR,
                                      child: _areaView(),
                                    ),
                                    lDivider20(),
                                    //이동방법
                                    Padding(
                                      padding: padding_20_LR,
                                      child: _moveView(),
                                    ),
                                    lDivider20(),
                                    //날짜,시간
                                    Padding(
                                      padding: padding_20_LR,
                                      child: _dateView(),
                                    ),
                                    lDivider20(),
                                    //희망근무조건
                                    Padding(
                                      padding: padding_20_LR,
                                      child: _locationView(),
                                    ),
                                    lDivider20(),
                                    //링크쌤인증
                                    Padding(
                                      padding: padding_20_LR,
                                      child: _authView(),
                                    ),
                                    // _groupView(),
                                    kbInsureView(context),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          _confirmButton(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    //링크쌤찾기에서 내 지원서 이면 아무런 버튼을 처리하지 않도록 한다.
    if (isMine || btnType == BottomBtnType.not) {
      return Center();
    }

    bool isView = getViewType() == ViewType.view;
    int reciver = _myInfoData!.userinfo!.user_id;
    return Row(
      children: [
        Expanded(
          child: lBtn(
            getViewType() == ViewType.view ? "채팅하기".tr() : "저장".tr(),
            // isView ? "채팅하기".tr() : "저장".tr(),
            margin: EdgeInsets.fromLTRB(20, 20, isView ? 10 : 20, 20),
            style: st_16(textColor: isView ? Commons.getColor() : color_white, fontWeight: FontWeight.bold),
            sideColor: isView ? Commons.getColor() : color_transparent,
            btnColor: isView ? color_white : Commons.getColor(),
            onClickAction: () async {
              if (getViewType() == ViewType.view) {
                // showDlg(msg: "채팅하기...연동하기");
                if (mqttManager.getCurrentRouteName == routeChatRoom) {
                  Commons.pageToMain(
                    context,
                    routeChatRoom,
                    arguments: ChatRoomPage(roomType: ChatRoomType.CREATE, receiver: reciver),
                  );
                } else {
                  Commons.page(
                    context,
                    routeChatRoom,
                    arguments: ChatRoomPage(roomType: ChatRoomType.CREATE, receiver: reciver),
                  );
                }
              } else {
                await apiHelper.requestLinkMomSave(LinkMomSaveRequest(reqdata: jsonEncode(data.jobItem.requestJob))).then((response) {
                  flag.disableLoading(fn: () => onUpDate());
                  if (response.getCode() == KEY_SUCCESS) {
                    if (Flags.getFlag(Flags.KEY_PUSH_LINKMOM).isShown) {
                      Commons.pagePop(context, data: data.jobItem.requestJob);
                    } else {
                      Flags.addFlag(Flags.KEY_PUSH_LINKMOM, isShown: true);
                      showPushBottomSheet(context, onClick: (action) {
                        if (DialogAction.confirm == action) {
                          Commons.pagePop(context, data: data.jobItem.requestJob);
                        }
                      });
                    }
                  }
                }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
              }
            },
          ),
        ),
        if (isView)
          Expanded(
            child: lBtn(
              "돌봄신청서_공유".tr(),
              margin: EdgeInsets.fromLTRB(10, 20, 20, 20),
              btnColor: Commons.getColor(),
              onClickAction: () async {
                var result = await Commons.page(context, routeChatCareInfo, arguments: ChatCareInfoPage(receiver: reciver)) as MyCareResultsData;
              },
            ),
          ),
      ],
    );
  }

  @override
  void onConfirmBtn() {
    flag.enableConfirm(fn: () => onUpDate());
  }

  ///내 프로필 조회
  Future<LinkMomMyInfoResponse> _requestJobMyInfo() async {
    flag.enableLoading(fn: () => onUpDate());
    await apiHelper.requestJobMyInfo().then((response) {
      flag.disableLoading(fn: () => onUpDate());
      responseData = response;
      if (response.getCode() == KEY_SUCCESS) {
        _myInfoData = responseData.getData();
        setDataMapping(_myInfoData!);
      }
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    return responseData;
  }

  ///링크쌤 리스트에서 상세로 들어 올 경우
  Future<LinkMomViewResponse> _requestJobProfile(int jobId) async {
    flag.enableLoading(fn: () => onUpDate());
    late LinkMomViewResponse _response;
    await apiHelper.requestLinkMomView(LinkMomViewRequest(job_id: jobId)).then((response) {
      flag.disableLoading(fn: () => onUpDate());
      if (response.getCode() == KEY_SUCCESS && response.isData()) {
        _response = response;
        _myInfoData = response.getData();
        //내 지원서 인지 여부 판단후, 스크랩 및 하단 채팅(저장)하기 버튼 처리하기
        isMine = auth.user.id == _myInfoData!.userinfo!.user_id;
        setDataMapping(_myInfoData!);
      }
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    return _response;
  }

  @override
  void onDataPage(_data) {
    if (_data is ReqJobData) {
      var _item = _data;

      if (_data == null) {
        onConfirmBtn();
        return;
      }

      //돌봄유형 데이터 만들기
      data.jobItem.lsJobCareData.clear();
      data.jobItem.lsJobCareData.addAll(data.jobItem.lsJobType.map((item) => SingleItem.clone(item)).toList());
      _item.servicetype!.forEach((value) {
        data.jobItem.lsJobCareData.forEach((item) {
          if (value == item.type) {
            item.isSelected = true;
          }
        });
      });

      //보육장소 데이터 만들기
      data.jobItem.makeData(viewType: getViewType());
      data.jobItem.lsJobAreaData.clear();
      data.jobItem.lsJobAreaData.addAll(data.jobItem.lsJobArea.map((item) => SingleItem.clone(item)).toList());
      _item.possible_area!.forEach((value) {
        data.jobItem.lsJobAreaData.forEach((item) {
          if (value == item.type) {
            item.isSelected = true;
            if (data.jobItem.requestJob.is_groupboyuk == 1 && item.type == PossibleArea.mom_daddy.value) {
              item.values![0].isSelected = true;
            } else if (data.jobItem.requestJob.is_dongsiboyuk == 1 && item.type == PossibleArea.link_mom.value) {
              item.values![0].isSelected = true;
            }
          }
        });
      });

      //희망급여 데이터 만들기
      data.jobItem.lsJobPayData.clear();
      data.jobItem.lsJobPayData.add(SingleItem("등원".tr(), content: data.jobItem.requestJob.fee_gotoschool.toString(), icon: Lcons.go_school(), type: ServiceType.serviceType_0.index));
      data.jobItem.lsJobPayData.add(SingleItem("하원".tr(), content: data.jobItem.requestJob.fee_afterschool.toString(), icon: Lcons.go_home(), type: ServiceType.serviceType_1.index));
      data.jobItem.lsJobPayData.add(SingleItem("보육".tr(), content: data.jobItem.requestJob.fee_boyuk.toString(), icon: Lcons.care(), type: ServiceType.serviceType_2.index));

      //돌봄대상 데이터 만들기
      data.jobItem.lsJobCareTargetData.clear();
      _item.care_ages!.asMap().forEach((index, value) {
        data.jobItem.lsJobCareTargetType.forEach((item) {
          if (value == item.type) {
            if (index == _item.care_ages!.length - 1) {
              item.isSelected = true;
            }
            data.jobItem.lsJobCareTargetData.add(item);
          }
        });
      });

      data.jobItem.requestJob.introduce = _item.introduce;
      data.jobItem.requestJob.care_introduce = _item.care_introduce;

      //링크쌤이 내 프로필이면 수정하기
      //어떻게 수정할지 SB확인후 처리 2021/10/05
      /*if (_myInfoData!.userinfo!.user_id == auth.user.id) {
        setViewType(ViewType.modify);
      }*/
    } else {}
    onConfirmBtn();
  }

  ///나의정보
  Widget _myInfoView() {
    return linkMomMyInfoView(
      context,
      data: _myInfoData,
      pay: getPayMinMax(),
      viewType: getViewType(),
      callBack: (action) {
        switch (action) {
          case DialogAction.follower:
            if (!_myInfoData!.userinfo!.is_follower) {
              apiHelper.requestLinkmomWishSave(LinkmomWishSaveRequest(bookingjobs: widget.jobId ?? 0)).then((response) {
                if (response.getCode() == KEY_SUCCESS) {
                  _requestJobProfile(widget.jobId!);
                }
                onUpDate();
              });
            } else {
              apiHelper.requestLinkmomWishDelete(LinkmomWishDeleteRequest(bookingjobs: widget.jobId ?? 0)).then((response) {
                if (response.getCode() == KEY_SUCCESS) {
                  _requestJobProfile(widget.jobId!);
                }
                onUpDate();
              });
            }
            break;
          case DialogAction.mode:
            _myInfoData!.status = _myInfoData!.getStatus().status ? BookingLinkMomStatus.end.value : BookingLinkMomStatus.inprogress.value;
            data.jobItem.requestJob.status = _myInfoData!.status;
            onUpDate();
            break;
        }
      },
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          sb_h_10,
          Row(
            children: [
              lText(getFirstName(), style: st_b_22(fontWeight: FontWeight.bold)),
              sb_w_05,
              locationAffiliateView(
                sizeLocation: 22,
                address: _myInfoData!.userinfo!.getAddress(),
                style: st_b_16(fontWeight: FontWeight.w500),
                isAffiliate: _myInfoData!.userinfo!.is_affiliate,
                sizeAffiliate: 17,
              ),
            ],
          ),
          sb_h_10,
          InkWell(
            onTap: () {
              showRankInfo();
            },
            child: lText(
              _myInfoData!.userinfo!.grade_linkmom_txt,
              style: st_15(textColor: Commons.getColorViewType(getViewType()), fontWeight: FontWeight.w500),
            ),
          ),
          sb_h_10,
          listViewChildInfoView(
            name: _myInfoData!.userinfo!.age,
            age: _myInfoData!.userinfo!.gender,
            fontWeight: FontWeight.w500,
            style: st_15(),
          ),
          sb_h_10,
          // TODO: GGUMBI 5/27/21 - 나의 정보는 프로필 화면에서 수정을 못하도록 한다. 기획/서버 합의
          listViewChildInfoView(
            name: _myInfoData!.userinfo!.numberofchild,
            age: _myInfoData!.userinfo!.job,
            fontWeight: FontWeight.w500,
            style: st_15(textColor: color_545454),
          ),
          lDivider20(color: color_d2d2d2, thickness: 1.0, padding: padding_20_TB),
          Row(
            children: [
              likeReview(
                  data: MyInfoLikeReViewItem(favoite_cnt_linkmom: _myInfoData!.userinfo!.reviewinfo!.favoite_cnt_linkmom, review_cnt_linkmom: _myInfoData!.userinfo!.reviewinfo!.review_cnt_linkmom),
                  viewType: getViewType(),
                  callBack: (action) {
                    if (action != null && action == DialogAction.review) {
                      if (_myInfoData!.userinfo!.user_id == 0) {
                        Commons.page(context, routeMypageLinkmomReview, arguments: LinkmomReviewPage());
                      } else {
                        Commons.page(context, routeReviewView, arguments: ReviewViewPage(userId: _myInfoData!.userinfo!.user_id, gubun: USER_TYPE.link_mom.index + 1));
                      }
                    }
                  },
                  color: Commons.getColor()),
            ],
          ),
        ],
      ),
    );
  }

  ///돌봄유형
  Widget _careTypeView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        contentView(
          data: ContentData(content: "돌봄유형".tr(), isModify: isModifyMode(), modifyStyle: _modityStyle),
          onClick: () async {
            var _data = await Commons.page(context, routeCare1,
                arguments: CareStep1Page(
                  data: data,
                  viewMode: ViewMode(viewType: ViewType.modify),
                ));

            if (_data != null) {
              data.jobItem.requestJob = _data;
              onDataPage(data.jobItem.requestJob);
            }
          },
        ),
        sb_h_10,
        Padding(
          padding: padding_30_LR,
          child: ListCareTypeView(
            data: data.jobItem.lsJobCareData,
            viewMode: getViewMode(),
            childAspectRatio: 1.0,
            count: 3,
            imageHeight: 45,
            imageWidth: 45,
          ),
        ),
      ],
    );
  }

  ///보육장소
  Widget _areaView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        contentView(
          data: ContentData(content: "보육장소".tr(), isModify: isModifyMode(), modifyStyle: _modityStyle),
          onClick: () async {
            var _data = await Commons.page(context, routeCare2,
                arguments: CareStep2Page(
                  data: data,
                  viewMode: ViewMode(viewType: ViewType.modify),
                ));

            if (_data != null) {
              data.jobItem.requestJob = _data;
              onDataPage(data.jobItem.requestJob);
            }
          },
        ),
        ListAreaView(
          data: data.jobItem.lsJobAreaData,
          viewMode: getViewMode(),
          count: 2,
          imageHeight: 45,
          imageWidth: 45,
        ),
      ],
    );
  }

  ///이동방법
  Widget _moveView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        contentView(
            data: ContentData(
              content: "이동방법".tr(),
              isModify: isModifyMode(),
              modifyStyle: _modityStyle,
              rightWidget: Row(
                children: [
                  sb_w_20,
                  lText(getMoveType(), style: st_b_16()),
                ],
              ),
            ),
            onClick: () async {
              data.jobItem.jobData = Commons.getJobTransportsData();
              var _data = await Commons.page(context, routeCare3,
                  arguments: CareStep3Page(
                    data: data,
                    viewMode: ViewMode(viewType: ViewType.modify),
                  ));

              if (_data != null) {
                data.jobItem.requestJob = _data;
                onDataPage(data.jobItem.requestJob);
              }
            }),
      ],
    );
  }

  ///날짜/시간
  Widget _dateView() {
    return Container(
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              contentView(
                  data: ContentData(content: "날짜시간".tr(), isModify: isModifyMode(), modifyStyle: _modityStyle),
                  onClick: () async {
                    var _data = await Commons.page(context, routeCare4,
                        arguments: CareStep4Page(
                          data: data,
                          viewMode: ViewMode(viewType: ViewType.modify),
                        ));
                    if (_data != null) {
                      data.jobItem.requestJob = _data;
                      _myInfoData!.caredate_min = data.jobItem.requestJob.caredate_min;
                      _myInfoData!.caredate_max = data.jobItem.requestJob.caredate_max;
                      _myInfoData!.careschedule = data.jobItem.requestJob.careschedule;
                      onDataPage(data.jobItem.requestJob);
                    }
                  }),
              sb_h_15,
              Column(
                  children: _myInfoData!.careschedule!
                      .map(
                        (e) => Padding(
                          padding: padding_10_B,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Lcons.calendar(size: 25),
                                  Padding(
                                    padding: padding_05_L,
                                    child: lText(
                                      StringUtils.getStringToDateYY(e.bookingdate!.first, e.bookingdate!.last),
                                      style: st_b_16(fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: padding_05_L,
                                      child: lText(
                                        '${"스케줄".tr()} ${e.bookingdate!.length}${"일".tr()}',
                                        style: st_b_16(fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                  children: e.times!
                                      .map(
                                        (times) => Padding(
                                          padding: padding_30_L,
                                          child: Row(
                                            children: [
                                              lText(
                                                '${times.stime} - ${times.etime}',
                                                style: st_b_16(fontWeight: FontWeight.w500),
                                              ),
                                              lText(
                                                ' (${StringUtils.careTotalTime(times.stime, times.etime)})',
                                                style: st_b_16(textColor: color_545454),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                      .toList()),
                            ],
                          ),
                        ),
                      )
                      .toList()),
              sb_h_15,
              lBtn(
                "근무날짜시간더보기".tr(),
                borderRadius: 8,
                btnColor: color_eeeeee,
                style: st_b_16(fontWeight: FontWeight.w500),
                padding: padding_0,
                margin: padding_0,
                onClickAction: () {
                  Commons.page(context, routeLinkmomSchedule, arguments: LinkmomSchedulePage(data: data.jobItem.requestJob.careschedule!));
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  ///희망지역
  Widget _locationView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        contentView(
            data: ContentData(content: "희망근무조건".tr(), isModify: isModifyMode(), modifyStyle: _modityStyle),
            onClick: () async {
              var _data = await Commons.page(context, routeCare5,
                  arguments: CareStep5Page(
                    data: data,
                    viewMode: ViewMode(viewType: ViewType.modify),
                  ));
              if (_data != null) {
                data.jobItem.requestJob = _data;
                _myInfoData!.fee_gotoschool = data.jobItem.requestJob.fee_gotoschool;
                _myInfoData!.fee_afterschool = data.jobItem.requestJob.fee_afterschool;
                _myInfoData!.fee_boyuk = data.jobItem.requestJob.fee_boyuk;
                onDataPage(data.jobItem.requestJob);
              }
            }),
        Column(
          children: [
            sb_h_15,
            titleValueView(
              "희망지역".tr(),
              maxLine: 2,
              rightWidget: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Row(
                    children: [
                      lText("1순위".tr(), style: st_b_16(fontWeight: FontWeight.bold)),
                      sb_w_05,
                      Expanded(
                          child: lAutoSizeText(
                        data.jobItem.requestJob.job_area_address_1,
                        maxLines: maxLineAddress,
                        style: st_b_16(),
                      )),
                    ],
                  ),
                  sb_h_05,
                  Row(
                    children: [
                      lText("2순위".tr(), style: st_b_16(fontWeight: FontWeight.bold)),
                      sb_w_05,
                      Expanded(
                          child: lAutoSizeText(
                        data.jobItem.requestJob.job_area_address_2,
                        maxLines: maxLineAddress,
                        style: st_b_16(),
                      )),
                    ],
                  )
                ],
              ),
              flex: flex,
              flex2: 8,
            ),
          ],
        ),
        Column(
          children: [
            titleValueView(
              "희망시급".tr(),
              flex: flex,
              flex2: 8,
              maxLineRight: maxLineAddress,
              padding: padding_0,
              rightWidget: Column(
                  children: data.jobItem.lsJobPayData
                      .map(
                        (e) => Padding(
                          padding: padding_05_B,
                          child: Row(
                            children: [
                              lText(
                                '${e.name} ${"돌봄".tr()}',
                                style: st_b_16(fontWeight: FontWeight.w500),
                              ),
                              sb_w_10,
                              lText(
                                '${"시급".tr()} ${StringUtils.formatPay(int.parse(e.content))}${"원".tr()}',
                                style: st_b_16(fontWeight: FontWeight.bold, textColor: Commons.getColor()),
                              ),
                            ],
                          ),
                        ),
                      )
                      .toList()),
            ),
          ],
        ),
        lDivider20(color: color_d2d2d2, padding: padding_15_TB, thickness: 1.0),
        Column(
          children: [
            titleValueView(
              "돌봄대상".tr(),
              flex: flex,
              flex2: 8,
              maxLineRight: maxLineAddress,
              padding: padding_0,
              rightWidget: Column(
                  children: data.jobItem.lsJobCareTargetData
                      .map(
                        (e) => Padding(
                          padding: e.isSelected ? padding_0 : padding_05_B,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              lText(
                                e.name,
                                style: st_b_16(fontWeight: FontWeight.w500),
                              ),
                              sb_w_03,
                              lText(
                                e.content,
                                style: st_b_14(),
                              ),
                            ],
                          ),
                        ),
                      )
                      .toList()),
            ),
          ],
        ),
        lDivider20(color: color_d2d2d2, padding: padding_15_TB, thickness: 1.0),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            titleValueView("한줄자기소개".tr()),
            Padding(
              padding: padding_05_B,
              child: lText(StringUtils.validateString(data.jobItem.requestJob.introduce) ? data.jobItem.requestJob.introduce : '-'),
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            sb_h_15,
            titleValueView("돌봄방식".tr()),
            Padding(
              padding: padding_05_B,
              child: lText(StringUtils.validateString(data.jobItem.requestJob.care_introduce) ? data.jobItem.requestJob.care_introduce : '-'),
            ),
          ],
        ),
      ],
    );
  }

  ///링크쌤 인증
  Widget _authView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        contentView(
          data: ContentData(content: "링크쌤인증".tr()),
        ),
        Padding(
          padding: padding_15_TB,
          child: lRoundContainer(
            borderRadius: 8.0,
            bgColor: color_eeeeee.withOpacity(0.42),
            padding: padding_10,
            child: InkWell(
              onTap: () async {
                var _data = await Commons.page(context, routeAuthCenter,
                    arguments: AuthCenterPage(
                      data: data,
                      id: _myInfoData!.authinfo!.auth_id,
                      viewType: getViewMode().viewType,
                      name: getFirstName(),
                    ));
                if (_data != null) {
                  data.jobItem.requestJob = _data;
                  onDataPage(data.jobItem.requestJob);
                }
              },
              child: Row(
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        margin: padding_02_R,
                        decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [BoxShadow(blurRadius: 10, color: Colors.grey.withOpacity(0.2))]),
                        child: Lcons.polygonBg(Commons.isLinkMom(), size: 35),
                      ),
                      lText('${_myInfoData!.authinfo!.grade_authlevel}', style: st_14(fontWeight: FontWeight.w700, letterSpacing: 0)),
                    ],
                  ),
                  lText('${"인증배지".tr()} ${_myInfoData!.authinfo!.grade_authlevel}${"개".tr()}', style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Lcons.nav_right(color: color_b2b2b2, size: 20),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        if (data.myInfoItem.lsAuthInfo.isNotEmpty)
          Wrap(
            spacing: 8.0,
            runSpacing: -4.0,
            children: [
              ...authInfoView(),
            ],
          ),
      ],
    );
  }

  ///해시태그
  authInfoView() {
    return data.myInfoItem.lsAuthInfo.map(
      (tag) => hashTagView(
        tag,
        textColor: Commons.getColorServiceTypeText(isReverse: getViewType() == ViewType.modify),
      ),
    );
  }

  ///시급 최소, 최대
  String getPayMinMax() {
    String pay = '';
    var listPay = [_myInfoData!.fee_gotoschool, _myInfoData!.fee_afterschool, _myInfoData!.fee_boyuk];
    final minPay = listPay.reduce(min);
    final maxPay = listPay.reduce(max);
    pay = minPay == maxPay ? '${StringUtils.formatPay(maxPay)}${"원".tr()}' : '${StringUtils.formatPay(minPay)}${"원".tr()} ~ ${StringUtils.formatPay(maxPay)}${"원".tr()}';
    return pay;
  }

  ///이동수단
  String getMoveType() {
    StringBuffer st = StringBuffer();
    List<int> lsMove = [];
    lsMove = data.jobItem.requestJob.vehicle_id!;

    lsMove.forEach((value) {
      if (value == MoveType.waking.index) {
        //도보
        st.write('${"도보".tr()}/');
      } else if (value == MoveType.car.index) {
        //자차
        st.write('${"자동차".tr()}/');
      } else if (value == MoveType.transport.index) {
        //대중교통
        st.write('${"대중교통".tr()}/');
      }
    });

    String value = st.toString();
    if (value.length != 0 && value.contains("/")) {
      value = value.substring(0, value.toString().length - 1);
    }
    return value;
  }

  void setDataMapping(JobMyInfoData _myInfoData) {
    data.jobItem.requestJob.servicetype = _myInfoData.servicetype;
    data.jobItem.requestJob.possible_area = _myInfoData.possible_area;
    data.jobItem.requestJob.is_groupboyuk = StringUtils.getBoolToInt(_myInfoData.is_groupboyuk);
    data.jobItem.requestJob.is_dongsiboyuk = StringUtils.getBoolToInt(_myInfoData.is_dongsiboyuk);
    data.jobItem.requestJob.vehicle_id = _myInfoData.vehicle_id;
    data.jobItem.requestJob.job_areaid_1 = _myInfoData.job_areaid_1;
    data.jobItem.requestJob.job_addressid_1 = _myInfoData.job_addressid_1;
    data.jobItem.requestJob.job_area_address_1 = _myInfoData.job_area_address_1;
    data.jobItem.requestJob.job_areaid_2 = _myInfoData.job_areaid_2;
    data.jobItem.requestJob.job_addressid_2 = _myInfoData.job_addressid_2;
    data.jobItem.requestJob.job_area_address_2 = _myInfoData.job_area_address_2;
    data.jobItem.requestJob.fee_gotoschool = _myInfoData.fee_gotoschool;
    data.jobItem.requestJob.fee_afterschool = _myInfoData.fee_afterschool;
    data.jobItem.requestJob.fee_boyuk = _myInfoData.fee_boyuk;
    data.jobItem.requestJob.care_ages = _myInfoData.care_ages;
    data.jobItem.requestJob.introduce = _myInfoData.introduce;
    data.jobItem.requestJob.care_introduce = _myInfoData.care_introduce;
    data.jobItem.requestJob.caredate_min = _myInfoData.caredate_min;
    data.jobItem.requestJob.caredate_max = _myInfoData.caredate_max;
    data.jobItem.requestJob.status = _myInfoData.status;

    List<ReqJobScheduleData> careschedule = [];
    _myInfoData.careschedule!.forEach((value) {
      //2022/06/22 이전에 오류가 있던 스케쥴에서 발생한 경우 예외처리 추가 [{"times": [], "removedate": null, "bookingdate": []}, {"times": [{"id": 1, "etime": "20:00", "is_am": 1, "is_pm": 1, "stime": "08:00", "is_repeat": 0, "durationtime": 720, "caredate_until": "2022-06-20"}], "removedate": null, "bookingdate": ["2022-07-31", "2022-07-10", "2022-06-20"]}]
      if(value.bookingdate!.isNotEmpty){
        careschedule.add(ReqJobScheduleData(
          times: value.times,
          bookingdate: value.bookingdate,
          removedate: value.removedate,
        ));
      }
    });
    data.jobItem.requestJob.careschedule!.addAll(careschedule);

    //링크쌤 인증 리스트 만들기
    data.myInfoItem.lsAuthInfo.clear();
    data.myInfoItem.lsAuthInfo.addAll(Commons.getAuthList(_myInfoData.authinfo!));

    onDataPage(data.jobItem.requestJob);
  }

  String getFirstName() {
    return widget.first_name ?? _myInfoData!.userinfo!.first_name;
  }
}
