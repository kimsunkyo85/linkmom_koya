import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/req_job_data.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/main/job_apply/job_profile/schedule_view.dart';

class LinkmomSchedulePage extends BaseStateful {
  final List<ReqJobScheduleData> data;

  LinkmomSchedulePage({required this.data});

  @override
  _LinkmomSchedulePageState createState() => _LinkmomSchedulePageState();
}

class _LinkmomSchedulePageState extends BaseStatefulState<LinkmomSchedulePage> {
  Map<DateTime, List<int>> _dates = {};
  Map<ScheduleData, List<DateTimeRange>> _detail = {};

  @override
  void initState() {
    super.initState();
    if (widget.data.isNotEmpty) {
      widget.data.forEach((jobSchedule) {
        jobSchedule.bookingdate!.sort();
        jobSchedule.bookingdate!.forEach((el) {
          _dates.update(StringUtils.parseYMD(el), (value) {
            if (!value.contains(widget.data.indexOf(jobSchedule))) {
              value.add(widget.data.indexOf(jobSchedule));
            }
            return value;
          }, ifAbsent: () => [widget.data.indexOf(jobSchedule)]);
        });
        DateTimeRange range = DateTimeRange(start: StringUtils.parseYMD(jobSchedule.bookingdate!.first), end: StringUtils.parseYMD(jobSchedule.bookingdate!.last));
        ScheduleData scheduleData = ScheduleData(range, jobSchedule.bookingdate!.length);
        jobSchedule.times!.forEach((schedule) {
          if (schedule.is_repeat == 1) {
            jobSchedule.bookingdate!.forEach((element) {
              DateTime time = StringUtils.parseYMD(element);
              DateTime end = StringUtils.parseYMD(schedule.caredate_until);
              int size = (end.add(Duration(days: 1)).difference(StringUtils.parseYMD(element)).inDays / 7).ceil();
              for (var i = 0; i < size; i++) {
                _dates.update(time.add(Duration(days: i * 7)), (value) {
                  if (!value.contains(widget.data.indexOf(jobSchedule))) {
                    value.add(widget.data.indexOf(jobSchedule));
                  }
                  return value;
                }, ifAbsent: () => [widget.data.indexOf(jobSchedule)]);
              }
            });
          }
          _detail.update(scheduleData, (v) {
            v.add(DateTimeRange(start: StringUtils.parseHD(schedule.stime), end: StringUtils.parseHD(schedule.etime)));
            return v;
          }, ifAbsent: () {
            return [DateTimeRange(start: StringUtils.parseHD(schedule.stime), end: StringUtils.parseHD(schedule.etime))];
          });
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        appBar: appBar("근무날짜확인".tr(), hide: false),
        body: Column(
          children: [
            Expanded(
                child: ScheduleView(
              schedule: _dates,
              scheduleDetail: _detail,
            )),
            lBtn("확인".tr(), btnColor: Commons.getColor(), onClickAction: () => Commons.pagePop(context)),
          ],
        ));
  }
}
