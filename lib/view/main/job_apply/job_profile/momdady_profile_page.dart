import 'dart:convert';

import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:linkmom/data/network/models/care_update_request.dart';
import 'package:linkmom/data/network/models/care_view_request.dart';
import 'package:linkmom/data/network/models/care_view_response.dart';
import 'package:linkmom/data/network/models/cares_wish_requests.dart';
import 'package:linkmom/data/network/models/chat_room_init_request.dart';
import 'package:linkmom/data/network/models/child_info_response.dart';
import 'package:linkmom/data/network/models/data/chat_room_data.dart';
import 'package:linkmom/data/network/models/data/chat_send_data.dart';
import 'package:linkmom/data/network/models/data/matching_init_view_data.dart';
import 'package:linkmom/data/network/models/matching_init_request.dart';
import 'package:linkmom/data/network/models/matching_init_response.dart';
import 'package:linkmom/data/network/models/matching_view_request.dart';
import 'package:linkmom/data/network/models/matching_view_response.dart';
import 'package:linkmom/data/network/models/req_data.dart';
import 'package:linkmom/data/network/models/temp_area_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_dailog/dlg_group_view.dart';
import 'package:linkmom/utils/custom_view/loading_page.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/auth_center/auth_center_page.dart';
import 'package:linkmom/view/main/chat/chat_room_page.dart';
import 'package:linkmom/view/main/job_apply/parents/parents_step_4_page.dart';
import 'package:linkmom/view/main/job_apply/parents/parents_step_5_page.dart';
import 'package:linkmom/view/main/job_apply/parents/parents_step_6_page.dart';
import 'package:linkmom/view/main/job_apply/parents/parents_step_8_page.dart';
import 'package:linkmom/view/main/job_apply/temp_area/temp_area_view_page.dart';
import 'package:linkmom/view/main/job_list/location_view.dart';
import 'package:linkmom/view/main/mypage/child_info/child_info_add_page.dart';

import '../../../../base/base_stateful.dart';
import '../../../../data/network/models/addr_search_response.dart';
import '../../../../data/storage/flag_manage.dart';
import '../../../../main.dart';
import '../../../../route_name.dart';
import '../../../../utils/modal_bottom_sheet/push_modal_bottom.dart';
import '../content_view.dart';
import '../place_view.dart';
import '../title_value_view.dart';
import 'chip_view.dart';
import 'kbinsure_view.dart';
import 'listview_child_info_view.dart';
import 'matching_sign_page.dart';
import 'momdady_my_info_view.dart';
import 'momdady_schedule_page.dart';

///- All Rights Reserved. Copyright(c) 2021 GGUMBI CO., Ltd
///- Created by   : platformbiz@ggumbi.com
///- version      : 1.0.0
///- see          : momdady_profile_page.dart - 돌봄신청서 & 계약서 & 서명하기
///- since        : 2021/01/22 / update:
///- [함수,메서드시]
class MomDadyProfilePage extends BaseStateful {
  final DataManager? data;
  final ViewMode? viewMode;
  final int bookingId;
  final int chattingtype;
  final MatchingInitRequest? matchingData;
  final BottomBtnType btnType;
  final int receiver;

  MomDadyProfilePage({
    this.data,
    this.viewMode,
    this.bookingId = 0,
    this.chattingtype = 0,
    this.matchingData,
    this.btnType = BottomBtnType.normal,
    this.receiver = 0,
  });

  @override
  _MomDadyProfilePageState createState() => _MomDadyProfilePageState(
        bookingId: bookingId,
        chattingtype: chattingtype,
        matchingData: matchingData,
        receiver: receiver,
        btnType: btnType,
      );
}

class _MomDadyProfilePageState extends BaseStatefulState<MomDadyProfilePage> {
  final int? bookingId;

  ///일반채팅, 돌봄채팅
  final int? chattingtype;
  final MatchingInitRequest? matchingData;
  BottomBtnType btnType;
  int receiver;

  Map<String, dynamic>? beforeData;
  Map<String, dynamic>? afterData;

  Map<String, dynamic>? beforeChildData;
  Map<String, dynamic>? afterChildData;

  _MomDadyProfilePageState({
    required this.bookingId,
    required this.chattingtype,
    this.matchingData,
    this.btnType = BottomBtnType.normal,
    this.receiver = 0,
  });

  int flex = 3;
  int flex2 = 7;

  ///주소 최대 라인
  int maxLineAddress = 3;

  ///경유지 최대 라인
  int maxLinePath = 4;

  String title = "돌봄신청서".tr();
  bool isContract = false;
  bool isMatching = false;

  late SignType signType;

  bool isShowDlg = false;
  ScrollController? _controller;

  List<ReqScheduleData> careschedule = [];

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
    isContract = false;
    if (getItemType() == ItemType.contract) {
      title = "돌봄계약서".tr();
      isContract = true;

      ///어느 한쪽이 돌봄계약서가 완료 되면, View -> init 서명하기, 그게아니면 init, 둘다 서명인 경우 view
      if (matchingData!.matchingStatus == MatchingStatus.waitLinkmom.value || matchingData!.matchingStatus == MatchingStatus.waitMomdaddy.value) {
        //맘대디 또는 링크쌤 계약상태(매칭대기 : 91, 92)
        _requestMatchingView(MatchingViewRequest(
          receiver: matchingData!.receiver,
          bookingcareservices: matchingData!.bookingcareservices,
          carematching: matchingData!.carematching,
        ));

        // view
      } else if (matchingData!.matchingStatus == MatchingStatus.waitPaid.value || matchingData!.matchingStatus == MatchingStatus.paid.value) {
        //맘대디 또는 링크쌤 계약상태(결제대기 : 99, 결제완료 : 100)
        _requestMatchingView(MatchingViewRequest(
          receiver: matchingData!.receiver,
          bookingcareservices: matchingData!.bookingcareservices,
          carematching: matchingData!.carematching,
        ));
      } else {
        //매칭중 : 0
        _requestCareProfile(bookingId!);
      }
    } else {
      title = "돌봄신청서".tr();
      _requestCareProfile(bookingId!);
    }

    _controller = ScrollController();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    onConfirmBtn();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: jobAppBar(
          title,
          isBack: true,
          isClose: false,
          onClick: () {
            Commons.showCloseDlg();
          },
        ),
        body: data.listViewDetailItem.careViewData == null
            ? flag.isLoading
                ? Center()
                : Center(child: lEmptyView())
            : lContainer(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          Expanded(
                            child: lScrollView(
                              controller: _controller,
                              padding: padding_20_B,
                              child: Container(
                                child: Column(
                                  children: [
                                    _myInfoView(),
                                    lDivider20(),
                                    //돌봄유형,보육장소
                                    Padding(
                                      padding: padding_20_LR,
                                      child: _careTypeView(),
                                    ),
                                    lDivider20(),
                                    //돌봄시간
                                    Padding(
                                      padding: padding_20_LR,
                                      child: _dateView(),
                                    ),
                                    lDivider20(),
                                    //이동방법
                                    Padding(
                                      padding: padding_20_LR,
                                      child: _moveView(),
                                    ),
                                    lDivider20(),
                                    //필요한돌봄
                                    Padding(
                                      padding: padding_20_LR,
                                      child: _addCareView(),
                                    ),
                                    lDivider20(),
                                    //맘대디인증
                                    Padding(
                                      padding: padding_20_LR,
                                      child: _authView(),
                                    ),

                                    if (matchingData != null && Commons.isContract(matchingData!.matchingStatus)) //돌봄계약서이고, 계약완료이면 서명한 내용을 보여준다.
                                      Column(
                                        children: [
                                          lDivider20(),
                                          //맘대디인증
                                          Padding(
                                            padding: padding_20_LRB,
                                            child: _contractView(),
                                          ),
                                        ],
                                      ),

                                    kbInsureView(context),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          _confirmButton(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    log.d('『GGUMBI』>>> _confirmButton : 상대방 아이디: ${this.receiver}, 버튼타입: $btnType, 매칭상태: $matchingData, 매칭데이터: $matchingData, isContract: $isContract, isMatching: $isMatching, viewType: ${getViewType()}<<< ');
    //1. 표시안하는경우를 먼저 체크 한다.
    //버튼 타입이 not
    if (btnType == BottomBtnType.not) {
      return Center();
    }

    bool isView = getViewType() == ViewType.view;
    // isView = isContract ? false : isView;
    //돌봄계약이고, 매칭 상태가 매칭성공 99이면, 맘대디이고 먼저 서명한 경우, 링크쌤이고 먼저 서명한 경우 보여주지 않는다.
    if (btnType == BottomBtnType.contract && matchingData!.matchingStatus >= MatchingStatus.waitPaid.value ||
        matchingData != null && !Commons.isLinkMom() && matchingData!.matchingStatus == MatchingStatus.waitMomdaddy.value ||
        matchingData != null && Commons.isLinkMom() && matchingData!.matchingStatus == MatchingStatus.waitLinkmom.value) {
      return Center();
    }
    //일반채팅에서 공유해서 들어오고, receiver가 0이 아닌 경우
    int receiver = this.receiver > 0 ? this.receiver : getCareViewData().userinfo!.user_id;
    int bookingcareservices = getCareViewData().booking_id;

    //2. 상태에 따라 버튼 이벤트 및 기능을 설정한다.
    String title = "저장".tr(); //기본 돌봄신청서 작성에서 신청서 저장
    if (isView) {
      if (isContract) {
        title = "서명하기".tr();
      } else {
        title = "채팅하기".tr();
      }
    }
    bool isStyle = !isContract && !isMatching;
    if (getViewType() == ViewType.modify) {
      isStyle = false; //수정모드 일시 저장하기 버튼 스타일
    }

    return Row(
      children: [
        Expanded(
          child: lBtn(
            title,
            isEnabled: flag.isConfirm,
            margin: EdgeInsets.fromLTRB(20, 20, isStyle ? 10 : 20, 20),
            style: st_16(textColor: isStyle ? Commons.getColor() : color_white, fontWeight: FontWeight.bold),
            sideColor: isStyle ? Commons.getColor() : color_transparent,
            btnColor: isStyle ? color_white : Commons.getColor(),
            onClickAction: () async {
              //돌봄신청서, 계약서, 채팅 하기 등등 그외 저장하기
              if (isView) {
                if (isContract) {
                  //돌봄계약서
                  var img = await Commons.page(context, routeMatchingSign, arguments: MatchingSignPage(type: storageHelper.user_type, callback: () => {}));
                  if (img != null) {
                    MatchingInitRequest request = MatchingInitRequest(
                      receiver: matchingData!.receiver,
                      bookingcareservices: matchingData!.bookingcareservices,
                      chattingroom: matchingData!.chattingroom,
                      signature: img,
                      carematching: matchingData!.carematching,
                      linkmom_address: data.tcSign.text,
                      linkmom_address_detail: data.tcSignDetail.text,
                    );
                    if (matchingData!.matchingStatus > MatchingStatus.inprogress.value && matchingData!.matchingStatus < MatchingStatus.waitMomdaddy.value) {
                      _requestMatchingRetry(request);
                    } else {
                      _requestMatchingInit(request);
                    }
                    onUpDate();
                  }
                } else {
                  //채팅하기 -> 이전 화면이 채팅방이면 재호출, 그게 아니면 채팅화면으로 이동
                  if (mqttManager.getCurrentRouteName == routeChatRoom) {
                    Commons.pageReplace(context, routeLoading,
                        isPrevious: true,
                        arguments: LoadingPage(
                          routeName: routeChatRoom,
                          pageData: ChatRoomPage(
                            roomType: ChatRoomType.CREATE,
                            receiver: receiver,
                            bookingcareservices: bookingcareservices,
                          ),
                        ));
                  } else {
                    Commons.page(
                      context,
                      routeChatRoom,
                      arguments: ChatRoomPage(roomType: ChatRoomType.CREATE, receiver: receiver, bookingcareservices: bookingcareservices),
                    );
                  }
                }
              } else if (getViewType() == ViewType.modify) {
                if (isModify() && isModifyChild()) {
                  Commons.pagePop(context);
                } else {
                  flag.enableLoading(fn: () => onUpDate());
                  await apiHelper
                      .requestCaresUpdate(CareUpdateRequest(
                    child: getCareViewChildInfoData().child_id,
                    booking_id: getCareViewData().booking_id,
                    servicetype: getCareViewData().servicetype,
                    possible_area: getCareViewData().possible_area,
                    reqdata: jsonEncode(data.jobItem.reqdata),
                  ))
                      .then((response) {
                    flag.disableLoading(fn: () => onUpDate());
                    if (response.getCode() == KEY_SUCCESS) {
                      if (Flags.getFlag(Flags.KEY_PUSH_MOMDADY).isShown) {
                        Commons.pagePop(context, data: DialogAction.update);
                      } else {
                        Flags.addFlag(Flags.KEY_PUSH_MOMDADY, isShown: true);
                        showPushBottomSheet(context, onClick: (action) {
                          if (DialogAction.confirm == action) {
                            Commons.pagePop(context, data: DialogAction.update);
                          }
                        });
                      }
                    }
                  }).catchError((e) {
                    flag.disableLoading(fn: () => onUpDate());
                  });
                }
              } else {
                Commons.pagePop(context);
              }
            },
          ),
        ),
        if (isView && !isContract && !isMatching)
          Expanded(
            child: lBtn(
              "매칭요청하기".tr(),
              isWidth: false,
              margin: EdgeInsets.fromLTRB(10, 20, 20, 20),
              btnColor: Commons.getColor(),
              onClickAction: () async {
                await apiHelper.requestMatchingView(MatchingViewRequest(receiver: receiver, bookingcareservices: bookingcareservices)).then((response) async {
                  //돌봄관리 매칭 내용/상태 화면이동 (matching_view)
                  if (response.getCode() == KEY_SUCCESS) {
                    showNormalDlg(
                        message: "진행중인계약_안내".tr(),
                        btnLeft: "아니오".tr(),
                        btnRight: "예".tr(),
                        onClickAction: (action) {
                          if (action == DialogAction.yes) {
                            Commons.pageToMain(
                              context,
                              routeChatRoom,
                              arguments: ChatRoomPage(roomType: ChatRoomType.VIEW, receiver: receiver, bookingcareservices: bookingcareservices),
                            );
                          }
                        });
                    // TODO: GGUMBI 2022/02/07 - 추후 돌봄관리쪽 수정후 돌봄진행상황으로 보낸다. 우선, 돌봄채팅으로 보내기!
                    // Commons.page(context, routeLinkmomApply,
                    //     arguments: LinkmomApplyPage(
                    //       data: data,
                    //       id: response.getData().carematching!,
                    //       title: "매칭진행상황".tr(),
                    //       isMatchingView: true,
                    //     ));
                    //매칭요청/수락 (matching_init)으로 채팅 이동
                  } else if (response.getCode() == KEY_4000_NO_DATA) {
                    await apiHelper.requestChatRoomInit(ChatRoomInitRequest(receiver: receiver, bookingcareservices: bookingcareservices)).then((response) async {
                      if (response.getCode() == KEY_SUCCESS) {
                        ChatRoomData chatData = response.getData();
                        int chatroomId = chatData.chatroom_id;
                        ContractData? callBackData = await Commons.page(context, routeMomDadyProfile,
                            arguments: MomDadyProfilePage(
                              viewMode: ViewMode(viewType: ViewType.view, itemType: ItemType.contract),
                              bookingId: bookingcareservices,
                              chattingtype: chatData.chattingtype,
                              btnType: BottomBtnType.contract,
                              matchingData: MatchingInitRequest(
                                receiver: receiver,
                                bookingcareservices: bookingcareservices,
                                chattingroom: chatroomId,
                                matchingStatus: chatData.carematching!.matching_status,
                                carematching: chatData.carematching!.matching_id,
                              ),
                            ));
                        if (callBackData != null)
                          Commons.pageToMain(
                            context,
                            routeChatRoom,
                            arguments: ChatRoomPage(roomType: ChatRoomType.CREATE, receiver: receiver, bookingcareservices: bookingcareservices),
                          );
                      } else {
                        flag.disableLoading(fn: () => onUpDate());
                        Commons.showSnackBar(context, response.getMsg());
                      }
                    }).catchError((e) {
                      flag.disableLoading(fn: () => onUpDate());
                    });
                  }
                });
              },
            ),
          ),
      ],
    );
  }

  @override
  void onConfirmBtn() {
    ///링크쌤이 돌봄계약서 서명시 주소 미 엽릭시 체크를 한다.
    if (isSignMode(isContract) && isGoSchool() || isSignMode(isContract) && isGoHome() || isSignMode(isContract) && isNuresery()) {
      if (!StringUtils.validateString(data.tcSign.text) || !StringUtils.validateString(data.tcSignDetail.text)) {
        flag.disableConfirm(fn: () => onUpDate());
        return;
      }
    }
    flag.enableConfirm(fn: () => onUpDate());
  }

  ///수정후 데이터 세팅 후 체크
  onConfirmBtnModify() {
    if (getViewType() == ViewType.modify) {
      afterData = data.jobItem.reqdata.toJson();
      afterChildData = getCareViewChildInfoData().toJson();
      // TODO: GGUMBI 2022/02/10 - 추후 저장에 대한 활성화, 비활성화 체크
      // if (isModify() && isModifyChild()) {
      //   flag.disableConfirm(fn: () => onUpDate());
      //   return true;
      // }
      // flag.enableConfirm(fn: () => onUpDate());
      onUpDate();
    }
  }

  ///맘대디 상세요청
  Future<CareViewResponse> _requestCareProfile(int bookingId) async {
    flag.enableLoading(fn: () => onUpDate());
    CareViewResponse _response = await apiHelper.requestCareView(CareViewRequest(booking_id: bookingId)).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    flag.disableLoading(fn: () => onUpDate());
    if (_response.getCode() == KEY_SUCCESS && _response.isData()) {
      _response = _response;
      setCareViewData(_response.getData());
      onDataPage(getCareViewData());
      //수정모드이면
      if (isModifyMode()) {
        data.jobItem.reqdata = getCareViewData().reqdata!.clone();
        data.jobItem.calcuTime = data.jobItem.reqdata.careschedule!.first.durationtime;
        //요청값 응답후 최초 값 세팅!
        beforeData = data.jobItem.reqdata.clone().toJson();
        beforeChildData = getCareViewChildInfoData().toJson();
        setModifyAddress();
        onConfirmBtnModify();
      }
    }
    return _response;
  }

  ///돌봄계약서 생성 및 업데이트 매칭 요청하기 requestMatchingInit
  Future<MatchingInitResponse> _requestMatchingInit(MatchingInitRequest data) async {
    flag.enableLoading(fn: () => onUpDate());
    MatchingInitResponse _response = await apiHelper.requestMatchingInit(data).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    flag.disableLoading(fn: () => onUpDate());
    if (_response.getCode() == KEY_SUCCESS) {
      _response = _response;
      Commons.pagePop(context, data: _response.getData().msgdata);
    } else {
      Commons.showToast(_response.getMsg());
    }
    return _response;
  }

  ///돌봄계약서 생성 및 업데이트 재매칭 요청하기 requestMatchingRetry
  Future<MatchingInitResponse> _requestMatchingRetry(MatchingInitRequest data) async {
    flag.enableLoading(fn: () => onUpDate());
    MatchingInitResponse _response = await apiHelper.requestMatchingRetry(data).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    flag.disableLoading(fn: () => onUpDate());
    if (_response.getCode() == KEY_SUCCESS) {
      _response = _response;
      Commons.pagePop(context, data: _response.getData().msgdata);
      //2022/01/18 동시에 재매칭 요청시, 응답으로 5000 수신후, init 재요청으로 처리
    } else if (_response.getCode() == KEY_5000_CONTRACTING) {
      _requestMatchingInit(data);
    } else {
      Commons.showToast(_response.getMsg());
    }
    return _response;
  }

  ///돌봄계약서 계약서보기 requestMatchingInit
  Future<MatchingViewResponse> _requestMatchingView(MatchingViewRequest data) async {
    flag.enableLoading(fn: () => onUpDate());
    MatchingViewResponse _response = await apiHelper.requestMatchingView(data).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    flag.disableLoading(fn: () => onUpDate());
    if (_response.getCode() == KEY_SUCCESS && _response.isData()) {
      setMatchingViewResponse(_response);
      setMatchingInitViewData(_response.getData());
      setCareViewData(_response.getData().bookingcareservices_info!);
      onDataPage(getCareViewData());
      setCareViewChildInfoData(getCareViewChildInfoData());
    }
    return _response;
  }

  ///내 신청서 이고 수정모드이면
  setModifyAddress() {
    if (isModifyMode()) {
      ReqSchoolToHomeData item = ReqSchoolToHomeData();
      if (getServiceType() == ServiceType.serviceType_0) {
        item = data.jobItem.reqdata.bookinggotoschool!.first;
        if (getCareViewData().careschool!.start_address.isNotEmpty && !getCareViewData().careschool!.start_address.contains(item.start_address_detail)) {
          getCareViewData().careschool!.start_address = getCareViewData().careschool!.start_address + ' ' + item.start_address_detail;
        }
        if (getCareViewData().careschool!.end_address.isNotEmpty && !getCareViewData().careschool!.end_address.contains(item.end_address_detail)) {
          getCareViewData().careschool!.end_address = getCareViewData().careschool!.end_address + ' ' + item.end_address_detail;
        }
      } else if (getServiceType() == ServiceType.serviceType_1) {
        item = data.jobItem.reqdata.bookingafterschool!.first;
        if (getCareViewData().careschool!.start_address.isNotEmpty && !getCareViewData().careschool!.start_address.contains(item.start_address_detail)) {
          getCareViewData().careschool!.start_address = getCareViewData().careschool!.start_address + ' ' + item.start_address_detail;
        }
        if (getCareViewData().careschool!.end_address.isNotEmpty && !getCareViewData().careschool!.end_address.contains(item.end_address_detail)) {
          getCareViewData().careschool!.end_address = getCareViewData().careschool!.end_address + ' ' + item.end_address_detail;
        }
      } else if (getServiceType() == ServiceType.serviceType_2) {
        ReqBoyukData item = data.jobItem.reqdata.bookingboyuk!.first;
        if (getCareViewData().careboyuk!.boyuk_address.isNotEmpty && !getCareViewData().careboyuk!.boyuk_address.contains(item.boyuk_address_detail)) {
          getCareViewData().careboyuk!.boyuk_address = getCareViewData().careboyuk!.boyuk_address + ' ' + item.boyuk_address_detail;
        }
        if (getCareViewData().careboyuk!.pathroute_comment.isNotEmpty) {
          getCareViewData().careboyuk!.pathroute_comment = item.pathroute_comment;
        }
      } else if (getServiceType() == ServiceType.serviceType_3) {
      } else if (getServiceType() == ServiceType.serviceType_4) {
      } else if (getServiceType() == ServiceType.serviceType_5) {}
    }
  }

  @override
  void onDataPage(_data) {
    if (_data is CareViewData) {
      if (_data == null) {
        onConfirmBtn();
        return;
      }

      //링크쌤 인증 리스트 만들기
      data.listViewDetailItem.lsAuthInfo.clear();
      data.listViewDetailItem.lsAuthInfo.addAll(Commons.getAuthList(getCareViewData().authinfo!));
      onOptionUpdate();
      data.jobItem.jobData = Commons.getJobData(getCareViewData().servicetype, getCareViewData().possible_area);
      data.jobItem.request.servicetype = getCareViewData().servicetype;
      data.jobItem.request.possible_area = getCareViewData().possible_area;
      data.jobItem.request.child = getCareViewChildInfoData().child_id;

      //내 신청서 인 경우 하단 채팅하기 등등 버튼을 보이지 않는다.
      if (isMyProfile() && matchingData == null && getViewType() != ViewType.modify) {
        btnType = BottomBtnType.not;
      }

      //내 신청서 상태가 구인중0, 기간만료8 인 경우에만 수정가능하다. 지난거라면 수정을 못하도록 막는다 2021/10/28
      if (!EnumUtils.isBookingStatus(getCareViewData().status)) {
        setViewType(ViewType.view);
      }
    } else {}
    onConfirmBtn();
  }

  ///나의정보
  Widget _myInfoView() {
    return Column(
      children: [
        expiredView(status: getCareViewData().status),
        Container(
          color: Commons.getColor(),
          child: Column(
            children: [
              if (isContract) //돌봄 계약서 호출시 사용
                lRoundContent(
                  content1: "돌봄계약서_안내".tr(),
                  textColor: color_white,
                  margin: padding_20,
                  padding: padding_15,
                  bgColor: Commons.getColor(),
                  borderColor: color_white,
                  borderRadius: 16.0,
                  crossAxisAlignment: CrossAxisAlignment.start,
                ),
              momDadyMyInfoView(
                context,
                data: getCareViewData(),
                pay: '${StringUtils.formatPay(getCareViewData().cost_sum)}${"원".tr()}',
                viewMode: getViewMode(),
                callBack: (action) async {
                  if (action == DialogAction.follower) {
                    getCareViewData().userinfo!.is_follower = !getCareViewData().userinfo!.is_follower;
                    onUpDate();
                    if (getCareViewData().userinfo!.is_follower) {
                      apiHelper.requestCaresWishSave(CaresWishSaveRequest(bookingcare: getCareViewData().booking_id)).then((response) {
                        flag.disableLoading();
                        if (response.getCode() == KEY_SUCCESS) {
                          getCareViewData().userinfo!.is_follower = true;
                        }
                        onUpDate();
                      });
                    } else {
                      apiHelper.requestCaresWishDelete(CaresWishDeleteRequest(bookingcare: getCareViewData().booking_id)).then((response) {
                        flag.disableLoading();
                        if (response.getCode() == KEY_SUCCESS) {
                          getCareViewData().userinfo!.is_follower = false;
                        }
                        onUpDate();
                      });
                    }
                  } else if (action == DialogAction.modify) {
                    var resultData = await Commons.page(context, routeParents8, arguments: ParentsStep8Page(data: data, viewMode: ViewMode(viewType: ViewType.modify)));
                    if (resultData != null) {
                      data.jobItem.reqdata = resultData;
                      getCareViewData().is_togetherboyuk = StringUtils.getIntToBool(data.jobItem.reqdata.is_togetherboyuk);
                      getCareViewData().is_negotiable = StringUtils.getIntToBool(data.jobItem.reqdata.is_negotiable);
                      _upDatePay(data.jobItem.reqdata.cost_sum, data.jobItem.reqdata.cost_avg);
                      onConfirmBtnModify();
                      // onUpDate();
                    }
                  }
                },
                content: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    sb_h_10,
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        lText(matchingData != null && matchingData!.matchingStatus >= MatchingStatus.paid.value ? getMatchingInitViewData().contract_info!.momdady_first_name : getCareViewData().userinfo!.first_name, style: st_b_22(fontWeight: FontWeight.bold)),
                        sb_w_05,
                        locationAffiliateView(
                          sizeLocation: 22,
                          address: getCareViewData().userinfo!.getAddress(),
                          style: st_b_16(fontWeight: FontWeight.w500),
                          isAffiliate: getCareViewData().userinfo!.is_affiliate,
                          sizeAffiliate: 17,
                        ),
                      ],
                    ),
                    lDivider20(color: color_dedede, thickness: 1.0, padding: padding_10_TB),
                    Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                      lText(
                        "아이정보".tr(),
                        style: st_b_16(fontWeight: FontWeight.bold),
                      ),
                      sb_w_20,
                      listViewChildInfoView(
                        name: matchingData != null && matchingData!.matchingStatus >= MatchingStatus.paid.value ? getMatchingInitViewData().contract_info!.child_name : getCareViewChildInfoData().child_name,
                        age: getCareViewChildInfoData().child_age,
                        gender: getCareViewChildInfoData().child_gender,
                        fontWeight: FontWeight.w500,
                        style: st_b_16(),
                      ),
                      if (getViewType() == ViewType.modify)
                        Expanded(
                          child: lInkWell(
                            splashColor: color_transparent,
                            highlightColor: color_transparent,
                            onTap: () async {
                              var resultData = await Commons.page(context, routeChildInfoAdd, arguments: ChildInfoAddPage(data: data, child_id: getCareViewChildInfoData().child_id, viewMode: ViewMode(viewType: ViewType.modify, itemType: ItemType.modify)));
                              if (resultData != null) {
                                data.jobItem.childInfo = resultData;
                                getCareViewChildInfoData().child_name = data.jobItem.childInfo.name!;
                                getCareViewChildInfoData().child_age = '${data.jobItem.childInfo.age}${"세".tr()}';
                                getCareViewChildInfoData().child_gender = EnumUtils.getGender(data.jobItem.childInfo.gender!).stringChild;
                                getCareViewChildInfoData().child_character = data.jobItem.childInfo.getStringAppend(data.jobItem.childInfo.character!);
                                getCareViewChildInfoData().is_allergy = data.jobItem.childInfo.is_allergy;
                                getCareViewChildInfoData().allergy_name = data.jobItem.childInfo.allergy_name;
                                getCareViewChildInfoData().allergy_message = data.jobItem.childInfo.allergy_message;
                                onUpDate();
                              }
                              onConfirmBtnModify();
                              // onUpDate();
                            },
                            child: lText(
                              "수정".tr(),
                              style: st_modify,
                              textAlign: TextAlign.right,
                            ),
                          ),
                        ),
                    ]),
                    sb_h_05,
                    Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                      Padding(
                        padding: padding_05_T,
                        child: lText(
                          "아이등록_아이성향".tr(),
                          style: st_b_16(fontWeight: FontWeight.bold),
                        ),
                      ),
                      sb_w_20,
                      //아이성향
                      Expanded(
                        child: Wrap(
                          spacing: 8.0,
                          runSpacing: -4.0,
                          children: [
                            ..._childCharacterView(),
                          ],
                        ),
                      ),
                    ]),
                    if (getCareViewChildInfoData().is_allergy)
                      Container(
                        padding: padding_10_T,
                        child: Column(
                          children: [
                            if (StringUtils.validateString(getCareViewChildInfoData().allergy_name))
                              Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                lText(
                                  "알레르기".tr(),
                                  style: st_b_16(fontWeight: FontWeight.bold),
                                ),
                                sb_w_20,
                                Expanded(
                                  child: lText(
                                    StringUtils.validateString(getCareViewChildInfoData().allergy_name) ? getCareViewChildInfoData().allergy_name : '-',
                                    style: st_b_16(),
                                  ),
                                ),
                              ]),
                            sb_h_10,
                            if (StringUtils.validateString(getCareViewChildInfoData().allergy_message))
                              Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                lText(
                                  "금기식품".tr(),
                                  style: st_b_16(fontWeight: FontWeight.bold),
                                ),
                                sb_w_20,
                                Expanded(
                                  child: lText(
                                    StringUtils.validateString(getCareViewChildInfoData().allergy_message) ? getCareViewChildInfoData().allergy_message : '-',
                                    style: st_b_16(),
                                  ),
                                ),
                              ]),
                          ],
                        ),
                      ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  ///돌봄유형, 보육장소
  Widget _careTypeView() {
    double iconSize = 30;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            lText(
              "돌봄유형".tr(),
              style: st_b_16(fontWeight: FontWeight.bold),
            ),
            sb_w_15,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Lcons.serviceType(getServiceType(), size: iconSize, color: Commons.getColor(), isEnabled: true),
                    sb_w_10,
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        lText(getServiceType().string, style: st_16(textColor: Commons.getColor(), fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ],
                ),
                if (getCareViewData().is_togetherboyuk)
                  Row(
                    children: [
                      Lcons.check_circle(isEnabled: true, color: Commons.getColor(), disableColor: color_dedede),
                      sb_w_03,
                      Row(
                        children: [
                          lAutoSizeText(
                            getGroupTitle(getAreaType()),
                            style: st_b_14(textColor: Commons.getColor()),
                            minFontSize: 5,
                            maxLines: 1,
                          ),
                          sb_w_03,
                          lInkWell(
                            onTap: () {
                              showNormalDlg(
                                title: getAreaType() == PossibleArea.mom_daddy ? "동시보육이란?".tr() : "그룹보육이란?".tr(),
                                titleAlign: Alignment.centerLeft,
                                paddingTitle: padding_30_T,
                                padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                                messageWidget: dlgGroupView(context, getAreaType() /*, timePrice: data.payItem.payPrice*/),
                              );
                            },
                            child: Lcons.info(color: color_dedede, isEnabled: true, size: 25),
                          ),
                        ],
                      ),
                    ],
                  )
              ],
            ),
          ],
        ),
        sb_h_10,
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            lText("보육장소".tr(), style: st_b_16(fontWeight: FontWeight.bold)),
            sb_w_15,
            getAreaType() == PossibleArea.mom_daddy ? Lcons.my_house(size: iconSize, color: Commons.getColor(), isEnabled: true) : Lcons.neighbor_house(size: iconSize, color: Commons.getColor(), isEnabled: true),
            sb_w_10,
            lText(
              Commons.possibleAreaSubText(getAreaType().name),
              style: st_16(textColor: Commons.getColor(), fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ],
    );
  }

  ///날짜/시간
  Widget _dateView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          children: [
            contentView(
                data: ContentData(content: "날짜시간".tr(), isModify: getViewType() == ViewType.modify ? true : false),
                onClick: () async {
                  var resultData = await Commons.page(context, routeParents4, arguments: ParentsStep4Page(data: data, viewMode: ViewMode(viewType: ViewType.modify)));

                  if (resultData != null) {
                    JobItem item = resultData as JobItem;
                    if (data.jobItem.calcuTime != item.totalTime) {
                      showNormalDlg(
                          message: "필요한돌봄안내".tr(),
                          btnLeft: "아니오".tr(),
                          btnRight: "예".tr(),
                          onClickAction: (action) async {
                            if (action == DialogAction.yes) {
                              var serviceData = await Commons.page(context, routeParents6,
                                  arguments: ParentsStep6Page(
                                    data: data,
                                    viewMode: getViewMode(),
                                    tabType: ServiceTabType.care,
                                  ));

                              if (serviceData != null) {
                                data.jobItem.reqdata = serviceData;
                                _upDateOption();
                                onOptionUpdate();
                              }
                            }
                          });
                    }
                    //이전 날짜와 다를시 필요한 돌봄 체크를 확인
                    data.jobItem = item;
                    if (item.reqdata.careschedule!.first.bookingdate.length != getCareViewData().caredate!.length) {
                      int totalPay = Commons.getTotalPay(item.reqdata.careschedule!.first.bookingdate.length, item.reqdata.cost_perhour, item.totalTime);
                      int avgPrice = Commons.getAvgPrice(item.reqdata.cost_perhour, item.totalTime, item.reqdata.cost_negotiable);
                      data.jobItem.reqdata.cost_sum = totalPay;
                      data.jobItem.reqdata.cost_avg = avgPrice;
                      getCareViewData().caredate = item.reqdata.careschedule!.first.bookingdate;
                      _upDatePay(totalPay, avgPrice);
                    }
                    getCareViewData().stime = item.reqdata.careschedule!.first.stime;
                    getCareViewData().etime = item.reqdata.careschedule!.first.etime;
                  }
                  onConfirmBtnModify();
                  // onUpDate();
                }),
            sb_h_15,
          ],
        ),
        titleValueView(
          "돌봄날짜".tr(),
          valueRight: getCareDate(),
          bottomAlign: Alignment.bottomLeft,
          flex: 1,
          flex2: 3,
        ),
        titleValueView(
          "돌봄시간".tr(),
          valueRight: getCareTime(),
          flex: 1,
          flex2: 3,
        ),
        sb_h_10,
        lBtn(
          getViewType() == ViewType.apply ? "돌봄날짜시간".tr() : "돌봄날짜확인".tr(),
          borderRadius: 8,
          btnColor: color_eeeeee,
          style: st_b_16(fontWeight: FontWeight.w500),
          padding: padding_0,
          margin: padding_0,
          onClickAction: () {
            Commons.page(context, routeMomdadySchedule,
                arguments: MomdadySchedulePage(
                  data: getViewType() == ViewType.modify ? data.jobItem.reqdata.careschedule!.first.bookingdate : getCareViewData().caredate!,
                  sTime: getViewType() == ViewType.modify ? data.jobItem.reqdata.careschedule!.first.stime : getCareViewData().stime,
                  eTime: getViewType() == ViewType.modify ? data.jobItem.reqdata.careschedule!.first.etime : getCareViewData().etime,
                ));
          },
        ),
      ],
    );
  }

  ///이동빙법화면
  Widget _moveView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          children: [
            contentView(
                data: ContentData(content: Commons.getMoveTitle(getServiceType()), isModify: getViewType() == ViewType.modify ? true : false),
                onClick: () async {
                  var resultData = await Commons.page(context, routeParents5, arguments: ParentsStep5Page(data: data, viewMode: ViewMode(viewType: ViewType.modify)));
                  if (resultData != null) {
                    data.jobItem.reqdata = resultData;
                    //이동수단 변경
                    //기존 응답 받은 데이터에 업데이트를 한다. 보육은 없다!
                    _upDateMove();
                    onOptionUpdate();
                  }
                  onUpDate();
                }),
            sb_h_15,
          ],
        ),
        //보육이 아닐때
        if (!isBoyuk(getServiceType()))
          Column(
            children: [
              titleValueView(
                "이동수단".tr(),
                valueRight: getMoveType(),
                maxLineRight: 2,
                flex: flex,
                flex2: flex2,
              ),
              titleValueView(
                "출발시간".tr(),
                valueRight: getStartTime(),
                flex: flex,
                flex2: flex2,
              ),
            ],
          ),

        if (isBoyuk(getServiceType())) sb_h_15,
        Column(
          children: [
            //계약서 작성시 링크쌤 일 경우 (등원이고 이웃집일 경우 또는 보육이고 이웃집일 링크쌤이 주소 등록)
            isSignMode(isContract) && isLinkMomAddress(getStartAddress(getServiceType(), getAreaType(), matchingStatus: matchingData?.matchingStatus))
                ? signView(getServiceType() == ServiceType.serviceType_2 ? SignType.boyukSign : SignType.startSign, Commons.getStartAddressTitle(getServiceType(), type: 1)) //링크쌤이고, 서명해야될 경우, 출발/돌봄장소
                : titleValueView(
                    Commons.getStartAddressTitle(getServiceType(), type: 1),
                    valueRight: getStartAddress(getServiceType(), getAreaType(), matchingStatus: matchingData?.matchingStatus),
                    flex: flex,
                    flex2: flex2,
                    maxLineRight: maxLineAddress,
                  ),
          ],
        ),

        titleValueView(
          "추가설명".tr(),
          padding: padding_0,
          valueRight: getStartComment(getServiceType()),
          flex: flex,
          flex2: flex2,
          maxLineRight: maxLineAddress,
        ),
        lDivider20(color: color_d2d2d2, thickness: 1.0, padding: padding_10_TB),

        //하원시 경우지
        if (getServiceType() == ServiceType.serviceType_1)
          if (StringUtils.validateString(getCareViewData().careschool!.pathroute_comment)) //경우지 하원
            Column(
              children: [
                titleValueView(
                  "경유지장소".tr(),
                  padding: padding_0,
                  rightWidget: lAutoSizeText(
                    getPathRoute(getServiceType(), matchingStatus: matchingData?.matchingStatus),
                    maxLines: maxLinePath,
                    style: st_b_16(),
                  ),
                  flex: flex,
                  flex2: flex2,
                ),
                lDivider20(color: color_d2d2d2, thickness: 1.0, padding: padding_10_TB),
              ],
            ),

        //보육 경우지
        if (getServiceType() == ServiceType.serviceType_2)
          if (StringUtils.validateString(getCareViewData().careboyuk!.pathroute_comment))
            Column(
              children: [
                titleValueView(
                  "경유지장소".tr(),
                  rightWidget: lAutoSizeText(
                    getPathRoute(getServiceType(), matchingStatus: matchingData?.matchingStatus),
                    maxLines: maxLinePath,
                    style: st_b_16(),
                  ),
                  flex: flex,
                  flex2: flex2,
                ),
              ],
            ),

        //보육이 아닐때 도착 설명
        if (!isBoyuk(getServiceType()))
          Column(
            children: [
              titleValueView(
                "도착시간".tr(),
                valueRight: getEndTime(getServiceType()),
                flex: flex,
                flex2: flex2,
              ),
              //계약서 작성시 링크쌤 일 경우, 하원이고 이웃집인 경우
              isSignMode(isContract) && isLinkMomAddress(getEndAddress(getServiceType(), getAreaType(), matchingStatus: matchingData?.matchingStatus))
                  ? signView(SignType.endSign, "도착장소".tr()) //링크쌤이고, 서명해야될 경우, 도착장소
                  : titleValueView(
                      //계약서가 아니면 일반
                      "도착장소".tr(),
                      valueRight: getEndAddress(getServiceType(), getAreaType(), matchingStatus: matchingData?.matchingStatus),
                      flex: flex,
                      flex2: flex2,
                      maxLineRight: maxLineAddress,
                    ),
              titleValueView(
                "추가설명".tr(),
                padding: padding_0,
                valueRight: getEndComment(getServiceType()),
                flex: flex,
                flex2: flex2,
                maxLineRight: maxLineAddress,
              ),
            ],
          ),
        //이웃집 위치 1,2순위 표시 (이웃집일 경우만 표시 + 보육&이웃집)
        if (getAreaType() == PossibleArea.link_mom || isBoyuk(getServiceType()) && getAreaType() == PossibleArea.link_mom)
          Column(
            children: [
              lDivider20(color: color_d2d2d2, thickness: 1.0, padding: padding_10_TB),
              titleValueView(
                "원하는링크쌤집위치_줄바꿈".tr(),
                maxLine: 2,
                padding: padding_0,
                rightWidget: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Row(
                      children: [
                        lText("1순위".tr(), style: st_b_16(fontWeight: FontWeight.bold)),
                        sb_w_05,
                        Expanded(
                            child: lAutoSizeText(
                          getLocation(getServiceType(), getAreaType(), 1),
                          maxLines: maxLineAddress,
                          style: st_b_16(),
                        )),
                      ],
                    ),
                    sb_h_05,
                    Row(
                      children: [
                        lText("2순위".tr(), style: st_b_16(fontWeight: FontWeight.bold)),
                        sb_w_05,
                        Expanded(
                            child: lAutoSizeText(
                          getLocation(getServiceType(), getAreaType(), 2),
                          maxLines: maxLineAddress,
                          style: st_b_16(),
                        )),
                      ],
                    )
                  ],
                ),
                flex: flex,
                flex2: flex2,
              ),
            ],
          ),
      ],
    );
  }

  ///필요한돌봄
  Widget _addCareView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        lInkWell(
          onTap: () {
            onUpDate();
          },
          child: contentView(
              data: ContentData(
                content: "필요한돌봄".tr(),
              ),
              onClick: (value) {
                onUpDate();
              }),
        ),
        sb_h_15,
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _addCareItemView(
              title: "보육".tr(),
              tabType: ServiceTabType.care,
              length: data.jobItem.boyukCnt,
              isEnable: data.jobItem.isBoyuk,
            ),
            _addCareItemView(
              title: "가사".tr(),
              tabType: ServiceTabType.work,
              length: data.jobItem.homeCareCnt,
              isEnable: data.jobItem.isHomeCare,
            ),
            _addCareItemView(
              title: "놀이".tr(),
              tabType: ServiceTabType.play,
              length: data.jobItem.playCnt,
              isEnable: data.jobItem.isPlay,
            ),
            _addCareItemView(
              title: "투약".tr(),
              tabType: ServiceTabType.drug,
              length: data.jobItem.playCnt,
              isEnable: true,
              isBadge: false,
            ),
          ],
        ),
      ],
    );
  }

  ///링크쌤 인증
  Widget _authView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        contentView(
          data: ContentData(content: "맘대디인증".tr()),
        ),
        Padding(
          padding: padding_15_TB,
          child: lRoundContainer(
            borderRadius: 8.0,
            bgColor: color_eeeeee.withOpacity(0.42),
            padding: padding_10,
            child: InkWell(
              onTap: () async {
                var _data = await Commons.page(context, routeAuthCenter,
                    arguments: AuthCenterPage(
                      data: data,
                      id: getCareViewData().authinfo!.auth_id,
                      viewType: getViewMode().viewType,
                      name: isContract ? getMatchingInitViewData().contract_info!.momdady_first_name : getCareViewData().userinfo!.first_name,
                    ));
                if (_data != null) {
                  data.jobItem.requestJob = _data;
                  onDataPage(data.jobItem.requestJob);
                }
              },
              child: Row(
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        margin: padding_02_R,
                        decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [BoxShadow(blurRadius: 10, color: Colors.grey.withOpacity(0.2))]),
                        child: Lcons.auth_count(size: 60, isEnabled: Commons.isLinkMom()),
                      ),
                      lText('${getCareViewData().authinfo!.grade_authlevel}', style: st_14(fontWeight: FontWeight.w700)),
                    ],
                  ),
                  sb_w_10,
                  lText('${"인증배지".tr()}${getCareViewData().authinfo!.grade_authlevel}${"개".tr()}', style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Lcons.nav_right(color: color_b2b2b2, size: 20),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        if (data.listViewDetailItem.lsAuthInfo.isNotEmpty)
          Wrap(
            spacing: 8.0,
            runSpacing: -4.0,
            children: [
              ...authInfoView(),
            ],
          ),
      ],
    );
  }

  ///돌봄계약서(서명)
  Widget _contractView() {
    return Column(
      children: [
        lRoundContent(
          content1: "개인정보제3자제공동의".tr(),
          padding: padding_15,
          textColor: color_545454,
          fontWeight: FontWeight.w500,
          margin: padding_02_B,
          borderRadius: 0,
          borderWidth: 0,
          borderColor: color_transparent,
          bgColor: color_b2b2b2.withOpacity(0.32),
          alignment: Alignment.center,
        ),
        Container(
          color: color_dedede.withOpacity(0.32),
          margin: padding_02_B,
          child: Row(
            children: [
              Expanded(
                flex: 4,
                child: lRoundContent(
                  content1: "개인정보제공받는자".tr(),
                  textColor: color_545454,
                  fontSize: 13,
                  borderRadius: 0,
                  borderWidth: 1,
                  borderColor: color_transparent,
                  bgColor: color_transparent,
                  textAlign: TextAlign.center,
                ),
              ),
              Expanded(
                flex: 5,
                child: lRoundContent(
                  content1: "개인정보제공받는자_내용".tr(),
                  textColor: color_545454,
                  fontSize: 13,
                  borderRadius: 0,
                  borderWidth: 1,
                  borderColor: color_eeeeee,
                  bgColor: color_white,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
        Container(
          color: color_dedede.withOpacity(0.32),
          margin: padding_02_B,
          child: Row(
            children: [
              Expanded(
                flex: 4,
                child: lRoundContent(
                  content1: "제공목적".tr(),
                  textColor: color_545454,
                  fontSize: 13,
                  borderRadius: 0,
                  borderWidth: 1,
                  borderColor: color_transparent,
                  bgColor: color_transparent,
                  textAlign: TextAlign.center,
                ),
              ),
              Expanded(
                flex: 5,
                child: lRoundContent(
                  content1: "제공목적_내용".tr(),
                  textColor: color_545454,
                  fontSize: 13,
                  borderRadius: 0,
                  borderWidth: 1,
                  borderColor: color_eeeeee,
                  bgColor: color_white,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
        Container(
          color: color_dedede.withOpacity(0.32),
          margin: padding_02_B,
          child: Row(
            children: [
              Expanded(
                flex: 4,
                child: lRoundContent(
                  content1: "제공정보".tr(),
                  textColor: color_545454,
                  fontSize: 13,
                  borderRadius: 0,
                  borderWidth: 1,
                  borderColor: color_transparent,
                  bgColor: color_transparent,
                  textAlign: TextAlign.center,
                ),
              ),
              Expanded(
                flex: 5,
                child: lRoundContent(
                  content1: "제공정보_내용".tr(),
                  textColor: color_545454,
                  fontSize: 13,
                  borderRadius: 0,
                  borderWidth: 1,
                  borderColor: color_eeeeee,
                  bgColor: color_white,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
        Container(
          color: color_dedede.withOpacity(0.32),
          margin: padding_02_B,
          child: Row(
            children: [
              Expanded(
                flex: 4,
                child: lRoundContent(
                  content1: "보유및이용기간".tr(),
                  textColor: color_545454,
                  fontSize: 13,
                  borderRadius: 0,
                  borderWidth: 1,
                  borderColor: color_transparent,
                  bgColor: color_transparent,
                  textAlign: TextAlign.center,
                ),
              ),
              Expanded(
                flex: 5,
                child: lRoundContent(
                  content1: "보유및이용기간_내용".tr(),
                  textColor: color_545454,
                  fontSize: 13,
                  borderRadius: 0,
                  borderWidth: 1,
                  borderColor: color_eeeeee,
                  bgColor: color_white,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
        Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: padding_15,
                    child: lAutoSizeText(StringUtils.getContractDate(getMatchingInitViewData().contract_info!.momdady_signdate), style: st_b_13()),
                  ),
                  lAutoSizeText(getMatchingInitViewData().contract_info!.momdady_first_name, style: st_b_16(fontWeight: FontWeight.w500)),
                  Container(
                    margin: padding_10,
                    alignment: Alignment.center,
                    child: CachedNetworkImage(
                      width: 100,
                      height: 100,
                      imageUrl: getMatchingInitViewData().contract_info!.momdady_signature,
                      imageBuilder: (context, imageProvider) => imageBuilder(imageProvider, width: 100, height: 100, fit: BoxFit.contain),
                      placeholder: (context, url) => emptySignature(url),
                      errorWidget: (context, url, error) => Center(),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  Padding(
                    padding: padding_15,
                    child: lAutoSizeText(StringUtils.getContractDate(getMatchingInitViewData().contract_info!.linkmom_signdate), style: st_b_13()),
                  ),
                  lAutoSizeText(getMatchingInitViewData().contract_info!.linkmom_first_name, style: st_b_16(fontWeight: FontWeight.w500)),
                  Container(
                    margin: padding_10,
                    alignment: Alignment.center,
                    child: CachedNetworkImage(
                      width: 100,
                      height: 100,
                      imageUrl: getMatchingInitViewData().contract_info!.linkmom_signature,
                      imageBuilder: (context, imageProvider) => imageBuilder(imageProvider, width: 100, height: 100, fit: BoxFit.contain),
                      placeholder: (context, url) => emptySignature(url),
                      errorWidget: (context, url, error) => Center(),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _addCareItemView({
    String title = '',
    ServiceTabType tabType = ServiceTabType.care,
    bool isEnable = false,
    bool isBadge = true,
    int length = 0,
    double size = 45,
  }) {
    int matchingStatus = matchingData == null ? 0 : matchingData!.matchingStatus;
    // ChildInfoData
    ChildInfoItem childInfoItem = ChildInfoItem.init();
    childInfoItem.itemData = ChildInfoData(child_name: getCareViewChildInfoData().child_name, id: getCareViewChildInfoData().child_id);
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: () async {
            var resultData = await Commons.page(context, routeParents6,
                arguments: ParentsStep6Page(
                  data: data,
                  viewMode: getViewMode(),
                  tabType: tabType,
                  matchingStatus: matchingStatus,
                  childInfo: childInfoItem,
                ));
            if (resultData != null) {
              data.jobItem.reqdata = resultData;
              _upDateOption();
              onOptionUpdate();
            }
          },
          child: Stack(
            alignment: Alignment.bottomRight,
            children: [
              Container(
                alignment: Alignment.center,
                padding: padding_10,
                decoration: BoxDecoration(
                  color: isEnable ? Commons.getColor().withOpacity(0.12) : color_eeeeee.withOpacity(0.42),
                  borderRadius: BorderRadius.circular(40),
                ),
                child: Lcons(
                  tabType.iconPath,
                  isEnabled: isEnable,
                  color: Commons.getColor(),
                  disableColor: color_dbdbdb,
                  size: size,
                ),
              ),
              if (isBadge)
                Container(
                  height: size,
                  width: size,
                  alignment: Alignment.bottomRight,
                  child: Badge(
                    elevation: 0,
                    badgeColor: isEnable ? Commons.getColor() : color_dbdbdb,
                    alignment: Alignment.bottomRight,
                    toAnimate: false,
                    badgeContent: lText('$length', style: st_15()),
                  ),
                ),
            ],
          ),
        ),
        sb_h_10,
        lAutoSizeText(
          title,
          style: st_b_16(fontWeight: FontWeight.w500),
        ),
      ],
    );
  }

  ///상세데이터
  setCareViewData(CareViewData careViewData) {
    data.listViewDetailItem.careViewData = careViewData;
  }

  ///상세데이터
  CareViewData getCareViewData() {
    return data.listViewDetailItem.careViewData!;
  }

  ///상세데이터
  setCareViewResponse(CareViewResponse careViewResponse) {
    data.listViewDetailItem.responseData = careViewResponse;
  }

  ///상세데이터
  CareViewResponse getCareViewResponse() {
    return data.listViewDetailItem.responseData;
  }

  ///돌봄계약서 Response 데이터
  setMatchingViewResponse(MatchingViewResponse responseData) {
    data.listViewDetailItem.responseContractData = responseData;
  }

  ///돌봄계약서 Response 데이터
  MatchingViewResponse getMatchingViewResponse() {
    return data.listViewDetailItem.responseContractData;
  }

  ///돌봄계약서 데이터
  setMatchingInitViewData(MatchingInitViewData responseData) {
    data.listViewDetailItem.matchingInitViewData = responseData;
  }

  ///돌봄계약서 데이터
  MatchingInitViewData getMatchingInitViewData() {
    return data.listViewDetailItem.matchingInitViewData!;
  }

  ///아이정보
  CareViewChildInfoData getCareViewChildInfoData() {
    return data.listViewDetailItem.careViewData!.childinfo!;
  }

  ///아이정보
  void setCareViewChildInfoData(CareViewChildInfoData childInfoData) {
    data.listViewDetailItem.careViewData!.childinfo = childInfoData;
  }

  ///아이성향
  _childCharacterView() {
    var character = getCareViewChildInfoData().child_character.split(",");
    return character.map((character) => childCharacterView(character));
  }

  ///해시태그
  authInfoView() {
    return data.listViewDetailItem.lsAuthInfo.map(
      (tag) => hashTagView(
        tag,
        textColor: Commons.getColorServiceTypeText(),
      ),
    );
  }

  ///돌봄날짜
  String getCareDate() {
    String type = '';
    try {
      int dateCnt = getViewType() == ViewType.modify ? data.jobItem.reqdata.careschedule!.first.bookingdate.length : getCareViewData().caredate!.length;
      String date = getViewType() == ViewType.modify ? data.jobItem.reqdata.careschedule!.first.bookingdate.first : getCareViewData().caredate!.first;
      if (dateCnt > 1) {
        type = '${StringUtils.getStringToDate(date)} ${"외".tr()} ${dateCnt - 1}${"일".tr()}';
      } else {
        type = StringUtils.getStringToDate(date);
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getCareDate : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }

    return type;
  }

  ///돌봄시간
  String getCareTime() {
    String type = '';
    try {
      String sTime = StringUtils.getStringToTime(getCareViewData().stime);
      String eTime = StringUtils.getStringToTime(getCareViewData().etime);
      type = '$sTime ~ $eTime';
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getCareTime : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return type;
  }

  ///출발시간
  String getStartTime() {
    String type = '';
    try {
      type = StringUtils.getStringToTime(getCareViewData().careschool!.start_time);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getStartTime : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return type;
  }

  ///이동수단
  String getMoveType() {
    StringBuffer st = StringBuffer();
    String value = '';
    try {
      getCareViewData().careschool!.vehicle_id!.forEach((value) {
        if (value == MoveType.waking.index) {
          //도보
          st.write('${"도보".tr()}/');
        } else if (value == MoveType.car.index) {
          //자차
          st.write('${"자동차".tr()}/');
        } else if (value == MoveType.transport.index) {
          //대중교통
          st.write('${"대중교통".tr()}/');
        }
      });

      value = st.toString();
      if (value.length != 0 && value.contains("/")) {
        value = value.substring(0, value.toString().length - 1);
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getMoveType : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  ///출발장소
  String getStartAddress(ServiceType serviceType, PossibleArea possibleArea, {int? matchingStatus = 0}) {
    String value = '';
    if (matchingStatus == null) {
      matchingStatus = MatchingStatus.inprogress.value;
    }
    try {
      if (serviceType == ServiceType.serviceType_0) {
        if (matchingStatus >= MatchingStatus.paid.value) {
          //계약이 완료된 상태이면 계약된 주소 정보를 보여준다.
          value = getMatchingInitViewData().contract_info!.start_address ?? getCareViewData().careschool!.start_address;
        } else {
          //등원이고, 이웃집
          if (possibleArea == PossibleArea.link_mom) {
            //링크쌤 모드이고, 매칭이 링크쌤이 먼저 진행(92), 그리고 맘대디가 매칭 계약서(99)를 작성하여도 링크쌤에게만 주소를 노출한다.
            if (matchingStatus >= MatchingStatus.waitLinkmom.value) {
              if (Commons.isLinkMom()) {
                value = getMatchingInitViewData().contract_info!.start_address ?? getCareViewData().careschool!.start_address;
              } else {
                value = "결제후확인가능".tr();
              }
            } else {
              value = "링크쌤이주소등록".tr();
            }
          } else {
            value = getCareViewData().careschool!.start_address;
          }
        }
      } else if (serviceType == ServiceType.serviceType_1) {
        if (matchingStatus >= MatchingStatus.paid.value) {
          value = getMatchingInitViewData().contract_info!.start_address ?? getCareViewData().careschool!.start_address;
        } else {
          value = getCareViewData().careschool!.start_address;
        }
      } else if (serviceType == ServiceType.serviceType_2) {
        if (matchingStatus >= MatchingStatus.paid.value) {
          //계약이 완료된 상태이면 계약된 주소 정보를 보여준다.
          value = getMatchingInitViewData().contract_info!.care_address ?? getCareViewData().careboyuk!.boyuk_address;
        } else {
          if (possibleArea == PossibleArea.link_mom) {
            //링크쌤 모드이고, 매칭이 링크쌤이 먼저 진행(92), 그리고 맘대디가 매칭 계약서(99)를 작성하여도 링크쌤에게만 주소를 노출한다.
            if (matchingStatus >= MatchingStatus.waitLinkmom.value) {
              if (Commons.isLinkMom()) {
                value = getMatchingInitViewData().contract_info!.care_address ?? getCareViewData().careboyuk!.boyuk_address;
              } else {
                value = "결제후확인가능".tr();
              }
            } else {
              value = "링크쌤이주소등록".tr();
            }
          } else {
            value = getCareViewData().careboyuk!.boyuk_address;
          }
        }
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getStartAddress : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return value;
  }

  ///원하는 이웃집 위치 1순위, 2순위
  String getLocation(ServiceType serviceType, PossibleArea possibleArea, int location) {
    String value = '';
    try {
      if (serviceType == ServiceType.serviceType_0) {
        //등원이고, 이웃집
        if (possibleArea == PossibleArea.link_mom) {
          if (location == 2) {
            value = getCareViewData().careschool!.nhn_area_2_address;
          } else {
            value = getCareViewData().careschool!.nhn_area_1_address;
          }
        }
      } else if (serviceType == ServiceType.serviceType_1) {
        //등원이고, 이웃집
        if (possibleArea == PossibleArea.link_mom) {
          if (location == 2) {
            value = getCareViewData().careschool!.nhn_area_2_address;
          } else {
            value = getCareViewData().careschool!.nhn_area_1_address;
          }
        }
      } else if (serviceType == ServiceType.serviceType_2) {
        //등원이고, 이웃집
        if (possibleArea == PossibleArea.link_mom) {
          if (location == 2) {
            value = getCareViewData().careboyuk!.nhn_area_2_address;
          } else {
            value = getCareViewData().careboyuk!.nhn_area_1_address;
          }
        }
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getLocation : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  ///출발 추가설명
  String getStartComment(ServiceType serviceType) {
    String value = '';
    try {
      if (serviceType == ServiceType.serviceType_0) {
        value = getCareViewData().careschool!.start_comment;
      } else if (serviceType == ServiceType.serviceType_1) {
        value = getCareViewData().careschool!.start_comment;
      } else if (serviceType == ServiceType.serviceType_2) {
        value = getCareViewData().careboyuk!.nhn_comment;
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getStartComment : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  ///하원시 경유지
  String getPathRoute(ServiceType serviceType, {int? matchingStatus = 0}) {
    String value = '';
    if (matchingStatus == null) {
      matchingStatus = MatchingStatus.inprogress.value;
    }
    try {
      if (serviceType == ServiceType.serviceType_0) {
      } else if (serviceType == ServiceType.serviceType_1) {
        //경유지 장소가 안내가 있으면 계약시 보여준다.
        if (matchingStatus >= MatchingStatus.paid.value) {
          //계약이 완료된 상태이면 계약된 주소 정보를 보여준다.
          value = getMatchingInitViewData().contract_info!.pathroute_comment!;
        } else {
          value = getCareViewData().careschool!.pathroute_comment;
        }
      } else if (serviceType == ServiceType.serviceType_2) {
        //경유지 장소가 안내가 있으면 계약시 보여준다.
        if (matchingStatus >= MatchingStatus.paid.value) {
          //계약이 완료된 상태이면 계약된 주소 정보를 보여준다.
          value = getMatchingInitViewData().contract_info!.pathroute_comment!;
        } else {
          value = getCareViewData().careboyuk!.pathroute_comment;
        }
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getPathRoute : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  ///도착시간
  String getEndTime(ServiceType serviceType) {
    String type = '';
    try {
      if (serviceType == ServiceType.serviceType_0) {
        type = getCareViewData().careschool!.end_time;
      } else if (serviceType == ServiceType.serviceType_1) {
        type = getCareViewData().careschool!.end_time;
      } else if (serviceType == ServiceType.serviceType_2) {
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}
      type = StringUtils.getStringToTime(type);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getEndTime : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return type;
  }

  ///도착장소
  String getEndAddress(ServiceType serviceType, PossibleArea possibleArea, {int? matchingStatus = 0}) {
    String value = '';
    if (matchingStatus == null) {
      matchingStatus = MatchingStatus.inprogress.value;
    }
    try {
      if (serviceType == ServiceType.serviceType_0) {
        if (matchingStatus >= MatchingStatus.paid.value) {
          //계약이 완료된 상태이면 계약된 주소 정보를 보여준다.
          value = getMatchingInitViewData().contract_info!.arrive_address ?? getCareViewData().careschool!.end_address;
        } else {
          value = getCareViewData().careschool!.end_address;
        }
      } else if (serviceType == ServiceType.serviceType_1) {
        if (matchingStatus >= MatchingStatus.paid.value) {
          //계약이 완료된 상태이면 계약된 주소 정보를 보여준다.
          value = getMatchingInitViewData().contract_info!.arrive_address ?? getCareViewData().careschool!.end_address;
        } else {
          //하원이고, 이웃집
          if (possibleArea == PossibleArea.link_mom) {
            //링크쌤 모드이고, 매칭이 링크쌤이 먼저 진행(92), 그리고 맘대디가 매칭 계약서(99)를 작성하여도 링크쌤에게만 주소를 노출한다.
            if (matchingStatus >= MatchingStatus.waitLinkmom.value) {
              if (Commons.isLinkMom()) {
                value = getMatchingInitViewData().contract_info!.arrive_address ?? getCareViewData().careschool!.end_address;
              } else {
                value = "결제후확인가능".tr();
              }
            } else {
              value = "링크쌤이주소등록".tr();
            }
          } else {
            value = getCareViewData().careschool!.end_address;
          }
        }
      } else if (serviceType == ServiceType.serviceType_2) {
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getEndAddress : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  ///도착 추가설명
  String getEndComment(ServiceType serviceType) {
    String value = '';
    try {
      if (serviceType == ServiceType.serviceType_0) {
        value = getCareViewData().careschool!.end_comment;
      } else if (serviceType == ServiceType.serviceType_1) {
        value = getCareViewData().careschool!.end_comment;
      } else if (serviceType == ServiceType.serviceType_2) {
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getEndComment : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  ///보육,가사,놀이 플래그 업데이트
  onOptionUpdate() {
    data.jobItem.isBoyuk = false;
    data.jobItem.boyukCnt = 0;
    if (getCareViewData().careoptions!.bookingboyukoptions_product!.length != 0) {
      data.jobItem.isBoyuk = true;
      data.jobItem.boyukCnt = getCareViewData().careoptions!.bookingboyukoptions_product!.length;
    }

    data.jobItem.isHomeCare = false;
    data.jobItem.homeCareCnt = 0;
    if (getCareViewData().careoptions!.bookinghomecareoptions_product!.length != 0) {
      data.jobItem.isHomeCare = true;
      data.jobItem.homeCareCnt = getCareViewData().careoptions!.bookinghomecareoptions_product!.length;
    }

    data.jobItem.isPlay = false;
    data.jobItem.playCnt = 0;
    if (getCareViewData().careoptions!.bookingplayoptions_product!.length != 0) {
      data.jobItem.isPlay = true;
      data.jobItem.playCnt = getCareViewData().careoptions!.bookingplayoptions_product!.length;
    }
    onConfirmBtnModify();
    // onUpDate();
  }

  ///내 신청서 인지 유무 판단
  bool isMyProfile() {
    return getCareViewData().userinfo!.user_id == auth.user.id;
  }

  ///서비스 유형(등원,하원,보육...)
  ServiceType getServiceType() {
    return EnumUtils.getServiceType(index: getCareViewData().servicetype);
  }

  ///이웃집, 우리집
  PossibleArea getAreaType() {
    return EnumUtils.getPossibleArea(getCareViewData().possible_area);
  }

  ///서명모드이고, 링크쌤일 경우 주소르를 입력, 매칭 상태가 링크쌤보다(92) 작을 경우만
  bool isSignMode(bool isContract) {
    return isContract && Commons.isLinkMom() && matchingData!.matchingStatus < MatchingStatus.waitLinkmom.value;
  }

  ///링크쌤이 직접등록
  bool isLinkMomAddress(String value) {
    if (value == "링크쌤이주소등록".tr() || value == "결제후확인가능".tr()) {
      return true;
    }
    return false;
  }

  ///동시, 그룹보육 내용
  String getGroupTitle(PossibleArea possibleArea) {
    String title = "형제자매동시보육이필요해요".tr();
    //등원+우리집
    if (possibleArea == PossibleArea.mom_daddy) {
      title = "형제자매동시보육이필요해요".tr();
      //등원+이웃집
    } else if (possibleArea == PossibleArea.link_mom) {
      title = "링크쌤집에서그룹보육도괜찮아요".tr();
    }
    return title;
  }

  ///등원이고 이웃집인 경우
  bool isGoSchool() {
    return getServiceType() == ServiceType.serviceType_0 && getAreaType() == PossibleArea.link_mom;
  }

  ///하원이고 이웃집인 경우
  bool isGoHome() {
    return getServiceType() == ServiceType.serviceType_1 && getAreaType() == PossibleArea.link_mom;
  }

  ///보육이고 이웃집인 경우
  bool isNuresery() {
    return getServiceType() == ServiceType.serviceType_2 && getAreaType() == PossibleArea.link_mom;
  }

  ///돌봄계약서 작성시 링크쌤이 계약시 주소 입력
  Widget signView(SignType type, String title) {
    signType = type;
    bool isStartSelect = StringUtils.validateString(data.jobItem.temp_area_start.temp_areaname);
    if (!isShowDlg) {
      //해당 링크쌤집 주소로 이동하기!
      WidgetsBinding.instance.addPostFrameCallback((_) {
        isShowDlg = true;
        showNormalDlg(
            message: "링크쌤집주소입력_안내".tr(),
            onClickAction: (action) {
              RenderBox? box = data.formSign.currentContext!.findRenderObject() as RenderBox?;
              Size sizeTime = box!.size;
              // log.d('『GGUMBI』>>> signView : sizeTime: $sizeTime, ${sizeTime.height}, ${box} <<< ');
              setState(() {
                final renderPos = box.localToGlobal(Offset.zero);
                // log.d('『GGUMBI』>>> signView : renderPos: $renderPos,  <<< ');
                final RenderAbstractViewport? viewport = RenderAbstractViewport.of(data.formSign.currentContext!.findRenderObject());
                final double vpHeight = viewport!.paintBounds.height;
                // log.d('『GGUMBI』>>> signView : vpHeight: $vpHeight, ${viewport.paintBounds.bottom}  <<< ');
                // log.d('『GGUMBI』>>> signView : {}: ${renderPos.dy - vpHeight + sizeTime.height},  <<< ');
                _controller!.jumpTo((renderPos.dy - vpHeight + sizeTime.height));
              });
            });
      });
    }

    //링크쌤이고, 서명해야될 경우
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              flex: flex,
              child: lText(
                isStartSelect ? "장소변경".tr() : "장소선택".tr(),
                style: st_b_16(fontWeight: FontWeight.bold),
              ),
            ),
            Expanded(
              flex: flex2,
              child: lBtnWrap(
                isStartSelect ? "장소변경".tr() : "장소선택".tr(),
                alignment: Alignment.centerLeft,
                height: 25,
                padding: padding_10_LR,
                textStyle: st_b_14(textColor: color_999999, fontWeight: FontWeight.w500),
                btnEnableColor: color_eeeeee.withOpacity(0.42),
                sideEnableColor: color_dedede.withOpacity(0.42),
                onClickAction: () async {
                  var result = await Commons.page(context, routeTempAreaView, arguments: TempAreaViewPage(viewMode: ViewMode(viewType: ViewType.apply), tempAreaData: data.jobItem.temp_area_end));
                  if (result != null && result is TempAreaResultData) {
                    var _data = result.data as TempAreaData;
                    data.jobItem.temp_area_start = _data;
                    data.tcSign.text = _data.temp_address;
                    data.tcSignDetail.text = _data.temp_address_detail;
                  }
                  onConfirmBtn();
                },
              ),
            ),
          ],
        ),
        Form(
          key: data.formSign,
          child: TextFormField(
            textAlignVertical: TextAlignVertical.center,
            controller: data.tcSign,
            style: stLogin,
            readOnly: true,
            validator: isStartEnable(getServiceType(), getAreaType()) ? StringUtils.validateAddress : null,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.emailAddress,
            decoration: underLineDecoration(
              "링크쌤집주소를입력해주세요".tr(),
              hintStyle: st_b_16(textColor: Commons.getColor() /*, fontWeight: FontWeight.bold*/),
              icon: Lcons.search(disableColor: color_222222),
              focus: true,
              focusColor: Commons.getColor(),
              counterText: StringUtils.validateString(data.tcSign.text) ? null : ' ',
            ),
            onTap: () async {
              Documents resultData = await Commons.page(context, routeAddressSearch);
              data.tcSign.text = resultData.getAddress();
              data.tcSignDetail.text = '';
              onConfirmBtn();
            },
            onChanged: (value) {},
            onEditingComplete: () {
              focusClear(context);
              onConfirmBtn();
            },
          ),
        ),
        //주소 상세
        if (StringUtils.validateString(data.tcSign.text))
          Padding(
            padding: padding_30_B,
            child: Form(
              key: data.formSignDetail,
              child: TextFormField(
                controller: data.tcSignDetail,
                style: stLogin,
                validator: isStartEnable(getServiceType(), getAreaType()) ? StringUtils.validateDetailAddress : null,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.emailAddress,
                decoration: underLineDecoration(
                  "장소_상세_힌트".tr(),
                  hintStyle: st_b_16(textColor: color_b2b2b2),
                  counterText: null,
                  focus: true,
                  focusColor: Commons.getColor(),
                ),
                onChanged: (value) {
                  onConfirmBtn();
                },
                onEditingComplete: () {
                  data.formSignDetail.currentState!.validate();
                  focusClear(context);
                  onConfirmBtn();
                },
              ),
            ),
          ),
      ],
    );
  }

  ///신청금액, 10분당 금액 업데이트
  _upDatePay(int costSum, int costAvg) {
    getCareViewData().cost_sum = costSum;
    getCareViewData().cost_avg = costAvg;
  }

  ///이동수단 업데이트(도보/자동차/대중교통)
  _upDateMove() {
    if (data.jobItem.reqdata.bookingafterschool!.isNotEmpty || data.jobItem.reqdata.bookinggotoschool!.isNotEmpty) {
      //이동수단
      ReqSchoolToHomeData item = ReqSchoolToHomeData();
      //등원 데이터
      if (getServiceType() == ServiceType.serviceType_0) {
        item = data.jobItem.reqdata.bookinggotoschool!.first;
        //하원 데이터
      } else if (getServiceType() == ServiceType.serviceType_1) {
        item = data.jobItem.reqdata.bookingafterschool!.first;
      }

      getCareViewData().careschool!.vehicle_id!.clear();
      getCareViewData().careschool!.vehicle_id!.addAll(item.product_id!);

      //출발장소
      getCareViewData().careschool!.start_time = item.start_time;
      getCareViewData().careschool!.start_address = item.start_address + ' ' + item.start_address_detail;
      getCareViewData().careschool!.start_comment = item.start_comment;
      //도착장소
      getCareViewData().careschool!.end_time = item.end_time;
      getCareViewData().careschool!.end_address = item.end_address + ' ' + item.end_address_detail;
      getCareViewData().careschool!.end_comment = item.end_comment;
      //경유지
      getCareViewData().careschool!.pathroute_comment = item.pathroute_comment;
      //원하는 링크쌤집 위치
      getCareViewData().careschool!.nhn_area_1_address = item.nhn_area_1_address;
      getCareViewData().careschool!.nhn_area_2_address = item.nhn_area_2_address;

      //보육일 경우
    } else if (data.jobItem.reqdata.bookingboyuk!.isNotEmpty) {
      ReqBoyukData item = data.jobItem.reqdata.bookingboyuk!.first;
      //출발장소
      getCareViewData().careboyuk!.boyuk_address = item.boyuk_address + ' ' + item.boyuk_address_detail;
      getCareViewData().careboyuk!.nhn_comment = item.boyuk_comment;
      getCareViewData().careboyuk!.pathroute_comment = item.pathroute_comment;
      getCareViewData().careboyuk!.nhn_area_1_address = item.nhn_area_1_address;
      getCareViewData().careboyuk!.nhn_area_2_address = item.nhn_area_2_address;
    }
  }

  ///필요한 돌봄 업데이트
  _upDateOption() {
    getCareViewData().careoptions!.bookingboyukoptions_product!.clear();
    data.jobItem.reqdata.bookingboyukoptions!.forEach((e) {
      getCareViewData().careoptions!.bookingboyukoptions_product!.add(e.product_id);
    });

    getCareViewData().careoptions!.bookinghomecareoptions_product!.clear();
    data.jobItem.reqdata.bookinghomecareoptions!.forEach((e) {
      getCareViewData().careoptions!.bookinghomecareoptions_product!.add(e.product_id);
    });

    getCareViewData().careoptions!.bookingplayoptions_product!.clear();
    data.jobItem.reqdata.bookingplayoptions!.forEach((e) {
      getCareViewData().careoptions!.bookingplayoptions_product!.add(e.product_id);
    });
  }

  ///신청서 정보
  bool isModify() {
    try {
      return beforeData.toString() == afterData.toString();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return true;
  }

  ///아이 정보
  bool isModifyChild() {
    try {
      return beforeChildData.toString() == afterChildData.toString();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return true;
  }
}
