import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/care_view_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../../../../main.dart';

///나의정보 뷰(맘대디)
Widget momDadyMyInfoView(BuildContext context, {CareViewData? data, String pay = '', ViewMode? viewMode, Widget? content, Function? callBack}) {
  double height = heightFull(context) * 0.5;
  return Stack(
    children: [
      //자기소개영역
      Container(
        color: Commons.getColor(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.centerLeft,
              width: widthFull(context) * 0.7,
              color: Commons.getColor(),
              margin: padding_10_TB,
              child: Padding(
                padding: padding_20,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        Lcons.won(color: Colors.white, isEnabled: true),
                        sb_w_05,
                        lText("돌봄예상금액".tr(), style: st_16(fontWeight: FontWeight.w500)),
                      ],
                    ),
                    sb_h_05,
                    Row(
                      children: [
                        lText(pay, style: st_22(fontWeight: FontWeight.bold)),
                        sb_w_05,
                        if (data!.is_negotiable)
                          lText(
                            '(${"협의가능".tr()})',
                            style: st_16(fontWeight: FontWeight.w500),
                          ),
                      ],
                    ),
                    sb_h_05,
                    Row(
                      children: [
                        lText(
                          '${"10분당환산금액".tr()}',
                          style: st_14(),
                        ),
                        sb_w_05,
                        lText(
                          '${StringUtils.formatPay(data.cost_avg)}${"원".tr()}',
                          style: st_14(fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                color: color_white,
                border: Border.all(color: color_white, width: 0),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16.0),
                  topRight: Radius.circular(16.0),
                ),
              ),
              child: Padding(
                padding: padding_20,
                child: content,
              ),
            ),
          ],
        ),
      ),

      if (viewMode!.viewType != ViewType.modify && viewMode.itemType != ItemType.contract && data.userinfo!.user_id != auth.user.id) //내가 등록한 신청서가 아닌 경우에만 처리
        lInkWell(
          onTap: () {
            if (callBack != null) {
              callBack(DialogAction.follower);
            }
          },
          child: Padding(
            padding: padding_20,
            child: Align(
              alignment: Alignment.topRight,
              child: Lcons.scrap(isEnabled: data.userinfo!.is_follower, size: 24, disableColor: Colors.white),
            ),
          ),
        ),

      if (viewMode.viewType == ViewType.modify)
        lInkWell(
          onTap: () {
            if (callBack != null) {
              callBack(DialogAction.modify);
            }
          },
          child: Padding(
            padding: padding_20,
            child: Align(
              alignment: Alignment.topRight,
              child: lText(
                "수정".tr(),
                style: st_15(textColor: color_white),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),

      Positioned.fill(
        top: (height / 5),
        child: Align(
          alignment: Alignment.topRight,
          child: Padding(
            padding: padding_20_LRB,
            child: InkWell(
                onTap: () {
                  // Commons.nextPage(context,
                  //     fn: () => FullPhotoPage(
                  //           url: data.userinfo!.profileimg,
                  //           name: "맘대디프로필".tr(),
                  //           fit: BoxFit.contain,
                  //         ));
                },
                child: lProfileAvatar(data.userinfo!.profileimg, radius: 55)),
          ),
        ),
      ),
    ],
  );
}
