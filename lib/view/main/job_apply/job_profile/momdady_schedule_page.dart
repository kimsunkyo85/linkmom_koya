import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/main/job_apply/job_profile/schedule_view.dart';


class MomdadySchedulePage extends BaseStateful {
  final List<String> data;
  final String sTime;
  final String eTime;

  MomdadySchedulePage({required this.data, this.sTime = '', this.eTime = ''});

  @override
  _MomdadySchedulePageState createState() => _MomdadySchedulePageState();
}

class _MomdadySchedulePageState extends BaseStatefulState<MomdadySchedulePage> {
  Map<DateTime, List<int>> _dates = {};
  Map<ScheduleData, List<DateTimeRange>> _detail = {};

  @override
  void initState() {
    super.initState();
    if (widget.data.isNotEmpty) {
      widget.data.forEach((e) {
        _dates.putIfAbsent(StringUtils.parseYMD(e), () => [0]);
      });
      DateTimeRange range = DateTimeRange(start: StringUtils.parseYMD(widget.data.first), end: StringUtils.parseYMD(widget.data.last));
      ScheduleData scheduleData = ScheduleData(range, widget.data.length);
      _detail.update(scheduleData, (v) {
        v.add(DateTimeRange(start: StringUtils.parseHD(widget.sTime), end: StringUtils.parseHD(widget.eTime)));
        return v;
      }, ifAbsent: () {
        return [DateTimeRange(start: StringUtils.parseHD(widget.sTime), end: StringUtils.parseHD(widget.eTime))];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        appBar: appBar("돌봄날짜시간".tr(), hide: false),
        body: Column(
          children: [
            Expanded(
                child: ScheduleView(
              schedule: _dates,
              scheduleDetail: _detail,
            )),
            lBtn("확인".tr(), btnColor: Commons.getColor(), onClickAction: () => Commons.pagePop(context)),
          ],
        ));
  }
}
