import 'package:flutter/material.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

///아이정보
Widget childCharacterView(String data) {
  return Chip(
      backgroundColor: color_white,
      side: BorderSide(width: 1.0, color: color_cecece),
      label: lText(
        data,
        style: st_13(textColor: color_545454, fontWeight: FontWeight.w500),
      ));
}

///해시태그
Widget hashTagView(String data, {Color textColor = color_545454, Color bgColor = color_white}) {
  return Chip(
      backgroundColor: bgColor,
      side: BorderSide(width: 1, color: textColor),
      shape: lRoundedRectangleBorder(width: 0, borderRadius: 16),
      label: lText(
        '#$data',
        style: st_13(textColor: textColor, fontWeight: FontWeight.bold),
      ));
  /*Chip(
      backgroundColor: color_white,
      side: BorderSide(width: 1.0, color: color_cecece),
      shape: lRoundedRectangleBorder(borderRadius: 8),
      label: lText(
        data,
        style: st_13(textColor: color_545454, fontWeight: FontWeight.w500),
      ));*/
}

///해시태그
Widget hashTagActionView(
  SingleItem data, {
  enableColor = color_main,
  disableColor = color_b2b2b2,
  borderRadius = 26.0,
  padding = padding_05,
  Function? onClick,
}) {
  return Container(
    margin: padding_10_B,
    child: ActionChip(
        backgroundColor: color_white,
        shape: lRoundedRectangleBorder(isSelected: data.isSelected, width: data.isSelected ? 2.0 : 1.0, borderRadius: borderRadius, enBorderColor: enableColor),
        onPressed: () {
          data.isSelected = !data.isSelected;
          if (onClick != null) {
            onClick();
          }
        },
        label: Padding(
          padding: padding,
          child: lText(
            data.name,
            style: st_b_16(fontWeight: FontWeight.w500, textColor: data.isSelected ? enableColor : disableColor),
          ),
        )),
  );
}
