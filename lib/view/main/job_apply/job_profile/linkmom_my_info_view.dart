import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/data/job_myinfo_data.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../../../../main.dart';

///나의정보 뷰(링크쌤)
Widget linkMomMyInfoView(BuildContext context, {JobMyInfoData? data, pay, ViewType viewType = ViewType.modify, Widget? content, Function? callBack}) {
  double height = heightFull(context) * 0.5;
  //내 지원서 인지 여부 판단후, 스크랩 및 하단 채팅(저장)하기 버튼 처리하기
  bool isMine = viewType == ViewType.modify && auth.user.id == data!.userinfo!.user_id;
  bool isFollower = auth.user.id == data!.userinfo!.user_id;
  bool _bookingState = data.getStatus().status;

  return Stack(
    children: [
      //자기소개영역
      Container(
        color: _bookingState ? Commons.getColorViewType(viewType) : color_b2b2b2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.centerLeft,
              width: widthFull(context) * 0.7,
              color: _bookingState ? Commons.getColorViewType(viewType) : color_b2b2b2,
              margin: padding_10_TB,
              child: Padding(
                padding: padding_20,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        Lcons.won(color: Colors.white, isEnabled: true),
                        sb_w_05,
                        lText("희망시급".tr(), style: st_16(fontWeight: FontWeight.w500)),
                      ],
                    ),
                    sb_h_05,
                    lText(pay, style: st_22(fontWeight: FontWeight.bold)),
                    sb_h_05,
                    if (data.introduce.isNotEmpty)
                      lAutoSizeText(
                        '“${data.introduce}”',
                        style: st_14(),
                        textAlign: TextAlign.left,
                        maxLines: 2,
                      ),
                  ],
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                color: color_white,
                border: Border.all(color: color_white, width: 0),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16.0),
                  topRight: Radius.circular(16.0),
                ),
              ),
              child: Padding(
                padding: padding_20,
                child: content,
              ),
            ),
          ],
        ),
      ),

      if (!isFollower) //내가 등록한 신청서가 아닌 경우에만 처리
        InkWell(
          onTap: () {
            if (callBack != null) {
              callBack(DialogAction.follower);
            }
          },
          child: Padding(
            padding: padding_20,
            child: Align(
              alignment: Alignment.topRight,
              child: Lcons.scrap(isEnabled: data.userinfo!.is_follower, size: 24, disableColor: Colors.white),
            ),
          ),
        ),

      if (isMine)
        Padding(
          padding: EdgeInsets.fromLTRB(0, 20, 10, 0),
          child: Align(
            alignment: Alignment.topRight,
            child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
              lText(_bookingState ? BookingLinkMomStatus.inprogress.name : BookingLinkMomStatus.end.name, style: st_13(fontWeight: FontWeight.w700)),
              SizedBox(
                width: 65,
                height: 30,
                child: Switch(
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  value: !_bookingState,
                  onChanged: (value) {
                    if (callBack != null) {
                      callBack(DialogAction.mode);
                    }
                  },
                  activeTrackColor: color_eeeeee,
                  inactiveTrackColor: color_eeeeee,
                  activeColor: color_b2b2b2,
                  inactiveThumbColor: color_d8afe5,
                ),
              )
            ]),
          ),
        ),

      Positioned.fill(
        top: (height / 5),
        child: Align(
          alignment: Alignment.topRight,
          child: Padding(
            padding: padding_20_LRB,
            child: InkWell(
                onTap: () {
                  /* Commons.nextPage(
                    context,
                    fn: () => FullPhotoPage(url: data.userinfo!.profileimg, name: "링크쌤프로필".tr() */ /*data.userinfo.first_name*/ /*),
                  );*/
                },
                child: lProfileAvatar(data.userinfo!.profileimg, radius: 55)),
          ),
        ),
      ),
    ],
  );
}
