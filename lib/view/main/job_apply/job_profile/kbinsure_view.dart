import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/svg_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

///KB 돌봄 보험 안내
Widget kbInsureView(BuildContext context) {
  return Container(
    margin: padding_20_T,
    padding: EdgeInsets.fromLTRB(20, 24, 20, 24),
    width: widthFull(context),
    color: color_eeeeee.withOpacity(0.42),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: padding_10_B,
          child: SvgPicture.asset(SVG_KB_INSURE, width: 120),
        ),
        lText("배상책임보험_가입".tr(), style: st_b_14(fontWeight: FontWeight.w700)),
        sb_h_05,
        lText("배상책임보험_가입_안내".tr(), style: st_14(textColor: color_545454)),
      ],
    ),
  );
}
