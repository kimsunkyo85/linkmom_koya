import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

///아이정보 (홍OO | 9세 | 여아 )
Widget listViewChildInfoView({
  String? name,
  String? age,
  String? gender,
  TextStyle? style,
  color: color_545454,
  fontWeight: FontWeight.normal,
  MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
  Function? callBack,
}) {
  if (style == null) {
    style = st_15();
  }
  return Row(
    mainAxisAlignment: mainAxisAlignment,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      if (StringUtils.validateString(name)) lText(name!, style: defaultStyle(color: color, fontWeight: fontWeight, fontSize: style.fontSize ?? 15)),
      if (StringUtils.validateString(age)) Container(height: 15, child: lDividerVertical(color: color_dedede, thickness: 1, padding: padding_05_LR)),
      if (StringUtils.validateString(age)) lText(age!, style: defaultStyle(color: color, fontWeight: fontWeight, fontSize: style.fontSize ?? 15)),
      if (StringUtils.validateString(gender)) Container(height: 15, child: lDividerVertical(color: color_dedede, thickness: 1, padding: padding_05_LR)),
      if (StringUtils.validateString(gender)) lText(gender!, style: defaultStyle(color: color, fontWeight: fontWeight, fontSize: style.fontSize ?? 15)),
    ],
  );
}
