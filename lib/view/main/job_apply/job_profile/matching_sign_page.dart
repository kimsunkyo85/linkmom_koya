import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cs_guide_request.dart';
import 'package:linkmom/data/network/models/cs_terms_view_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:signature/signature.dart';

class MatchingSignPage extends BaseStateful {
  final Function callback;
  final USER_TYPE type;
  MatchingSignPage({required this.callback, this.type = USER_TYPE.mom_daddy});

  @override
  _MatchingSignPageState createState() => _MatchingSignPageState();
}

class _MatchingSignPageState extends BaseStatefulState<MatchingSignPage> {
  late SignatureController _signCtrl;
  late Uint8List _signData = Uint8List(0);
  TermsViewData _guide = TermsViewData();

  @override
  void initState() {
    super.initState();
    _signCtrl = SignatureController(
      penStrokeWidth: 2,
      penColor: Colors.black,
      exportBackgroundColor: Colors.transparent,
      onDrawEnd: () => onUpDate(),
    );
    _reqeustCsUserGuide(widget.type);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
          appBar: appBar("돌봄계약서".tr(), hide: false),
          body: Column(
            children: [
              Expanded(
                child: lScrollView(padding: padding_0, child: Html(data: _guide.content)),
              ),
              Container(
                decoration: BoxDecoration(border: Border(top: BorderSide(color: color_dedede))),
                child: Column(
                  children: [
                    Stack(
                      alignment: Alignment.center,
                      children: [
                        lText("계약서동의_안내".tr(), style: st_16(textColor: color_cecece)),
                        Signature(
                          backgroundColor: Colors.transparent,
                          width: data.width(context, 1),
                          height: data.height(context, 0.25),
                          controller: _signCtrl,
                        ),
                      ],
                    ),
                    Container(
                        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                      lInkWell(
                        onTap: () {
                          _signCtrl.clear();
                          _signData = Uint8List(0);
                          onUpDate();
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: 60,
                          width: data.width(context, 0.5),
                          decoration: BoxDecoration(border: Border(top: BorderSide(color: color_dedede))),
                          child: lText("다시하기".tr(), style: st_16(textColor: color_545454, fontWeight: FontWeight.w500)),
                        ),
                      ),
                      lInkWell(
                        onTap: () async {
                          _signData = (await _signCtrl.toPngBytes()) ?? Uint8List(0);
                          if (_signData.isNotEmpty) await _trySign(context);
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: 60,
                          width: data.width(context, 0.5),
                          decoration: BoxDecoration(border: Border(top: BorderSide(color: color_dedede), left: BorderSide(color: color_dedede))),
                          child: lText("계약서동의".tr(), style: st_16(textColor: _signCtrl.isNotEmpty ? Commons.getColor() : color_c4c4c4, fontWeight: FontWeight.w500)),
                        ),
                      ),
                    ])),
                  ],
                ),
              ),
            ],
          )),
    );
  }

  Future<void> _trySign(BuildContext context) async {
    final imageFile = await Commons.bytesToImage(_signData, 'signature_${StringUtils.formatYMD(DateTime.now())}', "png");
    Commons.pagePop(context, data: imageFile);
  }

  void _reqeustCsUserGuide(USER_TYPE type) {
    try {
      CsGuide gubun = CsGuide.reserveMomdady;
      if (type == USER_TYPE.link_mom) gubun = CsGuide.reserveLinkmom;
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.reqeustCsUserGuideView(CsGuideRequest(gubun: gubun.index)).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _guide = response.getData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        log.e('_requestCsGuide >>>>>>>>>>>> $e');
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }
}
