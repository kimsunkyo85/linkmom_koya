import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/calendar/calendar_view.dart';
import 'package:linkmom/view/lcons.dart';

class ScheduleView extends StatefulWidget {
  final Map<DateTime, List<int>> schedule;
  final Map<ScheduleData, List<DateTimeRange>> scheduleDetail;
  ScheduleView({required this.schedule, required this.scheduleDetail});

  @override
  _ScheduleViewState createState() => _ScheduleViewState();
}

class _ScheduleViewState extends State<ScheduleView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return lScrollView(
      child: Column(
        children: [
          Container(
            child: CalendarView(
              selected: (date, events) => true,
              events: widget.schedule,
              colors: colorList,
              holidays: {},
            ),
          ),
          Container(
              child: Column(
                  children: widget.scheduleDetail.entries
                      .map((schedule) => Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  padding: EdgeInsets.fromLTRB(0, 20, 0, 15),
                                  child: Row(children: [
                                    Container(padding: padding_06_R, child: Lcons.calendar(size: 23)),
                                    lText(
                                      '${StringUtils.formatLocalMD(schedule.key.range.start)} - ${StringUtils.formatLocalMD(schedule.key.range.end)} (${"스케줄".tr()} ${schedule.key.dataLength}${"일".tr()})',
                                      style: st_b_16(fontWeight: FontWeight.w700),
                                    ),
                                  ])),
                              Container(
                                padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                                decoration: BoxDecoration(
                                  border: Border.all(color: color_dedede),
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                child: Column(
                                    children: schedule.value
                                        .map((t) => Container(
                                            padding: padding_10_B,
                                            child: Row(children: [
                                              lDot(color: colorList[widget.scheduleDetail.keys.toList().indexOf(schedule.key)], size: 12),
                                              sb_w_10,
                                              lText('${StringUtils.formatHD(t.start)} ~ ${StringUtils.formatHD(t.end)}', style: st_b_16(fontWeight: FontWeight.w500)),
                                              sb_w_05,
                                              lText('(${StringUtils.differenceDateTime([t.start, t.end])})', style: st_14(textColor: color_545454))
                                            ])))
                                        .toList()),
                              )
                            ],
                          ))
                      .toList())),
        ],
      ),
    );
  }
}

class ScheduleData {
  DateTimeRange range;
  int dataLength;

  ScheduleData(this.range, this.dataLength);
}
