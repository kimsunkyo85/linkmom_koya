import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/child_info_response.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/utils/listview/list_child_info_select.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../../../main.dart';

///진행상황, 아이정보 뷰 돌봄신청 컨텐츠 제목(필요한 돌봄 유형을 선택해주세요), Step 화면
Widget childInfoView(ChildInfo data, {EdgeInsets margin = stepMargin, EdgeInsetsGeometry padding = padding_10_LTR}) {
  ChildInfoItemData itemData = ChildInfoItemData(childCharacters: [], childData: ChildInfoData());
  log.d('『GGUMBI』>>> childInfoView : data: $data,  <<< ');
  try {
    String step = "${"step".tr()} ${data.step}";
    String name = data.name!;
    String age = '${data.age}${"세".tr()}(${itemData.lsNursery[data.nursery!]})';
    String gender = '${itemData.lsGender[data.gender! - 1]}';
    log.d('『GGUMBI』>>> childInfoView ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
    return Column(
      children: [
        if (data.isShow) lBtnStep(step, margin: margin),
        if (!data.isShow) sb_h_20,
        if (data.isShowChild)
          Container(
            alignment: Alignment.center,
            padding: padding,
            decoration: decorationChildInfo(),
            child: IntrinsicHeight(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  lChildInfoText(
                    name,
                  ),
                  Container(
                    margin: padding_03_TB,
                    child: lDividerVertical(),
                  ),
                  lChildInfoText(age),
                  Container(
                    margin: padding_03_TB,
                    child: lDividerVertical(),
                  ),
                  Row(children: [
                    if (data.gender == 1) Lcons.boy(),
                    if (data.gender == 2) Lcons.girl(),
                    sb_w_05,
                    lChildInfoText(gender),
                  ]),
                ],
              ),
            ),
          ),
        if (data.isShowChild) sb_h_17,
      ],
    );
  } catch (e) {
    return lText("");
  }
}

///신청내역 아이정보 뷰
Widget childInfoView2(ChildInfo data, {EdgeInsetsGeometry margin = stepMargin, EdgeInsetsGeometry padding = padding_10}) {
  ChildInfoItemData itemData = ChildInfoItemData(childCharacters: [], childData: ChildInfoData());
  log.d('『GGUMBI』>>> childInfoView : data: $data,  <<< ');
  // log.d('『GGUMBI』>>> childInfoView : data: $itemData,  <<< ');
  if (data.name == null || data.age == null || data.nursery == null || data.gender == null) {
    return lText('');
  }
  String name = data.name!;
  String age = '${data.age}${"세".tr()}(${itemData.lsNursery[data.nursery!]})';
  String gender = '${itemData.lsGender[data.gender! - 1]}';
  log.d('『GGUMBI』>>> childInfoView ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
  return Row(
    mainAxisSize: MainAxisSize.max,
    children: [
      Lcons.getGender(data.gender!, size: 40),
      sb_w_10,
      lAutoSizeText(
        name,
        style: stAppBarTitle,
        textAlign: TextAlign.start,
        maxLines: 1,
        minFontSize: 5,
      ),
      sb_w_10,
      lChildInfoText(
        age,
      ),
      sb_w_05,
      lChildInfoText(
        gender,
      ),
    ],
  );
}
