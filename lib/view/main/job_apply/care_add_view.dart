import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

///추가돌봄 추가
Widget careAddView({CareAddData? data, Function? onClick}) {
  if (data == null) {
    data = CareAddData();
  }
  return Column(
    // crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Row(
        children: [
          Expanded(
            child: lAutoSizeText(
              data.content!,
              style: st_16(isSelect: !data.isAdd, textColor: color_767676),
              textAlign: TextAlign.start,
              maxLines: 1,
              minFontSize: 5,
            ),
          ),
          lInkWell(
            onTap: data.isAdd
                ? () {
                    if (onClick != null) {
                      onClick();
                    }
                  }
                : null,
            child: Text(
              "추가".tr(),
              style: st_b_15(isSelect: !data.isAdd, textColor: color_main, disableColor: color_aaaaaa),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    ],
  );
}

class CareAddData {
  CareAddData({this.content, this.data, this.isAdd = true});

  String? content;
  List<CareAddDataItem>? data;
  bool isAdd;
}

class CareAddDataItem {
  CareAddDataItem({this.name, this.info});

  String? name;
  String? info;

  @override
  String toString() {
    return 'CareAddData{name: $name, info: $info}';
  }
}
