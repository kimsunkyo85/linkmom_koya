import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../../../main.dart';

///제목,아이콘,체크박스, 드럽다운
Widget titleToggleIconView(
  DataManager? data,
  title, {
  var titleStyle,
  String? value,
  String? selectValue,
  int price = 9000,
  /*Color color = color_767676, */
  FontWeight fontWeight = FontWeight.normal,
  TextAlign? textAlign,
  int maxLine = 1,
  String? value2,
  FontWeight fontWeight2 = FontWeight.normal,
  TextAlign textAlign2 = TextAlign.left,
  int maxLine2 = 1,
  bool isInfo = true,
  bool isToggle = false,
  bool isSelect = false,
  flex = 2,
  flex2 = 3,
  Widget? rightWidget,
  Color infoColor = color_main,
  Function? onClick,
}) {
  log.d('『GGUMBI』>>> titleToggleIconView : isSelect: $isSelect,  <<< ');
  return Column(
    crossAxisAlignment: CrossAxisAlignment.end,
    children: [
      Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: flex,
            child: Row(
              children: [
                lText(
                  title,
                  style: titleStyle == null ? st_b_18(fontWeight: FontWeight.bold) : titleStyle,
                ),
                sb_w_05,
                if (isInfo)
                  lInkWell(
                      onTap: () {
                        onClick!(DialogAction.info);
                      },
                      child: Lcons.info(color: infoColor, isEnabled: true, size: 25),
                  ),
              ],
            ),
          ),
          if (flex != 0) sb_w_10,
          Expanded(
              flex: flex2,
              child: rightWidget == null
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        sb_w_10,
                        Expanded(
                          child: lInkWell(
                            /*  onTap: isSelect
                          ? () {
                              onClick(DialogAction.select);
                            }
                          : null,*/
                            onTap: () {
                              onClick!(DialogAction.select);
                            },
                            child: lRoundContainer(
                              // bgColor: isSelect ? color_main : color_f8f8fa,
                              bgColor: color_white,
                              padding: padding_12,
                              borderColor: isSelect ? color_main : color_b2b2b2,
                              // borderWidth: isSelect ? 2 : 1,
                              borderWidth: 1,
                              borderRadius: 5.0,
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: padding_15_L,
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: lAutoSizeText(
                                        StringUtils.validateString(selectValue) ? selectValue ?? '' : '${StringUtils.formatPay(price)}${"원".tr()}',
                                        // style: st_15(textColor: isSelect ? color_white : color_aaaaaa),
                                        style: st_b_16(fontWeight: FontWeight.w500),
                                        textAlign: TextAlign.left,
                                        maxLines: 1,
                                      ),
                                    ),
                                    Lcons.nav_bottom(color: color_b2b2b2
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  : rightWidget),
        ],
      ),
      if (StringUtils.validateString(value)) sb_h_10,
      if (StringUtils.validateString(value))
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            sb_w_15,
            Expanded(
              flex: 2,
              child: lText(''),
            ),
            Expanded(
              flex: 3,
              child: Row(
                children: [
                  sb_w_10,
                  Expanded(
                    child: StringUtils.validateString(value2)
                        ? Row(
                            children: [
                              lText('${"10분당".tr()} ', style: st_14(textColor: color_999999)),
                              Expanded(
                                  child: lAutoSizeText(
                                '$value2${"원".tr()}',
                                textAlign: textAlign2,
                                maxLines: maxLine2,
                                minFontSize: 5,
                                style: st_14(textColor: color_999999),
                              )),
                            ],
                          )
                        : lText(''),
                  ),
                  InkWell(
                    onTap: () {
                      onClick!(DialogAction.toggle);
                    },
                    child: Row(
                      children: [
                        Lcons.check_circle(isEnabled: isToggle, color: Commons.getColor(), disableColor: color_dedede),
                        sb_w_05,
                        lText(value!),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
    ],
  );
}
