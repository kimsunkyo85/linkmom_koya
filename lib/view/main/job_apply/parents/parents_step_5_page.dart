import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/addr_search_response.dart';
import 'package:linkmom/data/network/models/req_data.dart';
import 'package:linkmom/data/network/models/temp_area_response.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/data/storage/model/menu_file_data.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/listview/list_movetype_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/job_apply/move_type_view.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../child_info_view.dart';
import '../content_view.dart';
import '../place_view.dart';
import 'parents_step_6_page.dart';

class ParentsStep5Page extends BaseStateful {
  @override
  _ParentsStep5PageState createState() => _ParentsStep5PageState();

  ParentsStep5Page({this.data, this.viewMode});

  final DataManager? data;
  final ViewMode? viewMode;
}

class _ParentsStep5PageState extends BaseStatefulState<ParentsStep5Page> {
  SingleItem _moveTypeItem = SingleItem('');
  String time = '00';
  int b_minutes = 0;
  ReqData? tempReqData;

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data!);
    _requestTempAreaView();
  }

  @override
  void onData(DataManager _data) {
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 5;

    ///수정일때는 STEP을 보여주지 않는다.
    if (getViewType() == ViewType.modify) {
      data.jobItem.childInfo.isShow = false;
      data.jobItem.calcuTime = _data.jobItem.reqdata.careschedule!.isNotEmpty ? _data.jobItem.reqdata.careschedule!.first.durationtime : 0;
      data.jobItem.request = _data.jobItem.request.clone();
      data.jobItem.reqdata = _data.jobItem.reqdata.clone();
    } else {
      data.jobItem.calcuTime = _data.jobItem.calcuTime;
      data.jobItem.request = _data.jobItem.request;
      data.jobItem.reqdata = _data.jobItem.reqdata;
    }

    data.jobItem.jobData = _data.jobItem.jobData;
    data.jobItem.tempResponse = _data.jobItem.tempResponse;
    tempReqData = data.jobItem.tempResponse.getData().reqdata;
    data.jobItem.totalTime = _data.jobItem.totalTime;
    data.jobItem.scheduleLength = _data.jobItem.scheduleLength;
    time = StringUtils.formatCalcuTime(data.jobItem.calcuTime, b_minutes);
    log.d('『GGUMBI』>>> onConfirmBtn : time: $time,  <<< ');

    _setMakeData();
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.totalTime: ${data.jobItem.totalTime}, ${data.jobItem.calcuTime}  <<< ');
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.request:  ${data.jobItem.reqdata},  <<< ');
    log.d('『GGUMBI』>>> onData : data.jobItem.jobData: ${data.jobItem.jobData},  <<< ');
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.tempResponse: ${data.jobItem.tempResponse},  <<< ');
    onDataPage(_data);
  }

  ///도보,자동차,대중교통 아이템 데이터 만들기
  _setMakeData() {
    var transportsData = data.jobItem.jobData![ServicesData.Key_transports];
    log.d('transportsData $transportsData,  <<< ');
    List<ServiceItem> serviceDatas = data.jobItem.jobData![ServicesData.Key_transports] as List<ServiceItem>;
    // DateTime currentTime = DateTime.now();
    // //등원
    // if (getServiceType() == ServiceType.serviceType_0) {
    //   data.jobItem.startTime = DateTime(currentTime.year, currentTime.month, currentTime.day, 9, 0);
    //   data.jobItem.endTime = DateTime(currentTime.year, currentTime.month, currentTime.day, 10, 0);
    //   //하원
    // } else if (getServiceType() == ServiceType.serviceType_1) {
    //   data.jobItem.startTime = DateTime(currentTime.year, currentTime.month, currentTime.day, 15, 0);
    //   data.jobItem.endTime = DateTime(currentTime.year, currentTime.month, currentTime.day, 16, 0);
    // }

    data.jobItem.startTime = StringUtils.getTimeToDateTime(data.jobItem.reqdata.careschedule!.first.stime);
    data.jobItem.endTime = data.jobItem.startTime.add(Duration(minutes: 30));
    // data.jobItem.endTime = StringUtils.getTimeToDateTime(data.jobItem.reqdata.careschedule!.first.etime);

    ///도보 - 1, 자동차 - 2, 대중교통 - 3
    data.jobItem.lsJobMoveType = Commons.makeTransports(serviceDatas);
  }

  @override
  Widget build(BuildContext context) {
    log.d('『GGUMBI』>>> build : _item: $_moveTypeItem,  <<< ');
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        resizeToAvoidBottomInset: true,
        context: context,
        appBar: jobAppBar(
          isBoyuk(getServiceType()) ? "돌봄주소".tr() : "이동방법".tr(),
          // isBack: getViewType() == ViewType.modify ? false : true,
          isClose: getViewType() == ViewType.apply ? true : false,
          hide: getViewType() == ViewType.modify ? false : true,
          onClick: () {
            if (getViewType() == ViewType.modify) {
              Commons.pagePop(context);
            } else {
              //스케줄 데이터 값 저장
              // data.jobItem.reqData.careschedule.addAll(data.jobItem.careschedule);
              Commons.showSaveTempDlg(data: data);
            }
          },
        ),
        body: lContainer(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step),
              Expanded(
                child: Column(
                  children: [
                    Expanded(
                      child: lScrollView(
                        padding: padding_Item_TB,
                        child: Column(children: [
                          Padding(
                            padding: padding_20_LR,
                            child: Column(
                              children: [
                                childInfoView(data.jobItem.childInfo),
                                InkWell(
                                    onTap: () {
                                      if (!Commons.isDebugMode) return;
                                      // TODO: GGUMBI 4/29/21 - 디버그용
                                      _setMoveType(1);

                                      if (isBoyuk(getServiceType())) {
                                        if (getAreaType() == PossibleArea.link_mom) {
                                          //원하는 이웃집 위치 1순위
                                          data.jobItem.boyukData.nhn_area_1_address = "경기 수원시 영통구 광교1동";
                                          data.tcHomeLocation1.text = data.jobItem.boyukData.nhn_area_1_address;
                                          data.jobItem.boyukData.nhn_area_1_id = "4111710300";
                                          data.jobItem.boyukData.nhn_address_1_id = "4111760000";

                                          //원하는 이웃집 위치 2순위
                                          data.jobItem.boyukData.nhn_area_2_address = "경기 수원시 영통구 하동";
                                          data.tcHomeLocation2.text = data.jobItem.boyukData.nhn_area_2_address;
                                          data.jobItem.boyukData.nhn_area_2_id = "4111710400";
                                          data.jobItem.boyukData.nhn_address_2_id = "4111761000";

                                          //원하는 이웃집 위치 거리
                                          data.jobItem.boyukData.nhn_radius = 1;
                                          _setLocationType(data.jobItem.boyukData.nhn_radius);
                                        } else {
                                          data.jobItem.boyukData.boyuk_address = "서울 강남구 밤고개로 76-2";
                                          data.tcPlaceStart.text = data.jobItem.boyukData.boyuk_address;

                                          // 보육장소 상세
                                          data.jobItem.boyukData.boyuk_address_detail = "22";
                                          data.tcPlaceStartDetail.text = data.jobItem.boyukData.boyuk_address_detail;
                                        }

                                        data.jobItem.boyukData.boyuk_comment = "없음";
                                        data.tcPlaceStartComment.text = data.jobItem.boyukData.boyuk_comment;

                                        data.jobItem.boyukData.pathroute_comment = "곧 바로 집으로 오세요~!";
                                        data.tcPlacePathRoute.text = data.jobItem.boyukData.pathroute_comment;

                                        //경유지 장소 설명
                                        if (StringUtils.validateString(data.tcPlacePathRoute.text)) {
                                          data.isSelect = true;
                                        }
                                      } else {
                                        //우리집, 하원+이웃집
                                        if (getAreaType() == PossibleArea.mom_daddy || getServiceType() == ServiceType.serviceType_1 && getAreaType() == PossibleArea.link_mom) {
                                          data.jobItem.moveData.start_address = "서울 강남구 밤고개로 76-2";
                                          data.tcPlaceStart.text = data.jobItem.moveData.start_address;

                                          // 출발장소 상세
                                          data.jobItem.moveData.start_address_detail = "22";
                                          data.tcPlaceStartDetail.text = data.jobItem.moveData.start_address_detail;
                                        }

                                        //시간선택
                                        //   data.jobItem.startTime = "10:00";
                                        DateTime currentTime = DateTime.now();
                                        // data.jobItem.startTime = DateTime(currentTime.year, currentTime.month, currentTime.day, 10, 0);
                                        data.jobItem.startTime = StringUtils.getTimeToDateTime(data.jobItem.reqdata.careschedule!.first.stime);

                                        data.jobItem.moveData.start_time = StringUtils.getTimeToStartTime(data.jobItem.startTime);
                                        data.tcPlaceStartTime.text = StringUtils.getTimeToString(data.jobItem.startTime);

                                        //출발장소 설명
                                        data.jobItem.moveData.start_comment = "없음";
                                        data.tcPlaceStartComment.text = data.jobItem.moveData.start_comment;

                                        //경유지 장소 설명
                                        data.jobItem.moveData.pathroute_comment = "곧 바로 오세요~";
                                        data.tcPlacePathRoute.text = data.jobItem.moveData.pathroute_comment;
                                        if (StringUtils.validateString(data.tcPlacePathRoute.text)) {
                                          data.isSelect = true;
                                        }

                                        // //등원+우리집
                                        // if (getServiceType() == ServiceType.go_school.index && getAreaType() == PossibleArea.ourhome.index ||
                                        //     getServiceType() == ServiceType.go_school.index && getAreaType() == PossibleArea.ourhome.index) {
                                        //   //도착장소
                                        //   data.jobItem.moveData.end_latitude = 37.4873802;
                                        //   data.jobItem.moveData.end_longitude = 127.10150700000001;
                                        //   data.jobItem.moveData.end_address = "서울 강남구 밤고개로 99";
                                        //   data.tcPlaceEnd.text = data.jobItem.moveData.end_address;
                                        //
                                        //   //도착장소상세
                                        //   data.jobItem.moveData.end_address_detail = "33";
                                        //   data.tcPlaceEndDetail.text = data.jobItem.moveData.end_address_detail;
                                        // }

                                        //도착장소 우리집, 하원+이웃집이 아닐때만 활성화
                                        if (getServiceType() == ServiceType.serviceType_0 && getAreaType() == PossibleArea.mom_daddy ||
                                            getServiceType() == ServiceType.serviceType_0 && getAreaType() == PossibleArea.link_mom ||
                                            getServiceType() == ServiceType.serviceType_1 && getAreaType() != PossibleArea.link_mom) {
                                          // if (getServiceType() == ServiceType.go_school.index && getAreaType() == PossibleArea.ourhome.index ||
                                          //     getServiceType() == ServiceType.go_school.index && getAreaType() == PossibleArea.nbhhome.index ||
                                          //     getServiceType() == ServiceType.go_home.index && getAreaType() == PossibleArea.ourhome.index) {
                                          data.jobItem.moveData.end_address = "서울 강남구 밤고개로 99";
                                          data.tcPlaceEnd.text = data.jobItem.moveData.end_address;

                                          //도착장소상세
                                          data.jobItem.moveData.end_address_detail = "33";
                                          data.tcPlaceEndDetail.text = data.jobItem.moveData.end_address_detail;
                                        }

                                        //시간선택
                                        //   data.jobItem.startTime = "10:00";
                                        // data.jobItem.endTime = DateTime(currentTime.year, currentTime.month, currentTime.day, 11, 0);
                                        data.jobItem.endTime = data.jobItem.startTime.add(Duration(minutes: 30));
                                        data.jobItem.moveData.end_time = StringUtils.getTimeToStartTime(data.jobItem.endTime);
                                        data.tcPlaceEndTime.text = StringUtils.getTimeToString(data.jobItem.endTime);

                                        //도착장소 설명
                                        data.jobItem.moveData.end_comment = "없음";
                                        data.tcPlaceEndComment.text = data.jobItem.moveData.end_comment;

                                        if (getAreaType() == PossibleArea.link_mom) {
                                          //원하는 이웃집 위치 1순위
                                          data.jobItem.moveData.nhn_area_1_address = "경기 수원시 영통구 광교1동";
                                          data.tcHomeLocation1.text = data.jobItem.moveData.nhn_area_1_address;
                                          data.jobItem.moveData.nhn_area_1_id = "4111710300";
                                          data.jobItem.moveData.nhn_address_1_id = "4111760000";

                                          //원하는 이웃집 위치 2순위
                                          data.jobItem.moveData.nhn_area_2_address = "경기 수원시 영통구 하동";
                                          data.tcHomeLocation2.text = data.jobItem.moveData.nhn_area_2_address;
                                          data.jobItem.moveData.nhn_area_2_id = "4111710400";
                                          data.jobItem.moveData.nhn_address_2_id = "4111761000";

                                          //원하는 이웃집 위치 거리
                                          data.jobItem.moveData.nhn_radius = 1;
                                          _setLocationType(data.jobItem.moveData.nhn_radius);
                                        }
                                      }
                                      onConfirmBtn();
                                    },
                                    child: contentView(
                                      data: ContentData(
                                        content: getMoveTitle(getServiceType()),
                                        isOverLapRight: !isBoyuk(getServiceType()),
                                      ),
                                      rightIcon: requiredText(),
                                      onClick: () {},
                                    )),
                                Row(
                                  children: [
                                    lText('${"돌봄이용시간".tr()} ', style: st_14(textColor: color_999999)),
                                    lText(time, style: st_b_14(textColor: color_ff3b30, fontWeight: FontWeight.bold)),
                                  ],
                                ),
                                //보육일 경운엔 숨김
                                if (getServiceType() != ServiceType.serviceType_2)
                                  Column(
                                    children: [
                                      sb_h_20,
                                      ListMoveTypeSelector(
                                        data: data.jobItem.lsJobMoveType,
                                        count: 3,
                                        margin: padding_10_LR,
                                        onItemClick: (value) {
                                          log.d('『GGUMBI』>>> build ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<<  $value');
                                          // storageHelper.setAddressVersion(0);
                                          _moveTypeItem = value as SingleItem;
                                          //선택하였다가 비활성화시 제일 마지막 아이템으로 보여준다.
                                          if (!_moveTypeItem.isSelected) {
                                            data.jobItem.lsJobMoveType.forEach((value) {
                                              if (value.isSelected) {
                                                _moveTypeItem = value;
                                              }
                                            });
                                          }
                                          onConfirmBtn();
                                        },
                                      ),
                                    ],
                                  ),
                                if (StringUtils.validateString(_moveTypeItem.content)) sb_h_20,
                                //이동방법에 대한 설명
                                moveTypeView(_moveTypeItem),
                                sb_h_20,
                              ],
                            ),
                          ),

                          //출발,도착 및 뷰
                          placeView(
                            context,
                            _moveTypeItem,
                            data,
                            getServiceType(),
                            getAreaType(),
                            callBack: (action) {
                              if (action != null) {
                                PlaceAction item = action as PlaceAction;
                                log.d('『GGUMBI』>>> build ★★★★★★★★★★★★★ callBack ★★★★★★★★★★★★★ <<< $item ');
                                //출발장소
                                if (item.viewType == MoveViewType.start) {
                                  data.jobItem.moveData.start_address = item.address!;
                                  data.jobItem.boyukData.boyuk_address = item.address!;
                                  data.jobItem.temp_area_start = TempAreaData();
                                }

                                //출발장소 상세
                                if (item.viewType == MoveViewType.startDetail) {
                                  data.jobItem.moveData.start_address_detail = item.data;
                                  data.jobItem.boyukData.boyuk_address_detail = item.data;
                                }

                                //출발장소, 출발장소 상세
                                if (item.viewType == MoveViewType.startSelect) {
                                  TempAreaData areaData = item.data as TempAreaData;
                                  data.jobItem.moveData.start_address = areaData.temp_address;
                                  data.jobItem.boyukData.boyuk_address = areaData.temp_address;
                                  data.jobItem.moveData.start_address_detail = areaData.temp_address_detail;
                                  data.jobItem.boyukData.boyuk_address_detail = areaData.temp_address_detail;
                                }

                                //시간선택
                                if (item.viewType == MoveViewType.startTime) {
                                  data.jobItem.startTime = item.data;
                                  data.jobItem.moveData.start_time = StringUtils.getTimeToStartTime(item.data);
                                }

                                //출발장소 설명
                                if (item.viewType == MoveViewType.startMsg) {
                                  data.jobItem.moveData.start_comment = item.data;
                                  data.jobItem.boyukData.boyuk_comment = item.data;
                                }

                                //도착장소
                                if (item.viewType == MoveViewType.end) {
                                  data.jobItem.moveData.end_address = item.address!;
                                  data.jobItem.temp_area_end = TempAreaData();
                                }

                                //도착장소상세
                                if (item.viewType == MoveViewType.endDetail) {
                                  data.jobItem.moveData.end_address_detail = item.data;
                                }

                                //도착장소, 도착소 상세
                                if (item.viewType == MoveViewType.endSelect) {
                                  TempAreaData areaData = item.data as TempAreaData;
                                  data.jobItem.moveData.end_address = areaData.temp_address;
                                  data.jobItem.moveData.end_address_detail = areaData.temp_address_detail;
                                }

                                //시간선택
                                if (item.viewType == MoveViewType.endTime) {
                                  data.jobItem.endTime = item.data;
                                  data.jobItem.moveData.end_time = StringUtils.getTimeToStartTime(item.data);
                                }

                                /// 도착메세지
                                if (item.viewType == MoveViewType.endMsg) {
                                  data.jobItem.moveData.end_comment = item.data;
                                }

                                //원하는 이웃집 위치 1순위
                                if (item.viewType == MoveViewType.location1) {
                                  Documents addressData = item.data as Documents;
                                  data.jobItem.moveData.nhn_area_1_address = addressData.address!.addressName;
                                  data.jobItem.moveData.nhn_area_1_id = addressData.address!.bCode;
                                  data.jobItem.moveData.nhn_address_1_id = addressData.address!.hCode;

                                  //보육
                                  data.jobItem.boyukData.nhn_area_1_address = addressData.address!.addressName;
                                  data.jobItem.boyukData.nhn_area_1_id = addressData.address!.bCode;
                                  data.jobItem.boyukData.nhn_address_1_id = addressData.address!.hCode;
                                }

                                //원하는 이웃집 위치 2순위
                                if (item.viewType == MoveViewType.location2) {
                                  Documents addressData = item.data as Documents;
                                  data.jobItem.moveData.nhn_area_2_address = addressData.address!.addressName;
                                  data.jobItem.moveData.nhn_area_2_id = addressData.address!.bCode;
                                  data.jobItem.moveData.nhn_address_2_id = addressData.address!.hCode;

                                  //원하는 이웃집 위치 2순위
                                  data.jobItem.boyukData.nhn_area_2_address = addressData.address!.addressName;
                                  data.jobItem.boyukData.nhn_area_2_id = addressData.address!.bCode;
                                  data.jobItem.boyukData.nhn_address_2_id = addressData.address!.hCode;
                                }

                                //원하는 이웃집 위치 거리
                                if (item.viewType == MoveViewType.locationType) {
                                  SingleItem singleItem = item.data as SingleItem;
                                  data.jobItem.moveData.nhn_radius = singleItem.type;
                                  data.jobItem.boyukData.nhn_radius = singleItem.type;
                                }

                                //경유지 추가
                                if (item.viewType == MoveViewType.routeAdd) {
                                  data.jobItem.moveData.pathroute_comment = item.data;
                                  data.jobItem.boyukData.pathroute_comment = item.data;
                                }

                                if (item.viewType == MoveViewType.startUpdate || item.viewType == MoveViewType.endUpdate) {
                                  //원래 데이터와 비교후, 없으면 장소명만 지운다.
                                  List<TempAreaData> areaList = item.data as List<TempAreaData>;
                                  bool isCheck = false;
                                  int areaId = 0;
                                  if (item.viewType == MoveViewType.startUpdate) {
                                    areaId = data.jobItem.temp_area_start.area_id!;
                                  } else if (item.viewType == MoveViewType.endUpdate) {
                                    areaId = data.jobItem.temp_area_end.area_id!;
                                  }

                                  areaList.forEach((beforeData) {
                                    if (areaId == beforeData.area_id) {
                                      isCheck = true;
                                    }
                                  });
                                  //원래 데이터와 같은게 없으면 장소명을 지운다.
                                  if (!isCheck) {
                                    if (item.viewType == MoveViewType.startUpdate) {
                                      data.jobItem.temp_area_start.temp_areaname = '';
                                    } else if (item.viewType == MoveViewType.endUpdate) {
                                      data.jobItem.temp_area_end.temp_areaname = '';
                                    }
                                  }
                                }

                                onConfirmBtn();
                              }
                            },
                          ),
                        ]),
                      ),
                    ),
                    _confirmButton(),
                  ],
                ),
              ),
              // wdDivider(),
            ],
          ),
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn(Commons.bottomBtnTitle(getViewType()), isEnabled: flag.isConfirm, onClickAction: () {
      try {
        DateTime minValue = StringUtils.getTimeToDateTime(data.jobItem.reqdata.careschedule!.first.stime);
        DateTime maxValue = StringUtils.getTimeToDateTime(data.jobItem.reqdata.careschedule!.first.etime);

        DateTime startTime = StringUtils.getTimeToStringFormat(data.tcPlaceStartTime.text);
        DateTime endTime = StringUtils.getTimeToStringFormat(data.tcPlaceEndTime.text);

        log.d('『GGUMBI』>>> showPickerDate : startTime: $startTime, endTime: $endTime, minValue: $minValue, maxValue: $maxValue, <<< ');
        if (startTime.isBefore(minValue) || startTime.isAfter(maxValue) && endTime.isBefore(minValue) || endTime.isAfter(maxValue)) {
          showNormalDlg(message: "돌봄이용시간안내".tr());
          return;
        }

        //출발 시간이 도착 시간 보다 느린 경우
        if (startTime.isAfter(endTime)) {
          showNormalDlg(message: "돌봄이용시간안내_출발".tr());
          return;
        }
        //도착 시간이 출발 시간 보다 빠를 경우
        if (endTime.isBefore(startTime)) {
          showNormalDlg(message: "돌봄이용시간안내_도착".tr());
          return;
        }
      } catch (e) {
        log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      }

      if (getViewType() == ViewType.modify) {
        Commons.pagePop(context, data: data.jobItem.reqdata);
      } else {
        data.jobItem.calcuTime = data.jobItem.totalTime - b_minutes; //이동방법 선택시 (날짜선택시간 - 기본 30분)
        Commons.page(context, routeParents6, arguments: ParentsStep6Page(data: data, viewMode: ViewMode()));
      }
    });
  }

  @override
  void onConfirmBtn() {
    bool isMoveType = false;
    data.jobItem.lsJobMoveType.asMap().forEach((index, value) {
      log.d('『GGUMBI』>>> onConfirmBtn : index: $index ${value.isSelected},  <<< ');
      if (value.isSelected) {
        isMoveType = true;
      }
    });
    //아무것도 선택 안하면 초기화한다.
    if (!isMoveType) {
      _moveTypeItem = SingleItem('');
    }

    bool isConfirmCheck = isConfirm(data, getServiceType(), getAreaType());
    log.d('『GGUMBI』>>> onConfirmBtn : isConfirmCheck: $isConfirmCheck,  <<< ');
    if (isConfirmCheck) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }

    log.d('『GGUMBI』>>> onData : data.jobItem.jobData: ${data.jobItem.jobData},  <<< ');
    log.d('『GGUMBI』>>> onData : data.jobItem.jobData: ${data.jobItem.reqdata},  <<< ');

    log.d('『GGUMBI』>>> onConfirmBtn : : ${data.tcPlaceStart.text},  <<< ');

    if (data.formPlaceStart.currentState != null && StringUtils.validateString(data.tcPlaceStart.text)) {
      data.formPlaceStart.currentState!.validate();
    }
    if (data.formPlaceStartDetail.currentState != null && StringUtils.validateString(data.tcPlaceStartDetail.text)) {
      data.formPlaceStartDetail.currentState!.validate();
    }
    if (data.formPlaceEnd.currentState != null && StringUtils.validateString(data.tcPlaceEnd.text)) {
      data.formPlaceEnd.currentState!.validate();
    }
    if (data.formPlaceEndDetail.currentState != null && StringUtils.validateString(data.tcPlaceEndDetail.text)) {
      data.formPlaceEndDetail.currentState!.validate();
    }

    List<int> productIds = [];
    log.d('『GGUMBI』>>> onConfirmBtn : data.jobItem.calcuTime: ${data.jobItem.calcuTime},  <<< ');
    b_minutes = 0;

    ///이동 선택 상세
    data.jobItem.lsJobMoveType.asMap().forEach((index, value) {
      if (value.isSelected) {
        // log.d('『GGUMBI』>>> _confirmButton : value: ${value.isSelected}, $value,  <<< ');
        ProductData productData = value.data as ProductData;
        log.d('『GGUMBI』>>> _confirmButton : value: $productData,  <<< ');
        productIds.add(productData.product_id);
        b_minutes = productData.product_data!.b_minutes;
      }
    });

    int totalTime = data.jobItem.calcuTime;
    // data.jobItem.totalTime = data.jobItem.totalTime - productData.product_data.b_minutes; //이동방법 선택시 (날짜선택시간 - 기본 30분)
    time = StringUtils.formatCalcuTime(totalTime, b_minutes);
    log.d('『GGUMBI』>>> onConfirmBtn : timetime: $time, ${data.jobItem.calcuTime},$totalTime, $b_minutes  <<< ');

    log.d('『GGUMBI』>>> onConfirmBtn : data.jobItem.calcuTime: ${data.jobItem.calcuTime},  <<< ');

    if (isBoyuk(getServiceType())) {
      ReqBoyukData boyukData = data.jobItem.boyukData;
      log.d('『GGUMBI』>>> onConfirmBtn : data.jobItem.boyukData: $boyukData,  <<< ');
      log.d('『GGUMBI』>>> onConfirmBtn : boyukData: $boyukData,  <<< ');
      ReqBoyukData item = ReqBoyukData(
        boyuk_address: boyukData.boyuk_address,
        boyuk_address_detail: boyukData.boyuk_address_detail,
        boyuk_comment: boyukData.boyuk_comment,
        pathroute_comment: data.isSelect ? boyukData.pathroute_comment : '',
        nhn_area_1_id: boyukData.nhn_area_1_id,
        nhn_address_1_id: boyukData.nhn_address_1_id,
        nhn_area_1_address: boyukData.nhn_area_1_address,
        nhn_area_2_id: boyukData.nhn_area_2_id,
        nhn_address_2_id: boyukData.nhn_address_2_id,
        nhn_area_2_address: boyukData.nhn_area_2_address,
        nhn_radius: boyukData.nhn_radius,
        nhn_comment: boyukData.nhn_comment,
      );

      log.d('『GGUMBI』>>> onConfirmBtn 1 : productIds: $productIds, $item  <<< ');
      data.jobItem.reqdata.bookingboyuk = [];
      data.jobItem.reqdata.bookingboyuk!.add(item);
    } else {
      ReqSchoolToHomeData moveData = data.jobItem.moveData;
      log.d('『GGUMBI』>>> onConfirmBtn : data.jobItem.moveData: ${data.jobItem.moveData},  <<< ');
      log.d('『GGUMBI』>>> onConfirmBtn : moveData: $moveData,  <<< ');

      ReqSchoolToHomeData item = ReqSchoolToHomeData(
        product_id: productIds,
        start_address: moveData.start_address,
        start_address_detail: moveData.start_address_detail,
        start_time: moveData.start_time,
        start_comment: moveData.start_comment,
        pathroute_comment: data.isSelect ? moveData.pathroute_comment : '',
        end_address: moveData.end_address,
        end_address_detail: moveData.end_address_detail,
        end_time: moveData.end_time,
        end_comment: moveData.end_comment,
        nhn_area_1_id: moveData.nhn_area_1_id,
        nhn_address_1_id: moveData.nhn_address_1_id,
        nhn_area_1_address: moveData.nhn_area_1_address,
        nhn_area_2_id: moveData.nhn_area_2_id,
        nhn_address_2_id: moveData.nhn_address_2_id,
        nhn_area_2_address: moveData.nhn_area_2_address,
        nhn_radius: moveData.nhn_radius,
        nhn_comment: moveData.nhn_comment,
      );

      log.d('『GGUMBI』>>> onConfirmBtn 1 : productIds: $productIds, $item  <<< ');

      //등원
      if (getServiceType() == ServiceType.serviceType_0) {
        log.d('『GGUMBI』>>> onConfirmBtn 2 : productIds: $productIds, $item  <<< ');
        data.jobItem.reqdata.bookinggotoschool = [];
        data.jobItem.reqdata.bookinggotoschool!.add(item);

        //하원
      } else if (getServiceType() == ServiceType.serviceType_1) {
        data.jobItem.reqdata.bookingafterschool = [];
        data.jobItem.reqdata.bookingafterschool!.add(item);
      }
    }
    data.jobItem.reqdata.temp_area_start = data.jobItem.temp_area_start;
    data.jobItem.reqdata.temp_area_end = data.jobItem.temp_area_end;

    log.d('『GGUMBI』>>> _confirmButton : data.jobItem.request: ${data.jobItem.reqdata},  <<< ');
  }

  @override
  void onDataPage(_data) {
    try {
      late List<ReqSchoolToHomeData> items;
      late List<ReqBoyukData> boyuks;
      if (data.jobItem.tempResponse.getDataList().isEmpty || getViewType() == ViewType.modify) {
        //등원 데이터
        if (getServiceType() == ServiceType.serviceType_0) {
          items = data.jobItem.reqdata.bookinggotoschool!;
          //하원 데이터
        } else if (getServiceType() == ServiceType.serviceType_1) {
          items = data.jobItem.reqdata.bookingafterschool!;
          //보육 데이터
        } else if (getServiceType() == ServiceType.serviceType_2) {
          boyuks = data.jobItem.reqdata.bookingboyuk!;
        }
      } else {
        //등원 데이터
        if (getServiceType() == ServiceType.serviceType_0) {
          items = tempReqData!.bookinggotoschool!; //임시 저장된 데이터

          //하원 데이터
        } else if (getServiceType() == ServiceType.serviceType_1) {
          items = tempReqData!.bookingafterschool!;

          //보육 데이터
        } else if (getServiceType() == ServiceType.serviceType_2) {
          boyuks = tempReqData!.bookingboyuk!;
        }
      }

      if (isBoyuk(getServiceType())) {
        if (boyuks.isNotEmpty) {
          _setBoyukData(boyuks[0]);
        }
      } else {
        //이동방법 도보, 자동차, 대중교통 세팅
        _setTempData(items);
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }

    onConfirmBtn();
  }

  ///서버에서 임시저장값을 세팅
  bool _setTempData(List<ReqSchoolToHomeData> items) {
    bool isSelected = false;
    if (items == null) {
      return isSelected;
    }
    items.forEach((item) {
      item.product_id!.forEach((value) {
        isSelected = true;
        _setSelectData(item);
        _setMoveType(value);
      });

      log.d('『GGUMBI』>>> _setTempData : isSelected: $isSelected,  <<< ');

      if (getAreaType() == PossibleArea.link_mom) {
        _setLocationType(item.nhn_radius);
      }
    });

    //step4에서 오면 시작,종료시간을 다시 세팅해준다. 임시 저장해서 넘어온 경우 위쪽에서 세팅을 한다.
    data.jobItem.startTime = StringUtils.getTimeToDateTime(data.jobItem.reqdata.careschedule!.first.stime);
    data.jobItem.endTime = StringUtils.getTimeToDateTime(data.jobItem.reqdata.careschedule!.first.etime);

    return isSelected;
  }

  ///이동방법 (도보,자동차,대중교통)서버에서 임시저장값을 세팅
  bool _setMoveType(int value) {
    bool isSelected = false;
    data.jobItem.lsJobMoveType.forEach((singleItem) {
      log.d('『GGUMBI』>>> _setMoveType ------------------------- : item: $value,  <<< ');
      log.d('『GGUMBI』>>> _isSeleted : singleItem: $singleItem,  <<< ');
      if (value == singleItem.type) {
        singleItem.isSelected = true;
        _moveTypeItem = singleItem;
      }
    });
    return isSelected;
  }

  ///이웃집 - 내위치에서 원하는 거리
  bool _setLocationType(int value) {
    bool isSelected = false;
    //이웃집 - 내위치에서 원하는 거리
    data.jobItem.lsJobLocation.forEach((singleItem) {
      log.d('『GGUMBI』>>> _setLocationType ------------------------- : item: $value,  <<< ');
      log.d('『GGUMBI』>>> _isSeleted : singleItem: $singleItem,  <<< ');
      if (value == singleItem.type) {
        singleItem.isSelected = true;
      }
    });
    return isSelected;
  }

  ///서버에서 임시저장 값을 세팅
  _setSelectData(ReqSchoolToHomeData item) {
    if (tempReqData != null && tempReqData!.temp_area_start != null && StringUtils.validateString(tempReqData!.temp_area_start!.temp_areaname)) {
      TempAreaData tempAreaData = tempReqData!.temp_area_start!;
      tempAreaData = getTempAreaData(tempAreaData);
      //삭제된 경우 초기화 그게 아니라면 돌봄관리에 있는 주소를 세팅한다.
      if (!tempAreaData.isAddress) {
        tempAreaData = TempAreaData();
      }
      data.jobItem.temp_area_start = tempAreaData;
      data.tcPlaceStart.text = tempAreaData.temp_address;
      data.tcPlaceStartDetail.text = tempAreaData.temp_address_detail;
      data.jobItem.moveData.start_address = tempAreaData.temp_address;
      data.jobItem.moveData.start_address_detail = tempAreaData.temp_address_detail;
    } else {
      if (data.jobItem.reqdata.temp_area_start != null && StringUtils.validateString(data.jobItem.reqdata.temp_area_start!.temp_areaname)) {
        data.jobItem.temp_area_start = data.jobItem.reqdata.temp_area_start!;
        data.tcPlaceStart.text = data.jobItem.temp_area_start.temp_address;
        data.tcPlaceStartDetail.text = data.jobItem.temp_area_start.temp_address_detail;
        data.jobItem.moveData.start_address = data.jobItem.temp_area_start.temp_address;
        data.jobItem.moveData.start_address_detail = data.jobItem.temp_area_start.temp_address_detail;
      } else {
        data.tcPlaceStart.text = item.start_address;
        data.tcPlaceStartDetail.text = item.start_address_detail;
        data.jobItem.moveData.start_address = item.start_address;
        data.jobItem.moveData.start_address_detail = item.start_address_detail;
      }
    }

    data.jobItem.startTime = StringUtils.getTimeToDateTime(item.start_time);
    data.tcPlaceStartTime.text = StringUtils.getStringToTime(item.start_time);
    data.jobItem.moveData.start_time = item.start_time;
    data.tcPlaceStartComment.text = item.start_comment;
    data.jobItem.moveData.start_comment = item.start_comment;
    data.jobItem.moveData.pathroute_comment = item.pathroute_comment;
    if (StringUtils.validateString(item.pathroute_comment)) {
      data.isSelect = true;
      data.tcPlacePathRoute.text = item.pathroute_comment;
    }

    if (tempReqData != null && tempReqData!.temp_area_end != null && StringUtils.validateString(tempReqData!.temp_area_end!.temp_areaname)) {
      TempAreaData tempAreaData = tempReqData!.temp_area_end!;
      tempAreaData = getTempAreaData(tempAreaData);
      //삭제된 경우 초기화 그게 아니라면 돌봄관리에 있는 주소를 세팅한다.
      if (!tempAreaData.isAddress) {
        tempAreaData = TempAreaData();
      }
      data.jobItem.temp_area_end = tempAreaData;
      data.tcPlaceEnd.text = tempAreaData.temp_address;
      data.tcPlaceEndDetail.text = tempAreaData.temp_address_detail;
      data.jobItem.moveData.end_address = tempAreaData.temp_address;
      data.jobItem.moveData.end_address_detail = tempAreaData.temp_address_detail;
    } else {
      if (data.jobItem.reqdata.temp_area_end != null && StringUtils.validateString(data.jobItem.reqdata.temp_area_end!.temp_areaname)) {
        data.jobItem.temp_area_end = data.jobItem.reqdata.temp_area_end!;
        data.tcPlaceEnd.text = data.jobItem.temp_area_end.temp_address;
        data.tcPlaceEndDetail.text = data.jobItem.temp_area_end.temp_address_detail;
        data.jobItem.moveData.end_address = data.jobItem.temp_area_end.temp_address;
        data.jobItem.moveData.end_address_detail = data.jobItem.temp_area_end.temp_address_detail;
      } else {
        data.tcPlaceEnd.text = item.end_address;
        data.tcPlaceEndDetail.text = item.end_address_detail;
        data.jobItem.moveData.end_address = item.end_address;
        data.jobItem.moveData.end_address_detail = item.end_address_detail;
      }
    }

    data.jobItem.endTime = StringUtils.getTimeToDateTime(item.end_time);
    data.tcPlaceEndTime.text = StringUtils.getStringToTime(item.end_time);
    data.jobItem.moveData.end_time = item.end_time;
    data.jobItem.moveData.end_comment = item.end_comment;
    data.tcPlaceEndComment.text = item.end_comment;
    data.jobItem.moveData.nhn_radius = item.nhn_radius;

    //이웃집
    data.tcHomeLocation1.text = item.nhn_area_1_address;
    data.jobItem.moveData.nhn_area_1_address = item.nhn_area_1_address;
    data.jobItem.moveData.nhn_area_1_id = item.nhn_area_1_id;
    data.jobItem.moveData.nhn_address_1_id = item.nhn_address_1_id;

    data.tcHomeLocation2.text = item.nhn_area_2_address;
    data.jobItem.moveData.nhn_area_2_address = item.nhn_area_2_address;
    data.jobItem.moveData.nhn_area_2_id = item.nhn_area_2_id;
    data.jobItem.moveData.nhn_address_2_id = item.nhn_address_2_id;
  }

  ///서버에서 임시저장 값을 세팅
  _setBoyukData(ReqBoyukData item) {
    if (tempReqData != null && tempReqData!.temp_area_start != null && StringUtils.validateString(tempReqData!.temp_area_start!.temp_areaname)) {
      TempAreaData tempAreaData = tempReqData!.temp_area_start!;
      tempAreaData = getTempAreaData(tempAreaData);
      //삭제된 경우 초기화 그게 아니라면 돌봄관리에 있는 주소를 세팅한다.
      if (!tempAreaData.isAddress) {
        tempAreaData = TempAreaData();
      }
      data.jobItem.temp_area_start = tempAreaData;
      data.tcPlaceStart.text = tempAreaData.temp_address;
      data.tcPlaceStartDetail.text = tempAreaData.temp_address_detail;
      data.jobItem.boyukData.boyuk_address = tempAreaData.temp_address;
      data.jobItem.boyukData.boyuk_address_detail = tempAreaData.temp_address_detail;
    } else {
      if (data.jobItem.reqdata.temp_area_start != null) {
        data.jobItem.temp_area_start = data.jobItem.reqdata.temp_area_start!;
        data.tcPlaceStart.text = data.jobItem.temp_area_start.temp_address;
        data.tcPlaceStartDetail.text = data.jobItem.temp_area_start.temp_address_detail;
        data.jobItem.boyukData.boyuk_address = data.jobItem.temp_area_start.temp_address;
        data.jobItem.boyukData.boyuk_address_detail = data.jobItem.temp_area_start.temp_address_detail;
      } else {
        data.tcPlaceStart.text = item.boyuk_address;
        data.tcPlaceStartDetail.text = item.boyuk_address_detail;
        data.jobItem.boyukData.boyuk_address = item.boyuk_address;
        data.jobItem.boyukData.boyuk_address_detail = item.boyuk_address_detail;
      }
    }

    data.tcPlaceStartComment.text = item.boyuk_comment;
    data.jobItem.boyukData.boyuk_comment = item.boyuk_comment;
    data.jobItem.boyukData.pathroute_comment = item.pathroute_comment;
    if (StringUtils.validateString(item.pathroute_comment)) {
      data.isSelect = true;
      data.tcPlacePathRoute.text = item.pathroute_comment;
    }

    //이웃집
    data.tcHomeLocation1.text = item.nhn_area_1_address;
    data.jobItem.boyukData.nhn_area_1_address = item.nhn_area_1_address;
    data.jobItem.boyukData.nhn_area_1_id = item.nhn_area_1_id;
    data.jobItem.boyukData.nhn_address_1_id = item.nhn_address_1_id;

    data.tcHomeLocation2.text = item.nhn_area_2_address;
    data.jobItem.boyukData.nhn_area_2_address = item.nhn_area_2_address;
    data.jobItem.boyukData.nhn_area_2_id = item.nhn_area_2_id;
    data.jobItem.boyukData.nhn_address_2_id = item.nhn_address_2_id;

    data.jobItem.boyukData.nhn_radius = item.nhn_radius;

    log.d('『GGUMBI』>>> _setBoyukData : getAreaType(): ${getAreaType()}, ${item.nhn_radius}  <<< ');
    if (getAreaType() == PossibleArea.link_mom) {
      _setLocationType(item.nhn_radius);
    }
  }

  ///서비스 유형(등원,하원,보육...)
  ServiceType getServiceType() {
    return EnumUtils.getServiceType(index: data.jobItem.request.servicetype);
    // return getCareViewData().servicetype;
  }

  ///이웃집, 우리집
  PossibleArea getAreaType() {
    return EnumUtils.getPossibleArea(data.jobItem.request.possible_area);
    // return getCareViewData().possible_area;
  }

  ///이동 방법 메시지 표시
  String getMoveTitle(ServiceType serviceType) {
    String title = "이동_메시지".tr();
    if (serviceType == ServiceType.serviceType_2) {
      title = "보육장소_메시지".tr();
    }
    return title;
  }

  TempAreaData getTempAreaData(TempAreaData tempAreaData) {
    TempAreaData item = TempAreaData();
    data.jobItem.tempAreaList.forEach((value) {
      value.isAddress = false;
      if (value.area_id == tempAreaData.area_id) {
        value.isAddress = true;
        item = value;
      }
    });
    return item;
  }

  ///돌봄장소 가져오기 요청
  void _requestTempAreaView() async {
    data.jobItem.tempAreaLength = 0;
    await apiHelper.requestTempAreaView().then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        data.jobItem.tempAreaList = response.getDataList();
        data.jobItem.tempAreaLength = response.getDataList().length;
        onDataPage(null);
      }
      onConfirmBtn();
    }).catchError((e) {});
  }
}
