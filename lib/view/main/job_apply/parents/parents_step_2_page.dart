import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/temp_request.dart';
import 'package:linkmom/data/network/models/temp_response.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/listview/list_type_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/main/job_apply/content_view.dart';
import 'package:linkmom/view/main/job_apply/parents/parents_step_3_page.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../child_info_view.dart';

class ParentsStep2Page extends BaseStateful {
  @override
  _ParentsStep2PageState createState() => _ParentsStep2PageState();

  ParentsStep2Page({this.data, this.viewMode});

  final DataManager? data;
  final ViewMode? viewMode;
}

class _ParentsStep2PageState extends BaseStatefulState<ParentsStep2Page> {
  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);

    // var datas = _data as DataManager;
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 2;
    data.jobItem.request = _data.jobItem.request;
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.request: ${data.jobItem.request},  <<< ');

    // onDataPage(_data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        resizeToAvoidBottomPadding: false,
        appBar: jobAppBar("돌봄유형".tr(), hide: true, onClick: () {
          Commons.showSaveTempDlg(data: data);
        }),
        body: lContainer(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step),
              Expanded(
                child: Column(
                  children: [
                    Expanded(
                      child: lScrollView(
                        child: Container(
                          width: double.infinity,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              childInfoView(data.jobItem.childInfo),
                              contentView(
                                  data: ContentData(content: "돌봄유형_안내메시지".tr()),
                                  onClick: () {
                                    log.d('『GGUMBI』>>> build ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
                                  }),
                              sb_h_20,
                              ListTypeSelector(
                                data: data.jobItem.lsJobType,
                                padding: padding_05,
                                onItemClick: (value) {
                                  // log.d('『GGUMBI』>>> build : data: $value,  <<< ');
                                  // log.d('『GGUMBI』>>> build : value as SingleItem: ${value as SingleItem},  <<< ');
                                  SingleItem item = value as SingleItem;

                                  ///서비스 타입 (등원,하원,보육,학습...)
                                  data.jobItem.request.servicetype = item.type;
                                  if (item.type == ServiceType.serviceType_3.index) {
                                    showNormalDlg(message: "서비스준비중".tr());
                                    item.isSelected = false;
                                    onConfirmBtn();
                                    return;
                                  }
                                  _requestTempView();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    _confirmButton(),
                  ],
                ),
              ),
              // wdDivider(),
            ],
          ),
        ),
      ),
      isOnlyLoading: true,
      opacity: 0.0,
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn("다음".tr(), isEnabled: flag.isConfirm, onClickAction: () {
      Commons.page(context, routeParents3, arguments: ParentsStep3Page(data: data));
    });
  }

  @override
  void onConfirmBtn() {
    bool isHome = false;
    data.jobItem.lsJobType.asMap().forEach((index, value) {
      if (value.isSelected) {
        isHome = true;
        data.jobItem.request.servicetype = index;
        return;
      }
    });

    if (isHome) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  ///아이정보, 서비스 0등원, 1하원, 2보육, 3학습, 4입주가사, 5이유식, 99전체
  _requestTempView() {
    flag.enableLoading(fn: () => onUpDate());
    apiHelper.requestTempView(TempRequest(child: data.jobItem.request.child, servicetype: data.jobItem.request.servicetype, depth: 99)).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        showNormalDlg(
            message: "임시저장불러오기".tr(),
            btnLeft: "취소".tr(),
            btnRight: "불러오기".tr(),
            onClickAction: (action) {
              if (DialogAction.yes == action) {
                onDataPage(response);
              } else if (DialogAction.no == action) {
                data.jobItem.tempResponse = TempResponse();
                data.jobItem.lsJobType.asMap().forEach((index, value) {
                  value.isSelected = false;
                });
              }
            });
      }
      flag.disableLoading(fn: () => onUpDate());
      onConfirmBtn();
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }

  @override
  void onDataPage(_data) {
    var item = _data as TempResponse;
    if (item.getDataList().isEmpty) {
      return;
    }

    data.jobItem.tempResponse = item;
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.tempResponse: ${data.jobItem.tempResponse},  <<< ');
    //돌봄유형 등원, 하원, 보육, 학습, 입주가사, 이유식
    data.jobItem.lsJobType.asMap().forEach((index, value) {
      if (index == item.getData().servicetype) {
        value.isSelected = true;
      } else {
        value.isSelected = false;
      }
    });
    onConfirmBtn();
  }
}
