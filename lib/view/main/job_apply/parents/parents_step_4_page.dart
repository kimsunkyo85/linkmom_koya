import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:linkmom/data/holidays/holidays_helper.dart';
import 'package:linkmom/data/network/models/req_data.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/calendar/calendar_view.dart';
import 'package:linkmom/view/calendar/momdaddy_calendar.dart';
import 'package:linkmom/view/calendar/schedule_controller.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../child_info_view.dart';
import '../content_view.dart';
import 'parents_step_5_page.dart';

class ParentsStep4Page extends BaseStateful {
  @override
  _ParentsStep4PageState createState() => _ParentsStep4PageState();

  ParentsStep4Page({this.data, this.viewMode});

  final DataManager? data;
  final ViewMode? viewMode;
}

class _ParentsStep4PageState extends BaseStatefulState<ParentsStep4Page> {
  List<Picker> pickes = [];
  CareSchedule _schedule = CareSchedule(bookingdate: []);
  PanelController _panelController = PanelController();
  ScheduleController _schedCtrl = ScheduleController();

  bool _timeSelected = false;
  bool _editMode = true;

  String endDate = "";

  Widget _panelArrow = Lcons.nav_top(color: color_cecece);

  @override
  initState() {
    super.initState();
    _clearDate();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (_schedule.bookingdate.isNotEmpty) {
        _openPanel();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 4;

    ///수정일때는 STEP을 보여주지 않는다.
    if (getViewType() == ViewType.modify) {
      data.jobItem.childInfo.isShow = false;
      data.jobItem.request = _data.jobItem.request.clone();
      data.jobItem.reqdata = _data.jobItem.reqdata.clone();
    } else {
      data.jobItem.request = _data.jobItem.request;
      data.jobItem.reqdata = _data.jobItem.reqdata;
    }

    data.jobItem.jobData = _data.jobItem.jobData;
    data.jobItem.tempResponse = _data.jobItem.tempResponse;
    data.jobItem.totalTime = _data.jobItem.totalTime;
    data.jobItem.scheduleLength = _data.jobItem.scheduleLength;
    log.d('『GGUMBI』>>> onData : viewType: ${getViewType()},  <<< ');
    log.d('『GGUMBI』>>> onData : totalTime: ${data.jobItem.totalTime},  <<< ');
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.request: ${data.jobItem.reqdata},  <<< ');
    log.d('『GGUMBI』>>> onData : data.jobItem.jobData: ${data.jobItem.jobData},  <<< ');
    log.d('『GGUMBI』>>> onData : data.jobItem.tempResponse: ${data.jobItem.tempResponse},  <<< ');

    onConfirmBtn();
    onDataPage(_data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
          key: data.scaffoldKey,
          context: context,
          appBar: jobAppBar(
            "날짜시간".tr(),
            isClose: getViewType() == ViewType.apply ? true : false,
            hide: getViewType() == ViewType.apply,
            onClick: () {
              if (getViewType() == ViewType.modify) {
                Commons.pagePop(context);
              } else {
                //스케줄 데이터 값 저장
                Commons.showSaveTempDlg(data: data);
              }
            },
          ),
          body: Container(
              child: Column(mainAxisSize: MainAxisSize.min, children: [
            Expanded(
              child: SlidingUpPanel(
                controller: _panelController,
                minHeight: 90,
                maxHeight: 200,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(24.0), topRight: Radius.circular(24.0)),
                body: Column(children: [
                  Expanded(
                      child: ListView(children: [
                    if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step),
                    sb_h_10,
                    childInfoView(data.jobItem.childInfo, margin: stepDateMargin),
                    Container(
                      margin: padding_20_LR,
                      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                        contentView(data: ContentData(content: "시간선택요청".tr())),
                      ]),
                    ),
                    Padding(
                        padding: padding_10,
                        child: CalendarView(
                          holidays: HolidaysHelper.getHolidays,
                          selected: selectDate,
                          eventLimit: 30,
                          height: 50,
                          colors: [color_main],
                          selectable: true,
                          events: {},
                          startDate: DateTime.now(),
                          duration: Duration(days: 395),
                          type: USER_TYPE.mom_daddy,
                          onCreateCalendar: (ctrl) {
                            _schedCtrl = ctrl;
                            if (ctrl.eventer.isEmpty) {
                              _schedCtrl.eventer.imports(_schedule.asMap());
                            }
                          },
                        )),
                    Container(height: Commons.height(context, 0.6)),
                  ]))
                ]),
                header: Container(
                  height: 50,
                  width: Commons.width(context, 1),
                  alignment: Alignment.center,
                  child: IconButton(
                    icon: _panelArrow,
                    onPressed: () {
                      setState(() {
                        if (_panelController.isPanelClosed) {
                          _openPanel();
                        } else {
                          _closePanel();
                        }
                      });
                    },
                  ),
                ),
                panel: StatefulBuilder(builder: (context, setState) {
                  return Container(
                    margin: padding_30_T,
                    child: Column(
                      children: [
                        if (!_timeSelected) _timeSelecter(),
                        if (_schedule.schedules.isNotEmpty)
                          lScrollView(
                            padding: padding_20_LR,
                            child: TimeBlock(
                                editDateFun: (repeat, state, update) {
                                  if (repeat) {
                                    showNormalDlg(message: "매주반복해제".tr());
                                    update(false);
                                  } else {
                                    List<DateTime> temp = _schedCtrl.selecter.getSchedule.keys.toList();
                                    _editMode = state;

                                    /// enable edit
                                    if (!state) {
                                      if (_isContainToday(temp) && _isBefore(_schedule.getStime, DateTime.now())) {
                                        showNormalDlg(message: "현재시간이후를선택".tr());
                                        _schedCtrl.selecter.getSchedule.removeWhere((date, event) => _isContainToday([date]));
                                        _editMode = true;
                                      } else if (temp.isNotEmpty) {
                                        temp.sort();
                                        temp.addAll(_schedCtrl.eventer.getSchedule.keys.toList());
                                        _schedCtrl.saveEvent();
                                        _schedule.clearSchedule();
                                        _schedule.addSchedule(temp);
                                        _updateBlock(false, list: _schedule.schedules);
                                      } else if (temp.isEmpty) {
                                        temp = _schedCtrl.eventer.getSchedule.keys.toList();
                                        if (temp.isNotEmpty) {
                                          temp.sort();
                                          _schedule.clearSchedule();
                                          _schedule.addSchedule(temp);
                                          _updateBlock(false, list: _schedule.schedules);
                                          _editMode = false;
                                        } else {
                                          showNormalDlg(message: "날짜선택_에러".tr());
                                          _editMode = true;
                                        }
                                      } else {
                                        showNormalDlg(message: "날짜선택_에러".tr());
                                        _editMode = true;
                                      }
                                      update(_editMode);
                                    } else {
                                      update(true);
                                    }
                                  }
                                },
                                deleteTimeFun: (isFirst) {
                                  _showDeleteMsg(
                                      ok: () {
                                        _timeSelected = false;
                                        _schedule = CareSchedule(bookingdate: []);
                                        _clearDate();
                                        _closePanel();
                                        onUpDate();
                                      },
                                      cancel: () {});
                                },
                                onRepeat: (isRepeat) {
                                  List<DateTime> updated = _updateRepeatSchedule(!isRepeat);
                                  updated.sort();
                                  if (isRepeat && _isEnableRepeat(_schedule.schedules)) {
                                    if (!listEquals(_schedule.schedules, updated)) {
                                      _showRepeatSelecter(updated);
                                    } else {
                                      _schedule.clearSchedule();
                                      _schedule.addSchedule(updated);
                                      _updateBlock(false, list: updated);
                                    }
                                  } else if (!isRepeat) {
                                    data.jobItem.scheduleLength = updated.length;
                                    _schedule.clearSchedule();
                                    _schedule.addSchedule(updated);
                                    _updateBlock(false, list: updated);
                                    isRepeat = false;
                                  } else {
                                    showNormalDlg(message: "매주반복_에러".tr());
                                    isRepeat = false;
                                  }
                                  _schedule.is_repeat = isRepeat ? 1 : 0;
                                  onUpDate();
                                },
                                editTimeFun: () {
                                  _showTimePicker(true, sTime: _schedule.getStime, eTime: _schedule.getEtime);
                                  _openPanel();
                                  onUpDate();
                                },
                                color: Commons.getColor(),
                                isRepeat: _schedule.is_repeat == 1,
                                date: _schedule.schedules,
                                startTime: _schedule.getStime,
                                endTime: _schedule.getEtime,
                                showTitle: true,
                                timeDesc: "(${"매주반복안내_하이라이트2_돌봄".tr()})",
                                showDeleteBtn: getViewType() != ViewType.modify),
                          ),
                      ],
                    ),
                  );
                }),
              ),
            ),
            _confirmButton(),
          ]))),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn(getViewType() == ViewType.modify ? "확인".tr() : (flag.isConfirm ? "다음".tr() : "확인".tr()), isEnabled: _isConfrim(), onClickAction: () {
      data.jobItem.reqdata.careschedule = [_schedule.getSchedule];
      data.jobItem.totalTime = data.jobItem.reqdata.careschedule!.first.durationtime; // only one element
      data.jobItem.calcuTime = data.jobItem.totalTime;
      log.d('『GGUMBI』>>> _confirmButton : data.jobItem.totalTime : ${data.jobItem.totalTime},  <<< ');
      log.d('『GGUMBI』>>> _confirmButton : : ${data.jobItem.calcuTime}, ${getViewType()} <<< ');
      data.jobItem.scheduleLength = data.jobItem.reqdata.careschedule!.first.bookingdate.length;
      data.jobItem.reqdata.careschedule!.first.bookingdate.sort(); //최근 데이터로 정렬하기 추가 2021/10/20
      if (getViewType() == ViewType.modify) {
        Commons.pagePop(context, data: data.jobItem);
      } else {
        Commons.page(context, routeParents5, arguments: ParentsStep5Page(data: data, viewMode: ViewMode()));
      }
    });
  }

  bool _isConfrim() {
    return flag.isConfirm;
  }

  @override
  void onConfirmBtn() {
    if (_schedule.bookingdate.isNotEmpty) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  @override
  void onDataPage(_data) {
    var item = _data as DataManager;
    log.d('『GGUMBI』>>> onDataPage : item: $item,  <<< ');

    if (_data.jobItem.reqdata.careschedule!.isEmpty) {
      return;
    }

    ///돌봄 신청에 사용할 데이터를 초기화 한다.
    if (getViewType() != ViewType.modify) {
      //수정모드로 올 경우에는 초기화 하지 않는다. 돌봄 신청시에만 초기화 작업을 한다.
      data.jobItem.reqdata = ReqData.init();
    }

    log.d('『GGUMBI』>>> onDataPage : : ${data.jobItem.tempResponse},  <<< ');
    if (getViewType() == ViewType.modify && data.jobItem.reqdata.careschedule!.isNotEmpty) {
      //날짜,시간 데이터
      data.jobItem.totalTime = data.jobItem.reqdata.careschedule!.first.durationtime;
      data.jobItem.calcuTime = data.jobItem.totalTime;
      data.jobItem.startDate = StringUtils.parseHD(data.jobItem.reqdata.careschedule!.first.stime);
      data.jobItem.endDate = StringUtils.parseHD(data.jobItem.reqdata.careschedule!.first.etime);
      _schedule.setSchedule(data.jobItem.reqdata.careschedule!.first);
      _restoreDate(_schedule.getSchedule);
    }
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.request.reqdata: ${data.jobItem.reqdata}, ${getViewType()}  <<< ');
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.request.reqdata: ${getViewType()}  <<< ');
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.reqData.careschedule: ${data.jobItem.reqdata.careschedule},  <<< ');

    onConfirmBtn();
  }

  bool selectDate(DateTime date, List<int> events) {
    if (!_editMode) return false;
    if (_schedule.bookingdate.isNotEmpty) {
      if (_schedule.bookingdate.length > 29) {
        _schedule.bookingdate.remove(date);
        showNormalDlg(message: "돌봄_날짜선택_에러".tr());
        return false;
      }
    }
    if (!_schedCtrl.selecter.hasDay(date) && _schedCtrl.selecter.length > 29) {
      showNormalDlg(message: "돌봄_날짜선택_에러".tr());
      return false;
    }
    if (events.contains(0)) {
      events.remove(0);
    } else {
      events.add(0);
    }
    onUpDate();
    return true;
  }

  Widget _timeSelecter() {
    bool enableAdd = _schedCtrl.selecter.isNotEmpty;
    return Container(
        padding: padding_20,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: lInkWell(
          onTap: () {
            if (_schedCtrl.selecter.isEmpty) {
              showNormalDlg(message: "돌봄_시간추가에러".tr());
            } else {
              _showTimePicker(false);
            }
          },
          child: lRoundContainer(
            padding: padding_18_TB,
            bgColor: Colors.white,
            borderColor: enableAdd ? Commons.getColor() : color_dedede,
            borderWidth: 1,
            alignment: Alignment.center,
            child: Row(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: [
              lText("시간선택".tr(), style: st_14(textColor: enableAdd ? color_545454 : color_b2b2b2, fontWeight: FontWeight.w500)),
              Lcons.add(color: Commons.getColor(), disableColor: color_b2b2b2, size: 16, isEnabled: enableAdd),
            ]),
          ),
        ));
  }

  void _showTimePicker(bool isEdit, {DateTime? sTime, DateTime? eTime}) {
    if (_schedCtrl.selecter.isNotEmpty || _schedule.bookingdate.isNotEmpty) {
      showScheduleTimePicker(
        context,
        (a, start, end) {
          switch (a) {
            case DialogAction.close:
              {
                _editMode = true;
                onUpDate();
              }
              break;
            case DialogAction.update:
              {
                _schedule.setTime(start, end);
              }
              break;
            case DialogAction.confirm:
              {
                if (_schedule.bookingdate.length > 30) {
                  showNormalDlg(message: "돌봄_날짜선택_에러".tr());
                } else if (_schedCtrl.selecter.hasDay(StringUtils.getUtcFromDate(DateTime.now())) && _isBefore(start, DateTime.now())) {
                  showNormalDlg(context: context, message: "현재시간이후를선택".tr());
                } else {
                  _schedCtrl.saveEvent();
                  int isAm = _isAm(start, end);
                  CareSchedule req = CareSchedule(
                    bookingdate: [],
                    stime: StringUtils.formatHD(start),
                    etime: StringUtils.formatHD(end),
                    isAm: isAm,
                    isPm: isAm == 1 ? 0 : 1,
                    id: 0,
                    durationtime: start.difference(end).inMinutes.abs(),
                  );
                  req.addSchedule(_schedCtrl.eventer.getSchedule.keys.toList());
                  _schedule.setSchedule(req);
                  data.jobItem.scheduleLength = _schedule.bookingdate.length;
                  _timeSelected = true;
                  _openPanel();
                  onConfirmBtn();
                }
              }
              break;
            default:
          }
          onUpDate();
        },
        start: sTime ?? _schedule.getStime,
        end: eTime ?? _schedule.getEtime,
      );
    } else {
      showNormalDlg(message: "날짜선택요청".tr());
    }
  }

  void _updateBlock(bool isEdit, {List<DateTime>? list}) {
    if (isEdit && list != null) {
      list.forEach((date) {
        _schedCtrl.eventer.toggle(date, [0]);
      });
    } else {
      _schedCtrl.eventer.clear();
      if (list != null) list.forEach((date) => _schedCtrl.eventer.append(date, events: [0]));
    }
    onConfirmBtn();
  }

  void _clearDate() {
    _editMode = true;
    data.jobItem.reqdata.careschedule!.clear();
    if (data.jobItem.tempResponse.dataList != null) {
      data.jobItem.tempResponse.getData().reqdata!.careschedule!.clear();
    }
    _schedCtrl.eventer.clear();
    _schedule.clearSchedule();
    onConfirmBtn();
  }

  bool _isEnableRepeat(List<DateTime> date) {
    date.sort();
    return date.last.weekday < DateTime.sunday && date.last.difference(date.first) < Duration(days: 7);
  }

  List<DateTime> _updateRepeatSchedule(bool clear, {DateTime? end, int repeatCount = 0}) {
    List<DateTime> list = [];
    if (clear) {
      int count = (_schedule.schedules.last.weekday - _schedule.schedules.first.weekday) + 1;
      if (count < 1) {
        List<DateTime> lastRepeat = _schedule.schedules.where((e) => e.isBefore(_schedule.schedules.first.add(Duration(days: 7)))).toList();
        count = lastRepeat.length;
      }
      list = List.generate(count, (index) => _schedule.schedules[index]);
    } else {
      int count = repeatCount == 0 ? (30 / _schedule.schedules.length).round() : repeatCount;
      _schedule.schedules.forEach((schedule) {
        if (end != null) count = (end.add(Duration(days: 1)).difference(schedule).inDays / 7).ceil();
        list.addAll(List.generate(count, (counter) => schedule.add(Duration(days: counter * 7))));
      });
    }
    onUpDate();
    list.sort();
    return list;
  }

  void _showDeleteMsg({Function? cancel, Function? ok}) {
    showNormalDlg(
      btnLeft: "취소".tr(),
      btnRight: "삭제".tr(),
      message: "삭제확인_메세지".tr(),
      onClickAction: (action) {
        switch (action) {
          case DialogAction.yes:
            ok!();
            break;
          case DialogAction.no:
            cancel!();
            break;
          default:
        }
      },
    );
  }

  void _restoreDate(ReqScheduleData req) {
    log.d('『GGUMBI』>>> _restoreDate : reqList: $req,  <<< ');
    bool isExpired = false;
    if (_schedule.schedules.isNotEmpty) {
      List<DateTime> list = _schedule.schedules;
      list.removeWhere((date) {
        //날짜가 지나지 않은것만 추가한다. 2021/10/20
        date.add(Duration(minutes: StringUtils.parseHD(req.stime).minute));
        if (!Commons.isExpired(date, day: 1)) {
          //오늘 날짜이고, 지금 시간보다 30분 늦으면
          if (Commons.isExpiredTime(date)) {
            isExpired = true;
          } else {
            isExpired = false;
          }
        } else {
          isExpired = true;
        }
        if (isExpired) _schedule.removeDate(date);
        return isExpired;
      });
      _schedCtrl.eventer.imports(_schedule.asMap());

      String nowTime = StringUtils.getTimeToString(DateTime.now().add(Duration(minutes: 30)));
      String msg = '${"날짜시간변경안내_1".tr()} $nowTime ${"날짜시간변경안내_2".tr()}';
      Widget messageWidget = TextHighlight(
        text: msg,
        term: nowTime,
        textStyle: st_14(textColor: color_545454),
        textStyleHighlight: st_14(textColor: color_545454, fontWeight: FontWeight.w700),
        textAlign: TextAlign.center,
      );

      if (_schedCtrl.eventer.isNotEmpty) {
        _timeSelected = true;
        _editMode = false;
      }

      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (isExpired) {
          showNormalDlg(messageWidget: messageWidget);
        }
        onUpDate();
      });
      _updateBlock(false, list: _schedule.schedules);
    }
  }

  void _showRepeatSelecter(List<DateTime> list) {
    _closePanel();
    Map<DateTime, List<int>> events = {};
    list.forEach((date) => events.putIfAbsent(date, () => [0]));
    showRepeatSelecter(
      context,
      list.last,
      (a, List<DateTime> updated) {
        switch (a) {
          case DialogAction.close:
            {
              _editMode = true;
              _schedule.is_repeat = 0;
              _openPanel();
            }
            break;
          case DialogAction.fail:
            {
              Commons.showToast("돌봄_날짜선택_에러".tr());
              onUpDate();
            }
            break;
          case DialogAction.update:
            {
              onUpDate();
            }
            break;
          case DialogAction.confirm:
            {
              updated.sort();
              data.jobItem.scheduleLength = updated.length;
              _schedule.clearSchedule();
              _schedule.addSchedule(updated);
              _updateBlock(false, list: updated);
              _openPanel();
            }
            break;
          default:
        }
      },
      originEvents: events,
      end: 30,
      info: "매주반복안내_돌봄".tr(),
      infoHighlight: "매주반복안내_하이라이트2_돌봄".tr(),
    );
  }

  void _closePanel() {
    _panelController.close();
    _panelArrow = Lcons.nav_top(color: color_cecece, isEnabled: true);
  }

  void _openPanel() {
    _panelController.open();
    _panelArrow = Lcons.nav_bottom(color: color_cecece, isEnabled: true);
  }

  bool _isBefore(DateTime start, DateTime now) {
    start = DateTime(now.year, now.month, now.day, start.hour, start.minute);
    return start.difference(now).inMinutes < 30;
  }

  bool _isContainToday(List<DateTime> time) {
    DateTime now = DateTime.now();
    return time.contains(DateTime(now.year, now.month, now.day).add(now.timeZoneOffset).toUtc());
  }

  int _isAm(DateTime sTime, DateTime eTime) {
    if (sTime.hour < 12 || eTime.hour < 12) {
      return 1;
    } else {
      return 0;
    }
  }
}
