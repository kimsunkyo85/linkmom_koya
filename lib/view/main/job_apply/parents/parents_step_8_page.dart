import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/care_request.dart';
import 'package:linkmom/data/network/models/temp_save_request.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_dailog/dlg_group_view.dart';
import 'package:linkmom/utils/custom_dailog/dlg_pay_view.dart';
import 'package:linkmom/utils/modal_bottom_sheet/push_modal_bottom.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/title_value_view.dart';

import '../../../../base/base_stateful.dart';
import '../../../../data/storage/flag_manage.dart';
import '../../../../main.dart';
import '../../../../route_name.dart';
import '../child_info_view.dart';
import '../title_toggle_icon_view.dart';
import 'parents_step_9_page.dart';

class ParentsStep8Page extends BaseStateful {
  @override
  _ParentsStep8PageState createState() => _ParentsStep8PageState();

  ParentsStep8Page({this.data, this.viewMode});

  final DataManager? data;
  final ViewMode? viewMode;
}

class _ParentsStep8PageState extends BaseStatefulState<ParentsStep8Page> {
  final PRICE = 10000.0;
  double payValue = 10000.0;

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 8;

    log.d('『GGUMBI』>>> onData : _data.jobItem.reqdata.cost_perhour.toDouble(): ${_data.jobItem.reqdata.cost_perhour.toDouble()},  <<< ');
    // data.jobItem.reqdata.cost_perhour = data.payItem.payPrice;
    ///수정일때는 STEP을 보여주지 않는다.
    if (isModifyMode()) {
      data.jobItem.childInfo.isShow = false;
      payValue = _data.jobItem.reqdata.cost_perhour == 0 ? PRICE : _data.jobItem.reqdata.cost_perhour.toDouble(); //기본시급 입력
      data.payItem.payPrice = _data.jobItem.reqdata.cost_perhour;
      data.jobItem.request = _data.jobItem.request.clone();
      data.jobItem.reqdata = _data.jobItem.reqdata.clone();
      //협의가능
      data.payItem.isPay = StringUtils.getIntToBool(data.jobItem.reqdata.is_negotiable);
      //그룹보육
      data.payItem.isGroup = StringUtils.getIntToBool(data.jobItem.reqdata.is_togetherboyuk);
      //신청기간
      data.jobItem.scheduleLength = data.jobItem.reqdata.careschedule!.isNotEmpty ? data.jobItem.reqdata.careschedule!.first.bookingdate.length : 0;
      //신청시간
      data.jobItem.totalTime = data.jobItem.reqdata.careschedule!.isNotEmpty ? data.jobItem.reqdata.careschedule!.first.durationtime : 0;
    } else {
      payValue = PRICE; //기본시급 입력
      _data.jobItem.reqdata.cost_perhour = PRICE.toInt();
      data.payItem.payPrice = _data.jobItem.reqdata.cost_perhour;
      data.jobItem.request = _data.jobItem.request;
      data.jobItem.reqdata = _data.jobItem.reqdata;
      data.jobItem.totalTime = _data.jobItem.totalTime;
      data.jobItem.scheduleLength = _data.jobItem.scheduleLength;
    }
    data.jobItem.jobData = _data.jobItem.jobData;
    data.jobItem.tempResponse = _data.jobItem.tempResponse;
    data.jobItem.calcuTime = _data.jobItem.calcuTime;
    data.listViewItem.areaValue = _data.listViewItem.areaValue;
    // data.jobItem.totalTime = 200;

    log.d('『GGUMBI』>>> onData : data.jobItem.scheduleLength: ${data.jobItem.scheduleLength},  <<< ');
    log.d('『GGUMBI』>>> onData : _data.jobItem.tempResponse: ${_data.jobItem.tempResponse},  <<< ');
    log.d('『GGUMBI』>>> onData : _data.jobItem.totalTime: ${_data.jobItem.totalTime},  <<< ');
    log.d('『GGUMBI』>>> onData : data.jobItem.totalTime: ${data.jobItem.totalTime},  <<< ');
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.request: ${data.jobItem.reqdata},  <<< ');
    log.d('『GGUMBI』>>> onData : data.jobItem.jobData: ${data.jobItem.jobData},  <<< ');
    log.d('『GGUMBI』>>> onData : data.jobItem.boyukData: ${data.jobItem.reqdata.bookingboyuk},  <<< ');
    log.d('『GGUMBI』>>> onData : _data.jobItem.reqdata: ${_data.jobItem.reqdata},  <<< ');
    log.d('『GGUMBI』>>> onData : getViewType(): ${getViewType()},  <<< ');
    onDataPage(_data);
    // onConfirmBtn();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: jobAppBar(
          "결제금액".tr(),
          hide: true,
          isClose: isModifyMode() ? false : true,
          onClick: () {
            Commons.showSaveTempDlg(data: data);
          },
        ),
        body: lContainer(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step),
              Expanded(
                child: Column(
                  children: [
                    Expanded(
                      child: lScrollView(
                        padding: padding_Item_TB,
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              //아이정보
                              Padding(
                                padding: padding_20_LR,
                                child: childInfoView(data.jobItem.childInfo),
                              ),
                              _payView(),
                              lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_05_TB),
                              _groupView(),
                              // //이웃집일 경우 그룹 보육에 대한 동의 화면을 보여준다.
                              // if (data.jobItem.request.possible_area == PossibleArea.nbhhome.index) _groupView(),
                              lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_10_TB),
                              _totalView(),

                              // _groupView(),
                            ],
                          ),
                        ),
                      ),
                    ),
                    _confirmButton(),
                  ],
                ),
              ),
              // wdDivider(),
            ],
          ),
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn(isModifyMode() ? "확인".tr() : "돌봄신청등록".tr(), /*isEnabled: flag.isConfirm,*/ onClickAction: () async {
      ///입력된 시급(원)
      data.jobItem.reqdata.cost_perhour = data.payItem.payPrice;

      data.jobItem.reqdata.cost_hoursum = Commons.getPrice(data.payItem.day, data.payItem.payPrice, data.jobItem.totalTime);

      ///협의용 금액 // TODO: GGUMBI 4/14/21 - 협의용 예약관리에서 사용 기본 값 : 0
      data.jobItem.reqdata.cost_negotiable = 0;

      ///비용합계 → 시급(보육비)+cost_negotiable
      data.jobItem.reqdata.cost_sum = Commons.getTotalPay(
        data.payItem.day,
        data.payItem.payPrice,
        data.jobItem.totalTime,
      );

      ///10분당 단가 → 전체금액 합계 ((cost_perhour X 신청시간) + cost_negotiable) / 6
      data.jobItem.reqdata.cost_avg = Commons.getAvgPrice(data.jobItem.reqdata.cost_perhour, data.jobItem.totalTime, data.jobItem.reqdata.cost_negotiable);

      ///비용협의가능 -> 예약관리 쪽에서 한다.
      data.jobItem.reqdata.is_negotiable = StringUtils.getBoolToInt(data.payItem.isPay);

      ///그룹보육 동의
      data.jobItem.reqdata.is_togetherboyuk = StringUtils.getBoolToInt(data.payItem.isGroup);

      CareRequest careRequest = CareRequest();
      careRequest.child = data.jobItem.request.child;
      careRequest.servicetype = data.jobItem.request.servicetype;
      careRequest.possible_area = data.jobItem.request.possible_area;
      careRequest.reqdata = jsonEncode(data.jobItem.reqdata);
      if (isModifyMode()) {
        Commons.pagePop(context, data: data.jobItem.reqdata);
      } else {
        setViewType(ViewType.apply);
        flag.enableLoading(fn: () => onUpDate());
        await apiHelper
            .requestTempSave(TempSaveRequest(
              child: data.jobItem.request.child,
              servicetype: data.jobItem.request.servicetype,
              possible_area: data.jobItem.request.possible_area,
              reqdata: jsonEncode(data.jobItem.reqdata),
            ))
            .catchError((e) => flag.disableLoading(fn: () => onUpDate()));

        await apiHelper.requestCaresSave(careRequest).then((response) {
          if (response.getCode() == KEY_SUCCESS) {
            if (Flags.getFlag(Flags.KEY_PUSH_MOMDADY).isShown) {
              Commons.page(context, routeParents9, arguments: ParentsStep9Page(data: data));
            } else {
              Flags.addFlag(Flags.KEY_PUSH_MOMDADY, isShown: true);
              showPushBottomSheet(context, onClick: (action) {
                if (DialogAction.confirm == action) {
                  Commons.page(context, routeParents9, arguments: ParentsStep9Page(data: data));
                }
              });
            }
          } else {
            showNormalDlg(message: response.getMsg());
          }
          flag.disableLoading(fn: () => onUpDate());
        }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      }
    });
  }

  @override
  void onConfirmBtn() {
    flag.enableConfirm(fn: () => onUpDate());
  }

  @override
  void onDataPage(_data) {
    var item = _data as DataManager;
    log.d('『GGUMBI』>>> onDataPage : item: $item,  <<< ');
    log.d('『GGUMBI』>>> onDataPage : : ${data.jobItem.tempResponse},  <<< ');

    //신청기간
    // data.payItem.day = data.jobItem.reqdata.careschedule[0].bookingdate.length;
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.scheduleLength: ${data.jobItem.scheduleLength},  <<< ');
    data.payItem.day = data.jobItem.scheduleLength;

    if (data.jobItem.tempResponse.getData().reqdata == null) return;

    log.d('『GGUMBI』>>> onDataPage : data.jobItem.tempResponse.getData() 1 : ${data.jobItem.tempResponse.getData()},  <<< ');
    //시급
    data.payItem.payPrice = data.jobItem.tempResponse.getData().reqdata!.cost_perhour;

    data.jobItem.reqdata.cost_perhour = data.payItem.payPrice;
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.tempResponse.getData() 2 : ${data.payItem.payPrice},  <<< ');
    if (data.payItem.payPrice < 1) {
      data.payItem.payPrice = PRICE.toInt();
      data.jobItem.reqdata.cost_perhour = PRICE.toInt();
    }

    payValue = data.payItem.payPrice.toDouble();

    log.d('『GGUMBI』>>> onDataPage : data.jobItem.tempResponse.getData().reqdata: ${data.jobItem.tempResponse.getData().reqdata},  <<< ');
    //협의가능
    data.payItem.isPay = StringUtils.getIntToBool(data.jobItem.tempResponse.getData().reqdata!.is_negotiable);
    //동시,그룹보육 여부
    data.payItem.isGroup = StringUtils.getIntToBool(data.jobItem.tempResponse.getData().reqdata!.is_togetherboyuk);

    log.d('『GGUMBI』>>> onDataPage : 시급 : ${data.payItem.payPrice},  <<< ');
    log.d('『GGUMBI』>>> onDataPage : 협의가능 : ${data.payItem.isPay},  <<< ');
    log.d('『GGUMBI』>>> onDataPage : 동시,그룹보육 여부 : ${data.payItem.isGroup},  <<< ');

    if (data.jobItem.tempResponse.getDataList().isEmpty) {
    } else {}

    onConfirmBtn();
  }

  ///시급,스피드매칭 화면
  Widget _payView() {
    return lContainer(
      padding: padding_20_LRB,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          titleToggleIconView(
            data,
            "시급입력".tr(),
            rightWidget: lText(''),
            onClick: (action) {
              log.d('『GGUMBI』>>> _carePaylView ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<<$action ');
              if (action == DialogAction.info) {
                showNormalDlg(messageWidget: dlgPayView());
              }
            },
            infoColor: color_dedede,
          ),
          sb_h_15,
          lText(
            "시급".tr(),
            style: st_b_16(fontWeight: FontWeight.w500),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              lText(
                '${StringUtils.formatPay(data.payItem.payPrice)}${"원".tr()}',
                style: st_22(textColor: color_main, fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
              Expanded(
                child: InkWell(
                  onTap: () {
                    data.payItem.isPay = !data.payItem.isPay;
                    data.jobItem.reqdata.is_negotiable = StringUtils.getBoolToInt(data.payItem.isPay);
                    onUpDate();
                  },
                  child: Padding(
                    padding: padding_05_TB,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Lcons.check_circle(isEnabled: data.payItem.isPay, color: Commons.getColor(), disableColor: color_cecece, size: 25),
                        sb_w_05,
                        lText("협의가능".tr(), style: st_b_14()),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          sb_h_05,
          SliderTheme(
            data: lSliderThemeData(context),
            child: Slider(
                value: payValue,
                min: 5000.0,
                max: 50000.0,
                divisions: 45,
                autofocus: false,
                label: StringUtils.formatPay(payValue.round()),
                onChanged: (double value) {
                  setState(() {
                    payValue = value.roundToDouble();
                    data.payItem.payPrice = payValue.toInt();
                    data.jobItem.reqdata.cost_perhour = data.payItem.payPrice;
                    // log.d('『GGUMBI』>>> _payView : _value: $value, ${value.roundToDouble()}, ${value.toInt()}  <<< ');
                  });
                  // onUpDate();
                }),
          ),
          Row(
            children: [
              Expanded(child: lText("5,000", style: st_14(textColor: color_b2b2b2))),
              Expanded(child: lText("27,000", style: st_14(textColor: color_b2b2b2), textAlign: TextAlign.center)),
              Expanded(child: lText("50,000", style: st_14(textColor: color_b2b2b2), textAlign: TextAlign.right)),
            ],
          ),
        ],
      ),
    );
  }

  ///그룹 보육
  Widget _groupView() {
    PossibleArea areaType = EnumUtils.getPossibleArea(data.jobItem.request.possible_area);
    return Padding(
      padding: padding_20,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Row(children: [
          lAutoSizeText(
            getGroupTitle(data.jobItem.request.servicetype, areaType.value),
            style: st_b_17(fontWeight: FontWeight.w700),
          ),
          lInkWell(
            onTap: () {
              showNormalDlg(
                title: areaType == PossibleArea.mom_daddy ? "동시보육이란?".tr() : "그룹보육이란?".tr(),
                titleAlign: Alignment.centerLeft,
                paddingTitle: padding_30_T,
                padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                messageWidget: dlgGroupView(context, areaType),
              );
            },
            child: Lcons.info(disableColor: color_dedede, size: 25),
          )
        ]),
        lInkWell(
          onTap: () {
            data.payItem.isGroup = !data.payItem.isGroup;
            data.jobItem.reqdata.is_togetherboyuk = StringUtils.getBoolToInt(data.payItem.isGroup);
            onUpDate();
          },
          child: Row(
            children: [
              lText("네".tr(), style: st_b_14(fontWeight: FontWeight.bold)),
              sb_w_02,
              Lcons.check_circle(disableColor: color_dedede, color: Commons.getColor(), size: 25, isEnabled: data.payItem.isGroup),
            ],
          ),
        ),
      ]),
    );
  }

  ///결제예정내역
  Widget _totalView() {
    return Padding(
      padding: padding_20_LTR,
      child: Container(
        child: Column(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                titleValueView(
                  "신청기간".tr(),
                  style: st_b_18(
                    fontWeight: FontWeight.w500,
                  ),
                  valueRight: '${data.payItem.day}',
                  styleRight: st_b_18(
                    fontWeight: FontWeight.w500,
                  ),
                  textAlignRight: TextAlign.right,
                  valueRight2: "일".tr(),
                  styleRight2: st_b_18(
                    fontWeight: FontWeight.w500,
                  ),
                ),
                titleValueView(
                  "신청시간".tr(),
                  padding: padding_0,
                  style: st_b_18(
                    fontWeight: FontWeight.w500,
                  ),
                  valueRight: StringUtils.formatCalcuTime(data.jobItem.totalTime, 0),
                  styleRight: st_b_18(
                    fontWeight: FontWeight.w500,
                  ),
                  textAlignRight: TextAlign.right,
                ),
                titleValueView(
                  "10분당".tr(),
                  style: st_b_14(textColor: color_999999),
                  valueRight: StringUtils.formatPay(Commons.getAvgPrice(data.jobItem.reqdata.cost_perhour, data.jobItem.totalTime, data.jobItem.reqdata.cost_negotiable)),
                  styleRight: st_b_14(textColor: color_999999),
                  textAlignRight: TextAlign.right,
                  valueRight2: "원".tr(),
                  styleRight2: st_b_14(textColor: color_999999),
                ),
                titleValueView(
                  "신청금액".tr(),
                  padding: padding_0,
                  style: st_b_18(
                    fontWeight: FontWeight.w500,
                  ),
                  // value2: "신청금액_안내".tr(),
                  // style2: st_14(textColor: color_999999),
                  // valueRight: StringUtils.formatPay(data.payItem.payPrice * data.payItem.day * getTotalTime(data.jobItem.totalTime)),
                  valueRight: StringUtils.formatPay(Commons.getPrice(data.payItem.day, data.payItem.payPrice, data.jobItem.totalTime)) + "원".tr(),
                  styleRight: st_b_18(
                    fontWeight: FontWeight.w500,
                  ),
                  textAlignRight: TextAlign.right,
                  // valueRight2: "원".tr(),
                ),
                titleValueView(
                  "원단위절사".tr(),
                  padding: padding_0,
                  style: st_b_14(textColor: color_999999),
                  valueRight: StringUtils.formatPay(Commons.getTotalFloor(data.payItem.day, data.payItem.payPrice, data.jobItem.totalTime, data.payItem.speedPrice, data.payItem.emergencyPrice)),
                  styleRight: st_b_14(textColor: color_999999),
                  textAlignRight: TextAlign.right,
                  valueRight2: "원".tr(),
                  styleRight2: st_b_14(textColor: color_999999),
                ),
                lDivider(color: color_main, padding: padding_20_TB, thickness: 2.0),
                titleValueView(
                  "결제예상금액".tr(),
                  valueRight: StringUtils.formatPay(Commons.getTotalPay(data.payItem.day, data.payItem.payPrice, data.jobItem.totalTime)),
                  style: st_b_20(fontWeight: FontWeight.bold),
                  styleRight: st_b_20(fontWeight: FontWeight.bold),
                  fontWeightRight: FontWeight.bold,
                  textAlignRight: TextAlign.right,
                  valueRight2: "원".tr(),
                  styleRight2: st_b_16(fontWeight: FontWeight.w500),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  ///동시, 그룹보육 내용
  String getGroupTitle(int serviceType, int possibleArea) {
    String title = "형제자매동시보육이필요하신가요".tr();
    //등원+우리집
    if (possibleArea == PossibleArea.mom_daddy.value) {
      title = "형제자매동시보육이필요하신가요".tr();
      //등원+이웃집
    } else if (possibleArea == PossibleArea.link_mom.value) {
      title = "링크쌤집에서그룹보육괜찮으신가요".tr();
    }
    return title;
  }
}
