import 'package:badges/badges.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/area_address_response.dart';
import 'package:linkmom/data/network/models/req_data.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/dropdown/custom_dropdown.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/job_profile/momdady_schedule_page.dart';
import 'package:linkmom/view/main/job_apply/title_value_view.dart';
import 'package:linkmom/view/main/mypage/child_info/child_info_add_page.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../child_info_view.dart';
import '../content_view.dart';
import 'parents_step_4_page.dart';
import 'parents_step_5_page.dart';
import 'parents_step_6_page.dart';
import 'parents_step_8_page.dart';

class ParentsStep7Page extends BaseStateful {
  @override
  _ParentsStep7PageState createState() => _ParentsStep7PageState();

  ParentsStep7Page({this.data, this.viewMode});

  final DataManager? data;
  final ViewMode? viewMode;
}

class _ParentsStep7PageState extends BaseStatefulState<ParentsStep7Page> {
  int flex = 3;
  int flex2 = 7;

  ///주소 최대 라인
  int maxLineAddress = 3;

  ///경유지 최대 라인
  int maxLinePath = 4;

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
    log.d('『GGUMBI』>>> initState : data.jobItem.request.reqdata: ${data.jobItem.reqdata},  <<< ');

    if (data.jobItem.reqdata.careschedule!.isEmpty) {
      data.jobItem.reqdata = ReqData(careschedule: []);
    }

    log.d('『GGUMBI』>>> initState : data.jobItem.reqdata.bookingboyuk: ${data.jobItem.reqdata.bookingboyuk},  <<< ');
    log.d('『GGUMBI』>>> initState : data.jobItem.request.reqdata.bookingplayoptions: ${widget.data!.jobItem.reqdata.bookingboyukoptions},  <<< ');
    log.d('『GGUMBI』>>> initState : data.jobItem.request.reqdata.bookingplayoptions: ${widget.data!.jobItem.reqdata.bookinghomecareoptions},  <<< ');
    log.d('『GGUMBI』>>> initState : data.jobItem.request.reqdata.bookingplayoptions: ${widget.data!.jobItem.reqdata.bookingplayoptions},  <<< ');
    // data.jobItem.request.reqdata.bookingboyuk.add(BoyukData(product_name: '학교', route_comment: '3시:30분 등원, 4시30분 하원 입니다.', route_address: '경기도 수원시 영통구 하동 1016-1 광교아파트 23동 1025호'));
    // data.jobItem.request.reqdata.bookingboyuk.add(BoyukData(product_name: '미술학원', route_comment: '미술학원에 들려주세요.', route_address: '경기도 수원시 영통구 하동 1016-1 광교아파트 23동 1025호'));
    // TODO: GGUMBI 3/22/21 - 애니메이션 사용시 사용할것
    // Future.delayed(Duration(seconds: 1)).then((_) {
    //  data.isAnimation = true;
    //  onUpDate();
    // });

    _requestAreaAddressView();
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 7;
    data.jobItem.request = _data.jobItem.request;
    data.jobItem.reqdata = _data.jobItem.reqdata;
    data.jobItem.jobData = _data.jobItem.jobData;
    data.jobItem.tempResponse = _data.jobItem.tempResponse;
    data.jobItem.totalTime = _data.jobItem.totalTime;
    data.jobItem.calcuTime = _data.jobItem.calcuTime;
    data.jobItem.scheduleLength = _data.jobItem.scheduleLength;
    log.d('『GGUMBI』>>> onData : data.jobItem.scheduleLength: ${data.jobItem.scheduleLength},  <<< ');
    log.d('『GGUMBI』>>> onDataPage : data.jobItem.request: ${data.jobItem.reqdata},  <<< ');
    log.d('『GGUMBI』>>> onData : data.jobItem.jobData: ${data.jobItem.jobData},  <<< ');
    log.d('『GGUMBI』>>> onData : data.jobItem.tempResponse: ${data.jobItem.tempResponse},  <<< ');
    onOptionUpdate();
    onDataPage(_data);
    // onConfirmBtn();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      ///하드웨어 back key 막기
      onWillPop: () => Future.value(false),
      child: lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          appBar: jobAppBar(
            "신청내역".tr(),
            isBack: false,
            hide: true,
            onClick: () {
              Commons.showSaveTempDlg(data: data);
            },
          ),
          body: lContainer(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step),
                Expanded(
                  child: Column(
                    children: [
                      Expanded(
                        child: lScrollView(
                          padding: padding_Item_TB,
                          child: Container(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                //스텝
                                Padding(
                                  padding: padding_Item_LR,
                                  child: childInfoView(data.jobItem.childInfo),
                                ),

                                //등원 돌봄 케어 서비스 유형
                                Padding(
                                  padding: padding_Item_LR,
                                  child: _careInfoView(),
                                ),

                                lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_10_TB),

                                //인증된 동네 기분 주소
                                Padding(
                                  padding: padding_Item_LR,
                                  child: _addrInfoView(),
                                ),
                                lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_15_TB),

                                //아이정보
                                Padding(
                                  padding: padding_Item_LR,
                                  child: _childInfoView(),
                                ),

                                lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_15_TB),

                                //날짜/시간
                                Padding(
                                  padding: padding_Item_LR,
                                  child: _dateView(),
                                ),
                                lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_15_TB),

                                //이동 방법
                                Padding(
                                  padding: padding_Item_LR,
                                  child: _moveView(),
                                ),
                                lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_15_TB),

                                //핖요한 돌봄
                                Padding(
                                  padding: padding_Item_LR,
                                  child: _addCareView(),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      _confirmButton(),
                    ],
                  ),
                ),
                // wdDivider(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn("다음".tr(), /*isEnabled: flag.isConfirm,*/ onClickAction: () {
      Commons.page(context, routeParents8, arguments: ParentsStep8Page(data: data, viewMode: ViewMode()));
    });
  }

  @override
  void onConfirmBtn() {
    flag.enableConfirm(fn: () => onUpDate());
  }

  @override
  void onDataPage(_data) {
    if (_data == null) {
      return;
    }

    if (_data is AreaAddressResponse) {
      data.listViewItem.lsAreaAddress = _data.getDataList();
      data.listViewItem.lsArea.clear();
      int index = 0;
      data.listViewItem.lsAreaAddress.asMap().forEach((position, value) {
        data.listViewItem.lsArea.add(value.getAddress3depth());
        if (data.jobItem.tempResponse.getDataList().isNotEmpty) {
          String tempCode = data.jobItem.tempResponse.getData().reqdata!.addr_code!.writed_addr_bcode + data.jobItem.tempResponse.getData().reqdata!.addr_code!.writed_addr_hcode;
          String valueCode = value.bcode + value.hcode;
          if (StringUtils.validateString(tempCode) && tempCode == valueCode) {
            index = position;
          }
        }
      });

      try {
        data.listViewItem.areaValue = data.listViewItem.lsAreaAddress[index].getAddress3depth();
        data.jobItem.reqdata.addr_code!.writed_addr_bcode = data.listViewItem.lsAreaAddress[index].bcode;
        data.jobItem.reqdata.addr_code!.writed_addr_hcode = data.listViewItem.lsAreaAddress[index].hcode;
      } catch (e) {
        // //통신요청 에러 및 기타 이유로 데이터가 없을 경우
        // String value = '${auth.user.my_info_data!.auth_address!.region_2depth} ${auth.user.my_info_data!.auth_address!.region_3depth}';
        // if (!StringUtils.validateString(auth.user.my_info_data!.auth_address!.region_3depth)) {
        //   value = '${auth.user.my_info_data!.auth_address!.region_2depth} ${auth.user.my_info_data!.auth_address!.region_4depth}';
        // }
        // data.listViewItem.areaValue = value;
        // data.jobItem.reqdata.addr_code!.writed_addr_bcode = auth.user.my_info_data!.auth_address!.getAddressName();
        // data.jobItem.reqdata.addr_code!.writed_addr_hcode = data.listViewItem.lsAreaAddress[index].hcode;
      }
      onUpDate();
    }
  }

  ///서비스 유형
  Widget _careInfoView() {
    return Column(
      children: [
        Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
                child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                lText(Commons.getServiceTypeToString(getServiceType()), textAlign: TextAlign.left, style: st_22(textColor: color_main, fontWeight: FontWeight.bold)),
                sb_w_10,
                lText(Commons.getAreaToString(getAreaType()), textAlign: TextAlign.left, style: st_16(textColor: color_main)),
              ],
            )),
            Lcons.serviceType(getServiceType(), size: 40, color: Commons.getColor(), isEnabled: true)
          ],
        ),
        sb_h_15,
      ],
    );
  }

  ///인증된 동네 기본 주소 선택
  Widget _addrInfoView() {
    return Column(
      children: [
        Row(
          children: [
            lText("인증된동네".tr(), style: st_b_18(fontWeight: FontWeight.bold)),
            sb_w_10,
            Expanded(
              child: SizedBox(
                height: tabHeight,
                child: CustomDropDown(
                  value: data.listViewItem.areaValue,
                  itemsList: data.listViewItem.lsArea,
                  isExpanded: true,
                  isDense: true,
                  leftWidget: Padding(
                    padding: padding_03_LR,
                    child: Lcons.my_location(size: 20),
                  ),
                  icon: Lcons.nav_bottom(color: color_222222),
                  style: st_b_15(fontWeight: FontWeight.w500),
                  overflow: TextOverflow.ellipsis,
                  onChanged: (value) {
                    data.listViewItem.lsAreaAddress.forEach((item) {
                      if (value == item.getAddress3depth()) {
                        data.listViewItem.areaValue = value;
                        data.jobItem.reqdata.addr_code!.writed_addr_bcode = item.bcode;
                        data.jobItem.reqdata.addr_code!.writed_addr_hcode = item.hcode;
                      }
                    });
                    log.d('『GGUMBI』>>> _addrInfoView : value: $value, ${data.listViewItem.areaValue}, ${data.jobItem.reqdata.addr_code}  <<< ');
                    onUpDate();
                  },
                ),
              ),
            ),
          ],
        ),
        sb_h_05,
      ],
    );
  }

  ///아이정보
  Widget _childInfoView() {
    return Container(
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              contentView(
                  data: ContentData(content: '', rightWidget: childInfoView2(data.jobItem.childInfo), isModify: true),
                  onClick: () async {
                    var _data = await Commons.page(context, routeChildInfoAdd, arguments: ChildInfoAddPage(data: data, child_id: data.jobItem.request.child, viewMode: ViewMode(viewType: ViewType.modify, itemType: ItemType.modify)));
                    if (_data != null) {
                      data.jobItem.childInfo = _data;
                    }
                    onUpDate();
                  }),
              sb_h_10,
            ],
          ),
        ],
      ),
    );
  }

  ///날짜/시간
  Widget _dateView() {
    return Container(
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              contentView(
                  data: ContentData(content: "날짜시간".tr(), isModify: true),
                  onClick: () async {
                    log.d('『GGUMBI』>>> _dateView : 날짜 시간 수정 이벤트  <<< ');
                    log.d('『GGUMBI』>>> _dateView : data.jobItem.reqdata: ${data.jobItem.reqdata},  <<< ');
                    log.d('『GGUMBI』>>> _dateView : data.jobItem.totalTime: ${data.jobItem.totalTime},  <<< ');
                    log.d('『GGUMBI』>>> _dateView : data.jobItem.calcuTime: ${data.jobItem.calcuTime},  <<< ');
                    log.d('『GGUMBI』>>> _dateView : data.jobItem.scheduleLength: ${data.jobItem.scheduleLength},  <<< ');
                    log.d('『GGUMBI』>>> _dateView : data.jobItem.reqdata.careschedule!: ${data.jobItem.reqdata.careschedule!},  <<< ');
                    var _data = await Commons.page(context, routeParents4, arguments: ParentsStep4Page(data: data, viewMode: ViewMode(viewType: ViewType.modify)));
                    log.d('『GGUMBI』>>> _dateView : _data: $_data,  <<< ');
                    if (_data != null && _data is JobItem) {
                      int previous = data.jobItem.totalTime;
                      data.jobItem.reqdata = _data.reqdata;
                      data.jobItem.totalTime = _data.totalTime;
                      data.jobItem.calcuTime = _data.calcuTime;
                      data.jobItem.scheduleLength = _data.scheduleLength;
                      log.d('『GGUMBI』>>> _dateView : data.jobItem.scheduleLength: ${data.jobItem.scheduleLength},  <<< ');
                      log.d('『GGUMBI』>>> _dateView : _data.jobItem.totalTime: ${data.jobItem.totalTime}, ${_data.totalTime} <<< ');
                      if (previous != _data.totalTime) {
                        showNormalDlg(
                            message: "필요한돌봄안내".tr(),
                            btnLeft: "아니오".tr(),
                            btnRight: "예".tr(),
                            onClickAction: (action) async {
                              if (action == DialogAction.yes) {
                                var serviceData = await Commons.page(context, routeParents6,
                                    arguments: ParentsStep6Page(
                                      data: data,
                                      viewMode: getViewMode(),
                                      tabType: ServiceTabType.care,
                                    ));

                                if (serviceData != null) {
                                  data.jobItem.reqdata = serviceData;
                                  onOptionUpdate();
                                }
                              }
                            });
                      }
                    }
                    onUpDate();
                    log.d('『GGUMBI』>>> _dateView : _data: $_data,  <<< ');
                  }),
              sb_h_15,
              titleValueView(
                "돌봄날짜".tr(),
                valueRight: getCareDate(),
                bottomAlign: Alignment.bottomLeft,
                flex: 1,
                flex2: 3,
              ),
              titleValueView(
                "돌봄시간".tr(),
                valueRight: getCareTime(),
                flex: 1,
                flex2: 3,
              ),
              sb_h_15,
              lBtn(
                "돌봄날짜확인".tr(),
                borderRadius: 8,
                btnColor: color_eeeeee,
                style: st_b_16(fontWeight: FontWeight.w500),
                padding: padding_0,
                margin: padding_0,
                onClickAction: () {
                  log.d('『GGUMBI』>>> _dateView : data.jobItem.reqdata.careschedule!.first.bookingdate: ${data.jobItem.reqdata.careschedule!.first.bookingdate},  <<< ');
                  log.d('『GGUMBI』>>>  돌봄날짜확인 dateView : : ${data.jobItem.reqdata.careschedule!.first.stime}, ${data.jobItem.reqdata.careschedule!.first.etime} <<< ');
                  Commons.page(context, routeMomdadySchedule,
                      arguments: MomdadySchedulePage(
                        data: data.jobItem.reqdata.careschedule!.first.bookingdate,
                        sTime: data.jobItem.reqdata.careschedule!.first.stime,
                        eTime: data.jobItem.reqdata.careschedule!.first.etime,
                      ));
                },
              ),
              sb_h_15,
            ],
          ),
        ],
      ),
    );
  }

  ///이동빙법화면
  Widget _moveView() {
    return Container(
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              contentView(
                  data: ContentData(content: Commons.getMoveTitle(getServiceType()), isModify: true),
                  onClick: () async {
                    log.d('『GGUMBI』>>> _dateView : 이동방법 수정 이벤트  <<< ');
                    var _data = await Commons.page(context, routeParents5, arguments: ParentsStep5Page(data: data, viewMode: ViewMode(viewType: ViewType.modify)));
                    if (_data != null) {
                      data.jobItem.reqdata = _data;
                      onOptionUpdate();
                    }
                    onUpDate();
                  }),
              if (!isBoyuk(getServiceType()))
                Column(
                  children: [
                    sb_h_15,
                    titleValueView(
                      "이동수단".tr(),
                      valueRight: getMoveType(getServiceType()),
                      maxLineRight: 2,
                      flex: flex,
                      flex2: flex2,
                    ),
                    titleValueView(
                      "출발시간".tr(),
                      valueRight: getStartTime(getServiceType()),
                      flex: flex,
                      flex2: flex2,
                    ),
                  ],
                ),
              if (isBoyuk(getServiceType())) sb_h_15,
              titleValueView(
                Commons.getStartAddressTitle(getServiceType()),
                valueRight: getStartAddress(getServiceType(), getAreaType()),
                flex: isBoyuk(getServiceType()) ? 0 : flex,
                flex2: isBoyuk(getServiceType()) ? 1 : flex2,
                maxLineRight: maxLineAddress,
              ),

              titleValueView(
                "추가설명".tr(),
                valueRight: getStartComment(getServiceType()),
                flex: flex,
                flex2: flex2,
                maxLineRight: maxLineAddress,
              ),
              lDivider(color: color_d2d2d2, padding: padding_15_TB, thickness: 1.0),
              sb_h_15,

              //하원시 경우지
              if (getServiceType() == ServiceType.serviceType_1)
                if (StringUtils.validateString(data.jobItem.reqdata.bookingafterschool![0].pathroute_comment))
                  Column(
                    children: [
                      titleValueView(
                        "경유지장소".tr(),
                        rightWidget: lAutoSizeText(
                          getPathRoute(getServiceType()),
                          maxLines: maxLinePath,
                          style: st_b_16(),
                        ),
                        flex: flex,
                        flex2: flex2,
                      ),
                      lDivider(color: color_d2d2d2, padding: padding_15_TB, thickness: 1.0),
                      sb_h_15,
                    ],
                  ),

              //보육 경우지
              if (getServiceType() == ServiceType.serviceType_2)
                if (StringUtils.validateString(data.jobItem.reqdata.bookingboyuk![0].pathroute_comment))
                  Column(
                    children: [
                      titleValueView(
                        "경유지장소".tr(),
                        rightWidget: lAutoSizeText(
                          getPathRoute(getServiceType()),
                          maxLines: maxLinePath,
                          style: st_b_16(),
                        ),
                        flex: flex,
                        flex2: flex2,
                      ),
                    ],
                  ),

              if (!isBoyuk(getServiceType()))
                Column(
                  children: [
                    titleValueView(
                      "도착시간".tr(),
                      valueRight: getEndTime(getServiceType()),
                      flex: flex,
                      flex2: flex2,
                    ),
                    titleValueView(
                      "도착장소".tr(),
                      valueRight: getEndAddress(getServiceType(), getAreaType()),
                      flex: flex,
                      flex2: flex2,
                      maxLineRight: maxLineAddress,
                    ),
                    titleValueView(
                      "추가설명".tr(),
                      valueRight: getEndComment(getServiceType()),
                      flex: flex,
                      flex2: flex2,
                      maxLineRight: maxLineAddress,
                    ),
                  ],
                ),
              //이웃집 위치 1,2순위 표시 (이웃집일 경우만 표시 + 보육&이웃집)
              if (getAreaType() == PossibleArea.link_mom || isBoyuk(getServiceType()) && getAreaType() == PossibleArea.link_mom)
                Column(
                  children: [
                    lDivider(color: color_d2d2d2, padding: padding_15_TB, thickness: 1.0),
                    sb_h_15,
                    titleValueView(
                      "원하는링크쌤집위치_줄바꿈".tr(),
                      maxLine: 2,
                      rightWidget: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Row(
                            children: [
                              lText("1순위".tr(), style: st_b_16(textColor: color_main, fontWeight: FontWeight.bold)),
                              sb_w_05,
                              Expanded(
                                  child: lAutoSizeText(
                                getLocation(getServiceType(), getAreaType(), 1),
                                maxLines: maxLineAddress,
                                style: st_b_16(),
                              )),
                            ],
                          ),
                          sb_h_05,
                          Row(
                            children: [
                              lText("2순위".tr(), style: st_b_16(textColor: color_main, fontWeight: FontWeight.bold)),
                              sb_w_05,
                              Expanded(
                                  child: lAutoSizeText(
                                getLocation(getServiceType(), getAreaType(), 2),
                                maxLines: maxLineAddress,
                                style: st_b_16(),
                              )),
                            ],
                          )
                        ],
                      ),
                      flex: flex,
                      flex2: flex2,
                    ),
                  ],
                ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _addCareItemView({
    String title = '',
    ServiceTabType tabType = ServiceTabType.care,
    bool isEnable = false,
    bool isBadge = true,
    int length = 0,
    double size = 45,
  }) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: () async {
            var resultData = await Commons.page(context, routeParents6,
                arguments: ParentsStep6Page(
                  data: data,
                  viewMode: ViewMode(viewType: ViewType.modify),
                  tabType: tabType,
                ));
            if (resultData != null) {
              data.jobItem.reqdata = resultData;
              onOptionUpdate();
            }
          },
          child: Stack(
            alignment: Alignment.bottomRight,
            children: [
              Container(
                alignment: Alignment.center,
                padding: padding_10,
                decoration: BoxDecoration(
                  color: isEnable ? Commons.getColor().withOpacity(0.12) : color_eeeeee.withOpacity(0.42),
                  borderRadius: BorderRadius.circular(40),
                ),
                child: Lcons(
                  tabType.iconPath,
                  isEnabled: isEnable,
                  color: Commons.getColor(),
                  disableColor: color_dbdbdb,
                  size: size,
                ),
              ),
              if (isBadge)
                Container(
                  height: size,
                  width: size,
                  alignment: Alignment.bottomRight,
                  child: Badge(
                    elevation: 0,
                    badgeColor: isEnable ? Commons.getColor() : color_dbdbdb,
                    alignment: Alignment.bottomRight,
                    toAnimate: false,
                    badgeContent: lText('$length', style: st_15()),
                  ),
                ),
            ],
          ),
        ),
        sb_h_10,
        lAutoSizeText(
          title,
          style: st_b_16(fontWeight: FontWeight.w500),
        ),
      ],
    );
  }

  ///서비스 유형(등원,하원,보육...)
  ServiceType getServiceType() {
    return EnumUtils.getServiceType(index: data.jobItem.request.servicetype);
    // return getCareViewData().servicetype;
  }

  ///이웃집, 우리집
  PossibleArea getAreaType() {
    return EnumUtils.getPossibleArea(data.jobItem.request.possible_area);
    // return getCareViewData().possible_area;
  }

  ///돌봄날짜
  String getCareDate() {
    String type = '';
    try {
      // int dateCnt = data.jobItem.reqdata.careschedule[0].bookingdate.length;
      int dateCnt = data.jobItem.scheduleLength;
      String date = data.jobItem.reqdata.careschedule!.first.bookingdate.first;
      if (dateCnt > 1) {
        type = '${StringUtils.getStringToDate(date)} ${"외".tr()} ${dateCnt - 1}${"일".tr()}';
      } else {
        type = StringUtils.getStringToDate(date);
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getCareDate : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }

    return type;
  }

  ///돌봄시간
  String getCareTime() {
    String type = '';
    try {
      String sTime = StringUtils.getStringToTime(data.jobItem.reqdata.careschedule![0].stime);
      String eTime = StringUtils.getStringToTime(data.jobItem.reqdata.careschedule![0].etime);
      type = '$sTime ~ $eTime';
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getCareTime : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return type;
  }

  ///출발시간
  String getStartTime(ServiceType serviceType) {
    String type = '';
    try {
      if (serviceType == ServiceType.serviceType_0) {
        type = data.jobItem.reqdata.bookinggotoschool![0].start_time;
      } else if (serviceType == ServiceType.serviceType_1) {
        type = data.jobItem.reqdata.bookingafterschool![0].start_time;
      } else if (serviceType == ServiceType.serviceType_2) {
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}
      log.d('『GGUMBI』>>> getStartTime : type: $type, $serviceType <<< ');
      type = StringUtils.getStringToTime(type);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getStartTime : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return type;
  }

  ///이동수단
  String getMoveType(ServiceType serviceType) {
    StringBuffer st = StringBuffer();
    List<int> lsMove = [];
    String value = '';
    try {
      if (serviceType == ServiceType.serviceType_0) {
        lsMove = data.jobItem.reqdata.bookinggotoschool![0].product_id!;
      } else if (serviceType == ServiceType.serviceType_1) {
        lsMove = data.jobItem.reqdata.bookingafterschool![0].product_id!;
      } else if (serviceType == ServiceType.serviceType_2) {
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}

      log.d('『GGUMBI』>>> getMoveType : lsMove: $lsMove,  <<< ');

      lsMove.forEach((value) {
        if (value == MoveType.waking.index) {
          //도보
          st.write('${"도보".tr()}/');
        } else if (value == MoveType.car.index) {
          //자차
          st.write('${"자동차".tr()}/');
        } else if (value == MoveType.transport.index) {
          //대중교통
          st.write('${"대중교통".tr()}/');
        }
      });

      value = st.toString();
      if (value.length != 0 && value.contains("/")) {
        value = value.substring(0, value.toString().length - 1);
      }

      log.d('『GGUMBI』>>> getMoveType : type: $value, $serviceType <<< ');
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getMoveType : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  ///출발장소
  String getStartAddress(ServiceType serviceType, PossibleArea possibleArea) {
    String value = '';
    try {
      if (serviceType == ServiceType.serviceType_0) {
        //등원이고, 이웃집
        if (possibleArea == PossibleArea.link_mom) {
          value = "링크쌤이주소등록".tr();
        } else {
          value = '${data.jobItem.reqdata.bookinggotoschool![0].start_address} ${data.jobItem.reqdata.bookinggotoschool![0].start_address_detail}';
        }
      } else if (serviceType == ServiceType.serviceType_1) {
        value = '${data.jobItem.reqdata.bookingafterschool![0].start_address} ${data.jobItem.reqdata.bookingafterschool![0].start_address_detail}';
      } else if (serviceType == ServiceType.serviceType_2) {
        if (possibleArea == PossibleArea.link_mom) {
          value = "링크쌤이주소등록".tr();
        } else {
          value = '${data.jobItem.reqdata.bookingboyuk![0].boyuk_address} ${data.jobItem.reqdata.bookingboyuk![0].boyuk_address_detail}';
        }
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}

      log.d('『GGUMBI』>>> getStartAddress : type: $value, $serviceType, $possibleArea <<< ');
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getStartAddress : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  ///원하는 이웃집 위치 1순위, 2순위
  String getLocation(ServiceType serviceType, PossibleArea possibleArea, int location) {
    String value = '';
    try {
      if (serviceType == ServiceType.serviceType_0) {
        //등원이고, 이웃집
        if (possibleArea == PossibleArea.link_mom) {
          if (location == 2) {
            value = data.jobItem.reqdata.bookinggotoschool![0].nhn_area_2_address;
          } else {
            value = data.jobItem.reqdata.bookinggotoschool![0].nhn_area_1_address;
          }
        }
      } else if (serviceType == ServiceType.serviceType_1) {
        //등원이고, 이웃집
        if (possibleArea == PossibleArea.link_mom) {
          if (location == 2) {
            value = data.jobItem.reqdata.bookingafterschool![0].nhn_area_2_address;
          } else {
            value = data.jobItem.reqdata.bookingafterschool![0].nhn_area_1_address;
          }
        }
      } else if (serviceType == ServiceType.serviceType_2) {
        //등원이고, 이웃집
        if (possibleArea == PossibleArea.link_mom) {
          if (location == 2) {
            value = data.jobItem.reqdata.bookingboyuk![0].nhn_area_2_address;
          } else {
            value = data.jobItem.reqdata.bookingboyuk![0].nhn_area_1_address;
          }
        }
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}

      log.d('『GGUMBI』>>> getStartAddress : type: $value, $serviceType, $possibleArea, $location <<< ');
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getLocation : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  ///출발 추가설명
  String getStartComment(ServiceType serviceType) {
    String value = '';
    try {
      if (serviceType == ServiceType.serviceType_0) {
        value = data.jobItem.reqdata.bookinggotoschool![0].start_comment;
      } else if (serviceType == ServiceType.serviceType_1) {
        value = data.jobItem.reqdata.bookingafterschool![0].start_comment;
      } else if (serviceType == ServiceType.serviceType_2) {
        value = data.jobItem.reqdata.bookingboyuk![0].boyuk_comment;
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}

      log.d('『GGUMBI』>>> getStartComment : type: $value, $serviceType <<< ');
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getStartComment : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  ///하원시 경유지
  String getPathRoute(ServiceType serviceType) {
    String value = '';
    try {
      if (serviceType == ServiceType.serviceType_0) {
      } else if (serviceType == ServiceType.serviceType_1) {
        value = data.jobItem.reqdata.bookingafterschool![0].pathroute_comment;
      } else if (serviceType == ServiceType.serviceType_2) {
        value = data.jobItem.reqdata.bookingboyuk![0].pathroute_comment;
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}

      log.d('『GGUMBI』>>> getPathRoute : type: $value, $serviceType <<< ');
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getPathRoute : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  ///도착시간
  String getEndTime(ServiceType serviceType) {
    String type = '';
    try {
      if (serviceType == ServiceType.serviceType_0) {
        type = data.jobItem.reqdata.bookinggotoschool![0].end_time;
      } else if (serviceType == ServiceType.serviceType_1) {
        type = data.jobItem.reqdata.bookingafterschool![0].end_time;
      } else if (serviceType == ServiceType.serviceType_2) {
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}
      log.d('『GGUMBI』>>> getEndTime : type: $type, $serviceType <<< ');
      type = StringUtils.getStringToTime(type);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getEndTime : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return type;
  }

  ///도착장소
  String getEndAddress(ServiceType serviceType, PossibleArea possibleArea) {
    log.d('『GGUMBI』>>> getEndAddress : bookingafterschool: ${data.jobItem.reqdata.bookingafterschool},  <<< ');
    String value = '';

    try {
      if (serviceType == ServiceType.serviceType_0) {
        value = '${data.jobItem.reqdata.bookinggotoschool![0].end_address} ${data.jobItem.reqdata.bookinggotoschool![0].end_address_detail}';
      } else if (serviceType == ServiceType.serviceType_1) {
        //하원이고, 이웃집
        if (possibleArea == PossibleArea.link_mom) {
          value = "링크쌤이주소등록".tr();
        } else {
          value = '${data.jobItem.reqdata.bookingafterschool![0].start_address} ${data.jobItem.reqdata.bookingafterschool![0].start_address_detail}';
        }
      } else if (serviceType == ServiceType.serviceType_2) {
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}

      log.d('『GGUMBI』>>> getEndAddress : type: $value, $serviceType <<< ');
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getEndAddress : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  ///도착 추가설명
  String getEndComment(ServiceType serviceType) {
    String value = '';
    try {
      if (serviceType == ServiceType.serviceType_0) {
        value = data.jobItem.reqdata.bookinggotoschool![0].end_comment;
      } else if (serviceType == ServiceType.serviceType_1) {
        value = data.jobItem.reqdata.bookingafterschool![0].end_comment;
      } else if (serviceType == ServiceType.serviceType_2) {
      } else if (serviceType == ServiceType.serviceType_3) {
      } else if (serviceType == ServiceType.serviceType_5) {}

      log.d('『GGUMBI』>>> getEndComment : type: $value, $serviceType <<< ');
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getEndComment : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  ///필요한돌봄
  Widget _addCareView() {
    log.d('『GGUMBI』>>> _addCareView ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ _addCareView ${getServiceType()}<<< ');
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          lInkWell(
            onTap: () {
              onUpDate();
            },
            child: contentView(
                data: ContentData(
                  content: "필요한돌봄".tr(),
                ),
                onClick: (value) {
                  onUpDate();
                }),
          ),
          sb_h_15,
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _addCareItemView(
                title: "보육".tr(),
                tabType: ServiceTabType.care,
                length: data.jobItem.boyukCnt,
                isEnable: data.jobItem.isBoyuk,
              ),
              _addCareItemView(
                title: "가사".tr(),
                tabType: ServiceTabType.work,
                length: data.jobItem.homeCareCnt,
                isEnable: data.jobItem.isHomeCare,
              ),
              _addCareItemView(
                title: "놀이".tr(),
                tabType: ServiceTabType.play,
                length: data.jobItem.playCnt,
                isEnable: data.jobItem.isPlay,
              ),
              _addCareItemView(
                title: "투약".tr(),
                tabType: ServiceTabType.drug,
                length: data.jobItem.playCnt,
                isEnable: true,
                isBadge: false,
              ),
            ],
          ),
        ],
      ),
    );
  }

  ///보육,가사,놀이 플래그 업데이트
  onOptionUpdate() {
    data.jobItem.isBoyuk = false;
    data.jobItem.boyukCnt = 0;
    if (data.jobItem.reqdata.bookingboyukoptions!.length != 0) {
      data.jobItem.isBoyuk = true;
      data.jobItem.boyukCnt = data.jobItem.reqdata.bookingboyukoptions!.length;
    }
    log.d('『GGUMBI』>>> onOptionUpdate : : ${data.jobItem.isBoyuk},  <<< ');
    log.d('『GGUMBI』>>> onOptionUpdate : : ${data.jobItem.reqdata.bookingboyukoptions!.length},  <<< ');

    data.jobItem.isHomeCare = false;
    data.jobItem.homeCareCnt = 0;
    if (data.jobItem.reqdata.bookinghomecareoptions!.length != 0) {
      data.jobItem.isHomeCare = true;
      data.jobItem.homeCareCnt = data.jobItem.reqdata.bookinghomecareoptions!.length;
    }

    log.d('『GGUMBI』>>> onOptionUpdate : : ${data.jobItem.isHomeCare},  <<< ');
    log.d('『GGUMBI』>>> onOptionUpdate : : ${data.jobItem.reqdata.bookinghomecareoptions!.length},  <<< ');

    data.jobItem.isPlay = false;
    data.jobItem.playCnt = 0;
    if (data.jobItem.reqdata.bookingplayoptions!.length != 0) {
      data.jobItem.isPlay = true;
      data.jobItem.playCnt = data.jobItem.reqdata.bookingplayoptions!.length;
    }
    log.d('『GGUMBI』>>> onOptionUpdate : : ${data.jobItem.isPlay},  <<< ');
    log.d('『GGUMBI』>>> onOptionUpdate : : ${data.jobItem.reqdata.bookingplayoptions!.length},  <<< ');
    onUpDate();
  }

  ///동네 최근 인증 리스트 가져오기
  _requestAreaAddressView() async {
    await apiHelper.requestAreaAddressView().then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        data.listViewItem.setClear();
        data.listViewItem.addressResponse = response;
        onDataPage(response);
      }
    }).catchError((e) {});
  }
}
