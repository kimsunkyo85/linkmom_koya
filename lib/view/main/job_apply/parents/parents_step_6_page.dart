import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/req_data.dart';
import 'package:linkmom/data/providers/selete_model.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/data/storage/model/menu_file_data.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/job_apply/services/care_serivces_page.dart';
import 'package:linkmom/view/main/job_apply/services/drug_serivces_page.dart';
import 'package:linkmom/view/main/job_apply/services/housework_serivces_page.dart';
import 'package:linkmom/view/main/job_apply/services/play_serivces_page.dart';
import 'package:provider/provider.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../../../../route_name.dart';
import '../child_info_view.dart';
import '../content_view.dart';
import 'parents_step_7_page.dart';

class ParentsStep6Page extends BaseStateful {
  _ParentsStep6PageState createState() => _ParentsStep6PageState();

  ParentsStep6Page({this.data, this.viewMode, this.tabType, this.matchingStatus = 0, this.childInfo});

  final DataManager? data;
  final ViewMode? viewMode;
  final ServiceTabType? tabType;
  final int? matchingStatus;
  final ChildInfoItem? childInfo;
}

class _ParentsStep6PageState extends BaseStatefulState<ParentsStep6Page> with SingleTickerProviderStateMixin {
  ServiceTabType _tabType = ServiceTabType.care;
  int _index = ServiceTabType.care.value;

  late TabController _controller;
  late SelectModel _selectModel;

  final List<Tab> _tabs = <Tab>[
    Tab(text: ServiceTabType.care.name),
    Tab(text: ServiceTabType.work.name),
    Tab(text: ServiceTabType.play.name),
    Tab(text: ServiceTabType.drug.name),
  ];

  @override
  initState() {
    super.initState();
    log.d('『GGUMBI』>>> initState : widget.pageType: ${widget.tabType}, \nwidget.id: ${widget.matchingStatus}, <<< ');
    if (widget.tabType != null) {
      _tabType = widget.tabType!;
      _index = _tabType.value;
    }
    _controller = TabController(length: _tabs.length, vsync: this);
    _controller.index = _index;
    _controller.addListener(() {
      setState(() {
        _index = _controller.index;
        _tabType = EnumUtils.getServiceTabType(_index);
      });
    });

    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 6;

    ///수정일때는 STEP을 보여주지 않는다.
    if (getViewType() == ViewType.modify || getViewType() == ViewType.view) {
      data.jobItem.childInfo.isShow = false;
      data.jobItem.calcuTime = _data.jobItem.reqdata.careschedule!.isNotEmpty ? _data.jobItem.reqdata.careschedule!.first.durationtime : 0;
      data.jobItem.request = _data.jobItem.request.clone();
      data.jobItem.reqdata = _data.jobItem.reqdata.clone();

      ///도보 - 1, 자동차 - 2, 대중교통 - 3
      List<ServiceItem> serviceDatas = _data.jobItem.jobData![ServicesData.Key_transports] as List<ServiceItem>;
      data.jobItem.lsJobMoveType = Commons.makeTransports(serviceDatas);

      int bMinutes = 0;

      ///이동수단에 대한 기본 시간을 뺀다.(기본 30분...메뉴파일 참조)
      if (_data.jobItem.reqdata.bookingafterschool!.isNotEmpty || _data.jobItem.reqdata.bookinggotoschool!.isNotEmpty) {
        //이동수단
        ReqSchoolToHomeData item = ReqSchoolToHomeData();

        if (_data.jobItem.reqdata.bookingafterschool!.isNotEmpty) {
          item = _data.jobItem.reqdata.bookingafterschool!.first;
        } else if (_data.jobItem.reqdata.bookinggotoschool!.isNotEmpty) {
          item = _data.jobItem.reqdata.bookinggotoschool!.first;
        }

        data.jobItem.lsJobMoveType.forEach((moveItem) {
          item.product_id!.forEach((selectItem) {
            if (moveItem.type == selectItem) {
              ProductData item = moveItem.data as ProductData;
              // item.product_data.b_minutes;
              bMinutes = item.product_data!.b_minutes;
            }
          });
        });
        log.d('『GGUMBI』>>> onData : b_minutes: $bMinutes,  <<< ');
        data.jobItem.calcuTime = data.jobItem.calcuTime - bMinutes;
      }
    } else {
      data.jobItem.calcuTime = _data.jobItem.calcuTime;
      data.jobItem.request = _data.jobItem.request;
      data.jobItem.reqdata = _data.jobItem.reqdata;
    }

    data.jobItem.jobData = _data.jobItem.jobData;
    data.jobItem.tempResponse = _data.jobItem.tempResponse;
    data.jobItem.totalTime = _data.jobItem.totalTime;
    data.jobItem.scheduleLength = _data.jobItem.scheduleLength;

    log.d('『GGUMBI』>>> onData : : ${_data.jobItem.totalTime},  <<< ');
    // log.d('『GGUMBI』>>> build : onData: ${StringUtils.formatCalcuTime(data.jobItem.calcuTime, _selectModel.totalTime)}, ${data.jobItem.calcuTime}, ${_selectModel.totalTime} <<< ');

    // log.d('『GGUMBI』>>> onData : data.jobItem.jobData: ${data.jobItem.jobData![ServicesData.Key_boyuks]},  <<< ');
    // log.d('『GGUMBI』>>> onData : data.jobItem.jobData: ${data.jobItem.jobData![ServicesData.Key_homecares]},  <<< ');
    // log.d('『GGUMBI』>>> onData : data.jobItem.jobData: ${data.jobItem.jobData![ServicesData.Key_plays]},  <<< ');
    List<ServiceItem> servicesData = data.jobItem.jobData![ServicesData.Key_boyuks] as List<ServiceItem>;
    log.d('『GGUMBI』>>> onData : servicesData: $servicesData,  <<< ');
    if (servicesData.isNotEmpty /*!= null*/) {
      data.serviceItem.makeCareData(
        data,
        getViewType(),
        servicesData,
        viewData: getViewType() == ViewType.view ? _data.listViewDetailItem.careViewData!.careoptions!.bookingboyukoptions_product : [],
      );
    }

    servicesData = data.jobItem.jobData![ServicesData.Key_homecares] as List<ServiceItem>;
    if (servicesData.isNotEmpty /*!= null*/) {
      data.serviceItem.makeWorkData(
        data,
        getViewType(),
        servicesData,
        viewData: getViewType() == ViewType.view ? _data.listViewDetailItem.careViewData!.careoptions!.bookinghomecareoptions_product : [],
      );
    }

    servicesData = data.jobItem.jobData![ServicesData.Key_plays] as List<ServiceItem>;
    if (servicesData.isNotEmpty /*!= null*/) {
      data.serviceItem.makePlayData(
        data,
        getViewType(),
        servicesData,
        viewData: getViewType() == ViewType.view ? _data.listViewDetailItem.careViewData!.careoptions!.bookingplayoptions_product : [],
      );
    }
    selectModel.setTotalTimeReset();
    int totalTime = data.jobItem.calcuTotalTime(data, getViewType());
    WidgetsBinding.instance.addPostFrameCallback((_) {
      ///화면 생성후 setState 호출
      selectModel.setTotalTime(totalTime: totalTime);
    });
    log.d('『GGUMBI』>>> onData : totalTime: $totalTime,  <<< ');
    onDataPage(_data);
  }

  @override
  Widget build(BuildContext context) {
    // String time = '';
    _selectModel = Provider.of<SelectModel>(context, listen: true);
    String time = StringUtils.formatCalcuTime(data.jobItem.calcuTime, _selectModel.totalTime);
    // if (data.jobItem.totalTime <= _selectModel.totalTime) {
    //   showDlg(msg: "서비스선택_초과안내".tr());
    // }
    // log.d('『GGUMBI』>>> build ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
    log.d('『GGUMBI』>>> build : time: $time, ${data.jobItem.calcuTime}, ${_selectModel.totalTime} <<< ');
    return lModalProgressHUD(
      // _loading.isLoading,
      flag.isLoading,
      lScaffold(
        resizeToAvoidBottomPadding: false,
        appBar: jobAppBar(
          "필요한돌봄".tr(),
          // isBack: getViewType() == ViewType.modify ? false : true,
          isClose: getViewType() == ViewType.apply ? true : false,
          hide: getViewType() == ViewType.modify || getViewType() == ViewType.view ? false : true,
          onClick: () {
            if (getViewType() == ViewType.modify) {
              //스케줄 데이터 값 저장
              // setData();
              Commons.pagePop(context);
            } else {
              //스케줄 데이터 값 저장
              setData();
              Commons.showSaveTempDlg(data: data);
            }
          },
        ),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step),
            sb_h_10,
            if (getViewType() != ViewType.view)
              Column(
                children: [
                  //아이정보
                  Padding(
                    padding: padding_Item_LR,
                    child: childInfoView(data.jobItem.childInfo),
                  ),
                  Padding(
                    padding: padding_20_LR,
                    child: contentView(data: ContentData(content: "필요한돌봄_안내".tr())),
                  ),
                  Padding(
                    padding: padding_20_LR,
                    child: Row(
                      children: [
                        lText('${"돌봄이용시간".tr()} '),
                        lText(time, style: st_b_14(textColor: color_ff3b30, fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                  sb_h_10,
                ],
              ),
            Expanded(
              child: DefaultTabController(
                length: /*_tabs.length*/ 3,
                child: lScaffold(
                  appBar: PreferredSize(
                    preferredSize: Size.fromHeight(kToolbarHeight),
                    // TabBar 구현. 각 컨텐트를 호출할 탭들을 등록
                    child: Container(
                      height: 40,
                      margin: padding_10_B,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.0),
                        border: Border(bottom: BorderSide(color: color_f4f4f4)),
                      ),
                      child: Padding(
                        padding: padding_15_LR,
                        child: TabBar(
                          labelPadding: padding_10_LR,
                          indicatorColor: Commons.getColor(),
                          // indicatorSize: TabBarIndicatorSize.label,
                          unselectedLabelColor: color_b2b2b2,
                          labelColor: Commons.getColor(),
                          labelStyle: st_18(
                            fontWeight: FontWeight.bold,
                          ),
                          unselectedLabelStyle: st_18(
                            fontWeight: FontWeight.bold,
                          ),
                          controller: _controller,
                          tabs: _tabs,
                        ),
                      ),
                    ),
                  ),

                  // TabVarView 구현. 각 탭에 해당하는 컨텐트 구성
                  body: TabBarView(
                    controller: _controller,
                    children: [
                      CareServicesPage(data: data, viewMode: getViewMode()),
                      HouseWorkServicesPage(data: data, viewMode: getViewMode()),
                      PlayServicesPage(data: data, viewMode: getViewMode()),
                      DrugServicesPage(
                        data: data,
                        viewMode: getViewMode(),
                        matchingStatus: widget.matchingStatus,
                        childInfo: widget.childInfo,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            if (getViewType() != ViewType.view) _confirmButton(),
            // _confirmButton(),
          ],
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn(_bottomBtnTitle(getViewType(), _tabType, widget.matchingStatus!), /*isEnabled: flag.isConfirm,*/ onClickAction: () {
      setData();
      if (getViewType() == ViewType.apply) {
        Commons.pageToMain(context, routeParents7, arguments: ParentsStep7Page(data: data, viewMode: ViewMode()));
      } else {
        Commons.pagePop(context, data: data.jobItem.reqdata);
      }
    });
  }

  ///하단 버튼 공통화 체크
  static String _bottomBtnTitle(ViewType viewType, ServiceTabType tabType, int matchingStatus) {
    String title = '';
    if (viewType == ViewType.modify) {
      title = tabType == ServiceTabType.drug ? "확인".tr() : "저장".tr();
    } else if (viewType == ViewType.view) {
      if (tabType == ServiceTabType.drug) {
        title = matchingStatus == MatchingStatus.paid.value
            ? Commons.isLinkMom()
                ? "돌봄일기바로가기".tr()
                : "투약의뢰서바로가기".tr()
            : "확인".tr();
      } else {
        title = "확인".tr();
      }
    } else {
      title = "다음".tr();
    }
    return title;
  }

  @override
  void onDataPage(_data) {
    var item = _data as DataManager;
    log.d('『GGUMBI』>>> onDataPage : item: $item,  <<< ');

    if (_data == null) {
      return;
    }

    log.d('『GGUMBI』>>> onDataPage : : ${data.jobItem.tempResponse},  <<< ');
    // if (data.jobItem.tempResponse.getDataList() == null || data.jobItem.tempResponse.getDataList().isNotEmpty) {
    if (data.jobItem.tempResponse.getDataList().isNotEmpty) {
      // data.jobItem.tempResponse = TempResponse();
      return;
    }
    // onConfirmBtn();
  }

  ///보육.가사,놀이 서비스 만들기
  void setData() {
    ///보육서비스
    data.jobItem.reqdata.bookingboyukoptions = [];
    data.serviceItem.lsBoyuk.forEach((care) {
      care.values!.forEach((item) {
        if (item.isSelected) {
          data.jobItem.reqdata.bookingboyukoptions!.add(ReqCareOptionData(product_id: item.products!.product_id, product_name: item.products!.product_data!.name, product_cost: item.products!.product_data!.b_cost, product_comment: ''));
          log.d('『GGUMBI』>>> _confirmButton : item: ${item.isSelected} ${item.products},  <<< ');
        }
      });
    });

    ///가사서비스
    data.jobItem.reqdata.bookinghomecareoptions = [];
    data.serviceItem.lsHomeCare.forEach((care) {
      care.values!.forEach((item) {
        if (item.isSelected) {
          data.jobItem.reqdata.bookinghomecareoptions!.add(ReqCareOptionData(product_id: item.products!.product_id, product_name: item.products!.product_data!.name, product_cost: item.products!.product_data!.b_cost, product_comment: ''));
          log.d('『GGUMBI』>>> _confirmButton : item: ${item.isSelected} ${item.products},  <<< ');
        }
      });
    });

    ///놀이서비스
    data.jobItem.reqdata.bookingplayoptions = [];
    data.serviceItem.lsPlay.forEach((care) {
      care.values!.forEach((item) {
        if (item.isSelected) {
          data.jobItem.reqdata.bookingplayoptions!.add(ReqCareOptionData(product_id: item.products!.product_id, product_name: item.products!.product_data!.name, product_cost: item.products!.product_data!.b_cost, product_comment: ''));
          log.d('『GGUMBI』>>> _confirmButton : item: ${item.isSelected} ${item.products},  <<< ');
        }
      });
    });
  }
}
