import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/temp_response.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/listview/list_type_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../child_info_view.dart';
import '../content_view.dart';
import 'parents_step_4_page.dart';

class ParentsStep3Page extends BaseStateful {
  @override
  _ParentsStep3PageState createState() => _ParentsStep3PageState();

  ParentsStep3Page({this.data, this.viewMode});

  final DataManager? data;
  final ViewMode? viewMode;
}

class _ParentsStep3PageState extends BaseStatefulState<ParentsStep3Page> {
  String title = '';

  ///임시저장한값 플래그(등원시 장소 변경시 안내 팝업용)
  bool isTemp = false;

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 3;
    data.jobItem.request = _data.jobItem.request;
    data.jobItem.reqdata = _data.jobItem.reqdata;
    data.jobItem.serviceType = EnumUtils.getServiceType(index: data.jobItem.request.servicetype);
    data.jobItem.makeData(serviceType: data.jobItem.serviceType);
    data.jobItem.tempResponse = _data.jobItem.tempResponse;
    title = "돌봄장소_선택".tr();
    // log.d('『GGUMBI』>>> onDataPage : data.jobItem.request: ${data.jobItem.reqdata},  <<< ');
    // log.d('『GGUMBI』>>> onDataPage : data.jobItem.tempResponse: ${data.jobItem.tempResponse},  <<< ');

    onDataPage(_data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        resizeToAvoidBottomPadding: false,
        appBar: jobAppBar(
          "돌봄장소".tr(),
          hide: true,
          onClick: () {
            Commons.showSaveTempDlg(data: data);
          },
        ),
        body: lContainer(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step),
              Expanded(
                child: Column(
                  children: [
                    Expanded(
                      child: lScrollView(
                        child: Container(
                          width: double.infinity,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              childInfoView(data.jobItem.childInfo),
                              contentView(
                                  data: ContentData(content: title),
                                  onClick: () {
                                    // log.d('『GGUMBI』>>> build ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
                                  }),
                              sb_h_20,
                              ListTypeSelector(
                                data: data.jobItem.lsJobArea,
                                serviceType: data.jobItem.serviceType,
                                onItemClick: (value) {
                                  SingleItem item = value;

                                  ///임시저장 값이 있고, 저장한 값과 다를 경우만 알려준다.
                                  if (isTemp) {
                                    if (data.jobItem.tempData.getData().possible_area != value.type) {
                                      showNormalDlg(
                                          message: "장소변경안내".tr(),
                                          btnLeft: "아니오".tr(),
                                          btnRight: "예".tr(),
                                          onClickAction: (action) {
                                            if (DialogAction.yes == action) {
                                              data.jobItem.request.possible_area = item.type;
                                              data.jobItem.jobData = Commons.getJobData(data.jobItem.request.servicetype, data.jobItem.request.possible_area);
                                              data.jobItem.tempResponse = TempResponse();
                                              onConfirmBtn();
                                            } else {
                                              data.jobItem.lsJobArea.forEach((lsValue) {
                                                if (item.type == lsValue.type) {
                                                  lsValue.isSelected = false;
                                                } else {
                                                  lsValue.isSelected = true;
                                                }
                                              });
                                              data.jobItem.tempResponse = TempResponse.clone(data.jobItem.tempData);
                                              onConfirmBtn();
                                            }
                                          });
                                      return;
                                    }
                                    data.jobItem.tempResponse = TempResponse.clone(data.jobItem.tempData);
                                  }

                                  data.jobItem.request.possible_area = item.type;
                                  data.jobItem.jobData = Commons.getJobData(data.jobItem.request.servicetype, data.jobItem.request.possible_area);
                                  onConfirmBtn();
                                },
                              ),
                              sb_h_30,
                            ],
                          ),
                        ),
                      ),
                    ),
                    _confirmButton(),
                  ],
                ),
              ),
              // wdDivider(),
            ],
          ),
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn("다음".tr(), isEnabled: flag.isConfirm, onClickAction: () {
      Commons.page(context, routeParents4, arguments: ParentsStep4Page(data: data, viewMode: ViewMode()));
    });
  }

  @override
  void onConfirmBtn() {
    bool isHome = false;
    data.jobItem.lsJobArea.asMap().forEach((index, value) {
      if (value.isSelected) {
        isHome = true;
        data.jobItem.request.possible_area = index + 1;
        return;
      }
    });

    if (isHome) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  @override
  void onDataPage(_data) {
    var item = _data as DataManager;
    isTemp = false;
    if (_data == null) {
      return;
    }

    // log.d('『GGUMBI』>>> onDataPage : : ${data.jobItem.tempResponse},  <<< ');
    // log.d('『GGUMBI』>>> onDataPage : : ${data.jobItem.tempResponse.getDataList()},  <<< ');
    if (data.jobItem.tempResponse.getDataList().isEmpty) {
      return;
    }

    //돌봄유형 등원, 하원, 보육, 학습, 입주가사, 이유식.getData()
    data.jobItem.lsJobArea.asMap().forEach((index, value) {
      if (index == data.jobItem.tempResponse.getData().possible_area - 1) {
        isTemp = true;
        value.isSelected = true;
        data.jobItem.request.possible_area = data.jobItem.tempResponse.getData().possible_area;
        data.jobItem.tempData = TempResponse.clone(data.jobItem.tempResponse);
      } else {
        value.isSelected = false;
      }
    });

    data.jobItem.jobData = Commons.getJobData(data.jobItem.request.servicetype, data.jobItem.request.possible_area);
    onConfirmBtn();
  }
}
