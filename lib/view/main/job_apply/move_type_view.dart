import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

import '../../../main.dart';

///도보,자차,대중교통 컨텐츠 뷰
Widget moveTypeView(SingleItem data) {
  log.d('『GGUMBI』>>> moveTypeView : data.content: ${data.content},  <<< ');
  return Column(
    children: [
      if (StringUtils.validateString(data.content))
        lRoundContent(
          padding: padding_15,
          textColor: color_222222,
          bgColor: color_eeeeee.withOpacity(0.38),
          borderColor: Colors.transparent,
          content1: data.content,
          content2: data.values![0].name,
          content3: data.values![1].name == "0" ? "" : '(+${StringUtils.formatPay(int.parse(data.values![1].name))}${"원".tr()})',
          fontSize: 14.0,
        ),
      if (StringUtils.validateString(data.content)) sb_h_17,
    ],
  );
}
