import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

///진행상황, 아이정보 뷰 - 36sp -> 20 돌봄신청 컨텐츠 제목(피룡한 돌봄 유형을 선택해주세요)
Widget contentView({ContentData? data, Widget? rightIcon, Function? onClick}) {
  if (data == null) {
    data = ContentData();
  }

  if (!StringUtils.validateString(data.modify)) {
    data.modify = "수정".tr();
  }

  if (data.modifyStyle == null) {
    data.modifyStyle = st_15(textColor: Commons.getColor());
  }

  return Column(
    // crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Row(
        children: [
          if (data.isInfo)
            Padding(
              padding: padding_05_R,
              child: lInkWell(
                onTap: () {
                  if (onClick != null) {
                    onClick();
                  }
                },
                child: Lcons.info(
                  color: color_main,
                  isEnabled: true,
                ),
              ),
            ),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                lAutoSizeText(
                  data.content!,
                  style: data.contentStyle == null ? st_b_20(fontWeight: FontWeight.w700) : data.contentStyle,
                  textAlign: TextAlign.start,
                  maxLines: 1,
                  minFontSize: 5,
                ),
                if (data.rightWidget != null) data.rightWidget!,
                if (data.rightWidget == null)
                  if (StringUtils.validateString(data.icon))
                    data.icon!.contains('svg')
                        ? InkWell(
                            onTap: () {
                              if (onClick != null) {
                                onClick();
                              }
                            },
                            child: Lcons(data.icon!, disableColor: color_cecece, size: 25))
                        : Row(
                            children: [
                              sb_w_02,
                              InkWell(
                                onTap: () {
                                  onClick!();
                                },
                                child: Lcons(data.icon!, disableColor: color_cecece, size: 25),
                              ),
                              sb_w_02,
                              // lText(
                              //   data?.content,
                              //   style: st_b_14(textColor: color_767676),
                              //   textAlign: TextAlign.center,
                              // ),
                            ],
                          ),
                if (data.rightWidget == null)
                  if (data.isOverLapRight) lTextIcon(title: "(중복가능)".tr(), textStyle: st_14(textColor: color_b2b2b2), rightIcon: rightIcon),
              ],
            ),
          ),
          if (data.isModify)
            lInkWell(
              splashColor: color_transparent,
              highlightColor: color_transparent,
              onTap: () {
                if (onClick != null) {
                  onClick();
                }
              },
              child: Padding(
                padding: padding_30_L,
                child: lText(
                  data.modify!,
                  style: data.modifyStyle,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          if (data.isExpanded)
            lInkWell(
              onTap: () {
                if (onClick != null) {
                  data!.isVisible = !data.isVisible;
                  onClick(data.isVisible);
                }
              },
              child: Padding(
                padding: padding_10_L,
                child: Lcons.upDown(data.isVisible),
              ),
            ),
        ],
      ),
      if (data.isOverLap)
        Container(
          alignment: Alignment.topLeft,
          child: lText(
            '중복선택이 가능합니다.',
            style: childStyle(),
          ),
        ),
    ],
  );
}

class ContentData {
  ContentData({
    this.content,
    this.contentStyle,
    this.rightWidget,
    this.data,
    this.isInfo = false,
    this.isModify = false,
    this.isOverLap = false,
    this.isOverLapRight = false,
    this.isExpanded = false,
    this.isVisible = true,
    this.icon,
    this.iconColor,
    this.modify,
    this.modifyStyle,
  });

  ContentData.init(
    this.content,
    this.contentStyle,
    this.rightWidget,
    this.data,
    this.isInfo,
    this.isModify,
    this.isOverLap,
    this.isOverLapRight,
    this.isExpanded,
    this.isVisible,
    this.icon,
    this.iconColor,
    this.modify,
    this.modifyStyle,
  ) {
    modify = "수정".tr();
    modifyStyle = st_modify;
    isInfo = false;
    isModify = false;
    isOverLap = false;
    isOverLapRight = false;
    isExpanded = false;
    isVisible = true;
    iconColor = Commons.getColor();
  }

  String? content;
  TextStyle? contentStyle;
  List<ContentInfoItem>? data;
  bool isInfo;
  bool isModify;
  bool isOverLap;
  bool isOverLapRight;
  bool isExpanded;
  bool isVisible;
  String? icon;
  Color? iconColor;
  String? modify;
  TextStyle? modifyStyle;
  Widget? rightWidget;

  @override
  String toString() {
    return 'ContentData{content: $content, data: $data, isInfo: $isInfo, isModify: $isModify, isOverLap: $isOverLap, isOverLapRight: $isOverLapRight, isExpanded: $isExpanded, isVisible: $isVisible, icon: $icon, contentStyle: $contentStyle, rightWidget: $rightWidget}';
  }
}

class ContentInfoItem {
  ContentInfoItem({this.name, this.info});

  String? name;
  String? info;

  @override
  String toString() {
    return 'ContentInfoItem{name: $name, info: $info}';
  }
}
