import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

///제목, 오른쪽 추가 설명
Widget titleValueView(
  title, {
  FontWeight fontWeight = FontWeight.bold,
  Color color = color_222222,
  TextAlign? textAlign,
  TextStyle? style,
  int maxLine = 1,
  String? valueLeft,
  FontWeight fontWeightLeft = FontWeight.normal,
  Color colorLeft = color_main,
  TextAlign textAlignLeft = TextAlign.left,
  TextStyle? styleLeft,
  int maxLineLeft = 1,
  FontWeight fontWeight2 = FontWeight.normal,
  Color color2 = color_main,
  String? value2,
  TextAlign textAlign2 = TextAlign.left,
  TextStyle? style2,
  int maxLine2 = 1,
  String? valueRight,
  FontWeight fontWeightRight = FontWeight.normal,
  Color colorRight = color_222222,
  bool isRightExpanded = true,
  TextAlign? textAlignRight,
  TextStyle? styleRight,
  int maxLineRight = 1,
  String? valueRight2,
  FontWeight fontWeightRight2 = FontWeight.w500,
  Color colorRight2 = color_222222,
  TextAlign? textAlignRight2,
  TextStyle? styleRight2,
  int maxLineRight2 = 1,
  String? valueBottom,
  FontWeight fontWeightBottom = FontWeight.normal,
  Color colorBottom = color_main,
  TextAlign? textAlignBottom,
  TextStyle? styleBottom,
  int maxLineBottom = 1,
  bottomAlign = Alignment.centerRight,
  padding = padding_15_B,
  Widget? rightWidget,
  flex = 1,
  flex2 = 1,
}) {
  return Padding(
    padding: padding,
    child: Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: flex,
              //제목
              child: Row(
                children: [
                  if (StringUtils.validateString(valueLeft))
                    lText(
                      valueLeft!,
                      style: styleLeft != null ? styleLeft : st_14(textColor: colorLeft, fontWeight: fontWeightLeft),
                      maxLines: maxLineLeft,
                      textAlign: textAlignLeft,
                    ),
                  lText(
                    title,
                    style: style != null ? style : st_16(textColor: color, fontWeight: fontWeight),
                    maxLines: maxLine,
                    textAlign: textAlign,
                  ),
                  if (StringUtils.validateString(value2))
                    lText(
                      value2!,
                      style: style2 != null ? style2 : st_14(textColor: color2, fontWeight: fontWeight2),
                      maxLines: maxLine2,
                      textAlign: textAlign2,
                    ),
                ],
              ),
            ),
            // sb_w_10,
            if (rightWidget != null)
              Expanded(
                flex: flex2,
                child: rightWidget,
              ),
            if (StringUtils.validateString(valueRight))
              Expanded(
                  flex: flex2,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    // crossAxisAlignment: isRightExpanded ? CrossAxisAlignment.center : CrossAxisAlignment.start,
                    mainAxisAlignment: isRightExpanded ? MainAxisAlignment.center : MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if (isRightExpanded)
                        //돌봄신청(결제금액에서 사용)
                        Expanded(
                          //오른쪽 가격
                          child: lAutoSizeText(
                            valueRight!,
                            style: styleRight != null ? styleRight : st_b_16(fontWeight: fontWeightRight, textColor: colorRight),
                            maxLines: maxLineRight,
                            textAlign: textAlignRight,
                          ),
                        ),
                      if (isRightExpanded && StringUtils.validateString(valueRight2))
                        lAutoSizeText(
                          valueRight2!,
                          style: styleRight2 != null ? styleRight2 : st_b_14(fontWeight: fontWeightRight2, textColor: colorRight2),
                          maxLines: maxLineRight2,
                          textAlign: textAlignRight2,
                        ),
                      if (!isRightExpanded)
                        //결제하기에서 사용
                        lAutoSizeText(
                          valueRight!,
                          style: styleRight != null ? styleRight : st_b_16(fontWeight: fontWeightRight, textColor: colorRight),
                          maxLines: maxLineRight,
                          textAlign: textAlignRight,
                        ),
                      if (!isRightExpanded && StringUtils.validateString(valueRight2))
                        Expanded(
                          child: lAutoSizeText(
                            valueRight2!,
                            style: styleRight2 != null ? styleRight2 : st_b_14(fontWeight: fontWeightRight2, textColor: colorRight2),
                            maxLines: maxLineRight2,
                            textAlign: textAlignRight2,
                          ),
                        ),
                    ],
                  )),
          ],
        ),
        if (StringUtils.validateString(valueBottom))
          Row(
            children: [
              Expanded(
                child: lText(""),
                flex: flex,
              ),
              sb_w_10,
              Expanded(
                flex: flex2,
                child: Container(
                  alignment: bottomAlign,
                  //거리 범위 값
                  child: lText(
                    valueBottom!,
                    style: styleBottom != null ? styleBottom : st_16(textColor: colorBottom, fontWeight: fontWeightBottom),
                    maxLines: maxLineBottom,
                    textAlign: textAlignBottom,
                  ),
                ),
              ),
            ],
          ),
      ],
    ),
  );
}
