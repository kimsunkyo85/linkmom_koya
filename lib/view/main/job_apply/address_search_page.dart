import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/addr_keyword_response.dart';
import 'package:linkmom/data/network/models/addr_search_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/lcons.dart';

class AddressSearchPage extends BaseStateful {
  final ViewType viewType;

  AddressSearchPage({this.viewType = ViewType.normal});

  @override
  _AddressSearchPageState createState() => _AddressSearchPageState();
}

class _AddressSearchPageState extends BaseStatefulState<AddressSearchPage> {
  AddrKeywordResponse _keywordList = AddrKeywordResponse(documents: []);
  AddrSearchResponse _addressList = AddrSearchResponse(documents: []);
  TextEditingController _tc = TextEditingController();
  bool _enableKeywordApi = true;
  FocusNode _focus = FocusNode();
  int _currentPage = 0;
  String _search = '';

  @override
  void initState() {
    super.initState();
    _keywordList = AddrKeywordResponse(documents: [], meta: Meta());
    _addressList = AddrSearchResponse(documents: [], meta: Meta());
    _enableKeywordApi = widget.viewType != ViewType.summary;
  }

  List<dynamic> get _getAddrList => _enableKeywordApi ? _keywordList.documents! : _addressList.documents!;

  Meta get _getMeta => _enableKeywordApi ? _keywordList.meta! : _addressList.meta!;

  @override
  Widget build(BuildContext context) {
    return lScaffold(
      appBar: appBar("주소검색".tr(), hide: false),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: padding_20,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (widget.viewType != ViewType.summary)
                    lIconText(
                      title: "장소로검색하기".tr(),
                      isExpanded: false,
                      icon: Lcons.radio_button(isEnabled: _enableKeywordApi, color: color_main, disableColor: color_999999),
                      textStyle: st_b_16(),
                      onClick: () {
                        _enableKeywordApi = true;
                        _getAddrList.clear();
                        _search = _tc.text;
                        onUpDate();
                      },
                    ),
                  lIconText(
                    title: "주소로검색하기".tr(),
                    isExpanded: false,
                    icon: Lcons.radio_button(isEnabled: !_enableKeywordApi, color: color_main, disableColor: color_999999),
                    textStyle: st_b_16(),
                    onClick: () {
                      _enableKeywordApi = false;
                      _getAddrList.clear();
                      _search = _tc.text;
                      onUpDate();
                    },
                  ),
                  sb_w_10,
                ],
              ),
            ),
            Container(
              padding: padding_20_LRB,
              decoration: BoxDecoration(border: Border(bottom: BorderSide(width: 8, color: color_e5e5ea.withOpacity(0.38)))),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Flexible(
                      child: Container(
                    child: TextField(
                      focusNode: _focus,
                      cursorHeight: 16,
                      cursorColor: color_main,
                      textAlignVertical: TextAlignVertical.center,
                      decoration: InputDecoration(
                        hintText: _enableKeywordApi ? "장소검색예시".tr() : "주소검색예시".tr(),
                        hintStyle: st_16(textColor: color_b2b2b2),
                        contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                        suffixIcon: Container(
                          width: 100,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              if (_tc.text.isNotEmpty)
                                lInkWell(
                                  onTap: () {
                                    _tc.clear();
                                    _search = '';
                                    onUpDate();
                                  },
                                  child: Lcons.clear(),
                                ),
                              lInkWell(
                                onTap: () {
                                  _doSearch();
                                },
                                child: Container(
                                  padding: padding_15,
                                  child: Lcons.search(color: color_999999),
                                ),
                              ),
                            ],
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: color_dedede), borderRadius: BorderRadius.circular(6)),
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: color_dedede), borderRadius: BorderRadius.circular(6)),
                      ),
                      controller: _tc,
                      onChanged: (value) => onUpDate(),
                      onEditingComplete: () {
                        _doSearch();
                      },
                    ),
                  )),
                ],
              ),
            ),
            _search.isNotEmpty
                ? Expanded(
                    child: ListNextView(
                      emptyView: Container(
                        height: Commons.height(context, 0.5),
                        padding: padding_20,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            lEmptyView(title: "정확한주소를입력하셨나요".tr(), padding: padding_0),
                            _guide(),
                          ],
                        ),
                      ),
                      showNextLoading: !_getMeta.isEnd,
                      onRefresh: () => _requestSearch(_tc.text),
                      onNext: () => _requestSearch(_tc.text, page: _currentPage, isNext: true),
                      view: _buildAddress(_getAddrList),
                    ),
                  )
                : _guide(),
          ],
        ),
      ),
    );
  }

  List<Widget> _buildAddress(dynamic lists) {
    if (lists != null) {
      return lists.map<Widget>((e) => _addressWidget(e)).toList();
    } else {
      return [];
    }
  }

  void _requestSearch(String key, {int page = 1, bool isNext = false}) {
    if (isNext && _getMeta.isEnd) return;
    _search = key;
    if (_enableKeywordApi) {
      _requestAddrSearchFromKeyword(key, page: page, isNext: isNext);
    } else {
      _requestAddrSearch(key, page: page, isNext: isNext);
    }
  }

  void _requestAddrSearch(String key, {int page = 1, bool isNext = false}) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestAddrSearch(key, page: page).then((response) {
        if (!isNext) {
          _addressList = response;
        } else {
          _addressList.documents!.addAll(response.documents ?? []);
          _addressList.meta = response.meta;
        }
        _currentPage++;
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        log.e('_requestAddrSearch >>>>>>>>>>>> e');
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }

  void _requestAddrSearchFromKeyword(String key, {int page = 1, bool isNext = false}) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestAddrSearchFromKeyword(key, page: page).then((response) {
        if (!isNext) {
          _keywordList = response;
        } else {
          _keywordList.documents!.addAll(response.documents ?? []);
          _keywordList.meta = response.meta;
        }
        _currentPage++;
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        log.e('_requestAddrSearch >>>>>>>>>>>> e');
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }

  Future<Documents> _requestAddrCoordinate(Documents addr) async {
    try {
      flag.enableLoading(fn: () => onUpDate());
      var response = await apiHelper.requestAddrCoordinate(double.tryParse(addr.address!.x) ?? 0, double.tryParse(addr.address!.y) ?? 0).catchError((e) {
        log.e('_requestAddrCoordinate >>>>>>>>>>>> e');
        flag.disableLoading(fn: () => onUpDate());
      });
      flag.disableLoading(fn: () => onUpDate());
      if (response.documents!.isNotEmpty) {
        addr.address!.hCode = response.documents!.firstWhere((e) => e.regionType == 'H').code;
        addr.address!.bCode = response.documents!.firstWhere((e) => e.regionType == 'B').code;
        addr.address!.region3DepthHName = response.documents!.firstWhere((e) => e.regionType == 'H').region3DepthName;
        addr.address!.region3DepthName = response.documents!.firstWhere((e) => e.regionType == 'B').region3DepthName;
        if (addr.address!.region3DepthHName != addr.address!.region3DepthName) {
          addr.address!.region3DepthName = addr.address!.region3DepthName + (addr.address!.region3DepthHName.isNotEmpty ? ' (${addr.address!.region3DepthHName})' : '');
        }
      }
      return addr;
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
      return addr;
    }
  }

  Widget _addressWidget(dynamic addr) {
    if (addr is KeywordDocuments) {
      return lInkWell(
        onTap: () => _getAddressCode(addr.roadAddressName.isNotEmpty ? addr.roadAddressName : addr.addressName).then((resp) {
          if (resp != null) Commons.pagePop(context, data: resp);
        }),
        child: Container(
            decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
            padding: padding_20,
            alignment: Alignment.centerLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextHighlight(
                  text: addr.placeName,
                  term: _tc.text,
                  textStyle: st_b_16(),
                  textStyleHighlight: st_16(textColor: color_main),
                ),
                if (addr.roadAddressName.isNotEmpty) _addr("도로명".tr(), addr.roadAddressName, addr.placeName),
                if (addr.addressName.isNotEmpty) _addr("지번".tr(), addr.addressName, ''),
              ],
            )),
      );
    } else if (addr is Documents) {
      return lInkWell(
        onTap: () {
          if (_isValid(addr)) {
            Commons.pagePop(context, data: addr);
          } else {
            _requestAddrCoordinate(addr).then((updatedAddr) => Commons.pagePop(context, data: updatedAddr));
          }
        },
        child: Container(
            decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
            padding: padding_20,
            alignment: Alignment.centerLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (addr.roadAddress!.addressName.isNotEmpty) _addr("도로명".tr(), addr.roadAddress!.addressName, addr.roadAddress!.buildingName, addrColor: color_222222, highlight: _get3DepthName(addr)),
                if (addr.address!.addressName.isNotEmpty) _addr("지번".tr(), addr.address!.addressName, '', addrColor: color_222222, highlight: _get3DepthName(addr)),
              ],
            )),
      );
    } else {
      return Container();
    }
  }

  Widget _addr(String type, String addr, String buildingName, {Color addrColor = color_999999, String highlight = ''}) {
    return Container(
      margin: padding_05_T,
      child: Row(children: [
        Container(
          width: 42,
          alignment: Alignment.center,
          decoration: BoxDecoration(border: Border.all(color: color_dedede)),
          padding: padding_04,
          child: lText(type, style: st_12(textColor: color_999999)),
        ),
        sb_w_05,
        _enableKeywordApi
            ? Container(width: Commons.width(context, 0.7), child: lText(addr, style: st_14(textColor: addrColor), maxLines: 2))
            : TextHighlight(
                text: addr + " ${buildingName.isNotEmpty ? '(' + buildingName + ')' : ''}",
                term: highlight,
                textStyle: st_14(textColor: addrColor),
                textStyleHighlight: st_14(textColor: color_main),
              ),
      ]),
    );
  }

  String _get3DepthName(Documents addr) {
    if (widget.viewType == ViewType.summary)
      return addr.address!.region3DepthHName.isNotEmpty ? addr.address!.region3DepthHName : addr.address!.region3DepthName;
    else
      return '';
  }

  void _doSearch() {
    if (_tc.text.isNotEmpty) {
      _requestSearch(_tc.text);
      focusClear(context);
    } else {
      Commons.showToast("검색어를입력해주세요".tr());
    }
  }

  Widget _guide() {
    return Container(
      padding: padding_20,
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        lText("주소입력_안내1".tr(), style: st_b_14(fontWeight: FontWeight.w500)),
        sb_h_03,
        _enableKeywordApi
            ? Padding(
                padding: padding_05_T,
                child: RichText(
                    text: TextSpan(children: [
                  TextSpan(text: "주소입력_안내_키워드1".tr(), style: st_b_13()),
                  TextSpan(text: "주소입력_안내_키워드2".tr(), style: st_b_13(textColor: color_b2b2b2)),
                ])),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  sb_h_05,
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(text: "주소입력_안내_주소1".tr(), style: st_b_13()),
                    TextSpan(text: "주소입력_안내_주소2".tr(), style: st_b_13(fontWeight: FontWeight.w500)),
                    TextSpan(text: "주소입력_안내_주소3".tr(), style: st_b_13(textColor: color_b2b2b2)),
                  ])),
                  sb_h_03,
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(text: "주소입력_안내_주소4".tr(), style: st_b_13()),
                    TextSpan(text: "주소입력_안내_주소5".tr(), style: st_b_13(fontWeight: FontWeight.w500)),
                    TextSpan(text: "주소입력_안내_주소6".tr(), style: st_b_13(textColor: color_b2b2b2)),
                  ]))
                ],
              ),
      ]),
    );
  }

  Future<Documents?> _getAddressCode(String key) async {
    var response = await apiHelper.requestAddrSearch(key).catchError((e) {
      log.e('_requestAddrSearch >>>>>>>>>>>> $e');
      flag.disableLoading(fn: () => onUpDate());
    });
    if (!_isValid(response.documents!.first)) {
      return await _requestAddrCoordinate(response.documents!.first);
    } else {
      return response.documents!.first;
    }
  }

  bool _isValid(Documents addr) {
    if (addr.address!.bCode.isEmpty || addr.address!.hCode.isEmpty) {
      return false;
    } else {
      return true;
    }
  }
}
