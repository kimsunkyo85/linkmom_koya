import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/block_requests.dart';
import 'package:linkmom/data/network/models/chat_message_list_request.dart';
import 'package:linkmom/data/network/models/chat_message_list_response.dart';
import 'package:linkmom/data/network/models/chat_message_send_request.dart';
import 'package:linkmom/data/network/models/chat_room_delete_request.dart';
import 'package:linkmom/data/network/models/chat_room_init_request.dart';
import 'package:linkmom/data/network/models/chat_room_view_request.dart';
import 'package:linkmom/data/network/models/data/chat_room_data.dart';
import 'package:linkmom/data/network/models/data/chat_send_data.dart';
import 'package:linkmom/data/network/models/data/matching_request_data.dart';
import 'package:linkmom/data/network/models/data/pay_cancel_save_explain_data.dart';
import 'package:linkmom/data/network/models/matching_cancel_request.dart';
import 'package:linkmom/data/network/models/matching_deny_request.dart';
import 'package:linkmom/data/network/models/matching_init_request.dart';
import 'package:linkmom/data/network/models/momdady_mycares_list_response.dart';
import 'package:linkmom/data/network/models/pay_cancel_info_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_init_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_view_request.dart';
import 'package:linkmom/data/network/models/pay_init_request.dart';
import 'package:linkmom/data/storage/flag_manage.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/listview/scroll_behavior.dart';
import 'package:linkmom/utils/route_action.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/chat/chat_care_info_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/momdady_profile_page.dart';
import 'package:linkmom/view/main/notify/care_notify_page.dart';
import 'package:linkmom/view/main/notify/community_notify_page.dart';
import 'package:linkmom/view/main/payment/payment_cancel_page.dart';
import 'package:linkmom/view/main/payment/payment_cancel_reason_page.dart';
import 'package:linkmom/view/main/payment/payment_page.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../../route_name.dart';
import 'chat_view/chat_bubble.dart';
import 'chat_view/chat_bubble_view.dart';
import 'chat_view/chat_care_info_view.dart';
import 'chat_view/chat_matching_view.dart';
import 'chat_view/chat_view.dart';
import 'full_photo.dart';

class ChatRoomPage extends BaseStateful {
  ///채팅방 생성(프로필에서 대화 신청시...) 일반 사용시 세팅안함
  final ChatRoomType roomType;

  ///상대방 id
  final int receiver;

  ///돌봄신청서 신청서 id
  final int? bookingcareservices;

  ///채팅 방 번호
  final int? chatroom_id;

  ///채팅 방 이름
  final String? roomName;

  ChatRoomPage({this.roomType = ChatRoomType.CREATE, required this.receiver, this.bookingcareservices, this.chatroom_id, this.roomName});

  @override
  ChatRoomPageState createState() => ChatRoomPageState(roomType: roomType, receiver: receiver, bookingcareservices: bookingcareservices, chatroom_id: chatroom_id, roomName: roomName);
}

class ChatRoomPageState extends BaseStatefulState<ChatRoomPage> with TickerProviderStateMixin {
  ChatRoomType? roomType;
  int receiver;
  int? bookingcareservices;
  int? chatroom_id;
  String? roomName;

  ChatRoomPageState({this.roomType, this.receiver = 0, this.bookingcareservices, this.chatroom_id, this.roomName});

  late int id;
  int cnt = 0;
  late DateTime beforeDateTime;
  late double maxWidth;

  late DateTime beforeRevDateTime;
  late DateTime afterRevDateTime;

  late DateTime beforeLastTime;
  late DateTime afterLastTime;
  late DateTime afterDateTime;

  bool _isLastMsg = false;
  String lastMsg = '';
  bool _isWaiting = true;
  bool _isScroll = false;
  bool _isRead = false;
  bool _isUploadFile = false;
  bool _isReceive = false; //채팅방에 있고, 상대방이 메시지를 보낸경우 하나라도 있으면 읽은 처리를 한다.

  double _progress = 0;

  ///채팅 여부 판단 tr
  ChattingType _chatMatchingType = ChattingType.normal;
  int _matchingStatus = MatchingStatus.inprogress.value;
  int _careStatus = CareStatus.inprogress.value;

  ///하단 메뉴
  Map<Widget, Function>? _menu;

  final ItemScrollController itemScrollController = ItemScrollController();
  final ItemPositionsListener itemPositionsListener = ItemPositionsListener.create();
  final TextEditingController textEditingController = TextEditingController();
  final FocusNode focusNode = FocusNode();
  EdgeInsets bottomPadding = padding_0;
  late File? imageFile;

  PanelController _panelCtrl = PanelController();
  bool isOpen = false;

  late Timer _timer;

  USER_TYPE currentType = storageHelper.user_type;
  ChatInfoData chatInfoData = ChatInfoData();
  ContractData contractData = ContractData();

  final double imgHeight = 200;
  final double imgWidth = 200;

  GlobalKey _keyContent = GlobalKey();
  GlobalKey _keyTime = GlobalKey();
  GlobalKey _keyProfile = GlobalKey();
  Size sizeTime = Size(0, 0);
  Size sizeContent = Size(0, 0);
  Size sizeProfile = Size(0, 0);
  double sizeAvatar = 20;
  final int paddingSend = 30;
  final int paddingReceiver = 35;

  bool isUpdate = false;
  bool isSend = true;

  @override
  void initState() {
    super.initState();
    id = auth.user.id;
    log.d('『GGUMBI』>>> initState ChatRoomPage : : $id, $roomType, $receiver, $chatroom_id, $roomName<<< ');
    setPublishData();
    mqttManager.setPublishOn('initState');
    if (!StringUtils.validateString(roomName)) {
      roomName = "";
    }

    if (Platform.isIOS) {
      bottomPadding = padding_20_B;
    } else {
      bottomPadding = padding_0;
    }
    focusNode.addListener(onFocusChange);
    itemPositionsListener.itemPositions.addListener(_scrollListener);
    _requestChatView();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (_panelCtrl.isAttached) {
        _panelCtrl.close();
      }
      pushProvider.setBadgeChatClear(isUpdate: true);
    });

    maxWidth = widthFull(navigatorKey.currentContext!);
    if (Platform.isAndroid) {
      maxWidth = widthFull(navigatorKey.currentContext!);
    }

    //기본값 세팅
    if (sizeContent.width == 0) {
      sizeTime = Size(70, 0);
      sizeContent = Size(maxWidth, 0);
    }
    sizeProfile = Size(50, 0);

    _start();
  }

  @override
  onResume() {
    super.onResume();
  }

  @override
  onPaused() {
    mqttManager.setPublishRoomOut();
    super.onPaused();
  }

  @override
  void dispose() {
    mqttManager.remove();
    imageCache.clear();
    _timer.cancel();

    try {
      focusClear(context);
    } catch (e) {}
    super.dispose();
  }

  /// 여기서는 4분 50초 마다 MQTT publish ON
  void _start() {
    _timer = Timer.periodic(Duration(seconds: 290), (timer) {
      mqttManager.setPublishOn('start');
    });
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
  }

  _scrollListener() async {
    try {
      // log.d('『GGUMBI』>>> _scrollListener 11111: _isScroll: ${itemPositionsListener.itemPositions.value.length}, _isLastMsg: ${itemPositionsListener.itemPositions.value.last.index}, ${data.chatRoom.lsChat.length} ');
      if (itemPositionsListener.itemPositions.value.length != 0 && itemPositionsListener.itemPositions.value.last.index >= data.chatRoom.lsChat.length - 1) {
        //제일 하단까지 인덱스가 안내려오는 경우가 있기 때문에 임시 조치
        if (_isScroll) {
          Future.delayed(Duration(milliseconds: 1)).then((value) {
            // log.d('『GGUMBI』>>> _scrollListener 22222: _isScroll: ${itemPositionsListener.itemPositions.value.length}, _isLastMsg: ${itemPositionsListener.itemPositions.value.last.index}, ${data.chatRoom.lsChat.length} ');
            _isScroll = false;
            _isLastMsg = false;
            onUpDate();
          });
        }
      } else {
        if (!_isScroll) {
          Future.delayed(Duration(milliseconds: 1)).then((value) {
            // log.d('『GGUMBI』>>> _scrollListener 33333: _isScroll: $_isScroll, _isLastMsg: $_isLastMsg, ');
            _isScroll = true;
            onUpDate();
          });
        }
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> _scrollListener : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  //정확한 표현을 위해 계산하여, maxwidth를 설정
  void calculateSize() => WidgetsBinding.instance.addPostFrameCallback((_) {
        try {
          // if (isUpdate) return;
          if (_keyTime.currentContext != null) {
            RenderBox? box = _keyTime.currentContext!.findRenderObject() as RenderBox?;
            sizeTime = box!.size;
            // log.d('『GGUMBI』>>> calculateSize size 11 : {}: ${box.size.width}, ${box.size.height}  <<< ');
          }
          if (_keyContent.currentContext != null) {
            RenderBox? box1 = _keyContent.currentContext!.findRenderObject() as RenderBox?;
            sizeContent = box1!.size;
            log.d('『GGUMBI』>>> calculateSize sizeContent 22 : {}: ${sizeContent.width}, ${sizeContent.height}, ${widthFull(context)}, ${widthFull(context) - sizeTime.width}, ${sizeContent.width}  <<< ');
          }
          if (_keyProfile.currentContext != null) {
            RenderBox? box1 = _keyProfile.currentContext!.findRenderObject() as RenderBox?;
            sizeProfile = box1!.size;
            log.d('『GGUMBI』>>> calculateSize sizeContent 33 : {}: ${sizeProfile.width}, ${sizeProfile.height},   <<< ');
          }
          isUpdate = true;
          // log.d('『GGUMBI』>>> calculateSize sizePhoto 44 : {}:   <<< ');
        } catch (e) {
          log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
        }
      });

  Widget _timeView() {
    return Padding(
      key: _keyTime,
      padding: EdgeInsets.only(bottom: 8.0, left: 5.0),
      child: lText(
        StringUtils.getStringToDate(DateTime.now().toString(), format: F_HMM),
        style: st_12(textColor: color_transparent),
      ),
    );
  }

  Widget _contentView() {
    return Padding(
      padding: padding_15,
      child: lText(
        'StringUtils.getStringToDate(DateTime.now().toString(), format: F_HMM)',
        key: _keyContent,
        style: st_12(textColor: color_transparent),
      ),
    );
  }

  Widget _profileView() {
    return Material(
      key: _keyProfile,
      child: CircleAvatar(
        foregroundColor: color_transparent,
        child: Image.asset(
          IMG_PERSON_SMALL_MOMDADY,
          color: color_transparent,
        ),
        radius: sizeAvatar,
        backgroundColor: color_transparent,
      ),
    );
  }

  ///하단으로 이동
  _moveDown() {
    log.d('『GGUMBI』>>> _moveDown : itemScrollController.isAttached: ${itemScrollController.isAttached}, focusNode.hasFocus: ${focusNode.hasFocus} <<< ');
    if (itemScrollController.isAttached) {
      itemScrollController.scrollTo(
        index: data.chatRoom.lsChat.length - 1,
        duration: Duration(seconds: 1),
        curve: Curves.easeInOutCubic,
        alignment: 0,
      );

      _isScroll = false;
      _isLastMsg = false;
    }
  }

  Future<void> onFocusChange() async {
    // log.d('『GGUMBI』>>> onFocusChange : focusNode.hasFocus: ${focusNode.hasFocus},  <<< ');
    if (focusNode.hasFocus) {
      bottomPadding = padding_0;
      Future.delayed(Duration(milliseconds: 1), () {
        if (!_isScroll) {
          _moveDown();
          onUpDate();
        }
      });
    } else {
      if (Platform.isIOS) {
        bottomPadding = padding_20_B;
      } else {
        bottomPadding = padding_0;
      }
    }
  }

  ///채팅방 정보 조회
  _requestChatView() async {
    log.d('『GGUMBI』>>> _requestChatView : id: $id, receiver: $receiver, bookingcareservices: $bookingcareservices, chatroom_id: $chatroom_id, $roomType<<< ');
    if (data.chatRoom.lsChat.isNotEmpty) {
      data.chatRoom.lsChat.clear();
      data.chatRoom.responseChat = ChatMessageListResponse();
      _isLastMsg = false;
      lastMsg = '';
      _isWaiting = true;
      _isScroll = false;
      _isRead = false;
    }
    flag.enableLoading(fn: () => onUpDate());
    if (chatroom_id == null || roomType == ChatRoomType.CREATE) {
      await apiHelper.requestChatRoomInit(ChatRoomInitRequest(receiver: receiver, bookingcareservices: bookingcareservices)).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          chatroom_id = response.getData().chatroom_id;
          _setChatRoomInfo(response.getData());
        } else {
          flag.disableLoading(fn: () => onUpDate());
          Commons.showSnackBar(context, response.getMsg());
        }
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } else if (roomType == ChatRoomType.VIEW) {
      await apiHelper.requestChatRoomView(ChatRoomViewRequest(receiver: receiver, chatroom_id: chatroom_id!)).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _setChatRoomInfo(response.getData());
        } else {
          flag.disableLoading(fn: () => onUpDate());
          Commons.showSnackBar(context, response.getMsg());
        }
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    }
  }

  ///채팅방 정보 세팅(이름 및 프로필)
  _setChatRoomInfo(ChatRoomData roomData) {
    setChatInfoData(roomData);
    log.d('『GGUMBI』>>> _setChatRoomInfo : {roomData.bookingcareservices_info.userinfo.id }: ${roomData.bookingcareservices_info?.userinfo?.id}, id:$id, ${roomData.carematching?.linkmom} <<< ');
    chatInfoData = ChatInfoData(momdady: roomData.carematching!.momdady, linkmom: roomData.carematching!.linkmom);
    Commons.setModeChange(momdadyId: roomData.carematching!.momdady, linkMomId: roomData.carematching!.linkmom);
    currentType = storageHelper.user_type;
    if (roomData.chattingtype == ChattingType.normal.value) {
      roomName = roomData.talker_info!.first_name;
    } else {
      roomName = roomData.talker_info!.first_name + ' ${(Commons.isLinkMom() ? "맘대디".tr() : "링크쌤".tr())}';
    }
    setChatMatchingType(roomData.chattingtype, true);
    _requestChatList();
  }

  ///채팅 대화 리스트 조회
  _requestChatList() async {
    String url = ApiEndPoint.EP_CHAT_MSG_LIST;
    if (StringUtils.validateString(data.chatRoom.responseChat.getData().next)) {
      url = data.chatRoom.responseChat.getData().next;
    } else {
      if (data.chatRoom.lsChat.length != 0) {
        return;
      }
    }
    log.d('『GGUMBI』>>> _requestChatList : url:------------------- $url,  ${data.chatRoom.responseChat}<<< ');
    await apiHelper.requestChatMessageList(ChatMessageListRequest(chatroom_id: getChatInfoData().chatroom_id, talker_id: getChatInfoData().talker_info!.id), url).then((response) {
      try {
        if (response.getCode() == KEY_SUCCESS) {
          data.chatRoom.responseChat = response;
          var list = response.getData().results as List<ChatSendData>;
          data.chatRoom.lsChat.addAll(list);
        }

        setPublishData();
        mqttManager.setSubscribe(mqttManager.topic);
        mqttManager.setPublishOn('_requestChatList');

        Future.delayed(Duration(milliseconds: 1)).then((value) {
          mqttManager.setPublishRoomIn();
        });
        flag.disableLoading(fn: () => onUpDate());
      } catch (e) {
        flag.disableLoading(fn: () => onUpDate());
        log.e('『GGUMBI』 Exception >>> _requestChatList : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      }
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  _requestPrevious(String url) {
    log.d('『GGUMBI』>>> _requestNext : url: $url  <<< ');
    try {
      if (url.isEmpty) return;
      apiHelper.requestChatMessageList(ChatMessageListRequest(chatroom_id: getChatInfoData().chatroom_id, talker_id: getChatInfoData().talker_info!.id), url).then((response) {
        try {
          if (response.getCode() == KEY_SUCCESS) {
            data.chatRoom.responseChat = response;
            var list = response.getData().results as List<ChatSendData>;
            data.chatRoom.lsChat.insertAll(0, list);
            mqttManager.setScroll(ChatScrollItem(position: list.length - 1));
          }
        } catch (e) {
          log.e('『GGUMBI』 Exception >>> _requestChatList : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
        }
      }).catchError((e) {});
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>>>> $e");
    }
  }

  ///메시지 보내기
  Future<void> _requestSendMessage(String msg, ChatMsgType type, {File? image, String? msgdata}) async {
    log.d('『GGUMBI』>>> onSendMessage : _isRead: $_isRead, content: $msg,  ChatMsgType: $type, PageName: ${mqttManager.getCurrentRouteName}<<< ');
    // type: 0 = text, 1 = image, 2 = sticker
    try {
      ChatSendData sendData = getSendData(msgtype: type, msg: msg, msgdata: msgdata);
      log.d('『GGUMBI』>>> _requestSendMessage : sendData: $sendData,  <<< ');
      await apiHelper
          .requestChatMessageSend(
              ChatMessageSendRequest(
                chattingroom: sendData.chattingroom,
                receiver: sendData.receiver,
                msgtype: sendData.msgtype!.index,
                msg: sendData.msg,
                image: image,
                msgdata: sendData.msgdata,
                sendtimestamp: sendData.sendtimestamp.toString(),
              ),
              callBack: image != null
                  ? (progress) {
                      _isUploadFile = true;
                      _progress = progress / 100;
                      onUpDate();
                      if (progress >= 100) {
                        Future.delayed(Duration(milliseconds: 500)).then((value) {
                          onUpDate();
                          _isUploadFile = false;
                        });
                      }
                    }
                  : null)
          .then((response) {
        //이미지 처리(수신 받은 url 추가)
        if (response.getCode() == KEY_SUCCESS) {
          imageFile = null;
          textEditingController.clear();
          if (data.chatRoom.lsChat.length == 0) {
            data.chatRoom.lsChat.add(ChatSendData(
              id: id,
              receiver: receiver,
              chattingroom: getChatInfoData().chatroom_id,
              msgtype: ChatMsgType.noData,
              sendtime: DateTime.now().toString(),
            ));
          }

          sendData.is_receive = _isRead;
          sendData.image = response.getData().image;
          sendData.image_thumbnail = response.getData().image_thumbnail;
          sendData.msgdata = response.getData().msgdata;
          addChatList(sendData);
        } else {
          Commons.showToast(response.getMsg());
        }
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> onSendMessage : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  ///거절하기
  Future<void> _requestMatchingDeny() async {
    try {
      MatchingDenyRequest requestData = MatchingDenyRequest();
      requestData.requestData = MatchingRequestData(
        receiver: receiver,
        bookingcareservices: bookingcareservices!,
        chattingroom: chatroom_id!,
        carematching: getChatInfoData().carematching!.matching_id,
      );

      await apiHelper.requestMatchingDeny(requestData).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          addChatList(getSendData(msgtype: ChatMsgType.matching, msg: "매칭요청을거절_메시지".tr(), msgdata: response.getData().msgdata));
          mqttManager.setPublishRoomIn();
        } else {
          Commons.showToast(response.getMsg());
        }
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> onSendMessage : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  ///매칭취소하기
  Future<void> _requestMatchingCancel() async {
    try {
      MatchingCancelRequest requestData = MatchingCancelRequest();
      requestData.requestData = MatchingRequestData(
        receiver: receiver,
        bookingcareservices: bookingcareservices!,
        chattingroom: chatroom_id!,
        carematching: getChatInfoData().carematching!.matching_id,
      );
      log.d('『GGUMBI』>>> _buildMenu : data.requestData : ${requestData.requestData},  <<< ');
      apiHelper.requestMatchingCancel(requestData).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          addChatList(getSendData(msgtype: ChatMsgType.matching, msg: "매칭취소_메시지".tr(), msgdata: response.getData().msgdata));
          mqttManager.setPublishRoomIn();
        } else {
          Commons.showToast(response.getMsg());
        }
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> onSendMessage : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  ///차단하기
  void _requestUserBlock(int userId, int matchingId) {
    try {
      BlockSaveRequest req = BlockSaveRequest(block_user: userId, block_matching: matchingId);
      apiHelper.requestBlockSave(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          getChatInfoData().talker_info!.is_block = true;
        }
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  ChatSendData getSendData({ChatMsgType? msgtype, dynamic msgdata, String msg = ''}) {
    ChatSendData sendData = ChatSendData(
      chattingroom: chatroom_id!,
      sender: id,
      receiver: receiver,
      msgtype: msgtype,
      chattingtype: getChatInfoData().chattingtype,
      msg: msg,
      msgdata: msgdata,
      sendtime: DateTime.now().toString(),
      sendtimestamp: StringUtils.getToSendTimeInt(),
    );
    return sendData;
  }

  ///매칭요청,수락,거절/ 매칭취소 / 결제하기 / 돌봄취소하기flutter stream add
  addChatList(ChatSendData receiverData, {bool isRequest = true}) {
    try {
      if (data.chatRoom.lsChat.isNotEmpty) {
        if (_isRead) {
          receiverData.is_receive = true;
        }
        data.chatRoom.lsChat.add(receiverData);
        if (receiverData.msgtype == ChatMsgType.matching) {
          setChatMatchingType(receiverData.chattingtype, false, receiverData: receiverData);
        }
        if (isRequest) {
          onUpDate();
          _moveDown();
        }
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> addChatList : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    log.d('『GGUMBI』>>> build : storageHelper.user_type: $currentType, ${storageHelper.user_type},  <<< ');
    // if (!isUpdate) calculateSize();
    calculateSize();
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar(
          roomName!,
          onClick: (action) {
            if (Commons.isDebugMode) {
              mqttManager.setPublishOff('chat');
            }
          },
          hide: false,
          onBack: () {
            //들어올때 모드와 다를 경우 모드를 체인지 한다.
            //2021/11/26 링크쌤/맘대디 채팅을 단일로 만들면서 이전 모드로 변경하는건 주석 처리한다.
            // if (currentType != storageHelper.user_type) {
            //   Commons.setModeChange(changeType: currentType, isShowToast: false);
            // }

            ///채팅방으로 이동후 stack에 아무것도 없을 경우 채팅 화면으로 보낸다. 추후 자세히 봐야할것!
            if (Navigator.canPop(context)) {
              // log.d('『GGUMBI』>>> build : chatRoomPage Back 1  <<< ');
              // Commons.pagePop(context, data: data.chatRoom.lsChat[data.chatRoom.lsChat.length - 1]);
              log.d('『GGUMBI』>>> build : _matchingStatus: $_matchingStatus, ${data.chatRoom.lsChat[data.chatRoom.lsChat.length - 1]} <<< ');
              try {
                Commons.pagePop(context, data: ChatBackItem(matchingStatus: _matchingStatus, chatData: data.chatRoom.lsChat[data.chatRoom.lsChat.length - 1]));
              } catch (e) {
                //통신에러로 데이터를 받지 못한상태이면 뒤로가기 2021/11/25
                Commons.pagePop(context);
              }
            } else {
              log.d('『GGUMBI』>>> build : chatRoomPage Back 2  <<< ');
              // goFindPage(index: 4);
              Commons.pagePopUntilFirst(context);
            }
          },

          // ///임시 테스트
          // onClick: (action) {
          //   if (action == DialogAction.test) {
          //     _requestChatView();
          //   }
          // },
          rightWidget: InkWell(
            onTap: () {
              showMenuModal(context, {
                lText("나가기".tr(), style: st_b_16(fontWeight: FontWeight.w500)): () => goRoomDelete(getChatInfoData().chatroom_id),
                lText("신고하기".tr(), style: st_b_16(fontWeight: FontWeight.w500)): () => goNotify(
                      receiver,
                      chatroom_id!,
                      getChatInfoData().carematching!.matching_id,
                      roomName,
                    ),
                lText("차단하기".tr(), style: st_b_16(isSelect: isCs(), textColor: color_222222, disableColor: color_cecece, fontWeight: FontWeight.w500)): () => !isCs()
                    ? showNormalDlg(
                        context: context,
                        title: getBlockTitle(),
                        message: getBlockContent(),
                        btnLeft: "취소".tr(),
                        onClickAction: (action) {
                          if (action == DialogAction.yes) {
                            _requestUserBlock(receiver, getChatInfoData().carematching != null ? getChatInfoData().carematching!.matching_id : 0);
                          }
                        })
                    : null,
              });
            },
            child: lContainer(padding: padding_10, child: Lcons.more()),
          ),
        ),
        context: context,
        body: Stack(
          children: [
            //실제 보이지 않는 영역(계산해주기 위함) 2021/11/18
            Visibility(
                visible: !isUpdate,
                child: Column(
                  children: [_contentView(), _timeView(), _profileView()],
                )),
            Column(
              children: [
                if (_chatMatchingType == ChattingType.care)
                  Wrap(
                    children: [
                      Card(
                        margin: padding_0,
                        elevation: isExpired() ? 0 : 5,
                        shadowColor: color_black.withOpacity(0.1),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.zero,
                        ),
                        child: Container(
                          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          alignment: Alignment.bottomCenter,
                          child: chatCareInfoView(context, data.chatRoom.careItem, viewName: '$ChatRoomPage', isModify: isCareModify(), callBack: (action, bookingId) async {
                            if (action == DialogAction.select) {
                              goDetailViewPage(bookingId, btnType: BottomBtnType.not);
                            } else if (action == DialogAction.toggle) {
                              _showInfoAlert(bookingId, type: currentType);
                            } else if (action == DialogAction.modify) {
                              var resultData = await Commons.page(context, routeMomDadyProfile,
                                  arguments: MomDadyProfilePage(
                                    viewMode: ViewMode(viewType: ViewType.modify),
                                    bookingId: bookingId,
                                  ));
                              if (resultData != null) {
                                _requestChatView();
                                // ContractData contractData = ContractData();
                                // contractData.rematching = true;
                                // contractData.matching_status = MatchingStatus.inprogress.value;
                                // addChatList(getSendData(msgtype: ChatMsgType.matching, msg: "돌봄신청서_수정_메시지_맘대디.".tr(), msgdata: contractData));
                              }
                            }
                          }),
                        ),
                      ),
                      if (isExpired())
                        Card(
                          margin: padding_0,
                          elevation: 5,
                          shadowColor: color_black.withOpacity(0.5),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.zero,
                          ),
                          child: expiredView(
                            status: getBookingInfoData().careinfo!.booking_status,
                          ),
                        ),
                    ],
                  ),
                buildListView(),
                buildInput(),
              ],
            ),
          ],
        ),
      ),
      isOnlyLoading: true,
    );
  }

  ///채팅 입력 화면 및 메뉴
  Widget buildInput() {
    return Wrap(
      children: [
        lContainer(
          color: color_eeeeee,

          ///ios에 따른 하단 간격 조정
          margin: bottomPadding,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              // 돌봄신청, 서명 기타 등등 메뉴
              Material(
                color: Colors.transparent,
                child: Container(
                  margin: padding_10,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(color: Commons.getColor(), borderRadius: BorderRadius.circular(8)),
                  child: IconButton(
                    padding: padding_0,
                    visualDensity: VisualDensity(horizontal: -4, vertical: -4),
                    icon: Lcons.menu(color: color_white, isEnabled: true, size: 14),
                    onPressed: () {
                      showMenuModal(
                        context,
                        _menu!,
                        enableCancel: false,
                      );
                    },
                  ),
                ),
              ),

              // 메시지 입력
              Flexible(
                child: lContainer(
                  color: color_transparent,
                  alignment: Alignment.center,
                  padding: padding_06_TB,
                  child: TextField(
                    style: st_b_15(),
                    controller: textEditingController,
                    textAlign: TextAlign.left,
                    textAlignVertical: TextAlignVertical.center,
                    textInputAction: TextInputAction.newline,
                    minLines: 1,
                    maxLines: 3,
                    cursorColor: Commons.getColor(),
                    focusNode: focusNode,
                    //특수문자, 이모지 등....필터링 2021/11/10 서버 작업으로 주석
                    // inputFormatters: [
                    //   FilteringTextInputFormatter.deny(RegExp(emojiRegexp)),
                    //   FilteringTextInputFormatter.deny(RegExp(emojiRegexp2)),
                    // ],
                    decoration: inputChatDecoration(),
                    onChanged: (value) {
                      // log.d('『GGUMBI』>>> buildInput : value: $value, ${value.codeUnits}, <<< ');
                      // log.d('『GGUMBI』>>> buildInput : "주문번호 입니다. \u{1f60e}"" <<< ');
                    },
                  ),
                ),
              ),

              // 메시지 보내기
              InkWell(
                child: Material(
                  color: Colors.transparent,
                  child: Container(
                    margin: padding_10,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
                    child: IconButton(
                      padding: padding_0,
                      visualDensity: VisualDensity(horizontal: -4, vertical: -4),
                      alignment: Alignment.center,
                      icon: Icon(
                        Icons.send,
                        color: Commons.getColor(),
                      ),
                      onPressed: null,
                    ),
                  ),
                ),
                onTap: () {
                  log.d('『GGUMBI』>>> buildInput ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
                  if (StringUtils.validateString(textEditingController.text)) {
                    // log.d('『GGUMBI』>>> buildInput : : ${mqttManager.isConnect},  <<< ');
                    // log.d('『GGUMBI』>>> buildInput : : ${mqttManager.client.connectionStatus!.state},  <<< ');
                    // log.d('『GGUMBI』>>> buildInput : : ${mqttManager.onConnected},  <<< ');
                    // log.d('『GGUMBI』>>> buildInput : : ${mqttManager.client.connectionStatus!.state},  <<< ');
                    if (isSend) {
                      isSend = false;
                      //2022/03/23 is_active == False or is_block == True  ”메시지를 수신 할 수 없는 사용자 입니다.”
                      if (!getChatInfoData().talker_info!.is_active || getChatInfoData().talker_info!.is_block) {
                        Commons.showToast("채팅차단_안내".tr());
                        isSend = true;
                        return;
                      }
                      _requestSendMessage(textEditingController.text, ChatMsgType.text, msgdata: jsonEncode(chatInfoData)).whenComplete(() {
                        isSend = true;
                      }).catchError((e) {
                        isSend = true;
                      });
                    }
                  } else {
                    Commons.showToast("채팅입력".tr());
                    // Commons.page(context, routePayMentCancelList, arguments: PayInfoRequest(contract_id: contractData.contract_id));
                  }
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  ///채팅 리스트 화면
  Widget buildListView() {
    return PreviousRequestListener(
      onPreviousRequest: () => _requestPrevious(data.chatRoom.responseChat.getData().next),
      child: Flexible(
        child: Container(
          color: color_f5f5f5.withOpacity(0.62),
          child: Stack(
            children: [
              StreamBuilder(
                initialData: data.chatRoom.lsChat,
                stream: mqttManager.mqttStream(),
                builder: (BuildContext context, snapshot) {
                  log.d('『GGUMBI』>>> buildListView : snapshot.hasData 1 : ${snapshot.hasData}, ${snapshot.connectionState}, ${snapshot.runtimeType}, List: ${snapshot.data is List}, AsyncSnapshot: ${snapshot.data is AsyncSnapshot}, _isScroll $_isScroll <<< ');
                  log.d('『GGUMBI』>>> buildListView : snapshot.hasData 2 : ${snapshot.hasData},: ${snapshot.data}}  <<< ');
                  if (snapshot.hasData) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                      case ConnectionState.waiting:
                        log.d('『GGUMBI』>>> buildListView waiting ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ ${snapshot.data is List}, ${data.chatRoom.lsChat.length}, $_isWaiting<<< ');
                        if (data.chatRoom.lsChat.length != 0 && _isWaiting) {
                          log.d('『GGUMBI』>>> buildListView waiting ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★  1 ${snapshot.data is List}, ${data.chatRoom.lsChat.length}<<< ');
                          Future.delayed(Duration(milliseconds: 1)).then((value) {
                            _isWaiting = false;
                            if (itemScrollController.isAttached) {
                              itemScrollController.scrollTo(
                                index: data.chatRoom.lsChat.length - 1,
                                duration: Duration(milliseconds: 1),
                              );
                            }
                          });
                        }
                        break;
                      case ConnectionState.done:
                      case ConnectionState.active:
                        try {
                          if (snapshot.data is AsyncSnapshot) {
                            AsyncSnapshot _data = snapshot.data as AsyncSnapshot;
                            log.d('『GGUMBI』>>> add : 10 : ${_data.connectionState}');
                            log.d('『GGUMBI』>>> buildListView : _data: ${_data.connectionState}, _isWaiting: $_isWaiting, _isScroll: $_isScroll, _isRead: $_isRead<<< ');

                            ///채팅방 입장 메시지 읽음 처리 On....(메시지를 보내고 ask로 응답을 받은 후 읽은 처리를 한다.)
                            if (_data.connectionState == ConnectionState.done) {
                              if (_data.data is ChatScrollItem) {
                                if (itemScrollController.isAttached) {
                                  itemScrollController.jumpTo(
                                    index: (_data.data as ChatScrollItem).position,
                                    alignment: 0,
                                  );
                                }
                                onUpDate();
                              } else {
                                _isRead = true;
                                setReceive();
                              }

                              ///채팅방 나가기
                            } else if (_data.connectionState == ConnectionState.none) {
                              _isRead = false;

                              ///메시지 처리
                            } else if (_data.connectionState == ConnectionState.active) {
                              log.d('『GGUMBI』>>> buildListView : {}: ${_data.data},  <<< ');
                              if (_data.data == null) {
                                WidgetsBinding.instance.addPostFrameCallback((_) {
                                  _requestChatView();
                                });
                              } else {
                                var receiverData = ChatSendData.fromJson(json.decode(_data.data));
                                log.d('『GGUMBI』>>> buildListView : receiverData: $receiverData,  <<< ');
                                bool isRematching = false;
                                if (receiverData.msgdata != null && receiverData.msgdata is ContractData) {
                                  //2022/05/17 돌봄신청서가 수정된 경우에는 채팅방을 다시 호출한다.
                                  isRematching = (receiverData.msgdata as ContractData).rematching;
                                }
                                if (isRematching) {
                                  WidgetsBinding.instance.addPostFrameCallback((_) {
                                    _requestChatView();
                                  });
                                } else {
                                  addChatList(receiverData, isRequest: false);
                                }
                                mqttManager.setPublishRoomIn();

                                log.d('『GGUMBI』>>> buildListView 0 ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ $_isScroll, $_isLastMsg<<< ');
                                isReceive();

                                ///스크롤 도중 메시지 수신시 하단에 띄워준다.
                                if (!_isScroll) {
                                  _moveDown(); //현제 보고 있는 화면이 아니면 최근 대화를 보여준다.
                                  log.d('『GGUMBI』>>> buildListView 1 ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
                                } else {
                                  log.d('『GGUMBI』>>> buildListView 2 ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
                                  _isLastMsg = true; //마지막 메시지 > 마지막 내용 플래그
                                  lastMsg = data.chatRoom.lsChat[data.chatRoom.lsChat.length - 1].msg;
                                  Future.delayed(Duration(milliseconds: 1)).then((value) {
                                    onUpDate();
                                  });
                                }
                              }
                            }
                          }
                        } catch (e) {
                          log.e('『GGUMBI』 Exception >>> buildListView : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
                        }
                        break;
                      default:
                        return Center();
                    }
                  }
                  return ScrollConfiguration(
                    behavior: LScrollBehavior(),
                    child: lScrollbar(
                      child: ScrollablePositionedList.builder(
                        minCacheExtent: 1500.0,
                        padding: padding_15,
                        itemBuilder: (context, index) {
                          if (index >= 0) {
                            return Padding(
                              padding: padding_05_TB,
                              child: buildChat(index, data.chatRoom.lsChat[index]),
                            );
                          } else {
                            return Container();
                          }
                        },
                        itemCount: data.chatRoom.lsChat.length,
                        physics: ClampingScrollPhysics(),
                        itemScrollController: itemScrollController,
                        itemPositionsListener: itemPositionsListener,
                      ),
                    ),
                  );
                },
              ),
              AnimatedOpacity(
                duration: Duration(milliseconds: 500),
                opacity: _isScroll ? 1.0 : 0.0,
                child: Visibility(
                  visible: _isScroll,
                  child: Padding(
                    padding: padding_20,
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: InkWell(onTap: _moveDown, child: Lcons.down(size: 40)),
                    ),
                  ),
                ),
              ),
              if (_isScroll && _isLastMsg)
                GestureDetector(
                  onTap: () {
                    _moveDown();
                  },
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: widthFull(context),
                      margin: padding_10,
                      padding: padding_05,
                      decoration: BoxDecoration(
                        border: Border.all(color: color_eeeeee, width: 0.5),
                        borderRadius: BorderRadius.circular(50),
                        color: color_highlight /*Commons.getColorBg()*/,
                      ),
                      child: lText(
                        lastMsg,
                        style: st_14(),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),

              //이미지 업로시
              if (_isUploadFile) uploadView(_progress),
            ],
          ),
        ),
      ),
    );
  }

  ///채팅화면
  Widget buildChat(int index, ChatSendData _data) {
    if (_data != null) {
      ///처음 대회 시작시(매너있게 대화해주세요...)
      if (_data.msgtype == ChatMsgType.noData)
        return Column(
          children: [
            setMsgNotify(),
            // timeView(index),
          ],
        );

      ///매칭 정보(시스템 메시지)
      if (_data.msgtype == ChatMsgType.matching) {
        bool isLast = isLastMessageSystem(index, data.chatRoom.lsChat);
        return Column(
          children: [
            if (isLastTime(index)) timeLineView(index, data.chatRoom.lsChat),
            matchingStatusView(
              context,
              getChatInfoData(),
              _data,
              isLast,
              id,
              _matchingStatus,
              callBack: (action, matchingStatus) {
                //계약서 보기
                if (action == DialogAction.contract) {
                  goMatchingPage(viewName: '$ChatRoomPage', itemType: ItemType.contract);
                  //결제하기
                } else if (action == DialogAction.pay) {
                  goPaymentPage();
                  //매칭 수량하기
                } else if (action == DialogAction.yes) {
                  goContractPage();
                  //매칭 거절하기
                } else if (action == DialogAction.no) {
                  String name = '${getChatInfoData().talker_info!.first_name} ${storageHelper.user_type == USER_TYPE.mom_daddy ? "링크쌤".tr() + "의".tr() : "맘대디".tr() + "의".tr()}';
                  showNormalDlg(
                    message: '$name ${"매칭거절_메시지".tr()}',
                    btnLeft: "아니오".tr(),
                    btnRight: "예".tr(),
                    onClickAction: (action) {
                      if (action == DialogAction.yes) {
                        _requestMatchingDeny();
                      }
                    },
                  );
                  //링크쌤 또는 맘대디 찾기
                } else if (action == DialogAction.find) {
                  goFindPage();
                  //취소내역확인하기
                } else if (action == DialogAction.cancel) {
                  ContractData contractData = _data.msgdata as ContractData;
                  goCancelViewPage(contractData.cancel_id!);
                  //환불규정확인하기
                } else if (action == DialogAction.cancelInfo) {
                  Commons.page(context, routePayMentRefundRule);
                  //돌봄관리
                } else if (action == DialogAction.schedule) {
                  Commons.pageToMainTabMove(context, tabIndex: TAB_SCHEDULE, subTabIndex: 1);
                  //돌봄신청서 수정
                } else if (action == DialogAction.modify) {
                  goContractPage();
                }
              },
            ),
          ],
        );
      }

      ///일반 메시지
      if (_data.sender == id) {
        // Right (my message)
        bool isLastSend = isLastMessage(id, index, data.chatRoom.lsChat);
        return Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            if (isLastTime(index)) timeLineView(index, data.chatRoom.lsChat),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                if (_data.msgtype == ChatMsgType.text || _data.msgtype == ChatMsgType.careInfo) senderView(_data, isLastSend, index),
                if (_data.msgtype == ChatMsgType.image)
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          // ///읽음, 안읽음 처리
                          if (!_data.is_receive)
                            Padding(
                              padding: EdgeInsets.only(left: 5.0, top: 5.0, right: 5.0),
                              child: Icon(
                                Icons.circle,
                                size: 5,
                                color: color_ff3b30,
                              ),
                            ),
                          if (isLastTimeSend(isLastSend, index))
                            Padding(
                              padding: EdgeInsets.only(bottom: isLastSend ? 15.0 : 8.0, right: 5.0),
                              child: lText(
                                StringUtils.getStringToDate(_data.sendtime.toString(), format: F_HMM),
                                style: st_12(textColor: color_b2b2b2),
                              ),
                            ),
                        ],
                      ),
                      Container(
                        child: OutlinedButton(
                          child: Material(
                            child: CachedNetworkImage(
                              imageUrl: _data.image_thumbnail,
                              width: imgWidth,
                              height: imgHeight,
                              imageBuilder: (context, imageProvider) => imageBuilder(imageProvider, width: imgWidth, height: imgHeight, color: Commons.getColorBg(), borderRadius: 8),
                              placeholder: (context, url) => placeholder(),
                              errorWidget: (context, url, error) => errorWidget(),
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(8.0)),
                            clipBehavior: Clip.antiAlias,
                          ),
                          onPressed: () {
                            Commons.page(context, routeFullPhoto, arguments: FullPhotoPage(name: roomName!, urlList: [_data.image]));
                          },
                          style: ButtonStyle(padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(0))),
                        ),
                        margin: EdgeInsets.only(bottom: isLastSend ? 20.0 : 10.0, right: 10.0),
                      ),
                    ],
                  ),
              ],
              mainAxisAlignment: MainAxisAlignment.end,
            ),
          ],
        );
      } else {
        // Left (peer message)
        bool isLastReceiver = isLastMessage(receiver, index, data.chatRoom.lsChat);

        return Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (isLastTime(index)) timeLineView(index, data.chatRoom.lsChat),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //프로필 이미지
                  lProfileAvatar(getChatInfoData().talker_info!.profileimg, radius: sizeAvatar),
                  if (_data.msgtype == ChatMsgType.text || _data.msgtype == ChatMsgType.careInfo) receiverView(_data, isLastReceiver, index),
                  if (_data.msgtype == ChatMsgType.image)
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                          padding: padding_10_L,
                          child: OutlinedButton(
                            child: Material(
                              child: CachedNetworkImage(
                                imageUrl: _data.image_thumbnail,
                                width: imgWidth,
                                height: imgHeight,
                                imageBuilder: (context, imageProvider) => imageBuilder(imageProvider, width: imgWidth, height: imgHeight, color: Commons.getColorBg(), borderRadius: 8),
                                placeholder: (context, url) => placeholder(),
                                errorWidget: (context, url, error) => errorWidget(),
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(8.0)),
                              clipBehavior: Clip.antiAlias,
                            ),
                            onPressed: () {
                              Commons.page(context, routeFullPhoto, arguments: FullPhotoPage(name: roomName!, urlList: [_data.image]));
                            },
                            style: ButtonStyle(padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(0))),
                          ),
                          margin: EdgeInsets.only(bottom: isLastReceiver ? 20.0 : 10.0, right: 10.0),
                        ),
                        if (isLastTimeReceiver(isLastReceiver, index))
                          Padding(
                            padding: EdgeInsets.only(bottom: isLastReceiver ? 15.0 : 8.0),
                            child: lText(
                              StringUtils.getStringToDate(_data.sendtime.toString(), format: F_HMM),
                              style: st_12(textColor: color_b2b2b2),
                            ),
                          ),
                      ],
                    ),
                ],
              ),
              Container(),
            ],
          ),
        );
      }
    } else {
      return SizedBox.shrink();
    }
  }

  ///내가 보낸 메시지
  senderView(ChatSendData _data, bool isLastSend, int index) {
    // log.d('『GGUMBI』>>> viewSize - senderView : : ${sizeContent.width - (sizeTime.width)}, sizeContent: $sizeContent, sizeTime: $sizeTime, sizePhoto: $sizePhoto  <<< ');
    double maxWidth = getMaxWidth();
    double maxCareWidth = getMaxWidthCare();
    return GestureDetector(
      onLongPress: () {
        Commons.showToast("복사되었습니다.");
        Clipboard.setData(ClipboardData(text: _data.msg));
      },
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              if (_data.msgtype != ChatMsgType.careInfo && isLastTimeSend(isLastSend, index))
                Padding(
                  padding: EdgeInsets.only(bottom: isLastSend ? 15.0 : 8.0, right: 5.0),
                  child: lText(
                    StringUtils.getStringToDate(_data.sendtime.toString(), format: F_HMM),
                    style: st_12(textColor: color_b2b2b2),
                  ),
                ),
            ],
          ),
          Stack(
            children: [
              if (_data.msgtype == ChatMsgType.text)
                ChatBubble(
                  elevation: 0.0,
                  clipper: ChatBubbleView(type: ChatSendType.send, isLast: isLastSend),
                  alignment: Alignment.topRight,
                  margin: EdgeInsets.only(bottom: isLastSend ? 15.0 : 8.0),
                  backGroundColor: Commons.getColorChatSend(),
                  child: Container(
                    constraints: BoxConstraints(
                      maxWidth: maxWidth,
                      // maxWidth: sizeContent.width - (sizeTime.width + (sizeTime.width * 0.9)),
                    ),
                    child: lText(
                      _data.msg,
                      style: st_b_15(),
                    ),
                  ),
                ),

              if (_data.msgtype == ChatMsgType.careInfo)
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ChatBubble(
                      elevation: 0.0,
                      clipper: ChatBubbleView(type: ChatSendType.send, isLast: isLastSend),
                      alignment: Alignment.topRight,
                      // margin: EdgeInsets.only(bottom: isLastSend ? 15.0 : 8.0),
                      backGroundColor: Commons.getColorChatSend(),
                      child: Container(
                        padding: padding_05_TB,
                        constraints: BoxConstraints(
                          maxWidth: maxCareWidth,
                        ),
                        child: Column(
                          children: [
                            chatCareInfoView(context, _data.msgdata as MyCareResultsData, id: id, sender: _data.sender, callBack: (action, bookingId) {
                              goDetailViewPage(bookingId, receiver: receiver);
                            }),
                            matchingChatView(receiverId: receiver, bookingcareservicesId: (_data.msgdata as MyCareResultsData).booking_id),
                          ],
                        ),
                      ),
                    ),
                    if (isLastTimeSend(isLastSend, index))
                      Padding(
                        padding: EdgeInsets.only(top: 5, bottom: isLastSend ? 15.0 : 8.0, right: 5.0),
                        child: lText(
                          StringUtils.getStringToDate(_data.sendtime.toString(), format: F_HMM),
                          style: st_12(textColor: color_b2b2b2),
                        ),
                      ),
                  ],
                ),

              ///읽음, 안읽음 처리
              if (!_data.is_receive)
                Padding(
                  padding: EdgeInsets.only(left: 8.0, top: 8.0),
                  child: Icon(
                    Icons.circle,
                    size: 5,
                    color: color_ff4c1b,
                  ),
                ),
            ],
          ),
        ],
      ),
    );
  }

  ///받은 메시지
  receiverView(ChatSendData _data, bool isLastReceiver, int index) {
    // log.d('『GGUMBI』>>> viewSize - receiverView : : ${sizeContent.width - (sizeTime.width + sizePhoto.width)}, sizeContent: $sizeContent, sizeTime: $sizeTime, sizePhoto: $sizePhoto  <<< ');
    double maxWidth = getMaxWidth(isSend: false);
    double maxCareWidth = getMaxWidthCare(isSend: false);
    return GestureDetector(
      onLongPress: () {
        Commons.showToast("복사되었습니다.");
        Clipboard.setData(ClipboardData(text: _data.msg));
      },
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: padding_05_LRB,
                child: lText(
                  getChatInfoData().talker_info!.first_name,
                  style: st_b_13(textColor: color_545454, fontWeight: FontWeight.bold),
                ),
              ),
              Stack(
                alignment: Alignment.topLeft,
                children: [
                  if (_data.msgtype == ChatMsgType.text)
                    ChatBubble(
                      elevation: 0.0,
                      clipper: ChatBubbleView(type: ChatSendType.receiver, isLast: isLastReceiver),
                      backGroundColor: Commons.getColorChatSend(isSend: false),
                      margin: EdgeInsets.only(bottom: isLastReceiver ? 15.0 : 8.0, left: 5.0),
                      child: Container(
                        constraints: BoxConstraints(
                          maxWidth: maxWidth,
                        ),
                        child: lText(
                          _data.msg,
                          style: st_b_15(),
                        ),
                      ),
                    ),
                  if (_data.msgtype == ChatMsgType.careInfo)
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        ChatBubble(
                          elevation: 0.0,
                          clipper: ChatBubbleView(type: ChatSendType.receiver, isLast: isLastReceiver),
                          backGroundColor: Commons.getColorChatSend(isSend: false),
                          margin: EdgeInsets.only(left: 5.0),
                          child: Container(
                            padding: padding_05_TB,
                            constraints: BoxConstraints(
                              maxWidth: maxCareWidth,
                            ),
                            child: Column(
                              children: [
                                chatCareInfoView(context, _data.msgdata, id: id, sender: _data.sender, callBack: (action, bookingId) {
                                  goDetailViewPage(bookingId, receiver: receiver);
                                }),
                                matchingChatView(receiverId: receiver, bookingcareservicesId: (_data.msgdata as MyCareResultsData).booking_id),
                              ],
                            ),
                          ),
                        ),
                        if (isLastTimeReceiver(isLastReceiver, index))
                          Padding(
                            padding: EdgeInsets.only(top: 5, bottom: isLastReceiver ? 15.0 : 8.0),
                            child: lText(
                              StringUtils.getStringToDate(_data.sendtime.toString(), format: F_HMM),
                              style: st_12(textColor: color_b2b2b2),
                            ),
                          ),
                      ],
                    ),
                ],
              ),
            ],
          ),
          if (_data.msgtype != ChatMsgType.careInfo && isLastTimeReceiver(isLastReceiver, index))
            Padding(
              // key: _keyTime,
              padding: EdgeInsets.only(bottom: isLastReceiver ? 15.0 : 8.0, left: 5.0),
              child: lText(
                StringUtils.getStringToDate(_data.sendtime.toString(), format: F_HMM),
                style: st_12(textColor: color_b2b2b2),
              ),
            ),
        ],
      ),
    );
  }

  Widget matchingChatView({int receiverId = 0, int bookingcareservicesId = 0}) {
    return InkWell(
      onTap: () {
        roomType = ChatRoomType.CREATE;
        receiver = receiverId;
        bookingcareservices = bookingcareservicesId;
        _requestChatView();
      },
      child: Padding(
        padding: padding_10_T,
        child: Container(
          width: sizeContent.width,
          child: lTextBtn('${"매칭채팅하기".tr()}',
              radius: 8,
              padding: padding_10,
              border: Border.all(color: Commons.getColor()),
              // bg: Commons.getColor().withAlpha(41),
              bg: color_transparent,
              rightIcon: Lcons.nav_right(size: 15, color: Commons.getColor()),
              style: st_b_15(
                fontWeight: FontWeight.w500,
                textColor: Commons.getColor(),
              )),
        ),
      ),
    );
  }

  double getMaxWidth({bool isSend = true}) {
    return sizeContent.width - (sizeTime.width + (isSend ? 0 : sizeProfile.width) + (isSend ? paddingSend : paddingReceiver));
  }

  ///돌봄신청서 공유시 사용
  double getMaxWidthCare({bool isSend = true}) {
    return sizeContent.width - ((isSend ? 0 : sizeProfile.width) + (isSend ? paddingSend : paddingReceiver)); //25 채팅 좌우 패딩값 - 프로필 이미지 오른쪽 패딩 5, 30
  }

  ///mqtt 세팅
  setPublishData() {
    mqttManager.setId(id);
    mqttManager.setReceiver(receiver);
    mqttManager.setChatroomId(chatroom_id);
  }

  ///상대방 받은 메시지 시간 체크 같은 시간이면 마지막 시간만 체크
  bool isLastTimeReceiver(bool isLastReceiver, int index) {
    int length = data.chatRoom.lsChat.length - 1;
    if (index < length) {
      afterRevDateTime = DateTime.parse(data.chatRoom.lsChat[index + 1].sendtime.toString());
      beforeRevDateTime = DateTime.parse(data.chatRoom.lsChat[index].sendtime.toString());
    } else {
      afterRevDateTime = DateTime.parse(data.chatRoom.lsChat[index].sendtime.toString());
      beforeRevDateTime = DateTime.parse(data.chatRoom.lsChat[index].sendtime.toString());
    }

    int before = beforeRevDateTime.day + beforeRevDateTime.hour + beforeRevDateTime.minute;
    int after = afterRevDateTime.day + afterRevDateTime.hour + afterRevDateTime.minute;
    bool isTime = true;
    if (before == after && !isLastReceiver) {
      isTime = false;
    }
    // log.d('『GGUMBI』>>> isLastTimeReceiver : isTime : $isTime, isLastReceiver : $isLastReceiver, before: $before, after: $after, ${beforeRevDateTime.minute}, ${afterRevDateTime.minute}, ${data.chatRoom.lsChat[index].msg}, ${data.chatRoom.lsChat[index].sendtime}<<< ');
    return isTime;
  }

  ///보낸 메시지 시간 체크 같은 시간이면 마지막 시간만 체크
  bool isLastTimeSend(bool isLastSend, int index) {
    int length = data.chatRoom.lsChat.length - 1;
    if (index < length) {
      afterDateTime = DateTime.parse(data.chatRoom.lsChat[index + 1].sendtime.toString());
      beforeDateTime = DateTime.parse(data.chatRoom.lsChat[index].sendtime.toString());
    } else {
      afterDateTime = DateTime.parse(data.chatRoom.lsChat[index].sendtime.toString());
      beforeDateTime = DateTime.parse(data.chatRoom.lsChat[index].sendtime.toString());
    }
    int before = beforeDateTime.day + beforeDateTime.hour + beforeDateTime.minute;
    int after = afterDateTime.day + afterDateTime.hour + afterDateTime.minute;
    bool isTime = true;
    if (before == after && !isLastSend) {
      isTime = false;
    }
    // log.d('『GGUMBI』>>> isLastTimeSend : isTime : $isTime, before: $before, after: $after, ${beforeDateTime.minute}, ${afterDateTime.minute}, ${data.chatRoom.lsChat[index].msg}, ${data.chatRoom.lsChat[index].sendtime}<<< ');
    return isTime;
  }

  ///메시지 날짜 시간 체크(2021년 7월 22일 2021년 7월 25일...)
  bool isLastTime(int index) {
    int length = data.chatRoom.lsChat.length - 1;
    bool isTime = false;
    if (index == 1) {
      isTime = true;
      return isTime;
    }
    if (index != 0 && index <= length) {
      beforeLastTime = DateTime.parse(data.chatRoom.lsChat[index - 1].sendtime.toString());
      afterLastTime = DateTime.parse(data.chatRoom.lsChat[index].sendtime.toString());

      int before = beforeLastTime.day;
      int after = afterLastTime.day;
      if (before != after) {
        isTime = true;
      }
      // log.d('『GGUMBI』>>> isLastTime : isTime : $isTime, before: $before, after: $after, ${beforeLastTime.day}, ${afterLastTime.day}, ${data.chatRoom.lsChat[index].msg}, ${data.chatRoom.lsChat[index].sendtime}<<< ');
    }
    return isTime;
  }

  ///사진 선택하기 (isMulti true, false)
  Future getImage({bool isMulti = true}) async {
    focusClear(context);
    ImagePicker imagePicker = ImagePicker();

    if (isMulti) {
      ///여러장 선택
      final List<XFile>? images = await imagePicker.pickMultiImage();
      images!.forEach((value) async {
        imageFile = File(value.path);
        if (imageFile != null) {
          flag.enableLoading();
          await uploadFile();
        }
      });
    } else {
      ///단일 선택
      PickedFile? pickedFile = await imagePicker.getImage(source: ImageSource.gallery);
      if (pickedFile != null) {
        imageFile = Commons.replaceFileName(File(pickedFile.path));
        if (imageFile != null) {
          flag.enableLoading();
          await uploadFile();
        }
      }
    }
  }

  ///사진 업로드
  Future uploadFile() async {
    _requestSendMessage("사진을보냈습니다".tr(), ChatMsgType.image, image: imageFile, msgdata: jsonEncode(chatInfoData));
    // Commons.showToast("API 호출 후 처리");
  }

  ///null 이면, 일반 채팅 아니면 돌봄 채팅으로간주
  setChatMatchingType(int status, bool isRequest, {ChatSendData? receiverData}) async {
    log.d('『GGUMBI』>>> setChatMatchingType : 채팅 종류: $status,  <<< ');
    _chatMatchingType = ChattingType.values.where((element) => element.value == status).last;
    if (_chatMatchingType == ChattingType.normal || getChatInfoData().carematching == null) {
      _matchingStatus = MatchingStatus.inprogress.value;
      _careStatus = CareStatus.inprogress.index;
    } else {
      if (isRequest) {
        ///채팅방 조회후 데이터
        _matchingStatus = getChatInfoData().carematching!.matching_status;
        _careStatus = getChatInfoData().carematching!.care_status;
        log.d('『GGUMBI』>>> setChatMatchingType : _careSatus: $_careStatus,  <<< ');
        log.d('『GGUMBI』>>> setChatMatchingType : _careSatus: ${getChatInfoData()},  <<< ');
        bookingcareservices = getChatInfoData().bookingcareservices;

        contractData = ContractData(
          contract_id: getChatInfoData().carematching!.contract_id,
          order_id: getChatInfoData().carematching!.order_id,
          momdady: getChatInfoData().carematching!.momdady,
          linkmom: getChatInfoData().carematching!.linkmom,
          care_status: getChatInfoData().carematching!.care_status,
          matching_status: getChatInfoData().carematching!.matching_status,
          deadline: getChatInfoData().carematching!.paid_deadline,
        );
        contractData.matching_status = MatchingStatus.paid.value;
        contractData.care_status = CareStatus.ready.value;

        data.chatRoom.careItem = MyCareResultsData(
          booking_id: getChatInfoData().bookingcareservices_info!.careinfo!.booking_id,
          childinfo: getChatInfoData().bookingcareservices_info!.childinfo,
          possible_area: Commons.getAreaToString(EnumUtils.getPossibleArea(getChatInfoData().bookingcareservices_info!.careinfo!.possible_area)),
          servicetype: Commons.getServiceTypeToString2(EnumUtils.getServiceType(index: getChatInfoData().bookingcareservices_info!.careinfo!.servicetype)),
          // possible_area: getChatInfoData().bookingcareservices_info.careinfo.possible_area,
          // servicetype: getChatInfoData().bookingcareservices_info.careinfo.servicetype,
          cost_sum: getChatInfoData().bookingcareservices_info!.careinfo!.cost_sum,
          // status: getChatInfoData().carematching.care_status,
          carescheduleinfo: CareScheduleInfo(
            stime: getChatInfoData().bookingcareservices_info!.careinfo!.stime,
            etime: getChatInfoData().bookingcareservices_info!.careinfo!.etime,
            caredate: getChatInfoData().bookingcareservices_info!.careinfo!.caredate,
          ),
        );
      } else {
        ///채팅에서 수신받아서 처리
        log.d('『GGUMBI』>>> setChatMatchingType : receiverData: $receiverData,  <<< ');
        if (receiverData != null && receiverData.msgdata is ContractData) {
          ContractData contractData = receiverData.msgdata as ContractData;
          _matchingStatus = contractData.matching_status;
          _careStatus = contractData.care_status;
          getChatInfoData().carematching!.order_id = contractData.order_id;
          getChatInfoData().carematching!.contract_id = contractData.contract_id;
          bookingcareservices = getChatInfoData().bookingcareservices;

          //2022/01/18 상대방 사유로 돌봄취소시 알림창 추가
          WidgetsBinding.instance.addPostFrameCallback((_) {
            if (contractData.popup_code > 0) {
              RouteAction.showEmergencyAlert(contractData.popup_code, momdaddy: contractData.momdady_first_name, linkmom: contractData.linkmom_first_name, date: contractData.deadline, callback: () {
                Commons.page(context, routePayMentCancelList, arguments: PayInfoRequest(contract_id: contractData.contract_id));
              });
            }
          });
          log.d('『GGUMBI』>>> setChatMatchingType 0 : _matchingSatus: $_matchingStatus, _careSatus: $_careStatus, bookingcareservices: $bookingcareservices,<<< ');
        }
      }
      log.d('『GGUMBI』>>> setChatMatchingType : _matchingSatus: $_matchingStatus, _careSatus: $_careStatus, bookingcareservices: $bookingcareservices,<<< ');
    }
    _buildMenu(_chatMatchingType, _matchingStatus, _careStatus);
    _checkInfoAlert();
  }

  ///신고하기 활성화 유므, 돌봄중(2)/돌봄부분취소(4)시에만 비활성화 나머지 활성화
  bool isCs() {
    try {
      return getChatInfoData().carematching!.care_status == CareStatus.inprogress.value || getChatInfoData().carematching!.care_status == CareStatus.cancelParts.value;
    } catch (e) {
      return false;
    }
  }

  ///하단 메뉴 만들기
  Map<Widget, Function> _buildMenu(ChattingType chatMatchingType, int matchingSates, int careSates) {
    log.d('『GGUMBI』>>> _buildMenu : _menu.isNotEmpty: $_menu,  <<< ');
    if (_menu != null) {
      _menu!.clear();
    }

    log.d('『GGUMBI』>>> _buildMenu : chatMatchingType: $chatMatchingType, matchingSates: $matchingSates, careSates: $careSates, <<< ');

    ///일반 채팅 인 경우
    if (chatMatchingType == ChattingType.normal) {
      _menu = {
        lText(
          "돌봄신청서_공유".tr(),
          style: st_b_16(fontWeight: FontWeight.w500, textColor: color_545454, disableColor: color_cecece),
        ): () async {
          dynamic result = await Commons.page(context, routeChatCareInfo, arguments: ChatCareInfoPage(receiver: receiver, className: '$ChatRoomPage')) as MyCareResultsData;
          // MyCareResultsData//돌봄신청서 내용
          log.d('『GGUMBI』>>> _buildMenu : result: $result,  <<< ');
          if (result != null) {
            if (!getChatInfoData().talker_info!.is_active || getChatInfoData().talker_info!.is_block) {
              Commons.showToast("채팅차단_안내".tr());
              isSend = true;
              return;
            }
            _requestSendMessage("돌봄신청서를공유하였습니다".tr(), ChatMsgType.careInfo, msgdata: jsonEncode(result));
          }
        }
      };

      ///돌봄 채팅 인 경우
    } else {
      CareStatus care = CareStatus.none;
      MatchingStatus match;
      match = EnumUtils.getMatchingStatus(index: matchingSates); //매칭 상태값 세팅하기
      care = EnumUtils.getCareStatus(index: careSates); //돌봄 상태값 세팅하기

      log.d('『GGUMBI』>>> _buildMenu : match: $match, care: $care, <<< ');

      //매칭중(대화중)일 경우에만 활성화, 그 외 비 활성화
      //재매칭인 경우 체크 돌봄상태 0 && 매칭상태 11, 12, 13, 14, 21, 22, 23
      var isMatchingRequest = match == MatchingStatus.inprogress ||
          care == CareStatus.none &&
              (match == MatchingStatus.inprogress ||
                  match == MatchingStatus.failNoRespL ||
                  match == MatchingStatus.failNoRespM ||
                  match == MatchingStatus.failDisagreeL ||
                  match == MatchingStatus.failDisagreeM ||
                  match == MatchingStatus.cancelLinkmom ||
                  match == MatchingStatus.cancelMomdaddy ||
                  match == MatchingStatus.cancelPaid);

      //기간 만료 인 경우 비활성화
      if (isExpired()) {
        isMatchingRequest = false;
      }

      //매칭 수락하기
      var isMatchingConfirm = false;
      if (!Commons.isLinkMom() && match.value == MatchingStatus.waitLinkmom.value || Commons.isLinkMom() && match.value == MatchingStatus.waitMomdaddy.value) {
        isMatchingConfirm = true;
      }

      log.d('『GGUMBI』>>> _buildMenu : isMatchingConfirm: $isMatchingConfirm, $match, $matchingSates <<< ');
      //계약대기 경우에만 활성화, 그 외 비 활성화
      var isMatchingCancel = match == MatchingStatus.waitMomdaddy || match == MatchingStatus.waitLinkmom || match == MatchingStatus.waitPaid;

      //결제대기
      var isPay = match == MatchingStatus.waitPaid;

      //결제완료(100)이고, 돌봄상태가 예정, 돌봄중일 경우만 활성화, 그외 비 활성화
      var isCareCancel = match == MatchingStatus.paid && (care == CareStatus.ready || care == CareStatus.inprogress || care == CareStatus.cancelParts) /* ? true : false*/;

      log.d('『GGUMBI』>>> _buildMenu : isMatchingRequest: $isMatchingRequest, isMatchingCancel: $isMatchingCancel, isCareCancel: $isCareCancel, <<< ');
      _menu = {
        lText(
          "매칭요청하기".tr(),
          style: st_b_16(fontWeight: FontWeight.w500, isSelect: isMatchingRequest, textColor: color_cecece, disableColor: color_545454),
        ): !isMatchingRequest
            ? () => {}
            : () {
                ///맘대디가 매칭요청하기 신청시
                ///1.매칭 진행 안내 다이얼로그 출력
                ///2.돌봄계약서 화면으로 이동(돌볌계약서 조회 contract/view)
                ///3.서명하기 진행 후 api 호출
                goContractPage();
              },

        lText(
          "매칭수락하기".tr(),
          style: st_b_16(fontWeight: FontWeight.w500, isSelect: isMatchingConfirm, textColor: color_cecece, disableColor: color_545454),
        ): isMatchingConfirm ? () => goContractPage() : () => {},
        // ): () => goContractPage(),

        lText(
          "매칭취소하기".tr(),
          style: st_b_16(fontWeight: FontWeight.w500, isSelect: isMatchingCancel, textColor: color_cecece, disableColor: color_545454),
        ): isMatchingCancel
            ? () {
                String name = '${getChatInfoData().talker_info!.first_name} ${storageHelper.user_type == USER_TYPE.mom_daddy ? "링크쌤".tr() + "과".tr() : "맘대디".tr() + "와".tr()}';
                showNormalDlg(
                  // msg: '${"매칭취소하기_안내".tr()}\n $name ${"매칭취소하기_취소".tr()}',
                  btnLeft: "아니오".tr(),
                  btnRight: "예".tr(),
                  messageWidget: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      lText(
                        "매칭취소하기_안내".tr(),
                        style: st_b_15(),
                        textAlign: TextAlign.center,
                      ),
                      sb_h_10,
                      lText(
                        '$name ${"매칭취소하기_취소".tr()}',
                        style: st_b_15(fontWeight: FontWeight.w500),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                  onClickAction: (action) {
                    if (action == DialogAction.yes) {
                      _requestMatchingCancel();
                    }
                  },
                );
              }
            : () => {},

        if (storageHelper.user_type == USER_TYPE.mom_daddy)
          lText(
            "결제하기".tr(),
            style: st_b_16(fontWeight: FontWeight.w500, isSelect: isPay, textColor: color_cecece, disableColor: color_545454),
          ): isPay ? () => goPaymentPage() : () => {},

        lText(
          "돌봄취소하기".tr(),
          style: st_b_16(fontWeight: FontWeight.w500, isSelect: isCareCancel, textColor: color_cecece, disableColor: color_545454),
        ): isCareCancel
            ? () {
                showNormalDlg(
                  message: '${"돌봄취소하기_안내".tr()}',
                  btnLeft: "취소".tr(),
                  btnRight: "돌봄취소하기".tr(),
                  onClickAction: (action) {
                    if (action == DialogAction.yes) {
                      goCareCancelPage();
                    }
                  },
                );
              }
            : () => {},
      };
      //결제 완료후 사진을 추가한다.
      if (match == MatchingStatus.paid || care.index > CareStatus.inprogress.index) {
        _menu!.addAll({
          lText(
            "사진".tr(),
            style: st_b_16(fontWeight: FontWeight.w500),
          ): () => getImage()
        });
      }
      log.d('『GGUMBI』>>> _buildMenu : menu: $_menu,  <<< ');
    }
    return _menu!;
  }

  ///돌봄 신청서 보기
  goDetailViewPage(int bookingId, {int? receiver = 0, BottomBtnType btnType = BottomBtnType.chat}) {
    Commons.page(context, routeMomDadyProfile,
        arguments: MomDadyProfilePage(
          viewMode: ViewMode(viewType: ViewType.view),
          bookingId: bookingId,
          chattingtype: getChatInfoData().chattingtype,
          receiver: receiver!,
          btnType: btnType,
        ));
  }

  ///매칭요청 -> 계약서 수락하기 서명
  goContractPage() {
    if (storageHelper.user_type == USER_TYPE.mom_daddy) {
      showNormalDlg(
        message: "매칭진행_안내".tr(),
        onClickAction: (action) {
          goMatchingPage(itemType: ItemType.contract);
        },
      );
    } else {
      goMatchingPage(itemType: ItemType.contract);
    }
  }

  ///돌봄신청서 상세 또는, 돌봄계약서 작성시 사용
  goMatchingPage({String viewName = '', ItemType? itemType}) async {
    log.d('『GGUMBI』>>> goMatchingPage ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
    var callBackData = await Commons.page(context, routeMomDadyProfile,
        arguments: MomDadyProfilePage(
          viewMode: ViewMode(viewType: ViewType.view, itemType: itemType!),
          bookingId: bookingcareservices!,
          chattingtype: getChatInfoData().chattingtype,
          btnType: BottomBtnType.contract,
          matchingData: MatchingInitRequest(
            receiver: receiver,
            bookingcareservices: bookingcareservices!,
            chattingroom: chatroom_id!,
            // matchingStatus: matchingStatus == null ? _matchingStatus : matchingStatus,
            matchingStatus: _matchingStatus,
            carematching: getChatInfoData().carematching!.matching_id,
          ),
        ));

    if (callBackData != null) {
      contractData = callBackData;
      addChatList(getSendData(msgtype: ChatMsgType.matching, msg: "매칭요청_메시지".tr(), msgdata: callBackData));
    }
    log.d('『GGUMBI』>>> goMatchingPage : callBackData: $callBackData,  <<< ');
  }

  ///맘대디 또는 링크쌤 찾기로 이동
  goFindPage({int index = 3}) {
    Commons.pageToMainTabMove(context, tabIndex: index);
    // Commons.pageToMain(context, routeHome,
    //     arguments: MainHome(
    //       tabIndex: index,
    //     ));
  }

  ///결제하기
  goPaymentPage() async {
    if (!await Commons.isEmailAuthCheck(context)) {
      return;
    }
    dynamic result = await Commons.page(context, routePayMent,
        arguments: PaymentPage(
          data: PayInitRequest(
            bookingcareservices: bookingcareservices!,
            carematching: getChatInfoData().carematching!.matching_id,
            carecontract: getChatInfoData().carematching!.contract_id,
          ),
          callBack: (oderId) {
            log.d('『GGUMBI』>>> goPaymentPage : oderId: $oderId,  <<< ');
            ContractData contractData = ContractData.clone(this.contractData);
            contractData.order_id = oderId;
            contractData.matching_status = MatchingStatus.paid.value;
            contractData.care_status = CareStatus.ready.value;
            addChatList(getSendData(msgtype: ChatMsgType.matching, msg: "결제가완료되었습니다".tr(), msgdata: contractData));
          },
        ));
    log.d('『GGUMBI』>>> _buildMenu : result: $result,  <<< ');
  }

  ///돌봄취소하기 페이지 이동
  goCareCancelPage() async {
    log.d('『GGUMBI』>>> goCareCancelPage : ${getBookingInfoData().careinfo!} <<< ');
    log.d('『GGUMBI』>>> goCareCancelPage : contractData: ${getChatInfoData().carematching},  <<< ');
    dynamic result = await Commons.page(context, routePayMentCancelReason,
        arguments: PaymentCancelReasonPage(
          payCancelInitRequest: PayCancelInitRequest(
            contract_id: getChatInfoData().carematching!.contract_id,
            order_id: getChatInfoData().carematching!.order_id,
          ),
          callBack: (resultData) {
            log.d('『GGUMBI』>>> goCareCancelPage : PayCanceSaveExplainData: $resultData,  <<< ');
            if (resultData != null) {
              PayCanceSaveExplainData data = resultData as PayCanceSaveExplainData;
              ContractData contractData = ContractData.clone(this.contractData);
              contractData.cancel_id = data.cancel_id;
              contractData.cancelreason = data.cancelreason;
              contractData.cancel_usertype = data.cancel_usertype;
              contractData.is_cancelexplain = data.is_cancelexplain!;
              contractData.is_totalcancel = data.is_totalcancel!;
              contractData.contract_id = getChatInfoData().carematching!.contract_id; //취소 요청후 응답 값에 없기 때문에 세팅!
              contractData.matching_status = data.contract_info!.matching_status;
              contractData.care_status = data.contract_info!.care_status;
              contractData.momdady_first_name = data.contract_info!.momdady_first_name;
              contractData.linkmom_first_name = data.contract_info!.linkmom_first_name;
              contractData.cancel_usertype = data.cancel_usertype;
              contractData.cancelreason = data.cancelreason;
              contractData.deadline = data.cancelexplain_deadline!;
              contractData.is_cancelexplain = data.is_cancelexplain!;
              contractData.is_totalcancel = data.is_totalcancel!;
              addChatList(getSendData(msgtype: ChatMsgType.matching, msg: "돌봄일정취소_메시지".tr(), msgdata: contractData));
            }
          },
        ));
    log.d('『GGUMBI』>>> _buildMenu : result: $result,  <<< ');
  }

  ///돌붐취소 내역
  goCancelViewPage(int cancelId) {
    log.d('『GGUMBI』>>> goCancelViewPage : getChatInfoData().carematching!.contract_id,: ${getChatInfoData().carematching!.contract_id},  <<< ');
    log.d('『GGUMBI』>>> goCancelViewPage : getChatInfoData().carematching!.order_id,: ${getChatInfoData().carematching!.order_id},  <<< ');
    Commons.page(context, routePayMentCancel,
        arguments: PaymentCancelPage(
          viewType: PayViewType.cancel_view,
          payCancelViewRequest: PayCancelViewRequest(
            contract_id: getChatInfoData().carematching!.contract_id,
            order_id: getChatInfoData().carematching!.order_id,
            cancel_id: cancelId,
          ),
          callBack: (resultData) {
            log.d('『GGUMBI』>>> goCareCancelPage : PayCanceSaveExplainData: $resultData,  <<< ');
            if (resultData != null) {
              PayCanceSaveExplainData data = resultData as PayCanceSaveExplainData;
              ContractData contractData = ContractData.clone(this.contractData);
              contractData.cancel_id = data.cancel_id;
              contractData.cancelreason = data.cancelreason;
              contractData.cancel_usertype = data.cancel_usertype;
              contractData.is_cancelexplain = data.is_cancelexplain!;
              contractData.is_totalcancel = data.is_totalcancel!;
              contractData.contract_id = getChatInfoData().carematching!.contract_id; //취소 요청후 응답 값에 없기 때문에 세팅!
              contractData.matching_status = data.contract_info!.matching_status;
              contractData.care_status = data.contract_info!.care_status;
              contractData.momdady_first_name = data.contract_info!.momdady_first_name;
              contractData.linkmom_first_name = data.contract_info!.linkmom_first_name;
              contractData.isLinkMom = data.cancel_usertype == CancelUserType.momdady.value ? false : true;
              contractData.deadline = data.cancelexplain_deadline!;
              addChatList(getSendData(msgtype: ChatMsgType.matching, msg: "돌봄일정취소_메시지".tr(), msgdata: contractData));
            }
          },
        ));
  }

  goRoomDelete(int roomId) {
    showNormalDlg(
        message: "채팅삭제_내용".tr(),
        btnLeft: "취소".tr(),
        btnRight: "나가기".tr(),
        onClickAction: (action) {
          if (action == DialogAction.yes) {
            List<int> lsDelete = [];
            lsDelete.add(roomId);
            apiHelper.requestChatRoomDelete(ChatRoomDeleteRequest(chatroom_id: lsDelete)).then((response) {
              if (response.getCode() == KEY_SUCCESS) {
                Commons.pagePop(context, data: ChatBackItem(isDelete: true));
              }
            });
          }
        });
  }

  ///신고하기 화면으로 이동
  goNotify(int receiver, int chatroomId, int matchingId, String? userName) {
    List<NotifyReason> reasons = [];

    if (getChatInfoData().chattingtype == ChattingType.normal.value) {
      Commons.page(context, routeCommunityNotify,
          arguments: CommunityNotifyPage(
            userId: id,
            boardId: 0,
            userName: userName,
          ));
    } else {
      String desc = "신고안내_돌봄".tr();
      if (Commons.isLinkMom()) {
        if (getChatInfoData().carematching!.care_status == CareStatus.none.value) {
          reasons = [
            NotifyReason.momdadyOutOfPlatform,
            NotifyReason.momdadyInappropriate,
            NotifyReason.momdadyAd,
            NotifyReason.other,
          ];
          desc = "신고안내_일반".tr();
        } else {
          reasons = [
            NotifyReason.momdadyRequestOT,
            NotifyReason.momdadyDiscountCost,
            NotifyReason.momdadyUnpaidOT,
            NotifyReason.momdadyNoshow,
            NotifyReason.momdadyOutOfPlatform,
            NotifyReason.momdadyInsult,
            NotifyReason.momdadyAssult,
            NotifyReason.momdadyAd,
            NotifyReason.momdadyInappropriate,
            NotifyReason.other,
          ];
        }
      } else {
        if (getChatInfoData().carematching!.care_status == CareStatus.none.value) {
          reasons = [
            NotifyReason.linkmomAd,
            NotifyReason.linkmomInappropriate,
            NotifyReason.linkmomOutOfPlatform,
            NotifyReason.other,
          ];
          desc = "신고안내_일반".tr();
        } else {
          reasons = [
            NotifyReason.linkmomNotSaveTime,
            NotifyReason.linkomoNoshow,
            NotifyReason.linkmomChargingOther,
            NotifyReason.linkmomOutOfPlatform,
            NotifyReason.linkmomInsult,
            NotifyReason.linkmomAssult,
            NotifyReason.linkmomAd,
            NotifyReason.linkmomInappropriate,
            NotifyReason.other,
          ];
        }
      }
      Commons.page(
        context,
        routeCareNotify,
        arguments: CareNotifyPage(
          reasons: reasons,
          matchingId: matchingId,
          userId: receiver,
          chatId: chatroomId,
          userName: userName,
          desc: desc,
        ),
      );
    }
  }

  ///채팅방 정보 데이터
  setChatInfoData(ChatRoomData roomData) {
    log.d('『GGUMBI』>>> setChatInfoData : roomData: $roomData,  <<< ');
    data.chatRoom.chatInfo = roomData;
  }

  ///채팅방 정보 데이터
  ChatRoomData getChatInfoData() {
    return data.chatRoom.chatInfo;
  }

  ///채팅방 정보 데이터 - 돌봄신청서 정보
  ChatBookingCareServicesInfoData getBookingInfoData() {
    return data.chatRoom.chatInfo.bookingcareservices_info!;
  }

  ///기간만료 체크
  bool isExpired() {
    return getBookingInfoData().careinfo!.booking_status == BookingStatus.expired.value;
  }

  ///돌봄신청서 수정 체크
  bool isCareModify() {
    return !Commons.isLinkMom() && _matchingStatus <= MatchingStatus.waitLinkmom.value || !Commons.isLinkMom() && isExpired();
  }

  ///맘대디 이름
  String getMomdadyName() {
    return Commons.isLinkMom() ? getChatInfoData().talker_info!.first_name : getChatInfoData().bookingcareservices_info!.userinfo!.first_name;
  }

  ///링크쌤 이름
  String getLinkMomName() {
    return Commons.isLinkMom() ? getChatInfoData().bookingcareservices_info!.userinfo!.first_name : getChatInfoData().talker_info!.first_name;
  }

  ///차단하기 타이틀
  String getBlockTitle() {
    String name = getChatInfoData().talker_info!.first_name;
    String value = "차단하시겠습니까".tr();

    if (getChatInfoData().chattingtype != ChattingType.normal.value) {
      value = Commons.isLinkMom() ? "맘대디를차단하시겠습니까".tr() : "링크쌤을차단하시겠습니까".tr();
    }

    return '$name $value';
  }

  ///차단하기 내용
  String getBlockContent() {
    return Commons.isLinkMom() ? "맘대디차단_안내".tr() : "링크쌤차단_안내".tr();
  }

  ///채팅방에 있고, 상대방 메시지 수신시, 읽지 않읂게 있을 경우 체크해서 변경해준다.
  isReceive() {
    if (!_isReceive) {
      data.chatRoom.lsChat.forEach((value) {
        if (!value.is_receive) {
          _isReceive = true;
        }
      });

      if (_isReceive) setReceive();
    }
  }

  ///메시지 읽은 처리
  setReceive({bool isReceive = true}) {
    data.chatRoom.lsChat.forEach((value) {
      value.is_receive = isReceive;
    });
  }

  void _showInfoAlert(int bookingId, {USER_TYPE? type}) {
    WidgetSpan menu = WidgetSpan(
        alignment: PlaceholderAlignment.middle,
        child: Container(
          width: 14,
          height: 14,
          padding: padding_03,
          margin: padding_01_R,
          decoration: BoxDecoration(color: Commons.getColor(), borderRadius: BorderRadius.circular(4)),
          child: Lcons.menu(size: 10),
        ));
    if (type == null) {
      showNormalDlg(
        messageWidget: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 20),
              child: Lcons.error_outline(color: color_545454, size: 30),
            ),
            RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    TextSpan(text: "일반채팅_신청서안내_1".tr(), style: st_b_14()),
                    menu,
                    TextSpan(text: "일반채팅_신청서안내_2".tr(), style: st_b_14()),
                    TextSpan(text: "돌봄신청서_공유".tr(), style: st_14(textColor: color_main)),
                    TextSpan(text: "일반채팅_신청서안내_3".tr(), style: st_b_14()),
                  ],
                ))
          ],
        ),
      );
    } else if (type == USER_TYPE.mom_daddy) {
      showNormalDlg(
        messageWidget: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 20),
              child: Lcons.error_outline(color: color_545454, size: 30),
            ),
            infoView("수정관련_안내_맘대디_1".tr(), widgetSpan: menu, content3: "버튼을눌러진행해주세요".tr()),
            sb_h_10,
            infoView("수정관련_안내_맘대디_2".tr()),
            sb_h_10,
            infoView("수정관련_안내_맘대디_3".tr()),
          ],
        ),
      );
    } else {
      showNormalDlg(
          messageWidget: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 20),
            child: Lcons.error_outline(color: color_545454, size: 30),
          ),
          infoView("수정관련_안내_링크쌤".tr(), widgetSpan: menu, content3: "버튼을눌러진행해주세요".tr(), color: color_545454),
        ],
      ));
    }
  }

  void _checkInfoAlert() {
    late String key;
    if (_chatMatchingType == ChattingType.normal) {
      key = Flags.KEY_SHOW_CHAT_ALERT_NORMAL;
    } else {
      if (Commons.isLinkMom()) {
        key = Flags.KEY_SHOW_CHAT_ALERT_LINKMOM;
      } else {
        key = Flags.KEY_SHOW_CHAT_ALERT_MOMDADDY;
      }
    }
    Flags.action(
      context,
      key,
      () => _showInfoAlert(bookingcareservices ?? 0, type: _chatMatchingType == ChattingType.normal ? null : currentType),
    );
  }
}

class ChatBackItem {
  int matchingStatus;
  ChatSendData? chatData;
  bool isDelete = false;

  ChatBackItem({this.matchingStatus = 0, this.chatData, this.isDelete = false});

  @override
  String toString() {
    return 'ChatBackItem{matchingStatus: $matchingStatus, chatData: $chatData, isDelete: $isDelete}';
  }
}

class ChatScrollItem {
  int position;

  ChatScrollItem({this.position = 0});

  @override
  String toString() {
    return 'ChatScrollItem{position: $position,}';
  }
}
