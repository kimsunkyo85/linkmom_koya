import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/chat_room_init_request.dart';
import 'package:linkmom/data/network/models/data/chat_room_data.dart';
import 'package:linkmom/data/network/models/matching_init_request.dart';
import 'package:linkmom/data/network/models/matching_view_request.dart';
import 'package:linkmom/data/network/models/momdady_mycares_list_request.dart';
import 'package:linkmom/data/network/models/momdady_mycares_list_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/job_apply/job_profile/momdady_profile_page.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../../../route_name.dart';
import '../../lcons.dart';
import 'chat_room_page.dart';
import 'chat_view/chat_care_info_view.dart';

class ChatCareInfoPage extends BaseStateful {
  @override
  _ChatCareInfoPageState createState() => _ChatCareInfoPageState(receiver: receiver!, className: className!);

  ChatCareInfoPage({this.data, this.receiver = 0, this.className = ''});

  final DataManager? data;

  final int? receiver;

  final String? className;
}

class _ChatCareInfoPageState extends BaseStatefulState<ChatCareInfoPage> {
  final int receiver;
  final String className;
  late int bookingcareservices;
  late ScrollController _controller;
  bool _visible = false;

  _ChatCareInfoPageState({
    this.receiver = 0,
    this.className = '',
  });

  @override
  initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    log.d('『GGUMBI』>>> initState : className: $className,  <<< ');
    super.initState();
    onData(widget.data ?? DataManager());
    _requestCaresList();
  }

  ///스크롤 리스너
  _scrollListener() {
    if (_controller.offset <= _controller.position.minScrollExtent && !_controller.position.outOfRange) {
      // if (_visible) {
      //   _visible = false;
      //   onUpDate();
      // }
    } else {
      if (!_visible) {
        _visible = true;
        onUpDate();
      }
    }

    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      ///조회된 데이터와, 리스트 갯수가 같으면 아무런 처리를하지않는다.
      if (data.chatCareInfo.responseData!.count == data.chatCareInfo.lsCareInfo.length) {
        return;
      }
      if (_visible) {
        _visible = false;
        //연속 조회
        _requestCaresList();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        key: data.scaffoldKey,
        appBar: appBar(
          "돌봄신청서_공유".tr(),
          isClose: false,
          hide: false,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: padding_20,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //・
                  lText("돌봄신청서_안내".tr(), style: st_b_16(fontWeight: FontWeight.w500)),
                  sb_h_10,
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      lText('・ ', style: st_b_13(textColor: color_545454)),
                      Expanded(
                        child: lText("돌봄신청서안내_내용1".tr(), style: st_b_13(textColor: color_545454)),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      lText('・ ', style: st_b_13(textColor: color_545454)),
                      Expanded(
                        child: lText(
                          "돌봄신청서안내_내용2".tr(),
                          style: st_b_13(textColor: color_545454),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: LinkmomRefresh(
                anotherScrollView: true,
                onRefresh: _onRefresh,
                child: Stack(
                  children: [
                    lScrollbar(
                      child: ListView.builder(
                          controller: _controller,
                          itemCount: data.chatCareInfo.lsCareInfo.length,
                          physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                          itemBuilder: (BuildContext context, int index) {
                            MyCareResultsData item = data.chatCareInfo.lsCareInfo[index];
                            return lContainer(
                              margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
                              child: Card(
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  // borderRadius: BorderRadius.zero,
                                  side: BorderSide(width: 1, color: color_eeeeee),
                                  borderRadius: BorderRadius.all(Radius.circular(16)),
                                ),
                                margin: padding_0,
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                  child: chatCareInfoView(context, item, viewName: '$ChatCareInfoPage', callBack: (action, bookingId) {
                                    if (action == DialogAction.select) {
                                      Commons.page(context, routeMomDadyProfile,
                                          arguments: MomDadyProfilePage(
                                            viewMode: ViewMode(viewType: ViewType.view),
                                            bookingId: bookingId,
                                          ));
                                    } else if (action == DialogAction.toggle) {
                                      setState(() {
                                        data.chatCareInfo.lsCareInfo.forEach((value) => value.isSelected = false);
                                        item.isSelected = true;
                                        data.chatCareInfo.selectItem = item;
                                        bookingcareservices = item.booking_id;
                                        onConfirmBtn();
                                      });
                                    }
                                  }),
                                ),
                              ),
                            );
                          }),
                    ),
                    if (data.chatCareInfo.lsCareInfo.isEmpty && !flag.isLoading) _emptyView(),
                  ],
                ),
              ),
            ),
            _confirmButton(),
          ],
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    String title = "돌봄채팅".tr();
    if (className == '$ChatRoomPage') {
      title = "공유하기".tr();
    }
    return Row(
      children: [
        Expanded(
          child: lBtn(title, isEnabled: flag.isConfirm, margin: EdgeInsets.fromLTRB(20, 20, 10, 20), onClickAction: () {
            if (className == '$ChatRoomPage') {
              Commons.pagePop(context, data: data.chatCareInfo.selectItem);
            } else {
              Commons.pageToMain(
                context,
                routeChatRoom,
                arguments: ChatRoomPage(roomType: ChatRoomType.VIEW, receiver: receiver, bookingcareservices: bookingcareservices),
              );
            }
          }),
        ),
        Expanded(
          child: lBtn("매칭요청하기".tr(), isEnabled: flag.isConfirm, margin: EdgeInsets.fromLTRB(10, 20, 20, 20), onClickAction: () async {
            await apiHelper.requestMatchingView(MatchingViewRequest(receiver: receiver, bookingcareservices: bookingcareservices)).then((response) async {
              if (response.getCode() == KEY_SUCCESS) {
                showNormalDlg(
                    message: "진행중인계약_안내".tr(),
                    btnLeft: "아니오".tr(),
                    btnRight: "예".tr(),
                    onClickAction: (action) {
                      if (action == DialogAction.yes) {
                        Commons.pageToMain(
                          context,
                          routeChatRoom,
                          arguments: ChatRoomPage(roomType: ChatRoomType.VIEW, receiver: receiver, bookingcareservices: bookingcareservices),
                        );
                      }
                    });
                // TODO: GGUMBI 2022/02/07 - 추후 돌봄관리쪽 수정후 돌봄진행상황으로 보낸다. 우선, 돌봄채팅으로 보내기!
                // Commons.page(context, routeMomdadyApply,
                //     arguments: MomdadyApplyPage(
                //       data: data,
                //       bookingId: bookingcareservices,
                //       info: data.chatCareInfo.selectItem,
                //     ));
                //매칭요청/수락 (matching_init)으로 채팅 이동
              } else if (response.getCode() == KEY_4000_NO_DATA) {
                await apiHelper.requestChatRoomInit(ChatRoomInitRequest(receiver: receiver, bookingcareservices: bookingcareservices)).then((response) async {
                  if (response.getCode() == KEY_SUCCESS) {
                    ChatRoomData chatData = response.getData();
                    int chatroomId = chatData.chatroom_id;
                    var callBackData = await Commons.page(context, routeMomDadyProfile,
                        arguments: MomDadyProfilePage(
                          viewMode: ViewMode(viewType: ViewType.view, itemType: ItemType.contract),
                          bookingId: bookingcareservices,
                          chattingtype: chatData.chattingtype,
                          btnType: BottomBtnType.contract,
                          matchingData: MatchingInitRequest(
                            receiver: receiver,
                            bookingcareservices: bookingcareservices,
                            chattingroom: chatroomId,
                            // matchingStatus: matchingStatus == null ? _matchingStatus : matchingStatus,
                            matchingStatus: chatData.carematching!.matching_status,
                            carematching: chatData.carematching!.matching_id,
                          ),
                        ));

                    if (callBackData != null) {
                      mqttManager.remove();
                      Commons.pageToMain(
                        context,
                        routeChatRoom,
                        arguments: ChatRoomPage(roomType: ChatRoomType.CREATE, receiver: receiver, bookingcareservices: bookingcareservices),
                      );
                    }
                  } else {
                    flag.disableLoading(fn: () => onUpDate());
                    Commons.showSnackBar(context, response.getMsg());
                  }
                }).catchError((e) {
                  flag.disableLoading(fn: () => onUpDate());
                });
              }
            });
          }),
        ),
      ],
    );
  }

  @override
  void onDataPage(_data) {
    super.onDataPage(_data);
    onConfirmBtn();
  }

  @override
  void onConfirmBtn() {
    bool isSelect = false;
    data.chatCareInfo.lsCareInfo.forEach((value) {
      if (value.isSelected) isSelect = true;
    });

    if (isSelect) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  //검색 데이터 세팅 or 초기화
  void setRequestData() {
    flag.disableConfirm(fn: () => onUpDate());
  }

  ///재조회
  Future<Null> _onRefresh() async {
    setRequestData();
    _requestCaresList();
  }

  _requestCaresList() {
    flag.enableLoading(fn: () => onUpDate());
    String url = ApiEndPoint.EP_MOMDADY_MYCARES_LIST;
    if (StringUtils.validateString(data.chatCareInfo.responseData?.next)) {
      url = data.chatCareInfo.responseData!.next;
    }
    apiHelper.requestMomdadyMyCaresList(MomdadyMyCaresListRequest.reqeustChat(), url: url).then((response) {
      flag.disableLoading(fn: () => onUpDate());
      data.chatCareInfo.lsCareInfo.clear();
      if (response.getCode() == KEY_SUCCESS) {
        data.chatCareInfo.responseData = response.getData();
        data.chatCareInfo.lsCareInfo.addAll(response.getData().results as List<MyCareResultsData>);
      }
    });
  }

  Widget _emptyView() {
    return Center(
      child: Container(
          padding: padding_30_LR,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Lcons.empty(size: 100),
              sb_h_10,
              // TextHighlight(text: "돌봄신청서_공유_안내".tr(), term: "돌봄관리".tr(), textStyle: st_b_14(), textStyleHighlight: st_b_14(fontWeight: FontWeight.w700)),
              RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(children: [
                    TextSpan(text: "돌봄신청서_공유_안내_1".tr(), style: st_b_14()),
                    TextSpan(
                        text: '\n${"돌봄관리".tr()}',
                        style: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.bold, decoration: TextDecoration.underline),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            Commons.pageToMainTabMove(context, tabIndex: TAB_SCHEDULE);
                          }),
                    TextSpan(text: "돌봄신청서_공유_안내_2".tr(), style: st_b_14()),
                  ])),
            ],
          )),
    );
  }
}
