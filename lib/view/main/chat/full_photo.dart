import 'dart:io';

import 'package:flutter/material.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../lcons.dart';

class FullPhotoPage extends StatelessWidget {
  final String name;
  final int index;
  final List<String>? urlList;
  final List<File>? fileList;
  final BoxFit fit;
  final Function? onPageChanged;

  FullPhotoPage({this.index = 0, this.name = '', this.urlList, this.fileList, this.fit = BoxFit.cover, this.onPageChanged});

  @override
  Widget build(BuildContext context) {
    return lScaffold(
      body: Stack(
        children: [
          FullPhotoView(index: index, urlList: urlList, fileList: fileList, fit: fit),
          Padding(
            padding: EdgeInsets.fromLTRB(20, kToolbarHeight, 20, 0),
            child: Align(
              alignment: Alignment.topLeft,
              child: IconButton(
                onPressed: () => Commons.pagePop(context),
                icon: Lcons.clear(size: 50),
                alignment: Alignment.topLeft,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class FullPhotoView extends StatefulWidget {
  final int index;
  final List<String>? urlList;
  final List<File>? fileList;
  final BoxFit? fit;
  final Function? onPageChanged;

  FullPhotoView({this.index = 0, this.urlList, this.fileList, this.fit = BoxFit.cover, this.onPageChanged});

  @override
  State createState() => FullPhotoViewState(index: index, urlList: urlList, fileList: fileList, fit: fit!);
}

class FullPhotoViewState extends State<FullPhotoView> {
  final int index;
  final List<String>? urlList;
  final List<File>? fileList;
  final BoxFit fit;
  final Function? onPageChanged;

  FullPhotoViewState({this.index = 0, this.urlList, this.fileList, this.fit = BoxFit.cover, this.onPageChanged});

  PageController _controller = PageController();

  @override
  void initState() {
    super.initState();
    _controller = PageController(initialPage: index);
  }

  @override
  Widget build(BuildContext context) {
    BoxDecoration backgroundDecoration = BoxDecoration(color: color_black);
    bool isImage = false;
    bool isUrl = false; //로컬 이미지 파일
    int length = 0;
    if (urlList != null && urlList!.length > 0) {
      length = urlList!.length;
      isImage = true;
      isUrl = true;
    } else if (fileList != null && fileList!.length > 0) {
      length = fileList!.length;
      isImage = true;
    }
    return isImage
        ? Stack(
            children: [
              Container(
                  color: color_black,
                  child: PhotoViewGallery.builder(
                    scrollPhysics: const BouncingScrollPhysics(),
                    builder: (BuildContext context, int index) {
                      return PhotoViewGalleryPageOptions(
                        imageProvider: isUrl
                            ? urlList![index].contains('assets')
                                ? Image.asset(urlList![index]).image
                                : Image.network(urlList![index]).image
                            : Image.file(fileList![index]).image,
                        initialScale: PhotoViewComputedScale.contained,
                        heroAttributes: PhotoViewHeroAttributes(tag: '${isUrl ? urlList![index] : fileList![index]}_$index'),
                      );
                    },
                    itemCount: length,
                    loadingBuilder: (context, event) => Center(
                      child: Container(
                        width: 30.0,
                        height: 30.0,
                        child: CircularProgressIndicator(
                          color: Commons.getColor(),
                          strokeWidth: 2,
                          value: event == null ? 0 : (event.cumulativeBytesLoaded / event.expectedTotalBytes!).toDouble(),
                        ),
                      ),
                    ),
                    backgroundDecoration: backgroundDecoration,
                    pageController: _controller,
                    onPageChanged: onPageChanged == null ? null : onPageChanged!(),
                  )),
              if (length > 1)
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: padding_20,
                    child: SmoothPageIndicator(
                        controller: _controller,
                        count: length,
                        effect: SlideEffect(
                          spacing: 6,
                          dotHeight: 7,
                          dotWidth: 7,
                          dotColor: color_eeeeee /*Colors.white.withAlpha(56)*/,
                          activeDotColor: Commons.getColor(),
                        )),
                  ),
                ),
            ],
          )
        : Center(
            child: errorWidget(),
          );
  }
}
