import 'package:flutter/material.dart';

class ChatBubble extends StatelessWidget {
  final CustomClipper<Path>? clipper;
  final Widget? child;
  final EdgeInsets? margin;
  final double? elevation;
  final Color? backGroundColor;
  final Color? shadowColor;
  final Alignment? alignment;

  ChatBubble({
    this.clipper,
    this.child,
    this.margin,
    this.elevation,
    this.backGroundColor,
    this.shadowColor,
    this.alignment,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: alignment ?? Alignment.topLeft,
      margin: margin ?? EdgeInsets.all(0),
      child: PhysicalShape(
        clipper: clipper!,
        elevation: elevation ?? 1,
        color: backGroundColor ?? Colors.blue,
        shadowColor: shadowColor ?? Colors.grey.shade200,
        child: Padding(
          padding: setPadding(),
          child: child ?? Container(),
        ),
      ),
    );
  }

  EdgeInsets setPadding() {
    return EdgeInsets.only(top: 10, bottom: 10, left: 12, right: 12);
    // if ((clipper as ChatBubbleView).type == ChatType.send) {
    //   return EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10);
    // } else {
    //   return EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 10);
    // }

    ///디자인 적용으로 기존거 주석처리...이 디자인으로 할 경우 주석 제거 후 사용
    // if ((clipper as ChatBubbleView).type == ChatType.send) {
    //   return EdgeInsets.only(top: 8, bottom: 8, left: 10, right: 20);
    // } else {
    //   return EdgeInsets.only(top: 8, bottom: 8, left: 20, right: 10);
    // }
  }
}
