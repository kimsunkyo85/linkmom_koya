import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/data/chat_room_data.dart';
import 'package:linkmom/data/network/models/data/chat_send_data.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/main/matchings/matching_state_view.dart';

import '../../../../main.dart';
import '../chat_room_page.dart';

///매칭진행상황
Widget matchingStatusView(BuildContext context, ChatRoomData roomData, ChatSendData data, bool isLast, int id, int matchingStatus, {Function? callBack}) {
  if (data.msgdata is ContractData) {
    ContractData item = data.msgdata as ContractData;
    Color highLightColor = Commons.getColor();
    String deadline = item.deadline;
    int matchingType = MatchingStateView.MATCHING_STATE;

    //2022/05/17 테스트용 볼봄신청서 수정시
    // log.d('『GGUMBI』>>> matchingStatusView : item: ${item.cancel_id},,, :$item,  <<< ');
    // if(item.rematching){
    //   item.cancel_id = 0;
    //   item.matching_status = MatchingStatus.inprogress.value;
    // }

    if (item.matching_status == MatchingStatus.failNoRespL.value || item.matching_status == MatchingStatus.cancelPaid.value || item.care_status > CareStatus.inprogress.value) {
      highLightColor = color_ff3b30;
    }
    var contentData = _getMatchingTitle(item);

    Color contentColor = color_545454;

    //돌봄취소가 일어나면 다르게 보여준다.
    // if (item.cancel_id != 0) {
    //   matchingType = MatchingStateView.CARE_STATE;
    // }

    //거절,취소시 보여주지 않는다.
    return Stack(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: widthFull(context),
              padding: padding_15,
              margin: EdgeInsets.only(bottom: 4.0),
              decoration: decorationChatSystem(color: Commons.getColor()),
              child: Column(
                children: [
                  //매칭 내용.
                  Padding(
                    padding: padding_05_T,
                    child: TextHighlight(
                      text: contentData[0],
                      term: contentData[1],
                      textStyleHighlight: st_16(textColor: highLightColor, fontWeight: FontWeight.bold),
                      textStyle: st_b_16(fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),

                  ///매칭 상태, 부분 취소 또는 취소일 경우는 보여주지 않는다. 2021/10/18
                  if (item.cancel_id == null || item.cancel_id == 0)
                    // if (!item.is_cancelexplain! && item.cancel_id != 0)
                    Column(
                      children: [
                        Wrap(
                          children: [
                            MatchingStateView(
                              state: item.matching_status,
                              type: matchingType,
                              time: deadline,
                              viewName: '$ChatRoomPage',

                              // onClick: onClicks,
                            ),
                          ],
                        ),
                      ],
                    ),

                  //매칭내용 안내
                  if (StringUtils.validateString(contentData[2]))
                    Column(
                      children: [
                        sb_h_10,
                        lText(
                          contentData[2],
                          style: st_b_14(textColor: contentColor),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),

                  //시간데이터
                  if (StringUtils.validateString(contentData[3]))
                    Padding(
                      padding: padding_10_T,
                      child: lText(
                        contentData[3],
                        style: st_b_18(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),

                  chatMatchingView(context, id, data, matchingStatus, callBack: callBack!),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: isLast ? 15.0 : 8.0, left: 5.0),
              child: lText(
                StringUtils.getStringToDate(data.sendtime.toString(), format: F_HMM),
                style: st_12(textColor: color_b2b2b2),
              ),
            ),
          ],
        ),

        ///읽음, 안읽음 처리
        if (!data.is_receive)
          Padding(
            padding: EdgeInsets.only(left: 8.0, top: 8.0),
            child: Icon(
              Icons.circle,
              size: 5,
              color: color_ff4c1b,
            ),
          ),
      ],
    );
  } else {
    return Center();
  }
}

///매칭 관련 화면 처리
///(0, '매칭중(대화중)'), (11, '매칭실패(링크쌤 응답없음)'), (12, '매칭실패(맘대디 응답없음)'), (13, '매칭실패(링크쌤 거절)'), (14, '매칭실패(맘대디 거절)'), (21, '매칭취소(링크쌤 계약취소)'), (22, '매칭취소(맘대디 계약취소)'), (23, '매칭취소(결제시간만료)'), (91, '매칭대기(링크쌤 계약대기)'), (92, '매칭대기(맘대디 계약대기)'), (99, '매칭성공(계약완료/결제대기중)') (100, '결제완료') (0, '없음(매칭상태 확인)'), (1, '돌봄예정'), (2, '돌봄 중'), (3, '돌봄종료(정상종료)'), (4, '돌봄취소(부분종료&일부취소)'), (5, '돌봄취소(전체취소)') (0, '계약중'), (11, '계약실패(거절/취소/응답없음/시간만료 등)'), (21, '계약완료 후 취소(부분)'), (22, '계약완료 후 취소(전체)'), (99, '매칭성공(계약완료/결제대기중)'), (100, '결제완료')
///- inprogress, // 매칭중(대화중)
///- failNoRespL, // 매칭실패 (링크쌤 응답없음)
///- failNoRespM, // 매칭실패 (맘대디 응답없음)
///- failDisagreeL, // 매칭실패 (링크쌤 거절)
///- failDisagreeM, // 매칭실패 (맘대디 거절)
///- cancelLinkmom, // 매칭취소 (링크쌤계약취소)
///- cancelMomdaddy, // 매칭취소 (맘대디 계약취소)
///- canelPaid,
///- waitMomdaddy, // 계약대기
///- waitLinkmom, // 계약대기 (linkmom)
///- waitPaid, // 결제완료, 결제 대기
///- paid // 결제완료
Widget chatMatchingView(BuildContext context, int id, ChatSendData data, int matchingStatus, {Function? callBack}) {
  if (data.msgdata is ContractData) {
    ContractData item = data.msgdata as ContractData;
    Color color = Commons.getColor();

    bool isMyReceiver = data.sender == id;

    ///현재 매칭 상태
    bool isCareConfirm = isConfirm(matchingStatus); /* == MatchingStatus.failDisagreeL.value || matchingStatus == MatchingStatus.failDisagreeM.value;*/
    // log.d('『GGUMBI』>>> chatMatchingView : isContract: $isContract, isCareConfirm: $isCareConfirm, isMyReceiver:$isMyReceiver, crrentmatchingStatus : $matchingStatus, item-matching_status: ${item.matching_status}, data : ${data.msg}, time: ${data.sendtime}<<< ');
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          //(0(rematching), '돌봄신청서 수정 매칭중(매칭중 상태로)')
          if (item.matching_status == MatchingStatus.inprogress.value && item.rematching)
            cancelModifyBtn(
              context,
              color,
              item.matching_status,
              callBack: callBack!,
            ),
          //응답없음 - (11, 12 '매칭실패(링크쌤 응답없음)')
          if (item.matching_status == MatchingStatus.failNoRespL.value || item.matching_status == MatchingStatus.failNoRespM.value)
            findView(
              context,
              color,
              item.matching_status,
              callBack: callBack!,
            ),
          //거절 - (13, 14 '매칭실패(링크쌤 거절)')
          if (item.matching_status == MatchingStatus.failDisagreeM.value || item.matching_status == MatchingStatus.failDisagreeL.value)
            findView(
              context,
              color,
              item.matching_status,
              callBack: callBack!,
            ),
          //계약취소 - (21, 22 '매칭취소(링크쌤 계약취소)')
          if (item.matching_status == MatchingStatus.cancelLinkmom.value || item.matching_status == MatchingStatus.cancelMomdaddy.value)
            findView(
              context,
              color,
              item.matching_status,
              callBack: callBack!,
            ),
          //(23, '매칭취소(결제시간만료)')
          if (item.matching_status == MatchingStatus.cancelPaid.value)
            findView(
              context,
              color,
              item.matching_status,
              callBack: callBack!,
            ),
          //(91, 92, '매칭대기(맘대디 계약대기)')
          if (item.matching_status == MatchingStatus.waitMomdaddy.value || item.matching_status == MatchingStatus.waitLinkmom.value)
            Padding(
              padding: padding_10_T,
              child: Column(
                children: [
                  statusView(context, matchingStatus, item, isCareConfirm, isMyReceiver, color, callBack),
                ],
              ),
            ),
          //(99, '매칭성공(계약완료/결제대기중)')
          if (item.matching_status == MatchingStatus.waitPaid.value) contractInfoView(context, color, item, matchingStatus, isCareConfirm, isMyReceiver, callBack: callBack!),
          //(100, '결제완료')
          if (item.matching_status == MatchingStatus.paid.value) cancelView(context, color, item, callBack: callBack!),
        ],
      ),
    );
  } else {
    return Center();
  }
}

// item.matching_status == MatchingStatus.waitMomdaddy.value && !isCareConfirm || item.matching_status == MatchingStatus.waitLinkmom.value && !isCareConfirm
//     ? confirmView(context, color, callBack: callBack)

Widget statusView(BuildContext context, int matchingStatus, ContractData item, bool isCareConfirm, bool isMyReceiver, Color color, callBack) {
  ///돌봄계약서 확인 (내가 보낸 메시지가 아니고, 대화 매칭 상태에 따른 표시)
  log.d('『GGUMBI』>>> statusView : matchingStatus: $matchingStatus, ${item.matching_status}, $isMyReceiver, $isCareConfirm <<< ');
  if (item.matching_status == MatchingStatus.waitMomdaddy.value && !isCareConfirm || item.matching_status == MatchingStatus.waitLinkmom.value && !isCareConfirm) {
    log.d('『GGUMBI』>>> statusView : matchingStatus 1 : $matchingStatus, ${item.matching_status}, $isMyReceiver, $isCareConfirm <<< ');
    if (isMyReceiver) {
      return contractInfoView(context, color, item, matchingStatus, isCareConfirm, isMyReceiver, callBack: callBack);
    } else {
      log.d('『GGUMBI』>>> statusView : matchingStatus 2 : $matchingStatus, ${item.matching_status}, $isMyReceiver, $isCareConfirm <<< ');
      if (matchingStatus >= MatchingStatus.waitPaid.value) {
        return statusText("매칭요청수락_메시지".tr());
      } else {
        return confirmView(context, color, matchingStatus, callBack: callBack);
      }
    }

    ///거절,수락하기 버튼
  } else if (matchingStatus == MatchingStatus.waitMomdaddy.value && item.matching_status == MatchingStatus.waitMomdaddy.value && !isCareConfirm || matchingStatus == MatchingStatus.waitLinkmom.value && item.matching_status == MatchingStatus.waitLinkmom.value && !isCareConfirm) {
    return confirmView(context, color, matchingStatus, callBack: callBack);

    ///거절했을 경우
  } else if (matchingStatus == MatchingStatus.failDisagreeL.value || matchingStatus == MatchingStatus.failDisagreeM.value) {
    return statusText("매칭요청거절_메시지".tr());

    ///응답이 없을 경우((23, '매칭취소(결제시간만료)')
  } else if (matchingStatus == MatchingStatus.cancelPaid.value) {
    return statusText("매칭응답_메시지".tr());

    ///수락했을 경우
  } else if (matchingStatus == MatchingStatus.waitPaid.value || matchingStatus == MatchingStatus.paid.value) {
    return statusText("매칭요청수락_메시지".tr());

    ///매칭 취소 경우
  } else if (matchingStatus == MatchingStatus.cancelLinkmom.value || matchingStatus == MatchingStatus.cancelMomdaddy.value || matchingStatus == MatchingStatus.cancelPaid.value) {
    return statusText("매칭이취소_메시지".tr());
    // return Center();
  }
  return Center();
}

Widget statusText(String msg) {
  //매칭 상태에 따라 기존 메시지들도 바뀌기 때문에 상태 값을 보여 주지 않도록 처리 2022/01/13
  return Center();
  // return lText(
  //   msg,
  //   // style: st_b_13(textColor: color_545454),
  //   style: st_b_15(textColor: Commons.getColor(), fontWeight: FontWeight.w500),
  //   textAlign: TextAlign.center,
  // );
}

///돌봄 계약서 확인
Widget contractInfoView(BuildContext context, Color color, ContractData item, int matchingStatus, bool isCareConfirm, bool isMyReceiver, {Function? callBack}) {
  bool isPay = !Commons.isLinkMom();
  DialogAction action = isPay && item.matching_status == MatchingStatus.waitPaid.value ? DialogAction.pay : DialogAction.contract;
  // log.d('『GGUMBI』>>> contractInfoView : action: $action, ${item.matching_status}, $matchingStatus  <<< ');
  return InkWell(
    onTap: () => callBack!(action, item.matching_status),
    child: isPay && matchingStatus == MatchingStatus.waitPaid.value
        ? Container(
            width: widthFull(context),
            padding: padding_10_T,
            child: lTextBtn("결제하기".tr(),
                radius: 8,
                padding: padding_15_TB,
                bg: color.withAlpha(41),
                style: st_b_15(
                  fontWeight: FontWeight.w500,
                  textColor: color,
                )),
          )
        : matchingStatus >= MatchingStatus.waitMomdaddy.value && matchingStatus <= MatchingStatus.waitPaid.value
            ? Container(
                width: widthFull(context),
                padding: padding_10_T,
                child: lTextBtn("돌봄계약서확인".tr(),
                    radius: 8,
                    padding: padding_15_TB,
                    bg: color.withAlpha(41),
                    style: st_b_15(
                      fontWeight: FontWeight.w500,
                      textColor: color,
                    )),
              )
            : Center(),
  );
}

///매칭취소, 거절, 기간 만료
Widget findView(BuildContext context, Color color, int matchingStatus, {Function? callBack}) {
  return InkWell(
    onTap: () => callBack!(DialogAction.find, matchingStatus),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        sb_h_05,
        Container(
          width: widthFull(context),
          padding: padding_10_T,
          child: lTextBtn(storageHelper.user_type == USER_TYPE.mom_daddy ? "링크쌤찾기".tr() : "맘대디찾기".tr(),
              radius: 8,
              padding: padding_15_TB,
              bg: color.withAlpha(41),
              style: st_b_15(
                fontWeight: FontWeight.w500,
                textColor: color,
              )),
        ),
      ],
    ),
  );
}

///매칭 거절/수락하기
Widget confirmView(BuildContext context, Color color, int matchingStatus, {Function? callBack}) {
  return Row(
    children: [
      Expanded(
          child: InkWell(
        onTap: () => callBack!(DialogAction.no, matchingStatus),
        child: lTextBtn("거절하기".tr(),
            radius: 8,
            padding: padding_15_TB,
            bg: color_eeeeee,
            style: st_b_15(
              textColor: color_545454,
            )),
      )),
      sb_w_10,
      Expanded(
          child: InkWell(
        onTap: () => callBack!(DialogAction.yes, matchingStatus),
        child: lTextBtn("수락하기".tr(),
            radius: 8,
            padding: padding_15_TB,
            bg: color.withAlpha(41),
            style: st_b_15(
              fontWeight: FontWeight.w500,
              textColor: color,
            )),
      )),
    ],
  );
}

///결제가 완료된 상태이면 빈화면, 결제 완료된 상태에서 취소시 돌봄 상태값을 보고 판단
Widget cancelView(BuildContext context, Color color, ContractData item, {Function? callBack}) {
  // log.d('『GGUMBI』>>> cancelView : item.care_status: ${item.matching_status}, ${item.care_status},  <<< ');
  if (item.matching_status == MatchingStatus.paid.value && item.cancel_id == null || item.cancel_id == 0) {
    return Padding(
      padding: padding_10_T,
      child: Row(
        children: [
          Expanded(
              child: InkWell(
            onTap: () => callBack!(DialogAction.contract, item.matching_status),
            child: lTextBtn("돌봄계약서확인".tr(),
                radius: 8,
                padding: padding_15_TB,
                bg: color_eeeeee,
                style: st_b_15(
                  textColor: color_545454,
                )),
          )),
          sb_w_10,
          Expanded(
              child: InkWell(
            onTap: () => callBack!(DialogAction.schedule, item.matching_status),
            child: lTextBtn(Commons.isLinkMom() ? "근무관리".tr() : "돌봄관리".tr(),
                radius: 8,
                padding: padding_15_TB,
                bg: color.withAlpha(41),
                style: st_b_15(
                  fontWeight: FontWeight.w500,
                  textColor: color,
                )),
          )),
        ],
      ),
    );
  }
  return cancelBtnView(
    context,
    color,
    item.matching_status,
    item.is_cancelexplain!,
    callBack: callBack!,
  );
}

///돌봄취소 취소내역확인하기, 환불규정보기
Widget cancelBtnView(BuildContext context, Color color, int matchingStatus, bool isCancelexplain, {Function? callBack}) {
  return Column(
    children: [
      sb_h_10,
      InkWell(
        onTap: () => callBack!(DialogAction.cancel, matchingStatus),
        child: Container(
          width: widthFull(context),
          padding: padding_10_T,
          child: lTextBtn("취소내역확인하기".tr(),
              radius: 8,
              padding: padding_15_TB,
              bg: color.withAlpha(41),
              style: st_b_15(
                fontWeight: FontWeight.w500,
                textColor: color,
              )),
        ),
      ),
      //소명 접수시 표시 하지 않는다.
      if (!isCancelexplain)
        Column(
          children: [
            sb_h_10,
            InkWell(
              onTap: () => callBack!(DialogAction.cancelInfo, matchingStatus),
              child: Container(
                width: widthFull(context),
                child: lTextBtn("환불규정확인하기".tr(),
                    radius: 8,
                    padding: padding_15_TB,
                    bg: color.withAlpha(41),
                    style: st_b_15(
                      fontWeight: FontWeight.w500,
                      textColor: color,
                    )),
              ),
            ),
          ],
        ),
    ],
  );
}

///매칭 중 돌봄신청서 수정
Widget cancelModifyBtn(BuildContext context, Color color, int matchingStatus, {Function? callBack}) {
  return InkWell(
    onTap: () => callBack!(DialogAction.modify, matchingStatus),
    child: Column(
      children: [
        sb_h_05,
        Container(
          width: widthFull(context),
          padding: padding_10_T,
          child: lTextBtn(Commons.isLinkMom() ? "계약서확인후서명하기".tr() : "다시매칭요청하기".tr(),
              radius: 8,
              padding: padding_15_TB,
              bg: color.withAlpha(41),
              style: st_b_15(
                fontWeight: FontWeight.w500,
                textColor: color,
              )),
        ),
      ],
    ),
  );
}

///메시지 내용 0:메시지, 1:메시지(하이라이트), 2:시간, 3:'',
List<String> _getMatchingTitle(ContractData data) {
  MatchingStatus status = EnumUtils.getMatchingStatus(index: data.matching_status);
  // log.d('『GGUMBI』>>> _getMatchingTitle : matchingStatus: $status,  <<< ');
  String momdady = data.momdady_first_name;
  String linkmom = data.linkmom_first_name;
  bool isLinkmom = Commons.isLinkMom();
  String value = isLinkmom ? momdady : linkmom;
  // log.d('『GGUMBI』>>> _getMatchingTitle : momdady: $momdady, linkmom: $linkmom, <<< ');
  List<String> values = ['', '', '', ''];
  switch (status) {
    case MatchingStatus.inprogress:
      if (data.rematching) {
        return isLinkmom
            ? [
                '${"매칭중".tr()} $momdady ${"맘대디".tr()}${"가".tr()}\n${"돌봄신청서_수정_메시지_링크쌤".tr()}',
                "돌봄신청서_수정_메시지_링크쌤_힌트".tr(),
                "돌봄신청서_수정_메시지_링크쌤_안내".tr(),
                '',
              ]
            : [
                '${"매칭중".tr()} ${"돌봄신청서_수정_메시지_맘대디".tr()}',
                "돌봄신청서_수정_메시지_맘대디_힌트".tr(),
                '',
                '',
              ];
      } else {
        return values;
      }
    case MatchingStatus.failNoRespL:
    case MatchingStatus.failNoRespM:
      return isLinkmom
          ? [
              '$momdady ${"맘대디".tr()}${"가".tr()}\n${"매칭응답_메시지".tr()}',
              "매칭응답_메시지".tr(),
              "매칭이되지않았네요_링크쌤".tr(),
              '',
            ]
          : [
              '$linkmom ${"링크쌤".tr()}${"이".tr()}\n${"매칭응답_메시지".tr()}',
              "매칭응답_메시지".tr(),
              "매칭이되지않았네요_맘대디".tr(),
              '',
            ];
    case MatchingStatus.failDisagreeL:
    case MatchingStatus.failDisagreeM:
      return isLinkmom
          ? [
              '$momdady ${"맘대디".tr()}${"의".tr()}\n${"매칭요청거절_메시지".tr()}',
              "매칭요청에거절".tr(),
              "매칭이되지않았네요_링크쌤".tr(),
              '',
            ]
          : [
              '$linkmom ${"링크쌤".tr()}${"이".tr()}\n${"매칭요청거절_메시지".tr()}',
              "매칭요청에거절".tr(),
              "매칭이되지않았네요_맘대디".tr(),
              '',
            ];
    case MatchingStatus.cancelLinkmom:
    case MatchingStatus.cancelMomdaddy:
      return isLinkmom
          ? [
              '$momdady ${"맘대디".tr()}${"와".tr()}\n${"돌봄일정취소_메시지".tr()}',
              "취소".tr(),
              '' /*status == MatchingStatus.cancelLinkmom ? "돌봄일정취소_취소시점".tr() : "돌봄일정취소_취소사유".tr()*/,
              '',
            ]
          : [
              '$linkmom ${"링크쌤".tr()}${"과".tr()}\n${"돌봄일정취소_메시지".tr()}',
              "취소".tr(),
              '' /*status == MatchingStatus.cancelLinkmom ? "돌봄일정취소_취소사유".tr() : "돌봄일정취소_취소시점".tr()*/,
              '',
            ];
    case MatchingStatus.cancelPaid:
      return isLinkmom
          ? [
              '$momdady ${"맘대디".tr()}${"가".tr()}\n${"매칭이취소_메시지".tr()}',
              "매칭이취소".tr(),
              "매칭이취소되었네요_링크쌤".tr(),
              '',
            ]
          : [
              '$linkmom ${"링크쌤".tr()}${"이".tr()}\n${"매칭이취소_메시지".tr()}',
              "매칭이취소".tr(),
              "매칭이취소되었네요_맘대디".tr(),
              '',
            ];
    case MatchingStatus.waitMomdaddy:
      // String time = "(${"서명시한".tr()} ${StringUtils.payDeadline(data.deadline, showMin: true)} ${'까지'.tr()})";
      String time = "${StringUtils.payDeadline(data.deadline, showMin: true)}${"까지".tr()} ${"서명".tr()}";
      return isLinkmom
          ? [
              '$momdady ${"맘대디".tr()}${"가".tr()}\n${"매칭요청_메시지".tr()}',
              "매칭을요청".tr(),
              "나의서명을기다리고있어요".tr(),
              time,
            ]
          : [
              '$linkmom ${"링크쌤".tr()}${"에게".tr()}\n${"매칭요청_메시지".tr()}',
              "매칭을요청".tr(),
              "상대방의서명을기다리고있어요".tr(),
              time,
            ];
    case MatchingStatus.waitLinkmom:
      // String time = "(${"서명시한".tr()} ${StringUtils.payDeadline(data.deadline, showMin: true)} ${'까지'.tr()})";
      String time = "${StringUtils.payDeadline(data.deadline, showMin: true)}${"까지".tr()} ${"서명".tr()}";
      return isLinkmom
          ? [
              '$momdady ${"맘대디".tr()}${"에게".tr()}\n${"매칭요청_메시지".tr()}',
              "매칭을요청".tr(),
              "상대방의서명을기다리고있어요".tr(),
              time,
            ]
          : [
              '$linkmom ${"링크쌤".tr()}${"이".tr()}\n${"매칭요청_메시지".tr()}',
              "매칭을요청".tr(),
              "나의서명을기다리고있어요".tr(),
              time,
            ];
    case MatchingStatus.waitPaid:
      // String time = "(${"결제시한".tr()} ${StringUtils.payDeadline(data.deadline, showMin: true)} ${'까지'.tr()})";
      String time = "${StringUtils.payDeadline(data.deadline, showMin: true)}${"까지".tr()} ${"결제".tr()}";
      return isLinkmom
          ? [
              '$momdady ${"맘대디".tr()}${"와".tr()}\n${"매칭완료_메시지".tr()}',
              "매칭이완료".tr(),
              "돌봄이확정_맘대디_안내".tr(),
              time,
            ]
          : [
              '$linkmom ${"링크쌤".tr()}${"과".tr()}\n${"매칭완료_메시지".tr()}',
              "매칭이완료".tr(),
              "돌봄이확정_맘대디_안내".tr(),
              time,
            ];
    case MatchingStatus.paid:
      // if (data.care_status <= CareStatus.ready.value) {
      if (data.cancel_id == null || data.cancel_id == 0) {
        return isLinkmom
            ? [
                // '$momdady ${"맘대디".tr()}${"와".tr()}\n${"결제완료_메시지".tr()}',
                "결제완료_메시지".tr(),
                "결제완료!".tr(),
                "결제완료_메시지_링크쌤_안내".tr(),
                '',
              ]
            : [
                // '$linkmom ${"링크쌤".tr()}${"과".tr()}\n${"결제완료_메시지".tr()}',
                "결제완료_메시지".tr(),
                "결제완료!".tr(),
                "결제완료_메시지_맘대디_안내".tr(),
                '',
              ];
      } else {
        String value = data.is_totalcancel! ? "취소".tr() : "부분취소".tr();
        String string = "되었습니다".tr();
        bool isCancelLinkMom = EnumUtils.getCancelSelectUserTypeBool(data.cancelreason);
        String explain = '';
        //링크쌤이고, 맘대디가 링크쌤 사유로 취소시 소명하기!, 맘대디이고, 링크쌤이 맘대디 사유로 취소시 소명하기!
        // log.d('『GGUMBI』>>> _getMatchingTitle : isLinkmom: $isLinkmom, ${data.cancel_usertype}, $isCancelLinkMom, ${StringUtils.payDeadline(data.deadline, showMin: true)} <<< ');
        CancelUserType cancelUserType = EnumUtils.getCancelUserType(data.cancel_usertype!);
        if (Commons.isExplain(cancelUserType, isCancelLinkMom)) {
          explain = StringUtils.validateString(data.deadline)
              ? Commons.isExpired(DateTime.parse(data.deadline))
                  ? '${StringUtils.payDeadline(data.deadline, showMin: true)}${"까지".tr()}\n${"소명시간이경과하였습니다".tr()}'
                  : '${"돌봄일정취소_사유_안내".tr()}\n${StringUtils.payDeadline(data.deadline, showMin: true)} ${"소명을진행해주세요".tr()}'
              : '';
        }

        String content = '';
        String contentHighlight = isCancelLinkMom ? '${"돌봄일정취소_메시지_링크쌤측".tr()} $value' : '${"돌봄일정취소_메시지_맘대디측".tr()} $value';
        if (data.is_cancelexplain == null) {
          data.is_cancelexplain = false;
        }
        if (isLinkmom) {
          //취소 소명하기가 접수된 경우
          if (data.is_cancelexplain!) {
            // content = '$momdady ${"맘대디".tr()}${"의".tr()} ${"소명접수".tr()}';
            content = cancelUserType.isLinkMom ? '$momdady ${"맘대디".tr()}${"의".tr()} ${"소명접수".tr()}\n${"소명내용_안내".tr()}' : '$linkmom ${"링크쌤".tr()}${"의".tr()} ${"소명접수".tr()}';
            explain = '';
          } else {
            content = '$momdady ${"맘대디".tr()}${"와".tr()} ${isCancelLinkMom ? '${"돌봄일정취소_메시지_링크쌤".tr()} $value$string' : '${"돌봄일정취소_메시지_맘대디".tr()} $value$string'}';
          }
          contentHighlight = data.is_cancelexplain!
              ? ''
              : isCancelLinkMom
                  ? '${"돌봄일정취소_메시지_링크쌤측".tr()} $value'
                  : '${"돌봄일정취소_메시지_맘대디측".tr()} $value';
        } else {
          if (data.is_cancelexplain!) {
            // content = '$linkmom ${"링크쌤".tr()}${"의".tr()} ${"소명접수".tr()}';
            content = cancelUserType.isLinkMom ? '$momdady ${"맘대디".tr()}${"의".tr()} ${"소명접수".tr()}' : '$linkmom ${"링크쌤".tr()}${"의".tr()} ${"소명접수".tr()}\n${"소명내용_안내".tr()}';
            explain = '';
          } else {
            content = '$linkmom ${"링크쌤".tr()}${"과".tr()} ${isCancelLinkMom ? '${"돌봄일정취소_메시지_링크쌤".tr()} $value$string' : '${"돌봄일정취소_메시지_맘대디".tr()} $value$string'}';
          }
          contentHighlight = data.is_cancelexplain!
              ? ''
              : isCancelLinkMom
                  ? '${"돌봄일정취소_메시지_링크쌤측".tr()} $value'
                  : '${"돌봄일정취소_메시지_맘대디측".tr()} $value';
        }

        return isLinkmom
            ? [
                content,
                contentHighlight,
                explain,
                '',
              ]
            : [
                content,
                contentHighlight,
                explain,
                '',
              ];
      }

    default:
      return values;
  }
}

bool isConfirm(int matchingStatus) {
  bool isConfirm = false;
  //응답 없음
  if (matchingStatus == MatchingStatus.failNoRespL.value || matchingStatus == MatchingStatus.failNoRespM.value) {
    isConfirm = true;
  }
  //거절
  if (matchingStatus == MatchingStatus.failDisagreeL.value || matchingStatus == MatchingStatus.failDisagreeM.value) {
    isConfirm = true;
  }
  //매칭취소
  if (matchingStatus == MatchingStatus.cancelLinkmom.value || matchingStatus == MatchingStatus.cancelMomdaddy.value) {
    isConfirm = true;
  }
  //매칭취소
  if (matchingStatus == MatchingStatus.cancelPaid.value) {
    isConfirm = true;
  }
  //계약성공(결제대기) 또는 결제 후
  if (matchingStatus >= MatchingStatus.waitPaid.value) {
    isConfirm = true;
  }
  return isConfirm;
}
