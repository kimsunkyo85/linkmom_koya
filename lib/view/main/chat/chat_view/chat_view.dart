import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/data/chat_send_data.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../../../../main.dart';

///매너있게 대화해 주세요.
Widget setMsgNotify() {
  return Container(
      margin: padding_10,
      padding: padding_20,
      decoration: BoxDecoration(color: color_f8859a.withAlpha(31), borderRadius: BorderRadius.circular(16)),
      child: Column(
        children: [
          lText("매너있게대화해주세요".tr(), style: st_16(textColor: color_f8859a, fontWeight: FontWeight.w500)),
          sb_h_15,
          lText("매너있게대화해주세요_안내".tr(), style: st_14(textColor: color_f8859a)),
        ],
      ));
}

///날짜 표시
Widget timeLineView(int index, List<ChatSendData> lsChat) {
  String date = '';
  try {
    date = lsChat[index].sendtime.toString();
  } catch (e) {
    log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
  }
  return Row(
    children: [
      Flexible(child: lDivider()),
      lRoundContainer(
        margin: padding_10,
        padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
        bgColor: color_transparent,
        // bgColor: color_80dbdbdb,
        borderColor: color_transparent,
        borderRadius: 50.0,
        child: lText(
          StringUtils.getStringToDate(date, format: F_YYYYMMDDE),
          style: st_b_14(textColor: color_b2b2b2),
        ),
      ),
      Flexible(child: lDivider()),
    ],
  );
}

/// ・ 표시
Widget infoView(
  String content, {
  String content2 = '',
  String content3 = '',
  int bookingStatus = 0,
  Color color = color_222222,
  Function? callBack,
  WidgetSpan? widgetSpan,
}) {
  bool isModify = EnumUtils.isBookingStatus(bookingStatus);
  return Text.rich(
    TextSpan(
      text: '$content ',
      style: st_b_14(textColor: color),
      children: [
        if (widgetSpan != null) widgetSpan,
        if (isModify)
          TextSpan(
            text: ' $content2',
            style: st_b_14(
              textColor: isModify ? Commons.getColor() : color,
              decoration: callBack == null
                  ? null
                  : isModify
                      ? TextDecoration.underline
                      : null,
            ),
            recognizer: TapGestureRecognizer()..onTap = callBack == null ? null : () => isModify ? callBack() : null,
          ),
        TextSpan(
          text: '$content3',
          style: st_b_14(
            textColor: color,
          ),
        ),
      ],
    ),
  );
}

///사진 업로드 화면
Widget uploadView(double progress) {
  return Align(
      alignment: Alignment.bottomCenter,
      child: LinearProgressIndicator(
        value: progress,
        color: Commons.getColor(),
        backgroundColor: color_eeeeee,
      ));
}

///마지막 메시지
bool isLastMessageSystem(int index, List<ChatSendData> lsChat) {
  int length = lsChat.length - 1;
  if (index == length) {
    return true;
  } else {
    return false;
  }
}

///메시지 체크 마지막
bool isLastMessage(int id, int index, List<ChatSendData> lsChat) {
  int length = lsChat.length - 1;
  if ((index < length && lsChat[index + 1].receiver == id) || index == length) {
    return true;
  } else {
    return false;
  }
}
