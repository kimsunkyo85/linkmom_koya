import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';

class ChatBubbleView extends CustomClipper<Path> {
  final ChatSendType type;
  final double radius;
  final double nipHeight;
  final double nipWidth;
  final double nipRadius;
  final bool isLast;
  final double viewWidth;

  ChatBubbleView({required this.type, this.radius = 10, this.nipHeight = 10, this.nipWidth = 10, this.nipRadius = 3, this.isLast = false, this.viewWidth = 10});

  @override
  Path getClip(Size size) {
    var path = Path();
    if (type == ChatSendType.send) {
      path.addRRect(RRect.fromLTRBAndCorners(
        0,
        0,
        size.width,
        size.height,
        topLeft: Radius.circular(radius),
        bottomLeft: Radius.circular(radius),
        bottomRight: Radius.circular(radius),
      ));
    } else if (type == ChatSendType.receiver) {
      path.addRRect(RRect.fromLTRBAndCorners(
        0,
        0,
        size.width,
        size.height,
        topRight: Radius.circular(radius),
        bottomLeft: Radius.circular(radius),
        bottomRight: Radius.circular(radius),
      ));
    } else if (type == ChatSendType.mode_momdady) {
      path.addRRect(RRect.fromLTRBAndCorners(
        0,
        0,
        size.width,
        size.height,
        topLeft: Radius.circular(radius),
        topRight: Radius.circular(radius),
        bottomLeft: Radius.circular(radius),
        bottomRight: Radius.circular(radius),
      ));
      final path2 = Path();
      path2.moveTo(viewWidth + (radius), 0);
      path2.lineTo(viewWidth, -radius);
      path2.lineTo(viewWidth - (radius), 0);
      path.addPath(path2, Offset.zero);
    } else if (type == ChatSendType.mode_linkmom) {
      path.addRRect(RRect.fromLTRBAndCorners(
        0,
        0,
        size.width,
        size.height,
        topLeft: Radius.circular(radius),
        topRight: Radius.circular(radius),
        bottomLeft: Radius.circular(radius),
        bottomRight: Radius.circular(radius),
      ));
      final path2 = Path();
      path2.moveTo(size.width - (viewWidth + radius), 0);
      path2.lineTo(size.width - (viewWidth), -radius);
      path2.lineTo(size.width - (viewWidth - radius), 0);
      path.addPath(path2, Offset.zero);
    }
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
