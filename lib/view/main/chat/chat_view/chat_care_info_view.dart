import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/momdady_mycares_list_response.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../chat_care_info_page.dart';
import '../chat_room_page.dart';

///돌봄신청서 요약 화면
Widget chatCareSummaryInfoView(List<String>? values, {Color textColor = color_222222}) {
  Widget valuesWidget = Container();
  if (values != null) {
    valuesWidget = Row(
        children: values
            .map((e) => Container(
                    child: Row(children: [
                  lAutoSizeText(StringUtils.validateString(e) ? e : '', style: st_b_14(textColor: textColor)),
                  if (values.last != e)
                    Container(
                      height: 10,
                      padding: padding_06_LR,
                      child: lDividerVertical(color: color_dedede, thickness: 1.5),
                    )
                ])))
            .toList());
  }
  return valuesWidget;
}

///채팅 신청서 공유하기 및 불러오기에서 사용
Widget chatCareInfoView(BuildContext context, MyCareResultsData item, {bool isSend = false, int id = 0, int sender = 0, String viewName = '', bool isModify = false, Function? callBack}) {
  return Stack(
    children: [
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              lInkWell(
                onTap: () => callBack!(DialogAction.select, item.booking_id),
                child: Row(
                  children: [
                    lTextBtn(
                      item.servicetype,
                      bg: EnumUtils.getServiceType(name: item.servicetype).color,
                      padding: EdgeInsets.only(top: 4, bottom: 4, left: 10, right: 10),
                      style: st_14(fontWeight: FontWeight.bold),
                    ),
                    sb_w_10,
                    Lcons.won(isEnabled: Commons.isLinkMom()),
                    sb_w_02,
                    lText(StringUtils.formatPay(item.cost_sum), style: st_16(textColor: Commons.getColor(), fontWeight: FontWeight.w700)),
                    sb_w_05,
                    Lcons.nav_right(size: 15, color: color_cecece),
                  ],
                ),
              ),
            ],
          ),
          sb_h_05,
          chatCareSummaryInfoView([item.childinfo!.child_name, item.childinfo!.child_age, item.childinfo!.child_gender]),
          sb_h_05,
          chatCareSummaryInfoView([
            StringUtils.getCareDateCount(item.carescheduleinfo!.caredate!),
            StringUtils.getCareTime([item.carescheduleinfo!.stime, item.carescheduleinfo!.etime])
          ]),
        ],
      ),
      Align(
        alignment: Alignment.topRight,
        child: InkWell(
          onTap: () {
            ///단일 선택시
            callBack!(DialogAction.toggle, item.booking_id);
          },
          child: Container(
            // padding: padding_10_L,
            padding: EdgeInsets.only(top: 0, bottom: 4, left: 10, right: 5),
            child: _topRight(viewName, item),
          ),
        ),
      ),
      if (isModify)
        Align(
          alignment: Alignment.bottomRight,
          heightFactor: 4,
          child: Container(
            width: Commons.width(context, 0.15),
            child: lIconText(
              align: MainAxisAlignment.end,
              padding: padding_10_L,
              isExpanded: false,
              onClick: () => callBack!(DialogAction.modify, item.booking_id),
              title: "수정".tr(),
              icon: Lcons.create(color: color_b2b2b2, isEnabled: true, size: 14),
              isLeft: false,
              textStyle: st_14(textColor: color_b2b2b2),
            ),
          ),
        ),
    ],
  );
}

Widget _topRight(String viewName, MyCareResultsData item) {
  late Widget valuesWidget;
  if (!StringUtils.validateString(viewName)) {
    valuesWidget = Center();
  } else if (viewName == '$ChatRoomPage') {
    valuesWidget = Lcons.error_outline(color: color_cecece, size: 25);
  } else if (viewName == '$ChatCareInfoPage') {
    valuesWidget = Lcons.check_circle(isEnabled: item.isSelected, color: Commons.getColor(), disableColor: color_dedede);
  }
  return valuesWidget;
}
