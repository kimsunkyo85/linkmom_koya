import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/chat_main_request.dart';
import 'package:linkmom/data/network/models/chat_room_delete_request.dart';
import 'package:linkmom/data/network/models/data/chat_main_result_data.dart';
import 'package:linkmom/data/network/models/data/chat_send_data.dart';
import 'package:linkmom/data/push/model/linkmompush.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/home_provider.dart';
import 'package:linkmom/utils/listview/list_chat_view.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:provider/provider.dart';

import '../../../main.dart';
import '../../../route_name.dart';
import '../../lcons.dart';
import 'chat_room_page.dart';

///현재 탭 index
int index = 0;

class ChatPage extends BaseStateful {
  USER_TYPE? userType;

  ChatPage({this.userType});

  @override
  ChatPageState createState() => ChatPageState(userType: userType);
}

class ChatPageState extends BaseStatefulState<ChatPage> with TickerProviderStateMixin {
  ChatPageState({this.userType});

  USER_TYPE? userType;

  int badgeCareChat = 0;
  int badge = 0;

  GlobalKey<ChatListPageState>? careKey = GlobalKey();
  GlobalKey<ChatListPageState>? normalKey = GlobalKey();
  List<Widget> tabPage = [];

  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    tabPage = [
      ChatListPage(key: careKey, type: ChatTabType.carematching, callBack: () => setBadgeClear(ChatTabType.carematching.index)),
      ChatListPage(key: normalKey, type: ChatTabType.normal, callBack: () => setBadgeClear(ChatTabType.normal.index)),
    ];
    _tabController = TabController(length: 2, vsync: this);
    _tabController.index = index;
  }

  updateChat(ChatTabType type) {
    if (type.value == ChatTabType.carematching.value) {
      if (careKey!.currentState != null) {
        careKey!.currentState!._onRefresh();
      }
    } else {
      if (normalKey!.currentState != null) {
        normalKey!.currentState!._onRefresh();
      }
    }
  }

  setBadgeClear(int index) {
    if (index == ChatTabType.carematching.index) {
      badgeCareChat = 0;
    } else {
      badge = 0;
    }
    onUpDate();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<bool> onBackPress() {
    return Future.value(false);
  }

  @override
  Widget build(BuildContext context) {
    final _pushData = Provider.of<LinkmomPushData>(context, listen: true);
    var provider = Provider.of<HomeNavigationProvider>(context);
    if (provider.chatUpdated) {
      provider.updatedEvent(TAB_CHAT);
      updateChat(index == ChatTabType.carematching.value ? ChatTabType.carematching : ChatTabType.normal);
    }
    if (_pushData.pushData.chatSendData != null) {
      if (EnumUtils.getNotificationType(_pushData.pushData.chatSendData!.pagename).isChat) {
        if (pushData.pushData.chatSendData!.chattingtype == ChattingType.care.value) {
          if (!_pushData.pushData.chatSendData!.is_receive) {
            badgeCareChat++;
          }
        } else {
          if (!_pushData.pushData.chatSendData!.is_receive) {
            badge++;
          }
        }
        onUpDate();
      }
    }
    return Column(
      children: [
        // TODO: GGUMBI 2021/07/08 - 추후 광고 영역시 사용
        /*lContainer(
              height: 80,
              width: widthFull(context),
              color: color_0085ff,
              child: Center(child: lText('배너 영억')),
            ),*/
        Expanded(
          child: DefaultTabController(
            length: tabPage.length,
            child: lScaffold(
              appBar: PreferredSize(
                preferredSize: Size.fromHeight(kToolbarHeight),
                // TabBar 구현. 각 컨텐트를 호출할 탭들을 등록
                child: Container(
                  height: tabHeight,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.0),
                    border: Border(bottom: BorderSide(color: color_dedede)),
                  ),
                  child: TabBar(
                    indicatorColor: Commons.getColor(),
                    unselectedLabelColor: color_222222,
                    labelColor: color_222222 /*Commons.getColor()*/,
                    labelStyle: st_18(
                      fontWeight: FontWeight.w500,
                    ),
                    unselectedLabelStyle: st_18(
                      fontWeight: FontWeight.w500,
                    ),
                    controller: _tabController,
                    onTap: (_index) {
                      index = _index;
                      setBadgeClear(index);
                    },
                    tabs: [
                      badgeView(ChatTabType.carematching.title, badgeCareChat),
                      badgeView(ChatTabType.normal.title, badge),
                    ],
                  ),
                ),
              ),

              // TabVarView 구현. 각 탭에 해당하는 컨텐트 구성
              body: TabBarView(
                controller: _tabController,
                children: tabPage,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class ChatListPage extends BaseStateful {
  Key? key;
  final ChatTabType? type;
  Function? callBack;

  @override
  ChatListPage({this.key, this.type, this.callBack});

  ChatListPageState createState() => ChatListPageState(key: key, type: type, callBack: callBack);
}

class ChatListPageState extends BaseStatefulState<ChatListPage> with AutomaticKeepAliveClientMixin<ChatListPage> {
  Key? key;
  ChatTabType? type;
  Function? callBack;

  ChatListPageState({this.key, this.type, this.callBack});

  bool isAdd = true;

  late ScrollController _controller;
  bool _visible = false;
  ViewFlag flag = ViewFlag();
  DataManager data = DataManager();

  @override
  initState() {
    data.chat = ChatItem.init();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
    if (auth.user.username.isNotEmpty) _requestChatMain();
  }

  ///재조회
  Future<Null> _onRefresh() async {
    _visible = false;
    _requestChatMain();
  }

  ///스크롤 리스너
  _scrollListener() {
    if (_controller.offset <= _controller.position.minScrollExtent && !_controller.position.outOfRange) {
      if (_visible) {
        _visible = false;
        onUpDate();
      }
    } else {
      if (!_visible) {
        _visible = true;
        onUpDate();
      }
    }
  }

  ///상단으로 이동
  _moveUp() {
    _controller.animateTo(0, curve: Curves.linear, duration: Duration(milliseconds: 100));
    _visible = false;
    onUpDate();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: _listView(),
          ),
        ],
      ),
    );
  }

  _requestChatMain() {
    flag.enableLoading(fn: () => onUpDate());
    apiHelper.requestChatMain(ChatMainRequest(bywho: type!.bywho), ApiEndPoint.EP_CHAT_MAIN).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        data.chat.chatMainResponse = response;
        data.chat.lsChat = response.getData().results as List<ChatMainResultsData>;
      }
      flag.disableLoading(fn: () => onUpDate());
      callBack!();
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  Widget _listView() {
    final _pushData = Provider.of<LinkmomPushData>(context, listen: true);
    try {
      if (_pushData.pushData.chatSendData != null) {
        if (EnumUtils.getNotificationType(_pushData.pushData.chatSendData!.pagename).isChat) {
          if (pushData.pushData.chatSendData!.chattingtype == ChattingType.care.value) {
            setPushUpdate(_pushData, ChatTabType.carematching);
          } else {
            setPushUpdate(_pushData, ChatTabType.normal);
          }
        } else {
          _pushData.setDataClear();
        }
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        body: ListNextView(
          view: data.chat.lsChat
              .map((chat) => Container(
                    decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
                    child: ChatView(
                      chat,
                      index: data.chat.lsChat.indexOf(chat),
                      tabType: type,
                      onItemClick: (item, action) async {
                        if (item is ChatMainResultsData) {
                          if (action == DialogAction.select) {
                            item.is_receive = true;
                            callBack!();
                            onUpDate();
                            var result = await Commons.page(context, routeChatRoom,
                                arguments: ChatRoomPage(
                                  roomType: ChatRoomType.VIEW,
                                  receiver: item.talker_info!.id,
                                  chatroom_id: item.chatroom_id,
                                ));
                            if (result is ChatBackItem) {
                              if (result.isDelete) {
                                data.chat.lsChat.remove(item);
                                if (data.chat.lsChat.isEmpty) {
                                  callBack!();
                                }
                                onUpDate();
                              } else {
                                setCurrentUpdate(result.chatData!);
                              }
                            }
                          } else if (action == DialogAction.delete) {
                            showNormalDlg(
                                message: "채팅삭제_내용".tr(),
                                btnLeft: "취소".tr(),
                                btnRight: "나가기".tr(),
                                onClickAction: (action) {
                                  if (action == DialogAction.yes) {
                                    List<int> lsDelete = [];
                                    lsDelete.add(item.chatroom_id);
                                    apiHelper.requestChatRoomDelete(ChatRoomDeleteRequest(chatroom_id: lsDelete)).then((response) {
                                      if (response.getCode() == KEY_SUCCESS) {
                                        data.chat.lsChat.remove(item);
                                        if (data.chat.lsChat.isEmpty) {
                                          callBack!();
                                        }
                                        onUpDate();
                                        Commons.showToast("완료되었습니다.".tr());
                                      }
                                    });
                                  }
                                });
                          }
                        }
                      },
                    ),
                  ))
              .toList(),
          emptyView: _emptyView(type ?? ChatTabType.normal),
          showNextLoading: data.chat.chatMainResponse.getData().next.isNotEmpty,
          onRefresh: _onRefresh,
          onNext: () => _requestNext(data.chat.chatMainResponse.getData().next),
        ),
      ),
      isOnlyLoading: true,
    );
  }

  _requestNext(String url) {
    try {
      if (url.isEmpty) return;
      flag.enableEndLoading(fn: () => onUpDate());
      apiHelper.requestChatMain(ChatMainRequest(bywho: type!.bywho), url).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          data.chat.chatMainResponse = response;
          var list = response.getData().results as List<ChatMainResultsData>;
          data.chat.lsChat.addAll(list);
        }
        flag.disableEndLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableEndLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>>>> $e");
    }
  }

  ///push data 수신시 처리
  setPushUpdate(LinkmomPushData _pushData, ChatTabType tabType) {
    isAdd = true;
    data.chat.lsChat.forEach((value) {
      // log.d('『GGUMBI』>>> _listView : value.chatroom_id: $type, $tabType, ${value.chatroom_id}, ${_pushData.pushData.data?.chattingroom}, ${_pushData.pushData.data?.chattingtype}, ${_pushData.pushData.data!.is_receive}  <<< ');
      if (_pushData.pushData.chatSendData != null && value.chatroom_id == _pushData.pushData.chatSendData!.chattingroom) {
        value.msg = _pushData.pushData.chatSendData!.msg;
        value.image = _pushData.pushData.chatSendData!.image;
        value.sendtime = _pushData.pushData.chatSendData!.sendtime;
        value.sendtimestamp = _pushData.pushData.chatSendData!.sendtimestamp;
        value.is_receive = _pushData.pushData.chatSendData!.is_receive;
        isAdd = false;
      }
    });
    if (tabType == type) {
      //새로운 채팅이면 재조회를 한다.
      if (isAdd) {
        _requestChatMain();
        isAdd = false;
      } else {
        data.chat.lsChat.sort((a, b) => b.sendtimestamp.compareTo(a.sendtimestamp));
      }
      _pushData.setDataClear();
      onUpDate();
    }
  }

  ///채팅방 나올때 처리
  setCurrentUpdate(ChatSendData sendData) {
    if (index == type!.value && data.chat.lsChat.isNotEmpty) {
      data.chat.lsChat.forEach((value) {
        if (value.chatroom_id == sendData.chattingroom) {
          value.msg = sendData.msg;
          value.image = sendData.image;
          value.sendtime = sendData.sendtime;
          value.sendtimestamp = sendData.sendtimestamp;
          value.is_receive = true;
        }
      });
      data.chat.lsChat.sort((a, b) => b.sendtimestamp.compareTo(a.sendtimestamp));
      onUpDate();
    }
  }

  ///탭 유지 코드
  @override
  bool get wantKeepAlive => true;

  Widget _emptyView(ChatTabType type) {
    return type == ChatTabType.normal
        ? lEmptyView(
            height: Commons.height(context, 0.7),
            img: Lcons.no_data_chat().name,
            title: "채팅친구가없어요".tr(),
            content: "링크쌤커뮤니티에서".tr(),
            textBtn: "커뮤니티마실가기".tr(),
            paddingBtn: padding_50_LR,
            fn: () => Commons.isGpsWithAddressCheck(context, nextRoute: routeChat, subIndex: TAB_COMMUNITY),
          )
        : lEmptyView(
            height: Commons.height(context, 0.7),
            img: Lcons.no_data_chat().name,
            title: "채팅친구가없어요".tr(),
            content: Commons.isLinkMom() ? "당신과잘맞는맘대디".tr() : "당신과잘맞는링크쌤".tr(),
            textBtn: Commons.isLinkMom() ? "맘대디찾기".tr() : "링크쌤찾기".tr(),
            paddingBtn: padding_50_LR,
            fn: () => Commons.isGpsWithAddressCheck(context, nextRoute: routeChat, subIndex: TAB_LIST),
          );
  }
}

class ChatViewData {
  final ChatTabType type;
  final int badgeLinkmom;
  final int badgeMomdady;
  final int badge;

  ChatViewData({this.type = ChatTabType.normal, this.badgeLinkmom = 0, this.badgeMomdady = 0, this.badge = 0});

  @override
  String toString() {
    return 'ChatViewData{type: $type, badgeLinkmom: $badgeLinkmom, badgeMomdady: $badgeMomdady, badge: $badge}';
  }
}
