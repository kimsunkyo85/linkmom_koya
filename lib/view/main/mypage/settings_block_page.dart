import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/block_list_response.dart';
import 'package:linkmom/data/network/models/block_requests.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

class SettingsUserBlockPage extends BaseStateful {
  final DataManager? data;

  SettingsUserBlockPage({this.data});

  @override
  _SettingsUserBlockPageState createState() => _SettingsUserBlockPageState();
}

class _SettingsUserBlockPageState extends BaseStatefulState<SettingsUserBlockPage> {
  @override
  void initState() {
    super.initState();
    _requestBlockList();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          appBar: appBar("차단관리".tr(), hide: false),
          body: ListNextView(
            header: Container(
              decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
              padding: padding_20_TB,
              margin: padding_20_LR,
              child: lText("차단된계정".tr(), style: st_b_16(fontWeight: FontWeight.w700)),
            ),
            view: data.blockList.results!.map((e) => _userBlock(e.block_user_profileimg, e.block_user_name, e.block_user)).toList(),
            onRefresh: () => _requestBlockList(),
            onNext: () => _requestNextList(),
            showNextLoading: data.blockList.next.isNotEmpty,
          ),
        ));
  }

  Widget _userBlock(String uri, String name, int id) {
    return Container(
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
      padding: padding_20_TB,
      margin: padding_20_LR,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              lProfileAvatar(uri, radius: 30),
              sb_w_20,
              lText(name),
            ],
          ),
          lBtnWrapOutLine("차단해제".tr(), onClickAction: () => _requestDeleteBlockUser(id), sideEnableColor: color_cecece, padding: padding_18_LR, textStyle: st_12(fontWeight: FontWeight.w500, textColor: color_999999))
        ],
      ),
    );
  }

  void _requestDeleteBlockUser(int id) async {
    apiHelper.requestBlockDelete(BlockDeleteRequest(block_user: id)).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        showNormalDlg(context: context, message: "차단이해제되었습니다".tr(), onClickAction: (action) => _requestBlockList());
      }
    });
  }

  void _requestBlockList() {
    flag.enableLoading(fn: () => onUpDate());
    apiHelper.requestBlockList().then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        data.blockList = response.getData();
      } else {
        data.blockList = BlockListData(results: []);
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }

  Future<void> _requestNextList() async {
    try {
      if (data.blockList.next.isNotEmpty) {
        apiHelper.requestNextBlockList(data.blockList.next).then((response) {
          if (response.getCode() == KEY_SUCCESS) {
            data.blockList.results!.addAll(response.getData().results!);
            data.blockList.next = response.getData().next;
            data.blockList.previous = response.getData().previous;
            data.blockList.count += response.getData().count;
          }
          flag.disableLoading(fn: () => onUpDate());
          return data.blockList;
        }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
