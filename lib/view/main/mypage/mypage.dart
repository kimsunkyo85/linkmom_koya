import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/mypage_myinfo_response.dart';
import 'package:linkmom/data/network/models/req_job_data.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/manager/firebase_analytics_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/auth_center/auth_center_page.dart';
import 'package:linkmom/view/main/chat/chat_view/chat_bubble.dart';
import 'package:linkmom/view/main/chat/chat_view/chat_bubble_view.dart';
import 'package:linkmom/view/main/job_apply/temp_area/temp_area_view_page.dart';
import 'package:linkmom/view/main/mypage/benefit/benefit_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/payment/payment_history_page.dart';
import 'package:linkmom/view/main/mypage/favorite/linkmom_favorite_page.dart';
import 'package:linkmom/view/main/mypage/favorite/momdaddy_favorite_page.dart';
import 'package:linkmom/view/main/mypage/my_info/my_account_manager_page.dart';
import 'package:linkmom/view/main/mypage/my_info/my_info_page.dart';
import 'package:linkmom/view/main/mypage/review/linkmom_review_page.dart';
import 'package:linkmom/view/main/mypage/review/momdaddy_review_page.dart';
import 'package:linkmom/view/main/mypage/work_noti_page.dart';
import 'package:linkmom/view/main/survey/survey_page.dart';
import 'package:provider/provider.dart';

import '../../../main.dart';
import '../../../route_name.dart';
import 'care_diary/linkmom_diary_list_page.dart';
import 'care_diary/momdady_diary_list_page.dart';
import 'child_info/child_info_page.dart';
import 'child_info/child_menu_page.dart';
import 'penalty/penalty_manage_page.dart';

class MyPage extends BaseStateful {
  final DataManager? data;

  MyPage({this.data});

  @override
  _MyPageState createState() => _MyPageState();
}

class _MyPageState extends BaseStatefulState<MyPage> {
  USER_TYPE _userType = USER_TYPE.mom_daddy;
  double iconSize = 40.0;

  MyInfoData get my => auth.user.my_info_data!;

  bool _isBlockUser = false;

  GlobalKey keyMomdady = GlobalKey();
  GlobalKey keyLinkmom = GlobalKey();
  Timer? _timer;
  bool _visible = false;
  bool _visibleView = false;
  Size sizeTime = Size(0, 0);
  Offset offset = Offset(0, 0);
  double vpHeight = 0;
  RenderAbstractViewport? viewport;
  int _modeCnt = 0;
  final int MODE_CNT = 10;

  @override
  void initState() {
    super.initState();
    iconSize = ScreenUtil().setWidth(40);
    _isBlockUser = storageHelper.penalty;
    getModeCnt();
    _requestInfo();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer!.cancel();
    }
    super.dispose();
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
  }

  getModeCnt() async {
    _modeCnt = await storageHelper.getModeCnt();
  }

  @override
  Widget build(BuildContext context) {
    ///채팅방에서 강제로 변경시 자동 반영
    _userType = storageHelper.user_type;
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: settingBtnAppBar("마이페이지".tr(), onClick: () => Commons.penaltyCheck(() => Commons.page(context, routeSettings), forceEnable: true), onBack: () => Commons.pagePop(context)),
        body: LinkmomRefresh(
            padding: padding_0,
            child: Stack(
              children: [
                ChangeNotifierProvider(
                  create: (ctx) => storageHelper,
                  child: Column(
                    children: [
                      lInkWell(
                          onTap: () => _toggleUserType(),
                          child: Container(
                            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                            color: Commons.getColor(),
                            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                              lText("맘대디".tr(), key: keyMomdady, style: st_16(fontWeight: FontWeight.w500, textColor: !Commons.isLinkMom() ? Colors.white : Colors.white.withAlpha(108))),
                              Padding(
                                padding: padding_10_LR,
                                child: FlutterSwitch(
                                  width: 60,
                                  padding: 2.0,
                                  toggleColor: Commons.getColor(),
                                  activeColor: Colors.white,
                                  inactiveColor: Colors.white,
                                  toggleSize: 30,
                                  value: Commons.isLinkMom(),
                                  onToggle: (value) => _toggleUserType(),
                                ),
                              ),
                              lText("링크쌤".tr(), key: keyLinkmom, style: st_16(fontWeight: FontWeight.w500, textColor: Commons.isLinkMom() ? Colors.white : Colors.white.withAlpha(108))),
                            ]),
                          )),
                      _profileComponent(),
                      lDivider(thickness: 8.0, color: color_61e5e5ea),
                      Container(
                          padding: padding_20,
                          child: Column(children: [
                            if (auth.user.username.isNotEmpty && Commons.isLinkMom())
                              Container(
                                  decoration: BoxDecoration(border: Border.all(color: color_eeeeee), borderRadius: BorderRadius.circular(16)),
                                  padding: padding_20,
                                  margin: padding_20_B,
                                  child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                    Row(children: [
                                      lIconText(title: "총돌봄시간".tr(), isExpanded: false, textStyle: st_15(textColor: color_999999), icon: Lcons.total_time()),
                                      Padding(
                                        padding: padding_05_L,
                                        child: TextHighlight(
                                          text: '${my.mypageInfoCnt == null ? 0 : my.mypageInfoCnt!.totalCareTime}${"시간".tr()}',
                                          term: '${my.mypageInfoCnt == null ? 0 : my.mypageInfoCnt!.totalCareTime}',
                                          textStyleHighlight: st_b_16(fontWeight: FontWeight.w700),
                                          textStyle: st_b_15(),
                                        ),
                                      ),
                                    ]),
                                    sb_h_10,
                                    Row(children: [
                                      lIconText(title: "총돌봄소득".tr(), isExpanded: false, textStyle: st_15(textColor: color_999999), icon: Lcons.total_income()),
                                      Padding(
                                        padding: padding_05_L,
                                        child: TextHighlight(
                                            text: '${my.mypageInfoCnt == null ? 0 : StringUtils.formatPay(my.mypageInfoCnt!.totalIncomeCash)}${"원".tr()}',
                                            term: '${my.mypageInfoCnt == null ? 0 : StringUtils.formatPay(my.mypageInfoCnt!.totalIncomeCash)}',
                                            textStyleHighlight: st_b_16(fontWeight: FontWeight.w700),
                                            textStyle: st_b_15()),
                                      ),
                                    ])
                                  ])),
                            Container(
                                padding: padding_10_LTR,
                                child: AspectRatio(
                                  aspectRatio: Commons.ratio(7.5),
                                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                    Commons.isLinkMom()
                                        ? TopIconButton(icon: Lcons.certified_center(size: iconSize, disableColor: color_999999, isEnabled: !_isBlockUser), title: "인증센터".tr(), onClick: () => _moveAuthCenter(), isBlockUser: _isBlockUser)
                                        : TopIconButton(icon: Lcons.my_child(size: iconSize, disableColor: color_999999, isEnabled: !_isBlockUser), title: "우리아이".tr(), onClick: () => _moveChild(), isBlockUser: _isBlockUser),
                                    TopIconButton(
                                      icon: Lcons.review_management(size: iconSize, disableColor: !_isBlockUser ? null : color_999999, isEnabled: !_isBlockUser ? Commons.isLinkMom() : !_isBlockUser),
                                      title: "후기관리".tr(),
                                      onClick: () => _moveReview(),
                                      isBlockUser: _isBlockUser,
                                    ),
                                    TopIconButton(
                                      icon: Lcons.like_list(size: iconSize, disableColor: !_isBlockUser ? null : color_999999, isEnabled: !_isBlockUser ? Commons.isLinkMom() : !_isBlockUser),
                                      title: "관심목록".tr(),
                                      onClick: () => _moveFavorite(),
                                      isBlockUser: _isBlockUser,
                                    ),
                                    TopIconButton(
                                      icon: Lcons.payment_details(size: iconSize, disableColor: !_isBlockUser ? null : color_999999, isEnabled: !_isBlockUser ? Commons.isLinkMom() : !_isBlockUser),
                                      title: "결제내역".tr(),
                                      onClick: () => _movePaymentHistory(),
                                      isBlockUser: _isBlockUser,
                                    ),
                                  ]),
                                )),
                          ])),
                      lDivider(thickness: 8.0, color: color_61e5e5ea),
                      Container(
                          padding: padding_20,
                          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                            lIconText(
                              title: Commons.isLinkMom() ? "근무관리".tr() : "돌봄관리".tr(),
                              icon: Lcons.info(color: color_cecece, isEnabled: true, size: 25),
                              textStyle: st_b_18(fontWeight: FontWeight.w700),
                              isExpanded: false,
                              padding: padding_02_R,
                              onClick: () => showNormalDlg(
                                context: context,
                                message: Commons.isLinkMom() ? "홈링크쌤돌봄일기안내".tr() : "홈맘대디돌봄일기안내".tr(),
                              ),
                              isLeft: false,
                            ),
                            sb_h_20,
                            Commons.isLinkMom()
                                ? Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                                    Flexible(child: lIconText(title: "돌봄일기작성".tr(), icon: Lcons.care_diary(size: 30, isEnabled: Commons.isLinkMom()), textStyle: st_b_14(), isExpanded: false, padding: padding_10_L, onClick: () => _moveDiary())),
                                    Flexible(child: lIconText(title: "출퇴근알림".tr(), icon: Lcons.commuting_to_work(size: 30), textStyle: st_b_14(), isExpanded: false, padding: padding_10_L, onClick: () => _moveWorkNoti()))
                                  ])
                                : Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                                    Flexible(child: lIconText(title: "돌봄일기_정산".tr(), icon: Lcons.care_diary(size: 30, isEnabled: Commons.isLinkMom()), textStyle: st_b_14(), isExpanded: false, padding: padding_10_L, onClick: () => _moveDiary())),
                                    Flexible(
                                      child: lIconText(
                                        title: "투약의뢰서".tr(),
                                        icon: Lcons.medication_form(size: 30),
                                        textStyle: st_b_14(textColor: _isBlockUser ? color_999999 : color_222222),
                                        isExpanded: false,
                                        padding: padding_10_L,
                                        onClick: () => Commons.penaltyCheck(() => _moveDrug()),
                                      ),
                                    )
                                  ])
                          ])),
                      lDivider(thickness: 8.0, color: color_61e5e5ea),
                      // menu
                      Container(
                          padding: padding_20_LR,
                          child: Column(
                            children: [
                              _menuComponent("나의성향".tr(), movePage: () => _moveSurvey(), disable: _isBlockUser),
                              _menuComponent("인증센터".tr(), movePage: () => _moveAuthCenter(), disable: _isBlockUser),
                              _menuComponent("계정관리".tr(), movePage: () => _moveAccountManage(), disable: _isBlockUser),
                              _menuComponent("인출신청".tr(), movePage: () => _moveWithdrawal(), disable: _isBlockUser),
                              _menuComponent("벌점스티커".tr(), movePage: () => _movePenaltyManage(), disable: _isBlockUser),
                              _menuComponent("혜택쿠폰함".tr(), movePage: () => _moveBenefit(), disable: _isBlockUser, showBorder: false /*Commons.isLinkMom() ? false : true*/),
                              // if (!Commons.isLinkMom()) _menuComponent("장소관리".tr(), movePage: () => _moveTempArea(), disable: _isBlockUser, showBorder: false),//2022/03/17 메뉴에서 제거하기로함
                            ],
                          )),
                      lDivider(thickness: 8.0, color: color_61e5e5ea),
                      Container(
                          padding: padding_20_LRB,
                          child: Column(
                            children: [
                              _menuComponent("고객센터".tr(), movePage: () => _moveCs(), disable: _isBlockUser, showBorder: false),
                            ],
                          )),
                    ],
                  ),
                ),
                if (_visibleView) _popView(),
                // _popView(),
              ],
            ),
            onRefresh: () => _requestInfo()),
      ),
    );
  }

  Widget _popView() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      RenderBox? box = keyMomdady.currentContext!.findRenderObject() as RenderBox?;
      sizeTime = box!.size;
      offset = box.localToGlobal(Offset.zero);
      viewport = RenderAbstractViewport.of(keyMomdady.currentContext!.findRenderObject());
      vpHeight = viewport!.paintBounds.height;

      box = keyLinkmom.currentContext!.findRenderObject() as RenderBox?;
      sizeTime = box!.size;
      offset = box.localToGlobal(Offset.zero);
      viewport = RenderAbstractViewport.of(keyLinkmom.currentContext!.findRenderObject());
      vpHeight = viewport!.paintBounds.height;
    });

    double top = sizeTime.height == 0 ? kTextTabBarHeight : (kTextTabBarHeight / 2) + sizeTime.height;
    return lContainer(
      color: color_transparent,
      margin: EdgeInsets.fromLTRB(sizeTime.width, top, sizeTime.width, 0),
      alignment: Alignment.topCenter,
      child: AnimatedOpacity(
        opacity: _visible ? 1.0 : 0.0,
        duration: Duration(milliseconds: 500),
        onEnd: () {
          if (!_visible) {
            _visibleView = false;
          }
          onUpDate();
        },
        child: InkWell(
          onTap: _visible
              ? () {
                  if (_timer != null) {
                    _timer!.cancel();
                    _visible = false;
                    onUpDate();
                  }
                }
              : null,
          child: ChatBubble(
            clipper: ChatBubbleView(
              type: _userType.type,
              viewWidth: sizeTime.width,
              radius: 6,
            ),
            alignment: Alignment.topCenter,
            margin: padding_05_T,
            backGroundColor: color_white,
            child: Padding(
              padding: EdgeInsets.fromLTRB(05, 03, 05, 03),
              child: TextHighlight(
                // _userType == USER_TYPE.mom_daddy ? "아이볼봄이 필요한 부모님이에요." : '아이를 돌봐주는 돌봄 성생님이에요.',
                text: _userType.typeContent,
                term: _userType.typeContentHint,
                textStyle: st_b_13(),
                textStyleHighlight: st_b_13(textColor: Commons.getColor()),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _menuComponent(String title, {bool showBorder = true, required Function movePage, bool disable = false}) {
    return lInkWell(
      onTap: () => Commons.penaltyCheck(() => movePage(), forceEnable: title == "돌봄일기".tr()),
      child: Column(children: [
        Container(
            padding: padding_20_TB,
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(border: Border(bottom: BorderSide(color: showBorder ? color_eeeeee : Colors.transparent))),
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              lText(title, style: st_b_15(fontWeight: FontWeight.w500, textColor: disable ? color_999999 : color_222222)),
              Lcons.nav_right(color: disable ? color_999999 : color_222222, size: 15),
            ])),
      ]),
    );
  }

  /// profile
  Widget _profileComponent() {
    return auth.user.username.isNotEmpty
        ? Column(children: [
            Container(
              color: Colors.white,
              padding: padding_20,
              child: Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: [
                lProfileAvatar(my.profileimg, radius: 35),
                sb_w_20,
                Container(
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  lIconText(
                    title: _userType == USER_TYPE.link_mom ? my.grade_linkmom_txt : my.grade_momdady_txt,
                    textStyle: st_b_15(textColor: Commons.getColor(), fontWeight: FontWeight.w500),
                    isExpanded: false,
                    isLeft: false,
                    padding: padding_0,
                    icon: lBtnWrapOutLine(
                      "등급".tr(),
                      textStyle: st_09(textColor: Commons.getColor()),
                      padding: padding_07_LR,
                      height: 20,
                      sideEnableColor: Commons.getColor(),
                      onClickAction: () => _excuteCheck(() => showRankInfo()),
                    ),
                  ),
                  lIconText(
                    title: my.first_name,
                    isExpanded: false,
                    isLeft: false,
                    icon: Lcons.nav_right(size: 15, color: color_cecece),
                    textStyle: st_b_18(fontWeight: FontWeight.w700),
                    padding: padding_05_R,
                    onClick: () => _moveMyInfo(),
                  ),
                  sb_h_05,
                  Commons.isLinkMom() && my.mypageInfoCnt != null
                      ? Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                          if (auth.account.authinfo != null)
                            TextHighlight(
                                text: "${"인증배지".tr()} ${my.mypageInfoCnt!.authCnt}${"개".tr()}",
                                term: my.mypageInfoCnt!.authCnt.toString(),
                                textStyle: st_13(textColor: color_545454),
                                textStyleHighlight: st_14(textColor: color_545454, fontWeight: FontWeight.w500)),
                          _divider(),
                          lInkWell(
                              onTap: () => _moveReview(),
                              child: TextHighlight(
                                  text: '${"후기".tr()} ${my.mypageInfoCnt!.reviewCntLinkmom}${"개".tr()}',
                                  term: my.mypageInfoCnt!.reviewCntLinkmom.toString(),
                                  textStyle: st_13(textColor: color_545454),
                                  textStyleHighlight: st_14(textColor: color_545454, fontWeight: FontWeight.w500))),
                          _divider(),
                          lIconText(
                            title: '${my.mypageInfoCnt!.favoiteCntLinkmom}',
                            icon: Lcons.heart(color: Commons.getColor(), size: 13),
                            isExpanded: false,
                            textStyle: st_14(textColor: color_545454, fontWeight: FontWeight.w500),
                          ),
                        ])
                      : lInkWell(
                          onTap: () => _moveMyInfo(),
                          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                            TextHighlight(
                                text: my.numberofchild != 99 ? "${"자녀".tr()} ${my.numberofchild.toString()} ${"명".tr()}" : "미등록".tr(),
                                term: my.numberofchild.toString(),
                                textStyle: st_13(textColor: color_545454),
                                textStyleHighlight: st_14(textColor: color_545454, fontWeight: FontWeight.w500)),
                            _divider(),
                            lText(_getJob(my.job), overflow: TextOverflow.ellipsis, style: st_13(textColor: color_545454)),
                            _divider(),
                            if (my.mypageInfoCnt != null)
                              lInkWell(
                                  onTap: () => _moveReview(),
                                  child: TextHighlight(
                                      text: '${"후기".tr()} ${my.mypageInfoCnt!.reviewCntMomdady}${"개".tr()}',
                                      term: my.mypageInfoCnt!.reviewCntMomdady.toString(),
                                      textStyle: st_13(textColor: color_545454),
                                      textStyleHighlight: st_14(textColor: color_545454, fontWeight: FontWeight.w500)))
                          ]),
                        ),
                  sb_h_05,
                  lIconText(
                      title: !Commons.isLinkMom()
                          ? "돌봄신청서보기".tr()
                          : auth.indexInfo.linkmominfo.isBookingjobs
                              ? "링크쌤지원서보기".tr()
                              : "링크쌤지원하기".tr(),
                      isExpanded: false,
                      isLeft: false,
                      icon: !Commons.isLinkMom()
                          ? Lcons.nav_right(size: 13, color: color_cecece)
                          : lBtnWrap(
                              my.bookingjobs_ing ? BookingLinkMomStatus.inprogress.name : BookingLinkMomStatus.end.name,
                              textStyle: st_10(fontWeight: FontWeight.bold),
                              height: 20,
                              padding: padding_10_LR,
                              sideEnableColor: color_transparent,
                              btnEnableColor: my.bookingjobs_ing ? Commons.getColor() : color_b2b2b2,
                            ),
                      textStyle: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.w700),
                      padding: padding_05_R,
                      onClick: () => _excuteCheck(() {
                            if (!Commons.isLinkMom()) {
                              Commons.pageToMainTabMove(context, tabIndex: TAB_SCHEDULE, isPop: true);
                            } else {
                              Commons.moveApplyPage(context, data, viewMode: ViewMode(viewType: ViewType.modify), callBack: (result) {
                                if (result != null) {
                                  if (result is ReqJobData) {
                                    _requestInfo();
                                  }
                                }
                              });
                            }
                          }, forceEnable: true)),
                ])),
              ]),
            ),
            Container(
                margin: padding_20_LRB,
                padding: padding_20,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: color_eeeeee.withOpacity(0.42),
                ),
                child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                  lInkWell(
                    onTap: () => _movePaymentHistory(tab: 1),
                    child: Column(children: [
                      lText("보유캐시".tr(), style: st_b_15(fontWeight: FontWeight.w500)),
                      sb_h_05,
                      lIconText(
                        title: my.mypageInfoCnt == null ? '0' : StringUtils.formatPay(my.mypageInfoCnt!.linkCash),
                        icon: Lcons.cash(size: 16, isEnabled: true),
                        isExpanded: false,
                        padding: padding_06_R,
                        isLeft: false,
                        textStyle: st_16(textColor: Commons.getColor(), fontWeight: FontWeight.w700),
                      ),
                    ]),
                  ),
                  Container(height: 30, child: lDividerVertical(thickness: 1, color: color_dedede)),
                  lInkWell(
                    onTap: () => _movePaymentHistory(tab: 2),
                    child: Column(children: [
                      lText("보유포인트".tr(), style: st_b_15(fontWeight: FontWeight.w500)),
                      sb_h_05,
                      lIconText(
                          title: my.mypageInfoCnt == null ? '0' : StringUtils.formatPay(my.mypageInfoCnt!.linkPoint),
                          icon: Lcons.point(size: 16),
                          isExpanded: false,
                          padding: padding_06_R,
                          isLeft: false,
                          textStyle: st_16(textColor: Commons.getColor(), fontWeight: FontWeight.w700)),
                    ]),
                  ),
                ])),
          ])
        : Container(
            padding: padding_20_TB,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                lText("회원전용서비스입니다".tr(), style: st_b_16(fontWeight: FontWeight.w500), textAlign: TextAlign.center),
                lBtnOutline("로그인".tr(), borderRadius: 16, margin: padding_20, sideColor: Commons.getColor(), style: st_16(textColor: Commons.getColor(), fontWeight: FontWeight.w700), onClickAction: () => Commons.pageClear(context, routeLogin))
              ],
            ));
  }

  void _toggleUserType() {
    switch (_userType) {
      case USER_TYPE.link_mom:
        storageHelper.setUserType(USER_TYPE.mom_daddy, callBack: (userType) => _userType = userType);
        break;
      case USER_TYPE.mom_daddy:
        storageHelper.setUserType(USER_TYPE.link_mom, callBack: (userType) => _userType = userType);
        break;
      default:
    }
    if (_timer != null) {
      _timer!.cancel();
    }
    _visibleView = true;
    onUpDate();
    getModeCnt();
    FbaManager.addLogEvent(viewName: routeMyPage, type: FbaLogType.menu, parameters: {FbaLogType.mode.name: _userType.name});
    if (_modeCnt <= MODE_CNT) {
      Future.delayed(Duration(milliseconds: 100)).then((_) {
        storageHelper.setModeCnt(_modeCnt + 1);
        _visible = true;
        _startTimer();
        onUpDate();
      });
    } else {
      _visibleView = false;
      _visible = false;
      onUpDate();
    }
  }

  void _startTimer() {
    _timer = Timer.periodic(Duration(milliseconds: 1000), (timer) {
      if (timer.tick == 4) {
        _timer!.cancel();
        _visible = false;
        onUpDate();
      }
    });
  }

  Widget _divider() {
    return Container(height: 14, margin: padding_05_LR, child: lDividerVertical(color: color_dedede, thickness: 1));
  }

  String _getJob(int job) => EnumUtils.getJobTypeValue(job).name;

  void _moveSurvey() {
    Commons.page(context, routeSurvey, arguments: SurveyPage(data: data));
  }

  void _moveAuthCenter() async {
    Commons.page(context, routeAuthCenter, arguments: AuthCenterPage(data: data));
  }

  void _moveMyInfo() {
    Commons.page(context, routeMyInfo, arguments: MyInfoPage(data: data, viewMode: ViewMode(viewType: ViewType.modify)));
  }

  void _moveChild() {
    Commons.page(context, routeChildMenu, arguments: ChildMenuPage(data: data));
  }

  void _moveFavorite() {
    if (storageHelper.user_type == USER_TYPE.link_mom) {
      Commons.page(context, routeMypageLinkmomFavorite, arguments: LinkmomFavoritePage(data: data));
    } else {
      Commons.page(context, routeMypageMomdaddyFavorite, arguments: MomdaddyFavoritePage(data: data));
    }
  }

  void _moveDiary() {
    if (storageHelper.user_type == USER_TYPE.link_mom) {
      Commons.page(context, routeLinkmomDiary, arguments: LinkmomDiaryListPage(data: data));
    } else {
      Commons.page(context, routeMomdadyDiary, arguments: MomdadyDiaryListPage(data: data));
    }
  }

  void _moveReview() {
    if (storageHelper.user_type == USER_TYPE.link_mom) {
      Commons.page(context, routeMypageLinkmomReview, arguments: LinkmomReviewPage(data: data));
    } else {
      Commons.page(context, routeMypageMomdaddyReview, arguments: MomdaddyReviewPage(data: data));
    }
  }

  void _movePaymentHistory({int tab = 0}) {
    Commons.page(context, routePaymentHistory, arguments: PaymentHistoryPage(tabIndex: tab));
  }

  void _moveAccountManage() {
    Commons.page(context, routeMyInfoAccount, arguments: AccountManagerPage(data: data));
  }

  void _movePenaltyManage() {
    Commons.page(context, routePenaltyManage, arguments: PenaltyManagePage(view: ViewType.normal));
  }

  void _moveWorkNoti() {
    Commons.page(context, routeWorkNoti, arguments: WorkNotiPage(data: data));
  }

  void _moveCs() {
    Commons.page(context, routeCs);
  }

  void _moveDrug() {
    ChildListPageMode mode = ChildListPageMode.drugInfo;
    Commons.page(context, routeChildInfo, arguments: ChildInfoPage(data: widget.data, mode: mode, viewMode: ViewMode(viewType: ViewType.apply)));
  }

  void _moveWithdrawal() {
    Commons.page(context, routeWithdrawal);
  }

  void _moveBenefit() {
    Commons.page(context, routeBenefit, arguments: BenefitPage(data: this.data));
  }

  void _moveTempArea() {
    Commons.page(context, routeTempAreaView, arguments: TempAreaViewPage(viewMode: ViewMode(viewType: ViewType.modify)));
  }

  void _requestInfo() {
    if (auth.user.username.isEmpty) return;
    apiHelper.requestAccount().then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        auth.setAccount = response.getData();
        onUpDate();
      }
    });
    apiHelper.requestMyInfoView().then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        auth.user.my_info_data = response.getData();
        onUpDate();
      }
    });
  }

  void _excuteCheck(Function onClick, {bool forceEnable = false}) {
    if (_isBlockUser && !forceEnable) {
      showNormalDlg(context: context, message: "사용정지된사용자입니다".tr());
    } else {
      lLoginCheck(context, () => onClick());
    }
  }
}

class TopIconButton extends StatelessWidget {
  final String title;
  final Widget icon;
  final Function onClick;
  final bool isBlockUser;

  TopIconButton({required this.title, required this.icon, required this.onClick, this.isBlockUser = false});

  @override
  Widget build(BuildContext context) {
    return lInkWell(
        onTap: () => Commons.penaltyCheck(() => onClick(), message: isBlockUser ? "사용정지된사용자입니다".tr() : null),
        child: Container(
            child: Column(children: [
          Padding(padding: padding_10_LRB, child: icon),
          lText(title, style: st_b_14(textColor: isBlockUser ? color_999999 : color_222222)),
        ])));
  }
}
