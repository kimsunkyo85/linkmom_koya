import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/mypage_myinfo_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';

Widget likeReview({MyInfoLikeReViewItem? data, ViewType? viewType, int like = 0, int review = 0, MainAxisAlignment mainAxisAlignment = MainAxisAlignment.center, required Function callBack, Color? color}) {
  if (storageHelper.user_type == USER_TYPE.mom_daddy) {
    if (viewType == ViewType.view) {
      like = data!.favoite_cnt_linkmom;
      review = data.review_cnt_linkmom;
    } else {
      like = data!.favoite_cnt_momdady;
      review = data.review_cnt_momdady;
    }
  } else {
    if (viewType == ViewType.view) {
      like = data!.favoite_cnt_momdady;
      review = data.review_cnt_momdady;
    } else {
      like = data!.favoite_cnt_linkmom;
      review = data.review_cnt_linkmom;
    }
  }

  return Row(
    mainAxisAlignment: mainAxisAlignment,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      lIconText(
        title: '$like',
        textStyle: st_b_15(fontWeight: FontWeight.w700),
        icon: Lcons.heart(
          color: color ?? (Commons.isLinkMom() ? color_main : color_linkmom),
          size: 18,
        ),
        padding: padding_05_L,
        isExpanded: false,
      ),
      lDot(color: color_b2b2b2, padding: padding_05_LR, size: 2),
      lInkWell(
        onTap: () => callBack(DialogAction.review),
        child: Padding(
          padding: padding_05_TB,
          child: TextHighlight(
            text: '${"후기".tr()} $review${"개".tr()}',
            term: '$review',
            textStyle: st_15(textColor: color_545454),
            textStyleHighlight: st_15(textColor: color_545454, fontWeight: FontWeight.bold),
          ),
        ),
      ),
      Lcons.nav_right(size: 12, color: color_cecece),
    ],
  );
}
