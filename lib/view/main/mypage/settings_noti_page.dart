import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/settings_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../../../main.dart';

class SettingsNotiPage extends BaseStateful {
  final SettingsData? data;

  SettingsNotiPage({this.data});

  @override
  _SettingsNotiPageState createState() => _SettingsNotiPageState();
}

class _SettingsNotiPageState extends BaseStatefulState<SettingsNotiPage> {
  late SettingsData _settings;

  @override
  void initState() {
    super.initState();
    _settings = widget.data ?? auth.getSettings;
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data = _data;
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar("알림".tr(), hide: false, onBack: () {
              Commons.pagePop(context, data: _settings);
            }),
            body: Container(
              padding: padding_20,
              child: Column(
                children: [
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          padding: padding_15_TB,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              lText("서비스알림".tr(), style: st_b_14(textColor: color_545454)),
                              FlutterSwitch(
                                  width: 60,
                                  padding: 3.0,
                                  activeColor: Commons.getColor(),
                                  inactiveColor: color_cecece,
                                  toggleSize: 30,
                                  value: _settings.is_push_service,
                                  onToggle: (value) {
                                    _settings.is_push_service = value;
                                    onUpDate();
                                  }),
                            ],
                          )),
                      Container(
                          padding: padding_15_TB,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              lText("맘대디알림".tr(), style: st_b_14(textColor: color_545454)),
                              FlutterSwitch(
                                  width: 60,
                                  padding: 3.0,
                                  activeColor: Commons.getColor(),
                                  inactiveColor: color_cecece,
                                  toggleSize: 30,
                                  value: _settings.is_push_momdady,
                                  onToggle: (value) {
                                    _settings.is_push_momdady = value;
                                    onUpDate();
                                  }),
                            ],
                          )),
                      Container(
                          padding: padding_15_TB,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              lText("링크쌤알림".tr(), style: st_b_14(textColor: color_545454)),
                              FlutterSwitch(
                                  width: 60,
                                  padding: 3.0,
                                  activeColor: Commons.getColor(),
                                  inactiveColor: color_cecece,
                                  toggleSize: 30,
                                  value: _settings.is_push_linkmom,
                                  onToggle: (value) {
                                    _settings.is_push_linkmom = value;
                                    onUpDate();
                                  }),
                            ],
                          )),
                      Container(
                          padding: padding_15_TB,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              lText("돌봄신청서알림".tr(), style: st_b_14(textColor: color_545454)),
                              FlutterSwitch(
                                  width: 60,
                                  padding: 3.0,
                                  activeColor: Commons.getColor(),
                                  inactiveColor: color_cecece,
                                  toggleSize: 30,
                                  value: _settings.is_push_care,
                                  onToggle: (value) {
                                    _settings.is_push_care = value;
                                    onUpDate();
                                  }),
                            ],
                          )),
                      Container(
                          padding: padding_15_TB,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              lText("링크쌤지원서알림".tr(), style: st_b_14(textColor: color_545454)),
                              FlutterSwitch(
                                  width: 60,
                                  padding: 3.0,
                                  activeColor: Commons.getColor(),
                                  inactiveColor: color_cecece,
                                  toggleSize: 30,
                                  value: _settings.is_push_job,
                                  onToggle: (value) {
                                    _settings.is_push_job = value;
                                    onUpDate();
                                  }),
                            ],
                          )),
                      Container(
                          padding: padding_15_TB,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              lText("커뮤니티알림".tr(), style: st_b_14(textColor: color_545454)),
                              FlutterSwitch(
                                  width: 60,
                                  padding: 3.0,
                                  activeColor: Commons.getColor(),
                                  inactiveColor: color_cecece,
                                  toggleSize: 30,
                                  value: _settings.is_push_community,
                                  onToggle: (value) {
                                    _settings.is_push_community = value;
                                    onUpDate();
                                  }),
                            ],
                          )),
                      Container(
                          padding: padding_15_T,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              lText("이벤트알림".tr(), style: st_b_14(textColor: color_545454)),
                              FlutterSwitch(
                                  width: 60,
                                  padding: 3.0,
                                  activeColor: Commons.getColor(),
                                  inactiveColor: color_cecece,
                                  toggleSize: 30,
                                  value: _settings.is_push_event,
                                  onToggle: (value) {
                                    _settings.is_push_event = value;
                                    onUpDate();
                                  }),
                            ],
                          )),
                      lDivider(padding: padding_24_TB),
                      lText("알림_안내".tr(), style: st_12(textColor: color_999999)),
                    ],
                  ))
                ],
              ),
            )));
  }
}
