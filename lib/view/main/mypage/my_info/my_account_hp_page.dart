import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/mypage/my_info/my_account_hp_change_page.dart';

import '../../../../main.dart';

class MyAccountHpPage extends BaseStateful {
  final DataManager? data;
  MyAccountHpPage({this.data});

  _MyAccountHpPageState createState() => _MyAccountHpPageState();
}

class _MyAccountHpPageState extends BaseStatefulState<MyAccountHpPage> {
  @override
  void initState() {
    super.initState();
    super.onData(widget.data ?? DataManager());
    data.tcMobile.text = auth.account.hpnumber;
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        context: context,
        resizeToAvoidBottomPadding: false,
        appBar: appBar("휴대폰번호".tr(), isClose: false, hide: false),
        body: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
          Expanded(
              child: Container(
                  padding: padding_20,
                  child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
                    sb_h_30,
                    Container(
                        margin: padding_20_T,
                        alignment: Alignment.centerLeft,
                        child: lText(
                          "휴대폰번호".tr(),
                          style: stAgrTitle,
                        )),
                    Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                      Row(children: [
                        Flexible(
                            child: Form(
                                key: data.formMobile,
                                child: Focus(
                                  child: TextFormField(
                                    controller: data.tcMobile,
                                    textInputAction: TextInputAction.done,
                                    keyboardType: TextInputType.number,
                                    enabled: false,
                                    decoration: underLineDecoration("휴대폰번호_힌트".tr(), counterText: ''),
                                  ),
                                ))),
                      ]),
                    ]),
                  ]))),
          lBtn("변경".tr(), onClickAction: () {
            Commons.page(context, routeMyAccountHpChange, arguments: MyAccountHpChangePage(data: data));
          }, btnColor: color_545454),
        ]));
  }
}
