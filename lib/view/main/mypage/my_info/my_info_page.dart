import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:linkmom/data/network/models/mypage_myinfo_response.dart';
import 'package:linkmom/data/network/models/mypage_myinfo_save_request.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/dropdown/custom_dropdown.dart';
import 'package:linkmom/utils/listview/list_authtype_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/auth_center/location/location_page.dart';
import 'package:linkmom/view/main/chat/full_photo.dart';
import 'package:linkmom/view/main/job_apply/content_view.dart';
import 'package:linkmom/view/main/mypage/review/linkmom_review_page.dart';
import 'package:linkmom/view/main/mypage/review/momdaddy_review_page.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../myinfo_view.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : my_info_page.dart - 나의 정보
/// since        : 5/6/21 / update:
class MyInfoPage extends BaseStateful {
  @override
  _MyInfoPageState createState() => _MyInfoPageState();

  MyInfoPage({this.data, required this.viewMode});

  final DataManager? data;
  final ViewMode viewMode;
}

class _MyInfoPageState extends BaseStatefulState<MyInfoPage> {
  ScrollController _controller = ScrollController();
  TextEditingController _nickCtrl = TextEditingController();

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode);
    onData(widget.data ?? DataManager());
    _requestMyInfoView();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        context: context,
        appBar: appBar(
          "나의정보".tr(),
          hide: !isModifyMode(),
          onClick: (a) {
            Commons.pagePop(context);
          },
        ),
        body: Column(
          children: [
            Expanded(
              child: lScrollView(
                padding: padding_0,
                controller: _controller,
                child: lContainer(
                  width: double.infinity,
                  child: Column(
                    children: <Widget>[
                      lContainer(
                        color: color_eeeeee,
                        height: heightFull(context) * 0.25,
                        child: InkWell(
                          onTap: () {
                            Commons.nextPage(context,
                                fn: () => FullPhotoPage(
                                      urlList: data.myInfoItem.request.profileimg != null ? [] : [data.myInfoItem.myInfoData.profileimg_original],
                                      fileList: data.myInfoItem.request.profileimg != null ? [data.myInfoItem.request.profileimg!] : null,
                                      name: "나의정보".tr(),
                                      fit: BoxFit.contain,
                                    ));
                          },
                          child: Stack(
                            children: [
                              Center(
                                child: lBtnWrap("환경인증_사진등록".tr(), btnEnableColor: color_white, textStyle: st_16(textColor: color_545454), sideColor: color_999999, sideEnableColor: color_dedede, onClickAction: () async {
                                  if (await Permission.camera.isPermanentlyDenied) {
                                    openAppSettings();
                                  }

                                  _showPhoto();
                                }),
                              ),
                              if (StringUtils.validateString(data.myInfoItem.myInfoData.profileimg))
                                CachedNetworkImage(
                                  imageUrl: data.myInfoItem.myInfoData.profileimg_original,
                                  imageBuilder: (context, imageProvider) => imageBuilder(imageProvider, fit: BoxFit.cover, width: widthFull(context), height: heightFull(context)),
                                  placeholder: (context, url) => placeholder(),
                                  errorWidget: (context, url, error) => errorWidget(size: 100),
                                )
                              else if (data.myInfoItem.request.profileimg != null)
                                Image.file(
                                  data.myInfoItem.request.profileimg!,
                                  frameBuilder: (context, child, frame, wasSynchronouslyLoaded) {
                                    if (frame == 0 || wasSynchronouslyLoaded) return child;
                                    return lLoadingIndicator();
                                  },
                                  fit: BoxFit.cover,
                                  width: widthFull(context),
                                ),
                              if (data.myInfoItem.request.profileimg != null || StringUtils.validateString(data.myInfoItem.myInfoData.profileimg))
                                Align(
                                  alignment: Alignment.bottomRight,
                                  child: InkWell(
                                    onTap: () async {
                                      //사진가져오기 권한이 필요한 경우...
                                      if (await Commons.permissionStorage()) {
                                        return;
                                      }
                                      _showPhoto();
                                    },
                                    child: Container(
                                      padding: padding_10,
                                      margin: padding_15,
                                      decoration: BoxDecoration(color: Colors.black.withOpacity(0.32), borderRadius: BorderRadius.circular(50)),
                                      child: Lcons.camera(color: color_dedede, size: 30),
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ),
                      lDivider(),
                      sb_h_10,
                      Padding(
                        padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                lText(data.myInfoItem.myInfoData.first_name, style: st_b_22(fontWeight: FontWeight.bold)),
                                sb_w_10,
                                lText('${data.myInfoItem.myInfoData.age}${"세".tr()}', style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)),
                                Container(height: 15, child: lDividerVertical(color: color_dedede, thickness: 1, padding: padding_05_LR)),
                                lText(Commons.getGenderToString(data.myInfoItem.myInfoData.gender), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)),
                              ],
                            ),
                            sb_h_10,
                            Row(
                              children: [
                                lIconText(
                                  padding: padding_0,
                                  title: _gradeValue(),
                                  textStyle: st_15(textColor: Commons.getColor(), fontWeight: FontWeight.w500),
                                  icon: Lcons.info(color: color_dedede, size: 22, isEnabled: true),
                                  onClick: () => showRankInfo(),
                                  isExpanded: false,
                                  isLeft: false,
                                ),
                                sb_w_10,
                                likeReview(
                                    color: Commons.getColor(),
                                    data: data.myInfoItem.myInfoData.reviewinfo,
                                    callBack: (action) {
                                      if (action != null && action == DialogAction.review) {
                                        if (storageHelper.user_type == USER_TYPE.link_mom) {
                                          Commons.page(context, routeMypageLinkmomReview, arguments: LinkmomReviewPage());
                                        } else {
                                          Commons.page(context, routeMypageMomdaddyReview, arguments: MomdaddyReviewPage());
                                        }
                                      }
                                    }),
                              ],
                            ),
                          ],
                        ),
                      ),
                      lDivider20(color: color_d4d4d4, thickness: 10.0, padding: padding_15_TB),
                      Padding(
                        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Column(
                          children: [
                            contentView(
                                data: ContentData(
                              content: "닉네임".tr(),
                              contentStyle: st_b_18(fontWeight: FontWeight.bold),
                            )),
                            TextField(
                              controller: _nickCtrl,
                              decoration: underLineDecoration(''),
                              onChanged: (value) => data.myInfoItem.request.nickname = value,
                            ),
                            Column(
                              children: [
                                contentView(
                                    data: ContentData(
                                  content: "나의주소".tr(),
                                  contentStyle: st_b_18(fontWeight: FontWeight.bold),
                                  rightWidget: requiredText(),
                                )),
                                sb_h_10,
                                if (StringUtils.validateString(data.myInfoItem.myInfoData.auth_address!.getAddressName().trim()))
                                  Column(
                                    children: [
                                      InkWell(
                                        onTap: () async {
                                          await Commons.page(context, routeLocation, arguments: LocationPage(data: data));
                                          _requestMyInfoView();
                                        },
                                        child: Row(
                                          children: [
                                            Expanded(child: lText(data.myInfoItem.myInfoData.auth_address!.getAddressName(), style: st_b_16(fontWeight: FontWeight.bold))),
                                            if (data.myInfoItem.isAuth)
                                              Padding(
                                                padding: padding_10_LR,
                                                child: lText("수정".tr(), style: st_b_16(fontWeight: FontWeight.w500, textColor: color_b2b2b2)),
                                              ),
                                          ],
                                        ),
                                      ),
                                      sb_h_15,
                                    ],
                                  ),
                                lBtnOutlineLeftImage(
                                  _authTitle(),
                                  style: st_16(fontWeight: FontWeight.bold, textColor: Commons.getColor()),
                                  bgColor: color_transparent,
                                  borderRadius: 8.0,
                                  imagePath: data.myInfoItem.isAuth ? Lcons.check().name : Lcons.my_location().name,
                                  imageColor: Commons.getColor(),
                                  imageSize: 20,
                                  sideColor: Commons.getColor(),
                                  onClickAction: () async {
                                    if (!data.myInfoItem.isAuth) {
                                      await Commons.page(context, routeLocation, arguments: LocationPage(data: data));
                                      _requestMyInfoView();
                                    }
                                  },
                                ),
                              ],
                            ),
                            sb_h_30,
                            ListAuthTypeSelector(
                              title: "아이가있으신가요?".tr(),
                              data: data.myInfoItem.lsChild,
                              enableColor: Commons.getColor(),
                              onItemClick: (value) {
                                SingleItem item = value as SingleItem;
                                data.myInfoItem.request.numberofchild = item.type;
                                onUpDate();
                              },
                            ),
                            sb_h_30,
                            contentView(
                                data: ContentData(
                              content: "현재나의직업".tr(),
                              contentStyle: st_b_18(fontWeight: FontWeight.bold),
                              rightWidget: requiredText(),
                            )),
                            sb_h_05,
                            CustomDropDown(
                              value: data.myInfoItem.dropValue,
                              itemsList: data.myInfoItem.jobs,
                              icon: Lcons.nav_bottom(color: color_222222),
                              margin: padding_0,
                              style: st_b_18(),
                              onChanged: (value) {
                                data.myInfoItem.dropValue = value;
                                data.myInfoItem.request.job = EnumUtils.getJobType(value).value;
                                onUpDate();
                              },
                            ),
                            lDivider(color: color_dedede),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            // wdDivider(),
            _confirmButton(),
          ],
        ),
      ),
    );
  }

  @override
  void onConfirmBtn() {
    onUpDate();
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn("저장".tr(), btnColor: Commons.getColor(), onClickAction: () {
      if (isConfirmBtn()) {
        _requestMyInfoSave(data.myInfoItem.request);
      }
    });
  }

  bool isConfirmBtn() {
    bool isAddress = false;
    if (data.myInfoItem.isAuth) {
      isAddress = true;
    }
    if (!isAddress) {
      setScrollPosition("동네인증체크_안내_2".tr());
      return false;
    }

    bool isChild = false;
    data.myInfoItem.lsChild.forEach((value) {
      if (value.isSelected) {
        isChild = true;
      }
    });

    if (!isChild) {
      setScrollPosition("아이선택_안내".tr(), isMove: true);
      return false;
    }

    bool isJob = false;
    if (StringUtils.validateString(data.myInfoItem.dropValue) && data.myInfoItem.dropValue != "선택".tr()) {
      isJob = true;
    }
    if (!isJob) {
      setScrollPosition("현재나의직업_안내".tr(), isMove: true);
      return false;
    }

    return true;
  }

  setScrollPosition(String message, {bool isMove = false}) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      showNormalDlg(
          message: message,
          onClickAction: (action) {
            if (isMove) {
              setState(() {
                _controller.animateTo(
                  _controller.position.maxScrollExtent,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.fastOutSlowIn,
                );
              });
            }
          });
    });
  }

  ///나의정보 요청
  _requestMyInfoView() async {
    flag.enableLoading(fn: () => onUpDate());
    await apiHelper.requestMyInfoView().then((response) async {
      if (response.getCode() == KEY_SUCCESS) {
        data.myInfoItem.response = response;
        onDataPage(response);
        _nickCtrl.text = response.getData().nickname;
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }

  ///나의정보 프로필 사진 삭제
  _requestMyInfoPhotoDelete() async {
    flag.enableLoading(fn: () => onUpDate());
    await apiHelper.requestMyInfoPhotoDelete().then((response) async {
      if (response.getCode() == KEY_SUCCESS) {
        data.myInfoItem.request.profileimg = null;
        data.myInfoItem.myInfoData.profileimg = '';
        data.myInfoItem.response.getData().profileimg = "";
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }

  ///나의정보 저장
  _requestMyInfoSave(MyPageMyInfoSaveRequest myPageData) async {
    flag.enableLoading(fn: () => onUpDate());
    await apiHelper.requestMyInfoSave(myPageData).then((response) async {
      if (response.getCode() == KEY_SUCCESS) {
        //동네인증 정보저장 갱신하기
        auth.user.my_info_data = response.getData();
        //2022/04/07 가이드 추가되면서 해당 화면 종료 후 집안환경 체크후 신청서, 지원서로 이동한다.
        Commons.pagePop(context, data: data.myInfoItem.myInfoData);
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }

  @override
  void onDataPage(_data) {
    if (_data == null) {
      return;
    }

    if (_data is MyPageMyInfoResponse) {
      MyPageMyInfoResponse item = _data;
      data.myInfoItem.myInfoData = _data.getData();
      data.myInfoItem.isAuth = _data.getData().auth_address!.is_gpswithaddress;

      data.myInfoItem.lsChild.asMap().forEach((index, value) {
        if (_data.getData().numberofchild == index) {
          value.isSelected = true;
        }
      });
      data.myInfoItem.request.numberofchild = _data.getData().numberofchild;

      ///직업
      data.myInfoItem.dropValue = EnumUtils.getJobTypeValue(_data.getData().job).name;
      data.myInfoItem.request.job = _data.getData().job;

      data.myInfoItem.request.nickname = _data.getData().nickname;

      data.myInfoItem.request.is_user_momdady = StringUtils.getBoolToInt(_data.getData().is_user_momdady);
      data.myInfoItem.request.is_user_linkmom = StringUtils.getBoolToInt(_data.getData().is_user_linkmom);
    }
  }

  _showPhoto() {
    showDlgCamera(
      btn1: "사진선택".tr(),
      btn2: "사진찍기".tr(),
      btn3: "삭제".tr(),
      onClickAction: (action) async {
        if (action == DialogAction.gallery) {
          data.myInfoItem.request.profileimg = await Commons.getImage(ImageSource.gallery);
          if (data.myInfoItem.request.profileimg != null) {
            data.myInfoItem.originalFile = data.myInfoItem.request.profileimg!;
            data.myInfoItem.myInfoData.profileimg = '';
          } else {
            data.myInfoItem.request.profileimg = data.myInfoItem.originalFile;
          }
        } else if (action == DialogAction.camera) {
          data.myInfoItem.request.profileimg = await Commons.getImage(ImageSource.camera);
          if (data.myInfoItem.request.profileimg != null) {
            data.myInfoItem.myInfoData.profileimg = '';
          } else {
            data.myInfoItem.request.profileimg = data.myInfoItem.originalFile;
          }
        } else if (action == DialogAction.delete) {
          if (StringUtils.validateString(data.myInfoItem.response.getData().profileimg)) {
            _requestMyInfoPhotoDelete();
          } else {
            data.myInfoItem.request.profileimg = null;
          }
        }
        onUpDate();
      },
    );
  }

  ///나의 등급
  String _gradeValue() {
    String value = '';
    if (storageHelper.user_type == USER_TYPE.mom_daddy) {
      value = data.myInfoItem.myInfoData.grade_momdady_txt;
    } else {
      value = data.myInfoItem.myInfoData.grade_linkmom_txt;
    }
    return value;
  }

  ///동네 인증 타이틀
  String _authTitle() {
    String value = "동네인증하기".tr();

    ///동네 인증시
    if (data.myInfoItem.myInfoData.auth_address != null && data.myInfoItem.myInfoData.auth_address!.is_gpswithaddress) {
      value = "동네인증_완료".tr();
    } else {
      if (data.myInfoItem.myInfoData.auth_address != null && StringUtils.validateString(data.myInfoItem.myInfoData.auth_address!.getAddressName())) {
        value = "동네인증_인증후".tr();
      } else {
        value = "동네인증하기".tr();
      }
    }
    return value;
  }
}
