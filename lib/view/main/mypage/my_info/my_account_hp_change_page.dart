import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/checkplus_response.dart';
import 'package:linkmom/data/network/models/myaccount_reset_request.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/view/compleat_view_page.dart';
import 'package:linkmom/view/login/checkplus_view_page.dart';

import '../../../../main.dart';

class MyAccountHpChangePage extends BaseStateful {
  final DataManager? data;
  MyAccountHpChangePage({this.data});

  _MyAccountHpChangePageState createState() => _MyAccountHpChangePageState();
}

class _MyAccountHpChangePageState extends BaseStatefulState<MyAccountHpChangePage> {
  @override
  void initState() {
    super.initState();
    super.onData(widget.data ?? DataManager());
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        context: context,
        resizeToAvoidBottomPadding: false,
        appBar: appBar("휴대폰번호변경".tr(), isClose: false, hide: false),
        body: CheckPlusView(
          callback: (response) {
            if (response.mobileno.isNotEmpty) {
              _requestHpAuth(response);
            }
          },
        ));
  }

  void _requestHpAuth(NiceAuthData authData) async {
    try {
      MyAccountResetHpRequest req = MyAccountResetHpRequest(
          hpnum: authData.mobileno, nice_di: authData.dupinfo, telco: authData.mobileco, nationalinfo: authData.nationalinfo);
      apiHelper.requestHpChange(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.page(context, routeCompleteView,
              arguments: CompleteViewPage(data: data, title: "휴대폰번호변경".tr(), resultMessage: response.getData().message));
        } else {
          showNormalDlg(
              message: response.getMsg(),
              onClickAction: (action) {
                Commons.pagePop(context);
              });
        }
      });
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>> $e");
    }
  }
}
