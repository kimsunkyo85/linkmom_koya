import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/myaccount_reset_request.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/compleat_view_page.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/view_modules.dart';

import '../../../../main.dart';

class MyAccountPwPage extends BaseStateful {
  final DataManager? data;
  MyAccountPwPage({this.data});

  _MyAccountPwPageState createState() => _MyAccountPwPageState();
}

class _MyAccountPwPageState extends BaseStatefulState<MyAccountPwPage> {
  FocusNode _pw1Focus = FocusNode();
  TextEditingController _ctl = TextEditingController();
  String? _validateMessage = '';
  String? _validateCurrentMessage = '';
  bool _confirmResult = true;
  bool _togglePw = true;
  GlobalKey<FormState> _currentPw = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    super.onData(widget.data ?? DataManager());
    data.tcPassword.clear();
    data.tcPasswordConfirm.clear();
    data.formPw = GlobalKey<FormState>();
    data.formPw2 = GlobalKey<FormState>();
    _pw1Focus.addListener(() {
      if (this.mounted) {
        setState(() => {});
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    data.tcPassword.clear();
    data.tcPasswordConfirm.clear();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
          context: context,
          resizeToAvoidBottomInset: false,
          appBar: appBar("비밀번호변경".tr(), isClose: false, hide: false),
          body: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
            Expanded(
                child: Container(
                    padding: padding_20,
                    child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Container(
                          margin: padding_20_T,
                          alignment: Alignment.centerLeft,
                          child: lText(
                            "현재비밀번호".tr(),
                            style: stAgrTitle,
                          )),
                      Form(
                          key: _currentPw,
                          child: TextFormField(
                            focusNode: _pw1Focus,
                            controller: _ctl,
                            obscureText: _togglePw,
                            style: stLogin,
                            cursorColor: color_00cfb5,
                            textInputAction: TextInputAction.next,
                            keyboardType: TextInputType.text,
                            maxLength: maxPasswordLength,
                            validator: _validateCurrentPw,
                            decoration: underlineDecorationMultiIcon("현재사용중인비밀번호를".tr(),
                                focusColor: color_545454,
                                conterText: ' ',
                                icons: [
                                  if (_pw1Focus.hasFocus) lInkWell(child: Lcons.clear(), onTap: () => _ctl.clear()),
                                  if (flag.isConfirm) Lcons.check(color: color_545454, isEnabled: true),
                                  sb_w_10,
                                  lInkWell(
                                      child: Lcons.visibility(isEnabled: _togglePw),
                                      onTap: () {
                                        _togglePw = !_togglePw;
                                        onUpDate();
                                      })
                                ],
                                focus: _pw1Focus.hasFocus),
                            onSaved: (val) => _ctl.text = val!,
                            onEditingComplete: () {
                              _currentPw.currentState!.validate();
                              focusNextTextField(context);
                              onUpDate();
                            },
                            onChanged: (value) {
                              _currentPw.currentState!.validate();
                              onUpDate();
                            },
                          )),
                      Container(margin: padding_20_T, alignment: Alignment.centerLeft, child: lText("새비밀번호".tr(), style: stAgrTitle)),
                      PasswordField(
                          data: data,
                          flag: flag,
                          validate: _validatePw,
                          hint: "신규비밀번호를".tr(),
                          onEditingComplete: (value) {
                            _validatePwConfirm(value, data.tcPasswordConfirm.text);
                          },
                          onChanged: (value) {
                            _validatePwConfirm(value, data.tcPasswordConfirm.text);
                            onUpDate();
                          },
                          focusedColor: color_545454),
                      PasswordConfirmField(
                          data: data,
                          flag: flag,
                          hint: "신규비밀번호를재".tr(),
                          onEditingComplete: (value) {
                            _validatePwConfirm(data.tcPassword.text, value);
                            focusClear(context);
                          },
                          onChanged: (value) {
                            _validatePwConfirm(data.tcPassword.text, value);
                            onUpDate();
                          },
                          focusedColor: color_545454),
                    ]))),
            lBtn("확인".tr(), isEnabled: flag.isConfirm, onClickAction: () {
              _requestPwConfirm();
            }, btnColor: color_545454),
          ])),
    );
  }

  void _validatePwConfirm(String pw, String pwConfirm) {
    if (StringUtils.validatePw(pw) == null && StringUtils.validatePwConfirm(pw, pwConfirm) == null) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  String? _validatePw(String? pw) {
    if (_confirmResult) {
      return StringUtils.validatePw(pw);
    } else {
      return _validateMessage;
    }
  }

  String? _validateCurrentPw(String? pw) {
    if (_confirmResult) {
      return StringUtils.validatePw(pw);
    } else {
      return _validateCurrentMessage;
    }
  }

  void _requestPwConfirm() async {
    try {
      flag.enableLoading(fn: () => onUpDate());
      MyAccountResetPwRequest req = MyAccountResetPwRequest(old_pwd: _ctl.text, pwd1: data.tcPassword.text, pwd2: data.tcPasswordConfirm.text);
      apiHelper.requestMyaccountResetPw(req).then((response) {
        flag.disableLoading(fn: () => onUpDate());
        if (response.getCode() == KEY_SUCCESS) {
          _confirmResult = true;
          Commons.page(context, routeCompleteView,
              arguments: CompleteViewPage(
                title: "비밀번호변경".tr(),
                resultMessage: response.getData().message,
                finishAction: () => Commons.pageToMain(context, routeMyInfoAccount),
              ));
        } else {
          if (response.getData().old_pwd.isNotEmpty) {
            _validateMessage = '';
            _validateCurrentMessage = response.getData().old_pwd;
          }
          if (response.getData().pwd1.isNotEmpty) {
            _validateMessage = response.getData().pwd1;
            _validateCurrentMessage = '';
          }
          _confirmResult = false;
          data.formPw.currentState!.validate();
          _currentPw.currentState!.validate();
        }
        onUpDate();
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  bool _isShowing(bool state, String pw1, String pw2, Function onUpDate, ViewFlag flag) {
    if (!state && pw1.isNotEmpty) {
      return _passwordConfirmCheck(pw1, pw2, onUpDate, flag);
    }
    return false;
  }

  bool _passwordConfirmCheck(String pw1, String pw2, Function onUpDate, ViewFlag flag) {
    if (pw1 == pw2) {
      flag.enablePasswordConfirm(fn: () => onUpDate());
      return true;
    } else {
      flag.disablePasswordConfirm(fn: () => onUpDate());
      return false;
    }
  }
}
