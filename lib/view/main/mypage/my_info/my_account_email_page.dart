import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/myaccount_reset_request.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/lcons.dart';

import '../../../../main.dart';

class MyAccountEmailPage extends BaseStateful {
  final DataManager? data;

  MyAccountEmailPage({this.data});

  MyAccountEmailPageState createState() => MyAccountEmailPageState();
}

class MyAccountEmailPageState extends BaseStatefulState<MyAccountEmailPage> {
  FocusNode _emailFocus = FocusNode();
  String _validateMessage = '';
  bool _isValid = false;

  @override
  void initState() {
    super.initState();
    super.onData(widget.data ?? DataManager());
    if (auth.account.email.isEmpty) {
      data.tcEmailEmail.text = auth.user.username;
    } else {
      data.tcEmailEmail.text = auth.account.email;
    }
    _setValidMessage();
    _isValid = auth.account.is_auth_email;
    _requestAccount();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
          context: context,
          resizeToAvoidBottomPadding: false,
          appBar: appBar("이메일인증_변경".tr(), isClose: false, hide: false, onBack: () {
            Commons.pagePop(context);
          }),
          body: LinkmomRefresh(
              onRefresh: () => _requestAccount(),
              child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
                Container(
                    margin: padding_20_T,
                    alignment: Alignment.centerLeft,
                    child: lText(
                      "이메일인증_이메일주소".tr(),
                      style: stAgrTitle,
                    )),
                Container(
                    alignment: Alignment.topLeft,
                    padding: padding_20_TB,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        lText(
                          "이메일인증_안내".tr(),
                          overflow: TextOverflow.visible,
                          style: st_b_13(),
                        ),
                        lText(
                          "이메일인증_안내_스팸".tr(),
                          style: st_b_13(),
                          overflow: TextOverflow.visible,
                        ),
                      ],
                    )),
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Row(children: [
                    Flexible(
                        child: Form(
                            key: data.formEmail,
                            child: Focus(
                              onFocusChange: (value) {
                                if (!value) {
                                  _validateEmail(data.tcEmailEmail.text);
                                }
                                onUpDate();
                              },
                              focusNode: _emailFocus,
                              child: TextFormField(
                                controller: data.tcEmailEmail,
                                textInputAction: TextInputAction.done,
                                onSaved: (value) => data.tcEmailEmail.text = value!,
                                inputFormatters: [StringUtils.whiteSpaceFormat()],
                                decoration: underLineDecoration('', icon: Lcons.clear(), isFn: true, fn: () {
                                  data.tcEmailEmail.clear();
                                }, focus: _emailFocus.hasFocus, focusColor: color_222222, errorStyle: st_b_12(textColor: color_error), counterText: ''),
                                onChanged: (value) {
                                  _isValid = false;
                                  _validateEmail(value);
                                  onUpDate();
                                },
                                onEditingComplete: () {
                                  _validateEmail(data.tcEmailEmail.text);
                                  focusClear(context);
                                },
                              ),
                            ))),
                    lBtnWrapOutLine(_isValid ? "재전송".tr() : "이메일인증_메일전송".tr(), isEnabled: _confirmAuthBtn(), textStyle: st_b_14(fontWeight: FontWeight.w500), onClickAction: () => _requestEmailAuth()),
                  ]),
                  Padding(
                    padding: padding_10_TB,
                    child: lText(_validateMessage, textAlign: TextAlign.start, style: _isValid ? st_12(textColor: color_main) : stErrorText),
                  ),
                ]),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.fromLTRB(25, 20, 25, 20),
                  color: color_eeeeee.withOpacity(0.32),
                  child: lText("이메일인증_내용".tr(), overflow: TextOverflow.visible, style: st_12(textColor: color_999999)),
                )
              ]))),
    );
  }

  bool _confirmAuthBtn() {
    if (data.tcEmailEmail.text.isNotEmpty) {
      return true;
    }
    return false;
  }

  void _validateEmail(String value) {
    if (StringUtils.validateEmail(value) != null) {
      _validateMessage = "실사용이메일입력".tr();
    }
    onUpDate();
  }

  void _requestEmailAuth() async {
    try {
      MyAccountResetEmailRequest req = MyAccountResetEmailRequest(email: data.tcEmailEmail.text);
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestEmailReset(req).then((response) {
        _isValid = response.getCode() == KEY_SUCCESS;
        if (response.getCode() == KEY_SUCCESS) {
          auth.account.email = req.email;
          auth.account.is_auth_email_send = true;
          _showSuccessDialog(response.getData().message, auth.account.is_auth_email);
        } else {
          if (response.getData().message.isNotEmpty) {
            _validateMessage = response.getData().message;
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _showSuccessDialog(String message, bool isSucced) {
    showNormalDlg(
        context: context,
        message: message,
        color: color_545454,
        onClickAction: (action) {
          switch (action) {
            case DialogAction.yes:
              Commons.pagePop(context, data: isSucced);
              break;
            default:
          }
        });
  }

  void _requestAccount() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestAccount().then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          auth.account = response.getData();
          _isValid = auth.account.is_auth_email;
          auth.indexInfo.userinfo.is_auth_email = _isValid;
        } else {
          _isValid = false;
        }
        _setValidMessage();
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _setValidMessage() {
    if (auth.account.is_auth_email) {
      _validateMessage = "이메일인증_인증".tr();
    } else {
      _validateMessage = "이메일인증_미인증".tr();
    }
  }
}
