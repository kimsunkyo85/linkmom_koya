import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/my_info/my_account_email_page.dart';
import 'package:linkmom/view/main/mypage/my_info/my_account_hp_page.dart';
import 'package:linkmom/view/main/mypage/my_info/my_account_pw_page.dart';

class AccountManagerPage extends BaseStateful {
  final DataManager? data;
  AccountManagerPage({this.data});

  @override
  _MyAccountManagerPageState createState() => _MyAccountManagerPageState();
}

class _MyAccountManagerPageState extends BaseStatefulState<AccountManagerPage> {
  @override
  void initState() {
    super.initState();
    onData(widget.data ?? DataManager());
    _requestAccount();
  }

  @override
  void onData(DataManager _data) {
    data = _data;
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        appBar: appBar("계정관리".tr(), isBack: true, isClose: false, hide: false, onBack: () => Commons.pagePop(context)),
        body: LinkmomRefresh(
            onRefresh: () => _requestAccount(),
            child: Column(children: [
              Container(
                  decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_dedede))),
                  padding: padding_20_TB,
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    Flexible(flex: 1, child: lText("아이디이메일".tr(), style: st_b_14(textColor: color_b2b2b2))),
                    Flexible(
                        flex: 2,
                        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                          lText(auth.account.username, style: st_b_16(fontWeight: FontWeight.w700)),
                        ]))
                  ])),
              Container(
                  decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_dedede))),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    Flexible(flex: 1, child: lText("이메일인증_변경".tr(), style: st_b_14(textColor: color_b2b2b2))),
                    Flexible(
                        flex: 2,
                        child: lInkWell(
                            child: Container(
                                padding: padding_20_TB,
                                child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                  lText(auth.account.email, style: st_b_14(textColor: auth.account.is_auth_email ? color_545454 : color_cecece)),
                                  Lcons.nav_right(size: 15),
                                ])),
                            onTap: () async {
                              var ret = await Commons.page(context, routeMyAccountEmail, arguments: MyAccountEmailPage(data: data));
                              if (ret != null) _requestAccount();
                            })),
                  ])),
              Container(
                  decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_dedede))),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    Flexible(flex: 1, child: lText("비밀번호변경".tr(), style: st_b_14(textColor: color_b2b2b2))),
                    Flexible(
                        flex: 2,
                        child: lInkWell(
                            child: Container(padding: padding_20_TB, child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [Lcons.nav_right(size: 15)])),
                            onTap: () {
                              Commons.page(context, routeMyAccountPw, arguments: MyAccountPwPage(data: data));
                            })),
                  ])),
              Container(
                  decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_dedede))),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    Flexible(flex: 1, child: lText("휴대폰번호_변경".tr(), style: st_b_14(textColor: color_b2b2b2))),
                    Flexible(
                        flex: 2,
                        child: lInkWell(
                            child: Container(padding: padding_20_TB, child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [Lcons.nav_right(size: 15)])),
                            onTap: () {
                              Commons.page(context, routeMyAccountHp, arguments: MyAccountHpPage(data: data));
                            })),
                  ])),
            ])));
  }

  void _requestAccount() {
    flag.enableLoading(fn: () => onUpDate());
    apiHelper.requestAccount().then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        auth.account = response.getData();
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }
}
