import 'dart:math';

import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/care_list_response.dart';
import 'package:linkmom/data/network/models/cares_wish_requests.dart';
import 'package:linkmom/data/network/models/community_scrap_request.dart';
import 'package:linkmom/data/providers/community_scarp_provider.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/dropdown/custom_dropdown.dart';
import 'package:linkmom/utils/listview/list_momdady_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/home_view.dart';
import 'package:linkmom/view/main/job_apply/job_profile/momdady_profile_page.dart';
import 'package:provider/provider.dart';

import '../../../../route_name.dart';
import 'favorite_view.dart';

class LinkmomFavoritePage extends BaseStateful {
  final DataManager? data;
  LinkmomFavoritePage({this.data});

  @override
  _MypageFavoritePageState createState() => _MypageFavoritePageState();
}

class _MypageFavoritePageState extends BaseStatefulState<LinkmomFavoritePage> with SingleTickerProviderStateMixin {
  CommunityScrapProvider get _provider => context.read<CommunityScrapProvider>();
  String _selected = "전체보기".tr();
  List<Category> _category = [];
  CareListData _dataList = CareListData(results: []);
  CareListData _viewDataList = CareListData(results: []);
  CareListData _recentDataList = CareListData(results: []);
  CareListData _overDataList = CareListData(results: []);
  final List<String> _itemList = ["전체보기".tr(), "찜한목록".tr(), "지난돌봄목록".tr()];
  var _setState;

  @override
  void initState() {
    super.initState();
    data = widget.data!;
    flag.enableLoading(fn: () => onUpDate());
    _getCaresList();
    _provider.addListener(_setState);
    _provider.fetch(isScrap: '1');
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  @override
  void dispose() {
    _provider.removeListener(_setState);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar("관심목록".tr()),
            body: FavoriteView(
              type: storageHelper.user_type,
              userCount: _viewDataList.count,
              infoCount: _provider.count,
              infoList: _provider.getDatas
                  .map<Widget>((e) => Container(
                        padding: _provider.getDatas.first == e ? padding_20_LTR : padding_20_LR,
                        child: CommunityBoard(
                          data: e,
                          isMark: true,
                          onClickMark: (id, isScrap) => _requestBookmark(id, !isScrap),
                        ),
                      ))
                  .toList(),
              infoNext: _provider.hasNext,
              infoRefresh: (isNext) => isNext ? _provider.next() : _provider.fetch(isScrap: '1'),
              infoSubCategory: Container(
                width: double.infinity,
                color: color_eeeeee,
                child: lScrollView(
                    padding: padding_10,
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: Category.values
                          .map(
                            (category) => Container(
                              margin: padding_10_R,
                              child: lTextBtn(
                                category.name,
                                bg: Colors.white,
                                border: Border.all(color: _category.contains(category) ? Commons.getColor() : Colors.transparent),
                                style: st_13(textColor: _category.contains(category) ? Commons.getColor() : color_999999, fontWeight: FontWeight.w500),
                                onClickEvent: () {
                                  if (_category.contains(category)) {
                                    _category.remove(category);
                                  } else {
                                    _category.add(category);
                                  }
                                },
                              ),
                            ),
                          )
                          .toList(),
                    )),
              ),
              userNext: _viewDataList.next.isNotEmpty,
              userDropdown: CustomDropDown(
                  margin: padding_0,
                  isExpanded: false,
                  bgColor: Colors.transparent,
                  icon: Transform.rotate(angle: 90 * pi / 180, child: Icon(Icons.sync_alt_sharp, size: 16)),
                  value: _selected,
                  style: st_b_13(),
                  itemsList: _itemList,
                  onChanged: (value) {
                    _selected = value;
                    _changeDropdown(value);
                  }),
              userList: (_viewDataList.results ?? [])
                  .map((momdaddy) => Container(
                        // padding: padding_10_T,
                        padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.04),
                            offset: Offset(0, 4),
                            blurRadius: 12.0,
                          )
                        ]),
                        child: Stack(
                          children: [
                            MomDadyView(
                              momdaddy,
                              padding: padding_20,
                              borderRadius: 16,
                              elevation: 0.5,
                              bgColor: color_white,
                              viewMode: data.listViewItem.viewMode,
                              borderColor: Colors.white,
                              onItemClick: (item, action) {
                                log.d('『GGUMBI』>>> build : action: $item, type: $action, <<< ');
                                if (item is CareListResultsData) {
                                  if (action == DialogAction.follower) {
                                    _requestFollow(!item.userinfo!.is_follower, item.booking_id);
                                  } else if (action == DialogAction.select) {
                                    Commons.page(context, routeMomDadyProfile,
                                        arguments: MomDadyProfilePage(
                                          viewMode: ViewMode(viewType: ViewType.view),
                                          bookingId: item.booking_id,
                                        ));
                                  }
                                }
                              },
                            ),
                          ],
                        ),
                      ))
                  .toList(),
              userRefresh: (isNext) => _getCaresList(isNext: isNext),
            )));
  }

  Future<void> _getCaresList({bool isNext = false}) async {
    try {
      if (isNext && _viewDataList.next.isEmpty) return;
      apiHelper.requestCaresWishList(next: _viewDataList.next).then((response) {
        _setListData(response.getData(), isNext);
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _setListData(CareListData list, bool isNext) {
    _dataList = list;
    if (_viewDataList.results == null) _viewDataList.results = [];
    if (list.results != null && list.results!.isNotEmpty) {
      _dataList.results!.forEach((CareListResultsData element) {
        element.caredate.sort();
        if (element.caredate.where((element) => StringUtils.parseYMD(element).isBefore(DateTime.now())).isNotEmpty) {
          _overDataList.results!.add(element);
        }
      });
      _dataList.results!.forEach((CareListResultsData element) {
        if (element.caredate.where((element) => StringUtils.parseYMD(element).isAfter(DateTime.now()) || StringUtils.parseYMD(element).isAtSameMomentAs(DateTime.now())).isNotEmpty) {
          _recentDataList.results!.add(element);
        }
      });
    } else {
      _overDataList = CareListData(results: []);
      _recentDataList = CareListData(results: []);
    }
    if (isNext) {
      _viewDataList.results!.addAll(_dataList.results ?? []);
      _viewDataList.next = _dataList.next;
    } else {
      _viewDataList = _dataList;
    }
  }

  void _changeDropdown(String value) {
    if (value == "전체보기".tr()) {
      _viewDataList = _dataList;
    } else if (value == "찜한목록".tr()) {
      _viewDataList = _recentDataList;
    } else if (value == "지난돌봄목록".tr()) {
      _viewDataList = _overDataList;
    } else {
      _viewDataList = _dataList;
    }
    onUpDate();
  }

  void _requestBookmark(int id, bool isScrap) {
    try {
      CommnunityScrapRequest request = CommnunityScrapRequest(shareId: id);
      if (isScrap) {
        apiHelper.reqeustCommunityScrapDelete(request).then((response) {
          if (response.getCode() != KEY_SUCCESS) {
            isScrap = !isScrap;
          }
        }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      } else {
        apiHelper.reqeustCommunityScrap(request).then((response) {
          if (response.getCode() != KEY_SUCCESS) {
            isScrap = !isScrap;
          }
        });
      }
      flag.disableLoading(fn: () => onUpDate());
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
  }

  void _requestDeleteCares(CareListResultsData item) {
    try {
      CaresWishDeleteRequest req = CaresWishDeleteRequest(bookingcare: item.booking_id);
      apiHelper.requestCaresWishDelete(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) item.userinfo!.is_follower = false;
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }

  void _requestFollow(bool isFollow, int id) {
    if (isFollow) {
      apiHelper.requestCaresWishSave(CaresWishSaveRequest(bookingcare: id)).then((response) {
        flag.disableLoading();
        if (response.getCode() == KEY_SUCCESS) {
          isFollow = true;
        }
        onUpDate();
      });
    } else {
      apiHelper.requestCaresWishDelete(CaresWishDeleteRequest(bookingcare: id)).then((response) {
        flag.disableLoading();
        if (response.getCode() == KEY_SUCCESS) {
          isFollow = false;
        }
        onUpDate();
      });
    }
  }
}
