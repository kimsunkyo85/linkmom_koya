import 'dart:math';

import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/community_scrap_request.dart';
import 'package:linkmom/data/network/models/linkmom_list_response.dart';
import 'package:linkmom/data/network/models/linkmom_wish_list_requests.dart';
import 'package:linkmom/data/providers/community_scarp_provider.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/dropdown/custom_dropdown.dart';
import 'package:linkmom/utils/listview/list_linkmom_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/home_view.dart';
import 'package:linkmom/view/main/job_apply/job_profile/linkmom_profile_page.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/view/main/mypage/review/review_view_page.dart';
import 'package:provider/provider.dart';

import '../../../../route_name.dart';
import 'favorite_view.dart';

class MomdaddyFavoritePage extends BaseStateful {
  final DataManager? data;
  MomdaddyFavoritePage({this.data});

  @override
  _MypageFavoritePageState createState() => _MypageFavoritePageState();
}

class _MypageFavoritePageState extends BaseStatefulState<MomdaddyFavoritePage> with SingleTickerProviderStateMixin {
  CommunityScrapProvider get _provider => context.read<CommunityScrapProvider>();
  ScrollController? _controller;
  String _selected = "전체보기".tr();
  List<Category> _category = [];
  LinkMomListData _dataList = LinkMomListData(results: []);
  LinkMomListData _viewDataList = LinkMomListData(results: []);
  LinkMomListData _recentDataList = LinkMomListData(results: []);
  LinkMomListData _overDataList = LinkMomListData(results: []);

  @override
  void initState() {
    super.initState();
    data = widget.data!;
    flag.enableLoading(fn: () => onUpDate());
    _getLinkmomList();
    _provider.addListener(() {
      if (this.mounted) setState(() {});
    });
    _provider.fetch(isScrap: '1');
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar("관심목록".tr()),
            body: FavoriteView(
              userCount: _viewDataList.count,
              infoList: _provider.getDatas
                  .map<Widget>((e) => Container(
                        padding: _provider.getDatas.first == e ? padding_20_LTR : padding_20_LR,
                        child: CommunityBoard(
                          isMark: true,
                          onClickMark: (id, isScarp) => _requestBookmark(id, !isScarp),
                          data: e,
                        ),
                      ))
                  .toList(),
              infoRefresh: (isNext) => isNext ? _provider.next() : _provider.fetch(isScrap: '1'),
              infoNext: _provider.hasNext,
              infoSubCategory: Container(
                width: double.infinity,
                color: color_eeeeee,
                child: lScrollView(
                    padding: padding_10,
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: Category.values
                          .map(
                            (category) => Container(
                              margin: padding_10_R,
                              child: lTextBtn(category.name,
                                  bg: Colors.white,
                                  border: Border.all(color: _category.contains(category) ? Commons.getColor() : Colors.transparent),
                                  style: st_13(textColor: _category.contains(category) ? Commons.getColor() : color_999999, fontWeight: FontWeight.w500), onClickEvent: () {
                                if (_category.contains(category)) {
                                  _category.remove(category);
                                } else {
                                  _category.add(category);
                                }
                              }),
                            ),
                          )
                          .toList(),
                    )),
              ),
              infoCount: _provider.count,
              userNext: _viewDataList.next.isNotEmpty,
              userDropdown: CustomDropDown(
                margin: padding_0,
                isExpanded: false,
                bgColor: Colors.transparent,
                icon: Transform.rotate(angle: 90 * pi / 180, child: Icon(Icons.sync_alt_sharp, size: 16)),
                value: _selected,
                style: st_b_13(),
                itemsList: ["전체보기".tr(), "찜한목록".tr(), "지난돌봄목록".tr()],
                onChanged: (value) {
                  _selected = value;
                  _changeDropdown(value);
                },
              ),
              userList: _viewDataList.results!
                  .map((linkssam) => Container(
                        margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
                        decoration: BoxDecoration(boxShadow: [BoxShadow(color: Colors.black.withAlpha(11), offset: Offset(0, 4), blurRadius: 12.0)]),
                        child: LinkMomView(
                          linkssam,
                          viewMode: ViewMode(viewType: ViewType.view),
                          padding: padding_20,
                          lsAuthInfo: ListLinkMomViewState().getAuthInfo(linkssam),
                          viewType: data.listViewItem.viewMode,
                          onItemClick: (item, action) {
                            log.d('『GGUMBI』>>> build : action: $item, type: $action, <<< ');
                            if (item is LinkMomListResultsData) {
                              log.d('『GGUMBI』>>> build : item.job_id: ${item.job_id},  <<< ');
                              if (action == DialogAction.follower) {
                                _requestFollow(!item.userinfo!.is_follower, item.job_id);
                              } else if (action == DialogAction.select) {
                                Commons.page(context, routeLinkMomProfile,
                                    arguments: LinkMomProfilePage(
                                      viewMode: ViewMode(viewType: ViewType.view),
                                      jobId: item.job_id,
                                    ));
                              } else if (action == DialogAction.review) {
                                Commons.page(context, routeReviewView, arguments: ReviewViewPage(userId: item.userinfo!.id, gubun: USER_TYPE.link_mom.index + 1));
                              }
                            }
                          },
                        ),
                      ))
                  .toList(),
              userRefresh: (isNext) => _getLinkmomList(isNext: isNext),
            )));
  }

  Future<void> _getLinkmomList({bool isNext = false}) async {
    try {
      if (isNext && _viewDataList.next.isEmpty) return;
      apiHelper.requestLinkmomWishList(next: _viewDataList.next).then((responseData) {
        if (responseData.getCode() == KEY_SUCCESS) {
          _dataList = responseData.getData();
          _setListData(isNext: isNext);
          onUpDate();
        }
      });
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
  }

  void _setListData({bool isNext = false}) {
    if (_dataList.results == null) {
      _viewDataList = LinkMomListData(results: []);
    } else {
      _dataList.results!.forEach((LinkMomListResultsData element) {
        if (StringUtils.parseYMD(element.caredate_max).isBefore(DateTime.now())) {
          _overDataList.results!.add(element);
        }
      });
      _dataList.results!.forEach((LinkMomListResultsData element) {
        if (StringUtils.parseYMD(element.caredate_max).isAfter(DateTime.now()) || StringUtils.parseYMD(element.caredate_max).isAtSameMomentAs(DateTime.now())) {
          _recentDataList.results!.add(element);
        }
      });
    }
    if (isNext) {
      _viewDataList.results!.addAll(_dataList.results ?? []);
      _viewDataList.next = _dataList.next;
    } else {
      _viewDataList = _dataList;
      _viewDataList.count = _dataList.count;
    }
  }

  void _changeDropdown(String value) {
    if (value == "전체보기".tr()) {
      _viewDataList = _dataList;
    } else if (value == "찜한목록".tr()) {
      _viewDataList = _recentDataList;
    } else if (value == "지난돌봄목록".tr()) {
      _viewDataList = _overDataList;
    } else {
      _viewDataList = _dataList;
    }
    onUpDate();
  }

  void _requestBookmark(int id, bool isScrap) {
    try {
      CommnunityScrapRequest request = CommnunityScrapRequest(shareId: id);
      if (isScrap) {
        apiHelper.reqeustCommunityScrapDelete(request).then((response) {
          if (response.getCode() != KEY_SUCCESS) {
            isScrap = !isScrap;
          }
        }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      } else {
        apiHelper.reqeustCommunityScrap(request).then((response) {
          if (response.getCode() != KEY_SUCCESS) {
            isScrap = !isScrap;
          }
        });
      }
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
  }

  void _requestFollow(bool isFollow, int id) {
    if (isFollow) {
      apiHelper.requestLinkmomWishSave(LinkmomWishSaveRequest(bookingjobs: id)).then((response) {
        onUpDate();
      });
    } else {
      apiHelper.requestLinkmomWishDelete(LinkmomWishDeleteRequest(bookingjobs: id)).then((response) {
        onUpDate();
      });
    }
  }
}
