import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/care_list_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/listview/list_momdady_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

class FavoriteCareListView extends ListMomDadyView {
  FavoriteCareListView({
    this.data,
    this.padding = padding_15_LR,
    this.borderRadius = 16,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.enableColor = color_545454,
    this.disableColor = color_999999,
    this.onItemClick,
    this.isOverlap = false,
    this.imagePath = '',
    this.isRight = true,
    this.viewMode = ViewType.normal,
    this.isLoading = false,
    this.controller,
  });

  final dynamic data;
  final Function? onItemClick;
  final padding;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color enableColor;
  final Color disableColor;
  final bool isOverlap;
  final String imagePath;
  final bool isRight;
  final ViewType viewMode;
  final isLoading;
  final ScrollController? controller;

  FavoriteViewState createState() => FavoriteViewState();
}

class FavoriteViewState extends ListMomDadyViewState {
  List<CareListResultsData> _data = [];

  var padding;
  late double borderRadius;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;
  late String imagePath;
  late bool isRight;
  late ScrollController? controller;
  late bool isLoading;
  late ViewType viewMode;

  @override
  Widget build(BuildContext context) {
    _data = widget.data ?? [];
    isLoading = widget.isLoading;
    viewMode = widget.viewMode;
    return _data.isEmpty
        ? isLoading
            ? LoadingIndicator(background: Colors.transparent)
            : lEmptyView(height: Commons.height(context, 0.5))
        : lScrollView(
            padding: padding_0,
            child: Column(
              children: _data
                  .map(
                    (e) => _buildProfile(_data.indexOf(e), e),
                  )
                  .toList(),
            ),
          );
  }

  @override
  onItemClick(int index, DialogAction action) {
    widget.onItemClick!(_data[index], action);
  }

  bool _isAfter(List<String> date) {
    return date.where((element) => StringUtils.parseYMD(element).isAfter(DateTime.now())).isEmpty;
  }

  Widget _buildProfile(int index, CareListResultsData e) {
    return Container(
        margin: padding_10_B,
        decoration: BoxDecoration(boxShadow: [BoxShadow(color: Colors.black.withAlpha(11), offset: Offset(0, 4), blurRadius: 12.0)]),
        child: Stack(
          children: [
            InkWell(
              onTap: () => onItemClick(index, DialogAction.select),
              child: MomDadyView(
                e,
                padding: padding,
                borderRadius: borderRadius,
                fontWeight: fontWeight,
                bgColor: bgColor,
                enableColor: enableColor,
                disableColor: disableColor,
                viewMode: viewMode,
                onItemClick: (item, action) {
                  log.d('『GGUMBI』>>> build : action: $action, index: ${_data.indexOf(e)}, <<< ');
                  onItemClick(_data.indexOf(e), action);
                },
              ),
            ),
            if (_isAfter(e.caredate))
              Positioned.fill(
                child: Container(
                  margin: padding_10_B,
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.6),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  alignment: Alignment.center,
                  child: lText("날짜가지난돌봄".tr(), style: st_16(fontWeight: FontWeight.w500)),
                ),
              ),
          ],
        ));
  }
}
