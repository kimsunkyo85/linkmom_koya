import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

class FavoriteView extends StatefulWidget {
  final USER_TYPE type;
  final List<Widget> userList;
  final Function userRefresh;
  final Widget userDropdown;
  final Function infoRefresh;
  final Widget infoSubCategory;
  final List<Widget> infoList;
  final int userCount;
  final int infoCount;
  final bool infoNext, userNext;

  FavoriteView({
    Key? key,
    this.type = USER_TYPE.mom_daddy,
    required this.userList,
    required this.userRefresh,
    required this.userDropdown,
    required this.infoRefresh,
    required this.infoSubCategory,
    required this.infoList,
    this.userCount = 0,
    this.infoCount = 0,
    this.infoNext = false,
    this.userNext = false,
  });

  @override
  _FavoriteViewState createState() => _FavoriteViewState();
}

class _FavoriteViewState extends State<FavoriteView> {
  @override
  Widget build(BuildContext context) {
    Size size = Size(MediaQuery.of(context).size.width, MediaQuery.of(context).size.height * 0.65);
    return TabPageView(
      tabbarName: [
        TabItem(title: '${widget.type == USER_TYPE.mom_daddy ? "찜한링크쌤".tr() : "찜한맘대디".tr()} (${widget.userCount})'),
        TabItem(title: '${"찜한정보".tr()} (${widget.infoCount})'),
      ],
      tabbarView: [
        Container(
          color: color_fafafa,
          child: SliverNextListView(
            pinned: Container(
              color: color_fafafa,
              padding: padding_20_LR,
              child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [widget.userDropdown]),
            ),
            view: widget.userList,
            onRefresh: () => widget.userRefresh(false),
            onNext: () => widget.userRefresh(true),
            showNextLoading: widget.userNext,
          ),
        ),
        SliverNextListView(
          pinned: widget.infoSubCategory,
          view: widget.infoList,
          onRefresh: () => widget.infoRefresh(false),
          onNext: () => widget.infoRefresh(true),
          showNextLoading: widget.infoNext,
        ),
      ],
    );
  }
}
