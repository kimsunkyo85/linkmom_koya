import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/carediary_claim_view_request.dart';
import 'package:linkmom/data/network/models/carediary_payment_request.dart';
import 'package:linkmom/data/network/models/carediary_payment_response.dart';
import 'package:linkmom/data/network/models/data/care_diary_claim_data.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_claim_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/recipe_view.dart';
import 'package:linkmom/view/main/mypage/review/review_save_page.dart';
import 'package:linkmom/view/main/payment/payment_refund_rule_page.dart';

class RecipeViewPage extends BaseStateful {
  final String appbarTitle;
  final int id;
  final USER_TYPE type;
  final PayStatus status;
  final int claimId;
  final int matchingId;
  final bool isLast;
  final bool isSignOut;
  RecipeViewPage({
    this.appbarTitle = ' ',
    this.type = USER_TYPE.mom_daddy,
    this.status = PayStatus.ready,
    this.id = 0,
    this.claimId = 0,
    this.matchingId = 0,
    this.isLast = false,
    this.isSignOut = false,
  });

  @override
  _RecipeViewState createState() => _RecipeViewState();
}

class _RecipeViewState extends BaseStatefulState<RecipeViewPage> {
  DiaryPaymentData _paymentInfo = DiaryPaymentData();
  CareDiaryClaimData _claim = CareDiaryClaimData();

  @override
  void initState() {
    super.initState();
    _requestPaymentData(widget.id);
    _requestClaimView(widget.claimId);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Commons.pagePop(context, data: widget.status == PayStatus.ready ? true : null);
        return Future.value(false);
      },
      child: lModalProgressHUD(
          flag.isLoading,
          lScaffold(
              appBar: diaryAppbar(
                  title: _paymentInfo.payStatusText,
                  hide: false,
                  rightW: _claim.answer.isNotEmpty
                      ? lInkWell(
                          onTap: () => Commons.page(context, routePayMentRefundRule, arguments: PaymentRefundRulePage()),
                          child: Container(
                            child: lText("환불규정".tr(), style: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
                            padding: padding_20,
                          ))
                      : Container()),
              body: Container(
                child: Column(
                  children: [
                    lRoundContainer(
                      padding: padding_16,
                      margin: padding_20,
                      borderWidth: 1.0,
                      child: lText(_getStateMessage(), style: st_14(textColor: Commons.getColor())),
                      bgColor: Colors.transparent,
                      borderColor: Commons.getColor(),
                    ),
                    Expanded(
                      child: lScrollView(
                          padding: padding_0,
                          child: Column(children: [
                            RecipeView(
                              type: storageHelper.user_type,
                              viewType: ViewType.view,
                              careDate: StringUtils.parseYMDE(StringUtils.parseYMD(_paymentInfo.caredate)),
                              payDate: StringUtils.parseYMDAH(_paymentInfo.payUpdatedate),
                              serviceType: EnumUtils.getServiceType(name: _paymentInfo.servicetype),
                              startTime: _paymentInfo.stime,
                              endTime: _paymentInfo.etime,
                              hourPay: _paymentInfo.costPerhour,
                              totalPay: _paymentInfo.payExpect,
                              tax: _paymentInfo.commission,
                              isRefund: _claim.content1.isNotEmpty,
                              cash: _paymentInfo.paidCharge,
                              penaltyCount: _paymentInfo.penaltyExpect,
                            ),
                            if (_claim.answer.isNotEmpty)
                              Column(children: [
                                lDivider(thickness: 8.0, color: color_dedede.withOpacity(0.32)),
                                lTitleBodyView(
                                  "관리자답변".tr(),
                                  _claim.answer,
                                  backgroundColor: storageHelper.user_type == USER_TYPE.link_mom ? color_f7f3f9 : color_eef8f6,
                                  borderColor: Colors.transparent,
                                  margin: padding_20,
                                ),
                                ClaimView(_claim, view: ViewType.view),
                              ]),
                          ])),
                    ),
                    if (_paymentInfo.isReview ? false : _paymentInfo.isEndCare && !widget.isSignOut)
                      lBtn("후기남기기".tr(), onClickAction: () {
                        Commons.pageToMain(context, routeReviewSave,
                            arguments: ReviewSavePage(
                              viewType: ViewType.modify,
                              matchingId: widget.matchingId,
                            ));
                      })
                  ],
                ),
              ))),
    );
  }

  void _requestPaymentData(int id) {
    flag.enableLoading(fn: () => onUpDate());
    apiHelper.reqeustDiaryPayment(CarediaryPaymentRequest(scheduleId: id)).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        _paymentInfo = response.getData();
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }

  void _requestClaimView(int id) {
    try {
      if (id == 0) return; // NOTE: not claim diary
      CareDiaryClaimViewRequest req = CareDiaryClaimViewRequest(claim_id: id);
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestDiaryClaimView(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _claim = response.getData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>> $e");
    }
  }

  String _getStateMessage() {
    if (Commons.isLinkMom() && widget.status == PayStatus.ready) {
      return "정산_정산대기".tr();
    } else {
      return "정산이완료".tr();
    }
  }
}
