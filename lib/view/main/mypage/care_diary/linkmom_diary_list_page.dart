import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/carediary_list_request.dart';
import 'package:linkmom/data/network/models/carediary_list_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_page_view.dart';

import 'diary_page_view.dart';

class LinkmomDiaryListPage extends BaseStateful {
  LinkmomDiaryListPage({this.data});

  final DataManager? data;

  @override
  _LinkmomDiaryListPageState createState() => _LinkmomDiaryListPageState();
}

class _LinkmomDiaryListPageState extends BaseStatefulState<LinkmomDiaryListPage> {
  CareDiaryListData _before = CareDiaryListData(results: []);
  CareDiaryListData _after = CareDiaryListData(results: []);

  @override
  void initState() {
    super.initState();
    if (widget.data != null) data = widget.data!;
    flag.enableLoading(fn: () => onUpDate());
    _requestDiaryList();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            context: context,
            appBar: appBar("돌봄일기".tr()),
            body: Container(
                color: Colors.white,
                child: DiaryPageView(
                  data: data,
                  reverse: true,
                  tabName: [
                    TabItem(title: "작성대기".tr()),
                    TabItem(title: "작성완료".tr()),
                  ],
                  filterList: [""],
                  leftList: _before,
                  rightList: _after,
                  onLeftRefresh: (next) => _requestBeforeDiary(next: next),
                  onRightRefresh: (next) => _requestAfterDiary(next: next),
                ))));
  }

  void _requestDiaryList() {
    try {
      _requestBeforeDiary();
      _requestAfterDiary();
    } catch (e) {
      log.e("Exception: >>>>>>>>> $e");
    }
  }

  void _requestBeforeDiary({bool next = false}) {
    if (next && _before.next.isEmpty) return;
    CareDiaryListReqeust before = CareDiaryListReqeust(
      gubun: USER_TYPE.link_mom.index + 1,
      type: 0, // before write
    );
    apiHelper.requestDiaryList(before, url: next ? _before.next : null).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        if (next) {
          _before.results!.addAll(response.getData().results ?? []);
          _before.previous = response.getData().previous;
          _before.next = response.getData().next;
        } else {
          _before = response.getData();
        }
      } else {
        _before = CareDiaryListData(results: []);
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }

  void _requestAfterDiary({bool next = false}) {
    if (next && _after.next.isEmpty) return;
    CareDiaryListReqeust after = CareDiaryListReqeust(
      gubun: USER_TYPE.link_mom.index + 1,
      type: 1, // after write
    );
    apiHelper.requestDiaryList(after, url: next ? _after.next : null).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        if (next) {
          _after.results!.addAll(response.getData().results ?? []);
          _after.previous = response.getData().previous;
          _after.next = response.getData().next;
        } else {
          _after = response.getData();
        }
      } else {
        _before = CareDiaryListData(results: []);
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }
}
