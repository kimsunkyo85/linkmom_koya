import 'package:flutter/material.dart';
import 'package:highlight_text/highlight_text.dart' as high;

import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/carediary_like_request.dart';
import 'package:linkmom/data/network/models/carediary_payment_request.dart';
import 'package:linkmom/data/network/models/carediary_reply_delete_request.dart';
import 'package:linkmom/data/network/models/carediary_reply_save_request.dart';
import 'package:linkmom/data/network/models/diary_view_request.dart';
import 'package:linkmom/data/network/models/diary_view_response.dart';
import 'package:linkmom/data/network/models/pay_cancel_view_request.dart';
import 'package:linkmom/data/storage/flag_manage.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/custom_modal.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_drug_list_page.dart';
import 'package:linkmom/view/main/notify/diary_notify_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_care_list_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_write_page.dart';
import 'package:linkmom/view/board/board_view.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_board_view.dart';
import 'package:linkmom/view/main/payment/payment_cancel_page.dart';

import 'diary_claim_page.dart';
import 'recipe_view_page.dart';

class DiaryViewPage extends BaseStateful {
  final int id;
  final USER_TYPE type;
  DiaryViewPage({this.id = 0, this.type = USER_TYPE.mom_daddy});

  @override
  _DiaryViewPageState createState() => _DiaryViewPageState();
}

class _DiaryViewPageState extends BaseStatefulState<DiaryViewPage> {
  DiaryViewModel _view = DiaryViewModel();
  PayStatus _payStatus = PayStatus.ready;
  bool _serviceCompleated = false;
  bool _showModal = true;
  List<int> _total = [];
  List<int> _complete = [];
  int _totalDrug = 0;
  int _currentDrug = 0;
  double _opacity = 1;
  bool _beforeDueDate = false;
  Size _size = Size.zero;
  DiaryTaskInfo _task = DiaryTaskInfo();
  ScrollController _ctrl = ScrollController();

  @override
  void initState() {
    super.initState();
    _requestDiaryView(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        context: context,
        resizeToAvoidBottomInset: true,
        appBar: diaryAppbar(
            title: "돌봄일기".tr(),
            hide: false,
            rightW: Commons.isLinkMom() && !_isLate() && _payStatus == PayStatus.ready
                ? lInkWell(
                    onTap: () => Commons.page(context, routeDiaryWrite, arguments: DiaryWritePage(id: _view.carecontractSchedule)),
                    child: Container(
                      alignment: Alignment.center,
                      padding: padding_20_LR,
                      child: lText("수정하기".tr(), style: st_15(fontWeight: FontWeight.w500, textColor: color_b579c8)),
                    ))
                : null),
        body: LinkmomRefresh(
          padding: padding_0,
          onRefresh: () => _requestDiaryView(widget.id),
          isFull: _payStatus.value > PayStatus.delayed.value,
          child: Builder(builder: (ctx) {
            if (_view.carecontractSchedule == 0) return lEmptyView(height: Commons.height(context, 0.5));
            switch (_payStatus) {
              case PayStatus.cancelReady:
              case PayStatus.penaltyReady:
                return _buildCancelView("해당돌봄일정이취소예정".tr());
              case PayStatus.cancel:
              case PayStatus.penalty:
                return _buildCancelView("해당돌봄일정이취소완료".tr());
              default:
                return _buildDiary();
            }
          }),
        ),
      ),
    );
  }

  ReplyData _buildReply(CarediaryReply reply) {
    return ReplyData(
        id: reply.id,
        parent: reply.parent,
        writer: "${reply.writerName} ${reply.writerGubun}",
        writtenTime: StringUtils.parseYMDHM(reply.updatedate),
        message: reply.reply,
        profile: reply.profileimg,
        reReply: _buildReplyList(reply.child ?? []),
        isDeleted: reply.isDelete == 1,
        level: reply.parent,
        showMenu: auth.user.id == reply.writerId && reply.isDelete == 1,
        replyMenu: {
          if (auth.user.id == reply.writerId)
            lText("삭제".tr(), style: st_16(textColor: color_ff3b30, fontWeight: FontWeight.w500)): () {
              showNormalDlg(
                context: context,
                message: "댓글을삭제하시겠습니까".tr(),
                btnLeft: "취소".tr(),
                onClickAction: (action) {
                  switch (action) {
                    case DialogAction.yes:
                      _requestReplyDelete(reply.id, widget.id);
                      break;
                    default:
                  }
                },
              );
            },
        });
  }

  List<ReplyData> _buildReplyList(List<CarediaryReply>? reply) {
    if (reply == null) {
      return [];
    }
    return reply.map((e) => _buildReply(e)).toList();
  }

  void _requestDiaryView(int id) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      DiaryViewRequest req = DiaryViewRequest(scheduleId: id);
      apiHelper.requestDiaryView(req).then((response) {
        _view = response.getData();

        int cnt = 0;
        cnt += _view.carediaryInfo!.option!.boyukoptions.length;
        cnt += _view.carediaryInfo!.option!.homecareoptions.length;
        cnt += _view.carediaryInfo!.option!.playoptions.length;
        _task.initCares = cnt;

        _task.setCares = _view.carediaryInfo!.completeOption!.completeBoyuk.length;
        _task.updateCares = _view.carediaryInfo!.completeOption!.completeHomecare.length;
        _task.updateCares = _view.carediaryInfo!.completeOption!.completePlay.length;
        _payStatus = EnumUtils.getPayStatus(index: _view.payStatus);
        _task.initDrugs = _view.carediaryInfo!.childdrugList!.length;
        _task.setDrugs = _view.carediaryInfo!.childdrugList!.where((element) => element.isDrug).length;
        if (_view.claimInfo != null && _payStatus == PayStatus.delayed) {
          if (response.getData().claimInfo!.claimAnswer) {
            showNormalDlg(
              message: "정산보류요청결과".tr(),
              onClickAction: (a) {
                if (a == DialogAction.yes) _moveClaim(_view.claimInfo!.claimId, id);
              },
            );
          }
          if (Commons.isLinkMom() && response.getData().claimInfo!.claimAfterDay.isNotEmpty && !_view.claimInfo!.claimAnswer) {
            DateTime time = StringUtils.formatYMDHMS(_view.claimInfo!.claimAfterDay).add(Duration(hours: 24));
            showNormalDlg(
              message: '${_view.carediaryInfo!.momdadyName}${"맘대디의정산보류요청이".tr()} ${StringUtils.formatMMDDHH(time)}${"소명을진행해주세요".tr()}',
              onClickAction: (a) {
                if (a == DialogAction.yes) _moveClaim(_view.claimInfo!.claimId, id);
              },
            );
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e("EXCEPTION >>>>>>>>>>>>>>> $e");
    }
  }

  void _requestReplyDelete(int id, int scheduleId) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      CareDiaryReplyDeleteRequest req = CareDiaryReplyDeleteRequest(id: id);
      apiHelper.requestDiaryReplyDelete(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _requestDiaryView(scheduleId);
        } else {
          showNormalDlg(message: response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>> $e");
    }
  }

  Future<void> _requestReplySave(int parent, String message, int id, {bool retry = true}) async {
    try {
      if (_view.carediaryReply == null) _view.carediaryReply = [];
      CareDiaryReplySaveRequest req = CareDiaryReplySaveRequest(
        carediary: id,
        writer_gubun: storageHelper.user_type.index + 1,
        reply: message,
        parent: parent,
      );
      var response = await apiHelper.requestDiaryReplySave(req);
      if (response.getCode() == KEY_SUCCESS) {
        int index = 0;
        if (parent > 0) index = _view.carediaryReply!.indexOf(_view.carediaryReply!.lastWhere((reply) => reply.id == parent || reply.parent == parent));
        _view.carediaryReplyCnt++;
        CarediaryReply reply = CarediaryReply(
          reply: response.getData().reply,
          writerName: auth.getMyInfoData.first_name,
          writerGubun: storageHelper.user_type.name,
          updatedate: StringUtils.formatYMDHM(DateTime.now()),
          profileimg: auth.getMyInfoData.profileimg,
          writerId: auth.user.id,
          parent: parent,
        );
        if (_view.carediaryReply!.isNotEmpty && parent != 0) {
          _view.carediaryReply!.insert(index + 1, reply);
        } else {
          _view.carediaryReply!.add(reply);
        }
      } else {
        showNormalDlg(message: response.getMsg());
        if (retry) _requestReplySave(id, message, parent, retry: false);
      }
      _ctrl.animateTo(_ctrl.position.maxScrollExtent, duration: Duration(milliseconds: 500), curve: Curves.easeOutBack);
      flag.disableLoading(fn: () => onUpDate());
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>> $e");
    }
  }

  void _requestLike(int id) {
    try {
      CareDiaryLikeRequest req = CareDiaryLikeRequest(carecontract_schedule: id);
      apiHelper.requestDiaryLike(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _view.likeYn = true;
        }
        onUpDate();
      });
    } catch (e) {
      log.e("Exception: >>>>>>>>>>>> $e");
    }
  }

  void _requestDiaryPaymentPatch(int id) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      CarediaryPaymentRequest req = CarediaryPaymentRequest(scheduleId: id);
      apiHelper.reqeustDiaryPaymentPatch(req).then((response) async {
        if (response.getCode() == KEY_SUCCESS) {
          var ret = await _moveRecipe(
            id,
            _view.matching,
            _view.claimInfo!.claimId,
            _view.isReview ? false : _view.isEndCare,
            _payStatus,
            _view.userinfo!.firstName.contains("탈퇴회원".tr()),
          );
          if (ret != null) {
            _requestDiaryView(id);
          }
        } else {
          if (response.getCode() == KEY_ALREADY_CALC) {
            showNormalDlg(message: response.getMsg(), onClickAction: (a) => {if (a == DialogAction.yes) _requestDiaryView(id)});
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        log.e('_requestDiaryPaymentPatch >>>>>>>>>>>> e');
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }

  void _moveClaim(int claimId, int scheduleId) {
    if (claimId != 0) {
      Commons.page(context, routeClaimView,
          arguments: DiaryClaimViewPage(
            claimId: claimId,
            viewType: widget.type == USER_TYPE.mom_daddy ? ViewType.view : ViewType.modify,
          ));
    } else {
      Commons.page(context, routeDiaryNotify, arguments: DiaryNotifyPage(id: scheduleId));
    }
  }

  void _showLikeAlert(bool isLike, int id) {
    if (isLike) {
      showNormalDlg(context: context, message: "맘대디가링크쌤의돌봄활동에대해남겨주는좋아요입니다".tr());
    } else {
      _view.likeYn = true;
      onUpDate();
      _requestLike(id);
    }
  }

  Widget _buildStatusBtn() {
    switch (_payStatus) {
      case PayStatus.ready:
        if (_getCareTimeRange.start.isAfter(DateTime.now())) {
          _buildEalyBtn();
        } else {
          _buildReadyBtn();
        }
        return _buildReadyBtn();
      case PayStatus.delayed:
        return _buildDelayedBtn();
      case PayStatus.compleat:
        return _buildCompleatBtn();
      case PayStatus.cancelReady:
        return _buildCancelBtn();
      case PayStatus.cancel:
        return _buildCancelBtn(name: "돌봄취소완료".tr());
      default:
        return _buildConcatBtn();
    }
  }

  Future<void> _moveCalc(int id) async {
    if (_payStatus == PayStatus.ready && !Commons.isLinkMom()) {
      showNormalDlg(
          message: "정산하기_확인".tr(),
          btnLeft: "취소".tr(),
          btnRight: "확인".tr(),
          onClickAction: (a) async {
            if (a == DialogAction.yes) {
              _requestDiaryPaymentPatch(id);
            }
          });
    } else {
      _moveRecipe(
        id,
        _view.matching,
        _view.claimInfo!.claimId,
        _view.isReview ? false : _view.isEndCare,
        _payStatus,
        _view.userinfo!.firstName.contains("탈퇴회원".tr()),
      );
    }
  }

  DateTimeRange get _getCareTimeRange {
    DateTimeRange time = DateTimeRange(start: DateTime.now(), end: DateTime.now());
    if (_view.caredate == null) {
      return time;
    } else {
      DateTime start = StringUtils.parseYMDHM(_view.caredate!.first + ' ' + _view.stime);
      DateTime end = StringUtils.parseYMDHM(_view.caredate!.first + ' ' + _view.etime);
      time = DateTimeRange(start: start, end: end);
    }
    return time;
  }

  bool _isLate() {
    return _getCareTimeRange.end.add(Duration(hours: 12)).isBefore(DateTime.now()) && _view.carediaryInfo!.content.isEmpty;
  }

  Widget _buildCancelView(String message) {
    return DiaryReadView(
      infoView: DiaryInfoView(
        type: EnumUtils.getServiceType(name: _view.servicetype),
        status: _payStatus.name,
        name: _view.childinfo?.childName ?? '',
        day: StringUtils.parseMDE(_view.caredate!.first),
        time: StringUtils.getCareTime([_view.stime, _view.etime]),
        statusStyle: st_15(textColor: color_b2b2b2, fontWeight: FontWeight.bold),
      ),
      body: BoardBody(
        hint: Container(
          height: Commons.height(context, 0.6),
          alignment: Alignment.center,
          child: lText(
            message,
            style: st_16(textColor: color_b2b2b2, fontWeight: FontWeight.bold),
          ),
        ),
      ),
      btn: _buildStatusBtn(),
    );
  }

  Widget _buildDiary() {
    _serviceCompleated = _view.carediaryInfo!.isServicetype != 0;
    return DiaryReadView(
      infoView: DiaryInfoView(
        type: EnumUtils.getServiceType(name: _view.servicetype),
        status: _payStatus.name,
        name: _view.childinfo!.childName,
        day: StringUtils.parseMDE(_view.caredate!.first),
        time: StringUtils.getCareTime([_view.stime, _view.etime]),
        statusStyle: st_15(
          textColor: _payStatus == PayStatus.ready ? Commons.getColor() : color_999999,
          fontWeight: FontWeight.bold,
        ),
      ),
      profile: DiaryBoardTitle(
        author: _view.userinfo!.firstName,
        writtenTime: _view.carediaryInfo!.updatedate.isEmpty
            ? _payStatus == PayStatus.compleat
                ? ""
                : "작성대기".tr()
            : StringUtils.parseYYMDAHM(_view.carediaryInfo!.updatedate),
        type: storageHelper.user_type,
        like: _view.likeYn,
        profileImg: _view.userinfo!.profileimg,
      ),
      topper: _payStatus.index < PayStatus.delayed.index
          ? Container(
              child: Column(children: [
                if (_serviceCompleated)
                  DiaryTask(
                    title: _serviceCompleated ? "${_view.servicetype}${"완료".tr()}" : "${_view.servicetype}".tr(),
                    isCompleated: _serviceCompleated,
                    onClickAction: () => {},
                  ),
                if (_task.drugCnt > 0)
                  DiaryTask(
                    title: '${_task.isCompleteDrug ? "투약완료".tr() : "투약".tr()}',
                    total: _task.drugCnt,
                    current: _task.completeDrugCnt,
                    isCompleated: _task.isCompleteDrug,
                    onClickAction: () => Commons.page(context, routeDiaryDrugList,
                        arguments: DiaryDrugListPage(
                          drugId: _view.carediaryInfo!.childdrugId,
                          scheduleId: _view.carecontractSchedule,
                          linkmomName: _view.userinfo!.firstName,
                          momdadyName: auth.user.my_info_data!.first_name,
                          isDrug: _task.isCompleteDrug,
                          onChecked: (checked) => {},
                        )),
                  ),
                if (_task.careCnt > 0)
                  DiaryTask(
                      title: "진행한돌봄".tr(),
                      total: _task.careCnt,
                      current: _task.completeCareCnt,
                      isCompleated: _task.careCnt == _task.completeCareCnt,
                      onClickAction: () => Commons.page(context, routeDiaryCareList,
                          arguments: DiaryCareListPage(
                            options: _view.carediaryInfo!.option,
                            compleats: _view.carediaryInfo!.completeOption,
                            type: storageHelper.user_type,
                            viewType: ViewType.view,
                            service: EnumUtils.getServiceType(name: _view.servicetype),
                            area: _view.possibleArea.contains(USER_TYPE.mom_daddy.name) ? 1 : 2,
                          ))),
              ]),
            )
          : Container(),
      body: _buildDiaryBody(
        _isLate(),
        Commons.isLinkMom(),
        height: _task.careCnt > 0 || _task.drugCnt > 0 || _task.isCompleteServ ? Commons.height(context, 0.4) : null,
      ),
      footer: _buildDiaryFooter(showLike: !Commons.isLinkMom() && _view.carediaryInfo!.content.isNotEmpty),
      btn: _buildDiaryBtn(),
      showReply: _view.carediaryInfo!.id != 0,
      reply: _view.carediaryReply!.map((e) => _buildReply(e)).toList(),
      replyLength: _view.carediaryReplyCnt,
      saveReplyAction: (parent, message) async {
        await _requestReplySave(parent, message, _view.carediaryInfo!.id);
      },
      modal: _buildModal(),
      size: _size,
    );
  }

  Widget _buildDiaryBtn() {
    return Container(
        padding: padding_20_T,
        child: Column(children: [
          _buildStatusBtn(),
          if (!Commons.isLinkMom() && _payStatus == PayStatus.ready)
            Container(
                padding: padding_20_T,
                child: ClaimText(onClickAction: () {
                  if (_getCareTimeRange.start.isAfter(DateTime.now())) {
                    showNormalDlg(
                        messageWidget: TextHighlight(
                      text: "돌봄이전_정산보류".tr(),
                      term: "돌봄이전_정산보류_하이라이트".tr(),
                      textStyleHighlight: st_13(textColor: color_main),
                    ));
                  } else {
                    showCancelAlert((a) {
                      if (a == DialogAction.yes) {
                        if (_view.claimInfo != null && _view.claimInfo!.claimId != 0) {
                          showNormalDlg(message: "정산_정산보류_안내2".tr());
                        } else {
                          _moveClaim(_view.claimInfo != null ? _view.claimInfo!.claimId : 0, _view.carecontractSchedule);
                        }
                      }
                    }, diaryClaimView: true);
                  }
                })),
        ]));
  }

  Widget? _buildModal() {
    if (_payStatus == PayStatus.ready && !Commons.isLinkMom() && _showModal) {
      if (_view.carediaryInfo!.id == 0) {
        if (Flags.isShowingCachedFlag(Flags.KEY_DIARY_M_BEFORE_WRITE)) return _buildBeforeModal();
      } else {
        if (Flags.isShowingCachedFlag(Flags.KEY_DIARY_M_AFTER_WRITE)) return _buildConcatModal();
      }
    }
    return null;
  }

  /// NOTE: after write before pay (for momdaddy)
  Widget _buildConcatModal() {
    _size = Size(0, Commons.height(context, 0.15));
    return AnimatedOpacity(
      opacity: _opacity,
      duration: Duration(milliseconds: 300),
      onEnd: () {
        _showModal = false;
        onUpDate();
      },
      child: Container(
        margin: padding_20_LR,
        child: ReleativeModal(
          onClickAction: () {
            _opacity = _opacity == 1 ? 0 : 1;
            Flags.shownCachedFlag(Flags.KEY_DIARY_M_AFTER_WRITE);
            onUpDate();
          },
          icon: Lcons.info_t(color: color_222222, isEnabled: true),
          title: "돌봄일기_정산안내_타이틀".tr(),
          titleStyle: st_16(fontWeight: FontWeight.w700, textColor: color_545454),
          bodyWidget: Container(
            padding: padding_10,
            child: high.TextHighlight(
              text: "돌봄일기_정산안내_안내".tr(),
              textStyle: st_14(textColor: color_545454, fontWeight: FontWeight.w500, height: 1.5),
              words: {
                "돌봄일기_정산안내_안내_2".tr(): high.HighlightedWord(
                  textStyle: st_14(textColor: color_main, fontWeight: FontWeight.w500, height: 1.5),
                  onTap: () => {},
                ),
                "즉시정산".tr(): high.HighlightedWord(
                  textStyle: st_14(textColor: color_main, fontWeight: FontWeight.w500, height: 1.5),
                  onTap: () => {},
                ),
              },
            ),
          ),
        ),
      ),
    );
  }

  /// NOTE: before write, before pay (for momdaddy)
  Widget _buildBeforeModal() {
    _size = Size(0, Commons.height(context, 0.18));
    return AnimatedOpacity(
      opacity: _opacity,
      duration: Duration(milliseconds: 300),
      onEnd: () {
        _showModal = false;
        onUpDate();
      },
      child: Container(
        margin: padding_20_LR,
        child: ReleativeModal(
          onClickAction: () {
            _opacity = _opacity == 1 ? 0 : 1;
            Flags.shownCachedFlag(Flags.KEY_DIARY_M_BEFORE_WRITE);
            onUpDate();
          },
          icon: Lcons.info_t(color: color_222222, isEnabled: true),
          title: "돌봄일기_작성안내_타이틀".tr(),
          titleStyle: st_16(fontWeight: FontWeight.w700, textColor: color_545454),
          bodyWidget: Container(
            padding: padding_10,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                high.TextHighlight(
                  text: "돌봄일기_즉시정산_안내".tr(),
                  textStyle: st_14(textColor: color_545454, fontWeight: FontWeight.w500, height: 1.5),
                  words: {
                    "돌봄일기_정산안내_안내_2".tr(): high.HighlightedWord(
                      textStyle: st_14(textColor: color_main, fontWeight: FontWeight.w500, height: 1.5),
                      onTap: () => {},
                    ),
                    "돌봄일기_작성안내_안내_3".tr(): high.HighlightedWord(
                      textStyle: st_14(textColor: color_main, fontWeight: FontWeight.w500, height: 1.5),
                      onTap: () => {},
                    ),
                  },
                ),
                sb_h_10,
                high.TextHighlight(
                  text: "돌봄일기_즉시정산_안내_1".tr(),
                  textStyle: st_14(textColor: color_545454, fontWeight: FontWeight.w500, height: 1.5),
                  words: {
                    "즉시정산".tr(): high.HighlightedWord(
                      textStyle: st_14(textColor: color_main, fontWeight: FontWeight.w500, height: 1.5),
                      onTap: () => {},
                    ),
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildDiaryFooter({bool showLike = true}) {
    if (showLike) {
      return Container(
          padding: padding_20_T,
          child: Container(
            margin: padding_20_TB,
            child: lInkWell(
                onTap: () => _showLikeAlert(_view.likeYn, _view.carecontractSchedule),
                child: Column(
                  children: [
                    Container(
                      margin: padding_05,
                      padding: padding_13,
                      decoration: BoxDecoration(
                        color: _view.likeYn ? Commons.getColor() : color_eeeeee.withOpacity(0.42),
                        borderRadius: BorderRadius.circular(40),
                      ),
                      child: Lcons.diary_like(size: 30, color: Colors.white, disableColor: color_c4c4c4, isEnabled: _view.likeYn),
                    ),
                    lText(
                      "오늘의돌봄좋아요".tr(),
                      style: st_15(
                        textColor: _view.likeYn ? Commons.getColor() : color_b2b2b2,
                        fontWeight: _view.likeYn ? FontWeight.w700 : FontWeight.w500,
                      ),
                    )
                  ],
                )),
          ));
    } else {
      return Container();
    }
  }

  Widget _buildReadyBtn() {
    return _btn(Commons.isLinkMom() ? "정산대기".tr() : "정산하기".tr(), () {
      _moveCalc(_view.carecontractSchedule);
    });
  }

  Widget _buildDelayedBtn() {
    return _btn("정산보류중".tr(), () {
      _moveClaim(_view.claimInfo!.claimId, _view.carecontractSchedule);
    });
  }

  Widget _buildCompleatBtn() {
    return _btn("정산완료".tr(), () => _moveCalc(_view.carecontractSchedule));
  }

  Widget _buildCancelBtn({String name = ""}) {
    return _btn(name.isEmpty ? "돌봄취소예정".tr() : name, () {
      Commons.page(context, routePayMentCancel,
          arguments: PaymentCancelPage(
            viewType: PayViewType.cancel_view,
            payCancelViewRequest: PayCancelViewRequest(
              order_id: _view.cancelInfo!.orderId,
              cancel_id: _view.cancelInfo!.cancelId,
              contract_id: _view.cancelInfo!.contractId,
            ),
          ));
    });
  }

  Widget _buildEalyBtn() {
    return _btn("돌봄예정".tr(), () {
      showNormalDlg(message: "정산하기_시간이전".tr());
    }, isEnabled: false);
  }

  Widget _buildConcatBtn() {
    return _btn("정산하기".tr(), () => _moveCalc(widget.id));
  }

  Widget _btn(String name, Function action, {bool isEnabled = true}) {
    return lBtn(name, onClickAction: action, isEnabled: isEnabled, margin: padding_0);
  }

  Widget _buildDiaryBody(bool isLate, bool isLinkmom, {double? height}) {
    if (isLate && isLinkmom) {
      return _buildLateBody();
    } else {
      return _buildNormalBody(height ?? Commons.height(context, 0.5), height != null);
    }
  }

  Widget _buildNormalBody(double height, bool showComponent) {
    return BoardBody(
      hint: Container(
        height: _view.carediaryInfo!.content.isEmpty && showComponent ? Commons.height(context, 0.2) : height,
        padding: padding_20_TB,
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Lcons.edit(size: 46, color: color_cecece, isEnabled: true),
            sb_h_15,
            lText("돌봄일기_미작성".tr(), style: st_16(textColor: color_b2b2b2, fontWeight: FontWeight.w500)),
          ],
        ),
      ),
      text: _view.carediaryInfo!.content,
      images: _view.carediaryImages!.map((e) => e.images).toList(),
    );
  }

  Widget _buildLateBody() {
    return AspectRatio(
      aspectRatio: Commons.width(context, 1) / Commons.height(context, 0.53),
      child: BoardBody(
        hint: Container(
          height: Commons.height(context, 0.4),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Lcons.edit_not_available(size: 46, color: color_cecece, isEnabled: true),
              sb_h_15,
              lText("돌봄일기_시간초과".tr(), style: st_16(textColor: color_b2b2b2, fontWeight: FontWeight.w500), textAlign: TextAlign.center),
              sb_h_15,
              TextHighlight(
                text: "돌봄일기_자동정산_안내".tr(),
                term: "돌봄일기_작성안내_안내_3".tr(),
                textStyle: st_15(textColor: color_b2b2b2),
                textStyleHighlight: st_15(textColor: color_linkmom),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
        text: _view.carediaryInfo != null ? _view.carediaryInfo!.content : '',
        images: _view.carediaryImages!.map((e) => e.images).toList(),
      ),
    );
  }

  Future<dynamic> _moveRecipe(int id, int matchingId, int claimId, bool isLast, PayStatus status, bool isSignOut) async {
    return await Commons.page(context, routeRecipe,
        arguments: RecipeViewPage(
          appbarTitle: _payStatus.name,
          id: id,
          type: widget.type,
          claimId: claimId,
          status: status,
          isLast: isLast,
          matchingId: matchingId,
          isSignOut: isSignOut,
        ));
  }
}
