import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/carediary_form_response.dart';
import 'package:linkmom/data/storage/model/menu_file_data.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../../../../main.dart';
import 'care_task_view.dart';

class DiaryCareListPage extends BaseStateful {
  final Option? options;
  CompleteOption? compleats;
  final USER_TYPE? type;
  final ViewType viewType;
  final Function? onSelected;
  final ServiceType? service;
  final int area;
  DiaryCareListPage({this.options, this.compleats, this.type, this.viewType = ViewType.modify, this.onSelected, this.service, this.area = 0}) {
    if (this.compleats == null) this.compleats = CompleteOption(completeBoyuk: [], completeHomecare: [], completePlay: []);
  }

  @override
  _DiaryCareListPageState createState() => _DiaryCareListPageState();
}

class _DiaryCareListPageState extends BaseStatefulState<DiaryCareListPage> {
  late Map<String, List<ServiceItem>> _job = {};
  bool _compleatedCare = false;
  bool _compleatedHomeCare = false;
  bool _compleatedPlay = false;

  @override
  void initState() {
    super.initState();
    if (_job.isEmpty) {
      _job = Map.from(Commons.getJobData(widget.service!.value, widget.area));
    }
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar("돌봄일기".tr(), hide: false, onBack: () => Commons.pagePop(context, data: widget.compleats)),
            body: Container(
                padding: padding_20,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: double.infinity,
                      alignment: Alignment.center,
                      padding: padding_20,
                      child: widget.type == USER_TYPE.link_mom ? lText("돌봄일기_링크쌤_돌봄확인".tr(), style: st_b_18(fontWeight: FontWeight.w700)) : lText("돌봄일기_맘대디_돌봄확인".tr(), style: st_b_18(fontWeight: FontWeight.w700)),
                    ),
                    Expanded(
                        child: TabPageView(
                      tabbarTitle: [
                        _buildTabbar("보육".tr(), widget.compleats!.completeBoyuk.length, widget.options!.boyukoptions.length),
                        _buildTabbar("가사".tr(), widget.compleats!.completeHomecare.length, widget.options!.homecareoptions.length),
                        _buildTabbar("놀이".tr(), widget.compleats!.completePlay.length, widget.options!.playoptions.length),
                      ],
                      tabbarView: [
                        Container(
                          child: widget.options!.boyukoptions.isNotEmpty
                              ? Column(
                                  children: [
                                    if (widget.viewType == ViewType.modify)
                                      _buildCounter(widget.compleats!.completeBoyuk.length, widget.options!.boyukoptions.length, _compleatedCare, () {
                                        _compleatedCare = !_compleatedCare;
                                        if (_compleatedCare) {
                                          widget.compleats!.completeBoyuk.clear();
                                          widget.compleats!.completeBoyuk.addAll(widget.options!.boyukoptions);
                                        } else {
                                          widget.compleats!.completeBoyuk.clear();
                                        }
                                        if (widget.onSelected != null) widget.onSelected!(ServicesData.Key_boyuks, widget.compleats!.completeBoyuk);
                                      }),
                                    Expanded(
                                      child: CareTaskView(
                                          selectable: widget.viewType == ViewType.modify,
                                          compleatTask: widget.compleats!.completeBoyuk,
                                          task: widget.options!.boyukoptions,
                                          services: _job[ServicesData.Key_boyuks]!.map((e) => ServiceItem(cares: e.cares, products: e.products!.where((e) => widget.options!.boyukoptions.contains(e.product_id)).toList())).toList(),
                                          updateEvent: (compleatTask) {
                                            widget.compleats!.completeBoyuk = compleatTask;
                                            _compleatedCare = (widget.compleats!.completeBoyuk.length == widget.options!.boyukoptions.length);
                                            if (widget.onSelected != null) widget.onSelected!(ServicesData.Key_boyuks, widget.compleats!.completeBoyuk);
                                            onUpDate();
                                          }),
                                    ),
                                  ],
                                )
                              : lEmptyView(),
                        ),
                        Container(
                          child: widget.options!.homecareoptions.isNotEmpty
                              ? Column(
                                  children: [
                                    if (widget.viewType == ViewType.modify)
                                      _buildCounter(widget.compleats!.completeHomecare.length, widget.options!.homecareoptions.length, _compleatedHomeCare, () {
                                        _compleatedHomeCare = !_compleatedHomeCare;
                                        if (_compleatedHomeCare) {
                                          widget.compleats!.completeHomecare.clear();
                                          widget.compleats!.completeHomecare.addAll(widget.options!.homecareoptions);
                                        } else {
                                          widget.compleats!.completeHomecare.clear();
                                        }
                                        if (widget.onSelected != null) widget.onSelected!(ServicesData.Key_homecares, widget.compleats!.completeHomecare);
                                      }),
                                    Expanded(
                                      child: CareTaskView(
                                        compleatTask: widget.compleats!.completeHomecare,
                                        task: widget.options!.homecareoptions,
                                        services: _job[ServicesData.Key_homecares]!.map((e) {
                                          log.d({
                                            '--- DATA     ': e,
                                          });
                                          return ServiceItem(cares: e.cares, products: e.products!.where((e) => widget.options!.homecareoptions.contains(e.product_id)).toList());
                                        }).toList(),
                                        updateEvent: (compleatTask) {
                                          widget.compleats!.completeHomecare = compleatTask;
                                          _compleatedHomeCare = (widget.compleats!.completeHomecare.length == widget.options!.homecareoptions.length);
                                          if (widget.onSelected != null) widget.onSelected!(ServicesData.Key_homecares, widget.compleats!.completeHomecare);
                                          onUpDate();
                                        },
                                      ),
                                    )
                                  ],
                                )
                              : lEmptyView(),
                        ),
                        Container(
                            child: widget.options!.playoptions.isNotEmpty
                                ? Column(
                                    children: [
                                      if (widget.viewType == ViewType.modify)
                                        _buildCounter(widget.compleats!.completePlay.length, widget.options!.playoptions.length, _compleatedPlay, () {
                                          _compleatedPlay = !_compleatedPlay;
                                          if (_compleatedPlay) {
                                            widget.compleats!.completePlay.clear();
                                            widget.compleats!.completePlay.addAll(widget.options!.playoptions);
                                          } else {
                                            widget.compleats!.completePlay.clear();
                                          }
                                          if (widget.onSelected != null) widget.onSelected!(ServicesData.Key_plays, widget.compleats!.completePlay);
                                        }),
                                      Expanded(
                                        child: CareTaskView(
                                            compleatTask: widget.compleats!.completePlay,
                                            task: widget.options!.playoptions,
                                            services: _job[ServicesData.Key_plays]!.map((e) => ServiceItem(cares: e.cares, products: e.products!.where((e) => widget.options!.playoptions.contains(e.product_id)).toList())).toList(),
                                            updateEvent: (compleatTask) {
                                              widget.compleats!.completePlay = compleatTask;
                                              _compleatedPlay = (widget.compleats!.completePlay.length == widget.options!.playoptions.length);
                                              if (widget.onSelected != null) widget.onSelected!(ServicesData.Key_plays, widget.compleats!.completePlay);
                                              onUpDate();
                                            }),
                                      )
                                    ],
                                  )
                                : lEmptyView()),
                      ],
                    ))
                  ],
                ))));
  }

  Widget _buildTabbar(String name, int current, int length) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (widget.type == USER_TYPE.link_mom)
            Container(
              padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
              decoration: BoxDecoration(color: current != length ? color_eeeeee : Commons.getColor(), borderRadius: BorderRadius.circular(16)),
              child: lText(("완료".tr()), style: st_12(fontWeight: FontWeight.w500)),
            ),
          sb_h_05,
          lCountText(name, current, length)
        ],
      ),
    );
  }

  Widget _buildCounter(int current, int length, bool checker, Function? onClickAction) {
    return lInkWell(
      onTap: () {
        checker = !checker;
        if (onClickAction != null) onClickAction();
        onUpDate();
      },
      child: Container(
        padding: padding_20_TB,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Lcons.check_circle(color: Commons.getColor(), disableColor: current == length ? Commons.getColor() : color_dedede, size: 22, isEnabled: checker),
            sb_w_05,
            lCountText(
              "전체완료".tr(),
              current,
              length,
              style: st_b_15(fontWeight: FontWeight.w500),
              pointStyle: st_15(textColor: Commons.getColor(), fontWeight: FontWeight.w500),
            )
          ],
        ),
      ),
    );
  }
}
