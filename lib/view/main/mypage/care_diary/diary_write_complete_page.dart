import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/compleat_view_page.dart';

class DiaryWriteCompletePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CompleteViewPage(
      title: "돌봄일기".tr(),
      body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Padding(
          padding: padding_20,
          child: Image.asset(ILLUST_INTRO, width: 150),
        ),
        lText("오늘도고생".tr(), style: st_b_20(fontWeight: FontWeight.w700)),
        sb_h_10,
        lText("돌봄일기작성감사합니다".tr(), style: st_16(textColor: color_999999, fontWeight: FontWeight.w500)),
      ]),
      btnColor: Commons.getColor(),
      finishAction: () => Commons.pageToMain(context, routeLinkmomDiary),
    );
  }
}
