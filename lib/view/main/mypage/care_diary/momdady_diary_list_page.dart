import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/carediary_list_request.dart';
import 'package:linkmom/data/network/models/carediary_list_response.dart';
import 'package:linkmom/data/network/models/child_info_request.dart';
import 'package:linkmom/data/network/models/child_info_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/custom_modal.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';

import 'diary_page_view.dart';

class MomdadyDiaryListPage extends BaseStateful {
  MomdadyDiaryListPage({this.data});

  final DataManager? data;

  @override
  _MomdadyDiaryListPageState createState() => _MomdadyDiaryListPageState();
}

class _MomdadyDiaryListPageState extends BaseStatefulState<MomdadyDiaryListPage> {
  CareDiaryListData _before = CareDiaryListData(results: []);
  CareDiaryListData _after = CareDiaryListData(results: []);
  List<String> _childList = ["전체".tr()];

  @override
  void initState() {
    super.initState();
    flag.enableLoading(fn: () => onUpDate());
    _requestChildList().then((value) => _childList.addAll(value));
    _requestDiaryList();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            context: context,
            appBar: appBar("돌봄일기".tr()),
            body: Stack(
              children: [
                DiaryPageView(
                  data: data,
                  tabName: [
                    TabItem(title: "작성완료".tr()),
                    TabItem(title: "작성대기".tr()),
                  ],
                  rightList: _before,
                  leftList: _after,
                  filterList: _childList,
                  onLeftRefresh: (next) => _requestAfterDiary(next: next),
                  onRightRefresh: (next) => _requestBeforeDiary(next: next),
                ),
                if (_after.results!.where((e) => !e.carediaryInfo!.momdadyConfirm).isNotEmpty)
                  Padding(
                      padding: EdgeInsets.only(top: kToolbarHeight),
                      child: NoticeModal(
                        message: "돌봄일기_좋아요".tr(),
                        useIcon: true,
                        backgroundColor: color_main,
                      )),
              ],
            )));
  }

  void _requestDiaryList() {
    try {
      _requestBeforeDiary();
      _requestAfterDiary();
      flag.disableLoading(fn: () => onUpDate());
    } catch (e) {
      log.e("Exception: >>>>>>>>> $e");
    }
  }

  void _requestBeforeDiary({bool next = false}) {
    CareDiaryListReqeust before = CareDiaryListReqeust(
      gubun: USER_TYPE.mom_daddy.index + 1,
      type: 0, // before write
    );
    if (next && _before.next.isEmpty) return;
    apiHelper.requestDiaryList(before, url: next ? _before.next : null).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        if (next) {
          if (_before.results == null) _before.results = [];
          _before.results!.addAll(response.getData().results!);
          _before.previous = response.getData().previous;
          _before.next = response.getData().next;
        } else {
          _before = response.getData();
        }
      } else {
        _before.results = [];
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  void _requestAfterDiary({bool next = false}) {
    CareDiaryListReqeust after = CareDiaryListReqeust(
      gubun: USER_TYPE.mom_daddy.index + 1,
      type: 1, // after write
    );
    if (next && _after.next.isEmpty) return;
    apiHelper.requestDiaryList(after, url: next ? _after.next : null).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        if (next) {
          if (_after.results == null) _after.results = [];
          _after.results!.addAll(response.getData().results!);
          _after.previous = response.getData().previous;
          _after.next = response.getData().next;
        } else {
          _after = response.getData();
        }
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  Future<List<String>> _requestChildList() async {
    var list;
    ChildInfoResponse response = await apiHelper.requestMyPageChildInfo(ChildInfoRequest(), HttpType.patch, ApiEndPoint.EP_MYPAGE_CHILD_INFO_LIST).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    if (response.getCode() == KEY_SUCCESS) {
      list = response.getDataList().map((e) => e.child_name).toList();
      flag.disableLoading(fn: () => onUpDate());
    }
    return list;
  }
}
