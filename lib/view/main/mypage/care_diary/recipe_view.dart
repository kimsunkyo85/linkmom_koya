import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

class RecipeView extends StatelessWidget {
  final USER_TYPE type;
  final String careDate;
  final String payDate;
  final bool showPayState;
  final ServiceType serviceType;
  final String endTime;
  final String startTime;
  final int hourPay;
  final int totalPay;
  final int tax;
  final EdgeInsets padding;
  final ViewType viewType;
  final bool isRefund;
  final int penaltyCount;
  final int cash;

  RecipeView({
    this.careDate = '',
    this.payDate = '',
    this.showPayState = false,
    this.serviceType = ServiceType.serviceType_1,
    this.endTime = '',
    this.startTime = '',
    this.hourPay = 0,
    this.totalPay = 0,
    this.tax = 0,
    this.type = USER_TYPE.mom_daddy,
    this.padding = padding_20,
    this.viewType = ViewType.normal,
    this.isRefund = false,
    this.penaltyCount = 0,
    this.cash = 0,
  });

  @override
  Widget build(BuildContext context) {
    int cost = (totalPay * (tax * 0.01)).round();
    return Container(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
            padding: padding_20,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                lText("정산일".tr(), style: st_b_18(fontWeight: FontWeight.w700)),
                lText(payDate, style: st_b_16()),
              ],
            )),
        lDivider(color: color_dedede.withAlpha(81), thickness: 8.0),
        Container(
            padding: padding_20,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                lText("서비스신청내역".tr(), style: st_b_18(fontWeight: FontWeight.w700)),
                sb_h_15,
                TypeComponent(type: "돌봄유형".tr(), data: serviceType.string, isExpanded: true),
                TypeComponent(type: "이용기간".tr(), data: careDate, isExpanded: true),
                TypeComponent(
                    type: "이용시간".tr(), data: StringUtils.differenceTime([startTime, endTime]), subData: '${StringUtils.parseAHMM(startTime)} ~ ${StringUtils.parseAHMM(endTime)}', subDataStyle: st_14(textColor: color_999999), isExpanded: true),
                TypeComponent(type: "설정시급".tr(), data: StringUtils.formatPay(hourPay) + "원".tr(), isExpanded: true),
              ],
            )),
        lDivider(color: color_dedede.withAlpha(81), thickness: 8.0),
        if (type == USER_TYPE.mom_daddy && !isRefund)
          Container(
              padding: padding_20,
              child: Column(
                children: [
                  TypeComponent(type: "정산내역".tr(), typeStyle: st_b_18(fontWeight: FontWeight.w700), isExpanded: true),
                  sb_h_10,
                  TypeComponent(
                      type: "정산_금액".tr(), data: StringUtils.formatPay(totalPay), subData: "원".tr(), typeStyle: st_b_16(), dataStyle: st_b_16(fontWeight: FontWeight.w700), subDataStyle: st_b_14(fontWeight: FontWeight.w500), isExpanded: true),
                ],
              )),
        Column(
            children: type == USER_TYPE.link_mom
                ? [
                    lDivider(color: color_dedede.withAlpha(81), thickness: 8.0),
                    Container(
                        padding: padding_20,
                        child: Column(
                          children: isRefund
                              ? [
                                  TypeComponent(type: "취소내역".tr(), typeStyle: st_b_18(fontWeight: FontWeight.w700), isExpanded: true),
                                  sb_h_20,
                                  TypeComponent(
                                      isExpanded: true,
                                      type: "캐시소득".tr(),
                                      typeStyle: st_b_16(fontWeight: FontWeight.w500),
                                      data: StringUtils.formatPay(cash),
                                      dataStyle: st_b_16(),
                                      icon: Padding(padding: padding_05_L, child: Lcons.cash(isEnabled: true))),
                                  TypeComponent(
                                      isExpanded: true,
                                      type: "플랫폼이용료".tr(),
                                      typeStyle: st_16(textColor: color_b2b2b2),
                                      data: StringUtils.formatPay(cost),
                                      dataStyle: st_16(textColor: color_b2b2b2),
                                      icon: Padding(padding: padding_05_L, child: Lcons.cash(isEnabled: true))),
                                  lDivider(color: Commons.getColor(), thickness: 2.0, padding: padding_20_TB),
                                  TypeComponent(
                                    isExpanded: true,
                                    type: "보상예정금액".tr(),
                                    typeStyle: st_b_18(fontWeight: FontWeight.w700),
                                    data: StringUtils.formatPay(cash),
                                    dataStyle: st_b_16(),
                                    icon: Padding(padding: padding_05_L, child: Lcons.cash(isEnabled: true)),
                                  ),
                                  TypeComponent(
                                    isExpanded: true,
                                    type: "총부과벌점스티커".tr(),
                                    typeStyle: st_b_18(fontWeight: FontWeight.w700),
                                    data: penaltyCount.toString(),
                                    dataStyle: st_16(fontWeight: FontWeight.w700, textColor: color_f0544c),
                                    subData: "개".tr(),
                                    subDataStyle: st_16(fontWeight: FontWeight.w700, textColor: color_f0544c),
                                  ),
                                ]
                              : [
                                  TypeComponent(type: "정산요금".tr(), typeStyle: st_b_18(fontWeight: FontWeight.w700), isExpanded: true),
                                  sb_h_20,
                                  TypeComponent(
                                      isExpanded: true,
                                      type: "캐시소득".tr(),
                                      typeStyle: st_b_16(fontWeight: FontWeight.w500),
                                      data: StringUtils.formatPay(totalPay),
                                      dataStyle: st_b_16(),
                                      icon: Padding(padding: padding_05_L, child: Lcons.cash(isEnabled: true))),
                                  TypeComponent(
                                      isExpanded: true,
                                      type: "플랫폼이용료".tr(),
                                      typeStyle: st_16(textColor: color_b2b2b2),
                                      data: StringUtils.formatPay(cost),
                                      dataStyle: st_16(textColor: color_b2b2b2),
                                      icon: Padding(padding: padding_05_L, child: Lcons.cash(isEnabled: true)),
                                      typeIcon: Lcons.rereply(),
                                      leftTypeIcon: true),
                                  lDivider(color: Commons.getColor(), thickness: 2.0, padding: padding_20_TB),
                                  TypeComponent(
                                      isExpanded: true,
                                      type: cash < 1 ? "정산예정소득".tr() : "총소득".tr(),
                                      typeStyle: st_b_18(fontWeight: FontWeight.w700),
                                      data: StringUtils.formatPay(cash < 1 ? totalPay - cost : cash),
                                      dataStyle: st_b_18(fontWeight: FontWeight.w700),
                                      icon: Padding(padding: padding_05_L, child: Lcons.cash(isEnabled: true))),
                                ],
                        )),
                  ]
                : []),
      ],
    ));
  }
}

class TypeComponent extends StatelessWidget {
  final String type;
  final String data;
  final String subData;
  final TextStyle? typeStyle;
  final TextStyle? dataStyle;
  final TextStyle? subDataStyle;
  final bool isExpanded;
  final Widget? icon;
  final Widget? typeIcon;
  final bool leftTypeIcon;
  final bool leftIcon;
  final EdgeInsets? margin;
  final int? leftFlex;
  final int? rightFlex;

  TypeComponent(
      {this.type = '',
      this.data = '',
      this.subData = '',
      this.typeStyle,
      this.dataStyle,
      this.subDataStyle,
      this.isExpanded = false,
      this.icon,
      this.typeIcon,
      this.leftTypeIcon = false,
      this.leftIcon = false,
      this.margin,
      this.leftFlex,
      this.rightFlex});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin ?? padding_03_TB,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: isExpanded
            ? [
                Row(children: [if (leftTypeIcon && typeIcon != null) typeIcon!, lText(type, style: typeStyle ?? st_b_16(), overflow: TextOverflow.ellipsis), if (!leftTypeIcon && typeIcon != null) typeIcon!]),
                Row(children: [
                  lText(data, style: dataStyle ?? st_b_16()),
                  if (subData.isNotEmpty) lText(subData, style: subDataStyle ?? st_14(textColor: color_999999)),
                  if (icon != null) icon!,
                ]),
              ]
            : [
                Flexible(
                    flex: leftFlex ?? 1, fit: FlexFit.tight, child: Row(children: [Container(width: Commons.width(context, 0.5), child: lText(type, style: typeStyle ?? st_b_16(), overflow: TextOverflow.ellipsis)), if (typeIcon != null) typeIcon!])),
                Flexible(
                    flex: rightFlex ?? 3,
                    fit: FlexFit.tight,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        lText(data, style: dataStyle ?? st_b_16(fontWeight: FontWeight.w500)),
                        sb_w_05,
                        if (leftIcon && icon != null) icon!,
                        if (subData.isNotEmpty) lText(subData, style: subDataStyle ?? st_14(textColor: color_999999)),
                        if (!leftIcon && icon != null) icon!,
                      ],
                    )),
              ],
      ),
    );
  }
}
