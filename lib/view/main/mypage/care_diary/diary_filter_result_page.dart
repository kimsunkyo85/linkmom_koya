import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/carediary_list_request.dart';
import 'package:linkmom/data/network/models/carediary_list_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

import 'diary_page_view.dart';
import 'diary_view_page.dart';
import 'diary_write_page.dart';

class DiaryFilterResultPage extends BaseStateful {
  final DataManager? data;
  final CareDiaryListResponse? resp;
  final int type;
  final USER_TYPE user;
  final DateTimeRange? range;
  final CareDiaryListReqeust? req;

  DiaryFilterResultPage({
    this.data,
    this.resp,
    this.type = 0,
    this.user = USER_TYPE.mom_daddy,
    required this.range,
    this.req,
  });

  @override
  _DiaryFilterResultPageState createState() => _DiaryFilterResultPageState();
}

class _DiaryFilterResultPageState extends BaseStatefulState<DiaryFilterResultPage> {
  late CareDiaryListResponse _resp;

  @override
  void initState() {
    super.initState();
    _resp = widget.resp ?? CareDiaryListResponse();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar("상세조회".tr(), hide: false),
        backgroundColor: color_fafafa,
        body: SliverNextListView(
          pinned: Filter(
            count: _resp.getData().count,
            onClickAction: () => Commons.pagePop(context),
            range: widget.range,
          ),
          view: (_resp.getData().results ?? [])
              .map(
                (e) => Container(
                    padding: padding_10_B,
                    child: widget.type == 1
                        ? AfterWriteDiary(
                            type: widget.user,
                            data: e,
                            onClickAction: () => Commons.page(context, routeDiaryView, arguments: DiaryViewPage(id: e.scheduleInfo!.scheduleId, type: storageHelper.user_type)),
                          )
                        : BeforeWriteDiary(
                            type: widget.user,
                            data: e,
                            onClickAction: () => _moveToDetail(
                              e.scheduleInfo!.scheduleId,
                              e.cancelinfo!.cancelId != 0,
                              StringUtils.parseYMDHM(e.scheduleInfo!.startCaredate),
                            ),
                          )),
              )
              .toList(),
          onRefresh: () => {},
          onNext: () => _requestNext(),
          showNextLoading: _resp.getData().next.isNotEmpty,
          emptyView: lEmptyView(
            height: Commons.height(context, 0.7),
            textString: "다른조건으로재검색".tr(),
            fn: () => Commons.pagePop(context),
          ),
        ),
      ),
    );
  }

  Future<void> _moveToDetail(int id, bool isCanceld, DateTime time, {PayStatus? status}) async {
    var ret;
    if (widget.type == 1) {
      if (isAfter(time)) {
        // NOTE: before care
        showNormalDlg(message: "돌봄일기_돌봄전".tr());
      } else {
        // NOTE: is linkmom and not canceled
        if (time.add(Duration(hours: 12)).isBefore(DateTime.now())) {
          Commons.page(
            context,
            routeDiaryView,
            arguments: DiaryViewPage(id: id, type: USER_TYPE.link_mom),
          );
        } else {
          if (status == PayStatus.delayed) {
            ret = await Commons.page(context, routeDiaryView, arguments: DiaryViewPage(id: id, type: storageHelper.user_type));
          } else {
            ret = await Commons.page(context, routeDiaryWrite, arguments: DiaryWritePage(id: id));
          }
        }
      }
    } else {
      ret = await Commons.page(context, routeDiaryView, arguments: DiaryViewPage(id: id, type: storageHelper.user_type));
    }
    if (ret != null) {
      Commons.pagePop(context);
    }
  }

  void _requestNext() {
    if (widget.resp != null && _resp.getData().next.isNotEmpty) {
      apiHelper.requestDiaryList(widget.req!, url: _resp.getData().next).then((response) {
        flag.disableLoading(fn: () => onUpDate());
        if (response.getCode() == KEY_SUCCESS) {
          _resp.dataList!.first.results!.addAll(response.getData().results!);
          _resp.dataList!.first.next = response.getData().next;
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    }
  }

  bool isAfter(DateTime time) => time.isAfter(DateTime.now());
}
