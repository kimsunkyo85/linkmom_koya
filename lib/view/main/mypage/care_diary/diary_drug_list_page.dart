import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/carediary_childdrug_ok_request.dart';
import 'package:linkmom/data/network/models/carediary_childdrug_view_request.dart';
import 'package:linkmom/data/network/models/carediary_childdrug_view_response.dart';
import 'package:linkmom/data/network/models/child_drug_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/child_info/child_drug_view.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class DiaryDrugListPage extends BaseStateful {
  final int drugId;
  final int scheduleId;
  final int diaryId;
  final String childName;
  final String momdadyName;
  final String linkmomName;
  final bool isDrug;
  final String? checkDate;
  final ViewType viewType;
  final Function onChecked;
  DiaryDrugListPage({this.isDrug = false, this.checkDate = '', this.viewType = ViewType.modify, required this.drugId, this.scheduleId = 0, this.diaryId = 0, this.childName = '', this.momdadyName = '', this.linkmomName = '', required this.onChecked});

  @override
  _ChildDrugListPageState createState() => _ChildDrugListPageState();
}

class _ChildDrugListPageState extends BaseStatefulState<DiaryDrugListPage> {
  CareDiaryDrugData _drugList = CareDiaryDrugData();
  PageController _pageController = PageController();
  List<int> _compleat = [];

  @override
  void initState() {
    super.initState();
    _requestDrugList();
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        appBar: appBar("투약의뢰서".tr(), isBack: false, isClose: true, onClick: (a) => Commons.pagePop(context, data: _compleat), hide: false),
        body: Container(
          height: data.height(context, 1),
          width: data.width(context, 1),
          child: _drugList.childdrugList != null
              ? Column(
                  children: [
                    if (_drugList.childdrugList!.length > 1)
                      Padding(
                          padding: padding_20_TB,
                          child: SmoothPageIndicator(
                            controller: _pageController,
                            count: _drugList.childdrugList!.length,
                            effect: ExpandingDotsEffect(
                              spacing: 6,
                              activeDotColor: color_545454,
                              dotColor: color_dedede,
                              dotWidth: 6,
                              dotHeight: 6,
                              radius: 8,
                            ),
                          )),
                    Expanded(
                        child: PageView(
                      controller: _pageController,
                      children: _drugList.childdrugList!
                          .map((e) => Container(
                                child: Column(
                                  children: [
                                    Expanded(child: lScrollView(padding: padding_0, child: ChildDrugView(_buildDrugData(e), widget.momdadyName))),
                                    lInkWell(
                                      onTap: () {
                                        if (!e.is_drug)
                                          showNormalDlg(
                                            context: context,
                                            message: "돌봄일기_투약완료_팝업".tr(),
                                            btnLeft: "취소".tr(),
                                            onClickAction: (action) {
                                              switch (action) {
                                                case DialogAction.yes:
                                                  _requestDrugOk(widget.scheduleId, e.childdruglist_id);
                                                  break;
                                                default:
                                              }
                                              onUpDate();
                                            },
                                          );
                                      },
                                      child: Container(
                                        height: data.height(context, 0.15),
                                        width: double.infinity,
                                        padding: padding_20,
                                        color: e.is_drug ? color_545454 : color_cecece,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Container(
                                                        decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius: BorderRadius.circular(36),
                                                      border: Border.all(color: e.is_drug ? color_545454 : color_dedede),
                                                    ),
                                                    child: Lcons.check(
                                                      color: color_545454,
                                                      disableColor: color_dedede,
                                                      size: 16,
                                                      isEnabled: e.is_drug,
                                                        )),
                                                sb_w_10,
                                                lText("돌봄일기_투약완료_안내".tr(), style: st_16(fontWeight: FontWeight.w500)),
                                              ],
                                            ),
                                            sb_h_10,
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                widget.viewType == ViewType.modify
                                                    ? lText("${StringUtils.parseYMDLocale(DateTime.now())}", style: st_16())
                                                    : lText(
                                                        widget.checkDate ?? StringUtils.parseYMDLocale(DateTime.now()),
                                                        style: st_16(),
                                                      ),
                                                sb_w_10,
                                                lText("${widget.linkmomName} ${"링크쌤".tr()}", style: st_16(fontWeight: FontWeight.w700)),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ))
                          .toList(),
                    ))
                  ],
                )
              : lEmptyView(title: "잠시만기다려주세요".tr()),
        ));
  }

  void _requestDrugList() {
    CareDiaryChildDrugViewRequest req = CareDiaryChildDrugViewRequest(carecontract_schedule: widget.scheduleId, childdrug_id: widget.drugId);
    apiHelper.requestDiaryDrugView(req).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        _drugList = response.getData();
        _compleat = response.getData().childdrugList!.where((e) => e.is_drug).map((e) => e.childdruglist_id).toList();
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }

  void _requestDrugOk(int scheduleId, int id) {
    try {
      widget.onChecked(true);
      onUpDate();
      apiHelper.requestDiaryDrugOk(CareDiaryChildDrugOkRequest(childdruglist_id: id, schedule_id: scheduleId)).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _drugList.childdrugList!.where((e) => e.childdruglist_id == id).first.is_drug = true;
        } else {
          _drugList.childdrugList!.where((e) => e.childdruglist_id == id).first.is_drug = false;
          widget.onChecked(false);
          showNormalDlg(message: response.getMsg());
        }
        onUpDate();
      });
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>>>>>> $e");
    }
  }

  ChildDrugReponseData _buildDrugData(ChildDrugList drug) {
    return ChildDrugReponseData(
      id: drug.childdruglist_id,
      child_name: _drugList.childName,
      user_signature: _drugList.userSignature,
      drug_date: _drugList.drugDate,
      druglists: [
        ChildDrugResData(
            drug_id: drug.childdruglist_id,
            drug_time: drug.drug_time,
            drug_repeat: drug.drug_repeat,
            drug_reqmessage: drug.drug_reqmessage,
            drug_amount_type0: drug.drug_amount_type0,
            drug_amount_type1: drug.drug_amount_type1,
            drug_amount_type2: drug.drug_amount_type2,
            drug_amount_type3: drug.drug_amount_type3,
            drug_keeping: drug.drug_keeping,
            drug_timetype: drug.drug_keeping,
            symptom: drug.symptom,
            drug_type: drug.drug_type.map((e) => EnumUtils.getDrugTypeIndex(e)).toList(),
            is_drug: drug.is_drug)
      ],
      createdate: drug.createdate,
    );
  }
}
