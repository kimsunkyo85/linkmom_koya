import 'package:flutter/material.dart';
import 'package:linkmom/data/storage/model/menu_file_data.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

class CareTaskView extends StatefulWidget {
  final List<int>? task;
  List<int>? compleatTask;
  final List<ServiceItem>? services;
  final Function? updateEvent;
  final bool selectable;
  // final
  CareTaskView({this.compleatTask, this.task, this.services, this.updateEvent, this.selectable = true}) {
    if (this.compleatTask == null) this.compleatTask = [];
  }

  @override
  _CareTaskViewState createState() => _CareTaskViewState();
}

class _CareTaskViewState extends State<CareTaskView> {
  @override
  Widget build(BuildContext context) {
    return lScrollView(
      padding: padding_0,
      child: Column(
          children: widget.services!
              .map((e) => e.products != null && e.products!.isNotEmpty
                  ? Container(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      lDingbatText(dingbatColor: Commons.getColor(), dingbatSize: 5, text: e.cares!.type_name, style: st_b_18(fontWeight: FontWeight.w700), width: 200, padding: padding_20_TB),
                        GridView.count(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            childAspectRatio: 72 / 77,
                            crossAxisCount: 4,
                            children: e.products!
                                .map((item) => lInkWell(
                                      onTap: () {
                                        setState(() {
                                        if (!widget.selectable) return;
                                          if (widget.compleatTask!.contains(item.product_id)) {
                                            widget.compleatTask!.remove(item.product_id);
                                          } else {
                                            widget.compleatTask!.add(item.product_id);
                                          }
                                          if (widget.updateEvent != null) widget.updateEvent!(widget.compleatTask);
                                        });
                                      },
                                      child: Container(
                                          child: Column(
                                        children: [
                                          Container(
                                            child: Lcons(
                                            Lcons.getServiceIcon(item.caretype, item.product_id).name,
                                            isEnabled: widget.compleatTask!.contains(item.product_id),
                                            color: Commons.getColor(),
                                            disableColor: color_b2b2b2,
                                          size: 48,
                                        )),
                                          Container(
                                          padding: padding_10_T,
                                          child: lText("${item.product_data!.name}",
                                              style: st_14(textColor: widget.compleatTask!.contains(item.product_id) ? Commons.getColor() : color_b2b2b2, fontWeight: FontWeight.w700), maxLines: 1, overflow: TextOverflow.ellipsis),
                                          )
                                        ],
                                      )),
                              ))
                                .toList())
                    ],
                  ))
                  : Container())
              .toList()),
    );
  }
}
