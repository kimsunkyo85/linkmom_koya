import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:highlight_text/highlight_text.dart' as plugin;
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/carediary_claim_cancel_request.dart';
import 'package:linkmom/data/network/models/carediary_claim_view_request.dart';
import 'package:linkmom/data/network/models/carediary_payment_request.dart';
import 'package:linkmom/data/network/models/data/care_diary_claim_data.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/compleat_view_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_claim_answer_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/payment/payment_history_page.dart';

import 'recipe_view_page.dart';

class DiaryClaimViewPage extends BaseStateful {
  final int claimId;
  final ViewType viewType;
  DiaryClaimViewPage({
    this.claimId = 0,
    this.viewType = ViewType.view,
  });

  @override
  _DiaryLinkmomClaimPageState createState() => _DiaryLinkmomClaimPageState();
}

class _DiaryLinkmomClaimPageState extends BaseStatefulState<DiaryClaimViewPage> {
  CareDiaryClaimData _claim = CareDiaryClaimData();
  @override
  void initState() {
    super.initState();
    _requestClaimView(widget.claimId);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar("정산보류".tr(), hide: false),
            body: LinkmomRefresh(
              onRefresh: () => _requestClaimView(widget.claimId),
              padding: padding_0,
              child: Column(
                children: [
                  Container(
                    height: Commons.height(context, 1) - 200,
                    child: ClaimView(_claim, view: widget.viewType, momdaddyName: _claim.momdadyName),
                  ),
                  _buildBtn(),
                ],
              ),
            )));
  }

  void _requestClaimView(int id) {
    try {
      CareDiaryClaimViewRequest req = CareDiaryClaimViewRequest(claim_id: id);
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestDiaryClaimView(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _claim = response.getData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>> $e");
    }
  }

  void _requestClaimCancel() {
    try {
      CareDiaryClaimCancelRequest req = CareDiaryClaimCancelRequest(claim_id: widget.claimId);
      apiHelper.requestDiaryClaimCancel(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.page(context, routeCompleteView,
              arguments: CompleteViewPage(
                data: data,
                title: "정산보류취소".tr(),
                resultMessage: "정산_정산보류_취소".tr(),
                footer: lText("정산_정산보류취소후정산".tr(), style: st_16(textColor: color_545454), textAlign: TextAlign.center),
                btnColor: Commons.getColor(),
                btn: MultiBtn(
                  disableBtn: 0,
                  btnTextlist: [
                    "결제취소하기".tr(),
                    "정산하기".tr(),
                  ],
                  onClickActions: [
                    () => Commons.pageToMain(context, routePaymentHistory, arguments: PaymentHistoryPage()),
                    () {
                      flag.enableLoading(fn: () => onUpDate());
                      apiHelper.reqeustDiaryPaymentPatch(CarediaryPaymentRequest(scheduleId: _claim.schedule)).then((response) {
                        if (response.getCode() == KEY_SUCCESS) {
                          Commons.page(
                            context,
                            routeRecipe,
                            arguments: RecipeViewPage(
                              appbarTitle: "정산완료".tr(),
                              id: _claim.schedule,
                              type: storageHelper.user_type,
                              status: PayStatus.compleat,
                            ),
                          );
                        }
                        flag.disableLoading(fn: () => onUpDate());
                      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
                    }
                  ],
                ),
              ));
        } else {
          showNormalDlg(message: "이미취소되었습니다".tr());
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>> $e");
    }
  }

  Widget _buildBtn() {
    if (Commons.isLinkMom()) {
      if (_claim.content2.isNotEmpty) {
        return lBtn("확인".tr(), onClickAction: () {
          Commons.pagePop(context, data: true);
        });
      } else {
        return lBtn("소명하기".tr(), onClickAction: () {
          Commons.page(
            context,
            routeClaimAnswer,
            arguments: DiaryClaimAnswerPage(id: _claim.schedule, claimReason: _claim.type),
          );
        });
      }
    } else {
      return lBtn("정산보류취소하기".tr(),
        onClickAction: () => _requestClaimCancel());
    }
  }
}

class ClaimView extends StatelessWidget {
  final CareDiaryClaimData claim;
  final ViewType view;
  final String momdaddyName;
  ClaimView(this.claim, {this.view = ViewType.view, this.momdaddyName = ''});

  @override
  Widget build(BuildContext context) {
    return lScrollView(
      padding: padding_20,
      child: Column(
        children: [
          _buildClaimReq(context),
          claim.content2.isNotEmpty ? _buildClaimResp(context) : _buildInfo(context),
        ],
      ),
    );
  }

  Widget _buildClaimReq(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (claim.answer.isNotEmpty) lTitleBodyView("관리자답변".tr(), claim.answer, backgroundColor: Commons.isLinkMom() ? color_f7f3f9 : color_eef8f6, borderColor: Colors.transparent),
        if (claim.answer.isEmpty && momdaddyName.isNotEmpty) lText(Commons.isLinkMom() ? "$momdaddyName ${"정산_정산보류_안내".tr()}" : "정산_정산보류_상태".tr(), style: st_18(fontWeight: FontWeight.w700, textColor: Commons.getColor())),
        lTitleBodyView(
          "${"보류사유".tr()} : ${EnumUtils.getDiaryClaim(claim.type).reason}",
          claim.content1,
          borderColor: Colors.transparent,
          backgroundColor: color_eeeeee.withOpacity(0.42),
        ),
        if (claim.carediaryClaimImages1 != null && claim.carediaryClaimImages1!.isNotEmpty)
          imageThumbnailView(
            context,
            claim.carediaryClaimImages1!.map((e) => e.imageThumbnail).toList(),
            origin: claim.carediaryClaimImages1!.map((e) => e.image).toList(),
          ),
      ],
    );
  }

  Widget _buildClaimResp(BuildContext context) {
    return Column(
      children: [
        lDivider(color: color_eeeeee, padding: padding_20_TB),
        lTitleBodyView(
          (view == ViewType.view ? USER_TYPE.link_mom.name + "의".tr() + ' ' : '') + "소명내용".tr(),
          claim.content2,
          backgroundColor: color_eeeeee.withOpacity(0.42),
          borderColor: Colors.transparent,
        ),
        if (claim.carediaryClaimImages2 != null && claim.carediaryClaimImages2!.isNotEmpty)
          imageThumbnailView(
            context,
            claim.carediaryClaimImages2!.map((e) => e.imageThumbnail).toList(),
            origin: claim.carediaryClaimImages2!.map((e) => e.image).toList(),
          ),
        if (claim.answer.isEmpty)
          lDingbatText(
              width: Commons.width(context, 0.85),
              dingbatColor: color_545454,
              dingbatSize: 2,
              padding: padding_10_TB,
              child: TextHighlight(
                text: "정산_정산보류_소명안내_2_3".tr(),
                term: "정산보류안내_하이라이트".tr(),
                textStyle: st_b_14(textColor: color_545454),
                textStyleHighlight: st_14(textColor: color_ff3b30),
              )),
      ],
    );
  }

  Widget _buildInfo(BuildContext context) {
    if (view != ViewType.view) {
    return Column(children: [
      lDingbatText(
        text: "정산_정산보류_소명안내_1".tr(),
        style: st_14(textColor: color_545454),
        dingbatColor: color_545454,
        dingbatSize: 2,
        padding: padding_02_B,
        width: Commons.width(context, 0.85),
      ),
      lDingbatText(
          dingbatColor: color_545454,
          dingbatSize: 2,
          padding: padding_02_B,
          width: Commons.width(context, 0.85),
          child: TextHighlight(
            text: "${"정산_정산보류_소명안내_2_1".tr()} (${StringUtils.payDeadline(claim.createdateAfter24)}${"까지".tr()}) ${"정산_정산보류_소명안내_2_2".tr()}",
            term: '(${StringUtils.payDeadline(claim.createdateAfter24)}${"까지".tr()})',
            textStyle: st_b_14(textColor: color_545454),
            textStyleHighlight: st_14(textColor: color_ff3b30),
          )),
      lDingbatText(
          dingbatColor: color_545454,
          dingbatSize: 2,
          padding: padding_02_B,
          width: Commons.width(context, 0.85),
          child: TextHighlight(
            text: "${"정산_정산보류_소명안내_3".tr()}",
            term: "48시간이내".tr(),
            textStyle: st_b_14(textColor: color_545454),
            textStyleHighlight: st_14(textColor: color_ff3b30),
          )),
      ]);
    } else {
      return Column(children: [
        lDingbatText(
            dingbatColor: color_545454,
            dingbatSize: 2,
            padding: padding_02_B,
            width: Commons.width(context, 0.85),
            child: TextHighlight(
              text: "정산_정산보류_소명안내_2_3".tr(),
              term: "정산보류안내_하이라이트".tr(),
              textStyle: st_b_14(textColor: color_545454),
              textStyleHighlight: st_14(textColor: color_ff3b30),
            )),
        lDingbatText(
            dingbatColor: color_545454,
            dingbatSize: 2,
            padding: padding_02_B,
            width: Commons.width(context, 0.85),
            child: plugin.TextHighlight(
              text: "맘대디_보류안내_1".tr(),
              textStyle: st_b_14(textColor: color_545454),
              words: {
                "3차례".tr(): plugin.HighlightedWord(textStyle: st_14(textColor: color_ff3b30)),
                "소명요청".tr(): plugin.HighlightedWord(textStyle: st_14(textColor: color_ff3b30)),
                "소명이없을경우맘대디".tr(): plugin.HighlightedWord(textStyle: st_14(textColor: color_ff3b30)),
              },
            )),
      ]);
    }
  }
}
