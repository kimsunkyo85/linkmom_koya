import 'package:highlight_text/highlight_text.dart' as high;

import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/care_list_response.dart';
import 'package:linkmom/data/network/models/carediary_form_request.dart';
import 'package:linkmom/data/network/models/carediary_form_response.dart';
import 'package:linkmom/data/network/models/carediary_save_request.dart';
import 'package:linkmom/data/network/models/carediary_update_request.dart';
import 'package:linkmom/data/network/models/data/care_child_info_data.dart';
import 'package:linkmom/data/network/models/matching_init_request.dart';
import 'package:linkmom/data/providers/image_loader_provider.dart';
import 'package:linkmom/data/storage/flag_manage.dart';
import 'package:linkmom/data/storage/model/menu_file_data.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/custom_modal.dart';
import 'package:linkmom/utils/listview/list_momdady_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/board/board_view.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/job_profile/momdady_profile_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_claim_page.dart';
import 'package:provider/provider.dart';

import 'diary_care_list_page.dart';
import 'diary_drug_list_page.dart';

class DiaryWritePage extends BaseStateful {
  final int id;
  DiaryWritePage({this.id = 0});

  @override
  _DiaryWritePageState createState() => _DiaryWritePageState();
}

class _DiaryWritePageState extends BaseStatefulState<DiaryWritePage> {
  CareDiaryFormData _form = CareDiaryFormData(caredate: []);
  TextEditingController _controller = TextEditingController();
  DiaryTaskInfo _task = DiaryTaskInfo();
  bool _isBefore = true;
  double _opacity = 1;
  bool _showModal = true;
  bool _isLate = false;

  @override
  void initState() {
    super.initState();
    _requestDiaryForm(widget.id);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      context.read<ImageLoader>().setUpdateCallback = onUpDate;
    });
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
          context: context,
          appBar: diaryAppbar(
              title: "돌봄일기".tr(),
              hide: false,
              rightW: _isBefore
                  ? lInkWell(
                      onTap: () {
                        if (context.read<ImageLoader>().getImages.isNotEmpty) {
                          _requestDiarySave(exit: true);
                        } else {
                          showNormalDlg(message: "사진은필수".tr());
                        }
                      },
                      child: Container(
                        alignment: Alignment.center,
                        padding: padding_20_LR,
                        child: lText("등록하기".tr(), style: st_15(fontWeight: FontWeight.w500, textColor: color_b579c8)),
                      ))
                  : Container()),
          body: Stack(
            children: [
              Container(
                  child: _form.carecontractSchedule != 0
                      ? ListView(
                          children: [
                            Container(
                                color: color_dedede.withAlpha(82),
                                child: lInkWell(
                                  onTap: () => Commons.page(context, routeMomDadyProfile,
                                      arguments: MomDadyProfilePage(
                                        data: data,
                                        viewMode: ViewMode(viewType: ViewType.view, itemType: ItemType.contract),
                                        bookingId: _form.bookingcareservices,
                                        matchingData: MatchingInitRequest(
                                          matchingStatus: MatchingStatus.paid.value,
                                          receiver: _form.userinfo!.userId,
                                          bookingcareservices: _form.bookingcareservices,
                                          carematching: _form.matching,
                                        ),
                                        btnType: BottomBtnType.not,
                                      )),
                                  child: Container(
                                      margin: padding_20,
                                      padding: padding_20,
                                      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16), boxShadow: [
                                        BoxShadow(
                                          color: Colors.black.withAlpha(11),
                                          offset: Offset(0, 4),
                                          blurRadius: 12,
                                        )
                                      ]),
                                      child: MomDadyView(
                                        CareListResultsData(
                                          booking_id: 0,
                                          cost_sum: _form.costSum,
                                          servicetype: _form.servicetype,
                                          possible_area: _form.possibleArea,
                                          stime: _form.stime,
                                          etime: _form.etime,
                                          caredate: _form.caredate,
                                          userinfo: CareUserInfoData(
                                            first_name: _form.userinfo!.firstName,
                                            user_id: _form.userinfo!.userId,
                                            region_3depth: _form.userinfo!.region3Depth,
                                            region_4depth: _form.userinfo!.region4Depth,
                                            profileimg: _form.userinfo!.profileimg,
                                            is_follower: false,
                                          ),
                                          childinfo: CareChildInfoData(
                                            child_name: _form.childinfo!.childName,
                                            child_gender: _form.childinfo!.childGender,
                                            child_age: _form.childinfo!.childAge,
                                          ),
                                        ),
                                        showFollow: false,
                                        viewMode: ViewType.normal,
                                        borderColor: Colors.white,
                                      )),
                                )),
                            if (EnumUtils.getCareType(name: _form.servicetype) != CareType.care)
                              Container(
                                width: double.infinity,
                                padding: padding_30_TB,
                                decoration: BoxDecoration(color: Colors.white, border: Border(bottom: BorderSide(color: color_dedede))),
                                child: Column(children: [
                                  lText(_form.servicetype + "이완료되었습니까".tr(), style: st_b_16(fontWeight: FontWeight.w700)),
                                  sb_h_10,
                                  lTextBtn("${_form.servicetype}완료".tr(),
                                      style: st_14(fontWeight: FontWeight.w700),
                                      rightIcon: Lcons.check(
                                        color: Colors.white,
                                        size: 15,
                                        isEnabled: true,
                                      ),
                                      radius: 32,
                                      bg: _task.isCompleteServ ? color_b579c8 : color_dedede, onClickEvent: () {
                                    if (!_task.isCompleteServ) {
                                      showNormalDlg(
                                          context: context,
                                          message: "아이가".tr() + _form.servicetype + "완료_메세지".tr(),
                                          btnLeft: "취소".tr(),
                                          onClickAction: (action) {
                                            switch (action) {
                                              case DialogAction.yes:
                                                _task.isCompleteServ = true;
                                                _requestDiarySave();
                                                break;
                                              default:
                                            }
                                            onUpDate();
                                          });
                                    }
                                  }),
                                ]),
                              ),
                            if (_task.drugCnt > 0)
                              Container(
                                  padding: padding_20,
                                  decoration: BoxDecoration(color: Colors.white, border: Border(bottom: BorderSide(color: color_dedede))),
                                  child: lInkWell(
                                      onTap: () => _moveDrug(_form.carediaryInfo!.childdrugId),
                                      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                        Row(children: [
                                          Lcons.check(color: color_b579c8, isEnabled: _task.isCompleteDrug, disableColor: color_cecece),
                                          sb_w_10,
                                          lCountText(
                                            _task.isCompleteDrug ? "돌봄일기_투약완료".tr() : "돌봄일기_투약하기".tr(),
                                            _task.completeDrugCnt,
                                            _task.drugCnt,
                                          )
                                        ]),
                                        Lcons.nav_right(color: color_cecece, size: 20)
                                      ]))),
                            Container(
                                padding: padding_20,
                                color: Colors.white,
                                child: lInkWell(
                                  onTap: () async {
                                    var ret = await Commons.page(context, routeDiaryCareList,
                                        arguments: DiaryCareListPage(
                                            options: _form.carediaryInfo!.option,
                                            compleats: _form.carediaryInfo!.completeOption,
                                            type: USER_TYPE.link_mom,
                                            service: EnumUtils.getServiceType(name: _form.servicetype),
                                            area: _form.possibleArea.contains(USER_TYPE.mom_daddy.name) ? 1 : 2,
                                            onSelected: (key, compleated) {
                                              if (_form.carediaryInfo!.completeOption == null) _form.carediaryInfo!.completeOption = CompleteOption(completeBoyuk: [], completeHomecare: [], completePlay: []);
                                              switch (key) {
                                                case ServicesData.Key_boyuks:
                                                  _form.carediaryInfo!.completeOption!.completeBoyuk = compleated;
                                                  break;
                                                case ServicesData.Key_homecares:
                                                  _form.carediaryInfo!.completeOption!.completeHomecare = compleated;
                                                  break;
                                                case ServicesData.Key_plays:
                                                  _form.carediaryInfo!.completeOption!.completePlay = compleated;
                                                  break;
                                                default:
                                              }
                                              _task.setCares = 0;
                                              _task.updateCares = _form.carediaryInfo!.completeOption!.completeBoyuk.length;
                                              _task.updateCares = _form.carediaryInfo!.completeOption!.completeHomecare.length;
                                              _task.updateCares = _form.carediaryInfo!.completeOption!.completePlay.length;
                                              onUpDate();
                                            }));
                                    if (ret != null) _requestDiarySave();
                                  },
                                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                    Row(children: [
                                      Lcons.check(color: color_b579c8, disableColor: color_cecece, isEnabled: _task.careCnt == _task.completeCareCnt),
                                      sb_w_10,
                                      lCountText("진행한돌봄에체크하기".tr(), _task.completeCareCnt, _task.careCnt),
                                    ]),
                                    Lcons.nav_right(color: color_cecece)
                                  ]),
                                )),
                            lDivider(color: color_eeeeee, thickness: 8.0),
                            ChangeNotifierProvider<ImageLoader>(
                              create: (_) => ImageLoader(),
                              builder: (ctx, widget) {
                                return BoardWriteView(
                                  controller: _controller,
                                  image: context.watch<ImageLoader>().getImages,
                                  hint: "돌봄일기_힌트_1".tr(),
                                  hintWidget: lText(
                                    "돌봄일기_힌트".tr(),
                                    style: st_18(textColor: color_b2b2b2, fontWeight: FontWeight.w700),
                                  ),
                                  update: () => onUpDate(),
                                  addImageAction: (images) => context.read<ImageLoader>().setImages = images,
                                );
                              },
                            ),
                          ],
                        )
                      : lEmptyView(height: data.height(context, 0.5))),
              if (!_isLate && Flags.isShowingCachedFlag(Flags.KEY_DIARY_L) && _showModal)
                Positioned(
                  bottom: 20,
                  right: 0,
                  left: 0,
                  child: _buildWriteModal(),
                ),
            ],
          )),
      showText: false,
    );
  }

  void _requestDiarySave({bool exit = false}) {
    try {
      if (exit) flag.enableLoading(fn: () => onUpDate());
      if (_form.carediaryInfo!.id == 0) {
        _saveDiary(exit);
      } else {
        _updateDiary(exit);
      }
    } catch (e) {
      log.e("Exception: >>>>>>>>>>>>>>>>>>> $e");
    }
  }

  void _requestDiaryForm(int id, {bool reload = false}) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      CareDiaryFormRequest req = CareDiaryFormRequest(schedule_id: id);
      apiHelper.requestDiaryForm(req).then((response) async {
        if (response.getCode() == KEY_SUCCESS) {
          _form = response.getData();
          if (_form.carediaryInfo!.option != null) {
            int count = 0;
            count = _form.carediaryInfo!.option!.boyukoptions.length;
            count += _form.carediaryInfo!.option!.homecareoptions.length;
            count += _form.carediaryInfo!.option!.playoptions.length;
            _task.initCares = count;
          }
          if (_form.carediaryInfo!.completeOption != null) {
            _task.setCares = _form.carediaryInfo!.completeOption!.completeBoyuk.length;
            _task.updateCares = _form.carediaryInfo!.completeOption!.completeHomecare.length;
            _task.updateCares = _form.carediaryInfo!.completeOption!.completePlay.length;
          }
          _controller.text = _form.carediaryInfo!.content;
          _task.initDrugs = _form.carediaryInfo!.childdrugList!.length;
          _task.setServ = _form.carediaryInfo!.isServicetype == 1;
          _task.setDrugs = _form.carediaryInfo!.childdrugList!.where((element) => element.isDrug).length;
          if (_form.carediaryImages!.isNotEmpty) {
            context.read<ImageLoader>().loadImage(_form.carediaryImages!.map((e) => e.images).toList(), context);
          } else {
            context.read<ImageLoader>().clear();
          }
        }
        flag.disableLoading(fn: () => onUpDate());
        // NOTE: if After 12 hours from end care time.
        if (EnumUtils.getPayStatus(name: _form.payStatus) == PayStatus.delayed && !reload && !_form.claimInfo!.claimAnswer) {
          DateTime time = StringUtils.formatYMDHMS(_form.claimInfo!.claimAfterDay).add(Duration(hours: 24));
          showNormalDlg(
            message: '${_form.carediaryInfo!.momdadyName}${"맘대디의정산보류요청이".tr()} ${StringUtils.formatMMDDHH(time)}${"소명을진행해주세요".tr()}',
            onClickAction: (a) {
              if (a == DialogAction.yes) _moveClaimPage(_form.claimInfo!.claimId);
            },
          );
        }
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e("Exception: >>>>>>>>>>>>>>>>>>> $e");
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _saveDiary(bool exit) {
    try {
      // NOTE: 1 = compleat
      CareDiarySaveRequest req = CareDiarySaveRequest(
        carecontract_schedule: widget.id,
        is_servicetype: _task.isCompleteServ ? 1 : 0,
        is_childdrug: _task.isCompleteDrug ? 1 : 0,
        content: _controller.text,
        image: context.read<ImageLoader>().getImages,
        complete_boyuk: _form.carediaryInfo!.completeOption!.completeBoyuk.join(','),
        complete_homecare: _form.carediaryInfo!.completeOption!.completeHomecare.join(','),
        complete_play: _form.carediaryInfo!.completeOption!.completePlay.join(','),
      );
      apiHelper.requestDiarySave(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _form.carediaryInfo!.id = response.getData().id;
          if (exit) _writeComplete();
        } else {
          showNormalDlg(context: context, message: response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e("EXCEPTION : >>>>>>>>>>>>>>> $e");
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _updateDiary(bool exit) {
    try {
      // NOTE: 1 = compleat
      CareDiaryUpdateRequest req = CareDiaryUpdateRequest(
        id: _form.carediaryInfo!.id,
        carecontract_schedule: _form.carecontractSchedule,
        is_servicetype: _task.isCompleteServ ? 1 : 0,
        is_childdrug: _task.isCompleteDrug ? 1 : 0,
        content: _controller.text,
        image: context.read<ImageLoader>().getImages,
        complete_boyuk: _form.carediaryInfo!.completeOption!.completeBoyuk.join(','),
        complete_homecare: _form.carediaryInfo!.completeOption!.completeHomecare.join(','),
        complete_play: _form.carediaryInfo!.completeOption!.completePlay.join(','),
      );
      apiHelper.requestDiaryUpdate(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _form.carediaryInfo!.id = response.getData().id;
          if (exit) _writeComplete();
        } else {
          showNormalDlg(context: context, message: response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>>>>>>>>>>>>> $e");
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  Future<void> _moveDrug(int drugId) async {
    var ret = await Commons.page(context, routeDiaryDrugList,
        arguments: DiaryDrugListPage(
          linkmomName: auth.user.my_info_data!.first_name,
          momdadyName: _form.userinfo!.firstName,
          drugId: drugId,
          scheduleId: widget.id,
          diaryId: _form.carediaryInfo!.id,
          onChecked: (checked) => {},
        ));
    if (ret is List) {
      _requestDiaryForm(widget.id, reload: true);
    }
  }

  void _writeComplete() {
    Commons.pageReplace(context, routeDiaryWriteComplete);
  }

  void _moveClaimPage(int claimId) {
    Commons.page(context, routeClaimView,
        arguments: DiaryClaimViewPage(
          claimId: claimId,
          viewType: ViewType.modify,
        ));
  }

  Widget _buildWriteModal() {
    return AnimatedOpacity(
      opacity: _opacity,
      duration: Duration(milliseconds: 300),
      onEnd: () {
        _showModal = false;
        onUpDate();
      },
      child: Container(
        margin: padding_20_LR,
        child: ReleativeModal(
          showPin: false,
          onClickAction: () {
            _opacity = _opacity == 1 ? 0 : 1;
            Flags.shownCachedFlag(Flags.KEY_DIARY_L);
            onUpDate();
          },
          color: color_linkmom,
          icon: Lcons.info_t(color: color_222222, isEnabled: true),
          title: "돌봄일기_작성안내_타이틀".tr(),
          titleStyle: st_16(fontWeight: FontWeight.w700, textColor: color_545454),
          bodyWidget: Container(
            padding: padding_10,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                high.TextHighlight(
                  text: "돌봄일기_작성안내_안내".tr(),
                  textStyle: st_14(textColor: color_545454, fontWeight: FontWeight.w500, height: 1.5),
                  words: {
                    "돌봄일기_작성안내_안내_2".tr(): high.HighlightedWord(
                      textStyle: st_14(textColor: color_linkmom, fontWeight: FontWeight.w500, height: 1.5),
                      onTap: () => {},
                    ),
                    "돌봄일기_작성안내_안내_3".tr(): high.HighlightedWord(
                      textStyle: st_14(textColor: color_linkmom, fontWeight: FontWeight.w500, height: 1.5),
                      onTap: () => {},
                    ),
                  },
                ),
                sb_h_10,
                high.TextHighlight(
                  text: "돌봄일기_작성안내_안내_1".tr(),
                  textStyle: st_14(textColor: color_545454, fontWeight: FontWeight.w500, height: 1.5),
                  words: {
                    "돌봄일기_작성안내_안내_4".tr(): high.HighlightedWord(
                      textStyle: st_14(textColor: color_linkmom, fontWeight: FontWeight.w500, height: 1.5),
                      onTap: () => {},
                    ),
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class DiaryTaskInfo {
  int drugCnt;
  int completeDrugCnt;
  int careCnt;
  int completeCareCnt;
  bool isCompleteServ;
  bool isCompleteDrug;
  bool isCompleteCares;

  DiaryTaskInfo({
    this.drugCnt = 0,
    this.completeDrugCnt = 0,
    this.careCnt = 0,
    this.completeCareCnt = 0,
    this.isCompleteServ = false,
    this.isCompleteDrug = false,
    this.isCompleteCares = false,
  });

  set initDrugs(int cnt) {
    this.drugCnt = cnt;
  }

  set setDrugs(int cnt) {
    this.completeDrugCnt = cnt;
    if (drugCnt > 0) this.isCompleteDrug = this.completeDrugCnt == this.drugCnt;
  }

  set initCares(int cnt) {
    this.careCnt = cnt;
    if (careCnt > 0) this.isCompleteCares = this.completeCareCnt == this.careCnt;
  }

  set setCares(int cnt) {
    this.completeCareCnt = cnt;
    if (careCnt > 0) this.isCompleteCares = this.completeCareCnt == this.careCnt;
  }

  set updateCares(int cnt) {
    this.completeCareCnt += cnt;
    if (careCnt > 0) this.isCompleteCares = this.completeCareCnt == this.careCnt;
  }

  set setServ(bool status) {
    this.isCompleteServ = status;
  }
}
