import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/carediary_childdrug_ok_request.dart';
import 'package:linkmom/data/network/models/child_drug_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/child_info/child_drug_view.dart';
import 'package:easy_localization/easy_localization.dart';

class DiaryDrugPage extends BaseStateful {
  final DataManager? data;
  final String momdadyName;
  final String linkmomName;
  final bool isDrug;
  final String? checkDate;
  final ViewType viewType;
  final int diaryId;
  final int drugId;
  final int scheduleId;
  final ChildDrugReponseData? drugData;
  DiaryDrugPage({this.drugId = 0, this.scheduleId = 0, this.data, this.momdadyName = ' ', this.isDrug = false, this.checkDate, this.linkmomName = ' ', this.viewType = ViewType.normal, this.diaryId = 0, this.drugData});

  @override
  _DiaryDrugPageState createState() => _DiaryDrugPageState();
}

class _DiaryDrugPageState extends BaseStatefulState<DiaryDrugPage> {
  bool _checked = false;
  ChildDrugReponseData _drug = ChildDrugReponseData();
  @override
  void initState() {
    super.initState();
    _checked = widget.isDrug;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () => Commons.pagePop(context, data: _checked),
        child: lScaffold(
            body: Container(
              child: Column(
                children: [
                  widget.drugData != null ? Expanded(child: ChildDrugView(widget.drugData!, widget.momdadyName)) : lEmptyView(title: "잠시만기다려주세요".tr()),
                  Container(
                    height: data.height(context, 0.15),
                    width: double.infinity,
                    padding: padding_20,
                    color: color_545454,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            lInkWell(
                                onTap: () {
                                  showNormalDlg(
                                    context: context,
                                    message: "돌봄일기_투약완료_팝업".tr(),
                                    btnLeft: "취소".tr(),
                                    onClickAction: (action) {
                                      switch (action) {
                                        case DialogAction.yes:
                                          _requestDrugOk(widget.scheduleId, widget.drugId);
                                          break;
                                        default:
                                      }
                                      onUpDate();
                                  });
                                },
                                child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(36),
                                      border: Border.all(color: _checked ? color_545454 : color_dedede),
                                    ),
                                    child: Lcons.check(color: color_545454, disableColor: color_dedede, isEnabled: _checked, size: 16
                                    ))),
                            sb_w_10,
                            lText("돌봄일기_투약완료_안내".tr(), style: st_16(fontWeight: FontWeight.w500)),
                          ],
                        ),
                        sb_h_10,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            widget.viewType == ViewType.modify
                            ? lText("${StringUtils.parseYMDLocale(DateTime.now())}", style: st_16())
                            : lText(
                                widget.checkDate ?? StringUtils.parseYMDLocale(DateTime.now()),
                                style: st_16(),
                              ),
                            sb_w_10,
                            lText("${widget.linkmomName} ${"링크쌤".tr()}", style: st_16(fontWeight: FontWeight.w700)),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            )));
  }

  void _requestDrugOk(int scheduleId, int id) {
    try {
      apiHelper.requestDiaryDrugOk(CareDiaryChildDrugOkRequest(childdruglist_id: id, schedule_id: scheduleId)).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _checked = true;
        }
        onUpDate();
      });
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>>>>>> $e");
    }
  }
}
