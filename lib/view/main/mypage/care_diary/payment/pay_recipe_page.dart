import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PaymentRecipePage extends BaseStateful {
  final String url;
  PaymentRecipePage({this.url = ''});
  @override
  _PayRecipePageState createState() => _PayRecipePageState();
}

class _PayRecipePageState extends BaseStatefulState<PaymentRecipePage> {
  @override
  Widget build(BuildContext context) {
    log.d("|| ${widget.url}");
    return lScaffold(
        appBar: appBar("영수증".tr(), hide: false),
        body: Container(
            child: widget.url.isNotEmpty
                ? WebView(
                    initialUrl: widget.url,
                    javascriptMode: JavascriptMode.unrestricted,
                  )
                : lEmptyView()));
  }
}
