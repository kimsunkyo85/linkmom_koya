import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cash_list_response.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/lcons.dart';

import 'payment_view.dart';

class PaymentExpiredPage extends BaseStateful {
  final dynamic info;
  PaymentExpiredPage({required this.info});
  @override
  _PaymentExpiredPageState createState() => _PaymentExpiredPageState();
}

class _PaymentExpiredPageState extends BaseStatefulState<PaymentExpiredPage> {
  @override
  Widget build(BuildContext context) {
    List list = widget.info.results.where((e) => EnumUtils.getPaymentReason((widget.info is CashList) ? e.cashFlag : e.pointFlag) == PaymentReason.disappeared).toList();
    return lScaffold(
      appBar: appBar("결제내역".tr(), hide: false),
      body: widget.info is CashList
          ? Column(
              children: [
                ExpiredFund(
                  title: "보유캐시".tr(),
                  expiredTitle: "30일이내소멸예정캐시".tr(),
                  desc: "소멸예정캐시가있습니다".tr(),
                  value: widget.info.sumCash,
                  expiredValue: widget.info.fireCash,
                  info: widget.info,
                  viewType: ViewType.summary,
                  unit: Lcons.cash(isEnabled: true),
                ),
                lDivider(thickness: 8.0, color: color_fafafa),
                list.isNotEmpty ? Column(children: list.map<Widget>((e) => CashHistory(data: e)).toList()) : Padding(padding: padding_30_TB, child: lEmptyView()),
              ],
            )
          : Column(
              children: [
                ExpiredFund(
                  title: "보유포인트".tr(),
                  expiredTitle: "30일이내소멸예정포인트".tr(),
                  desc: "소멸예정포인트가있습니다".tr(),
                  value: widget.info.sumPoint,
                  expiredValue: widget.info.firePoint,
                  info: widget.info,
                  viewType: ViewType.summary,
                  unit: Lcons.point(),
                ),
                lDivider(thickness: 8.0, color: color_fafafa),
                list.isNotEmpty ? Column(children: list.map<Widget>((e) => PointHistory(data: e)).toList()) : Padding(padding: padding_30_TB, child: lEmptyView()),
              ],
            ),
    );
  }
}
