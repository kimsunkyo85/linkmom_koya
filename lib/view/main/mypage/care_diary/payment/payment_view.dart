import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:linkmom/data/network/models/cash_list_response.dart';
import 'package:linkmom/data/network/models/matching_init_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_info_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_init_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_view_request.dart';
import 'package:linkmom/data/network/models/pay_list_response.dart';
import 'package:linkmom/data/network/models/point_list_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/svg_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/job_profile/momdady_profile_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/payment/pay_recipe_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/recipe_view.dart';
import 'package:linkmom/view/main/payment/payment_cancel_page.dart';
import 'package:linkmom/view/main/payment/payment_cancel_reason_page.dart';

import '../recipe_view_page.dart';

class PayHistory extends StatelessWidget {
  final ServiceType type;
  final Widget? unit;
  final PayData? data;

  PayHistory({this.type = ServiceType.serviceType_0, this.unit, this.data});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: padding_20_LTR,
        decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
        padding: padding_20_B,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            lInkWell(
              onTap: () => showModalBottomSheet(
                  context: context,
                  backgroundColor: Colors.transparent,
                  builder: (ctx) {
                    return PaymentDetail(data!, name: type.string);
                  }),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                      flex: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          lText(StringUtils.parseYYMMDD(data!.paidAt), style: st_12(textColor: color_b2b2b2, fontWeight: FontWeight.normal)),
                          sb_h_05,
                          Row(children: [
                            lText(type.string, style: st_b_13(fontWeight: FontWeight.normal)),
                            Container(height: 10, child: lDividerVertical(thickness: 1, padding: padding_05_LR)),
                            lText(data!.childinfo!.childName, style: st_b_13(fontWeight: FontWeight.normal)),
                          ]),
                          sb_h_03,
                          Row(children: [
                            lText(StringUtils.getCareDateCount(data!.careScheduleinfo!.caredate), style: st_b_13(fontWeight: FontWeight.normal)),
                            Container(height: 13, child: lDividerVertical(thickness: 1, padding: padding_05_LR)),
                            lText('${StringUtils.parseAHMM(data!.careScheduleinfo!.stime)} ~ ${StringUtils.parseAHMM(data!.careScheduleinfo!.etime)}', style: st_b_13(fontWeight: FontWeight.normal))
                          ])
                        ],
                      )),
                  Flexible(
                      child: TypeComponent(
                    isExpanded: true,
                    data: StringUtils.formatPay(data!.productPrice),
                    subData: "원".tr(),
                    dataStyle: st_b_16(fontWeight: FontWeight.w700),
                    subDataStyle: st_b_16(fontWeight: FontWeight.w700),
                  ))
                ],
              ),
            ),
            if (data!.paymentCancel!.isNotEmpty)
              Padding(
                padding: padding_12_T,
                child: Column(
                  children: data!.paymentCancel!.map((cancel) {
                    return lInkWell(
                      onTap: () => showModalBottomSheet(
                        context: context,
                        backgroundColor: Colors.transparent,
                        builder: (ctx) => PaymentDetail(data!, name: "취소정보".tr(), isRefundView: true, cancel: cancel),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Flexible(
                              flex: 2,
                              child: Row(
                                children: [
                                  Container(padding: padding_05_RB, child: SvgPicture.asset(SVG_ICON_L, color: color_dedede, width: 6)),
                                  lText('${"결제취소".tr()}${cancel.isRefund ? "완료".tr() : "요청".tr()} (${StringUtils.parseYYMMDD(cancel.createdate)})', style: st_13(fontWeight: FontWeight.w500, textColor: color_999999)),
                                ],
                              )),
                          Flexible(
                              child: TypeComponent(
                            isExpanded: true,
                            data: '-${StringUtils.formatPay(cancel.requestCancelPrice)}',
                            dataStyle: st_14(fontWeight: FontWeight.w500, textColor: color_999999),
                            icon: Row(
                              children: [
                                sb_w_02,
                                lText("원".tr(), style: st_14(fontWeight: FontWeight.w500, textColor: color_999999)),
                                Lcons.nav_right(color: color_cecece, size: 13),
                              ],
                            ),
                          ))
                        ],
                      ),
                    );
                  }).toList(),
                ),
              ),
          ],
        ));
  }
}

class PaymentDetail extends StatelessWidget {
  final String name;
  final PayData data;
  final bool isRefundView;
  final PaymentCancel? cancel;

  PaymentDetail(this.data, {this.name = '', this.isRefundView = false, this.cancel}) {
    assert(isRefundView ? cancel != null : true);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding_20_T,
      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.vertical(top: Radius.circular(16))),
      child: lScrollView(
          padding: padding_0,
          child: Wrap(
            children: [
              Container(
                padding: padding_20,
                child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: [
                  Row(children: [
                    lText(data.careScheduleinfo!.servicetype, style: st_b_16(fontWeight: FontWeight.w700)),
                    Container(height: 15, child: lDividerVertical(thickness: 1, padding: padding_05_LR)),
                    lText(data.childinfo!.childName, style: st_b_16(fontWeight: FontWeight.w700)),
                  ]),
                  sb_h_05,
                  Row(children: [
                    lText(StringUtils.getCareDateCount(data.careScheduleinfo!.caredate), style: st_b_16(fontWeight: FontWeight.w700)),
                    Container(height: 13, child: lDividerVertical(thickness: 1, padding: padding_05_LR)),
                    lText('${StringUtils.parseAHMM(data.careScheduleinfo!.stime)} ~ ${StringUtils.parseAHMM(data.careScheduleinfo!.etime)}', style: st_b_16(fontWeight: FontWeight.w700))
                  ]),
                  sb_h_15,
                  if (data.receiptUrl.isNotEmpty)
                    Container(
                      margin: padding_30_B,
                      child: lTextBtn("영수증".tr(),
                          style: st_13(textColor: color_545454, fontWeight: FontWeight.w500),
                          bg: color_eeeeee.withOpacity(0.72),
                          rightIcon: Lcons.nav_right(color: color_545454, size: 10),
                          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                          onClickEvent: () => Commons.page(context, routePaymentRecipe, arguments: PaymentRecipePage(url: data.receiptUrl))),
                    ),
                  isRefundView
                      ? Refund(cancel!, cardName: data.cardName, cardNumber: data.cardNumber)
                      : Payment(
                          payType: "결제정보".tr(),
                          dateType: "결제일자".tr(),
                          date: StringUtils.parseDotYMD(data.paidAt),
                          detailList: [
                            TypeComponent(
                              type: "결제금액".tr(),
                              data: StringUtils.formatPay(data.productPrice),
                              isExpanded: true,
                              typeStyle: st_b_16(fontWeight: FontWeight.w700),
                              dataStyle: st_b_16(fontWeight: FontWeight.w700),
                              icon: Padding(padding: padding_02_L, child: lText("원".tr(), style: st_b_16(fontWeight: FontWeight.w500))),
                            ),
                            if (data.usedPoint > 0)
                              TypeComponent(
                                type: "포인트".tr(),
                                data: StringUtils.formatPay(data.usedPoint),
                                isExpanded: true,
                                typeIcon: Lcons.rereply(),
                                leftTypeIcon: true,
                                typeStyle: st_15(fontWeight: FontWeight.w500, textColor: color_b2b2b2),
                                icon: Padding(padding: padding_02_L, child: Lcons.point(size: 16)),
                              ),
                            if (data.usedCash > 0)
                              TypeComponent(
                                type: "캐시".tr(),
                                data: StringUtils.formatPay(data.usedCash),
                                isExpanded: true,
                                typeIcon: Lcons.rereply(),
                                leftTypeIcon: true,
                                typeStyle: st_15(fontWeight: FontWeight.w500, textColor: color_b2b2b2),
                                icon: Padding(padding: padding_02_L, child: Lcons.cash(isEnabled: true, size: 16)),
                              ),
                            if (data.usedPg > 0)
                              TypeComponent(
                                type: EnumUtils.getPayTypeString(data.payMethod, data.pgProvider).string,
                                data: StringUtils.formatPay(data.usedPg),
                                isExpanded: true,
                                typeIcon: Lcons.rereply(),
                                leftTypeIcon: true,
                                typeStyle: st_15(fontWeight: FontWeight.w500, textColor: color_b2b2b2),
                                icon: Padding(padding: padding_03_L, child: lText("원".tr(), style: st_14(textColor: color_545454))),
                              ),
                            if (data.usedPg > 0 && data.cardName.isNotEmpty)
                              TypeComponent(
                                type: '${data.cardName}(${data.cardNumber})',
                                isExpanded: true,
                                typeIcon: sb_w_20,
                                leftTypeIcon: true,
                                typeStyle: st_15(fontWeight: FontWeight.w500, textColor: color_b2b2b2),
                              )
                          ],
                        ),
                ]),
              ),
              Padding(
                padding: padding_20,
                child: Row(children: [
                  data.paymentCancel != null && data.paymentCancel!.isNotEmpty
                      ? Flexible(
                          child: lBtnOutline("취소내역".tr(),
                              borderRadius: 8,
                              margin: padding_0,
                              style: st_14(fontWeight: FontWeight.w700, textColor: color_545454),
                              sideColor: color_dedede,
                              onClickAction: () => Commons.page(context, routePayMentCancelList, arguments: PayInfoRequest(contract_id: data.contractId))))
                      : Flexible(
                          child: lBtnOutline(
                          "돌봄취소".tr(),
                          borderRadius: 8,
                          margin: padding_0,
                          style: st_14(fontWeight: FontWeight.w700, textColor: color_545454),
                          sideColor: color_dedede,
                          onClickAction: () => showNormalDlg(
                              context: context,
                              message: "돌봄취소하기_안내".tr(),
                              btnLeft: "취소".tr(),
                              btnRight: "돌봄취소하기".tr(),
                              onClickAction: (a) {
                                if (a == DialogAction.yes)
                                  Commons.page(context, routePayMentCancelReason,
                                      arguments: PaymentCancelReasonPage(
                                          payCancelInitRequest: PayCancelInitRequest(
                                        order_id: data.orderId,
                                        contract_id: data.contractId,
                                      )));
                              }),
                        )),
                  sb_w_10,
                  Flexible(
                      child: lBtnOutline("돌봄계약서".tr(),
                          borderRadius: 8,
                          margin: padding_0,
                          style: st_14(fontWeight: FontWeight.w700, textColor: Commons.getColor()),
                          sideColor: Commons.getColor(),
                          onClickAction: () => _moveContract(context, data.careScheduleinfo!.booking_id, data.matchinginfo!.momdady == auth.user.id ? data.matchinginfo!.linkmom : data.matchinginfo!.momdady, data.matchinginfo!.matchingId))),
                ]),
              ),
              lDivider(color: color_dedede),
              lBtnOutline("확인".tr(), onClickAction: () => Commons.pagePop(context), style: st_16(textColor: color_545454, fontWeight: FontWeight.w700), sideColor: Colors.transparent)
            ],
          )),
    );
  }

  void _moveContract(BuildContext context, int id, int receiver, int matchingId) {
    Commons.page(context, routeMomDadyProfile,
        arguments: MomDadyProfilePage(
          viewMode: ViewMode(viewType: ViewType.view, itemType: ItemType.contract),
          bookingId: id,
          btnType: BottomBtnType.not,
          matchingData: MatchingInitRequest(
            bookingcareservices: id,
            receiver: receiver,
            carematching: matchingId,
            matchingStatus: MatchingStatus.paid.value,
          ),
        ));
  }
}

class Refund extends StatelessWidget {
  final String cardName;
  final String cardNumber;
  final PaymentCancel data;

  const Refund(this.data, {this.cardName = '', this.cardNumber = ''});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.vertical(top: Radius.circular(16))),
        child: Payment(
          payType: "취소정보".tr(),
          dateType: "취소일자".tr(),
          date: StringUtils.parseDotYMD(data.createdate),
          detailList: [
            TypeComponent(
                type: "취소금액".tr(),
                data: StringUtils.formatPay(data.requestCancelPrice),
                isExpanded: true,
                typeStyle: st_b_16(fontWeight: FontWeight.w700),
                dataStyle: st_b_16(fontWeight: FontWeight.w700),
                icon: Padding(padding: padding_02_L, child: lText("원".tr(), style: st_14(textColor: color_545454)))),
            if (data.refundPoint > 0)
              TypeComponent(
                type: "포인트".tr(),
                data: StringUtils.formatPay(data.refundPoint),
                isExpanded: true,
                typeIcon: Lcons.rereply(),
                leftTypeIcon: true,
                typeStyle: st_15(fontWeight: FontWeight.w500, textColor: color_b2b2b2),
                icon: Padding(padding: padding_02_L, child: Lcons.point(size: 16)),
              ),
            if (data.refundCash > 0)
              TypeComponent(
                type: "캐시".tr(),
                data: StringUtils.formatPay(data.refundCash),
                isExpanded: true,
                typeIcon: Lcons.rereply(),
                leftTypeIcon: true,
                typeStyle: st_15(fontWeight: FontWeight.w500, textColor: color_b2b2b2),
                icon: Padding(padding: padding_02_L, child: Lcons.cash(isEnabled: true, size: 16)),
              ),
            if (data.cancelledPrice > 0)
              TypeComponent(
                type: "신용카드".tr(),
                data: StringUtils.formatPay(data.cancelledPrice),
                isExpanded: true,
                typeIcon: Lcons.rereply(),
                leftTypeIcon: true,
                typeStyle: st_15(fontWeight: FontWeight.w500, textColor: color_b2b2b2),
                icon: Padding(padding: padding_02_L, child: lText("원".tr(), style: st_14(textColor: color_545454))),
              ),
            if (cardNumber.isNotEmpty)
              TypeComponent(
                type: '$cardName($cardNumber)',
                isExpanded: true,
                typeIcon: sb_w_20,
                leftTypeIcon: true,
                typeStyle: st_15(fontWeight: FontWeight.w500, textColor: color_b2b2b2),
              )
          ],
        ));
  }
}

class Payment extends StatelessWidget {
  final String dateType;
  final String payType;
  final String date;
  final List<Widget> detailList;

  Payment({this.dateType = '', this.date = '', required this.detailList, this.payType = ''});

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Wrap(children: [
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        lText(payType, style: st_b_16(fontWeight: FontWeight.w700)),
        Row(children: [
          lText(dateType, style: st_13(textColor: color_999999)),
          Container(height: 13, child: lDividerVertical(thickness: 1, padding: padding_05_LR)),
          lText(date, style: st_13(textColor: color_999999)),
        ]),
      ]),
      lDivider(thickness: 2.0, color: Commons.getColor(), padding: padding_10_TB),
      Column(children: detailList),
    ]));
  }
}

class CashHistory extends StatelessWidget {
  final ServiceType type;
  final Widget? unit;
  final CashData data;

  CashHistory({this.type = ServiceType.serviceType_0, this.unit, required this.data});

  @override
  Widget build(BuildContext context) {
    bool _isNegative = false;
    PaymentReason t = EnumUtils.getPaymentReason(data.cashFlag);
    _isNegative = !(data.cashFlag < PaymentReason.paid.value);
    return lInkWell(
        onTap: () => showModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            elevation: 12,
            builder: (ctx) {
              return t == PaymentReason.paid ? PaymentDetail(data.paymentInfo) : DetailPopup(title: data.cashReason, list: _getDetailData(t), type: t, data: data, isHide: false);
            }),
        child: Container(
            margin: padding_20_LTR,
            decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
            padding: padding_20_B,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TypeComponent(
                  type: StringUtils.parseYYMMDD(data.createdate),
                  typeStyle: st_12(textColor: color_b2b2b2),
                  data: t != PaymentReason.paid && t != PaymentReason.withdrawal ? '${"사용기한".tr()} ${StringUtils.parseYYMMDD(data.cashExpiredate)}' : '',
                  dataStyle: st_12(textColor: Commons.getColor()),
                  isExpanded: true,
                ),
                TypeComponent(
                  type: data.cashReason,
                  typeStyle: st_b_14(fontWeight: FontWeight.w500),
                  data: '${_isNegative ? '' : '+'} ${StringUtils.formatPay(data.usedCash)}',
                  dataStyle: st_16(
                      textColor: t == PaymentReason.disappeared
                          ? color_b2b2b2
                          : _isNegative
                              ? color_ff3b30
                              : color_222222,
                      fontWeight: FontWeight.w700),
                  icon: Padding(padding: padding_02_L, child: Lcons.cash(isEnabled: t == PaymentReason.add || t == PaymentReason.refund, size: 15)),
                  leftFlex: 3,
                  rightFlex: 2,
                ),
              ],
            )));
  }

  List<Widget> _getDetailData(PaymentReason type) {
    switch (type) {
      case PaymentReason.add:
        return [
          Payment(payType: "결제정보".tr(), dateType: "정산일자".tr(), date: StringUtils.parseDotYMD(data.createdate), detailList: [
            TypeComponent(
              type: "돌봄일자".tr(),
              data: StringUtils.parseDotYMD(data.createdate),
              isExpanded: true,
              typeStyle: st_b_16(fontWeight: FontWeight.w500),
              dataStyle: st_b_16(),
            ),
            sb_h_10,
            TypeComponent(
              type: "정산_금액".tr(),
              data: StringUtils.formatPay(data.usedCash),
              icon: Padding(padding: padding_03_L, child: Lcons.cash(isEnabled: true, size: 16)),
              isExpanded: true,
              typeStyle: st_b_16(fontWeight: FontWeight.w500),
              dataStyle: st_b_16(fontWeight: FontWeight.w700),
            ),
            sb_h_10,
            TypeComponent(
              type: "캐시만료일".tr(),
              data: StringUtils.parseDotYMD(data.cashExpiredate),
              isExpanded: true,
              typeStyle: st_b_16(fontWeight: FontWeight.w500),
              dataStyle: st_b_16(),
            ),
          ])
        ];
      case PaymentReason.refund:
        return [
          Payment(payType: "결제정보".tr(), dateType: "정산일자".tr(), date: StringUtils.parseDotYMD(data.createdate), detailList: [
            TypeComponent(
              type: "돌봄일자".tr(),
              data: StringUtils.parseDotYMD(data.createdate),
              isExpanded: true,
              typeStyle: st_b_16(fontWeight: FontWeight.w500),
              dataStyle: st_b_16(),
            ),
            TypeComponent(
              type: "정산_금액".tr(),
              data: StringUtils.formatPay(data.usedCash),
              icon: Padding(padding: padding_03_L, child: Lcons.cash(isEnabled: true, size: 16)),
              isExpanded: true,
              typeStyle: st_b_16(fontWeight: FontWeight.w500),
              dataStyle: st_b_16(fontWeight: FontWeight.w700),
            ),
            TypeComponent(
              type: "캐시만료일".tr(),
              data: StringUtils.parseDotYMD(data.cashExpiredate),
              isExpanded: true,
              typeStyle: st_b_16(fontWeight: FontWeight.w500),
              dataStyle: st_b_16(),
            ),
          ])
        ];
      case PaymentReason.paid:
      // return PaymentDetail(data.paymentInfo, name: type.name);
      case PaymentReason.withdrawal:
        return [
          TypeComponent(
            type: "인출일자".tr(),
            data: StringUtils.parseDotYMD(data.createdate),
            isExpanded: true,
            typeStyle: st_b_16(fontWeight: FontWeight.w500),
            dataStyle: st_b_16(),
            margin: padding_12_B,
          ),
          TypeComponent(
            type: "캐시인출".tr(),
            data: StringUtils.formatPay(data.usedCash.abs()),
            icon: Padding(padding: padding_03_L, child: Lcons.cash(isEnabled: true, size: 16)),
            isExpanded: true,
            typeStyle: st_b_16(fontWeight: FontWeight.w500),
            dataStyle: st_b_16(fontWeight: FontWeight.w700),
            margin: padding_0,
          ),
          Padding(
            padding: padding_12_B,
            child: lText('(${StringUtils.formatPay(data.usedCash.abs())}${"원".tr()})', style: st_14(textColor: color_545454, fontWeight: FontWeight.w500)),
          ),
          TypeComponent(
            type: "입금금액".tr(),
            data: StringUtils.formatPay(data.usedCash.abs()),
            icon: Padding(padding: padding_03_L, child: lText("원".tr(), style: st_b_14(fontWeight: FontWeight.w500))),
            isExpanded: true,
            typeStyle: st_b_16(fontWeight: FontWeight.w500),
            dataStyle: st_b_16(fontWeight: FontWeight.w700),
            margin: padding_12_B,
          ),
        ];
      case PaymentReason.disappeared:
        return [
          TypeComponent(
            type: "소멸일자".tr(),
            data: data.cashExpiredate,
            isExpanded: true,
            typeStyle: st_b_16(fontWeight: FontWeight.w500),
            dataStyle: st_b_16(),
            margin: padding_12_B,
          ),
          TypeComponent(
            type: "소멸금액".tr(),
            data: StringUtils.formatPay(data.usedCash),
            icon: Padding(padding: padding_03_L, child: Lcons.cash(isEnabled: true, size: 16)),
            isExpanded: true,
            typeStyle: st_b_16(fontWeight: FontWeight.w500),
            dataStyle: st_b_16(fontWeight: FontWeight.w700),
          ),
        ];
    }
  }
}

class PointHistory extends StatelessWidget {
  final ServiceType type;
  final Widget? unit;
  final PointData data;

  PointHistory({this.type = ServiceType.serviceType_0, this.unit, required this.data});

  @override
  Widget build(BuildContext context) {
    bool _isNegative = false;
    PaymentReason t = EnumUtils.getPaymentReason(data.pointFlag);
    _isNegative = !(data.pointFlag < PaymentReason.paid.value);
    return lInkWell(
        onTap: () => showModalBottomSheet(
            context: context,
            backgroundColor: Colors.transparent,
            builder: (ctx) {
              return t == PaymentReason.paid
                  ? PaymentDetail(data.paymentInfo)
                  : DetailPopup(
                      title: data.pointReason,
                      list: _getDetailData(t),
                      data: data,
                      isHide: true,
                    );
            }),
        child: Container(
            margin: padding_20_LTR,
            decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
            padding: padding_20_B,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TypeComponent(
                  type: StringUtils.parseYYMMDD(data.createdate),
                  typeStyle: st_12(textColor: color_b2b2b2),
                  data: t != PaymentReason.paid ? '${"사용기한".tr()} ${StringUtils.parseYYMMDD(data.pointExpiredate)}' : '',
                  dataStyle: st_12(textColor: Commons.getColor()),
                  isExpanded: true,
                ),
                TypeComponent(
                  type: data.pointReason,
                  typeStyle: st_b_14(fontWeight: FontWeight.w500),
                  data: '${_isNegative ? '' : '+'} ${StringUtils.formatPay(data.usedPoint)}',
                  dataStyle: st_16(
                      textColor: t == PaymentReason.disappeared
                          ? color_b2b2b2
                          : _isNegative
                              ? color_ff3b30
                              : color_222222,
                      fontWeight: FontWeight.w700),
                  icon: Padding(padding: padding_02_L, child: Lcons.point(isEnabled: t == PaymentReason.add || t == PaymentReason.refund, size: 16)),
                  leftFlex: 3,
                  rightFlex: 2,
                ),
              ],
            )));
  }

  List<Widget> _getDetailData(PaymentReason type) {
    switch (type) {
      case PaymentReason.add:
        return [
          TypeComponent(
            type: "돌봄일자".tr(),
            data: StringUtils.parseDotYMD(data.createdate),
            isExpanded: true,
            typeStyle: st_b_16(fontWeight: FontWeight.w500),
            dataStyle: st_b_16(),
          ),
          TypeComponent(
            type: "정산_금액".tr(),
            data: StringUtils.formatPay(data.usedPoint),
            icon: Lcons.point(size: 16),
            isExpanded: true,
            typeStyle: st_b_16(fontWeight: FontWeight.w500),
            dataStyle: st_b_16(fontWeight: FontWeight.w700),
          ),
          TypeComponent(
            type: "캐시만료일".tr(),
            data: StringUtils.parseDotYMD(data.pointExpiredate),
            isExpanded: true,
            typeStyle: st_b_16(fontWeight: FontWeight.w500),
            dataStyle: st_b_16(),
          ),
        ];

      case PaymentReason.refund:
        return [
          TypeComponent(
            type: "돌봄일자".tr(),
            data: StringUtils.parseDotYMD(data.createdate),
            isExpanded: true,
            typeStyle: st_b_16(fontWeight: FontWeight.w500),
            dataStyle: st_b_16(),
          ),
          TypeComponent(
            type: "정산_금액".tr(),
            data: StringUtils.formatPay(data.usedPoint),
            icon: Lcons.point(size: 16),
            isExpanded: true,
            typeStyle: st_b_16(fontWeight: FontWeight.w500),
            dataStyle: st_b_16(fontWeight: FontWeight.w700),
          ),
          TypeComponent(
            type: "캐시만료일".tr(),
            data: StringUtils.parseDotYMD(data.pointExpiredate),
            isExpanded: true,
            typeStyle: st_b_16(fontWeight: FontWeight.w500),
            dataStyle: st_b_16(),
          ),
        ];
      case PaymentReason.disappeared:
        return [
          TypeComponent(
            type: "소멸일자".tr(),
            data: data.pointExpiredate,
            isExpanded: true,
            typeStyle: st_b_16(fontWeight: FontWeight.w500),
            dataStyle: st_b_16(),
          ),
          TypeComponent(
            type: "소멸금액".tr(),
            data: StringUtils.formatPay(data.usedPoint),
            icon: Lcons.point(size: 16),
            isExpanded: true,
            typeStyle: st_b_16(fontWeight: FontWeight.w500),
            dataStyle: st_b_16(fontWeight: FontWeight.w700),
          ),
        ];
      default:
        return [];
    }
  }
}

class DetailPopup extends StatelessWidget {
  final String title;
  final List<Widget> list;
  final bool isHide;
  final PaymentReason type;
  final dynamic data;

  DetailPopup({required this.title, required this.list, this.isHide = false, this.type = PaymentReason.disappeared, this.data});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding_20_TB,
      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.vertical(top: Radius.circular(16))),
      child: Wrap(
        children: [
          Padding(padding: padding_20, child: lText(title, style: st_b_20(fontWeight: FontWeight.w700))),
          lDivider(color: isHide ? Colors.transparent : color_eeeeee, padding: padding_20_LRB),
          Container(padding: padding_20_LR, child: Column(crossAxisAlignment: CrossAxisAlignment.end, children: list)),
          lDivider(color: color_eeeeee, padding: padding_20_T),
          (type == PaymentReason.disappeared || type == PaymentReason.withdrawal)
              ? lBtnOutline("확인".tr(), onClickAction: () => Commons.pagePop(context), style: st_16(textColor: color_545454, fontWeight: FontWeight.w700), sideColor: Colors.transparent)
              : Padding(
                  padding: padding_20,
                  child: Row(children: [
                    Flexible(
                        child: lBtnOutline(
                      "돌봄계약서".tr(),
                      borderRadius: 8,
                      margin: padding_0,
                      style: st_14(fontWeight: FontWeight.w700, textColor: Commons.getColor()),
                      sideColor: Commons.getColor(),
                      onClickAction: () {
                        int id = data is PayData ? data.careScheduleinfo!.booking_id : data.paymentInfo.careScheduleinfo!.booking_id;
                        int receiver = data is PayData ? _getId(auth.user.id, data.data.matchinginfo!.momdady, data.data.matchinginfo!.linkmom) : _getId(auth.user.id, data.paymentInfo.matchinginfo!.momdady, data.paymentInfo.matchinginfo!.linkmom);
                        int matchingId = data is PayData ? data.data.matchinginfo!.matchingId : data.paymentInfo.matchinginfo!.matchingId;
                        _moveContract(context, id, receiver, matchingId);
                      },
                    )),
                    sb_w_10,
                    if (type == PaymentReason.paid)
                      Flexible(
                          child: lBtnOutline(
                        "결제내역".tr(),
                        borderRadius: 8,
                        margin: padding_0,
                        style: st_14(fontWeight: FontWeight.w700, textColor: color_545454),
                        sideColor: color_dedede,
                        onClickAction: () => showNormalDlg(
                            context: context,
                            message: "돌봄취소하기_안내".tr(),
                            btnLeft: "취소".tr(),
                            btnRight: "돌봄취소하기".tr(),
                            onClickAction: (a) {
                              if (a == DialogAction.yes)
                                Commons.page(context, routePayMentCancelReason,
                                    arguments: PaymentCancelReasonPage(
                                        payCancelInitRequest: PayCancelInitRequest(
                                      order_id: data.orderId,
                                      contract_id: data.contractId,
                                    )));
                            }),
                      )),
                    if (type == PaymentReason.refund)
                      Flexible(
                          child: lBtnOutline(
                        "취소내역".tr(),
                        borderRadius: 8,
                        margin: padding_0,
                        style: st_14(fontWeight: FontWeight.w700, textColor: color_545454),
                        sideColor: color_dedede,
                        onClickAction: () => Commons.page(context, routePayMentCancel,
                            arguments: PaymentCancelPage(
                              payCancelViewRequest: PayCancelViewRequest(order_id: data.orderId, contract_id: data.contractId, cancel_id: data.cancelId),
                            )),
                      )),
                    if (type == PaymentReason.add)
                      Flexible(
                          child: lBtnOutline(
                        "정산내역".tr(),
                        borderRadius: 8,
                        margin: padding_0,
                        style: st_14(fontWeight: FontWeight.w700, textColor: color_545454),
                        sideColor: color_dedede,
                        onClickAction: () => Commons.page(context, routeRecipe, arguments: RecipeViewPage(appbarTitle: "정산완료".tr(), id: data.schedulePaid.scheduleId, status: PayStatus.compleat)),
                      )),
                  ]),
                ),
        ],
      ),
    );
  }

  void _moveContract(BuildContext context, int id, int receiver, int matchingId) {
    Commons.page(context, routeMomDadyProfile,
        arguments: MomDadyProfilePage(
          viewMode: ViewMode(viewType: ViewType.view, itemType: ItemType.contract),
          bookingId: id,
          btnType: BottomBtnType.not,
          matchingData: MatchingInitRequest(
            bookingcareservices: id,
            receiver: receiver,
            carematching: matchingId,
            matchingStatus: MatchingStatus.paid.value,
          ),
        ));
  }

  int _getId(int me, int momdaddy, int linkssam) {
    return momdaddy == me ? linkssam : momdaddy;
  }
}

class FilterResult extends StatelessWidget {
  final String title;
  final int value;
  final String desc;
  final Widget result;

  const FilterResult({this.title = '', this.value = 0, this.desc = '', required this.result});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          lRoundContainer(
              margin: padding_20,
              borderColor: color_eeeeee,
              padding: padding_20,
              bgColor: Colors.white,
              child: Column(children: [
                TypeComponent(type: title, data: StringUtils.formatPay(value), isExpanded: true, icon: Padding(padding: padding_03_L, child: Lcons.point())),
                lText(desc, style: st_12(textColor: color_b2b2b2)),
              ])),
          lDivider(thickness: 8),
          result
        ],
      ),
    );
  }
}

class ExpiredFund extends StatelessWidget {
  final String title;
  final String desc;
  final String expiredTitle;
  final int value;
  final int expiredValue;
  final dynamic info;
  final Widget unit;
  final Function? showDetail;
  final ViewType viewType;

  const ExpiredFund({this.title = '', this.desc = '', this.value = 0, required this.info, required this.unit, this.expiredTitle = '', this.expiredValue = 0, this.showDetail, this.viewType = ViewType.normal});

  @override
  Widget build(BuildContext context) {
    return lRoundContainer(
        margin: padding_20,
        borderColor: color_eeeeee,
        padding: padding_20,
        bgColor: Colors.white,
        child: Column(
          children: [
            if (viewType == ViewType.normal)
              TypeComponent(
                type: title,
                data: StringUtils.formatPay(value),
                isExpanded: true,
                icon: Padding(padding: padding_03_L, child: unit),
                typeStyle: st_b_13(),
                dataStyle: st_b_20(fontWeight: FontWeight.w700),
              ),
            if (viewType == ViewType.normal) lDivider(color: color_eeeeee, padding: padding_10_TB),
            lInkWell(
                child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.center, children: [
              Flexible(
                  flex: 5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TypeComponent(
                        type: expiredTitle,
                        data: StringUtils.formatPay(expiredValue),
                        isExpanded: true,
                        icon: Padding(padding: padding_03_L, child: unit),
                        dataStyle: st_b_16(fontWeight: FontWeight.w500),
                      ),
                      if (desc.isNotEmpty) lText(desc, style: st_12(textColor: color_b2b2b2)),
                    ],
                  )),
            ]))
          ],
        ));
  }
}
