import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cash_list_response.dart';
import 'package:linkmom/data/network/models/pay_list_response.dart';
import 'package:linkmom/data/network/models/point_list_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/main/mypage/care_diary/payment/payment_history_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/payment/payment_view.dart';

class PaymentFilterResultPage extends BaseStateful {
  final DataManager? data;
  final DateTimeRange range;
  final dynamic info;
  final Function? onSearch;
  PaymentFilterResultPage({this.data, required this.range, required this.info, this.onSearch});
  @override
  _PaymentFilterResultPageState createState() => _PaymentFilterResultPageState();
}

class _PaymentFilterResultPageState extends BaseStatefulState<PaymentFilterResultPage> {
  String _next = '';

  @override
  void initState() {
    super.initState();
    _next = widget.info.next;
  }

  @override
  Widget build(BuildContext context) {
    dynamic result = widget.info;
    return lScaffold(
        backgroundColor: color_fafafa,
        appBar: appBar("결제내역".tr(), hide: false, isClose: true, onClick: (a) => Commons.pageToMain(context, routePaymentHistory, arguments: PaymentHistoryPage())),
        body: SliverNextListView(
            onNext: () async {
              if (widget.onSearch != null && _next.isNotEmpty) {
                var ret = await widget.onSearch!(false, _next);
                _next = ret.getData().next;
                result.results.addAll(ret.getData().results);
                onUpDate();
              }
            },
            onRefresh: () => {if (widget.onSearch != null) widget.onSearch!(true, null)},
            showNextLoading: _next.isNotEmpty,
            pinned: Filter(range: widget.range, onClickAction: () => Commons.pagePop(context), count: widget.info.count),
            view: widget.info.results.map<Widget>((e) {
              if (widget.info is PayList) {
                return PayHistory(data: e);
              } else if (widget.info is CashList) {
                return CashHistory(data: e);
              } else if (widget.info is PointList) {
                return PointHistory(data: e);
              } else {
                return Container();
              }
            }).toList(),
          emptyView: lEmptyView(
            height: Commons.height(context, 0.7),
            textString: "다른조건으로재검색".tr(),
            fn: () => Commons.pagePop(context),
          ),
        ));
  }
}
