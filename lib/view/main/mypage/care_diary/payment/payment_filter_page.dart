import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/filter_page_view.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

class PaymentFilterPage extends BaseStateful {
  final Function onSearch;
  final List<FilterOption> filterOptions;

  PaymentFilterPage({required this.onSearch, required this.filterOptions});

  @override
  _PaymentFilterPageState createState() => _PaymentFilterPageState();
}

class _PaymentFilterPageState extends BaseStatefulState<PaymentFilterPage> {
  FilterSelect _filterSelect = FilterSelect();

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        appBar: appBar("상세조회".tr(), hide: false, isBack: false, isClose: true, onClick: (a) => Commons.pagePop(context)),
        body: FilterPageView(
            filterOptions: widget.filterOptions,
            listType: ListType.payment,
            rangeType: [-1, -2, -3],
            rangeText: [
              "1개월".tr(),
              "2개월".tr(),
              "3개월".tr(),
            ],
            filterSelect: _filterSelect,
            searchAction: (range, options) {
              if (range is FilterSelect) {
                _filterSelect = range;
                widget.onSearch(range.range!, options);
              } else {
                widget.onSearch(range, options);
              }
            }));
  }
}
