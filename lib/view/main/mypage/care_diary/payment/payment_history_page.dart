import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cash_list_request.dart';
import 'package:linkmom/data/network/models/cash_list_response.dart';
import 'package:linkmom/data/network/models/pay_list_request.dart';
import 'package:linkmom/data/network/models/pay_list_response.dart';
import 'package:linkmom/data/network/models/point_list_request.dart';
import 'package:linkmom/data/network/models/point_list_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/filter_page_view.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/care_diary/payment/payment_expired_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/payment/payment_filter_result_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/payment/payment_view.dart';
import 'package:provider/provider.dart';

import 'payment_filter_page.dart';
import '../../../../../data/providers/payment_history_provider.dart';

class PaymentHistoryPage extends BaseStateful {
  final int tabIndex;
  PaymentHistoryPage({this.tabIndex = 0});
  @override
  _PaymentHistoryPage createState() => _PaymentHistoryPage();

  static const int TAB_MONEY = 0;
  static const int TAB_CASH = 1;
  static const int TAB_POINT = 2;
}

class _PaymentHistoryPage extends BaseStatefulState<PaymentHistoryPage> with SingleTickerProviderStateMixin {
  late DateTimeRange _range;
  late PaymentHistoryProvider _provider;
  TabController? _ctrl;

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data = _data;
  }

  @override
  void initState() {
    super.initState();
    _provider = context.read<PaymentHistoryProvider>();
    _range = DateTimeRange(start: DateTime.now().add(Duration(days: -30)), end: DateTime.now());
    flag.enableLoading(fn: () => onUpDate());
    _requestPayList(range: _range);
    _requestCashList(range: _range);
    _requestPointList(range: _range);
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        context: context,
        appBar: appBar("결제내역".tr()),
        body: TabPageView(
          onCreateView: (ctrl) {
            if (_ctrl == null) {
              _ctrl = ctrl;
              _ctrl!.animateTo(widget.tabIndex, curve: Curves.fastOutSlowIn);
            }
          },
          tabbarName: [TabItem(title: "결제".tr()), TabItem(title: "캐시".tr()), TabItem(title: "포인트".tr())],
          tabbarView: [
            SliverFilterLists(
              onNext: () => _requestPayList(url: _provider.pay.next.isNotEmpty ? _provider.pay.next : null),
              onRefresh: () => _requestPayList(),
              filter: Filter(
                  range: _range,
                  onClickAction: () => Commons.page(context, routePaymentFilter,
                      arguments: PaymentFilterPage(
                        onSearch: (range, options) => _requestPayList(range: range, doSearch: true, filter: options),
                        filterOptions: [
                          FilterOption(type: FilterType.paymentState, info: [
                            FilterInfo(value: 0, title: "결제".tr(), desc: "결제_설명".tr()),
                            FilterInfo(value: 1, title: "결제취소".tr(), desc: "결제취소_설명".tr()),
                          ])
                        ],
                      ))),
              footer: _provider.pay.results.isNotEmpty
                  ? Column(children: _provider.pay.results.map((e) => PayHistory(data: e, type: EnumUtils.getServiceType(name: e.careScheduleinfo!.servicetype))).toList())
                  : Padding(padding: padding_30_TB, child: lEmptyView(height: data.height(context, 0.4))),
            ),
            // NOTE: cash Tab
            SliverNextListView(
              onNext: () => _requestCashList(url: _provider.cash.next, isNext: true),
              onRefresh: () => _requestCashList(),
              topper: ExpiredFund(
                title: "보유캐시".tr(),
                expiredTitle: "30일이내소멸예정캐시".tr(),
                desc: _provider.cash.fireCash > 0 ? "소멸예정캐시가있습니다".tr() : '',
                value: _provider.cash.sumCash,
                expiredValue: _provider.cash.fireCash,
                info: _provider.cash,
                unit: Lcons.cash(isEnabled: true),
              ),
              pinned: Filter(
                  range: _range,
                  onClickAction: () => Commons.page(context, routePaymentFilter,
                      arguments: PaymentFilterPage(
                        onSearch: (range, options) => _requestCashList(range: range, doSearch: true, filter: options),
                        filterOptions: [
                          FilterOption(type: FilterType.cashAdd, info: [
                            FilterInfo(value: PaymentReason.add.value, title: "캐시소득".tr(), desc: "캐시소득_설명".tr()),
                            FilterInfo(value: PaymentReason.refund.value, title: "돌봄취소_환불".tr(), desc: "돌봄취소_환불_설명".tr()),
                          ]),
                          FilterOption(type: FilterType.cashDisappreard, info: [
                            FilterInfo(value: PaymentReason.withdrawal.value, title: "캐시인출".tr(), desc: "캐시인출_설명".tr()),
                            FilterInfo(value: PaymentReason.paid.value, title: "돌봄요금결제".tr(), desc: "캐시_돌봄요금결제_설명".tr()),
                            FilterInfo(value: PaymentReason.disappeared.value, title: "캐시소멸".tr(), desc: "캐시소멸_설명".tr()),
                          ]),
                        ],
                      ))),
              showNextLoading: _provider.cash.next.isNotEmpty,
              view: _provider.cash.results.map((e) => CashHistory(data: e)).toList(),
            ),
            SliverNextListView(
              onNext: () => _requestPointList(url: _provider.point.next.isNotEmpty ? _provider.point.next : null),
              onRefresh: () => _requestPointList(),
              topper: ExpiredFund(
                title: "보유포인트".tr(),
                expiredTitle: "30일이내소멸예정포인트".tr(),
                desc: _provider.point.firePoint > 0 ? "소멸예정포인트가있습니다".tr() : '',
                value: _provider.point.sumPoint,
                expiredValue: _provider.point.firePoint,
                info: _provider.point,
                showDetail: () => Commons.page(context, routePaymentExpired, arguments: PaymentExpiredPage(info: _provider.point)),
                unit: Lcons.point(),
              ),
              pinned: Filter(
                range: _range,
                onClickAction: () => Commons.page(context, routePaymentFilter,
                    arguments: PaymentFilterPage(
                      onSearch: (range, options) => _requestPointList(range: range, doSearch: true, filter: options),
                      filterOptions: [
                        FilterOption(type: FilterType.pointAdd, info: [
                          FilterInfo(value: PaymentReason.add.value, title: "이벤트포인트".tr(), desc: "이벤트포인트_설명".tr()),
                          FilterInfo(value: PaymentReason.refund.value, title: "포인트환불".tr(), desc: "포인트환불_설명".tr()),
                        ]),
                        FilterOption(type: FilterType.pointDisappreard, info: [
                          FilterInfo(value: PaymentReason.paid.value, title: "돌봄요금결제".tr(), desc: "포인트_돌봄요금결제_설명".tr()),
                          FilterInfo(value: PaymentReason.disappeared.value, title: "포인트소멸".tr(), desc: "포인트소멸_설명".tr()),
                        ]),
                      ],
                    )),
              ),
              view: _provider.point.results.map((e) => PointHistory(data: e)).toList(),
              showNextLoading: _provider.point.next.isNotEmpty,
            )
          ],
        ));
  }

  void _requestPayList({DateTimeRange? range, String? url, bool doSearch = false, List<FilterOption>? filter, bool isNext = false}) {
    try {
      List<String>? flags;
      if (filter != null) {
        flags = [];
        filter.forEach((e) {
          e.info.where((i) => i.checked).forEach((i) {
            flags!.add(i.value.toString());
          });
        });
      }
      PayListRequest req = range == null
          ? PayListRequest(sDate: StringUtils.formatYYYYMMDD(_range.start), eDate: StringUtils.formatYYYYMMDD(_range.end), paymentFlag: ['0', '1'])
          : PayListRequest(
              sDate: StringUtils.formatYYYYMMDD(range.start),
              eDate: StringUtils.formatYYYYMMDD(range.end),
              paymentFlag: flags,
            );
      if (isNext && url != null && url.isEmpty) return;
      apiHelper.reqeustPaymentList(req, url: url).then((response) {
        if (doSearch) {
          Commons.page(context, routePaymentFilterResult, arguments: PaymentFilterResultPage(range: range!, info: response.getData(), onSearch: (isRefresh, url) => _requestNext(next: isRefresh ? null : url, request: req)));
        } else {
          if (response.getCode() == KEY_SUCCESS) {
            if (isNext) {
              _provider.appendPay(response.getData());
            } else {
              _provider.setPay = response.getData();
            }
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e,
      });
    }
  }

  Future<void> _requestCashList({DateTimeRange? range, String? url, bool doSearch = false, List<FilterOption>? filter, bool isNext = false}) async {
    try {
      List<String>? flags;
      if (filter != null) {
        flags = [];
        filter.forEach((e) {
          e.info.where((i) => i.checked).forEach((i) {
            flags!.add(i.value.toString());
          });
        });
      }
      CashListRequest req = range == null
          ? CashListRequest(sDate: StringUtils.formatYYYYMMDD(_range.start), eDate: StringUtils.formatYYYYMMDD(_range.end))
          : CashListRequest(sDate: StringUtils.formatYYYYMMDD(range.start), eDate: StringUtils.formatYYYYMMDD(range.end), cashFlag: flags);
      if (isNext && url != null && url.isEmpty) return;
      await apiHelper.reqeustCashList(req, url: url).then((response) {
        if (doSearch) {
          Commons.page(context, routePaymentFilterResult, arguments: PaymentFilterResultPage(range: range!, info: response.getData(), onSearch: (isRefresh, url) => _requestNext(next: isRefresh ? null : url, request: req)));
        } else {
          if (response.getCode() == KEY_SUCCESS) {
            if (isNext) {
              _provider.appendCash(response.getData());
            } else {
              _provider.setCash = response.getData();
            }
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e,
      });
    }
  }

  Future<void> _requestPointList({DateTimeRange? range, String? url, bool doSearch = false, List<FilterOption>? filter, bool isNext = false}) async {
    try {
      List<String>? flags;
      if (filter != null) {
        flags = [];
        filter.forEach((e) {
          e.info.where((i) => i.checked).forEach((i) {
            flags!.add(i.value.toString());
          });
        });
      }

      PointListRequest req = range == null
          ? PointListRequest(sDate: StringUtils.formatYYYYMMDD(_range.start), eDate: StringUtils.formatYYYYMMDD(_range.end))
          : PointListRequest(sDate: StringUtils.formatYYYYMMDD(range.start), eDate: StringUtils.formatYYYYMMDD(range.end), pointFlag: flags);
      if (isNext && url != null && url.isEmpty) return;
      await apiHelper.reqeustPointList(req, url: url).then((response) {
        if (doSearch) {
          Commons.page(context, routePaymentFilterResult,
              arguments: PaymentFilterResultPage(
                range: range!,
                info: response.getData(),
                onSearch: (isRefresh, url) => _requestNext(next: isRefresh ? null : url, request: req),
              ));
        } else {
          if (response.getCode() == KEY_SUCCESS) {
            if (isNext) {
              _provider.appendPoint(response.getData());
            } else {
              _provider.setPoint = response.getData();
            }
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e,
      });
    }
  }

  Future<dynamic> _requestNext({dynamic request, String? next}) async {
    if (next != null && next.isEmpty) return;
    // NOTE: if Refresh
    if (next == null) {
      if (request is PointListRequest) {
        _requestPointList(range: _range);
      } else if (request is CashListRequest) {
        _requestCashList(range: _range);
      } else {
        _requestPayList(range: _range);
      }
      return;
    }
    // NOTE: if Next
    FormData formData = FormData();
    var response = await apiHelper.apiCall(HttpType.post, next, apiHeader.publicApiHeader, request.toFormData(formData), isFile: true);
    var resp = Commons.returnResponse(response);
    if (request is PointListRequest) {
      return PointListResponse.fromJson(resp);
    } else if (request is CashListRequest) {
      return CashListResponse.fromJson(resp);
    } else {
      return PayListResponse.fromJson(resp);
    }
  }
}
