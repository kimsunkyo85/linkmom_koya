import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/carediary_list_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_filter_result_page.dart';

import '../../../../utils/custom_view/filter_page_view.dart';

class DiaryFilterPage extends BaseStateful {
  final DataManager? data;
  final int type;

  DiaryFilterPage({this.data, this.type = 0});

  @override
  _DiaryFilterPageState createState() => _DiaryFilterPageState();
}

class _DiaryFilterPageState extends BaseStatefulState<DiaryFilterPage> {
  DateTime _first = Jiffy(DateTime.now()).startOf(Units.MONTH).dateTime;
  DateTime _last = DateTime.now();

  List<FilterOption> _options = [];
  late USER_TYPE _type;
  FilterSelect _filterSelect = FilterSelect();

  @override
  void initState() {
    super.initState();
    _type = storageHelper.user_type;
    _options = [
      FilterOption(
        type: FilterType.payState,
        info: PayStatus.values.where((e) => (e != PayStatus.cancel && e != PayStatus.penalty)).map((e) => FilterInfo(value: e.value, title: _statusName(e))).toList(),
      ),
      FilterOption(type: FilterType.serviceType, info: ServiceType.values.sublist(0, 3).map((e) => FilterInfo(value: e.index, title: e.string)).toList()),
    ];
    if (!Commons.isLinkMom()) _options.add(FilterOption(type: FilterType.readState, info: [FilterInfo(value: 1, title: "안읽음".tr()), FilterInfo(value: 2, title: "읽음".tr())]));
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar("상세조회".tr(), hide: false),
        body: FilterPageView(
          data: data,
          filterOptions: _options,
          listType: ListType.mycares,
          rangeType: [-1, -2, -3],
          rangeText: [
            "1개월전".tr(),
            "2개월전".tr(),
            "3개월전".tr(),
          ],
          first: _first,
          last: _last,
          filterSelect: _filterSelect,
          searchAction: (range, options) {
            if (range is FilterSelect) {
              _filterSelect = range;
              _requestFilter(range.range!, options);
            } else {
              _requestFilter(range, options);
            }
          },
        ),
      ),
    );
  }

  void _requestFilter(DateTimeRange range, List<FilterOption> options) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      int? readState;
      String payStatus = '';
      String serviceType = '';
      int unread = 1;
      int read = 2;
      List<int> selected = [];
      options.forEach((e) {
        switch (e.type) {
          case FilterType.readState:
            selected.addAll(e.info.where((i) => i.checked).map((i) => i.value).toList());
            break;
          case FilterType.payState:
            payStatus = e.info.where((i) => i.checked).map((i) => i.value).join(',');
            break;
          case FilterType.serviceType:
            serviceType = e.info.where((i) => i.checked).map((i) => i.value).join(',');
            break;
          default:
        }
      });
      if (!Commons.isLinkMom()) {
        if (selected.contains(read) && selected.contains(unread)) {
          readState = 0;
        } else {
          readState = selected.last;
        }
      }
      CareDiaryListReqeust req = CareDiaryListReqeust(
        gubun: _type.index + 1,
        type: widget.type,
        start_date: StringUtils.formatYMD(range.start),
        end_date: StringUtils.formatYMD(range.end),
        pay_status: payStatus,
        servicetype: serviceType,
        is_confirm: readState,
      );
      apiHelper.requestDiaryList(req).then((response) {
        flag.disableLoading(fn: () => onUpDate());
        Commons.page(context, routeDiaryFilterResult, arguments: DiaryFilterResultPage(data: data, resp: response, type: widget.type, user: _type, range: range, req: req));
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      log.e("Exception >>>>>>>>>>>>> $e");
    }
  }

  String _statusName(PayStatus type) {
    switch (type) {
      case PayStatus.ready:
      case PayStatus.compleat:
      case PayStatus.delayed:
        return type.name;
      case PayStatus.cancelReady:
        return "돌봄취소예정".tr();
      case PayStatus.penaltyReady:
        return "벌점부여예정".tr();
      case PayStatus.cancel:
      case PayStatus.penalty:
        return "돌봄취소완료".tr();
      default:
        return "돌봄시작".tr();
    }
  }
}
