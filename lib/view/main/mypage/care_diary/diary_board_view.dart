import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/lcons.dart';
import '../../../board/board_view.dart';

class DiaryReadView extends BoardReadView {
  final DiaryBoardTitle? profile;
  final Widget? body;
  final Widget? footer, topper, btn;
  List<ReplyData>? reply;
  int replyLength;
  final bool showReply;
  final Function? saveReplyAction;
  final Widget? infoView;
  final Widget? modal;
  final Offset offset;
  final Size size;

  DiaryReadView({this.profile, this.body, this.reply, this.footer, this.topper, this.replyLength = 0, this.saveReplyAction, this.showReply = false, this.infoView, this.modal, this.offset = Offset.zero, this.size = Size.zero, this.btn})
      : super(profile: profile, body: body, reply: reply, footer: footer, topper: topper, replyLength: replyLength, saveReplyAction: saveReplyAction, showReply: showReply);

  @override
  _DiaryReadViewState createState() => _DiaryReadViewState();
}

class _DiaryReadViewState extends BoardReadViewState<DiaryReadView> {
  final GlobalKey _btn = GlobalKey();
  int? _focusedId = 0;
  FocusNode _replyFocus = FocusNode();
  TextEditingController _controller = TextEditingController();
  Offset _offset = Offset.zero;
  Size _size = Size.zero;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        RenderBox btnBox = _btn.currentContext!.findRenderObject() as RenderBox;
        _size = btnBox.size;
        Offset btnOffset = btnBox.localToGlobal(Offset.zero);
        _offset = btnOffset;
      });
    });
  }

  @override
  void setState(VoidCallback fn) {
    RenderBox btnBox = _btn.currentContext!.findRenderObject() as RenderBox;
    _size = btnBox.size;
    Offset btnOffset = btnBox.localToGlobal(Offset.zero);
    _offset = btnOffset;
    super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.bottomCenter, children: [
      Column(mainAxisSize: MainAxisSize.max, mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Container(
          color: Colors.white,
          child: Column(children: [
            if (widget.infoView != null) widget.infoView!,
            Padding(
              padding: padding_20_LR,
              child: Column(children: [
                if (widget.profile != null) widget.profile!,
                if (widget.topper != null) widget.topper!,
                if (widget.title != null) widget.title!,
              ]),
            ),
            if (widget.body != null)
              ConstrainedBox(
                constraints: BoxConstraints(minHeight: Commons.height(context, 0.3), maxHeight: double.infinity),
                child: widget.body!,
              ),
          ]),
        ),
        if (widget.footer != null)
          Container(
            color: Colors.white,
            padding: padding_20_LR,
            child: widget.footer!,
          ),
        if (widget.btn != null)
          Container(
            key: _btn,
            color: Colors.white,
            padding: padding_20_LRB,
            child: widget.btn!,
          ),
        if (widget.showReply)
          Column(children: [
            ReplyView(
                reply: widget.reply!,
                replyLength: widget.replyLength,
                addAnswer: (parent) {
                  _focusedId = parent;
                  _replyFocus.requestFocus();
                }),
            Container(
                padding: EdgeInsets.fromLTRB(15, 10, 15, 0),
                color: color_eeeeee.withOpacity(0.52),
                alignment: Alignment.centerLeft,
                child: ConstrainedBox(
                  constraints: BoxConstraints(maxHeight: 150, minHeight: 85),
                  child: TextField(
                    focusNode: _replyFocus,
                    textAlign: TextAlign.left,
                    textInputAction: TextInputAction.done,
                    controller: _controller,
                    maxLines: null,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(15, 10, 5, 10),
                      isDense: true,
                      fillColor: Colors.white,
                      filled: true,
                      suffixIcon: TextButton(
                          onPressed: () async {
                            await widget.saveReplyAction!(_focusedId ?? 0, _controller.text);
                            focusClear(context);
                            _controller.text = "";
                            _focusedId = 0;
                          },
                          child: lText("등록".tr(), style: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.w500))),
                      border: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(24)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(24)),
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(24)),
                      hintText: "댓글을남겨주세요".tr(),
                      hintStyle: st_14(textColor: color_cecece),
                    ),
                    onChanged: (value) {
                      setState(() {});
                    },
                  ),
                ))
          ]),
      ]),
      if (widget.modal != null)
        Positioned(
          top: _offset.dy - _size.height - widget.size.height - 70,
          right: 0,
          left: 0,
          child: widget.modal!,
        ),
    ]);
  } 
}

class DiaryBoardTitle extends BoardProfile {
  final USER_TYPE type;
  final bool like;

  DiaryBoardTitle({this.like = false, this.type = USER_TYPE.mom_daddy, String? author, String? writtenTime, String? profileImg}) : super(author: author!, writtenTime: writtenTime!, profileImg: profileImg);

  @override
  _DiaryBoardTitleState createState() => _DiaryBoardTitleState();
}

class _DiaryBoardTitleState extends BoardProfileState<DiaryBoardTitle> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding_15_T,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          super.build(context),
          if (widget.type == USER_TYPE.link_mom)
            Container(
              margin: padding_05,
              padding: padding_10,
              decoration: BoxDecoration(
                color: widget.like ? Commons.getColor() : color_eeeeee.withOpacity(0.42),
                borderRadius: BorderRadius.circular(40),
              ),
              child: Lcons.diary_like(color: Colors.white, disableColor: color_c4c4c4, size: 25, isEnabled: widget.like),
            ),
        ],
      ),
    );
  }
}

class DiaryInfoView extends StatelessWidget {
  final ServiceType type;
  final String status;
  final String name;
  final String day;
  final String time;
  final TextStyle? statusStyle;
  final TextStyle? typeStyle;
  final TextStyle? infoStyle;
  const DiaryInfoView({
    Key? key,
    this.type = ServiceType.serviceType_0,
    this.status = "",
    this.name = "",
    this.day = "",
    this.time = "",
    this.typeStyle,
    this.statusStyle,
    this.infoStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: padding_20_LR,
      padding: EdgeInsets.fromLTRB(0, 20, 0, 15),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
      child: Container(
        padding: padding_10_B,
        child: Column(children: [
          Padding(
            padding: padding_10_B,
            child: Row(children: [
              lTextBtn(type.string, bg: type.color, style: typeStyle ?? st_13(fontWeight: FontWeight.w700)),
              sb_w_10,
              lText(status, style: statusStyle ?? st_15(textColor: Commons.getColor(), fontWeight: FontWeight.w700)),
            ]),
          ),
          Row(children: [
            lText(name),
            Container(height: 13, margin: padding_05_LR, child: lDividerVertical(thickness: 1.0, color: color_dedede)),
            lText(day, style: infoStyle ?? st_b_14()),
            Container(height: 13, margin: padding_05_LR, child: lDividerVertical(thickness: 1.0, color: color_dedede)),
            lText(time, style: infoStyle ?? st_b_14()),
          ]),
        ]),
      ),
    );
  }
}

class DiaryTask extends StatelessWidget {
  final String title;
  final int total;
  final int current;
  final bool isCompleated;
  final Function onClickAction;
  DiaryTask({this.title = '', this.total = 0, this.current = 0, required this.onClickAction, this.isCompleated = false});

  @override
  Widget build(BuildContext context) {
    return lInkWell(
        onTap: () => onClickAction(),
        child: Container(
          padding: padding_20,
          margin: padding_05_TB,
          decoration: BoxDecoration(
            color: color_eeeeee.withOpacity(0.42),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Row(children: [
            Padding(
              padding: padding_05_R,
              child: Lcons.check(color: Commons.getColor(), disableColor: color_dedede, isEnabled: isCompleated),
            ),
            total == 0
                ? lText(title, style: st_b_15(fontWeight: FontWeight.w500))
                : lCountText(
                    title,
                    current,
                    total,
                    style: st_b_15(fontWeight: FontWeight.w500),
                    pointStyle: st_15(textColor: Commons.getColor()),
                  ),
          ]),
        ));
  }
}

class ClaimText extends StatelessWidget {
  final Function onClickAction;
  const ClaimText({Key? key, required this.onClickAction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return lInkWell(
      onTap: () => onClickAction(),
      child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        Lcons.error_outline(color: color_b2b2b2, size: 15),
        sb_w_02,
        TextHighlight(
          text: "정산_정산보류하기".tr(),
          textStyle: st_12(textColor: color_b2b2b2),
          term: "정산보류가필요하다면".tr(),
          textStyleHighlight: st_12(textColor: color_main, decoration: TextDecoration.underline),
        ),
      ]),
    );
  }
}
