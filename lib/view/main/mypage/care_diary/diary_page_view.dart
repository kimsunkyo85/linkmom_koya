import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/carediary_like_request.dart';
import 'package:linkmom/data/network/models/carediary_list_response.dart';
import 'package:linkmom/data/network/models/data/care_diary_list_results.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/dropdown/custom_dropdown.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/view/lcons.dart';

import 'diary_filter_page.dart';
import 'diary_view_page.dart';
import 'diary_write_page.dart';

class DiaryPageView extends StatefulWidget {
  final DataManager data;
  final List<TabItem>? tabName;
  final CareDiaryListData leftList;
  final CareDiaryListData rightList;

  /// is linkmom == true
  final bool reverse;
  final List<String>? filterList;
  final Function onLeftRefresh;
  final Function onRightRefresh;
  DiaryPageView({this.tabName, required this.leftList, required this.rightList, this.reverse = false, this.filterList, required this.onLeftRefresh, required this.data, required this.onRightRefresh}) {
    assert(tabName != null);
  }

  @override
  _DiaryViewState createState() => _DiaryViewState();
}

class _DiaryViewState extends State<DiaryPageView> {
  String _leftVal = "전체".tr();
  String _rightVal = "전체".tr();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        if (widget.filterList != null) {
          _leftVal = widget.filterList!.first;
          _rightVal = widget.filterList!.first;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return TabPageView(
      bgColor: color_eeeeee.withOpacity(0.42),
      tabbarName: widget.tabName,
      tabbarView: [
        SliverNextListView(
          pinned: _buildFilterView(widget.reverse, widget.reverse ? 0 : 1),
          view: widget.leftList.results!
              .where((e) => _leftVal == widget.filterList!.first || e.scheduleInfo!.childName == _leftVal)
              .toList()
              .map((e) => Container(
                  margin: padding_10_B,
                  child: widget.reverse
                      ? BeforeWriteDiary(
                          type: USER_TYPE.link_mom,
                          data: e,
                          onClickAction: () => _moveToDetail(
                            e.scheduleInfo!.scheduleId,
                            e.cancelinfo!.cancelId == 0,
                            StringUtils.parseYMDHM(e.scheduleInfo!.startCaredate),
                            status: EnumUtils.getPayStatus(index: e.payStatus),
                          ),
                        )
                      : AfterWriteDiary(
                          type: USER_TYPE.mom_daddy,
                          data: e,
                          onClickAction: () => _moveToDetail(
                            e.scheduleInfo!.scheduleId,
                            e.cancelinfo!.cancelId == 0,
                            StringUtils.parseYMDHM(e.scheduleInfo!.startCaredate),
                            status: EnumUtils.getPayStatus(index: e.payStatus),
                          ),
                        )))
              .toList(),
          onRefresh: () => widget.onLeftRefresh(false),
          onNext: () => widget.onLeftRefresh(true),
          showNextLoading: widget.leftList.next.isNotEmpty,
        ),
        SliverNextListView(
          pinned: _buildFilterView(widget.reverse, widget.reverse ? 1 : 0),
          view: widget.rightList.results!
              .where((e) => _rightVal == widget.filterList!.first || e.scheduleInfo!.childName == _rightVal)
              .toList()
              .map((e) => Container(
                  margin: padding_05_TB,
                  child: widget.reverse
                      ? AfterWriteDiary(
                          type: USER_TYPE.link_mom,
                          data: e,
                          onClickAction: () => Commons.page(
                                context,
                                routeDiaryView,
                                arguments: DiaryViewPage(id: e.scheduleInfo!.scheduleId, type: storageHelper.user_type),
                              ))
                      : BeforeWriteDiary(
                          type: USER_TYPE.mom_daddy,
                          data: e,
                          onClickAction: () => _moveToDetail(
                            e.scheduleInfo!.scheduleId,
                            e.cancelinfo!.cancelId != 0,
                            StringUtils.parseYMDHM(e.scheduleInfo!.startCaredate),
                          ),
                        )))
              .toList(),
          onRefresh: () => widget.onRightRefresh(false),
          onNext: () => widget.onRightRefresh(true),
          showNextLoading: widget.rightList.next.isNotEmpty,
        ),
      ],
      emptyView: lEmptyView(height: Commons.height(context, 0.5)),
    );
  }

  Widget _buildFilterView(bool hide, int type) {
    return Container(
        padding: padding_20_LR,
        width: double.infinity,
        color: color_fafafa,
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            !hide
                ? Container(
                    width: 100,
                    height: 60,
                    child: CustomDropDown(
                      bgColor: Colors.transparent,
                      itemHeight: 50,
                      value: type == 0 ? _leftVal : _rightVal,
                      itemsList: widget.filterList ?? [],
                      icon: Lcons.nav_bottom(
                        color: color_222222,
                        size: 20,
                      ),
                      style: st_b_14(),
                      onChanged: (value) {
                        setState(() {
                          if (type == 0) {
                            _leftVal = value;
                          } else {
                            _rightVal = value;
                          }
                        });
                      },
                      isExpanded: true,
                      isDense: true,
                    ))
                : Container(),
            lInkWell(
              onTap: () => Commons.page(context, routeReportFilter, arguments: DiaryFilterPage(data: widget.data, type: type)),
              child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [Lcons.filter(size: 20), sb_w_10, lText("상세검색".tr())]),
            ),
          ],
        ));
  }

  Future<void> _moveToDetail(int id, bool isCanceld, DateTime time, {PayStatus? status}) async {
    var ret;
    if (widget.reverse) {
      if (isAfter(time)) {
        // NOTE: before care
        showNormalDlg(message: "돌봄일기_돌봄전".tr());
      } else {
        // NOTE: is linkmom and not canceled
        if (time.add(Duration(hours: 12)).isBefore(DateTime.now())) {
          Commons.page(
            context,
            routeDiaryView,
            arguments: DiaryViewPage(id: id, type: USER_TYPE.link_mom),
          );
        } else {
          if (status == PayStatus.delayed) {
            ret = await Commons.page(context, routeDiaryView, arguments: DiaryViewPage(id: id, type: storageHelper.user_type));
          } else {
            ret = await Commons.page(context, routeDiaryWrite, arguments: DiaryWritePage(id: id));
          }
        }
      }
    } else {
      ret = await Commons.page(context, routeDiaryView, arguments: DiaryViewPage(id: id, type: storageHelper.user_type));
    }
    if (ret != null) {
      widget.onRightRefresh(false);
      widget.onLeftRefresh(false);
    }
  }

  bool isAfter(DateTime time) => time.isAfter(DateTime.now());
}

class BeforeWriteDiary extends StatefulWidget {
  final USER_TYPE type;
  final CareDiaryListResults? data;
  final Function? onClickAction;
  BeforeWriteDiary({this.type = USER_TYPE.link_mom, this.data, this.onClickAction});

  @override
  _BeforeWriteDiaryState createState() => _BeforeWriteDiaryState();
}

class _BeforeWriteDiaryState extends State<BeforeWriteDiary> {
  PayStatus _payStatus = PayStatus.ready;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.data != null) _payStatus = EnumUtils.getPayStatus(index: widget.data!.payStatus);
    return widget.type == USER_TYPE.mom_daddy
        ? lInkWell(
            onTap: () => widget.onClickAction!(),
            child: Container(
                color: Colors.white,
                padding: padding_20,
                child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                  Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                      if (widget.data!.carediaryInfo != null && !widget.data!.carediaryInfo!.momdadyConfirm)
                        Container(
                          padding: padding_05_B,
                          child: lDot(),
                        ),
                      Row(children: [
                        lTextBtn(widget.data!.scheduleInfo!.servicetype, style: st_13(fontWeight: FontWeight.w700), bg: EnumUtils.getServiceType(name: widget.data!.scheduleInfo!.servicetype).color),
                        sb_w_10,
                        lText(_payStatus.name, style: st_15(textColor: _payStatus == PayStatus.ready ? Commons.getColor() : color_999999, fontWeight: FontWeight.w700))
                      ]),
                      sb_h_10,
                      Row(children: [
                        lText(widget.data!.scheduleInfo!.childName, style: st_b_14()),
                        Container(height: 13, padding: padding_06_LR, child: lDividerVertical(color: color_cecece, thickness: 1.0)),
                        lText(StringUtils.getDateYMDHM(widget.data!.scheduleInfo!.startCaredate), style: st_b_14()),
                        Container(height: 13, padding: padding_06_LR, child: lDividerVertical(color: color_cecece, thickness: 1.0)),
                        lText('${StringUtils.getTimeYMDHM(widget.data!.scheduleInfo!.startCaredate)} ~ ${StringUtils.getTimeYMDHM(widget.data!.scheduleInfo!.endCaredate)}', style: st_b_14()),
                      ]),
                    ]),
                    lInkWell(
                        onTap: () {
                          setState(() {
                            if (!widget.data!.likeYn && _payStatus.value < PayStatus.cancelReady.value) {
                              widget.data!.likeYn = true;
                              _requestLike(widget.data!.scheduleInfo!.scheduleId);
                            } else if (_payStatus.value < PayStatus.penalty.value && _payStatus.value > PayStatus.compleat.value) {
                              // note: nothing
                            } else {
                              showNormalDlg(context: context, message: "맘대디가링크쌤의돌봄활동에대해남겨주는좋아요입니다".tr());
                            }
                          });
                        },
                        child: Container(
                            padding: padding_07,
                            decoration: BoxDecoration(
                              color: widget.data!.likeYn ? Commons.getColor().withAlpha(33) : color_dedede.withAlpha(108),
                              borderRadius: BorderRadius.circular(56),
                            ),
                            child: Lcons.diary_like(isEnabled: widget.data!.likeYn, color: Commons.getColor(), disableColor: _payStatus.value < PayStatus.cancelReady.value ? color_b2b2b2 : color_dedede, size: 25))),
                  ]),
                  lDivider(color: color_eeeeee, padding: padding_10_TB),
                  Container(
                      height: 35,
                      child: Row(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.start, children: [
                        lText('${widget.data!.scheduleInfo!.linkmomName} ${"링크쌤".tr()}', style: st_b_15(fontWeight: FontWeight.w700)),
                        Container(
                            width: Commons.width(context, 0.65),
                            child: ListView(scrollDirection: Axis.horizontal, children: [
                              if (widget.data!.isChilddrug)
                                Container(
                                    margin: padding_10_L,
                                    child: lTextBtn("투약".tr(),
                                        leftIcon: Lcons.check(size: 18, isEnabled: widget.data!.isChilddrug, color: color_b579c8, disableColor: color_dedede),
                                        bg: color_eeeeee.withOpacity(0.42),
                                        radius: 8,
                                        style: st_b_13(fontWeight: FontWeight.w500))),
                              if (widget.data!.isServicetype)
                                Container(
                                    margin: padding_10_L,
                                    child: lTextBtn(widget.data!.scheduleInfo!.servicetype,
                                        leftIcon: Lcons.check(size: 18, isEnabled: widget.data!.isServicetype, color: color_linkmom, disableColor: color_dedede),
                                        bg: color_eeeeee.withOpacity(0.42),
                                        radius: 8,
                                        style: st_b_13(fontWeight: FontWeight.w500))),
                              if (widget.data!.isOptions)
                                Container(
                                    margin: padding_10_L,
                                    child: lTextBtn("필요한돌봄".tr(), leftIcon: Lcons.check(size: 18, color: color_dedede, isEnabled: true), bg: color_eeeeee.withOpacity(0.42), radius: 8, style: st_b_13(fontWeight: FontWeight.w500))),
                              if ((!widget.data!.isChilddrug && !widget.data!.isServicetype && !widget.data!.isOptions))
                                Container(
                                  width: Commons.width(context, 0.65),
                                  alignment: Alignment.center,
                                  child: lText("돌봄일기를기다려주세요".tr(), style: st_13(textColor: color_999999, fontWeight: FontWeight.w500), textAlign: TextAlign.center),
                                )
                            ]))
                      ]))
                ])))
        : lInkWell(
            onTap: () => widget.onClickAction!(),
            child: Container(
              color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: padding_20,
                      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                          Row(children: [
                            lTextBtn(widget.data!.scheduleInfo!.servicetype, style: st_13(fontWeight: FontWeight.w700), bg: EnumUtils.getServiceType(name: widget.data!.scheduleInfo!.servicetype).color),
                            sb_w_10,
                            lText(_payStatus.name, style: st_15(textColor: _payStatus == PayStatus.ready ? Commons.getColor() : color_999999, fontWeight: FontWeight.w700)),
                          ]),
                          sb_h_10,
                          Row(children: [
                            lText(widget.data!.scheduleInfo!.childName, style: st_b_14()),
                            Container(height: 13, padding: padding_06_LR, child: lDividerVertical(color: color_cecece, thickness: 1.0)),
                            lText(StringUtils.getDateYMDHM(widget.data!.scheduleInfo!.startCaredate), style: st_b_14()),
                            Container(height: 13, padding: padding_06_LR, child: lDividerVertical(color: color_cecece, thickness: 1.0)),
                            lText('${StringUtils.getTimeYMDHM(widget.data!.scheduleInfo!.startCaredate)} ~ ${StringUtils.getTimeYMDHM(widget.data!.scheduleInfo!.endCaredate)}', style: st_b_14()),
                          ]),
                        ]),
                      ])),
                  lDivider(color: color_eeeeee),
                  Container(
                      padding: padding_20_TB,
                      child: _payStatus == PayStatus.cancelReady || _payStatus == PayStatus.penaltyReady
                          ? lText("취소예정".tr(), style: st_14(textColor: color_545454, fontWeight: FontWeight.w500))
                          : Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [lText("돌봄일기작성하기".tr(), style: st_14(textColor: color_545454, fontWeight: FontWeight.w500)), sb_w_05, Lcons.create(size: 14, disableColor: color_999999)])),
                ],
              ),
            ),
          );
  }

  void _requestLike(int id) {
    try {
      CareDiaryLikeRequest req = CareDiaryLikeRequest(carecontract_schedule: id);
      apiHelper.requestDiaryLike(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          widget.data!.likeYn = true;
        }
      });
    } catch (e) {
      log.e("Exception: >>>>>>>>>>>> $e");
    }
  }
}

class AfterWriteDiary extends StatefulWidget {
  final USER_TYPE type;
  final CareDiaryListResults? data;
  final Function? onClickAction;
  AfterWriteDiary({this.type = USER_TYPE.mom_daddy, this.data, this.onClickAction});
  @override
  _AfterWriteDiaryState createState() => _AfterWriteDiaryState();
}

class _AfterWriteDiaryState extends State<AfterWriteDiary> {
  PayStatus _payStatus = PayStatus.ready;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.data != null) _payStatus = EnumUtils.getPayStatus(index: widget.data!.payStatus);
    return lInkWell(
        onTap: () => widget.onClickAction!(),
        child: Container(
            color: Colors.white,
            padding: padding_20,
            child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Stack(children: [
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  if (widget.type == USER_TYPE.mom_daddy && !widget.data!.carediaryInfo!.momdadyConfirm)
                    Container(
                      padding: padding_05_B,
                      child: lDot(),
                    ),
                  Row(children: [
                    lTextBtn(widget.data!.scheduleInfo!.servicetype, style: st_13(fontWeight: FontWeight.w700), bg: EnumUtils.getServiceType(name: widget.data!.scheduleInfo!.servicetype).color, padding: EdgeInsets.fromLTRB(10, 5, 10, 5)),
                    sb_w_10,
                    lText(_payStatus.name, style: st_15(textColor: _payStatus == PayStatus.ready ? Commons.getColor() : color_999999, fontWeight: FontWeight.w700))
                  ]),
                  sb_h_10,
                  Row(children: [
                    lText(widget.data!.scheduleInfo!.childName, style: st_b_14()),
                    Container(height: 13, padding: padding_06_LR, child: lDividerVertical(color: color_cecece, thickness: 1.0)),
                    lText(StringUtils.getDateYMDHM(widget.data!.scheduleInfo!.startCaredate), style: st_b_14()),
                    Container(height: 13, padding: padding_06_LR, child: lDividerVertical(color: color_cecece, thickness: 1.0)),
                    lText('${StringUtils.getTimeYMDHM(widget.data!.scheduleInfo!.startCaredate)} ~ ${StringUtils.getTimeYMDHM(widget.data!.scheduleInfo!.endCaredate)}', style: st_b_14()),
                  ]),
                ]),
                if (!Commons.isLinkMom())
                  Positioned(
                    top: 0,
                    right: 0,
                    child: lInkWell(
                        onTap: () {
                          if (widget.type == USER_TYPE.mom_daddy && !widget.data!.likeYn) {
                            setState(() {
                              widget.data!.likeYn = true;
                              _requestLike(widget.data!.scheduleInfo!.scheduleId);
                            });
                          } else {
                            showNormalDlg(context: context, message: "맘대디가링크쌤의돌봄활동에대해남겨주는좋아요입니다".tr());
                          }
                        },
                        child: Container(
                            padding: padding_07,
                            decoration: BoxDecoration(
                              color: widget.data!.likeYn ? Commons.getColor().withAlpha(82) : color_dedede.withAlpha(108),
                              borderRadius: BorderRadius.circular(56),
                            ),
                            child: Lcons.diary_like(isEnabled: widget.data!.likeYn, color: Commons.getColor(), disableColor: color_b2b2b2, size: 25))),
                  ),
              ]),
              lDivider(color: color_eeeeee, padding: padding_10_TB),
              Container(
                  child: Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Container(
                    padding: padding_06_TB,
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                      if (widget.type == USER_TYPE.mom_daddy) Container(margin: padding_06_B, child: lText('${widget.data!.scheduleInfo!.linkmomName} ${USER_TYPE.link_mom.name}', style: st_b_15(fontWeight: FontWeight.w700))),
                      Container(
                        width: Commons.width(context, widget.data!.carediaryInfo!.thumbnail.isNotEmpty ? 0.7 : 0.88),
                        child: lText(widget.data!.carediaryInfo!.content, maxLines: 2, style: st_b_14(textColor: color_545454), overflow: TextOverflow.ellipsis),
                      ),
                    ])),
                if (widget.data!.carediaryInfo!.thumbnail.isNotEmpty)
                  ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: CachedImage(widget.data!.carediaryInfo!.thumbnail),
                  )
              ])),
              Row(children: [
                lText(StringUtils.parseYMDAHM(widget.data!.carediaryInfo!.updatedate), style: st_b_13(textColor: color_b2b2b2)),
                Row(children: [
                  Container(
                    width: 2,
                    height: 2,
                    margin: padding_05_LR,
                    child: lDot(color: color_b2b2b2),
                  ),
                  Lcons.chat(color: color_b2b2b2, size: 16),
                  sb_w_05,
                  lText(widget.data!.carediaryInfo!.replyCnt.toString(), style: st_b_13(textColor: color_b2b2b2))
                ]),
              ]),
            ])));
  }

  void _requestLike(int id) {
    try {
      CareDiaryLikeRequest req = CareDiaryLikeRequest(carecontract_schedule: id);
      apiHelper.requestDiaryLike(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          setState(() {
            widget.data!.likeYn = true;
          });
        }
      });
    } catch (e) {
      log.e("Exception: >>>>>>>>>>>> $e");
    }
  }
}
