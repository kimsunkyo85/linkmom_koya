import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/carediary_claim_save_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/compleat_view_page.dart';
import 'package:provider/provider.dart';

import '../../../../data/providers/image_loader_provider.dart';
import '../../../image/image_add_view.dart';

class DiaryClaimAnswerPage extends BaseStateful {
  final int id;
  final int claimReason;

  DiaryClaimAnswerPage({
    this.id = 0,
    this.claimReason = 0,
  });

  @override
  _DiaryClaimAnswerPageState createState() => _DiaryClaimAnswerPageState();
}

class _DiaryClaimAnswerPageState extends BaseStatefulState<DiaryClaimAnswerPage> {
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    context.read<ImageLoader>().loadImage([], context);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          resizeToAvoidBottomInset: false,
          context: context,
          appBar: appBar("정산보류".tr(), hide: false),
          body: Column(
            children: [
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      color: Colors.white,
                      padding: padding_20,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: double.infinity,
                            padding: padding_10,
                            decoration: BoxDecoration(color: Colors.white, border: Border.all(color: color_dedede), borderRadius: BorderRadius.circular(8)),
                            child: TextField(
                              controller: _controller,
                              style: st_b_14(),
                              decoration: InputDecoration(
                                fillColor: Colors.white,
                                hintMaxLines: 10,
                                hintText: "정산_소명요청안내".tr(),
                                hintStyle: st_14(textColor: color_b2b2b2),
                                isDense: true,
                                contentPadding: padding_0,
                                border: OutlineInputBorder(borderSide: BorderSide.none),
                              ),
                              maxLines: 5,
                              maxLength: 1000,
                              textInputAction: TextInputAction.newline,
                              buildCounter: (_, {required currentLength, required isFocused, maxLength}) => Padding(
                                padding: padding_25_L,
                                child: Container(
                                    alignment: Alignment.centerRight,
                                    child: lText(
                                      currentLength.toString() + "/" + StringUtils.formatPay(maxLength!),
                                      style: st_15(textColor: color_b2b2b2),
                                    )),
                              ),
                              onChanged: (v) => onUpDate(),
                            ),
                          ),
                          Container(
                              padding: padding_20_T,
                              child: ChangeNotifierProvider<ImageLoader>(
                                create: (_) => ImageLoader(),
                                builder: (ctx, child) => Container(
                                  child: ImageAddView(
                                    max: 5,
                                    data: context.read<ImageLoader>().getImages,
                                    onItemClick: (image, index) {
                                      context.read<ImageLoader>().setImages = image;
                                      onConfirmBtn();
                                    },
                                  ),
                                ),
                              )),
                          lDingbatText(
                            width: Commons.width(context, 0.85),
                            text: "소명하기_링크쌤2".tr(),
                            style: st_14(textColor: color_545454),
                            dingbatColor: color_545454,
                            dingbatSize: 2,
                            padding: padding_30_T,
                          )
                        ],
                      ))
                ],
              )),
              lBtn("소명하기".tr(), isEnabled: _controller.text.isNotEmpty, onClickAction: () => showNormalDlg(context: context, message: "소명하기_안내".tr(), btnLeft: "취소".tr(), onClickAction: (a) => {if (a == DialogAction.yes) _requestClaimAnswer()}))
            ],
          ),
        ));
  }

  void _requestClaimAnswer() {
    try {
      CareDiaryClaimSaveRequest req = CareDiaryClaimSaveRequest(
        schedule: widget.id,
        image: context.read<ImageLoader>().getImages,
        content2: _controller.text,
      );
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestDiaryClaimSaveLinkmom(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.page(context, routeCompleteView,
              arguments: CompleteViewPage(
                data: data,
                title: "접수완료".tr(),
                resultMessage: "정산_소명요청완료".tr(),
                footer: lText("정산_소명요청완료안내".tr(), style: st_b_14(), textAlign: TextAlign.center),
                finishAction: () => Commons.pageReplace(context, routeLinkmomDiary, isPrevious: true),
                btnColor: color_b579c8,
              ));
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>>>>>> $e");
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}
