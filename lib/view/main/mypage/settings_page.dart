import 'package:cupertino_will_pop_scope/cupertino_will_pop_scope.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/settings_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/login/unregister/register_disable_pw_page.dart';
import 'package:linkmom/view/main/mypage/settings_block_page.dart';
import 'package:linkmom/view/main/mypage/settings_noti_page.dart';
import 'package:provider/provider.dart';

class SettingsPage extends BaseStateful {
  SettingsPage();

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends BaseStatefulState<SettingsPage> {
  @override
  void initState() {
    super.initState();
    //2022/04/26 로그인과 splash 요청
    // flag.enableLoading(fn: () => onUpDate());
    // _requestSettingsView();
    if (deviceInfo.getVersion() != '${storageHelper.appVersion.verMajor}.${storageHelper.appVersion.verMinor}.${storageHelper.appVersion.verPatch}') {
      if (deviceInfo.major < storageHelper.appVersion.verMajor && deviceInfo.minor < storageHelper.appVersion.verMinor && deviceInfo.patch < storageHelper.appVersion.verPatch) Commons.setUpdateFlag = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ConditionalWillPopScope(
        shouldAddCallback: true,
        onWillPop: () {
          _saveRequest();
          return Future.value(true);
        },
        child: lModalProgressHUD(
            flag.isLoading,
            lScaffold(
                appBar: appBar("설정".tr(), hide: false, onBack: () {
                  _saveRequest();
                }),
                body: ChangeNotifierProvider(
                  create: (ctx) => storageHelper,
                  child: Container(
                    padding: padding_20,
                    child: Column(
                      children: [
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            _textButton("알림".tr(), nextPage: true, onClickAction: () async {
                              var ret = await Commons.page(context, routeNotiSettings, arguments: SettingsNotiPage(data: auth.getSettings));
                              if (ret != null) {
                                auth.setSettings = ret;
                              }
                            }),
                            lDivider(color: color_eeeeee),
                            Container(
                                padding: padding_15_TB,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    lText("자동로그인".tr(), style: st_b_15(fontWeight: FontWeight.w500)),
                                    FlutterSwitch(
                                        width: 60,
                                        padding: 3.0,
                                        activeColor: Commons.getColor(),
                                        inactiveColor: color_cecece,
                                        toggleSize: 30,
                                        value: auth.getSettings.is_auth_login,
                                        onToggle: (value) {
                                          auth.getSettings.is_auth_login = value;
                                          auth.setSettings = auth.getSettings;
                                          storageHelper.setSettings(auth.getSettings);
                                          onUpDate();
                                        }),
                                  ],
                                )),
                            lDivider(color: color_eeeeee),
                            _textButton("로그아웃".tr(), onClickAction: () => _logoutRequest()),
                            lDivider(color: color_eeeeee),
                            _textButton("차단관리".tr(), nextPage: true, onClickAction: () => Commons.page(context, routeSettingsUserBlock, arguments: SettingsUserBlockPage(data: data))),
                            lDivider(color: color_eeeeee),
                            _textButton("탈퇴하기".tr(), nextPage: true, onClickAction: () => Commons.page(context, routeRegisterDisablePw, arguments: RegisterDisablePwPage())),
                            lDivider(color: color_eeeeee),
                            Container(
                              padding: padding_20_TB,
                              alignment: Alignment.centerLeft,
                              child: lIconText(
                                  title: '${"현재버전".tr()} ${deviceInfo.getVersion()}',
                                  icon: lTextBtn(
                                    Commons.needUpdate ? "업데이트".tr() : "최신버전".tr(),
                                    style: st_b_12(textColor: Commons.needUpdate ? color_222222 : color_999999),
                                    padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                                    onClickEvent: () {
                                      if (Commons.needUpdate) {
                                        Commons.pagePop(context);
                                        showDlgVersion(
                                            msg: storageHelper.appVersion.contents,
                                            onClickAction: (a) {
                                              if (a == DialogAction.update) {
                                                Commons.lLaunchUrl(storageHelper.appVersion.marketUrl);
                                              }
                                            });
                                      }
                                    },
                                    bg: Commons.needUpdate ? color_eeeeee : Colors.transparent,
                                  ),
                                  textStyle: st_b_15(fontWeight: FontWeight.w500),
                                  padding: padding_0,
                                  isLeft: false,
                                  onClick: () => {}),
                            ),
                            lDivider(color: color_eeeeee),
                          ],
                        ))
                      ],
                    ),
                  ),
                ))));
  }

  Widget _textButton(String title, {bool disable = false, required Function onClickAction, bool nextPage = false}) {
    return lInkWell(
      onTap: () => onClickAction(),
      child: Container(
        alignment: Alignment.centerLeft,
        padding: padding_20_TB,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            lText(title, style: st_b_15(textColor: disable ? color_b2b2b2 : color_222222, fontWeight: FontWeight.w500)),
            nextPage ? Lcons.nav_right(size: 15, color: disable ? color_cecece : color_999999) : Container(),
          ],
        ),
      ),
    );
  }

  void _saveRequest() {
    try {
      SettingsRequest req = SettingsRequest(
          is_push_linkmom: auth.getSettings.is_push_linkmom ? 1 : 0,
          is_push_momdady: auth.getSettings.is_push_momdady ? 1 : 0,
          is_push_event: auth.getSettings.is_push_event ? 1 : 0,
          is_push_service: auth.getSettings.is_push_service ? 1 : 0,
          is_push_community: auth.getSettings.is_push_community ? 1 : 0,
          is_push_care: auth.getSettings.is_push_care ? 1 : 0,
          is_push_job: auth.getSettings.is_push_job ? 1 : 0,
          is_auth_login: auth.getSettings.is_auth_login ? 1 : 0);
      apiHelper.requestSettings(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          auth.setSettings = response.getData();
          storageHelper.setSettings(response.getData());
          if (mounted) {
            Commons.pagePop(context);
          }
        } else {
          showNormalDlg(message: response.getMsg());
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _logoutRequest() {
    showNormalDlg(
        context: context,
        message: "로그아웃안내".tr(),
        btnRight: "확인".tr(),
        btnLeft: "취소".tr(),
        onClickAction: (action) async {
          if (action == DialogAction.yes) {
            await auth.requestLogout(context);
          }
        });
  }

//2022/04/26 로그인과 splash 요청
// void _requestSettingsView() {
//   try {
//     apiHelper.requestSettingsView().then((response) {
//       if (response.getCode() == KEY_SUCCESS) {
//         storageHelper.setSettings(response.getData());
//       }
//       flag.disableLoading(fn: () => onUpDate());
//     }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
//   } catch (e) {
//     flag.disableLoading(fn: () => onUpDate());
//   }
// }
}
