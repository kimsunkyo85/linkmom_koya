import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/data/network/models/coupon_view_request.dart';
import 'package:linkmom/data/network/models/coupon_view_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

class CouponDetailPage extends BaseStateful {
  final int id;
  CouponDetailPage({this.id = 0});

  @override
  _CouponDetailPageState createState() => _CouponDetailPageState();
}

class _CouponDetailPageState extends BaseStatefulState<CouponDetailPage> {
  CouponViewData _coupon = CouponViewData();

  @override
  void initState() {
    super.initState();
    _requestCouponDetail(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          appBar: appBar("혜택쿠폰함".tr(), hide: false),
          body: LinkmomRefresh(
            padding: padding_20_TB,
            onRefresh: () => _requestCouponDetail(widget.id),
            child: Column(
              children: [
                Container(
                  padding: padding_20_LR,
                  child: Column(
                    children: [
                      Container(
                        height: 162,
                        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                          CachedNetworkImage(
                            imageUrl: _coupon.couponBrandBi,
                            width: 58,
                            height: 16,
                            errorWidget: (ctx, value, _) => lText(
                              _coupon.couponBrand,
                              style: st_15(textColor: color_222222, fontWeight: FontWeight.bold),
                            ),
                          ),
                          sb_h_08,
                          lText(_coupon.couponName, style: st_26(textColor: Commons.getColor(), fontWeight: FontWeight.bold)),
                          sb_h_15,
                          lTextBtn(
                            "사이트바로가기".tr(),
                            style: st_b_15(fontWeight: FontWeight.w500),
                            rightIcon: Lcons.nav_right(color: color_cecece),
                            bg: Colors.white,
                            radius: 32,
                            padding: EdgeInsets.fromLTRB(15, 08, 15, 08),
                            border: Border.all(color: color_cecece.withOpacity(0.62)),
                            onClickEvent: () => Commons.lLaunchUrl(_coupon.couponBrandSite),
                          ),
                        ]),
                      ),
                      lDivider(color: color_eeeeee, padding: padding_30_TB),
                      _infoContainer("사용기간".tr(), StringUtils.fromTo(_coupon.usePeriodSdate, _coupon.usePeriodEdate) + ' ' + "까지".tr()),
                      _infoContainer(
                        "쿠폰번호".tr(),
                        _coupon.couponCode,
                        btn: lTextBtn(
                          "복사".tr(),
                          bg: Colors.white,
                          border: Border.all(color: color_dedede),
                          radius: 24,
                          style: st_13(textColor: color_545454, fontWeight: FontWeight.w500),
                          onClickEvent: () => {Commons.copyToClipBoard(_coupon.couponCode, message: _coupon.couponCode + ' ' + "복사되었습니다".tr())},
                        ),
                      ),
                      _infoContainer('쿠폰사용횟수', _coupon.couponUseCntInfo),
                    ],
                  ),
                ),
                lDivider20(),
                Container(
                  padding: padding_20_LR,
                  child: Html(
                    customRenders: {
                      networkSourceMatcher(): networkImageRender(
                        loadingWidget: () => LoadingIndicator(
                          showText: false,
                          background: color_transparent,
                        ),
                      ),
                    },
                    data: _coupon.couponInfo,
                    style: {
                      'p': Style(
                        fontFamily: defaultStyle().fontFamily,
                      ),
                    },
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  Widget _infoContainer(String title, String value, {Widget? btn}) {
    return Container(
      padding: padding_10,
      margin: padding_10_T,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          lText(title, style: st_15(fontWeight: FontWeight.w500, textColor: color_b2b2b2)),
          sb_h_10,
          lIconText(title: value, textStyle: st_b_15(), icon: btn, isLeft: false, padding: padding_10_R, isExpanded: false),
        ],
      ),
    );
  }

  Widget _howToContainer(String title, List<String> value) {
    return Container(
      width: double.infinity,
      padding: padding_10,
      margin: padding_10_T,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          lText(title, style: st_b_16(fontWeight: FontWeight.w500)),
          sb_h_15,
          Column(
            children: value
                .map((e) => lDingbatText(
                      text: e,
                      dingbatColor: color_545454,
                      dingbatSize: 2,
                      padding: padding_05_B,
                      style: st_b_13(),
                    ))
                .toList(),
          )
        ],
      ),
    );
  }

  void _requestCouponDetail(int id, {bool isNext = false}) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      CouponViewRequest req = CouponViewRequest(id: id);
      apiHelper.requestCouponView(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _coupon = response.getData();
        } else {
          _coupon = CouponViewData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        log.e('_requestCouponDetail >>>>>>>>>>>> e');
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }
}
