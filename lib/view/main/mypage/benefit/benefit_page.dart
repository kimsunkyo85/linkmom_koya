import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/data/network/api_header.dart';
import 'package:linkmom/data/network/models/coupon_list_request.dart';
import 'package:linkmom/data/network/models/coupon_list_responst.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/svg_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:provider/provider.dart';

import 'coupon_detail_page.dart';

class BenefitPage extends BaseStateful {
  final DataManager? data;
  BenefitPage({this.data});

  @override
  _BenefitPageState createState() => _BenefitPageState();
}

class _BenefitPageState extends BaseStatefulState<BenefitPage> with SingleTickerProviderStateMixin {
  late TabController _tabCtrl;
  int _availableCnt = 0;
  int _expiredCnt = 0;

  @override
  void initState() {
    super.initState();
    _tabCtrl = TabController(length: 2, vsync: this);
    _whileGetCoupons();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar("혜택쿠폰함".tr()),
        body: TabPageView(
          onCreateView: (ctrl) {
            _tabCtrl = ctrl;
          },
          tabbarName: [
            TabItem(title: '${"사용가능".tr()} $_availableCnt'),
            TabItem(title: '${"사용기간만료".tr()} $_expiredCnt'),
          ],
          tabbarView: [
            Container(
              padding: padding_20_T,
              child: ChangeNotifierProvider<CouponList>(
                create: (context) => CouponList(),
                child: ListNextView(
                  view: context.read<CouponList>().coupons.map((e) => _buildCoupon(e)).toList(),
                  onRefresh: () => _requestCoupon(),
                  onNext: () => _requestCoupon(next: context.read<CouponList>().next, isNext: true),
                  showNextLoading: context.read<CouponList>().next.isNotEmpty,
                ),
              ),
            ),
            Container(
              padding: padding_20_T,
              child: ChangeNotifierProvider<ExpiredList>(
                create: (context) => ExpiredList(),
                child: ListNextView(
                  view: context.read<ExpiredList>().coupons.map((e) => _buildCoupon(e, isDisable: true)).toList(),
                  onRefresh: () => _requestCoupon(isExpired: true),
                  onNext: () => _requestCoupon(next: context.read<ExpiredList>().next, isNext: true, isExpired: true),
                  showNextLoading: context.read<ExpiredList>().next.isNotEmpty,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildCoupon(CouponListData coupon, {bool isDisable = false}) {
    return Container(
      margin: padding_15_B,
      child: Stack(
        alignment: Alignment.center,
        children: [
          SvgPicture.asset(SVG_COUPON_BG, height: 184),
          Container(
            height: 184,
            padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
            width: Commons.width(context, 1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                lInkWell(
                  onTap: () => {if (!isDisable) Commons.page(context, routeCouponDetail, arguments: CouponDetailPage(id: coupon.id))},
                  child: Container(
                    padding: EdgeInsets.fromLTRB(20, 5, 20, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                          CachedNetworkImage(
                              imageUrl: coupon.couponBrandBi,
                              width: 58,
                              height: 16,
                              errorWidget: (ctx, value, _) => lText(
                                    coupon.couponBrand,
                                    style: st_15(
                                      textColor: isDisable ? color_b2b2b2 : color_222222,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )),
                          if (!isDisable)
                            lIconText(
                              title: "사용조건".tr(),
                              icon: Lcons.nav_right(size: 15, color: color_cecece),
                              isLeft: false,
                              isExpanded: false,
                              textStyle: st_13(textColor: color_b2b2b2, fontWeight: FontWeight.w500),
                              padding: padding_02_R,
                            ),
                        ]),
                        lText(
                          coupon.couponName,
                          style: st_24(textColor: isDisable ? color_b2b2b2 : Commons.getColor(), fontWeight: FontWeight.bold),
                        ),
                        sb_h_06,
                        lText(StringUtils.fromTo(coupon.usePeriodSdate, coupon.usePeriodEdate) + ' ' + "까지".tr(), style: st_13(textColor: isDisable ? color_b2b2b2 : color_b2b2b2)),
                      ],
                    ),
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    lText(coupon.couponCode, style: st_13(textColor: isDisable ? color_b2b2b2 : color_222222)),
                    sb_w_10,
                    lTextBtn(
                      "복사".tr(),
                      bg: Colors.white,
                      border: Border.all(color: color_dedede),
                      radius: 24,
                      style: st_13(textColor: isDisable ? color_999999 : color_545454, fontWeight: FontWeight.w500),
                      onClickEvent: () => {if (!isDisable) Commons.copyToClipBoard(coupon.couponCode, message: coupon.couponCode + ' ' + "복사되었습니다".tr())},
                    ),
                  ],
                ),
              ],
            ),
          ),
          if (isDisable) SvgPicture.asset(SVG_COUPON_DISABLE, height: 184),
        ],
      ),
    );
  }

  Future<void> _requestCoupon({bool isExpired = false, String next = '', bool isNext = false}) async {
    try {
      CouponListRequest req = CouponListRequest(gubun: isExpired ? 1 : 0);
      if (!isNext) {
        apiHelper.requestCouponList(req).then((response) {
          if (response.getCode() == KEY_SUCCESS) {
            if (isExpired) {
              context.read<ExpiredList>().clearCoupon();
              _expiredCnt = response.getData().count;
              context.read<ExpiredList>().appendCoupon(response.getData().results ?? []);
            } else {
              context.read<CouponList>().clearCoupon();
              _availableCnt = response.getData().count;
              context.read<CouponList>().refreshCoupon(response.getData().results ?? []);
            }
          }
          flag.disableLoading(fn: () => onUpDate());
        }).catchError((e) {
          log.e('_requestBenefit >>>>>>>>>>>> e');
          flag.disableLoading(fn: () => onUpDate());
        });
      } else {
        if (next.isEmpty) {
          return;
        }
        var response = await apiHelper.apiCall(HttpType.post, next, ApiHeader().publicApiHeader, req);
        var responseJson = Commons.returnResponse(response);
        CouponListResponse responseData = CouponListResponse.fromJson(responseJson);

        if (isExpired) {
          context.read<ExpiredList>().appendCoupon(responseData.getData().results ?? []);
        } else {
          context.read<CouponList>().appendCoupon(responseData.getData().results ?? []);
        }
      }
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }

  void _whileGetCoupons() {
    int count = 2;
    Future.forEach(List.generate(count, (index) => index), (gubun) async {
      flag.enableLoading(fn: () => onUpDate());
      await _requestCoupon(isExpired: gubun == 1);
      flag.disableLoading(fn: () => onUpDate());
    });
  }
}

class CouponList extends ChangeNotifier {
  List<CouponListData> coupons = [];
  String next = '';
  String previous = '';
  int cnt = 0;

  void refreshCoupon(List<CouponListData> list) {
    coupons.clear();
    coupons.addAll(list);
    notifyListeners();
  }

  void appendCoupon(List<CouponListData> list) {
    coupons.addAll(list);
    notifyListeners();
  }

  void clearCoupon() {
    coupons.clear();
    notifyListeners();
  }

  void setUrl(String next, String previous) {
    this.next = next;
    this.previous = previous;
  }

  set count(int count) => cnt = count;

  int get count => cnt;
}

class ExpiredList extends CouponList {
  @override
  void refreshCoupon(List<CouponListData> list) {
    coupons.clear();
    coupons.addAll(list);
    notifyListeners();
  }

  @override
  void appendCoupon(List<CouponListData> list) {
    coupons.addAll(list);
    notifyListeners();
  }

  @override
  void clearCoupon() {
    coupons.clear();
    notifyListeners();
  }
}
