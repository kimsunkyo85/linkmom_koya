import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/child_info_delete_request.dart';
import 'package:linkmom/data/network/models/child_info_request.dart';
import 'package:linkmom/data/network/models/child_info_response.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_child_info_select.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/job_apply/child_info_view.dart';
import 'package:linkmom/view/main/job_apply/content_view.dart';
import 'package:linkmom/view/main/job_apply/parents/parents_step_2_page.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../../../../route_name.dart';
import 'child_drug_list_page.dart';
import 'child_info_add_page.dart';

class ChildInfoPage extends BaseStateful {
  @override
  _ChildInfoPageState createState() => _ChildInfoPageState();

  ChildInfoPage({this.data, this.viewMode, this.mode = ChildListPageMode.childInfo});

  final DataManager? data;
  final ViewMode? viewMode;
  final ChildListPageMode? mode;
}

class _ChildInfoPageState extends BaseStatefulState<ChildInfoPage> {
  PageController controller = PageController();

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
    log.d('『GGUMBI』>>> initState : viewType: ${getViewType()}, \nwidget.data: ${widget.data},  <<< ');

    ///환경인증 가져오기 요청
    data.jobItem.childInfo.step = 1;
    data.jobItem.childInfo.isShow = true;
    if (widget.mode == ChildListPageMode.drugInfo) data.jobItem.childInfo.isShow = false;
    _requestChildInfo();
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    log.d('『GGUMBI』>>> onData : _data: $_data,  <<< ');
    // setViewType(_data?.viewType);
    data.jobItem.childInfo = ChildInfo.clone(auth.childInfo);
    data.jobItem.childInfo.step = 1;
    if (getViewType() == ViewType.modify) {
      data.jobItem.childInfo.isShow = false;
    }
    // data.jobItem.request = _data.jobItem.request;
    // onDataPage(_data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        resizeToAvoidBottomPadding: false,
        appBar: appBar(widget.mode == ChildListPageMode.childInfo ? "아이등록".tr() : "투약의뢰서".tr(), isClose: false, hide: getViewType() == ViewType.apply ? true : false),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            if (getViewType() == ViewType.apply) lPercentIndicator(step: data.jobItem.childInfo.step),
            Expanded(
              child: Column(
                children: [
                  Expanded(
                    child: lScrollView(
                      padding: padding_Item,
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            if (getViewType() == ViewType.apply) childInfoView(data.jobItem.childInfo),
                            if (getViewType() != ViewType.apply) sb_h_20,
                            contentView(data: ContentData(content: "아이선택_안내".tr())),
                            sb_h_25,
                            ChildInfoSelector(
                              data: data.childInfoItem.lsChildInfo,
                              mode: widget.mode,
                              viewType: getViewType(),
                              onItemClick: getViewType() == ViewType.modify
                                  ? null
                                  : (value) async {
                                      if (value != null) {
                                        ChildInfoItemData item = value as ChildInfoItemData;
                                        if (item.type == ItemType.modify) {
                                          if (getViewType() == ViewType.apply) return;

                                          data.childInfoItem.itemData = item.childData;
                                          auth.onUpdateChildInfo(1, item.childName, item.childAge, item.childNursery, item.childGender, ChildInfoData.getCharatersString(item.childCharacters), data: data.childInfoItem.itemData);
                                          var value = await Commons.page(context, routeChildInfoAdd, arguments: ChildInfoAddPage(data: data, child_id: item.childData.id, viewMode: ViewMode(viewType: ViewType.modify, itemType: item.type)));
                                          if (value != null) {
                                            _requestChildInfo();
                                          }
                                          //삭제
                                        } else if (item.type == ItemType.delete) {
                                          _requestChildInfoDelete(ChildInfoDeleteRequest(child_id: item.childData.id));
                                          data.childInfoItem.lsChildInfo.removeAt(item.index);
                                        } else {
                                          if (item.type == ItemType.select) {
                                            data.childInfoItem.itemData = item.childData;
                                            auth.onUpdateChildInfo(1, item.childName, item.childAge, item.childNursery, item.childGender, ChildInfoData.getCharatersString(item.childCharacters), data: data.childInfoItem.itemData);
                                          }
                                          if (widget.viewMode != null && widget.viewMode!.viewType != ViewType.apply) {
                                            var value = await Commons.page(context, routeChildInfoAdd, arguments: ChildInfoAddPage(data: data, child_id: item.childData.id, viewMode: ViewMode(viewType: ViewType.modify, itemType: ItemType.modify)));
                                            if (value != null) {
                                              _requestChildInfo();
                                            }
                                          }
                                        }
                                      }
                                      onConfirmBtn();
                                    },
                            ),
                              lBtnOutline(
                                "+아이추가".tr(),
                                sideColor: color_dedede,
                                textColor: color_b2b2b2,
                                margin: padding_20_TB,
                                style: st_16(fontWeight: FontWeight.w500, textColor: color_999999),
                                onClickAction: () async {
                                  setItemType(ItemType.add);
                                  var value = await Commons.page(context, routeChildInfoAdd, arguments: ChildInfoAddPage(data: data, child_id: 0, viewMode: ViewMode(itemType: ItemType.add, viewType: ViewType.add)));
                                  if (value != null) {
                                    _requestChildInfo();
                                  }
                                },
                              )
                          ],
                        ),
                      ),
                    ),
                  ),
                  if ((getViewType() == ViewType.apply))
                    lBtn("다음".tr(), isEnabled: flag.isConfirm, onClickAction: () {
                      if (widget.mode != null && widget.mode == ChildListPageMode.drugInfo) {
                        Commons.page(context, routeChildDrugList, arguments: ChildDrugListPage(data: data, child: data.childInfoItem));
                      } else {
                        setViewType(ViewType.apply);
                        data.jobItem.request.child = data.childInfoItem.itemData.id;
                        Commons.page(context, routeParents2, arguments: ParentsStep2Page(data: data));
                      }
                    }, btnColor: Commons.getColor()),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void onConfirmBtn() {
    //data가 있는 경우만
    if (data.isChildInfoData()) {
      flag.disableConfirm(fn: () => onUpDate());
      data.childInfoItem.lsChildInfo.forEach((value) {
        if (value.isSelected) {
          flag.enableConfirm(fn: () => onUpDate());
        }
      });
    }
  }

  ///아이정보 가져오기 요청
  void _requestChildInfo() async {
    flag.enableLoading(fn: () => onUpDate());
    await apiHelper.requestMyPageChildInfo(ChildInfoRequest(), HttpType.patch, ApiEndPoint.EP_MYPAGE_CHILD_INFO_LIST).then((response) {
      data.childInfoItem.response = response;
      flag.disableLoading(fn: () => onUpDate());

      data.childInfoItem.lsChildInfo.clear();
      if (response.getCode() == KEY_SUCCESS) {
        onDataPage(response);
      }
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  ///아이정보 삭제 요청
  void _requestChildInfoDelete(ChildInfoDeleteRequest _data) async {
    flag.enableLoading(fn: () => onUpDate());
    await apiHelper.requestMyPageChildInfoDelete(_data, ApiEndPoint.EP_MYPAGE_CHILD_INFO_DELETE).then((response) {
      flag.disableLoading(fn: () => onUpDate());
      if (response.getCode() == KEY_SUCCESS) {
        // onDataPage(response);
      }
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  @override
  void onDataPage(_data) {
    ChildInfoResponse response = _data as ChildInfoResponse;
    response.dataList!.asMap().forEach((index, value) {
      data.childInfoItem.itemData = response.dataList![index];
      data.childInfoItem.lsChildInfo.add(ChildInfoItemData(
        childData: data.childInfoItem.itemData,
        childName: data.childInfoItem.itemData.child_name,
        childAge: data.childInfoItem.itemData.child_age,
        childNursery: data.childInfoItem.itemData.child_nursery,
        childGender: data.childInfoItem.itemData.child_gender,
        childCharacters: [],
      ));
    });
    onUpDate();
    onConfirmBtn();
  }
}
