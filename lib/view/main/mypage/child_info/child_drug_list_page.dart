import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/child_drug_list_request.dart';
import 'package:linkmom/data/network/models/child_drug_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/child_info/child_drug_add_page.dart';
import 'package:linkmom/view/main/mypage/child_info/child_drug_view_page.dart';

class ChildDrugListPage extends BaseStateful {
  final DataManager? data;
  final ChildInfoItem? child;
  ChildDrugListPage({this.data, this.child});

  @override
  _ChildDrugListPageState createState() => _ChildDrugListPageState();
}

class _ChildDrugListPageState extends BaseStatefulState<ChildDrugListPage> {
  bool _enableLoader = false;
  int _index = 0;
  ChildDrugResData? _selected;

  @override
  void initState() {
    super.initState();
    _requestDrugList();
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
      appBar: appBar("투약의뢰서".tr(), isClose: false, hide: false, onBack: () => Commons.pagePop(context)),
      body: Container(
        margin: padding_20,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          sb_h_20,
          lText("투약의뢰서_안내".tr()),
          sb_h_10,
          Flexible(
            child: lScrollView(
                padding: padding_0,
                child: Column(
                  children: data.childInfoItem.drugInfoItem.drugList.map((e) => _drugSaveListItem(e)).toList(),
                )),
          ),
          // add
          lBtnOutline('+ ${"투약의뢰하기".tr()}', sideColor: color_222222, style: st_b_16(fontWeight: FontWeight.w500), onClickAction: () async {
            if (!_enableLoader) {
              var list = await Commons.page(context, routeChildDrugAdd, arguments: ChildDrugAddPage(data: data, child: widget.child));
              if (list != null) {
                log.i({
                  '--- TITLE      ': '--------------- PAGE POPPED ---------------',
                  '--- VALUE      ': list.child.drugInfoItem.drugList.toString(),
                });
                data.childInfoItem.drugInfoItem.drugList.clear();
                data.childInfoItem.drugInfoItem.drugList.addAll(list.child.drugInfoItem.drugList);
              }
              onUpDate();
            }
          }),
          sb_h_20,

          /// duplicate
          lBtn("투약의뢰서불러오기".tr(),
              margin: padding_0,
              btnColor: _index != 0 ? color_545454 : color_eeeeee,
              style: st_16(
                  textColor: _enableLoader
                      ? Colors.white
                      : _enableLoader
                          ? color_cecece
                          : color_222222,
                  fontWeight: FontWeight.w500), onClickAction: () async {
            if (_enableLoader && _selected != null) {
              var list = await Commons.page(context, routeChildDrugAdd, arguments: ChildDrugAddPage(data: data, child: widget.child, drug: _selected));
              if (list != null) {
                log.i({
                  '--- TITLE      ': '--------------- PAGE POPPED ---------------',
                  '--- VALUE      ': list.child.drugInfoItem.drugList.toString(),
                });
                data.childInfoItem.drugInfoItem.drugList.clear();
                data.childInfoItem.drugInfoItem.drugList.addAll(list.child.drugInfoItem.drugList);
              }
            }
            if (widget.child != null && data.childInfoItem.drugInfoItem.drugList.isNotEmpty) _enableLoadMode();
            onUpDate();
          }),
        ]),
      ),
    );
  }

  Widget _drugSaveListItem(ChildDrugReponseData list) {
    return Column(
        children: list.druglists!
            .map((e) => lInkWell(
                onTap: () async {
                  var ret = await Commons.page(context, routeChildDrugView,
                      arguments: ChildDrugViewPage(
                          data: data, drugInfo: ChildDrugReponseData(id: list.id, child: list.child, child_name: list.child_name, drug_date: list.drug_date, user_signature: list.user_signature, createdate: list.createdate, druglists: [e])));
                  if (ret != null && ret == KEY_SUCCESS) {
                    _requestDrugList();
                  }
                },
                child: Column(children: [
                  Container(
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(children: [
                            if (_enableLoader)
                              IconButton(
                                visualDensity: VisualDensity.compact,
                                onPressed: () {
                                  _index = e.drug_id;
                                  _selected = e;
                                  onUpDate();
                                },
                                icon: Lcons.radio(isEnabled: _index == e.drug_id, disableColor: color_eeeeee),
                              ),
                            Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
                              Row(children: [
                                lText(list.drug_date, style: st_b_14(fontWeight: FontWeight.w500)),
                                sb_w_10,
                                lText('(${StringUtils.parseAHMM(e.drug_time)})', style: st_b_14(fontWeight: FontWeight.w500)),
                              ]),
                              lText(widget.child!.itemData.child_name, style: st_b_16(fontWeight: FontWeight.w500)),
                            ]),
                          ]),
                          lBtnWrapCustom(e.is_drug ? "투약완료".tr() : "투약예정".tr(),
                              borderRadius: 8, padding: padding_25_LR, btnDisableColor: color_545454, isEnabled: e.is_drug, btnEnableColor: color_cecece, textDisableColor: Colors.white, textEnableColor: Colors.white)
                        ],
                      )),
                  sb_h_10,
                  Divider(color: color_dedede, thickness: 1),
                  sb_h_10,
                ])))
            .toList());
  }

  void _requestDrugList() async {
    try {
      flag.enableLoading(fn: () => onUpDate());
      ChildDrugListRequest datas = ChildDrugListRequest(child: widget.child!.itemData.id);
      apiHelper.requestDrugList(datas).then((response) {
        if (data.childInfoItem.drugInfoItem.drugList.isNotEmpty) data.childInfoItem.drugInfoItem.drugList.clear();
        data.childInfoItem.drugInfoItem.drugList.addAll(response.dataList!);
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      log.i({
        '--- TITLE        ': '--------------- CALL EXCEPTION ---------------',
        '--- EXCEPTION ': e,
      });
    }
  }

  void _enableLoadMode() {
    _enableLoader = !_enableLoader;
    _index = 0;
  }
}
