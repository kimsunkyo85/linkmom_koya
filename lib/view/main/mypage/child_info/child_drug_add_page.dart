// Copyright 2021 linkmom
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/child_drug_request.dart';
import 'package:linkmom/data/network/models/child_drug_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/picker/date_picker.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/calendar/calendar_selecter.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:signature/signature.dart';

import 'child_drug_list_page.dart';

class ChildDrugAddPage extends BaseStateful {
  final DataManager? data;
  ChildInfoItem? child;
  ChildDrugResData? drug;

  ChildDrugAddPage({this.data, this.child, this.drug});

  @override
  _ChildDrugAddPage createState() => _ChildDrugAddPage();
}

class _ChildDrugAddPage extends BaseStatefulState<ChildDrugAddPage> {
  late SignatureController _controller;
  late Uint8List _signature;
  List<DateTime> _times = [];
  List<DrugModel> _drugList = [];

  String name = '';

  @override
  void initState() {
    super.initState();
    onData(widget.data ?? DataManager());
    _controller = SignatureController(
      penStrokeWidth: 2,
      penColor: Colors.black,
      exportBackgroundColor: Colors.transparent,
    );
    _times = [];
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      DrugModel model = DrugModel(
        typeData: {},
      );
      name = widget.child!.itemData.child_name;
      if (widget.drug != null) {
        Map<DrugType, String> drugs = {};
        if (widget.drug!.drug_type != null) {
          widget.drug!.drug_type!.forEach((e) {
            switch (e) {
              case 0:
                drugs.putIfAbsent(EnumUtils.getDrugType(index: e), () => widget.drug!.drug_amount_type0);
                break;
              case 1:
                drugs.putIfAbsent(EnumUtils.getDrugType(index: e), () => widget.drug!.drug_amount_type1);
                break;
              case 2:
                drugs.putIfAbsent(EnumUtils.getDrugType(index: e), () => widget.drug!.drug_amount_type2);
                break;
              case 3:
                drugs.putIfAbsent(EnumUtils.getDrugType(index: e), () => widget.drug!.drug_amount_type3);
                break;
              default:
            }
          });
        }
        model = DrugModel(
          typeData: drugs,
          count: int.tryParse(widget.drug!.drug_repeat) ?? 0,
          symptom: widget.drug!.symptom,
          detail: widget.drug!.drug_reqmessage,
          timeType: EnumUtils.getDrugTimeType(index: widget.drug!.drug_timetype),
        );
      }
      _drugList.add(model);
      onConfirmBtn();
    });
  }

  @override
  void onData(DataManager data) {
    super.onData(data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
          context: context,
          appBar: appBar("투약의뢰서".tr(), hide: false, onBack: () => Commons.pagePop(context)),
          body: Container(
              child: Column(children: [
            Expanded(
                child: lScrollView(
                    padding: padding_20_TB,
                    child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                      Container(
                          padding: padding_20,
                          child: Column(children: [
                            Row(children: [lText("투약의뢰서_아이이름".tr(), style: st_b_16(fontWeight: FontWeight.w700)), sb_w_20, lText(name, style: st_b_16(fontWeight: FontWeight.w500))]),
                            sb_h_10,
                            Row(children: [
                              Flexible(child: _titleBuilder("투약의뢰서_투약날짜".tr(), require: true)),
                              sb_w_10,
                              _dateSelecter("투약의뢰서_투약시작힌트".tr()),
                            ]),
                          ])),
                      lDivider(thickness: 8.0, color: color_61e5e5ea),
                      Container(
                          child: Column(
                        children: _drugList
                            .map((e) => Container(
                                  padding: padding_20,
                                  child: DrugWriteView(
                                      model: e,
                                      showDelete: _drugList.length > 1,
                                      onDelete: (model) {
                                        _drugList.remove(model);
                                        onConfirmBtn();
                                      },
                                      onUpdate: onConfirmBtn),
                                  decoration: BoxDecoration(border: Border(bottom: BorderSide(width: 8, color: color_61e5e5ea))),
                                ))
                            .toList(),
                      )),
                      lInkWell(
                          onTap: () {
                            int index = _drugList.length;
                            if (index > 2) {
                              showNormalDlg(message: "투약의뢰서_최대갯수".tr());
                            } else {
                              _drugList.add(DrugModel(
                                typeData: {},
                              ));
                            }
                            onConfirmBtn();
                          },
                          child: Container(
                            alignment: Alignment.center,
                            height: data.height(context, 0.15),
                            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                              Lcons.add(isEnabled: _drugList.length > 2, color: color_dedede, disableColor: color_222222),
                              lText("투약의뢰서_약추가하기".tr(), style: st_b_16(textColor: _drugList.length > 2 ? color_dedede : color_222222)),
                            ]),
                          )),
                      lDivider(thickness: 8.0, color: color_61e5e5ea),
                      Container(
                          padding: padding_20,
                          child: Column(children: [
                            Container(
                              width: double.infinity,
                              padding: EdgeInsets.fromLTRB(20, 28, 20, 28),
                              color: color_eeeeee,
                              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                lText("투약의뢰서_고지내용1".tr(), style: st_b_14()),
                                lText("투약의뢰서_고지내용2".tr(), style: st_b_14()),
                                lText("투약의뢰서_고지내용3".tr(), style: st_b_14()),
                              ]),
                            ),
                            Container(
                              margin: padding_15_T,
                              child: Column(children: [
                                lText("투약의뢰서_서명내용".tr(), style: st_b_16()),
                                sb_h_10,
                                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                                  lText(DateFormat.yMMMd().format(DateTime.now()), style: st_b_16()),
                                  sb_w_10,
                                  lText(auth.user.my_info_data!.first_name, style: st_16(textColor: color_main, fontWeight: FontWeight.w700)),
                                ])
                              ]),
                            )
                          ])),
                      lBtn(
                        "투약의뢰서_서명하기".tr(),
                        btnColor: color_545454,
                        padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
                        borderRadius: 6,
                        style: st_14(textColor: Colors.white, fontWeight: FontWeight.w400),
                        isEnabled: flag.isConfirm,
                        onClickAction: () {
                          showDlgSign(context, btnLeft: "서명하기_다시서명하기".tr(), btnRight: "투약의뢰하기".tr(), onClickAction: (action, imageFile, signature) {
                            _signature = signature;
                            if (flag.isConfirm) {
                              _saveDrugRequest();
                            } else {
                              showNormalDlg(message: "누락된정보확인".tr());
                            }
                            onUpDate();
                          });
                        },
                      ),
                    ]))),
          ]))),
    );
  }

  Widget _dateSelecter(String hint) {
    _times.sort();
    List<String> buffer = [];
    bool added = false;
    _times.forEach((element) {
      if (_times.first.month < element.month) {
        buffer.add(!added ? DateFormat.MMMd().format(element) : DateFormat.d().format(element));
        added = true;
      } else {
        buffer.add(DateFormat.d().format(element));
      }
    });
    return Container(
      width: data.width(context, 0.67),
      child: StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
        return lInkWell(
            child: lRoundContainer(
                bgColor: Colors.white,
                borderRadius: 8.0,
                borderWidth: 1,
                borderColor: color_dedede,
                child: Container(
                  child: Row(mainAxisSize: MainAxisSize.max, mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    lText(
                      _times.isNotEmpty
                          ? _times.length > 1
                              ? '${StringUtils.formatDotYMD(_times.first)} ~ ${StringUtils.formatDotYMD(_times.last)}'
                              : '${StringUtils.formatDotYMD(_times.last)}'
                          : '$hint',
                      style: st_14(textColor: _times.isEmpty ? color_cecece : color_222222),
                    ),
                    Lcons.calendar(),
                  ]),
                )),
            onTap: () async {
              _times = [];
              showNormalDlg(
                  messageWidget: Container(
                      height: 320,
                      width: 273,
                      child: DateRangeSelecter(
                          color: [color_main.withAlpha(102), color_main.withAlpha(51)],
                          selectCallback: (date, events) {
                            setState(() {
                              if (_times.length > 2) {
                                _times.clear();
                                _times.add(date);
                              } else if (_times.isNotEmpty && _times.contains(date)) {
                                _times.remove(date);
                              } else {
                                _times.add(date);
                              }
                              _times.sort();
                            });
                          },
                          startDate: DateTime.now(),
                          duration: Duration(days: 4),
                          mode: Selecter.view,
                          endDate: _times.isEmpty ? DateTime.now().add(Duration(days: 4)) : _times.first.add(Duration(days: 4)),
                          onCalendarChanged: (start, end) => {})),
                  padding: padding_0,
                  title: "날짜선택".tr(),
                  onClickAction: (action) => onConfirmBtn());
            });
      }),
    );
  }

  int drugNameToInt(String type) {
    if (type == "투약의뢰서_가루약".tr()) {
      return DrugType.powder.index;
    } else if (type == "투약의뢰서_물약".tr()) {
      return DrugType.potion.index;
    } else if (type == "투약의뢰서_연고".tr()) {
      return DrugType.ointment.index;
    } else {
      return DrugType.other.index;
    }
  }

  @override
  void onConfirmBtn() {
    if (_drugList.where((e) => e.typeData.isNotEmpty && e.time != null).length == _drugList.length && _times.isNotEmpty) {
      flag.enableConfirm(fn: onUpDate);
    } else {
      flag.disableConfirm(fn: onUpDate);
    }
  }

  Future<void> _saveDrugRequest() async {
    try {
      flag.enableLoading(fn: () => onUpDate());
      List<DateTime> range = List.generate(_times.last.difference(_times.first).inDays + 1, (index) => _times.first.add(Duration(days: index)));
      ChildDrugSaveReqeust req = ChildDrugSaveReqeust(
          child: widget.child!.itemData.id,
          reqdata: ChildDrugData(
              drugdate: range.map((e) => StringUtils.formatYMD(e)).toList(),
              druglist: _drugList
                  .map((e) => ChildDrugInfoData(
                      symptom: e.symptom,
                      drug_time: StringUtils.formatHD(e.time!),
                      drug_timetype: e.timeType.index,
                      drug_type: e.typeData.keys.map((e) => e.index).toList(),
                      drug_repeat: e.count.toString(),
                      drug_amount_type0: e.typeData[DrugType.potion] ?? '',
                      drug_amount_type1: e.typeData[DrugType.powder] ?? '',
                      drug_amount_type2: e.typeData[DrugType.ointment] ?? '',
                      drug_amount_type3: e.typeData[DrugType.other] ?? '',
                      drug_reqmessage: e.detail,
                      drug_keeping: e.storage))
                  .toList()),
          user_signature: await Commons.bytesToImage(_signature, "${ChildDrugSaveReqeust.Key_user_signature}_${StringUtils.formatYMD(DateTime.now())}", "png"));
      apiHelper.requestSaveDrug(req).then((response) {
        flag.disableLoading(fn: () => onUpDate());
        if (response.getCode() == KEY_SUCCESS) {
          data.childInfoItem.drugInfoItem.drugList.addAll(response.dataList!);
          Commons.pagePop(context, data: ChildDrugListPage(child: data.childInfoItem));
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      log.i({
        '--- TITLE      ': '--------------- CALL EXCEPTION ---------------',
        '--- EXCEPTION ': e,
      });
    }
  }

  Widget _titleBuilder(String title, {require = false}) {
    return RichText(
        text: TextSpan(children: [
      TextSpan(text: title, style: stAgrTitle),
      require ? TextSpan(text: " *", style: tvBgHighlight) : TextSpan(),
    ]));
  }
}

class DrugModel {
  String symptom;
  int count;
  int storage;
  String detail;
  Map<DrugType, String?> typeData;
  DrugTimeType timeType;
  DateTime? time;
  DrugModel({
    this.symptom = '',
    this.count = 0,
    this.storage = 0,
    this.detail = '',
    required this.typeData,
    this.timeType = DrugTimeType.after,
    this.time,
  });

  set setType(DrugTimeType type) => this.timeType = type;

  set setSymptom(String symptom) => this.symptom = symptom;
  set setCount(int count) => this.count;
  set setStorage(int strg) => this.storage;
  set setDetail(String detail) => this.detail;
  set setTypeData(Map<DrugType, String?> type) => this.typeData;
  set setTime(DateTime time) => this.time = time;
}

class DrugWriteView extends StatefulWidget {
  final DrugModel model;
  final Function onDelete;
  final Function onUpdate;
  final bool showDelete;
  DrugWriteView({
    this.showDelete = false,
    required this.model,
    required this.onDelete,
    required this.onUpdate,
  });

  @override
  State<DrugWriteView> createState() => _DrugWriteViewState();
}

class _DrugWriteViewState extends State<DrugWriteView> {
  OutlineInputBorder _defaultBorder = OutlineInputBorder(borderSide: BorderSide(color: color_dedede), borderRadius: BorderRadius.circular(8));
  List<TextEditingController> _drugCtrls = [];
  late TextEditingController _symptomCtrl;
  late TextEditingController _countCtrl;
  late TextEditingController _detailCtrl;

  @override
  void initState() {
    super.initState();
    _drugCtrls = List.generate(DrugType.values.length, (index) => TextEditingController(text: widget.model.typeData[DrugType.values[index]]));
    _symptomCtrl = TextEditingController(text: widget.model.symptom);
    _countCtrl = TextEditingController(text: widget.model.count > 0 ? widget.model.count.toString() : null);
    _detailCtrl = TextEditingController(text: widget.model.detail);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          _titleBuilder("투약의뢰서_투약시간".tr(), require: true),
          if (widget.showDelete)
            lTextBtn("삭제".tr(),
                onClickEvent: () => widget.onDelete(widget.model),
                padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
                bg: color_c4c4c4.withOpacity(0.2),
                style: st_14(
                  textColor: color_b2b2b2,
                  fontWeight: FontWeight.w500,
                )),
        ]),
        Padding(
          padding: padding_10_TB,
          child: Row(
            children: [
              Row(
                  children: DrugTimeType.values
                      .map((e) => lInkWell(
                            onTap: () => setState(() {
                              widget.model.timeType = e;
                              widget.onUpdate();
                            }),
                            child: Row(children: [
                              Lcons.radio(isEnabled: widget.model.timeType == e),
                              sb_w_05,
                              lText(e.name, style: st_b_16()),
                              sb_w_10,
                            ]),
                          ))
                      .toList()),
            ],
          ),
        ),
        lInkWell(
          onTap: () {
            showSingleTimePicker(context, (hour, min) {
              setState(() {
                widget.model.time = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, hour, min);
                widget.onUpdate();
              });
            }, "투약의뢰서_투약시간".tr(), initTime: DateTime.now());
          },
          child: Container(
              width: double.infinity,
              height: 50,
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: color_dedede,
                  ),
                  borderRadius: BorderRadius.circular(8.0)),
              padding: padding_10,
              child: lText(widget.model.time != null ? StringUtils.formatAHM(widget.model.time!) : "투약시간을입력해주세요".tr(), style: widget.model.time != null ? st_b_14() : st_14(textColor: color_cecece))),
        ),
        sb_h_10,
        Container(
          margin: padding_10_TB,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _titleBuilder("투약의뢰서_증상".tr()),
              sb_h_10,
              TextFormField(
                controller: _symptomCtrl,
                maxLines: 1,
                textAlign: TextAlign.start,
                textInputAction: TextInputAction.next,
                onChanged: (value) {
                  setState(() {
                    widget.model.setSymptom = value;
                    widget.onUpdate();
                  });
                },
                onEditingComplete: () => focusNextTextField(context),
                style: st_b_14(),
                decoration: InputDecoration(
                  isDense: true,
                  hintStyle: st_14(textColor: color_cecece),
                  hintText: "투약의뢰서_증상힌트".tr(),
                  border: _defaultBorder,
                  enabledBorder: _defaultBorder,
                  focusedBorder: _defaultBorder,
                  contentPadding: padding_15,
                ),
              ),
            ],
          ),
        ),
        sb_h_10,
        _titleBuilder("투약의뢰서_약종류".tr(), require: true),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: DrugType.values.map((e) {
            if (e.index < DrugType.ointment.index) {
              return Container(
                  child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                Flexible(
                  child: lIconText(
                    isExpanded: false,
                    title: e.name,
                    textStyle: st_b_14(fontWeight: FontWeight.w500),
                    icon: widget.model.typeData.containsKey(e) ? Lcons.checkbox(isEnabled: true, color: color_545454, size: 25) : Lcons.checkbox(isEnabled: true, color: color_cecece, size: 25),
                    onClick: () {
                      _updateType(e);
                    },
                  ),
                ),
                sb_w_10,
                Flexible(
                  child: Container(
                    width: 90,
                    margin: EdgeInsets.fromLTRB(0, 10, 10, 10),
                    child: TextField(
                        controller: _drugCtrls[DrugType.values.indexOf(e)],
                        textAlign: TextAlign.center,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.number,
                        style: st_b_14(),
                        decoration: InputDecoration(
                          isDense: true,
                          hintStyle: st_14(textColor: color_cecece),
                          hintText: "투약예시".tr(),
                          border: _defaultBorder,
                          enabledBorder: _defaultBorder,
                          focusedBorder: _defaultBorder,
                          contentPadding: padding_15,
                        ),
                        onChanged: (value) {
                          setState(() {
                            widget.model.typeData[e] = value;
                            widget.onUpdate();
                          });
                        },
                        onEditingComplete: () => focusNextTextField(context)),
                  ),
                ),
                Flexible(flex: 2, child: lText(e.unit, style: st_b_14())),
              ]));
            } else {
              return Container(
                margin: padding_10_TB,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    lIconText(
                      isExpanded: false,
                      title: e.name,
                      textStyle: st_b_14(fontWeight: FontWeight.w500),
                      icon: widget.model.typeData.containsKey(e) ? Lcons.checkbox(isEnabled: true, color: color_545454, size: 25) : Lcons.checkbox(isEnabled: true, color: color_cecece, size: 25),
                      onClick: () => _updateType(e),
                    ),
                    sb_h_10,
                    TextFormField(
                      maxLines: 1,
                      textAlign: TextAlign.start,
                      textInputAction: TextInputAction.next,
                      controller: _drugCtrls[DrugType.values.indexOf(e)],
                      onChanged: (value) {
                        setState(() {
                          widget.model.typeData[e] = value;
                          widget.onUpdate();
                        });
                      },
                      onEditingComplete: () => focusNextTextField(context),
                      style: st_b_14(),
                      decoration: InputDecoration(
                        isDense: true,
                        hintText: e.index == DrugType.ointment.index ? "투약의뢰서_연고안내".tr() : "투약의뢰서_기타안내".tr(),
                        hintStyle: st_14(textColor: color_cecece),
                        border: _defaultBorder,
                        enabledBorder: _defaultBorder,
                        focusedBorder: _defaultBorder,
                        contentPadding: padding_15,
                      ),
                    ),
                  ],
                ),
              );
            }
          }).toList(),
        ),
        Container(
            child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          Flexible(child: _titleBuilder("투약의뢰서_투약횟수".tr())),
          sb_w_10,
          Container(
            width: 90,
            margin: EdgeInsets.fromLTRB(0, 10, 10, 10),
            child: TextField(
                controller: _countCtrl,
                textAlign: TextAlign.center,
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.number,
                style: st_b_14(),
                decoration: InputDecoration(
                  isDense: true,
                  hintStyle: st_14(textColor: color_cecece),
                  hintText: "투약예시".tr(),
                  border: _defaultBorder,
                  enabledBorder: _defaultBorder,
                  focusedBorder: _defaultBorder,
                  contentPadding: padding_15,
                ),
                onChanged: (value) {
                  setState(() {
                    widget.model.count = int.tryParse(value) ?? 0;
                    widget.onUpdate();
                  });
                },
                onEditingComplete: () => focusNextTextField(context)),
          ),
          lText("회".tr(), style: st_b_14()),
        ])),
        Padding(
          padding: padding_10_TB,
          child: Row(children: [
            lText("투약의뢰서_보관방법".tr(), style: st_b_16(fontWeight: FontWeight.w700)),
            sb_w_10,
            Row(
              children: [
                lIconText(
                    title: "투약의뢰서_실온".tr(),
                    textStyle: st_b_16(),
                    isExpanded: false,
                    icon: Lcons.radio(isEnabled: widget.model.storage == 0),
                    onClick: () {
                      setState(() {
                        widget.model.storage = 0;
                        widget.onUpdate();
                      });
                    }),
                sb_w_10,
                lIconText(
                    title: "투약의뢰서_냉장".tr(),
                    textStyle: st_b_16(),
                    isExpanded: false,
                    icon: Lcons.radio(isEnabled: widget.model.storage == 1),
                    onClick: () {
                      setState(() {
                        widget.model.storage = 1;
                        widget.onUpdate();
                      });
                    }),
              ],
            )
          ]),
        ),
        Container(
            margin: padding_10_TB,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                lText("추가전달사항".tr(), style: stAgrTitle),
                sb_h_10,
                Container(
                  child: TextField(
                      maxLines: 3,
                      controller: _detailCtrl,
                      textInputAction: TextInputAction.done,
                      keyboardType: TextInputType.multiline,
                      style: st_b_14(),
                      decoration: InputDecoration(
                        contentPadding: padding_15,
                        hintText: "추가전달사항_힌트".tr(),
                        hintStyle: st_14(textColor: color_cecece),
                        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(6), borderSide: BorderSide(color: color_dedede, width: 1.0)),
                        counter: TextHighlight(text: '${widget.model.detail.isNotEmpty ? widget.model.detail.length : '0'}/1,000', term: '/1,000', textStyle: st_b_14(), textStyleHighlight: st_14(textColor: color_cecece)),
                        focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(6), borderSide: BorderSide(color: color_dedede, width: 1.0)),
                      ),
                      onChanged: (value) {
                        widget.model.detail = value;
                        widget.onUpdate();
                      },
                      onEditingComplete: () => focusClear(context)),
                ),
              ],
            )),
      ],
    );
  }

  void _updateType(DrugType type) {
    setState(() {
      if (widget.model.typeData.containsKey(type)) {
        widget.model.typeData.remove(type);
        _drugCtrls[type.index].clear();
      } else {
        widget.model.typeData.putIfAbsent(type, () => '');
      }
      widget.onUpdate();
    });
  }

  Widget _titleBuilder(String title, {require = false}) {
    return Padding(
      padding: padding_05_B,
      child: RichText(
          text: TextSpan(children: [
        TextSpan(text: title, style: stAgrTitle),
        require ? TextSpan(text: " *", style: tvBgHighlight) : TextSpan(),
      ])),
    );
  }
}
