// Copyright 2021 linkmom
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/child_drug_delete_request.dart';
import 'package:linkmom/data/network/models/child_drug_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../../../../main.dart';
import 'child_drug_view.dart';

class ChildDrugViewPage extends BaseStateful {
  final DataManager? data;
  final ChildDrugReponseData? drugInfo;

  ChildDrugViewPage({this.data, this.drugInfo});

  @override
  _ChildDrugAddPage createState() => _ChildDrugAddPage();
}

class _ChildDrugAddPage extends BaseStatefulState<ChildDrugViewPage> {
  @override
  void initState() {
    super.initState();
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager data) {
    super.onData(data);
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        appBar: appBar("투약의뢰서".tr(), hide: false, onBack: () => Commons.pagePop(context)),
        body: lScrollView(
          padding: padding_0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: lText(widget.drugInfo!.druglists!.where((element) => element.is_drug).isNotEmpty ? "투약완료".tr() : "투약예정".tr(), style: st_13(fontWeight: FontWeight.w500)),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.transparent),
                  color: widget.drugInfo!.druglists!.where((element) => element.is_drug).isNotEmpty ? color_cecece : color_545454,
                  borderRadius: BorderRadius.circular(8),
                ),
                padding: EdgeInsets.only(left: 25, right: 25, top: 10, bottom: 10),
                margin: EdgeInsets.fromLTRB(20, 20, 20, 10),
              ),
              ChildDrugView(widget.drugInfo!, auth.user.my_info_data!.first_name),
              widget.drugInfo!.druglists!.where((element) => element.is_drug).isEmpty
                  ? lBtnOutline("삭제".tr(), margin: padding_20_LRB, onClickAction: () {
                      showNormalDlg(
                        onClickAction: (action) {
                          switch (action) {
                            case DialogAction.yes:
                              _deleteRequest();
                              break;
                            default:
                          }
                        },
                        message: "투약의뢰서_삭제".tr(),
                        btnLeft: "취소".tr(),
                        btnRight: "삭제".tr(),
                      );
                    }, sideColor: color_545454, style: st_b_16(fontWeight: FontWeight.w700, textColor: color_545454))
                  : Container(),
            ],
          ),
        ));
  }

  void _deleteRequest() async {
    ChildDrugDeleteRequest req = ChildDrugDeleteRequest(child: widget.drugInfo!.child, drug_id: widget.drugInfo!.druglists!.first.drug_id);
    flag.enableLoading(fn: () => onUpDate());
    apiHelper.requestDrugDelete(req).then((response) {
      try {
        flag.disableLoading(fn: () => onUpDate());
        if (response.getCode() == KEY_SUCCESS) {
          Commons.pagePop(context, data: response.getCode());
        } else {
          showNormalDlg(message: response.getMsg());
        }
      } catch (e) {
        flag.disableLoading(fn: () => onUpDate());
      }
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }
}
