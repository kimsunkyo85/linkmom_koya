import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/child_info_request.dart';
import 'package:linkmom/data/network/models/child_info_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/board/type_selecter.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/child_info_view.dart';
import 'package:linkmom/view/main/job_apply/content_view.dart';
import 'package:linkmom/view/main/mypage/child_info/child_info_page.dart';
import 'package:linkmom/view/view_modules.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import '../../../../route_name.dart';

class ChildInfoAddPage extends BaseStateful {
  @override
  _ChildInfoPageState createState() => _ChildInfoPageState(child_id: child_id);

  //data 수정시 사용, 아이 등록시 새로운 데이터 적용
  ChildInfoAddPage({this.data, this.viewMode, this.mode, required this.child_id});

  final DataManager? data;
  final ViewMode? viewMode;
  final ChildListPageMode? mode;
  final int child_id;
}

class _ChildInfoPageState extends BaseStatefulState<ChildInfoAddPage> {
  _ChildInfoPageState({this.child_id = 0});

  final int child_id;
  PageController controller = PageController();

  String _birthValidMsg = '';

  FocusNode _birthYearFocus = FocusNode();
  FocusNode _birthMonthFocus = FocusNode();
  FocusNode _birthDayFocus = FocusNode();

  TextEditingController _birthYearCtrl = TextEditingController();
  TextEditingController _birthMonthCtrl = TextEditingController();
  TextEditingController _birthDayCtrl = TextEditingController();

  // List<String> _birthDate = List.filled(3, '00');

  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
    // context로부터 settings.arguments값을 추출하여 arg로 저장
    ///환경인증 가져오기 요청
    log.d('『GGUMBI』>>> initState : viewType: ${getViewType()}, \ngetItemType(): ${getItemType()}, \ndata: ${data.childInfoItem}, <<< ');

    _birthYearFocus.addListener(onUpDate);
    _birthMonthFocus.addListener(onUpDate);
    _birthDayFocus.addListener(onUpDate);
    if (getViewType() != ViewType.add) {
      _requestChildInfo();
    }
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    if (getViewType() == ViewType.modify) {
      ///수정일때는 STEP을 보여주지 않는다.
      data.jobItem.childInfo.isShow = false;
      data.childInfoItem.itemData = auth.childInfoItemData;
      data.jobItem.request = _data.jobItem.request.clone();
      data.jobItem.reqdata = _data.jobItem.reqdata.clone();
    }
    // onDataPage(data);
  }

  @override
  void dispose() {
    super.dispose();
    // clearTcData(onUpDate);
    data.childInfoItem.characterValue = 0;
    data.childInfoItem.itemData = ChildInfoData();
    data.childInfoItem.lsCharacterValue.clear();
    data.childInfoItem.requestBirth = '';
    data.childInfoItem.lsNursery.forEach((element) {
      element.isSelected = false;
    });
    data.childInfoItem.lsGender.forEach((element) {
      element.isSelected = false;
    });
    data.birth = '';
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
      key: data.scaffoldKey,
      context: context,
      resizeToAvoidBottomPadding: false,
      appBar: appBar(
        "아이정보".tr(),
        onClick: (a) {
          Commons.pagePop(context);
        },
        hide: false,
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: flag.isLoading
                ? LoadingIndicator(background: Colors.transparent)
                : Column(
                    children: [
                      Expanded(
                        child: lScrollView(
                          padding: padding_Item,
                          child: Container(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                if (getViewType() == ViewType.apply)
                                  Column(
                                    children: [
                                      childInfoView(data.jobItem.childInfo),
                                    ],
                                  ),
                                sb_h_20,
                                contentView(data: ContentData(content: "아이정보_안내".tr())),
                                if (getViewType() != ViewType.add)
                                  Padding(
                                    padding: padding_10_T,
                                    child: lText("아이정보수정_안내".tr(), color: Commons.getColor()),
                                  ),
                                sb_h_20,
                                _name(),
                                sb_h_10,
                                _gender(),
                                sb_h_30,
                                _birth(),
                                sb_h_10,
                                _nursery(),
                                sb_h_30,
                                _condition(),
                                sb_h_30,
                                _allergyView(),
                              ],
                            ),
                          ),
                        ),
                      ),
                      _confirmButton(),
                    ],
                  ),
          ),
        ],
      ),
    );
  }

  ///아이이름 입력 폼
  Widget _name() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      sb_h_10,
      _buildTitle("아이등록_아이이름".tr()),
      sb_h_10,
      NameField(
          lastField: true,
          focusColor: color_545454,
          hint: "아이등록_아이이름힌트".tr(),
          viewType: getViewType(),
          onEditingComplete: (state) {
            data.childInfoItem.request.child_name = data.tcName.text;
            onConfirmBtn();
          },
          onChanged: (valid) => data.childInfoItem.request.child_name = data.tcName.text,
          data: data),
    ]);
  }

  ///아이 성별
  Widget _gender() {
    List<SingleItem> _data = data.childInfoItem.lsGender;
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      _buildTitle("아이등록_아이성별".tr()),
      sb_h_15,
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: _data.map((e) {
          return GestureDetector(
            onTap: getViewType() == ViewType.view
                ? null
                : () => setState(() {
                      _data.forEach((element) {
                        element.isSelected = false;
                      });
                      e.isSelected = true;
                      onConfirmBtn();
                      focusClear(context);
                    }),
            child: Container(
                width: data.width(context, 0.4),
                alignment: Alignment.center,
                padding: padding_15_TB,
                decoration: BoxDecoration(
                    border: Border.all(
                      color: e.isSelected ? Commons.getColor() : color_dedede,
                      width: e.isSelected ? 2 : 1,
                    ),
                    borderRadius: BorderRadius.circular(26)),
                child: lText(e.name, style: st_16(textColor: e.isSelected ? Commons.getColor() : color_dedede, fontWeight: e.isSelected ? FontWeight.w500 : FontWeight.normal))),
          );
        }).toList(),
      ),
    ]);
  }

  ///생년월일
  Widget _birth() {
    return Form(
        key: data.formBirth,
        child: Container(
            child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildTitle("생년월일".tr()),
            sb_h_15,
            lInkWell(
                onTap: getViewType() == ViewType.view
                    ? null
                    : () {
                        if (!getReadOnlyState()) {
                          focusClear(context);
                          _showDatePicker();
                        }
                      },
                child: Row(mainAxisSize: MainAxisSize.max, children: [
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _birthYearCtrl,
                        focusNode: _birthYearFocus,
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.next,
                        textAlign: TextAlign.center,
                        style: st_b_16(isSelect: getReadOnlyState(), disableColor: color_dbdbdb, textColor: getViewType() == ViewType.view ? color_dedede : color_222222),
                        enabled: false,
                        maxLength: 4,
                        decoration: _underlineDeco("YYYY", _birthYearFocus),
                      )),
                  Container(margin: padding_10, child: Lcons.slash(color: color_dedede)),
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _birthMonthCtrl,
                        focusNode: _birthMonthFocus,
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.next,
                        textAlign: TextAlign.center,
                        style: st_b_16(isSelect: getReadOnlyState(), disableColor: color_999999, textColor: getViewType() == ViewType.view ? color_dedede : color_222222),
                        enabled: false,
                        maxLength: 2,
                        decoration: _underlineDeco("MM", _birthMonthFocus),
                      )),
                  Container(margin: padding_10, child: Lcons.slash(color: color_dedede)),
                  Flexible(
                      flex: 2,
                      child: TextFormField(
                        controller: _birthDayCtrl,
                        focusNode: _birthDayFocus,
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.done,
                        textAlign: TextAlign.center,
                        style: st_b_16(isSelect: getReadOnlyState(), disableColor: color_999999, textColor: getViewType() == ViewType.view ? color_dedede : color_222222),
                        enabled: false,
                        maxLength: 2,
                        decoration: _underlineDeco("DD", _birthDayFocus),
                      ))
                ])),
            sb_h_10,
            lText(_birthValidMsg, style: st_12(textColor: color_ff3b30, fontWeight: FontWeight.w400)),
          ],
        )));
  }

  ///보육기관
  Widget _nursery() {
    List<SingleItem> _data = data.childInfoItem.lsNursery;
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      _buildTitle("아이등록_아이가다니는보육기관".tr(), require: true),
      sb_h_10,
      Container(
          child: GridView.count(
        childAspectRatio: 2,
        crossAxisCount: 3,
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        children: _data.map((e) {
          return lInkWell(
              onTap: getViewType() == ViewType.view
                  ? null
                  : () {
                      _data.forEach((element) {
                        element.isSelected = false;
                      });
                      e.isSelected = !e.isSelected;
                      onConfirmBtn();
                      focusClear(context);
                    },
              child: Container(
                  margin: padding_05,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: e.isSelected
                          ? Commons.getColor()
                          : getViewType() == ViewType.view
                              ? color_dedede
                              : color_999999,
                      width: e.isSelected ? 2 : 1,
                    ),
                    borderRadius: BorderRadius.circular(26),
                  ),
                  child: Container(
                    margin: padding_10,
                    child: lText(e.name,
                        style: st_14(
                            textColor: e.isSelected
                                ? Commons.getColor()
                                : getViewType() == ViewType.view
                                    ? color_dedede
                                    : color_999999,
                            fontWeight: FontWeight.w500)),
                  )));
        }).toList(),
      )),
    ]);
  }

  Widget _confirmButton() {
    return getViewType() == ViewType.view
        ? Center()
        : lBtn("저장".tr(), isEnabled: flag.isConfirm, onClickAction: () {
            //수정일 경우에는 화면을 체인지 시켜 준다. 정보추가에서도 마찬가지로 세팅 할것!
            switch (getItemType()) {
              case ItemType.add:
                _requestChildInfoAddUpdate(HttpType.post, ApiEndPoint.EP_MYPAGE_CHILD_INFO_SAVE, data.childInfoItem.request);
                break;
              case ItemType.modify:
              case ItemType.apply:
                showNormalDlg(
                    message: "아이정보수정저장_안내".tr(),
                    btnLeft: "아니오".tr(),
                    btnRight: "예".tr(),
                    onClickAction: (action) {
                      switch (action) {
                        case DialogAction.no:
                          break;
                        case DialogAction.yes:
                          data.childInfoItem.request.child_id = child_id;
                          _requestChildInfoAddUpdate(HttpType.put, ApiEndPoint.EP_MYPAGE_CHILD_INFO_UPDATE, data.childInfoItem.request);
                          break;
                        default:
                      }
                    });
                break;
              default:
                Commons.page(context, routeChildInfo, arguments: ChildInfoPage(data: data));
            }
          }, btnColor: Commons.getColor());
  }

  @override
  void onConfirmBtn() {
    bool isName = data.tcName.text.isNotEmpty;
    if (isName) {
      data.childInfoItem.request.child_name = data.tcName.text;
    }

    bool isGender = false;
    data.childInfoItem.lsGender.asMap().forEach((key, value) {
      if (value.isSelected) {
        isGender = true;
        data.childInfoItem.request.child_gender = value.type;
      }
    });

    bool isBirth = false;
    if (StringUtils.validateString(data.birth)) {
      isBirth = true;
      data.childInfoItem.requestBirth = data.birth;
      data.childInfoItem.request.child_birth = data.childInfoItem.requestBirth;
    }

    bool isNursery = false;
    data.childInfoItem.lsNursery.asMap().forEach((index, value) {
      if (value.isSelected) {
        isNursery = true;
        data.childInfoItem.request.child_nursery = index;
      }
    });

    ///추가정보 정보 세팅 후 다음 페이지로 이동
    bool isChar = false;
    data.childInfoItem.request.child_character = [];
    data.childInfoItem.lsCharacterValue.forEach((value) {
      isChar = true;
      data.childInfoItem.request.child_character!.add(value);
    });

    // TODO: GGUMBI 5/20/21 - 아이정보 api 수정으로 주석처리
    /* data.childInfoItem.request.child_reqmessage = data.childInfoItem.itemData.child_reqmessage;
    data.childInfoItem.request.child_allergy = data.childInfoItem.itemData.child_allergy;*/

    //알러지 정보
    bool isAllergy = true;
    data.childInfoItem.request.is_allergy = data.childInfoItem.itemData.is_allergy;
    data.childInfoItem.request.allergy_name = data.tcAllergyName.text;
    data.childInfoItem.request.allergy_message = data.tcAllergyMessage.text;
    if (data.childInfoItem.request.is_allergy && !StringUtils.validateString(data.childInfoItem.request.allergy_name)) {
      isAllergy = false;
    }

    if (isName && isGender && isBirth && isChar && isNursery && isAllergy) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }

    log.d('『GGUMBI』>>> onConfirmBtn : _request: ${data.childInfoItem.request},  <<< ');
  }

  @override
  void onDataPage(_data) {
    if (_data == null || getItemType() == ItemType.add) {
      return;
    }

    ChildInfoData item = _data;
    log.d('『GGUMBI』>>> onDataPage : item: $item,  <<< ');
    data.childInfoItem.itemData = item;

    //이름
    data.tcName.text = item.child_name;

    //성별
    data.childInfoItem.lsGender.asMap().forEach((index, value) {
      if (value.type == item.child_gender) {
        value.isSelected = true;
      } else {
        value.isSelected = false;
      }
    });

    //생년월일
    data.childInfoItem.requestBirth = item.child_birth;
    List<String> birth = item.child_birth.split('-');
    if (birth.isNotEmpty) {
      _birthYearCtrl.text = birth.first;
      _birthMonthCtrl.text = birth[1];
      _birthDayCtrl.text = birth.last;
      data.birth = birth.join('-');
    }

    //보육기관
    data.childInfoItem.lsNursery.asMap().forEach((index, value) {
      if (index == item.child_nursery) {
        value.isSelected = true;
      } else {
        value.isSelected = false;
      }
    });

    data.childInfoItem.lsCharacterValue.clear();
    if (item.child_character != null) {
      item.child_character!.forEach((value) {
        data.childInfoItem.lsCharacter.forEach((e) {
          if (e.data == value) {
            e.selected = true;
          }
        });
        data.childInfoItem.lsCharacterValue.add(value);
      });
    }

    //알레르기
    data.tcAllergyName.text = item.allergy_name;
    data.tcAllergyMessage.text = item.allergy_message;

    onConfirmBtn();
  }

  Widget _buildTitle(String title, {bool require = true}) {
    return RichText(
        text: TextSpan(children: [
      TextSpan(text: title, style: st_b_18(fontWeight: FontWeight.w700)),
      TextSpan(text: require ? "  *" : " ", style: st_18(textColor: color_ff3b30)),
    ]));
  }

  Widget _condition() {
    return Column(
      children: [
        Row(children: [
          _buildTitle("아이등록_아이성향".tr()),
          sb_w_10,
          lText("아이등록_아이성향_조건".tr(), style: st_13(textColor: color_999999)),
        ]),
        sb_h_20,
        _smartSelecter("아이등록_아이성향_키워드".tr(), data.childInfoItem.lsCharacter, "아이등록_아이성향_키워드".tr()),
      ],
    );
  }

  Widget _smartSelecter(String title, List<SelecterModel> source, String subTitle) {
    return TypeSelecter(
      title: title,
      models: source,
      viewType: getViewType(),
      onSelectedData: (List<dynamic> datas) {
        log.d('『GGUMBI』>>> _smartSelecter : datas: $datas,  <<< ');
        data.childInfoItem.lsCharacterValue = datas.map((e) => int.parse(e.toString())).toList();
        onConfirmBtn();
      },
    );
  }

  Widget _allergyView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        lText("알레르기여부".tr(), style: st_b_18(fontWeight: FontWeight.w700)),
        sb_h_10,
        lInkWell(
          onTap: () {
            data.childInfoItem.itemData.is_allergy = !data.childInfoItem.itemData.is_allergy;
            onConfirmBtn();
          },
          child: Row(
            children: [
              // Lcons.check_circle(disableColor: color_dedede, color: Commons.getColor(), size: 25, isEnabled: data.payItem.isGroup),
              Lcons.checkbox(disableColor: color_dedede, color: Commons.getColor(), size: 25, isEnabled: data.childInfoItem.itemData.is_allergy),
              sb_w_02,
              lText("알레르기가있어요".tr(), style: st_b_14(fontWeight: FontWeight.bold)),
            ],
          ),
        ),
        sb_h_15,
        Form(
          key: data.formAllergyName,
          child: TextFormField(
            enabled: data.childInfoItem.itemData.is_allergy,
            textAlignVertical: TextAlignVertical.center,
            controller: data.tcAllergyName,
            maxLines: 2,
            maxLength: 30,
            style: st_b_16(),
            validator: StringUtils.validateAllergy,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.emailAddress,
            decoration: bgDecoration(
              "알레르기가_힌트".tr(),
              bgColor: color_white,
              textColor: color_b2b2b2,
              isLine: true,
              maxLength: 30,
              lineDisableColor: color_e0e0e0,
              lineErrorColor: data.childInfoItem.itemData.is_allergy ? color_b2b2b2 : color_e0e0e0,
            ),
            onChanged: (value) {
              data.formAllergyName.currentState!.validate();
              onConfirmBtn();
            },
            onEditingComplete: () {
              data.formAllergyName.currentState!.validate();
              focusClear(context);
              onConfirmBtn();
            },
          ),
        ),
        sb_h_15,
        Form(
          key: data.formAllergyMessage,
          child: TextFormField(
            enabled: data.childInfoItem.itemData.is_allergy,
            textAlignVertical: TextAlignVertical.center,
            controller: data.tcAllergyMessage,
            maxLines: 3,
            maxLength: 100,
            style: st_b_16(),
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.emailAddress,
            decoration: bgDecoration(
              "알레르기가_힌트2".tr(),
              bgColor: color_white,
              textColor: color_b2b2b2,
              isLine: true,
              maxLength: 100,
              lineDisableColor: color_e0e0e0,
            ),
            onChanged: (value) {
              data.formAllergyMessage.currentState!.validate();
              onConfirmBtn();
            },
            onEditingComplete: () {
              data.formAllergyMessage.currentState!.validate();
              focusClear(context);
              onConfirmBtn();
            },
          ),
        ),
      ],
    );
  }

  ///아이정보 가져오기 요청
  void _requestChildInfo() async {
    flag.enableLoading(fn: () => onUpDate());
    await apiHelper.requestMyPageChildInfo(ChildInfoRequest(child_id: child_id), HttpType.patch, ApiEndPoint.EP_MYPAGE_CHILD_INFO_LIST).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        onDataPage(response.getData());
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  ///아이정보 추가 및 업데이트
  void _requestChildInfoAddUpdate(HttpType type, String url, ChildInfoRequest _data) async {
    flag.enableLoading(fn: () => onUpDate());
    apiHelper.requestMyPageChildInfo(_data, type, url).then((response) {
      data.childInfoItem.response = response;
      data.childInfoItem.lsChildInfo.clear();
      if (response.getCode() == KEY_SUCCESS) {
        if (getViewType() == ViewType.modify) {
          data.childInfoItem.response.getDataList().forEach((value) {
            if (child_id == value.id) {
              data.childInfoItem.itemData = ChildInfoData.clone(value);
              auth.onUpdateChildInfo(1, value.child_name, value.child_age, value.child_nursery, value.child_gender, value.child_character!, data: data.childInfoItem.itemData);
            }
          });
        }

        Commons.pagePop(context, data: auth.childInfo);
        // switch (getItemType()) {
        //   case ItemType.add:
        //     data.childInfoItem.lsCharacterValue.clear();
        //     Commons.pageToMain(context, routeChildInfo);
        //     break;
        //   case ItemType.modify:
        //   case ItemType.apply:
        //     Commons.pagePop(context, data: auth.childInfo);
        //     break;
        //   default:
        // }
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  InputDecoration _underlineDeco(String hint, FocusNode focus) {
    return underLineDecoration(hint, counterText: '', hintStyle: st_16(textColor: color_999999), focus: focus.hasFocus, focusColor: color_545454);
  }

  bool getReadOnlyState() {
    return widget.viewMode!.itemType == ItemType.apply;
  }

  void _showDatePicker() {
    DatePicker.showDatePicker(
      context,
      theme: DatePickerTheme(backgroundColor: Colors.white, itemStyle: TextStyle(color: color_222222), doneStyle: TextStyle(color: color_545454, fontWeight: FontWeight.w700), titleHeight: 60.0, itemHeight: 40.0),
      locale: LocaleType.ko,
      minTime: DateTime.now().add(Duration(days: -(365 * 19))),
      maxTime: DateTime.now(),
      onConfirm: (time) {
        data.birth = DateFormat().addPattern('yyyy-MM-dd').format(time);
        _birthYearCtrl.text = time.year.toString();
        _birthMonthCtrl.text = StringUtils.getNumberAddZero(time.month);
        _birthDayCtrl.text = StringUtils.getNumberAddZero(time.day);
        onConfirmBtn();
      },
      currentTime: data.birth.isEmpty ? DateTime.now() : DateTime.parse(data.birth),
      onChanged: (time) {
        data.birth = DateFormat().addPattern('yyyy-MM-dd').format(time);
        _birthYearCtrl.text = time.year.toString();
        _birthMonthCtrl.text = StringUtils.getNumberAddZero(time.month);
        _birthDayCtrl.text = StringUtils.getNumberAddZero(time.day);
      },
    );
  }
}
