import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/child_drug_response.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

class ChildDrugView extends StatelessWidget {
  ChildDrugReponseData drugInfo;
  String name;

  ChildDrugView(this.drugInfo, this.name);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Container(
            alignment: Alignment.centerLeft,
            padding: padding_20,
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              _textCompnent("투약의뢰서_아이이름".tr(), drugInfo.child_name),
              _textCompnent("투약의뢰서_투약날짜".tr(), drugInfo.drug_date),
            ])),
        Container(child: Column(children: drugInfo.druglists!.map((e) => _drugRequestDetail(context, e)).toList())),
        Container(
          width: double.infinity,
          padding: EdgeInsets.fromLTRB(20, 28, 20, 28),
          margin: padding_20,
          color: color_eeeeee,
          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            lText("투약의뢰서_고지내용1".tr(), style: st_b_14()),
            lText("투약의뢰서_고지내용2".tr(), style: st_b_14()),
            lText("투약의뢰서_고지내용3".tr(), style: st_b_14()),
          ]),
        ),
        Container(
          margin: padding_15_T,
          child: Column(children: [
            lText("투약의뢰서_서명내용".tr(), style: st_b_16()),
            sb_h_10,
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  lText(StringUtils.formatYMDLocale(drugInfo.createdate), style: st_b_16()),
              sb_w_10,
              lText(name, style: st_b_16(fontWeight: FontWeight.w700)),
            ])
          ]),
        ),
        Container(
            margin: padding_20,
            alignment: Alignment.center,
            child: CachedNetworkImage(
              imageUrl: drugInfo.user_signature,
              imageBuilder: (context, imageProvider) => imageBuilder(imageProvider, fit: BoxFit.contain),
              placeholder: (context, url) => placeholder(),
              errorWidget: (context, url, error) => errorWidget(),
            )),
          ]),
      sb_h_20,
    ]));
  }

  Widget _textCompnent(String title, String value) {
    return Container(
        padding: padding_05_TB,
        child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
          Flexible(flex: 1, fit: FlexFit.tight, child: Container(child: lText(title, style: stAgrTitle))),
          Flexible(flex: 2, child: lText(value, style: st_b_15())),
        ]));
  }

  Widget _drugRequestDetail(BuildContext context, ChildDrugResData e) {
    return Container(
      child: Column(
        children: [
          lDivider(color: color_61e5e5ea, thickness: 8.0),
          Container(
              padding: padding_20,
              child: Column(children: [
                Container(
                    padding: padding_10_TB,
                    child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                      Flexible(flex: 1, fit: FlexFit.tight, child: Container(child: lText("투약의뢰서_투약시간".tr(), style: stAgrTitle))),
                      Flexible(flex: 1, child: lText(DrugTimeType.values[e.drug_timetype].name, style: st_b_15(fontWeight: FontWeight.w500))),
                      sb_w_05,
                      Flexible(flex: 1, child: lText(StringUtils.parseAHMM(e.drug_time), style: st_b_15())),
                    ])),
                _textCompnent("투약의뢰서_증상".tr(), e.symptom),
                _drugTypeComponent(context, e.drug_type!, [e.drug_amount_type0, e.drug_amount_type1, e.drug_amount_type2, e.drug_amount_type3]),
                _textCompnent("투약의뢰서_투약횟수".tr(), e.drug_repeat),
                _textCompnent("투약의뢰서_보관방법".tr(), e.drug_keeping == 0 ? "투약의뢰서_실온".tr() : "투약의뢰서_냉장".tr()),
                Container(
                    padding: padding_10_TB,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        lText("추가전달사항".tr(), style: stAgrTitle),
                        sb_h_15,
                        Container(
                            width: double.infinity,
                            child: lText(e.drug_reqmessage, style: st_b_15()),
                            padding: padding_10,
                            decoration: BoxDecoration(border: Border.all(color: e.drug_reqmessage.isEmpty ? color_eeeeee : color_dedede), color: Colors.transparent, borderRadius: BorderRadius.circular(8))),
                      ],
                    ))
              ])),
          lDivider(color: color_61e5e5ea, thickness: 8.0),
        ],
      ),
    );
  }

  Widget _drugTypeComponent(BuildContext context, List<int> type, List<String> value) {
    type.sort();
    return Container(
      padding: padding_10_TB,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Flexible(flex: 1, fit: FlexFit.tight, child: lText("투약의뢰서_약종류".tr(), style: stAgrTitle)),
          Flexible(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: type.map((e) {
                  DrugType d = EnumUtils.getDrugType(index: e);
                  return Row(children: [
                    Lcons.checkbox(color: color_545454, isEnabled: true),
                    sb_w_05,
                    lText(d.name, style: st_b_15(fontWeight: FontWeight.w500)),
                    sb_w_05,
                    lText('${value[e]} ${d.unit}', style: st_b_15()),
                  ]);
                }).toList(),
              ))
        ],
      ),
    );
  }
}
