import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/child_info/child_info_page.dart';

class ChildMenuPage extends BaseStateful {
  ChildMenuPage({this.data});
  DataManager? data;
  @override
  _ChildMenuPageState createState() => _ChildMenuPageState();
}

class _ChildMenuPageState extends BaseStatefulState<ChildMenuPage> {
  @override
  Widget build(BuildContext context) {
    return lScaffold(
        resizeToAvoidBottomPadding: false,
        appBar: appBar("우리아이".tr(), isBack: true, onClick: (a) {
          Commons.pagePop(context, data: data);
        }, hide: false),
        body: Container(
          padding: padding_20,
          child: Column(
            children: [
              lInkWell(
                onTap: () {
                  ChildListPageMode mode = ChildListPageMode.childInfo;
                  Commons.page(context, routeChildInfo, arguments: ChildInfoPage(data: widget.data, mode: mode, viewMode: ViewMode(viewType: ViewType.view),));
                },
                child: Column(children: [
                  Container(
                    margin: padding_18_TB,
                    alignment: Alignment.centerLeft,
                    child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                      lText("아이등록".tr(), style: st_b_15(fontWeight: FontWeight.w500)),
                      Lcons.nav_right(color: color_999999, size: 15)
                    ])),
                  lDivider(color: color_eeeeee)
                ]),
              ),
              lInkWell(
                onTap: () {
                  ChildListPageMode mode = ChildListPageMode.drugInfo;
                  Commons.page(context, routeChildInfo, arguments: ChildInfoPage(data: widget.data, mode: mode, viewMode: ViewMode(viewType: ViewType.apply)));
                },
                child: Column(children: [
                  Container(
                    margin: padding_18_TB,
                    alignment: Alignment.centerLeft,
                    child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                      lText("투약의뢰서".tr(), style: st_b_15(fontWeight: FontWeight.w500)),
                      Lcons.nav_right(color: color_999999, size: 15)
                    ])),
                  lDivider(color: color_eeeeee)
                ]),
              ),
            ],
          )),
    );
  }
}