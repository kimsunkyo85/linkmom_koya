import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/penalty_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/penalty/penalty_history_page.dart';
import 'package:linkmom/view/main/mypage/penalty/penalty_remove_page.dart';

class PenaltyManagePage extends BaseStateful {
  final ViewType view;
  PenaltyManagePage({this.view = ViewType.normal});

  @override
  _PenaltyManagePageState createState() => _PenaltyManagePageState();
}

class _PenaltyManagePageState extends BaseStatefulState<PenaltyManagePage> {
  PenaltyData _penaltyInfo = PenaltyData(penaltyCount: 0);

  @override
  void initState() {
    super.initState();
    _requestGetPanelty();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: diaryAppbar(
              title: "벌점스티커".tr(),
              hide: false,
              rightW: lInkWell(
                onTap: () => Commons.page(
                  context,
                  routePenaltyHistory,
                  arguments: PenaltyHistoryPage(penalty: _penaltyInfo, index: PenaltyHistoryPage.rules),
                ),
                child: Container(padding: padding_20_LTR, child: lText("벌점규정".tr(), style: st_14(textColor: Commons.getColor()), fontWeight: FontWeight.w500)),
              ),
            ),
            body: Container(
              child: Column(
                children: [
                  Expanded(
                      child: lScrollView(
                          padding: padding_20,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              sb_h_20,
                              lText("내가받은벌점".tr(), style: st_b_18(fontWeight: FontWeight.w700)),
                              sb_h_20,
                              GridView.builder(
                                  shrinkWrap: true,
                                  padding: padding_10_TB,
                                  physics: NeverScrollableScrollPhysics(),
                                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 5,
                                    childAspectRatio: 1,
                                  ),
                                  itemCount: 15,
                                  itemBuilder: (context, index) {
                                    return Container(
                                        alignment: Alignment.center,
                                        margin: padding_05,
                                        decoration: BoxDecoration(
                                          color: index >= _penaltyInfo.penaltyCount ? Colors.transparent : color_fa784f,
                                            border: index >= _penaltyInfo.penaltyCount
                                                ? Border.all(color: color_eeeeee, width: 2)
                                                : Border.all(color: color_eeeeee, width: 0),
                                            borderRadius: BorderRadius.circular(40)),
                                        child: index < _penaltyInfo.penaltyCount
                                            ? Padding(
                                                padding: padding_10,
                                                child: Lcons.warning_sticker(size: 60),
                                              )
                                            : lText(_getDesc(index), style: st_b_12(fontWeight: FontWeight.w700)));
                                  }),
                              sb_h_30,
                              lDingbatText(
                                  dingbatSize: 2,
                                  dingbatColor: color_222222,
                                  child: TextHighlight(
                                    text: "스티커_5개".tr(),
                                    term: "스티커_5개_하이라이트".tr(),
                                    textStyle: st_b_14(),
                                    textStyleHighlight: st_b_14(fontWeight: FontWeight.w700),
                                  )),
                              lDingbatText(
                                  dingbatSize: 2,
                                  dingbatColor: color_222222,
                                  child: TextHighlight(
                                    text: "스티커_10개".tr(),
                                    term: "스티커_10개_하이라이트".tr(),
                                    textStyle: st_b_14(),
                                    textStyleHighlight: st_b_14(fontWeight: FontWeight.w700),
                                  )),
                              lDingbatText(
                                  dingbatSize: 2,
                                  dingbatColor: color_222222,
                                  child: TextHighlight(
                                    text: "스티커_15개".tr(),
                                    term: "스티커_15개_하이라이트".tr(),
                                    textStyle: st_b_14(),
                                    textStyleHighlight: st_b_14(fontWeight: FontWeight.w700),
                                  )),
                            ],
                          ))),

                    lBtnOutline(
                    "벌점스티커내역".tr(),
                    sideColor: Commons.getColor(),
                    onClickAction: () => Commons.page(context, routePenaltyHistory, arguments: PenaltyHistoryPage(penalty: _penaltyInfo)),
                    margin: padding_20_LR,
                    style: st_16(textColor: Commons.getColor(), fontWeight: FontWeight.w700),
                  ),
                  if (widget.view == ViewType.normal)
                    lBtn("벌점스티커없애기".tr(), isEnabled: _penaltyInfo.count != 0, onClickAction: () async {
                      var ret = await Commons.page(context, routePenaltyRemove, arguments: PenaltyRemovePage(penalty: _penaltyInfo));
                      if (ret != null) {
                        _requestGetPanelty();
                      }
                    })
                ],
              ),
            )));
  }

  String _getDesc(int index) {
    switch (index + 1) {
      case 5:
        return "후기블록".tr();
      case 10:
        return "사용중지".tr();
      case 15:
        return "회원탈퇴".tr();
      default:
        return "";
    }
  }

  void _requestGetPanelty() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestPenalty().then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _penaltyInfo = response.getData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e("EXCEPTION >>>>>>>>>>>>> $e");
    }
  }
}
