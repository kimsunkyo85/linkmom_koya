import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/penalty_remove_request.dart';
import 'package:linkmom/data/network/models/penalty_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/penalty/penalty_history_page.dart';

class PenaltyRemovePage extends BaseStateful {
  final PenaltyData? penalty;
  PenaltyRemovePage({this.penalty});

  @override
  _PenaltyRemovePageState createState() => _PenaltyRemovePageState();
}

class _PenaltyRemovePageState extends BaseStatefulState<PenaltyRemovePage> {
  int REVIEW = 81;
  int LIKE = 80;

  List<int> _selected = [];
  int _selectType = 0;
  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: diaryAppbar(
              title: "벌점스티커없애기".tr(),
              rightW: lInkWell(
                onTap: () => Commons.page(
                  context,
                  routePenaltyHistory,
                  arguments: PenaltyHistoryPage(penalty: widget.penalty, index: PenaltyHistoryPage.rules),
                ),
                child: Container(padding: padding_20_LTR, child: lText("벌점규정".tr(), style: st_14(textColor: Commons.getColor()), fontWeight: FontWeight.w500)),
              ),
            ),
            body: lScrollView(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Padding(padding: padding_20_T, child: lText("없애려는스티커를선택해주세요".tr(), style: st_b_15(fontWeight: FontWeight.w700))),
                  GridView.builder(
                      shrinkWrap: true,
                      padding: padding_20_TB,
                      physics: NeverScrollableScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 5,
                        childAspectRatio: 1,
                      ),
                      itemCount: widget.penalty!.penaltyCount,
                      itemBuilder: (context, index) {
                        return lInkWell(
                            onTap: () {
                              if (_selected.contains(index)) {
                                _selected.remove(index);
                              } else {
                                _selected.add(index);
                              }
                              onUpDate();
                            },
                            child: Container(
                                width: 60,
                                height: 60,
                                alignment: Alignment.center,
                                margin: padding_05,
                                decoration: BoxDecoration(color: color_fa784f, borderRadius: BorderRadius.circular(40)),
                                child: Stack(
                                  children: [
                                    Padding(
                                      padding: padding_10,
                                      child: Lcons.warning_sticker(size: 60),
                                    ),
                                    if (_selected.contains(index))
                                      Container(
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(color: color_222222.withOpacity(0.62), borderRadius: BorderRadius.circular(50)),
                                        child: Lcons.check(color: Colors.white, size: 35, isEnabled: true),
                                      ),
                                  ],
                                )));
                      }),
                  Padding(
                    padding: padding_10_TB,
                    child: lText("스티커_제거안내".tr(), style: st_b_15(fontWeight: FontWeight.w700)),
                  ),
                  lText("선택한항목이차감".tr(), style: st_b_14()),
                  sb_h_10,
                  Table(
                    border: TableBorder(
                      horizontalInside: BorderSide(color: color_eeeeee),
                      bottom: BorderSide(color: color_eeeeee),
                    ),
                    columnWidths: {
                      0: FixedColumnWidth(20),
                      1: FixedColumnWidth(80),
                      2: FlexColumnWidth(),
                      3: FixedColumnWidth(20),
                      4: FlexColumnWidth(),
                    },
                    defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                    children: <TableRow>[
                      TableRow(decoration: BoxDecoration(color: color_fafafa), children: [
                        sb_w_10,
                        sb_h_50,
                        Center(child: lText("차감전".tr(), style: st_b_13(fontWeight: FontWeight.w500))),
                        sb_w_10,
                        Center(child: lText("차감후".tr(), style: st_b_13(fontWeight: FontWeight.w500))),
                      ]),
                      TableRow(children: [
                        Center(child: lInkWell(onTap: () => {_selectType = LIKE, onUpDate()}, child: Lcons.radio_button(isEnabled: _selectType == LIKE, color: Commons.getColor()))),
                        Center(child: lText("좋아요".tr(), style: st_b_15(fontWeight: FontWeight.w500))),
                        Center(child: lIconText(title: '${widget.penalty!.likeCount}', icon: Lcons.heart(color: Commons.getColor(), isEnabled: true, size: 15), textStyle: st_b_15(), isExpanded: false, align: MainAxisAlignment.center)),
                        Center(child: Lcons.nav_right(color: color_dedede)),
                        Center(child: Container(child: _calcMessage(_selected.length, LIKE), height: 80)),
                      ]),
                      TableRow(children: [
                        Center(child: lInkWell(onTap: () => {_selectType = REVIEW, onUpDate()}, child: Lcons.radio_button(isEnabled: _selectType == REVIEW, color: Commons.getColor()))),
                        Center(child: lText("후기".tr(), style: st_b_15(fontWeight: FontWeight.w500))),
                        Center(
                            child: Column(children: [
                          Row(mainAxisAlignment: MainAxisAlignment.center, children: [lText('${"맘대디".tr()} ${widget.penalty!.momdadyReviewCount}${"개".tr()}', style: st_b_15())]),
                          sb_h_10,
                          Row(mainAxisAlignment: MainAxisAlignment.center, children: [lText('${"링크쌤".tr()} ${widget.penalty!.linkmomReviewCount}${"개".tr()}', style: st_b_15())])
                        ])),
                        Center(child: Lcons.nav_right(color: color_dedede)),
                        Center(child: Container(child: _calcMessage(_selected.length, REVIEW), height: 95)),
                      ]),
                    ],
                  ),
                  Container(
                      padding: padding_20_TB,
                      child: Column(children: [
                        lText("스티커_차감안내_1".tr(), style: st_b_14()),
                        lText("스티커_차감안내_2".tr(), style: st_b_14()),
                        lText("스티커_차감안내_3".tr(), style: st_b_14()),
                        lText("스티커_차감안내_4".tr(), style: st_b_14()),
                      ])),
                ]),
                lBtn("확인".tr(), margin: padding_0, isEnabled: _selected.isNotEmpty && _selectType != 0, onClickAction: () {
                  if (_selectType == LIKE) {
                    showNormalDlg(
                      message: "벌점스티커_좋아요차감확인".tr(),
                      onClickAction: (a) => {if (a == DialogAction.yes) _requestRemove()},
                      btnLeft: "아니오".tr(),
                      btnRight: "예".tr(),
                    );
                  } else {
                    _requestRemove();
                  }
                }),
              ],
            ))));
  }

  int _calcCount(int target, int total) {
    return total - (target * 5);
  }

  List<int> _count(int remover, int momdaddy, int linkmom) {
    if (remover > 0) {
      int count = remover * 5;
      int first = momdaddy - count;
      int second = 0;
      if (first < 0) {
        second = linkmom + first;
        first = 0;
      } else {
        second = linkmom;
      }
      return [first, second];
    } else {
      return [momdaddy, linkmom];
    }
  }

  Widget _calcMessage(int count, int type) {
    List<int> reviewCounts = [widget.penalty!.momdadyReviewCount, widget.penalty!.linkmomReviewCount];
    if (type == LIKE) {
      int value = _calcCount(count, widget.penalty!.likeCount);
      if (value.isNegative) {
        return _notAvailable();
      } else {
        return lIconText(title: '$value', icon: Lcons.heart(color: Commons.getColor(), isEnabled: true, size: 15), textStyle: st_b_15(fontWeight: FontWeight.w700), isExpanded: false, align: MainAxisAlignment.center);
      }
    } else {
      reviewCounts = _count(count, widget.penalty!.momdadyReviewCount, widget.penalty!.linkmomReviewCount);
      int value = reviewCounts.first + reviewCounts.last;
      if (value.isNegative) {
        return _notAvailable();
      } else {
        return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [lText('${"맘대디".tr()} ${reviewCounts.first}${"개".tr()}', style: st_b_15(fontWeight: FontWeight.w700))]),
          sb_h_10,
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [lText('${"링크쌤".tr()} ${reviewCounts.last}${"개".tr()}', style: st_b_15(fontWeight: FontWeight.w700))])
        ]);
      }
    }
  }

  void _requestRemove() {
    try {
      PenaltyRemoveRequest req = PenaltyRemoveRequest(count: _selected.length, penalty: _selectType);
      flag.enableLoading(fn: onUpDate);
      apiHelper.requestPenaltyRemove(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          flag.disableLoading(fn: onUpDate);
          Commons.pagePop(context, data: response.getData());
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>>>>>>>>>>>> $e");
    }
  }

  Widget _notAvailable() {
    return Container(alignment: Alignment.center, child: lText("차감불가".tr(), style: st_b_15(fontWeight: FontWeight.w700)));
  }
}
