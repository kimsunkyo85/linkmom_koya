import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cs_guide_request.dart';
import 'package:linkmom/data/network/models/cs_terms_view_response.dart';
import 'package:linkmom/data/network/models/penalty_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

class PenaltyHistoryPage extends BaseStateful {
  final PenaltyData? penalty;
  final int index;
  PenaltyHistoryPage({this.penalty, this.index = 0});

  static const int history = 0;
  static const int rules = 1;
  @override
  _PenaltyHistoryPageState createState() => _PenaltyHistoryPageState();
}

class _PenaltyHistoryPageState extends BaseStatefulState<PenaltyHistoryPage> with SingleTickerProviderStateMixin {
  TermsViewData _term = TermsViewData();
  PenaltyData _penaltyInfo = PenaltyData(penaltyCount: 0);
  late TabController _tabCtrl;

  @override
  void initState() {
    super.initState();
    _tabCtrl = TabController(length: 2, vsync: this);
    _penaltyInfo = widget.penalty!;
    _requestPenaltyTerm();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Future.delayed(Duration(seconds: 1)).then((value) => _tabCtrl.animateTo(widget.index));
    });
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          appBar: appBar("벌점스티커".tr()),
          body: Container(
              child: TabPageView(
            onCreateView: (ctrl) => _tabCtrl = ctrl,
            tabbarName: [TabItem(title: "벌점스티커내역".tr()), TabItem(title: "부과기준".tr())],
            tabbarView: [
              NextRequestListener(
                onNextRequest: () => _requestGetPanelty(next: true),
                child: LinkmomRefresh(
                    onRefresh: () => _requestGetPanelty(),
                    padding: padding_0,
                    child: _penaltyInfo.results != null
                        ? Column(children: _penaltyInfo.results!.map((e) => _historyUnit(e.createdate, e.penaltyText, e.count, e.answer)).toList())
                        : lEmptyView(height: data.height(context, 0.5))),
              ),
              LinkmomRefresh(
                  padding: padding_0,
                  child: Html(
                    data: _term.content,
                    customRenders: {
                      networkSourceMatcher(): networkImageRender(
                        loadingWidget: () => LoadingIndicator(
                          showText: false,
                          background: color_transparent,
                        ),
                      ),
                    },
                  ),
                  onRefresh: () => _requestPenaltyTerm())
            ],
          )),
        ));
  }

  Widget _historyUnit(String date, String name, int count, String answer) {
    return Column(
      children: [
        answer.isNotEmpty
            ? Container(
          child: Theme(
            data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
            child: ExpansionTile(
                tilePadding: padding_20,
                childrenPadding: padding_20_LRB,
                backgroundColor: color_eeeeee.withOpacity(0.32),
                iconColor: color_222222,
                collapsedIconColor: color_222222,
                title: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          lText(StringUtils.parseYYMMDD(date), style: st_12(textColor: color_999999)),
                          sb_h_06,
                          Container(
                            width: Commons.width(context, 0.6),
                            child: lText(name, overflow: TextOverflow.ellipsis, style: st_b_14(fontWeight: FontWeight.w500)),
                          ),
                        ],
                      ),
                      lText(count.toString(), style: st_16(fontWeight: FontWeight.w500, textColor: count < 0 ? color_ff3b30 : color_222222)),
                    ],
                  ),
                ),
                children: [
                  lDivider(padding: padding_20_B, color: color_eeeeee),
                  Container(
                    alignment: Alignment.topLeft,
                    child: lText(answer, style: st_b_14()),
                  ),
                ]),
          ))
            : Container(
                padding: EdgeInsets.fromLTRB(20, 20, 60, 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        lText(StringUtils.parseYYMMDD(date), style: st_12(textColor: color_999999)),
                        sb_h_06,
                        Container(
                          width: Commons.width(context, 0.6),
                          child: lText(name, overflow: TextOverflow.ellipsis, style: st_b_14(fontWeight: FontWeight.w500)),
                        ),
                      ],
                    ),
                    lText(count.toString(), style: st_16(fontWeight: FontWeight.w500, textColor: count < 0 ? color_ff3b30 : color_222222)),
                  ],
                ),
              ),
        lDivider(padding: padding_20_LR, color: color_eeeeee),
      ],
    );
  }

  void _requestPenaltyTerm() {
    try {
      CsGuideRequest req = CsGuideRequest(gubun: CsGuide.penalty.index);
      apiHelper.reqeustCsUserGuideView(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _term = response.getData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>> $e");
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestGetPanelty({bool next = false}) {
    try {
      if (next && _penaltyInfo.next.isEmpty) return;
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestPenalty(url: next ? _penaltyInfo.next : null).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          if (next) {
            _penaltyInfo.results!.addAll(response.getData().results!);
            _penaltyInfo.next = response.getData().next;
            _penaltyInfo.previous = response.getData().previous;
          } else {
            _penaltyInfo = response.getData();
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e("EXCEPTION >>>>>>>>>>>>> $e");
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}

class PenaltyRules {
  final String reason;
  final String sticker;
  final bool isHeader;
  PenaltyRules({required this.reason, required this.sticker, this.isHeader = false});
}
