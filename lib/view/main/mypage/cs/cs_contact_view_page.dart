import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cs_contact_delete_request.dart';
import 'package:linkmom/data/network/models/cs_contact_list_response.dart';
import 'package:linkmom/data/network/models/cs_contact_view_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/board/board_view.dart';

class CsContactViewPage extends BaseStateful {
  final int id;
  CsContactViewPage({this.id = 0});

  @override
  _CsContactViewPageState createState() => _CsContactViewPageState();
}

class _CsContactViewPageState extends BaseStatefulState<CsContactViewPage> {
  ContactData _contactData = ContactData(contactusImages: []);
  @override
  void initState() {
    super.initState();
    _requestContact(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar("문의하기".tr(), hide: false),
            body: RefreshView(
              padding: padding_0,
              onRefresh: () => {},
              onNext: () => {},
              child: lScrollView(
                padding: padding_0,
                child: BoardReadView(
                  padding: padding_0,
                  title: Container(
                    margin: padding_20,
                    padding: padding_20_B,
                    decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
                    child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          lText(_contactData.typetext, style: st_12(textColor: color_999999, fontWeight: FontWeight.w700)),
                          sb_h_05,
                          lText(_contactData.title, style: st_b_15(fontWeight: FontWeight.w500)),
                          sb_h_05,
                          lText(StringUtils.parseYYMMDD(_contactData.createdate), style: st_13(textColor: color_999999)),
                          sb_h_05,
                        ],
                      ),
                      lTextBtn(
                        _contactData.answer.isEmpty ? "미답변".tr() : "답변완료".tr(),
                        radius: 8,
                        style: _contactData.answer.isEmpty ? st_12(textColor: color_b2b2b2, fontWeight: FontWeight.w700) : st_b_12(fontWeight: FontWeight.w700),
                        bg: color_eeeeee,
                      ),
                    ]),
                  ),
                  body: BoardBody(
                    text: _contactData.content,
                    images: _contactData.contactusImages.map((e) => e.image).toList(),
                  ),
                  footer: Container(
                    child: Column(children: [
                      Container(
                        padding: padding_20,
                        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                          if (_contactData.answer.isEmpty)
                            lTextBtn("수정".tr(), style: st_13(textColor: color_999999, fontWeight: FontWeight.w700), bg: Colors.transparent, padding: padding_0, onClickEvent: () async {
                              Commons.pagePop(context, data: _contactData);
                            }),
                          if (_contactData.answer.isEmpty) Container(height: 10, child: lDividerVertical(padding: padding_10_LR, thickness: 1)),
                          lTextBtn(
                            "삭제".tr(),
                            style: st_13(textColor: color_999999, fontWeight: FontWeight.w700),
                            bg: Colors.transparent,
                            padding: padding_0,
                            onClickEvent: () => showNormalDlg(context: context, message: "삭제확인_타이틀".tr(), btnLeft: "취소".tr(), onClickAction: (action) => {if (action == DialogAction.yes) _requestDelete()}),
                          ),
                        ]),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(20, 30, 20, 60),
                        color: color_eeeeee.withOpacity(0.42),
                        child: Column(children: [
                          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                            lText(_contactData.answer.isEmpty ? "문의하기_관리자가".tr() : "관리자답변".tr(), style: st_b_15(fontWeight: FontWeight.w700)),
                            if (_contactData.answer.isNotEmpty) lText(StringUtils.parseYYMMDD(_contactData.answerdate), style: st_13(textColor: color_999999)),
                          ]),
                          sb_h_10,
                          _contactData.answer.isEmpty ? Container(alignment: Alignment.centerLeft, child: lText("문의하기_답변설명".tr(), style: st_14(textColor: color_545454))) : Html(data: _contactData.answer),
                        ]),
                      ),
                    ]),
                  ),
                  showReply: false,
                ),
              ),
            )));
  }

  void _requestDelete() {
    try {
      CsContactDeleteRequest req = CsContactDeleteRequest(id: _contactData.id);
      apiHelper.reqeustCsContactDelete(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.pagePop(context, data: false);
        }
      });
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
  }

  void _requestContact(int id) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      CsContactViewRequest req = CsContactViewRequest(id: id);
      apiHelper.reqeustCsContactView(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _contactData = response.getData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
  }
}
