import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cs_contact_list_response.dart';
import 'package:linkmom/data/network/models/cs_contact_save_request.dart';
import 'package:linkmom/data/network/models/cs_contact_update_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/board/board_view.dart';
import 'package:linkmom/view/lcons.dart';

class CsContactWritePage extends BaseStateful {
  final ViewType type;
  final ContactData contactData;
  final Function onSended;

  CsContactWritePage({this.type = ViewType.normal, required this.contactData, required this.onSended});

  @override
  _CsContactWritePageState createState() => _CsContactWritePageState();
}

class _CsContactWritePageState extends BaseStatefulState<CsContactWritePage> {
  List<File> _images = [];
  TextEditingController _titleCtrl = TextEditingController();
  TextEditingController _bodyCtrl = TextEditingController();
  ContactType? _selected;
  FocusNode _titleFocus = FocusNode();
  FocusNode _focus = FocusNode();
  FixedExtentScrollController _pickerCtrl = FixedExtentScrollController();
  bool isModify = false;

  @override
  void initState() {
    super.initState();
    _titleCtrl.text = widget.contactData.title;
    _bodyCtrl.text = widget.contactData.content;
    isModify = widget.contactData.id != 0 ? true : false;
    widget.contactData.contactusImages.forEach((e) async {
      File file = await Commons.networkToFile(context, e.image);
      _images.add(file);
      onUpDate();
    });
    if (widget.contactData.typetext.isNotEmpty) _selected = ContactType.values.where((e) => e.name == widget.contactData.typetext).first;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Future.delayed(Duration(seconds: 1)).then((value) => log.d('|| ${_images.length}'));
    });
    _titleFocus.addListener(() => onUpDate());
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          context: context,
          backgroundColor: color_fafafa,
          body: Column(children: [
            Expanded(
                child: lScrollView(
                    padding: padding_0,
                    child: Column(children: [
                      lInkWell(
                          onTap: () {
                            _selected = ContactType.care;
                            onUpDate();
                            showCupertinoModalPopup(
                                context: context,
                                builder: (BuildContext context) {
                                  return Container(
                                      color: Colors.white,
                                      height: data.height(context, 0.3),
                                      child: Column(children: [
                                        Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                                          CupertinoButton(
                                            child: lText("완료".tr(), style: st_15(fontWeight: FontWeight.w500, textColor: Colors.blue)),
                                            onPressed: () => Commons.pagePop(context),
                                          ),
                                        ]),
                                        Expanded(
                                            child: CupertinoPicker(
                                                scrollController: _pickerCtrl,
                                                useMagnifier: true,
                                                onSelectedItemChanged: (idx) {
                                                  _selected = ContactType.values[idx];
                                                  onUpDate();
                                                },
                                                itemExtent: 50,
                                                children: ContactType.values.map((e) => Container(alignment: Alignment.center, child: lText(e.name, style: st_18(textColor: color_545454, fontWeight: FontWeight.w500)))).toList()))
                                      ]));
                                });
                          },
                          child: Container(
                              color: Colors.white,
                              padding: padding_20,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [lText(_selected == null ? "문의유형을선택해주세요".tr() : _selected!.name, style: st_b_15(fontWeight: FontWeight.w500)), Lcons.nav_bottom(color: color_cecece)],
                              ))),
                      lDivider(thickness: 8.0, color: color_dedede.withAlpha(82)),
                      Container(
                        color: Colors.white,
                        child: Column(
                          children: [
                            Container(
                              padding: padding_20_LR,
                              child: TextField(
                                focusNode: _titleFocus,
                                controller: _titleCtrl,
                                style: st_b_15(fontWeight: FontWeight.w700),
                                textInputAction: TextInputAction.next,
                                onChanged: (value) => onUpDate(),
                                onSubmitted: (_) {
                                  FocusScope.of(context).requestFocus(_focus);
                                },
                                decoration: underLineDecoration(
                                  "제목을입력해주세요".tr(),
                                  focusColor: color_222222,
                                  icon: Lcons.clear(color: color_cecece),
                                  isFn: true,
                                  focus: _titleFocus.hasFocus,
                                  hintStyle: st_15(textColor: color_b2b2b2, fontWeight: FontWeight.w700),
                                  counterText: '',
                                  fn: () => _titleCtrl.clear(),
                                ),
                              ),
                            ),
                            BoardWriteView(
                              image: _images,
                              controller: _bodyCtrl,
                              update: () => onUpDate(),
                              hint: "문의할내용을입력해주세요".tr(),
                              addImageAction: (images) => _images = images,
                              focus: _focus,
                            ),
                          ],
                        ),
                      ),
                      Container(
                          width: double.infinity,
                          padding: padding_20,
                          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                            lText("문의사항_1".tr(), style: st_b_14()),
                            sb_h_20,
                            Row(children: [
                              lText("고객센터".tr(), style: st_b_14(fontWeight: FontWeight.w500)),
                              sb_w_10,
                              lText("고객센터전화번호".tr(), style: st_b_14()),
                            ]),
                            Row(children: [
                              lText("상담시간".tr(), style: st_b_14(fontWeight: FontWeight.w500)),
                              sb_w_10,
                              lText("고객센터운영시간".tr(), style: st_b_14()),
                            ]),
                            sb_h_10,
                            lText("문의사항_2".tr(), style: st_b_14()),
                            lBtn(isModify ? "수정하기".tr() : "등록하기".tr(), margin: padding_30_TB, isEnabled: _selected != null && _bodyCtrl.text.isNotEmpty && _titleCtrl.text.isNotEmpty, onClickAction: () => {_request()})
                          ])),
                    ])))
          ]),
        ));
  }

  void _requestSave() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      CsContactSaveRequest req = CsContactSaveRequest(title: _titleCtrl.text, content: _bodyCtrl.text, type: _selected!.index, image: _images);
      apiHelper.reqeustCsContactSave(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          showNormalDlg(context: context, message: "등록이완료되었습니다".tr(), onClickAction: (a) => {if (a == DialogAction.yes) _onSended()});
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () {
            onUpDate();
            _onSended();
          }));
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
  }

  void _requestUpdate(int id) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      CsContactUpdateRequest req = CsContactUpdateRequest(id: id, title: _titleCtrl.text, content: _bodyCtrl.text, type: _selected!.index, image: _images);
      apiHelper.reqeustCsContactUpdate(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          showNormalDlg(context: context, message: "수정이완료되었습니다".tr(), onClickAction: (a) => {if (a == DialogAction.yes) _onSended()});
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () {
            onUpDate();
            _onSended();
          }));
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
  }

  void _onSended() {
    _titleCtrl.clear();
    _bodyCtrl.clear();
    _selected = null;
    _images = [];
    widget.onSended();
  }

  void _request() {
    focusClear(context);
    showNormalDlg(
      context: context,
      message: isModify ? "수정하시겠습니까".tr() : "등록하시겠습니까".tr(),
      btnLeft: "취소".tr(),
      onClickAction: (a) => {if (a == DialogAction.yes) isModify ? _requestUpdate(widget.contactData.id) : _requestSave()},
    );
  }
}
