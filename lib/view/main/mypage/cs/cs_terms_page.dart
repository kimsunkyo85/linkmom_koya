import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cs_terms_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import 'cs_terms_view_page.dart';

class CsTermsPage extends BaseStateful {
  CsTermsPage();

  @override
  _CsTermsPageState createState() => _CsTermsPageState();
}

class _CsTermsPageState extends BaseStatefulState<CsTermsPage> {
  List<TermsData> _terms = [];

  @override
  void initState() {
    super.initState();
    _requestTerms();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar("이용약관".tr(), hide: false),
            body: _terms.isNotEmpty
                ? lScrollView(
                    child: Column(
                      children: _terms.map((e) => _menuComponent(e.title, movePage: () => Commons.page(context, routeCsTermsView, arguments: CsTermsViewPage(terms: EnumUtils.getTerms(e.id))))).toList(),
                    ),
                  )
                : lEmptyView()));
  }

  Widget _menuComponent(String title, {bool enable = true, Function? movePage}) {
    return lInkWell(
      onTap: () => {if (enable) movePage!()},
      child: Column(children: [
        Container(
            height: 50,
            alignment: Alignment.centerLeft,
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [lText(title, style: st_b_15(textColor: enable ? color_222222 : color_cecece, fontWeight: FontWeight.w500)), Lcons.nav_right(color: enable ? color_999999 : color_cecece, size: 15)])),
        lDivider(color: color_eeeeee)
      ]),
    );
  }

  void _requestTerms() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.reqeustCsTermsList().then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _terms = response.getData().results!;
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e("EXCEPTION >>>>>>>>>>>>>>>>>>> $e");
    }
  }
}
