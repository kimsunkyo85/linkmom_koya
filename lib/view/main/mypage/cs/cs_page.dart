import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

class CsPage extends BaseStateful {
  CsPage();

  @override
  _CsPageState createState() => _CsPageState();
}

class _CsPageState extends BaseStatefulState<CsPage> {
  @override
  Widget build(BuildContext context) {
    return lScaffold(
        appBar: appBar("고객센터".tr(), hide: false),
        body: lScrollView(
          padding: padding_0,
          child: Container(
            padding: padding_20,
            child: Column(
              children: [
                _menuComponent("공지사항".tr(), movePage: () => Commons.page(context, routeCsNotice)),
                _menuComponent("자주묻는질문".tr(), movePage: () => Commons.page(context, routeCsQna)),
                _menuComponent("돌봄이용수칙".tr(), movePage: () => Commons.page(context, routeCsGuide)),
                _menuComponent("돌봄서비스범위".tr(), movePage: () => Commons.page(context, routeCsService)),
                _menuComponent("문의하기".tr(), movePage: () => Commons.page(context, routeCsContact)),
                _menuComponent("이용약관".tr(), movePage: () => Commons.page(context, routeCsTerms)),
                sb_h_40,
                lTextBtn("채팅상담".tr(),
                    bg: color_f8e100,
                    padding: EdgeInsets.fromLTRB(60, 15, 60, 15),
                    leftIcon: Padding(
                      padding: padding_10_LR,
                      child: image(imagePath: KAKAO_CHAT, size: 30),
                    ),
                    style: st_15(
                      textColor: color_402522,
                      fontWeight: FontWeight.w500,
                    ),
                    onClickEvent: () => Commons.lLaunchUrl(ApiEndPoint.KAKAO_CHAT_URL)),
              ],
            ),
          ),
        ));
  }

  Widget _menuComponent(String title, {bool enable = true, Function? movePage}) {
    return lInkWell(
      onTap: () => {if (enable) movePage!()},
      child: Column(children: [
        Container(
            height: 50,
            alignment: Alignment.centerLeft,
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [lText(title, style: st_b_15(textColor: enable ? color_222222 : color_cecece, fontWeight: FontWeight.w500)), Lcons.nav_right(color: enable ? color_999999 : color_cecece, size: 15)])),
        lDivider(color: color_eeeeee)
      ]),
    );
  }
}
