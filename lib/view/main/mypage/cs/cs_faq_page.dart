import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/api_header.dart';
import 'package:linkmom/data/network/models/cs_faq_request.dart';
import 'package:linkmom/data/network/models/cs_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

class CsFaqPage extends BaseStateful {
  CsFaqPage();

  @override
  _CsFaqPageState createState() => _CsFaqPageState();
}

class _CsFaqPageState extends BaseStatefulState<CsFaqPage> {
  Map<USER_TYPE, List<NoticeListData>> _faqs = {USER_TYPE.mom_daddy: [], USER_TYPE.link_mom: []};
  List<TabItem> _tabbarName = [
    TabItem(title: "서비스이용".tr()),
    TabItem(title: "회원인증".tr()),
    TabItem(title: "결제환불".tr()),
    TabItem(title: "기타문의".tr()),
  ];

  double _height = 1000;

  @override
  void initState() {
    super.initState();
    _faqs[USER_TYPE.mom_daddy] = List.generate(_tabbarName.length, (i) => NoticeListData(results: []));
    _faqs[USER_TYPE.link_mom] = List.generate(_tabbarName.length, (i) => NoticeListData(results: []));
    _requestFaqList();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _height = Commons.height(context, 0.75);
    });
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          appBar: appBar("자주묻는질문".tr()),
          body: _faqs[USER_TYPE.mom_daddy]!.isNotEmpty
              ? TabPageView(
                  tabbarName: USER_TYPE.values.map((e) => TabItem(title: e.name)).toList(),
                  tabbarView: [
                    Container(
                        width: double.infinity,
                        height: _height,
                        child: TabPageView(
                          useOtherTabbar: true,
                          focusedTabbarStyle: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.w700),
                          tabbarName: _tabbarName,
                          tabbarView: _faqs[USER_TYPE.mom_daddy]!.map((e) => _buildFaq(e, _faqs[USER_TYPE.mom_daddy]!.indexOf(e), USER_TYPE.mom_daddy)).toList(),
                        )),
                    Container(
                        width: double.infinity,
                        height: _height,
                        child: TabPageView(
                          useOtherTabbar: true,
                          focusedTabbarStyle: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.w700),
                          tabbarName: _tabbarName,
                          tabbarView: _faqs[USER_TYPE.link_mom]!.map((e) => _buildFaq(e, _faqs[USER_TYPE.link_mom]!.indexOf(e), USER_TYPE.link_mom)).toList(),
                        )),
                  ],
                )
              : lEmptyView(height: data.height(context, 0.5)),
        ));
  }

  Future<void> _requestFaqList() async {
    try {
      flag.enableLoading(fn: () => onUpDate());
      _requestFaqWhile(USER_TYPE.mom_daddy, 0);
      _requestFaqWhile(USER_TYPE.link_mom, 0);
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  Future<void> _requestFaqWhile(USER_TYPE type, int index) async {
    if (index > _tabbarName.length - 1) return;
    CsFaqRequest req = CsFaqRequest(gubun: type.index + 1, type: index);
    var response = await apiHelper.reqeustCsFaq(req);
    if (response.getCode() == KEY_SUCCESS && response.dataList!.isNotEmpty) {
      _faqs[type]![index] = response.getData();
    }
    flag.disableLoading(fn: () => onUpDate());
    _requestFaqWhile(type, ++index);
  }

  Future<void> _requestNext(USER_TYPE user, int index) async {
    CsResponse responseData;
    try {
      if (_faqs[user]![index].next.isEmpty) return;
      CsFaqRequest req = CsFaqRequest(gubun: user.index + 1, type: index);
      var response = await apiHelper.apiCall(HttpType.patch, _faqs[user]![index].next, ApiHeader().publicApiHeader, req);
      var responseJson = Commons.returnResponse(response);
      responseData = CsResponse.fromJson(responseJson);
      _faqs[user]![index].results!.addAll(responseData.getData().results!);
      _faqs[user]![index].next = responseData.getData().next;
      _faqs[user]![index].previous = responseData.getData().previous;
      onUpDate();
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
  }

  Widget _buildFaq(NoticeListData value, int index, USER_TYPE type) {
    return value.count > 0
        ? NextRequestListener(
            onNextRequest: () => _requestNext(type, index),
            child: Container(
              color: Colors.white,
              child: value.results!.isNotEmpty
                  ? lScrollView(
                      padding: padding_0,
                      child: Column(
                        children: value.results!.map((e) => FaqTile(title: e.title, content: e.content)).toList(),
                      ),
                    )
                  : lEmptyView(
                      height: data.height(context, 0.4),
                    ),
            ),
          )
        : lEmptyView(height: data.height(context, 0.5));
  }
}

class FaqTile extends StatelessWidget {
  final String title;
  final String content;

  FaqTile({required this.title, required this.content});

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Theme(
          data: Theme.of(context).copyWith(dividerColor: Colors.transparent), // remove border
          child: ExpansionTile(
            backgroundColor: color_fafafa,
            collapsedIconColor: color_999999,
            iconColor: color_999999,
            childrenPadding: padding_10,
            title: lText(title, style: st_b_15(fontWeight: FontWeight.w500)),
            children: [
              Html(
                data: content,
                customRenders: {
                  networkSourceMatcher(): networkImageRender(
                    loadingWidget: () => LoadingIndicator(
                      showText: false,
                      background: color_transparent,
                    ),
                  ),
                },
              )
            ],
          )),
      lDivider(color: color_eeeeee, padding: padding_15_LR)
    ]);
  }
}
