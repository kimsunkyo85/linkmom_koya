import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/api_header.dart';
import 'package:linkmom/data/network/models/cs_response.dart';
import 'package:linkmom/data/network/models/cs_service_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

class CsServicePage extends BaseStateful {
  CsServicePage();

  @override
  _CsServicePageState createState() => _CsServicePageState();
}

class _CsServicePageState extends BaseStatefulState<CsServicePage> {
  Map<int, List<NoticeResult>> _service = {};
  String? _next;
  String? _previous;

  @override
  void initState() {
    super.initState();
    _requestService();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          appBar: appBar("돌봄서비스범위".tr()),
          body: TabPageView(
            tabbarName: [TabItem(title: "우리집에서".tr()), TabItem(title: "이웃집에서".tr())],
            tabbarView: [
              _service[1] != null
                  ? lScrollView(
                      child: RefreshIndicator(
                        child: Column(
                            children: _service[1]!
                                .map((e) => lInkWell(
                                      onTap: () => {},
                                      child: Column(
                                        children: [
                                          Html(
                                            data: e.content,
                                            customRenders: {
                                              networkSourceMatcher(): networkImageRender(
                                                loadingWidget: () => LoadingIndicator(
                                                  showText: false,
                                                  background: color_transparent,
                                                ),
                                              ),
                                            },
                                          ),
                                        ],
                                      ),
                                    ))
                                .toList()),
                        onRefresh: () => _requestNext(1),
                      ),
                    )
                  : lEmptyView(),
              _service[2] != null
                  ? lScrollView(
                      child: RefreshIndicator(
                      child: Column(
                          children: _service[2]!
                              .map((e) => lInkWell(
                                    onTap: () => {},
                                    child: Column(
                                      children: [
                                        Html(
                                          data: e.content,
                                          customRenders: {
                                            networkSourceMatcher(): networkImageRender(
                                              loadingWidget: () => LoadingIndicator(
                                                showText: false,
                                                background: color_transparent,
                                              ),
                                            ),
                                          },
                                        ),
                                      ],
                                    ),
                                  ))
                              .toList()),
                      onRefresh: () => _requestNext(2),
                    ))
                  : lEmptyView(),
            ],
          ),
        ));
  }

  Future<void> _requestService() async {
    try {
      flag.enableLoading(fn: () => onUpDate());
      for (var i = 1; i < 3; i++) {
        CsServiceRequest req = CsServiceRequest(place: i);
        await apiHelper.reqeustCsService(req).then((response) {
          if (response.getCode() == KEY_SUCCESS) {
            _service.putIfAbsent(i, () => response.getData().results!);
            _next = response.getData().next;
            _previous = response.getData().previous;
          }
        }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      }
      flag.disableLoading(fn: () => onUpDate());
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  Future<void> _requestNext(int type) async {
    CsResponse responseData;
    try {
      flag.enableLoading(fn: () => onUpDate());
      CsServiceRequest req = CsServiceRequest(place: type);
      var response = await apiHelper.apiCall(HttpType.patch, _next!, ApiHeader().publicApiHeader, req).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      var responseJson = Commons.returnResponse(response);
      responseData = CsResponse.fromJson(responseJson);
      _service[type]!.addAll(responseData.getData().results!);
      _next = responseData.getData().next;
      _previous = responseData.getData().previous;
      flag.disableLoading(fn: () => onUpDate());
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}
