import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/api_header.dart';
import 'package:linkmom/data/network/models/cs_contact_list_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/mypage/cs/cs_contact_write_page.dart';

import 'cs_contact_view_page.dart';

class CsContactPage extends BaseStateful {
  final int tabIndex;

  CsContactPage({this.tabIndex = 0});

  @override
  _CsContactPageState createState() => _CsContactPageState();
}

class _CsContactPageState extends BaseStatefulState<CsContactPage> with SingleTickerProviderStateMixin {
  ListData _list = ListData(results: []);
  TabController? _tabController;
  ContactData _contactData = ContactData(contactusImages: []);

  @override
  void initState() {
    super.initState();
    _requestContacts();
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
      context: context,
      appBar: appBar("문의하기".tr()),
      body: TabPageView(
        onCreateView: (ctrl) {
          _tabController = ctrl;
          if (_tabController != ctrl) _tabController!.animateTo(widget.tabIndex);
        },
        tabbarName: [TabItem(title: "문의하기".tr()), TabItem(title: "나의문의내역".tr())],
        tabbarView: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    child: CsContactWritePage(
                        contactData: _contactData,
                        onSended: () async {
                          _contactData = ContactData(contactusImages: []);
                          await _requestContacts();
                          Future.delayed(Duration(milliseconds: 100)).then((value) => _tabController!.animateTo(1));
                        })),
              ],
            ),
          ),
          ListNextView(
            header: Container(
              decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
              padding: padding_20_B,
              margin: padding_20,
              child: lText("${"총".tr()} ${_list.count}${"건".tr()}"),
            ),
            view: _list.results.map((e) => _buildContact(e)).toList(),
            onRefresh: () => _requestContacts(),
            onNext: () => _requestNext(),
            showNextLoading: _list.next.isNotEmpty,
          ),
        ],
      ),
    );
  }

  Future<void> _requestContacts() async {
    try {
      flag.enableLoading(fn: () => onUpDate());
      await apiHelper.reqeustCsContactList().then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _list = response.getData();
        } else {
          _list = ListData(results: []);
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
  }

  Future<void> _requestNext() async {
    CsContactListResponse responseData;
    try {
      if (_list.next.isEmpty) return;
      var response = await apiHelper.apiCall(HttpType.post, _list.next, ApiHeader().publicApiHeader, null).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      var responseJson = Commons.returnResponse(response);
      responseData = CsContactListResponse.fromJson(responseJson);
      _list.results.addAll(responseData.getData().results);
      _list.next = responseData.getData().next;
      _list.previous = responseData.getData().previous;
      flag.disableLoading(fn: () => onUpDate());
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
  }

  Widget _buildContact(ContactData contect) {
    return lInkWell(
      onTap: () async {
        var ret = await Commons.page(context, routeCsContactView, arguments: CsContactViewPage(id: contect.id));
        if (ret != null) {
          _tabController!.animateTo(0);
          _contactData = ret;
          onUpDate();
        }
        _requestContacts();
      },
      child: Container(
          decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
          padding: padding_20,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  lText(contect.typetext, style: st_12(textColor: color_999999, fontWeight: FontWeight.w700)),
                  sb_h_05,
                  lText(contect.title, style: st_b_15(fontWeight: FontWeight.w500)),
                  sb_h_05,
                  lText(StringUtils.parseYYMMDD(contect.createdate), style: st_13(textColor: color_999999)),
                ],
              ),
              lTextBtn(contect.answer.isEmpty ? "미답변".tr() : "답변완료".tr(), radius: 8, style: contect.answer.isEmpty ? st_12(textColor: color_b2b2b2, fontWeight: FontWeight.w700) : st_b_12(fontWeight: FontWeight.w700), bg: color_eeeeee),
            ],
          )),
    );
  }
}
