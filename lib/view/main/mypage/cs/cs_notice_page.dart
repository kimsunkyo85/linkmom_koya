import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cs_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/cs/cs_notice_view_page.dart';

class CsNoticePage extends BaseStateful {
  CsNoticePage();

  @override
  _CsNoticePageState createState() => _CsNoticePageState();
}

class _CsNoticePageState extends BaseStatefulState<CsNoticePage> {
  NoticeListData _notices = NoticeListData(results: []);

  @override
  void initState() {
    super.initState();
    _requestNoticeList();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          appBar: appBar("공지사항".tr(), hide: false),
          body: RefreshView(
            onRefresh: () => _requestNoticeList(),
            onNext: () => _requestNext(),
            child: _notices.results!.isNotEmpty
                ? lScrollView(
                    child: Container(
                      height: Commons.height(context, 0.9),
                      child: Column(
                        children: _notices.results!
                            .map((e) => lInkWell(
                                  onTap: () => Commons.page(context, routeCsNoticeView, arguments: CsNoticeViewPage(notice: e)),
                                  child: Container(
                                      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
                                      padding: padding_20_TB,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                            lText(e.title, style: st_b_15(fontWeight: FontWeight.w500)),
                                            lText(StringUtils.parseYYMMDD(e.createdate), style: st_12(textColor: color_999999)),
                                          ]),
                                          Lcons.nav_right(size: 15, color: color_999999)
                                        ],
                                      )),
                                ))
                            .toList(),
                      ),
                    ),
                )
                : lEmptyView(height: data.height(context, 0.5)),
          ),
        ));
  }

  void _requestNoticeList() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.reqeustCsNotice().then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _notices = response.getData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e('EXCEPTION >>>>>>>>>>>>>>>>>>>>> $e');
    }
  }

  Future<void> _requestNext() async {
    CsResponse responseData;
    try {
      flag.enableLoading(fn: () => onUpDate());
      var response = await apiHelper.apiCall(HttpType.patch, _notices.next, apiHeader.publicApiHeader, null).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      var responseJson = Commons.returnResponse(response);
      responseData = CsResponse.fromJson(responseJson);
      if (responseData.getCode() == KEY_SUCCESS) {
        _notices.results!.addAll(responseData.getData().results!);
      }
      flag.disableLoading(fn: () => onUpDate());
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
  }
}
