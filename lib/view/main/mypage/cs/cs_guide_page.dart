import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cs_guide_request.dart';
import 'package:linkmom/data/network/models/cs_terms_view_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

class CsGuidePage extends BaseStateful {
  CsGuidePage();

  @override
  _CsGuidePageState createState() => _CsGuidePageState();
}

class _CsGuidePageState extends BaseStatefulState<CsGuidePage> {
  Map<CsGuide, TermsViewData> _guide = {};

  @override
  void initState() {
    super.initState();
    _requestService();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          appBar: appBar("돌봄이용수칙".tr()),
          body: TabPageView(
            tabbarName: [TabItem(title: "맘대디이용수칙".tr()), TabItem(title: "링크쌤이용수칙".tr())],
            tabbarView: [
              _guide[CsGuide.momdady] != null
                  ? lScrollView(
                      child: Html(
                        data: _guide[CsGuide.momdady]!.content,
                        customRenders: {
                          networkSourceMatcher(): networkImageRender(
                            loadingWidget: () => LoadingIndicator(
                              showText: false,
                              background: color_transparent,
                            ),
                          ),
                        },
                      ),
                      padding: padding_0)
                  : lEmptyView(
                      height: Commons.height(context, 0.6),
                    ),
              _guide[CsGuide.linkmom] != null
                  ? lScrollView(
                      child: Html(
                        data: _guide[CsGuide.linkmom]!.content,
                        customRenders: {
                          networkSourceMatcher(): networkImageRender(
                            loadingWidget: () => LoadingIndicator(
                              showText: false,
                              background: color_transparent,
                            ),
                          ),
                        },
                      ),
                      padding: padding_0)
                  : lEmptyView(
                      height: Commons.height(context, 0.6),
                    ),
            ],
          ),
        ));
  }

  void _requestService({CsGuide? type}) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      if (type == null) {
        CsGuideRequest mReq = CsGuideRequest(gubun: CsGuide.momdady.index);
        apiHelper.reqeustCsUserGuideView(mReq).then((response) {
          if (response.getCode() == KEY_SUCCESS) {
            log.d('|| ' + response.getData().content);
            _guide[CsGuide.momdady] = response.getData();
          }
          flag.disableLoading(fn: () => onUpDate());
        }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
        CsGuideRequest lReq = CsGuideRequest(gubun: CsGuide.linkmom.index);
        apiHelper.reqeustCsUserGuideView(lReq).then((response) {
          if (response.getCode() == KEY_SUCCESS) {
            log.d('|| ' + response.getData().content);
            _guide[CsGuide.linkmom] = response.getData();
          }
            flag.disableLoading(fn: () => onUpDate());
        }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      } else {
        CsGuideRequest req = CsGuideRequest(gubun: type.index);
        apiHelper.reqeustCsUserGuideView(req).then((response) {
          if (response.getCode() == KEY_SUCCESS) {
            log.d('|| ' + response.getData().content);
            _guide[type] = response.getData();
          }
          flag.disableLoading(fn: () => onUpDate());
        }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      }
    } catch (e) {
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}
