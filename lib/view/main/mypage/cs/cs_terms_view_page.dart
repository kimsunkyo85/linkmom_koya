import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cs_terms_view_request.dart';
import 'package:linkmom/data/network/models/cs_terms_view_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:flutter_html_all/flutter_html_all.dart';

class CsTermsViewPage extends BaseStateful {
  final Terms terms;

  CsTermsViewPage({this.terms = Terms.privacy});

  @override
  _CsTermsViewPageState createState() => _CsTermsViewPageState();
}

class _CsTermsViewPageState extends BaseStatefulState<CsTermsViewPage> {
  TermsViewData? _terms;

  @override
  void initState() {
    super.initState();
    _requestTerms();
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
      appBar: appBar(_terms == null ? widget.terms.name : _terms!.title, hide: false),
      body: Builder(builder: (ctx) {
        try {
          return LinkmomRefresh(
            onRefresh: () => _requestTerms(),
            padding: padding_0,
            child: Html(
              style: {
                "td": Style(fontSize: FontSize(8)),
              },
              customRenders: {
                tagMatcher("table"): CustomRender.widget(
                  widget: (context, buildChildren) => Container(
                    child: tableRender.call().widget!.call(context, buildChildren),
                    padding: padding_10_R,
                  ),
                ),
              },
              data: _terms!.content,
              onCssParseError: (str, mes) => str,
              onImageError: (obj, stackTrace) => log.e('onImageError: ' + obj.toString()),
              shrinkWrap: true,
            ),
          );
        } catch (e) {
          return lEmptyView(title: "잠시만기다려주세요".tr());
        }
      }),
    );
  }

  void _requestTerms() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      CsTermsViewRequest req = CsTermsViewRequest(id: widget.terms.value);
      apiHelper.reqeustCsTermsView(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _terms = response.getData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e("EXCEPTION >>>>>>>>>>>>>>>>>>> $e");
    }
  }
}
