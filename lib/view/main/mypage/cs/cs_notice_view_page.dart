import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/cs_response.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

class CsNoticeViewPage extends BaseStateful {
  final NoticeResult? notice;
  CsNoticeViewPage({this.notice});

  @override
  _CsNoticePageViewState createState() => _CsNoticePageViewState();
}

class _CsNoticePageViewState extends BaseStatefulState<CsNoticeViewPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar("공지사항".tr(), hide: false),
            body: lScrollView(
                child: Container(
              width: double.infinity,
              child: Column(
                children: [
                  Container(
                      width: double.infinity,
                      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
                      padding: padding_20_TB,
                      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                        lText(widget.notice!.title, style: st_b_16(fontWeight: FontWeight.w500)),
                        lText(StringUtils.parseYYMMDD(widget.notice!.createdate), style: st_13(textColor: color_999999)),
                      ])),
                  sb_h_30,
                  Html(
                    data: widget.notice!.content,
                    customRenders: {
                      networkSourceMatcher(): networkImageRender(
                        loadingWidget: () => LoadingIndicator(
                          showText: false,
                          background: color_transparent,
                        ),
                      ),
                    },
                  )
                ],
              ),
            ))));
  }
}
