import 'package:dotted_border/dotted_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/withdrawal_list_request.dart';
import 'package:linkmom/data/network/models/withdrawal_list_response.dart';
import 'package:linkmom/data/network/models/withdrawal_request.dart';
import 'package:linkmom/data/network/models/withdrawal_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/compleat_view_page.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/content_view.dart';
import 'package:linkmom/view/main/mypage/care_diary/recipe_view.dart';
import 'package:linkmom/view/main/mypage/withdrawal/withdrawal_filter_page.dart';

class WithdrawalPage extends BaseStateful {
  WithdrawalPage();

  @override
  _WithdrawalPageState createState() => _WithdrawalPageState();
}

class _WithdrawalPageState extends BaseStatefulState<WithdrawalPage> {
  TextEditingController _payCtrl = TextEditingController();
  late DateTimeRange _range;
  FocusNode _focus = FocusNode();
  WithdrawalListData _list = WithdrawalListData(results: []);

  WithdrawalData get account => storageHelper.accountInfo;

  @override
  void initState() {
    super.initState();
    _range = DateTimeRange(start: Jiffy(DateTime.now()).startOf(Units.MONTH).dateTime, end: Jiffy(DateTime.now()).endOf(Units.MONTH).dateTime);
    _requestWithdrawalList();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      if (account.bankAccount.isEmpty) await storageHelper.getAccount();
      onUpDate();
    });
    _focus.addListener(() => onUpDate());
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            context: context,
            resizeToAvoidBottomPadding: false,
            resizeToAvoidBottomInset: false,
            appBar: appBar("캐시인출".tr()),
            body: TabPageView(tabbarName: [
              TabItem(title: "캐시인출".tr()),
              TabItem(title: "인출내역".tr()),
            ], tabbarView: [
              GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    focusClear(context);
                  },
                  child: Column(
                    children: [
                      Expanded(
                          child: lScrollView(
                        padding: padding_0,
                        child: Column(
                          children: [
                            Padding(
                              padding: padding_20,
                              child: Column(mainAxisSize: MainAxisSize.max, children: [
                                Padding(
                                  padding: padding_05_TB,
                                  child: TypeComponent(
                                    type: "인출가능캐시".tr(),
                                    typeStyle: st_b_16(fontWeight: FontWeight.w500),
                                    data: StringUtils.formatPay(auth.user.my_info_data!.mypageInfoCnt!.linkCash),
                                    dataStyle: st_b_16(fontWeight: FontWeight.w700),
                                    icon: Padding(padding: padding_05_L, child: Lcons.cash(isEnabled: true)),
                                    isExpanded: true,
                                  ),
                                ),
                                Padding(
                                    padding: padding_05_TB,
                                    child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, mainAxisSize: MainAxisSize.max, children: [
                                      Column(crossAxisAlignment: CrossAxisAlignment.start, children: [lText("인출신청캐시".tr(), style: st_b_16(fontWeight: FontWeight.w500)), lText("캐시_단위".tr(), style: st_12(textColor: color_999999))]),
                                      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                        Container(
                                          padding: padding_10,
                                          margin: padding_05,
                                          width: 180,
                                          decoration: BoxDecoration(border: Border.all(color: color_dedede), borderRadius: BorderRadius.circular(8)),
                                          child: TextField(
                                            focusNode: _focus,
                                            keyboardType: TextInputType.number,
                                            textAlign: TextAlign.right,
                                            textInputAction: TextInputAction.done,
                                            decoration: InputDecoration(
                                              suffixIcon: lInkWell(
                                                onTap: () {
                                                  _payCtrl.clear();
                                                  onUpDate();
                                                },
                                                child: _focus.hasFocus ? Lcons.clear() : Container(),
                                              ),
                                              suffixIconConstraints: BoxConstraints.tight(Size(20, 20)),
                                              border: OutlineInputBorder(borderSide: BorderSide.none),
                                              contentPadding: padding_0,
                                              isDense: true,
                                              hintText: '0',
                                              hintStyle: st_15(textColor: color_b2b2b2),
                                            ),
                                            maxLines: 1,
                                            controller: _payCtrl,
                                            onChanged: (value) => onUpDate(),
                                          ),
                                        ),
                                        Lcons.cash(isEnabled: true),
                                      ]),
                                    ])),
                              ]),
                            ),
                            lDivider(color: Commons.getColor(), thickness: 2, padding: padding_20_LR),
                            if (account.bankAccount.isNotEmpty)
                              Padding(
                                padding: EdgeInsets.fromLTRB(20, 15, 20, 0),
                                child: TypeComponent(
                                  type: "입금예정금액".tr(),
                                  typeStyle: st_b_18(fontWeight: FontWeight.w700),
                                  data: StringUtils.formatPay(int.tryParse(_payCtrl.text) ?? 0),
                                  dataStyle: st_b_18(fontWeight: FontWeight.w700),
                                  icon: Padding(padding: padding_05_LR, child: lText("원".tr())),
                                  isExpanded: true,
                                ),
                              ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 10, 20, 20),
                              child: TypeComponent(
                                type: "잔여캐시".tr(),
                                typeStyle: st_b_16(fontWeight: FontWeight.w500),
                                data: _calc() > -1 ? StringUtils.formatPay(_calc()) : '0',
                                dataStyle: st_b_16(fontWeight: FontWeight.w700, textColor: _calc() > -1 ? color_222222 : color_ff3b30),
                                icon: Padding(padding: padding_05_LR, child: lText("원".tr())),
                                isExpanded: true,
                              ),
                            ),
                            lDivider(color: color_dedede.withOpacity(0.32), thickness: 8, padding: padding_10_TB),
                            Padding(
                              padding: padding_20,
                              child: Column(
                                children: [
                                  contentView(data: ContentData(content: "인출계좌정보".tr())),
                                  Center(
                                    child: account.bankAccount.isNotEmpty
                                        ? Account(
                                            data: account,
                                            onDelete: () {
                                              showNormalDlg(
                                                  message: "계좌삭제_안내".tr(),
                                                  btnLeft: "취소".tr(),
                                                  onClickAction: (a) async {
                                                    if (a == DialogAction.yes) {
                                                      await storageHelper.setAccount(WithdrawalData());
                                                      onUpDate();
                                                    }
                                                  });
                                            },
                                          )
                                        : EmptyAccount(callback: () async {
                                            var ret = await Commons.page(context, routeRegisterAccount);
                                            if (ret != null) {
                                              onUpDate();
                                            }
                                          }),
                                  ),
                                  lText("인출요청_안내".tr())
                                ],
                              ),
                            )
                          ],
                        ),
                      )),
                      lBtn(
                        "캐시인출하기".tr(),
                        isEnabled: _confirmBtn((int.tryParse(_payCtrl.text) ?? 0), auth.user.my_info_data!.mypageInfoCnt!.linkCash),
                        onClickAction: () => _requestWithdrawal(account, int.tryParse(_payCtrl.text) ?? 0),
                      ),
                    ],
                  )),
              NextRequestListener(
                onNextRequest: () => _requestWithdrawalList(isNext: true),
                child: LinkmomRefresh(
                    padding: padding_0,
                    onRefresh: () => _requestWithdrawalList(),
                    child: Column(
                      children: [
                        Filter(range: _range, onClickAction: () => Commons.page(context, routeWithdrawalFilter, arguments: WithdrawalFilterPage())),
                        _list.results.isNotEmpty ? Column(children: _list.results.map<Widget>((e) => Withdrawal(data: e)).toList()) : lEmptyView(padding: padding_20, height: data.height(context, 0.5))
                      ],
                    )),
              )
            ])));
  }

  bool _confirmBtn(int cash, int total) {
    return cash > 0 && (total - cash) > -1 && account.accountUser.isNotEmpty;
  }

  Future<void> _requestWithdrawal(WithdrawalData data, int cash) async {
    try {
      focusClear(context);
      flag.enableLoading(fn: () => onUpDate());
      String name = await encryptHelper.encodeData(data.accountUser);
      String account = await encryptHelper.encodeData(data.bankAccount);
      WithdrawalRequest req = WithdrawalRequest(bankName: data.bankName, accountUser: name, bankAccount: account, withdrawalCash: cash);
      apiHelper.requestWithdrawal(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.page(context, routeCompleteView,
              arguments: CompleteViewPage(
                title: "캐시인출하기".tr(),
                resultMessage: "캐시인출요청이접수되었습니다.".tr(),
                btn: lBtn("확인".tr(), onClickAction: () => Commons.pagePop(context)),
                footer: Container(
                  margin: EdgeInsets.only(top: 50),
                  padding: EdgeInsets.symmetric(vertical: 30),
                  decoration: BoxDecoration(border: Border(top: BorderSide(color: color_eeeeee))),
                  child: Column(
                    children: [
                      TypeComponent(
                        type: "신청일자".tr(),
                        data: StringUtils.parseYMDHMS(response.getData().createdate),
                        isExpanded: true,
                        typeStyle: st_b_16(fontWeight: FontWeight.w500),
                        dataStyle: st_b_16(fontWeight: FontWeight.w500),
                      ),
                      TypeComponent(
                          type: "입금예정액".tr(),
                          data: StringUtils.formatPay(response.getData().withdrawalCash),
                          isExpanded: true,
                          icon: Padding(
                            padding: padding_02_L,
                            child: lText("원".tr(), style: st_b_16(fontWeight: FontWeight.w700)),
                          ),
                          typeStyle: st_b_16(fontWeight: FontWeight.w500),
                          dataStyle: st_b_16(fontWeight: FontWeight.w700)),
                      TypeComponent(
                        type: "입금정보".tr(),
                        data: '${response.getData().bankNameText} (${response.getData().bankAccount})',
                        isExpanded: true,
                        typeStyle: st_b_16(fontWeight: FontWeight.w500),
                        dataStyle: st_b_16(fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                ),
              ));
        } else {
          showNormalDlg(message: response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestWithdrawalList({bool isNext = false}) {
    try {
      if (isNext && _list.next.isEmpty) return;
      WithdrawalListRequest req = WithdrawalListRequest(
        startDate: StringUtils.formatYMD(_range.start),
        endDate: StringUtils.formatYMD(_range.end),
      );
      apiHelper.requestWithdrawalList(req, next: isNext ? _list.next : null).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          if (isNext) {
            _list.results.addAll(response.getData().results);
          } else {
            _list = response.getData();
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  int _calc() {
    return auth.user.my_info_data!.mypageInfoCnt!.linkCash - (int.tryParse(_payCtrl.text) ?? 0);
  }
}

class Account extends StatelessWidget {
  final WithdrawalData data;
  final Function onDelete;

  const Account({Key? key, required this.data, required this.onDelete}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String maskedAccount = data.bankAccount.length < 4 ? data.bankAccount : data.bankAccount.substring(0, 3) + '*******' + data.bankAccount.substring(data.bankAccount.length - 4, data.bankAccount.length);
    double width = 250;
    double height = 149;
    return Container(
        margin: padding_30,
        child: Stack(
          alignment: Alignment.topRight,
          children: [
            Container(
              margin: padding_15,
              width: width,
              height: height,
              decoration: BoxDecoration(color: Commons.isLinkMom() ? color_ab81b8 : color_a0c6c1, borderRadius: BorderRadius.circular(8)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: padding_20,
                    child: lText(data.bankNameText, style: st_16(fontWeight: FontWeight.w700)),
                  ),
                  Container(
                    width: width,
                    height: height / 3,
                    padding: EdgeInsets.fromLTRB(20, 14, 20, 16),
                    decoration: BoxDecoration(color: Colors.white.withOpacity(0.1), borderRadius: BorderRadius.vertical(bottom: Radius.circular(8))),
                    child: lText("${data.accountUser} $maskedAccount", style: st_14(fontWeight: FontWeight.w500), overflow: TextOverflow.fade, softWrap: false),
                  )
                ],
              ),
            ),
            lInkWell(
              onTap: () => onDelete(),
              child: Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black.withOpacity(0.8)),
                padding: padding_05,
                child: Lcons.nav_close(color: Colors.white, isEnabled: true),
              ),
            )
          ],
        ));
  }
}

class EmptyAccount extends StatelessWidget {
  final Function callback;

  const EmptyAccount({Key? key, required this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = 250;
    double height = 149;
    return lInkWell(
      onTap: () => callback(),
      child: Container(
        margin: padding_30,
        width: width,
        height: height,
        decoration: BoxDecoration(color: color_eeeeee.withOpacity(0.42), borderRadius: BorderRadius.circular(8)),
        child: DottedBorder(
          radius: Radius.circular(8),
          borderType: BorderType.RRect,
          padding: padding_20,
          color: color_dedede,
          dashPattern: [4, 4],
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: padding_07,
                  margin: padding_10,
                  decoration: BoxDecoration(color: Commons.getColor(), shape: BoxShape.circle),
                  child: Lcons.add(color: Colors.white, isEnabled: true, size: 25),
                ),
                lText("계좌등록하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w700)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Withdrawal extends StatelessWidget {
  final WithdrawalData data;

  const Withdrawal({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: padding_20,
        decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
        padding: padding_20_B,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                lText(StringUtils.parseYYMMDD(data.createdate), style: st_12(textColor: color_b2b2b2, fontWeight: FontWeight.normal)),
                sb_h_05,
                lText(!data.isWithdrawal ? "신청완료".tr() : "입금완료".tr(), style: st_b_14(fontWeight: FontWeight.w500)),
              ],
            ),
            Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
              TypeComponent(
                isExpanded: true,
                type: StringUtils.formatPay(data.withdrawalCash),
                typeStyle: st_b_16(fontWeight: FontWeight.w500),
                typeIcon: Padding(
                  padding: padding_02_L,
                  child: lText("원".tr(), style: st_b_14(fontWeight: FontWeight.w500)),
                ),
              ),
              sb_h_05,
              lText("${data.bankNameText} (${data.bankAccount})", style: st_13(textColor: color_999999, fontWeight: FontWeight.w500)),
            ])
          ],
        ));
  }
}
