import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/data/storage/model/bank_model.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

class BankListPage extends BaseStateful {
  BankListPage();

  @override
  _BankListPageState createState() => _BankListPageState();
}

class _BankListPageState extends BaseStatefulState<BankListPage> {
  BankModel _selected = BankModel();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (storageHelper.bankList.isEmpty) {
        if (storageHelper.bankList.isEmpty) await storageHelper.getBankListWithoutPath();
        onUpDate();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        appBar: appBar("은행선택".tr()),
        body: TabPageView(
          tabbarName: [
            TabItem(title: "은행".tr()),
            TabItem(title: "증권사".tr()),
          ],
          tabbarView: [
            lScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Padding(
                  padding: padding_30_TB,
                  child: lText("은행을선택해주세요".tr(), style: st_b_20(fontWeight: FontWeight.bold)),
                ),
                GridView.count(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  crossAxisCount: 3,
                  childAspectRatio: 106 / 80,
                  children: storageHelper.bankList
                      .where((e) => (int.tryParse(e.code) ?? 0) < 200)
                      .map((e) => lInkWell(
                          onTap: () => Commons.pagePop(context, data: e),
                          child: Bank(
                            icon: Image.asset(bank((int.tryParse(e.code) ?? 0)), width: 48),
                            title: e.name.replaceAll("은행".tr(), ''),
                            selected: _selected == e,
                          )))
                      .toList(),
                )
              ]),
            ),
            lScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Padding(
                  padding: padding_30_TB,
                  child: lText("증권사를선택해주세요".tr(), style: st_b_20(fontWeight: FontWeight.bold)),
                ),
                GridView.count(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  crossAxisCount: 3,
                  childAspectRatio: 106 / 80,
                  children: storageHelper.bankList
                      .where((e) => (int.tryParse(e.code) ?? 0) > 199)
                      .map((e) => lInkWell(
                          onTap: () => Commons.pagePop(context, data: e),
                          child: Bank(
                            icon: Image.asset(bank((int.tryParse(e.code) ?? 0)), width: 48),
                            title: e.name.replaceAll("증권".tr(), ''),
                            selected: _selected == e,
                          )))
                      .toList(),
                )
              ]),
            ),
          ],
        ));
  }
}

class Bank extends StatefulWidget {
  final Widget icon;
  final String title;
  final bool selected;
  const Bank({Key? key, required this.icon, required this.title, this.selected = false}) : super(key: key);

  @override
  State<Bank> createState() => _BankState();
}

class _BankState extends State<Bank> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: padding_05_RB,
      decoration: BoxDecoration(
        color: color_eeeeee.withOpacity(0.12),
        borderRadius: BorderRadius.circular(16),
        border: Border.all(color: color_cecece),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: padding_02,
            child: widget.icon,
          ),
          lText(widget.title, style: st_13(textColor: color_545454, fontWeight: FontWeight.w500))
        ],
      ),
    );
  }
}
