import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/data/network/models/withdrawal_list_request.dart';
import 'package:linkmom/data/network/models/withdrawal_list_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

import 'withdrawal_page.dart';

class WithdarawalFilterResultPage extends BaseStateful {
  final DateTimeRange? range;
  final WithdrawalListData? list;
  WithdarawalFilterResultPage({this.range, this.list});

  @override
  _WithdarawalFilterResultPageState createState() => _WithdarawalFilterResultPageState();
}

class _WithdarawalFilterResultPageState extends BaseStatefulState<WithdarawalFilterResultPage> {
  WithdrawalListData _list = WithdrawalListData(results: []);

  @override
  void initState() {
    super.initState();
    _list = widget.list!;
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        backgroundColor: color_fafafa,
        appBar: appBar("캐시인출".tr()),
        body: SliverNextListView(
          pinned: Filter(range: widget.range, onClickAction: () => Commons.pagePop(context)),
          view: _list.results.map<Widget>((e) => Withdrawal(data: e)).toList(),
          onRefresh: () => _requestWithdrawalList(),
          onNext: () => _requestWithdrawalList(isNext: true),
          showNextLoading: _list.next.isNotEmpty,
          emptyView: lEmptyView(
            height: Commons.height(context, 0.7),
            textString: "다른조건으로재검색".tr(),
            fn: () => Commons.pagePop(context),
          ),
        ));
  }

  void _requestWithdrawalList({bool isNext = false}) {
    try {
      WithdrawalListRequest req = WithdrawalListRequest(
        startDate: StringUtils.formatYMD(widget.range!.start),
        endDate: StringUtils.formatYMD(widget.range!.end),
      );
      if (isNext && _list.next.isEmpty) return;
      apiHelper.requestWithdrawalList(req, next: isNext ? _list.next : null).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          if (isNext) {
            _list.results.addAll(response.getData().results);
            _list.next = response.getData().next;
          } else {
            _list = response.getData();
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}
