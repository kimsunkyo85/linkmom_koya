import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/withdrawal_list_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/filter_page_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/main/mypage/withdrawal/withdrawal_filter_result_page.dart';

class WithdrawalFilterPage extends BaseStateful {
  @override
  _WithdrawalFilterPageState createState() => _WithdrawalFilterPageState();
}

class _WithdrawalFilterPageState extends BaseStatefulState<WithdrawalFilterPage> {
  List<FilterOption> filter = [
    FilterOption(type: FilterType.withdrawalStatus, info: [
      FilterInfo(
        value: WithdrawalStatus.ready.value,
        title: WithdrawalStatus.ready.name,
        desc: "입금예정_안내".tr(),
      ),
      FilterInfo(
        value: WithdrawalStatus.complete.value,
        title: WithdrawalStatus.complete.name,
        desc: "입금완료_안내".tr(),
      )
    ]),
  ];

  FilterSelect _filterSelect = FilterSelect();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar("상세조회".tr(), hide: false),
        body: FilterPageView(
          filterOptions: filter,
          listType: ListType.withdrawalStatus,
          rangeType: [-1, -2, -3],
          rangeText: ["1${"개월".tr()}", "2${"개월".tr()}", "3${"개월".tr()}"],
          filterSelect: _filterSelect,
          searchAction: (range, options) {
            if (range is FilterSelect) {
              _filterSelect = range;
              _requestWithdrawalList(range.range!, options);
            } else {
              _requestWithdrawalList(range, options);
            }
          },
        ),
      ),
    );
  }

  void _requestWithdrawalList(DateTimeRange range, List<FilterOption> option) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      List<int> checked = option.first.info.where((e) => e.checked).map((e) => e.value).toList();
      WithdrawalListRequest req = WithdrawalListRequest(
        startDate: StringUtils.formatYMD(range.start),
        endDate: StringUtils.formatYMD(range.end),
        isWithdrawal: checked.length > 1 || checked.isEmpty ? null : checked.first,
      );
      apiHelper.requestWithdrawalList(req).then((response) {
        flag.disableLoading(fn: () => onUpDate());
        Commons.page(context, routeWithdrawalFilterResult, arguments: WithdarawalFilterResultPage(list: response.getData(), range: range));
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }
}
