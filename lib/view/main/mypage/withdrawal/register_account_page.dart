import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/data/network/models/withdrawal_response.dart';
import 'package:linkmom/data/storage/model/bank_model.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/content_view.dart';

class RegisterAccountPage extends BaseStateful {
  RegisterAccountPage();

  @override
  _RegisterAccountPageState createState() => _RegisterAccountPageState();
}

class _RegisterAccountPageState extends BaseStatefulState<RegisterAccountPage> {
  TextEditingController _accountCtrl = TextEditingController();
  BankModel _selectedBank = BankModel();
  bool _checked = false;

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        appBar: appBar("입금계좌등록".tr(), hide: false),
        body: Column(
          children: [
            Expanded(
              child: lScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: padding_20_TB,
                      child: contentView(data: ContentData(content: "계좌정보를입력해주세요".tr())),
                    ),
                    Container(
                        margin: padding_10_B,
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                          Padding(
                            padding: padding_10_B,
                            child: lText("은행선택".tr(), style: st_13(textColor: color_545454, fontWeight: FontWeight.w500)),
                          ),
                          Container(
                            decoration: BoxDecoration(border: Border.all(color: color_dedede), borderRadius: BorderRadius.circular(8)),
                            child: lIconText(
                                padding: padding_15,
                                title: (int.tryParse(_selectedBank.code) ?? 0) > 0 ? _selectedBank.name : "은행을선택해주세요".tr(),
                                textStyle: st_15(textColor: (int.tryParse(_selectedBank.code) ?? 0) > 0 ? color_222222 : color_cecece),
                                icon: Padding(child: Lcons.bottom_more_arrow(size: 25, isEnabled: true, color: color_545454), padding: padding_10_R),
                                isExpanded: true,
                                isLeft: false,
                                onClick: () async {
                                  var account = await Commons.page(context, routeBankList);
                                  if (account != null) {
                                    _selectedBank = account;
                                    onUpDate();
                                  }
                                }),
                          ),
                        ])),
                    sb_h_20,
                    RoundData(
                      title: "계좌번호".tr(),
                      style: st_13(textColor: color_545454, fontWeight: FontWeight.w500),
                      bodyWidget: TextField(
                        keyboardType: TextInputType.number,
                        inputFormatters: [FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))],
                        decoration: InputDecoration(
                          border: OutlineInputBorder(borderSide: BorderSide.none),
                          contentPadding: padding_0,
                          isDense: true,
                          hintText: "계좌번호를입력해주세요".tr(),
                          hintStyle: st_15(textColor: color_cecece),
                        ),
                        maxLines: 1,
                        controller: _accountCtrl,
                        onChanged: (value) {
                          value.replaceAll(new RegExp(r'[0-9]'), '');
                          onUpDate();
                        },
                      ),
                    ),
                    sb_h_20,
                    RoundData(
                      title: "예금주_본인명의계좌만가능".tr(),
                      style: st_13(textColor: color_545454, fontWeight: FontWeight.w500),
                      bodyWidget: lText(auth.user.my_info_data!.first_name, style: st_b_15()),
                    ),
                    lRoundContainer(
                        borderRadius: 8.0,
                        bgColor: color_eeeeee.withOpacity(0.32),
                        padding: padding_20,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            lText("인출_안내_1".tr(), style: st_b_14(fontWeight: FontWeight.w500)),
                            sb_h_05,
                            lText("인출_안내_2".tr(), style: st_b_13(), overflow: TextOverflow.clip),
                          ],
                        )),
                  ],
                ),
              ),
            ),
            sb_h_30,
            Padding(
              padding: padding_20_LTR,
              child: lIconText(
                  title: "다음에도계좌사용하기".tr(),
                  textStyle: st_b_14(fontWeight: FontWeight.w500),
                  icon: Lcons.check_circle(size: 25, isEnabled: _checked, color: Commons.getColor(), disableColor: color_dedede),
                  onClick: () {
                    _checked = !_checked;
                    onUpDate();
                  }),
            ),
            lBtn("등록하기".tr(), isEnabled: (int.tryParse(_selectedBank.code) ?? 0) > 0 && _accountCtrl.text.isNotEmpty, onClickAction: () async {
              await storageHelper.setAccount(WithdrawalData(
                accountUser: auth.user.my_info_data!.first_name,
                withdrawalCash: 0,
                bankName: _selectedBank.code,
                bankNameText: _selectedBank.name,
                bankAccount: _accountCtrl.text,
              ));
              Commons.pagePop(context, data: true);
            }),
          ],
        ));
  }
}
