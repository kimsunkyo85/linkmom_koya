import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/review_list_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/main/mypage/review/review_view.dart';

import 'review_list_reviewinfo_response.dart';

class ReviewViewPage extends BaseStateful {
  final int userId;
  final int gubun;
  ReviewViewPage({this.userId = 0, this.gubun = 1});

  @override
  _ReviewViewPageState createState() => _ReviewViewPageState();
}

class _ReviewViewPageState extends BaseStatefulState<ReviewViewPage> {
  ReviewInfoResultData _info = ReviewInfoResultData(result: []);
  @override
  void initState() {
    super.initState();
    if (widget.userId != 0) {
      _requestReviewInfo(widget.userId);
    }
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          appBar: appBar("후기".tr(), hide: false),
          body: _info.userinfo != null ? ReviewView(_info, viewType: ViewType.view) : lEmptyView(title: "등록된후기가없습니다".tr()),
        ));
  }

  void _requestReviewInfo(int id) {
    ReviewListRequest req = ReviewListRequest(gubun: widget.gubun, type: 2, userId: id);
    flag.enableLoading(fn: () => onUpDate());
    apiHelper.requestReviewInfoList(req).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        _info = response.getData();
      }
      flag.disableLoading(fn: () => onUpDate());
    });
  }
}
