import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/review_view_response.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import 'review_list_reviewinfo_response.dart';

class ReviewView extends StatefulWidget {
  final ReviewInfoResultData reviewList;
  final ViewType viewType;
  ReviewView(this.reviewList, {this.viewType = ViewType.normal});

  @override
  State<ReviewView> createState() => _ReviewViewState();
}

class _ReviewViewState extends State<ReviewView> with TickerProviderStateMixin {
  late TabController _reviewCtrl;

  @override
  void initState() {
    super.initState();
    _reviewCtrl = TabController(vsync: this, length: 3);
  }

  @override
  Widget build(BuildContext context) {
    return lScrollView(
        padding: padding_20,
        child: widget.reviewList.userinfo != null
            ? Column(
                children: [
                  widget.reviewList.userinfo!.name.isNotEmpty
                      ? Container(
                          margin: padding_20_TB,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              lProfileAvatar(widget.reviewList.userinfo!.profileimg),
                              sb_h_15,
                              lText("${widget.reviewList.userinfo!.grade}", style: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
                              sb_h_05,
                              lText("${widget.reviewList.userinfo!.name} ${"링크쌤".tr()}", style: st_b_18(fontWeight: FontWeight.w700)),
                            ],
                          ))
                      : Container(
                          margin: padding_20_TB,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              lProfileAvatar(widget.reviewList.userinfo!.profileimg),
                              sb_h_15,
                              lText("${widget.reviewList.userinfo!.grade}", style: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
                              sb_h_05,
                              lText("${widget.reviewList.userinfo!.name} ${"링크쌤".tr()}", style: st_b_18(fontWeight: FontWeight.w700)),
                            ],
                          )),
                  Column(
                    children: [
                      lDivider(color: color_eeeeee),
                      Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                        Row(
                          children: [
                            Lcons.review_info(isEnabled: Commons.isLinkMom(), size: 40),
                            sb_w_10,
                            Column(
                              children: [
                                lText("총후기".tr(), style: st_13(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
                                lText(_getReview(widget.reviewList.userinfo!), style: st_18(textColor: color_545454, fontWeight: FontWeight.w700)),
                              ],
                            )
                          ],
                        ),
                        Container(
                          height: 50,
                          margin: EdgeInsets.fromLTRB(30, 20, 30, 20),
                          child: lDividerVertical(color: color_eeeeee, thickness: 1),
                        ),
                        Row(
                          children: [
                            Lcons.diary_like_type(isEnabled: Commons.isLinkMom(), size: 40),
                            sb_w_10,
                            Column(
                              children: [
                                lText("좋아요".tr(), style: st_13(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
                                lText(_getLike(widget.reviewList.userinfo!), style: st_18(textColor: color_545454, fontWeight: FontWeight.w700)),
                              ],
                            )
                          ],
                        )
                      ]),
                      lDivider(color: color_eeeeee),
                    ],
                  ),
                  widget.reviewList.result.isNotEmpty
                      ? Container(
                          margin: padding_20_T,
                          child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: [
                            Container(
                              margin: padding_10_TB,
                              width: Commons.width(context, 0.5),
                              height: kTextTabBarHeight - 25,
                              child: TabBar(
                                controller: _reviewCtrl,
                                tabs: [
                                  Tab(text: "등원".tr()),
                                  Tab(text: "하원".tr()),
                                  Tab(text: "보육".tr()),
                                ],
                                indicator: BoxDecoration(border: Border(bottom: BorderSide(color: Commons.getColor(), width: 2))),
                                indicatorSize: TabBarIndicatorSize.label,
                                labelStyle: st_16(fontWeight: FontWeight.w700),
                                labelColor: Commons.getColor(),
                                unselectedLabelColor: color_dedede,
                                unselectedLabelStyle: st_16(fontWeight: FontWeight.w700),
                              ),
                            ),
                            AspectRatio(
                              aspectRatio: Commons.ratio(2),
                              child: Container(
                                  padding: padding_13_L,
                                  child: TabBarView(controller: _reviewCtrl, children: [
                                    widget.reviewList.result.where((t) => t.type < 10).isNotEmpty
                                        ? Column(
                                            children: widget.reviewList.result
                                                .where((t) => t.type < 10)
                                                .map((e) => Container(
                                                        child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: [
                                                        Container(
                                                            padding: padding_10_R,
                                                            child: Stack(
                                                              alignment: Alignment.center,
                                                              children: [Lcons.review_cnt_bg(isEnabled: Commons.isLinkMom(), disableColor: color_e4f1f0, color: color_b579c8_op10), lText('${e.count}', style: st_b_14(fontWeight: FontWeight.w700))],
                                                            )),
                                                        lText("${e.review}", style: st_b_14()),
                                                      ],
                                                    )))
                                                .toList())
                                        : lEmptyView(),
                                    widget.reviewList.result.where((t) => t.type < 20 && t.type > 10).isNotEmpty
                                        ? Column(
                                            children: widget.reviewList.result
                                                .where((t) => t.type < 20 && t.type > 10)
                                                .map((e) => Container(
                                                        child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: [
                                                        Container(
                                                            padding: padding_10_R,
                                                            child: Stack(
                                                              alignment: Alignment.center,
                                                              children: [Lcons.review_cnt_bg(isEnabled: Commons.isLinkMom(), disableColor: color_e4f1f0, color: color_b579c8_op10), lText('${e.count}', style: st_b_14(fontWeight: FontWeight.w700))],
                                                            )),
                                                        lText("${e.review}", style: st_b_14()),
                                                      ],
                                                    )))
                                                .toList())
                                        : lEmptyView(),
                                    widget.reviewList.result.where((t) => t.type < 30 && t.type > 20).isNotEmpty
                                        ? Column(
                                            children: widget.reviewList.result
                                                .where((t) => t.type < 30 && t.type > 20)
                                                .map((e) => Container(
                                                        child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: [
                                                        Container(
                                                            padding: padding_10_R,
                                                            child: Stack(
                                                              alignment: Alignment.center,
                                                              children: [Lcons.review_cnt_bg(isEnabled: Commons.isLinkMom(), disableColor: color_e4f1f0, color: color_b579c8_op10), lText('${e.count}', style: st_b_14(fontWeight: FontWeight.w700))],
                                                            )),
                                                        lText("${e.review}", style: st_b_14()),
                                                      ],
                                                    )))
                                                .toList())
                                        : lEmptyView()
                                  ])),
                            )
                          ]))
                      : Container(
                          padding: padding_20,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              lText("작성된후기가없습니다".tr(), style: st_b_16(fontWeight: FontWeight.w700)),
                              if (widget.viewType == ViewType.normal) lText("맘대디에게후기를요청해주세요".tr(), style: st_14(textColor: color_999999)),
                            ],
                          ),
                        ),
                ],
              )
            : lEmptyView(height: Commons.height(context, 0.4)));
  }

  String _getReview(ReviewUserInfo info) {
    return "${info.reviewLevel}${"개".tr()}";
  }

  String _getLike(ReviewUserInfo info) {
    return "${info.favoriteLevel}${"개".tr()}";
  }
}

class ReviewBlock extends StatelessWidget {
  final ReviewInfoResultData info;
  const ReviewBlock(this.info);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
          children: info.result.map((e) {
        return Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
                padding: padding_10_R,
                child: Stack(
                  alignment: Alignment.center,
                  children: [Lcons.review_cnt_bg(color: color_b579c8_op10), lText('${e.count}', style: st_b_14(fontWeight: FontWeight.w700))],
                )),
            lText("${e.review}"),
          ],
        ));
      }).toList()),
    );
  }
}
