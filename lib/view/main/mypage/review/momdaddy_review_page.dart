import 'dart:math';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/review_list_request.dart';
import 'package:linkmom/data/network/models/review_list_response.dart';
import 'package:linkmom/data/network/models/review_view_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/dropdown/custom_dropdown.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/review/review_list_reviewinfo_response.dart';
import 'package:linkmom/view/main/mypage/review/review_save_page.dart';

class MomdaddyReviewPage extends BaseStateful {
  final DataManager? data;
  final int index;

  MomdaddyReviewPage({this.data, this.index = 0});

  @override
  _MypageMomdaddyReviewPageState createState() => _MypageMomdaddyReviewPageState();

  static const int WRITE = 0;
  static const int LIST = 1;
  static const int RECEIVE = 2;
}

class _MypageMomdaddyReviewPageState extends BaseStatefulState<MomdaddyReviewPage> with SingleTickerProviderStateMixin {
  TabController? _controller;
  String? _selected;
  int _totalReviewCnt = 0;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 3, vsync: this);
    _controller!.animateTo(widget.index);
    _selected = "전체".tr();
    flag.enableLoading(fn: () => onUpDate());
    _requestGetList();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar("후기관리".tr()),
            body: TabPageView(
              tabbarName: [
                TabItem(title: "후기남기기".tr()),
                TabItem(title: "내가남긴후기".tr()),
                TabItem(title: "내가받은후기".tr()),
              ],
              tabbarView: [
                Container(
                  color: color_fafafa,
                  child: ListNextView(
                    header: Container(margin: padding_20, child: lText("${"작성안된후기총".tr()}${data.reviewItem.mWritableList.count}${"개".tr()}")),
                    view: data.reviewItem.mWritableList.results!
                        .map((e) => _reviewBlock(
                              e.carescheduleinfo!.caredate!.first,
                              e.carescheduleinfo!.servicetype_text,
                              e.linkmom_name,
                              lastDate: e.carescheduleinfo!.caredate!.last,
                              result: e,
                            ))
                        .toList(),
                    showNextLoading: data.reviewItem.mWritableList.next.isNotEmpty,
                    onRefresh: () => _requestGetList(index: MomdaddyReviewPage.WRITE),
                    onNext: () => _requestGetList(next: true, url: data.reviewItem.mWritableList.next),
                  ),
                ),
                Container(
                  color: color_fafafa,
                  child: ListNextView(
                    header: Container(
                      margin: EdgeInsets.fromLTRB(20, 5, 0, 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(child: lText("${"완료된후기총".tr()}${data.reviewItem.mCompleatList.count}${"개".tr()}")),
                          CustomDropDown(
                            margin: padding_20_R,
                            isExpanded: false,
                            bgColor: Colors.transparent,
                            icon: Transform.rotate(angle: 90 * pi / 180, child: Icon(Icons.sync_alt_sharp, size: 16)),
                            value: _selected,
                            style: st_b_13(),
                            itemsList: ["전체".tr(), "1개월".tr(), "3개월".tr(), "6개월".tr()],
                            onChanged: (value) {
                              _selected = value;
                              onUpDate();
                            },
                          ),
                        ],
                      ),
                    ),
                    view: _chooseFilter(data.reviewItem.mCompleatList.results!, _selected ?? '')
                        .map((e) => _reviewCompletaBlock(
                              e.carescheduleinfo!.caredate!.first,
                              e.carescheduleinfo!.servicetype_text,
                              e.carescheduleinfo!.caredate!.first,
                              lastDate: e.carescheduleinfo!.caredate!.last,
                              result: e,
                            ))
                        .toList(),
                    showNextLoading: data.reviewItem.mCompleatList.next.isNotEmpty,
                    onRefresh: () => _requestGetList(index: MomdaddyReviewPage.LIST),
                    onNext: () => _requestGetList(next: true, url: data.reviewItem.mCompleatList.next),
                  ),
                ),
                LinkmomRefresh(
                  onRefresh: () => _requestGetList(index: MomdaddyReviewPage.RECEIVE),
                  child: Container(
                    height: data.height(context, 1),
                    child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
                      Column(
                        children: [
                          Column(
                            children: [
                              Lcons.review_info(size: 60),
                              lText("총후기".tr(), style: st_13(textColor: color_main, fontWeight: FontWeight.w500)),
                            ],
                          ),
                          lText('$_totalReviewCnt${"개".tr()}', style: st_22(textColor: color_545454, fontWeight: FontWeight.w700)),
                          sb_h_20,
                          lText("링크쌤이나에게남긴후기에요".tr(), style: st_b_18(fontWeight: FontWeight.w700)),
                        ],
                      ),
                      Container(margin: padding_30_TB, child: lDivider()),
                      data.reviewItem.mReceiveList.result.isNotEmpty
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                _myReviewBlock(data.reviewItem.mReceiveList),
                              ],
                            )
                          : Container(
                              height: data.height(context, 0.4),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  lText("작성된후기가없습니다".tr(), style: st_b_16(fontWeight: FontWeight.w700)),
                                  lText("링크쌤에게후기를요청해주세요".tr(), style: st_14(textColor: color_999999)),
                                ],
                              )),
                    ]),
                  ),
                ),
              ],
              onCreateView: (ctrl) => _controller = ctrl,
            )));
  }

  Widget _reviewBlock(String careDate, String careType, String name, {required String lastDate, required ReviewResultsData result}) {
    return lInkWell(
        onTap: () {
          ReviewViewRequest req = ReviewViewRequest(matching: result.matching, gubun: USER_TYPE.mom_daddy.index + 1);
          apiHelper.requestReviewView(req).then((response) async {
            if (response.getCode() == KEY_SUCCESS) {
              var ret = await Commons.page(context, routeReviewSave, arguments: ReviewSavePage(viewType: ViewType.modify, result: response.getData()));
              if (ret != null) {
                _requestGetList();
              }
            }
          });
        },
        child: Container(
          margin: padding_10_B,
          width: double.infinity,
          decoration: BoxDecoration(color: Colors.white, boxShadow: [BoxShadow(offset: Offset(0, 4), blurRadius: 12, color: Colors.black.withAlpha(11))]),
          child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.center, children: [
            Container(
                padding: padding_20,
                width: data.width(context, 0.85),
                child: Row(mainAxisAlignment: MainAxisAlignment.start, mainAxisSize: MainAxisSize.max, crossAxisAlignment: CrossAxisAlignment.center, children: [
                  lProfileAvatar(result.profileimg, radius: 25),
                  sb_w_10,
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _desc("돌봄날짜".tr(), careDate == lastDate ? '${StringUtils.parseMDE(careDate)}' : '${StringUtils.parseMDE(careDate)} ~ ${StringUtils.parseMDE(lastDate)}'),
                      sb_h_03,
                      _desc("돌봄유형".tr(), careType),
                    ],
                  ),
                ])),
            lDivider(color: color_eeeeee),
            Container(
              padding: EdgeInsets.fromLTRB(40, 20, 40, 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  lText("${result.linkmom_name} ${"링크쌤의후기를작성해주세요".tr()}", style: st_14(textColor: color_545454, fontWeight: FontWeight.w500)),
                  Lcons.nav_right(color: Commons.getColor(), size: 15),
                ],
              ),
            )
          ]),
        ));
  }

  Widget _reviewCompletaBlock(String careDate, String careType, String writeDate, {required String lastDate, required ReviewResultsData result}) {
    return lInkWell(
        onTap: () {
          ReviewViewRequest req = ReviewViewRequest(matching: result.matching, gubun: USER_TYPE.mom_daddy.index + 1);
          apiHelper.requestReviewView(req).then((response) {
            if (response.getCode() == KEY_SUCCESS) {
              Commons.page(context, routeReviewSave, arguments: ReviewSavePage(viewType: ViewType.view, viewData: response.getData()));
            }
          });
        },
        child: Container(
          margin: padding_10_B,
          width: double.infinity,
          decoration: BoxDecoration(color: Colors.white, boxShadow: [BoxShadow(offset: Offset(0, 4), blurRadius: 12, color: Colors.black.withAlpha(11))]),
          child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.center, children: [
            Container(
                padding: padding_20,
                width: data.width(context, 0.85),
                child: Row(mainAxisSize: MainAxisSize.max, crossAxisAlignment: CrossAxisAlignment.center, children: [
                  Expanded(flex: 1, child: lProfileAvatar(result.profileimg, radius: 25)),
                  Expanded(
                      flex: 4,
                      child: Column(
                        children: [
                          _desc("돌봄날짜".tr(), lastDate.isEmpty ? '${StringUtils.parseMDE(careDate)}' : '${StringUtils.parseMDE(careDate)} ~ ${StringUtils.parseMDE(lastDate)}'),
                          _desc("돌봄유형".tr(), careType),
                          _desc("아이정보".tr(), result.childinfo),
                        ],
                      )),
                ])),
            lDivider(color: color_eeeeee),
            Container(
              padding: EdgeInsets.fromLTRB(40, 20, 40, 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  lText("${result.linkmom_name} ${"링크쌤의후기작성이완료되었습니다".tr()}", style: st_b_14(disableColor: color_222222, fontWeight: FontWeight.w500)),
                  Lcons.check(color: Commons.getColor(), isEnabled: true),
                ],
              ),
            )
          ]),
        ));
  }

  Widget _myReviewBlock(ReviewInfoResultData cntList) {
    return Container(
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: cntList.result.map((e) {
            return Container(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                    padding: padding_10_R,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [Lcons.review_cnt_bg(), lText('${e.count}', style: st_b_14(fontWeight: FontWeight.w700))],
                    )),
                lText(e.review),
              ],
            ));
          }).toList()),
    );
  }

  Widget _desc(String title, String info) {
    return Row(mainAxisAlignment: MainAxisAlignment.start, children: [lText(title, style: st_b_14(textColor: color_999999)), sb_w_08, lText(info, style: st_b_14())]);
  }

  void _requestGetList({bool next = false, String? url, int? index}) {
    try {
      if (next && url == null) return;
      if (index != null) {
        ReviewListRequest req = ReviewListRequest(gubun: USER_TYPE.mom_daddy.index + 1, type: index);
        if (index < 2) {
          _requestReviewList(index);
        } else {
          _totalReviewCnt = 0;
          apiHelper.requestReviewInfoList(req, url: url).then((response) {
            if (response.getCode() == KEY_SUCCESS) {
              data.reviewItem.mReceiveList = response.getData();
              data.reviewItem.mReceiveList.result.forEach((e) {
                _totalReviewCnt += e.count;
              });
              auth.user.my_info_data!.mypageInfoCnt!.reviewCntMomdady = _totalReviewCnt;
            } else {
              data.reviewItem.mReceiveList = ReviewInfoResultData(result: []);
            }
            flag.disableLoading(fn: () => onUpDate());
          }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
        }
      } else {
        for (var i = 0; i < _controller!.length; i++) {
          ReviewListRequest req = ReviewListRequest(gubun: USER_TYPE.mom_daddy.index + 1, type: i);
          if (i != 2) {
            _requestReviewList(i);
          } else {
            apiHelper.requestReviewInfoList(req).then((response) {
              if (response.getCode() == KEY_SUCCESS) {
                data.reviewItem.mReceiveList = response.getData();
                data.reviewItem.mReceiveList.result.forEach((e) {
                  _totalReviewCnt += e.count;
                });
              } else {
                data.reviewItem.mReceiveList = ReviewInfoResultData(result: []);
              }
              flag.disableLoading(fn: () => onUpDate());
            }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
          }
        }
      }
    } catch (e) {}
  }

  void _requestReviewList(int index) {
    ReviewListRequest req = ReviewListRequest(gubun: USER_TYPE.mom_daddy.index + 1, type: index);
    apiHelper.requestReviewList(req).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        switch (index) {
          case 0:
            data.reviewItem.mWritableList = response.getData();
            break;
          case 1:
            data.reviewItem.mCompleatList = response.getData();
            break;
          default:
        }
      } else {
        switch (index) {
          case 0:
            data.reviewItem.mWritableList = ReviewListData(results: []);
            break;
          case 1:
            data.reviewItem.mCompleatList = ReviewListData(results: []);
            break;
          default:
        }
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }

  List<ReviewResultsData> _chooseFilter(List<ReviewResultsData> list, String selected) {
    return list.where((e) {
      int time = 0;
      if (selected == "1개월".tr()) {
        time = -30;
      } else if (selected == "3개월".tr()) {
        time = -90;
      } else if (selected == "6개월".tr()) {
        time = -180;
      } else {
        return true;
      }
      return StringUtils.parseYMD(e.carescheduleinfo!.caredate!.first).isAfter(DateTime.now().add(Duration(days: time)));
    }).toList();
  }
}
