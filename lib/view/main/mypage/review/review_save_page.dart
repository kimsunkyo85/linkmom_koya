import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/review_list_response.dart';
import 'package:linkmom/data/network/models/review_save_request.dart';
import 'package:linkmom/data/network/models/review_view_request.dart';
import 'package:linkmom/data/network/models/review_view_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

class ReviewSavePage extends BaseStateful {
  final ViewType viewType;
  final ReviewViewData? viewData;
  final ReviewViewData? result;
  final int matchingId;
  ReviewSavePage({this.viewType = ViewType.modify, this.viewData, this.result, this.matchingId = 0});

  @override
  _ReviewSavePageState createState() => _ReviewSavePageState();
}

class _ReviewSavePageState extends BaseStatefulState<ReviewSavePage> {
  CareScheduleInfo _careInfo = CareScheduleInfo();
  String _childName = '', _profileImg = '', _name = '';
  List<int>? _review;
  List<ReviewInfo> _reviewInfo = [];
  Map<int, bool> _checked = {};

  @override
  void initState() {
    super.initState();
    if (widget.viewType == ViewType.modify) {
      if (widget.result != null) {
        _careInfo = widget.result!.carescheduleinfo!;
        _childName = widget.result!.childinfo;
        _profileImg = widget.result!.profileimg;
        _name = widget.result!.linkmom_name.isEmpty ? widget.result!.momdady_name : widget.result!.linkmom_name;
        _reviewInfo = widget.result!.reviewinfo ?? [];
        _review = widget.result!.review ?? [];
      } else {
        _requestView(widget.matchingId);
      }
    } else {
      _careInfo = widget.viewData!.carescheduleinfo!;
      _childName = widget.viewData!.childinfo;
      _profileImg = widget.viewData!.profileimg;
      _name = widget.viewData!.linkmom_name.isEmpty ? widget.viewData!.momdady_name : widget.viewData!.linkmom_name;
      _reviewInfo = widget.viewData!.reviewinfo ?? [];
      _review = widget.viewData!.review ?? [];
    }
    _reviewInfo.forEach((element) {
      _checked.putIfAbsent(element.review, () => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    String appbarTitle;
    if (storageHelper.user_type == USER_TYPE.link_mom) {
      appbarTitle = "후기남기기".tr();
    } else {
      appbarTitle = "후기남기기".tr();
    }
    String startDate = '';
    String endDate = '';
    String sTime = '';
    String eTime = '';
    if (_reviewInfo.isNotEmpty) {
      startDate = StringUtils.parseMDE(_careInfo.caredate!.first);
      endDate = StringUtils.parseMDE(_careInfo.caredate!.last);
      sTime = StringUtils.parseAHMM(_careInfo.stime);
      eTime = StringUtils.parseAHMM(_careInfo.etime);
    }
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar(appbarTitle, hide: false),
            body: Container(
              child: Column(
                children: [
                  Expanded(
                    child: lScrollView(
                      padding: padding_0,
                      child: _reviewInfo.isNotEmpty
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                    color: color_fafafa,
                                    child: Container(
                                      margin: padding_20,
                                      padding: padding_20,
                                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(16), color: Colors.white, boxShadow: [BoxShadow(offset: Offset(0, 4), blurRadius: 12, color: Colors.black.withAlpha(11))]),
                                      child: Column(
                                        children: [
                                          Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                                            lTextBtn("돌봄종료".tr(), style: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.w500), bg: Commons.getColorBg(), padding: EdgeInsets.fromLTRB(15, 5, 15, 5)),
                                            sb_w_10,
                                            lText('${"정산_금액".tr()} ${StringUtils.formatPay(_careInfo.amount)}${"원".tr()}', style: st_14(textColor: color_999999, fontWeight: FontWeight.w500)),
                                          ]),
                                          Padding(
                                            padding: padding_10_T,
                                            child: Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center, children: [
                                              lProfileAvatar(_profileImg, radius: 23),
                                              Container(
                                                  padding: padding_15_L,
                                                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.center, children: [
                                                    Row(children: [
                                                      _desc(startDate, subInfo: endDate),
                                                      Container(height: 13, padding: padding_06_LR, child: lDividerVertical(color: color_cecece, thickness: 1.0)),
                                                      lText(_childName, style: st_b_14()),
                                                    ]),
                                                    sb_h_06,
                                                    Row(children: [
                                                      _desc(sTime, subInfo: eTime),
                                                      Container(height: 13, padding: padding_06_LR, child: lDividerVertical(color: color_cecece, thickness: 1.0)),
                                                      lText(_careInfo.servicetype_text, style: st_b_14()),
                                                    ]),
                                                  ]))
                                            ]),
                                          ),
                                        ],
                                      ),
                                    )),
                                Container(
                                  padding: padding_20_LRB,
                                  color: Colors.white,
                                  child: Column(
                                    children: [
                                      Container(padding: padding_30, child: _buildNameWidget(_name, widget.viewType)),
                                      lDivider(color: color_eeeeee),
                                      Container(
                                        padding: padding_10_TB,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            if (storageHelper.user_type == USER_TYPE.mom_daddy) lText(_careInfo.servicetype_text, style: st_b_16(fontWeight: FontWeight.w700)),
                                            sb_h_10,
                                            Column(
                                              children: _reviewInfo.map((e) => _selecter(e, widget.viewType != ViewType.modify ? _review!.contains(e.review) : true)).toList(),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            )
                          : lEmptyView(height: Commons.height(context, 0.5), title: "잠시만기다려주세요".tr()),
                    ),
                  ),
                  widget.viewType == ViewType.view ? Container() : lBtn("저장".tr(), onClickAction: () => _requestReviewSave(), isEnabled: _isConfirm()),
                ],
              ),
            )));
  }

  Widget _selecter(ReviewInfo message, bool isShowing) {
    return isShowing
        ? lInkWell(
            onTap: () {
              if (widget.viewType == ViewType.modify) {
                _checked[message.review] = !(_checked[message.review] ?? false);
                onUpDate();
              }
            },
            child: Container(
              margin: padding_08_TB,
              child: Row(children: [
                widget.viewType == ViewType.view
                    ? Lcons.check(color: Commons.getColor(), isEnabled: true)
                    : Lcons.check_circle(isEnabled: (_checked[message.review] != null ? _checked[message.review]! : false), color: Commons.getColor(), disableColor: color_d2d2d2),
                sb_w_10,
                lText(message.review_text)
              ]),
            ))
        : Container();
  }

  Widget _desc(String info, {String? subInfo}) {
    return Container(
      child: Row(children: [info == subInfo ? lText(info, style: st_b_14()) : lText('$info ~ $subInfo', style: st_b_14())]),
    );
  }

  bool _isConfirm() {
    return widget.viewType == ViewType.modify && _checked.values.contains(true);
  }

  void _requestReviewSave() {
    try {
      List<int> review = [];
      _checked.forEach((key, value) {
        if (value) review.add(key);
      });
      review.sort();
      ReviewSaveRequest req = ReviewSaveRequest(
        matching: widget.result != null ? widget.result!.matching : widget.matchingId,
        gubun: storageHelper.user_type.index + 1, // 1 - momdaddy, 2 - linkmom
        review: review,
      );
      apiHelper.requestReviewSave(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.pagePop(context, data: req);
        } else {
          showNormalDlg(message: response.getMsg());
        }
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  Widget _buildNameWidget(String name, ViewType type) {
    if (!Commons.isLinkMom()) {
      return _buildLinkmomName(name, type == ViewType.modify);
    } else {
      return _buildMomdaddyName(name, type == ViewType.modify);
    }
  }

  Widget _buildLinkmomName(String name, bool showComment) {
    return Padding(
      padding: padding_15,
      child: Column(
        children: [
          lText("$name ${"링크쌤".tr()}", style: st_b_18(fontWeight: FontWeight.w700)),
          if (showComment) Padding(padding: padding_10_T, child: lText("링크쌤이마음에드셨다면".tr(), style: st_13(textColor: color_999999))),
        ],
      ),
    );
  }

  Widget _buildMomdaddyName(String name, bool showComment) {
    return Padding(
      padding: padding_15,
      child: Column(
        children: [
          lText("$name ${"맘대디".tr()}", style: st_b_18(fontWeight: FontWeight.w700)),
          if (showComment) Padding(padding: padding_10_T, child: lText("맘대디에게후기를남겨주세요".tr(), style: st_13(textColor: color_999999))),
        ],
      ),
    );
  }

  void _requestView(int id) {
    ReviewViewRequest req = ReviewViewRequest(matching: id, gubun: storageHelper.user_type.index + 1);
    apiHelper.requestReviewView(req).then((response) async {
      if (response.getCode() == KEY_SUCCESS) {
        ReviewViewData resp = response.getData();
        _careInfo = resp.carescheduleinfo!;
        _childName = resp.childinfo;
        _profileImg = resp.profileimg;
        _name = resp.linkmom_name.isEmpty ? resp.momdady_name : resp.linkmom_name;
        _reviewInfo = resp.reviewinfo ?? [];
        _review = resp.review ?? [];
      }
      onUpDate();
    });
  }
}
