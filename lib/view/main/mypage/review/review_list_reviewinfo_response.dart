import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/data/network/models/review_view_response.dart';
import 'package:linkmom/main.dart';

@JsonSerializable()
class RevieInfoResponse {
  HeaderResponse? dataHeader;

  @JsonKey(name: Key_data)
  List<ReviewInfoResultData>? dataList;

  RevieInfoResponse({
    this.dataHeader,
    this.dataList,
  });

  ReviewInfoResultData getData() => dataList != null && dataList!.length != 0 ? dataList!.first : ReviewInfoResultData(result: []);

  List<ReviewInfoResultData> getDataList() => dataList!;

  int getCode() => dataHeader!.getCode();

  String getMsg() => dataHeader!.getMsg();

  factory RevieInfoResponse.fromJson(Map<String, dynamic> json) {
    HeaderResponse dataHeader = HeaderResponse.fromJson(json);
    List<ReviewInfoResultData> datas = [];
    try {
      datas = json[Key_data].map<ReviewInfoResultData>((i) => ReviewInfoResultData.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return RevieInfoResponse(
      dataHeader: dataHeader,
      dataList: datas,
    );
  }
}

@JsonSerializable()
class ReviewInfoResultData {
  static const String Key_result = 'result';
  static const String Key_user_info = 'user_info';

  ReviewInfoResultData({required this.result, this.userinfo});

  @JsonKey(name: Key_user_info)
  ReviewUserInfo? userinfo;
  @JsonKey(name: Key_result)
  List<ReviewResult> result = [];

  factory ReviewInfoResultData.fromJson(Map<String, dynamic> json) {
    List<ReviewResult> datas = [];
    try {
      datas = json[Key_result].map<ReviewResult>((i) => ReviewResult.fromJson(i)).toList();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> fromJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return ReviewInfoResultData(
      userinfo: ReviewUserInfo.fromJson(json[Key_user_info]),
      result: datas,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_result] = this.result;
    data[Key_user_info] = this.userinfo;
    return data;
  }

  @override
  String toString() {
    return 'ReviewInfoData{ $Key_result:${result.toString()}}';
  }
}


@JsonSerializable()
class ReviewResult {
  static const String Key_count = 'count';
  static const String Key_type = 'type';
  static const String Key_review = 'review';

  ReviewResult({required this.count, required this.review, this.type = 0});

  @JsonKey(name: Key_count)
  int count = 0;
  @JsonKey(name: Key_type)
  int type = 0;
  @JsonKey(name: Key_review)
  String review = '';

  factory ReviewResult.fromJson(Map<String, dynamic> json) {
    return ReviewResult(
      type: json[Key_type] as int,
      count: json[Key_count] as int,
      review: json[Key_review] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data[Key_type] = this.type;
    data[Key_count] = this.count;
    data[Key_review] = this.review;
    return data;
  }

  @override
  String toString() {
    return "ReviewResult{ $Key_count: $count, $Key_review: $count, $Key_type: $type}";
  }
}