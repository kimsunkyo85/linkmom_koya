import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/review_list_request.dart';
import 'package:linkmom/data/network/models/review_list_response.dart';
import 'package:linkmom/data/network/models/review_view_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/review/review_list_reviewinfo_response.dart';
import 'package:linkmom/view/main/mypage/review/review_save_page.dart';
import 'package:linkmom/view/main/mypage/review/review_view.dart';

class LinkmomReviewPage extends BaseStateful {
  final DataManager? data;
  final int index;
  LinkmomReviewPage({this.data, this.index = 0});

  @override
  _MypageLinkmomReviewPageState createState() => _MypageLinkmomReviewPageState();
  static const int LIST = 0;
  static const int WRITE = 1;
  static const int RECEIVE = 2;
}

class _MypageLinkmomReviewPageState extends BaseStatefulState<LinkmomReviewPage> with SingleTickerProviderStateMixin {
  TabController? _controller;
  int _totalReviewCnt = 0;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 3, vsync: this);
    _controller!.animateTo(widget.index);
    _requestGetList();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          context: context,
          appBar: appBar("후기관리".tr()),
          body: TabPageView(
            tabbarName: [
              TabItem(title: "내가받은후기".tr()),
              TabItem(title: "후기남기기".tr()),
              TabItem(title: "내가남긴후기".tr()),
            ],
            tabbarView: [
              ReviewView(data.reviewItem.lReceiveList),
              Container(
                padding: padding_10_TB,
                color: color_fafafa,
                child: ListNextView(
                  header: Container(margin: padding_20, child: lText("${"작성안된후기총".tr()}${data.reviewItem.lWritableList.count}${"개가있습니다".tr()}")),
                  view: data.reviewItem.lWritableList.results!.isNotEmpty
                      ? data.reviewItem.lWritableList.results!
                          .map((e) => _reviewBlock(
                                e.carescheduleinfo!.caredate!.first,
                                e.carescheduleinfo!.servicetype_text,
                                e.momdady_name,
                                lastDate: e.carescheduleinfo!.caredate!.last,
                                result: e,
                              ))
                          .toList()
                      : [],
                  showNextLoading: data.reviewItem.lWritableList.next.isNotEmpty,
                  onRefresh: () => _requestReview(0),
                  onNext: () => _requestNextList(data.reviewItem.lWritableList, 0),
                ),
              ),
              Container(
                padding: padding_10_TB,
                color: color_fafafa,
                child: ListNextView(
                  header: Container(margin: padding_20, child: lText("${"완료된후기총".tr()}${data.reviewItem.lCompleatList.count}${"개가있습니다".tr()}")),
                  view: data.reviewItem.lCompleatList.results!.isNotEmpty
                      ? data.reviewItem.lCompleatList.results!
                          .map((e) => _reviewBlock(
                                e.carescheduleinfo!.caredate!.first,
                                e.carescheduleinfo!.servicetype_text,
                                e.momdady_name,
                                lastDate: e.carescheduleinfo!.caredate!.last,
                                isCompleated: true,
                                result: e,
                              ))
                          .toList()
                      : [],
                  showNextLoading: data.reviewItem.lCompleatList.next.isNotEmpty,
                  onRefresh: () => _requestReview(1),
                  onNext: () => () => _requestNextList(data.reviewItem.lCompleatList, 1),
                ),
              ),
            ],
          ),
        ));
  }

  Widget _reviewBlock(String careDate, String careType, String name, {required String lastDate, bool isCompleated = false, required ReviewResultsData result}) {
    return lInkWell(
        onTap: () {
          ReviewViewRequest req = ReviewViewRequest(matching: result.matching, gubun: USER_TYPE.link_mom.index + 1);
          apiHelper.requestReviewView(req).then((response) async {
            if (response.getCode() == KEY_SUCCESS) {
              var ret =
                  await Commons.page(context, routeReviewSave, arguments: ReviewSavePage(viewType: isCompleated ? ViewType.view : ViewType.modify, result: isCompleated ? null : response.getData(), viewData: isCompleated ? response.getData() : null));
              if (!isCompleated && ret != null) {
                _requestGetList();
              }
            }
          });
        },
        child: Container(
          decoration: BoxDecoration(color: Colors.white, boxShadow: [BoxShadow(offset: Offset(0, 4), blurRadius: 12, color: Colors.black.withAlpha(11))]),
          margin: padding_10_B,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  padding: padding_20,
                  width: data.width(context, 0.85),
                  child: Row(mainAxisSize: MainAxisSize.max, crossAxisAlignment: CrossAxisAlignment.center, children: [
                    Expanded(flex: 1, child: lProfileAvatar(result.profileimg, radius: 25)),
                    sb_w_10,
                    Expanded(
                        flex: 4,
                        child: Column(
                          children: [
                            _desc("돌봄날짜".tr(), careDate == lastDate ? '${StringUtils.parseMDE(careDate)}' : '${StringUtils.parseMDE(careDate)} ~ ${StringUtils.parseMDE(lastDate)}'),
                            _desc("돌봄유형".tr(), careType),
                            _desc("아이정보".tr(), result.childinfo),
                          ],
                        )),
                  ])),
              lDivider(color: color_eeeeee),
              Container(
                padding: EdgeInsets.fromLTRB(40, 20, 40, 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    lText("${result.momdady_name} ${isCompleated ? "맘대디의후기작성이완료되었습니다".tr() : "맘대디의후기를작성해주세요".tr()}", style: st_14(textColor: color_545454, fontWeight: FontWeight.w500)),
                    Lcons(isCompleated ? Lcons.check().name : Lcons.nav_right().name, color: Commons.getColor(), size: 17, isEnabled: true)
                  ],
                ),
              )
            ],
          ),
        ));
  }

  Widget _desc(String title, String info) {
    return Container(
      child: Row(children: [lText(title, style: st_b_14(textColor: color_999999)), sb_w_08, lText(info, style: st_b_14())]),
    );
  }

  void _requestGetList() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      for (var i = 0; i < _controller!.length; i++) {
        if (i != 2) {
          _requestReview(i);
        } else {
          ReviewListRequest req = ReviewListRequest(gubun: USER_TYPE.link_mom.index + 1, type: i);
          apiHelper.requestReviewInfoList(req).then((response) {
            if (response.getCode() == KEY_SUCCESS) {
              data.reviewItem.lReceiveList = response.getData();
              data.reviewItem.lReceiveList.result.forEach((element) {
                _totalReviewCnt += element.count;
                log.d('review cnt = $_totalReviewCnt');
              });
              auth.user.my_info_data!.mypageInfoCnt!.reviewCntLinkmom = _totalReviewCnt;
            } else {
              data.reviewItem.lReceiveList = ReviewInfoResultData(result: []);
            }
            onUpDate();
          });
        }
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  void _requestReview(int index) {
    try {
      ReviewListRequest req = ReviewListRequest(gubun: USER_TYPE.link_mom.index + 1, type: index);
      apiHelper.requestReviewList(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          switch (index) {
            case 0:
              data.reviewItem.lWritableList = response.getData();
              break;
            case 1:
              data.reviewItem.lCompleatList = response.getData();
              break;
            default:
          }
        } else {
          switch (index) {
            case 0:
              data.reviewItem.lWritableList = ReviewListData(results: []);
              break;
            case 1:
              data.reviewItem.lCompleatList = ReviewListData(results: []);
              break;
            default:
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        log.e('_requestReview >>>>>>>>>>>> e');
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }

  Future<void> _requestNextList(ReviewListData list, int index) async {
    try {
      if (list.next.isEmpty) return;
      ReviewListRequest req = ReviewListRequest(gubun: USER_TYPE.link_mom.index + 1, type: index);
      apiHelper.requestReviewList(req, url: list.next).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          list.next = response.getData().next;
          list.previous = response.getData().previous;
          list.results!.addAll(response.getData().results!);
        }
        flag.disableLoading(fn: () => onUpDate());
        return data.blockList;
      });
    } catch (e) {
      log.i({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- error ': e,
      });
      return null;
    }
  }
}
