import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/worktime_allim_request.dart';
import 'package:linkmom/data/network/models/worktime_list_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

class WorkNotiPage extends BaseStateful {
  final DataManager? data;
  WorkNotiPage({this.data});

  @override
  _WorkNotiPageState createState() => _WorkNotiPageState();
}

class _WorkNotiPageState extends BaseStatefulState<WorkNotiPage> {
  String _next = '';
  String _previous = '';
  @override
  void initState() {
    super.initState();
    flag.enableLoading(fn: () => onUpDate());
    _requestWorkTimeList();
  }

  List<WorkNotiModel> _models = [];
  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
          appBar: appBar("출퇴근알림".tr(), hide: false),
          body: Padding(
            padding: padding_20,
            child: ListNextView(
              header: Padding(
                padding: padding_20_TB,
                child: lText("최근출퇴근알림목록입니다".tr(), style: st_14(textColor: color_545454)),
              ),
              view: _models.isNotEmpty ? _models.map((e) => _buildNotiItem(e)).toList() : [],
              onRefresh: () => _requestWorkTimeList(),
              onNext: () => _requestWorkTimeList(next: true),
              showNextLoading: _next.isNotEmpty,
            ),
          )),
    );
  }

  Widget _buildNotiItem(WorkNotiModel model) {
    DateTime start = StringUtils.parseYMDHM(model.notiData.startCaredate);
    DateTime end = StringUtils.parseYMDHM(model.notiData.endCaredate);
    CareStatus status = _getState(start, end);
    return Container(
        child: Column(children: [
      Container(
          padding: padding_20_TB,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              lTextBtn(status.name, bg: color_eeeeee, radius: 16, style: st_13(textColor: status == CareStatus.end ? color_999999 : Commons.getColor(), fontWeight: FontWeight.w700)),
              sb_h_10,
              Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                lText(model.notiData.childName),
                Container(height: 12, margin: padding_06_LR, child: lDividerVertical(color: color_dedede)),
                lText(StringUtils.parseMDE(model.notiData.startCaredate)),
                Container(height: 12, margin: padding_06_LR, child: lDividerVertical(color: color_dedede)),
                lText('${StringUtils.formatAHM(start)} ~ ${StringUtils.formatAHM(end)}'),
              ]),
              sb_h_10,
              Container(
                width: data.width(context, 1),
                child: Row(children: [
                  _notiBtn(model.checkedStart ? "출근".tr() : "출근알림".tr(), _getEnableState(start), model.checkedStart, model.notiData.scheduleId, 1),
                  _notiBtn(model.checkedEnd ? "퇴근".tr() : "퇴근알림".tr(), _getEnableState(end), model.checkedEnd, model.notiData.scheduleId, 2),
                ]),
              ),
            ],
          )),
      Divider(color: color_999999),
    ]));
  }

  bool _getEnableState(DateTime date) {
    return (DateTime.now().isBefore(date.add(Duration(minutes: 30))) && DateTime.now().isAfter(date.add(Duration(minutes: -30))));
  }

  Widget _notiBtn(String text, bool isEnabled, bool ended, int id, int type) {
    return Flexible(
        flex: 1,
        child: lInkWell(
          onTap: () {
            if (isEnabled) _requestWorkTimeAllim(id, type);
          },
          child: Container(
              margin: padding_03,
              padding: padding_15,
              width: double.infinity,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: isEnabled ? Commons.getColor() : color_eeeeee,
                borderRadius: BorderRadius.circular(8),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [if (ended) Lcons.check(color: Colors.white, disableColor: color_999999, size: 20, isEnabled: isEnabled), sb_w_05, lText(text, style: st_16(fontWeight: FontWeight.w700, textColor: isEnabled ? Colors.white : color_999999))],
              )),
        ));
  }

  void _requestWorkTimeList({bool next = false}) {
    try {
      if (next && _next.isEmpty) return;
      apiHelper.requestWorkTimeList(url: next ? _next : null).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          if (next) {
            _models.addAll(response.getData().results!.map((e) => WorkNotiModel(notiData: e, checkedStart: e.startAllim.isNotEmpty, checkedEnd: e.endAllim.isNotEmpty)).toList());
          } else {
            _models = response.getData().results!.map((e) => WorkNotiModel(notiData: e, checkedStart: e.startAllim.isNotEmpty, checkedEnd: e.endAllim.isNotEmpty)).toList();
          }
          _next = response.getData().next;
          _previous = response.getData().previous;
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestWorkTimeAllim(int id, int type) {
    try {
      apiHelper.requestWorkTimeAllim(WorkTimeAllimRequest(scheduleId: id, gubun: type)).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          if (type == 1) {
            _models.where((e) => e.notiData.scheduleId == id).first.checkedStart = true;
          } else {
            _models.where((e) => e.notiData.scheduleId == id).first.checkedEnd = true;
          }
        } else {
          showNormalDlg(context: context, message: response.getMsg());
        }
        onUpDate();
      });
    } catch (e) {
      onUpDate();
    }
  }

  CareStatus _getState(DateTime start, DateTime end) {
    return start.isAfter(DateTime.now()) && DateTime.now().isBefore(end)
        ? CareStatus.ready
        : start.isBefore(DateTime.now()) && DateTime.now().isBefore(end)
            ? CareStatus.inprogress
            : CareStatus.end;
  }
}

class WorkNotiModel {
  final WorkTimeResults notiData;
  bool checkedStart;
  bool checkedEnd;
  WorkNotiModel({required this.notiData, this.checkedStart = false, this.checkedEnd = false});
}
