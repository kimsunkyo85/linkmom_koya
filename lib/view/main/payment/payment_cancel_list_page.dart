import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/pay_cancel_info_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_info_response.dart';
import 'package:linkmom/data/network/models/pay_cancel_view_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/chat/chat_view/chat_care_info_view.dart';

import '../../../route_name.dart';
import '../../lcons.dart';
import 'model/service_data.dart';
import 'payment_cancel_page.dart';
import 'view/service_view.dart';

class PaymentCancelListPage extends BaseStateful {
  ///결제취소 요청 전 데이터
  final PayInfoRequest? payCancelInfoRequest;

  Function? callBack;

  PaymentCancelListPage({this.payCancelInfoRequest, this.callBack});

  @override
  PaymentCancelPageState createState() => PaymentCancelPageState(payCancelInfoRequest: payCancelInfoRequest, callBack: callBack);
}

class PaymentCancelPageState extends BaseStatefulState<PaymentCancelListPage> {
  ///결제취소 요청 전 데이터
  PayInfoRequest? payCancelInfoRequest;
  Function? callBack;

  PaymentCancelPageState({this.payCancelInfoRequest, this.callBack});

  PayInfoData payInfoData = PayInfoData.init();

  bool isMine = false;

  @override
  void initState() {
    super.initState();
    _requestPayCancelInfoView(payCancelInfoRequest!);
  }

  ///돌봄취소 내역보기
  _requestPayCancelInfoView(PayInfoRequest data) async {
    try {
      flag.enableLoading(fn: () => onUpDate());
      await apiHelper.reqeustPayCacelInfo(data).then((response) {
        //이미지 처리(수신 받은 url 추가)
        if (response.getCode() == KEY_SUCCESS) {
          onDataPage(response.getData());
        } else {
          Commons.showToast(response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> onSendMessage : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n$e');
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar(
          "취소내역".tr(),
          hide: false,
        ),
        context: context,
        body: lContainer(
          child: flag.isLoading
              ? Center()
              : payInfoData.cancel_info!.isEmpty
                  ? lEmptyView()
                  : Column(
                      children: [
                        // 서비스신청내역
                        Padding(
                          padding: padding_20,
                          child: careInfoView(
                            ServiceViewData.init(
                              isBtn: false,
                              servicetype: EnumUtils.getServiceTypeIndex(payInfoData.care_scheduleinfo!.servicetype),
                              caredate: payInfoData.care_scheduleinfo!.caredate,
                              caretime: [payInfoData.care_scheduleinfo!.stime, payInfoData.care_scheduleinfo!.etime],
                              cost_perhour: payInfoData.care_scheduleinfo!.cost_perhour,
                              cost_sum: payInfoData.care_scheduleinfo!.cost_sum,
                              durationtime: payInfoData.care_scheduleinfo!.durationtime,
                            ),
                          ),
                        ),

                        if (!flag.isLoading) lDivider(color: color_d4d4d4, thickness: 10.0),

                        Expanded(
                          child: Container(
                            color: color_eeeeee.withOpacity(0.32),
                            child: Padding(
                              padding: padding_15,
                              child: ListView.builder(
                                  itemCount: payInfoData.cancel_info!.length,
                                  physics: const AlwaysScrollableScrollPhysics(),
                                  itemBuilder: (BuildContext context, int index) {
                                    return _itemView(
                                      payInfoData.cancel_info![index],
                                    );
                                  }),
                            ),
                          ),
                        ),
                      ],
                    ),
        ),
      ),
      opacity: 0.0,
    );
  }

  @override
  void onDataPage(_data) {
    super.onDataPage(_data);
    payInfoData = _data;
    isMine = Commons.isCareIsMine(payInfoData.momdady!);
  }

  Widget _itemView(PayInfoCancelData data) {
    List<String> dates = [];
    dates.add(data.start_caredate);
    //데이터 포맷을 만들어서 세팅.
    if (data.other_schedule_cnt > 0) {
      for (int i = 0; i < data.other_schedule_cnt; i++) {
        dates.add(data.start_caredate);
      }
    }
    return InkWell(
      onTap: () {
        Commons.page(context, routePayMentCancel,
            arguments: PaymentCancelPage(
              viewType: PayViewType.cancel_view,
              payCancelViewRequest: PayCancelViewRequest(
                contract_id: payCancelInfoRequest!.contract_id,
                order_id: data.order_id,
                cancel_id: data.cancel_id,
              ),
            ));
      },
      child: Card(
        color: color_white,
        elevation: 0.1,
        semanticContainer: false,
        borderOnForeground: false,
        shape: lRoundedRectangleBorder(
          width: 0,
          disBorderColor: color_transparent,
          borderRadius: 8,
        ),
        margin: padding_15_B,
        child: Padding(
          padding: padding_20,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  lText('${data.is_refund ? "취소완료".tr() : "취소예정".tr()}', style: st_b_15(fontWeight: FontWeight.bold)),
                  Expanded(
                    child: lText(
                      StringUtils.getContractDate(data.is_refund ? data.revoked_at : data.cancel_request_date),
                      color: color_999999,
                      fontSize: 12,
                      textAlign: TextAlign.right,
                    ),
                  ),
                ],
              ),
              sb_h_05,
              chatCareSummaryInfoView([
                StringUtils.getCareDateCount(dates),
                StringUtils.getCareTime([payInfoData.care_scheduleinfo!.stime, payInfoData.care_scheduleinfo!.etime])
              ]),
              lDivider(color: color_eeeeee, padding: padding_10_TB),
              //금액,스티커
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      lText(
                        '${isMine ? "취소금액".tr() : "보상금액".tr()}',
                        fontSize: 15,
                        color: color_999999,
                        fontWeight: FontWeight.w500,
                      ),
                      sb_w_10,
                      Expanded(
                        child: Row(
                          children: [
                            Expanded(
                              child: lText(
                                '${StringUtils.formatPay(isMine ? data.request_cancel_price : data.linkmom_paid_price)}${"원".tr()}',
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                                textAlign: TextAlign.right,
                              ),
                            ),
                            // sb_w_10,
                            Lcons.nav_right(color: color_999999, size: 15),
                          ],
                        ),
                      ),
                    ],
                  ),
                  //내 신청서가(링크쌤)이 아니면 벌점 스티커는 보여주지 않는다.
                  if (!isMine)
                    Padding(
                      padding: padding_05_T,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          lText(
                            "벌점스티커".tr(),
                            fontSize: 15,
                            color: color_999999,
                            fontWeight: FontWeight.w500,
                          ),
                          sb_w_10,
                          Expanded(
                            child: Row(
                              children: [
                                Expanded(
                                  child: lText(
                                    '${StringUtils.formatPay(data.penalty_expect)}${"개".tr()}',
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    textAlign: TextAlign.right,
                                  ),
                                ),
                                // sb_w_10,
                                Lcons.nav_right(color: color_999999, size: 15),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  //취소사유
                  Padding(
                    padding: padding_15_T,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        lText(
                          '${EnumUtils.getCancelSelectUserType(data.cancelreason)} ${"취소".tr()}',
                          style: st_14(textColor: EnumUtils.getCancelSelectUserTypeColor(data.cancelreason), fontWeight: FontWeight.w500),
                        ),
                        //상대방 사유하고, 환불 완료이면 보여주지 않는다.
                        if (Commons.isExplain(EnumUtils.getCancelUserType(data.cancel_usertype), EnumUtils.getCancelSelectUserTypeBool(data.cancelreason)) && !data.is_refund)
                          Padding(
                            padding: padding_03_T,
                            child: lText(
                                StringUtils.validateString(data.cancelexplain_deadline)
                                    ? !Commons.isExpired(DateTime.parse(data.cancelexplain_deadline))
                                        ? '${StringUtils.payDeadline(data.cancelexplain_deadline, showMin: true)} ${"소명을진행해주세요".tr()}'
                                        : '${StringUtils.payDeadline(data.cancelexplain_deadline, showMin: true)} ${"소명시간이경과하였습니다".tr()}'
                                    : '',
                                style: st_13(textColor: color_ff3b30)),
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
