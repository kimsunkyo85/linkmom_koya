import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/pay_cancel_explain_request.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:provider/provider.dart';

import '../../../data/providers/image_loader_provider.dart';
import '../../../main.dart';
import '../../../route_name.dart';
import '../../image/image_add_view.dart';

///소명하기
class PaymentExplainPage extends BaseStateful {
  ///소명하기 요청 전 데이터
  final PayCancelExplainRequest? explainRequest;

  Function? callBack;

  PaymentExplainPage({this.explainRequest, this.callBack});

  @override
  PaymentExplainPageState createState() => PaymentExplainPageState(explainRequest: explainRequest, callBack: callBack);
}

class PaymentExplainPageState extends BaseStatefulState<PaymentExplainPage> {
  ///소명하기 요청 전 데이터
  PayCancelExplainRequest? explainRequest = PayCancelExplainRequest();
  Function? callBack;

  PaymentExplainPageState({this.explainRequest, this.callBack});

  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    context.read<ImageLoader>().loadImage([], context);
    onConfirmBtn();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar(
          "소명하기".tr(),
          hide: false,
        ),
        context: context,
        body: lContainer(
          child: Column(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Container(
                      child: Expanded(
                        child: lScrollView(
                          padding: padding_20,
                          child: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: padding_20_B,
                                  decoration: BoxDecoration(color: Colors.white, border: Border.all(color: color_dedede), borderRadius: BorderRadius.circular(6)),
                                  padding: padding_10,
                                  child: lScrollbar(
                                    child: TextField(
                                      controller: _controller,
                                      keyboardType: TextInputType.multiline,
                                      textInputAction: TextInputAction.done,
                                      minLines: 5,
                                      maxLines: 10,
                                      decoration: InputDecoration(
                                          contentPadding: padding_0,
                                          hintText: storageHelper.user_type.name + "취소소명하기_힌트".tr(),
                                          hintStyle: st_14(textColor: color_b2b2b2),
                                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                                          border: OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent))),
                                      maxLength: 1000,
                                      onChanged: (value) => onConfirmBtn(),
                                      buildCounter: (_, {required currentLength, required isFocused, maxLength}) => Padding(
                                        padding: padding_25_L,
                                        child: Container(
                                            alignment: Alignment.centerRight,
                                            child: lText(
                                              currentLength.toString() + "/" + StringUtils.formatPay(maxLength!),
                                              style: st_15(textColor: color_b2b2b2),
                                            )),
                                      ),
                                      onEditingComplete: () {
                                        onConfirmBtn();
                                        focusClear(context);
                                      },
                                    ),
                                  ),
                                ),
                                Container(
                                    padding: padding_05_TB,
                                    child: ChangeNotifierProvider<ImageLoader>(
                                      create: (_) => ImageLoader(),
                                      builder: (ctx, child) => Container(
                                        child: ImageAddView(
                                          max: 5,
                                          data: context.read<ImageLoader>().getImages,
                                          onItemClick: (image, index) {
                                            context.read<ImageLoader>().setImages = image;
                                            onConfirmBtn();
                                          },
                                        ),
                                      ),
                                    )),
                                Column(
                                  children: [
                                    sb_h_20,
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        lText('・'),
                                        Expanded(
                                          child: lText(Commons.isLinkMom() ? "소명하기_링크쌤2".tr() : "소명하기_맘대디2".tr(), color: color_545454),
                                          // child: TextHighlight(
                                          //   text: "취소날짜선택_안내_맘대디_1".tr(),
                                          //   term: "취소날짜선택_안내_맘대디_1_힌트".tr(),
                                          //   textStyleHighlight: st_b_14(textColor: Commons.getColor()),
                                          //   textStyle: st_b_14(textColor: color_545454),
                                          // ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    _confirmButton(),
                  ],
                ),
              ),
              // wdDivider(),
            ],
          ),
        ),
      ),
      opacity: 0.0,
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn("요청하기".tr(), isEnabled: flag.isConfirm, onClickAction: () async {
      // callBack!(PayCanceSaveExplainData()); //결제완료시 채팅으로 데이터 전달!!!
      // Commons.page(context, routePayMentExplainComplete, isPrevious: true,);
      showNormalDlg(
          message: "소명하기_안내".tr(),
          btnLeft: "아니오".tr(),
          btnRight: "예".tr(),
          onClickAction: (action) async {
            if (action == DialogAction.yes) {
              try {
                flag.enableLoading(fn: () => onUpDate());
                await apiHelper.reqeustPayCancelExplain(explainRequest!).then((response) {
                  if (response.getCode() == KEY_SUCCESS) {
                    if (callBack != null) {
                      callBack!(response.getData()); //결제완료시 채팅으로 데이터 전달!!!
                    }
                    Commons.page(
                      context,
                      routePayMentExplainComplete,
                      isPrevious: true,
                    );
                  } else {
                    Commons.showSnackBar(context, response.getMsg());
                  }
                  flag.disableLoading(fn: () => onUpDate());
                }).catchError((e) {
                  flag.disableLoading(fn: () => onUpDate());
                });
              } catch (e) {
                flag.disableLoading(fn: () => onUpDate());
              }
            }
          });
    });
  }

  @override
  void onConfirmBtn() {
    explainRequest!.cancelexplain_msg = _controller.text;
    explainRequest!.cancelexplain_img = context.read<ImageLoader>().getImages;
    if (StringUtils.validateString(_controller.text)) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }
}
