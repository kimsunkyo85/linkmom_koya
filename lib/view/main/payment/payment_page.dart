import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iamport_flutter/model/payment_data.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/pay_init_request.dart';
import 'package:linkmom/data/network/models/pay_init_response.dart';
import 'package:linkmom/data/network/models/pay_prev_request.dart';
import 'package:linkmom/data/providers/agreement_model.dart';
import 'package:linkmom/data/providers/model/agreement.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/label_radio.dart';
import 'package:linkmom/utils/manager.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/login/agreement/widgets/agreement_header.dart';
import 'package:linkmom/view/login/agreement/widgets/agreement_list.dart';
import 'package:linkmom/view/main/job_apply/job_profile/momdady_schedule_page.dart';
import 'package:linkmom/view/main/job_apply/title_toggle_icon_view.dart';
import 'package:linkmom/view/main/mypage/my_info/my_account_email_page.dart';
import 'package:linkmom/view/main/payment/model/service_data.dart';
import 'package:provider/provider.dart';

import 'model/peice_view_data.dart';
import 'payment_iamport_page.dart';
import 'view/price_view.dart';
import 'view/service_view.dart';

class PaymentPage extends BaseStateful {
  ///결제 전 데이터
  final PayInitRequest data;

  Function? callBack;

  PaymentPage({required this.data, this.callBack});

  @override
  PaymentPageState createState() => PaymentPageState(payRequestData: data, callBack: callBack);
}

class PaymentPageState extends BaseStatefulState<PaymentPage> {
  ///결제 전 데이터
  PayInitRequest payRequestData;
  Function? callBack;

  PaymentPageState({required this.payRequestData, this.callBack});

  late FocusNode _pointFNode, _cashFNode;
  Color _pointColor = color_cecece, _cashColor = color_cecece;

  List<PayType>? payTypes;
  PayType? _payType;
  AgreementModel? _agreementModel;
  bool isVisible = true;

  ///보유포인트, 보유캐시, 전액사용 높이
  double height = 45;

  bool isPoint = false;
  bool isCacsh = false;

  ///캐시,포인트 사전결제 요청 값
  PayRrevRequest payRrevRequest = PayRrevRequest();

  ///사전결제 요청 응답값
  PayInitData? payInitData = PayInitData.init();

  ///실제 아임포트 데이터
  PaymentData? payData = PaymentData(
    merchantUid: '',
    payMethod: '',
    amount: 0,
    appScheme: '',
    buyerTel: '',
  );

  ///요청후 플래그(요청하지 않고, 뒤로가기 할시 사용할 포인트 삭제)
  bool isRequest = false;

  @override
  void initState() {
    _pointFNode = FocusNode();
    _cashFNode = FocusNode();

    log.d('『GGUMBI』>>> initState payRequestData : : $payRequestData,  <<< ');
    String url = "http://ggumbi.com/";
    AgreementModel agreementModel = AgreementModel();
    agreementModel.createAgrrement(Agreement(title: "전체동의_결제".tr(), completed: false, isItem: false));
    agreementModel.addAgrrement(Agreement(title: "개인정보제3자".tr(), id: Terms.privacy, completed: false));
    agreementModel.addAgrrement(Agreement(title: "환불및취소약관".tr(), id: Terms.refund, completed: false));
    Provider.of<AgreementModel>(context, listen: false).createAgrrement(agreementModel.header);
    Provider.of<AgreementModel>(context, listen: false).addAll(agreementModel.lists);

    // children: _terms.map((e) => _menuComponent(e.title, movePage: () => Commons.page(context, routeCsTermsView, arguments: CsTermsViewPage(terms: e)))).toList(),

    payTypes = [
      PayType.card,
      // PayType.bank,
      PayType.pay,
    ];
    super.initState();
    _reqeustPayInit(payRequestData);
  }

  @override
  void dispose() {
    _pointFNode.dispose();
    _cashFNode.dispose();
    super.dispose();

    log.d('『GGUMBI』>>> dispose ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
  }

  @override
  onResume() {
    super.onResume();
  }

  @override
  onPaused() {
    super.onPaused();
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
  }

  Future<void> _reqeustPayInit(PayInitRequest data) async {
    try {
      flag.enableLoading(fn: () => onUpDate());
      await apiHelper.reqeustPayInit(data).then((response) {
        //이미지 처리(수신 받은 url 추가)
        if (response.getCode() == KEY_SUCCESS) {
          payInitData = response.getData();
        } else {
          if (response.getCode() == KEY_3700) {
            //2022/03/30 결제하기 페이지 이동시 이메일이 인증되지 않았으면, 인증하도록 화면으로 이동한다.
            showNormalDlg(
                message: response.getMsg(),
                onClickAction: (action) async {
                  var result = await Commons.page(context, routeMyAccountEmail, arguments: MyAccountEmailPage());
                  if (result == null || !result) {
                    Commons.pagePop(context);
                  } else {
                    _reqeustPayInit(payRequestData);
                  }
                });
          } else {
            Commons.showToast(response.getMsg());
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> onSendMessage : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    getPointCashCheck();
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar(
          "결제하기".tr(),
          hide: false,
          onBack: () {
            log.d('『GGUMBI』>>> build ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
            // 1. 캐시/포인트 사전결제 하기
            // 2. 취소시 캐시/포인트 사전결제 취소하기!
            apiHelper.reqeustPayRrevBack(payRrevRequest);
            Commons.pagePop(context);
            // //3. 결제취소 요청 전
            // apiHelper.reqeustPayCancelInit(PayCancelInitRequest(order_id: payInitData!.order_id!, contract_id: payInitData!.contract_id!));
          },
        ),
        context: context,
        body: lContainer(
          child: Column(
            children: [
              Expanded(
                child: lScrollView(
                  padding: padding_Item_TB,
                  child: Container(
                    child: Column(
                      children: [
                        //서비스신청내역
                        Padding(
                          padding: padding_20,
                          child: careInfoView(
                              ServiceViewData.init(
                                servicetype: payInitData!.bookingcareservices_info!.servicetype,
                                caredate: payInitData!.bookingcareservices_info!.caredate ?? [],
                                caretime: [payInitData!.bookingcareservices_info!.stime, payInitData!.bookingcareservices_info!.etime],
                                cost_perhour: payInitData!.bookingcareservices_info!.cost_perhour,
                                cost_sum: payInitData!.bookingcareservices_info!.cost_sum,
                                durationtime: payInitData!.bookingcareservices_info!.durationtime,
                              ), callBack: () {
                            Commons.page(navigatorKey.currentContext, routeMomdadySchedule,
                                arguments: MomdadySchedulePage(
                                  data: payInitData!.bookingcareservices_info!.caredate!,
                                  sTime: payInitData!.bookingcareservices_info!.stime,
                                  eTime: payInitData!.bookingcareservices_info!.etime,
                                ));
                          }),
                        ),
                        lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_20_TB),
                        //보유포인트
                        Padding(
                          padding: padding_Item_LR,
                          child: _pointView(),
                        ),
                        lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_20_TB),
                        //보유캐시
                        Padding(
                          padding: padding_Item_LR,
                          child: _cashView(),
                        ),
                        lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_20_TB),
                        //결제요금
                        Padding(
                          padding: padding_Item_LR,
                          child: priceView(
                            PriceViewData.init(
                              cost_sum: payInitData!.bookingcareservices_info!.cost_sum,
                              point: StringUtils.getStringToInt(data.tcPoint.text),
                              cache: StringUtils.getStringToInt(data.tcCache.text),
                            ),
                          ),
                        ),
                        lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_20_TB),
                        //결제방법
                        Padding(
                          padding: padding_Item_LR,
                          child: _paySelectView(),
                        ),
                        lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_20_TB),
                        //전체동의
                        Padding(
                          padding: padding_Item_LR,
                          child: _agreeView(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              _confirmButton(),
              // NOTE: enable after pg confirm.
              // if (Commons.isDebugMode) _companyInfo(),
              _companyInfo(),
            ],
          ),
        ),
      ),
      opacity: 0.0,
    );
  }

  ///PG사 신청 임의 화면
  Widget _companyInfo() {
    return Padding(
      padding: padding_20_LRB,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              lText("상호명:", color: color_545454, fontWeight: FontWeight.w500),
              lText("일상의감동  "),
              lText("070-5228-2023"),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              lText("대표자:", color: color_545454, fontWeight: FontWeight.w500),
              lText("최진희  "),
              lText("사업자번호:", color: color_545454, fontWeight: FontWeight.w500),
              lText("151-87-02063"),
            ],
          ),
          lAutoSizeText("경기도 용인시 수지구 광교호수로 359번길 8, 1층", style: st_14(textColor: color_545454, fontWeight: FontWeight.w500)),
        ],
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn("결제하기".tr(), isEnabled: flag.isConfirm, onClickAction: () async {
      //1. 캐시/포인트 사전결제 하기
      //2. 취소시 캐시/포인트 사전결제 취소하기!
      log.d('『GGUMBI』>>> 결제하기 - 결제화면 $payRrevRequest,  <<< ');
      if (payRrevRequest.used_point! > 0 || payRrevRequest.used_cash! > 0) {
        flag.enableLoading(fn: () => flag.disableConfirm(fn: () => onUpDate()));
        await apiHelper.reqeustPayRrevUsed(payRrevRequest).then((response) async {
          if (response.getCode() == KEY_SUCCESS) {
            log.d('『GGUMBI』>>> 결제하기 - 결제화면 1 $response,  <<< ');
            dynamic result = await Commons.page(context, routePayMentIamPort,
                arguments: PaymentIamportPage(
                  payInitData: payInitData,
                  payData: payData,
                  payRrevRequest: payRrevRequest,
                ));
            log.d('『GGUMBI』>>> 결제하기 - 결제화면 11 $result,  <<< ');
            if (result != null) {
              callBack!(payInitData!.order_id!);
            }
          } else {
            //2021/11/02 포인트,캐시등 먼저 차감후 오류시 안내후 다시 결제를 하도록 한다.
            if (response.getCode() == KEY_6001 || response.getCode() == KEY_6002 || response.getCode() == KEY_6003) {
              _reqeustPayInit(payRequestData).then((value) {
                data.tcPoint.text = '';
                data.tcCache.text = '';
                showNormalDlg(message: response.getMsg());
              });
            } else {
              Commons.showSnackBar(context, response.getMsg());
            }
          }
          flag.disableLoading(fn: () => flag.enableConfirm(fn: () => onUpDate()));
        }).catchError((e) {
          flag.disableLoading(fn: () => flag.enableConfirm(fn: () => onUpDate()));
        });
      } else {
        dynamic result = await Commons.page(context, routePayMentIamPort,
            arguments: PaymentIamportPage(
              payInitData: payInitData,
              payData: payData,
              payRrevRequest: payRrevRequest,
            ));
        if (result != null) {
          callBack!(payInitData!.order_id!);
        }
      }
    });
  }

  @override
  void onConfirmBtn() {
    //결제방법선택
    bool isPayType = false;
    if (_payType != null && _payType != PayType.free) {
      isPayType = true;
    }
    //결제금액이 0원이면 결제수단 선택을 하지 않아도 바로 결제로 넘어간다.
    bool isFree = isPayAmount();
    if (isFree) {
      isPayType = true;
      _payType = PayType.free;
      /* }else{
      _payType = null;*/
    }

    bool isAgree = _agreementCheck(_agreementModel!);
    log.d('『GGUMBI』>>> onConfirmBtn : isPayType: $isPayType,  isAgree: $isAgree ,isFree: $isFree, $_payType <<< ');
    if (isPayType && isAgree) {
      int price = calcuPrice(
        payInitData!.bookingcareservices_info!.cost_sum,
        StringUtils.getStringToInt(data.tcPoint.text),
        StringUtils.getStringToInt(data.tcCache.text),
      );
      int taxFree = 0;
      int vat = 0;
      //카드 : 면세금액 95%
      //카카오 간편결제 : 100% 결제금액, 면세금액 0% -> 2022/03/04 카카오페이도 동일하게 적용
      //계좌이체 : 면세금액 95%
      //부가세 : (전체금액 - 면세금액) * 0.10 (10%)
      log.d('『GGUMBI』>>> onConfirmBtn : : ${(price * 0.95).round()}, ${(price * 0.95)} <<< ');
      double value = (price * 0.95);
      taxFree = value.toInt();
      value = (price - taxFree) * 0.10;
      vat = value.round();
      log.d('『GGUMBI』>>> onConfirmBtn : price: $price, taxFree: $taxFree,vat: $vat, <<< ');
      payData = PaymentData(
        //pg사
        pg: EnumUtils.getPayType(_payType!.index).payPg,
        //결제수단
        payMethod: EnumUtils.getPayType(_payType!.index).payMethod,
        //가상계좌입금기한
        vbankDue: payInitData!.paid_deadline,
        //사업자번호
        bizNum: "151-87-02063",
        //주문명
        name: payInitData!.bookingcareservices_info!.product_name,
        //결제금액
        amount: price,
        //면제금액
        taxFree: taxFree,
        //부가가치세
        vat: vat,
        //주문번호
        merchantUid: payInitData!.order_id!,
        //앱 스키마
        appScheme: KEY_APP_SCHEME,
        //사용자 이름
        buyerName: payInitData!.user_info!.first_name,
        //구매자전화번호
        buyerTel: payInitData!.user_info!.hpnumber.toString(),
        //구매자이메일
        buyerEmail: payInitData!.user_info!.email,
        escrow: false,
      );
      payData!.displayCardQuota = [1, 2, 3];

      log.d('『GGUMBI』>>> onConfirmBtn : : ${payData!.pg}, ${payData!.payMethod}, $_payType, ${EnumUtils.getPayType(_payType!.index).payMethod}  <<< ');
      //포인트 사용 유무
      payRrevRequest = PayRrevRequest(
        order_id: payData!.merchantUid,
        total_fee: payData!.amount as int,
        used_point: StringUtils.getStringToInt(data.tcPoint.text),
        used_cash: StringUtils.getStringToInt(data.tcCache.text),
      );
      payInitData!.used_point = payRrevRequest.used_point.toString();
      payInitData!.used_cash = payRrevRequest.used_cash.toString();

      // data: PaymentData.fromJson({
      //   'pg': 'html5_inicis',                                          // PG사
      //   'payMethod': 'card',                                           // 결제수단
      //   'name': '아임포트 결제데이터 분석',                                  // 주문명
      //   'merchantUid': 'mid_${DateTime.now().millisecondsSinceEpoch}', // 주문번호
      //   'amount': 39000,                                               // 결제금액
      //   'buyerName': '홍길동',                                           // 구매자 이름
      //   'buyerTel': '01012345678',                                     // 구매자 연락처
      //   'buyerEmail': 'example@naver.com',                             // 구매자 이메일
      //   'buyerAddr': '서울시 강남구 신사동 661-16',                         // 구매자 주소
      //   'buyerPostcode': '06018',                                      // 구매자 우편번호
      //   'appScheme': 'example',                                        // 앱 URL scheme
      //   'display' : {
      //     'cardQuota' : [2,3]                                            //결제창 UI 내 할부개월수 제한
      //   }
      // }),
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  ///보유포인트
  Widget _pointView() {
    return Container(
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              lPriceText(
                isPoint: true,
                isString: false,
                title: "보유포인트".tr(),
                titleFontSize: 18,
                titleFontWeight: FontWeight.bold,
                value: payInitData!.point_total!,
                valueFontSize: 16,
                valueFontWeight: FontWeight.bold,
                valueColor: color_f0544c,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 2,
                    child: SizedBox(
                      height: height,
                      child: Form(
                        key: data.formPoint,
                        child: Focus(
                          onFocusChange: (state) {
                            if (state) {
                              _pointColor = Commons.getColor();
                            } else {
                              _pointColor = color_b2b2b2;
                              String check = StringUtils.validatePoint(data.tcPoint.text, payInitData!.point_total!.toString());

                              if (check.isNotEmpty) {
                                Commons.showToast(check);
                                // } else {
                                //   isPointCashCheck(data.tcPoint.text);
                              }
                              //최종결재금액보다 포인트가 많을 경우 먼저 계산한다.
                              if (StringUtils.getStringToInt(data.tcPoint.text) >= payInitData!.bookingcareservices_info!.cost_sum) {
                                data.tcPoint.text = StringUtils.formatPay(payInitData!.bookingcareservices_info!.cost_sum - StringUtils.getStringToInt(data.tcCache.text));
                              }
                            }

                            if (StringUtils.getStringToInt(data.tcPoint.text) > 0) data.tcPoint.text = StringUtils.formatPay(StringUtils.getStringToInt(data.tcPoint.text));
                            if (!state) data.formPoint.currentState!.validate();
                            onUpDate();
                          },
                          child: TextFormField(
                            focusNode: _pointFNode,
                            cursorColor: Commons.getColor(),
                            key: Key(KEY_ID),
                            controller: data.tcPoint,
                            // validator: (value) => StringUtils.validatePoint(value, payInitData!.point_total!.toString()),
                            style: st_b_16(fontWeight: FontWeight.w500),
                            inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                            textInputAction: TextInputAction.next,
                            keyboardType: TextInputType.number,
                            enabled: payData!.amount != 0 ? true : false,
                            readOnly: payData!.amount != 0 ? false : true,
                            decoration: lineDecorationExceptHint(
                              '0',
                              () {
                                data.tcPoint.clear();
                                onUpDate();
                              },
                              _pointColor,
                              // _pointFNode.hasFocus,
                              true,
                              floatingLabelBehavior: FloatingLabelBehavior.never,
                              contentPadding: padding_10_LR,
                              radius: 8,
                            ),
                            onSaved: (val) => data.point = val!,
                            onEditingComplete: () {
                              focusNextTextField(context);
                              data.formPoint.currentState!.validate();
                              String check = StringUtils.validatePoint(data.tcPoint.text, payInitData!.point_total!.toString());
                              if (check.isNotEmpty) {
                                Commons.showToast(check);
                                // } else {
                                //   isPointCashCheck(data.tcPoint.text);
                              }
                            },
                            // onChanged: (value) => isPointCashCheck(value),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: lBtn(
                      _checkString(StringUtils.getStringToInt(data.tcPoint.text), payInitData!.point_total!, payInitData!.bookingcareservices_info!.cost_sum),
                      margin: padding_10_L,
                      btnColor: color_eeeeee,
                      height: height,
                      style: st_b_14(),
                      borderRadius: 8,
                      onClickAction: () {
                        if (_checkString(StringUtils.getStringToInt(data.tcPoint.text), payInitData!.point_total!, payInitData!.bookingcareservices_info!.cost_sum) == "사용안함".tr()) {
                          data.tcPoint.text = '';
                        } else {
                          //결제 금액과 남은 결제 금액이 다를 경우만 사용할 수 있다.
                          if (payData!.amount != 0) {
                            //최종결재금액보다 포인트가 많을 경우 먼저 계산한다.
                            if (payInitData!.point_total! >= payInitData!.bookingcareservices_info!.cost_sum) {
                              data.tcPoint.text = StringUtils.formatPay(payInitData!.bookingcareservices_info!.cost_sum - StringUtils.getStringToInt(data.tcCache.text));
                            } else {
                              data.tcPoint.text = StringUtils.formatPay(payInitData!.point_total!);
                            }
                            payData!.amount = calcuPrice(
                              payInitData!.bookingcareservices_info!.cost_sum,
                              StringUtils.getStringToInt(data.tcPoint.text),
                              StringUtils.getStringToInt(data.tcCache.text),
                            );
                          }
                        }
                        onUpDate();
                      },
                    ),
                  ),
                ],
              ),
              sb_h_10,
              lText("포인트사용안내".tr(), style: st_12(textColor: color_999999)),
            ],
          ),
          sb_h_10,
        ],
      ),
    );
  }

  ///보유캐시
  Widget _cashView() {
    return Container(
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              lPriceText(
                isCache: true,
                isString: false,
                title: "보유캐시".tr(),
                titleFontSize: 18,
                titleFontWeight: FontWeight.bold,
                value: payInitData!.cache_total!,
                valueFontSize: 16,
                valueFontWeight: FontWeight.bold,
                valueColor: color_f0544c,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 2,
                    child: SizedBox(
                      height: height,
                      child: Form(
                        key: data.formCache,
                        child: Focus(
                          onFocusChange: (state) {
                            if (state) {
                              _cashColor = Commons.getColor();
                            } else {
                              _cashColor = color_b2b2b2;
                              String check = StringUtils.validateCash(data.tcCache.text, payInitData!.cache_total!.toString());
                              if (check.isNotEmpty) {
                                Commons.showToast(check);
                                // } else {
                                //   isPointCashCheck(data.tcCache.text);
                              }
                              //최종결재금액보다 포인트가 많을 경우 먼저 계산한다.
                              if (StringUtils.getStringToInt(data.tcCache.text) >= payInitData!.bookingcareservices_info!.cost_sum) {
                                data.tcCache.text = StringUtils.formatPay(payInitData!.bookingcareservices_info!.cost_sum - StringUtils.getStringToInt(data.tcPoint.text));
                              }

                              // if (StringUtils.getStringToInt(data.tcCache.text) >= payInitData!.cache_total!) {
                              //   data.tcCache.text = StringUtils.formatPay(payInitData!.cache_total!);
                              // }
                            }

                            if (StringUtils.getStringToInt(data.tcCache.text) > 0) data.tcCache.text = StringUtils.formatPay(StringUtils.getStringToInt(data.tcCache.text));
                            if (!state) data.formCache.currentState!.validate();
                            onUpDate();
                          },
                          child: TextFormField(
                            focusNode: _cashFNode,
                            cursorColor: Commons.getColor(),
                            key: Key(KEY_PASSWORD),
                            controller: data.tcCache,
                            // validator: (value) => StringUtils.validatePoint(value, payInitData!.point_total!.toString()),
                            style: st_b_16(fontWeight: FontWeight.w500),
                            inputFormatters: <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly],
                            textInputAction: TextInputAction.done,
                            keyboardType: TextInputType.number,
                            enabled: payData!.amount != 0 ? true : false,
                            // readOnly: payData!.amount != 0 ? false : true,
                            decoration: lineDecorationExceptHint(
                              '0',
                              () {
                                data.tcCache.clear();
                                onUpDate();
                              },
                              _cashColor,
                              // _cashFNode.hasFocus,
                              true,
                              floatingLabelBehavior: FloatingLabelBehavior.never,
                              contentPadding: padding_10_LR,
                              radius: 8,
                            ),
                            onSaved: (val) => data.cash = val!,
                            onEditingComplete: () {
                              // focusNextTextField(context);
                              focusClear(context);
                              data.formCache.currentState!.validate();
                              String check = StringUtils.validateCash(data.tcCache.text, payInitData!.cache_total!.toString());
                              if (check.isNotEmpty) {
                                Commons.showToast(check);
                                // } else {
                                //   isPointCashCheck(data.tcCache.text);
                              }
                            },
                            // onChanged: (value) =>  isPointCashCheck(value),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: lBtn(
                      _checkString(StringUtils.getStringToInt(data.tcCache.text), payInitData!.cache_total!, payInitData!.bookingcareservices_info!.cost_sum),
                      margin: padding_10_L,
                      btnColor: color_eeeeee,
                      height: height,
                      style: st_b_14(),
                      borderRadius: 8,
                      onClickAction: () {
                        if (_checkString(StringUtils.getStringToInt(data.tcCache.text), payInitData!.cache_total!, payInitData!.bookingcareservices_info!.cost_sum) == "사용안함".tr()) {
                          data.tcCache.text = '';
                        } else {
                          if (payData!.amount != 0) {
                            //최종결재금액보다 포인트가 많을 경우 먼저 계산한다.
                            if (payInitData!.cache_total! >= payInitData!.bookingcareservices_info!.cost_sum) {
                              data.tcCache.text = StringUtils.formatPay(payInitData!.bookingcareservices_info!.cost_sum - StringUtils.getStringToInt(data.tcPoint.text));
                            } else {
                              data.tcCache.text = StringUtils.formatPay(payInitData!.cache_total!);
                            }
                            log.d('『GGUMBI』>>> _pointView : payData!.amount 2 : ${payData!.amount}, ${payInitData!.bookingcareservices_info!.cost_sum} <<< ');
                            payData!.amount = calcuPrice(
                              payInitData!.bookingcareservices_info!.cost_sum,
                              StringUtils.getStringToInt(data.tcPoint.text),
                              StringUtils.getStringToInt(data.tcCache.text),
                            );
                          }
                        }
                        onUpDate();
                      },
                    ),
                  ),
                ],
              ),
              sb_h_10,
              lText("캐시사용안내".tr(), style: st_12(textColor: color_999999)),
            ],
          ),
          sb_h_10,
        ],
      ),
    );
  }

  ///결제방법
  Widget _paySelectView() {
    log.d('『GGUMBI』>>> _paySelectView : _payType: $_payType,  <<< ');
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          titleToggleIconView(
            data,
            "결제방법".tr(),
            isInfo: false,
            rightWidget: Center(),
          ),
          sb_h_10,
          Column(
            children: payTypes!
                .map((value) => Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            LabeledRadio(
                              toggleable: !isPayAmount(),
                              label: value.string,
                              padding: padding_10_B,
                              value: value,
                              groupValue: _payType,
                              isLine: true,
                              style: st_b_15(isSelect: isPayAmount(), textColor: color_222222, disableColor: color_eeeeee),
                              onChanged: (PayType? newValue) {
                                _payType = newValue!;
                                onConfirmBtn();
                              },
                            ),
                          ],
                        ),
                      ],
                    ))
                .toList(),
          ),
        ],
      ),
    );
  }

  ///전체동의
  Widget _agreeView() {
    _agreementModel = Provider.of<AgreementModel>(context, listen: true);
    log.d('『GGUMBI』>>> _agreeView : _agreementModel: $_agreementModel,  <<< ');
    _agreementCheck(_agreementModel!);
    onConfirmBtn();
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Consumer<AgreementModel>(
              builder: (context, value, child) => AgreementHeader(
                    agreement: value.header,
                    contentMargin: padding_10_TB,
                    isVisible: true,
                    callBack: (isVisible) {
                      this.isVisible = isVisible;
                      onUpDate();
                    },
                  )),
          lDivider(color: color_eeeeee, padding: padding_15_TB),
          Visibility(
            maintainState: true,
            maintainAnimation: true,
            maintainSize: true,
            visible: isVisible,
            child: Container(
              child: Consumer<AgreementModel>(
                builder: (context, agreement, child) => Wrap(
                  children: [
                    AgreementList(
                      lists: agreement.lists,
                      contentPadding: padding_0,
                      itemExtent: 40,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  getPointCashCheck() {
    payData!.amount = calcuPrice(
      //2021/10/15 100단위로 없애고 1원단위 까지 사용할 수 있음
      payInitData!.bookingcareservices_info!.cost_sum,
      StringUtils.getStringToInt(data.tcPoint.text),
      StringUtils.getStringToInt(data.tcCache.text),
    );
    log.d('『GGUMBI』>>> getPointCashCheck : : ${payData!.amount},  <<< ');
  }

  bool isPointCashCheck(String value) {
    log.d('『GGUMBI』>>> isPointCashCheck : calcu % 100: ${(int.parse(value) % 100)}, $value <<< ');
    int calcu = int.parse(value);
    if (calcu % 100 == 0) {
      payData!.amount = calcuPrice(
        payInitData!.bookingcareservices_info!.cost_sum,
        StringUtils.getStringToInt(data.tcPoint.text),
        StringUtils.getStringToInt(data.tcCache.text),
      );
      return true;
    }
    Commons.showToast("100원단위로입력해주세요.");
    return false;
  }

  ///포인트, 캐시 (최대값과 같을 경우)
  String _checkString(int currentValue, int max, int price) {
    if (currentValue == max || currentValue > 0) {
      return "사용안함".tr();
    }
    return "전액사용".tr();
  }

  ///동의 선택
  bool _agreementCheck(AgreementModel data) {
    bool isCheck = false;
    int lehgnt = data.lists.length;
    for (int i = 0; i < lehgnt; i++) {
      log.d('『GGUMBI』>>> _agreementCheck : ${data.lists[i].title}, ${data.lists[i].completed},  <<< ');
      if (!data.lists[i].completed) {
        isCheck = false;
        break;
      }
      isCheck = true;
    }
    log.d('『GGUMBI』>>> _agreementCheck : isCheck: $isCheck,  <<< ');
    return isCheck;
  }

  ///신청금액 - 포인트 - 캐시 = 최종결제금액
  int calcuPrice(int costSum, int point, int cache) {
    log.d('『GGUMBI』>>> calcuPrice : cost_sum: $costSum, point: $point,cache: $cache, <<< ');
    return costSum - point - cache;
  }

  bool isPayAmount() {
    log.d('『GGUMBI』>>> isPayAmount : {payData!.amount}: ${payData!.amount},  <<< ');
    return payData!.amount == 0 ? true : false;
  }
}
