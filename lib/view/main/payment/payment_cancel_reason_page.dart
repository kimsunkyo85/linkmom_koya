import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/pay_cancel_init_request.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/label_radio.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/main/job_apply/title_toggle_icon_view.dart';
import 'package:provider/provider.dart';

import '../../../data/providers/image_loader_provider.dart';
import '../../../main.dart';
import '../../../route_name.dart';
import '../../image/image_add_view.dart';
import 'payment_cancel_page.dart';

///취소사유 선택
class PaymentCancelReasonPage extends BaseStateful {
  ///결제취소 요청 전 데이터
  final PayCancelInitRequest? payCancelInitRequest;

  final List<File>? images;

  ViewMode? viewMode;

  Function? callBack;

  bool? isChange = false;

  PaymentCancelReasonPage({this.payCancelInitRequest, this.images, this.isChange, this.viewMode, this.callBack});

  @override
  PaymentCancelReasonPageState createState() => PaymentCancelReasonPageState(payCancelInitRequest: payCancelInitRequest, images: images, viewMode: viewMode, callBack: callBack);
}

class PaymentCancelReasonPageState extends BaseStatefulState<PaymentCancelReasonPage> {
  ///결제취소 요청 전 데이터
  PayCancelInitRequest? payCancelInitRequest = PayCancelInitRequest();
  List<File>? images = [];
  ViewMode? viewMode = ViewMode();
  Function? callBack;

  PaymentCancelReasonPageState({this.payCancelInitRequest, this.images, this.viewMode, this.callBack});

  List<CancelType>? cancelTypes;

  CancelType? cancelType;

  TextEditingController _controller = TextEditingController();

  bool isChang = false;

  @override
  void initState() {
    super.initState();
    log.d('『GGUMBI』>>> initState : payCancelInitRequest: $payCancelInitRequest,  <<< ');
    log.d('『GGUMBI』>>> initState : images: $images`,  <<< ');
    log.d('『GGUMBI』>>> initState : viewMode: $viewMode,  <<< ');
    if (Commons.isLinkMom()) {
      cancelTypes = [
        CancelType.linkmom_21,
        CancelType.linkmom_22,
        CancelType.linkmom_23,
        CancelType.linkmom_24,
        CancelType.linkmom_29,
        CancelType.momdady_16,
        CancelType.momdady_15,
        CancelType.momdady_13,
        CancelType.momdady_19,
      ];
    } else {
      cancelTypes = [
        CancelType.momdady_11,
        CancelType.momdady_12,
        CancelType.momdady_13,
        CancelType.momdady_14,
        CancelType.momdady_19,
        CancelType.linkmom_26,
        CancelType.linkmom_25,
        CancelType.linkmom_23,
        CancelType.linkmom_29,
      ];
    }
    context.read<ImageLoader>().loadImage([], context);
    onDataPage(payCancelInitRequest);
  }

  @override
  void onDataPage(_data) {
    if (images == null || widget.images == null) {
      images = [];
    } else {
      images = widget.images;
      context.read<ImageLoader>().setImages = images!;
    }
    if (_data is PayCancelInitRequest) {
      if (payCancelInitRequest != null) {
        log.d('『GGUMBI』>>> onDataPage : payCancelInitRequest: $_data,  <<< ');
        if (_data.cancelreason! > 0) {
          cancelType = EnumUtils.getCancelSelect(_data.cancelreason!);
        }
        _controller.text = _data.cancelreason_msg!;
      }
      onConfirmBtn();
    }
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar(
          "돌봄취소".tr(),
          hide: false,
          rightWidget: InkWell(
            onTap: () {
              Commons.page(context, routePayMentRefundRule);
            },
            child: Container(
              alignment: Alignment.center,
              padding: padding_20_LR,
              child: lText("환불규정".tr(), color: Commons.getColor(), fontWeight: FontWeight.w500),
            ),
          ),
        ),
        context: context,
        body: lContainer(
          child: Column(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Expanded(
                      child: lScrollView(
                        padding: padding_20,
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              //취소 사유 내역
                              Padding(
                                padding: padding_10_TB,
                                child: titleToggleIconView(null, "취소사유를선택해주세요".tr(), isInfo: true, infoColor: color_cecece, rightWidget: Center(), flex: 0, onClick: (action) {
                                  showNormalDlg(
                                    title: "취소사유".tr(),
                                    messageWidget: Column(
                                      children: [
                                        TextHighlight(
                                          text: Commons.isLinkMom() ? "취소사유선택_링크쌤_안내1".tr() : "취소사유선택_맘대디_안내1".tr(),
                                          term: Commons.isLinkMom() ? "링크쌤(본인)".tr() : "맘대디(본인)".tr(),
                                          textStyle: st_16(textColor: color_545454),
                                          textStyleHighlight: st_16(textColor: color_545454, fontWeight: FontWeight.bold),
                                        ),
                                        sb_h_10,
                                        TextHighlight(
                                          text: Commons.isLinkMom() ? "취소사유선택_링크쌤_안내2".tr() : "취소사유선택_맘대디_안내2".tr(),
                                          term: Commons.isLinkMom() ? "맘대디귀책사유".tr() : "링크쌤귀책사유".tr(),
                                          textStyle: st_16(textColor: color_545454),
                                          textStyleHighlight: st_16(textColor: color_545454, fontWeight: FontWeight.bold),
                                        ),
                                        sb_h_10,
                                        lText(
                                          Commons.isLinkMom() ? "취소사유선택_링크쌤_안내3".tr() : "취소사유선택_맘대디_안내3".tr(),
                                          style: st_16(textColor: color_545454, fontWeight: FontWeight.w500),
                                        ),
                                      ],
                                    ),
                                    paddingTitle: EdgeInsets.fromLTRB(10, 30, 10, 10),
                                    padding: EdgeInsets.fromLTRB(10, 10, 10, 20),
                                    titleAlign: Alignment.centerLeft,
                                  );
                                }),
                              ),

                              Column(
                                children: cancelTypes!
                                    .map((e) => Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            if (Commons.isLinkMom() && e == CancelType.linkmom_21 || !Commons.isLinkMom() && e == CancelType.linkmom_26)
                                              Padding(
                                                padding: padding_10_TB,
                                                child: lText("링크쌤측사유".tr(),
                                                    style: st_b_16(
                                                      textColor: color_linkmom,
                                                      fontWeight: FontWeight.bold,
                                                    )),
                                              ),

                                            //맘대디 모드일때.
                                            if (!Commons.isLinkMom() && e == CancelType.momdady_11 || Commons.isLinkMom() && e == CancelType.momdady_16)
                                              Padding(
                                                padding: padding_10_TB,
                                                child: lText("맘대디측사유".tr(),
                                                    style: st_b_16(
                                                      textColor: color_momdady,
                                                      fontWeight: FontWeight.bold,
                                                    )),
                                              ),
                                            Row(
                                              children: [
                                                LabeledRadio(
                                                  label: e.string,
                                                  padding: padding_0,
                                                  value: e,
                                                  groupValue: cancelType,
                                                  isLine: true,
                                                  style: st_b_15(),
                                                  radioColor: getColorRadio(e),
                                                  onChanged: (CancelType? newValue) {
                                                    //채팅에서 바로 들어온 경우
                                                    if (cancelType == null || viewMode!.viewType == ViewType.apply) {
                                                      cancelType = newValue;
                                                      onConfirmBtn();
                                                      return;
                                                    }

                                                    //맘대디 또는 링크쌤 사유로 변경시 알림
                                                    isChang = isChange(cancelType!, newValue!);
                                                    log.d('『GGUMBI』>>> build : isChange: $isChang, cancelType: $cancelType,newValue: $newValue, <<< ');
                                                    if (isChang) {
                                                      showNormalDlg(
                                                          title: "취소사유변경".tr(),
                                                          message: "취소사유변경_안내".tr(),
                                                          padding: padding_10,
                                                          btnLeft: "아니오".tr(),
                                                          btnRight: "예".tr(),
                                                          onClickAction: (action) {
                                                            switch (action) {
                                                              case DialogAction.yes:
                                                                cancelType = newValue;
                                                                onConfirmBtn();
                                                                break;
                                                              default:
                                                            }
                                                          });
                                                    } else {
                                                      cancelType = newValue;
                                                      onConfirmBtn();
                                                    }
                                                  },
                                                ),
                                              ],
                                            ),

                                            if (!Commons.isLinkMom() && e == CancelType.momdady_19 || Commons.isLinkMom() && e == CancelType.linkmom_29) lDivider(padding: padding_20_TB, color: color_dedede),
                                          ],
                                        ))
                                    .toList(),
                              ),

                              Padding(
                                padding: padding_20_TB,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    lText('・ ', style: st_b_14(textColor: Commons.getColor())),
                                    Expanded(
                                      child: lText(
                                        Commons.isLinkMom() ? "취소사유_안내_링크쌤힌트".tr() : "취소사유_안내_맘대디힌트".tr(),
                                        style: st_b_14(textColor: Commons.getColor()),
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                              Container(
                                margin: padding_20_B,
                                decoration: BoxDecoration(color: Colors.white, border: Border.all(color: color_dedede), borderRadius: BorderRadius.circular(6)),
                                padding: padding_10,
                                child: TextField(
                                  controller: _controller,
                                  keyboardType: TextInputType.multiline,
                                  textInputAction: TextInputAction.done,
                                  maxLines: 4,
                                  decoration: InputDecoration(
                                      hintMaxLines: 4,
                                      // for hint overflow
                                      contentPadding: padding_0,
                                      hintText: Commons.isLinkMom() ? "취소사유_링크쌤_안내".tr() : "취소사유_맘대디_안내".tr(),
                                      hintStyle: st_14(textColor: color_b2b2b2),
                                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                                      border: OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent))),
                                  maxLength: 1000,
                                  onChanged: (value) => onUpDate(),
                                  buildCounter: (_, {required currentLength, required isFocused, maxLength}) => Padding(
                                    padding: padding_25_L,
                                    child: Container(
                                        alignment: Alignment.centerRight,
                                        child: lText(
                                          currentLength.toString() + "/" + StringUtils.formatPay(maxLength!),
                                          style: st_15(textColor: color_b2b2b2),
                                        )),
                                  ),
                                ),
                              ),
                              Container(
                                  padding: padding_05_TB,
                                  child: ChangeNotifierProvider<ImageLoader>(
                                    create: (_) => ImageLoader(),
                                    builder: (ctx, child) => Container(
                                      child: ImageAddView(
                                        max: 5,
                                        data: context.read<ImageLoader>().getImages,
                                        onItemClick: (image, index) {
                                          context.read<ImageLoader>().setImages = image;
                                        },
                                      ),
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ),
                    ),
                    _confirmButton(),
                  ],
                ),
              ),
              // wdDivider(),
            ],
          ),
        ),
      ),
      opacity: 0.0,
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn(viewMode!.viewType == ViewType.apply ? "다음".tr() : "저장".tr(), isEnabled: flag.isConfirm, onClickAction: () async {
      onConfirmBtn();
      if (isCancelAlert(cancelType!)) {
        showCancelAlert((action) {
          switch (action) {
            case DialogAction.yes:
              goPage();
              break;
          }
        });
      } else {
        goPage();
      }
    });
  }

  goPage() async {
    images = context.read<ImageLoader>().getImages;
    if (viewMode!.viewType == ViewType.apply) {
      var data = await Commons.page(context, routePayMentCancel,
          arguments: PaymentCancelPage(
            payCancelInitRequest: payCancelInitRequest,
            images: images,
            callBack: (payCancelSaveRequest) {
              if (callBack != null) {
                callBack!(payCancelSaveRequest);
              }
              // Commons.pagePop(context);
            },
          ));
      if (data != null) {
        var action = data as DialogAction;
        if (action == DialogAction.finish) {
          Commons.pagePop(context);
        }
      }
    } else {
      Commons.pagePop(context,
          data: PaymentCancelReasonPage(
            payCancelInitRequest: payCancelInitRequest,
            images: images,
            isChange: isChang,
          ));
    }
  }

  @override
  void onConfirmBtn() {
    // 취소사유선택
    bool isCancelType = false;
    if (cancelType != null) {
      isCancelType = true;
      payCancelInitRequest!.cancelreason = cancelType!.value;
    }

    //사유 설명
    payCancelInitRequest!.cancelreason_msg = _controller.text;

    if (isCancelType) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  Color getColorRadio(CancelType type) {
    Color color = Commons.getColor();
    if (type.value > CancelType.momdady_19.value) {
      color = color_linkmom;
    } else {
      color = color_main;
    }
    return color;
  }

  bool isChange(CancelType type, CancelType nowType) {
    if (type != nowType) {
      //선택한 값이 맘대디에서 링크쌤으로 변경시
      if (nowType.value > CancelType.momdady_19.value && type.value <= CancelType.momdady_19.value) {
        return true;
      } else if (nowType.value < CancelType.linkmom_20.value && type.value >= CancelType.linkmom_20.value) {
        return true;
      }
    }
    return false;
  }

  ///서로 상대방 사유시 선택
  bool isCancelAlert(CancelType type) {
    //내가 링크쌤이고, 맘대디 사유 선택시 true,
    if (type.value <= CancelType.momdady_19.value && Commons.isLinkMom()) {
      return true;
    } else if (type.value >= CancelType.linkmom_20.value && !Commons.isLinkMom()) {
      return true;
    }
    return false;
  }
}
