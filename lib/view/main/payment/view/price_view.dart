import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/svg_style.dart';
import 'package:linkmom/view/main/job_apply/title_toggle_icon_view.dart';
import 'package:linkmom/view/main/payment/model/peice_view_data.dart';

import '../../../../main.dart';

///결제요금(결제)
Widget priceView(PriceViewData data, {Function? callBack}) {
  log.d('『GGUMBI』>>> priceView : data: $data,  <<< ');
  bool isPay = StringUtils.validateString(data.date);
  return Column(
    children: [
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          titleToggleIconView(
            null,
            data.title,
            isInfo: false,
            rightWidget: isPay
                ? lText(
                    StringUtils.getContractDate(data.date),
                    textAlign: TextAlign.right,
                  )
                : Center(),
          ),
          sb_h_10,
          lPriceText(
            title: "신청금액".tr(),
            value: data.cost_sum,
            padding: padding_05_B,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: padding_03_T,
                child: svgImage(imagePath: SVG_ICON_L, size: 7),
              ),
              sb_w_05,
              Expanded(
                child: lPriceText(
                  title: "포인트사용".tr(),
                  titleFontSize: 15,
                  titleColor: color_545454,
                  titleFontWeight: FontWeight.normal,
                  value: data.point,
                  isPoint: true,
                  valueColor: color_545454,
                ),
              ),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: padding_03_T,
                child: svgImage(imagePath: SVG_ICON_L, size: 7),
              ),
              sb_w_05,
              Expanded(
                child: lPriceText(
                  title: "캐시사용".tr(),
                  titleFontSize: 15,
                  titleColor: color_545454,
                  titleFontWeight: FontWeight.normal,
                  value: data.cache,
                  isCache: true,
                  valueColor: color_545454,
                ),
              ),
            ],
          ),
          lPriceText(
            title: isPay ? '${EnumUtils.getPayTypeString(data.pay_method, data.pg_provider).string}' : "남은결제금액".tr(),
            value: data.calcuPrice(),
            padding: padding_0,
          ),
        ],
      ),
      lDivider(color: color_main, padding: padding_15_TB, thickness: 2.0),
      lPriceText(
        title: "최종결제금액".tr(),
        titleFontWeight: FontWeight.bold,
        titleFontSize: 18,
        value: data.calcuPrice(),
        valueFontWeight: FontWeight.bold,
        valueFontSize: 18,
      ),
    ],
  );
}

///결제내역 (결제후 또는 결제 취소 사용)
Widget priceInfoView(PriceViewData? data, {PayViewType payViewType = PayViewType.cancel_init, bool initiallyExpanded = false, Function? callBack}) {
  return ListTileTheme(
    contentPadding: padding_0,
    dense: true,
    child: Theme(
      data: Theme.of(navigatorKey.currentContext!).copyWith(dividerColor: Colors.transparent), // re,
      child: ExpansionTile(
          expandedAlignment: Alignment.topLeft,
          tilePadding: padding_0,
          childrenPadding: padding_0,
          iconColor: color_222222,
          collapsedIconColor: color_222222,
          initiallyExpanded: initiallyExpanded,
          title: titleToggleIconView(
            null,
            data!.title,
            isInfo: false,
            rightWidget: lText(
              StringUtils.getContractDate(data.date),
              textAlign: TextAlign.right,
            ),
          ),
          children: [
            Container(
              child: Column(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      lPriceText(
                        title: "신청금액".tr(),
                        value: data.cost_sum,
                        padding: padding_05_B,
                      ),
                      sb_h_05,
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: padding_03_T,
                            child: svgImage(imagePath: SVG_ICON_L, size: 7),
                          ),
                          sb_w_05,
                          Expanded(
                            child: lPriceText(
                              title: "포인트사용".tr(),
                              titleFontSize: 15,
                              titleColor: color_545454,
                              titleFontWeight: FontWeight.normal,
                              value: data.point,
                              isPoint: true,
                              valueColor: color_545454,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: padding_03_T,
                            child: svgImage(imagePath: SVG_ICON_L, size: 7),
                          ),
                          sb_w_05,
                          Expanded(
                            child: lPriceText(
                              title: "캐시사용".tr(),
                              titleFontSize: 15,
                              titleColor: color_545454,
                              titleFontWeight: FontWeight.normal,
                              value: data.cache,
                              isCache: true,
                              valueColor: color_545454,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: padding_03_T,
                            child: svgImage(imagePath: SVG_ICON_L, size: 7),
                          ),
                          sb_w_05,
                          Expanded(
                            child: lPriceText(
                              title: "결제금액".tr(),
                              titleFontSize: 15,
                              titleColor: color_545454,
                              titleFontWeight: FontWeight.normal,
                              value: data.calcuPrice(),
                              valueColor: color_545454,
                            ),
                          ),
                        ],
                      ),
                      if (data.calcuPrice() > 0)
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: padding_03_T,
                              child: svgImage(imagePath: SVG_ICON_L, size: 7, color: color_transparent),
                            ),
                            sb_w_05,
                            Expanded(
                              child: lPriceText(
                                title: '${data.card_name}(${data.card_number})',
                                titleFontSize: 15,
                                titleColor: color_b2b2b2,
                                titleFontWeight: FontWeight.normal,
                              ),
                            ),
                          ],
                        ),
                    ],
                  ),
                ],
              ),
            ),
          ]),
    ),
  );
}
