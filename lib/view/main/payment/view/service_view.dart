import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/job_apply/title_toggle_icon_view.dart';
import 'package:linkmom/view/main/job_apply/title_value_view.dart';
import 'package:linkmom/view/main/payment/model/service_data.dart';

import '../../../../main.dart';

///서비스 신청내역
Widget careInfoView(ServiceViewData? data, {Function? callBack}) {
  int flex = 1;
  int flex2 = 3;
  var padding = padding_10_B;
  //신청기간
  String date = '${data!.caredate!.length}${"일".tr()}';
  if (data.durationtime == null) {
    data.durationtime = 0;
  }
  //신청시간
  String time = StringUtils.formatCalcuTime(data.durationtime!, 0);

  log.d('『GGUMBI』>>> careInfoView : {data.servicetype}: ${data.caredate},  <<< ');

  return data.caredate!.isEmpty
      ? Center()
      : Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            titleToggleIconView(null, data.title, isInfo: false, rightWidget: Center()),
            sb_h_15,
            titleValueView(
              "돌봄유형".tr(),
              color: color_b2b2b2,
              fontWeight: FontWeight.normal,
              valueRight: StringUtils.getServiceTypeString(data.servicetype!),
              flex: flex,
              flex2: flex2,
              padding: padding,
              // textAlignRight: TextAlign.right,
            ),
            titleValueView(
              "신청기간".tr(),
              color: color_b2b2b2,
              fontWeight: FontWeight.normal,
              valueRight: date,
              isRightExpanded: false,
              valueRight2: '  ${StringUtils.getCareDateCount(data.caredate!)}',
              colorRight2: color_999999,
              fontWeightRight2: FontWeight.normal,
              textAlignRight2: TextAlign.left,
              flex: flex,
              flex2: flex2,
              padding: padding,
            ),
            titleValueView(
              "신청시간".tr(),
              color: color_b2b2b2,
              fontWeight: FontWeight.normal,
              valueRight: time,
              //총 신청시간 서버 작업시 진행
              isRightExpanded: false,
              valueRight2: '  ${StringUtils.getCareTime(data.caretime!)}',
              colorRight2: color_999999,
              fontWeightRight2: FontWeight.normal,
              textAlignRight2: TextAlign.left,
              flex: flex,
              flex2: flex2,
              padding: padding,
            ),
            titleValueView(
              Commons.isLinkMom() ? "설정시급".tr() : "신청금액".tr(),
              color: color_b2b2b2,
              fontWeight: FontWeight.normal,
              valueRight: '${StringUtils.formatPay(Commons.isLinkMom() ? data.cost_perhour! : data.cost_sum!)}${"원".tr()}',
              isRightExpanded: false,
              valueRight2: Commons.isLinkMom() ? '' : '  (${"시급".tr()} ${StringUtils.formatPay(data.cost_perhour!)}${"원".tr()}x${time}x$date)',
              colorRight2: color_999999,
              fontWeightRight2: FontWeight.normal,
              textAlignRight2: TextAlign.left,
              flex: flex,
              flex2: flex2,
              padding: padding,
            ),
            if (StringUtils.validateString(data.btnTitle))
              Column(
                children: [
                  sb_h_10,
                  lBtnOutline(data.btnTitle!, style: st_b_16(fontWeight: FontWeight.w500, textColor: Commons.getColor()), borderRadius: 8, sideColor: Commons.getColor(), onClickAction: () {
                    callBack!();
                  }),
                ],
              ),
          ],
        );
}
