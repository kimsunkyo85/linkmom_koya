import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/data/pay_cancel_save_explain_data.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/title_toggle_icon_view.dart';

import '../../chat/full_photo.dart';

///소명하기(취소사유)뷰
///1. 관리자답볍 : 최상단 관리자 답변이 있으면 보여준다., 없으면 보여주지 않는다.
///2. 취소사유 : 취소사유와, 취소사유 설명이 있으면 보여준다.
///3. 소명하기 : 소명하기 내용이 없으면 보여준다.(맘대디 -> 링크쌤이 맘대디측 사유로 할시, 링크쌤 -> 맘대디가 링크쌤측 사유로 할시)
///4. 소명내용 : 소명 내용이 있으면 소명하기 내용은 숨김처리후 소명 내용을 보여준다.
Widget canceldReasonView(PayCanceSaveExplainData data, bool isCancelLinkMom, bool isRefund, PayViewType viewType, {Function? callBack}) {
  TextEditingController tcManager = TextEditingController();
  TextEditingController tcCancelReason = TextEditingController();
  TextEditingController tcCancelExplain = TextEditingController();
  //관리자답변
  bool isManager = StringUtils.validateString(data.manager_msg);
  tcManager.text = isManager ? data.manager_msg! : '';
  //취소사유
  bool isCancelReason = StringUtils.validateString(data.cancelreason_msg);
  tcCancelReason.text = isCancelReason ? data.cancelreason_msg! : '';
  //소명하기
  bool isCancelExplain = StringUtils.validateString(data.cancelexplain_msg);
  tcCancelExplain.text = isCancelExplain ? data.cancelexplain_msg! : '';

  CancelUserType cancelUserType = EnumUtils.getCancelUserType(data.cancel_usertype!);
  //소명하기 상대방이 나의 사유로 취소 요청이 들어 온 경우 소명하기 내용을 표시한다.
  //링크쌤이고 맘대디가 취소 요청시, 맘대디이고, 링크쌤이 취소 요청시 소명을 할 수 있도록 보여준다.
  bool isExplainView = false;

  /// only use message. if(cancel_userType == 1) show linkmom text
  USER_TYPE type = data.cancel_usertype == 1 ? USER_TYPE.link_mom : USER_TYPE.mom_daddy;
  String isAgreeTxt = data.is_cancelreason_agree ? "동의".tr() : "소명".tr();
  String name = isCancelLinkMom ? data.contract_info!.linkmom_first_name : data.contract_info!.momdady_first_name;
  String message = "$name ${type.name}${type.joinTxt} ${"취소사유에".tr()} $isAgreeTxt ${"했어요".tr()}";
  log.d('『GGUMBI』>>> canceldReasonView : : $cancelUserType, : $isCancelLinkMom, <<< ');
  log.d('『GGUMBI』>>> _getMatchingTitle : isCancelExplain: ${Commons.isLinkMom()}, ${data.cancel_usertype}, $isCancelLinkMom, ${StringUtils.payDeadline(data.cancelexplain_deadline!, showMin: true)}, $isCancelExplain <<< ');
  isExplainView = Commons.isExplain(cancelUserType, isCancelLinkMom);
  DateTime deadline = StringUtils.parseYMDHM(data.cancelexplain_deadline!);
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      //관리자답변 -------------------------------------------------
      if (isManager)
        Padding(
          padding: padding_10_B,
          child: textFieldView(
            tcManager,
            title: "관리자답변".tr(),
            colorBg: Commons.getColorServiceTypeBg(),
            colorBorder: color_transparent,
          ),
        ),
      //취소사유 -------------------------------------------------
      Padding(
        padding: padding_24_B,
        child: titleToggleIconView(
          null,
          "취소사유".tr(),
          isInfo: false,
          rightWidget: Center(),
        ),
      ),
      lText(
        EnumUtils.getCancelSelectUserType(data.cancelreason!),
        style: st_16(textColor: EnumUtils.getCancelSelectUserTypeColor(data.cancelreason!), fontWeight: FontWeight.bold),
      ),
      Padding(
        padding: padding_10_TB,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Lcons.check_circle(
              isEnabled: true,
              size: 24,
              color: EnumUtils.getCancelSelectUserTypeColor(data.cancelreason!),
            ),
            sb_w_05,
            Expanded(
              child: lText(
                EnumUtils.getCancelSelect(data.cancelreason!).string,
                style: st_b_15(textColor: color_545454, fontWeight: FontWeight.w500),
              ),
            ),
          ],
        ),
      ),
      //취소사유가 있으면 보여줌 -------------------------------------------------
      if (isCancelReason)
        textFieldView(
          tcCancelReason,
          images: data.cancelreason_img,
          colorBg: color_fafafa,
          colorBorder: color_transparent,
        ),
      if (Commons.isExplanable(cancelUserType, isCancelLinkMom)) lDivider(padding: padding_25_B, color: color_eeeeee),
      //소명하기 내용이 있으면 보여주지 않는다. (맘대디 -> 링크쌤이 맘대디측 사유로 할시, 링크쌤 -> 맘대디가 링크쌤측 사유로 할시) -------------------------------------------------
      if (Commons.isExplanable(cancelUserType, isCancelLinkMom))
        explainStateView(
          deadline,
          isExplainView,
          isCancelExplain,
          data.is_cancelreason_agree,
          (a) => callBack!(a),
          name,
          type,
          isCancelLinkMom,
          message,
          isAgreeTxt,
          data.cancelexplain_img ?? [],
          tcCancelExplain,
        ),
    ],
  );
}

Widget textFieldView(TextEditingController controller, {String? title, Color colorBg = color_white, Color colorBorder = color_dedede, List<CancelImgData>? images}) {
  return Column(
    children: [
      if (StringUtils.validateString(title))
        Column(
          children: [
            titleToggleIconView(
              null,
              title,
              isInfo: false,
              rightWidget: Center(),
            ),
          ],
        ),
      Container(
        margin: padding_10_TB,
        decoration: BoxDecoration(color: colorBg, border: Border.all(color: colorBorder), borderRadius: BorderRadius.circular(6)),
        padding: padding_10,
        child: lScrollbar(
          child: TextField(
            controller: controller,
            keyboardType: TextInputType.multiline,
            textInputAction: TextInputAction.done,
            readOnly: true,
            minLines: 2,
            maxLines: 5,
            decoration: InputDecoration(
                contentPadding: padding_0,
                counterText: '',
                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent)),
                border: OutlineInputBorder(borderSide: BorderSide(color: Colors.transparent))),
            maxLength: 1000,
          ),
        ),
      ),
      sb_h_10,
      if (images != null && images.isNotEmpty)
        Container(
          height: 100,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: images
                .map((e) => FittedBox(
                      fit: BoxFit.cover,
                      child: Container(
                        margin: padding_10_R,
                        width: 122,
                        height: 122,
                        child: InkWell(
                          onTap: () => Commons.nextPage(navigatorKey.currentContext, fn: () => FullPhotoPage(index: images.indexOf(e), urlList: List<String>.generate(images.length, (index) => images[index].image!))),
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: Hero(
                                tag: e,
                                // child: CachedNetworkImage(imageUrl: e.image!, fit: BoxFit.cover),
                                child: CachedNetworkImage(
                                  imageUrl: e.image!,
                                  imageBuilder: (context, imageProvider) => imageBuilder(imageProvider),
                                  placeholder: (context, url) => placeholder(),
                                  errorWidget: (context, url, error) => errorWidget(),
                                ),
                              )),
                        ),
                      ),
                    ))
                .toList(),
          ),
        ),
    ],
  );
}

Widget explainStateView(
  DateTime deadline,
  bool isExplainView,
  bool isCancelExplain,
  bool isAgree,
  Function callback,
  String name,
  USER_TYPE type,
  bool isCancelLinkMom,
  String message,
  String isAgreeTxt,
  List<CancelImgData> img,
  TextEditingController txtCtrl,
) {
  String deadlineTxt = StringUtils.formatYMDAHM(deadline);
  if (isAgree) {
    // show agree message
    return TextHighlight(
      text: message,
      term: isAgreeTxt,
      textStyle: st_b_16(fontWeight: FontWeight.w700),
      textStyleHighlight: st_16(fontWeight: FontWeight.w700, textColor: Commons.getColor()),
    );
  } else {
    if (isCancelExplain) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextHighlight(
            text: message,
            term: isAgreeTxt,
            textStyle: st_b_16(fontWeight: FontWeight.w700),
            textStyleHighlight: st_16(fontWeight: FontWeight.w700, textColor: Commons.getColor()),
          ),
          sb_h_10,
          textFieldView(
            txtCtrl,
            images: img,
            colorBg: color_fafafa,
            colorBorder: color_transparent,
          ),
        ],
      );
    } else if (deadline.isAfter(DateTime.now())) {
      if (isExplainView) {
        /// showAgreeButton;
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            titleToggleIconView(
              null,
              "취소사유에동의하시나요".tr(),
              isInfo: false,
              rightWidget: Container(),
              flex: 5,
            ),
            lText("(${StringUtils.payDeadline(deadlineTxt, showMin: true)}${"까지".tr()})", style: st_b_12(textColor: color_999999)),
            Container(
              padding: padding_20_TB,
              width: 400,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                      child: lBtn(
                    "소명이필요".tr(),
                    onClickAction: () => callback(DialogAction.no),
                    margin: padding_0,
                    style: st_16(fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                    height: 64,
                  )),
                  sb_w_10,
                  Flexible(
                      child: lBtn(
                    "동의합니다".tr(),
                    onClickAction: () => showNormalDlg(message: Commons.isLinkMom() ? "취소동의_링크쌤".tr() : "취소동의_맘대디".tr(), onClickAction: (a) => {if (a == DialogAction.yes) callback(DialogAction.yes)}),
                    margin: padding_0,
                    style: st_16(fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                    height: 64,
                  )),
                ],
              ),
            ),
          ],
        );
      } else {
        // show wait message
        return Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              lText('${"소명하기_안내1".tr()} $name ${type.name}${"의".tr()}\n${"취소기간_이전".tr()}', style: st_b_16(fontWeight: FontWeight.w700)),
              lText("(${StringUtils.payDeadline(deadlineTxt, showMin: true)}${"까지".tr()})", style: st_b_12(textColor: color_999999)),
            ],
          ),
        );
      }
    } else {
      // show Expired message. same the momdaddy, linkmom.
      return Column(
        children: [
          lText('${"소명시간경과_안내".tr()}${isCancelLinkMom ? "소명시간경과_맘대디".tr() : "소명시간경과_링크쌤".tr()}', style: st_b_14(fontWeight: FontWeight.w500)),
          sb_h_20,
          lBtn("고객센터문의".tr(), margin: padding_0, onClickAction: () => callback(DialogAction.next)),
        ],
      );
    }
  }
}
