import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/svg_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/job_apply/title_toggle_icon_view.dart';
import 'package:linkmom/view/main/payment/model/refund_data.dart';

///결제요금(결제)
///- RefundViewData - 환불데이터
///- isLinkMom - 취사 사유(맘대디 또는 링크쌤)
///- is_refund - 환불 완료 여부
///- viewType - 취소 신청, 또는 뷰
Widget refundInfoView(RefundViewData data, int priceTotal, bool isRefund, PayViewType viewType, {Function? callBack}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      titleToggleIconView(
        null,
        data.data!.title,
        isInfo: false,
        rightWidget: Center(),
      ),
      sb_h_10,
      lPriceText(
        isString: false,
        isValue: true,
        title: "취소날짜".tr(),
        titleFontWeight: FontWeight.w500,
        valueString: viewType == PayViewType.cancel_init ? data.getCancelDate() : data.getCancelViewDate(),
        valueFontWeight: FontWeight.w500,
      ),

      //취소 선택한 금액
      Column(
        children: [
          lPriceText(
            isString: false,
            title: "취소금액".tr(),
            titleFontWeight: FontWeight.w500,
            value: priceTotal,
            valueFontWeight: FontWeight.w500,
          ),
        ],
      ),

      // //맘대디디고, 환불 예정인 경우, 환불완료이면 보여주지 않는다, 신청또는 환불 예정인 경우
      // if (!Commons.isLinkMom() && !isRefund)
      //   Column(
      //     children: [
      //       lPriceText(
      //         isString: false,
      //         title: "환불예정금액".tr(),
      //         titleFontWeight: FontWeight.w500,
      //         value: data.refundInfoData!.cancelled_price!,
      //         valueFontWeight: FontWeight.w500,
      //         valueColor: color_f0544c,
      //       ),
      //     ],
      //   ),

      //링크쌤일 경우
      if (Commons.isLinkMom())
        Column(children: [
          Column(
            children: [
              lPriceText(
                title: isRefund ? "보상금액".tr() : "보상예정금액".tr(),
                titleFontWeight: FontWeight.w500,
                value: data.payCancelSelectData!.priceTotal!,
                valueFontWeight: FontWeight.w500,
                padding: padding_05_B,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: padding_03_T,
                    child: svgImage(imagePath: SVG_ICON_L, size: 7),
                  ),
                  sb_w_05,
                  Expanded(
                    child: lPriceText(
                      title: "플랫폼이용료".tr(),
                      titleFontSize: 15,
                      titleColor: color_b2b2b2,
                      titleFontWeight: FontWeight.normal,
                      value: data.platformFee(),
                      valueColor: color_b2b2b2,
                      padding: padding_0,
                    ),
                  ),
                ],
              ),
              lDivider(color: Commons.getColor(), thickness: 2.0, padding: padding_20_TB),
              lPriceText(
                isString: false,
                isCache: true,
                title: isRefund ? "총보상금액".tr() : "총보상예정금액".tr(),
                titleFontWeight: FontWeight.bold,
                titleFontSize: 18,
                value: data.platformFeeTotal(),
                valueFontWeight: FontWeight.bold,
                valueFontSize: 18,
              ),
              lPriceText(
                isString: false,
                isValue: true,
                title: "총부과벌점스티커".tr(),
                titleFontWeight: FontWeight.bold,
                titleFontSize: 18,
                valueString: '${data.payCancelSelectData!.stickerTotal!} ${"개".tr()}',
                valueFontWeight: FontWeight.bold,
                valueColor: color_f0544c,
                valueFontSize: 18,
              ),
            ],
          ),
        ]),

      //맘대디이고, 환불완료이면 상세하게 보여준다 2021/10/28 환불예정인 경우 최종환불금액을 제외하고 보여준다.
      if (!Commons.isLinkMom())
        Column(
          children: [
            lPriceText(
                isInfo: true,
                isPoint: true,
                isString: false,
                title: "환불_포인트".tr(),
                titleFontWeight: FontWeight.w500,
                value: data.refundInfoData!.refund_point!,
                valueFontWeight: FontWeight.w500,
                valueColor: color_f0544c,
                callBack: () {
                  showNormalDlg(
                    title: "포인트환불안내".tr(),
                    padding: padding_10,
                    messageWidget: Column(
                      children: [
                        sb_h_10,
                        lText(
                          "환불예정포인트_안내_1".tr(),
                          style: st_16(textColor: color_545454),
                          textAlign: TextAlign.center,
                        ),
                        sb_h_10,
                        lText(
                          "환불예정포인트_안내_2".tr(),
                          style: st_16(textColor: color_545454, fontWeight: FontWeight.w500),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  );
                }),
            //만료포인트 (취소신청 및 예정, 완료에서 만료 포인트가 있으면 보여준다.)
            if (data.refundInfoData!.expired_point! > 0)
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: padding_03_T,
                    child: svgImage(imagePath: SVG_ICON_L, size: 7),
                  ),
                  sb_w_05,
                  Expanded(
                    child: lPriceText(
                      title: "만료_포인트".tr(),
                      titleFontSize: 15,
                      titleColor: color_b2b2b2,
                      titleFontWeight: FontWeight.normal,
                      value: data.refundInfoData!.expired_point!,
                      isPoint: true,
                      isString: false,
                      valueColor: color_f0544c /*color_545454*/,
                    ),
                  ),
                ],
              ),
            lPriceText(
              isCache: true,
              isString: false,
              title: "환불_캐시".tr(),
              titleFontWeight: FontWeight.w500,
              value: data.refundInfoData!.refund_cash!,
              valueFontWeight: FontWeight.w500,
              valueColor: color_f0544c,
            ),

            //환불금액 환불금액이 있거나, free 결제외 결제수단을 보여준다.
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                lPriceText(
                  title: "환불금액".tr(),
                  titleFontWeight: FontWeight.w500,
                  value: data.refundInfoData!.cancelled_price!,
                  valueFontWeight: FontWeight.w500,
                  valueColor: color_f0544c,
                  padding: padding_05_B,
                ),
                //포인트+캐시 결제시 환불금액은 0원 외 신용카드, 간편결제수단 표시
                if (data.data!.pay_method != PayType.free.string /*|| data.refundInfoData!.cancelled_price! > 0*/)
                  Padding(
                    padding: padding_03_T,
                    child: lText('${data.data!.card_name}(${data.data!.card_number})', style: st_15(textColor: color_b2b2b2)),
                  ),
              ],
            ),
          ],
        ),
      //2021/11/17 환불규정 안으로 이동 주석 처리함
      // Column(
      //   children: [
      //     sb_h_20,
      //     Row(
      //       crossAxisAlignment: CrossAxisAlignment.start,
      //       children: [
      //         lText('・'),
      //         Expanded(
      //           child: TextHighlight(
      //             text: "취소날짜선택_안내_맘대디_1".tr(),
      //             term: "취소날짜선택_안내_맘대디_1_힌트".tr(),
      //             textStyleHighlight: st_b_14(textColor: Commons.getColor()),
      //             textStyle: st_b_14(textColor: color_545454),
      //           ),
      //         ),
      //       ],
      //     ),
      //   ],
      // ),
    ],
  );
}
