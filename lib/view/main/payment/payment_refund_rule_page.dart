import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/main/job_apply/title_toggle_icon_view.dart';

import '../../lcons.dart';

///환불규정 안내화면
class PaymentRefundRulePage extends BaseStateful {
  Function? callBack;

  PaymentRefundRulePage({this.callBack});

  @override
  PaymentPageState createState() => PaymentPageState(callBack: callBack);
}

class PaymentPageState extends BaseStatefulState<PaymentRefundRulePage> {
  Function? callBack;

  PaymentPageState({this.callBack});

  List<SingleItem> infoList = [];

  List<SingleItem> infoTimeMomdady = [
    SingleItem('48${"시간".tr()} ${"미만".tr()}~\n24${"시간".tr()} ${"이상".tr()}', data: st_b_12(textColor: color_transparent, fontWeight: FontWeight.w500), isSelected: true),
    SingleItem('48${"시간".tr()} ${"이상".tr()}', data: st_b_12(fontWeight: FontWeight.w500)),
    SingleItem('48${"시간".tr()} ${"미만".tr()}~\n24${"시간".tr()} ${"이상".tr()}', data: st_b_12(fontWeight: FontWeight.w500)),
    SingleItem('24${"시간".tr()} ${"미만".tr()}~\n6${"시간".tr()} ${"이상".tr()}', data: st_b_12(fontWeight: FontWeight.w500)),
    SingleItem('6${"시간".tr()} ${"미만".tr()}~\n${"돌봄".tr()} ${"시작후".tr()}', type: 1, data: st_b_12(textColor: color_f0544c, fontWeight: FontWeight.w500)),
  ];

  List<SingleItem> infoTimeLinkmom = [
    SingleItem('48${"시간".tr()} ${"미만".tr()}~\n24${"시간".tr()} ${"이상".tr()}', data: st_b_12(textColor: color_transparent, fontWeight: FontWeight.w500), isSelected: true),
    SingleItem('48${"시간".tr()} ${"이상".tr()}', data: st_b_12(fontWeight: FontWeight.w500)),
    SingleItem('48${"시간".tr()} ${"미만".tr()}~\n24${"시간".tr()} ${"이상".tr()}', data: st_b_12(fontWeight: FontWeight.w500)),
    SingleItem('24${"시간".tr()} ${"미만".tr()}~\n6${"시간".tr()} ${"이상".tr()}', data: st_b_12(fontWeight: FontWeight.w500)),
    SingleItem('6${"시간".tr()} ${"미만".tr()}~\n${"돌봄".tr()} ${"시작전".tr()}', data: st_b_12(textColor: color_f0544c, fontWeight: FontWeight.w500)),
    SingleItem('${"돌봄".tr()} ${"시작후".tr()}', type: 1, data: st_b_12(textColor: color_f0544c, fontWeight: FontWeight.w500)),
  ];

  List<SingleItem> infoMomdady = [
    SingleItem('${"맘대디".tr()} ${"환불".tr()}', name2: '${"링크쌤".tr()} ${"취소보상".tr()}', data: st_b_14(fontWeight: FontWeight.bold), isSelected: true),
    SingleItem('100% ${"환불".tr()}', name2: "취소보상없음".tr(), data: st_14(fontWeight: FontWeight.w500)),
    SingleItem('50% ${"환불".tr()}', name2: "50%취소보상".tr(), data: st_14(fontWeight: FontWeight.w500)),
    SingleItem('20% ${"환불".tr()}', name2: "80%취소보상".tr(), data: st_14(fontWeight: FontWeight.w500)),
    SingleItem('${"환불".tr()} ${"불가".tr()}', name2: "100%취소보상".tr(), type: 1, data: st_14(fontWeight: FontWeight.w500)),
  ];

  List<SingleItem> infoLinkMom = [
    SingleItem('${"링크쌤".tr()} ${"벌점스티커".tr()}', name2: '${"맘대디".tr()} ${"환불".tr()}', data: st_b_14(fontWeight: FontWeight.bold), isSelected: true),
    SingleItem('${"벌점스티커".tr()} ${"없음".tr()}', name2: '100% ${"환불".tr()}', data: st_14(fontWeight: FontWeight.w500)),
    SingleItem('${"벌점스티커".tr()} 1${"개".tr()}', name2: '100% ${"환불".tr()}', data: st_14(fontWeight: FontWeight.w500)),
    SingleItem('${"벌점스티커".tr()} 2${"개".tr()}', name2: '100% ${"환불".tr()}', data: st_14(fontWeight: FontWeight.w500)),
    SingleItem('${"벌점스티커".tr()} 3${"개".tr()}', name2: '100% ${"환불".tr()}', data: st_14(fontWeight: FontWeight.w500)),
    SingleItem('${"벌점스티커".tr()} 4${"개".tr()}', name2: '100% ${"환불".tr()}', type: 1, data: st_14(fontWeight: FontWeight.w500)),
  ];

  @override
  void initState() {
    super.initState();

    infoList = Commons.isLinkMom()
        ? [
            SingleItem("돌봄취소안내_안내_링크쌤_1".tr(), name2: "돌봄취소안내_링크쌤사유로취소".tr()),
            SingleItem("돌봄취소안내_안내_링크쌤_2".tr(), name2: "돌봄취소안내_받은벌점".tr()),
            SingleItem("돌봄취소안내_안내_링크쌤_3".tr(), name2: "돌봄취소안내_6시간".tr()),
            SingleItem("돌봄취소안내_안내_링크쌤_4".tr(), name2: "돌봄취소안내_맘대디사유취소".tr()),
            SingleItem("돌봄취소안내_안내_링크쌤_5".tr(), name2: "돌봄취소안내_상대방사유로취소".tr()),
          ]
        : [
            SingleItem("돌봄취소안내_안내_맘대디_1".tr(), name2: "돌봄취소안내_6시간".tr()),
            SingleItem("돌봄취소안내_안내_맘대디_2".tr(), name2: "돌봄취소안내_맘대디사유취소".tr()),
            SingleItem("돌봄취소안내_안내_맘대디_3".tr(), name2: "돌봄취소안내_링크쌤사유로취소".tr()),
            SingleItem("돌봄취소안내_안내_맘대디_4".tr(), name2: "돌봄취소안내_받은벌점".tr()),
            SingleItem("돌봄취소안내_안내_맘대디_5".tr(), name2: "돌봄취소안내_상대방사유로취소".tr()),
            SingleItem("돌봄취소안내_안내_맘대디_6".tr(), name2: "돌봄취소안내_결제했던수단".tr()),
          ];
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar(
          "환불규정".tr(),
          hide: false,
        ),
        context: context,
        body: lContainer(
          child: Column(
            children: [
              Expanded(
                child: lScrollView(
                  padding: padding_20,
                  child: Column(
                    children: [
                      sb_h_10, //・
                      titleToggleIconView(null, "돌봄취소안내".tr(), isInfo: false, rightWidget: Center()),

                      Column(
                        children: infoList
                            .map((e) => Column(
                                  children: [
                                    Padding(
                                      padding: padding_05_T,
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          lText('・'),
                                          Expanded(
                                            child: TextHighlight(
                                              text: e.name,
                                              term: e.name2,
                                              textStyleHighlight: st_b_14(textColor: Commons.getColor()),
                                              textStyle: st_b_14(textColor: color_545454),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ))
                            .toList(),
                      ),

                      lBtn(
                        "벌점스티커자세히보기".tr(),
                        margin: padding_20_TB,
                        btnColor: color_white,
                        sideColor: Commons.getColor(),
                        isIconLeft: false,
                        icon: Lcons.nav_right(size: 15, color: Commons.getColor()),
                        style: st_16(textColor: Commons.getColor(), fontWeight: FontWeight.bold),
                        onClickAction: () => Commons.page(context, routePenaltyManage),
                      ),
                      /*lBtnOutline('${"벌점스티커자세히보기".tr()} >', margin: padding_20_TB, onClickAction: () {
                        Commons.page(context, routePenaltyManage);
                      }),*/

                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 20, 0, 15),
                        child: lText(Commons.isLinkMom() ? "링크쌤사유취소요청시".tr() : "맘대디사유취소요청시".tr(), fontSize: 18, fontWeight: FontWeight.bold),
                      ),

                      _ruleView(
                        Commons.isLinkMom() ? infoTimeLinkmom : infoTimeMomdady,
                        Commons.isLinkMom() ? infoLinkMom : infoMomdady,
                        Commons.isLinkMom() ? color_linkmom : color_momdady,
                        color_white,
                        Commons.isLinkMom() ? 3 : 2,
                        Commons.isLinkMom() ? 2 : 3,
                        Commons.isLinkMom() ? 50 : 70,
                      ),
                      lDivider20(thickness: 1.0, color: color_eeeeee, padding: padding_25_T),

                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 20, 0, 15),
                        child: lText(Commons.isLinkMom() ? "맘대디사유취소요청시".tr() : "링크쌤사유취소요청시".tr(), fontSize: 18, fontWeight: FontWeight.bold),
                      ),

                      _ruleView(
                        Commons.isLinkMom() ? infoTimeMomdady : infoTimeLinkmom,
                        Commons.isLinkMom() ? infoMomdady : infoLinkMom,
                        Commons.isLinkMom() ? color_momdady : color_linkmom,
                        color_white,
                        Commons.isLinkMom() ? 2 : 3,
                        Commons.isLinkMom() ? 3 : 2,
                        Commons.isLinkMom() ? 70 : 50,
                      ),
                      sb_h_20,
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _ruleView(List<SingleItem> infoTimes, List<SingleItem> infoList, Color colorBg, Color colorLine, int flex, int flex2, double height) {
    return Column(
      children: [
        Row(
          children: [
            Column(
              children: infoTimes.map((e) => e.isSelected ? _timeView(e, height) : Center()).toList(),
            ),
            Expanded(
              child: Column(
                children: infoList.map((e) => e.isSelected ? _infoView(e, colorBg, colorLine, flex, flex2, height) : Center()).toList(),
              ),
            ),
          ],
        ),
        Container(
          decoration: decorationRound(radius: 0),
          child: Row(
            children: [
              Column(
                children: infoTimes.map((e) => e.isSelected ? Center() : _timeView(e, height)).toList(),
              ),
              Expanded(
                child: Column(
                  children: infoList.map((e) => e.isSelected ? Center() : _infoView(e, colorBg, colorLine, flex, flex2, height)).toList(),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _timeView(SingleItem e, double height) {
    return Column(
      children: [
        Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  padding: padding_10_L,
                  height: e.isSelected ? null : height,
                  alignment: Alignment.center,
                  child: lAutoSizeText(
                    e.name,
                    textAlign: TextAlign.center,
                    style: e.data != null ? e.data : st_b_14(),
                    maxLines: 2,
                  )),
            ],
          ),
        ),
        if (e.type != 1) lDivider(thickness: 1.0, color: color_transparent, padding: padding_0),
      ],
    );
  }

  Widget _infoView(SingleItem e, Color colorBg, Color colorLine, int flex, int flex2, double height) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: flex,
              child: Padding(
                padding: padding_10_L,
                child: Column(
                  children: [
                    Container(
                      height: e.isSelected ? null : height,
                      alignment: Alignment.center,
                      color: e.isSelected ? color_transparent : colorBg,
                      child: lAutoSizeText(
                        e.name,
                        style: e.data != null ? e.data : st_b_14(),
                        maxLines: 2,
                      ),
                    ),
                    if (e.type != 1) lDivider(thickness: 1.0, color: colorLine, padding: padding_0),
                    // if (!e.isSelected) DottedLine(lineThickness: 1, dashGapLength: 4, dashColor: colorDot),
                  ],
                ),
              ),
            ),
            Expanded(
                flex: flex2,
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      height: e.isSelected ? null : height,
                      color: e.isSelected ? color_transparent : color_dbdbdb.withOpacity(0.5),
                      child: lAutoSizeText(
                        e.name2,
                        textAlign: TextAlign.center,
                        style: st_b_14(fontWeight: e.isSelected ? FontWeight.bold : FontWeight.w500),
                        maxLines: 2,
                      ),
                    ),
                    if (e.type != 1) lDivider(thickness: 1.0, color: colorLine, padding: padding_0),
                  ],
                )),
          ],
        ),
      ],
    );
  }
}
