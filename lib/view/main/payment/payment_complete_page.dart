import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/main/job_apply/job_profile/momdady_schedule_page.dart';

import '../../../main.dart';
import '../../../route_name.dart';
import 'model/peice_view_data.dart';
import 'model/service_data.dart';
import 'view/price_view.dart';
import 'view/service_view.dart';

class PaymentCompletePage extends BaseStateful {
  PayMentCompleteData? payMentCompleteData;

  PaymentCompletePage({this.payMentCompleteData});

  @override
  PaymentCompletePageState createState() => PaymentCompletePageState(payMentCompleteData: payMentCompleteData);
}

class PaymentCompletePageState extends BaseStatefulState<PaymentCompletePage> {
  PayMentCompleteData? payMentCompleteData;

  PaymentCompletePageState({this.payMentCompleteData});

  @override
  void initState() {
    super.initState();
    log.d('『GGUMBI』>>> initState : payMentCompleteData: $payMentCompleteData,  <<< ');
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  onResume() {
    super.onResume();
  }

  @override
  onPaused() {
    super.onPaused();
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar(
          "결제완료".tr(),
          hide: false,
          isBack: false,
        ),
        context: context,
        body: lContainer(
          child: Column(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Expanded(
                      child: lScrollView(
                        padding: padding_Item_TB,
                        child: Container(
                          child: Column(
                            children: [
                              //서비스신청내역
                              Padding(
                                padding: padding_20,
                                child: careInfoView(payMentCompleteData!.serviceViewData, callBack: () {
                                  Commons.page(context, routeMomdadySchedule,
                                      arguments: MomdadySchedulePage(
                                        data: payMentCompleteData!.serviceViewData!.caredate!,
                                        sTime: payMentCompleteData!.serviceViewData!.caretime![0],
                                        eTime: payMentCompleteData!.serviceViewData!.caretime![1],
                                      ));
                                }),
                              ),
                              lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_20_TB),
                              //결제요금
                              Padding(
                                padding: padding_Item_LR,
                                child: priceInfoView(
                                  payMentCompleteData!.priceViewData,
                                  initiallyExpanded: true,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    _confirmButton(),
                  ],
                ),
              ),
              // wdDivider(),
            ],
          ),
        ),
      ),
      opacity: 0.0,
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn("확인".tr(), isEnabled: true, onClickAction: () {
      // Commons.page(context, routeParents3, arguments: ParentsStep3Page(data: data));
      log.d('『GGUMBI』>>> _confirmButton : .....: .....,  <<< ');
      Commons.pagePop(context, data: DialogAction.previous);
    });
  }
}

class PayMentCompleteData {
  ///신청서 신청내역 데이터
  ServiceViewData? serviceViewData;

  ///결제정보 데이터
  PriceViewData? priceViewData;

  PayMentCompleteData({this.serviceViewData, this.priceViewData});

  @override
  String toString() {
    return 'PayMentCompleteData{serviceViewData: $serviceViewData, priceViewData: $priceViewData}';
  }
}
