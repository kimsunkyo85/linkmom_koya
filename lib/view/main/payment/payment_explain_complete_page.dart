import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../../lcons.dart';

class PaymentExplainCompletePage extends BaseStateful {
  @override
  PaymentExplainCompletePageState createState() => PaymentExplainCompletePageState();
}

class PaymentExplainCompletePageState extends BaseStatefulState<PaymentExplainCompletePage> {
  @override
  void initState() {
    super.initState();
    flag.enableConfirm(fn: () => onUpDate());
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      ///하드웨어 back key 막기
      onWillPop: () => Future.value(false),
      child: lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          key: data.scaffoldKey,
          resizeToAvoidBottomPadding: false,
          appBar: appBar(
            "접수완료".tr(),
            isBack: false,
            isClose: false,
            hide: false,
          ),
          body: lContainer(
            child: Column(
              children: [
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _completeView(),
                    ],
                  ),
                ),
                _confirmButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn("확인".tr(), isEnabled: flag.isConfirm, onClickAction: () {
      Commons.pagePop(context);
    });
  }

  ///돌봄 취소신청이 완료되었습니다.
  Widget _completeView() {
    return Padding(
      padding: padding_20,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          sb_h_30,
          Lcons.complete(size: 50, disableColor: color_545454),
          sb_h_20,
          lAutoSizeText(
            "정산_소명요청완료".tr(),
            style: st_b_20(fontWeight: FontWeight.bold),
          ),
          sb_h_10,
          lText(
            "취소_소명요청완료안내".tr(),
            style: st_b_16(textColor: color_545454),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
