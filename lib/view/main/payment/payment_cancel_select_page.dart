import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/data/pay_care_diary_info_data.dart';
import 'package:linkmom/data/network/models/pay_cancel_init_response.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';

import '../../../main.dart';
import '../../lcons.dart';

///취소 선택
class PaymentCancelSelectPage extends BaseStateful {
  ///결제취소 선택 데이터
  final PayCancelInitData? payCancelSelectData;

  ///맘대디 또는 링크쌤 사유 구별
  final bool? isLinkMom;

  ///취소화면 세팅 (기본 돌봄취소 cancel_init)
  PayViewType? viewType;

  Function? callBack;

  PaymentCancelSelectPage({this.payCancelSelectData, this.isLinkMom = false, this.viewType = PayViewType.cancel_init, this.callBack});

  @override
  PaymentCancelSelectPageState createState() => PaymentCancelSelectPageState(payCancelSelectData: payCancelSelectData, isLinkMom: isLinkMom, viewType: viewType, callBack: callBack);
}

class PaymentCancelSelectPageState extends BaseStatefulState<PaymentCancelSelectPage> {
  ///결제취소 선택 데이터
  final PayCancelInitData? payCancelSelectData;

  ///맘대디 또는 링크쌤 사유 구별
  final bool? isLinkMom;

  ///취소화면 세팅 (기본 돌봄취소 cancel_init)
  PayViewType? viewType;

  Function? callBack;

  PaymentCancelSelectPageState({this.payCancelSelectData, this.isLinkMom = false, this.viewType = PayViewType.cancel_init, this.callBack});

  List<SingleItem> infoList = [];

  @override
  void initState() {
    super.initState();
    onDataPage(payCancelSelectData);
  }

  @override
  void onDataPage(_data) {
    if (_data is PayCancelInitData) {
      if (payCancelSelectData != null) {
        log.d('『GGUMBI』>>> onDataPage : payCancelInitRequest: $_data,  <<< ');
        // for (int i = 0; i < 100; i++) {
        // payCancelInitData!.carediary_info!.add(PayCarediaryInfoData(carediary_id: 1));
        // }
      }

      //링크쌤측 사유
      if (isLinkMom!) {
        if (Commons.isLinkMom()) {
          infoList.add(SingleItem("취소날짜선택_안내_링크쌤_1".tr(), name2: "취소날짜선택_안내_링크쌤_1_힌트".tr()));
        } else {
          infoList.add(SingleItem("취소날짜선택_안내_맘대디_2".tr(), name2: "취소날짜선택_안내_맘대디_2_힌트".tr()));
          infoList.add(SingleItem("취소날짜선택_안내_맘대디_3".tr(), name2: "취소날짜선택_안내_맘대디_3_힌트".tr(), isSelected: true));
        }
        //맘대디측 사유
      } else {
        if (Commons.isLinkMom()) {
          infoList.add(SingleItem("취소날짜선택_안내_링크쌤_2".tr(), name2: "취소날짜선택_안내_링크쌤_2_힌트".tr()));
          infoList.add(SingleItem("취소날짜선택_안내_링크쌤_3".tr(), name2: "취소날짜선택_안내_링크쌤_3_힌트".tr(), isSelected: true));
        }
        //2021/11/17 환불규정 안으로 이동 주석 처리
        /*else {
          infoList.add(SingleItem("취소날짜선택_안내_맘대디_1".tr(), name2: "취소날짜선택_안내_맘대디_1_힌트".tr()));
        }*/
      }

      onConfirmBtn();
    }
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar(
          "날짜선택".tr(),
          hide: false,
          rightWidget: InkWell(
            onTap: () {
              Commons.page(context, routePayMentRefundRule);
            },
            child: Container(
              alignment: Alignment.center,
              padding: padding_20_LR,
              child: lText("환불규정".tr(), color: Commons.getColor(), fontWeight: FontWeight.w500),
            ),
          ),
        ),
        body: lContainer(
          padding: padding_20_T,
          child: Column(
            children: [
              Padding(
                padding: padding_20_LR,
                child: _headerView(payCancelSelectData!),
              ),
              Expanded(
                child: payCancelSelectData!.carediary_info!.isEmpty
                    ? flag.isLoading
                        ? LoadingIndicator(background: Colors.transparent)
                        : lEmptyView()
                    : Padding(
                        padding: padding_20_LR,
                        child: lScrollbar(
                          child: ListView.builder(
                              itemCount: payCancelSelectData!.carediary_info!.length,
                              physics: const AlwaysScrollableScrollPhysics(),
                              itemBuilder: (BuildContext context, int index) {
                                return _itemView(
                                  payCancelSelectData!.carediary_info![index],
                                  payCancelSelectData!.isAll!,
                                );
                              }),
                        ),
                      ),
              ),
              lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_20_TB),
              Padding(
                padding: padding_20_LR,
                child: Commons.isLinkMom()
                    ? Column(
                        children: [
                          lPriceText(
                            isValue: true,
                            title: "취소내역".tr(),
                            titleFontSize: 18,
                            titleFontWeight: FontWeight.bold,
                            padding: padding_0,
                          ),
                          lDivider(color: Commons.getColor(), thickness: 2.0, padding: padding_20_TB),
                          lPriceText(
                            isString: false,
                            title: "총보상예정금액".tr(),
                            titleFontSize: 18,
                            titleFontWeight: FontWeight.bold,
                            value: payCancelSelectData!.getPriceTotal(payCancelSelectData!, viewType!, Commons.isLinkMom()),
                            valueFontSize: 16,
                            valueFontWeight: FontWeight.bold,
                          ),
                          lPriceText(
                            isValue: true,
                            title: "총부과벌점스티커".tr(),
                            titleFontSize: 18,
                            titleFontWeight: FontWeight.bold,
                            valueString: '${payCancelSelectData!.stickerTotal!} ${"개".tr()}',
                            valueFontSize: 16,
                            valueColor: color_f0544c,
                            valueFontWeight: FontWeight.bold,
                            padding: padding_05_B,
                          )
                        ],
                      )
                    : lPriceText(
                        title: "합계".tr(),
                        titleFontSize: 18,
                        titleFontWeight: FontWeight.bold,
                        value: payCancelSelectData!.getPriceTotal(payCancelSelectData!, viewType!, Commons.isLinkMom()),
                        valueFontSize: 16,
                        valueFontWeight: FontWeight.bold,
                        padding: padding_05_B,
                      ),
              ),
              //환불 안내 문구
              Padding(
                padding: viewType == PayViewType.cancel_view ? padding_20_LRB : padding_20_LR,
                child: Column(
                  children: infoList
                      .map((e) => Column(
                            children: [
                              Padding(
                                padding: !e.isSelected ? padding_10_T : padding_0,
                                // padding: padding_10_T,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    lText('・', color: !e.isSelected ? color_191919 : color_transparent),
                                    Expanded(
                                      child: TextHighlight(
                                        text: e.name,
                                        term: e.name2,
                                        textStyleHighlight: st_b_14(textColor: Commons.getColor()),
                                        textStyle: st_b_14(textColor: color_545454),
                                      ),
                                      //
                                      // lText(
                                      //   e,
                                      //   color: color_545454,
                                      // ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ))
                      .toList(),
                ),
              ),
              _confirmButton(),
            ],
          ),
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    ///취소완료화면에서 온 경우 버튼을 생성하지 않는다.
    if (viewType == PayViewType.cancel_view) {
      return Center();
    }
    return lBtn("확인".tr(), isEnabled: flag.isConfirm, onClickAction: () async {
      Commons.pagePop(context, data: payCancelSelectData);
    });
  }

  @override
  void onConfirmBtn() {
    // 취소선택
    bool isCancel = false;
    payCancelSelectData!.carediary_info!.forEach((element) {
      if (element.isCheck!) {
        isCancel = true;
      }
    });
    if (isCancel) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  Widget _headerView(PayCancelInitData data) {
    int flex1 = 1, flex2 = 1, flex3 = 1;
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            if (viewType == PayViewType.cancel_init)
              InkWell(
                  onTap: () {
                    setState(() {
                      data.isAll = !data.isAll!;
                      data.carediary_info!.forEach((element) {
                        bool isNot = isStartCare(element.start_caredate!, element.pay_status!);
                        if (isNot) {
                          element.isCheck = data.isAll!;
                          onConfirmBtn();
                        }
                      });
                    });
                  },
                  child: Padding(
                    padding: padding_10_R,
                    child: Lcons.checkbox(isEnabled: true, color: data.isAll! ? Commons.getColor() : color_cecece, size: 24),
                  )),
            Expanded(
                flex: flex1,
                child: Column(
                  children: [
                    lText(
                      "취소할날짜".tr(),
                      style: st_b_15(fontWeight: FontWeight.bold),
                      textAlign: TextAlign.left,
                    ),
                    lText(
                      "취소상태".tr(),
                      style: st_b_15(fontWeight: FontWeight.bold),
                      textAlign: TextAlign.left,
                    ),
                  ],
                )),
            Expanded(
                flex: flex2,
                child: Column(
                  children: [
                    lText(
                      Commons.isLinkMom() ? "정산_금액".tr() : "결제금액".tr(),
                      style: st_b_15(fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                    lText(
                      Commons.isLinkMom() ? "정산비율".tr() : "환불비율".tr(),
                      style: st_b_15(fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )),
            Expanded(
                flex: flex3,
                child: Column(
                  children: [
                    lText(
                      Commons.isLinkMom() ? "보상금액".tr() : "환불예정금액".tr(),
                      style: st_b_15(fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                    if (Commons.isLinkMom())
                      lText(
                        "벌점스티커".tr(),
                        style: st_b_15(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                  ],
                )),
          ],
        ),
        lDivider(color: color_dedede, padding: padding_10_TB),
      ],
    );
  }

  ///취소날짜 선택 아이템
  Widget _itemView(PayCarediaryInfoData data, bool isAll) {
    bool isNot = isEnable(data.pay_status!);
    int flex1 = 1, flex2 = 1, flex3 = 1;
    return Padding(
      padding: padding_10_B,
      child: InkWell(
        onTap: !isNot
            ? null
            : () {
                setState(() {
                  if (isStartCare(data.start_caredate!, data.pay_status!)) {
                    data.isCheck = !data.isCheck!;
                    onConfirmBtn();
                  } else {
                    log.d('『GGUMBI』>>> _itemView :   <<< ');
                    showNormalDlg(
                        messageWidget: Column(
                      children: [
                        TextHighlight(
                          text: '${"돌봄취소_시작후_안내".tr()}(${"돌봄시작시간".tr()}:${StringUtils.payDeadline(data.start_caredate!, showMin: true)})',
                          // text: '${"돌봄취소_시작후_안내".tr()}',
                          term: "돌봄취소_시작후_안내_힌트".tr(),
                          textStyle: st_14(textColor: color_545454),
                          textStyleHighlight: st_14(textColor: Commons.getColor(), fontWeight: FontWeight.w500),
                        ),
                      ],
                    ));
                  }
                });
              },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            if (viewType == PayViewType.cancel_init)
              Padding(
                padding: padding_10_R,
                child: Lcons.checkbox(isEnabled: true, color: data.pay_status != PayStatus.compleat.value && data.isCheck! ? Commons.getColor() : color_cecece, size: 24),
              ),
            Expanded(
                flex: flex1,
                child: Column(
                  children: [
                    lText(
                      StringUtils.getStringToNewDate(data.start_caredate!, format: F_YYMMDD),
                      textAlign: TextAlign.left,
                      style: st_b_15(isSelect: !isNot, disableColor: disableColor(data.pay_status!)),
                    ),
                    lAutoSizeText(
                      payStatusTxt(data.start_caredate!, data.end_caredate!, data.pay_status!, data.pay_status_txt!),
                      textAlign: TextAlign.left,
                      style: st_b_14(isSelect: !isNot, disableColor: disableColor(data.pay_status!)),
                      maxLines: 1,
                    ),
                  ],
                )),
            Expanded(
                flex: flex2,
                child: Column(
                  children: [
                    lText(
                      '${StringUtils.formatPay(data.pay_expect)} ${"원".tr()}',
                      textAlign: TextAlign.center,
                      style: st_b_15(isSelect: !isNot, disableColor: disableColor(data.pay_status!)),
                    ),
                    lText(
                      Commons.isLinkMom() ? '(${data.linkmom_pay_rate}% ${getStringPercent(data.linkmom_pay_rate!, isNot)})' : '(${data.momdady_pay_rate}% ${getStringPercent(data.momdady_pay_rate!, isNot)})',
                      textAlign: TextAlign.center,
                      style: st_b_14(isSelect: !isNot, disableColor: disableColor(data.pay_status!)),
                    ),
                  ],
                )),
            Expanded(
                flex: flex3,
                child: Column(
                  children: [
                    lText(
                      Commons.isLinkMom() ? '${StringUtils.formatPay(data.linkmom_charge)} ${"원".tr()}' : '${StringUtils.formatPay(data.momdady_charge)} ${"원".tr()}',
                      textAlign: TextAlign.center,
                      style: st_b_15(isSelect: !isNot, disableColor: disableColor(data.pay_status!), fontWeight: FontWeight.bold),
                    ),
                    if (Commons.isLinkMom())
                      lText(
                        '${StringUtils.formatPay(data.linkmom_penalty)} ${"개".tr()}',
                        textAlign: TextAlign.center,
                        style: st_b_14(isSelect: !isNot, disableColor: disableColor(data.pay_status!), fontWeight: FontWeight.bold),
                      ),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  ///정산예정:0인 경우만 취소가 가능하다 2021/12/23 최종점검 : 76
  bool isEnable(int payStatus) {
    //2021/12/16 정산예정 외에는 선택을 못하도록 한다.
    if (payStatus >= PayStatus.compleat.value) {
      return false;
    }
    return true;
  }

  ///돌봄시작 시간으로 구분
  ///돌봄 일정이 지나지 않고, 정산예정:0인 경우만 취소가 가능하다 2021/12/23 최종점검 : 76
  bool isStartCare(String date, int payStatus) {
    //2021/12/16 정산예정 외에는 선택을 못하도록 한다.
    if (payStatus >= PayStatus.compleat.value) {
      return false;
    }
    // return true;
    // 2021/12/23
    return !Commons.isExpired(DateTime.parse(date));
  }

  ///2021/12/16 정산예정 외에는 선택을 못하도록 한다. 최종점검 : 76
  ///2022/01/10 정산예정(0)이고, 돌봄시작과 종료 시간을 보고 '돌봄시작'문구로 변경한다. 그 외 서버에서 내려 준 값으로 표시
  String payStatusTxt(String startCareDate, String endCareDate, int payStatus, String payStatusText) {
    // log.d('『GGUMBI』>>> isStartCare : payStatus: $payStatus, $startCareDate, ${Commons.isExpired(DateTime.parse(startCareDate))}, $endCareDate, ${Commons.isExpired(DateTime.parse(endCareDate))}  <<< ');
    bool isAfter = true;
    bool isBefore = true;

    if (payStatus == PayStatus.ready.value) {
      isAfter = Commons.isExpired(DateTime.parse(startCareDate));
      isBefore = Commons.isExpired(DateTime.parse(endCareDate));

      //돌봄 시작 시간이 지나고, 돌봄 종료시간이 지나지 않은 경우만 돌봄시작으로 문구 2022/01/10
      if (isAfter && !isBefore) {
        payStatusText = "돌봄시작".tr();
      }
    }
    return payStatusText;
  }

  Color disableColor(int payStatus) {
    if (payStatus >= PayStatus.cancelReady.value) {
      return color_ff3b30;
    }

    return color_cecece;
  }

  ///환불 또는 보상 문구
  String getStringPercent(int value, bool isNot) {
    return Commons.isLinkMom() ? "보상".tr() : "환불".tr();
  }
}
