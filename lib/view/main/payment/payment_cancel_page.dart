import 'dart:async';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/data/pay_cancel_save_explain_data.dart';
import 'package:linkmom/data/network/models/data/pay_refund_info_data.dart';
import 'package:linkmom/data/network/models/pay_cancel_explain_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_info_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_init_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_init_response.dart';
import 'package:linkmom/data/network/models/pay_cancel_save_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_view_request.dart';
import 'package:linkmom/data/network/models/payments_cancel_agree_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/title_toggle_icon_view.dart';
import 'package:linkmom/view/main/payment/model/refund_data.dart';
import 'package:linkmom/view/main/payment/payment_cancel_reason_page.dart';
import 'package:linkmom/view/main/payment/payment_cancel_select_page.dart';
import 'package:linkmom/view/main/payment/payment_explain_page.dart';

import 'model/peice_view_data.dart';
import 'model/service_data.dart';
import 'view/canceled_reason_view.dart';
import 'view/price_view.dart';
import 'view/refund_view.dart';
import 'view/service_view.dart';

class PaymentCancelPage extends BaseStateful {
  ///결제취소 요청 전 데이터
  final PayCancelInitRequest? payCancelInitRequest;

  ///결제취소 내역보기 데이터
  final PayCancelViewRequest? payCancelViewRequest;

  ///취소화면 세팅 (기본 돌봄취소 cancel_init)
  PayViewType? viewType;

  List<File>? images = [];

  Function? callBack;

  PaymentCancelPage({this.payCancelInitRequest, this.payCancelViewRequest, this.images, this.viewType = PayViewType.cancel_init, this.callBack});

  @override
  PaymentCancelPageState createState() => PaymentCancelPageState(payCancelInitRequest: payCancelInitRequest, payCancelViewRequest: payCancelViewRequest, images: images, viewType: viewType, callBack: callBack);
}

class PaymentCancelPageState extends BaseStatefulState<PaymentCancelPage> {
  ///결제취소 요청 전 데이터
  PayCancelInitRequest? payCancelInitRequest;

  ///결제취소 내역보기 데이터
  PayCancelViewRequest? payCancelViewRequest;

  ///취소화면 세팅 (기본 돌봄취소 cancel_init)
  PayViewType? viewType;
  List<File>? images = [];
  Function? callBack;

  PaymentCancelPageState({this.payCancelInitRequest, this.payCancelViewRequest, this.viewType = PayViewType.cancel_init, this.images, this.callBack});

  ///사전결제 요청 응답값
  PayCancelInitData? payCancelInitData;

  ///결제취소 요청 save
  PayCancelSaveRequest? payCancelSaveRequest;

  ///결제 취소 내역
  PayCanceSaveExplainData? payCancelViewData;

  ///취소 선택 값
  PayCancelInitData? payCancelSelectData;

  ///환불정보 데이터
  RefundViewData? refundViewData;

  ///실제환불 금액
  List<int> refundData = [0, 0, 0, 0];

  ///맘대디 또는 링크쌤 사유 구별
  bool? isCancelLinkMom = false;

  ///환불여부 완료 안내
  bool? isRefund = false;

  @override
  void initState() {
    super.initState();
    log.d('『GGUMBI』>>> initState : payCancelInitRequest: $payCancelInitRequest,  <<< ');
    log.d('『GGUMBI』>>> initState : images: $images,  <<< ');
    log.d('『GGUMBI』>>> initState : viewType: $viewType,  <<< ');
    if (viewType == PayViewType.cancel_init) {
      _reqeustPayCancelInit(payCancelInitRequest!);
    } else {
      _reqeustPayCancelView(payCancelViewRequest!);
    }
    onConfirmBtn();
  }

  ///돌봄취소 요청
  _reqeustPayCancelInit(PayCancelInitRequest data) async {
    try {
      flag.enableLoading(fn: () => onUpDate());
      await apiHelper.reqeustPayCancelInit(data).then((response) {
        //이미지 처리(수신 받은 url 추가)
        if (response.getCode() == KEY_SUCCESS) {
          onDataPage(response.getData());
        } else {
          Commons.showToast(response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> onSendMessage : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n$e');
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  ///돌봄취소 내역보기
  _reqeustPayCancelView(PayCancelViewRequest data) async {
    try {
      flag.enableLoading(fn: () => onUpDate());
      await apiHelper.reqeustPayCancelView(data).then((response) {
        //이미지 처리(수신 받은 url 추가)
        if (response.getCode() == KEY_SUCCESS) {
          onDataPage(response.getData());
        } else {
          Commons.showToast(response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> onSendMessage : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n$e');
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  String getTitle() {
    String title = "돌봄취소".tr();
    if (viewType == PayViewType.cancel_init) {
      title = "돌봄취소".tr();
    } else {
      title = '${"돌봄취소".tr()} ${isRefund! ? "완료".tr() : "예정".tr()}';
    }
    return title;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      ///하드웨어 back key 막기
      onWillPop: () => Future.value(viewType == PayViewType.cancel_init ? false : true),
      child: lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          appBar: appBar(
            getTitle(),
            hide: false,
            onBack: () {
              //취소사유를 종료하지 않고 넘어왔기 때문에 뒤로가기시 종료를 할 수 있도록 한다.
              Commons.pagePop(context, data: DialogAction.finish);
            },
          ),
          context: context,
          body: lContainer(
            child: Column(
              children: [
                Expanded(
                  child: lScrollView(
                    padding: padding_Item_TB,
                    child: Container(
                      child: Column(
                        children: [
                          //취소 사유 내역
                          Column(
                            children: [
                              Padding(
                                padding: padding_20_LR,
                                child: viewType == PayViewType.cancel_init
                                    ? titleToggleIconView(null, "취소사유내역".tr(),
                                        isInfo: false,
                                        flex: 0,
                                        rightWidget: InkWell(
                                            onTap: () async {
                                              var data = await Commons.page(context, routePayMentCancelReason,
                                                  arguments: PaymentCancelReasonPage(
                                                    payCancelInitRequest: PayCancelInitRequest.clone(payCancelInitRequest!),
                                                    images: images!.map((e) => e).toList(),
                                                    viewMode: ViewMode(viewType: ViewType.modify),
                                                  ));
                                              if (data != null) {
                                                PaymentCancelReasonPage result = data as PaymentCancelReasonPage;
                                                payCancelInitRequest = result.payCancelInitRequest!;
                                                images = result.images!;
                                                if (result.isChange!) {
                                                  payCancelSelectData = null;
                                                  _reqeustPayCancelInit(payCancelInitRequest!);
                                                } else {
                                                  onDataPage(result.payCancelInitRequest!);
                                                }
                                              }
                                            },
                                            child: Container(
                                                alignment: Alignment.centerRight,
                                                child: Padding(
                                                  padding: padding_20_TB,
                                                  child: Row(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Expanded(
                                                        child: lText(
                                                          getCancelTitle(),
                                                          style: st_16(textColor: payCancelInitRequest == null ? color_white : EnumUtils.getCancelSelectUserTypeColor(payCancelInitRequest!.cancelreason!), fontWeight: FontWeight.w500),
                                                          textAlign: TextAlign.right,
                                                        ),
                                                      ),
                                                      Visibility(
                                                        maintainState: true,
                                                        maintainAnimation: true,
                                                        maintainSize: true,
                                                        visible: viewType == PayViewType.cancel_init ? true : false,
                                                        child: Lcons.nav_right(color: color_999999),
                                                      ),
                                                    ],
                                                  ),
                                                ))), onClick: (action) {
                                        callBack!();
                                      })
                                    : payCancelViewData == null
                                        ? Center()
                                        : Column(
                                            children: [
                                              titleToggleIconView(
                                                null,
                                                "취소일시".tr(),
                                                isInfo: false,
                                                rightWidget: Padding(
                                                  padding: padding_20_TB,
                                                  child: lText(
                                                    payCancelViewData == null ? '' : StringUtils.getContractDate(payCancelViewData!.cancel_req_date!),
                                                    textAlign: TextAlign.right,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                              ),
                              if (!flag.isLoading) lDivider(color: color_dedede.withOpacity(0.32), thickness: 8.0, padding: padding_10_T),
                            ],
                          ),
                          //서비스신청내역
                          if (payCancelInitData != null)
                            Column(
                              children: [
                                Padding(
                                  padding: padding_20,
                                  child: careInfoView(
                                      // ServiceViewData.clone(_makeServieData()),
                                      ServiceViewData.init(
                                        btnTitle: viewType == PayViewType.cancel_init ? "취소날짜선택".tr() : "취소날짜확인".tr(),
                                        servicetype: payCancelInitData!.bookingcareservices_info!.servicetype,
                                        caredate: payCancelInitData!.bookingcareservices_info!.caredate,
                                        caretime: [payCancelInitData!.bookingcareservices_info!.stime, payCancelInitData!.bookingcareservices_info!.etime],
                                        cost_perhour: payCancelInitData!.bookingcareservices_info!.cost_perhour,
                                        cost_sum: payCancelInitData!.bookingcareservices_info!.cost_sum,
                                        durationtime: payCancelInitData!.bookingcareservices_info!.durationtime,
                                      ), callBack: () async {
                                    // bool isCancel = viewType == PayViewType.cancel_init ? true : false;
                                    if (viewType == PayViewType.cancel_init) {
                                      bool isCancel = true;
                                      //취소내역으로 보내기!
                                      if (payCancelInitData!.contract_info != null) {
                                        if (payCancelInitData!.contract_info!.contract_status == ContractStatus.contract_cancel_all.value) {
                                          isCancel = false;
                                        } else {
                                          //2022/01/18 부분취소인 경우 carediary_info.pay_status : 정산 예정[0]인 경우 & 돌봄시작전 인 경우만 취소 가능
                                          payCancelInitData!.carediary_info!.forEach((value) {
                                            if (value.pay_status == PayStatus.ready.value && !Commons.isExpired(DateTime.parse(value.start_caredate!))) {
                                              isCancel = false;
                                            }
                                          });
                                        }
                                        if (isCancel) {
                                          showNormalDlg(
                                              message: "돌봄취소가능_안내".tr(),
                                              btnLeft: "아니오".tr(),
                                              btnRight: "예".tr(),
                                              onClickAction: (a) {
                                                switch (a) {
                                                  case DialogAction.yes:
                                                    Commons.page(context, routePayMentCancelList, arguments: PayInfoRequest(contract_id: payCancelInitData!.contract_info!.contract_id!));
                                                    break;
                                                  default:
                                                }
                                              });
                                          return;
                                        }
                                      }
                                    }

                                    var data = await Commons.page(context, routePayMentCancelSelect,
                                        arguments: PaymentCancelSelectPage(
                                          payCancelSelectData: PayCancelInitData.clone(payCancelInitData!),
                                          isLinkMom: isCancelLinkMom,
                                          viewType: viewType,
                                        ));
                                    if (data != null) {
                                      payCancelSelectData = data;
                                      onDataPage(payCancelSelectData);
                                    }
                                  }),
                                ),

                                //맘대디만 결제내역을 보여준다,
                                if (!Commons.isLinkMom())
                                  Column(
                                    children: [
                                      lDivider(color: color_dedede.withOpacity(0.32), thickness: 8.0, padding: padding_10_TB),
                                      //결제내역
                                      Padding(
                                        padding: padding_Item_LR,
                                        child: priceInfoView(
                                          PriceViewData.init(
                                            title: "결제내역".tr(),
                                            cost_sum: payCancelInitData!.payment_info!.product_price,
                                            point: payCancelInitData!.payment_info!.used_point,
                                            cache: payCancelInitData!.payment_info!.used_cash,
                                            date: payCancelInitData!.payment_info!.createdate,
                                            pay_method: payCancelInitData!.payment_info!.pay_method,
                                            card_name: payCancelInitData!.payment_info!.card_name,
                                            card_number: payCancelInitData!.payment_info!.card_number,
                                            pg_provider: payCancelInitData!.payment_info!.pg_provider,
                                          ),
                                          payViewType: PayViewType.cancel_init,
                                        ),
                                      ),
                                    ],
                                  ),

                                //환불 금액, 취소내역
                                if (payCancelSelectData != null)
                                  Column(
                                    children: [
                                      lDivider(color: color_dedede.withOpacity(0.32), thickness: 8.0, padding: padding_20_TB),
                                      //환불내역
                                      Padding(
                                        padding: padding_Item_LR,
                                        child: refundInfoView(refundViewData!, payCancelSelectData!.priceTotal!, isRefund!, viewType!),
                                      ),
                                    ],
                                  ),

                                if (payCancelViewData != null && viewType != PayViewType.cancel_init)
                                  Column(
                                    children: [
                                      lDivider(color: color_dedede.withOpacity(0.32), thickness: 8.0, padding: padding_20_TB),
                                      //취소사유
                                      Padding(
                                        padding: padding_Item_LR,
                                        child: canceldReasonView(payCancelViewData!, isCancelLinkMom!, isRefund!, viewType!, callBack: (a) {
                                          if (a == DialogAction.yes) {
                                            _requestPaymentsCancelAgree();
                                          } else if (a == DialogAction.no) {
                                            //소명하러 가기
                                            Commons.page(context, routePayMentExplain,
                                                arguments: PaymentExplainPage(
                                                  explainRequest: PayCancelExplainRequest(
                                                    contract_id: payCancelViewData!.contract_info!.contract_id,
                                                    order_id: payCancelViewData!.order_id,
                                                    cancel_id: payCancelViewData!.cancel_id,
                                                  ),
                                                  callBack: (resultData) {
                                                    //소명 완료후 데이터
                                                    if (resultData != null) {
                                                      PayCanceSaveExplainData data = resultData as PayCanceSaveExplainData;
                                                      payCancelViewData = PayCanceSaveExplainData.clone(data);
                                                      if (callBack != null) {
                                                        callBack!(resultData); //채팅창으로 전달시 사용....(소명완료 메시지)
                                                      }
                                                      onUpDate();
                                                    }
                                                  },
                                                ));
                                          } else {
                                            Commons.page(context, routeCs);
                                          }
                                        }),
                                      ),
                                    ],
                                  ),
                              ],
                            ),
                        ],
                      ),
                    ),
                  ),
                ),
                _confirmButton(),
              ],
            ),
          ),
        ),
        opacity: 0.0,
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    if (viewType == PayViewType.cancel_view) {
      return Center();
    }
    log.d('『GGUMBI』>>> _confirmButton : payCancelSaveRequest: $payCancelSaveRequest,  <<< ');
    return lBtn(payCancelSelectData == null ? "취소날짜를선택해주세요".tr() : "취소신청하기".tr(), isEnabled: flag.isConfirm, onClickAction: () async {
      if (payCancelSelectData != null) {
        showNormalDlg(
            message: "취소신청하기_안내".tr(),
            btnLeft: "아니오".tr(),
            btnRight: "예".tr(),
            onClickAction: (action) async {
              if (action == DialogAction.yes) {
                await apiHelper.reqeustPayCancelSave(payCancelSaveRequest!).then((response) {
                  if (response.getCode() == KEY_SUCCESS) {
                    if (callBack != null) {
                      callBack!(response.getData()); //결제완료시 채팅으로 데이터 전달!!!
                    }
                    Commons.pageReplace(context, routePayMentCancelComplete, isPrevious: true);
                  } else {
                    Commons.showSnackBar(context, response.getMsg());
                  }
                }).catchError((e) {});
              }
            });
      }
    });
  }

  @override
  void onConfirmBtn() {
    bool isCancelSelect = false;
    if (payCancelSelectData == null) {
      isCancelSelect = false;
    } else {
      isCancelSelect = true;
    }
    log.d('『GGUMBI』>>> onConfirmBtn : isCancelSelect: $isCancelSelect, $payCancelSaveRequest,  <<< ');
    if (isCancelSelect) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  @override
  void onDataPage(_data) {
    super.onDataPage(_data);
    if (_data is PayCancelInitData || _data is PayCanceSaveExplainData) {
      log.d('『GGUMBI』>>> onDataPage : _data: $_data,  <<< ');
      if (_data is PayCanceSaveExplainData) {
        payCancelViewData = _data;
        isRefund = _data.is_refund;
        payCancelInitData = PayCancelInitData(
          contract_id: payCancelViewRequest!.contract_id,
          order_id: _data.order_id,
          bookingcareservices_info: _data.bookingcareservices_info,
          linkmom: _data.contract_info!.linkmom,
          momdady: _data.contract_info!.momdady,
          carediary_info: _data.carediary_info,
          payment_info: _data.payment_info,
        );
        //실제금액 + 포인트 + 캐시
        payCancelInitData!.payment_info!.product_price = _data.payment_info!.calcuPrice();
        payCancelInitData!.getPriceTotal(payCancelInitData!, viewType!, Commons.isLinkMom());
        // payCancelInitData!.priceTotal = payCancelInitData!.priceTotal;

        payCancelInitRequest = PayCancelInitRequest(
          contract_id: payCancelInitData!.contract_id,
          order_id: payCancelInitData!.order_id,
          cancelreason: _data.cancelreason,
          cancelreason_msg: _data.cancelreason_msg,
        );

        //환불내역 데이터
        payCancelSelectData = payCancelInitData;
      } else {
        payCancelInitData = _data;
      }

      //실제 취소 금액을 계산해서 세팅
      refundData = getRefundPointCache();
      log.d('『GGUMBI』>>> onDataPage : refundData: $refundData,  <<< ');
      refundViewData = RefundViewData.init(
        refundInfoData: PayRefundInfoData(
          refund_point: refundData[0],
          refund_cash: refundData[1],
          cancelled_price: refundData[2],
          expired_point: refundData[3],
        ),
        payCancelSelectData: payCancelSelectData,
        data: PriceViewData.init(
          title: Commons.isLinkMom() ? "취소내역".tr() : "환불내역".tr(),
          cost_sum: payCancelInitData!.payment_info!.product_price,
          point: payCancelInitData!.payment_info!.used_point,
          cache: payCancelInitData!.payment_info!.used_cash,
          date: payCancelInitData!.payment_info!.createdate,
          pay_method: payCancelInitData!.payment_info!.pay_method,
          card_name: payCancelInitData!.payment_info!.card_name,
          card_number: payCancelInitData!.payment_info!.card_number,
          pg_provider: payCancelInitData!.payment_info!.pg_provider,
        ),
      );

      payCancelSaveRequest = PayCancelSaveRequest(
        order_id: payCancelInitData!.order_id!,
        contract_id: payCancelInitData!.contract_id!,
        request_cancel_price: refundData[2],
        cancelreason: payCancelInitRequest!.cancelreason!,
        cancelreason_msg: payCancelInitRequest!.cancelreason_msg!,
        cancelreason_img: images,
        carediary_id: payCancelInitData!.getCareDiarys(),
      );

      //내 취소사유 화면 후 동작 이미지 또는 메시지(맘대디측사유에서만 변경시, 링크쌤사유에서만 변경시 데이터 업데이트)
    } else if (_data is PayCancelInitRequest) {
      payCancelSaveRequest!.cancelreason = _data.cancelreason;
      payCancelSaveRequest!.cancelreason_msg = _data.cancelreason_msg;
      payCancelSaveRequest!.cancelreason_img = images;
    }
    isCancelLinkMom = EnumUtils.getCancelSelectUserTypeBool(payCancelInitRequest!.cancelreason!);
    // //링크쌤 모드에서는 반대로 한다.
    // if (Commons.isLinkMom()) {
    //   isLinkMom = !isLinkMom!;
    // }
    log.d('『GGUMBI』>>> onDataPage : payCancelSaveRequest: $payCancelSaveRequest,  <<< ');

    onConfirmBtn();
  }

  ///취소 사유 내역 타이틀
  String getCancelTitle() {
    String value = '';
    if (payCancelInitRequest != null) {
      value = EnumUtils.getCancelSelectUserType(payCancelInitRequest!.cancelreason!);
    }
    return value;
  }

  ServiceViewData _makeServieData() {
    return ServiceViewData.init(
      btnTitle: viewType == PayViewType.cancel_init ? "취소날짜선택".tr() : "취소날짜확인".tr(),
      servicetype: payCancelInitData!.bookingcareservices_info!.servicetype,
      caredate: payCancelInitData!.bookingcareservices_info!.caredate,
      caretime: [payCancelInitData!.bookingcareservices_info!.stime, payCancelInitData!.bookingcareservices_info!.etime],
      cost_perhour: payCancelInitData!.bookingcareservices_info!.cost_perhour,
      cost_sum: payCancelInitData!.bookingcareservices_info!.cost_sum,
      durationtime: payCancelInitData!.bookingcareservices_info!.durationtime,
    );
  }

  ///포인트 < 캐시 < 카드 순으로 소진 (0:포인트, 1:캐시, 2:합걔)
  List<int> getRefundPointCache() {
    int price = 0, point = 0, expiredPoint = 0, cache = 0;

    List<int> calcuList = [];
    //취소 신청 완료내역에서는 -
    if (viewType == PayViewType.cancel_init) {
      price = payCancelInitData!.priceTotal ?? 0; //합계
      point = payCancelInitData!.refund_info!.refund_point ?? 0; //가능포인트
      expiredPoint = payCancelInitData!.refund_info!.expired_point ?? 0; //만료포인트
      cache = payCancelInitData!.refund_info!.refund_cash ?? 0; //가능캐시
      calcuList = [0, 0, price, expiredPoint];
      int valuePoint = 0;
      int valueCache = 0;
      log.d('『GGUMBI』>>> getRefundPointCache : viewType: $viewType, ${payCancelInitData!.refund_info}, price: $price, point: $point, expired_point:$expiredPoint, cache: $cache, <<< ');

      //만료포인트가 취소금액 보다 크면 0
      if (expiredPoint > price) {
        price = 0;
      } else {
        //만료예정 포인트가 있을 경우
        if (expiredPoint > 0) {
          valuePoint = point + expiredPoint;
          log.d('『GGUMBI』>>> getRefundPointCache : 만료포인트 계산 1 : $viewType, price: $price, point: $point, expired_point:$expiredPoint, cache: $cache, valuePoint: $valuePoint, valueCache:$valueCache,<<< ');
        } else {
          if (point >= price) {
            valuePoint = point - (point - price);
          } else {
            valuePoint = point;
          }
          log.d('『GGUMBI』>>> getRefundPointCache : 만료포인트 계산 2 : $viewType, price: $price, point: $point, expired_point:$expiredPoint, cache: $cache, valuePoint: $valuePoint, valueCache:$valueCache,<<< ');
        }

        //환불가능 캐시가 있을경우
        if (cache > 0) {
          log.d('『GGUMBI』>>> getRefundPointCache : 캐시 계산 0 : $viewType, price: $price, point: $point, expired_point:$expiredPoint, cache: $cache, valuePoint: $valuePoint, valueCache:$valueCache,<<< ');
          //환불가능 캐시가 (취소금액-포인트) 보가 크거나 같으면
          if (cache >= (price - valuePoint)) {
            valueCache = price - valuePoint;
            log.d('『GGUMBI』>>> getRefundPointCache : 캐시 계산 1 : $viewType, price: $price, point: $point, expired_point:$expiredPoint, cache: $cache, valuePoint: $valuePoint, valueCache:$valueCache,<<< ');
          } else {
            valueCache = cache;
            log.d('『GGUMBI』>>> getRefundPointCache : 캐시 계산 2 : $viewType, price: $price, point: $point, expired_point:$expiredPoint, cache: $cache, valuePoint: $valuePoint, valueCache:$valueCache,<<< ');
          }
        } else {
          valueCache = 0;
        }

        //포인트가 0보다 작으면 0
        if (valuePoint < 0) {
          valuePoint = 0;
        }

        //환불캐시가 0보다 작으면 0
        if (valueCache < 0) {
          valueCache = 0;
        }
      }

      calcuList[0] = valuePoint;
      calcuList[1] = valueCache;
      calcuList[2] = price - valuePoint - valueCache;
      log.d('『GGUMBI』>>> getRefundPointCache : 환불금액 : $viewType, price: $price, point: $point, expired_point:$expiredPoint, cache: $cache, valuePoint: $valuePoint, valueCache:$valueCache,<<< ');

      //취소 신청,예정은 그대로 보여준다.
    } else {
      price = payCancelViewData!.refund_info!.cancelled_price!;
      point = payCancelViewData!.refund_info!.refund_point!;
      cache = payCancelViewData!.refund_info!.refund_cash!;
      expiredPoint = payCancelViewData!.refund_info!.expired_point!;
      log.d('『GGUMBI』>>> getRefundPointCache : viewType: $viewType, price: $price, point: $point, expired_point:$expiredPoint, cache: $cache, <<< ');
      calcuList = [0, 0, price, expiredPoint];
      calcuList[0] = point;
      calcuList[1] = cache;
      calcuList[2] = price;
    }

    log.d('『GGUMBI』>>> getRefundPointCache : calcuList: $calcuList,  <<< ');
    return calcuList;
  }

  void _requestPaymentsCancelAgree() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper
          .requestPaymentsCancelAgree(PaymentsCancelAgreeRequest(
        orderId: payCancelViewData!.order_id ?? '',
        cancelId: payCancelViewData!.cancel_id ?? 0,
      ))
          .then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _reqeustPayCancelView(payCancelViewRequest!);
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        log.e('_requestPaymentsCancelAgree >>>>>>>>>>>> e');
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }
}
