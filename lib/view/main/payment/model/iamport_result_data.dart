import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/network/models/pay_save_request.dart';

@JsonSerializable()
class IamportResultData {
  static const String Key_imp_uid = 'imp_uid';
  static const String Key_merchant_uid = 'merchant_uid';
  static const String Key_error_msg = 'error_msg';
  static const String Key_data = 'data';
  static const String Key_issuccess = 'issuccess';
  static const String Key_imp_success = 'imp_success';
  static const String Key_success = 'success';

  IamportResultData({
    this.imp_uid = '',
    this.merchant_uid = '',
    this.error_msg = '결제에 실패하였습니다.',
    this.issuccess = false,
    this.paySaveRequest,
  });

  @JsonKey(name: Key_imp_uid)
  final String imp_uid;
  @JsonKey(name: Key_merchant_uid)
  final String merchant_uid;
  @JsonKey(name: Key_error_msg)
  final String error_msg;
  @JsonKey(name: Key_issuccess)
  final bool issuccess;
  @JsonKey(name: Key_data)
  PaySaveRequest? paySaveRequest;

  factory IamportResultData.fromJson(Map<String, String> json) {
    bool isSuccess = getIsSuccessed(json);
    PaySaveRequest paySaveRequest = PaySaveRequest(
      imp_uid: json[Key_imp_uid] ?? '',
      merchant_uid: json[Key_merchant_uid] ?? '',
      status: isSuccess ? 'paid' : 'ready',
    );
    return IamportResultData(
      imp_uid: paySaveRequest.imp_uid!,
      merchant_uid: paySaveRequest.merchant_uid!,
      error_msg: json[Key_error_msg] ?? '',
      issuccess: isSuccess,
      paySaveRequest: paySaveRequest,
    );
  }

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data[Key_imp_uid] = this.imp_uid;
    data[Key_merchant_uid] = this.merchant_uid;
    data[Key_error_msg] = this.error_msg;
    return data;
  }

  ///결제여부 판단
  static bool getIsSuccessed(Map<String, String> result) {
    if (result[Key_imp_success] == 'true') {
      return true;
    }
    if (result[Key_success] == 'true') {
      return true;
    }
    return false;
  }

  @override
  String toString() {
    return 'IamportResultData{imp_uid: $imp_uid, merchant_uid: $merchant_uid, error_msg: $error_msg, issuccess: $issuccess, paySaveRequest: $paySaveRequest}';
  }
}
