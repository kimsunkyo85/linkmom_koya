import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class CustomData {
  static const String Key_user_id = 'user_id';
  static const String Key_contract_id = 'contract_id';
  static const String Key_product_price = 'product_price';
  static const String Key_used_cash = 'used_cash';
  static const String Key_used_point = 'used_point';
  static const String Key_used_pg = 'used_pg';

  CustomData({
    this.user_id = 0,
    this.contract_id = 0,
    this.product_price = 0,
    this.used_cash = 0,
    this.used_point = 0,
    this.used_pg = 0,
  });

  @JsonKey(name: Key_user_id)
  final int user_id;
  @JsonKey(name: Key_contract_id)
  final int contract_id;
  @JsonKey(name: Key_product_price)
  final int product_price;
  @JsonKey(name: Key_used_cash)
  final int used_cash;
  @JsonKey(name: Key_used_point)
  final int used_point;
  @JsonKey(name: Key_used_pg)
  final int used_pg;

  factory CustomData.fromJson(Map<String, dynamic> json) {
    return CustomData(
      user_id: json[Key_user_id] as int,
      contract_id: json[Key_contract_id] as int,
      product_price: json[Key_product_price] as int,
      used_cash: json[Key_used_cash] as int,
      used_point: json[Key_used_point] as int,
      used_pg: json[Key_used_pg] as int,
    );
  }

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data[Key_user_id] = this.user_id.toString();
    data[Key_contract_id] = this.contract_id.toString();
    data[Key_product_price] = this.product_price.toString();
    data[Key_used_cash] = this.used_cash.toString();
    data[Key_used_point] = this.used_point.toString();
    data[Key_used_pg] = this.used_pg.toString();
    return data;
  }

  @override
  String toString() {
    return 'CustomData{user_id: $user_id, contract_id: $contract_id, product_price: $product_price, used_cash: $used_cash, used_point: $used_point, used_pg: $used_pg}';
  }
}
