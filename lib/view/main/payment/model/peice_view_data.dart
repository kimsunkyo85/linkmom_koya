import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/string_util.dart';

class PriceViewData {
  ///결제금액, 결제내역....
  String title;

  ///포인트
  int point;

  ///캐시
  int cache;

  ///결제금액(최종)
  int cost_sum;

  ///결제일(취소)
  String date;

  ///결제타입(신용카드,간편결제,가상계좌)
  String pay_method;

  ///pg사
  String pg_provider;

  ///결제이름(카드,은행명)
  String card_name;

  ///카드번호,은행계좌번호
  String card_number;

  PriceViewData.init({
    this.title = '',
    this.point = 0,
    this.cache = 0,
    this.cost_sum = 0,
    this.date = '',
    this.pay_method = '',
    this.pg_provider = '',
    this.card_name = '',
    this.card_number = '',
  }) {
    if (!StringUtils.validateString(title)) {
      title = "결제내역".tr();
    }

    if (!StringUtils.validateString(date)) {
      date = '';
    }
  }

  ///신청금액 - 포인트 - 캐시 = 최종결제금액
  int calcuPrice() {
    return cost_sum - point - cache;
  }

  setPoint(String point) {
    this.point = StringUtils.getStringToInt(point);
  }

  setCache(String cache) {
    this.cache = StringUtils.getStringToInt(cache);
  }

  @override
  String toString() {
    return 'PriceViewData{title: $title, point: $point, cache: $cache, cost_sum: $cost_sum, date: $date, pay_method: $pay_method, pg_provider: $pg_provider, card_name: $card_name, card_number: $card_number}';
  }
}
