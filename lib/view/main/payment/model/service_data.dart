import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/string_util.dart';

class ServiceViewData {
  ///서비스 신청내역 타이틀...기타
  String? title;

  ///하단 버튼 이름
  String? btnTitle;

  ///돌봄유형(등원,하원,보육,학습,등)
  int? servicetype;

  ///돌봄 날짜 스케쥴
  List<String>? caredate;

  ///돌봄시작,종료시간
  List<String>? caretime;

  ///돌봄 총 신청시간(분)
  int? durationtime;

  ///시급
  int? cost_perhour;

  ///결제금액(최종)
  int? cost_sum;

  bool isBtn = true;

  ServiceViewData.init({
    this.title,
    this.btnTitle,
    this.servicetype = -1,
    this.caredate,
    this.caretime,
    this.durationtime = 0,
    this.cost_perhour = 0,
    this.cost_sum = 0,
    this.isBtn = true,
  }) {
    title = "돌봄신청내역".tr();
    btnTitle = "돌봄날짜확인".tr();
    if (!StringUtils.validateString(btnTitle)) {
      btnTitle = "돌봄날짜확인".tr();
    }
    if (!isBtn) {
      btnTitle = '';
    }
  }

  ServiceViewData({
    this.title,
    this.btnTitle,
    this.servicetype = -1,
    this.caredate,
    this.caretime,
    this.durationtime = 0,
    this.cost_perhour = 0,
    this.cost_sum = 0,
    this.isBtn = true,
  });

  ServiceViewData.clone(ServiceViewData item)
      : this(
          title: item.title,
          btnTitle: item.btnTitle,
          servicetype: item.servicetype,
          caredate: item.caredate,
          caretime: item.caretime,
          durationtime: item.durationtime,
          cost_perhour: item.cost_perhour,
          cost_sum: item.cost_sum,
          isBtn: item.isBtn,
        );

  @override
  String toString() {
    return 'ServiceViewData{title: $title, btnTitle: $btnTitle, servicetype: $servicetype, caredate: $caredate, caretime: $caretime, durationtime: $durationtime, cost_perhour: $cost_perhour, cost_sum: $cost_sum, isBtn: $isBtn}';
  }
}
