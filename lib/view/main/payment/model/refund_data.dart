import 'package:linkmom/data/network/models/data/pay_refund_info_data.dart';
import 'package:linkmom/data/network/models/pay_cancel_init_response.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/view/main/payment/model/peice_view_data.dart';

import '../../../../main.dart';

class RefundViewData {
  ///환불데이터
  PayRefundInfoData? refundInfoData;

  ///결제정보
  PriceViewData? data;

  ///취소 선택 값
  PayCancelInitData? payCancelSelectData;

  RefundViewData.init({
    this.refundInfoData,
    this.data,
    this.payCancelSelectData,
  });

  ///신청금액 - 포인트 - 캐시 = 최종결제금액
  int calcuPrice(int costSum, int point, int cache) {
    return costSum - point - cache;
  }

  ///플랫폼 수수료 5% 반올림 처리
  int platformFee() {
    return (payCancelSelectData!.priceTotal! * 0.05).round();
  }

  ///전체 - 수수료 5%
  int platformFeeTotal() {
    return payCancelSelectData!.priceTotal! - platformFee();
  }

  ///취소 선택한 날짜 표시
  String getCancelDate() {
    List<String> list = [];
    payCancelSelectData!.carediary_info!.forEach((e) {
      if (e.isCheck!) {
        list.add(e.start_caredate!);
      }
    });
    return StringUtils.getCareDateCount(list);
  }

  ///취소 한 날짜 표시
  String getCancelViewDate() {
    List<String> list = [];
    log.d('『GGUMBI』>>> getCancelViewDate : : ${payCancelSelectData!.carediary_info},  <<< ');
    payCancelSelectData!.carediary_info!.forEach((e) {
      list.add(e.start_caredate!);
    });
    return StringUtils.getCareDateCount(list);
  }

  @override
  String toString() {
    return 'RefundViewData{refundInfoData: $refundInfoData, data: $data, payCancelSelectData: $payCancelSelectData}';
  }
}
