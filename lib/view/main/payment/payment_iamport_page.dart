import 'dart:convert';
import 'dart:math';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:iamport_flutter/iamport_payment.dart';
import 'package:iamport_flutter/model/payment_data.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/pay_init_response.dart';
import 'package:linkmom/data/network/models/pay_prev_request.dart';
import 'package:linkmom/data/network/models/pay_save_request.dart';
import 'package:linkmom/data/network/models/pay_save_response.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/payment/payment_complete_page.dart';

import '../../../main.dart';
import 'model/custom_data.dart';
import 'model/iamport_result_data.dart';
import 'model/peice_view_data.dart';
import 'model/service_data.dart';
import 'utils/index.dart';

class PaymentIamportPage extends BaseStateful {
  ///결제 전 데이터
  final PayInitData? payInitData;

  ///결제 데이터
  final PaymentData? payData;

  ///캐시/포인트 데이터
  final PayRrevRequest? payRrevRequest;

  PaymentIamportPage({this.payInitData, this.payData, this.payRrevRequest});

  @override
  PaymentIamportPageState createState() => PaymentIamportPageState(payInitData: payInitData, payData: payData, payRrevRequest: payRrevRequest);
}

class PaymentIamportPageState extends BaseStatefulState<PaymentIamportPage> {
  ///결제 전 데이터
  PayInitData? payInitData;

  ///결제 데이터
  PaymentData? payData;

  ///캐시/포인트 데이터
  PayRrevRequest? payRrevRequest;

  PaymentIamportPageState({this.payInitData, this.payData, this.payRrevRequest});

  ///서버로 보낼 데이터
  CustomData? customData;

  ///결제완료여부
  bool isComplete = false;

  @override
  Widget build(BuildContext context) {
    customData = CustomData(
      user_id: auth.user.id,
      contract_id: payInitData!.contract_id!,
      product_price: (payData!.amount as int) + payRrevRequest!.used_cash! + payRrevRequest!.used_point!,
      used_cash: payRrevRequest!.used_cash!,
      used_point: payRrevRequest!.used_point!,
      used_pg: payData!.amount as int,
    );
    payData!.customData = customData!.toJson();

    // TODO: GGUMBI 2021/09/27 - 임시
    // payData!.amount = 1000;
    payData!.merchantUid = '${payData!.merchantUid}|${Random().nextInt(99999)}';
    // data.appScheme = 'example';
    log.d('『GGUMBI』>>> build : ${payData!.pg}, ${payData!.appScheme}, ${Utils.getUserCodeByPg(payData!.pg!)}');
    log.d({
      'pg': payData!.pg,
      'payMethod': payData!.payMethod,
      '가맹점': Utils.getUserCodeByPg(payData!.pg!),
      'appScheme': payData!.appScheme,
      'name': payData!.name,
      'merchantUid': payData!.merchantUid,
      'amount': payData!.amount,
      'buyerName': payData!.buyerName,
      'buyerTel': payData!.buyerTel,
      'buyerEmail': payData!.buyerEmail,
      'buyerAddr': payData!.buyerAddr,
      'buyerPostcode': payData!.buyerPostcode,
      'displayCardQuota': payData!.displayCardQuota,
    });
    //결제할 금액이 0원이면
    if (payData!.payMethod == PayType.free.payMethod || payData!.amount == 0) {
      Future.delayed(Duration(seconds: 2)).then((_) {
        log.d('『GGUMBI』>>> 결제하기 - 아임포트 결제화면 - 포인트/캐시로 결제 3 - $payRrevRequest,  <<< ');
        // jsonEncode(data.jobItem.reqdata);
        PaySaveRequest request = PaySaveRequest(
          imp_uid: payRrevRequest!.order_id!,
          merchant_uid: payInitData!.order_id!,
          status: "paid",
          reqdata: jsonEncode(PaySaveReqData(
            amount: payData!.amount as int,
            custom_data: customData,
            imp_uid: payRrevRequest!.order_id!,
            merchant_uid: payRrevRequest!.order_id!,
            name: payData!.name,
            paid_at: (DateTime.now().toUtc().millisecondsSinceEpoch ~/ 1000).toString(),
            pay_method: PayType.free.string,
            status: "paid",
          ).toJson()),
        );
        log.d('『GGUMBI』>>> 결제하기 - 아임포트 결제화면 - 포인트/캐시로 결제 요청 4 - $request,  <<< ');
        apiHelper.reqeustPaySave(request).then((response) {
          log.d('『GGUMBI』>>> 결제하기 - 아임포트 결제화면 - 포인트/캐시로 결제 요청 5 - ${response.getData()},  <<< ');
          //성공이거나, 이미 결제된 내용이면 결제완료 화면으로 이동
          if (response.getCode() == KEY_SUCCESS || response.getCode() == PayError.i_6000.state) {
            goPayCompletePage(response.getData());
          } else {
            showNormalDlg(
              message: response.getMsg(),
              onClickAction: (action) {
                Commons.pagePop(context);
              },
            );
          }
        });
      });
      return Material(
        child: lContainer(
          color: color_white,
          height: heightFull(context),
          child: LoadingIndicator(
            message: "결제중입니다".tr(),
            background: color_transparent,
            style: st_b_14(fontWeight: FontWeight.w500),
          ),
        ),
      );
    }
    return Material(
      child: Container(
        color: color_white,
        height: heightFull(context),
        child: IamportPayment(
          appBar: appBar(
            "결제하기".tr(),
            hide: false,
            onBack: () {
              log.d('『GGUMBI』>>> build ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
              //1. 캐시/포인트 사전결제 하기
              //2. 취소시 캐시/포인트 사전결제 취소하기! (결제완료가아니고, 캐시나 포인트 사용을 한 경우 취소한다)
              if (!isComplete && payRrevRequest!.used_point! > 0 || !isComplete && payRrevRequest!.used_cash! > 0) {
                apiHelper.reqeustPayRrevBack(payRrevRequest!);
              }
              Commons.pagePop(context);
            },
          ),
          initialChild: Container(
            color: Colors.amberAccent,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset('assets/images/iamport-logo.png'),
                  Container(
                    padding: EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
                    child: Text('잠시만 기다려주세요...', style: TextStyle(fontSize: 20.0)),
                  ),
                ],
              ),
            ),
          ),
          userCode: Utils.getUserCodeByPg(payData!.pg!),
          data: payData!,
          callback: (Map<String, String> result) {
            // Commons.pageReplace(context, routePayMentResult, arguments: result);
            IamportResultData resultData = IamportResultData.fromJson(result);
            log.d('『GGUMBI』>>> 결제하기 - 아임포트 결제화면 - 일반결제하기 요청 55 - $resultData, ${resultData.paySaveRequest},  <<< ');
            if (resultData.issuccess) {
              apiHelper.reqeustPaySave(resultData.paySaveRequest!).then((response) {
                //성공이거나, 이미 결제된 내용이면 결제완료 화면으로 이동
                if (response.getCode() == KEY_SUCCESS || response.getCode() == PayError.i_6000.state) {
                  goPayCompletePage(response.getData());
                } else {
                  showNormalDlg(
                    message: response.getMsg(),
                    onClickAction: (action) {
                      Commons.pagePop(context);
                    },
                  );
                }
              });
            } else {
              showNormalDlg(
                message: resultData.error_msg /*'결제에 실패하였습니다'*/,
                onClickAction: (action) {
                  Commons.pagePop(context);
                },
              );
            }
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();

    //1. 캐시/포인트 사전결제 하기
    //2. 취소시 캐시/포인트 사전결제 취소하기! (결제완료가아니고, 캐시나 포인트 사용을 한 경우 취소한다)
    if (!isComplete && payRrevRequest!.used_point! > 0 || !isComplete && payRrevRequest!.used_cash! > 0) {
      apiHelper.reqeustPayRrevBack(payRrevRequest!);
    }
  }

  ///결제 완료 페이지로 이동
  goPayCompletePage(PaySaveData data) {
    log.d('『GGUMBI』>>> 결제하기 - 아임포트 결제화면 - 결제완료 화면이동 6 - $data,  <<< ');
    Commons.pageReplace(
      navigatorKey.currentContext,
      routePayMentComplete,
      arguments: PaymentCompletePage(
        payMentCompleteData: PayMentCompleteData(
          serviceViewData: ServiceViewData.init(
            title: "서비스신청내역".tr(),
            btnTitle: "돌봄날짜확인".tr(),
            servicetype: payInitData!.bookingcareservices_info!.servicetype,
            caredate: payInitData!.bookingcareservices_info!.caredate,
            caretime: [payInitData!.bookingcareservices_info!.stime, payInitData!.bookingcareservices_info!.etime],
            cost_perhour: payInitData!.bookingcareservices_info!.cost_perhour,
            cost_sum: payInitData!.bookingcareservices_info!.cost_sum,
            durationtime: payInitData!.bookingcareservices_info!.durationtime,
          ),
          priceViewData: PriceViewData.init(
            title: "결제내역".tr(),
            cost_sum: payInitData!.bookingcareservices_info!.cost_sum,
            point: data.used_point,
            cache: data.used_cash,
            pay_method: data.pay_method,
            card_name: data.card_name,
            card_number: data.card_number,
            date: data.paid_at,
            pg_provider: data.pg_provider,
          ),
        ),
      ),
      isPrevious: true,
    );
  }
}
