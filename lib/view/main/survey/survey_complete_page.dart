import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/survey/survey_page.dart';

class SurveyCompletePage extends BaseStateful {
  final DataManager? data;

  SurveyCompletePage({this.data});

  @override
  _SurveyCompleatPageState createState() => _SurveyCompleatPageState();
}

class _SurveyCompleatPageState extends BaseStatefulState<SurveyCompletePage> {
  @override
  void initState() {
    super.initState();
    onData(widget.data ?? DataManager());

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  @override
  void onData(DataManager data) {
    super.onData(data);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () => Commons.pageToMain(context, routeSurvey),
        child: lModalProgressHUD(
            flag.isLoading,
            lScaffold(
              backgroundColor: Colors.white,
              appBar: appBar("나의성향".tr(), hide: false, isBack: false, isClose: true, onClick: (a) => Commons.pageToMain(context, routeSurvey)),
              body: Column(children: [
                Container(
                    child: Expanded(
                        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Lcons.check_circle(isEnabled: true, color: color_545454, size: 72),
                  sb_h_30,
                  lText("나의성향_완료".tr(), style: st_b_20(fontWeight: FontWeight.w500), textAlign: TextAlign.center),
                  sb_h_07,
                  lText("나의성향_매칭안내".tr(), style: st_b_16(), textAlign: TextAlign.center),
                ]))),
                lBtn("확인".tr(), btnColor: color_545454, onClickAction: () {
                  Commons.pageToMain(context, routeSurvey, arguments: SurveyPage(data: data));
                }),
              ]),
            )));
  }
}
