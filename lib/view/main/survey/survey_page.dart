import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/survey_question_request.dart';
import 'package:linkmom/data/network/models/survey_list_request.dart';
import 'package:linkmom/data/network/models/survey_list_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/survey/survey_question_page.dart';

class SurveyPage extends BaseStateful {
  final DataManager data;
  SurveyPage({required this.data});

  @override
  _SurveyPageState createState() => _SurveyPageState();
}

class _SurveyPageState extends BaseStatefulState<SurveyPage> {
  List<SurveyInfoData> _requireSurveys = [];
  List<SurveyInfoData> _subSurveys = [];
  SurveyData _survey = SurveyData();
  @override
  void initState() {
    super.initState();
    onData(widget.data);
    flag.enableLoading(fn: () => onUpDate());
    _requestSurvey();
  }

  @override
  void onData(DataManager? data) {
    super.onData(data!);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            backgroundColor: color_fafafa,
            appBar: appBar("나의성향".tr(), hide: false),
            body: NextRequestListener(
              onNextRequest: () => {},
              child: lScrollView(
                padding: padding_0,
                child: Container(
                    padding: padding_20,
                    child: Column(children: [
                      Container(width: data.width(context, 1), alignment: Alignment.center, padding: padding_20_TB, child: lText("나의성향_안내".tr(), textAlign: TextAlign.start, overflow: TextOverflow.fade, style: st_b_14(textColor: color_999999, fontWeight: FontWeight.w500))),
                      Column(children: _requireSurvey(_requireSurveys)),
                      lDivider(color: color_eeeeee),
                      sb_h_30,
                      Column(mainAxisSize: MainAxisSize.min, children: _singleSubSurvey(_subSurveys)),
                    ])),
              ),
            )));
  }

  List<Widget> _requireSurvey(List<SurveyInfoData> data) {
    if (data.isEmpty) return [];
    return data.map((e) {
      return lInkWell(
          onTap: () => _startSurvey(e.id),
          child: Container(
            padding: padding_20,
            margin: padding_30_B,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(16), color: Colors.white, boxShadow: [
              BoxShadow(
                offset: Offset(0, 4),
                color: Colors.black.withAlpha(11),
                blurRadius: 12.0,
              ),
            ]),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(children: [
                  Container(
                    margin: padding_15_R,
                    width: 46,
                    height: 46,
                    padding: padding_09,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: color_e4f1f0.withAlpha(132),
                    ),
                    child: Lcons.survey_require(color: color_main, size: 30),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: padding_05_B,
                        alignment: Alignment.center,
                        padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: color_545454,
                        ),
                        child: lText("필수".tr(), style: st_11()),
                      ),
                      lText(e.surveyname, style: st_b_18(fontWeight: FontWeight.w700)),
                    ],
                  ),
                ]),
                sb_h_10,
                Container(
                    padding: padding_10_T,
                    child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                      lText(e.answer.isNotEmpty ? "다시하기".tr() : "테스트시작".tr(), style: st_b_14(textColor: color_999999, fontWeight: FontWeight.w500)),
                      sb_w_05,
                      Lcons.nav_right(size: 14, color: color_999999),
                    ])),
              ],
            ),
          ));
    }).toList();
  }

  List<Widget> _singleSubSurvey(List<SurveyInfoData> data) {
    return data.map((e) {
      return lInkWell(
          onTap: () => _startSurvey(data.first.id),
          child: Container(
            padding: padding_20,
            margin: padding_20_B,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(16), color: Colors.white, boxShadow: [
              BoxShadow(
                offset: Offset(0, 4),
                color: Colors.black.withAlpha(11),
                blurRadius: 12.0,
              ),
            ]),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(children: [
                  Container(
                    margin: padding_15_R,
                    width: 46,
                    height: 46,
                    padding: padding_09,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: color_ede6ef.withAlpha(132),
                    ),
                    child: Lcons.getSubSurvey(data.indexOf(e), color: color_linkmom),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      lText(data.first.surveyname, style: st_b_18(fontWeight: FontWeight.w700)),
                    ],
                  ),
                ]),
                sb_h_10,
                Container(
                    padding: padding_10_T,
                    child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                      lText(data.first.answer.isNotEmpty ? "다시하기".tr() : "테스트시작".tr(), style: st_b_14(textColor: color_999999, fontWeight: FontWeight.w500)),
                      sb_w_05,
                      Lcons.nav_right(size: 14, color: color_999999),
                    ])),
              ],
            ),
          ));
    }).toList();
  }

  void _startSurvey(int id) {
    try {
      SurveyQuestionRequest req = SurveyQuestionRequest(page: id);
      apiHelper.requestSurveyQuestion(req, id).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          data.myInfoItem.questions.clear();
          data.myInfoItem.questions.addAll(response.getData().results!);
          Commons.page(context, routeSurveyQuestion,
              arguments: SurveyQuestionPage(
                data: data,
                count: response.getData().count,
                sequence: data.myInfoItem.questions.first.question_seq,
                type: SurveyType.mytype,
                question: data.myInfoItem.questions,
                surveyNumber: id,
                title: data.myInfoItem.surveyInfo.results!.where((element) => element.id == id).first.surveyname),
          );
        } else {
          showNormalDlg(message: response.getMsg());
        }
      });
    } catch (e) {
    }
  }

  void _requestSurvey({bool next = false}) async {
    try {
      if (next && _survey.next.isEmpty) return;
      SurveyListRequest req = SurveyListRequest(page: 1);
      apiHelper.requestSurvey(req, next: next ? _survey.next : null).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          if (next) {
            data.myInfoItem.surveyInfo.next = response.getData().next;
            data.myInfoItem.surveyInfo.previous = response.getData().previous;
            data.myInfoItem.surveyInfo.results!.addAll(response.getData().results!);
          } else {
            data.myInfoItem.surveyInfo = response.getData();
            _requireSurveys = [];
            _subSurveys = [];
          }
          data.myInfoItem.surveyInfo.results!.forEach((element) {
            if (element.is_nar) {
              _requireSurveys.add(element);
            } else {
              _subSurveys.add(element);
            }
          });
        } else {
          showNormalDlg(message: response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}
