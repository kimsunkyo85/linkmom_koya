import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/survey_answer_request.dart';
import 'package:linkmom/data/network/models/survey_question_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/auth_center/authcenter_stress_result_page.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import 'survey_complete_page.dart';

class SurveyQuestionPage extends BaseStateful {
  final DataManager? data;
  final List<SurveyQuestionInfo> question;
  final int? sequence;
  final int? count;
  final SurveyType? type;
  final int? surveyNumber;
  final String? title;
  SurveyQuestionPage({this.data, required this.question, this.sequence = 1, this.count = 0, this.type, this.surveyNumber = 1, this.title});

  @override
  _MyTypeTestPageState createState() => _MyTypeTestPageState();
}

class _MyTypeTestPageState extends BaseStatefulState<SurveyQuestionPage> {
  List<String> _answer = [];
  String _selected = "";
  String _question = "";
  @override
  void initState() {
    super.initState();
    onData(widget.data ?? DataManager());
    if (widget.question.isNotEmpty && widget.sequence != null) {
      SurveyQuestionInfo element = widget.question[widget.sequence!-1];
      if (element.answer1.isNotEmpty) {
        _answer.add(element.answer1);
      }
      if (element.answer2.isNotEmpty) {
        _answer.add(element.answer2);
      }
      if (element.answer3.isNotEmpty) {
        _answer.add(element.answer3);
      }
      if (element.answer4.isNotEmpty) {
        _answer.add(element.answer4);
      }
      if (element.answer5.isNotEmpty) {
        _answer.add(element.answer5);
      }
    }
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      this._question = widget.question[widget.sequence! - 1].question;
      onUpDate();
    });
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data = _data;
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar(widget.title!, hide: false),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                    child: Container(
                        padding: padding_20,
                        child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                          Column(children: [
                            sb_h_20,
                            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                              lText(widget.title ?? ' ', style: st_b_18(fontWeight: FontWeight.w700)),
                              RichText(
                                  text: TextSpan(children: [
                                TextSpan(text: widget.sequence.toString(), style: st_b_14(textColor: color_545454)),
                                TextSpan(text: "/", style: st_b_14(textColor: color_cecece)),
                                TextSpan(text: widget.count.toString(), style: st_b_14(textColor: color_cecece)),
                              ]))
                            ]),
                            sb_h_15,
                            StepProgressIndicator(
                              padding: 0,
                              selectedColor: color_main,
                              unselectedColor: color_6bd5eae8,
                              roundedEdges: Radius.circular(16),
                              size: 8.0, // tickness
                              currentStep: widget.sequence!,
                              totalSteps: widget.count == null ? 8 : widget.count!,
                            )
                          ]),
                          Padding(
                            padding: padding_20_TB,
                            child: Column(
                              children: [
                                widget.type != SurveyType.mytype ? lText('Q${widget.sequence}.', style: st_b_22(textColor: color_main, fontWeight: FontWeight.w700)) : Container(),
                                sb_h_10,
                                Container(margin: padding_20, child: lText(this._question, style: st_b_20(fontWeight: FontWeight.w700), textAlign: TextAlign.center)),
                              ],
                            ),
                          ),
                          widget.type == SurveyType.mytype
                              ? Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: _answer.map((e) {
                                    return lInkWell(
                                        onTap: () {
                                          _selected = e;
                                          _addAnswer(e);
                                          onUpDate();
                                          if (!_isLast()) {
                                            Commons.page(context, routeSurveyQuestion,
                                                arguments: SurveyQuestionPage(
                                                  data: data,
                                                  question: data.myInfoItem.questions,
                                                  sequence: widget.sequence! + 1,
                                                  count: widget.count,
                                                  type: widget.type,
                                                  surveyNumber: widget.surveyNumber,
                                                  title: widget.title,
                                                ));
                                          } else {
                                            if (data.myInfoItem.surveyAnswer.length == widget.count) {
                                              flag.enableConfirm();
                                            }
                                          }
                                        },
                                        child: Container(
                                          height: 100,
                                          width: double.infinity,
                                          alignment: Alignment.center,
                                          padding: widget.type != SurveyType.mytype ? padding_15 : padding_20,
                                          margin: padding_08_TB,
                                          decoration: BoxDecoration(border: Border.all(color: _selected == e ? color_222222 : Colors.white, width: 2), borderRadius: BorderRadius.circular(16), color: Colors.white, boxShadow: [
                                            BoxShadow(
                                              offset: Offset(0, 2),
                                              color: Colors.black.withAlpha(21),
                                              blurRadius: 18.0,
                                            )
                                          ]),
                                          child: lText(e, textAlign: TextAlign.center, style: st_b_16(), overflow: TextOverflow.visible),
                                        ));
                                  }).toList())
                              : AspectRatio(
                                  aspectRatio: 300 / 100,
                                  child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: _answer.map((e) {
                                        int point = int.parse(e.replaceAll(RegExp('[^0-9]'), ''));
                                        String message;
                                        switch (point) {
                                        case 1:
                                            message = "전혀".tr();
                                            break;
                                        case 3:
                                            message = "가끔".tr();
                                            break;
                                        case 5:
                                            message = "상당히자주".tr();
                                            break;
                                          default:
                                            message = '';
                                        }
                                        return lInkWell(
                                            onTap: () {
                                              _selected = e;
                                              _addAnswer(e);
                                              onUpDate();
                                              if (!_isLast()) {
                                                Commons.page(context, routeSurveyQuestion,
                                                    arguments: SurveyQuestionPage(
                                                      data: data,
                                                      question: data.myInfoItem.questions,
                                                      sequence: widget.sequence! + 1,
                                                      count: widget.count,
                                                      type: widget.type,
                                                      surveyNumber: widget.surveyNumber,
                                                      title: widget.title,
                                                    ));
                                              } else {
                                                if (data.myInfoItem.surveyAnswer.length == widget.count) {
                                                  flag.enableConfirm();
                                                }
                                              }
                                            },
                                            child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
                                              Container(
                                                padding: padding_15,
                                                margin: padding_08,
                                                decoration: BoxDecoration(border: Border.all(color: _selected == e ? color_222222 : Colors.white, width: 2), color: Colors.white, shape: BoxShape.circle, boxShadow: [
                                                  BoxShadow(
                                                    offset: Offset(0, 2),
                                                    color: Colors.black.withAlpha(21),
                                                    blurRadius: 18.0,
                                                  )
                                                ]),
                                                child: lText(point.toString(), textAlign: TextAlign.center, style: st_b_24(fontWeight: FontWeight.w700)),
                                              ),
                                            point % 2 != 0 ? lText(message, style: st_b_14(textColor: color_b2b2b2, fontWeight: FontWeight.w700)) : lText(" "),
                                            ]));
                                      }).toList()),
                              ),
                        ]))),
                _isLast()
                    ? lBtn("저장".tr(), isEnabled: flag.isConfirm, btnColor: color_545454, onClickAction: () {
                        _requestSendAnswer();
                      })
                    : lBtn(' ', btnColor: Colors.transparent),
              ],
            )));
  }

  bool _isLast() {
    return widget.sequence == widget.count;
  }

  void _addAnswer(String answer) {
    int number = _answer.indexOf(answer) + 1;
    int index = widget.sequence! - 1;
    if (data.myInfoItem.surveyAnswer.length > widget.sequence! - 1) {
      data.myInfoItem.surveyAnswer[index] = SurveyAnswerData(question_seq: widget.sequence!, answer: number.toString());
    } else {
      data.myInfoItem.surveyAnswer.add(SurveyAnswerData(question_seq: widget.sequence!, answer: number.toString()));
    }
  }

  void _requestSendAnswer() async {
    try {
      SurveyAnswerRequest req = SurveyAnswerRequest(survey: widget.surveyNumber!, answer: data.myInfoItem.surveyAnswer);
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestSurveyAnswer(req).then((response) {
        log.i({
          '--- TITLE        ': '--------------- CALL RESPONSE ---------------',
          '--- responseJson ': response,
        });
        if (response.getCode() == KEY_SUCCESS) {
          _movePage();
        } else {
          showNormalDlg(message: response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- CALL Exception ---------------',
        '--- Exception': e.toString(),
      });
    }
  }

  void _movePage() {
    switch (widget.type) {
      case SurveyType.mytype:
        data.myInfoItem.surveyAnswer.clear();
        Commons.page(context, routeSurveyCompleat, arguments: SurveyCompletePage(data: data));
        break;
      case SurveyType.stress:
        Commons.page(context, routeAuthCenterStressResult,
            arguments: AuthcenterStressResultPage(data: data, surveyName: widget.title!));
        break;
      default:
    }
  }
}
