import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/community_list_response.dart';
import 'package:linkmom/data/network/models/index_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/manager/firebase_analytics_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/route_action.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/community/community_event_page.dart';
import 'package:linkmom/view/main/community/community_rank_page.dart';
import 'package:linkmom/view/main/community/community_view_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_view_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_write_page.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../link_page.dart';

class HomeView extends StatefulWidget {
  final Widget bottom;
  final Function onClickAction;
  final List<String> titles;
  final List<LinkmomHomeBanner>? desc;
  final USER_TYPE type;
  final String title;
  final Function onRefresh;
  final String? jobBtnTitle;

  HomeView({required this.titles, required this.bottom, required this.onClickAction, this.desc, this.type = USER_TYPE.mom_daddy, this.title = '', this.jobBtnTitle, required this.onRefresh});

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> with SingleTickerProviderStateMixin {
  late TabController _tabCtrl;
  bool _isLinkmom = false;

  @override
  void initState() {
    super.initState();
    _tabCtrl = TabController(length: widget.titles.length, vsync: this);
    _tabCtrl.index = widget.type == USER_TYPE.mom_daddy ? 0 : 1;
    _tabCtrl.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    _isLinkmom = widget.type == USER_TYPE.link_mom;
    Color color = _tabCtrl.index == 0 ? color_momdady : color_linkmom;
    return LinkmomRefresh(
        padding: padding_0,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: [
          Container(
            height: 300,
            child: lScaffold(
              appBar: AppBar(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent,
                  leading: Container(),
                  flexibleSpace: PreferredSize(
                      child: Stack(
                        alignment: Alignment.bottomCenter,
                        children: [
                          Container(
                              height: kToolbarHeight - 10,
                              child: Row(
                                  children: List.generate(
                                      widget.titles.length,
                                      (index) => Flexible(
                                              child: Container(
                                            height: kMinInteractiveDimension,
                                            decoration: BoxDecoration(
                                              color: color_dedede,
                                              borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
                                            ),
                                          ))))),
                          TabBar(
                            labelColor: color,
                            unselectedLabelColor: Colors.white,
                            labelStyle: st_16(fontWeight: FontWeight.w700),
                            unselectedLabelStyle: st_16(fontWeight: FontWeight.w700),
                            controller: _tabCtrl,
                            // onTap: (index) {
                            //   Commons.setModeChange(changeType: index == 0 ? USER_TYPE.mom_daddy : USER_TYPE.link_mom, isShowToast: false);
                            // },
                            indicator: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(color: color),
                              borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
                            ),
                            // tabs: [Tab(text: widget.titles[0]), Tab(text: widget.titles[1])],
                            tabs: List.generate(
                              widget.titles.length,
                              (index) => Tab(text: widget.titles[index]),
                            ),
                          ),
                          Container(
                              height: 1.2,
                              child: Row(
                                  children: List.generate(
                                      widget.titles.length,
                                      (index) => Flexible(
                                              child: Container(
                                            color: index == _tabCtrl.index ? Colors.white : color,
                                          ))))),
                        ],
                      ),
                      preferredSize: Size.fromHeight(kToolbarHeight))),
              body: TabBarView(controller: _tabCtrl, children: widget.desc!),
            ),
          ),
          widget.bottom
        ]),
        onRefresh: () => widget.onRefresh());
  }
}

class LinkmomHomeBanner extends StatelessWidget {
  final String title;
  final String? illust;
  final String btnTitle;
  final Function onClickAction;
  final Function onJoin;
  final bool isLinkmom;

  const LinkmomHomeBanner({required this.title, this.illust, this.btnTitle = '', required this.onClickAction, required this.onJoin, this.isLinkmom = false});

  @override
  Widget build(BuildContext context) {
    Color color = isLinkmom ? color_linkmom : color_momdady;
    return Wrap(
      children: [
        Stack(fit: StackFit.loose, children: [
          Container(
            height: 200,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fitHeight,
                alignment: FractionalOffset.centerRight,
                image: AssetImage(illust!),
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Stack(
                children: [
                  Positioned.fill(
                    top: 15,
                    right: isLinkmom ? 15 : 45,
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: Stack(
                        children: [
                          lText(
                            isLinkmom ? "잠깐".tr() : "긴급".tr(),
                            style: st_b_15(fontWeight: FontWeight.w700, height: 1.55, textColor: color),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(isLinkmom ? 10 : 10, 15, 0, 0),
                            child: Lcons.home_v(size: 25, disableColor: color),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.fromLTRB(20, 40, 20, 10),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          lText(title, style: st_b_22(fontWeight: FontWeight.w700, height: 1.55)),
                          lInkWell(
                            onTap: () => onClickAction(),
                            child: Padding(
                              padding: padding_10_T,
                              child: Lcons.page_arrow(color: color, size: 32),
                            ),
                          )
                        ],
                      )),
                ],
              ),
              if (btnTitle.isNotEmpty) lBtn(btnTitle, onClickAction: onJoin, btnColor: color),
            ],
          ),
        ]),
      ],
    );
  }
}

class JoinBtn extends StatelessWidget {
  final String title;
  final Widget? icon;
  final Function onClickAction;
  final Color color;
  final TextStyle? style;

  const JoinBtn({required this.title, this.icon, required this.onClickAction, this.color = Colors.white, this.style});

  @override
  Widget build(BuildContext context) {
    return lInkWell(
        onTap: () => lLoginCheck(context, () => onClickAction()),
        child: Container(
          padding: padding_20_TB,
          decoration: BoxDecoration(
            color: color,
            boxShadow: [BoxShadow(blurRadius: 22, offset: Offset(0, 4), color: Colors.black.withAlpha(40))],
            borderRadius: BorderRadius.circular(32),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (icon != null) Padding(padding: padding_05_R, child: icon!),
              lText(title, style: style ?? st_17(fontWeight: FontWeight.w700)),
            ],
          ),
        ));
  }
}

class EventViewer extends StatefulWidget {
  final List<EventBanner> imageSliders;
  double ratio;
  String? viewName;

  EventViewer(this.imageSliders, {this.ratio = 0, this.viewName});

  @override
  State<EventViewer> createState() => _EventViewerState();
}

class _EventViewerState extends State<EventViewer> {
  int _current = 0;
  CarouselControllerImpl _carouselCtrl = CarouselControllerImpl();

  @override
  void initState() {
    super.initState();
    widget.viewName = widget.viewName ?? routeHome;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.ratio == 0) {
      widget.ratio = Commons.getEventBannerHeight();
    }
    return Container(
        padding: padding_20_LTR,
        child: widget.imageSliders.isNotEmpty
            ? Column(children: [
                CarouselSlider(
                  carouselController: _carouselCtrl,
                  items: widget.imageSliders
                      .map((e) => lInkWell(
                          onTap: () {
                            if (e.linkUrl.isEmpty) {
                              Commons.page(context, routeCommunityEvent, arguments: CommunityEventPage(id: e.id));
                            } else if (e.linkUrl.startsWith(RouteAction.routeScheme)) {
                              RouteAction.onLink(context, e.linkUrl);
                            } else {
                              Commons.lLaunchUrl(e.linkUrl);
                            }
                            FbaManager.addLogEvent(viewName: widget.viewName, type: FbaLogType.event, parameters: {FbaLogType.click.name: e.id, EventBanner.Key_link_url: e.linkUrl});
                          },
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: CachedNetworkImage(
                                fit: BoxFit.cover,
                                imageUrl: e.bannerUrl,
                                imageBuilder: (ctx, imgProvider) => imageBuilder(imgProvider, width: 500.0),
                                placeholder: (ctx, url) => placeholder(),
                                errorWidget: (ctx, url, error) {
                                  e.bannerUrl = '';
                                  return Container(
                                    color: Commons.getColor().withOpacity(0.05),
                                    child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                                      lText("이미지를표시할수없습니다".tr(), style: st_12(textColor: Commons.getColor())),
                                    ]),
                                  );
                                },
                              ))))
                      .toList(),
                  options: CarouselOptions(
                    viewportFraction: 1,
                    aspectRatio: widget.ratio,
                    autoPlay: widget.imageSliders.length <= 1 ? false : true,
                    enlargeCenterPage: true,
                    autoPlayCurve: Curves.ease,
                    onPageChanged: (idx, reason) => setState(() => _current = idx),
                  ),
                ),
                sb_h_15,
                if (widget.imageSliders.length > 1)
                  AnimatedSmoothIndicator(
                    count: widget.imageSliders.length,
                    effect: ExpandingDotsEffect(
                      spacing: 6,
                      activeDotColor: color_b2b2b2,
                      dotColor: color_dedede,
                      dotWidth: 6,
                      dotHeight: 6,
                      radius: 8,
                    ),
                    activeIndex: _current,
                  ),
              ])
            : lEmptyView(title: null));
  }
}

class HomeRankTab extends StatefulWidget {
  final USER_TYPE type;
  final List<List<RankingData>> rankList;
  final Function onClick;

  HomeRankTab(this.type, {required this.rankList, required this.onClick}) {
    assert(rankList.length == RankType.values.length);
  }

  @override
  _HomeRankTabState createState() => _HomeRankTabState();
}

class _HomeRankTabState extends State<HomeRankTab> with SingleTickerProviderStateMixin {
  int _index = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        padding: padding_30_TB,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: padding_20_LRB,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  lText("링크쌤랭킹".tr(), style: st_b_20(fontWeight: FontWeight.w700)),
                  lIconText(title: "전체보기".tr(), icon: Lcons.nav_right(color: color_dedede), textStyle: st_14(textColor: color_999999, fontWeight: FontWeight.w500), isExpanded: false, isLeft: false, onClick: () => widget.onClick())
                ],
              ),
            ),
            Padding(
              padding: padding_20_L,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: RankType.values
                    .map((e) => Padding(
                        padding: padding_10_R,
                        child: lTextBtn(e.name,
                            radius: 20,
                            border: _index == e.index ? Border.all(color: Colors.transparent) : Border.all(color: color_eeeeee),
                            style: _index == e.index ? st_13(fontWeight: FontWeight.w700, textColor: widget.type == USER_TYPE.link_mom ? color_linkmom : color_main) : st_13(fontWeight: FontWeight.w500, textColor: color_999999),
                            bg: _index == e.index
                                ? widget.type == USER_TYPE.mom_daddy
                                    ? color_eef8f6
                                    : color_fcf4f6
                                : Colors.white,
                            padding: EdgeInsets.fromLTRB(10, 7, 10, 7),
                            onClickEvent: () => setState(() => _index = e.index))))
                    .toList(),
              ),
            ),
            widget.rankList[_index].isNotEmpty
                ? lScrollView(
                    padding: padding_20_LR,
                    scrollDirection: Axis.horizontal,
                    child: Row(
                        children: widget.rankList[_index]
                            .map((r) => lInkWell(
                                onTap: () => lLoginCheck(
                                      context,
                                      () => Commons.page(context, routeCommunityRank, arguments: CommunityRankPage(rank: widget.rankList[_index].indexOf(r) + 1, type: RankType.values[_index], id: r.userId, hcode: r.hcode)),
                                    ),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    boxShadow: [
                                      BoxShadow(
                                        offset: Offset(0, 4),
                                        color: Colors.black.withOpacity(0.08),
                                        blurRadius: 15,
                                      ),
                                    ],
                                    color: Colors.white,
                                  ),
                                  padding: EdgeInsets.fromLTRB(20, 20, 30, 20),
                                  margin: EdgeInsets.fromLTRB(0, 20, 15, 20),
                                  child: Row(
                                    children: [
                                      _index == 0
                                          ? Container(
                                              margin: padding_10,
                                              padding: padding_10_R,
                                              height: 37 * 2,
                                              child: Column(
                                                children: [
                                                  getRankImg(widget.rankList[_index].indexOf(r) + 1, radius: 45),
                                                  rankTransition(r.increase),
                                                ],
                                              ),
                                            )
                                          : Stack(alignment: Alignment.topLeft, children: [
                                              Container(margin: padding_10, child: lProfileAvatar(r.profileimg, radius: 37)),
                                              getRankImg(widget.rankList[_index].indexOf(r) + 1, radius: 34),
                                            ]),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          lText('${r.firstName} ${"링크쌤".tr()}', style: st_b_15(fontWeight: FontWeight.w700)),
                                          sb_h_05,
                                          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                            lText(r.age, style: st_13(textColor: color_999999)),
                                            Container(padding: padding_05_LR, height: 15, child: lDividerVertical(thickness: 1, color: color_dedede)),
                                            lText(r.currentRegion3depth, style: st_13(textColor: color_999999)),
                                          ]),
                                          sb_h_05,
                                          Row(children: [
                                            lText("돌봄누적소득".tr(), style: st_13(textColor: color_999999)),
                                            sb_w_10,
                                            lText('${StringUtils.formatPay(r.priceSum)}${"원".tr()}', style: st_b_13(fontWeight: FontWeight.w500)),
                                          ]),
                                          sb_h_05,
                                          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                            lInkWell(
                                              onTap: () => null,
                                              child: Row(children: [
                                                Lcons.heart(color: widget.type == USER_TYPE.link_mom ? color_linkmom : color_main, size: 15),
                                                sb_w_05,
                                                lText(r.likeCnt.toString(), style: st_b_14(fontWeight: FontWeight.w700)),
                                              ]),
                                            ),
                                            Container(margin: padding_05_L, child: lDot(size: 2, color: color_cecece)),
                                            //2022/06/16 홈화면에서 후기 이동 삭제
                                            Row(children: [
                                              TextHighlight(
                                                text: '${"후기".tr()} ${r.reviewCnt}${"개".tr()}',
                                                term: '${r.reviewCnt}',
                                                textStyle: st_b_13(),
                                                textStyleHighlight: st_b_13(fontWeight: FontWeight.w700),
                                              ),
                                              // sb_w_02,
                                              // Lcons.nav_right(size: 13, color: color_cecece),
                                            ]),
                                          ])
                                        ],
                                      )
                                    ],
                                  ),
                                )))
                            .toList()))
                : rankEmptyView(context, null, false, title: "아직등록된콘텐츠가없어요".tr(), isHome: true),
          ],
        ));
  }
}

class DiaryList extends StatelessWidget {
  final List<CareDiary> data;
  final String message;
  final Function showAll;

  const DiaryList({required this.data, this.message = '', required this.showAll});

  @override
  Widget build(BuildContext context) {
    return data.isNotEmpty
        ? Container(
            padding: padding_20,
            child: Column(children: [
              Container(
                padding: padding_10_TB,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    lIconText(
                      title: "돌봄일기".tr(),
                      textStyle: st_b_20(fontWeight: FontWeight.w700),
                      icon: Lcons.info(color: color_cecece, size: 30, isEnabled: true),
                      isExpanded: false,
                      isLeft: false,
                      onClick: () => showNormalDlg(context: context, message: message, color: Commons.getColor()),
                    ),
                    lIconText(
                      title: "전체보기".tr(),
                      icon: Lcons.nav_right(color: color_dedede),
                      textStyle: st_14(textColor: color_999999, fontWeight: FontWeight.w500),
                      isExpanded: false,
                      isLeft: false,
                      onClick: () => lLoginCheck(context, () => showAll()),
                    )
                  ],
                ),
              ),
              Container(
                  child: Column(
                      children: data
                          .map((e) => lInkWell(
                              onTap: () => lLoginCheck(context, () {
                                    if (StringUtils.parseYMDHM(e.startCaredate).isAfter(DateTime.now())) {
                                      // NOTE: Before care (not started care)
                                      showNormalDlg(message: "돌봄일기_돌봄전".tr());
                                    } else {
                                      if (Commons.isLinkMom() && StringUtils.parseYMDHM(e.endCaredate).add(Duration(hours: 12)).isAfter(DateTime.now()) && !e.isCarediary && e.payStatus < PayStatus.cancelReady.value) {
                                        /// NOTE: started care
                                        Commons.page(context, routeDiaryWrite, arguments: DiaryWritePage(id: e.scheduleId));
                                      } else {
                                        Commons.page(
                                          context,
                                          routeDiaryView,
                                          arguments: DiaryViewPage(
                                            id: e.scheduleId,
                                            type: storageHelper.user_type,
                                          ),
                                        );
                                      }
                                    }
                                  }),
                              child: Container(
                                  padding: padding_15,
                                  margin: padding_05_TB,
                                  decoration: BoxDecoration(
                                    border: Border.all(color: color_eeeeee),
                                    borderRadius: BorderRadius.circular(16),
                                  ),
                                  child: Row(children: [
                                    Container(
                                      height: 40,
                                      width: 40,
                                      padding: padding_05,
                                      margin: padding_15_R,
                                      decoration: BoxDecoration(
                                        color: careColor[EnumUtils.getCareTypeIndex(e.servicetype)],
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: Lcons.serviceType(EnumUtils.getServiceType(name: e.servicetype), color: Colors.white, isEnabled: true),
                                    ),
                                    Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                                        lText(e.servicetype, style: st_14(fontWeight: FontWeight.w700, textColor: careColor[EnumUtils.getCareTypeIndex(e.servicetype)])),
                                        sb_w_10,
                                        Row(children: [
                                          if (e.isCarediary) Container(padding: padding_05_L, child: Lcons.check(color: Commons.getColor(), isEnabled: true)),
                                          lText(!e.isCarediary ? "돌봄일기미작성".tr() : "작성완료".tr(), style: st_b_14(fontWeight: FontWeight.w700)),
                                          lText(' (${e.payStatusText})', style: st_12(fontWeight: FontWeight.w700, textColor: Commons.getColor())),
                                        ]),
                                      ]),
                                      sb_h_03,
                                      Row(children: [
                                        lText(StringUtils.parseMDE(e.endCaredate), style: st_13(textColor: color_999999)),
                                        Container(height: 15, child: lDividerVertical(color: color_cecece, thickness: 1, padding: padding_07_LR)),
                                        lText('${StringUtils.getTimeYMDHM(e.startCaredate)} ~ ${StringUtils.getTimeYMDHM(e.endCaredate)}', style: st_13(textColor: color_999999)),
                                        Container(height: 15, child: lDividerVertical(color: color_cecece, thickness: 1, padding: padding_07_LR)),
                                        lText(e.childName, style: st_13(textColor: color_999999)),
                                      ]),
                                    ])
                                  ]))))
                          .toList())),
            ]))
        : Container();
  }
}

class HomeCommunityTab extends StatefulWidget {
  final List<List<Community>> communityList;
  final Function onClick;

  HomeCommunityTab({required this.communityList, required this.onClick});

  @override
  _HomeCommunityTabState createState() => _HomeCommunityTabState();
}

class _HomeCommunityTabState extends State<HomeCommunityTab> with SingleTickerProviderStateMixin {
  int _index = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: padding_20_LRB,
        width: double.infinity,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: padding_10_B,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  lText("소통나눔".tr(), style: st_b_20(fontWeight: FontWeight.w700)),
                  lIconText(title: "전체보기".tr(), icon: Lcons.nav_right(color: color_dedede), textStyle: st_14(textColor: color_999999, fontWeight: FontWeight.w500), isExpanded: false, isLeft: false, onClick: () => widget.onClick(DialogAction.select))
                ],
              ),
            ),
            widget.communityList[_index].isNotEmpty
                ? Column(
                    mainAxisSize: MainAxisSize.max,
                    children: widget.communityList[_index]
                        .map((r) => CommunityBoard(
                              data: CommunityBoardData(
                                  communityShare: CommunityShare(
                                    id: r.id,
                                    title: r.title,
                                    content: r.content,
                                    shareStatus: r.shareStatus,
                                    updatedate: r.updatedate,
                                  ),
                                  communityShareImages: r.communityShareImages,
                                  userinfo: CommunityUserInfo(
                                    nickname: r.writerName,
                                    region3Depth: r.writeAddrName,
                                    profileimg: r.writerThumnail,
                                  )),
                            ))
                        .toList())
                : lEmptyView(
                    isIcon: false,
                    title: "등록된콘텐츠가없어요".tr(),
                    textString: "첫번째로글작성하기".tr(),
                    fn: () async {
                      if (!Commons.isGpsWithAddressCheck(context)) {
                        return;
                      }
                      var ret = await Commons.page(context, routeCommunityWrite);
                      if (ret != null) widget.onClick(DialogAction.update);
                    }),
          ],
        ));
  }
}

class CommunityBoard extends StatefulWidget {
  final CommunityBoardData data;
  final bool isMark;
  final Function? onClickMark;

  CommunityBoard({required this.data, this.isMark = false, this.onClickMark}) {
    if (isMark) assert(onClickMark != null);
  }

  @override
  State<CommunityBoard> createState() => _CommunityBoardState();
}

class _CommunityBoardState extends State<CommunityBoard> {
  @override
  Widget build(BuildContext context) {
    return lInkWell(
        onTap: () => lLoginCheck(context, () => Commons.page(context, routeCommunityView, arguments: CommunityViewPage(id: widget.data.communityShare!.id))),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            border: Border.all(color: color_eeeeee),
            color: Colors.white,
          ),
          padding: padding_20,
          margin: padding_10_TB,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  lIconText(
                    title: widget.data.userinfo!.nickname,
                    icon: lProfileAvatar(widget.data.userinfo!.profileimg, radius: 11),
                    textStyle: st_13(textColor: color_545454, fontWeight: FontWeight.w500),
                    isExpanded: false,
                  ),
                  Row(children: [
                    lText(StringUtils.parseYYMMDD(widget.data.communityShare!.updatedate), style: st_13(textColor: color_b2b2b2)),
                    Container(padding: padding_05_LR, height: 13, child: lDividerVertical(thickness: 1, color: color_dedede)),
                    lIconText(
                      title: widget.data.userinfo!.region3Depth,
                      icon: Lcons.location(color: color_999999, size: 15),
                      textStyle: st_13(textColor: color_999999),
                      padding: padding_05_R,
                      isExpanded: false,
                    ),
                    if (widget.isMark)
                      lInkWell(
                          onTap: () {
                            setState(() {
                              widget.data.communityShare!.isScrap = !widget.data.communityShare!.isScrap;
                            });
                            widget.onClickMark!(widget.data.communityShare!.id, widget.data.communityShare!.isScrap);
                          },
                          child: Lcons.scrap(
                            isEnabled: widget.data.communityShare!.isScrap,
                            color: color_ffbb56,
                            disableColor: color_b2b2b2,
                          )),
                  ]),
                ],
              ),
              sb_h_10,
              Row(children: [
                lText(widget.data.communityShare!.shareStatus, style: st_15(fontWeight: FontWeight.w700, textColor: color_00cfb5)),
                if (widget.data.communityShare!.shareStatus.isNotEmpty) sb_w_10,
                Flexible(
                  child: lText(
                    widget.data.communityShare!.title,
                    style: st_b_18(fontWeight: FontWeight.w700),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ]),
              sb_h_10,
              lText(widget.data.communityShare!.content, style: st_14(textColor: color_545454), maxLines: 3, overflow: TextOverflow.clip),
              if (widget.data.communityShareImages!.isNotEmpty)
                lScrollView(
                  scrollDirection: Axis.horizontal,
                  physics: NeverScrollableScrollPhysics(),
                  padding: padding_0,
                  child: Row(
                      children: widget.data.communityShareImages!
                          .map((e) => e.imagesThumbnail.isEmpty && e.imageThumbnail.isEmpty
                              ? Container()
                              : Container(
                                  padding: padding_10_T,
                                  margin: padding_05_R,
                                  width: 80,
                                  height: 90,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(8),
                                    child: CachedNetworkImage(
                                      imageUrl: e.imagesThumbnail.isEmpty ? e.imageThumbnail : e.imagesThumbnail,
                                      fit: BoxFit.cover,
                                    ),
                                  )))
                          .toList()),
                )
            ],
          ),
        ));
  }
}

class CsComponent extends StatelessWidget {
  final String title;
  final Widget? icon;
  final bool isLinkmom;
  final Function onClickAction;

  const CsComponent({this.title = '', this.icon, this.isLinkmom = false, required this.onClickAction});

  @override
  Widget build(BuildContext context) {
    return lInkWell(
      onTap: () => onClickAction(),
      child: Container(
          child: Column(
        children: [
          Container(
              height: 60,
              width: 60,
              margin: padding_10,
              padding: padding_15,
              decoration: BoxDecoration(
                color: isLinkmom ? color_f7f3f9 : color_f1faf8,
                borderRadius: BorderRadius.circular(50),
              ),
              child: icon ?? sb_h_30),
          lText(title, style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)),
        ],
      )),
    );
  }
}
