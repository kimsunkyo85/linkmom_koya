import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/apply_delete_request.dart';
import 'package:linkmom/data/network/models/block_requests.dart';
import 'package:linkmom/data/network/models/care_list_response.dart';
import 'package:linkmom/data/network/models/common_matching_view_request.dart';
import 'package:linkmom/data/network/models/data/chat_send_data.dart';
import 'package:linkmom/data/network/models/data/linkmom_result_data.dart';
import 'package:linkmom/data/network/models/data/matching_request_data.dart';
import 'package:linkmom/data/network/models/matching_cancel_request.dart';
import 'package:linkmom/data/network/models/matching_init_request.dart';
import 'package:linkmom/data/network/models/matching_pay_request.dart';
import 'package:linkmom/data/network/models/matching_sign_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_info_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_init_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/listview/list_momdady_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/chat/chat_room_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/momdady_profile_page.dart';
import 'package:linkmom/view/main/matchings/matching_state_view.dart';
import 'package:linkmom/view/main/notify/care_notify_page.dart';
import 'package:linkmom/view/main/payment/payment_cancel_reason_page.dart';

class LinkmomApplyPage extends BaseStateful {
  final DataManager? data;
  final String title;
  final bool isMatchingView;
  final int id;

  LinkmomApplyPage({this.data, this.title = '', this.isMatchingView = false, this.id = 0});

  @override
  _LinkmomApplyPageState createState() => _LinkmomApplyPageState();
}

class _LinkmomApplyPageState extends BaseStatefulState<LinkmomApplyPage> {
  LinkmomResultsData _data = LinkmomResultsData();
  CareStatus _careStatus = CareStatus.none;
  MatchingStatus _matchingStatus = MatchingStatus.inprogress;
  BookingStatus _bookingState = BookingStatus.inprogress;

  @override
  void initState() {
    super.initState();
    flag.enableLoading(fn: () => onUpDate());
    _requestMatchingView();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          appBar: diaryAppbar(
              title: widget.title,
              hide: false,
              rightW: IconButton(
                  onPressed: () {
                    showMenuModal(context, {
                      if (_data.care_status != 0 && _data.care_status < CareStatus.cancelAll.index) lText("돌봄취소하기".tr(), style: st_b_16(fontWeight: FontWeight.w500)): () => _moveCancelPage(_data.order_id, _data.contract_id),
                      lText("신고하기".tr(), style: st_b_16(fontWeight: FontWeight.w500)): () => _moveToNotify(_data.matching_id, _data.userinfo!.id, _data.userinfo!.first_name),
                      lText("차단하기".tr(), style: st_b_16(fontWeight: FontWeight.w500, disableColor: color_cecece, isSelect: _matchingStatus > MatchingStatus.waitPaid)): () => {if (_matchingStatus < MatchingStatus.paid) _showBlockDialog()},
                      if (widget.isMatchingView ? _matchingStatus < MatchingStatus.waitPaid : _careStatus == CareStatus.none) lText("매칭취소하기".tr(), style: st_b_16(fontWeight: FontWeight.w500)): () => _requestMatchingCancel(),
                      if (widget.isMatchingView ? _matchingStatus < MatchingStatus.waitPaid : _careStatus == CareStatus.none)
                        lText("삭제".tr(), style: st_16(textColor: color_ff3b30, fontWeight: FontWeight.w500)): () => _showDeleteDialog(_data.userinfo!.first_name),
                      if (!widget.isMatchingView && (_careStatus < CareStatus.inprogress && _careStatus > CareStatus.end)) lText("소명하기".tr(), style: st_b_16(fontWeight: FontWeight.w500)): () => _moveClaimAnswer(),
                    });
                  },
                  icon: Lcons.more())),
          backgroundColor: color_fafafa,
          body: LinkmomRefresh(
              onRefresh: () => _requestMatchingView(),
              anotherScrollView: true,
              child: lScrollView(
                physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
                padding: padding_0,
                child: _data.care_scheduleinfo != null
                    ? Container(
                        height: Commons.height(context, 0.9) - 20,
                        child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                          Expanded(
                            child: Column(
                              children: [
                                expiredView(status: _bookingState.value),
                                Container(
                                    padding: padding_20_T,
                                    child: Column(children: [
                                      lInkWell(
                                        onTap: () => Commons.page(context, routeMomDadyProfile,
                                            arguments: MomDadyProfilePage(
                                              data: data,
                                              viewMode: ViewMode(viewType: ViewType.view, itemType: ItemType.contract),
                                              btnType: BottomBtnType.not,
                                              bookingId: _data.care_scheduleinfo!.booking_id,
                                              matchingData: MatchingInitRequest(
                                                matchingStatus: _data.matching_status,
                                                receiver: _data.userinfo!.id,
                                                bookingcareservices: _data.care_scheduleinfo!.booking_id,
                                                carematching: _data.matching_id,
                                                chattingroom: _data.chatting!.chatroomId,
                                              ),
                                            )),
                                        child: Container(
                                          margin: padding_20_LRB,
                                          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16), boxShadow: [BoxShadow(blurRadius: 12, offset: Offset(0, 4), color: Colors.black.withAlpha(11))]),
                                          child: MomDadyView(
                                            CareListResultsData(
                                                booking_id: _data.care_scheduleinfo!.booking_id,
                                                cost_sum: _data.care_scheduleinfo!.cost_sum,
                                                servicetype: _data.care_scheduleinfo!.servicetype,
                                                possible_area: _data.care_scheduleinfo!.possible_area,
                                                stime: _data.care_scheduleinfo!.stime,
                                                etime: _data.care_scheduleinfo!.etime,
                                                caredate: _data.care_scheduleinfo!.caredate,
                                                userinfo: CareUserInfoData(
                                                  first_name: _data.userinfo!.first_name,
                                                  user_id: _data.userinfo!.id,
                                                  region_3depth: _data.userinfo!.region_3depth,
                                                  region_4depth: _data.userinfo!.region_4depth,
                                                  profileimg: _data.userinfo!.profileimg,
                                                  is_follower: _data.userinfo!.is_follower,
                                                ),
                                                childinfo: _data.childinfo),
                                            enableColor: Commons.getColor(),
                                            viewMode: ViewType.normal,
                                            padding: padding_20,
                                            moreBtn: Align(
                                              alignment: Alignment.topRight,
                                              child: lInkWell(
                                                onTap: () => Commons.page(context, routeChatRoom,
                                                    arguments: ChatRoomPage(
                                                      receiver: _data.userinfo!.id,
                                                      bookingcareservices: _data.care_scheduleinfo!.booking_id,
                                                      chatroom_id: _data.chatting!.chatroomId,
                                                    )),
                                                child: Padding(
                                                  padding: padding_20,
                                                  child: Lcons.chat(color: Commons.getColor(), size: 25),
                                                ),
                                              ),
                                            ),
                                            onItemClick: (item, action) {
                                              if (action == DialogAction.select) {
                                                if (_data.matching_status >= MatchingStatus.paid.value) {
                                                  Commons.page(context, routeMomDadyProfile,
                                                      arguments: MomDadyProfilePage(
                                                          viewMode: ViewMode(viewType: ViewType.view, itemType: ItemType.contract),
                                                          bookingId: _data.care_scheduleinfo!.booking_id,
                                                          btnType: BottomBtnType.not,
                                                          matchingData: MatchingInitRequest(
                                                            receiver: _data.userinfo!.id,
                                                            bookingcareservices: _data.care_scheduleinfo!.booking_id,
                                                            chattingroom: _data.chatting!.chatroomId,
                                                            matchingStatus: _data.matching_status,
                                                            carematching: _data.matching_id,
                                                          )));
                                                } else {
                                                  Commons.page(context, routeMomDadyProfile,
                                                      arguments: MomDadyProfilePage(
                                                        viewMode: ViewMode(viewType: ViewType.view),
                                                        bookingId: _data.care_scheduleinfo!.booking_id,
                                                        btnType: BottomBtnType.not,
                                                      ));
                                                }
                                              }
                                            },
                                          ),
                                        ),
                                      ),
                                      Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                        Padding(padding: padding_20_LTR, child: lText(widget.isMatchingView ? "매칭진행상황".tr() : "돌봄진행상황".tr(), style: st_b_18(fontWeight: FontWeight.w700))),
                                        MatchingStateView(
                                          state: widget.isMatchingView ? _data.matching_status : _data.care_status,
                                          type: widget.isMatchingView ? MatchingStateView.MATCHING_STATE : MatchingStateView.CARE_STATE,
                                          time: _data.paid_deadline,
                                          name: '${_data.userinfo!.first_name} ${"맘대디".tr()}',
                                          isRematch: _data.rematching,
                                        ),
                                      ]),
                                    ])),
                              ],
                            ),
                          ),
                          _buildBtn(),
                        ]))
                    : lEmptyView(title: "데이터가없습니다".tr(), height: data.height(context, 0.8)),
              )),
        ));
  }

  void _moveToNotify(int matchingId, int userId, String name) {
    List<NotifyReason> reasons = [];
    if (widget.isMatchingView) {
      reasons = [
        NotifyReason.momdadyAd,
        NotifyReason.momdadyInappropriate,
        NotifyReason.momdadyOutOfPlatform,
        NotifyReason.other,
      ];
    } else {
      reasons = [
        NotifyReason.momdadyRequestOT,
        NotifyReason.momdadyDiscountCost,
        NotifyReason.momdadyUnpaidOT,
        NotifyReason.momdadyNoshow,
        NotifyReason.momdadyOutOfPlatform,
        NotifyReason.momdadyInsult,
        NotifyReason.momdadyAssult,
        NotifyReason.momdadyAd,
        NotifyReason.momdadyInappropriate,
        NotifyReason.other,
      ];
    }
    Commons.page(context, routeCareNotify, arguments: CareNotifyPage(reasons: reasons, matchingId: matchingId, userId: userId, userName: name));
  }

  void _requestUserBlock(int userId, int matchingId) {
    try {
      BlockSaveRequest req = BlockSaveRequest(block_user: userId, block_matching: matchingId);
      apiHelper.requestBlockSave(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.pagePop(context, data: data);
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestApplyDelete(int matchingId) {
    try {
      ApplyDeleteRequest req = ApplyDeleteRequest(matching_id: matchingId.toString());
      apiHelper.requestApplyDelete(req).then((response) {
        Commons.pagePop(context, data: data);
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  Widget _buildBtn() {
    MatchingStatus status = _matchingStatus;
    if (_data.rematching) return MultiBtn(btnList: [lBtn("계약서확인후서명하기".tr(), margin: padding_0, onClickAction: () => goSignPage())]);
    if (widget.isMatchingView) {
      switch (status) {
        case MatchingStatus.inprogress:
          return MultiBtn(btnList: [lBtn("매칭요청하기".tr(), margin: padding_0, onClickAction: () => goSignPage())]);
        case MatchingStatus.waitLinkmom:
          return MultiBtn(btnList: [lBtn("서명요청하기".tr(), margin: padding_0, onClickAction: () => _requestSign(_data.userinfo!.id, _data.care_scheduleinfo!.booking_id, _data.matching_id, _data.chatting!.chatroomId))]);
        case MatchingStatus.waitMomdaddy:
          return MultiBtn(btnList: [lBtn("서명하기".tr(), margin: padding_0, onClickAction: () => goSignPage())]);
        case MatchingStatus.waitPaid:
          return MultiBtn(btnList: [lBtn("맘대디에게결제요청하기".tr(), margin: padding_0, onClickAction: () => _requestPay(_data.userinfo!.id, _data.care_scheduleinfo!.booking_id, _data.matching_id, _data.chatting!.chatroomId))]);
        case MatchingStatus.cancel:
        case MatchingStatus.cancelLinkmom:
        case MatchingStatus.cancelMomdaddy:
        case MatchingStatus.cancelPaid:
        case MatchingStatus.failDisagreeL:
        case MatchingStatus.failDisagreeM:
        case MatchingStatus.failNoRespL:
        case MatchingStatus.failNoRespM:
          return MultiBtn(btnList: [lBtn("다시매칭요청하기".tr(), margin: padding_0, onClickAction: () => goSignPage())]);
        case MatchingStatus.paid:
          return MultiBtn(btnList: [lBtn("돌봄진행상황".tr(), margin: padding_0, onClickAction: () => Commons.pageReplace(context, routeLinkmomApply, arguments: LinkmomApplyPage(data: data, title: "돌봄진행상황".tr(), isMatchingView: false, id: widget.id)))]);
        default:
          return Container();
      }
    } else {
      switch (_careStatus) {
        case CareStatus.ready:
          return MultiBtn(btnList: [
            lBtn("돌봄일기".tr(), margin: padding_0, btnColor: color_dedede, onClickAction: () {
              showNormalDlg(
                  messageWidget: TextHighlight(
                textAlign: TextAlign.center,
                text: "돌봄예정_일기안내".tr(),
                term: "돌봄예정_일기안내_1".tr(),
                textStyleHighlight: st_b_14(decoration: TextDecoration.underline),
              ));
            })
          ]);
        case CareStatus.inprogress:
        case CareStatus.end:
          return MultiBtn(btnList: [lBtn("돌봄일기".tr(), margin: padding_0, onClickAction: () => Commons.page(context, routeLinkmomDiary))]);
        case CareStatus.cancelParts:
          return MultiBtn(btnList: [lBtn("취소내역확인".tr(), margin: padding_05_R, onClickAction: () => _moveCancelHistory(_data.contract_id)), lBtn("돌봄일기".tr(), margin: padding_05_L, onClickAction: () => Commons.page(context, routeLinkmomDiary))]);
        case CareStatus.cancelAll:
          return MultiBtn(btnList: [lBtn("취소내역확인".tr(), margin: padding_0, onClickAction: () => _moveCancelHistory(_data.contract_id))]);
        default:
          return Container();
      }
    }
  }

  void _requestPay(int id, int bookingId, int matchingId, int chatroomId) {
    try {
      MatchingPayRequest req = MatchingPayRequest();
      req.requestData = MatchingRequestData(receiver: id, bookingcareservices: bookingId, carematching: matchingId);
      apiHelper.requestMatchingPay(req).then((response) {
        showNormalDlg(context: context, message: "요청이완료되었습니다".tr(), color: Commons.getColor());
      }).catchError((e) => showNormalDlg(message: "잠시후다시시도해주세요".tr()));
    } catch (e) {}
  }

  void _requestSign(int id, int bookingId, int matchingId, int chatroomId) {
    if (_bookingState.isExpire) {
      showNormalDlg(message: _bookingState.expireDlg);
      return;
    }
    try {
      MatchingSignRequest req = MatchingSignRequest();
      req.requestData = MatchingRequestData(receiver: id, bookingcareservices: bookingId, carematching: matchingId, chattingroom: chatroomId);
      apiHelper.requestMatchingSign(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          showNormalDlg(context: context, message: "요청이완료되었습니다".tr(), color: Commons.getColor());
        } else {
          showNormalDlg(context: context, message: response.getMsg(), color: Commons.getColor());
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestMatchingView() {
    try {
      CommonMatchingViewRequest req = CommonMatchingViewRequest(matchingId: widget.id);
      apiHelper.reqeustLinkmomMatchingView(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _data = response.getData();
          _careStatus = EnumUtils.getCareStatus(index: _data.care_status);
          _matchingStatus = EnumUtils.getMatchingStatus(index: _data.matching_status);
          _bookingState = EnumUtils.getBookingStatus(value: _data.care_scheduleinfo!.booking_status);
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  ///매칭 요청하기
  goSignPage() async {
    if (_bookingState.isExpire) {
      showNormalDlg(message: _bookingState.expireDlg);
      return;
    }
    ContractData? contractData = await Commons.page(context, routeMomDadyProfile,
        arguments: MomDadyProfilePage(
          data: data,
          viewMode: ViewMode(viewType: ViewType.view, itemType: ItemType.contract),
          bookingId: _data.care_scheduleinfo!.booking_id,
          btnType: BottomBtnType.contract,
          matchingData: MatchingInitRequest(
            matchingStatus: _data.matching_status,
            receiver: _data.userinfo!.id,
            bookingcareservices: _data.care_scheduleinfo!.booking_id,
            carematching: _data.matching_id,
            chattingroom: _data.chatting!.chatroomId,
          ),
        ));
    if (contractData != null) {
      _data.matching_status = contractData.matching_status;
      _data.paid_deadline = contractData.deadline;
      _requestMatchingView();
    }
  }

  void _moveCancelPage(String order, int contract) {
    showNormalDlg(
      message: '${"돌봄취소하기_안내".tr()}',
      btnLeft: "취소".tr(),
      btnRight: "돌봄취소하기".tr(),
      onClickAction: (action) {
        if (action == DialogAction.yes) {
          Commons.page(context, routePayMentCancelReason,
              arguments: PaymentCancelReasonPage(
                  payCancelInitRequest: PayCancelInitRequest(order_id: order, contract_id: contract),
                  callBack: (saveReq) {
                    _requestMatchingView();
                  }));
        }
      },
    );
  }

  void _moveCancelHistory(int id) {
    Commons.page(context, routePayMentCancelList, arguments: PayInfoRequest(contract_id: id));
  }

  void _showBlockDialog() {
    showNormalDlg(
      context: context,
      title: '${_data.userinfo!.first_name} ${"맘대디를차단하시겠습니까".tr()}',
      message: "맘대디차단_안내".tr(),
      btnLeft: "취소".tr(),
      onClickAction: (action) {
        switch (action) {
          case DialogAction.yes:
            _requestUserBlock(_data.userinfo!.id, _data.matching_id);
            break;
          default:
        }
      },
    );
  }

  void _showDeleteDialog(String name) {
    showNormalDlg(
      context: context,
      message: '$name ${"맘대디".tr()} ${"지원내역삭제".tr()}',
      btnLeft: "취소".tr(),
      onClickAction: (action) {
        switch (action) {
          case DialogAction.yes:
            _requestApplyDelete(_data.matching_id);
            break;
          default:
        }
      },
    );
  }

  void _moveClaimAnswer() {
    Commons.page(context, routeClaimAnswer);
  }

  Future<void> _requestMatchingCancel() async {
    try {
      MatchingCancelRequest requestData = MatchingCancelRequest();
      requestData.requestData = MatchingRequestData(
        receiver: _data.userinfo!.id,
        bookingcareservices: _data.care_scheduleinfo!.booking_id,
        chattingroom: _data.chatting!.chatroomId,
        carematching: _data.matching_id,
      );
      apiHelper.requestMatchingCancel(requestData).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          showNormalDlg(message: "매칭이성공적으로취소되었습니다".tr(), onClickAction: (a) => {if (a == DialogAction.yes) Commons.pagePop(context, data: true)});
        } else {
          showNormalDlg(message: response.getMsg());
        }
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> onSendMessage : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }
}
