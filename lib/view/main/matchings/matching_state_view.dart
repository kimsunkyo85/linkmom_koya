import 'package:dotted_line/dotted_line.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/data/auth_info_data.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/main/chat/chat_room_page.dart';

class MatchingStateView extends StatefulWidget {
  static const int CARE_STATE = 0;
  static const int MATCHING_STATE = 1;

  final int state;
  final double dashWidth;
  final int type;
  final String time;
  final bool fromMe, isRematch;
  final String viewName;
  final List<Function>? onClick;
  final String name;
  final Widget? btn;
  final Widget? bottom;

  MatchingStateView({
    this.state = 0,
    this.dashWidth = 50,
    this.type = CARE_STATE,
    this.time = '',
    this.fromMe = false,
    this.onClick,
    this.viewName = '',
    this.name = '',
    this.btn,
    this.bottom,
    this.isRematch = false,
  });

  @override
  _MatchingStateViewState createState() => _MatchingStateViewState();

  static List<SingleItem> getAuthInfo(AuthInfoData info) => _MatchingStateViewState.getAuthInfo(info);
}

class _MatchingStateViewState extends State<MatchingStateView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool isChatView = widget.viewName == '$ChatRoomPage';
    switch (widget.type) {
      case MatchingStateView.CARE_STATE:
        CareStatus status = EnumUtils.getCareStatus(index: widget.state);
        return Padding(
          padding: padding_0,
          child: StatusView(
            status: Status(
              isChatView: isChatView,
              statusList: _getCareStatusList(status)
                  .map((e) => Step(
                        icon: e.icon,
                        text: e.name,
                        isChecked: status == CareStatus.cancelParts ? e == CareStatus.inprogress : e == status,
                        color: status > CareStatus.cancelParts ? color_f66962 : Commons.getColor(),
                      ))
                  .toList(),
            ),
            message: isChatView
                ? ""
                : status > CareStatus.end
                    ? ""
                    : status.message,
            btn: widget.btn,
            bottom: isChatView ? Container() : widget.bottom ?? _buildCustomMessage(status, widget.name),
            isChatView: isChatView,
          ),
        );
      case MatchingStateView.MATCHING_STATE:
        MatchingStatus status = EnumUtils.getMatchingStatus(index: widget.state);
        return StatusView(
          status: Status(
              isChatView: isChatView,
              statusList: _getMatchStatusList(status)
                  .map((e) => isChatView
                      ? Step(
                          icon: e.icon,
                          text: e > MatchingStatus.success ? "맘대디".tr() : e.name,
                          subText: e == MatchingStatus.waitPaid
                              ? '${"결제대기".tr()}'
                              : e == MatchingStatus.paid
                                  ? '${"결제완료".tr()}'
                                  : e.subName.isNotEmpty
                                      ? '(${e.subName})'
                                      : '',
                          isChecked: e == status,
                          color: status > MatchingStatus.inprogress && status < MatchingStatus.waitMomdaddy ? color_f66962 : Commons.getColor(),
                        )
                      : Step(
                          icon: e.icon,
                          text: e.name,
                          isChecked: e == status,
                          subText: e.subName.isNotEmpty ? '(${e.subName})' : '',
                          color: status > MatchingStatus.inprogress && status < MatchingStatus.waitMomdaddy ? color_f66962 : Commons.getColor(),
                        ))
                  .toList(),
              current: status,
              focus: _getMatchStatusList(status).indexOf(status)),
          message: isChatView
              ? ""
              : status > MatchingStatus.fail && status < MatchingStatus.waitMomdaddy
                  ? (Commons.isLinkMom() ? "매칭이되지않았네요_링크쌤".tr() : "매칭이되지않았네요_맘대디".tr())
                  : _getMessageFromType(storageHelper.user_type, status, widget.isRematch, widget.name),
          bottom: isChatView ? Container() : widget.bottom ?? _buildCustomMessage(status, widget.name, isRematch: widget.isRematch),
          btn: widget.btn,
          isChatView: isChatView,
        );
      default:
        return Container();
    }
  }

  Widget _buildCustomMessage(dynamic status, String name, {bool isRematch = false}) {
    String subMessage = Commons.isLinkMom() ? "결제대기중일경우맘대디에게".tr() : "결제를진행해주세요".tr();
    if (status is MatchingStatus) {
      if (isRematch) {
        return Container(
          padding: padding_20_TB,
          child: Column(
            children: [
              !Commons.isLinkMom()
                  ? TextHighlight(
                      text: '${"매칭중".tr()} ${"돌봄신청서_수정_메시지_맘대디".tr()}',
                      term: '돌봄신청서_수정_메시지_맘대디_힌트'.tr(),
                      textStyle: st_b_16(textColor: color_545454, fontWeight: FontWeight.w500),
                      textStyleHighlight: st_b_16(textColor: Commons.getColor(), fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    )
                  : TextHighlight(
                      text: '${"매칭중".tr()} $name${"가".tr()}\n${"돌봄신청서_수정_메시지_링크쌤".tr()}',
                      term: '돌봄신청서_수정_메시지_링크쌤_힌트'.tr(),
                      textStyle: st_b_16(textColor: color_545454, fontWeight: FontWeight.w500),
                      textStyleHighlight: st_b_16(textColor: Commons.getColor(), fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
              if (Commons.isLinkMom())
                Column(
                  children: [
                    sb_h_10,
                    lText(
                      "돌봄신청서_수정_메시지_링크쌤_안내".tr(),
                      style: st_b_14(textColor: color_545454),
                    ),
                  ],
                ),
            ],
          ),
        );
      } else if (status == MatchingStatus.inprogress) {
        return Padding(
          padding: padding_20_TB,
          child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: [
            lText("아직매칭요청을안하셨네요".tr(), style: st_b_16(textColor: color_545454, fontWeight: FontWeight.w500)),
            sb_h_10,
            lText("상대방이마음에드셨다면".tr(), style: st_b_14(textColor: color_545454)),
          ]),
        );
      } else if (status == MatchingStatus.waitPaid) {
        return Container(
          padding: padding_20,
          child: Column(
            children: [
              lText("${StringUtils.payDeadline(widget.time, showMin: true)} ${"까지".tr()}", style: st_b_16(fontWeight: FontWeight.w700), textAlign: TextAlign.center),
              sb_h_10,
              Padding(
                padding: padding_20_LR,
                child: TextHighlight(
                  text: '${"맘대디가".tr()} ${StringUtils.payDeadline(widget.time, showMin: true)}${"까지".tr()} ${"미결제시매칭이취소됩니다".tr()}\n$subMessage',
                  term: "${StringUtils.payDeadline(widget.time, showMin: true)}${"까지".tr()}",
                  textAlign: TextAlign.center,
                  textStyle: st_14(textColor: color_545454),
                  textStyleHighlight: st_14(textColor: Commons.getColor()),
                ),
              ),
            ],
          ),
        );
      } else {
        return Container();
      }
    } else {
      if ((status as CareStatus) > CareStatus.end) {
        return Container(
            child: Wrap(alignment: WrapAlignment.center, children: [
          TextHighlight(
            text: '$name${"와".tr()} ${"예정된돌봄일정이".tr()} ${status.name}${"링크쌤_취소내역확인".tr()}',
            term: status.name,
            textStyle: st_b_16(fontWeight: FontWeight.w500),
            textStyleHighlight: st_16(fontWeight: FontWeight.w500, textColor: color_f66962),
            textAlign: TextAlign.center,
          ),
        ]));
      } else {
        return Container();
      }
    }
  }

  static List<SingleItem> getAuthInfo(AuthInfoData info) {
    List<SingleItem> items = [];
    if (info.hashtag_anmal.isNotEmpty) items.add(SingleItem(info.hashtag_anmal));
    if (info.hashtag_babysiter.isNotEmpty) items.add(SingleItem(info.hashtag_babysiter));
    if (info.hashtag_career_1.isNotEmpty) items.add(SingleItem(info.hashtag_career_1));
    if (info.hashtag_career_2.isNotEmpty) items.add(SingleItem(info.hashtag_career_2));
    if (info.hashtag_cctv.isNotEmpty) items.add(SingleItem(info.hashtag_cctv));
    if (info.hashtag_codiv_vaccine.isNotEmpty) items.add(SingleItem(info.hashtag_codiv_vaccine));
    if (info.hashtag_criminal.isNotEmpty) items.add(SingleItem(info.hashtag_criminal));
    if (info.hashtag_deungbon.isNotEmpty) items.add(SingleItem(info.hashtag_deungbon));
    if (info.hashtag_education.isNotEmpty) items.add(SingleItem(info.hashtag_education));
    if (info.hashtag_graduated.isNotEmpty) items.add(SingleItem(info.hashtag_graduated));
    if (info.hashtag_healthy.isNotEmpty) items.add(SingleItem(info.hashtag_healthy));
    if (info.hashtag_personality.isNotEmpty) items.add(SingleItem(info.hashtag_personality));
    return items;
  }

  List<MatchingStatus> _getMatchStatusList(MatchingStatus current) {
    switch (current) {
      case MatchingStatus.failNoRespL:
      case MatchingStatus.failNoRespM:
      case MatchingStatus.failDisagreeL:
      case MatchingStatus.failDisagreeM:
      case MatchingStatus.cancelLinkmom:
      case MatchingStatus.cancelMomdaddy:
        return [
          MatchingStatus.inprogress,
          current,
        ];
      case MatchingStatus.inprogress:
      case MatchingStatus.waitMomdaddy:
      case MatchingStatus.waitLinkmom:
        return [
          current,
          MatchingStatus.success,
          MatchingStatus.waitPaid,
        ];
      case MatchingStatus.cancelPaid:
      case MatchingStatus.waitPaid:
        return [
          MatchingStatus.inprogress,
          MatchingStatus.success,
          current,
        ];
      case MatchingStatus.paid:
        return [
          MatchingStatus.inprogress,
          MatchingStatus.success,
          current,
        ];
      default:
        return [];
    }
  }

  List<CareStatus> _getCareStatusList(CareStatus current) {
    switch (current) {
      case CareStatus.ready:
      case CareStatus.inprogress:
      case CareStatus.end:
      case CareStatus.cancelParts:
        return [CareStatus.ready, CareStatus.inprogress, CareStatus.end];
      case CareStatus.cancelAll:
        return [CareStatus.ready, CareStatus.cancelAll];
      default:
        return [CareStatus.ready, CareStatus.inprogress, CareStatus.end];
    }
  }

  String _getMessageFromType(USER_TYPE type, MatchingStatus status, bool isRematch, String name) {
    if (isRematch) {
      return '';
    } else if (status == MatchingStatus.waitLinkmom) {
      return type == USER_TYPE.mom_daddy ? "나의서명을기다리고있어요".tr() : "상대방의서명을기다리고있어요".tr();
    } else if (status == MatchingStatus.waitMomdaddy) {
      return type == USER_TYPE.mom_daddy ? "상대방의서명을기다리고있어요".tr() : "나의서명을기다리고있어요".tr();
    } else {
      return status.message;
    }
  }
}

class Step extends StatefulWidget {
  final bool isChecked;
  final String text;
  final String subText;
  final Widget icon;
  final Color color;

  Step({Key? key, this.isChecked = false, this.text = '', required this.icon, this.color = color_b2b2b2, this.subText = ''}) : super(key: key);

  @override
  _StepState createState() => _StepState();
}

class _StepState extends State<Step> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: [
        Container(
            width: 50,
            height: 50,
            margin: padding_02_LR,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: widget.isChecked ? widget.color.withAlpha(56) : color_b2b2b2.withAlpha(56),
            ),
            child: Container(
                margin: padding_05,
                padding: padding_05,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: widget.isChecked ? widget.color : color_cecece,
                ),
                child: widget.icon)),
        Container(
            padding: padding_05_TB,
            alignment: Alignment.center,
            child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              lText(widget.text, style: st_15(textColor: widget.isChecked ? widget.color : color_999999, fontWeight: FontWeight.w700), textAlign: TextAlign.center),
              lText(widget.subText, style: st_12(textColor: widget.isChecked ? widget.color : color_999999, fontWeight: FontWeight.w700), textAlign: TextAlign.center),
            ])),
      ]),
    );
  }
}

class Status extends StatelessWidget {
  final bool isChatView;
  final List<Step> statusList;
  final MatchingStatus current;
  final int focus;

  const Status({Key? key, required this.statusList, this.current = MatchingStatus.inprogress, this.isChatView = true, this.focus = 0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = isChatView ? MediaQuery.of(context).size.width * (statusList.length > 2 ? 0.1 : 0.25) : MediaQuery.of(context).size.width * (statusList.length > 2 ? 0.13 : 0.25);
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: statusList.map((e) {
          return Wrap(children: [
            Padding(padding: padding_05_LR, child: e),
            if (e != statusList.last)
              Container(
                  padding: padding_25_T,
                  alignment: Alignment.center,
                  width: width,
                  child: statusList.indexOf(e) > focus - 1
                      ? DottedLine(
                          lineThickness: 2,
                          dashGapLength: 4,
                          dashLength: 6,
                          dashColor: color_b2b2b2.withOpacity(0.2),
                        )
                      : lDivider(thickness: 2.0, color: color_b2b2b2.withOpacity(0.2)))
          ]);
          // return e;
        }).toList(),
      ),
    );
  }
}

class StatusView extends StatelessWidget {
  final String title;
  final Status status;
  final Widget? bottom;
  final String message;
  final Widget? btn;
  final bool isChatView;

  const StatusView({Key? key, this.title = '', required this.status, this.bottom, this.message = '', this.btn, this.isChatView = false});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(children: [
          lText(title, style: st_b_18(fontWeight: FontWeight.w700)),
          Padding(padding: isChatView ? padding_0 : padding_15_LR, child: status),
          message.isNotEmpty ? Padding(padding: padding_20, child: lText(message, style: st_14(textColor: color_545454), textAlign: TextAlign.center)) : bottom ?? Container(),
        ]),
        if (btn != null) btn!
      ],
    );
  }
}
