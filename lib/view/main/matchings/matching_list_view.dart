import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:linkmom/data/network/models/data/linkmom_result_data.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/filter_page_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/calendar/calendar_view.dart';
import 'package:linkmom/view/main/chat/chat_room_page.dart';
import 'package:linkmom/view/main/matchings/matching_filter_page.dart';

import 'linkmom_matching_profile.dart';

class MatchingListView extends StatefulWidget {
  final DataManager data;
  DateTimeRange? range = DateTimeRange(start: Jiffy(DateTime.now()).startOf(Units.MONTH).dateTime, end: Jiffy(DateTime.now()).endOf(Units.MONTH).dateTime);
  final Widget? items;
  final Function? onFilterClickAction;
  final List<FilterOption>? filter;
  final CalendarView? calendar;
  final ListType? listType;
  final int count;
  final ScrollController? controller;
  final bool isBookingjobs;

  MatchingListView({this.count = 0, required this.data, this.range, this.items, this.filter, this.calendar, this.onFilterClickAction, this.listType, this.controller, this.isBookingjobs = false});

  @override
  _MatchingListViewState createState() => _MatchingListViewState();
}

class _MatchingListViewState extends State<MatchingListView> with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    widget.data.caresItem.searchRange = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: CustomScrollView(physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()), slivers: <Widget>[
        if (widget.calendar != null) SliverToBoxAdapter(child: Container(color: Colors.white, child: widget.calendar!)),
        SliverAppBar(
          backgroundColor: color_fafafa,
          shadowColor: Colors.transparent,
          foregroundColor: Colors.transparent,
          pinned: true,
          flexibleSpace: Filter(
            count: widget.count,
            onClickAction: () {
              if (widget.onFilterClickAction != null) {
                widget.onFilterClickAction!();
              } else {
                Commons.page(context, routeMatchingFilter, arguments: MatchingFilterPage(data: widget.data, filterOptions: widget.filter!, listType: widget.listType!));
              }
            },
            range: widget.range!,
          ),
        ),
        SliverToBoxAdapter(child: Container(color: color_fafafa, child: widget.items!))
      ]),
    );
  }

  void updateRange(DateTime start, DateTime end) {
    widget.range = DateTimeRange(start: start, end: end);
  }

  void updateItems(List<Widget> items) {
    setState(() => {});
  }
}

class MatchingEmptyView extends StatelessWidget {
  final bool isLoading;
  final int type;
  final Function? onClick;
  final bool isBookingjobs;

  MatchingEmptyView({this.isLoading = false, this.type = 0, this.onClick, this.isBookingjobs = false});

  @override
  Widget build(BuildContext context) {
    String content = '';

    if (isBookingjobs) {
      content = Commons.isLinkMom() ? '${"원하는맘대디를".tr()}' : '${"마음에드는링크쌤을".tr()}';
    } else {
      content = Commons.isLinkMom() ? '${"링크쌤지원서".tr()} ${"등록하고".tr()}\n${"원하는맘대디를".tr()}' : '${"돌봄신청서".tr()} ${"등록하고".tr()}\n${"마음에드는링크쌤을".tr()}';
    }
    return AspectRatio(
      aspectRatio: Commons.ratio(4),
      child: lEmptyView(
        isLoading: isLoading,
        isIcon: false,
        title: _getTypeString(type),
        content: content,
        textString: _getBtnMessage(),
        paddingBtn: padding_50_LR,
        fn: () => onClick!(),
      ),
    );
  }

  String _getTypeString(int type) {
    switch (type) {
      case 1:
        return "내돌봄신청서가없어요".tr();
      case 2:
        return "예정된돌봄일정이없어요".tr();
      default:
        return "돌봄신청건이없어요".tr();
    }
  }

  String _getBtnMessage() {
    if (Commons.isLinkMom()) {
      if (isBookingjobs) {
        return "맘대디찾기".tr();
      } else {
        return "링크쌤지원하기".tr();
      }
    } else {
      return "메인_돌봄신청하기".tr();
    }
  }
}

class LinkmomApplyTab extends StatefulWidget {
  final ScrollController controller;
  final List<LinkmomResultsData> results;
  final String bywho;
  final Function onClickAction;
  final bool hideStatus;
  final bool isMatchingView;

  LinkmomApplyTab({Key? key, required this.controller, required this.results, this.bywho = '', required this.onClickAction, this.hideStatus = false, this.isMatchingView = true}) : super(key: key);

  @override
  _LinkmomApplyTabState createState() => _LinkmomApplyTabState();
}

class _LinkmomApplyTabState extends State<LinkmomApplyTab> {
  ScrollController controller = ScrollController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Wrap(
        children: widget.results
            .map<Widget>((e) => LinkmomMatchingProfile(
                name: e.userinfo!.first_name,
                profile: e.userinfo!.profileimg,
                price: e.care_scheduleinfo!.cost_sum,
                area: e.userinfo!.getAddress(),
                childInfo: [e.childinfo!.child_name, e.childinfo!.child_age, e.childinfo!.child_gender],
                careDate: e.care_scheduleinfo!.caredate,
                careTime: [e.care_scheduleinfo!.stime, e.care_scheduleinfo!.etime],
                careLocation: Commons.possibleAreaSubText(e.care_scheduleinfo!.possible_area),
                useHeader: true,
                onClickAction: (i) => widget.onClickAction(e),
                bookingStatus: EnumUtils.getBookingStatus(value: e.care_scheduleinfo!.booking_status),
                serviceType: EnumUtils.getServiceType(name: e.care_scheduleinfo!.servicetype),
                matchingStatus: EnumUtils.getMatchingStatusNameV(widget.hideStatus ? 0 : e.matching_status),
                chattingCnt: e.chatting!.chattingmessageCnt,
                careStatus: EnumUtils.getCareStatus(index: e.care_status),
                isMatchingView: widget.isMatchingView,
                chattingAction: () {
                  e.chatting!.chattingmessageCnt = 0;
                  Commons.page(context, routeChatRoom,
                      arguments: ChatRoomPage(
                        chatroom_id: e.chatting!.chatroomId,
                        receiver: e.userinfo!.id,
                        bookingcareservices: e.care_scheduleinfo!.booking_id,
                      ));
                }))
            .toList(),
      ),
    );
  }
}
