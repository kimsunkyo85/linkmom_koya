import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/job_profile/momdady_profile_page.dart';

import '../../../main.dart';
import '../../../route_name.dart';

class MomdadyMatchingProfile extends StatelessWidget {
  final String childName;
  final String linkmomName;
  final List<String>? careDate;
  final List<String>? careTime;
  final Function? onClickAction;
  final ServiceType serviceType;
  final CareStatus status;
  final int tome;
  final int byme;
  final int booking_id;
  final bool isMatchingView;
  final BookingStatus bookingStatus;

  MomdadyMatchingProfile({
    this.childName = ' ',
    this.careDate,
    this.careTime,
    this.onClickAction,
    this.serviceType = ServiceType.serviceType_0,
    this.status = CareStatus.ready,
    this.linkmomName = '',
    this.tome = 0,
    this.byme = 0,
    this.booking_id = 0,
    this.isMatchingView = false,
    this.bookingStatus = BookingStatus.inprogress,
  });

  @override
  Widget build(BuildContext context) {
    log.d('『GGUMBI』>>> build : EnumUtils.getBookingStatusIndex(bookingStatus!): ${bookingStatus.index}, $bookingStatus  <<< ');
    Color textColor = color_main;
    if (isMatchingView) {
      if (bookingStatus != BookingStatus.inprogress) textColor = color_999999;
    } else {
      if (status > CareStatus.cancelParts) {
        textColor = color_f66962;
      }
    }
    return lInkWell(
      onTap: () => onClickAction!(DialogAction.select, 0),
      child: Container(
        margin: padding_20_B,
        width: double.infinity,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(16), color: Colors.white, boxShadow: [BoxShadow(color: Colors.black.withAlpha(11), offset: Offset(0, 4), blurRadius: 12.0)]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: padding_20,
              child: Column(
                children: [
                  Container(
                      padding: padding_10_B,
                      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                        Row(
                          children: [
                            lTextBtn(isMatchingView ? bookingStatus.name : status.name, style: st_13(textColor: textColor, fontWeight: FontWeight.w700), bg: color_eeeeee),
                            sb_w_10,
                            lTextBtn(serviceType.string, style: st_13(fontWeight: FontWeight.w700), bg: serviceType.color),
                          ],
                        ),
                        //2021/10/18 구인중일때만 수정보기 버튼 추가, 구인중0, 기간만료8 경우만 수정할 수 있도록 한다.
                        if (EnumUtils.isBookingStatus(bookingStatus.value))
                          lIconText(
                            padding: padding_10,
                            isExpanded: false,
                            onClick: () => goMomDadyProfilePage(context, ViewType.modify, booking_id),
                            title: "수정".tr(),
                            icon: Lcons.create(color: color_b2b2b2, isEnabled: true, size: 14),
                            isLeft: false,
                            textStyle: st_14(textColor: color_b2b2b2),
                          ),
                      ])),
                  lInkWell(
                    onTap: () {
                      // goMomDadyProfilePage(context, ViewType.view, booking_id);
                      // 2021/12/17 신청서로 가던 부분을 내 신청서와 돌봄진행상황으로 이동하게끔 처리
                      onClickAction!(DialogAction.select, 0);
                    },
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            _infoView("아이정보".tr(), value: childName),
                            _infoView("돌봄날짜".tr(), value: StringUtils.getCareDateCount(careDate!)),
                            _infoView("돌봄시간".tr(), value: StringUtils.getCareTime(careTime!)),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            bookingStatus == BookingStatus.expired
                ? Container(
                    width: double.infinity,
                    height: 44,
                    color: color_main.withOpacity(0.08),
                    alignment: Alignment.center,
                    child: lText("돌봄관리_기간만료_안내".tr(), style: st_14(textColor: color_00b09a)),
                  )
                : lDivider(color: color_eeeeee, padding: padding_0),
            Padding(
              padding: padding_20,
              child: _bottomWidget(linkmomName, !isMatchingView),
            ),
          ],
        ),
      ),
    );
  }

  Widget _infoView(String title, {String value = '', List<String>? values}) {
    Widget valuesWidget = Container();
    if (values != null) {
      valuesWidget = Row(
        children: values
            .map((e) => Container(
                    child: Row(
                  children: [
                    if (values.first == e) lNameText(e) else lAutoSizeText(e, style: st_b_14()),
                    if (values.last != e) Container(height: 13, padding: padding_06_LR, child: lDividerVertical(color: color_cecece, thickness: 1.0)),
                  ],
                )))
            .toList(),
      );
    } else {
      valuesWidget = Container(child: Row(children: [lAutoSizeText(value, style: st_b_14())]));
    }
    return Row(mainAxisAlignment: MainAxisAlignment.start, children: [lText(title, style: st_b_14(textColor: color_999999)), sb_w_10, valuesWidget]);
  }

  Widget _bottomWidget(String name, bool showLinkmom) {
    if (showLinkmom) {
      return Container(
          alignment: Alignment.center,
          padding: padding_10_LR,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              lText('$name ${"링크쌤".tr()}', style: st_15(textColor: color_main, fontWeight: FontWeight.w700)),
              Lcons.person(isEnabled: true, color: Commons.getColor()),
            ],
          ));
    } else {
      return Container(
          child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          lInkWell(
            onTap: () => onClickAction!(DialogAction.select, 0),
            child: Container(
              padding: padding_10_LR,
              child: Row(children: [
                lText("나에게지원".tr(), style: st_14(textColor: color_545454)),
                sb_w_10,
                lText("$tome${"명".tr()}", style: st_14(textColor: color_main, fontWeight: FontWeight.w700)),
              ]),
            ),
          ),
          Container(alignment: Alignment.center, height: 20, child: lDividerVertical(color: color_dedede, thickness: 1)),
          lInkWell(
            onTap: () => onClickAction!(DialogAction.select, 1),
            child: Container(
              padding: padding_10_LR,
              child: Row(children: [
                lText("내가지원".tr(), style: st_14(textColor: color_545454)),
                sb_w_10,
                lText("$byme${"명".tr()}", style: st_14(textColor: color_main, fontWeight: FontWeight.w700)),
              ]),
            ),
          ),
        ],
      ));
    }
  }

  goMomDadyProfilePage(BuildContext context, ViewType viewType, int bookingId) async {
    var resultData = await Commons.page(context, routeMomDadyProfile,
        arguments: MomDadyProfilePage(
          viewMode: ViewMode(viewType: viewType),
          bookingId: bookingId,
        ));

    if (resultData != null) {
      onClickAction!(DialogAction.update, 0);
    }
  }
}
