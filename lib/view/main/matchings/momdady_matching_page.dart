import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/holidays/holidays_helper.dart';
import 'package:linkmom/data/network/models/data/matching_list_request_data.dart';
import 'package:linkmom/data/network/models/data/momdady_result_data.dart';
import 'package:linkmom/data/network/models/data/page_data.dart';
import 'package:linkmom/data/network/models/momdady_mycares_list_request.dart';
import 'package:linkmom/data/network/models/momdady_mycares_list_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/filter_page_view.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/home_provider.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/calendar/calendar_view.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/matchings/matching_list_view.dart';
import 'package:linkmom/view/main/matchings/momdady_apply_page.dart';
import 'package:linkmom/view/main/matchings/momdady_cares_page.dart';
import 'package:linkmom/view/main/matchings/momdady_matching_profile.dart';
import 'package:provider/provider.dart';

class MomdadyMatchingPage extends BaseStateful {
  final DataManager? data;
  final TabController? controller;

  MomdadyMatchingPage({this.data, this.controller});

  @override
  _MomdadyMatchingPageState createState() => _MomdadyMatchingPageState();
}

class _MomdadyMatchingPageState extends BaseStatefulState<MomdadyMatchingPage> with TickerProviderStateMixin {
  String _first = StringUtils.formatYMD(Jiffy(DateTime.now()).startOf(Units.MONTH).dateTime);
  String _last = StringUtils.formatYMD(Jiffy(DateTime.now()).endOf(Units.MONTH).dateTime);

  DateTimeRange _careRange = DateTimeRange(start: Jiffy(DateTime.now()).startOf(Units.MONTH).dateTime, end: Jiffy(DateTime.now()).endOf(Units.MONTH).dateTime);
  DateTimeRange _matchRange = DateTimeRange(start: Jiffy(DateTime.now()).startOf(Units.MONTH).dateTime, end: Jiffy(DateTime.now()).endOf(Units.MONTH).dateTime);

  PageData _caresList = PageData(results: []);
  PageData _matchList = PageData(results: []);

  ScrollController _controller = ScrollController();
  ScrollController _innerController = ScrollController();

  @override
  void initState() {
    super.initState();
    if (auth.user.username.isNotEmpty) {
      _requestMyCaresList().then((value) => _requestMatchingList());
    }
    _innerController.addListener(() {
      if (_innerController.offset != _innerController.initialScrollOffset) {
        _controller.animateTo(_controller.position.maxScrollExtent, duration: Duration(milliseconds: 50), curve: Curves.ease);
      }
      if (_innerController.offset == _innerController.initialScrollOffset) {
        _controller.animateTo(_controller.initialScrollOffset, duration: Duration(milliseconds: 50), curve: Curves.ease);
      }
    });
  }

  @override
  void dispose() {
    data.caresItem.searchRange = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<HomeNavigationProvider>(context);
    if (provider.scheduleUpdated) {
      provider.updatedEvent(TAB_SCHEDULE);
      _requestMyCaresList().then((value) => _requestMatchingList());
    }
    return TabPageView(
      tabbarName: [
        TabItem(title: "내신청서".tr()),
        TabItem(title: "돌봄진행상황".tr()),
      ],
      tabbarView: [
        lModalProgressHUD(
          flag.isLoading,
          NextRequestListener(
            onNextRequest: () => {if (_caresList.next.isNotEmpty) _requestMyCaresList(url: _caresList.next, hideLoading: true)},
            child: LinkmomRefresh(
                anotherScrollView: true,
                controller: _controller,
                padding: padding_0,
                child: MatchingListView(
                  data: data,
                  listType: ListType.mycares,
                  count: _caresList.count,
                  range: _careRange,
                  items: _buildList(_caresList.results, component: (e) => _caresComponent(e), emptyType: 1, isLoading: flag.isLoading),
                  calendar: _buildCalendar(
                    events: data.caresItem.momdadyCareEvents,
                    onCalendarChanged: _onCareCalendarChanged,
                    onSelected: (date, events) {
                      return false;
                    },
                  ),
                  filter: [
                    FilterOption(type: FilterType.bookingState, info: HireStatus.values.map((e) => FilterInfo(value: e.value, title: e.name, desc: e.desc)).toList()),
                    FilterOption(type: FilterType.serviceType, info: ServiceType.values.sublist(0, 3).map((e) => FilterInfo(value: e.value, title: e.string)).toList()),
                  ],
                ),
                onRefresh: () => _requestMyCaresList()),
          ),
          isOnlyLoading: true,
        ),
        lModalProgressHUD(
          flag.isLoading,
          NextRequestListener(
            onNextRequest: () => {if (_matchList.next.isNotEmpty) _requestMatchingList(url: _matchList.next, hideLoading: true)},
            child: LinkmomRefresh(
                anotherScrollView: true,
                controller: _controller,
                padding: padding_0,
                child: MatchingListView(
                  data: data,
                  listType: ListType.mMatching,
                  count: _matchList.count,
                  range: _matchRange,
                  items: _buildList(_matchList.results, component: (e) => _matchingComponent(e), emptyType: 2, isLoading: flag.isLoading),
                  calendar: _buildCalendar(
                    events: data.caresItem.momdadyMatchEvents,
                    onCalendarChanged: _onMatchCalendarChanged,
                    onSelected: (date, events) {
                      return false;
                    },
                  ),
                  filter: [
                    FilterOption(type: FilterType.careState, info: CareStateType.values.map((e) => FilterInfo(value: e.value, title: e.name, desc: e.desc)).toList()),
                    FilterOption(type: FilterType.serviceType, info: ServiceType.values.sublist(0, 3).map((e) => FilterInfo(value: e.value, title: e.string)).toList()),
                  ],
                ),
                onRefresh: () => _requestMatchingList()),
          ),
          isOnlyLoading: true,
        )
      ],
      floatingActionButton: FloatingActionButton(
        heroTag: USER_TYPE.mom_daddy,
        backgroundColor: Commons.getColor(),
        onPressed: () => Commons.moveApplyPage(context, data),
        child: Lcons.care_edit(color: Colors.white, size: 35),
      ),
    );
  }

  Future _requestMyCaresList({String? url, bool hideLoading = false}) async {
    try {
      if (!hideLoading) flag.enableLoading(fn: () => onUpDate());
      MomdadyMyCaresListRequest req = MomdadyMyCaresListRequest(s_date: _first, e_date: _last, status: HireStatus.values.map((e) => e.value).toList());
      await apiHelper.requestMomdadyMyCaresList(req, url: url).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          if (url != null) {
            _caresList.results.addAll(response.getData().results);
            _caresList.next = response.getData().next;
            _caresList.previous = response.getData().previous;
            _caresList.count += response.getData().count;
          } else {
            _caresList = response.getData();
          }
        } else {
          _caresList.count = 0;
          _caresList.results = [];
        }
        _caresList.results.forEach((e) => _parseEvents(EnumUtils.getServiceTypeIndex(e.servicetype), e.carescheduleinfo.caredate, data.caresItem.momdadyCareEvents));
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestMatchingList({String url = '', bool hideLoading = false}) async {
    try {
      if (!hideLoading) flag.enableLoading(fn: () => onUpDate());
      List<int> status = CareStatus.values.map((e) => e.index).toList();
      MatchingListRequest req = MatchingListRequest(s_date: _first, e_date: _last, care_status: status, bywho: 'matched');
      await apiHelper.requestMomdadyMatchingList(req, url: url.isNotEmpty ? url : null).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          if (url.isNotEmpty) {
            _matchList.results.addAll(response.getData().results);
            _matchList.next = response.getData().next;
            _matchList.previous = response.getData().previous;
            _matchList.count += response.getData().count;
          } else {
            _matchList = response.getData();
          }
        } else {
          _matchList.count = 0;
          _matchList.results = [];
        }
        _matchList.results.forEach((e) => _parseEvents(EnumUtils.getServiceTypeIndex(e.care_scheduleinfo.servicetype), e.care_scheduleinfo.caredate, data.caresItem.momdadyMatchEvents));
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _parseEvents(int id, List events, Map<DateTime, List<int>> calenderEvents) {
    // string list -> DateTime list
    events.forEach((e) {
      DateTime date = StringUtils.parseYMD(e);
      date = Jiffy(date.toLocal()).startOf(Units.DAY).dateTime;
      if (!calenderEvents.containsKey(date)) {
        calenderEvents.putIfAbsent(date, () => [id]);
      } else {
        calenderEvents.update(date, (value) {
          if (!calenderEvents[date]!.contains(id)) value.add(id);
          return value;
        });
      }
    });
  }

  Widget _caresComponent(MyCareResultsData ret) {
    return MomdadyMatchingProfile(
        childName: ret.childinfo!.child_name,
        careDate: ret.carescheduleinfo!.caredate!,
        careTime: [ret.carescheduleinfo!.stime, ret.carescheduleinfo!.etime],
        serviceType: EnumUtils.getServiceType(name: ret.servicetype),
        tome: ret.carematching_cnt!.tome_cnt,
        byme: ret.carematching_cnt!.byme_cnt,
        booking_id: ret.booking_id,
        bookingStatus: EnumUtils.getBookingStatus(value: ret.booking_status),
        isMatchingView: true,
        onClickAction: (action, index) async {
          if (DialogAction.update == action) {
            _requestMyCaresList();
          } else {
            var value = await Commons.page(context, routeMomdadyApply, arguments: MomdadyApplyPage(data: data, bookingId: ret.booking_id, info: ret, tabIndex: index));
            if (value != null) {
              _requestMyCaresList();
            }
          }
        });
  }

  Widget _matchingComponent(MomdadyResultsData ret) {
    return MomdadyMatchingProfile(
      childName: ret.childinfo!.child_name,
      careDate: ret.care_scheduleinfo!.caredate,
      careTime: [ret.care_scheduleinfo!.stime, ret.care_scheduleinfo!.etime],
      serviceType: EnumUtils.getServiceType(name: ret.care_scheduleinfo!.servicetype),
      bookingStatus: EnumUtils.getBookingStatus(name: ret.care_scheduleinfo!.status),
      status: EnumUtils.getCareStatus(index: ret.care_status),
      onClickAction: (a, index) {
        if (DialogAction.select == a) {
          Commons.page(context, routeMomdadyCares, arguments: MomdadyCaresPage(data: data, matchingId: ret.matching_id));
        }
      },
      linkmomName: ret.userinfo!.first_name,
      booking_id: ret.care_scheduleinfo!.booking_id,
      isMatchingView: false,
    );
  }

  void _updateRange(DateTime start, DateTime end) {
    _first = StringUtils.formatYMD(start);
    _last = StringUtils.formatYMD(end);
    onUpDate();
  }

  void _onMatchCalendarChanged(DateTime focused) {
    log.i("_onMatchCalendarChanged $focused");
    _matchRange = DateTimeRange(start: StringUtils.getFirstDay(focused), end: StringUtils.getLastDay(focused));
    _updateRange(_matchRange.start, _matchRange.end);
    _requestMatchingList(hideLoading: true);
  }

  void _onCareCalendarChanged(DateTime focused) {
    log.i("_onCareCalendarChanged $focused");
    _careRange = DateTimeRange(start: StringUtils.getFirstDay(focused), end: StringUtils.getLastDay(focused));
    _updateRange(_careRange.start, _careRange.end);
    _requestMyCaresList(hideLoading: true);
  }

  Widget _buildList(List<dynamic> results, {required Function component, int emptyType = 0, bool isLoading = false}) {
    log.d('『GGUMBI』>>> _buildList : results: $results,  <<< ');
    return results.isNotEmpty
        ? Padding(padding: padding_20, child: Column(children: results.map((e) => component(e)).toList().cast<Widget>()))
        : MatchingEmptyView(
            type: emptyType,
            isLoading: isLoading,
            onClick: () => Commons.moveApplyPage(context, data),
          );
  }

  CalendarView _buildCalendar({
    required bool Function(DateTime, List<int>) onSelected,
    required Map<DateTime, List<int>> events,
    required Function(DateTime) onCalendarChanged,
  }) {
    return CalendarView(
      selected: onSelected,
      colors: careColor,
      height: 50,
      events: events,
      holidays: HolidaysHelper.getHolidays,
      onCalendarChanged: onCalendarChanged,
    );
  }
}
