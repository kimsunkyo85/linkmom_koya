import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/view/lcons.dart';

class LinkmomMatchingProfile extends StatelessWidget {
  final String profile;
  final String name;
  final int price;
  final String area;
  final List<String>? childInfo;
  final List<String>? careDate;
  final List<String>? careTime;
  final String careLocation;
  final bool useHeader;
  final Function(int)? onClickAction;
  final BookingStatus bookingStatus;
  final ServiceType serviceType;
  final String matchingStatus;
  final int chattingCnt;
  final bool useShadow;
  final CareStatus careStatus;
  final Function? chattingAction;
  final bool isMatchingView;

  LinkmomMatchingProfile(
      {this.name = ' ',
      this.area = ' ',
      this.childInfo,
      this.careDate,
      this.careTime,
      this.careLocation = ' ',
      this.useHeader = true,
      this.onClickAction,
      this.bookingStatus = BookingStatus.inprogress,
      this.serviceType = ServiceType.serviceType_0,
      this.matchingStatus = ' ',
      this.chattingCnt = 0,
      this.profile = ' ',
      this.price = 0,
      this.useShadow = true,
      this.careStatus = CareStatus.none,
      this.chattingAction,
    this.isMatchingView = true,
  });

  @override
  Widget build(BuildContext context) {
    return lInkWell(
      onTap: () => onClickAction!(0),
      child: Container(
        width: double.infinity,
        height: 220,
        padding: padding_20,
        margin: padding_20_B,
        decoration: useShadow
            ? BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: Colors.white,
                boxShadow: [BoxShadow(color: Colors.black.withAlpha(11), offset: Offset(0, 4), blurRadius: 12.0)],
              )
            : BoxDecoration(color: Colors.white),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (useHeader) // header
              Expanded(
                  flex: 1,
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            isMatchingView
                                ? _textView(bookingStatus.name, color_eeeeee, Commons.getColor())
                                : _textView(careStatus != CareStatus.cancelParts ? careStatus.name : CareStatus.inprogress.name, color_eeeeee, careStatus.index > CareStatus.cancelParts.index ? color_ff3b30 : Commons.getColor()),
                            sb_w_05,
                            _textView(serviceType.string, serviceType.color, Colors.white),
                          ],
                        ),
                        Row(
                          children: [
                            if (careStatus.index == 0)
                              lText(matchingStatus,
                                  style: st_b_14(
                                      fontWeight: FontWeight.w700,
                                      textColor: EnumUtils.getMatchingStatus(name: matchingStatus).value < MatchingStatus.waitMomdaddy.value && EnumUtils.getMatchingStatus(name: matchingStatus).value > MatchingStatus.inprogress.value
                                          ? color_f66962
                                          : color_545454)),
                            Badge(
                                position: BadgePosition.topEnd(top: 5, end: 10),
                                badgeColor: color_ff4c1b,
                                showBadge: chattingCnt != 0,
                                child: IconButton(icon: Lcons.chat(color: Commons.getColor(), size: 30), visualDensity: VisualDensity(), alignment: Alignment.center, onPressed: () => chattingAction!())),
                          ],
                        )
                      ],
                    ),
                  )),
            if (useHeader) Container(child: lDivider(color: color_eeeeee), margin: padding_10_TB),
            Expanded(
                flex: 3,
                child: Container(
                  child: Row(
                    children: [
                      Expanded(
                          flex: 1,
                          child: Container(
                              padding: padding_10_R,
                              child: Column(
                                children: [
                                  lProfileAvatar(profile, radius: 30),
                                  sb_h_10,
                                  lNameText(name),
                                ],
                              ))),
                      Expanded(
                          flex: 3,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Row(children: [
                                Lcons.won(isEnabled: Commons.isLinkMom()),
                                sb_w_05,
                                lText(StringUtils.formatPay(price), style: st_16(textColor: Commons.getColor(), fontWeight: FontWeight.w700))
                              ]),
                              Row(children: [Lcons.location(color: color_545454, size: 20), lText(area, style: st_b_13(textColor: color_545454))]),
                              _infoView("아이정보".tr(), values: childInfo!, useName: true),
                              _infoView("돌봄날짜".tr(), value: StringUtils.getCareDateCount(careDate!)),
                              _infoView("돌봄시간".tr(), value: StringUtils.getCareTime(careTime!)),
                              _infoView("보육장소".tr(), value: careLocation),
                            ],
                          ))
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }

  Widget _textView(String text, Color bgColor, Color textColor) {
    return Container(decoration: BoxDecoration(color: bgColor, borderRadius: BorderRadius.circular(16)), padding: EdgeInsets.fromLTRB(10, 5, 10, 5), child: lText(text, style: st_13(textColor: textColor, fontWeight: FontWeight.w700)));
  }

  Widget _infoView(String title, {String value = ' ', List<String>? values, bool useName = false}) {
    Widget valuesWidget;
    if (useName) {
      valuesWidget = Row(
          children: values!
              .map((e) => Container(
                  child: Row(children: [if (values.first == e) lNameText(e) else lAutoSizeText(e, style: st_b_13()), if (values.last != e) Container(height: 13, padding: padding_06_LR, child: lDividerVertical(color: color_cecece, thickness: 1.0))])))
              .toList());
    } else {
      valuesWidget = lAutoSizeText(value, style: st_b_13());
    }
    return Row(mainAxisAlignment: MainAxisAlignment.start, children: [lText(title, style: st_b_13(textColor: color_999999)), sb_w_10, valuesWidget]);
  }
}
