import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/data/linkmom_result_data.dart';
import 'package:linkmom/data/network/models/data/momdady_result_data.dart';
import 'package:linkmom/data/network/models/data/page_data.dart';
import 'package:linkmom/data/network/models/linkmom_apply_list_response.dart';
import 'package:linkmom/data/network/models/linkmom_matching_list_response.dart';
import 'package:linkmom/data/network/models/momdady_matching_list_response.dart';
import 'package:linkmom/data/network/models/momdady_mycares_list_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/main/matchings/linkmom_apply_page.dart';
import 'package:linkmom/view/main/matchings/linkmom_matching_profile.dart';
import 'package:linkmom/view/main/matchings/momdady_apply_page.dart';
import 'package:linkmom/view/main/matchings/momdady_matching_profile.dart';

import '../chat/chat_room_page.dart';
import 'momdady_cares_page.dart';

class MatchingFilterResultPage extends BaseStateful {
  final DataManager data;
  final PageData result;
  final TabController? controller;
  final dynamic request;
  final ListType type;
  final String url;
  final DateTimeRange? range;

  MatchingFilterResultPage({required this.data, required this.result, this.controller, this.request, this.url = '', this.type = ListType.mMatching, this.range});

  @override
  _MatchingFilterResultPageState createState() => _MatchingFilterResultPageState();
}

class _MatchingFilterResultPageState extends BaseStatefulState<MatchingFilterResultPage> with SingleTickerProviderStateMixin {
  DateTime _first = Jiffy(DateTime.now()).startOf(Units.MONTH).dateTime;
  DateTime _last = Jiffy(DateTime.now()).endOf(Units.MONTH).dateTime;

  PageData _list = PageData(results: []);

  @override
  void initState() {
    super.initState();
    onData(widget.data);
    _list = widget.result;
  }

  @override
  void onData(DataManager _data) {
    data = _data;
    super.onData(_data);
  }

  @override
  void dispose() {
    data.caresItem.searchRange = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          backgroundColor: color_fafafa,
          appBar: appBar(Commons.isLinkMom() ? "근무관리".tr() : "돌봄관리".tr(),
              hide: false,
              isClose: true,
              onBack: () => Commons.pagePop(context),
              onClick: (action) {
                Commons.pageToMainTabMove(context, tabIndex: TAB_SCHEDULE);
              }),
          body: SliverNextListView(
            pinned: Filter(
              onClickAction: () => Commons.pagePop(context),
              range: widget.range ?? DateTimeRange(start: _first, end: _last),
              count: _list.count,
            ),
            view: _list.results.map((e) => Container(margin: _list.results.first == e ? padding_20_LTR : padding_20_LR, child: _component(e))).toList(),
            onRefresh: () => _requests(widget.url),
            onNext: () => _requests(_list.next, isNext: true),
            showNextLoading: _list.next.isNotEmpty,
            emptyView: lEmptyView(
              height: Commons.height(context, 0.7),
              textString: "다른조건으로재검색".tr(),
              fn: () => Commons.pagePop(context),
            ),
          ),
        ));
  }

  Widget _component(dynamic ret) {
    if (ret != null) {
      if (ret is LinkmomResultsData) {
        return _linkmomComponent(ret);
      } else {
        return _momdadyComponent(ret);
      }
    } else {
      return Container();
    }
  }

  Widget _momdadyComponent(dynamic ret) {
    if (ret is MomdadyResultsData) {
      // NOTE: cares list component (paid)
      return MomdadyMatchingProfile(
        childName: ret.childinfo!.child_name,
        careDate: ret.care_scheduleinfo!.caredate,
        careTime: [ret.care_scheduleinfo!.stime, ret.care_scheduleinfo!.etime],
        serviceType: EnumUtils.getServiceType(name: ret.care_scheduleinfo!.servicetype),
        status: EnumUtils.getCareStatus(index: ret.care_status),
        onClickAction: (a, index) {
          if (a == DialogAction.update) {
            _requests('');
          } else {
            Commons.page(context, routeMomdadyCares, arguments: MomdadyCaresPage(data: data, matchingId: ret.matching_id));
          }
        },
        linkmomName: ret.userinfo!.first_name,
        booking_id: ret.care_scheduleinfo!.booking_id,
        bookingStatus: EnumUtils.getBookingStatus(name: ret.care_scheduleinfo!.status),
      );
    } else {
      // NOTE: apply list component (before paid)
      return MomdadyMatchingProfile(
        childName: ret.childinfo!.child_name,
        careDate: ret.carescheduleinfo!.caredate!,
        careTime: [ret.carescheduleinfo!.stime, ret.carescheduleinfo!.etime],
        serviceType: EnumUtils.getServiceType(name: ret.servicetype),
        onClickAction: (a, index) async {
          if (a == DialogAction.update) {
            _requests('');
          } else {
            var resultData = await Commons.page(context, routeMomdadyApply, arguments: MomdadyApplyPage(data: data, bookingId: ret.booking_id, tabIndex: index, info: ret));
            if (resultData != null) {
              _requests('');
            }
          }
        },
        booking_id: ret.booking_id,
        bookingStatus: EnumUtils.getBookingStatus(value: ret.booking_status),
        isMatchingView: true,
        tome: ret.carematching_cnt!.tome_cnt,
        byme: ret.carematching_cnt!.byme_cnt,
      );
    }
  }

  Widget _linkmomComponent(LinkmomResultsData ret) {
    return LinkmomMatchingProfile(
      name: ret.userinfo!.first_name,
      profile: ret.userinfo!.profileimg,
      price: ret.care_scheduleinfo!.cost_sum,
      area: ret.userinfo!.getAddress(),
      childInfo: [ret.childinfo!.child_name, ret.childinfo!.child_age, ret.childinfo!.child_gender],
      careDate: ret.care_scheduleinfo!.caredate,
      careTime: [ret.care_scheduleinfo!.stime, ret.care_scheduleinfo!.etime],
      careLocation: Commons.possibleAreaSubText(ret.care_scheduleinfo!.possible_area),
      useHeader: true,
      onClickAction: (index) => _showProfile(ret.matching_id, ret.bywho!.is_tome, ret.care_status == 0),
      bookingStatus: EnumUtils.getBookingStatus(value: ret.care_scheduleinfo!.booking_status),
      serviceType: EnumUtils.getServiceType(name: ret.care_scheduleinfo!.servicetype),
      matchingStatus: _getMatchingStateName(ret.matching_status),
      chattingCnt: ret.chatting!.chattingmessageCnt,
      chattingAction: () => Commons.page(context, routeChatRoom,
          arguments: ChatRoomPage(
            receiver: ret.userinfo!.id,
            bookingcareservices: ret.care_scheduleinfo!.booking_id,
            chatroom_id: ret.chatting!.chatroomId,
          )),
    );
  }

  String _getMatchingStateName(int value) {
    String ret = '';
    MatchingStatus.values.forEach((element) {
      if (element.value == value) ret = element.name;
    });
    return ret;
  }

  void _showProfile(int id, bool bywho, bool isMatchingView) {
    Commons.page(context, routeLinkmomApply,
        arguments: LinkmomApplyPage(
          data: data,
          id: id,
          title: isMatchingView
              ? bywho
                  ? "내가지원한맘대디".tr()
                  : "나에게지원한맘대디".tr()
              : "돌봄진행상황".tr(),
          isMatchingView: isMatchingView,
        ));
  }

  Future<void> _requests(String url, {bool isNext = false}) async {
    var responseData;
    try {
      if (isNext && url.isEmpty) return;
      var response = await apiHelper.apiCall(HttpType.post, url, apiHeader.publicApiHeader, FormData.fromMap(widget.request.toJson()), isFile: true).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      var responseJson = Commons.returnResponse(response);
      switch (widget.type) {
        case ListType.mycares:
          responseData = MomdadyMyCaresListResponse.fromJson(responseJson);
          if (responseData.getCode() == KEY_SUCCESS) {
            if (isNext) {
              _list.results.addAll(responseData.getData().results);
              _list.next = responseData.getData().next;
            } else {
              _list = responseData.getData();
            }
          }
          break;
        case ListType.mMatching:
          responseData = MomdadyMatchingListResponse.fromJson(responseJson);
          if (responseData.getCode() == KEY_SUCCESS) {
            if (isNext) {
              _list.results.addAll(responseData.getData().results);
              _list.next = responseData.getData().next;
            } else {
              _list = responseData.getData();
            }
          }
          break;
        case ListType.lMatching:
          responseData = LinkmomMatchingListResponse.fromJson(responseJson);
          if (responseData.getCode() == KEY_SUCCESS) {
            if (isNext) {
              _list.results.addAll(responseData.getData().results);
              _list.next = responseData.getData().next;
            } else {
              _list = responseData.getData();
            }
          }
          break;
        case ListType.apply:
          responseData = LinkmomApplyListResponse.fromJson(responseJson);
          if (responseData.getCode() == KEY_SUCCESS) {
            if (isNext) {
              _list.results.addAll(responseData.getData().results);
              _list.next = responseData.getData().next;
            } else {
              _list = responseData.getData();
            }
          }
          break;
        default:
      }
      flag.disableLoading(fn: () => onUpDate());
      return responseData;
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      return responseData;
    }
  }
}
