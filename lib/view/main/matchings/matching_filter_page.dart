import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/data/matching_list_request_data.dart';
import 'package:linkmom/data/network/models/data/page_data.dart';
import 'package:linkmom/data/network/models/linkmom_apply_list_request.dart';
import 'package:linkmom/data/network/models/momdady_mycares_list_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/filter_page_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/main/matchings/matching_filter_result_page.dart';

class MatchingFilterPage extends BaseStateful {
  final DataManager data;
  final List<FilterOption> filterOptions;
  final ListType listType;

  MatchingFilterPage({required this.data, required this.filterOptions, required this.listType});

  @override
  _MatchingFilterPageState createState() => _MatchingFilterPageState();
}

class _MatchingFilterPageState extends BaseStatefulState<MatchingFilterPage> {
  List<String> _timeList = [
    "1개월전".tr(),
    "2개월전".tr(),
    "3개월전".tr(),
    "1개월후".tr(),
    "2개월후".tr(),
    "3개월후".tr(),
  ];

  FilterSelect _filterSelect = FilterSelect();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar("상세조회".tr(), hide: false, isClose: true, isBack: false, onClick: (action) => Commons.pagePop(context)),
            body: FilterPageView(
              data: data,
              rangeType: [-1, -2, -3, 1, 2, 3],
              rangeText: _timeList,
              filterOptions: widget.filterOptions,
              listType: widget.listType,
              filterSelect: _filterSelect,
              searchAction: (range, options) {
                if (range is FilterSelect) {
                  _filterSelect = range;
                  _requestSearchCares(range.range!, options);
                } else {
                  _requestSearchCares(range, options);
                }
              },
              endDate: DateTime.now().add(Duration(days: 365)),
            )));
  }

  Future<void> _requestSearchCares(DateTimeRange range, List<FilterOption> options, {String? url, bool fromResult = false}) async {
    try {
      flag.enableLoading(fn: () => onUpDate());
      List<int> careTypes = [];
      List<int> bookingTypes = [];
      List<int> serviceTypes = [];
      String bywho = '';
      options.forEach((e) {
        switch (e.type) {
          case FilterType.bywho:
            List<String> str = e.info.where((element) => element.checked).map((e) => e.value == 1 ? TOME : BYME).toList();
            if (str.contains(TOME) && str.contains(BYME)) {
              bywho = BYMETOME;
            } else {
              bywho = str.first;
            }
            break;
          case FilterType.bookingState:
            bookingTypes.addAll(e.info.where((t) => t.checked).map((e) => e.value).toList());
            break;
          case FilterType.careState:
            careTypes.addAll(e.info.where((t) => t.checked).map((e) => e.value).toList());
            break;
          case FilterType.serviceType:
            serviceTypes.addAll(e.info.where((t) => t.checked).map((e) => e.value).toList());
            break;
          default:
        }
      });

      switch (widget.listType) {
        case ListType.mycares:
          MomdadyMyCaresListRequest req = MomdadyMyCaresListRequest(
            s_date: StringUtils.formatYMD(range.start),
            e_date: StringUtils.formatYMD(range.end),
            status: bookingTypes,
            servicetype: serviceTypes,
          );
          apiHelper.requestMomdadyMyCaresList(req, url: url).then((response) {
            PageData list = response.getData();
            data.caresItem.searchRange = DateTimeRange(start: range.start, end: range.end);
            if (!fromResult)
              Commons.page(context, routeMatchingFilterResult,
                  arguments: MatchingFilterResultPage(
                    request: req,
                    data: data,
                    result: list,
                    url: ApiEndPoint.EP_MOMDADY_MYCARES_LIST,
                    type: widget.listType,
                    range: data.caresItem.searchRange,
                  ));
            flag.disableLoading(fn: () => onUpDate());
          }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
          break;
        case ListType.mMatching:
          MatchingListRequest req = MatchingListRequest(
            s_date: StringUtils.formatYMD(range.start),
            e_date: StringUtils.formatYMD(range.end),
            care_status: careTypes,
            servicetype: serviceTypes,
            bywho: 'matched$bywho',
          );
          apiHelper.requestMomdadyMatchingList(req, url: url).then((response) {
            PageData list = response.getData();
            data.caresItem.searchRange = DateTimeRange(start: range.start, end: range.end);
            if (!fromResult)
              Commons.page(context, routeMatchingFilterResult,
                  arguments: MatchingFilterResultPage(
                    request: req,
                    data: data,
                    result: list,
                    url: ApiEndPoint.EP_MOMDADY_MATCHING_LIST,
                    type: widget.listType,
                    range: data.caresItem.searchRange,
                  ));
            flag.disableLoading(fn: () => onUpDate());
          }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
          break;
        case ListType.apply:
          LinkmomApplyListRequest req = LinkmomApplyListRequest(
            s_date: StringUtils.formatYMD(range.start),
            e_date: StringUtils.formatYMD(range.end),
            status: bookingTypes,
            servicetype: serviceTypes,
            bywho: bywho,
          );
          apiHelper.requestLinkmomApplyList(req, url: url).then((response) {
            PageData list = response.getData();
            data.caresItem.searchRange = DateTimeRange(start: range.start, end: range.end);
            if (!fromResult)
              Commons.page(context, routeMatchingFilterResult,
                  arguments: MatchingFilterResultPage(
                    request: req,
                    data: data,
                    result: list,
                    url: ApiEndPoint.EP_LINKMOM_APPLY_LIST,
                    type: widget.listType,
                    range: data.caresItem.searchRange,
                  ));
            flag.disableLoading(fn: () => onUpDate());
          }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
          break;
        case ListType.lMatching:
          MatchingListRequest req = MatchingListRequest(
            s_date: StringUtils.formatYMD(range.start),
            e_date: StringUtils.formatYMD(range.end),
            care_status: careTypes,
            servicetype: serviceTypes,
            bywho: 'matched$bywho',
          );
          apiHelper.requestLinkmomMatchingList(req, url: url).then((response) {
            PageData list = response.getData();
            data.caresItem.searchRange = DateTimeRange(start: range.start, end: range.end);
            if (!fromResult)
              Commons.page(context, routeMatchingFilterResult,
                  arguments: MatchingFilterResultPage(
                    request: req,
                    data: data,
                    result: list,
                    url: ApiEndPoint.EP_LINKMOM_MATCHING_LIST,
                    type: widget.listType,
                    range: data.caresItem.searchRange,
                  ));
            flag.disableLoading(fn: () => onUpDate());
          }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
          break;
        default:
      }
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e('『GGUMBI』 Exception >>> ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }
}
