import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/block_requests.dart';
import 'package:linkmom/data/network/models/common_matching_view_request.dart';
import 'package:linkmom/data/network/models/data/momdady_result_data.dart';
import 'package:linkmom/data/network/models/data/pay_data.dart';
import 'package:linkmom/data/network/models/linkmom_list_response.dart';
import 'package:linkmom/data/network/models/matching_init_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_info_request.dart';
import 'package:linkmom/data/network/models/pay_cancel_init_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/listview/list_linkmom_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/chat/chat_room_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/linkmom_profile_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/momdady_profile_page.dart';
import 'package:linkmom/view/main/matchings/matching_state_view.dart';
import 'package:linkmom/view/main/mypage/care_diary/momdady_diary_list_page.dart';
import 'package:linkmom/view/main/mypage/review/review_view_page.dart';
import 'package:linkmom/view/main/notify/care_notify_page.dart';
import 'package:linkmom/view/main/payment/payment_cancel_reason_page.dart';

class MomdadyCaresPage extends BaseStateful {
  final DataManager? data;
  final int matchingId;

  MomdadyCaresPage({this.data, this.matchingId = -1});

  @override
  _MomdadyCaresPageState createState() => _MomdadyCaresPageState();
}

class _MomdadyCaresPageState extends BaseStatefulState<MomdadyCaresPage> {
  MomdadyResultsData _data = MomdadyResultsData();

  @override
  void initState() {
    super.initState();
    _requestMatchingView();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            backgroundColor: color_fafafa,
            appBar: diaryAppbar(
                title: "돌봄진행상황".tr(),
                hide: false,
                rightW: lInkWell(
                    onTap: () => showMenuModal(context, {
                          lText("돌봄취소하기".tr(), style: st_b_16(fontWeight: FontWeight.w500, disableColor: color_cecece, isSelect: _data.care_status > CareStatus.cancelParts.value)): () => {
                                if (_data.care_status < CareStatus.cancelAll.value) _moveCancelPage(_data.order_id, _data.contract_id),
                              },
                          lText("신고하기".tr(), style: st_b_16(fontWeight: FontWeight.w500)): () => _moveToNotify(
                                _data.userinfo!.id,
                                _data.matching_id,
                                _data.userinfo!.first_name,
                              ),
                          // NOTE: disable block
                          lText("차단하기".tr(), style: st_16(fontWeight: FontWeight.w500, textColor: color_cecece)): () => {},
                        }),
                    child: Container(
                      child: Lcons.more(),
                      padding: padding_20_R,
                    ))),
            body: LinkmomRefresh(
                onRefresh: () => _requestMatchingView(),
                padding: padding_0,
                child: _data.care_scheduleinfo != null
                    ? Container(
                        color: color_fafafa,
                        child: Column(children: [
                          Container(
                            decoration: BoxDecoration(color: Colors.white, boxShadow: [BoxShadow(blurRadius: 12, offset: Offset(0, 4), color: Colors.black.withAlpha(11))]),
                            child: lInkWell(
                                onTap: () => Commons.page(context, routeMomDadyProfile,
                                    arguments: MomDadyProfilePage(
                                      data: data,
                                      viewMode: ViewMode(viewType: ViewType.view, itemType: ItemType.contract),
                                      bookingId: _data.care_scheduleinfo!.booking_id,
                                      btnType: BottomBtnType.not,
                                      matchingData: MatchingInitRequest(
                                        matchingStatus: _data.matching_status,
                                        receiver: _data.userinfo!.id,
                                        bookingcareservices: _data.care_scheduleinfo!.booking_id,
                                        carematching: _data.matching_id,
                                        chattingroom: _data.chatting!.chatroomId,
                                      ),
                                    )),
                                child: Container(
                                    padding: padding_20,
                                    child: Column(children: [
                                      Row(children: [
                                        lTextBtn(
                                            _data.care_status > CareStatus.cancelParts.index
                                                ? "돌봄취소".tr()
                                                : _data.care_status == CareStatus.cancelParts.index
                                                    ? CareStatus.inprogress.name
                                                    : EnumUtils.getCareStatusName(_data.care_status),
                                            bg: color_eeeeee,
                                            style: st_13(textColor: _data.care_status > CareStatus.cancelParts.index ? color_ff3b30 : Commons.getColor(), fontWeight: FontWeight.w700)),
                                        sb_w_10,
                                        lTextBtn(_data.care_scheduleinfo!.servicetype, bg: EnumUtils.getServiceType(name: _data.care_scheduleinfo!.servicetype).color, style: st_13(fontWeight: FontWeight.w700)),
                                        sb_w_10,
                                        lText(_data.childinfo!.child_name),
                                      ]),
                                      sb_h_10,
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Row(children: [
                                            lText(StringUtils.getCareDateCount(_data.care_scheduleinfo!.caredate), style: st_b_14()),
                                            Container(height: 13, margin: padding_05_LR, child: lDividerVertical(thickness: 1.0, color: color_dedede)),
                                            lText(StringUtils.getCareTime([_data.care_scheduleinfo!.stime, _data.care_scheduleinfo!.etime]), style: st_b_14()),
                                          ]),
                                        ],
                                      )
                                    ]))),
                          ),
                          Container(
                            margin: padding_20_LTR,
                            decoration: BoxDecoration(boxShadow: [BoxShadow(blurRadius: 12, offset: Offset(0, 4), color: Colors.black.withAlpha(10))]),
                            child: LinkMomView(
                              LinkMomListResultsData(
                                userinfo: _data.userinfo,
                                authinfo: _data.authinfo,
                                job_id: _data.job_scheduleinfo!.job_id,
                                servicetype: _data.job_scheduleinfo!.servicetype,
                                possible_area: _data.job_scheduleinfo!.possible_area,
                                introduce: " ",
                                fee_range: PayData(fee_min: _data.job_scheduleinfo!.fee_min, fee_max: _data.job_scheduleinfo!.fee_max),
                                caredate_min: _data.job_scheduleinfo!.caredate_min,
                                caredate_max: _data.job_scheduleinfo!.caredate_max,
                                careschedule_cnt: _data.job_scheduleinfo!.careschedule_cnt,
                              ),
                              viewMode: ViewMode(viewType: ViewType.view),
                              viewType: ViewType.normal,
                              lsAuthInfo: MatchingStateView.getAuthInfo(_data.authinfo!),
                              padding: padding_20,
                              showFollow: true,
                              moreBtn: Align(
                                alignment: Alignment.topRight,
                                child: lInkWell(
                                  onTap: () => Commons.page(context, routeChatRoom,
                                      arguments: ChatRoomPage(
                                        roomType: ChatRoomType.VIEW,
                                        receiver: _data.userinfo!.id,
                                        bookingcareservices: _data.care_scheduleinfo!.booking_id,
                                      )),
                                  child: Padding(
                                    padding: padding_20,
                                    child: Lcons.chat(color: Commons.getColor(), size: 25),
                                  ),
                                ),
                              ),
                              onItemClick: (item, action) {
                                if (action == DialogAction.review) {
                                  Commons.page(context, routeReviewView, arguments: ReviewViewPage(userId: _data.userinfo!.id, gubun: USER_TYPE.link_mom.index + 1));
                                } else if (action == DialogAction.select) {
                                  Commons.page(context, routeLinkMomProfile,
                                      arguments: LinkMomProfilePage(
                                        //2021/12/16 링크쌤 모드에서만 수정하도록 변경, 리스트에선 뷰로 본다.
                                        viewMode: ViewMode(viewType: ViewType.view),
                                        jobId: _data.job_scheduleinfo!.job_id,
                                        first_name: _data.userinfo!.first_name,
                                        btnType: BottomBtnType.not,
                                      ));
                                }
                              },
                            ),
                          ),
                          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                            Padding(padding: padding_20, child: lText("돌봄진행상황".tr(), style: st_b_18(fontWeight: FontWeight.w700), textAlign: TextAlign.left)),
                            MatchingStateView(
                              state: _data.care_status,
                              dashWidth: data.width(context, 0.15),
                              btn: _buildBtn(),
                              type: MatchingStateView.CARE_STATE,
                              name: '${_data.userinfo!.first_name} ${"링크쌤".tr()}${"과".tr()}',
                            ),
                          ]),
                        ]),
                      )
                    : lEmptyView(title: "잠시만기다려주세요".tr(), padding: padding_30, height: data.height(context, 0.8)))));
  }

  void _moveToNotify(int userId, int matchingId, String name) {
    Commons.page(context, routeCareNotify,
        arguments: CareNotifyPage(
          reasons: [
            NotifyReason.linkmomNotSaveTime,
            NotifyReason.linkomoNoshow,
            NotifyReason.linkmomChargingOther,
            NotifyReason.linkmomOutOfPlatform,
            NotifyReason.linkmomInsult,
            NotifyReason.linkmomAssult,
            NotifyReason.linkmomAd,
            NotifyReason.linkmomInappropriate,
            NotifyReason.other,
          ],
          matchingId: matchingId,
          userId: userId,
          userName: name,
        ));
  }

  Widget _buildBtn() {
    switch (EnumUtils.getCareStatus(index: _data.care_status)) {
      case CareStatus.ready:
        return MultiBtn(btnList: [
          lBtn("돌봄일기".tr(),
              btnColor: color_dedede,
              onClickAction: () => showNormalDlg(
                  context: context,
                  messageWidget: Container(
                    child: Column(children: [
                      TextHighlight(
                        text: "돌봄일기안내_맘대디_1".tr(),
                        term: "돌봄일기안내_맘대디_강조".tr(),
                        textAlign: TextAlign.center,
                        textStyle: st_14(textColor: color_545454),
                        textStyleHighlight: st_14(textColor: color_545454, fontWeight: FontWeight.w700),
                      ),
                      lText("돌봄일기안내_맘대디_2".tr(), style: TextStyle(fontSize: 14, color: color_545454, decoration: TextDecoration.underline), textAlign: TextAlign.center),
                    ]),
                  )),
              margin: padding_0)
        ]);
      case CareStatus.inprogress:
      case CareStatus.end:
        return MultiBtn(btnList: [
          lBtn(
            "돌봄일기".tr(),
            onClickAction: () => Commons.page(context, routeMomdadyDiary, arguments: MomdadyDiaryListPage()),
            margin: padding_0,
          )
        ]);
      case CareStatus.cancelParts:
        return MultiBtn(btnList: [
          lBtn(
            "취소내역확인".tr(),
            onClickAction: () => Commons.page(context, routePayMentCancelList, arguments: PayInfoRequest(contract_id: _data.contract_id)),
            margin: padding_05_R,
          ),
          lBtn(
            "돌봄일기".tr(),
            onClickAction: () => Commons.page(context, routeMomdadyDiary, arguments: MomdadyDiaryListPage()),
            margin: padding_05_L,
          )
        ]);
      case CareStatus.cancelAll:
        return MultiBtn(btnList: [
          lBtn(
            "취소내역확인".tr(),
            onClickAction: () => Commons.page(
              context,
              routePayMentCancelList,
              arguments: PayInfoRequest(contract_id: _data.contract_id),
            ),
            margin: padding_0,
          )
        ]);
      default:
        return Container();
    }
  }

  void _moveCancelPage(String order, int contract) {
    showNormalDlg(
      message: '${"돌봄취소하기_안내".tr()}',
      btnLeft: "취소".tr(),
      btnRight: "돌봄취소하기".tr(),
      onClickAction: (action) {
        if (action == DialogAction.yes) {
          Commons.page(context, routePayMentCancelReason,
              arguments: PaymentCancelReasonPage(
                  payCancelInitRequest: PayCancelInitRequest(order_id: order, contract_id: contract),
                  callBack: (saveReq) {
                    _requestMatchingView();
                  }));
        }
      },
    );
  }

  void _requestMatchingView() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      CommonMatchingViewRequest req = CommonMatchingViewRequest(matchingId: widget.matchingId);
      apiHelper.reqeustMomdadyMatchingView(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _data = response.getData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _userBlock(int userId, String name, int matchingId) {
    showNormalDlg(
        context: context,
        title: '$name ${"링크쌤을차단하시겠습니까".tr()}',
        message: "링크쌤차단_안내".tr(),
        btnLeft: "취소".tr(),
        onClickAction: (action) {
          if (action == DialogAction.yes) {
            _requestUserBlock(userId, matchingId);
          }
        });
  }

  void _requestUserBlock(int userId, int matchingId) {
    try {
      BlockSaveRequest req = BlockSaveRequest(block_user: userId, block_matching: matchingId);
      apiHelper.requestBlockSave(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.pagePop(context);
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}
