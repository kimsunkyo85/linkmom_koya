import 'package:easy_localization/easy_localization.dart';
import 'package:expandable_page_view/expandable_page_view.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/apply_delete_request.dart';
import 'package:linkmom/data/network/models/block_requests.dart';
import 'package:linkmom/data/network/models/data/chat_send_data.dart';
import 'package:linkmom/data/network/models/data/matching_request_data.dart';
import 'package:linkmom/data/network/models/data/momdady_result_data.dart';
import 'package:linkmom/data/network/models/data/pay_data.dart';
import 'package:linkmom/data/network/models/linkmom_list_response.dart';
import 'package:linkmom/data/network/models/matching_cancel_request.dart';
import 'package:linkmom/data/network/models/matching_init_request.dart';
import 'package:linkmom/data/network/models/matching_sign_request.dart';
import 'package:linkmom/data/network/models/momdady_apply_list_request.dart';
import 'package:linkmom/data/network/models/momdady_mycares_list_response.dart';
import 'package:linkmom/data/network/models/momdady_mycares_status_update_request.dart';
import 'package:linkmom/data/network/models/pay_init_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/listview/list_linkmom_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/chat/chat_room_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/linkmom_profile_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/momdady_profile_page.dart';
import 'package:linkmom/view/main/matchings/matching_state_view.dart';
import 'package:linkmom/view/main/matchings/momdady_cares_page.dart';
import 'package:linkmom/view/main/mypage/review/review_view_page.dart';
import 'package:linkmom/view/main/notify/care_notify_page.dart';
import 'package:linkmom/view/main/payment/payment_page.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class MomdadyApplyPage extends BaseStateful {
  final DataManager? data;
  final int bookingId;
  final MyCareResultsData? info;
  final int tabIndex;
  final String bywho;

  MomdadyApplyPage({this.data, this.bookingId = 0, this.info, this.tabIndex = 0, this.bywho = ''});

  @override
  _MomdadyApplyPageState createState() => _MomdadyApplyPageState();
}

class _MomdadyApplyPageState extends BaseStatefulState<MomdadyApplyPage> with TickerProviderStateMixin {
  Map<String, List<dynamic>> _list = {TOME: [], BYME: []};
  PageController _toMeCtrl = PageController();
  PageController _byMeCtrl = PageController();
  BookingStatus _bookingState = BookingStatus.inprogress;
  bool _editable = false; // NOTE: revers
  int _toMePage = 0;
  int _byMePage = 0;
  TabController? _tabController;
  String _childName = ' ';
  String _careDate = ' ';
  String _careTimes = ' ';
  ServiceType _service = ServiceType.serviceType_2;

  bool _isModify = false; //돌봄신청서가 수정된 경우

  @override
  void initState() {
    super.initState();
    if (widget.info != null) _bookingState = EnumUtils.getBookingStatus(value: widget.info!.booking_status);
    _editable = !(_bookingState == BookingStatus.inprogress);
    if (widget.info != null) {
      _childName = widget.info!.childinfo!.child_name;
      _careDate = StringUtils.getCareDate(widget.info!.carescheduleinfo!.caredate!);
      _careTimes = StringUtils.getCareTime([widget.info!.carescheduleinfo!.stime, widget.info!.carescheduleinfo!.etime]);
      _service = EnumUtils.getServiceType(name: widget.info!.servicetype);
    }
    flag.enableLoading(fn: () => onUpDate());
    _requestApplyList();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () => Commons.pagePop(context, data: data),
        child: lModalProgressHUD(
            flag.isLoading,
            lScaffold(
              backgroundColor: color_fafafa,
              appBar: diaryAppbar(
                  title: "내신청서".tr(),
                  hide: false,
                  onBack: () {
                    Commons.pagePop(context, data: _isModify ? _isModify : null);
                  },
                  rightW: IconButton(
                      onPressed: () {
                        late MomdadyResultsData current;
                        switch (_tabController!.index) {
                          case 0:
                            current = _list[TOME]![_toMePage];
                            break;
                          case 1:
                            current = _list[BYME]![_byMePage];
                            break;
                        }
                        showMenuModal(context, {
                          lText(
                            "매칭취소하기".tr(),
                            style: st_b_16(
                              fontWeight: FontWeight.w500,
                              disableColor: color_cecece,
                              isSelect: !(current.matching_status > MatchingStatus.cancelPaid.value && current.matching_status < MatchingStatus.paid.value),
                            ),
                          ): () => {
                                if (current.matching_status > MatchingStatus.cancelPaid.value && current.matching_status < MatchingStatus.paid.value)
                                  _matchingCancel(
                                    current.userinfo!.id,
                                    current.care_scheduleinfo!.booking_id,
                                    current.chatting!.chatroomId,
                                    current.matching_id,
                                    current.userinfo!.first_name,
                                  )
                              },
                          lText("신고하기".tr(), style: st_b_16(fontWeight: FontWeight.w500)): () => _moveToNotify(current.userinfo!.user_id, current.matching_id, current.userinfo!.first_name),
                          lText("차단하기".tr(), style: st_b_16(textColor: current.matching_status < MatchingStatus.paid.value ? color_222222 : color_cecece, fontWeight: FontWeight.w500)): () => {
                                if (current.matching_status < MatchingStatus.paid.value)
                                  showNormalDlg(
                                      context: context,
                                      title: '${current.userinfo!.first_name} ${"링크쌤을차단하시겠습니까".tr()}',
                                      message: "링크쌤차단_안내".tr(),
                                      btnLeft: "취소".tr(),
                                      onClickAction: (action) {
                                        if (action == DialogAction.yes) {
                                          _requestUserBlock(current.userinfo!.id, current.matching_id);
                                        }
                                      })
                              },
                          lText("삭제".tr(), style: st_16(textColor: current.matching_status < MatchingStatus.paid.value ? color_ff3b30 : color_cecece, fontWeight: FontWeight.w500)): () => {
                                if (current.matching_status < MatchingStatus.paid.value)
                                  showNormalDlg(
                                      context: context,
                                      message: '${current.userinfo!.first_name} ${"링크쌤".tr()} ${"지원내역삭제".tr()}',
                                      btnLeft: "취소".tr(),
                                      onClickAction: (action) {
                                        if (action == DialogAction.yes) {
                                          _requestApplyDelete(current.matching_id);
                                        }
                                      })
                              }
                        });
                      },
                      icon: Lcons.more())),
              body: Column(
                children: [
                  Container(
                      decoration: BoxDecoration(color: Colors.white, boxShadow: [BoxShadow(blurRadius: 12, offset: Offset(0, 4), color: Colors.black.withAlpha(11))]),
                      child: Column(
                        children: [
                          lInkWell(
                              onTap: () => Commons.page(context, routeMomDadyProfile,
                                  arguments: MomDadyProfilePage(
                                    data: data,
                                    viewMode: ViewMode(viewType: ViewType.view),
                                    bookingId: widget.bookingId,
                                    btnType: BottomBtnType.not,
                                    matchingData: MatchingInitRequest(matchingStatus: 0),
                                  )),
                              child: Container(
                                  padding: EdgeInsets.fromLTRB(20, 20, 10, 20),
                                  child: Stack(children: [
                                    Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      Row(
                                        children: [
                                          lTextBtn(_service.string, bg: _service.color, style: st_13(textColor: Colors.white, fontWeight: FontWeight.w700)),
                                          Padding(
                                            padding: padding_10_LR,
                                            child: lText(_childName, style: st_b_14()),
                                          ),
                                          if (_bookingState.isExpire)
                                            lIconText(
                                              align: MainAxisAlignment.end,
                                              isExpanded: false,
                                              onClick: () async {
                                                var resultData = await Commons.page(context, routeMomDadyProfile,
                                                    arguments: MomDadyProfilePage(
                                                      viewMode: ViewMode(viewType: ViewType.modify),
                                                      bookingId: widget.bookingId,
                                                    ));
                                                if (resultData != null) {
                                                  _requestApplyList();
                                                  _isModify = true;
                                                }
                                              },
                                              title: "수정".tr(),
                                              icon: Lcons.create(color: color_b2b2b2, isEnabled: true, size: 14),
                                              isLeft: false,
                                              textStyle: st_14(textColor: color_b2b2b2),
                                            ),
                                        ],
                                      ),
                                      sb_h_10,
                                      Row(children: [
                                        lText(_careDate, style: st_b_14()),
                                        Container(height: 13, margin: padding_05_LR, child: lDividerVertical(thickness: 1.0, color: color_dedede)),
                                        lText(_careTimes, style: st_b_14()),
                                      ]),
                                    ]),
                                    Align(
                                      alignment: Alignment.topRight,
                                      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
                                        lText(_bookingState != BookingStatus.inprogress ? BookingStatus.end.name : _bookingState.name,
                                            style: st_13(fontWeight: FontWeight.w700, textColor: _bookingState.value != BookingStatus.inprogress.value ? color_999999 : Commons.getColor(), disableColor: color_b2b2b2)),
                                        SizedBox(
                                          width: 65,
                                          height: 30,
                                          child: Switch(
                                            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                            value: _bookingState != BookingStatus.inprogress,
                                            onChanged: (value) {
                                              if (_bookingState.isExpire) {
                                                showNormalDlg(message: _bookingState.expireDlg);
                                                return;
                                              }
                                              if (_bookingState == BookingStatus.inprogress) {
                                                _bookingState = BookingStatus.end;
                                              } else if (_bookingState == BookingStatus.end) {
                                                _bookingState = BookingStatus.inprogress;
                                              }
                                              _requestApplyState(!value);
                                            },
                                            activeTrackColor: color_eeeeee,
                                            inactiveTrackColor: color_eeeeee,
                                            activeColor: color_b2b2b2,
                                            inactiveThumbColor: Commons.getColor(),
                                          ),
                                        )
                                      ]),
                                    ),
                                  ]))),
                          expiredView(status: _bookingState.value),
                        ],
                      )),
                  Expanded(
                    child: TabPageView(
                      bgColor: color_fafafa,
                      hideBgColor: true,
                      tabBgColor: color_fafafa,
                      tabbarName: [
                        TabItem(title: "${"나에게지원".tr()} ${_list[TOME]!.length}${"명".tr()}"),
                        TabItem(title: "${"내가지원".tr()} ${_list[BYME]!.length}${"명".tr()}"),
                      ],
                      tabbarView: [
                        LinkmomRefresh(
                          onRefresh: () => _requestApplyList(),
                          padding: padding_20_T,
                          child: Column(children: [
                            if (_list[TOME]!.length > 1)
                              Center(
                                child: Padding(
                                  padding: padding_20_LRB,
                                  child: SmoothPageIndicator(
                                      controller: _toMeCtrl,
                                      count: _list[TOME]!.length,
                                      effect: WormEffect(
                                        spacing: 8,
                                        dotHeight: 8,
                                        dotWidth: 8,
                                        dotColor: color_cae1de,
                                        activeDotColor: Commons.getColor(),
                                      )),
                                ),
                              ),
                            _list[TOME]!.isNotEmpty
                                ? ExpandablePageView(
                                    estimatedPageSize: 0,
                                    controller: _toMeCtrl,
                                    onPageChanged: (page) => _toMePage = page,
                                    children: _list[TOME]!.map((e) => _linkmomProfile(e)).toList(),
                                  )
                                : lEmptyView(height: data.height(context, 0.5)),
                          ]),
                        ),
                        LinkmomRefresh(
                          onRefresh: () => _requestApplyList(),
                          padding: padding_20_T,
                          child: Column(children: [
                            if (_list[BYME]!.length > 1)
                              Center(
                                child: Padding(
                                  padding: padding_20_LRB,
                                  child: SmoothPageIndicator(
                                      controller: _byMeCtrl,
                                      count: _list[BYME]!.length,
                                      effect: WormEffect(
                                        spacing: 8,
                                        dotHeight: 8,
                                        dotWidth: 8,
                                        dotColor: color_cae1de,
                                        activeDotColor: Commons.getColor(),
                                      )),
                                ),
                              ),
                            _list[BYME]!.isNotEmpty
                                ? ExpandablePageView(
                                    estimatedPageSize: 0,
                                    controller: _byMeCtrl,
                                    onPageChanged: (page) => _byMePage = page,
                                    children: _list[BYME]!.map((e) => _linkmomProfile(e)).toList(),
                                  )
                                : lEmptyView(height: data.height(context, 0.5)),
                          ]),
                        )
                      ],
                      onCreateView: (controller) {
                        if (_tabController != controller) {
                          _tabController = controller;
                          _tabController!.animateTo(widget.tabIndex);
                        }
                      },
                    ),
                  )
                ],
              ),
            )));
  }

  Widget _linkmomProfile(MomdadyResultsData ret) {
    return Wrap(
      children: [
        Container(
          margin: padding_20_LRB,
          decoration: BoxDecoration(boxShadow: [BoxShadow(blurRadius: 12, offset: Offset(0, 4), color: Colors.black.withAlpha(10))]),
          child: LinkMomView(
            LinkMomListResultsData(
              userinfo: ret.userinfo,
              authinfo: ret.authinfo,
              job_id: ret.job_scheduleinfo!.job_id,
              servicetype: ret.job_scheduleinfo!.servicetype,
              possible_area: ret.job_scheduleinfo!.possible_area,
              introduce: " ",
              fee_range: PayData(fee_min: ret.job_scheduleinfo!.fee_min, fee_max: ret.job_scheduleinfo!.fee_max),
              caredate_min: ret.job_scheduleinfo!.caredate_min,
              caredate_max: ret.job_scheduleinfo!.caredate_max,
              careschedule_cnt: ret.job_scheduleinfo!.careschedule_cnt,
            ),
            viewMode: ViewMode(viewType: ViewType.view),
            viewType: ViewType.normal,
            lsAuthInfo: MatchingStateView.getAuthInfo(ret.authinfo!),
            padding: padding_20,
            showFollow: true,
            moreBtn: Align(
              alignment: Alignment.topRight,
              child: InkWell(
                  onTap: () async {
                    //채팅으로 들어가서 결제 후 상태값 업데이트
                    var matchingStatus = await Commons.page(context, routeChatRoom,
                        arguments: ChatRoomPage(
                          receiver: ret.userinfo!.id,
                          bookingcareservices: ret.care_scheduleinfo!.booking_id,
                          chatroom_id: ret.chatting!.chatroomId,
                        ));
                    if (matchingStatus != null) {
                      ret.matching_status = (matchingStatus as ChatBackItem).matchingStatus;
                      onUpDate();
                    }
                  },
                  child: Padding(
                    padding: padding_20,
                    child: Lcons.chat(color: Commons.getColor(), size: 25),
                  )),
            ),
            onItemClick: (item, action) {
              if (action == DialogAction.review) {
                Commons.page(context, routeReviewView, arguments: ReviewViewPage(userId: ret.userinfo!.id, gubun: USER_TYPE.link_mom.index + 1));
              } else if (action == DialogAction.select) {
                Commons.page(context, routeLinkMomProfile,
                    arguments: LinkMomProfilePage(
                      //2021/12/16 링크쌤 모드에서만 수정하도록 변경, 리스트에선 뷰로 본다.
                      viewMode: ViewMode(viewType: ViewType.view),
                      jobId: ret.job_scheduleinfo!.job_id,
                      first_name: ret.userinfo!.first_name,
                      btnType: BottomBtnType.not,
                    ));
              }
            },
          ),
        ),
        Wrap(
          children: [
            Padding(
              padding: padding_20_LR,
              child: lText("매칭진행상황".tr(), style: st_b_18(fontWeight: FontWeight.w700)),
            ),
            MatchingStateView(
              state: ret.matching_status,
              type: MatchingStateView.MATCHING_STATE,
              time: ret.paid_deadline,
              btn: _buildBtn(ret),
              name: '${ret.userinfo!.first_name} ${"링크쌤".tr()}',
              isRematch: ret.rematching,
            ),
          ],
        ),
      ],
    );
  }

  void _requestApplyList() {
    try {
      if (widget.bywho.isNotEmpty) {
        MomdadyApplyListRequest req = MomdadyApplyListRequest(bywho: widget.bywho, booking_id: widget.bookingId);
        apiHelper.requestMomdadyApplyList(req).then((response) {
          if (response.getCode() == KEY_SUCCESS) {
            _list[widget.bywho] = response.getData().results;
            MomdadyResultsData ret = _list[widget.bywho]!.first;
            _bookingState = EnumUtils.getBookingStatus(name: ret.care_scheduleinfo!.status);
            _childName = ret.childinfo!.child_name;
            _careDate = StringUtils.getCareDate(ret.care_scheduleinfo!.caredate);
            _careTimes = StringUtils.getCareTime([ret.care_scheduleinfo!.stime, ret.care_scheduleinfo!.etime]);
            _service = EnumUtils.getServiceType(name: ret.care_scheduleinfo!.servicetype);
          }
          flag.disableLoading(fn: () => onUpDate());
          Future.delayed(Duration(seconds: 1), () {
            _tabController!.animateTo(widget.bywho == TOME ? 0 : 1);
          });
        }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      } else {
        flag.enableLoading(fn: () => onUpDate());
        MomdadyApplyListRequest to = MomdadyApplyListRequest(bywho: TOME, booking_id: widget.bookingId);
        apiHelper.requestMomdadyApplyList(to).then((response) {
          if (response.getCode() == KEY_SUCCESS) {
            _list[TOME] = [];
            _list[TOME]!.addAll(response.getData().results);
            MomdadyResultsData ret = _list[TOME]!.first;
            _bookingState = EnumUtils.getBookingStatus(name: ret.care_scheduleinfo!.status);
            _childName = ret.childinfo!.child_name;
            _careDate = StringUtils.getCareDate(ret.care_scheduleinfo!.caredate);
            _careTimes = StringUtils.getCareTime([ret.care_scheduleinfo!.stime, ret.care_scheduleinfo!.etime]);
            _service = EnumUtils.getServiceType(name: ret.care_scheduleinfo!.servicetype);
            if (_list[TOME]!.isEmpty) {
              _tabController!.animateTo(0);
            }
          }
          flag.disableLoading(fn: () => onUpDate());
        }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
        MomdadyApplyListRequest by = MomdadyApplyListRequest(bywho: BYME, booking_id: widget.bookingId);
        apiHelper.requestMomdadyApplyList(by).then((response) {
          if (response.getCode() == KEY_SUCCESS) {
            _list[BYME] = [];
            _list[BYME]!.addAll(response.getData().results);
            MomdadyResultsData ret = _list[BYME]!.first;
            _childName = ret.childinfo!.child_name;
            _careDate = StringUtils.getCareDate(ret.care_scheduleinfo!.caredate);
            _careTimes = StringUtils.getCareTime([ret.care_scheduleinfo!.stime, ret.care_scheduleinfo!.etime]);
            _service = EnumUtils.getServiceType(name: ret.care_scheduleinfo!.servicetype);
            if (_list[BYME]!.isEmpty) {
              _tabController!.animateTo(1);
            }
          }
          flag.disableLoading(fn: () => onUpDate());
        }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      }
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  Widget emptyDataView() {
    return Container(
      height: data.height(context, 0.6),
      alignment: Alignment.center,
      child: lText("내역이없습니다".tr(), style: st_16(textColor: color_999999, fontWeight: FontWeight.w700)),
    );
  }

  void _requestApplyState(bool status) {
    try {
      MomdadyMyCaresStatusUpdateRequest req = MomdadyMyCaresStatusUpdateRequest(booking_id: widget.bookingId, status: status ? BookingStatus.inprogress.value : BookingStatus.end.value);
      apiHelper.requestMyCaresStatusUpdate(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _bookingState = EnumUtils.getBookingStatus(value: req.status);
        } else {
          _bookingState = BookingStatus.end;
          showNormalDlg(context: context, message: response.getMsg());
        }
        onUpDate();
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {}
  }

  void _requestUserBlock(int userId, int matchingId) {
    try {
      BlockSaveRequest req = BlockSaveRequest(block_user: userId, block_matching: matchingId);
      apiHelper.requestBlockSave(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.pagePop(context);
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {}
  }

  void _moveToNotify(int userId, int matchingId, String name) {
    Commons.page(context, routeCareNotify,
        arguments: CareNotifyPage(
          reasons: [
            NotifyReason.linkmomAd,
            NotifyReason.linkmomInappropriate,
            NotifyReason.linkmomOutOfPlatform,
            NotifyReason.other,
          ],
          matchingId: matchingId,
          userId: userId,
          userName: name,
        ));
  }

  void _requestApplyDelete(int matchingId) {
    try {
      ApplyDeleteRequest req = ApplyDeleteRequest(matching_id: matchingId.toString());
      apiHelper.requestApplyDelete(req).then((response) {
        Commons.pagePop(context);
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {}
  }

  Widget _buildBtn(MomdadyResultsData ret) {
    MatchingStatus status = EnumUtils.getMatchingStatus(index: ret.matching_status);
    if (ret.rematching) status = MatchingStatus.cancel;
    switch (status) {
      case MatchingStatus.waitMomdaddy:
        return MultiBtn(btnList: [lBtn("서명요청하기".tr(), onClickAction: () => _requestSign(ret.userinfo!.id, ret.care_scheduleinfo!.booking_id, ret.matching_id, ret.chatting!.chatroomId), margin: padding_0)]);
      case MatchingStatus.waitLinkmom:
        return MultiBtn(btnList: [lBtn("서명하기".tr(), onClickAction: () => goSignPage(ret), margin: padding_0)]);
      case MatchingStatus.waitPaid:
        return MultiBtn(btnList: [lBtn("결제하기".tr(), onClickAction: () => goPaymentPage(ret), margin: padding_0)]);
      case MatchingStatus.failDisagreeL:
      case MatchingStatus.failDisagreeM:
      case MatchingStatus.failNoRespL:
      case MatchingStatus.failNoRespM:
      case MatchingStatus.cancelLinkmom:
      case MatchingStatus.cancelMomdaddy:
      case MatchingStatus.cancel:
      case MatchingStatus.cancelPaid:
        return MultiBtn(btnList: [
          lBtn("다시매칭요청하기".tr(), onClickAction: () {
            if (_bookingState == BookingStatus.expired) {
              String nowTime = StringUtils.getTimeToString(DateTime.now().add(Duration(minutes: 30)));
              String msg = '${"날짜시간변경안내_1".tr()} $nowTime ${"날짜시간변경안내_2".tr()}';
              Widget messageWidget = TextHighlight(
                text: msg,
                term: nowTime,
                textStyle: st_14(textColor: color_545454),
                textStyleHighlight: st_14(textColor: color_545454, fontWeight: FontWeight.w700),
                textAlign: TextAlign.center,
              );

              showNormalDlg(
                  messageWidget: messageWidget,
                  onClickAction: (a) async {
                    if (a == DialogAction.yes) {
                      var resultData = await Commons.page(context, routeMomDadyProfile,
                          arguments: MomDadyProfilePage(
                            viewMode: ViewMode(viewType: ViewType.modify),
                            bookingId: ret.care_scheduleinfo!.booking_id,
                          ));
                      if (resultData != null) {
                        _requestApplyList();
                      }
                    }
                  });
            } else {
              goSignPage(ret);
            }
          }, margin: padding_0)
        ]);
      case MatchingStatus.paid:
        return MultiBtn(btnList: [lBtn("돌봄진행상황".tr(), onClickAction: () => Commons.pageReplace(context, routeMomdadyCares, arguments: MomdadyCaresPage(data: data, matchingId: ret.matching_id)), margin: padding_0)]);
      case MatchingStatus.inprogress:
        return MultiBtn(btnList: [lBtn("매칭요청하기".tr(), onClickAction: () => goSignPage(ret), margin: padding_0)]);
      default:
        return Container();
    }
  }

  void _requestSign(int id, int bookingId, int matchingId, int chatroomId) {
    if (_bookingState.isExpire) {
      showNormalDlg(message: _bookingState.expireDlg);
      return;
    }
    try {
      MatchingSignRequest req = MatchingSignRequest();
      req.requestData = MatchingRequestData(receiver: id, bookingcareservices: bookingId, carematching: matchingId);
      apiHelper.requestMatchingSign(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          showNormalDlg(context: context, message: response.getData().message, color: Commons.getColor());
        } else {
          showNormalDlg(context: context, message: response.getMsg(), color: Commons.getColor());
        }
      });
    } catch (e) {}
  }

  ///매칭 요청하기
  void goSignPage(MomdadyResultsData ret) async {
    if (_bookingState.isExpire) {
      showNormalDlg(message: _bookingState.expireDlg);
      return;
    }
    ContractData? contractData = await Commons.page(context, routeMomDadyProfile,
        arguments: MomDadyProfilePage(
          data: data,
          viewMode: ViewMode(viewType: ViewType.view, itemType: ItemType.contract),
          bookingId: ret.care_scheduleinfo!.booking_id,
          btnType: BottomBtnType.contract,
          matchingData: MatchingInitRequest(
            matchingStatus: ret.matching_status,
            receiver: ret.userinfo!.id,
            bookingcareservices: ret.care_scheduleinfo!.booking_id,
            carematching: ret.matching_id,
            chattingroom: ret.chatting!.chatroomId,
          ),
        ));

    if (contractData != null) {
      ret.matching_status = contractData.matching_status;
      ret.paid_deadline = contractData.deadline;
      onUpDate();
    }
  }

  ///결제 페이지로 이동
  Future<void> goPaymentPage(MomdadyResultsData ret) async {
    if (!await Commons.isEmailAuthCheck(context)) {
      return;
    }

    await Commons.page(context, routePayMent,
        arguments: PaymentPage(
          data: PayInitRequest(
            bookingcareservices: ret.care_scheduleinfo!.booking_id,
            carematching: ret.matching_id,
            carecontract: ret.contract_id,
          ),
          //결제화면에서트 결제완료시, 종료시 상태값 업데이
          callBack: (a) {
            _requestApplyList();
          },
        ));
  }

  void _matchingCancel(int userId, int bookingServ, int chatroomId, int matchingId, String name) {
    showNormalDlg(
      btnLeft: "아니오".tr(),
      btnRight: "예".tr(),
      messageWidget: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          lText(
            "매칭취소하기_안내".tr(),
            style: st_b_15(),
            textAlign: TextAlign.center,
          ),
          sb_h_10,
          lText(
            '$name ${"매칭취소하기_취소".tr()}',
            style: st_b_15(fontWeight: FontWeight.w500),
            textAlign: TextAlign.center,
          ),
        ],
      ),
      onClickAction: (action) {
        if (action == DialogAction.yes) {
          _requestMatchingCancel(userId, bookingServ, chatroomId, matchingId);
        }
      },
    );
  }

  Future<void> _requestMatchingCancel(int receiver, int bookingServ, int chatroomId, int matchingId) async {
    try {
      MatchingCancelRequest req = MatchingCancelRequest();
      req.requestData = MatchingRequestData(
        receiver: receiver,
        bookingcareservices: bookingServ,
        chattingroom: chatroomId,
        carematching: matchingId,
      );
      log.d('『GGUMBI』>>> _buildMenu : data.requestData : ${req.requestData},  <<< ');
      apiHelper.requestMatchingCancel(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          showNormalDlg(message: "매칭이성공적으로취소되었습니다".tr(), onClickAction: (a) => {if (a == DialogAction.yes) _requestApplyList()});
        } else {
          showNormalDlg(message: response.getMsg(), onClickAction: (a) => {if (a == DialogAction.yes) _requestApplyList()});
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {}
  }
}
