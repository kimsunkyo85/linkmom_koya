import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/holidays/holidays_helper.dart';
import 'package:linkmom/data/network/models/data/linkmom_result_data.dart';
import 'package:linkmom/data/network/models/data/matching_list_request_data.dart';
import 'package:linkmom/data/network/models/data/page_data.dart';
import 'package:linkmom/data/network/models/linkmom_apply_list_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/filter_page_view.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/home_provider.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/calendar/calendar_view.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/main_home.dart';
import 'package:linkmom/view/main/matchings/linkmom_apply_page.dart';
import 'package:linkmom/view/main/matchings/matching_list_view.dart';
import 'package:provider/provider.dart';

class LinkmomMatchingPage extends BaseStateful {
  final DataManager? data;
  final PageController? controller;

  LinkmomMatchingPage({this.data, this.controller});

  @override
  _LinkmomMatchingPageState createState() => _LinkmomMatchingPageState();
}

class _LinkmomMatchingPageState extends BaseStatefulState<LinkmomMatchingPage> with TickerProviderStateMixin {
  ScrollController _controller = ScrollController();
  late TabController _applyCtrl;
  late TabController _matchCtrl;

  String _first = StringUtils.formatYMD(Jiffy(DateTime.now()).startOf(Units.MONTH).dateTime);
  String _last = StringUtils.formatYMD(Jiffy(DateTime.now()).endOf(Units.MONTH).dateTime);

  late LinkmomSchedule _matchList;
  late LinkmomSchedule _applyList;

  String _matchNext = '';
  String _applyNext = '';

  @override
  void initState() {
    super.initState();
    DateTimeRange range = DateTimeRange(start: Jiffy(DateTime.now()).startOf(Units.MONTH).dateTime, end: Jiffy(DateTime.now()).endOf(Units.MONTH).dateTime);
    _matchList = LinkmomSchedule(tome: LinkmomScheduleData(TOME, []), range: range, byme: LinkmomScheduleData(BYME, []));
    _applyList = LinkmomSchedule(tome: LinkmomScheduleData(TOME, []), range: range, byme: LinkmomScheduleData(BYME, []));
    _applyCtrl = TabController(length: 2, vsync: this);
    _matchCtrl = TabController(length: 2, vsync: this);
    if (auth.user.username.isNotEmpty) {
      flag.enableLoading(fn: () => onUpDate());
      _requestApplyList(null, null).then((value) => _requestMatchList());
    }
  }

  @override
  void dispose() {
    data.caresItem.searchRange = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<HomeNavigationProvider>(context);
    if (provider.scheduleUpdated) {
      provider.updatedEvent(TAB_SCHEDULE);
      flag.enableLoading(fn: () => onUpDate());
      _requestApplyList(null, null).then((value) => _requestMatchList());
    }
    return TabPageView(
      tabbarName: [TabItem(title: "지원내역".tr()), TabItem(title: "돌봄진행상황".tr())],
      tabbarView: [
        lScaffold(
          body: lModalProgressHUD(
            flag.isLoading,
            NextRequestListener(
              onNextRequest: () => _requestApplyList(_first, _last, next: true),
              child: LinkmomRefresh(
                  controller: _controller,
                  padding: padding_0,
                  anotherScrollView: true,
                  child: MatchingListView(
                      data: data,
                      count: _applyList.tome.count + _applyList.byme.count,
                      listType: ListType.apply,
                      range: _applyList.range,
                      filter: [
                        FilterOption(type: FilterType.bookingState, info: HireStatus.values.map((e) => FilterInfo(value: e.value, title: e.name, desc: e.desc)).toList()),
                        FilterOption(type: FilterType.serviceType, info: ServiceType.values.sublist(0, 3).map((e) => FilterInfo(value: e.index, title: e.string)).toList()),
                        FilterOption(type: FilterType.bywho, info: [FilterInfo(value: 1, title: "나에게지원한".tr(), desc: "나에게지원한_설명".tr()), FilterInfo(value: 2, title: "내가지원한".tr(), desc: "내가지원한_설명".tr())])
                      ],
                      calendar: _buildCalendar(
                        onSelected: (date, events) {
                          return false;
                        },
                        events: data.caresItem.linkmomApplyEvents,
                        onCalendarChanged: _onApplyCalendarChanged,
                      ),
                      items: Container(
                        padding: padding_20,
                        height: _getHeight(_applyCtrl.index == 0 ? _applyList.tome.count : _applyList.byme.count),
                        color: color_fafafa,
                        child: TabPageView(
                          bgColor: color_fafafa,
                          useOtherTabbar: true,
                          tabbarStyle: st_16(fontWeight: FontWeight.w500, textColor: color_999999),
                          focusedTabbarStyle: st_b_16(fontWeight: FontWeight.w700),
                          tabbarPadding: padding_20_LR,
                          onCreateView: (TabController ctrl) {
                            _applyCtrl = ctrl;
                            _applyCtrl.addListener(() {
                              if (_applyCtrl.indexIsChanging) onUpDate();
                            });
                          },
                          tabbarName: [
                            TabItem(title: "${"나에게지원".tr()} ${_applyList.tome.count}${"명".tr()}"),
                            TabItem(title: "${"내가지원".tr()} ${_applyList.byme.count}${"명".tr()}"),
                          ],
                          tabbarView: [
                            _applyList.tome.isNotEmpty
                                ? LinkmomApplyTab(
                                    controller: _controller,
                                    onClickAction: (e) => _showDetail(e, false, isMatch: true),
                                    results: _applyList.tome.data,
                                    bywho: TOME,
                                    hideStatus: false,
                                  )
                                : MatchingEmptyView(
                                    isLoading: flag.isLoading,
                                    type: 3,
                                    isBookingjobs: auth.indexInfo.linkmominfo.isBookingjobs,
                                    onClick: () => _btnAction(),
                                  ),
                            _applyList.byme.isNotEmpty
                                ? LinkmomApplyTab(
                                    controller: _controller,
                                    onClickAction: (e) => _showDetail(e, true, isMatch: true),
                                    results: _applyList.byme.data,
                                    bywho: BYME,
                                    hideStatus: false,
                                  )
                                : MatchingEmptyView(
                                    isLoading: flag.isLoading,
                                    type: 3,
                                    isBookingjobs: auth.indexInfo.linkmominfo.isBookingjobs,
                                    onClick: () => _btnAction(),
                                  ),
                          ],
                        ),
                      )),
                  onRefresh: () {
                    flag.enableLoading(fn: () => onUpDate());
                    _requestApplyList(StringUtils.formatYMD(_applyList.range.start), StringUtils.formatYMD(_applyList.range.end));
                  }),
            ),
            isOnlyLoading: true,
          ),
        ),
        lModalProgressHUD(
          flag.isLoading,
          NextRequestListener(
            onNextRequest: () => _requestMatchList(next: true),
            child: LinkmomRefresh(
                controller: _controller,
                padding: padding_0,
                anotherScrollView: true,
                child: MatchingListView(
                    data: data,
                    count: _matchList.tome.count + _matchList.byme.count,
                    listType: ListType.lMatching,
                    range: _matchList.range,
                    filter: [
                      FilterOption(type: FilterType.careState, info: CareStateType.values.map((e) => FilterInfo(value: e.value, title: e.name, desc: e.desc)).toList()),
                      FilterOption(type: FilterType.serviceType, info: ServiceType.values.sublist(0, 3).map((e) => FilterInfo(value: e.index, title: e.string)).toList()),
                      FilterOption(type: FilterType.bywho, info: [FilterInfo(value: 1, title: "나에게지원한".tr(), desc: "나에게지원한_설명".tr()), FilterInfo(value: 2, title: "내가지원한".tr(), desc: "내가지원한_설명".tr())])
                    ],
                    calendar: _buildCalendar(
                      events: data.caresItem.linkmomMatchEvents,
                      onCalendarChanged: _onMatchCalendarChanged,
                      onSelected: (date, events) {
                        return false;
                      },
                    ),
                    items: Container(
                      height: _getHeight(_matchCtrl.index == 0 ? _matchList.tome.data.length : _matchList.byme.data.length),
                      padding: padding_20,
                      color: color_fafafa,
                      child: TabPageView(
                        onCreateView: (TabController ctrl) {
                          _matchCtrl = ctrl;
                          _matchCtrl.addListener(() {
                            if (_matchCtrl.indexIsChanging) onUpDate();
                          });
                        },
                        bgColor: color_fafafa,
                        useOtherTabbar: true,
                        focusedTabbarStyle: st_b_16(fontWeight: FontWeight.w700),
                        tabbarStyle: st_16(fontWeight: FontWeight.w500, textColor: color_999999),
                        tabbarPadding: padding_20_LR,
                        tabbarName: [
                          TabItem(title: "${"나에게지원".tr()} ${_matchList.tome.count}${"명".tr()}"),
                          TabItem(title: "${"내가지원".tr()} ${_matchList.byme.count}${"명".tr()}"),
                        ],
                        tabbarView: [
                          Container(
                            width: double.infinity,
                            child: _matchList.tome.isNotEmpty
                                ? LinkmomApplyTab(
                                    isMatchingView: false,
                                    controller: _controller,
                                    onClickAction: (e) => _showDetail(e, false, isMatch: false),
                                    results: _matchList.tome.data,
                                    bywho: TOME,
                                    hideStatus: false,
                                  )
                                : MatchingEmptyView(
                                    isLoading: flag.isLoading,
                                    isBookingjobs: auth.indexInfo.linkmominfo.isBookingjobs,
                                    type: 2,
                                    onClick: () => _btnAction(),
                                  ),
                          ),
                          Container(
                            width: double.infinity,
                            child: _matchList.byme.isNotEmpty
                                ? LinkmomApplyTab(
                                    isMatchingView: false,
                                    controller: _controller,
                                    onClickAction: (e) => _showDetail(e, true, isMatch: false),
                                    results: _matchList.byme.data,
                                    bywho: BYME,
                                    hideStatus: false,
                                  )
                                : MatchingEmptyView(
                                    isLoading: flag.isLoading,
                                    isBookingjobs: auth.indexInfo.linkmominfo.isBookingjobs,
                                    type: 2,
                                    onClick: () => _btnAction(),
                                  ),
                          ),
                        ],
                      ),
                    )),
                onRefresh: () {
                  flag.enableLoading(fn: () => onUpDate());
                  _requestMatchList();
                }),
          ),
          isOnlyLoading: true,
        ),
      ],
      floatingActionButton: FloatingActionButton(
        heroTag: USER_TYPE.link_mom,
        backgroundColor: Commons.getColor(),
        onPressed: () => Commons.moveApplyPage(context, data, viewMode: ViewMode(viewType: ViewType.modify)),
        child: Container(alignment: Alignment.center, child: Lcons.care_edit(color: Colors.white, size: 35)),
      ),
    );
  }

  Future _requestApplyList(String? first, String? last, {bool next = false, bool fromCalendar = false}) async {
    try {
      if (next && _applyNext.isEmpty) return;
      if (!next) _applyList.clear();
      LinkmomApplyListRequest tome = LinkmomApplyListRequest(s_date: first, e_date: last, status: HireStatus.values.map((e) => e.value).toList(), bywho: TOME, servicetype: ServiceType.values.sublist(0, 3).map((e) => e.index).toList());
      await apiHelper.requestLinkmomApplyList(tome, url: next ? _applyNext : null).then((response) {
        _updateInfo(response, _applyNext, _applyList.tome, _applyList.range, data.caresItem.linkmomApplyEvents);
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      LinkmomApplyListRequest byme = LinkmomApplyListRequest(s_date: first, e_date: last, status: HireStatus.values.map((e) => e.value).toList(), bywho: BYME, servicetype: ServiceType.values.sublist(0, 3).map((e) => e.index).toList());
      apiHelper.requestLinkmomApplyList(byme, url: next ? _applyNext : null).then((response) {
        _updateInfo(response, _applyNext, _applyList.byme, _applyList.range, data.caresItem.linkmomApplyEvents);
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestMatchList({bool next = false, bool fromCalendar = false}) async {
    try {
      List<int> status = [
        CareStatus.ready.index,
        CareStatus.inprogress.index,
        CareStatus.end.index,
        CareStatus.cancelParts.index,
      ];
      if (next && _matchNext.isEmpty) return;
      if (!next) _matchList.clear();
      MatchingListRequest tome = MatchingListRequest(s_date: _first, e_date: _last, care_status: status, bywho: 'matched' + TOME, servicetype: ServiceType.values.sublist(0, 3).map((e) => e.value).toList());
      await apiHelper.requestLinkmomMatchingList(tome, url: next ? _matchNext : null).then((response) {
        _updateInfo(response, _matchNext, _matchList.tome, _matchList.range, data.caresItem.linkmomMatchEvents);
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      MatchingListRequest byme = MatchingListRequest(s_date: _first, e_date: _last, care_status: status, bywho: 'matched' + BYME, servicetype: ServiceType.values.sublist(0, 3).map((e) => e.value).toList());
      apiHelper.requestLinkmomMatchingList(byme, url: next ? _matchNext : null).then((response) {
        _updateInfo(response, _matchNext, _matchList.byme, _matchList.range, data.caresItem.linkmomMatchEvents);
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _parseEvents(int id, List events, Map calenderEvents) {
    // string list -> DateTime list
    events.forEach((element) {
      DateTime date = StringUtils.parseYMD(element);
      date = Jiffy(date.toLocal()).startOf(Units.DAY).dateTime;
      if (!calenderEvents.containsKey(date)) {
        calenderEvents.putIfAbsent(date, () => [id]);
      } else {
        if (!calenderEvents[date].contains(id)) calenderEvents[date].add(id);
      }
    });
  }

  void _updateRange(DateTime start, DateTime end) {
    _first = StringUtils.formatYMD(start);
    _last = StringUtils.formatYMD(end);
  }

  void _onApplyCalendarChanged(DateTime focused) {
    log.i("_onApplyCalendarChanged $focused");
    _applyList.range = DateTimeRange(start: StringUtils.getFirstDay(focused), end: StringUtils.getLastDay(focused));
    _updateRange(_applyList.range.start, _applyList.range.end);
    _requestApplyList(StringUtils.formatYMD(_applyList.range.start), StringUtils.formatYMD(_applyList.range.end), fromCalendar: true);
  }

  void _onMatchCalendarChanged(DateTime focused) {
    log.i("_onMatchCalendarChanged $focused");
    _matchList.range = DateTimeRange(start: StringUtils.getFirstDay(focused), end: StringUtils.getLastDay(focused));
    _updateRange(_matchList.range.start, _matchList.range.end);
    _requestMatchList(fromCalendar: true);
  }

  Future<void> _showDetail(LinkmomResultsData ret, bool fromMe, {bool isMatch = false}) async {
    var result = await Commons.page(context, routeLinkmomApply,
        arguments: LinkmomApplyPage(
            data: data,
            title: isMatch
                ? fromMe
                    ? "내가지원한맘대디".tr()
                    : "나에게지원한맘대디".tr()
                : "돌봄진행상황".tr(),
            id: ret.matching_id,
            isMatchingView: isMatch));
    if (result != null) {
      flag.enableLoading(fn: () => onUpDate());
      _requestApplyList(null, null).then((value) => _requestMatchList());
    }
  }

  void _btnAction() {
    if (auth.indexInfo.linkmominfo.isBookingjobs) {
      Commons.pageToMain(context, routeHome, arguments: MainHome(tabIndex: TAB_LIST));
    } else {
      Commons.moveApplyPage(context, data);
    }
  }

  void _updateInfo(dynamic response, String next, LinkmomScheduleData schedule, DateTimeRange range, Map<DateTime, List<int>> calenderEvents) {
    if (response.getCode() == KEY_SUCCESS) {
      PageData page = response.getData();
      next = response.getData().next;
      schedule.count = response.getData().count ?? 0;
      page.results.forEach((e) {
        _parseEvents(EnumUtils.getServiceTypeIndex(e.care_scheduleinfo.servicetype), e.care_scheduleinfo.caredate, calenderEvents);
        schedule.data.add(e);
      });
    }
    Future.delayed(
        Duration(seconds: 1),
        () => {
              flag.disableLoading(fn: () => onUpDate()),
            });
  }

  double _getHeight(int length) {
    return ((length < 1 ? 1 : length) * 250) + 100;
  }

  CalendarView _buildCalendar({
    required bool Function(DateTime, List<int>) onSelected,
    required Map<DateTime, List<int>> events,
    required Function(DateTime) onCalendarChanged,
  }) {
    return CalendarView(
      selected: onSelected,
      colors: careColor,
      height: 50,
      events: events,
      holidays: HolidaysHelper.getHolidays,
      onCalendarChanged: onCalendarChanged,
    );
  }
}

class LinkmomSchedule {
  LinkmomScheduleData tome;
  LinkmomScheduleData byme;
  DateTimeRange range;

  LinkmomSchedule({required this.tome, required this.byme, required this.range});

  void clear() {
    tome.data.clear();
    tome.count = 0;
    byme.data.clear();
    byme.count = 0;
  }
}

class LinkmomScheduleData {
  String bywho;
  int count;
  List<LinkmomResultsData> data;

  LinkmomScheduleData(this.bywho, this.data, {this.count = 0});

  bool get isEmpty => count < 1;

  bool get isNotEmpty => count > 0;

  void clear() => {data.clear(), count = 0};
}
