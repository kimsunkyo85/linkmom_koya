import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/notification_delete_request.dart';
import 'package:linkmom/data/network/models/notification_list_request.dart';
import 'package:linkmom/data/network/models/notification_list_response.dart';
import 'package:linkmom/data/push/model/linkmompush.dart';
import 'package:linkmom/utils/route_action.dart';
import 'package:linkmom/view/link_page.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:provider/provider.dart';

int badgeCare = 0;
int badgeCommunity = 0;
int badgeMy = 0;
int badgeEvent = 0;
int badgeCs = 0;

class NotiCenterPage extends BaseStateful {
  final DataManager? data;

  NotiCenterPage({this.data});

  @override
  _NotiCenterPageState createState() => _NotiCenterPageState();
}

class _NotiCenterPageState extends BaseStatefulState<NotiCenterPage> {
  List<String?> _next = List.filled(6, '');
  List<String> _previous = List.filled(6, '');
  bool _deleteMode = false;
  late TabController _ctrl;
  List<int> _deleteList = [];
  int _index = 0;
  List<NotiList> _list = List.generate(6, (i) => NotiList(deleteAction: (i) => {}, clickAction: (i, p) => {}, request: (t) => {}));

  List<List<NotificationData>> get _notiList => data.notification.list;

  @override
  void initState() {
    super.initState();
    if (widget.data != null) data = widget.data!;
    _buildList();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _ctrl.addListener(() {
        _index = _ctrl.index;
        setBadgeClear(_index);
        onUpDate();
      });
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  @override
  Widget build(BuildContext context) {
    final _pushData = Provider.of<LinkmomPushData>(context, listen: true);
    if (_pushData.pushData.chatSendData?.pagename != NotificationType.chatting.value) {
      int pageName = _pushData.pushData.chatSendData?.pagename ?? 0;
      setBadgeCount(_getIndex(pageName));
      _pushData.setDataClear();
      onUpDate();
    }

    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: diaryAppbar(
                title: "알림".tr(),
                rightW: _index == NotificationTabType.all.index || _index == NotificationTabType.event.index || _index == NotificationTabType.cs.index
                    ? Container()
                    : lInkWell(
                        onTap: () {
                          _deleteMode = !_deleteMode;
                          onUpDate();
                        },
                        child: Container(padding: padding_20_LTR, child: lText(_deleteMode ? "삭제취소".tr() : "알림삭제".tr(), style: st_15(textColor: color_b2b2b2, fontWeight: FontWeight.w500))))),
            body: SlideTabPageView(
              tabbarStyle: st_b_16(fontWeight: FontWeight.w500),
              tabbarName: [
                TabItem(title: "전체".tr()),
                TabItem(title: "돌봄관리".tr(), badge: badgeCare),
                TabItem(title: "커뮤니티".tr(), badge: badgeCommunity),
                TabItem(title: "마이페이지".tr(), badge: badgeMy),
                TabItem(title: "이벤트".tr(), badge: badgeEvent),
                TabItem(title: "고객센터".tr(), badge: badgeCs),
              ],
              tabbarView: _notiList
                  .map((e) => NextRequestListener(
                        onNextRequest: () => _requestNext(_getType(_notiList.indexOf(e))),
                        child: NotiList(
                          data: _notiList[_notiList.indexOf(e)],
                          type: _getType(_notiList.indexOf(e)),
                          userType: storageHelper.user_type,
                          deleteAction: (id, page, type) {
                            _deleteList.addAll(id);
                            log.d({
                              '--- TITLE    ': '--------------- DELETE NOTIFICATION ---------------',
                              '--- ids  ': '${id.toString()}',
                            });
                            _requestDelete(_deleteList, page, type);
                          },
                          showDelete: (_getType(_notiList.indexOf(e)) == NotificationType.event || _getType(_notiList.indexOf(e)) == NotificationType.cs) ? false : _deleteMode,
                          clickAction: (page, id, info) {
                            Commons.penaltyCheck(() => _movePage(page, id, info), forceEnable: EnumUtils.getNotificationType(page) == NotificationType.diary);
                          },
                          request: (type) => _requestNotiList(type == 0 ? null : type),
                        ),
                      ))
                  .toList(),
              onCreateView: (ctrl) => _ctrl = ctrl,
            )));
  }

  NotificationType _getType(int index) {
    switch (index) {
      case 1:
        return NotificationType.chatCareManage;
      case 2:
        return NotificationType.community;
      case 3:
        return NotificationType.mypage;
      case 4:
        return NotificationType.event;
      case 5:
        return NotificationType.cs;
      default:
        return NotificationType.all;
    }
  }

  int _getIndex(int page) {
    NotificationType type = EnumUtils.getNotificationType(page);
    switch (type) {
      case NotificationType.care:
      case NotificationType.jobs:
      case NotificationType.diary:
      case NotificationType.chatCareManage:
      case NotificationType.careManage:
      case NotificationType.pay:
        return NotificationTabType.care.index;
      case NotificationType.community:
      case NotificationType.communityBoard:
        return NotificationTabType.community.index;
      case NotificationType.mypage:
      case NotificationType.mypagePenalty:
      case NotificationType.mypageReview:
      case NotificationType.mypageExpiredPoint:
        return NotificationTabType.mypage.index;
      case NotificationType.event:
        return NotificationTabType.event.index;
      case NotificationType.cs:
      case NotificationType.csNotice:
      case NotificationType.csContact:
        return NotificationTabType.cs.index;
      default:
        return NotificationTabType.all.index;
    }
  }

  Future<List<NotificationData>> _requestNotiList(int page) async {
    try {
      int index = _getIndex(page);
      NotificationListRequest req = NotificationListRequest(pagename: page == NotificationType.all.value ? null : page /*, gubun: storageHelper.user_type.index + 1*/);
      var response = await apiHelper.reqeustNotificationList(req);
      if (response.getCode() == KEY_SUCCESS) {
        _notiList[index] = [];
        _next[index] = response.getData().next;
        _previous[index] = response.getData().previous;
        _notiList[index] = _exceptChat(response.getData().results);
        _buildList();
      }
      onUpDate();
      return _notiList[index];
    } catch (e) {
      return [];
    }
  }

  void _requestNext(NotificationType type) async {
    try {
      int page = _getIndex(type.value);
      if (_next[page] == null || _next[page]!.isEmpty) {
        log.d('Next is empty');
        return;
      }
      NotificationListRequest req = NotificationListRequest(pagename: type == NotificationType.all ? null : type.value /*, gubun: storageHelper.user_type.index + 1*/);
      apiHelper.reqeustNotificationList(req, url: _next[page]).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _next[page] = response.getData().next;
          _previous[page] = response.getData().previous;
          _notiList[page].addAll(_exceptChat(response.getData().results));
        }
        onUpDate();
      });
    } catch (e) {
    }
  }

  void _requestDelete(List<int> list, int pagename, int gubun) {
    if (list.isEmpty) return;
    try {
      NotificationDeleteRequest req = NotificationDeleteRequest(pagename: pagename, id: list /*, gubun: gubun*/);
      apiHelper.reqeustNotificationDelete(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          list.forEach((index) => _list.removeAt(index));
        }
        onUpDate();
      });
    } catch (e) {
    }
  }

  void _movePage(int typeValue, int? id, NotificationMsgData data) {
    NotificationType type = EnumUtils.getNotificationType(typeValue);
    RouteAction.onNotification(context, id ?? 0, type, data);
  }

  List<NotificationData> _exceptChat(List<NotificationData> response) {
    return response.where((element) => element.pagename != 0).toList();
  }

  ///badge초기화
  setBadgeClear(int _index) {
    if (_index == NotificationTabType.care.index) {
      badgeCare = 0;
    } else if (_index == NotificationTabType.community.index) {
      badgeCommunity = 0;
    } else if (_index == NotificationTabType.mypage.index) {
      badgeMy = 0;
    } else if (_index == NotificationTabType.event.index) {
      badgeEvent = 0;
    } else if (_index == NotificationTabType.cs.index) {
      badgeCs = 0;
    }
  }

  ///badge활성화
  setBadgeCount(int _index) {
    if (_index == NotificationTabType.care.index) {
      badgeCare++;
    } else if (_index == NotificationTabType.community.index) {
      badgeCommunity++;
    } else if (_index == NotificationTabType.mypage.index) {
      badgeMy++;
    } else if (_index == NotificationTabType.event.index) {
      badgeEvent++;
    } else if (_index == NotificationTabType.cs.index) {
      badgeCs++;
    }
  }

  void _buildList() {
    _list = _notiList
        .map((e) => NotiList(
              deleteAction: (i) => {},
              clickAction: (i, p) => {},
              request: (t) => {},
              type: _getType(_notiList.indexOf(e)),
              userType: storageHelper.user_type,
              data: e,
            ))
        .toList();
  }
}

class NotiList extends StatefulWidget {
  final NotificationType type;
  final List<NotificationData>? data;
  final USER_TYPE userType;
  final Function deleteAction;
  final Function clickAction;
  final bool showDelete;
  final Function request;

  NotiList({this.type = NotificationType.chatCareManage, this.userType = USER_TYPE.mom_daddy, required this.deleteAction, this.showDelete = false, required this.clickAction, required this.request, this.data});

  @override
  _NotiListState createState() => _NotiListState();
}

class _NotiListState extends State<NotiList> {
  List<NotificationData> _list = [];
  bool _hide = false;

  @override
  void initState() {
    super.initState();
    if (widget.data != null) _list = widget.data!;
    _request();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
            child: widget.data != null && widget.data!.isNotEmpty
                ? AnimatedOpacity(
                    onEnd: () => setState(() => _hide = true),
                    duration: Duration(milliseconds: 200),
                    opacity: _hide ? 0 : 1,
                    child: Container(
                        child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
                      if (widget.showDelete)
                        Container(
                          width: double.infinity,
                          alignment: Alignment.centerRight,
                          color: color_eeeeee.withOpacity(0.52),
                          padding: padding_20,
                          child: lInkWell(
                            onTap: () {
                              showNormalDlg(
                                  context: context,
                                  message: '${widget.type.name} ${"알림을모두삭제하시겠습니까".tr()}',
                                  btnLeft: "취소".tr(),
                                  onClickAction: (a) {
                                    setState(() {
                                      if (a == DialogAction.yes) {
                                        widget.deleteAction(widget.data!.map((e) => e.id).toList(), widget.type.value, widget.userType.index + 1);
                                      }
                                    });
                                  });
                            },
                            child: lText("전체삭제".tr(), style: st_15(textColor: color_999999, fontWeight: FontWeight.w500)),
                          ),
                        ),
                      Expanded(
                          child: LinkmomRefresh(
                        anotherScrollView: true,
                        onRefresh: () => _request(),
                        child: ListView(
                          physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                          children: _list
                              .map((e) => NotiUnit(
                                    userType: widget.userType,
                                    id: e.id,
                                    dataId: e.msgData!.nextId == 0 ? e.nextId : e.msgData!.nextId,
                                    body: e.message,
                                    date: e.senddate,
                                    isRead: e.isReceive,
                                    data: e.msgData,
                                    type: EnumUtils.getNotificationType(e.pagename),
                                    showDelete: widget.showDelete,
                                    onDeleteAction: widget.deleteAction,
                                    onClickAction: widget.clickAction,
                                  ))
                              .toList(),
                        ),
                      )),
                      Container(
                        padding: padding_20,
                        width: double.infinity,
                        child: lIconText(title: "알림메세지_유효기간안내".tr(), icon: Lcons.info(size: 16, color: color_dedede, isEnabled: true), textStyle: st_12(textColor: color_999999)),
                      ),
                    ])))
                : ListView(
                    physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                    children: [
                      Container(
                        child: lEmptyView(),
                        constraints: BoxConstraints(
                          maxHeight: contentHeight(context),
                        ),
                      )
                    ],
                  )),
      ],
    );
  }

  void _request() {
    widget.request(widget.type.value).then((resp) {
      setState(() {
        _list = resp;
      });
    });
  }
}

class NotiUnit extends StatefulWidget {
  final USER_TYPE userType;
  final NotificationType type;
  final int id;
  final int dataId;
  final int? page;
  final String? title;
  final String body;
  final String date;
  final bool isRead;
  final bool showDelete;
  final NotificationMsgData? data;
  final Function onDeleteAction;
  final Function onClickAction;

  NotiUnit({this.userType = USER_TYPE.mom_daddy, this.id = 0, this.dataId = 0, this.title, this.body = '', this.date = '', this.isRead = false, this.showDelete = false, required this.onDeleteAction, required this.onClickAction, this.type = NotificationType.chatCareManage, this.page, this.data});

  @override
  _NotiUnitState createState() => _NotiUnitState();
}

class _NotiUnitState extends State<NotiUnit> {
  bool _isHide = false;
  bool _isHideEnd = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
        onEnd: () => setState(() => _isHideEnd = true),
        opacity: _isHide ? 0 : 1,
        duration: Duration(milliseconds: 200),
        child: _isHideEnd
            ? Container()
            : lInkWell(
                onTap: () => widget.onClickAction(widget.type.value, widget.dataId, widget.data),
                child: Container(
                  padding: padding_15,
                  decoration: BoxDecoration(
                    border: Border(bottom: BorderSide(color: color_eeeeee)),
                    color: Colors.white,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (!widget.isRead) lDot(padding: padding_0),
                      Container(
                        margin: padding_03_T,
                        padding: padding_07,
                        decoration: BoxDecoration(color: Commons.getColor(), borderRadius: BorderRadius.circular(50)),
                        child: Lcons.getNotiIcons(widget.type),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          padding: padding_15_LR,
                          width: double.infinity,
                          child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.start, children: [
                            lText(widget.title ?? widget.type.name, style: st_b_13(fontWeight: FontWeight.w700)),
                            sb_h_02,
                            lText(widget.body, style: st_b_14(), overflow: TextOverflow.visible),
                            sb_h_08,
                            lText(StringUtils.parseYYMDAHM(widget.date), style: st_13(textColor: color_b2b2b2)),
                          ]),
                        ),
                      ),
                      if (widget.showDelete)
                        lInkWell(
                          onTap: () {
                            setState(() {
                              _isHide = true;
                              widget.onDeleteAction([widget.id], widget.type.value, widget.userType.index + 1);
                            });
                          },
                          child: Padding(padding: padding_20_TB, child: Lcons.clear(color: color_999999, size: 20, isEnabled: true)),
                        )
                    ],
                  ),
                )));
  }
}

class NotiModel {
  final int type;
  final String message;
  final String date;
  final bool isRead;

  NotiModel({required this.type, required this.message, required this.date, this.isRead = false});
}
