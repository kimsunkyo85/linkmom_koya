import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateless.dart';

class TabReview extends BaseStateless {
  TabReview({Key? key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        title,
        style: TextStyle(fontSize: 40),
      ),
    );
  }
}
