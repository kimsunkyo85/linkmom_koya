import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/area_address_response.dart';
import 'package:linkmom/data/network/models/area_code_search_response.dart';
import 'package:linkmom/data/network/models/job_list_request.dart';
import 'package:linkmom/data/network/models/linkmom_list_response.dart';
import 'package:linkmom/data/network/models/linkmom_wish_list_requests.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/home_provider.dart';
import 'package:linkmom/utils/listview/list_linkmom_view.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/address_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/linkmom_profile_page.dart';
import 'package:linkmom/view/main/job_list/search_util.dart';
import 'package:linkmom/view/main/mypage/review/review_view_page.dart';
import 'package:provider/provider.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';
import 'count_sort_view.dart';
import 'job_filter_page.dart';
import 'location_view.dart';
import 'servicetype_view.dart';

class LinkMomListPage extends BaseStateful {
  @override
  _LinkMomListPageState createState() => _LinkMomListPageState();

  LinkMomListPage({this.data, this.viewMode, this.sendCode = 0});

  final DataManager? data;
  final ViewMode? viewMode;
  final int sendCode;
}

class _LinkMomListPageState extends BaseStatefulState<LinkMomListPage> {
  late ScrollController _controller;
  bool _visible = false;

  List<AreaCodeSearchData> areaSelects = [];
  AreaCodeSearchData codeData = AreaCodeSearchData();
  SendCodeType sendCode = SendCodeType.SCDefault;

  @override
  initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    areaSelects = [];
    super.initState();
    sendCode = EnumUtils.getSendCodeType(widget.sendCode);
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
    if (auth.user.username.isNotEmpty) {
      _requestAreaAddressView();
    }
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    data.listViewItem.address_name = auth.user.my_info_data!.auth_address!.getLocationName();
    setRequestData();
  }

  ///재조회
  Future<Null> _onRefresh() async {
    _visible = false;
    _requestLinkMomList();
  }

  ///스크롤 리스너
  _scrollListener() {
    if (_controller.offset <= _controller.position.minScrollExtent && !_controller.position.outOfRange) {
      if (_visible) {
        _visible = false;
        onUpDate();
      }
    } else {
      if (!_visible) {
        _visible = true;
        onUpDate();
      }
    }
  }

  ///상단으로 이동
  _moveUp() {
    // _controller.jumpTo(0);
    _controller.animateTo(0, curve: Curves.linear, duration: Duration(milliseconds: 100));
    _visible = false;
    onUpDate();
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<HomeNavigationProvider>(context);
    if (provider.listUpdated) {
      provider.updatedEvent(TAB_LIST);
      sendCode = EnumUtils.getSendCodeType(provider.sendCode);
      provider.setSendCodeReset();
      setRequestData();
      _requestAreaAddressView();
    }
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        body: Container(
          color: color_fafafa,
          child: ListNextView(
            controller: _controller,
            topper: Container(
              color: Colors.white,
              child: Column(
                children: [
                  locationView(data, callBack: (action, value) async {
                    if (action == DialogAction.location) {
                      if (value == "다른지역검색".tr()) {
                        var ret = await Commons.page(navigatorKey.currentContext, routeAddress, arguments: AddressPage(data: data));
                        if (ret != null) {
                          codeData = ret.addressItem.codeSearchData;
                          data.listViewItem.areaValue = codeData.getAddress();
                          data.listViewItem.bcode = codeData.bcode;
                          data.listViewItem.hcode = codeData.hcode;

                          bool isAdd = false;
                          data.listViewItem.lsAreaAddress.forEach((value) {
                            if (value.bcode == codeData.bcode && value.hcode == codeData.hcode && !isAdd) {
                              isAdd = true;
                              // 2022/06/23 codeData : AreaCodeSearchData{bcode: 4117310200, hcode: 4117355000, address1: 경기, address2: 안양시 동안구, address3: 관양1동, area_name: 관양동} 비정상 데이터
                              // -> 2022/06/23 codeData : AreaCodeSearchData{bcode: 4117310200, hcode: 4117355000, address1: 경기, address2: 안양시 동안구, address3: 관양동, area_name: 관양1동} 정상 데이터
                              // 기존에 동네 인증된 주소는 관양1동, 현재 선택된 동은 관양동으로 되어 있기 때문에 오류가 발생하여, 예외로 처리하여 추가한다.
                              if (data.listViewItem.areaValue != codeData.address3) {
                                data.listViewItem.areaValue = value.is_affiliate ? value.region_building_name : value.region_3depth;
                              }
                            }
                          });

                          AreaAddressData areaData = getAreaData(codeData);

                          if (areaSelects.isEmpty && !isAdd) {
                            areaSelects.add(codeData);
                            data.listViewItem.lsArea.add(areaData.getAddress());
                            data.listViewItem.lsAreaAddress.add(areaData);
                          } else {
                            if (!isAdd) {
                              if (areaSelects.length >= 3) {
                                areaSelects.add(codeData);
                                areaSelects.removeAt(0);
                                int pos = areaSelects.length;
                                data.listViewItem.lsArea.asMap().forEach((index, value) {
                                  if (value == "다른지역검색".tr()) {
                                    pos = index + 1;
                                  }
                                });
                                data.listViewItem.lsArea.add(areaData.getAddress());
                                data.listViewItem.lsAreaAddress.add(areaData);
                                data.listViewItem.lsArea.removeAt(pos);
                                data.listViewItem.lsAreaAddress.removeAt(pos);
                              } else {
                                areaSelects.add(codeData);
                                data.listViewItem.lsArea.add(areaData.getAddress());
                                data.listViewItem.lsAreaAddress.add(areaData);
                              }
                            }
                          }
                          _requestLinkMomList();
                        }
                      } else {
                        bool isRequest = false;
                        data.listViewItem.lsAreaAddress.forEach((item) {
                          if (value == item.getAddressListSearch() || value == sendCode.title) {
                            data.listViewItem.bcode = item.bcode;
                            data.listViewItem.hcode = item.hcode;
                            isRequest = true;
                          }
                        });
                        if (isRequest) {
                          _requestLinkMomList();
                        }
                      }
                    } else if (action == DialogAction.careZone) {
                      data.listViewItem.searchZoneValue = value;
                      _onRefresh();
                    } else if (action == DialogAction.filter) {
                      var _data = await Commons.page(context, routeJobFilter, arguments: JobFilterPage(data: data));
                      if (_data != null) {
                        data.listViewItem.jobListRequest = _data;
                        _onRefresh();
                      }
                    }
                    onUpDate();
                  }),
                  jobServiceTypeView(data, callBack: (action, type) async {
                    if (action == DialogAction.select) {
                      _onRefresh();
                    }
                  }),
                ],
              ),
            ),
            header: Container(
              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
              child: countSortView(data, cnt: getDataCount(), callBack: (action, value) {
                if (action == DialogAction.select) {
                  data.listViewItem.sortValue = value;
                  _onRefresh();
                } else if (action == DialogAction.mode) {
                  if (data.listViewItem.viewMode == ViewType.normal) {
                    data.listViewItem.viewMode = ViewType.summary;
                  } else {
                    data.listViewItem.viewMode = ViewType.normal;
                  }
                }
                onUpDate();
              }),
            ),
            view: data.listViewItem.linkMomListData.results != null
                ? data.listViewItem.linkMomListData.results!
                    .map((linkssam) => Container(
                          margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
                          decoration: BoxDecoration(boxShadow: [BoxShadow(color: Colors.black.withAlpha(11), offset: Offset(0, 4), blurRadius: 12.0)]),
                          child: LinkMomView(
                            linkssam,
                            viewMode: ViewMode(viewType: ViewType.view),
                            padding: padding_20,
                            lsAuthInfo: ListLinkMomViewState().getAuthInfo(linkssam),
                            viewType: data.listViewItem.viewMode,
                            onItemClick: (item, action) async {
                              if (item is LinkMomListResultsData) {
                                if (action == DialogAction.follower) {
                                  if (!item.userinfo!.is_follower) {
                                    apiHelper.requestLinkmomWishSave(LinkmomWishSaveRequest(bookingjobs: item.job_id)).then((response) {
                                      item.userinfo!.is_follower = response.getCode() == KEY_SUCCESS;
                                      onUpDate();
                                    });
                                  } else {
                                    apiHelper.requestLinkmomWishDelete(LinkmomWishDeleteRequest(bookingjobs: item.job_id)).then((response) {
                                      item.userinfo!.is_follower = response.getCode() != KEY_SUCCESS;
                                      onUpDate();
                                    });
                                  }
                                } else if (action == DialogAction.select) {
                                  var ret = await Commons.page(context, routeLinkMomProfile,
                                      arguments: LinkMomProfilePage(
                                        //2021/12/16 링크쌤 모드에서만 수정하도록 변경, 리스트에선 뷰로 본다.
                                        viewMode: ViewMode(viewType: ViewType.view),
                                        jobId: item.job_id,
                                      ));
                                  if (ret != null) {
                                    _onRefresh();
                                  }
                                } else if (action == DialogAction.review) {
                                  if (item.penalty_5) {
                                    showNormalDlg(context: context, message: "후기를볼수없는링크쌤입니다".tr());
                                  } else {
                                    Commons.page(context, routeReviewView, arguments: ReviewViewPage(userId: item.userinfo!.user_id, gubun: USER_TYPE.link_mom.index + 1));
                                  }
                                }
                              } else {
                                if (action == DialogAction.filter) {
                                  var _data = await Commons.page(context, routeJobFilter, arguments: JobFilterPage(data: data));
                                  if (_data != null) {
                                    data.listViewItem.jobListRequest = _data;
                                    _onRefresh();
                                  }
                                }
                              }
                            },
                          ),
                        ))
                    .toList()
                : [],
            onRefresh: _onRefresh,
            onNext: () => _requestNext(data.listViewItem.responseLinkMomData.getData().next),
            showNextLoading: data.listViewItem.responseLinkMomData.getData().next.isNotEmpty,
            emptyView: lEmptyView(
              isLoading: flag.isLoading,
              height: Commons.height(context, 0.5),
              img: Lcons.no_data_linkssam().name,
              title: "해당조건의링크쌤이없어요".tr(),
              textString: "다른조건으로재검색".tr(),
              paddingBtn: padding_50_LR,
              fn: () async {
                var _data = await Commons.page(context, routeJobFilter, arguments: JobFilterPage(data: data));
                if (_data != null) {
                  data.listViewItem.jobListRequest = _data;
                  _onRefresh();
                }
              },
            ),
          ),
        ),
        floatingActionButton: AnimatedOpacity(
          duration: Duration(milliseconds: 500),
          opacity: 1.0, //_visible ? 1.0 : 0.0,
          child: Visibility(
            visible: _visible,
            child: Padding(
              padding: padding_20,
              child: Align(
                alignment: Alignment.bottomRight,
                child: InkWell(onTap: _moveUp, child: Lcons.top(size: 40)),
              ),
            ),
          ),
        ),
      ),
      isOnlyLoading: true,
    );
  }

  ///동네 최근 인증 리스트 가져오기
  _requestAreaAddressView() async {
    flag.enableLoading(fn: () => onUpDate());
    await apiHelper.requestAreaAddressView().then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        // data.listViewItem.setClear();
        data.listViewItem.addressResponse = response;

        ///화면을 나갔다 들어오면 초기화 한다.
        data.listViewItem.jobListRequest = JobListRequest.init();
        onDataPage(response);
        _requestLinkMomList();
      } else {
        data.listViewItem.addressResponse = AreaAddressResponse();
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  ///링크쌤 리스트 조회
  _requestLinkMomList() async {
    setRequestData();
    flag.enableLoading(fn: () => onUpDate());
    await apiHelper.requestLinkMomList(data.listViewItem.jobListRequest.toFormData(), ApiEndPoint.EP_BOOKING_LINKMOM_LIST).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        data.listViewItem.responseLinkMomData = response;
      } else {
        data.listViewItem.linkMomListData = LinkMomListData();
        data.listViewItem.responseLinkMomData = LinkMomListResponse();
      }
      onDataPage(data.listViewItem.responseLinkMomData);
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }

  _requestNext(String url) {
    try {
      if (url.isEmpty) return;
      flag.enableEndLoading(fn: () => onUpDate());
      apiHelper.requestLinkMomList(data.listViewItem.jobListRequest.toFormData(), url).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          data.listViewItem.responseLinkMomData = response;
          data.listViewItem.linkMomListData.results!.addAll(response.getData().results!);
        }
        flag.disableEndLoading(fn: () => onUpDate());
      }).catchError((e) {
        flag.disableEndLoading(fn: () => onUpDate());
      });
    } catch (e) {}
  }

  @override
  void onDataPage(_data) {
    if (_data is AreaAddressResponse) {
      data.listViewItem.lsAreaAddress = _data.getDataList();
      data.listViewItem.lsArea.clear();
      data.listViewItem.lsAreaAddress.forEach((value) {
        data.listViewItem.lsArea.add(value.getAddressListSearch());
      });

      AreaAddressData item = data.listViewItem.lsAreaAddress.first;
      //처음 호출시 첫번째로 세팅 후 목록을 조회하다.
      data.listViewItem.lsArea.add("다른지역검색".tr());

      //Push나 알림으로 들어와서 모드 변경시 해당 값을 세팅하지 않고 초기화한다.
      if (sendCode.isRegion) {
        if (sendCode.title.isNotEmpty) {
          data.listViewItem.lsArea.add(sendCode.title);
        }
        if (!data.listViewItem.isSendCode) {
          data.listViewItem.isSendCode = true;
          data.listViewItem.areaValue = sendCode.title;
          data.listViewItem.bcode = '0';
          data.listViewItem.hcode = '0';
        }
      }

      data.listViewItem.areaValue = data.listViewItem.areaValue.isNotEmpty ? data.listViewItem.areaValue : item.getAddressListSearch();
      data.listViewItem.bcode = data.listViewItem.bcode.isNotEmpty ? data.listViewItem.bcode : item.bcode;
      data.listViewItem.hcode = data.listViewItem.hcode.isNotEmpty ? data.listViewItem.hcode : item.hcode;

      try {
        if (!data.listViewItem.lsAreaAddress.contains(data.listViewItem.areaValue) && !sendCode.isRegion) {
          data.listViewItem.areaValue = item.getAddressListSearch();
          data.listViewItem.bcode = item.bcode;
          data.listViewItem.hcode = item.hcode;
        }
      } catch (e) {
        data.listViewItem.areaValue = item.getAddressListSearch();
        data.listViewItem.bcode = item.bcode;
        data.listViewItem.hcode = item.hcode;
      }

      //전체로 초기화한다.
      data.listViewItem.searchZoneValue = data.listViewItem.searchZoneValue.isNotEmpty ? data.listViewItem.searchZoneValue : SearchZoneStatus.all.name;

      if (areaSelects.isNotEmpty) {
        areaSelects.forEach((value) {
          AreaAddressData areaData = getAreaData(value);
          data.listViewItem.lsArea.add(areaData.getAddressListSearch());
          data.listViewItem.lsAreaAddress.add(areaData);
        });
      }

      setRequestData();
    } else if (_data is LinkMomListResponse) {
      if (_data.dataList == null || _data.dataList!.isEmpty) {
        return;
      }
      data.listViewItem.linkMomListData = _data.getData();
    }
  }

  ///검색 데이터 세팅 or 초기화
  void setRequestData() {
    //내위치 거리
    data.listViewItem.jobListRequest.radius = Commons.getRadiusType(data.listViewItem.radiusValue);
    //서비스선택(돌봄유형)
    data.listViewItem.jobListRequest.servicetype!.clear();

    data.listViewItem.lsServiceType.forEach((value) {
      if (value.isSelected) {
        data.listViewItem.jobListRequest.servicetype!.add(value.type);
      }
    });
    //정렬
    data.listViewItem.jobListRequest.orderingtype = Commons.getSortType(data.listViewItem.sortValue);

    data.listViewItem.jobListRequest.bcode = data.listViewItem.bcode;
    data.listViewItem.jobListRequest.hcode = data.listViewItem.hcode;

    data.listViewItem.jobListRequest.addr_gubun = EnumUtils.getSearchZoneStatus(data.listViewItem.searchZoneValue).value;

    if (data.listViewItem.areaValue == sendCode.title) {
      data.listViewItem.jobListRequest.is_region = sendCode.requestValue;
    } else {
      //돌봄희망지역이 아닐 경우 플래그를 초기화
      data.listViewItem.jobListRequest.is_region = 0;
    }
  }

  int getDataCount() {
    return data.listViewItem.linkMomListData.count;
  }
}
