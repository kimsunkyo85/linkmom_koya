import 'package:flutter/material.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

///이름
Widget nameView(String name, {TextStyle? firstStyle, TextStyle? secondStyle}) {
  var data = StringUtils.nameToFormat(name);
  if (firstStyle == null) {
    firstStyle = st_b_20(fontWeight: FontWeight.w500);
  }
  if (secondStyle == null) {
    secondStyle = st_b_17();
  }
  return data.isNotEmpty
      ? lNameText(name, style: firstStyle, highlightStyle: secondStyle)
      : lText(
          StringUtils.validateString(name) ? name : '',
          style: st_b_17(
            fontWeight: FontWeight.w500,
          ),
        );
}
