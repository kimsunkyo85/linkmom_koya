import 'package:linkmom/data/network/models/area_address_response.dart';
import 'package:linkmom/data/network/models/area_code_search_response.dart';

AreaAddressData getAreaData(AreaCodeSearchData data) {
  AreaAddressData areaData = AreaAddressData(
    bcode: data.bcode,
    hcode: data.hcode,
    region_1depth: data.address1,
    region_2depth: data.address2,
    region_3depth: data.area_name,
    region_3depth_h: data.address3,
  );
  return areaData;
}
