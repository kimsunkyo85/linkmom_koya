import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/dropdown/custom_dropdown.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/svg_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

///리스트 갯수 및 정렬 화면
Widget countSortView(DataManager data, {int cnt = 0, Function? callBack}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Padding(
        padding: padding_10_T,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            lText('${"총".tr()} $cnt${"명".tr()}'),
            sb_w_05,
            CustomDropDown(
              value: data.listViewItem.sortValue,
              itemsList: data.listViewItem.lsSort,
              isExpanded: false,
              icon: svgImage(imagePath: SVG_SORT, size: 25),
              style: st_b_14(),
              margin: padding_0,
              bgColor: color_transparent,
              onChanged: (value) {
                data.listViewItem.sortValue = value;
                if (callBack != null) {
                  callBack(DialogAction.select, value);
                }
              },
            ),
          ],
        ),
      ),
    ],
  );
}
