import 'package:flutter/material.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_job_servicetype_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

import '../../lcons.dart';

///돌봄유형 서비스 선택(등원, 하원, 보육, 학습)
Widget jobServiceTypeView(DataManager data, {Function? callBack}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Padding(
        padding: padding_location,
        child: Row(
          children: [
            Expanded(
              child: ListJobServiceTypeSelector(
                data: data.listViewItem.lsServiceType,
                isOverlap: true,
                onItemClick: (value) {
                  if (callBack != null) {
                    int type = (value as SingleItem).type;
                    callBack(DialogAction.select, type);
                  }
                },
              ),
            ),
          ],
        ),
      ),
      lDivider(color: color_eeeeee),
    ],
  );
}

Widget filterIcon() {
  return lRoundContainer(
    padding: EdgeInsets.fromLTRB(8, 5, 8, 5),
    bgColor: Commons.getColorReversBg(),
    child: Lcons.filter(size: 25),
  );
}
