import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/dropdown/custom_dropdown.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

///내 위치 정보, 내 위치 거리 선택 뷰
Widget locationView(DataManager data, {Function? callBack}) {
  return Column(
    children: [
      Padding(
        padding: padding_location,
        child: Row(
          children: [
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(
                  height: tabHeight,
                  child: CustomDropDown(
                    value: data.listViewItem.areaValue,
                    itemsList: data.listViewItem.lsArea,
                    isExpanded: false,
                    isDense: true,
                    leftWidget: Lcons.location(size: 20),
                    icon: Lcons.nav_bottom(color: color_222222),
                    style: st_b_17(fontWeight: FontWeight.bold),
                    onChanged: (value) async {
                      if (value != "다른지역검색".tr()) {
                        data.listViewItem.areaValue = value;
                      }
                      if (callBack != null) {
                        callBack(DialogAction.location, value);
                      }
                    },
                  ),
                ),
              ],
            ),
            // 동네인증지역,돌봄가능지역
            Expanded(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  SizedBox(
                    height: tabHeight,
                    child: CustomDropDown(
                      value: data.listViewItem.searchZoneValue,
                      itemsList: data.listViewItem.lsSearchZone,
                      margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                      isExpanded: false,
                      isDense: true,
                      icon: Lcons.nav_bottom(color: color_222222),
                      style: st_b_17(fontWeight: FontWeight.bold),
                      onChanged: (value) {
                        data.listViewItem.searchZoneValue = value;
                        if (callBack != null) {
                          callBack(DialogAction.careZone, value);
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
            sb_w_05,
            InkWell(
                onTap: () {
                  if (callBack != null) {
                    callBack(DialogAction.filter, null);
                  }
                },
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Lcons.filter(size: 25),
                  ],
                )),
          ],
        ),
      ),
      lDivider(color: color_eeeeee),
    ],
  );
}

/// 아이콘, 동이름, 제휴 이름
Widget locationAffiliateView({
  double sizeLocation = 20,
  Color colorLocation = color_545454,
  String address = '',
  Color textColor = color_545454,
  TextStyle? style,
  bool? isAffiliate = false,
  Widget? iconAffiliate,
  double sizeAffiliate = 15,
}) {
  iconAffiliate = iconAffiliate ?? Lcons.affiliate_ipark(size: sizeAffiliate);
  isAffiliate = isAffiliate ?? false;
  return Row(
    children: [
      Lcons.location(
        size: sizeLocation,
        disableColor: colorLocation,
      ),
      sb_w_04,
      lTextIcon(
        title: address,
        textStyle: style ??
            st_15(
              textColor: textColor,
            ),
        paddingRight: padding_02_L,
        rightIcon: isAffiliate ? iconAffiliate : null,
      ),
    ],
  );
}
