import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/job_list_request.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/listview/grid_care_target_select.dart';
import 'package:linkmom/utils/listview/grid_day_select.dart';
import 'package:linkmom/utils/listview/list_care_target_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/job_apply/job_profile/chip_view.dart';
import 'package:linkmom/view/main/job_apply/title_value_view.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';

class JobFilterPage extends BaseStateful {
  @override
  _JobProfilePageState createState() => _JobProfilePageState();

  JobFilterPage({this.data, this.viewMode});

  final DataManager? data;
  final ViewMode? viewMode;
}

class _JobProfilePageState extends BaseStatefulState<JobFilterPage> {
  @override
  initState() {
    super.initState();
    onViewMode(widget.viewMode ?? ViewMode());
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);

    ///복사한 다음 뒤로갈시 세팅을 하지 않는다.
    data.filterItem.request = JobListRequest.clone(_data.listViewItem.jobListRequest);
    onDataPage(data.filterItem.request);
  }

  ///초기화
  _reset() {}

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar(
          "상세검색".tr(),
          hide: false,
          reset: true,
          onClick: (action) {
            if (action == DialogAction.delete) {
              showNormalDlg(
                  message: "초기화 하시겠습니까?",
                  btnLeft: "아니오".tr(),
                  btnRight: "예".tr(),
                  onClickAction: (action) {
                    if (action == DialogAction.yes) {
                      data.filterItem.request = JobListRequest.init();

                      //시급
                      data.filterItem.currentRangeValues = RangeValues(
                        data.filterItem.request.costmin.toDouble(),
                        data.filterItem.request.costmax.toDouble(),
                      );

                      //돌봄장소
                      data.filterItem.lsArea.forEach((value) {
                        value.isSelected = false;
                      });

                      //가능한 지역 1순위, 2순위
                      data.tcHomeLocation1.text = '';
                      data.tcHomeLocation2.text = '';

                      //돌봄요일
                      data.filterItem.lsDay.forEach((value) {
                        value.isSelected = false;
                      });

                      //돌봄시간
                      data.filterItem.lsTime.asMap().forEach((index, value) {
                        value.isSelected = false;
                      });

                      //돌봄대상
                      data.filterItem.lsJobCareTargetType.forEach((value) {
                        value.isSelected = false;
                      });

                      //해시태그
                      data.filterItem.lsTag.forEach((value) {
                        value.isSelected = false;
                      });
                      onUpDate();
                    }
                  });
            }
          },
        ),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Column(
                children: [
                  Expanded(
                    child: lScrollView(
                      padding: padding_Item_TB,
                      child: Container(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            sb_h_15,
                            //희망급여
                            _payView(),
                            lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_15_TB),

                            //돌봄장소, 가능지역
                            Padding(
                              padding: padding_Item_LR,
                              child: _careAreaView(),
                            ),
                            lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_15_TB),

                            //돌봄요일,시간
                            Padding(
                              padding: padding_Item_LR,
                              child: _careDayView(),
                            ),
                            lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_15_TB),

                            //돌봄대상
                            Padding(
                              padding: padding_Item_LR,
                              child: _careTargetView(),
                            ),
                            lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_15_TB),

                            //해시태그
                            Padding(
                              padding: padding_Item_LR,
                              child: _careTagView(),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  _confirmButton(),
                ],
              ),
            ),
            // wdDivider(),
          ],
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn("저장".tr(), btnColor: Commons.getColor(), onClickAction: () {
      //시급
      data.filterItem.request.costmin = data.filterItem.currentRangeValues.start.toInt();
      data.filterItem.request.costmax = data.filterItem.currentRangeValues.end.toInt();

      //돌봄장소
      data.filterItem.request.possible_area!.clear();
      data.filterItem.lsArea.forEach((value) {
        if (value.isSelected) {
          data.filterItem.request.possible_area!.add(value.type);
        }
      });

      //가능한 지역 1순위, 2순위
      data.filterItem.request.area_code!.clear();
      data.filterItem.request.area_code_data!.forEach((value) {
        if (StringUtils.validateString(value.name)) {
          data.filterItem.request.area_code!.add(value.keyName);
        }
      });

      //돌봄요일
      data.filterItem.request.dayofweek!.clear();
      data.filterItem.lsDay.forEach((value) {
        if (value.isSelected) {
          data.filterItem.request.dayofweek!.add(value.type);
        }
      });

      //돌봄시간 임시
      data.filterItem.request.is_am = data.filterItem.lsTime[0].type;
      data.filterItem.request.is_pm = data.filterItem.lsTime[1].type;

      //돌봄대상
      data.filterItem.request.care_ages!.clear();
      data.filterItem.lsJobCareTargetType.forEach((value) {
        if (value.isSelected) {
          data.filterItem.request.care_ages!.add(value.type);
        }
      });

      //해시태그
      data.filterItem.request.authtag!.clear();
      data.filterItem.lsTag.forEach((value) {
        if (value.isSelected) {
          data.filterItem.request.authtag!.add(value.keyName);
        }
      });

      //1순위, 2순위 데이터 만들기
      data.filterItem.request.area_code_data!.forEach((value) {
        if (StringUtils.validateString(value.name)) {
          data.filterItem.request.area_code!.add(value.keyName); //법정동코드
        }
      });
      log.d('『GGUMBI』>>> _confirmButton : data.filterItem.request: ${data.filterItem.request},  <<< ');
      Commons.pagePop(context, data: data.filterItem.request);
    });
  }

  @override
  void onDataPage(_data) {
    if (_data is JobListRequest) {
      //시급
      data.filterItem.currentRangeValues = RangeValues(
        data.filterItem.request.costmin.toDouble(),
        data.filterItem.request.costmax.toDouble(),
      );

      //돌봄장소
      data.filterItem.lsArea.forEach((value) {
        data.filterItem.request.possible_area!.forEach((area) {
          if (area == value.type) {
            value.isSelected = true;
          }
        });
      });

      //가능한 지역 1순위, 2순위
      data.filterItem.request.area_code_data!.asMap().forEach((index, value) {
        if (index == 0 && value != null) {
          data.tcHomeLocation1.text = value.name;
        } else if (index == 1 && value != null) {
          data.tcHomeLocation2.text = value.name;
        }
      });

      //돌봄요일
      data.filterItem.lsDay.forEach((value) {
        data.filterItem.request.dayofweek!.forEach((day) {
          if (day == value.type) {
            value.isSelected = true;
          }
        });
      });

      //돌봄시간
      data.filterItem.lsTime.asMap().forEach((index, value) {
        if (index == 0 && data.filterItem.request.is_am == TimeType.on.index) {
          value.isSelected = true;
          value.type = TimeType.on.index;
        } else if (index == 1 && data.filterItem.request.is_pm == TimeType.on.index) {
          value.isSelected = true;
          value.type = TimeType.on.index;
        }
      });

      //돌봄대상
      data.filterItem.lsJobCareTargetType.forEach((value) {
        data.filterItem.request.care_ages!.forEach((type) {
          if (type == value.type) {
            value.isSelected = true;
          }
        });
      });

      //해시태그
      data.filterItem.lsTag.forEach((value) {
        data.filterItem.request.authtag!.forEach((area) {
          if (area == value.keyName) {
            value.isSelected = true;
          }
        });
      });
    }
    onUpDate();
  }

  ///시급,스피드매칭 화면
  Widget _payView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: padding_20_LR,
          child: titleValueView(getPayTitle(),
              style: st_b_18(
                fontWeight: FontWeight.w500,
              ),
              padding: padding_0),
        ),
        sb_h_15,
        Padding(
          padding: padding_20_LR,
          child: lText(
            '${"시급".tr()} ${StringUtils.formatPay(data.filterItem.request.costmin)}${"원".tr()} ~ ${StringUtils.formatPay(data.filterItem.request.costmax)}${"원".tr()}',
            style: st_18(textColor: Commons.getColor(), fontWeight: FontWeight.bold),
            textAlign: TextAlign.left,
          ),
        ),
        sb_h_05,
        SliderTheme(
          data: lSliderThemeData(
            context,
            activeTrackColor: Commons.getColor(),
            inactiveTrackColor: color_dedede,
            thumbColor: Commons.getColor(),
          ),
          child: RangeSlider(
              values: data.filterItem.currentRangeValues,
              min: 5000.0,
              max: 50000.0,
              divisions: 45,
              labels: RangeLabels(
                data.filterItem.request.costmin.round().toString(),
                data.filterItem.request.costmax.round().toString(),
              ),
              onChanged: (RangeValues values) {
                setState(() {
                  data.filterItem.currentRangeValues = values;
                  data.filterItem.request.costmin = data.filterItem.currentRangeValues.start.round().toInt();
                  data.filterItem.request.costmax = data.filterItem.currentRangeValues.end.round().toInt();
                });
              }),
        ),
        Padding(
          padding: padding_20_LR,
          child: Row(
            children: [
              Expanded(child: lText('${"최소".tr()} 5,000${"원".tr()}', style: st_14(textColor: color_999999))),
              Expanded(child: lText('${"최대".tr()} 50,000${"원".tr()}', style: st_14(textColor: color_999999), textAlign: TextAlign.right)),
            ],
          ),
        ),
        sb_h_15,
      ],
    );
  }

  ///돌봄장소
  Widget _careAreaView() {
    return Container(
      child: Column(children: [
        ListCareTargetSelector(
          title: "돌봄장소".tr(),
          data: data.filterItem.lsArea,
          user_type: storageHelper.user_type,
          padding: padding_15_LR,
          borderRadius: 26,
          height: 50.0,
          enableColor: Commons.getColor(),
          disableColor: color_999999,
          onItemClick: (value) {
            log.d('『GGUMBI』>>> _careAreaView ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
          },
        ),
        sb_h_20,
        // TODO: GGUMBI 2022/01/26 - 맘대디/링크쌤찾기 No.88처리사하면서 돌봄 1순위, 2순위 선택 주석처리
        // titleValueView(
        //   getAreaTitle(),
        //   style: st_b_18(fontWeight: FontWeight.w500),
        //   padding: padding_0,
        // ),
        // sb_h_10,
        // Row(
        //   crossAxisAlignment: CrossAxisAlignment.start,
        //   children: [
        //     Padding(
        //       padding: padding_12_T,
        //       child: lText("1순위".tr(), style: st_b_14(fontWeight: FontWeight.bold)),
        //     ),
        //     sb_w_15,
        //     Expanded(
        //       child: Form(
        //         key: data.formHomeLocation1,
        //         child: TextFormField(
        //           textAlignVertical: TextAlignVertical.center,
        //           controller: data.tcHomeLocation1,
        //           style: stLogin,
        //           readOnly: true,
        //           textInputAction: TextInputAction.done,
        //           keyboardType: TextInputType.emailAddress,
        //           decoration: underLineDecoration(
        //             "동명(읍,면)으로검색(ex.하동)".tr(),
        //             icon: Lcons.search(disableColor: color_222222),
        //             focus: true,
        //             focusColor: color_dbdbdb,
        //             width: 1.0,
        //           ),
        //           onTap: () async {
        //             DataManager dataManager = await Commons.nextPage(context, routeName: routeAddress, arguments: AddressPage(data: data));
        //             if (dataManager.addressItem.codeSearchData != null) {
        //               data.tcHomeLocation1.text = '${dataManager.addressItem.codeSearchData!.address1} ${dataManager.addressItem.codeSearchData!.address2} ${dataManager.addressItem.codeSearchData!.address3}';
        //               data.filterItem.request.area_code_data!.insert(0, SingleItem(data.tcHomeLocation1.text, keyName: dataManager.addressItem.codeSearchData!.bcode));
        //             }
        //
        //             log.d('『GGUMBI』>>> placeView : data.addressItem.codeSearchData: ${dataManager.addressItem.codeSearchData},  <<< ');
        //           },
        //         ),
        //       ),
        //     ),
        //   ],
        // ),
        // Row(
        //   crossAxisAlignment: CrossAxisAlignment.start,
        //   children: [
        //     Padding(
        //       padding: padding_12_T,
        //       child: lText(
        //         "2순위".tr(),
        //         style: st_b_14(fontWeight: FontWeight.bold),
        //       ),
        //     ),
        //     sb_w_15,
        //     Expanded(
        //       child: Form(
        //         key: data.formHomeLocation2,
        //         child: TextFormField(
        //           controller: data.tcHomeLocation2,
        //           textAlignVertical: TextAlignVertical.center,
        //           style: stLogin,
        //           readOnly: true,
        //           textInputAction: TextInputAction.done,
        //           keyboardType: TextInputType.emailAddress,
        //           decoration: underLineDecoration(
        //             "동명(읍,면)으로검색(ex.하동)".tr(),
        //             icon: Lcons.search(disableColor: color_222222),
        //             focus: true,
        //             focusColor: color_dbdbdb,
        //             width: 1.0,
        //             contentPadding: EdgeInsets.symmetric(vertical: -5),
        //           ),
        //           onTap: () async {
        //             DataManager dataManager = await Commons.nextPage(context, routeName: routeAddress, arguments: AddressPage(data: data));
        //             if (dataManager.addressItem.codeSearchData != null) {
        //               data.tcHomeLocation2.text = '${dataManager.addressItem.codeSearchData!.address1} ${dataManager.addressItem.codeSearchData!.address2} ${dataManager.addressItem.codeSearchData!.address3}';
        //               data.filterItem.request.area_code_data!.insert(1, SingleItem(data.tcHomeLocation2.text, keyName: dataManager.addressItem.codeSearchData!.bcode));
        //             }
        //             log.d('『GGUMBI』>>> placeView : data.addressItem.codeSearchData: ${dataManager.addressItem.codeSearchData},  <<< ');
        //           },
        //         ),
        //       ),
        //     ),
        //   ],
        // ),
      ]),
    );
  }

  ///돌뵴요일,돌봄시간
  Widget _careDayView() {
    return Container(
      child: Column(children: [
        GridDaySelector(
          title: "돌봄요일".tr(),
          data: data.filterItem.lsDay,
          count: 7,
          user_type: storageHelper.user_type,
          borderRadius: 100,
          childAspectRatio: 1.0,
          enableColor: Commons.getColor(),
          disableColor: color_999999,
          onItemClick: (value) {
            log.d('『GGUMBI』>>> _careAreaView ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
          },
        ),
        sb_h_10,
        ListCareTargetSelector(
          title: "돌봄시간".tr(),
          data: data.filterItem.lsTime,
          user_type: storageHelper.user_type,
          padding: padding_20_LR,
          height: 45.0,
          borderRadius: 26,
          enableColor: Commons.getColor(),
          disableColor: color_999999,
          onItemClick: (value) {
            if (value is SingleItem) {
              if (value.isSelected) {
                value.type = TimeType.on.index;
              } else {
                value.type = TimeType.off.index;
              }
            }
            log.d('『GGUMBI』>>> _careAreaView ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
          },
        ),
        sb_h_15,
      ]),
    );
  }

  ///돌봄대상
  Widget _careTargetView() {
    return Container(
      child: Column(children: [
        GridCareTargetSelector(
          title: "돌봄대상".tr(),
          data: data.filterItem.lsJobCareTargetType,
          count: 2,
          user_type: storageHelper.user_type,
          margin: padding_05_LR,
          padding: padding_moveTypeLinkMom,
          borderRadius: 32,
          enableColor: Commons.getColor(),
          onItemClick: (value) {
            log.d('『GGUMBI』>>> _careTargetView ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
          },
        ),
        sb_h_15,
      ]),
    );
  }

  ///해시태그
  Widget _careTagView() {
    return Container(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: [
        // GridCareTargetSelector(
        //   title: "해시태그".tr(),
        //   data: data.filterItem.lsTag,
        //   user_type: storageHelper.user_type,
        //   padding: padding_20_LR,
        //   margin: padding_05,
        //   count: 3,
        //   childAspectRatio: 1.8,
        //   borderRadius: 26,
        //   enableColor: Commons.getColorRevers(),
        //   disableColor: color_999999,
        //   onItemClick: (value) {
        //     log.d('『GGUMBI』>>> _careAreaView ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
        //   },
        // ),

        sb_h_10,
        titleValueView("인증태그".tr(),
            style: st_b_18(
              fontWeight: FontWeight.w500,
            ),
            padding: padding_0),
        sb_h_10,
        Wrap(
          spacing: 8.0,
          runSpacing: -4.0,
          children: [
            ...authInfoView(),
          ],
        ),
        // sb_h_15,
      ]),
    );
  }

  ///해시태그
  authInfoView() {
    return data.filterItem.lsTag.map((tag) => hashTagActionView(tag, enableColor: Commons.getColor(), onClick: () {
          onUpDate();
        }));
    // return hashTagView(data.listViewDetailItem.lsAuthInfo);
  }

  ///시급 타이틀
  String getPayTitle() {
    String title = "희망급여".tr();
    if (storageHelper.user_type == USER_TYPE.mom_daddy) {
      title = "희망급여".tr();
    } else {
      title = "돌봄시급".tr();
    }
    return title;
  }

  ///돌봄장소 1순위, 2순위
  String getAreaTitle() {
    String title = "돌봄활동이가능한지역".tr();
    if (storageHelper.user_type == USER_TYPE.mom_daddy) {
      title = "돌봄활동이가능한지역".tr();
    } else {
      title = "돌봄지역".tr();
    }
    return title;
  }
}
