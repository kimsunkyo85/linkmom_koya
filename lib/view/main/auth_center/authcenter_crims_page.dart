import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_keyboard/flutter_secure_keyboard.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/auth_state_response.dart';
import 'package:linkmom/data/network/models/authcenter_criminal_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:signature/signature.dart';

class AuthCenterCrimsPage extends BaseStateful {
  final DataManager? data;
  final String? surveyName;
  AuthCenterCrimsPage({this.data, this.surveyName});

  @override
  _AuthCenterCrimsPageState createState() => _AuthCenterCrimsPageState();
}

class _AuthCenterCrimsPageState extends BaseStatefulState<AuthCenterCrimsPage> {
  Uint8List _signature = Uint8List.fromList([]);
  late SignatureController _controller;
  TextEditingController _birth = TextEditingController();
  FocusNode _birthNode = FocusNode();
  SecureKeyboardController secureKeyboardController = SecureKeyboardController();
  int _birthLength = 13;
  AuthState _state = AuthState();

  @override
  void initState() {
    super.initState();
    onData(widget.data ?? DataManager());
    _controller = SignatureController(
      penStrokeWidth: 2,
      penColor: Colors.black,
      exportBackgroundColor: Colors.transparent,
    );
    _requestCrimsState();
  }

  @override
  void onData(DataManager data) {
    super.onData(data);
    this.data = data;
  }

  @override
  Widget build(BuildContext context) {
    return WithSecureKeyboard(
      controller: secureKeyboardController,
      child: lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          context: context,
          backgroundColor: color_white,
          appBar: appBar("인증센터_범죄인증".tr(), hide: false),
          body: Container(
              child: lScrollbar(
            child: ListView(children: [
              Container(
                  padding: padding_20,
                  child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                    Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.end, children: [
                      lText("범죄이력조회_타이틀".tr(), style: st_b_20(fontWeight: FontWeight.w700)),
                      sb_w_05,
                      lInkWell(
                          onTap: () => {
                                showNormalDlg(
                                  context: context,
                                  title: "인증센터_범죄인증".tr(),
                                  titleAlign: Alignment.bottomLeft,
                                  message: "범죄인증_팝업".tr(),
                                  msgAlign: TextAlign.left,
                                  btnRight: "확인".tr(),
                                  padding: padding_30_TB,
                                  onClickAction: (action) => {},
                                )
                              },
                          child: Lcons.info(color: color_dedede, isEnabled: true)),
                    ]),
                    sb_h_20,
                    lDivider(color: color_eeeeee),
                    sb_h_20,
                    Container(
                        width: double.infinity,
                        padding: padding_20,
                        decoration: BoxDecoration(
                          color: color_6bd5eae8,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [lText("유효기간_안내".tr()), lText("폐기안내".tr()), lText("범죄인증_안내_3".tr())])),
                  ])),
              if (_state.cancelReason.isNotEmpty)
                Container(
                  margin: padding_20_LR,
                    padding: padding_10,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: color_eeeeee.withOpacity(0.32),
                      borderRadius: BorderRadius.circular(8),
                    ),
                  child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextHighlight(
                      text: AuthImageType.crims.name + " " + "반려사유".tr(),
                      term: "반려사유".tr(),
                      textStyle: st_15(textColor: color_545454, fontWeight: FontWeight.w500),
                      textStyleHighlight: st_15(textColor: color_ff3b30, fontWeight: FontWeight.w500),
                    ),
                    sb_h_05,
                    lText(_state.cancelReason, style: st_13(textColor: color_545454)),
                    sb_h_05,
                  ],
                )),
              Container(
                  padding: padding_20,
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      lText("범죄이력_조회_동의서".tr(), style: st_b_18(fontWeight: FontWeight.w700)),
                      sb_h_20,
                      lText("조회 대상자"),
                      sb_h_20,
                      lDivider(color: color_545454),
                      sb_h_20,
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(children: [
                            Flexible(flex: 1, child: Container(width: double.infinity, padding: padding_10_TB, child: lText("성명".tr(), style: st_b_14(fontWeight: FontWeight.w500)))),
                            Flexible(flex: 2, child: Container(width: double.infinity, padding: padding_10_TB, child: lText(auth.user.my_info_data!.first_name, style: st_b_14(fontWeight: FontWeight.w500)))),
                          ]),
                          Row(children: [
                            Flexible(flex: 1, child: Container(width: double.infinity, padding: padding_10_TB, child: lText("주민등록번호".tr(), style: st_b_14(fontWeight: FontWeight.w500)))),
                            Flexible(
                                flex: 2,
                                child: Container(
                                    width: double.infinity,
                                    height: 70,
                                    padding: padding_10_TB,
                                    child: TextFormField(
                                      controller: _birth,
                                      obscureText: true,
                                      enableInteractiveSelection: false,
                                      focusNode: _birthNode,
                                      onTap: () {
                                        secureKeyboardController.show(
                                          maxLength: _birthLength,
                                          type: SecureKeyboardType.NUMERIC,
                                          focusNode: _birthNode,
                                          initText: _birth.text,
                                          onDoneKeyPressed: (List<int> charCodes) {
                                            _birth.text = String.fromCharCodes(charCodes);
                                            onUpDate();
                                          },
                                        );
                                      },
                                      decoration: InputDecoration(
                                          hintText: "숫자만입력".tr(),
                                          counterText: '',
                                          border: OutlineInputBorder(borderSide: BorderSide(color: color_dedede)),
                                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: color_dedede)),
                                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: color_dedede)),
                                          hintStyle: st_14(textColor: color_b2b2b2)),
                                    ))),
                          ]),
                          Row(children: [
                            Flexible(flex: 1, child: Container(width: double.infinity, padding: padding_10_TB, child: lText("연락처".tr(), style: st_b_14(fontWeight: FontWeight.w500)))),
                            Flexible(flex: 2, child: Container(width: double.infinity, padding: padding_10_TB, child: lText(auth.account.hpnumber, style: st_b_14(fontWeight: FontWeight.w500)))),
                          ]),
                          sb_h_15,
                          lDivider(color: color_eeeeee),
                          sb_h_15,
                          Container(
                            width: double.infinity,
                            child: lText("범죄이력_조회_동의안내".tr(), style: st_b_12()),
                          ),
                          sb_h_20,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              lText("동의자".tr(), style: st_b_14()),
                              sb_w_10,
                              lText(auth.user.my_info_data!.first_name, style: st_b_16(fontWeight: FontWeight.w700)),
                              sb_w_10,
                              lBtnWrap(
                                _signature.isNotEmpty ? "서명완료".tr() : "투약의뢰서_서명하기".tr(),
                                btnEnableColor: color_545454,
                                padding: EdgeInsets.fromLTRB(25, 15, 25, 15),
                                borderRadius: 6,
                                isEnabled: true,
                                textStyle: st_13(),
                                onClickAction: () {
                                  showDlgSign(context, btnLeft: "다시하기".tr(), btnRight: "서명하기_서명저장".tr(), onClickAction: (action, imageFile, signature) {
                                    _signature = signature;
                                    onUpDate();
                                  });
                                },
                              ),
                            ],
                          ),
                          sb_h_25,
                          lDivider(color: color_545454),
                          sb_h_25,
                          Container(
                            child: lText(
                              "범죄이력_조회_개인정보안내".tr(),
                              style: st_b_12(),
                            ),
                          )
                        ],
                      )
                    ],
                  )),
              sb_h_10,
              lBtn("동의서제출".tr(), btnColor: color_545454, isEnabled: _isConfirm(), onClickAction: () {
                flag.enableLoading(fn: () => onUpDate());
                _requestSaveCriminal();
              }),
            ]),
          )),
        ),
        showText: false,
      ),
    );
  }

  void _requestSaveCriminal() async {
    try {
      AuthCenterCriminalRequest req = AuthCenterCriminalRequest(name: auth.user.my_info_data!.first_name, hpnum: auth.account.hpnumber, jumin: _birth.text, agreement_picture: await Commons.bytesToImage(_signature, "criminal_siganture", "png"));
      apiHelper.requestSaveCriminal(req).then((response) {
        flag.disableLoading(fn: () => onUpDate());
        if (response.getCode() == KEY_SUCCESS) {
          showNormalDlg(message: "신청이완료되었습니다".tr(), onClickAction: (a) => {if (a == DialogAction.yes) Commons.pagePop(context, data: true)});
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  bool _isConfirm() {
    return _signature.isNotEmpty && _birth.text.isNotEmpty && _birth.text.length == _birthLength;
  }

  void _requestCrimsState() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestAuthStateCriminalView().then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _state = response.getData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        log.e('_requestDeungbonState >>>>>>>>>>>> e');
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }
}
