import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/auth_state_request.dart';
import 'package:linkmom/data/network/models/auth_state_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/main/mypage/cs/cs_terms_view_page.dart';

import 'authcenter_file_upload_view.dart';

class AuthCenterHealthPage extends BaseStateful {
  final DataManager? data;
  AuthCenterHealthPage({this.data});

  @override
  _AuthCenterHealthPageState createState() => _AuthCenterHealthPageState();
}

class _AuthCenterHealthPageState extends BaseStatefulState<AuthCenterHealthPage> {
  bool _privacyAgree = false;
  bool _sensitiveInfoAgree = false;
  List<AuthState> _state = [];

  @override
  void initState() {
    super.initState();
    onData(widget.data ?? DataManager());
    _requestAuthState();
  }

  @override
  void onData(DataManager data) {
    super.onData(data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      AuthcenterFileUploadView(
        data: data,
        desc: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          TextHighlight(
            text: "주민번호안내".tr(),
            term: "주민번호안내_하이라이트".tr(),
            textStyleHighlight: st_13(textColor: color_ff3b30, fontWeight: FontWeight.w500),
            textStyle: st_b_13(),
          ),
          lText("폐기안내".tr(), style: st_b_13()),
          lText("발급일안내".tr(), style: st_b_13())
        ]),
        title: "건강인증_타이틀".tr(),
        sampleImg: [IMG_AUTH_EXAMPLE_HEALTH],
        uploadType: [AuthImageType.health, AuthImageType.covid19Vaccin],
        isConfirmList: [_privacyAgree, _sensitiveInfoAgree],
        howToBlock: [
          _howToCommentBlock(1, lText("건강인증_1".tr(), style: st_b_14(), overflow: TextOverflow.clip)),
          sb_h_06,
          _howToCommentBlock(2, lText("건강인증_2".tr(), style: st_b_14(), overflow: TextOverflow.clip)),
          sb_h_06,
          _howToCommentBlock(3, lText("건강인증_3".tr(), style: st_b_14(), overflow: TextOverflow.clip)),
          sb_h_06,
          lText("건강인증_코로나".tr(), style: st_b_14(textColor: color_ff3b30, fontWeight: FontWeight.w500)),
        ],
        appbarTitle: "인증센터_건강인증".tr(),
        subAgreement: [
          Agreement(
            name: "민감정보동의".tr(),
            agree: _sensitiveInfoAgree,
            action: (value) {
              _sensitiveInfoAgree = value;
              onUpDate();
            },
            terms: () => Commons.page(context, routeCsTermsView, arguments: CsTermsViewPage(terms: Terms.sensitive)),
          ),
        ],
        showAuthMessage: false,
        alertTitle: "인증센터_건강인증".tr(),
        alertBody: "건강인증_팝업".tr(),
        updateChecker: (value) {
          _privacyAgree = value;
          onUpDate();
        },
        onSended: (action) {
          switch (action) {
            case DialogAction.on:
              flag.enableLoading(fn: () => onUpDate());
              break;
            case DialogAction.confirm:
              flag.disableLoading(fn: () => onUpDate());
              showNormalDlg(
                  context: context,
                  message: "신청이완료되었습니다".tr(),
                  onClickAction: (a) {
                    if (a == DialogAction.yes) Commons.pagePop(context, data: true);
                  });
              break;
            default:
              flag.disableLoading(fn: () => onUpDate());
          }
        },
        state: _state,
      ),
      showText: false,
    );
  }

  Widget _howToCommentBlock(int index, Widget child) {
    return Row(crossAxisAlignment: CrossAxisAlignment.start, children: [lText("$index.", style: st_14(textColor: color_main, fontWeight: FontWeight.w900)), sb_w_05, Container(width: data.width(context, 0.8), child: child)]);
  }

  void _requestAuthState() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestAuthStateView(AuthStateViewRequest(gubun: AuthImgType.health.index)).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _state = response.getDataList();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        log.e('_requestAuthState >>>>>>>>>>>> $e');
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }
}
