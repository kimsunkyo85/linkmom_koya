import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/auth_state_request.dart';
import 'package:linkmom/data/network/models/auth_state_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/main/auth_center/authcenter_file_upload_view.dart';

class AuthCenterGraduatedPage extends BaseStateful {
  final DataManager? data;

  AuthCenterGraduatedPage({this.data});

  @override
  _AuthCenterGraduatedPageState createState() => _AuthCenterGraduatedPageState();
}

class _AuthCenterGraduatedPageState extends BaseStatefulState<AuthCenterGraduatedPage> {
  List<AuthState> _state = [];
  bool _privacyAgree = false;

  @override
  void initState() {
    super.initState();
    onData(widget.data ?? DataManager());
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {});
    _requestAuthState();
  }

  @override
  void onData(DataManager data) {
    super.onData(data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      AuthcenterFileUploadView(
        data: data,
        desc: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          TextHighlight(
            text: "주민번호안내".tr(),
            term: "주민번호안내_하이라이트".tr(),
            textStyleHighlight: st_13(textColor: color_ff3b30, fontWeight: FontWeight.w500),
            textStyle: st_b_13(),
          ),
          lText("폐기안내".tr(), style: st_b_13()),
          lText("발급일안내_1년".tr(), style: st_b_13())
        ]),
        title: "학력인증_타이틀".tr(),
        sampleImg: [IMG_AUTH_EXAMPLE_GRADUATION],
        uploadType: [
          AuthImageType.schoolCertif,
          AuthImageType.graduation,
        ],
        appbarTitle: "인증센터_학력인증".tr(),
        alertTitle: "인증센터_학력인증".tr(),
        alertBody: "학력인증_팝업".tr(),
        updateChecker: (value) {
          _privacyAgree = value;
        },
        onSended: (action) {
          switch (action) {
            case DialogAction.on:
              flag.enableLoading(fn: () => onUpDate());
              break;
            case DialogAction.confirm:
              flag.disableLoading(fn: () => onUpDate());
              showNormalDlg(
                  context: context,
                  message: "신청이완료되었습니다".tr(),
                  onClickAction: (a) {
                    if (a == DialogAction.yes) Commons.pagePop(context, data: true);
                  });
              break;
            default:
              flag.disableLoading(fn: () => onUpDate());
          }
        },
        state: _state,
      ),
      showText: false,
    );
  }

  void _requestAuthState() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestAuthStateView(AuthStateViewRequest(gubun: AuthImgType.gradue.index)).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _state = response.getDataList();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        log.e('_requestAuthState >>>>>>>>>>>> $e');
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }
}
