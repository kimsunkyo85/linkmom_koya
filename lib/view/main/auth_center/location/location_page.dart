import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:device_info_plus/device_info_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linkmom/data/network/models/area_address_request.dart';
import 'package:linkmom/data/network/models/area_address_response.dart';
import 'package:linkmom/data/network/models/area_default_request.dart';
import 'package:linkmom/data/network/models/area_gps_view_request.dart';
import 'package:linkmom/data/network/models/area_gps_view_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/home_provider.dart';
import 'package:linkmom/utils/listview/list_area_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:provider/provider.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : my_info_page.dart - 동네인증
/// since        : 5/7/21 / update:
class LocationPage extends BaseStateful {
  @override
  _LocationPageState createState() => _LocationPageState();

  LocationPage({this.data});

  final DataManager? data;
}

final LatLng defaultLatLng = LatLng(37.56014642936966, 126.97525408505783);

///기본 주고 경기도 수원시 하동....
LatLng _kMapCenter = defaultLatLng;

class _LocationPageState extends BaseStatefulState<LocationPage> {
  GoogleMapController? controller;
  Set<Marker> _markers = Set();
  CameraPosition _parisCameraPosition = CameraPosition(target: _kMapCenter, zoom: 7.0);

  BitmapDescriptor? _markerIcon;
  Uint8List? markerIcon;
  Position? location;
  double zoom = 16;
  AndroidDeviceInfo? _android;

  ///GPS 수신여부
  bool isLocation = false;

  @override
  initState() {
    super.initState();
    onData(widget.data ?? DataManager());

    ///동네 인증 가져오기 요청
    _requestAreaAddressView();
    _determinePosition();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      _android = await Commons.onAndroidInfo();
    });
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      showLocationSetting();
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        showLocationSetting();
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      showLocationSetting();
      return Future.error('Location permissions are permanently denied, we cannot request permissions.');
    }

    _currentLocation();
    setCustomMapPin();
    addMarker();
    return Future.value(location);
  }

  @override
  onResume() async {
    bool serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (serviceEnabled && location == null) {
      _currentLocation();
      setCustomMapPin();
      addMarker();
    }

    if (!serviceEnabled) {
      showLocationSetting();
    }
    return super.onResume();
  }

  @override
  Widget build(BuildContext context) {
    //링크쌤이고, 인증된 기본 동네가 1개이상이고, 링크쌤 지원서가 있을 경우
    bool isView = Commons.isLinkMom() && data.locationAuthItem.lsAuth.length > 1 && auth.indexInfo.linkmominfo.isBookingjobs;
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        key: data.scaffoldKey,
        appBar: appBar("동네인증".tr(), isClose: false, hide: false),
        body: Column(
          children: [
            Expanded(
              child: lScrollView(
                padding: padding_0,
                child: lContainer(
                  width: double.infinity,
                  child: Column(
                    children: <Widget>[
                      Column(
                        children: [
                          _googleMap(),
                          Padding(
                            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // TODO: GGUMBI 2021/12/09 - 내년 1월 릴리즈시 주석 제거
                                sb_h_10,
                                Row(
                                  children: [
                                    lText("현재위치".tr(), style: st_b_16(fontWeight: FontWeight.bold)),
                                    sb_w_05,
                                    lText(isLocation ? data.locationAuthItem.addressCurrent : "동네인증_주소가져오기".tr(), style: st_b_16(textColor: Commons.getColor(), fontWeight: FontWeight.bold)),
                                    if (!isLocation)
                                      SizedBox(
                                        child: CircularProgressIndicator(
                                          color: Commons.getColor(),
                                          strokeWidth: 1,
                                        ),
                                        width: 10,
                                        height: 10,
                                      ),
                                  ],
                                ),
                                sb_h_20,
                                lBtnOutlineLeftImage(
                                  data.locationAuthItem.authTitle,
                                  // isEnabled: isLocation,
                                  style: st_16(fontWeight: FontWeight.bold),
                                  bgColor: Commons.getColor(),
                                  borderRadius: 8.0,
                                  imagePath: data.locationAuthItem.isAuth ? Lcons.check().name : Lcons.my_location().name,
                                  imageColor: color_white,
                                  imageSize: 20,
                                  sideColor: color_transparent,
                                  onClickAction: () {
                                    if (!data.locationAuthItem.isAuth) {
                                      AreaGpsViewData item = data.locationAuthItem.responseGpsView.getData();
                                      if (!StringUtils.validateString(item.gps_h_code) || !StringUtils.validateString(item.gps_b_code)) {
                                        showNormalDlg(
                                            message: "동네인증_GPS가져오기".tr(),
                                            onClickAction: (action) {
                                              isLocation = false;
                                              onUpDate();
                                              _currentLocation();
                                            });
                                        return;
                                      }
                                      AreaAddressRequest request = AreaAddressRequest(
                                        bcode: data.locationAuthItem.responseGpsView.getData().gps_b_code,
                                        hcode: data.locationAuthItem.responseGpsView.getData().gps_h_code,
                                        region_1depth: data.locationAuthItem.responseGpsView.getData().region_1depth,
                                        region_2depth: data.locationAuthItem.responseGpsView.getData().region_2depth,
                                        region_3depth: data.locationAuthItem.responseGpsView.getData().region_3depth,
                                        region_3depth_h: data.locationAuthItem.responseGpsView.getData().region_3depth_h,
                                        region_building_name: data.locationAuthItem.responseGpsView.getData().region_building_name,
                                        latitude: _kMapCenter.latitude.toString(),
                                        longitude: _kMapCenter.longitude.toString(),
                                        is_gpswithaddress: StringUtils.getBoolToInt(true),
                                      );
                                      _requestAreaAddressSave(request);
                                    }
                                  },
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    if (!(_android != null && _android!.version.sdkInt! < 31))
                                      Padding(
                                        padding: EdgeInsets.fromLTRB(0, 15, 15, 0),
                                        child: InkWell(
                                          onTap: () => showNormalDlg(
                                            message: "안드로이드_정확한위치사용".tr(),
                                            msgAlign: TextAlign.left,
                                            onClickAction: (a) => {if (a == DialogAction.yes) Commons.openLocationSetting()},
                                            btnLeft: "취소".tr(),
                                            btnRight: "이동하기".tr(),
                                          ),
                                          child: lTextBtn(
                                            "위치가맞지않아요".tr(),
                                            style: st_b_14(textColor: color_999999),
                                            bg: color_transparent,
                                            padding: padding_0,
                                            rightIcon: Lcons.info(disableColor: color_dedede),
                                          ),
                                        ),
                                      ),
                                    Padding(
                                      padding: padding_15_T,
                                      child: InkWell(
                                        onTap: () => showNormalDlg(
                                          messageWidget: Column(
                                            children: [
                                              lText(
                                                "동네인증하기_안내".tr(),
                                                style: st_14(textColor: color_545454),
                                              ),
                                              lText(
                                                '\n${"동네인증하기_GPS_안내".tr()}',
                                                style: st_14(textColor: color_ff3b30),
                                              ),
                                            ],
                                          ),
                                          msgAlign: TextAlign.left,
                                        ),
                                        child: lTextBtn(
                                          "동네인증을왜하나요".tr(),
                                          style: st_b_14(textColor: color_999999),
                                          bg: color_transparent,
                                          padding: padding_0,
                                          rightIcon: Lcons.info(disableColor: color_dedede),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          if (data.locationAuthItem.lsAuth.isNotEmpty)
                            Column(
                              children: [
                                lDivider(color: color_d4d4d4, thickness: 10.0, padding: padding_15_TB),
                                Padding(
                                  padding: padding_20_LR,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: padding_05_TB,
                                        child: Row(
                                          children: [
                                            lText("동네인증_기본변경".tr(), style: st_b_16(fontWeight: FontWeight.bold)),
                                            lText(' (${"최대3개".tr()})', style: st_b_14(textColor: color_999999)),
                                          ],
                                        ),
                                      ),
                                      if (isView)
                                        Padding(
                                          padding: padding_10_B,
                                          child: lText("인증동네_링크쌤안내".tr(), style: st_b_14(textColor: color_999999)),
                                        ),
                                      sb_h_05,
                                      Padding(
                                        padding: padding_15_B,
                                        child: ListAreaSelector(
                                          data: data.locationAuthItem.lsAuth,
                                          alignment: Alignment.centerLeft,
                                          height: 50,
                                          isWidth: true,
                                          borderRadius: 8,
                                          isBorderRadius: false,
                                          onItemClick: (value) {
                                            SingleItem item = value as SingleItem;
                                            AreaAddressData addressData = item.data as AreaAddressData;
                                            _requestAreaDefaultSet(AreaDefaultRequest(address_id: addressData.address_id));
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            // wdDivider(),
            // _confirmButton(),
          ],
        ),
      ),
      isOnlyLoading: true,
    );
  }

  @override
  void onDataPage(_data) {
    super.onDataPage(_data);
    onConfirmBtn();
  }

  @override
  void onConfirmBtn() {
    data.locationAuthItem.authTitle = getAuthTitle(data.locationAuthItem.isAuth);
    bool isAddress = false;
    if (StringUtils.validateString(data.locationAuthItem.addressSelect)) {
      isAddress = true;
    } else {
      isAddress = false;
    }

    if (isAddress) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  String getAuthTitle(bool isFlag) {
    String title = "동네인증하기".tr();
    if (isFlag) {
      title = "동네인증_완료".tr();
    } else {
      title = "동네인증하기".tr();
    }
    log.d('『GGUMBI』>>> getAuthTitle : isFlag: $isFlag,  $title  <<< ');
    return title;
  }

  ///동네인증 최근인증 주소 가져오기 요청
  Future<AreaAddressResponse> _requestAreaAddressView() async {
    var response = await apiHelper.requestAreaAddressView();
    if (response.getCode() == KEY_SUCCESS) {
      data.locationAuthItem.response = response;
      if (response.dataList!.isNotEmpty) {
        data.locationAuthItem.lsAuth.clear();
        response.dataList!.asMap().forEach((index, value) {
          data.locationAuthItem.lsAuth.add(SingleItem('${value.getAddress3depth()}', data: value, isSelected: index == 0 ? true : false));
        });
      }
    }

    onConfirmBtn();
    return response;
  }

  ///동네인증저장하기 요청
  Future<AreaAddressResponse> _requestAreaAddressSave(AreaAddressRequest authData) async {
    flag.enableLoading(fn: () => onUpDate());
    var response = await apiHelper.requestAreaAddressSave(authData).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    flag.disableLoading(fn: () => onUpDate());
    if (response.getCode() == KEY_SUCCESS) {
      data.locationAuthItem.response = response;
      data.locationAuthItem.isAuth = response.getData().is_gpswithaddress;
      _requestAreaDefaultSet(AreaDefaultRequest(address_id: response.getData().address_id));
    }
    onConfirmBtn();
    return response;
  }

  ///기본동네인증설정하기 요청
  Future<AreaAddressResponse> _requestAreaDefaultSet(AreaDefaultRequest data) async {
    var response = await apiHelper.requestAreaDefaultSet(data);
    if (response.getCode() == KEY_SUCCESS) {
      Commons.showToast("변경되었습니다".tr());
      Provider.of<HomeNavigationProvider>(context, listen: false).setListUpdated(false, isUpate: true);
      _requestAreaAddressView();
    }
    onConfirmBtn();
    return response;
  }

  ///구글맵 위젯
  Widget _googleMap() {
    return SizedBox(
      height: heightFull(context) * 0.34,
      child: Stack(
        children: [
          GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: _parisCameraPosition,
            markers: _markers,
            myLocationEnabled: false,
            mapToolbarEnabled: false,
            myLocationButtonEnabled: false,
            zoomControlsEnabled: false,
            onMapCreated: _onMapCreated,
            zoomGesturesEnabled: true,
            gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
              new Factory<OneSequenceGestureRecognizer>(
                () => new EagerGestureRecognizer(),
              ),
            ].toSet(),
            // circles: circles,//원형
          ),
          Padding(
            padding: padding_05,
            child: Align(
              alignment: Alignment.bottomRight,
              child: InkWell(
                onTap: _currentLocation,
                child: Container(
                  padding: padding_10,
                  height: 60,
                  decoration: BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                  child: Lcons.my_location(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _currentLocation() async {
    //요청후 30초 지난후에도 응답이 없을 경우 해당 플래그 해제
    Future.delayed(Duration(seconds: 30)).then((value) {
      isLocation = true;
      onUpDate();
    });
    location = await Geolocator.getCurrentPosition();
    if (location == null) {
      location = await Geolocator.getLastKnownPosition(forceAndroidLocationManager: true);
    }
    _kMapCenter = LatLng(location!.latitude, location!.longitude);
    _parisCameraPosition = CameraPosition(target: _kMapCenter, zoom: zoom);
    AreaGpsViewRequest areaData = AreaGpsViewRequest(
      latitude: _kMapCenter.latitude.toString(),
      longitude: _kMapCenter.longitude.toString(),
    );
    var response = await apiHelper.requestAreaGpsView(areaData);
    // log.d('『GGUMBI』>>> _currentLocation : response : $response,  <<< ');
    data.locationAuthItem.responseGpsView = response;
    data.locationAuthItem.addressCurrent = getAddress();
    isLocation = true;
    _onCameraPosition();
    addMarker();
    onUpDate();
  }

  String getAddress() {
    AreaGpsViewData item = data.locationAuthItem.responseGpsView.getData();
    String value = '${item.region_2depth} ${item.region_3depth}';
    if (StringUtils.getStringToBool(item.region_3depth_h)) {
      value = '${item.region_2depth} ${item.region_3depth}(${item.region_3depth_h})';
    }
    return value;
  }

  ///마커 만들기
  void addMarker() {
    String markerIdVal = '내 위치';
    final MarkerId markerId = MarkerId(markerIdVal);
    _markers.clear();
    Marker marker = Marker(
      markerId: markerId,
      position: _kMapCenter,
      icon: BitmapDescriptor.fromBytes(markerIcon!),
      infoWindow: InfoWindow(title: markerIdVal),
      onTap: () {
        log.d('『GGUMBI』>>> _add ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
      },
      onDragEnd: (LatLng position) {
        log.d('『GGUMBI』>>> _add ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
      },
    );
    _markers.add(marker);
  }

  ///지도 컨트롤 만들기
  void _onMapCreated(GoogleMapController controllerParam) {
    setState(() {
      controller = controllerParam;
      _onCameraPosition();
    });
  }

  void _onCameraPosition() {
    controller!.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(_kMapCenter.latitude, _kMapCenter.longitude), zoom: zoom),
      ),
    );
  }

  ///원형
  Set<Circle> circles = Set.from([
    Circle(
        circleId: CircleId("myCircle"),
        // radius: 4000,
        radius: 200,
        center: _kMapCenter,
        // center: _createCenter,
        fillColor: Color.fromRGBO(204, 255, 204, 0.2),
        strokeColor: Color.fromRGBO(153, 204, 153, 0.8),
        strokeWidth: 2,
        onTap: () {
          print('circle pressed');
        })
  ]);

  ///Marker 아이콘 만들기
  void setCustomMapPin() async {
    markerIcon = await getBytesFromAsset(Commons.isLinkMom() ? 'assets/images/location_pin_linkmom.png' : 'assets/images/location_pin_momdady.png', 150);
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!.buffer.asUint8List();
  }

  showLocationSetting() {
    showNormalDlg(
        message: "동네인증_GPS_OFF".tr(),
        onClickAction: (action) {
          Commons.openLocationSetting();
        });
  }
}
