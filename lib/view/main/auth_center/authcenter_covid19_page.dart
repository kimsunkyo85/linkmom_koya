import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/survey_answer_request.dart';
import 'package:linkmom/data/network/models/survey_list_request.dart';
import 'package:linkmom/data/network/models/survey_list_response.dart';
import 'package:linkmom/data/network/models/survey_question_request.dart';
import 'package:linkmom/data/network/models/survey_question_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

class AuthCenterCovid19Page extends BaseStateful {
  final DataManager? data;
  final String? surveyName;
  AuthCenterCovid19Page({this.data, this.surveyName});

  @override
  _AuthCenterCovid19PageState createState() => _AuthCenterCovid19PageState();
}

class _AuthCenterCovid19PageState extends BaseStatefulState<AuthCenterCovid19Page> {
  String YES = '1';
  String NO = '2';
  List<String> _answer = [];
  int surveyNumber = 0;
  bool _allCheck = false;
  SurveyData _survey = SurveyData();

  @override
  void initState() {
    super.initState();
    onData(widget.data ?? DataManager());
    _requestSurveyList();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _requestSurvey();
    });
  }

  @override
  void onData(DataManager data) {
    super.onData(data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
          backgroundColor: Colors.white,
          appBar: appBar("코로나_자가_진단".tr(), hide: false),
          body: Column(children: [
            Expanded(
                child: lScrollView(
                    child: Column(children: [
              sb_h_30,
              Row(mainAxisSize: MainAxisSize.max, mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Container(margin: padding_15_R, child: Lcons.info(color: color_dedede, size: 48, isEnabled: true)),
                if (_survey.results != null)
                  Flexible(
                  flex: 3,
                  child: lText(_survey.results!.first.surveyname, style: st_b_20(fontWeight: FontWeight.w700), overflow: TextOverflow.clip),
                )
              ]),
              lDingbatText(text: "자가진단기간안내".tr(), dingbatColor: color_545454, padding: padding_20_TB, dingbatSize: 5),
              lDivider(color: color_eeeeee, padding: padding_30_B),
              Column(mainAxisAlignment: MainAxisAlignment.end, children: data.myInfoItem.questions.isEmpty ? [] : data.myInfoItem.questions.map((e) => _questionBlock(e)).toList()),
              lDivider(color: color_eeeeee, padding: padding_30_B),
              Row(children: [
                lInkWell(
                    onTap: () => _allNoCheck(),
                    child: Container(
                        child: Row(children: [
                      Lcons.check_circle(isEnabled: _allCheck, color: color_545454, disableColor: color_eeeeee),
                      sb_w_10,
                      lText("모두_아니오".tr(), style: st_b_16(fontWeight: FontWeight.w500)),
                    ]))),
              ]),
            ]))),
            lBtn("자가_진단_완료".tr(), btnColor: color_545454, isEnabled: _isConfirm(), onClickAction: () {
              _requestSendAnswer();
            }),
          ]),
        ));
  }

  Widget _questionBlock(SurveyQuestionInfo question) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(child: lText('${question.question_seq}. ${question.question}', style: st_b_16(fontWeight: FontWeight.w500), overflow: TextOverflow.clip)),
        sb_h_15,
        Row(children: [
          _selecter(question.answer1, question.question_seq, 1),
          sb_w_10,
          _selecter(question.answer2, question.question_seq, 2),
        ]),
        sb_h_30,
      ],
    );
  }

  void _requestSurvey() async {
    try {
      SurveyQuestionRequest req = SurveyQuestionRequest(page: 1);
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestCovidQuestion(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          data.myInfoItem.questions.clear();
          data.myInfoItem.questions.addAll(response.getData().results!);
          surveyNumber = response.getData().results!.first.survey;
          _answer = List.filled(response.getData().count, "");
          onUpDate();
        } else {
          showNormalDlg(message: response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  Widget _selecter(String answer, int index, int answerIndex) {
    if (answer.isNotEmpty) {
      return lInkWell(
          onTap: () {
            _answer[index - 1] = answerIndex.toString();
            if (answerIndex == 1) {
              _allCheck = false;
            }
            onUpDate();
          },
          child: Container(child: Row(children: [Lcons.radio_button(isEnabled: _answer.isNotEmpty && _answer[index - 1] == answerIndex.toString(), color: color_545454), sb_w_05, lText(answer, style: st_b_16())])));
    } else {
      return Container();
    }
  }

  void _allNoCheck() {
    if (!_allCheck) {
      _answer = List.filled(data.myInfoItem.questions.length, YES);
    }
    _allCheck = !_allCheck;
    onUpDate();
  }

  void _requestSendAnswer() {
    try {
      int index = 1;
      List<SurveyAnswerData> answer = _answer.map((e) {
        return SurveyAnswerData(question_seq: index++, answer: e);
      }).toList();
      SurveyAnswerRequest req = SurveyAnswerRequest(survey: surveyNumber, answer: answer);
      apiHelper.requestSurveyAnswer(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          if (_answer.where((element) => element == NO).isNotEmpty) {
            showNormalDlg(
                context: context,
                title: "코로나_증상_의심".tr(),
                message: "코로나_증상_의심_안내".tr(),
                btnLeft: "다시확인하기".tr(),
                msgAlign: TextAlign.left,
                onClickAction: (action) => {if (action == DialogAction.yes) Commons.pagePop(context, data: response.getData().createdate)},
                color: color_545454);
          } else {
            Commons.pagePop(context, data: response.getData().createdate);
          }
        } else {
          showNormalDlg(messageWidget: lText(response.getMsg()));
        }
      });
    } catch (e) {
    }
  }

  bool _isConfirm() {
    return _answer.where((element) => element.isEmpty).isEmpty;
  }

  void _requestSurveyList() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestSurvey(SurveyListRequest(page: SurveyType.covid19.value), type: SurveyType.covid19).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _survey = response.getData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}
