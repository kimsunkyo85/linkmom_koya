import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/auth_center/auth_center_page.dart';

class AuthcenterStressResultPage extends BaseStateful {
  final DataManager? data;
  final String surveyName;
  AuthcenterStressResultPage({this.data, this.surveyName = ''});

  @override
  _AuthcenterStressResultPageState createState() => _AuthcenterStressResultPageState();
}

class _AuthcenterStressResultPageState extends BaseStatefulState<AuthcenterStressResultPage> {
  int _ret = 0;
  late StressLevel _level;
  @override
  void initState() {
    super.initState();
    onData(widget.data ?? DataManager());
    _level = _setLevel();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      widget.data!.myInfoItem.surveyAnswer.forEach((element) {
        _ret += int.parse(element.answer);
      });
      _level = _setLevel();
      onUpDate();
    });
  }

  @override
  void onData(DataManager data) {
    super.onData(data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            backgroundColor: Colors.white,
            appBar: appBar(widget.surveyName, hide: false),
            body: Column(
              children: [
                Expanded(
                    child: lScrollView(
                        padding: padding_0,
                        child: Container(
                          margin: padding_20,
                          child: Column(mainAxisAlignment: MainAxisAlignment.end, crossAxisAlignment: CrossAxisAlignment.start, children: [
                            sb_h_50,
                            _level.icon,
                            sb_h_15,
                            lText(_level.title, textAlign: TextAlign.start, style: st_b_22(fontWeight: FontWeight.w700)),
                            sb_h_10,
                            lText(_level.desc, textAlign: TextAlign.start, style: st_14(textColor: color_545454)),
                            sb_h_20,
                            Container(
                              alignment: Alignment.center,
                              margin: padding_10_TB,
                              padding: padding_25,
                              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16), boxShadow: [
                                BoxShadow(
                                  offset: Offset(0, 2),
                                  color: Colors.black.withAlpha(21),
                                  blurRadius: 18.0,
                                ),
                              ]),
                              child: Column(children: [
                                Row(children: [
                                  Flexible(flex: 2, child: Container(width: double.infinity, child: lText("나의점수".tr(), style: st_14(textColor: color_b2b2b2, fontWeight: FontWeight.w700)))),
                                  Flexible(flex: 5, child: Container(width: double.infinity, child: lText('${_ret.toString()}${"점".tr()}', style: st_18(textColor: color_main, fontWeight: FontWeight.bold)))),
                                ]),
                                Container(
                                  margin: padding_20_TB,
                                  width: double.infinity,
                                  child: lDivider(color: color_eeeeee),
                                ),
                                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                  Flexible(flex: 2, child: Container(width: double.infinity, child: lText("평가기준".tr(), style: st_14(textColor: color_b2b2b2, fontWeight: FontWeight.w700)))),
                                  Flexible(
                                      flex: 2,
                                      child: Container(
                                          width: double.infinity,
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              lText("1점이상".tr(), style: st_b_14()),
                                              lText("9점이상".tr(), style: st_b_14()),
                                              lText("18점이상".tr(), style: st_b_14()),
                                            ],
                                          ))),
                                  Flexible(
                                      flex: 3,
                                      child: Container(
                                        width: double.infinity,
                                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                          TextHighlight(text: "스트레스_낮음".tr(), term: "낮음".tr(), textStyleHighlight: st_b_14(fontWeight: FontWeight.w600), textStyle: st_b_14()),
                                          TextHighlight(text: "스트레스_보통".tr(), term: "보통".tr(), textStyleHighlight: st_b_14(fontWeight: FontWeight.w600), textStyle: st_b_14()),
                                          TextHighlight(text: "스트레스_심함".tr(), term: "심함".tr(), textStyleHighlight: st_b_14(fontWeight: FontWeight.w600), textStyle: st_b_14()),
                                        ]),
                                      )),
                                ]),
                              ]),
                            ),
                            sb_h_05,
                            Container(alignment: Alignment.centerRight, child: lText("스트레스_출처".tr(), textAlign: TextAlign.end, style: st_12(textColor: color_b2b2b2))),
                            sb_h_30,
                          ]),
                        ))),
                lText("스트레스_재검사안내".tr(), textAlign: TextAlign.end, style: st_14(textColor: color_main)),
                lBtn("확인".tr(), btnColor: color_545454, onClickAction: () {
                  Commons.pageToMain(context, routeAuthCenter, arguments: AuthCenterPage());
                }),
              ],
            )));
  }

  StressLevel _setLevel() {
    if (_ret > -1 && _ret < 9) {
      return StressLevel(title: "스트레스_낮음_안내".tr(), desc: "스트레스_낮음_안내_설명".tr(), icon: Lcons.stress_low(size: 70, color: color_9edd6d));
    } else if (_ret > 8 && _ret < 15) {
      return StressLevel(title: "스트레스_보통_안내".tr(), desc: "스트레스_보통_안내_설명".tr(), icon: Lcons.stress_mid(size: 70, color: color_faa139));
    } else {
      return StressLevel(title: "스트레스_심함_안내".tr(), desc: "스트레스_심함_안내_설명".tr(), icon: Lcons.stress_high(size: 70, color: color_ff3b30));
    }
  }
}

class StressLevel {
  String title;
  String desc;
  Widget icon;
  StressLevel({this.title = ' ', this.desc = ' ', required this.icon});
}
