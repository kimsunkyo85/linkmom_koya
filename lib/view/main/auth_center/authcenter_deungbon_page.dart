import 'dart:io';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/addr_search_response.dart';
import 'package:linkmom/data/network/models/auth_state_response.dart';
import 'package:linkmom/data/network/models/authcenter_deungbon_request.dart';
import 'package:linkmom/data/network/models/authcenter_deungbon_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';

import '../chat/full_photo.dart';
import 'authcenter_file_upload_view.dart';

class AuthCenterDeungbonPage extends BaseStateful {
  final DataManager? data;

  AuthCenterDeungbonPage({this.data});

  @override
  _AuthcenterDeungbonPageState createState() => _AuthcenterDeungbonPageState();
}

class _AuthcenterDeungbonPageState extends BaseStatefulState<AuthCenterDeungbonPage> {
  bool _privacyAgree = false;
  bool _userInfo = false;
  bool _addressInfo = false;
  bool _childInfo = false;
  File? _image;
  String? _address = "";
  AuthState _state = AuthState();

  OutlineInputBorder defaultBorder = OutlineInputBorder(
    borderSide: BorderSide(color: Colors.transparent),
    borderRadius: BorderRadius.circular(8),
  );

  OutlineInputBorder disableBorder = OutlineInputBorder(
    borderSide: BorderSide(color: color_dedede),
    borderRadius: BorderRadius.circular(8),
  );

  OutlineInputBorder enableBorder = OutlineInputBorder(
    borderSide: BorderSide(color: color_545454),
    borderRadius: BorderRadius.circular(8),
  );

  List<DeungbonChildData> _childUnit = [];

  @override
  void initState() {
    super.initState();
    onData(widget.data ?? DataManager());
    _childUnit.add(DeungbonChildData());
    _requestDeungbonState();
  }

  @override
  void onData(DataManager data) {
    super.onData(data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        context: context,
        backgroundColor: color_white,
        appBar: appBar("인증센터_등본인증".tr(), hide: false),
        body: Container(
          child: lScrollbar(
            child: ListView(children: [
              Container(
                  padding: padding_20,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      sb_h_30,
                      Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.end, children: [
                        lText("등본인증_타이틀".tr(), style: st_b_20(fontWeight: FontWeight.w700)),
                        sb_w_05,
                        lInkWell(
                            onTap: () => {
                                  showNormalDlg(
                                    context: context,
                                    title: "인증센터_등본인증".tr(),
                                    titleAlign: Alignment.bottomLeft,
                                    message: "등본인증_팝업".tr(),
                                    msgAlign: TextAlign.left,
                                    btnRight: "확인".tr(),
                                    padding: padding_30_TB,
                                    onClickAction: (action) => {},
                                  ),
                                },
                            child: Lcons.info(color: color_dedede, isEnabled: true, size: 25)),
                      ]),
                      sb_h_20,
                      lDivider(color: color_eeeeee),
                      sb_h_20,
                      Container(
                          width: double.infinity,
                          padding: padding_20,
                          decoration: BoxDecoration(
                            color: color_6bd5eae8,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                            TextHighlight(
                              text: "주민번호안내".tr(),
                              term: "주민번호안내_하이라이트".tr(),
                              textStyleHighlight: st_13(textColor: color_ff3b30, fontWeight: FontWeight.w500),
                              textStyle: st_b_13(),
                            ),
                            lText("폐기안내".tr(), style: st_b_13()),
                            lText("발급일안내".tr(), style: st_b_13()),
                          ])),
                      sb_h_20,
                      if (_state.cancelReason.isNotEmpty)
                        Container(
                            padding: padding_10,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: color_eeeeee.withOpacity(0.32),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextHighlight(
                                  text: AuthImageType.deungbon.name + " " + "반려사유".tr(),
                                  term: "반려사유".tr(),
                                  textStyle: st_15(textColor: color_545454, fontWeight: FontWeight.w500),
                                  textStyleHighlight: st_15(textColor: color_ff3b30, fontWeight: FontWeight.w500),
                                ),
                                sb_h_05,
                                lText(_state.cancelReason, style: st_13(textColor: color_545454)),
                                sb_h_05,
                              ],
                            )),
                      Container(
                        decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
                        child: Theme(
                            data: Theme.of(context).copyWith(dividerColor: Colors.transparent), // remove border
                            child: ExpansionTile(
                                expandedAlignment: Alignment.topLeft,
                                tilePadding: padding_0,
                                childrenPadding: padding_0,
                                iconColor: color_222222,
                                collapsedIconColor: color_222222,
                                title: lText(
                                  "예시이미지".tr(),
                                  style: st_b_16(fontWeight: FontWeight.w500),
                                ),
                                children: [
                                  Stack(
                                    children: [
                                      Image.asset(IMG_AUTH_EXAMPLE_DEUNGBON),
                                      Positioned(
                                          right: 10,
                                          bottom: 10,
                                          child: lInkWell(
                                            child: Container(
                                              padding: padding_06,
                                              decoration: BoxDecoration(color: color_060606.withOpacity(0.42), shape: BoxShape.circle),
                                              child: Lcons.search(disableColor: Colors.white, size: 25),
                                            ),
                                            onTap: () => Commons.nextPage(context, fn: () => FullPhotoPage(urlList: [IMG_AUTH_EXAMPLE_DEUNGBON])),
                                          ))
                                    ],
                                  )
                                ])),
                      ),
                      AuthImageButton(
                          type: AuthImageType.deungbon,
                          onClickAction: (image) {
                            _image = image;
                            data.authItem.deungbonReq.deungbon_picture = image;
                            onConfirmBtn();
                          },
                          showBorder: false),
                    ],
                  )),
              lDivider(color: color_eeeeee, thickness: 8.0),
              sb_h_30,
              Container(
                  padding: padding_20,
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    lText("체크사항".tr(), style: st_b_18(fontWeight: FontWeight.w700)),
                    sb_h_10,
                    Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.center, children: [
                      lInkWell(
                          onTap: () => {_userInfo = !_userInfo, onUpDate()},
                          child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                            Lcons.open_checkbox(isEnabled: _userInfo, color: color_545454, disableColor: color_dedede),
                            sb_w_10,
                            lText("본인_이름및주민번호".tr(), style: st_b_15()),
                          ])),
                      sb_h_10,
                      Container(
                          child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Flexible(
                              child: TextFormField(
                            textAlign: TextAlign.center,
                            textInputAction: TextInputAction.next,
                            keyboardType: TextInputType.text,
                            maxLength: 10,
                            validator: StringUtils.validateName,
                            decoration: InputDecoration(
                              hintText: "본인이름".tr(),
                              labelStyle: st_14(textColor: color_b2b2b2),
                              isDense: true,
                              counterText: '',
                              hintStyle: st_14(textColor: color_cecece),
                              enabledBorder: _userInfo ? enableBorder : disableBorder,
                              focusedBorder: enableBorder,
                            ),
                            onChanged: (value) {
                              _userInfo = value.isNotEmpty;
                              data.authItem.deungbonReq.name = value;
                              onConfirmBtn();
                            },
                          )),
                          sb_w_10,
                          Flexible(
                              child: TextFormField(
                            textAlign: TextAlign.center,
                            textInputAction: TextInputAction.next,
                            keyboardType: TextInputType.number,
                            maxLength: 6,
                            validator: _validateBirth,
                            decoration: InputDecoration(
                              hintText: "주민번호앞자리".tr(),
                              labelStyle: st_14(textColor: color_b2b2b2),
                              isDense: true,
                              counterText: '',
                              hintStyle: st_14(textColor: color_cecece),
                              enabledBorder: _userInfo ? enableBorder : disableBorder,
                              focusedBorder: enableBorder,
                            ),
                            onChanged: (value) {
                              _userInfo = value.isNotEmpty;
                              data.authItem.deungbonReq.birth = value;
                              onConfirmBtn();
                            },
                          )),
                          Container(margin: padding_15_L, height: 32, width: 32)
                        ],
                      )),
                      sb_h_30,
                    ]),
                    Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.center, children: [
                      lInkWell(
                          onTap: () {
                            _addressInfo = !_addressInfo;
                            if (!_addressInfo) _address = null;
                            onConfirmBtn();
                          },
                          child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                            Lcons.open_checkbox(isEnabled: _addressInfo, color: color_545454, disableColor: color_dedede),
                            sb_w_10,
                            lText("주소입력요청".tr(), style: st_b_15()),
                          ])),
                      sb_h_10,
                      Container(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          lInkWell(
                            onTap: () async {
                              Documents? ret = await Commons.page(context, routeAddressSearch);
                              if (ret != null) {
                                _address = ret.getAddress();
                                data.authItem.deungbonReq.bcode = ret.address!.bCode;
                                data.authItem.deungbonReq.hcode = ret.address!.hCode;
                                data.authItem.deungbonReq.region_1depth = ret.address!.region1DepthName;
                                data.authItem.deungbonReq.region_2depth = ret.address!.region2DepthName;
                                data.authItem.deungbonReq.region_3depth_h = ret.address!.region3DepthHName;
                                data.authItem.deungbonReq.region_3depth = ret.address!.region3DepthName;
                                _addressInfo = _address != null;
                              }
                              onConfirmBtn();
                            },
                            child: lRoundContainer(
                                borderRadius: 8.0,
                                borderWidth: 1.0,
                                padding: padding_15,
                                bgColor: Colors.transparent,
                                borderColor: _addressInfo ? color_545454 : color_dedede,
                                alignment: Alignment.centerLeft,
                                child: lText(_address ?? "주소입력요청".tr(), style: st_b_14(textColor: _address == null ? color_b2b2b2 : color_222222, fontWeight: FontWeight.w500))),
                          ),
                          sb_h_15,
                          lRoundContainer(
                              bgColor: Colors.transparent,
                              padding: padding_0,
                              borderColor: _addressInfo ? color_545454 : color_dedede,
                              borderRadius: 8.0,
                              borderWidth: 1.0,
                              child: TextField(
                                textAlign: TextAlign.left,
                                textInputAction: TextInputAction.done,
                                keyboardType: TextInputType.text,
                                style: st_b_14(fontWeight: FontWeight.w500),
                                decoration: InputDecoration(
                                  contentPadding: padding_15,
                                  hintText: "상세주소입력요청".tr(),
                                  hintStyle: st_14(textColor: color_cecece),
                                  isDense: true,
                                  border: defaultBorder,
                                  focusedBorder: defaultBorder,
                                  enabledBorder: defaultBorder,
                                ),
                                onChanged: (value) {
                                  data.authItem.deungbonReq.region_4depth = value;
                                  onConfirmBtn();
                                },
                              )),
                        ],
                      )),
                      sb_h_30,
                    ]),
                    Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.center, children: [
                      lInkWell(
                          onTap: () => {_childInfo = !_childInfo, onConfirmBtn()},
                          child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                            Lcons.open_checkbox(isEnabled: _childInfo, color: color_545454, disableColor: color_dedede),
                            sb_w_10,
                            lText("자녀_이름및주민번호".tr(), style: st_b_15()),
                          ])),
                      sb_h_10,
                      Column(
                        children: _childUnit
                            .map((e) => ChildInfo(
                                  e,
                                  (value) {
                                    _childInfo = value;
                                    onConfirmBtn();
                                  },
                                  validateBirth: _validateBirth,
                                  addInfo: () {
                                    _childUnit.add(DeungbonChildData());
                                    onConfirmBtn();
                                  },
                                  enableBorder: enableBorder,
                                  disableBorder: disableBorder,
                                  isLast: _childUnit.last == e,
                                ))
                            .toList(),
                      ),
                      sb_h_20,
                      lText("등본일치확인".tr(), style: st_13(textColor: color_ff3b30)),
                      sb_h_20,
                    ]),
                  ])),
              lDivider(color: color_eeeeee, thickness: 8.0),
              Container(
                  padding: padding_20_TB,
                  color: Colors.white,
                  child: Column(
                    children: [
                      sb_h_20,
                      lInkWell(
                          onTap: () => {_privacyAgree = !_privacyAgree, onConfirmBtn()},
                          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                            Lcons.check_circle(isEnabled: _privacyAgree, disableColor: color_cecece, color: color_545454),
                            sb_w_10,
                            lText("개인정보처리동의".tr()),
                          ])),
                      lBtn("인증신청".tr(), btnColor: color_545454, isEnabled: flag.isConfirm, onClickAction: () {
                        flag.enableLoading(fn: () => onConfirmBtn());
                        _requestUploadImage();
                      }),
                    ],
                  )),
            ]),
          ),
        ),
      ),
      showText: false,
    );
  }

  @override
  void onConfirmBtn() {
    if (_privacyAgree && _image != null && _userInfo && _addressInfo && data.authItem.deungbonReq.region_4depth.isNotEmpty) {
      flag.enableConfirm(fn: onUpDate);
    } else {
      flag.disableConfirm(fn: onUpDate);
    }
  }

  String? _validateBirth(String? value) {
    DateTime time = DateFormat().addPattern('yyMMdd').parse(value!);
    if (time.isAfter(DateTime.now()) || time.isBefore(DateTime.now().add(Duration(days: 19 * 365)))) {
      return "아이등록_생년월일_초과오류".tr();
    } else {
      return null;
    }
  }

  Future<void> _requestUploadImage() async {
    try {
      if (data.authItem.deungbonReq.region_3depth_h.isEmpty) {
        showNormalDlg(message: "정확한 주소를 입력해주세요.");
        flag.disableLoading(fn: () => onUpDate());
        return;
      }
      data.authItem.deungbonReq.childinfo = [];
      await Future.forEach(_childUnit, (DeungbonChildData e) async {
        String name = await encryptHelper.encodeData(e.name);
        String birth = await encryptHelper.encodeData(e.birth);
        data.authItem.deungbonReq.childinfo!.add("$name|$birth");
      });
      FormData formData = FormData();
      data.authItem.deungbonReq.toFormData(formData).then((value) {
        apiHelper.apiCall(HttpType.post, ApiEndPoint.EP_AUTHCENTER_DEUNGBOM_SAVE, apiHeader.publicApiHeader, value, isFile: true).then((response) {
          flag.disableLoading(fn: () => onUpDate());
          var responseJson = Commons.returnResponse(response);
          AuthCenterDeungbonResponse responseData = AuthCenterDeungbonResponse.fromJson(responseJson);
          if (responseData.getCode() == KEY_SUCCESS) {
            showNormalDlg(message: "신청이완료되었습니다".tr(), onClickAction: (a) => {if (a == DialogAction.yes) Commons.pagePop(context, data: true)});
          }
        }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestDeungbonState() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestAuthStateDeungbonView().then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _state = response.getData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        log.e('_requestDeungbonState >>>>>>>>>>>> e');
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
      log.e({
        '--- TITLE    ': '--------------- EXCEPTION ---------------',
        '--- DATA     ': e,
      });
    }
  }
}

class ChildInfo extends StatefulWidget {
  DeungbonChildData child;
  final Function updateInfo;
  final Function addInfo;
  final String? Function(String?) validateBirth;
  final bool isLast;
  final OutlineInputBorder enableBorder, disableBorder;

  ChildInfo(this.child, this.updateInfo, {required this.validateBirth, required this.addInfo, this.isLast = true, required this.enableBorder, required this.disableBorder});

  @override
  _ChildInfoState createState() => _ChildInfoState();
}

class _ChildInfoState extends State<ChildInfo> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: padding_04,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Flexible(
                child: TextFormField(
              style: st_14(textColor: color_545454),
              textAlign: TextAlign.center,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.text,
              maxLength: 10,
              validator: StringUtils.validateName,
              decoration: InputDecoration(
                hintText: "자녀이름".tr(),
                isDense: true,
                counterText: '',
                hintStyle: st_14(textColor: color_cecece),
                enabledBorder: widget.child.name.isNotEmpty ? widget.enableBorder : widget.disableBorder,
                focusedBorder: widget.enableBorder,
              ),
              onChanged: (value) {
                setState(() {
                  widget.child.name = value;
                  widget.updateInfo(value.isNotEmpty);
                });
              },
            )),
            sb_w_10,
            Flexible(
                child: TextFormField(
              style: st_14(textColor: color_545454),
              textAlign: TextAlign.center,
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.number,
              maxLength: 6,
              validator: widget.validateBirth,
              decoration: InputDecoration(
                hintText: "주민번호앞자리".tr(),
                labelStyle: st_14(textColor: color_b2b2b2),
                isDense: true,
                counterText: '',
                hintStyle: st_14(textColor: color_cecece),
                enabledBorder: widget.child.birth.isNotEmpty ? widget.enableBorder : widget.disableBorder,
                focusedBorder: widget.enableBorder,
              ),
              onChanged: (value) {
                setState(() {
                  widget.child.birth = value;
                  widget.updateInfo(value.isNotEmpty);
                });
              },
            )),
            lInkWell(
                onTap: () {
                  widget.addInfo();
                },
                child: Container(
                    margin: padding_15_L,
                    height: 32,
                    width: 32,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(color: widget.isLast ? color_c4c4c4.withAlpha(52) : Colors.transparent, borderRadius: BorderRadius.circular(100)),
                    child: Lcons.add(isEnabled: widget.isLast, color: color_999999, disableColor: Colors.transparent))),
          ],
        ));
  }
}
