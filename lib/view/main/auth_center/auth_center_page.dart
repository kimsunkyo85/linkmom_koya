import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/authcenter_authmain_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_auth_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/auth_center/authcenter_career_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_crims_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_deungbon_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_educate_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_graduated_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_health_page.dart';
import 'package:linkmom/view/main/auth_center/home_state/auth_home_page.dart';
import 'package:linkmom/view/main/auth_center/home_state/home_state_page.dart';
import 'package:linkmom/view/main/auth_center/location/location_page.dart';

import '../../../base/base_stateful.dart';
import '../../../main.dart';
import 'authcenter_covid19_page.dart';
import 'authcenter_stress_page.dart';

class AuthCenterPage extends BaseStateful {
  final ViewType viewType;

  @override
  _AuthCenterPageState createState() => _AuthCenterPageState();

  AuthCenterPage({this.data, this.id, this.viewType = ViewType.modify, this.name = ''});

  final DataManager? data;
  final int? id;
  final String name;
}

class _AuthCenterPageState extends BaseStatefulState<AuthCenterPage> {
  List<AuthcenterItem> _authList = [];
  List<AuthcenterItem> _selfAuthList = [];
  UserHomeStateData _homeState = UserHomeStateData();

  @override
  initState() {
    super.initState();
    onData(widget.data ?? DataManager());
    _initLists();
    _requestAuthInfo();
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar("인증센터".tr(), isClose: false, hide: false),
        body: flag.isLoading
            ? null
            : lContainer(
                child: Column(
                  children: [
                    Expanded(
                      child: LinkmomRefresh(
                        onRefresh: () => _requestAuthInfo(),
                        padding: padding_0,
                        child: Container(
                          child: _infoView(context),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  @override
  void onConfirmBtn() {
    log.d('『GGUMBI』>>> onConfirmBtn ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
    flag.enableConfirm(fn: () => onUpDate());
  }

  @override
  void onDataPage(_data) {
    onConfirmBtn();
  }

  ///인증 센터 안내
  Widget _infoView(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            color: Colors.white,
            padding: padding_20,
            child: ListAuthView(
              data: _authList,
              count: 3,
              onItemClick: (value) {
                log.d('『GGUMBI』>>> _infoView : value: $value,  <<< ');
                if (widget.viewType == ViewType.modify || value.type < 3) _showAuthInfo(context, value);
              },
            ),
          ),
          lDivider(color: color_4de5e5ea, thickness: 8.0),
          Container(padding: padding_20_T, child: lText("자가진단항목".tr(), style: st_b_20(fontWeight: FontWeight.w700))),
          Container(
            padding: padding_20_LR,
            width: widthFull(context),
            color: Colors.white,
            child: ListAuthView(
              data: _selfAuthList,
              count: 3,
              isSelf: true,
              onItemClick: (value) async {
                if (widget.viewType == ViewType.modify) {
                  log.d('『GGUMBI』>>> _infoView : value: $value,  <<< ');
                  if (value.type == 9) {
                    var ret = await Commons.page(context, routeAuthCenterCovid19, arguments: AuthCenterCovid19Page(data: data));
                    if (ret != null) {
                      value.isSelected = true;
                      value.date = ret;
                      _requestAuthInfo();
                    }
                  } else if (value.type == 10) {
                    var ret = await Commons.page(context, routeAuthCenterStress, arguments: AuthCenterStressPage(data: data));
                    if (ret != null) {
                      _requestAuthInfo();
                    }
                  }
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  void _updateAuthItem(AuthcenterItem item, AuthMainData value) {
    String date = '';
    switch (item.type) {
      case 0:
        date = value.auth_date_hp;
        break;
      case 1:
        date = value.auth_date_address;
        break;
      case 2:
        date = value.auth_date_homestate;
        break;
      case 3:
        date = value.auth_date_criminal;
        break;
      case 4:
        date = value.auth_date_healthy;
        break;
      case 5:
        date = value.auth_date_deungbon;
        break;
      case 6:
        date = value.auth_date_career;
        break;
      case 7:
        date = value.auth_date_graduated;
        break;
      case 8:
        date = value.auth_date_education;
        break;
      case 9:
        date = value.auth_date_covid;
        break;
      case 10:
        date = value.auth_date_stress;
        break;
      case 11:
        date = value.auth_date_personality;
        break;
      default:
    }
    item.date = date.isEmpty ? "미완료".tr() : _getAuthState(date);
    if (item.date == "인증반려".tr()) {
      item.state = AuthStatus.fail;
      item.isSelected = false;
      item.isEnabled = true;
    } else if (item.date.contains("인증대기".tr())) {
      item.state = AuthStatus.ready;
      item.isSelected = false;
      if (item.type == 3 || item.type == 5) {
        item.isEnabled = false;
      } else {
        item.isEnabled = true;
      }
    } else if (item.date.contains("미완료".tr())) {
      item.state = AuthStatus.none;
      item.isSelected = false;
      item.isEnabled = true;
    } else {
      item.state = AuthStatus.success;
      item.isSelected = date.isNotEmpty;
      item.isEnabled = true;
    }
  }

  String _getAuthState(String date) {
    if (date.contains(AUTH_READY)) {
      return "인증대기".tr();
    } else if (date.contains(AUTH_FAIL)) {
      return "인증반려".tr();
    } else {
      return DateFormat().addPattern('yy.MM.dd').format(StringUtils.parseYMD(date));
    }
  }

  String _btnText(int type, AuthStatus state) {
    switch (type) {
      case 1:
        if (widget.viewType == ViewType.modify) {
          return "동네인증하기".tr();
        } else {
          return state == AuthStatus.success ? "동네인증완료".tr() : "동네인증필요".tr();
        }
      case 2:
        if (widget.viewType != ViewType.view) {
          return _homeState.nbhhome_type!.isEmpty ? "집안환경등록".tr() : "집안환경수정".tr();
        } else {
          return state == AuthStatus.success ? "집안환경확인".tr() : "집안환경등록필요".tr();
        }
      default:
        if (state == AuthStatus.success) {
          return "인증성공".tr();
        } else if (state == AuthStatus.fail) {
          return "인증반려_재등록".tr();
        } else if (state == AuthStatus.ready) {
          return "인증대기".tr();
        } else {
          return "인증받기".tr();
        }
    }
  }

  void _requestAuthInfo() {
    flag.enableLoading(fn: () => onUpDate());
    apiHelper.requestAuthCenterMain(id: widget.id).then((response) {
      _homeState = response.getData().userhomestat!;
      _authList.forEach((e) {
        _updateAuthItem(e, response.getData());
      });
      _selfAuthList.forEach((e) {
        _updateAuthItem(e, response.getData());
      });
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }

  void _initLists() {
    _authList.add(AuthcenterItem(name: "인증센터_본인인증".tr(), type: 0, desc: "본인인증_안내".tr(), icon: Lcons.self_certification_active()));
    _authList.add(AuthcenterItem(name: "인증센터_동네인증".tr(), type: 1, desc: "동네인증_안내".tr(), icon: Lcons.local_certification_active()));
    _authList.add(AuthcenterItem(name: "인증센터_집안환경".tr(), type: 2, desc: "집안환경_안내".tr(), icon: Lcons.family_environment_active()));
    _authList.add(AuthcenterItem(name: "인증센터_범죄인증".tr(), type: 3, desc: "범죄인증_안내".tr(), warn: "허위서류_안내".tr(), icon: Lcons.criminal_certification_active()));
    _authList.add(AuthcenterItem(name: "인증센터_건강인증".tr(), type: 4, desc: "건강인증_안내".tr(), warn: "허위서류_안내".tr(), icon: Lcons.health_certification_active()));
    _authList.add(AuthcenterItem(name: "인증센터_등본인증".tr(), type: 5, desc: "등본인증_안내".tr(), warn: "허위서류_안내".tr(), icon: Lcons.copy_certification_active()));
    _authList.add(AuthcenterItem(name: "인증센터_경력인증".tr(), type: 6, desc: "경력인증_안내".tr(), warn: "허위서류_안내".tr(), icon: Lcons.career_certification_active()));
    _authList.add(AuthcenterItem(name: "인증센터_학력인증".tr(), type: 7, desc: "학력인증_안내".tr(), warn: "허위서류_안내".tr(), icon: Lcons.academic_bg_certification()));
    _authList.add(AuthcenterItem(name: "인증센터_교육인증".tr(), type: 8, desc: "교육인증_안내".tr(), warn: "허위서류_안내".tr(), icon: Lcons.education_certification_active()));
    _selfAuthList.add(AuthcenterItem(name: "인증센터_코로나".tr(), type: 9, desc: '', icon: Lcons.covid_self_test()));
    _selfAuthList.add(AuthcenterItem(name: "인증센터_스트레스".tr(), type: 10, desc: '', icon: Lcons.stress_test()));
  }

  void _showAuthInfo(BuildContext context, AuthcenterItem item) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (ctx) {
          return Container(
            height: 324,
            decoration: BoxDecoration(
              color: color_white,
              borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
            ),
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(10, 30, 10, 0),
                  child: Column(
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            AuthSymbol(data: item, showCheck: false, enable: true),
                            lText(item.name, style: st_b_20(fontWeight: FontWeight.w700)),
                            lText(item.desc, style: st_b_16(), textAlign: TextAlign.center),
                            if (item.type > 2) lText(item.warn, style: st_12(textColor: color_ff3b30), textAlign: TextAlign.center),
                          ],
                        ),
                      ),
                      item.type == 0
                          ? lBtnOutline(
                              "${"가입날짜".tr()} : ${StringUtils.parseYMDLocale(StringUtils.formatYYMMDD(item.date))}",
                              style: st_16(textColor: color_main, fontWeight: FontWeight.w500),
                              margin: padding_20_B,
                              padding: padding_0,
                              sideColor: Colors.transparent,
                            )
                          : lBtn(
                              _btnText(item.type, item.state),
                              isEnabled: item.isEnabled,
                              btnColor: color_545454,
                              onClickAction: () => _movePage(ctx, item.type),
                            ),
                    ],
                  ),
                ),
                Positioned(
                  right: 20,
                  top: 20,
                  child: Container(
                    alignment: Alignment.centerRight,
                    child: lInkWell(
                      child: Lcons.nav_close(size: 25),
                      onTap: () => Commons.pagePop(ctx),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  Future<void> _movePage(BuildContext ctx, int type) async {
    var ret;
    await Commons.pagePop(ctx);
    switch (type) {
      case 1:
        if (widget.viewType != ViewType.view) Commons.page(context, routeLocation, arguments: LocationPage(data: data));
        break;
      case 2:
        if (widget.viewType == ViewType.modify && _homeState.ourhome_animal!.isEmpty) {
          Commons.page(context, routeAuthHome, arguments: AuthHomeStatePage(data: data, viewType: ViewType.apply));
        } else {
          var ret = await Commons.page(context, routeHomeState,
              arguments: HomeStatePage(
                data: data,
                homeState: _homeState,
                name: widget.name,
                viewType: widget.viewType,
              ));
          if (ret != null) {
            _homeState = UserHomeStateData(
              ourhome_type: ret.ourhome_type,
              ourhome_cctv: ret.ourhome_cctv,
              ourhome_animal: ret.ourhome_animal,
              ourhome_picture1: ret.ourhome_picture1,
              ourhome_picture2: ret.ourhome_picture2,
              ourhome_picture1_thumbnail: ret.ourhome_picture1,
              ourhome_picture2_thumbnail: ret.ourhome_picture2,
              nbhhome_type: ret.nbhhome_type,
              nbhhome_cctv: ret.nbhhome_cctv,
              nbhhome_animal: ret.nbhhome_animal,
              nbhhome_picture: ret.nbhhome_picture,
            );
          }
        }
        break;
      case 3:
        ret = await Commons.page(context, routeAuthCenterCrims, arguments: AuthCenterCrimsPage(data: data));
        break;
      case 4:
        ret = await Commons.page(context, routeAuthCenterHealth, arguments: AuthCenterHealthPage(data: data));
        break;
      case 5:
        ret = await Commons.page(context, routeAuthCenterDeungbon, arguments: AuthCenterDeungbonPage(data: data));
        break;
      case 6:
        ret = await Commons.page(context, routeAuthCenterCareer, arguments: AuthCenterCareerPage(data: data));
        break;
      case 7:
        ret = await Commons.page(context, routeAuthCenterGraduated, arguments: AuthCenterGraduatedPage(data: data));
        break;
      case 8:
        ret = await Commons.page(context, routeAuthCenterEducate, arguments: AuthCenterEducatePage(data: data));
        break;
    }
    if (ret != null) {
      _requestAuthInfo();
    }
  }
}
