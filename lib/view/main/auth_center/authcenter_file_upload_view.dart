import 'dart:io';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/auth_state_response.dart';
import 'package:linkmom/data/network/models/authcenter_img_save_request.dart';
import 'package:linkmom/data/network/models/authcenter_img_save_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/cs/cs_terms_view_page.dart';

import '../chat/full_photo.dart';

class AuthcenterFileUploadView extends StatefulWidget {
  final DataManager? data;
  final Widget? desc;
  final String title, alertTitle, alertBody;
  final List<String>? sampleImg;
  final List<bool>? isConfirmList;
  final List<AuthImageType>? uploadType;
  final String appbarTitle;
  final List<Widget>? howToBlock;
  final List<Widget>? subAgreement;
  final bool showAuthMessage;
  final Function updateChecker;
  final Function onSended;
  final List<AuthState>? state;

  AuthcenterFileUploadView({
    this.data,
    this.desc,
    this.title = '',
    this.sampleImg,
    this.isConfirmList,
    this.uploadType,
    this.appbarTitle = '',
    this.howToBlock,
    this.subAgreement,
    this.showAuthMessage = true,
    this.alertTitle = '',
    this.alertBody = '',
    required this.updateChecker,
    required this.onSended,
    this.state,
  });

  @override
  _AuthcenterFileUploadViewState createState() => _AuthcenterFileUploadViewState();
}

class _AuthcenterFileUploadViewState extends State<AuthcenterFileUploadView> {
  Map<AuthImageType, String> _image = {};
  bool _privacyAgree = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        backgroundColor: color_white,
        appBar: appBar(widget.appbarTitle, hide: false),
        body: lScrollView(
            padding: padding_0,
            child: Column(children: [
              Container(
                  padding: padding_20,
                  child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                    sb_h_30,
                    Row(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.end, children: [
                      lText(widget.title, style: st_b_19(fontWeight: FontWeight.w700), overflow: TextOverflow.visible),
                      sb_w_02,
                      lInkWell(
                          onTap: () => {
                                showNormalDlg(
                                  context: context,
                                  title: widget.alertTitle,
                                  titleAlign: Alignment.bottomLeft,
                                  message: widget.alertBody,
                                  msgAlign: TextAlign.left,
                                  btnRight: "확인".tr(),
                                  padding: padding_30_TB,
                                  onClickAction: (action) => {},
                                )
                              },
                          child: Lcons.info(color: color_dedede, isEnabled: true, size: 25)),
                    ]),
                    sb_h_20,
                    lDivider(color: color_eeeeee),
                    sb_h_20,
                    if (widget.desc != null)
                      Container(
                          width: double.infinity,
                          padding: padding_20,
                          decoration: BoxDecoration(
                            color: color_6bd5eae8,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: widget.desc!),
                    sb_h_20,
                    if (widget.state != null && widget.state!.where((e) => e.status == AuthStatus.fail.index).isNotEmpty)
                      Container(
                        padding: padding_10,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: color_eeeeee.withOpacity(0.32),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: widget.state!.map((e) => _buildFailMessage(type: e.authImgtype, reason: e.cancelReason, status: e.status)).toList(),
                        ),
                      ),
                    if (widget.howToBlock != null)
                      Container(
                        padding: padding_10_TB,
                        decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
                        child: Theme(
                          data: Theme.of(context).copyWith(dividerColor: Colors.transparent), // remove border
                          child: ExpansionTile(
                            expandedAlignment: Alignment.topLeft,
                            tilePadding: padding_0,
                            childrenPadding: padding_0,
                            iconColor: color_222222,
                            collapsedIconColor: color_222222,
                            title: lText("발급방법".tr(), style: st_b_16(fontWeight: FontWeight.w500)),
                            children: widget.howToBlock!,
                          ),
                        ),
                      ),
                    Container(
                      padding: padding_10_TB,
                      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_eeeeee))),
                      child: Theme(
                          data: Theme.of(context).copyWith(dividerColor: Colors.transparent), // remove border
                          child: ExpansionTile(
                              expandedAlignment: Alignment.topLeft,
                              tilePadding: padding_0,
                              childrenPadding: padding_0,
                              iconColor: color_222222,
                              collapsedIconColor: color_222222,
                              title: lText("예시이미지".tr(), style: st_b_16(fontWeight: FontWeight.w500)),
                              expandedCrossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Column(
                                    children: widget.sampleImg!
                                        .map((e) => Stack(children: [
                                              Image.asset(e),
                                              Positioned(
                                                  right: 10,
                                                  bottom: 10,
                                                  child: lInkWell(
                                                    child: Container(
                                                      padding: padding_06,
                                                      decoration: BoxDecoration(color: color_060606.withOpacity(0.42), shape: BoxShape.circle),
                                                      child: Lcons.search(disableColor: Colors.white, size: 25),
                                                    ),
                                                    onTap: () => Commons.nextPage(context, fn: () => FullPhotoPage(urlList: widget.sampleImg)),
                                                  ))
                                            ]))
                                        .toList()),
                                if (widget.showAuthMessage)
                                  Padding(
                                    padding: padding_05_TB,
                                    child: lText("인증배지부여".tr(), style: st_13(textColor: color_ff3b30)),
                                  ),
                              ])),
                    ),
                    Column(
                        children: widget.uploadType!
                            .map((e) => AuthImageButton(
                                type: e,
                                state: widget.state!.firstWhere(
                                  (stat) => stat.authImgtype == e.value,
                                  orElse: () => AuthState(authImgtype: e.value, status: AuthStatus.none.index),
                                ),
                                onClickAction: (image) {
                                  _encImage(image).then((encImg) {
                                    setState(() {
                                      _image.update(e, (v) => encImg, ifAbsent: () => encImg);
                                    });
                                  });
                                },
                                showBorder: e != widget.uploadType!.last))
                            .toList()),
                  ])),
              lDivider(color: color_eeeeee, thickness: 8.0),
              Container(
                  alignment: Alignment.center,
                  color: Colors.white,
                  padding: padding_20_T,
                  width: Commons.width(context, 0.45),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Agreement(
                          name: "개인정보처리동의".tr(),
                          agree: _privacyAgree,
                          action: (a) {
                            setState(() => _privacyAgree = a);
                            widget.updateChecker(_privacyAgree);
                          },
                          terms: () => Commons.page(context, routeCsTermsView, arguments: CsTermsViewPage(terms: Terms.privacy))),
                      if (widget.subAgreement != null) Column(crossAxisAlignment: CrossAxisAlignment.start, children: widget.subAgreement!.map((e) => Container(margin: padding_04_TB, child: e)).toList()),
                    ],
                  )),
              lBtn("인증신청".tr(), btnColor: color_545454, isEnabled: _isConfirm(), onClickAction: () {
                _requestUploadImage();
              }),
            ])));
  }

  bool _isConfirm() {
    return _image.isNotEmpty && (_privacyAgree && (widget.isConfirmList != null ? widget.isConfirmList!.where((element) => !element).isEmpty : true));
  }

  Future<void> _requestUploadImage() async {
    widget.onSended(DialogAction.on);
    try {
      Future.forEach(_image.keys, (AuthImageType e) async {
        AuthcenterImgSaveRequest req = AuthcenterImgSaveRequest(auth_imgtype: e.value);
        FormData formData = FormData();
        req.toFormData(formData, encImg: _image[e]);
        var response = await apiHelper.apiCall(HttpType.post, ApiEndPoint.EP_AUTHCENTER_IMG_SAVE, apiHeader.publicApiHeader, formData, isFile: true);
        var responseJson = Commons.returnResponse(response);
        AuthcenterImgSaveResponse responseData = AuthcenterImgSaveResponse.fromJson(responseJson);
        if (e == _image.keys.last) widget.onSended(DialogAction.off);
        if (responseData.getCode() == KEY_SUCCESS) {
          if (e == _image.keys.last) {
            widget.onSended(DialogAction.confirm);
          }
        } else {
          widget.onSended(DialogAction.cancel);
        }
      });
    } catch (e) {
      widget.onSended(DialogAction.off);
    }
  }

  Widget _buildFailMessage({int type = 0, String reason = '', int status = 0}) {
    if (status != AuthStatus.fail.index) {
      return Container();
    } else {
      return Container(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextHighlight(
            text: EnumUtils.getAuthImageType(type).name + " " + "반려사유".tr(),
            term: "반려사유".tr(),
            textStyle: st_15(textColor: color_545454, fontWeight: FontWeight.w500),
            textStyleHighlight: st_15(textColor: color_ff3b30, fontWeight: FontWeight.w500),
          ),
          lText(reason, style: st_13(textColor: color_545454)),
          sb_h_10,
        ],
      ));
    }
  }

  Future<String> _encImage(File file) async {
    return await encryptHelper.encodeData(file);
  }
}

class AuthImageButton extends StatefulWidget {
  final AuthImageType type;
  final Function? onClickAction;
  final bool showBorder;
  final AuthState? state;

  AuthImageButton({Key? key, required this.type, this.onClickAction, this.showBorder = true, this.state}) : super(key: key);

  @override
  State<AuthImageButton> createState() => _AuthImageButtonState();
}

class _AuthImageButtonState extends State<AuthImageButton> {
  File? _image;
  bool _isEdit = false;
  bool _isDisable = false;

  @override
  void initState() {
    super.initState();
    _isEdit = _image != null;
    if (widget.state != null) {
      if (widget.state!.status == AuthStatus.success.index) {
        _isEdit = widget.state!.status == AuthStatus.success.index;
      } else {
        _isDisable = widget.state!.status == AuthStatus.ready.index;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(children: [
      Container(
          decoration: BoxDecoration(border: Border(bottom: BorderSide(color: widget.showBorder ? color_eeeeee : Colors.transparent))),
          padding: widget.showBorder ? padding_20_TB : EdgeInsets.fromLTRB(0, 20, 0, 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  lText(widget.type.name, style: st_b_16(fontWeight: FontWeight.w500)),
                  if (widget.state != null) _getStateName(widget.state),
                ],
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                lTextBtn(
                  _isEdit
                      ? "사진변경".tr()
                      : _isDisable
                          ? "인증대기".tr()
                          : "사진등록".tr(),
                  leftIcon: Padding(
                    padding: padding_07_R,
                    child: Lcons.upload(size: 18, isEnabled: _isEdit, color: color_main, disableColor: _isDisable ? color_dedede : color_545454),
                  ),
                  style: st_14(
                      fontWeight: FontWeight.w500,
                      textColor: _isEdit
                          ? color_main
                          : _isDisable
                              ? color_dedede
                              : color_545454),
                  border: Border.all(
                      color: _isEdit
                          ? color_main
                          : _isDisable
                              ? color_dedede
                              : color_545454,
                      width: 1),
                  radius: 8,
                  bg: Colors.transparent,
                  padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                  onClickEvent: () {
                    if (!_isDisable)
                      showDlgCamera(
                          btn1: "사진선택".tr(),
                          btn2: "사진찍기".tr(),
                          onClickAction: (action) async {
                            log.d('『GGUMBI』>>> build : action: $action,  <<< ');
                            if (action == DialogAction.gallery) {
                              _image = await Commons.getImage(ImageSource.gallery, maxWidth: 700);
                            } else if (action == DialogAction.camera) {
                              _image = await Commons.getImage(ImageSource.camera, maxWidth: 700);
                            }
                            setState(() {
                              _isEdit = _image != null;
                              if (widget.onClickAction != null) widget.onClickAction!(_image);
                            });
                          });
                  },
                ),
              ])
            ],
          ))
    ]));
  }

  Widget _getStateName(AuthState? status) {
    if (status != null) {
      switch (status.status) {
        case 1:
          return lText("인증성공".tr(), style: st_12(textColor: color_main));
        case 2:
          return lText("인증반려".tr(), style: st_12(textColor: color_ff3b30));
        case 0:
          return lText("인증대기".tr(), style: st_12(textColor: color_999999));
        default:
          return lText("", style: st_12(textColor: color_999999));
      }
    } else {
      return lText("", style: st_12(textColor: color_999999));
    }
  }
}

class Agreement extends StatefulWidget {
  bool agree;
  final String name;
  final Function action;
  final Function terms;

  Agreement({Key? key, this.name = '', required this.action, this.agree = false, required this.terms}) : super(key: key);

  @override
  _AgreementState createState() => _AgreementState();
}

class _AgreementState extends State<Agreement> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        lInkWell(
            onTap: () {
              setState(() {
                widget.agree = !widget.agree;
                widget.action(widget.agree);
              });
            },
            child: Lcons.check_circle(disableColor: color_cecece, color: color_545454, isEnabled: widget.agree)),
        sb_w_10,
        lInkWell(onTap: () => widget.terms(), child: lText(widget.name, style: st_b_14())),
      ],
    );
  }
}
