import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/view/lcons.dart';

class AuthCenterPersonaltyPage extends BaseStateful {
  final DataManager? data;
  AuthCenterPersonaltyPage({this.data});

  @override
  _AuthCenterPersonaltyPageState createState() => _AuthCenterPersonaltyPageState();
}

class _AuthCenterPersonaltyPageState extends BaseStatefulState<AuthCenterPersonaltyPage> {
  bool _privacyAgree = false;

  @override
  void initState() {
    super.initState();
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager data) {
    super.onData(data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            backgroundColor: color_white,
            appBar: appBar("인증센터_인적성".tr(), hide: false),
            body: Container(
                child: ListView(children: [
              Container(
                  padding: padding_20,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      sb_h_20,
                      lText("인적성_타이틀".tr(), style: st_b_20(fontWeight: FontWeight.w700)),
                      sb_h_20,
                      lDivider(color: color_eeeeee),
                      sb_h_20,
                      Container(
                          width: double.infinity,
                          padding: padding_20,
                          decoration: BoxDecoration(
                            color: color_6bd5eae8,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                lText("인적성_안내_시간제한".tr()), 
                                lText("인적성_안내_재응시".tr()),
                                lText("인적성_안내_불이익".tr())
                              ])),
                      sb_h_30,
                      lText("링크쌤_인적성_검사".tr(), style: st_b_16(fontWeight: FontWeight.w700)),
                      sb_h_08,
                      lText("링크쌤_인적성_검사_안내".tr(), style: st_b_13()),
                      sb_h_20,
                      lBtnOutline("인적성_검사_시작".tr(), borderRadius: 8.0, sideColor: color_545454, 
                        style: st_b_14(textColor: color_545454, fontWeight: FontWeight.w500), 
                        onClickAction: () => {}),
                      sb_h_40,
                    ],
                  )),
              lDivider(color: color_eeeeee, thickness: 8.0),
              sb_h_10,
              Container(
                  padding: padding_20_TB,
                  color: Colors.white,
                  child: Column(
                    children: [
                      sb_h_20,
                      lInkWell(
                          onTap: () => {_privacyAgree = !_privacyAgree, onUpDate()},
                          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                            Lcons.check_circle(isEnabled: _privacyAgree,
                                disableColor: color_cecece,
                                color: color_545454),
                            sb_w_10,
                            lText("개인정보처리동의".tr()),
                          ])),
                      lBtn("인증신청".tr(), btnColor: color_545454, isEnabled: _privacyAgree, onClickAction: () {
                        flag.enableLoading(fn: () => onUpDate());
                        onUpDate();
                      }),
                    ],
                  )),
            ]))));
  }

  Widget _howToCommentBlock(int index, Widget child) {
    return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [lText("$index.", style: st_14(textColor: color_main, fontWeight: FontWeight.w900)), sb_w_05, child]);
  }
}
