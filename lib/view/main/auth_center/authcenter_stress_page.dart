import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/survey_list_response.dart';
import 'package:linkmom/data/network/models/survey_question_request.dart';
import 'package:linkmom/data/network/models/survey_list_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/survey/survey_question_page.dart';

class AuthCenterStressPage extends BaseStateful {
  final DataManager? data;
  final String surveyName;
  AuthCenterStressPage({this.data, this.surveyName = ''});

  @override
  _AuthCenterStressPageState createState() => _AuthCenterStressPageState();
}

class _AuthCenterStressPageState extends BaseStatefulState<AuthCenterStressPage> {
  List<String> _answer = [];
  int surveyNumber = 0;
  SurveyData _survey = SurveyData();

  @override
  void initState() {
    super.initState();
    onData(widget.data ?? DataManager());
    _requestSurveyData();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _requestSurvey();
    });
  }

  @override
  void onData(DataManager data) {
    super.onData(data);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar("인증센터_스트레스".tr(), hide: false),
            body: Container(
                child: Stack(alignment: Alignment.bottomCenter, children: [
              Positioned(
                bottom: data.height(context, 0.1) * (Commons.ratioChecker(data.height(context, 1), data.width(context, 1)) ? -1 : -5),
                right: 0,
                left: 0,
                child: Image.asset(ILLUST_STRESS),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                      padding: EdgeInsets.only(top: 70),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          lText("인증센터_스트레스".tr(), style: st_b_30(fontWeight: FontWeight.w700)),
                          sb_h_15,
                          lText("스트레스검사_안내".tr(), style: st_b_16()),
                        ],
                      )),
                  lBtn("테스트시작".tr(), btnColor: color_545454, onClickAction: () {
                    _startSurvey();
                  }),
                ],
              ),
            ]))));
  }

  void _requestSurvey() async {
    try {
      SurveyListRequest req = SurveyListRequest(page: 1);
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestSurvey(req, type: SurveyType.stress).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          data.myInfoItem.surveyInfo = response.getData();
          surveyNumber = response.getData().results!.first.id;
          onUpDate();
        } else {
          showNormalDlg(message: response.getMsg());
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
      flag.disableLoading(fn: () => onUpDate());
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _startSurvey() {
    try {
      SurveyQuestionRequest req = SurveyQuestionRequest(page: 1);
      apiHelper.requestStressQuestion(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          data.myInfoItem.questions.clear();
          data.myInfoItem.questions.addAll(response.getData().results!);
          Commons.page(context, routeSurveyQuestion,
              arguments: SurveyQuestionPage(
                data: data,
                count: response.getData().count,
                sequence: data.myInfoItem.questions.first.question_seq,
                type: SurveyType.stress,
                question: data.myInfoItem.questions,
                surveyNumber: response.getData().results!.first.survey,
                title: "인증센터_스트레스".tr(),
              ));
        } else {
          showNormalDlg(message: response.getMsg());
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestSurveyData() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      apiHelper.requestSurvey(SurveyListRequest(page: SurveyType.stress.value), type: SurveyType.stress).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _survey = response.getData();
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}
