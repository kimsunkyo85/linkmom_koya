import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';

import '../../../../base/base_stateful.dart';
import 'ourhome_page.dart';

class AuthHomeStatePage extends BaseStateful {
  final ViewType viewType;
  @override
  _HomeStatePageState createState() => _HomeStatePageState();

  AuthHomeStatePage({this.data, this.viewType = ViewType.modify});

  final DataManager? data;
}

class _HomeStatePageState extends BaseStatefulState<AuthHomeStatePage> {
  @override
  initState() {
    super.initState();
    onData(widget.data ?? DataManager());
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: appBar("집안환경".tr(), hide: false, isClose: false),
        body: Column(
          children: [
            Expanded(
              child: Center(
                child: Container(
                  padding: padding_20,
                  width: double.infinity,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Lcons.edit_house(color: Commons.getColor(), size: 100),
                      sb_h_20,
                      lText(getTitle(), style: st_b_20(fontWeight: FontWeight.bold)),
                      sb_h_30,
                      TextHighlight(
                        text: "집안환경_설명".tr(),
                        term: "맞춤매칭".tr(),
                        textStyle: st_b_18(),
                        textStyleHighlight: st_b_18(textColor: Commons.getColor()),
                      ),
                      TextHighlight(
                        text: "집안환경_설명2".tr(),
                        term: "인증배지".tr(),
                        textStyle: st_b_18(),
                        textStyleHighlight: st_b_18(textColor: Commons.getColor()),
                      ),
                      sb_h_15,
                      lText("집안환경_설명3".tr(), style: st_b_12(textColor: color_999999)),
                      SizedBox(
                        width: 1,
                        height: appBarHeight(),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            // wdDivider(),
            _confirmButton(),
          ],
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn("다음".tr(), btnColor: Commons.getColor(), onClickAction: () {
      Commons.page(context, routeOurHome, arguments: OurHomeStatePage(data: data, viewType: widget.viewType));
    });
  }

  String getTitle() {
    String value = '${"돌봄신청전".tr()}${"집안환경_선택".tr()}';
    if (storageHelper.user_type == USER_TYPE.mom_daddy) {
      value = '${"돌봄신청전".tr()}${"집안환경_선택".tr()}';
    } else {
      value = '${"링크쌤등록전".tr()}${"집안환경_선택".tr()}';
    }
    return value;
  }
}
