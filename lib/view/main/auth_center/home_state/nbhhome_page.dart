import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/authcenter_home_request.dart';
import 'package:linkmom/data/network/models/authcenter_home_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/listview/list_authtype_select.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/guide/guide_momdady_page.dart';
import 'package:linkmom/view/main/job_apply/title_toggle_icon_view.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';

class NbhHomeStatePage extends BaseStateful {
  final ViewType viewType;
  final String name;
  final Function? callback;
  @override
  _HomeStatePageState createState() => _HomeStatePageState();

  NbhHomeStatePage({this.data, this.viewType = ViewType.modify, this.name = '', this.callback});

  final DataManager? data;
}

class _HomeStatePageState extends BaseStatefulState<NbhHomeStatePage> {
  @override
  initState() {
    super.initState();
    log.d('『GGUMBI』>>> initState : data.homeItem.responsa: ${widget.data},  <<< ');
    log.d('『GGUMBI』>>> initState : data.homeItem.response: ${data.homeItem.response},  <<< ');
    onData(widget.data ?? DataManager());
  }

  @override
  void onData(DataManager data) {
    super.onData(data);
    this.data.homeItem = data.homeItem;
    log.d('『GGUMBI』>>> onData : : ${data.homeItem},  <<< ');
    onDataPage(this.data.homeItem.response);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: widget.viewType == ViewType.apply ? appBar("집안환경".tr(), isClose: false, hide: false) : null,
        body: Column(
          children: [
            Expanded(
              child: lScrollView(
                child: Container(
                  width: double.infinity,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      sb_h_30,
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          widget.viewType == ViewType.view
                              ? lText('${widget.name}${"원하는이웃집환경".tr()}', style: st_b_22(fontWeight: FontWeight.bold))
                              : Expanded(
                                  child: Column(
                                    children: [
                                      lText('${"환경인증_이웃집환경".tr()}${"을".tr()}', style: st_b_22(fontWeight: FontWeight.bold), textAlign: TextAlign.left),
                                      titleToggleIconView(data, "환경인증_선택해주세요".tr(), titleStyle: st_b_22(fontWeight: FontWeight.bold), isInfo: true, flex: 6, flex2: 1, infoColor: color_dedede, rightWidget: lText(''), onClick: (action) {
                                        log.d('『GGUMBI』>>> _carePaylView ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<<$action ');
                                        showNormalDlg(
                                          title: "환경인증_이웃집환경".tr(),
                                          messageWidget: lText(
                                            "환경인증_이웃집_안내".tr(),
                                            style: st_16(textColor: color_545454),
                                            textAlign: TextAlign.center,
                                          ),
                                        );
                                      }),
                                    ],
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                  ),
                                ),
                          Lcons.neighbor_house(size: 70)
                        ],
                      ),
                      sb_h_30,
                      ListAuthTypeSelector(
                        enableColor: _getColor(),
                        title: "환경인증_이웃집주거타입".tr(),
                        data: data.homeItem.ls_nbhhome,
                        isOverlap: true,
                        isEditable: widget.viewType != ViewType.view,
                        onItemClick: (value) {
                          log.d('『GGUMBI』>>> build : data: $value,  <<< ');
                          onConfirmBtn();
                        },
                      ),
                      sb_h_20,
                      ListAuthTypeSelector(
                        enableColor: _getColor(),
                        title: "환경인증_CCTV".tr(),
                        data: data.homeItem.ls_nbhhome_cctv,
                        isEditable: widget.viewType != ViewType.view,
                        onItemClick: (value) {
                          log.d('『GGUMBI』>>> build : data: $value,  <<< ');
                          onConfirmBtn();
                        },
                      ),
                      sb_h_20,
                      ListAuthTypeSelector(
                        enableColor: _getColor(),
                        title: "환경인증_반려동물".tr(),
                        data: data.homeItem.ls_nbhhome_animal,
                        isEditable: widget.viewType != ViewType.view,
                        onItemClick: (value) {
                          log.d('『GGUMBI』>>> build : data: $value,  <<< ');
                          onConfirmBtn();
                        },
                      ),
                      sb_h_20,
                      ListAuthTypeSelector(
                        enableColor: _getColor(),
                        title: "환경인증_이웃집사진등록".tr(),
                        data: data.homeItem.ls_nbhhome_picture,
                        isEditable: widget.viewType != ViewType.view,
                        onItemClick: (value) {
                          log.d('『GGUMBI』>>> build : data: $value,  <<< ');
                          onConfirmBtn();
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
            // wdDivider(),
            if (widget.viewType != ViewType.view) _confirmButton(),
          ],
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn(widget.viewType == ViewType.apply ? "다음".tr() : "수정".tr(), isEnabled: flag.isConfirm, btnColor: _getColor(), onClickAction: () {
      _requestAuthHome(HttpType.put, data.homeItem.request);
    });
  }

  @override
  void onConfirmBtn() {
    bool isHome = false;
    data.homeItem.request.nbhhome_type = [];
    data.homeItem.ls_nbhhome.asMap().forEach((index, value) {
      if (value.isSelected) {
        isHome = true;
        data.homeItem.request.nbhhome_type!.add(value.type);
      }
    });

    bool isCctv = false;
    data.homeItem.ls_nbhhome_cctv.asMap().forEach((index, value) {
      if (value.isSelected) {
        isCctv = true;
        data.homeItem.request.nbhhome_cctv = value.type;
        return;
      }
    });

    bool isAnimal = false;
    data.homeItem.ls_nbhhome_animal.asMap().forEach((index, value) {
      if (value.isSelected) {
        isAnimal = true;
        data.homeItem.request.nbhhome_animal = value.type;
        return;
      }
    });

    bool isImage = false;
    data.homeItem.ls_nbhhome_picture.asMap().forEach((index, value) {
      if (value.isSelected) {
        isImage = true;
        data.homeItem.request.nbhhome_picture = value.type;
        return;
      }
    });

    log.d('『GGUMBI』>>> onConfirmBtn : isHome: $isHome, \nisCctv: $isCctv,\nisAnimal: $isAnimal,\nisImage: $isImage, _images: ${data.homeItem.request},<<< ');
    if (isHome && isCctv && isAnimal && isImage) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      if (data.homeItem.isSkip) {
        flag.enableConfirm(fn: () => onUpDate());
      } else {
        flag.disableConfirm(fn: () => onUpDate());
      }
    }
  }

  @override
  void onDataPage(_data) {
    if (data.homeItem.response.dataList == null || data.homeItem.response.dataList!.isEmpty) {
      onConfirmBtn();
      return;
    }

    data.homeItem.ls_nbhhome.asMap().forEach((index, value) {
      if (value.keyName == HomeStateData.Key_nbhhome_type) {
        var item = data.homeItem.response.getData().nbhhome_type;
        item!.forEach((data) {
          if (value.type == data) {
            value.isSelected = true;
          }
        });
      }
    });

    data.homeItem.ls_nbhhome_cctv.asMap().forEach((index, value) {
      if (value.keyName == HomeStateData.Key_nbhhome_cctv) {
        if (value.type == data.homeItem.response.getData().nbhhome_cctv) {
          value.isSelected = true;
        } else {
          value.isSelected = false;
        }
      }
    });

    data.homeItem.ls_nbhhome_animal.asMap().forEach((index, value) {
      if (value.keyName == HomeStateData.Key_nbhhome_animal) {
        if (value.type == data.homeItem.response.getData().nbhhome_animal) {
          value.isSelected = true;
        } else {
          value.isSelected = false;
        }
      }
    });

    data.homeItem.ls_nbhhome_picture.asMap().forEach((index, value) {
      if (value.keyName == HomeStateData.Key_nbhhome_picture) {
        if (value.type == data.homeItem.response.getData().nbhhome_picture) {
          value.isSelected = true;
        } else {
          value.isSelected = false;
        }
      }
    });
    // onUpDate();
    onConfirmBtn();
  }

  ///환경인증 요청
  _requestAuthHome(HttpType type, AuthCenterHomeRequest _data) async {
    flag.enableLoading(fn: () => onUpDate());
    await apiHelper.requestHomeStateSave(_data, type, ApiEndPoint.EP_AUTH_CENTER_HOME_SAVE).then((response) async {
      flag.disableLoading(fn: () => onUpDate());
      if (response.getCode() == KEY_SUCCESS) {
        auth.user.my_info_data!.isAuthHomestate = true;
      }
      data.homeItem.response = response;
      if (widget.viewType == ViewType.modify) {
        if (widget.callback != null) widget.callback!(response.getData());
      } else {
        setViewType(ViewType.apply);
        if (Commons.isLinkMom()) {
          Commons.pageToMain(context, routeGuideLinkmom);
        } else {
          Commons.pageToMain(context, routeGuideMomdady, arguments: GuideMomDadyPage());
        }
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  Color _getColor() {
    return widget.viewType == ViewType.apply ? color_545454 : Commons.getColor();
  }
}
