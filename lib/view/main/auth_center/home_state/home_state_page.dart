import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/authcenter_authmain_response.dart';
import 'package:linkmom/data/network/models/authcenter_home_request.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/image/image_add_page.dart';
import 'package:linkmom/view/main/auth_center/home_state/nbhhome_page.dart';
import 'package:linkmom/view/main/auth_center/home_state/ourhome_page.dart';

class HomeStatePage extends StatefulWidget {
  final DataManager? data;
  final UserHomeStateData? homeState;
  final String name;
  final ViewType viewType;
  HomeStatePage({this.data, this.homeState, this.name = '', this.viewType = ViewType.view});

  @override
  _HomeStatePageState createState() => _HomeStatePageState();
}

class _HomeStatePageState extends State<HomeStatePage> {
  @override
  void initState() {
    super.initState();
    widget.data!.homeItem.response.dataList = null;
    widget.data!.homeItem.ls_ourhome[_getIndex(widget.homeState!.ourhome_type)].isSelected = true;
    widget.data!.homeItem.ls_outhome_cctv[_getIndex(widget.homeState!.ourhome_cctv)].isSelected = true;
    if (widget.homeState!.ourhome_cctv < 2) widget.data!.homeItem.ls_outhome_cctv[widget.homeState!.ourhome_cctv].isSelected = false;
    widget.homeState!.ourhome_animal!.forEach((e) {
      widget.data!.homeItem.ls_outhome_animal.firstWhere((animal) => animal.type == e).isSelected = true;
    });
    widget.homeState!.nbhhome_type!.forEach((e) {
      widget.data!.homeItem.ls_nbhhome[e].isSelected = true;
    });
    widget.data!.homeItem.ls_nbhhome_cctv[_getIndex(widget.homeState!.nbhhome_cctv)].isSelected = true;
    widget.data!.homeItem.ls_nbhhome_cctv[widget.homeState!.nbhhome_cctv].isSelected = false;
    widget.data!.homeItem.ls_nbhhome_animal[_getIndex(widget.homeState!.nbhhome_animal)].isSelected = true;
    widget.data!.homeItem.ls_nbhhome_animal[widget.homeState!.nbhhome_animal].isSelected = false;
    widget.data!.homeItem.ls_nbhhome_picture[_getIndex(widget.homeState!.nbhhome_picture)].isSelected = true;
    widget.data!.homeItem.ls_nbhhome_picture[widget.homeState!.nbhhome_picture].isSelected = false;
    widget.data!.homeItem.images = [
      if (widget.homeState!.ourhome_picture1 != null && widget.homeState!.ourhome_picture1!.isNotEmpty)
        ImageItem(
          key: AuthCenterHomeRequest.Key_ourhome_picture1,
          imageThumbnail: widget.homeState!.ourhome_picture1_thumbnail,
          imageUrl: widget.homeState!.ourhome_picture1,
        ),
      if (widget.homeState!.ourhome_picture2 != null && widget.homeState!.ourhome_picture2!.isNotEmpty)
        ImageItem(
          key: AuthCenterHomeRequest.Key_ourhome_picture2,
          imageThumbnail: widget.homeState!.ourhome_picture2_thumbnail,
          imageUrl: widget.homeState!.ourhome_picture2,
        ),
    ];
    if (widget.data!.homeItem.images.where((element) => element.imageUrl!.isNotEmpty).isEmpty) widget.data!.homeItem.images = [];
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
      appBar: appBar("집안환경"),
      body: TabPageView(
        tabbarName: [
          TabItem(title: _buildName(widget.viewType, !Commons.isLinkMom())),
          TabItem(title: _buildName(widget.viewType, Commons.isLinkMom())),
        ],
        tabbarView: [
          OurHomeStatePage(
            viewType: widget.viewType,
            data: widget.data,
            name: widget.name,
            callback: (resp) {
              Commons.pagePop(context, data: resp);
            },
          ),
          NbhHomeStatePage(
            viewType: widget.viewType,
            data: widget.data,
            name: widget.name,
            callback: (resp) {
              Commons.pagePop(context, data: resp);
            },
          ),
        ],
      ),
    );
  }

  String _buildName(ViewType view, bool isLinkmom) {
    return view == ViewType.view ? Commons.possibleAreaSubText(isLinkmom ? USER_TYPE.link_mom.name : USER_TYPE.mom_daddy.name) : Commons.possibleAreaSubText(isLinkmom ? USER_TYPE.mom_daddy.name : USER_TYPE.link_mom.name);
  }

  int _getIndex(int count) => count == 0
      ? count + 1
      : count == 1
          ? count - 1
          : count;
}
