import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/authcenter_home_delete_request.dart';
import 'package:linkmom/data/network/models/authcenter_home_request.dart';
import 'package:linkmom/data/network/models/authcenter_home_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/listview/list_authtype_select.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/image/image_add_page.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/auth_center/home_state/nbhhome_page.dart';
import 'package:linkmom/view/main/job_apply/content_view.dart';
import 'package:linkmom/view/main/job_apply/title_toggle_icon_view.dart';

import '../../../../base/base_stateful.dart';
import '../../../../main.dart';

class OurHomeStatePage extends BaseStateful {
  final ViewType viewType;
  final String name;
  final Function? callback;

  @override
  _HomeStatePageState createState() => _HomeStatePageState();

  OurHomeStatePage({this.data, this.viewType = ViewType.modify, this.name = '', this.callback});

  final DataManager? data;
}

class _HomeStatePageState extends BaseStatefulState<OurHomeStatePage> {
  @override
  initState() {
    super.initState();
    onData(widget.data ?? DataManager());

    ///환경인증 가져오기 요청
    if (widget.viewType == ViewType.apply) _requestAuthHome(HttpType.post, null);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      onConfirmBtn();
    });
  }

  @override
  void onData(DataManager _data) {
    super.onData(_data);
    this.data = widget.data ?? DataManager();
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        appBar: widget.viewType == ViewType.apply ? appBar("집안환경".tr(), isClose: false, hide: false) : null,
        body: Column(
          children: [
            Expanded(
              child: lScrollView(
                child: Container(
                  width: double.infinity,
                  child: Column(
                    children: <Widget>[
                      sb_h_30,
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          widget.viewType == ViewType.view
                              ? lText('${widget.name}${"집의환경".tr()}', style: st_b_22(fontWeight: FontWeight.bold))
                              : Expanded(
                                  child: Column(
                                    children: [
                                      lText('${"환경인증_우리집환경".tr()}${"을".tr()}', style: st_b_22(fontWeight: FontWeight.bold), textAlign: TextAlign.left),
                                      titleToggleIconView(data, "환경인증_선택해주세요".tr(), titleStyle: st_b_22(fontWeight: FontWeight.bold), isInfo: true, flex: 6, flex2: 1, infoColor: color_dedede, rightWidget: lText(''), onClick: (action) {
                                        showNormalDlg(
                                          title: "환경인증_우리집환경".tr(),
                                          messageWidget: TextHighlight(
                                            text: "환경인증_우리집_안내".tr(),
                                            textAlign: TextAlign.center,
                                            textStyle: st_16(textColor: color_545454),
                                            term: "환경인증_우리집_안내_하이라이트".tr(),
                                            textStyleHighlight: st_16(
                                              textColor: color_545454,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        );
                                      }),
                                    ],
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                  ),
                                ),
                          Lcons.my_house(size: 70),
                        ],
                      ),
                      sb_h_30,
                      ListAuthTypeSelector(
                        title: "환경인증_우리집주거타입".tr(),
                        enableColor: _getColor(),
                        data: data.homeItem.ls_ourhome,
                        isEditable: widget.viewType != ViewType.view,
                        onItemClick: (value) {
                          onConfirmBtn();
                        },
                      ),
                      sb_h_20,
                      ListAuthTypeSelector(
                        title: "환경인증_CCTV".tr(),
                        enableColor: _getColor(),
                        data: data.homeItem.ls_outhome_cctv,
                        isEditable: widget.viewType != ViewType.view,
                        onItemClick: (value) {
                          onConfirmBtn();
                        },
                      ),
                      sb_h_20,
                      ListAuthTypeSelector(
                        title: "환경인증_반려동물".tr(),
                        enableColor: _getColor(),
                        data: data.homeItem.ls_outhome_animal,
                        isOverlap: true,
                        isEditable: widget.viewType != ViewType.view,
                        onItemClick: (value) {
                          onConfirmBtn();
                        },
                      ),
                      sb_h_20,
                      Column(
                        children: [
                          contentView(
                              data: ContentData(
                            content: "환경인증_우리집사진등록".tr(),
                            contentStyle: st_b_17(fontWeight: FontWeight.bold),
                          )),
                          sb_h_10,
                          widget.viewType == ViewType.view
                              ? data.homeItem.images.isEmpty
                                  ? Container(
                                      padding: padding_10_TB,
                                      alignment: Alignment.centerLeft,
                                      child: lText("등록된사진이없습니다".tr(), style: st_14(fontWeight: FontWeight.w500, textColor: color_999999)),
                                    )
                                  : imageThumbnailView(
                                      context,
                            data.homeItem.images.map((e) => e.imageThumbnail ?? '').toList(),
                                      origin: data.homeItem.images.map((e) => e.imageUrl ?? '').toList(),
                                    )
                              : ImageAdd(
                                  title: "환경인증_우리집사진등록".tr(),
                                  data: data.homeItem.images,
                                  onItemClick: (value, index) {
                                    if (index != -1) {
                                      ImageItem item = value as ImageItem;
                                      if (item.key!.isNotEmpty) {
                                        if (StringUtils.validateString(item.imageUrl)) {
                                          _requestAuthHomeDelete(HttpType.delete, AuthCenterHomeDeleteRequest(fileid: item.key!));
                                          if (item.key == AuthCenterHomeRequest.Key_ourhome_picture1) {
                                            data.homeItem.response.getData().ourhome_picture1 = '';
                                          } else {
                                            data.homeItem.response.getData().ourhome_picture2 = '';
                                          }
                                        }
                                      }
                                    } else {
                                      data.homeItem.images = value;
                                    }
                                    onConfirmBtn();
                                  },
                                  onCheck: (isCheck) {
                                    data.homeItem.isSkip = isCheck;
                                    onConfirmBtn();
                                  },
                                ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            // wdDivider(),
            if (widget.viewType != ViewType.view) _confirmButton(),
          ],
        ),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return lBtn(widget.viewType == ViewType.apply ? "다음".tr() : "수정".tr(), isEnabled: flag.isConfirm, btnColor: _getColor(), onClickAction: () {
      if (widget.viewType == ViewType.apply) {
        Commons.page(context, routeNbhHome, arguments: NbhHomeStatePage(data: data, viewType: widget.viewType));
      } else {
        _requestAuthHomeSave(data.homeItem.request);
      }
    });
  }

  @override
  void onConfirmBtn() {
    bool isHome = false;
    data.homeItem.ls_ourhome.asMap().forEach((index, value) {
      if (value.isSelected) {
        isHome = true;
        data.homeItem.request.ourhome_type = value.type;
        return;
      }
    });

    bool isCctv = false;
    data.homeItem.ls_outhome_cctv.asMap().forEach((index, value) {
      if (value.isSelected) {
        isCctv = true;
        data.homeItem.request.ourhome_cctv = value.type;
        return;
      }
    });

    bool isAnimal = false;
    data.homeItem.request.ourhome_animal = [];
    data.homeItem.ls_outhome_animal.asMap().forEach((index, value) {
      if (value.isSelected) {
        isAnimal = true;
        data.homeItem.request.ourhome_animal!.add(value.type);
      }
    });

    bool isImage = false;
    // TODO: GGUMBI 4/15/21 - 이미지 2개로 변경
    if (widget.viewType == ViewType.apply) {
      data.homeItem.request.ourhome_picture1 = null;
      data.homeItem.request.ourhome_picture2 = null;
      if (data.homeItem.images.isNotEmpty) {
        data.homeItem.images.asMap().forEach((index, value) {
          if (value.key == AuthCenterHomeRequest.Key_ourhome_picture1) {
            data.homeItem.request.ourhome_picture1 = value.file;
          } else if (value.key == AuthCenterHomeRequest.Key_ourhome_picture2) {
            data.homeItem.request.ourhome_picture2 = value.file;
          }
        });

        log.d('『GGUMBI』>>> onConfirmBtn : data.homeItem.request.ourhome_picture1: ${data.homeItem.request.ourhome_picture1},  <<< ');
        log.d('『GGUMBI』>>> onConfirmBtn : data.homeItem.request.ourhome_picture2: ${data.homeItem.request.ourhome_picture2},  <<< ');
        isImage = true;
      } else {
        log.d('『GGUMBI』>>> onConfirmBtn : data.homeItem.response: ${data.homeItem.response},  <<< ');
        if (data.homeItem.response.dataList!.length != 0 && StringUtils.validateString(data.homeItem.response.getData().ourhome_picture1) || StringUtils.validateString(data.homeItem.response.getData().ourhome_picture2)) {
          isImage = true;
        }
      }
    } else {
      data.homeItem.images.forEach((image) {
        if (data.homeItem.request.ourhome_picture1 == null) {
          data.homeItem.request.ourhome_picture1 = image.file;
        } else {
          data.homeItem.request.ourhome_picture2 = image.file;
        }
      });
    }

    log.d('『GGUMBI』>>> onConfirmBtn : isHome: $isHome, \nisCctv: $isCctv,\nisAnimal: $isAnimal,\nisImage: $isImage, _images: ${data.homeItem.images}, ${data.homeItem.isSkip}<<< ');
    log.d('『GGUMBI』>>> onConfirmBtn : _request: ${data.homeItem.request},  <<< ');
    if (isHome && isCctv && isAnimal) {
      // isImage 이미지 필수사항이 아니기 때문에 체크를 제외한다. 2021/06/15
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      if (data.homeItem.isSkip) {
        flag.enableConfirm(fn: () => onUpDate());
      } else {
        flag.disableConfirm(fn: () => onUpDate());
      }
    }
  }

  ///환경인증 요청
  _requestAuthHome(HttpType type, AuthCenterHomeRequest? _data) async {
    // flag.enableLoading(fn: () => onUpDate());
    await apiHelper.requestHomeStateView(type, ApiEndPoint.EP_AUTH_CENTER_HOME_VIEW).then((response) async {
      if (response.getCode() == KEY_4000_NO_DATA) {
        flag.disableLoading(fn: () => onUpDate());
        data.homeItem.response = AuthCenterHomeResponse.init();
        return null;
      } else {
        data.homeItem.response = response;
        if (widget.viewType != ViewType.apply) widget.data!.homeItem.response = response;
      }

      ///삭제
      if (type == HttpType.delete) {
        ///가져오기
      } else if (type == HttpType.post) {
        if (response.getCode() == KEY_SUCCESS) {
          data.homeItem.ls_ourhome.asMap().forEach((index, value) {
            if (value.keyName == HomeStateData.Key_ourhome_type) {
              if (value.type == data.homeItem.response.getData().ourhome_type) {
                value.isSelected = true;
              } else {
                value.isSelected = false;
              }
            }
          });

          data.homeItem.ls_outhome_cctv.asMap().forEach((index, value) {
            if (value.keyName == HomeStateData.Key_ourhome_cctv) {
              if (value.type == data.homeItem.response.getData().ourhome_cctv) {
                value.isSelected = true;
              } else {
                value.isSelected = false;
              }
            }
          });

          data.homeItem.ls_outhome_animal.asMap().forEach((index, value) {
            if (value.keyName == HomeStateData.Key_ourhome_animal) {
              var item = data.homeItem.response.getData().ourhome_animal;
              item!.forEach((data) {
                if (value.type == data) {
                  value.isSelected = true;
                }
              });
            }
          });
          onConfirmBtn();
          log.d('『GGUMBI』>>> _requestAuthHome : response.getData().homestateimg: ${response.getData().ourhome_picture1},  <<< ');
        }
        flag.disableLoading(fn: () => onUpDate());

        ///저장
      } else {
        Commons.page(context, routeNbhHome, arguments: NbhHomeStatePage(viewType: widget.viewType));
      }
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  ///환경인증 삭제
  _requestAuthHomeDelete(HttpType type, AuthCenterHomeDeleteRequest _data) async {
    await apiHelper.requestHomeStateDelete(_data, type, ApiEndPoint.EP_AUTH_CENTER_HOME_DELETE).then((response) async {
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  String getTitle() {
    String value = '${"환경인증_우리집환경".tr()}${"환경인증_선택해주세요".tr()}';
    return value;
  }

  void _requestAuthHomeSave(AuthCenterHomeRequest _data) async {
    flag.enableLoading(fn: () => onUpDate());
    await apiHelper.requestHomeStateSave(_data, HttpType.put, ApiEndPoint.EP_AUTH_CENTER_HOME_SAVE).then((response) async {
      flag.disableLoading(fn: () => onUpDate());
      if (response.getCode() == KEY_SUCCESS) {
        auth.user.my_info_data!.isAuthHomestate = true;
        if (widget.callback != null) {
          widget.callback!(response.getData());
        }
      } else {
        showNormalDlg(message: response.getMsg());
      }
      data.homeItem.response = response;

      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }

  Color _getColor() {
    return widget.viewType == ViewType.apply ? color_545454 : Commons.getColor();
  }
}
