import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/mypage_myinfo_response.dart';
import 'package:linkmom/data/storage/model/flag_data.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/home_provider.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/care_diary/linkmom_diary_list_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/momdady_diary_list_page.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import 'home_view.dart';
import 'mypage/work_noti_page.dart';

class MainPage extends BaseStateful {
  MainPage();

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends BaseStatefulState<MainPage> with TickerProviderStateMixin {
  bool _inLoading = true;
  bool _isHide = true;

  @override
  void initState() {
    super.initState();
    log.d('『GGUMBI』>>> initState : {auth.indexInfo.userinfo.firstName}: ${auth.indexInfo.userinfo.firstName},  <<< ');
    log.d('『GGUMBI』>>> initState : {auth.indexInfo.momdadyinfo.gradeMomdady}: ${auth.indexInfo.momdadyinfo.gradeMomdady},  <<< ');
    flag.enableLoading(fn: () => onUpDate());
    _requestIndex();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (storageHelper.penalty) {
        if (!storageHelper.flags.penalty!.second) {
          String message = '${"벌점이열개".tr()} ${StringUtils.formatYYMMDDHH(storageHelper.penaltyDate)} ~ ${StringUtils.formatYYMMDDHH(storageHelper.penaltyDate.add(Duration(days: 7)))}${"사용이중지된계정입니다".tr()}';
          storageHelper.setPenalty(PenaltyFlag(second: true));
          showNormalDlg(context: context, message: message);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<HomeNavigationProvider>(context);
    if (provider.mainUpdated) {
      provider.updatedEvent(TAB_HOME);
      _requestRefreshIndex();
    }
    return AnimatedOpacity(
      onEnd: () => setState(() => {_isHide = false}),
      duration: Duration(milliseconds: 500),
      opacity: _isHide ? 1 : 0,
      child: _inLoading
          ? CircleIndicator()
          : HomeView(
              onRefresh: () => _requestRefreshIndex(),
              titles: ["맘대디".tr(), "링크쌤".tr()],
              title: !Commons.isLinkMom() ? "우리아이맡기기_안내".tr() : "동네아이돌보기_안내".tr(),
              type: storageHelper.user_type,
              jobBtnTitle: !Commons.isLinkMom() ? "우리아이맡기기".tr() : "동네아이돌보기".tr(),
              onClickAction: () => null,
              desc: [
                LinkmomHomeBanner(
                  isLinkmom: false,
                  title: "우리아이맡기기_안내".tr(),
                  btnTitle: "우리아이맡기기".tr(),
                  illust: ILLUST_INTRO,
                  onClickAction: () => null,
                  onJoin: () => Commons.penaltyCheck(() {
                    Commons.setModeChange(changeType: USER_TYPE.mom_daddy);
                    Commons.moveApplyPage(context, data, isLinkMomMode: false);
                  }),
                ),
                LinkmomHomeBanner(
                  isLinkmom: true,
                  title: "동네아이돌보기_안내".tr(),
                  btnTitle: "동네아이돌보기".tr(),
                  illust: ILLUST_GO_HOME_SCHOOL,
                  onClickAction: () => null,
                  onJoin: () => Commons.penaltyCheck(() {
                    Commons.setModeChange(changeType: USER_TYPE.link_mom);
                    Commons.moveApplyPage(context, data, isLinkMomMode: true);
                  }),
                ),
              ],
              bottom: Container(
                  width: double.infinity,
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisSize: MainAxisSize.max, children: [
                    DiaryList(
                      data: Commons.isLinkMom() ? auth.indexInfo.linkmominfo.carediary : auth.indexInfo.momdadyinfo.carediary,
                      message: Commons.isLinkMom() ? "홈링크쌤돌봄일기안내".tr() : "홈맘대디돌봄일기안내".tr(),
                      showAll: () {
                        Commons.penaltyCheck(() {
                          if (Commons.isLinkMom()) {
                            Commons.page(context, routeLinkmomDiary, arguments: LinkmomDiaryListPage(data: data));
                          } else {
                            Commons.page(context, routeMomdadyDiary, arguments: MomdadyDiaryListPage(data: data));
                          }
                        }, forceEnable: true);
                      },
                    ),
                    if (Commons.isLinkMom() && auth.indexInfo.linkmominfo.carediary.isNotEmpty)
                      lInkWell(
                          onTap: () => Commons.penaltyCheck(() => Commons.page(context, routeWorkNoti, arguments: WorkNotiPage())),
                          child: Container(
                            margin: EdgeInsets.fromLTRB(20, 10, 20, 30),
                            padding: padding_15_TB,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(16),
                              border: Border.all(color: Commons.getColor()),
                              color: Colors.white,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Lcons.check(color: Commons.getColor(), isEnabled: true),
                                lText("출퇴근알림".tr(), style: st_16(fontWeight: FontWeight.w700, textColor: Commons.getColor())),
                              ],
                            ),
                          )),
                    if (!auth.indexInfo.linkmominfo.carediary.isNotEmpty) sb_h_20,
                    EventViewer(auth.indexInfo.eventBanner),
                    // ranking
                    HomeRankTab(storageHelper.user_type,
                        rankList: [
                          auth.indexInfo.priceRanking.map((e) => e).toList(),
                          auth.indexInfo.likeRanking.map((e) => e).toList(),
                          auth.indexInfo.reviewRanking.map((e) => e).toList(),
                        ],
                        onClick: () => Commons.penaltyCheck(() => Commons.pageToMainTabMove(context, tabIndex: TAB_COMMUNITY, subTabIndex: 1, isMove: true))),
                    HomeCommunityTab(
                        communityList: [
                          auth.indexInfo.momdadyinfo.communityAll,
                        ],
                        onClick: (action) {
                          if (DialogAction.select == action) {
                            Commons.penaltyCheck(() => Commons.pageToMainTabMove(context, tabIndex: TAB_COMMUNITY, subTabIndex: 0, isMove: true));
                          } else if (DialogAction.update == action) {
                            _requestRefreshIndex();
                          }
                        }),
                    Container(
                        padding: padding_20,
                        child: Column(children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              lText("고객센터".tr(), style: st_b_20(fontWeight: FontWeight.w700)),
                              lIconText(
                                title: "전체보기".tr(),
                                icon: Lcons.nav_right(color: color_dedede),
                                textStyle: st_14(textColor: color_999999, fontWeight: FontWeight.w500),
                                isExpanded: false,
                                isLeft: false,
                                onClick: () => Commons.penaltyCheck(() => Commons.page(context, routeCs)),
                              )
                            ],
                          ),
                          Container(
                              padding: padding_20_TB,
                              child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                                CsComponent(
                                  title: "돌봄이용수칙".tr(),
                                  icon: Lcons.guide(color: Commons.getColor()),
                                  onClickAction: () => {Commons.penaltyCheck(() => Commons.page(context, routeCsGuide))},
                                  isLinkmom: Commons.isLinkMom(),
                                ),
                                CsComponent(
                                  title: "자주묻는질문".tr(),
                                  icon: Lcons.qna(color: Commons.getColor()),
                                  onClickAction: () => {Commons.penaltyCheck(() => Commons.page(context, routeCsQna))},
                                  isLinkmom: Commons.isLinkMom(),
                                ),
                                CsComponent(
                                  title: "일대일문의".tr(),
                                  icon: Lcons.contact(color: Commons.getColor()),
                                  onClickAction: () => {Commons.penaltyCheck(() => Commons.page(context, routeCsContact))},
                                  isLinkmom: Commons.isLinkMom(),
                                ),
                              ]))
                        ])),
                    // NOTE: another event viewer
                    EventViewer(auth.indexInfo.adBanner, ratio: 335 / 84),
                    sb_h_15,
                  ])),
            ),
    );
  }

  void _requestIndex() {
    try {
      apiHelper.requestIndex().then((response) async {
        _inLoading = false;
        if (response.getCode() == KEY_SUCCESS) {
          auth.setIndexInfo = response.getData();
          if (auth.indexInfo.userinfo.firstName.isNotEmpty) {
            storageHelper.setMomdady(auth.indexInfo.userinfo.isUserMomdady);
            storageHelper.setLinkmom(auth.indexInfo.userinfo.isUserLinkmom);
            auth.user.my_info_data = MyInfoData(
              first_name: auth.indexInfo.userinfo.firstName,
              age: auth.indexInfo.userinfo.age,
              profileimg: auth.indexInfo.userinfo.profileimg,
              is_user_linkmom: auth.indexInfo.userinfo.isUserLinkmom,
              is_user_momdady: auth.indexInfo.userinfo.isUserMomdady,
              gender: auth.indexInfo.userinfo.gender.contains("여자".tr()) ? 1 : 0,
              grade_linkmom_txt: auth.indexInfo.linkmominfo.gradeLinkmom,
              grade_momdady_txt: auth.indexInfo.momdadyinfo.gradeMomdady,
              auth_address: auth.indexInfo.userinfo.authAddress,
              isAuthHomestate: auth.indexInfo.userinfo.isAuthHomestate,
            );
            auth.account.is_auth_email = auth.indexInfo.userinfo.is_auth_email;
          }
          if (auth.indexInfo.userinfo.authAddress != null && !auth.indexInfo.userinfo.authAddress!.is_gpswithaddress && auth.user.my_info_data!.first_name.isNotEmpty) {
            Commons.isGpsWithAddressCheck(context);
          }
        }

        if (!auth.indexInfo.allim) {
          pushProvider.setBadgeNotify(isUpdate: true);
        }

        if (!auth.indexInfo.chat) {
          pushProvider.setBadgeChat(isUpdate: true);
        }

        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestRefreshIndex() {
    try {
      apiHelper.reqeustRefreshIndex().then((response) async {
        _inLoading = false;
        if (response.getCode() == KEY_SUCCESS) {
          auth.indexInfo.momdadyinfo = response.getData().momdadyinfo;
          auth.indexInfo.linkmominfo = response.getData().linkmominfo;
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}
