import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/community_rank_view_request.dart';
import 'package:linkmom/data/network/models/community_rank_view_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/auth_center/auth_center_page.dart';
import 'package:linkmom/view/main/chat/chat_room_page.dart';

class CommunityRankPage extends BaseStateful {
  final int rank;
  final RankType type;
  final int id;
  final int hcode;
  final int scroll;

  CommunityRankPage({required this.rank, required this.type, required this.id, required this.hcode, this.scroll = 0});

  @override
  _CommunityRankPageState createState() => _CommunityRankPageState();
}

class _CommunityRankPageState extends BaseStatefulState<CommunityRankPage> with SingleTickerProviderStateMixin {
  late TabController _reviewCtrl;
  late ScrollController _scrollCtrl;
  late RankData _rank = RankData();
  List<List<RankReviewInfo>> _review = [];

  @override
  void initState() {
    super.initState();
    _reviewCtrl = TabController(length: 3, vsync: this);
    _scrollCtrl = ScrollController();
    _requestRankView();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (widget.scroll > 0) {
        Future.delayed(Duration(seconds: 1), () {
          if (_scrollCtrl.hasClients) {
            _scrollCtrl.animateTo(_scrollCtrl.position.maxScrollExtent, curve: Curves.ease, duration: Duration(milliseconds: 300));
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
          appBar: diaryAppbar(title: widget.type.name, hide: false),
          body: LinkmomRefresh(
            onRefresh: () => _requestRankView(),
            controller: _scrollCtrl,
            padding: padding_0,
            child: flag.isLoading
                ? Center()
                : _rank.reviewinfo == null
                    ? lEmptyView(height: Commons.height(context, 0.7))
                    : Column(children: [
                        Column(children: [
                          Container(
                              padding: padding_20_LTR,
                              child: Column(children: [
                                Row(children: [
                                  _getRankImg(widget.rank),
                                  Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                    lText('${_rank.userinfo!.firstName} ${"링크쌤".tr()}', style: st_b_20(fontWeight: FontWeight.w700)),
                                    sb_h_05,
                                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                      lText(_rank.userinfo!.age, style: st_14(textColor: color_545454)),
                                      Container(padding: padding_05_LR, height: 15, child: lDividerVertical(thickness: 1, color: color_dedede)),
                                      lText(_rank.userinfo!.gender, style: st_14(textColor: color_545454)),
                                      Container(padding: padding_05_LR, height: 15, child: lDividerVertical(thickness: 1, color: color_dedede)),
                                      lText(_rank.userinfo!.region3Depth, style: st_14(textColor: color_545454)),
                                    ]),
                                  ]),
                                ]),
                              ])),
                          Container(
                              margin: padding_20,
                              padding: padding_20,
                              decoration: BoxDecoration(
                                color: color_dedede.withAlpha(56),
                                borderRadius: BorderRadius.circular(16),
                              ),
                              child: Container(
                                  child: Column(
                                children: [
                                  Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
                                    _getRankImg(widget.rank, width: 35),
                                    sb_w_10,
                                    Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                      lText("${widget.type.name} ${widget.rank}${"위".tr()}", style: st_16(textColor: widget.rank < 4 ? color_ffbb56 : color_222222, fontWeight: FontWeight.w700)),
                                      TextHighlight(
                                        text: "최근활동집계정보".tr(),
                                        term: "삼십일간".tr(),
                                        textStyle: st_13(textColor: color_999999, fontWeight: FontWeight.w500),
                                        textStyleHighlight: TextStyle(decoration: TextDecoration.underline, fontWeight: FontWeight.w500, fontSize: 13, color: color_999999),
                                      )
                                    ])
                                  ]),
                                  Padding(padding: padding_15_TB, child: Divider(color: color_dedede)),
                                  Row(children: [
                                    Lcons.total_income(size: 24),
                                    sb_w_10,
                                    lText("돌봄누적소득".tr(), style: st_15(textColor: color_999999)),
                                    sb_w_10,
                                    TextHighlight(
                                      text: '${StringUtils.formatPay(_rank.rankinginfo!.priceSum)}${"원".tr()}',
                                      term: "원".tr(),
                                      textStyle: st_b_16(fontWeight: FontWeight.w700),
                                      textStyleHighlight: st_b_15(),
                                    )
                                  ]),
                                  sb_h_05,
                                  Row(children: [
                                    Lcons.review(size: 24),
                                    sb_w_10,
                                    lText("후기".tr(), style: st_15(textColor: color_999999)),
                                    sb_w_10,
                                    TextHighlight(
                                      text: '${_rank.rankinginfo!.reviewCnt}${"개".tr()}',
                                      term: "개".tr(),
                                      textStyle: st_b_16(fontWeight: FontWeight.w700),
                                      textStyleHighlight: st_b_15(),
                                    )
                                  ]),
                                  sb_h_05,
                                  Row(children: [
                                    Lcons.like(color: color_222222),
                                    sb_w_10,
                                    lText("좋아요".tr(), style: st_15(textColor: color_999999)),
                                    sb_w_10,
                                    TextHighlight(
                                      text: '${_rank.rankinginfo!.likeCnt}${"개".tr()}',
                                      term: "개".tr(),
                                      textStyle: st_b_16(fontWeight: FontWeight.w700),
                                      textStyleHighlight: st_b_15(),
                                    ),
                                  ]),
                                ],
                              ))),
                        ]),
                        lDivider(color: color_eeeeee, thickness: 8, padding: padding_20_T),
                        Container(
                            padding: padding_20,
                            child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: [
                              Container(
                                  width: double.infinity,
                                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.start, children: [
                                    if (_rank.careinfo!.servicetype != null) Padding(padding: padding_20_TB, child: lText("돌봄유형".tr(), style: st_b_15())),
                                    if (_rank.careinfo!.servicetype != null)
                                      Container(
                                          alignment: Alignment.topLeft,
                                          width: 230,
                                          child: GridView.builder(
                                              padding: padding_0,
                                              shrinkWrap: true,
                                              itemCount: _rank.careinfo?.servicetype?.length ?? 03,
                                              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: 2),
                                              physics: NeverScrollableScrollPhysics(),
                                              itemBuilder: (ctx, idx) {
                                                return Container(
                                                  child: Row(children: [
                                                    Lcons.serviceType(EnumUtils.getServiceType(name: _rank.careinfo!.servicetype![idx]), color: Commons.getColor(), size: 40, isEnabled: true),
                                                    sb_w_10,
                                                    lText(_rank.careinfo!.servicetype![idx], style: st_15(textColor: Commons.getColor(), fontWeight: FontWeight.w500))
                                                  ]),
                                                );
                                              })),
                                  ])),
                              Container(
                                  height: 110,
                                  margin: padding_20_T,
                                  padding: padding_20,
                                  decoration: BoxDecoration(
                                    color: color_dedede.withAlpha(56),
                                    borderRadius: BorderRadius.circular(16),
                                  ),
                                  child: Container(
                                    child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, crossAxisAlignment: CrossAxisAlignment.center, children: [
                                      Container(
                                          child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                        lText("총돌봄시간".tr(), style: st_13(textColor: color_999999)),
                                        sb_h_06,
                                        lText('${_rank.careinfo?.totalCaretime ?? 0}', style: st_b_18(fontWeight: FontWeight.w700)),
                                        lText("시간".tr(), style: st_b_13()),
                                      ])),
                                      Container(
                                          padding: padding_25_LR,
                                          decoration: BoxDecoration(border: Border(left: BorderSide(color: color_dedede), right: BorderSide(color: color_dedede))),
                                          child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                            lText("링크쌤등급".tr(), style: st_13(textColor: color_999999)),
                                            sb_h_06,
                                            lText('${_rank.careinfo?.gradeLinkmom ?? ' '}', style: st_b_15(fontWeight: FontWeight.w700)),
                                            sb_h_10,
                                          ])),
                                      lInkWell(
                                        onTap: () => Commons.page(context, routeAuthCenter, arguments: AuthCenterPage(id: _rank.careinfo!.authId, viewType: ViewType.view)),
                                        child: Container(
                                            child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                          lText("인증배지개수".tr(), style: st_13(textColor: color_999999)),
                                          sb_h_06,
                                          lText('${_rank.careinfo?.gradeAuthlevel ?? 0}', style: st_b_18(fontWeight: FontWeight.w700)),
                                          lText("배지".tr(), style: st_b_13()),
                                        ])),
                                      ),
                                    ]),
                                  )),
                              Container(
                                  padding: padding_20_T,
                                  alignment: Alignment.centerLeft,
                                  child: Wrap(
                                      spacing: 10,
                                      runSpacing: 10,
                                      children: _getAuthInfo(_rank.careinfo!)
                                          .map((e) => lTextBtn(
                                                '#${e.name}',
                                                bg: Colors.white,
                                                radius: 16,
                                                border: Border.all(color: Commons.isLinkMom() ? color_a485ad : color_80ada7),
                                                style: st_12(textColor: Commons.isLinkMom() ? color_a485ad : color_80ada7, fontWeight: FontWeight.w700),
                                              ))
                                          .toList())),
                            ])),
                        if (_review.isNotEmpty) lDivider(color: color_dedede, padding: padding_20),
                        if (_review.isNotEmpty)
                          Container(
                            margin: padding_10_TB,
                            height: data.height(context, 0.5),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(padding: padding_25_LR, child: lText("총후기".tr(), style: st_b_18(fontWeight: FontWeight.w700))),
                                Container(
                                  margin: padding_10,
                                  width: Commons.width(context, 0.5),
                                  height: kTextTabBarHeight - 25,
                                  child: TabBar(
                                    controller: _reviewCtrl,
                                    tabs: [
                                      Tab(text: "등원".tr()),
                                      Tab(text: "하원".tr()),
                                      Tab(text: "보육".tr()),
                                    ],
                                    indicator: BoxDecoration(border: Border(bottom: BorderSide(color: Commons.getColor(), width: 2))),
                                    indicatorSize: TabBarIndicatorSize.label,
                                    labelStyle: st_16(fontWeight: FontWeight.w700),
                                    labelColor: Commons.getColor(),
                                    unselectedLabelColor: color_dedede,
                                    unselectedLabelStyle: st_16(fontWeight: FontWeight.w700),
                                  ),
                                ),
                                Expanded(
                                    child: Container(
                                        padding: padding_20_LR,
                                        child: flag.isLoading
                                            ? Center()
                                            : TabBarView(
                                                controller: _reviewCtrl,
                                                children: _review
                                                    .map((e) => e.isNotEmpty
                                                        ? Container(
                                                            child: Wrap(
                                                                children: e
                                                                    .map((r) => r.count > 0
                                                                        ? Row(
                                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                                            children: [
                                                                              Container(
                                                                                  padding: padding_10_R,
                                                                                  child: Stack(
                                                                                    alignment: Alignment.center,
                                                                                    children: [Lcons.review_cnt_bg(isEnabled: Commons.isLinkMom(), disableColor: color_e4f1f0, color: color_b579c8_op10), lText('${r.count}', style: st_b_14(fontWeight: FontWeight.w700))],
                                                                                  )),
                                                                              lText("${r.reviewText}", style: st_b_14()),
                                                                            ],
                                                                          )
                                                                        : Container())
                                                                    .toList()))
                                                        : lEmptyView(title: "작성된후기가없습니다".tr()))
                                                    .toList())))
                              ],
                            ),
                          ),
                        if (_rank.userinfo != null && _rank.userinfo!.userId != auth.user.id) lBtn("채팅하기".tr(), onClickAction: () => Commons.page(context, routeChatRoom, arguments: ChatRoomPage(receiver: _rank.userinfo!.userId, roomType: ChatRoomType.CREATE))),
                      ]),
          )),
      isOnlyLoading: true,
    );
  }

  Widget _getRankImg(int rank, {double width = 0.0}) {
    switch (rank) {
      case 1:
        return Image.asset(IMG_RANK_1, width: width);
      case 2:
        return Image.asset(IMG_RANK_2, width: width);
      case 3:
        return Image.asset(IMG_RANK_3, width: width);
      default:
        return Container();
    }
  }

  void _requestRankView() {
    try {
      flag.enableLoading(fn: () => onUpDate());
      CommunityRankViewRequest req = CommunityRankViewRequest(user_id: widget.id, gubun: widget.type.index, hcode: widget.hcode);
      apiHelper.reqeustCommunityRankView(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _rank = response.getData();
          _review = List.generate(3, (index) => []);
          if (response.getData().reviewinfo != null)
            response.getData().reviewinfo!.forEach((e) {
              if (e.review < 10) {
                _review[ServiceType.serviceType_0.value].add(e);
              } else if (e.review > 10 && e.review < 20) {
                _review[ServiceType.serviceType_1.value].add(e);
              } else {
                _review[ServiceType.serviceType_2.value].add(e);
              }
            });
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  static List<SingleItem> _getAuthInfo(Careinfo info) {
    List<SingleItem> items = [];
    if (info.hashtagCctv) items.add(SingleItem("CCTV있음".tr()));
    if (info.hashtagAnmal) items.add(SingleItem("반려동물있음".tr()));
    if (info.hashtagCodivVaccine) items.add(SingleItem("코로나백신".tr()));
    if (info.hashtagDeungbon) items.add(SingleItem("등본인증".tr()));
    if (info.hashtagCriminal) items.add(SingleItem("인증센터_범죄인증".tr()));
    if (info.hashtagPersonality) items.add(SingleItem("인증센터_인적성".tr()));
    if (info.hashtagHealthy) items.add(SingleItem("인증센터_건강인증".tr()));
    if (info.hashtagCareer2) items.add(SingleItem("보육교사".tr()));
    if (info.hashtagEducation) items.add(SingleItem("인증센터_교육인증".tr()));
    if (info.hashtagCareer1) items.add(SingleItem("인증센터_경력인증".tr()));
    if (info.hashtagGraduated) items.add(SingleItem("인증센터_학력인증".tr()));
    if (info.hashtagBabysiter) items.add(SingleItem("인증센터_교육인증".tr()));
    return items;
  }
}
