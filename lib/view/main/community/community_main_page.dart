import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/community_list_response.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/home_provider.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:provider/provider.dart';

import '../../../main.dart';
import 'community_list_view.dart';
import 'community_event_view.dart';
import 'community_rank_view.dart';

class CommunityMainPage extends BaseStateful {
  final ViewType viewType;
  final int? tabIndex;

  CommunityMainPage({this.viewType = ViewType.normal, this.tabIndex});

  @override
  _CommunityMainPageState createState() => _CommunityMainPageState();
}

class _CommunityMainPageState extends BaseStatefulState<CommunityMainPage> with TickerProviderStateMixin {
  late TabController _communityTab;
  int _initTab = 0;
  Region _all = Region(region2Depth: "전국".tr(), region3Depth: "전국".tr(), hcode: 0);
  List<Region> _regions = [];

  @override
  void initState() {
    super.initState();
    _regions.add(_all);
    _communityTab = TabController(length: 3, vsync: this);
    if (auth.user.username.isNotEmpty) {
      flag.enableLoading(fn: () => onUpDate());
    }
  }

  @override
  Widget build(BuildContext context) {
    var _provider = Provider.of<HomeNavigationProvider>(context, listen: true);
    if (_initTab != _provider.subIndex) {
      _initTab = _provider.subIndex;
      _provider.clearSubIndex();
    }
    return lScaffold(
      body: Container(
        child: TabPageView(
          onCreateView: (ctrl) {
            if (_communityTab != ctrl) {
              _communityTab = ctrl;
            }
            if (_initTab != _communityTab.index) {
              _communityTab.animateTo(_initTab);
              _initTab = 0;
            }
          },
          tabbarName: [
            TabItem(title: "소통나눔".tr()),
            TabItem(title: "링크쌤랭킹".tr()),
            TabItem(title: "이벤트".tr()),
          ],
          tabbarView: [
            // NOTE: community
            CommunityListView(needUpdate: false),
            // NOTE: ranking Tap
            CommunityRankView(data: data),
            // NOTE: event Tap
            CommunityEventView(),
          ],
        ),
      ),
    );
  }
}
