import 'dart:async';
import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/api_header.dart';
import 'package:linkmom/data/network/models/block_requests.dart';
import 'package:linkmom/data/network/models/community_delete_request.dart';
import 'package:linkmom/data/network/models/community_event_list_response.dart';
import 'package:linkmom/data/network/models/community_event_reply_save_request.dart';
import 'package:linkmom/data/network/models/community_event_view_request.dart';
import 'package:linkmom/data/network/models/community_event_view_response.dart';
import 'package:linkmom/utils/route_action.dart';
import 'package:linkmom/view/link_page.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/board/board_view.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/notify/community_notify_page.dart';
import 'package:share_plus/share_plus.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CommunityEventPage extends BaseStateful {
  final int id;
  final String title;
  final String fromTo;
  final EventInfo? event;
  final String url;

  CommunityEventPage({required this.id, this.title = '', this.fromTo = '', this.event, this.url = ''});

  @override
  _CommunityEventPageState createState() => _CommunityEventPageState();
}

class _CommunityEventPageState extends BaseStatefulState<CommunityEventPage> {
  EventData _event = EventData(results: []);
  final Set<Factory<OneSequenceGestureRecognizer>>? gestureRecognizers = [Factory(() => VerticalDragGestureRecognizer())].toSet();
  WebViewController? _ctrl;
  double _height = 350;
  bool _webviewEnable = false;
  Set<JavascriptChannel> _channel = Set();
  String? _url;

  @override
  void initState() {
    super.initState();
    deeplinkAction = null;
    _initJsChannel();
    _webviewEnable = widget.event != null ? widget.event!.webviewUrl.isNotEmpty : false;
    _requestEventView(widget.id);
    log.d({'params': widget.url});
    _url = _validUrl(widget.url);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _height = Commons.height(context, 1) - (_event.results != null && _event.results!.isNotEmpty ? (kBottomNavigationBarHeight * 3) : 90);
      onUpDate();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        appBar: diaryAppbar(title: "이벤트".tr(), hide: false, ctx: context),
        body: lModalProgressHUD(
          flag.isLoading,
          _event.content.isNotEmpty
              ? BoardViewer(
                  onRefresh: () => _requestEventView(widget.id),
                  onNextReply: () => _requestEventView(widget.id, next: true),
                  physics: ClampingScrollPhysics(),
                  padding: padding_0,
                  topper: _webviewEnable
                      ? null
                      : Column(mainAxisSize: MainAxisSize.max, children: [
                          Container(
                            padding: padding_30,
                            child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
                              // NOTE: If using html
                              _event.title.contains('</p>')
                                  ? Html(data: _event.title, style: {
                                      "p": Style(
                                        textDecorationColor: color_222222,
                                        fontWeight: FontWeight.w500,
                                        fontSize: FontSize(20),
                                        textAlign: TextAlign.center,
                                      ),
                                    })
                                  : lText(_event.title, style: st_b_20(fontWeight: FontWeight.w500)),
                              sb_h_15,
                              lText('${StringUtils.parseDotYMD(_event.startDate)} ~ ${StringUtils.parseDotYMD(_event.endDate)}', style: st_14(textColor: color_999999)),
                            ]),
                          ),
                          lDivider(thickness: 8, color: color_eeeeee)
                        ]),
                  body: _webviewEnable
                      ? Container(
                          padding: padding_20_B,
                          width: double.infinity,
                          height: _height,
                          child: WebView(
                            debuggingEnabled: Commons.isDebugMode,
                            initialCookies: [
                              WebViewCookie(
                                name: "isApp",
                                value: "Y",
                                domain: _concatUrl(_url ?? _event.webviewUrl, widget.url),
                              ),
                            ],
                            gestureRecognizers: gestureRecognizers,
                            initialUrl: _concatUrl(_url ?? _event.webviewUrl, widget.url),
                            javascriptMode: JavascriptMode.unrestricted,
                            javascriptChannels: _channel,
                            onWebViewCreated: (ctrl) async {
                              _ctrl = ctrl;
                              _ctrl!.loadUrl(
                                _concatUrl(_url ?? _event.webviewUrl, widget.url),
                                headers: {
                                  HEADER_AUTHORIZATION: apiHeader.publicApiHeader[HEADER_AUTHORIZATION] ?? '',
                                  HEADER_API_TOKEN_CSRF: apiHeader.publicApiHeader[HEADER_API_TOKEN_CSRF] ?? '',
                                  HEADER_KEY_COOKIE: 'isApp="Y"',
                                },
                              );
                              log.d({
                                "log": "",
                                HEADER_KEY_COOKIE: 'isApp="Y"',
                                HEADER_AUTHORIZATION: apiHeader.publicApiHeader[HEADER_AUTHORIZATION] ?? '',
                                HEADER_API_TOKEN_CSRF: apiHeader.publicApiHeader[HEADER_API_TOKEN_CSRF] ?? '',
                              });
                            },
                            onPageFinished: (url) async {
                              log.d({'load url': url});
                            },
                          ),
                        )
                      : Center(
                          child: Html(
                          data: _event.content,
                          customRenders: {
                            networkSourceMatcher(): networkImageRender(
                              loadingWidget: () => LoadingIndicator(
                                showText: false,
                                background: color_transparent,
                              ),
                            ),
                          },
                        )),
                  footer: Container(),
                  showReply: widget.event != null ? widget.event!.isReply : _event.results != null,
                  replyLength: _event.results != null && _event.results!.isNotEmpty ? _event.results!.length : 0,
                  reply: _event.results != null && _event.results!.isNotEmpty
                      ? _event.results!
                          .map((e) => ReplyData(
                                id: e.id,
                                parent: e.parent,
                                writer: e.writerNickname,
                                message: e.reply,
                                profile: e.profileimg,
                                isDeleted: e.isDelete == 1,
                                level: e.level,
                                writtenTime: StringUtils.parseYMDHM(e.updatedate),
                                replyMenu: {
                                  if (e.writerId == auth.user.id) lText("삭제하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _requestReplyDelete(widget.id, e.id),
                                  if (e.writerId != auth.user.id) lText("신고하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _moveNofity(e.id, e.writerId, e.writerNickname),
                                  if (e.writerId != auth.user.id) lText("신고하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _blockUser(e.writerNickname, e.writerId)
                                },
                              ))
                          .toList()
                      : [],
                  replyPadding: padding_20_LRB,
                  saveReplyAction: (parent, text) {
                    if (auth.getMyInfoData.first_name.isNotEmpty) {
                      _requestReplySave(widget.id, text, parent);
                    } else {
                      showNormalDlg(message: "로그인해주세요".tr(), onClickAction: (a) => {if (a == DialogAction.yes) Commons.pageClear(context, routeLogin)});
                    }
                  },
                  dock: Lcons.nav_bottom(),
                )
              : Container(),
        ));
  }

  void _requestEventView(int id, {bool next = false}) {
    try {
      if (next && _event.next.isEmpty) return;
      flag.enableLoading(fn: () => onUpDate());
      CommnunityEventViewRequest req = CommnunityEventViewRequest(id: id);
      apiHelper.reqeustCommunityEventView(req, url: next ? _event.next : null).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          if (next) {
            _event.results!.addAll(response.getData().results!);
            _event.next = response.getData().next;
            _event.previous = response.getData().previous;
          } else {
            _event = response.getData();
            _webviewEnable = _event.webviewUrl.isNotEmpty;
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  Future<void> _requestReplySave(int id, String text, int parent, {bool retry = true}) async {
    try {
      CommunityEventReplySaveRequest req = CommunityEventReplySaveRequest(eventId: id, reply: text, parent: parent);
      var response = await apiHelper.reqeustCommunityEventReplySave(req);
      if (response.getCode() == KEY_SUCCESS) {
        _event.count++;
        _event.results!.add(EventReply(
          reply: text,
          id: response.getData().id,
          profileimg: auth.getMyInfoData.profileimg,
          updatedate: StringUtils.formatYMDHM(DateTime.now()),
          parent: parent,
          writerNickname: response.getData().writerNickname,
          level: parent,
        ));
      } else {
        if (retry) _requestReplySave(id, text, parent, retry: false);
      }
      flag.disableLoading(fn: () => onUpDate());
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestReplyDelete(int eventId, int id) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      CommunityDeleteRequest req = CommunityDeleteRequest(id: id);
      apiHelper.reqeustCommunityEventReplyDel(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _requestEventView(eventId);
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestUserBlock(int userId) {
    try {
      BlockSaveRequest req = BlockSaveRequest(block_user: userId);
      apiHelper.requestBlockSave(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.showToast(response.getMsg());
        }
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _moveNofity(int replyId, int userId, String? userName) {
    Commons.page(context, routeCommunityNotify, arguments: CommunityNotifyPage(userId: userId, boardId: 0, eventReplyId: replyId, userName: userName));
  }

  void _initJsChannel() {
    _channel.add(JavascriptChannel(
      name: 'onClick',
      onMessageReceived: (JavascriptMessage message) {
        log.d({'onClick channel': 'onClick ${message.message}'});
        Uri uri = Uri.parse(message.message);
        String url = uri.queryParameters['link'] ?? message.message.toString();
        if (url.isNotEmpty) RouteAction.onLink(context, url);
      },
    ));
    _channel.add(JavascriptChannel(
      name: 'alert',
      onMessageReceived: (JavascriptMessage message) {
        log.d({'alert channel': '${message.message}'});
        showNormalDlg(message: message.message);
      },
    ));
    _channel.add(JavascriptChannel(
      name: 'close',
      onMessageReceived: (JavascriptMessage message) {
        log.d({'close channel': '${message.message}'});
        if (Commons.isLogin) {
          Commons.pageToMain(context, routeHome);
        } else {
          Commons.pageClear(context, routeLogin);
        }
      },
    ));
    _channel.add(JavascriptChannel(
      name: 'share',
      onMessageReceived: (JavascriptMessage jsMsg) async {
        log.d({'share channel': '${jsMsg.message}'});
        Map shareData = json.decode(jsMsg.message);
        if (shareData.isNotEmpty) await Share.share(shareData['url'], subject: shareData['subject']);
      },
    ));
    _channel.add(JavascriptChannel(
      name: 'toast',
      onMessageReceived: (JavascriptMessage jsMsg) async {
        log.d({'toast channel': '${jsMsg.message}'});
        Commons.showToast(jsMsg.message);
      },
    ));
  }

  String? _validUrl(String url) {
    if (url.isEmpty || !url.startsWith('http')) {
      return null;
    } else {
      return url;
    }
  }

  String _concatUrl(String eventUrl, String params) {
    int i = params.indexOf("?");
    if (eventUrl == params) return eventUrl;
    if (i > 0)
      return eventUrl + params.substring(i, params.length);
    else
      return eventUrl;
  }

  void _blockUser(String name, int id) {
    showNormalDlg(message: name + "차단하시겠습니까".tr(), btnLeft: "취소".tr(), onClickAction: (a) => {if (a == DialogAction.yes) _requestUserBlock(id)});
  }
}
