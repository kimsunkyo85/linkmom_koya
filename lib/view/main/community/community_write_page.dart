import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/community_list_response.dart';
import 'package:linkmom/data/network/models/community_save_request.dart';
import 'package:linkmom/data/network/models/community_update_request.dart';
import 'package:linkmom/data/network/models/community_view_request.dart';
import 'package:linkmom/data/providers/image_loader_provider.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/board/board_view.dart';
import 'package:linkmom/view/image/image_add_view.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:provider/provider.dart';

class CommunityWritePage extends BaseStateful {
  final int id;
  final String? title;
  final String? body;
  final List<String>? images;
  final Category? category;
  final int? status;
  final bool reload;
  final List<Region>? region;

  CommunityWritePage({this.id = 0, this.title, this.body, this.images, this.category, this.status, this.reload = false, this.region});

  @override
  _CommunityWritePageState createState() => _CommunityWritePageState();
}

class _CommunityWritePageState extends BaseStatefulState<CommunityWritePage> {
  Category _currentType = Category.careComm;
  Region? _currentRegion;
  TextEditingController _titleCtrl = TextEditingController();
  TextEditingController _bodyCtrl = TextEditingController();
  final _imageLength = 5;
  FocusNode _bodyFocus = FocusNode();
  FocusNode _titleFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    if (widget.title != null) _titleCtrl.text = widget.title!;
    if (widget.category != null) _currentType = widget.category!;
    if (widget.reload) {
      _requestCommunityView(widget.id);
    } else {
      if (widget.body != null) _bodyCtrl.text = widget.body!;
    }

    context.read<ImageLoader>().loadImage([], context);

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      context.read<ImageLoader>().setUpdateCallback = onUpDate;
      _loadImages(widget.images ?? []);
      if (widget.region != null) _currentRegion = widget.region![1];
      _titleFocus.addListener(() => onUpDate());
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          _pagePop();
          return Future.value(false);
        },
        child: lModalProgressHUD(
          flag.isLoading,
          lScaffold(
            context: context,
            appBar: diaryAppbar(
              title: "글쓰기".tr(),
              hide: false,
              rightW: lTextBtn(
                "등록하기",
                onClickEvent: () => _requestSave(),
                bg: Colors.transparent,
                style: st_15(fontWeight: FontWeight.w500, textColor: Commons.getColor()),
              ),
              onBack: () => _pagePop(),
            ),
            resizeToAvoidBottomPadding: false,
            body: Container(
                child: Column(
              children: [
                Expanded(
                  child: lScrollView(
                    padding: padding_20_LTR,
                    child: BoardWriteView(
                      controller: _bodyCtrl,
                      padding: padding_0,
                      showCount: false,
                      topper: Column(
                        children: [
                          Row(children: [
                            lIconText(
                              title: _currentType.name,
                              textStyle: st_b_15(fontWeight: FontWeight.w500),
                              padding: padding_10_R,
                              icon: Lcons.nav_bottom(),
                              isExpanded: false,
                              isLeft: false,
                              onClick: () {
                                showModalBottomSheet(
                                    context: context,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
                                    ),
                                    builder: (ctx) {
                                      return lScrollView(
                                          child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: padding_30_TB,
                                            child: lText("카테고리를선택해주세요".tr(), style: st_b_18(fontWeight: FontWeight.bold)),
                                          ),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: Category.values
                                                .map((e) => lInkWell(
                                                      onTap: () {
                                                        focusClear(context);
                                                        _currentType = e;
                                                        onUpDate();
                                                        Commons.pagePop(context);
                                                      },
                                                      child: Padding(
                                                        padding: padding_25_B,
                                                        child: lText(e.name, style: st_b_16()),
                                                      ),
                                                    ))
                                                .toList(),
                                          ),
                                        ],
                                      ));
                                    });
                              },
                            ),
                            sb_w_10,
                            if (widget.region != null)
                              Row(
                                children: [
                                  Lcons.location(),
                                  sb_w_03,
                                  DropdownButton<Region>(
                                    value: _currentRegion,
                                    onChanged: (value) {
                                      _currentRegion = value;
                                      focusClear(context);
                                      onUpDate();
                                    },
                                    icon: Lcons.nav_bottom(),
                                    hint: lText(widget.region![1].region2Depth, style: st_b_15(fontWeight: FontWeight.w500)),
                                    underline: Container(),
                                    items: widget.region!
                                        .sublist(1)
                                        .map((e) => DropdownMenuItem<Region>(
                                            value: e,
                                            child: lText(
                                              e.region2Depth,
                                              style: st_b_15(fontWeight: FontWeight.w500),
                                            )))
                                        .toList(),
                                  ),
                                ],
                              ),
                          ]),
                          TextField(
                            controller: _titleCtrl,
                            style: st_b_18(fontWeight: FontWeight.w700),
                            focusNode: _titleFocus,
                            decoration: InputDecoration(
                              hintText: "제목".tr(),
                              hintStyle: st_18(textColor: color_b2b2b2, fontWeight: FontWeight.w700),
                              border: InputBorder.none,
                            ),
                            onChanged: (value) => onConfirmBtn(),
                          ),
                          lDivider(color: color_eeeeee),
                        ],
                      ),
                      hintWidget: Container(
                        padding: padding_20_TB,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            lText(_currentType.hint, style: st_15(textColor: color_b2b2b2)),
                            sb_h_30,
                            Container(
                              padding: padding_20,
                              decoration: BoxDecoration(
                                color: color_f5f5f5,
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Lcons.info_t(disableColor: color_999999),
                                      sb_w_05,
                                      Flexible(
                                        child: lText("커뮤니티_위반안내_1".tr(), style: st_13(textColor: color_999999)),
                                      )
                                    ],
                                  ),
                                  sb_h_10,
                                  lText(
                                    "커뮤니티_위반안내_2".tr(),
                                    style: st_13(textColor: color_b2b2b2),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      update: () => onConfirmBtn(),
                      bodyLength: null,
                      useImage: false,
                      onCreate: (focus) {
                        _bodyFocus = focus;
                        _bodyFocus.addListener(() => onUpDate());
                      },
                    ),
                  ),
                ),
                Column(
                  children: [
                    ChangeNotifierProvider<ImageLoader>(
                      create: (_) => ImageLoader(),
                      builder: (ctx, child) => Container(
                        margin: EdgeInsets.fromLTRB(20, 0, 0, 15),
                        child: ImageAddView(
                          max: _imageLength,
                          data: context.read<ImageLoader>().getImages,
                          onItemClick: (image, index) {
                            context.read<ImageLoader>().setImages = image;
                          },
                        ),
                      ),
                    ),
                    if (_bodyFocus.hasFocus || _titleFocus.hasFocus)
                      Container(
                        decoration: BoxDecoration(border: Border(top: BorderSide(color: color_eeeeee))),
                        alignment: Alignment.centerRight,
                        height: 40,
                        padding: padding_10_R,
                        child: lInkWell(
                          onTap: () => focusClear(context),
                          child: Lcons.key_down(size: 30),
                        ),
                      ),
                  ],
                ),
              ],
            )),
          ),
        ));
  }

  void _requestSave() {
    if (_titleCtrl.text.isEmpty || _bodyCtrl.text.isEmpty) {
      showNormalDlg(context: context, message: "내용이비어있습니다".tr());
    } else {
      try {
        flag.enableLoading(fn: () => onUpDate());
        if (widget.title != null) {
          CommunityUpdateRequest req = CommunityUpdateRequest(
            id: widget.id,
            title: _titleCtrl.text,
            category: _currentType.index,
            content: _bodyCtrl.text,
            image: context.read<ImageLoader>().getImages,
            shareStatus: widget.status ?? 0,
          );
          apiHelper.reqeustCommunityUpdate(req).then((response) {
            if (response.getCode() == KEY_SUCCESS) {
              Commons.pagePop(context, data: response.getData());
            } else {
              showNormalDlg(context: context, message: "커뮤니티_작성_에러".tr());
            }
            flag.disableLoading(fn: () => onUpDate());
          }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
        } else {
          CommunitySaveRequest req = CommunitySaveRequest(
            title: _titleCtrl.text,
            category: _currentType.index,
            content: _bodyCtrl.text,
            image: context.read<ImageLoader>().getImages,
            hcode: _currentRegion == null ? widget.region!.first.hcode : _currentRegion!.hcode,
          );
          apiHelper.reqeustCommunitySave(req).then((response) {
            if (response.getCode() == KEY_SUCCESS) {
              Commons.pagePop(context, data: response.getData());
            } else {
              showNormalDlg(context: context, message: "커뮤니티_작성_에러".tr());
            }
            flag.disableLoading(fn: () => onUpDate());
          }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
        }
      } catch (e) {
        flag.disableLoading(fn: () => onUpDate());
      }
    }
  }

  void _loadImages(List<String> urls) {
    context.read<ImageLoader>().loadImage(urls, context);
  }

  @override
  void onConfirmBtn() {
    super.onConfirmBtn();
    if (_titleCtrl.text.isNotEmpty || _bodyCtrl.text.isNotEmpty) {
      flag.enableConfirm(fn: onUpDate);
    } else {
      flag.disableConfirm(fn: onUpDate);
    }
  }

  void _requestCommunityView(int id) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      CommunityListViewRequest req = CommunityListViewRequest(id: id);
      apiHelper.reqeustCommunityView(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _bodyCtrl.text = response.getData().communityShare.content;
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _pagePop() {
    if (_bodyCtrl.text.isNotEmpty) {
      showNormalDlg(
        message: "커뮤니티_취소".tr(),
        btnLeft: "네".tr(),
        btnRight: "아니오".tr(),
        onClickAction: (a) => {if (a != DialogAction.yes) Commons.pagePop(context)},
      );
    } else {
      Commons.pagePop(context);
    }
  }
}
