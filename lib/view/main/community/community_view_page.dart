import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/block_requests.dart';
import 'package:linkmom/data/network/models/community_delete_request.dart';
import 'package:linkmom/data/network/models/community_like_request.dart';
import 'package:linkmom/data/network/models/community_list_response.dart';
import 'package:linkmom/data/network/models/community_reply_save_request.dart';
import 'package:linkmom/data/network/models/community_scrap_request.dart';
import 'package:linkmom/data/network/models/community_update_request.dart';
import 'package:linkmom/data/network/models/community_view_request.dart';
import 'package:linkmom/data/network/models/community_view_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/board/board_view.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/chat/chat_room_page.dart';
import 'package:linkmom/view/main/community/community_write_page.dart';
import 'package:linkmom/view/main/notify/community_notify_page.dart';

class CommunityViewPage extends BaseStateful {
  final int id;
  final bool toReply;

  CommunityViewPage({this.id = 0, this.toReply = false});

  @override
  _CommunityViewPageState createState() => _CommunityViewPageState();
}

class _CommunityViewPageState extends BaseStatefulState<CommunityViewPage> {
  int responseCode = 0;
  CommunityViewData _viewDetail = CommunityViewData(communityShare: CommunityShare(), communityShareImages: [], results: [], userinfo: CommunityUserInfo());
  GlobalKey _replyView = GlobalKey();
  bool _needUpdate = false;

  @override
  void initState() {
    super.initState();
    flag.enableLoading(fn: () => onUpDate());
    _requestCommunityView(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Commons.pagePop(context, data: _needUpdate ? _viewDetail : null);
        return Future.value(false);
      },
      child: lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            context: context,
            appBar: appBar(_viewDetail.communityShare.category.isNotEmpty ? _viewDetail.communityShare.category : "커뮤니티".tr(), hide: false, onBack: () => Commons.pagePop(context, data: _needUpdate ? _viewDetail : null)),
            body: responseCode == KEY_4100_DATA_DELETE
                ? lEmptyView(height: Commons.height(context, 0.4), title: "해당글이삭제되었습니다".tr())
                : BoardViewer(
                    toReply: widget.toReply,
                    onRefresh: () => _requestCommunityView(widget.id),
                    onNextReq: () => _requestCommunityView(widget.id, isNext: true),
                    profile: BoardProfile(
                      author: _viewDetail.userinfo.nickname,
                      writtenTime: _viewDetail.communityShare.updatedate,
                      profileImg: _viewDetail.userinfo.profileimg,
                      location: _viewDetail.userinfo.region3Depth,
                      showCommunityAvatar: true,
                      padding: padding_15_TB,
                      margin: padding_10_B,
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(color: color_eeeeee)),
                      ),
                      rightBtn: lInkWell(
                        onTap: () {
                          showMenuModal(
                              context,
                              _viewDetail.userinfo.userId == auth.user.id
                                  ? {
                                      lText("수정하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _moveWrite(),
                                      lText("삭제하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _deleteBoard(widget.id),
                                    }
                                  : {
                                      lText("채팅하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _moveChatting(_viewDetail.userinfo.userId),
                                      lText("차단하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _blockUser(_viewDetail.userinfo.nickname, _viewDetail.userinfo.userId),
                                      lText("신고하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _moveNofity(_viewDetail.userinfo.userId, _viewDetail.communityShare.id, name: _viewDetail.userinfo.nickname),
                                    });
                        },
                        child: Lcons.more(color: color_999999),
                      ),
                    ),
                    title: Container(
                      alignment: Alignment.centerLeft,
                      padding: padding_10_TB,
                      child: lText(_viewDetail.communityShare.title, style: st_b_18(fontWeight: FontWeight.w700)),
                    ),
                    topper: Container(
                      padding: padding_10_T,
                      width: data.width(context, 0.9),
                      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                        lTextBtn(_viewDetail.communityShare.category, bg: Colors.white, border: Border.all(color: Commons.getColor()), style: st_12(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
                        _viewDetail.userinfo.userId == auth.user.id && _viewDetail.communityShare.category == Category.give.name
                            ? lIconText(
                                isExpanded: false,
                                title: _viewDetail.communityShare.shareStatus,
                                isLeft: false,
                                icon: Lcons.nav_bottom(color: color_999999, size: 20),
                                textStyle: st_14(textColor: _viewDetail.communityShare.shareStatus == ShareStatus.compleate.name ? color_999999 : Commons.getColor(), fontWeight: FontWeight.w500),
                                onClick: () => showMenuModal(context, {
                                      lText(ShareStatus.inprogress.name, style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _requestUpdateStatus(ShareStatus.inprogress),
                                      lText(ShareStatus.reserve.name, style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _requestUpdateStatus(ShareStatus.reserve),
                                      lText(ShareStatus.compleate.name, style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _requestUpdateStatus(ShareStatus.compleate),
                                    }))
                            : Container(
                                padding: padding_10_L,
                                child: lText(
                                  _viewDetail.communityShare.shareStatus,
                                  style: st_15(textColor: _viewDetail.communityShare.shareStatus == ShareStatus.compleate.name ? color_999999 : Commons.getColor(), fontWeight: FontWeight.w700),
                                ),
                              )
                      ]),
                    ),
                    body: BoardBody(
                      padding: padding_20_T,
                      text: _viewDetail.communityShare.content,
                      images: _viewDetail.communityShareImages.map((e) => e.images).toList(),
                      viewType: ViewType.normal,
                    ),
                    footer: Container(
                      key: _replyView,
                      padding: padding_20_B,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          lText("댓글".tr(), style: st_15(textColor: color_999999, fontWeight: FontWeight.w500)),
                          Container(padding: padding_05_LR, child: Lcons.chat(color: color_999999, size: 20)),
                          lText("${_viewDetail.count}", style: st_15(textColor: color_999999, fontWeight: FontWeight.w500)),
                        ],
                      ),
                    ),
                    dock: Container(
                      alignment: Alignment.center,
                      padding: padding_20_LR,
                      height: kBottomNavigationBarHeight,
                      decoration: BoxDecoration(
                        border: Border(top: BorderSide(color: color_eeeeee)),
                        color: Colors.white,
                      ),
                      child: BoardLikeMenus(
                        like: _viewDetail.communityShare.likeCnt,
                        reply: _viewDetail.count,
                        isBookMark: _viewDetail.communityShare.isScrap,
                        isLike: _viewDetail.communityShare.isLike,
                        bookmarkOnClick: () => _requestBookmark(widget.id, _viewDetail.communityShare.isScrap),
                        likeOnClick: () => _requestLike(widget.id, _viewDetail.communityShare.isLike),
                      ),
                    ),
                    showReply: true,
                    reply: _viewDetail.results
                        .map((e) => ReplyData(
                              id: e.id,
                              level: e.level,
                              parent: e.parent,
                              message: e.reply,
                              writer: e.writerNickname,
                              writtenTime: StringUtils.parseYMDHM(e.updatedate),
                              profile: e.profileimg,
                              isDeleted: e.isBlock || e.isDelete == 1,
                              replyMenu: e.writerId == auth.user.id
                                  ? {
                                      lText("삭제하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => e.isDelete != 1 ? _deleteReply(e.id) : showNormalDlg(message: "이미삭제된댓글입니다".tr()),
                                    }
                                  : {
                                      lText("채팅하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _moveChatting(e.writerId),
                                      lText("차단하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _blockUser(e.writerNickname, e.writerId),
                                      lText("신고하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _moveNofity(e.writerId, _viewDetail.communityShare.id, commentId: e.id, name: e.writerNickname),
                                    },
                              isWriter: _viewDetail.userinfo.userId == e.writerId,
                              isMine: auth.user.id == e.writerId,
                            ))
                        .toList(),
                    saveReplyAction: (parent, text) async {
                      await _requestReplySave(widget.id, text, parent);
                    },
                    replyLength: _viewDetail.count,
                    fromComm: true,
                  )),
      ),
    );
  }

  void _requestCommunityView(int id, {bool isNext = false}) {
    try {
      if (isNext && _viewDetail.next.isEmpty) return;
      CommunityListViewRequest req = CommunityListViewRequest(id: id);
      apiHelper.reqeustCommunityView(req, next: isNext ? _viewDetail.next : null).then((response) {
        responseCode = response.getCode();
        if (response.getCode() == KEY_SUCCESS) {
          if (isNext) {
            _viewDetail.results.addAll(response.getData().results);
            _viewDetail.next = response.getData().next;
            _viewDetail.previous = response.getData().previous;
          } else {
            _viewDetail = response.getData();
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestLike(int id, bool isLike) {
    try {
      _needUpdate = true;
      if (isLike) {
        _viewDetail.communityShare.likeCnt--;
        _viewDetail.communityShare.isLike = false;
        apiHelper.reqeustCommunityLikeDelete(CommunityLikeRequest(shareId: id)).then((response) {
          if (response.getCode() != KEY_SUCCESS) {
            _viewDetail.communityShare.likeCnt++;
            _viewDetail.communityShare.isLike = true;
          }
          onUpDate();
        }).catchError((e) {
          _viewDetail.communityShare.likeCnt++;
          _viewDetail.communityShare.isLike = true;
          onUpDate();
        });
      } else {
        _viewDetail.communityShare.likeCnt++;
        _viewDetail.communityShare.isLike = true;
        apiHelper.reqeustCommunityLike(CommunityLikeRequest(shareId: id)).then((response) {
          if (response.getCode() != KEY_SUCCESS) {
            _viewDetail.communityShare.likeCnt--;
            _viewDetail.communityShare.isLike = false;
          }
          onUpDate();
        }).catchError((e) {
          _viewDetail.communityShare.likeCnt--;
          _viewDetail.communityShare.isLike = false;
          onUpDate();
        });
      }
      onUpDate();
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestBookmark(int id, bool isScrap) {
    try {
      _needUpdate = true;
      if (isScrap) {
        _viewDetail.communityShare.isScrap = false;
        apiHelper.reqeustCommunityScrapDelete(CommnunityScrapRequest(shareId: id)).then((response) {
          if (response.getCode() != KEY_SUCCESS) {
            _viewDetail.communityShare.isScrap = true;
          }
          onUpDate();
        }).catchError((e) {
          _viewDetail.communityShare.isScrap = true;
          onUpDate();
        });
      } else {
        _viewDetail.communityShare.isScrap = true;
        apiHelper.reqeustCommunityScrap(CommnunityScrapRequest(shareId: id)).then((response) {
          if (response.getCode() != KEY_SUCCESS) {
            _viewDetail.communityShare.isScrap = false;
          } else {
            Commons.showToast("북마크안내".tr());
          }
          onUpDate();
        }).catchError((e) {
          _viewDetail.communityShare.isScrap = false;
          onUpDate();
        });
      }
      onUpDate();
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  Future<void> _requestReplySave(int id, String text, int parent, {bool retry = true}) async {
    try {
      _needUpdate = true;
      CommunityReplySaveRequest req = CommunityReplySaveRequest(shareId: id, reply: text, parent: parent);
      var response = await apiHelper.reqeustCommunityReplySave(req);
      if (response.getCode() == KEY_SUCCESS) {
        _viewDetail.count++;
        _viewDetail.communityShare.replyCnt++;
        int index = 0;
        if (parent > 0) index = _viewDetail.results.indexOf(_viewDetail.results.lastWhere((reply) => reply.id == parent || reply.parent == parent));
        Reply reply = Reply(
          id: response.getData().id,
          parent: parent,
          reply: text,
          writerId: auth.user.id,
          level: parent,
          writerNickname: response.getData().writerNickname,
          profileimg: response.getData().profileimg,
          updatedate: response.getData().updatedate,
        );
        if (_viewDetail.results.isNotEmpty && parent != 0) {
          _viewDetail.results.insert(index + 1, reply);
        } else {
          _viewDetail.results.add(reply);
        }
      } else {
        if (retry) {
          log.e({
            '--- TITLE    ': '--------------- ${response.getCode()} ---------------',
            '--- MESSAGE  ': response.getMsg(),
            '--- DESC     ': "Do retry",
          });
          _requestReplySave(id, text, parent, retry: false);
        }
      }
      flag.disableLoading(fn: () => onUpDate());
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestReplyDelete(int id) {
    try {
      _needUpdate = true;
      CommunityDeleteRequest req = CommunityDeleteRequest(id: id);
      apiHelper.reqeustCommunityReplyDel(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _requestCommunityView(widget.id);
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestDelete(int id) {
    try {
      _needUpdate = true;
      CommunityDeleteRequest req = CommunityDeleteRequest(id: id);
      apiHelper.reqeustCommunityDelete(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          showNormalDlg(context: context, message: response.getData().message, color: Commons.getColor(), onClickAction: (action) => Commons.pagePop(context, data: true));
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestUpdateStatus(ShareStatus status) {
    try {
      _needUpdate = true;
      String latestStatus = _viewDetail.communityShare.shareStatus;
      _viewDetail.communityShare.shareStatus = status.name;
      onUpDate();
      CommunityUpdateRequest req = CommunityUpdateRequest(
        id: widget.id,
        category: EnumUtils.getCategory(name: _viewDetail.communityShare.category).index,
        shareStatus: status.index + 1,
        content: _viewDetail.communityShare.content,
        title: _viewDetail.communityShare.title,
      );
      apiHelper.reqeustCommunityUpdate(req).then((response) {
        if (response.getCode() != KEY_SUCCESS) {
          _viewDetail.communityShare.shareStatus = latestStatus;
        }
        onUpDate();
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _requestUserBlock(int userId) {
    try {
      BlockSaveRequest req = BlockSaveRequest(block_user: userId);
      apiHelper.requestBlockSave(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.showToast(response.getMsg());
        }
      }).catchError((e) {
        flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  Future<void> _moveWrite() async {
    var ret = await Commons.page(context, routeCommunityWrite,
        arguments: CommunityWritePage(
          id: widget.id,
          title: _viewDetail.communityShare.title,
          body: _viewDetail.communityShare.content,
          images: _viewDetail.communityShareImages.map((e) => e.images).toList(),
          category: EnumUtils.getCategory(name: _viewDetail.communityShare.category),
          status: EnumUtils.getShareStatus(name: _viewDetail.communityShare.shareStatus).index + 1,
        ));
    if (ret != null) {
      _requestCommunityView(widget.id);
    }
  }

  void _moveNofity(int id, int viewId, {int? commentId, String? name}) {
    Commons.page(context, routeCommunityNotify,
        arguments: CommunityNotifyPage(
          userId: id,
          boardId: viewId,
          replyId: commentId,
          userName: name,
        ));
  }

  void _moveChatting(int id) {
    Commons.page(context, routeChatRoom, arguments: ChatRoomPage(receiver: id));
  }

  void _deleteReply(int id) {
    showNormalDlg(message: "삭제확인_타이틀".tr(), btnLeft: "취소".tr(), onClickAction: (a) => {if (a == DialogAction.yes) _requestReplyDelete(id)});
  }

  void _deleteBoard(int id) {
    showNormalDlg(message: "삭제확인_타이틀".tr(), btnLeft: "취소".tr(), onClickAction: (a) => {if (a == DialogAction.yes) _requestDelete(id)});
  }

  void _blockUser(String name, int id) {
    showNormalDlg(message: name + "차단하시겠습니까".tr(), btnLeft: "취소".tr(), onClickAction: (a) => {if (a == DialogAction.yes) _requestUserBlock(id)});
  }
}
