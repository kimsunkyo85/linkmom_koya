import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/providers/community_event_provider.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/board/board_view.dart';
import 'package:linkmom/view/main/community/community_event_page.dart';
import 'package:provider/provider.dart';

import '../../lcons.dart';

class CommunityEventView extends StatefulWidget {
  final int? tab;

  CommunityEventView({Key? key, this.tab}) : super(key: key);

  @override
  State<CommunityEventView> createState() => _CommunityEventViewState();
}

class _CommunityEventViewState extends State<CommunityEventView> with TickerProviderStateMixin {
  CommunityEventProvider get _provider => context.read<CommunityEventProvider>();
  late TabController _eventTab;
  final int _inprogress = 0;
  final int _over = 1;
  var _setState;

  @override
  void initState() {
    super.initState();
    _eventTab = TabController(length: 2, vsync: this);
    _setState = () {
      if (this.mounted) setState(() {});
    };
    _provider.addListener(_setState);
    _provider.fetch();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (widget.tab != null) _eventTab.animateTo(widget.tab ?? 0);
    });
  }

  @override
  void deactivate() {
    _provider.removeListener(_setState);
    super.deactivate();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CommunityEventProvider>(
      create: (context) => CommunityEventProvider(),
      child: Container(
          child: Column(children: [
        Container(
          padding: padding_05_TB,
          child: VerticalDecoTabBar(
              tabBar: TabBar(
                  tabs: [
                Tab(text: "진행중인이벤트".tr()),
                Tab(text: "종료된이벤트".tr()),
              ],
                  controller: _eventTab,
                  unselectedLabelStyle: st_b_15(fontWeight: FontWeight.w700),
                  unselectedLabelColor: color_b2b2b2,
                  labelStyle: st_b_15(fontWeight: FontWeight.w700),
                  labelColor: Commons.getColor(),
                  indicatorColor: Colors.transparent,
                  onTap: (value) {
                    setState(() {});
                  })),
        ),
        Expanded(
            child: TabBarView(controller: _eventTab, children: [
          ListNextView(
            view: _provider
                .getData(_inprogress)
                .results!
                .map((e) => Container(
                      color: color_dedede.withOpacity(0.32),
                      child: EventBanner(
                          title: e.title,
                          bannerUrl: e.image,
                          fromTo: '${StringUtils.parseDotYMD(e.startDate)} ~ ${StringUtils.parseDotYMD(e.endDate)}',
                          onClickAction: () => Commons.page(context, routeCommunityEvent,
                              arguments: CommunityEventPage(
                                id: e.id,
                                title: e.title,
                                fromTo: '${StringUtils.parseDotYMD(e.startDate)} ~ ${StringUtils.parseDotYMD(e.endDate)}',
                                event: e,
                              ))),
                    ))
                .toList(),
            onRefresh: () => _provider.refresh(_inprogress),
            onNext: () => _provider.next(_inprogress),
            showNextLoading: _provider.getData(_inprogress).next.isNotEmpty,
            emptyView: lEmptyView(height: Commons.height(context, 0.5), img: Lcons.no_data_event().name, title: "이벤트준비중이에요".tr()),
          ),
          ListNextView(
            view: _provider
                .getData(_over)
                .results!
                .map((e) => Container(
                      color: color_dedede.withOpacity(0.32),
                      child: EventBanner(
                          title: e.title,
                          bannerUrl: e.image,
                          fromTo: '${StringUtils.parseDotYMD(e.startDate)} ~ ${StringUtils.parseDotYMD(e.endDate)}',
                          onClickAction: () => Commons.page(context, routeCommunityEvent,
                              arguments: CommunityEventPage(
                                id: e.id,
                                title: e.title,
                                fromTo: '${StringUtils.parseDotYMD(e.startDate)} ~ ${StringUtils.parseDotYMD(e.endDate)}',
                                event: e,
                              ))),
                    ))
                .toList(),
            onRefresh: () => _provider.refresh(_over),
            onNext: () => _provider.next(_over),
            showNextLoading: _provider.getData(_over).next.isNotEmpty,
            emptyView: lEmptyView(height: Commons.height(context, 0.5), img: Lcons.no_data_event().name, title: "종료된이벤트가없어요".tr()),
          ),
        ]))
      ])),
    );
  }
}
