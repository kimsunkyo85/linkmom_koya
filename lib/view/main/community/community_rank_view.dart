import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/community_list_response.dart';
import 'package:linkmom/data/providers/model/community_rank_provider.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/tab_page_view.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/community/community_rank_page.dart';
import 'package:provider/provider.dart';

class CommunityRankView extends StatefulWidget {
  final DataManager data;

  CommunityRankView({Key? key, required this.data}) : super(key: key);

  @override
  State<CommunityRankView> createState() => _CommunityRankViewState();
}

class _CommunityRankViewState extends State<CommunityRankView> with TickerProviderStateMixin {
  CommunityRankProvider get _provider => context.read<CommunityRankProvider>();
  Region? _currentRegion;
  late TabController _tabCtrl;
  var _setState;

  @override
  void initState() {
    super.initState();
    _setState = () {
      if (this.mounted) setState(() {});
    };
    _provider.addListener(_setState);
    _provider.fetch();
    _tabCtrl = TabController(vsync: this, length: 3);
  }

  @override
  void deactivate() {
    _provider.removeListener(_setState);
    super.deactivate();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CommunityRankProvider>(
      create: (context) => CommunityRankProvider(),
      child: lModalProgressHUD(
        _provider.showLoading,
        Container(
            width: Commons.width(context, 1),
            child: _provider.getDataList.isNotEmpty
                ? Container(
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    Container(
                        width: double.infinity,
                        color: color_eeeeee,
                        padding: padding_10_LR,
                        child: Row(
                          children: [
                            Lcons.location(color: color_222222, size: 16),
                            sb_w_05,
                            DropdownButton<Region>(
                              icon: Lcons.nav_bottom(),
                              underline: Container(),
                              hint: lText("전국".tr(), style: st_b_14(fontWeight: FontWeight.w500)),
                              items: _provider.getRegions.length > 1
                                  ? _provider.getRegions
                                      .map((e) => DropdownMenuItem(
                                            value: e,
                                            child: lText(e.region2Depth + ' ' + e.region3DepthH, style: st_b_14(fontWeight: FontWeight.w500)),
                                          ))
                                      .toList()
                                  : [],
                              onChanged: (value) {
                                setState(() {
                                  _currentRegion = value;
                                  _provider.fetch(hcode: value!.hcode);
                                });
                              },
                              style: st_b_14(fontWeight: FontWeight.w500),
                              value: _currentRegion,
                            )
                          ],
                        )),
                    Expanded(
                        child: Container(
                            child: TabPageView(
                                onCreateView: (ctrl) => _tabCtrl = ctrl,
                                useOtherTabbar: true,
                                focusedTabbarStyle: st_15(fontWeight: FontWeight.w700, textColor: Commons.getColor()),
                                tabbarStyle: st_15(fontWeight: FontWeight.w700, textColor: color_b2b2b2),
                                tabbarName: [
                                  TabItem(title: "금액랭킹".tr()),
                                  TabItem(title: "좋아요랭킹".tr()),
                                  TabItem(title: "후기랭킹".tr()),
                                ],
                                tabbarView: _provider.getDataList.isNotEmpty
                                    ? _provider.getDataList.map((e) {
                                        int index = _provider.getDataList.indexOf(e);
                                        return NextRequestListener(
                                          onNextRequest: () => _provider.next(index),
                                          child: LinkmomRefresh(
                                              padding: padding_0,
                                              child: e.results != null && e.results!.isNotEmpty
                                                  ? Wrap(children: [
                                                      Padding(padding: EdgeInsets.fromLTRB(20, 16, 20, 8), child: lText("랭킹집계기준".tr(), style: st_12(textColor: color_b2b2b2))),
                                                      Container(
                                                          child: Column(
                                                              mainAxisSize: MainAxisSize.min,
                                                              children: e.results!.map((r) {
                                                                int idx = e.results!.indexOf(r);
                                                                return lInkWell(
                                                                    onTap: () => Commons.page(
                                                                          context,
                                                                          routeCommunityRank,
                                                                          arguments: CommunityRankPage(rank: idx + 1, type: RankType.values[index], id: r.userinfo!.userId, hcode: r.userinfo!.hcode),
                                                                        ),
                                                                    child: Container(
                                                                      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_dedede.withAlpha(56), width: 8))),
                                                                      padding: padding_25,
                                                                      child: Row(
                                                                        children: [
                                                                          Container(
                                                                            child: RankType.cost.index != index
                                                                                ? Container(
                                                                                    margin: padding_10_R,
                                                                                    child: Row(
                                                                                      children: [
                                                                                        Container(
                                                                                          child: Column(
                                                                                            children: [
                                                                                              lText(
                                                                                                (idx + 1).toString(),
                                                                                                style: st_20(textColor: (idx + 1) < 4 ? color_ffbb56 : color_222222, fontWeight: FontWeight.w700),
                                                                                              ),
                                                                                              rankTransition(r.rankinginfo!.increase),
                                                                                            ],
                                                                                          ),
                                                                                        ),
                                                                                        sb_w_10,
                                                                                        Column(
                                                                                          children: [
                                                                                            Stack(alignment: Alignment.topLeft, children: [
                                                                                              Container(margin: padding_10, child: lProfileAvatar(r.userinfo!.profileimg, radius: 30)),
                                                                                              getRankImg(idx + 1),
                                                                                            ]),
                                                                                          ],
                                                                                        ),
                                                                                      ],
                                                                                    ),
                                                                                  )
                                                                                : Container(
                                                                                    margin: EdgeInsets.fromLTRB(0, 0, 30, 0),
                                                                                    child: Column(
                                                                                      children: [
                                                                                        getRankImg(e.results!.indexOf(r) + 1, radius: 50, showNumber: true),
                                                                                        rankTransition(r.rankinginfo!.increase),
                                                                                      ],
                                                                                    )),
                                                                          ),
                                                                          Column(
                                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                                              lText('${r.userinfo!.firstName} ${"링크쌤".tr()}', style: st_b_15(fontWeight: FontWeight.w700)),
                                                                              sb_h_05,
                                                                              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                                                                lText(r.userinfo!.age, style: st_13(textColor: color_545454)),
                                                                                Container(padding: padding_05_LR, height: 15, child: lDividerVertical(thickness: 1, color: color_dedede)),
                                                                                lText(r.userinfo!.gender, style: st_13(textColor: color_545454)),
                                                                                Container(padding: padding_05_LR, height: 15, child: lDividerVertical(thickness: 1, color: color_dedede)),
                                                                                lText(r.userinfo!.currentRegion3depth, style: st_13(textColor: color_545454)),
                                                                              ]),
                                                                              sb_h_05,
                                                                              Row(children: [
                                                                                lText("돌봄누적소득".tr(), style: st_13(textColor: color_999999)),
                                                                                sb_w_10,
                                                                                lText('${StringUtils.formatPay(r.rankinginfo!.priceSum)}${"원".tr()}', style: st_b_13(fontWeight: FontWeight.w500)),
                                                                              ]),
                                                                              sb_h_05,
                                                                              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                                                                lInkWell(
                                                                                  onTap: () => null,
                                                                                  child: Row(children: [
                                                                                    Lcons.heart(color: Commons.getColor(), size: 12),
                                                                                    sb_w_05,
                                                                                    lText(r.rankinginfo!.likeCnt.toString(), style: st_b_14(fontWeight: FontWeight.w700)),
                                                                                  ]),
                                                                                ),
                                                                                Container(margin: padding_05_L, child: lDot(size: 2, color: color_cecece)),
                                                                                lInkWell(
                                                                                  onTap: () => Commons.page(context, routeCommunityRank,
                                                                                      arguments: CommunityRankPage(
                                                                                        rank: e.results!.indexOf(r) + 1,
                                                                                        type: RankType.values[index],
                                                                                        id: r.userinfo!.userId,
                                                                                        hcode: r.userinfo!.hcode,
                                                                                        scroll: 1,
                                                                                      )),
                                                                                  child: TextHighlight(
                                                                                    text: '${"후기".tr()} ${r.rankinginfo!.reviewCnt}${"개".tr()}',
                                                                                    term: '${r.rankinginfo!.reviewCnt}',
                                                                                    textStyle: st_b_13(),
                                                                                    textStyleHighlight: st_b_13(fontWeight: FontWeight.w700),
                                                                                  ),
                                                                                ),
                                                                              ])
                                                                            ],
                                                                          )
                                                                        ],
                                                                      ),
                                                                    ));
                                                              }).toList())),
                                                      if (_provider.hasNext(index))
                                                        lInkWell(
                                                            child: Container(
                                                                height: 100,
                                                                color: color_dedede.withAlpha(56),
                                                                alignment: Alignment.center,
                                                                child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                                                                  lIconText(
                                                                    icon: Lcons.add(color: color_545454),
                                                                    title: "더보기".tr(),
                                                                    textStyle: st_14(textColor: color_545454, fontWeight: FontWeight.w500),
                                                                    isExpanded: false,
                                                                  ),
                                                                ])),
                                                            onTap: () => _provider.next(index)),
                                                    ])
                                                  : Container(
                                                      height: Commons.height(context, 0.5),
                                                      child: rankEmptyView(context, widget.data, _provider.showLoading),
                                                    ),
                                              onRefresh: () => _provider.refresh(index, hcode: _currentRegion == null ? null : _currentRegion!.hcode)),
                                        );
                                      }).toList()
                                    : List.generate(
                                        3,
                                        (index) => Container(
                                              height: Commons.height(context, 0.5),
                                              child: rankEmptyView(context, widget.data, _provider.showLoading),
                                            ))))),
                  ]))
                : Container(
                    height: Commons.height(context, 0.5),
                    child: rankEmptyView(context, widget.data, _provider.showLoading),
                  )),
      ),
    );
  }
}
