import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/community_list_response.dart';
import 'package:linkmom/data/network/models/community_view_response.dart';
import 'package:linkmom/data/providers/community_list_provider.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/home_provider.dart';
import 'package:linkmom/utils/listview/list_next_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/board/board_view.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/chat/chat_room_page.dart';
import 'package:linkmom/view/main/community/community_view_page.dart';
import 'package:linkmom/view/main/community/community_write_page.dart';
import 'package:linkmom/view/main/notify/community_notify_page.dart';
import 'package:provider/provider.dart';

class CommunityListView extends StatefulWidget {
  final bool needUpdate;

  CommunityListView({this.needUpdate = false, Key? key}) : super(key: key);

  @override
  State<CommunityListView> createState() => _CommunityListViewState();
}

class _CommunityListViewState extends State<CommunityListView> {
  CommunityListProvider get _provider => context.read<CommunityListProvider>();
  List<Category> _selectedCategoty = [];
  Region? _currentRegion;
  var _setState;

  @override
  void initState() {
    super.initState();
    _setState = () {
      if (this.mounted) setState(() {});
    };
    _provider.addListener(_setState);
    _provider.fetch();
  }

  @override
  void deactivate() {
    _provider.removeListener(_setState);
    super.deactivate();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<HomeNavigationProvider>(context);
    if (provider.communityUpdated) {
      provider.updatedEvent(TAB_COMMUNITY);
      _provider.fetch();
    }
    return ChangeNotifierProvider<CommunityListProvider>(
      create: (context) => CommunityListProvider(),
      child: lModalProgressHUD(
        false,
        lScaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: () async {
              var ret = await Commons.page(context, routeCommunityWrite, arguments: CommunityWritePage(region: _provider.getRegions));
              if (ret != null) _refresh();
            },
            backgroundColor: Commons.getColor(),
            child: Lcons.care_edit(size: 35),
          ),
          body: ListNextView(
            onNext: () => _provider.next(),
            onRefresh: () => _provider.refresh(),
            header: _buildMenu(),
            showNextLoading: _provider.hasNext,
            emptyView: lEmptyView(
              height: Commons.height(context, 0.5),
              img: Lcons.no_data_community().name,
              title: "등록된콘텐츠가없어요".tr(),
              content: "첫번째로당신의소통글을남겨보세요".tr(),
            ),
            view: _provider.getDatas
                .map((e) => lInkWell(
                      onTap: () => _moveView(e),
                      child: _buildBoardView(e),
                    ))
                .toList(),
          ),
        ),
        isOnlyLoading: true,
      ),
    );
  }

  void _request() {
    if (_currentRegion == null) _currentRegion = _provider.getRegions.first;
    _provider.fetch(
      hcode: _currentRegion!.hcode > 0 ? _currentRegion!.hcode.toString() : "",
      category: _selectedCategoty.map((e) => e.index).toList(),
    );
  }

  Widget _buildType(String type, {String status = ''}) {
    if (status.isEmpty)
      return lTextBtn(type, bg: Colors.transparent, border: Border.all(color: Commons.getColor()));
    else
      return Row(children: [
        lTextBtn(type, bg: Colors.transparent, border: Border.all(color: Commons.getColor())),
        sb_w_10,
        lText(status, style: st_15(textColor: status.contains("완료".tr()) ? color_999999 : Commons.getColor(), fontWeight: FontWeight.w700)),
      ]);
  }

  void _moveNofity(int userId, int boardId, String? userName) {
    Commons.page(context, routeCommunityNotify, arguments: CommunityNotifyPage(userId: userId, boardId: boardId, userName: userName));
  }

  void _moveChatting(int id) {
    Commons.page(context, routeChatRoom, arguments: ChatRoomPage(receiver: id));
  }

  Widget _buildBoardView(CommunityBoardData e) {
    return BoardPreview(
      body: e.communityShare!.content,
      title: BoardProfile(
        showCommunityAvatar: true,
        author: e.userinfo!.nickname,
        writtenTime: e.communityShare!.updatedate,
        profileImg: e.userinfo!.profileimg,
        location: e.userinfo!.region3Depth,
        padding: padding_10_B,
        margin: padding_10_B,
      ),
      onMenuClick: () => {
        showMenuModal(
          context,
          e.userinfo!.userId == auth.user.id
              ? {
                  lText("수정하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _moveWrite(e),
                  lText("삭제하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _deleteBoard(e.communityShare!.id),
                }
              : {
                  lText("채팅하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _moveChatting(e.userinfo!.userId),
                  lText("차단하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _userBlock(e.userinfo!.userId, e.userinfo!.nickname),
                  lText("신고하기".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)): () => _moveNofity(e.userinfo!.userId, e.communityShare!.id, e.userinfo!.nickname)
                },
        )
      },
      bodyTitle: e.communityShare!.title,
      images: e.communityShareImages!.map((e) => e.image).toList(),
      thumbnail: e.communityShareImages!.map((e) => e.imageThumbnail).toList(),
      likes: BoardLikeMenus(
          isBookMark: e.communityShare!.isScrap,
          isLike: e.communityShare!.isLike,
          reply: e.communityShare!.replyCnt,
          like: e.communityShare!.likeCnt,
          likeOnClick: () {
            setState(() {
              _provider.like(e.communityShare!.id, e.communityShare!.isLike);
              e.communityShare!.isLike = !e.communityShare!.isLike;
              if (e.communityShare!.isLike) {
                e.communityShare!.likeCnt++;
              } else {
                e.communityShare!.likeCnt--;
              }
            });
          },
          onReplyClick: () {
            _moveView(e, toReply: true);
          },
          bookmarkOnClick: () {
            setState(() {
              _provider.bookmark(e.communityShare!.id, e.communityShare!.isScrap);
              e.communityShare!.isScrap = !e.communityShare!.isScrap;
              if (e.communityShare!.isScrap) Commons.showToast("북마크안내".tr());
            });
          }),
      topper: _buildType(e.communityShare!.category, status: e.communityShare!.shareStatus),
    );
  }

  Widget _buildMenu() {
    return Column(
      children: [
        Container(
          height: 60,
          width: double.infinity,
          color: color_dedede.withAlpha(81),
          child: lScrollView(
              padding: padding_15,
              scrollDirection: Axis.horizontal,
              child: Row(
                children: Category.values
                    .map(
                      (e) => Padding(
                        padding: padding_05_R,
                        child: lTextBtn(
                          e.name,
                          bg: Colors.white,
                          border: Border.all(color: _selectedCategoty.contains(e) ? Commons.getColor() : Colors.transparent),
                          style: st_13(textColor: _selectedCategoty.contains(e) ? Commons.getColor() : color_999999, fontWeight: FontWeight.w500),
                          onClickEvent: () {
                            setState(() {
                              if (_selectedCategoty.contains(e)) {
                                _selectedCategoty.remove(e);
                              } else {
                                _selectedCategoty.add(e);
                              }
                              _request();
                            });
                          },
                        ),
                      ),
                    )
                    .toList(),
              )),
        ),
        Container(
          margin: padding_10_L,
          padding: padding_10_LR,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Lcons.location(color: color_222222, size: 16),
                  sb_w_05,
                  DropdownButton<Region>(
                    icon: Lcons.nav_bottom(),
                    underline: Container(),
                    hint: lText(_provider.getRegions.first.region2Depth, style: st_b_14(fontWeight: FontWeight.w500)),
                    items: context
                        .read<CommunityListProvider>()
                        .getRegions
                        .map((e) => DropdownMenuItem(
                              value: e,
                              child: lText(e.region2Depth, style: st_b_14(fontWeight: FontWeight.w500)),
                            ))
                        .toList(),
                    onChanged: (value) {
                      setState(() {
                        _currentRegion = value;
                        _request();
                      });
                    },
                    style: st_b_14(fontWeight: FontWeight.w500),
                    value: _currentRegion,
                  ),
                ],
              ),
              lIconText(
                onClick: () {
                  setState(() {
                    _provider.setShowingMine = !_provider.isShowingMine;
                    _request();
                  });
                },
                title: "내게시글".tr(),
                textStyle: st_13(textColor: _provider.isShowingMine ? color_222222 : color_999999, fontWeight: _provider.isShowingMine ? FontWeight.w500 : FontWeight.w400),
                icon: Lcons.check(color: Commons.getColor(), disableColor: color_cecece, isEnabled: _provider.isShowingMine),
                isExpanded: false,
              )
            ],
          ),
        ),
      ],
    );
  }

  Future<void> _moveView(CommunityBoardData data, {bool toReply = false}) async {
    var ret = await Commons.page(
      context,
      routeCommunityView,
      arguments: CommunityViewPage(id: data.communityShare!.id, toReply: toReply),
    );
    setState(() {
      if (ret is CommunityViewData) _provider.updateData(ret);
    });
  }

  Future<void> _moveWrite(CommunityBoardData info) async {
    var ret = await Commons.page(context, routeCommunityWrite,
        arguments: CommunityWritePage(
          region: _provider.getRegions,
          id: info.communityShare!.id,
          title: info.communityShare!.title,
          body: info.communityShare!.content,
          images: info.communityShareImages != null ? info.communityShareImages!.map((e) => e.image).toList() : [],
          category: EnumUtils.getCategory(name: info.communityShare!.category),
          status: EnumUtils.getShareStatus(name: info.communityShare!.shareStatus).index + 1,
          reload: true,
        ));
    if (ret is bool && ret) _refresh();
  }

  void _deleteBoard(int id) {
    showNormalDlg(message: "삭제확인_타이틀".tr(), btnLeft: "취소".tr(), onClickAction: (a) => {if (a == DialogAction.yes) _provider.delete(id)});
  }

  void _userBlock(int id, String name) {
    showNormalDlg(message: name + "차단하시겠습니까".tr(), btnLeft: "취소".tr(), onClickAction: (a) => {if (a == DialogAction.yes) _provider.block(id)});
  }

  void _refresh() {
    _provider.setShowingMine = false;
    _provider.refresh();
  }
}
