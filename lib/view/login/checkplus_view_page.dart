import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/checkplus_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:webview_cookie_manager/webview_cookie_manager.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../main.dart';

class CheckPlusView extends BaseStateful {
  final DataManager? data;
  final Function callback;

  CheckPlusView({this.data, required this.callback});

  @override
  _CheckPlusViewState createState() => _CheckPlusViewState();
}

class _CheckPlusViewState extends BaseStatefulState<CheckPlusView> {
  final String androidCheckPlus = "startCheckPlus";
  final cookieManager = WebviewCookieManager();
  final _androidView = MethodChannel("startCheckPlus");
  late WebViewController _controller;
  late AndroidViewController _androidCtrl;
  Set<JavascriptChannel> _channels = Set();

  @override
  void initState() {
    super.initState();
    try {
      cookieManager.clearCookies();
      _controller.clearCache();
      _channels.clear();
    } catch (e) {
      log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    _channels.add(JavascriptChannel(
        name: 'checkplus',
        onMessageReceived: (message) {
          log.d("receive checkplus ${message.message}");
          NiceAuthData response = NiceAuthData.fromJson(json.decode(message.message));
          if (response.returnMsg.contains("성공".tr())) {
            widget.callback(response);
          } else {
            showNormalDlg(message: response.returnMsg);
          }
        }));
    _androidView.setMethodCallHandler((call) async {
      if (call.method == androidCheckPlus) {
        log.d("setMethodCallHandler: ${call.arguments('data')}");
      }
    });
    flag.enableLoading(fn: () => onUpDate());
  }

  @override
  Widget build(BuildContext context) {
    final String viewType = 'hybrid-view-type';
    final Map<String, dynamic> creationParams = <String, dynamic>{};
    creationParams.putIfAbsent('URL', () => ApiEndPoint.CHECKPLUS_URL);
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            body: Platform.isAndroid
                ? PlatformViewLink(
                    viewType: viewType,
                    surfaceFactory: (context, controller) {
                      // NOTE: deliver to native
                      _androidView.invokeMethod(androidCheckPlus).then((value) {
                        try {
                          log.d("received: ${value.toString()}");
                          NiceAuthData response = NiceAuthData.fromJson(json.decode(value));
                          if (response.returnMsg.contains("성공".tr())) {
                            widget.callback(response);
                          } else {
                            showNormalDlg(message: response.returnMsg);
                          }
                        } catch (e) {
                          showNormalDlg(message: e.toString());
                        }
                      });
                      return AndroidViewSurface(
                        controller: controller as AndroidViewController,
                        gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{
                          Factory<VerticalDragGestureRecognizer>(
                            () => VerticalDragGestureRecognizer(),
                          ),
                        },
                        hitTestBehavior: PlatformViewHitTestBehavior.opaque,
                      );
                    },
                    onCreatePlatformView: (params) {
                      Future.delayed(Duration(milliseconds: 500)).then((value) => flag.disableLoading(fn: () => onUpDate()));
                      return PlatformViewsService.initSurfaceAndroidView(
                        id: params.id,
                        viewType: viewType,
                        layoutDirection: ui.TextDirection.ltr,
                        creationParams: creationParams,
                        creationParamsCodec: StandardMessageCodec(),
                      )
                        ..addOnPlatformViewCreatedListener(params.onPlatformViewCreated)
                        ..create();
                    },
                  )
                : WebView(
                    initialUrl: ApiEndPoint.CHECKPLUS_URL,
                    javascriptMode: JavascriptMode.unrestricted,
                    javascriptChannels: _channels,
                    gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{
                      Factory<VerticalDragGestureRecognizer>(
                        () => VerticalDragGestureRecognizer(),
                      ),
                    },
                    navigationDelegate: (request) async {
                      log.d("received request: $request");
                      if (request.url.contains('intent://')) {
                        return NavigationDecision.navigate;
                      } else if (request.url.contains('market://details?id=') || request.url.contains('play.google.com/store')) {
                        await Commons.lLaunchUrl(request.url);
                        return NavigationDecision.navigate;
                      } else {
                        return NavigationDecision.navigate;
                      }
                    },
                    onWebViewCreated: (controller) async {
                      _controller = controller;
                      _controller.loadUrl(ApiEndPoint.CHECKPLUS_URL);
                    },
                    onWebResourceError: (err) {
                      log.d("error: ${err.description}, type: ${err.errorType!.index}, code: ${err.errorCode}");
                    },
                    debuggingEnabled: Commons.isDebugMode,
                    onPageFinished: (url) async {
                      flag.disableLoading(fn: () => onUpDate());
                      await _getCoolkies(url).then((cookies) async {
                        await WebviewCookieManager().setCookies(cookies);
                      });
                    },
                    onPageStarted: (url) {
                      log.d("started $url");
                    },
                  )));
  }

  Future<List<Cookie>> _getCoolkies(String url) async {
    final gotCookies = await cookieManager.getCookies(url);
    for (var item in gotCookies) {
      log.d("cookies: $item");
    }
    return gotCookies;
  }
}
