import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/login/find/find_password_page.dart';

import '../../../base/base_stateful.dart';
import '../../../main.dart';
import '../login_page.dart';

class FindIdCompletePage extends BaseStateful {
  FindIdCompletePage({this.data, this.id, this.name});

  final String? id;
  final String? name;
  final DataManager? data;

  FindIdCompletePageState createState() => FindIdCompletePageState();
}

class FindIdCompletePageState extends BaseStatefulState<FindIdCompletePage> {
  var _flag = ViewFlag();

  @override
  initState() {
    super.initState();
    super.onData(widget.data ?? DataManager());
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      // demo of some additional parameters
      WillPopScope(
          onWillPop: () => Commons.pageToMain(context, routeLogin),
          child: lScaffold(
            appBar: appBar("아이디확인".tr(), isBack: false, hide: false),
            body: Container(
              margin: padding_20_LR,
              child: Column(
                children: [
                  Expanded(child: _contentPage(widget.name ?? ' ', widget.id ?? ' ')
                      ),
                  sb_h_40,
                  lBtn("로그인".tr(), margin: EdgeInsets.only(bottom: 5), onClickAction: () {
                    log.d('『GGUMBI』>>> build : widget.id: ${widget.id},  <<< ');
                    Commons.pageClear(context, routeLogin, arguments: LoginPage(id: widget.id!));
                  }, btnColor: color_main),
                  lBtn("비밀번호찾기".tr(), margin: EdgeInsets.only(top: 5, bottom: 25), btnColor: Colors.white, style: st_b_16(textColor: color_00cfb5), onClickAction: () async {
                    Commons.page(context, routeFindPw, arguments: FindPasswordPage(data: widget.data!, id: widget.id!));
                  }),
                ],
              ),
            ),
          )),
      showText: false,
    );
  }

  Widget _contentPage(String name, mail) {
    String maskedName = '';
    if (name.length < 3) {
      maskedName = '${name[0]}◯';
    } else {
      maskedName = '${name[0]}◯${name[name.length - 1]}'; // show name first, last
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Lcons.complete(),
        sb_h_20,
        lText("$maskedName " + "아이디확인_메시지1".tr(), style: stFindResult),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            lText(
              mail,
              style: TextStyle(decoration: TextDecoration.underline, fontSize: 20),
            ),
            lText("아이디확인_메시지2".tr(), style: stFindResult)
          ],
        )
      ],
    );
  }
}
