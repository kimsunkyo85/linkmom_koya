import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/exists_request.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/login/find/find_checkplus_page.dart';
import 'package:linkmom/view/view_modules.dart';

import '../../../base/base_stateful.dart';
import '../../../main.dart';

class FindPasswordPage extends BaseStateful {
  FindPasswordPage({this.data, this.id});

  final String? id;
  final DataManager? data;

  FindPagePasswordState createState() => FindPagePasswordState();
}

class FindPagePasswordState extends BaseStatefulState<FindPasswordPage> with TickerProviderStateMixin {
  @override
  initState() {
    super.initState();
    if (widget.id != null) {
      log.d('『GGUMBI』>>> initState : widget.id: ${widget.id},  <<< ');
    }
    flag.disableConfirm(fn: () => onUpDate());
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        context: context,
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        appBar: appBar("비밀번호찾기".tr(), isClose: false, hide: false),
        body: Container(
          padding: padding_20,
          child: Column(
            children: [
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  sb_h_20,
                  lText("에러_아이디_확인".tr(), style: st_b_16(fontWeight: FontWeight.w700)),
                  sb_h_20,
                  IdField(
                      data: data,
                      onEditingComplete: (value) {
                        if (data.registerItem.idConfirm) flag.enableConfirm(fn: () => onUpDate());
                      },
                      onChanged: (value) {
                        if (data.registerItem.idConfirm) flag.enableConfirm(fn: () => onUpDate());
                      })
                ],
              )),
              lBtn("확인".tr(), isEnabled: flag.isConfirm, margin: padding_0, onClickAction: () => _requestExistsId()),
            ],
          ),
        ));
  }

  void _requestExistsId() {
    try {
      apiHelper.requestExists(ExistsRequest(username: data.tcId.text)).then((response) {
        /// NOTE: If already joined user. move the checkplus page
        if (response.getCode() != KEY_SUCCESS) {
          Commons.page(context, routeFindCheckplus, arguments: FindCheckplusPage(data: data, id: data.tcId.text));
        } else {
          showNormalDlg(message: "에러_아이디_확인2".tr());
        }
      });
    } catch (e) {
    }
  }
}
