import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

import '../../../base/base_stateful.dart';
import 'find_mobile_page.dart';

class FindIdPage extends BaseStateful {
  FindPageIdState createState() => FindPageIdState();
}

class FindPageIdState extends BaseStatefulState<FindIdPage> with TickerProviderStateMixin {

  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    // clearTcData(onUpDate);
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
      context: context,
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: appBar("아이디찾기".tr(), isClose: false, hide: false),
      body: FindMobilePage(data: data,type: FindType.id));
  }
}
