import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/checkplus_response.dart';
import 'package:linkmom/data/network/models/findid_request.dart';
import 'package:linkmom/data/network/models/reqauth_request.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/login/find/find_id_complete_page.dart';
import 'package:linkmom/view/view_modules.dart';

import '../../../main.dart';
import '../../../route_name.dart';

class FindMobilePage extends BaseStateful {
  final FindType? type;
  final DataManager? data;
  final NiceAuthData? authData;

  FindMobilePage({this.data, this.type, this.authData});

  @override
  _FindMobilePageState createState() => _FindMobilePageState();
}

class _FindMobilePageState extends BaseStatefulState<FindMobilePage> {
  bool _nameConfirm = false;
  bool _mobileConfirm = false;
  bool _authConfirm = false;
  bool _idConfirm = false;

  @override
  void initState() {
    super.initState();
    super.onData(widget.data ?? DataManager());
  }

  @override
  void dispose() {
    super.dispose();
    // clearTcData(onUpDate);
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
      context: context,
      body: Container(
        child: Column(children: [
          Expanded(
              child: Container(
                  margin: padding_20_LR,
                  child: ListView(
                    children: [
                      sb_h_50,
                      NameField(
                          data: data,
                          onChanged: (value) {
                            _nameConfirm = value.isNotEmpty;
                            onUpDate();
                          },
                          onEditingComplete: (value) {
                            _nameConfirm = value.isNotEmpty;
                            onUpDate();
                          }),
                      widget.type == FindType.pw
                          ? IdField(
                              data: data,
                              onEditingComplete: (value) {
                                _idConfirm = value;
                                onUpDate();
                              },
                              onChanged: () => {})
                          : Container(),
                      MobileField(
                        data: data,
                        flag: flag,
                        onChanged: (value) {
                          if (value.isNotEmpty) data.findItem.enableMobile(fn: () => onUpDate());
                        },
                        onEditingComplete: (value) {
                          if (value.isNotEmpty) data.findItem.enableMobile(fn: () => onUpDate());
                        },
                        onClick: () => _requestAuth(), // reqauth screeen 0: register, 1: other
                      ),
                      AuthCodeTimerField(
                          data: data,
                          onEditingComplete: (value) {
                            if (value.isNotEmpty) data.findItem.enableMobileAuth(fn: () => onUpDate());
                          },
                          onChanged: (value) {
                            if (value.isNotEmpty) data.findItem.enableMobileAuth(fn: () => onUpDate());
                          }),
                    ],
                  ))),
          lBtn(!_isConfirm() ? "확인".tr() : "다음".tr(), isEnabled: _isConfirm(), onClickAction: () {
            switch (widget.type) {
              case FindType.id:
                _requestFindId();
                break;
              default:
            }
          }, btnColor: color_main)
        ]),
      ),
    );
  }

  bool _isConfirm() {
    switch (widget.type) {
      case FindType.id:
        return data.findItem.isMobile && data.findItem.isMobileAuth && _nameConfirm;
      // case FindType.pw:
      //   ret = data.findItem.isMobile && data.findItem.isMobileAuth && _nameConfirm;
      //   break;
      default:
        return true;
    }
  }

  void debugAction() {
    data.tcMobile.text = "01000000000";
    data.tcName.text = "테스트";
    flag.enableConfirm(fn: () => onUpDate());
  }

  bool isMobileNumberConfirm() {
    return data.findItem.isMobileNumberConfirm = StringUtils.validateMobile(data.tcMobile.text) == null ? true : false;
  }

  bool isMobileNameConfirm() {
    return data.findItem.isMobileNameConfirm = StringUtils.validateName(data.tcName.text) == null ? true : false;
  }

  bool isMobileAuthConfirm() {
    return data.findItem.isMobileAuthConfirm = StringUtils.validateAuthNumberLength(data.tcAuth.text) == null ? true : false;
  }

  bool isIdConfirm() {
    return data.findItem.isEmailNameConfirm = StringUtils.validateName(data.tcEmailName.text) == null ? true : false;
  }

  void _requestFindId() async {
    FindIdRequest req = FindIdRequest(
        auth_number: data.tcAuth.text,
        first_name: data.tcName.text,
        auth_type: "2", // phone
        auth_key: data.tcMobile.text);
    try {
      flag.enableLoading(fn: () => onUpDate());
      focusClear(context);
      apiHelper.requestFindId(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.pageToMain(context, routeFindIdComplete, arguments: FindIdCompletePage(data: data, id: response.getData().username, name: data.tcName.text));
        } else {
          showNormalDlg(context: context, message: response.getMsg());
        }
        data.findItem.disableTimerMobile(fn: () => onUpDate());
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  _requestAuth() {
    ReqauthRequest req = ReqauthRequest(auth_type: AuthType.mobile.index.toString(), auth_key: data.tcMobile.text, req_screen: 1);
    flag.enableLoading(fn: () => onUpDate());
    apiHelper.requestReqauth(req).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        showNormalDlg(message: response.getMsg());
        data.findItem.enableTimerMobile(fn: () => onUpDate());
      }
      data.formMobile.currentState!.validate();
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
      data.findItem.disableTimerMobile(fn: () => onUpDate());
    });
  }
}
