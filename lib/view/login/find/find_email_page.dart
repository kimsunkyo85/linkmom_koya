import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/view_modules.dart';

/// This class is unused. please check deletable.
class FindEmailPage extends BaseStateful {
  final FindType? type;
  final DataManager? data;
  FindEmailPage({this.data, this.type});

  @override
  _FindEmailPageState createState() => _FindEmailPageState();
}

class _FindEmailPageState extends BaseStatefulState<FindEmailPage> {

  @override
  void initState() {
    super.initState();
    super.onData(widget.data ?? DataManager());

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
      context: context,
      body: Container(
        child: Column(children: [
          Expanded(
            child: Container(
              margin: padding_20_LR,
              child: Column(
                children: [
                  sb_h_50,
                  NameField(
                          data: data,
                    onEditingComplete: (name) {
                      isMobileNameConfirm();
                    },
                          onChanged: (value) => {}),
                      MobileField(
                        data: data,
                        flag: flag,
                        onEditingComplete: (value) {},
                        onChanged: (value) => {},
                        onClick: () => {},
                      ),
                      AuthCodeTimerField(
                        data: data,
                    lastField: widget.type == FindType.id, 
                    onEditingComplete: (value) {
                          if (value.isNotEmpty) flag.enableConfirm(fn: () => onUpDate());
                        },
                        onChanged: (value) => {},
                      ),
                      IdField(
                          data: data,
                          onChanged: (value) {
                      isMobileNumberConfirm();
                    },
                    onEditingComplete: () {
                      isEmailNameConfirm();
                    })
                ],
              ))),
          _confirmButton(widget.type!),
        ]),
      ),
    );
  }

  ///확인 버튼 클릭시 이벤트
  Widget _confirmButton(FindType type) {
    return lBtn(flag.isConfirm ? "확인".tr() : "다음".tr(), isEnabled: flag.isConfirm, onClickAction: () => {
    }, btnColor: color_main);
  }

  bool isEmailAuthConfirm() {
    return data.findItem.isEmailAuthConfirm =
        StringUtils.validateAuthNumberLength(data.tcEmailAuth.text) == null
            ? true
            : false;
  }

  bool isEmailNameConfirm() {
    return data.findItem.isEmailNameConfirm =
        StringUtils.validateName(data.tcEmailName.text) == null ? true : false;
  }

  bool isEmailEmailConfirm() {
    return data.findItem.isEmailNumberConfirm =
        StringUtils.validateEmail(data.tcEmailEmail.text) == null
            ? true
            : false;
  }

  void debugAction() {
    data.tcEmailAuth.text = "222222";
    data.tcEmailEmail.text = "0101234567";
    data.tcEmailName.text = "testname";
    flag.enableConfirm(fn: () => onUpDate());
  }

  bool isMobileNumberConfirm() {
    return data.findItem.isMobileNumberConfirm =
        StringUtils.validateMobile(data.tcMobile.text) == null ? true : false;
  }

  bool isMobileNameConfirm() {
    return data.findItem.isMobileNameConfirm =
        StringUtils.validateName(data.tcName.text) == null ? true : false;
  }

  bool isMobileAuthConfirm() {
    return data.findItem.isMobileAuthConfirm =
        StringUtils.validateAuthNumberLength(data.tcAuth.text) == null
            ? true
            : false;
  }

}
