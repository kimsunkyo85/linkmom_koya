import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/checkplus_response.dart';
import 'package:linkmom/data/network/models/findpw_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/login/password_change/password_change_page.dart';
import 'package:linkmom/view/login/checkplus_view_page.dart';
import 'package:easy_localization/easy_localization.dart';

class FindCheckplusPage extends BaseStateful {
  final DataManager? data;
  final String? id;
  FindCheckplusPage({this.data, this.id});

  @override
  _FindCheckplusPageState createState() => _FindCheckplusPageState();
}

class _FindCheckplusPageState extends BaseStatefulState<FindCheckplusPage> {
  @override
  Widget build(BuildContext context) {
    return lScaffold(
        appBar: appBar(
          "비밀번호찾기".tr(),
          hide: false,
          rightWidget: lInkWell(
            child: Container(width: 50, height: 10),
            onLongPress: () => Commons.page(context, routePwChange, arguments: PasswordChangePage(data: data)),
          ), // NOTE: for test
        ),
        body: CheckPlusView(
          data: data,
          callback: (response) {
            if (response != null) {
              _requestFindPw(response);
            }
          },
        ));
  }

  Future<void> _requestFindPw(NiceAuthData response) async {
    FindPwRequest req = FindPwRequest(
      username: widget.id!,
      nice_di: response.dupinfo,
    );
    try {
      apiHelper.requestFindPw(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.page(context, routePwChange,
              arguments: PasswordChangePage(data: data, req: req, accessToken: response.getData().access));
        } else {
          showNormalDlg(message: response.getMsg(), onClickAction: (a) => {Commons.pagePop(context)});
        }
      });
    } catch (e) {
    }
  }
}
