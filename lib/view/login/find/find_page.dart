import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/providers/loading_model.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/login/find/find_password_page.dart';
import 'package:provider/provider.dart';

import '../../../base/base_stateful.dart';
import '../../../main.dart';
import 'find_id_page.dart';

class FindPage extends BaseStateful {
  final String? pageType;
  final String? id;

  FindPage({this.pageType, this.id});

  FindPageState createState() => FindPageState();
}

class FindPageState extends BaseStatefulState<FindPage> with SingleTickerProviderStateMixin {
  String _title = "아이디찾기".tr();
  int _index = 0;
  late TabController _controller;
  final List<Tab> _tabs = <Tab>[
    Tab(text: "아이디찾기".tr()),
    Tab(text: "비밀번호찾기".tr()),
  ];

  @override
  initState() {
    super.initState();
    log.d('『GGUMBI』>>> initState : widget.pageType: ${widget.pageType}, \nwidget.id: ${widget.id}, <<< ');
    if (widget.pageType != null) {
      if (widget.pageType == 'pw') {
        _title = "비밀번호찾기".tr();
        _index = 1;
      } else {
        _title = "아이디찾기".tr();
        _index = 0;
      }
    }

    _controller = TabController(length: _tabs.length, vsync: this);
    _controller.index = _index;
    _controller.addListener(() {
      setState(() {
        _index = _controller.index;
        _title = _tabs[_index].text!;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final _loading = Provider.of<LoadingModel>(context, listen: true);
    return lModalProgressHUD(
      _loading.isLoading,
      Scaffold(
        appBar: appBar(_title),
        body: DefaultTabController(
          length: _tabs.length,
          child: Scaffold(
            backgroundColor: color_white,
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(kToolbarHeight),
              // TabBar 구현. 각 컨텐트를 호출할 탭들을 등록
              child: Container(
                height: kToolbarHeight,
                child: TabBar(
                  indicatorColor: color_00cfb5,
                  unselectedLabelColor: color_b2b2b2,
                  indicatorWeight: 2,
                  labelColor: color_00cfb5,
                  labelStyle: TextStyle(
                    fontSize: size_18,
                    fontWeight: FontWeight.w500,
                  ),
                  controller: _controller,
                  tabs: _tabs,
                ),
              ),
            ),
            // TabVarView 구현. 각 탭에 해당하는 컨텐트 구성
            body: TabBarView(
              controller: _controller,
              children: [
                FindIdPage(),
                FindPasswordPage(id: widget.id!),
              ],
            ),
          ),
        ),
      ),
      showText: false,
    );
  }
}
