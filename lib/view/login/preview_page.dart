import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

class PreviewPage extends BaseStateful {
  PreviewPage();

  @override
  _PreviewPageState createState() => _PreviewPageState();
}

class _PreviewPageState extends BaseStatefulState<PreviewPage> {
  @override
  Widget build(BuildContext context) {
    return lScaffold(
        body: Stack(alignment: Alignment.center, children: [
      Image.asset(ILLUST_INTRO, width: 150),
      Column(mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Flexible(child: Padding(padding: EdgeInsets.symmetric(vertical: 60), child: Image.asset(IMG_LOGO, width: 120))),
        sb_h_05,
        Flexible(child: lText("어떤서비스가필요하신가요".tr(), style: st_b_26(fontWeight: FontWeight.w700), textAlign: TextAlign.center)),
        SizedBox(height: 180),
        Flexible(
            flex: 2,
            child: Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.center, children: [
              lInkWell(
                child: Container(
                  width: data.width(context, 0.5),
                  padding: padding_30,
                  color: color_00cfb5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      lText("부모님".tr(), style: st_22(fontWeight: FontWeight.w700)),
                      lText("아이를 믿고\n맡길 수 있는\n이웃 돌보미가\n필요해요.", style: TextStyle(fontSize: 15, height: 1.5, color: Colors.white), textAlign: TextAlign.center),
                      Lcons.fill_arrow(color: Colors.white, size: 40)
                    ],
                  ),
                ),
                onTap: () {
                  storageHelper.setUserType(USER_TYPE.mom_daddy);
                  Commons.pageToMain(context, routeHome);
                },
              ),
              lInkWell(
                  onTap: () {
                    storageHelper.setUserType(USER_TYPE.link_mom);
                    Commons.pageToMain(context, routeHome);
                  },
                  child: Container(
                    width: data.width(context, 0.5),
                    padding: padding_30,
                    color: color_b579c8,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        lText("돌보미".tr(), style: st_22(fontWeight: FontWeight.w700)),
                        lText("여유시간에\n이웃아이 돌보며\n소득의 성취감을\n느끼고싶어요.", style: TextStyle(fontSize: 15, height: 1.5, color: Colors.white), textAlign: TextAlign.center),
                        Lcons.fill_arrow(color: Colors.white, size: 40)
                      ],
                    ),
                  ))
            ]))
      ])
    ]));
  }
}
