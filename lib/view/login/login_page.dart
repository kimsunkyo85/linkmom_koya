import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/login_request.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/login/find/find_password_page.dart';
import 'package:linkmom/view/main/mypage/cs/cs_terms_view_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../link_page.dart';
import '../../main.dart';
import '../../route_name.dart';
import '../view_modules.dart';
import 'find/find_page.dart';

class LoginPage extends BaseStateful {
  LoginPage({this.id = ''});

  final String id;

  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends BaseStatefulState<LoginPage> {
  var flag = ViewFlag();
  var data = DataManager();
  late FocusNode _idFNode, _pwFNode;
  Color _idColor = color_b2b2b2;
  Color _pwColor = color_b2b2b2;

  @override
  void initState() {
    super.initState();
    _loadUsername();
    _idFNode = FocusNode();
    _pwFNode = FocusNode();
    super.onData(data);
    _idFNode.addListener(() {
      onUpDate();
    });
    _pwFNode.addListener(() {
      onUpDate();
    });
    DeepLinkManager(context);
  }

  @override
  void dispose() {
    super.dispose();
    _idFNode.dispose();
    _pwFNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
      context: context,
      key: data.scaffoldKey,
      resizeToAvoidBottomInset: false,
      body: lModalProgressHUD(
        flag.isLoading,
        DoubleBackToCloseApp(
          snackBar: SnackBar(
            content: lText("앱종료_메시지".tr(), style: stBack),
          ),
          child: Form(
            child: Container(
              height: MediaQuery.of(context).size.height,
              padding: padding_20_LTR,
              child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                Expanded(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onLongPress: Commons.isDebugMode
                          ? () {
                              Commons.setTestMode(callBack: () {});
                            }
                          : null,
                      child: SizedBox(
                        child: Image.asset(
                          IMG_LOGO,
                          width: 209,
                          alignment: Alignment.center,
                        ),
                      ),
                    ),
                    sb_h_50,
                    Login(
                      data: data,
                      flag: flag,
                      onIdChanged: (value) => onUpDate(),
                      onIdEditingComplete: (value) => onUpDate(),
                      onPwChanged: (value) => onUpDate(),
                      onPwEditingComplete: (value) => onUpDate(),
                    ),
                    _loginButton(),
                    _findButton(),
                  ],
                )),
                Padding(
                  padding: padding_15_TB,
                  child: TextButton(
                    onPressed: () => Commons.pageReplace(context, routeIntro),
                    child: lText("링크맘이궁금하세요".tr(), style: st_16(textColor: color_999999, decoration: TextDecoration.underline, fontWeight: FontWeight.w500)),
                  ),
                ),
                lBtnOutline("회원가입".tr(), style: st_16(fontWeight: FontWeight.bold, textColor: color_main), onClickAction: () => Commons.page(context, routeAgreement), sideColor: color_main),
                Padding(
                  padding: padding_15,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                        TextButton(
                          style: ButtonStyle(visualDensity: VisualDensity(horizontal: -4, vertical: -4)),
                          onPressed: () => Commons.page(context, routeCsTermsView, arguments: CsTermsViewPage(terms: Terms.total)),
                          child: lText("이용약관".tr(), style: st_13(textColor: color_cecece, decoration: TextDecoration.underline)),
                        ),
                        TextButton(
                          style: ButtonStyle(visualDensity: VisualDensity(horizontal: -4, vertical: -4)),
                          onPressed: () => Commons.page(context, routeCsTermsView, arguments: CsTermsViewPage(terms: Terms.privacy)),
                          child: lText("개인정보처리방침".tr(), style: st_13(textColor: color_cecece, decoration: TextDecoration.underline)),
                        ),
                      ]),
                    ],
                  ),
                )
              ]),
            ),
          ),
        ),
        isOnlyLoading: true,
      ),
    );
  }

  ///로그인 버튼 클릭시 이벤트
  Widget _loginButton() {
    return GestureDetector(
      onLongPressStart: data.tcId.text == "220324009@linkmom" || Commons.isDebugMode
          ? (_) {
              setState(() {
                showNormalDlg(
                  title: "로그인 아이디 선택(테스트용)",
                  closeable: true,
                  messageWidget: SizedBox(
                    width: widthDlg(context),
                    height: heightDlg(context),
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        ListTile(
                          title: lText("API서버 변경"),
                          onTap: () {
                            Commons.pagePop(context);
                            Commons.setTestMode();
                          },
                        ),
                        ListTile(
                          title: lText("ggumgi@naver.com"),
                          onTap: () {
                            data.tcId.text = "ggumbi@naver.com";
                            data.tcPassword.text = "fldzmaka1!";
                            data.id = data.tcId.text;
                            data.password = data.tcPassword.text;
                            data.formId.currentState!.validate();
                            data.formPw.currentState!.validate();
                            data.registerItem.idConfirm = true;
                            data.registerItem.passwordConfirm = true;
                            _confirmCheck();
                            Commons.pagePop(context);
                          },
                        ),
                        ListTile(
                          title: lText("kimsk85@naver.com"),
                          onTap: () {
                            data.tcId.text = "kimsk85@naver.com";
                            data.tcPassword.text = "fldzmaka1!";
                            data.id = data.tcId.text;
                            data.password = data.tcPassword.text;
                            data.formId.currentState!.validate();
                            data.formPw.currentState!.validate();
                            data.registerItem.idConfirm = true;
                            data.registerItem.passwordConfirm = true;
                            _confirmCheck();
                            Commons.pagePop(context);
                          },
                        ),
                        ListTile(
                          title: lText("linkmom@linkmom.co.kr"),
                          onTap: () {
                            data.tcId.text = "linkmom@linkmom.co.kr";
                            data.tcPassword.text = "1qa2ws#ed";
                            data.id = data.tcId.text;
                            data.password = data.tcPassword.text;
                            data.formId.currentState!.validate();
                            data.formPw.currentState!.validate();
                            data.registerItem.idConfirm = true;
                            data.registerItem.passwordConfirm = true;
                            _confirmCheck();
                            Commons.pagePop(context);
                          },
                        ),
                        ListTile(
                          title: lText("test2@gmail.com"),
                          onTap: () {
                            data.tcId.text = "test2@gmail.com";
                            data.tcPassword.text = "adjflkajfj!#4";
                            data.id = data.tcId.text;
                            data.password = data.tcPassword.text;
                            data.formId.currentState!.validate();
                            data.formPw.currentState!.validate();
                            data.registerItem.idConfirm = true;
                            data.registerItem.passwordConfirm = true;
                            _confirmCheck();
                            Commons.pagePop(context);
                          },
                        ),
                        ListTile(
                          title: lText("kimsk85@naver.com"),
                          onTap: () {
                            data.tcId.text = "kimsk85@naver.com";
                            data.tcPassword.text = "R";
                            data.id = data.tcId.text;
                            data.password = data.tcPassword.text;
                            data.formId.currentState!.validate();
                            data.formPw.currentState!.validate();
                            data.registerItem.idConfirm = true;
                            data.registerItem.passwordConfirm = true;
                            _confirmCheck();
                            Commons.pagePop(context);
                          },
                        ),
                        ListTile(
                          title: lText("luna0805@naver.com"),
                          onTap: () {
                            data.tcId.text = "luna0805@naver.com";
                            data.tcPassword.text = "R";
                            data.id = data.tcId.text;
                            data.password = data.tcPassword.text;
                            data.formId.currentState!.validate();
                            data.formPw.currentState!.validate();
                            data.registerItem.idConfirm = true;
                            data.registerItem.passwordConfirm = true;
                            _confirmCheck();
                            Commons.pagePop(context);
                          },
                        ),
                        ListTile(
                          title: lText("lhh7645@naver.com"),
                          onTap: () {
                            data.tcId.text = "lhh7645@naver.com";
                            data.tcPassword.text = "qwer1234!";
                            data.id = data.tcId.text;
                            data.password = data.tcPassword.text;
                            data.formId.currentState!.validate();
                            data.formPw.currentState!.validate();
                            data.registerItem.idConfirm = true;
                            data.registerItem.passwordConfirm = true;
                            _confirmCheck();
                            Commons.pagePop(context);
                          },
                        ),
                        ListTile(
                          title: lText("link@link.com"),
                          onTap: () {
                            data.tcId.text = "link@link.com";
                            data.tcPassword.text = "fldzmaka1!";
                            data.id = data.tcId.text;
                            data.password = data.tcPassword.text;
                            data.formId.currentState!.validate();
                            data.formPw.currentState!.validate();
                            data.registerItem.idConfirm = true;
                            data.registerItem.passwordConfirm = true;
                            _confirmCheck();
                            Commons.pagePop(context);
                          },
                        ),
                      ],
                    ),
                  ),
                );
              });
            }
          : null,
      onLongPressUp: () => flag.disableLoading(fn: () => onUpDate()),
      child: lBtn("로그인".tr(), isEnabled: _confirmCheck(), margin: padding_20_T, onClickAction: () => _requestLogin(), btnColor: color_main),
    );
  }

  ///아이디찾기, 비밀번호 찾기
  Widget _findButton() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          TextButton(
            style: ButtonStyle(alignment: Alignment.centerRight),
            onPressed: () {
              Commons.page(context, routeFindId, arguments: FindPage(pageType: 'id'));
            },
            child: lText(
              "아이디찾기".tr(),
              style: stFindBtn,
            ),
          ),
          Container(height: 15, child: lDividerVertical(color: color_dedede, thickness: 1)),
          TextButton(
            style: ButtonStyle(alignment: Alignment.centerLeft),
            onPressed: () {
              Commons.page(context, routeFindPw, arguments: FindPasswordPage(id: ""));
            },
            child: lText(
              "비밀번호찾기".tr(),
              style: stFindBtn,
            ),
          )
        ],
      ),
    );
  }

  ///로그인 확인 버튼 활성화
  bool _confirmCheck() {
    if (data.registerItem.isLoginConfirm()) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
    return flag.isConfirm;
  }

  void _loadUsername() async {
    try {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      var _username = _prefs.getString("saved_username") ?? "";
      var _remeberMe = _prefs.getBool("remember_me") ?? false;

      if (_remeberMe) {
        data.tcId.text = _username;
      }

      log.d('『GGUMBI』>>> _loadUsername : widget.id: ${widget.id},  <<< ');
    } catch (e) {
      log.e('『GGUMBI』>>> _loadUsername : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
  }

  Future<void> _requestLogin() async {
    flag.enableLoading(fn: () => onUpDate());
    LoginRequest req = LoginRequest(username: data.tcId.text.toString().trim(), password: data.tcPassword.text.toString().trim());
    apiHelper.requestLogin(req).then((response) async {
      try {
        if (response.getCode() == KEY_SUCCESS) {
          //2022/04/27 우선 마지막 모드로 로그인 할 수 있도록 한다.
          // if (response.getData().user!.is_user_linkmom) storageHelper.setUserType(USER_TYPE.link_mom);
          // if (response.getData().user!.is_user_momdady) storageHelper.setUserType(USER_TYPE.mom_daddy);
          Commons.doLoginFlow(context, '$LoginPage', response.getData(), callBack: () => {data.tcId.clear(), data.tcPassword.clear()});
          apiHelper.requestSettingsView().then((response) {
            if (response.getCode() == KEY_SUCCESS) {
              storageHelper.setSettings(response.getData());
            }
          });
        } else {
          if (response.getCode() == KEY_1401_UNAUTHORIZED || response.getCode() == 1402) {
            auth.setAutoLogin = false;
            showNormalDlg(
              message: response.getMsg(),
            );
          }
        }
        flag.disableLoading(fn: () => onUpDate());
      } catch (e) {
        flag.disableLoading(fn: () => onUpDate());
      }
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
    });
  }
}
