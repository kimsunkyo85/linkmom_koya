import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/register_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/firebase_analytics_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/login/register/register_complete_page.dart';

class RegisterInvitePage extends BaseStateful {
  final RegisterRequest registerData;
  final bool enableInvite;

  RegisterInvitePage({required this.registerData, this.enableInvite = true});

  @override
  _RegisterInvitePageState createState() => _RegisterInvitePageState();
}

class _RegisterInvitePageState extends BaseStatefulState<RegisterInvitePage> {
  final int _errAlreadyClose = 1918;
  final int _errInvite = 1919;

  JoinPath? _selectedPath;
  FocusNode _focus = FocusNode();
  bool _isValid = true;
  TextEditingController _inviteField = TextEditingController();
  late GlobalKey<FormState> _inviteKey = new GlobalKey<FormState>();
  String? _errorMsg;
  List<JoinPath> _items = [];

  @override
  void initState() {
    super.initState();
    _focus.addListener(() {
      onUpDate();
    });
    _items.addAll(JoinPath.values.sublist(1));
    _items.add(JoinPath.other);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (!widget.enableInvite) {
        _inviteField.text = '';
        _inviteKey.currentState!.validate();
      } else {
        _inviteField.text = widget.registerData.inviteCode;
      }
      onUpDate();
    });
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        context: context,
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: false,
        appBar: appBar("부가정보입력".tr()),
        body: Column(
          children: [
            lineStepIndicator(RegisterIndex.values.length - 1, RegisterIndex.invite.index),
            Form(
              key: _inviteKey,
              child: Expanded(
                child: Container(
                  padding: padding_20,
                  child: Column(
                    children: [
                      Container(
                          margin: padding_20_T,
                          alignment: Alignment.centerLeft,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              lText(
                                "가입경로선택".tr(),
                                style: stSubTitle,
                              ),
                              lText("*", style: st_22(textColor: color_ff3b30)),
                            ],
                          )),
                      sb_h_50,
                      Container(
                        width: data.width(context, 1),
                        padding: padding_10_B,
                        decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_dedede))),
                        child: lTextIcon(
                            align: MainAxisAlignment.spaceBetween,
                            title: _selectedPath != null ? _selectedPath!.name : "가입경로선택_힌트".tr(),
                            textStyle: st_16(textColor: _selectedPath != null ? color_222222 : color_b2b2b2),
                            rightIcon: Lcons.nav_bottom(color: color_222222),
                            onClick: () {
                              _selectedPath = _items.first;
                              onUpDate();
                              showBottomSelectBox(context, list: _items.map((e) => e.name).toList(), onChanged: (idx) {
                                _selectedPath = _items[idx];
                                onUpDate();
                              });
                            }),
                      ),
                      sb_h_50,
                      Container(
                        alignment: Alignment.centerLeft,
                        child: lText(
                          "추천인코드안내".tr(),
                          style: stSubTitle,
                        ),
                      ),
                      sb_h_30,
                      TextFormField(
                        controller: _inviteField,
                        keyboardType: TextInputType.text,
                        cursorColor: color_main,
                        focusNode: _focus,
                        validator: _validateInviteCode,
                        enabled: widget.enableInvite,
                        maxLength: 8,
                        onChanged: (value) {
                          _inviteKey.currentState!.validate();
                          onUpDate();
                        },
                        onEditingComplete: () {
                          _inviteField.text = _inviteField.text.toUpperCase();
                          _inviteKey.currentState!.validate();
                          focusClear(context);
                          onUpDate();
                        },
                        decoration: widget.enableInvite
                            ? underLineDecoration(
                                "초대코드입력".tr(),
                                focus: _focus.hasFocus,
                                errorStyle: st_14(textColor: color_ff3b30),
                              )
                            : bgDecoration(
                                "초대코드입력".tr(),
                                errorStyle: st_14(textColor: color_ff3b30),
                                bgColor: color_eeeeee,
                              ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            lBtn("회원가입완료".tr(), btnColor: color_main, isEnabled: _selectedPath != null, onClickAction: () {
              widget.registerData.joinPath = _selectedPath!.index;
              _inviteField.text = _inviteField.text.toUpperCase();
              widget.registerData.inviteCode = _inviteField.text;
              _tryRegister();
            }),
          ],
        ),
      ),
    );
  }

  String? _validateInviteCode(String? code) {
    if (!widget.enableInvite) {
      return "재가입초대코드안내".tr();
    } else if (!_isValid) {
      return "초대코드에러".tr();
    } else if (_errorMsg != null && _errorMsg!.isNotEmpty) {
      return _errorMsg;
    } else {
      return '';
    }
  }

  void _tryRegister() {
    flag.enableLoading(fn: () => onUpDate());
    apiHelper.requestRegister(widget.registerData).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        FbaManager.fba.logSignUp(signUpMethod: 'email');
        Commons.pageToMain(context, routeRegisterComplete,
            arguments: RegisterCompletePage(
              registerData: widget.registerData,
            ));
      } else {
        if (response.getCode() == _errAlreadyClose) {
          widget.registerData.inviteCode = '';
          _inviteField.clear();
          showNormalDlg(
            message: response.getMsg(),
            onClickAction: (a) => {if (a == DialogAction.yes) _tryRegister()},
          );
        } else if (response.getCode() == _errInvite) {
          _isValid = false;
          _errorMsg = response.getMsg();
          data.registerItem.registerErrors.username = response.getData().username;
          data.registerItem.registerErrors.first_name = response.getData().first_name;
          _inviteKey.currentState!.validate();
        }
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }
}
