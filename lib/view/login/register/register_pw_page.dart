import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/register_request.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../view_modules.dart';
import 'register_invite_page.dart';

class RegisterPwPage extends BaseStateful {
  final RegisterRequest registerData;
  final bool enableInvite;
  RegisterPwPage({required this.registerData, this.enableInvite = true});

  RegisterPwPageState createState() => RegisterPwPageState();
}

class RegisterPwPageState extends BaseStatefulState<RegisterPwPage> {
  @override
  void initState() {
    super.initState();
    flag.disablePasswordConfirm(fn: () => onUpDate());
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            context: context,
            resizeToAvoidBottomPadding: false,
            appBar: appBar("비밀번호입력".tr(), isClose: false),
            body: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
              lineStepIndicator(RegisterIndex.values.length - 1, RegisterIndex.pw.index),
              Expanded(
                  child: Container(
                      padding: padding_20,
                      child: ListView(children: [
                        Container(
                            margin: padding_30_TB,
                            alignment: Alignment.centerLeft,
                            child: lText(
                              "비밀번호입력타이틀".tr(),
                              style: stSubTitle,
                            )),
                        PasswordField(
                          onChanged: (value) {
                            widget.registerData.password = value;
                            _passwordConfirmCheck();
                            if (value.isNotEmpty) _passwordConfirmCheck();
                          },
                          onEditingComplete: (value) => _passwordConfirmCheck(),
                          data: data,
                          flag: flag,
                        ),
                        PasswordConfirmField(
                          onChanged: (value) {
                            widget.registerData.password2 = value;
                            if (value.isNotEmpty) _passwordConfirmCheck();
                          },
                          onEditingComplete: (value) => _passwordConfirmCheck(),
                          data: data,
                          flag: flag,
                        ),
                      ]))),
              lBtn("다음".tr(), onClickAction: () {
                focusClear(context);
                Commons.page(context, routeRegisterInvite, arguments: RegisterInvitePage(registerData: widget.registerData, enableInvite: widget.enableInvite));
              }, isEnabled: flag.isPasswordConfirm, btnColor: color_main)
            ])));
  }

  void _passwordConfirmCheck() {
    if (widget.registerData.password.isEmpty || widget.registerData.password2.isEmpty) {
      flag.disablePasswordConfirm(fn: () => onUpDate());
      return;
    }
    if (StringUtils.validatePwConfirm(widget.registerData.password, widget.registerData.password2) == null) {
      flag.enablePasswordConfirm(fn: () => onUpDate());
    } else {
      flag.disablePasswordConfirm(fn: () => onUpDate());
    }
  }
}
