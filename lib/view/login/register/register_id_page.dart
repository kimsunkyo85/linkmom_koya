import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/exists_request.dart';
import 'package:linkmom/data/network/models/register_request.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/login/register/register_pw_page.dart';
import 'package:linkmom/view/view_modules.dart';

import '../../../main.dart';

class RegisterIdPage extends BaseStateful {
  final RegisterRequest registerData;
  final bool enableInvite;

  RegisterIdPage({required this.registerData, this.enableInvite = true});

  RegisterIdPageState createState() => RegisterIdPageState();
}

class RegisterIdPageState extends BaseStatefulState<RegisterIdPage> {
  String _btnMsg = "중복확인".tr();
  String? _confirmMessage;

  @override
  void initState() {
    super.initState();
    flag.disableConfirm(fn: () => onUpDate());
    flag.disableExists(fn: () => onUpDate());
    data.tcId.clear();
  }

  @override
  void dispose() {
    flag.disableExists(fn: () => onUpDate());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        context: context,
        resizeToAvoidBottomPadding: false,
        appBar: appBar("아이디입력".tr(), isClose: false),
        body: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
          lineStepIndicator(RegisterIndex.values.length - 1, RegisterIndex.id.index),
          Expanded(
              child: Container(
            alignment: Alignment.centerLeft,
            padding: padding_20,
            child: ListView(children: [
              Container(
                  margin: padding_20_TB,
                  alignment: Alignment.centerLeft,
                  child: lText(
                    "메일_입력".tr(),
                    style: stSubTitle,
                  )),
              sb_h_25,
              Container(
                child: TextHighlight(
                  text: "웰컴포인트_안내".tr(),
                  term: "웰컴포인트".tr(),
                  textStyle: st_b_16(),
                  textStyleHighlight: st_16(textColor: color_main),
                ),
              ),
              sb_h_50,
              EmailAddressSelecter(
                onEditingComplete: (value) {
                  flag.disableExists(fn: () => onUpDate()); // reset duplicate check
                },
                onChanged: (value) {
                  if (StringUtils.validateEmail(value) == null) {
                    flag.enableConfirm(fn: () => onUpDate());
                  }
                  _confirmMessage = '';
                  flag.disableExists(fn: () => onUpDate()); // reset duplicate check
                },
                isValidMail: flag.isExists,
                message: _confirmMessage,
                data: data,
                flag: flag,
              ),
            ]),
          )),
          lBtn(_upDateBtnMsg(), isEnabled: flag.isExists || flag.isConfirm, onClickAction: () {
            _setRegisterData();
            if (flag.isExists) {
              Commons.page(context, routeRegisterPw, arguments: RegisterPwPage(registerData: widget.registerData, enableInvite: widget.enableInvite));
            } else {
              _confirmMail();
            }
          }, btnColor: color_main),
        ]));
  }

  void _confirmMail() {
    String mail = StringUtils.getEmail(data.tcId.text, data.dropValue);
    focusClear(context);
    flag.enableLoading(fn: () => onUpDate());
    apiHelper.requestExists(ExistsRequest(username: mail)).then((response) {
      _confirmMessage = response.getMsg();
      if (response.msg_cd == KEY_SUCCESS) {
        flag.enableExists(fn: () => onUpDate());
      } else {
        flag.disableExists(fn: () => onUpDate());
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }

  void _setRegisterData() {
    String id = StringUtils.getEmail(data.tcId.text, data.dropValue);
    widget.registerData.username = id;
    widget.registerData.email = id;
  }

  String _upDateBtnMsg() {
    if (flag.isConfirm && flag.isExists) {
      return "다음".tr();
    } else {
      return "중복확인".tr();
    }
  }
}
