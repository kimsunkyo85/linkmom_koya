import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/login_request.dart';
import 'package:linkmom/data/network/models/mypage_myinfo_save_request.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../../../data/network/models/register_request.dart';
import '../../../main.dart';
import '../../../route_name.dart';

class RegisterCompletePage extends BaseStateful {
  RegisterCompletePage({this.registerData});

  final RegisterRequest? registerData;

  RegisterCompletePageState createState() => RegisterCompletePageState();
}

class RegisterCompletePageState extends BaseStatefulState<RegisterCompletePage> {
  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () => Commons.pageClear(context, routeLogin),
        child: lModalProgressHUD(
            flag.isLoading,
            lScaffold(
              body: Container(
                  alignment: Alignment.center,
                  child: Column(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment: MainAxisAlignment.end, children: [
                    Flexible(
                      child: Container(
                        margin: padding_30,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            lText("회원가입완료메세지".tr(), style: st_24(textColor: color_main, fontWeight: FontWeight.bold), textAlign: TextAlign.center),
                            sb_h_10,
                            lText("회원가입완료_화면선택".tr(), style: st_16(textColor: color_545454)),
                          ],
                        ),
                      ),
                    ),
                    Image.asset(ILLUST_INTRO, width: 150),
                    Flexible(
                        flex: 1,
                        child: Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.center, children: [
                          lInkWell(
                            onTap: () => _selected(USER_TYPE.mom_daddy),
                            child: Container(
                              width: data.width(context, 0.5),
                              padding: padding_30,
                              color: color_00cfb5,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  lText("맘대디".tr(), style: st_22(fontWeight: FontWeight.w700)),
                                  lText("회원가입완료_맘대디".tr(), style: TextStyle(fontSize: 15, height: 1.5, color: Colors.white), textAlign: TextAlign.center),
                                  Lcons.fill_arrow(color: Colors.white, size: 40),
                                ],
                              ),
                            ),
                          ),
                          lInkWell(
                              onTap: () => _selected(USER_TYPE.link_mom),
                              child: Container(
                                width: data.width(context, 0.5),
                                padding: padding_30,
                                color: color_b579c8,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    lText("링크쌤".tr(), style: st_22(fontWeight: FontWeight.w700)),
                                    lText("회원가입완료_링크쌤".tr(), style: TextStyle(fontSize: 15, height: 1.5, color: Colors.white), textAlign: TextAlign.center),
                                    Lcons.fill_arrow(color: Colors.white, size: 40),
                                  ],
                                ),
                              ))
                        ]))
                  ])),
            )));
  }

  ///확인 버튼 클릭시 이벤트
  void _tryLogin(USER_TYPE type) {
    flag.disableTimer(fn: () => onUpDate());
    flag.enableLoading(fn: () => onUpDate());
    LoginRequest req = LoginRequest(
      username: widget.registerData!.username,
      password: widget.registerData!.password,
      is_user_linkmom: type == USER_TYPE.link_mom ? 1 : 0,
      is_user_momdady: 1, // momdaddy is always true
    );
    apiHelper.requestLogin(req).then((response) {
      if (response.getCode() == KEY_SUCCESS) {
        showNormalDlg(
          message: "회원가입_웰컴포인트안내".tr(),
          onClickAction: (a) {
            _requestSettingsView();
            Commons.pageClear(context, routeHome);
          },
        );
      } else {
        showNormalDlg(message: response.getMsg());
      }
      flag.disableLoading(fn: () => onUpDate());
    }).catchError((e) {
      flag.disableLoading(fn: () => onUpDate());
      showDlgException();
    });
  }

  void _selected(USER_TYPE type) {
    storageHelper.setUserType(type);
    if (widget.registerData == null) {
      _requestUserUpdate(type);
    } else {
      _tryLogin(type);
    }
  }

  _requestUserUpdate(USER_TYPE type) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      MyPageMyInfoSaveRequest req = MyPageMyInfoSaveRequest(is_user_linkmom: type == USER_TYPE.link_mom ? 1 : 0, is_user_momdady: type == USER_TYPE.mom_daddy ? 1 : 0);
      apiHelper.requestMyInfoSave(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          _requestSettingsView();
          Commons.pageClear(context, routeHome);
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  _requestSettingsView() {
    try {
      apiHelper.requestSettingsView().then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          response.getData().is_auth_login = true;
          auth.setAutoLogin = true;
          storageHelper.setSettings(response.getData());
        }
      });
    } catch (e) {}
  }
}
