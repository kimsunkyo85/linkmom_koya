import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/checkplus_response.dart';
import 'package:linkmom/data/network/models/exists_request.dart';
import 'package:linkmom/data/network/models/register_request.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/login/register/register_id_page.dart';
import 'package:linkmom/view/login/checkplus_view_page.dart';

import '../../../main.dart';
import '../../../route_name.dart';

class RegisterAuthPage extends BaseStateful {
  ///이용약관에서 넘어 온 값
  final RegisterRequest registerData;
  RegisterAuthPage({required this.registerData});

  RegisterAuthPageState createState() => RegisterAuthPageState();
}

class RegisterAuthPageState extends BaseStatefulState<RegisterAuthPage> {
  ///회원가입 입력폼 에러 체크 플래그

  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    // clearTcData(onUpDate);
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
          context: context,
          appBar: appBar(
            "본인인증".tr(),
            rightWidget: lInkWell(
              child: Container(width: 50, height: 10),
              onLongPress: () {
                if (!Commons.isDebugMode) return;
                showNormalDlg(
                    messageWidget: Column(children: [
                      lText("테스트계정에 쓸 핸드폰번호"),
                      TextField(
                        onChanged: (value) => widget.registerData.hpnumber = value,
                        keyboardType: TextInputType.number,
                      ),
                    ]),
                    onClickAction: (a) {
                      widget.registerData.first_name = '테스터';
                      widget.registerData.gender = '1';
                      widget.registerData.birth = '1990-09-09';
                      widget.registerData.nationalinfo = '1';
                      widget.registerData.telco = 'SKT';
                      widget.registerData.nice_ci = DateTime.now().toIso8601String();
                      widget.registerData.nice_di = DateTime.now().toIso8601String();
                      Commons.page(context, routeRegisterId, arguments: RegisterIdPage(registerData: widget.registerData));
                    });
              },
            ), // NOTE: for test
          ),
          resizeToAvoidBottomPadding: false,
          resizeToAvoidBottomInset: true,
          body: Container(
            child: Column(
              children: [
                lineStepIndicator(RegisterIndex.values.length - 1, RegisterIndex.auth.index),
                Expanded(child: CheckPlusView(
                  callback: (response) {
                    flag.enableLoading(fn: () => onUpDate());
                    _setRegisterData(response);
                    _requestDiExists(widget.registerData.nice_di);
                  },
                ))
              ],
            ),
          )),
    );
  }

  ///데이터 세팅
  _setRegisterData(NiceAuthData resp) {
    data.name = resp.name;
    data.mobile = resp.mobileno;

    widget.registerData.first_name = resp.name;

    ///본인인증 확인시 응답 값으로 세팅
    widget.registerData.gender = resp.gender;
    widget.registerData.birth = StringUtils.parseDateForNormal(resp.birthdate);
    widget.registerData.nationalinfo = resp.nationalinfo;
    widget.registerData.telco = resp.mobileco;
    widget.registerData.hpnumber = resp.mobileno;
    widget.registerData.nice_ci = resp.conninfo;
    widget.registerData.nice_di = resp.dupinfo;

    log.d('『GGUMBI』>>> _setRegisterData : widget.registerData: ${widget.registerData},  <<< ');
  }

  void _requestDiExists(String di) {
    try {
      apiHelper.requestDiExists(ExistsDiRequest(nice_di: di)).then((response) {
        flag.disableLoading(fn: () => onUpDate());
        if (response.getCode() == KEY_SUCCESS) {
          Commons.page(context, routeRegisterId, arguments: RegisterIdPage(registerData: widget.registerData, enableInvite: !response.getData().isRejoin));
        } else {
          showNormalDlg(message: response.getMsg(), onClickAction: (action) => Commons.pagePop(context));
        }
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}
