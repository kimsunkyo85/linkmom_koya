import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateless.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

class PolicyPage extends BaseStateless {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBar("이용약관".tr(), isClose: false),
        body: lScrollView(
          padding: padding_0,
          child: Container(
              margin: padding_20_LR,
              child: lText(
                '\n\n㈜ 링크맘 이용 약관 \n\n제 1 장 총칙\n\n제 1 조 (목 적)\n이 약관은 이용자가 ㈜링크맘(이하 “회사”)이 운영하는 인터넷 서비스 사이트 및 어플리케이션 (이하 “사이트”)을 통해 제공하는 인터넷 전자상거래 관련 서비스(이하 “서비스”)와 관련하여 회사와 이용자의 권리, 의무, 책임사항 및 서비스 이용절차에 관한 사항을 규정함을 목적으로 합니다.\n\n제 2 조 (용어의 정의)\n이 약관에서 사용하는 용어의 정의는 다음과 같습니다.\n① "서비스"란 회사가 아이 도우미를 구하려는 부모님회원과 도우미가 되려는 돌봄이회원들로부터 제공, 등록받은 자료를 정리하고 DB화하여 각 회원들이 맞춤형으로 중개를 받는 서비스입니다. \n② "회원"이란 회사에 개인정보를 제공하여 회원등록을 완료한 자로서 이 약관에 따라 회사와 서비스 이용계약을 체결한 자를 말합니다. "회원"은 "부모님회원"과 "돌봄이회원”으로 구분되며, 회사가 제공하는 서비스를 이용하는 모두를 말합니다. \n③ “부모님회원”이란 회사에 개인정보를 제공하고 회사가 정한 부모님 회원 가입 양식에 따라 가입 한 자로서 회사와 서비스 이용계약을 체결하고 회원 아이디(ID)를 부여받아 회사의 서비스를 이용하면서 원하는 맞춤 돌봄이를 찾기 위해 이용할 수 있는 자를 말합니다. \n④ “돌봄이회원”이란 회사에 개인정보를 제공하고, 회사가 정한 돌보미 회원 가입 양식에 따라 가입한 자로서 회사의 서비스 이용계약을 체결하고 회원 아이디(ID)를 부여받은 후, 각종 인증을 받을 수 있게 유도하며 회사심사 및 교육 절차를 이수하고 부모님회원에게 돌봄 서비스를 제공하고자 하는 자를 말합니다. \n⑤ “전환회원”이란 부모님회원이 돌봄이회원으로 전환이 가능하며, 반대로 돌봄이회원이 부모님회원으로 전환이 가능합니다. 단, 부모님회원에서 돌봄이회원으로 전환 시, 이용계약체결 및 회사가 원하는 심사는 이행해야 합니다.\n⑥ “돌봄신청”이란 부모님회원이 서비스를 통해 원하는 구체적인 정보를 찾아서 부모님회원이 정한 돌봄이와 아이를 만나서 서비스를 제공받을 수 있게 신청하는 것을 의미합니다.',
              )),
        )
        // WebView(initialUrl: "http://ggumbi.com/"),
        );
  }
}
