import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/register_request.dart';
import 'package:linkmom/data/providers/agreement_model.dart';
import 'package:linkmom/data/providers/model/agreement.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/login/agreement/widgets/agreement_header.dart';
import 'package:linkmom/view/login/agreement/widgets/agreement_list.dart';
import 'package:linkmom/view/login/register/register_auth_page.dart';
import 'package:provider/provider.dart';

import '../../../base/base_stateful.dart';
import '../../../main.dart';

class RegisterAgreementPage extends BaseStateful {
  final String code;
  final int joinPath;

  AgreementPageState createState() => AgreementPageState();

  RegisterAgreementPage({this.isAnimation = true, this.code = '', this.joinPath = 1});

  final bool isAnimation;
}

class AgreementPageState extends BaseStatefulState<RegisterAgreementPage> {
  RegisterRequest _registerData = RegisterRequest();

  AgreementModel _agreementModel = AgreementModel();

  void initList() {
    _agreementModel.createAgrrement(Agreement(title: "전체동의".tr(), completed: false, isItem: false));
    _agreementModel.addAgrrement(Agreement(title: _title(Terms.total, true), id: Terms.total, completed: false));
    _agreementModel.addAgrrement(Agreement(title: _title(Terms.privacy, true), id: Terms.privacy, completed: false));
    _agreementModel.addAgrrement(Agreement(title: _title(Terms.location, true), id: Terms.location, completed: false));
    _agreementModel.addAgrrement(Agreement(title: _title(Terms.refund, true), id: Terms.refund, completed: false));
    _agreementModel.addAgrrement(Agreement(title: _title(Terms.event, false), id: Terms.event, completed: false));
    Provider.of<AgreementModel>(context, listen: false).createAgrrement(_agreementModel.header);
    Provider.of<AgreementModel>(context, listen: false).addAll(_agreementModel.lists);
  }

  @override
  void initState() {
    super.initState();
    _registerData.joinPath = widget.joinPath;
    if (widget.code.isNotEmpty) _registerData.inviteCode = widget.code;
    initList();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final aggrementProvider = Provider.of<AgreementModel>(context, listen: true);
    _agreementCheck(aggrementProvider);

    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        resizeToAvoidBottomPadding: false,
        appBar: appBar("약관동의".tr(), isClose: false, onBack: () {
          Commons.pagePop(context, data: true);
        }),
        body: SafeArea(
          child: Consumer<AgreementModel>(
            builder: (context, value, child) => Column(
              children: <Widget>[
                lineStepIndicator(RegisterIndex.values.length - 1, RegisterIndex.agreement.index),
                Container(
                  margin: EdgeInsets.only(left: 20, top: 48, bottom: 46),
                  alignment: Alignment.centerLeft,
                  child: lText(
                    "약관안내".tr(),
                    style: stSubTitle,
                  ),
                ),
                AgreementHeader(agreement: value.header),
                Container(
                  margin: EdgeInsets.only(top: 26, bottom: 24),
                  width: data.width(context, 0.9),
                  child: Divider(color: color_eeeeee),
                ),
                Expanded(
                  child: AgreementList(lists: value.lists),
                ),
                _confirmButton(),
              ],
            ),
          ),
        ),
      ),
      showText: false,
    );
  }

  ///다음 버튼 클릭시 이벤트
  Widget _confirmButton() {
    return Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Container(
        alignment: Alignment.bottomCenter,
        child: lBtn("다음".tr(), isEnabled: flag.isConfirm, margin: padding_20, onClickAction: () {
          flag.disableLoading(fn: () => onUpDate());
          Commons.page(context, routeRegisterAuth, arguments: RegisterAuthPage(registerData: _registerData));
        }, btnColor: color_main),
      ),
    ]);
  }

  _agreementCheck(AgreementModel data) {
    data.lists.forEach((e) {
      switch (e.id) {
        case Terms.total:
          _registerData.aggement_serviceterms = e.completed ? AGREE : DISAGREE;
          break;
        case Terms.privacy:
          _registerData.aggement_otheruser = e.completed ? AGREE : DISAGREE;
          break;
        case Terms.location:
          _registerData.aggement_gps = e.completed ? AGREE : DISAGREE;
          break;
        case Terms.refund:
          _registerData.aggement_refund = e.completed ? AGREE : DISAGREE;
          break;
        case Terms.event:
          _registerData.aggement_event = e.completed ? AGREE : DISAGREE;
          break;
        default:
      }
    });
    if (data.lists.isNotEmpty && data.lists.sublist(0, data.lists.length - 1).where((e) => !e.completed).isEmpty) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
    log.d('『GGUMBI』>>> _agreementCheck : _registerData: $_registerData,  <<< ');
  }

  String _title(Terms title, bool require) {
    return '[' + (require ? "필수".tr() : "선택".tr()) + '] ' + title.name;
  }
}
