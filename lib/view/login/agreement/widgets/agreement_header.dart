import 'package:flutter/material.dart';
import 'package:linkmom/data/providers/agreement_model.dart';
import 'package:linkmom/data/providers/model/agreement.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:provider/provider.dart';

import '../../../../main.dart';

class AgreementHeader extends StatefulWidget {
  Agreement agreement;
  final contentMargin;
  final contentPadding;
  final titleMargin;
  final isVisible;
  Function? callBack;

  AgreementHeader({
    required this.agreement,
    this.contentMargin = padding_20_L,
    this.contentPadding = padding_02,
    this.titleMargin = padding_10_L,
    this.isVisible = false,
    this.callBack,
  });

  AgreementHeaderState createState() => AgreementHeaderState(
        agreement: this.agreement,
        contentMargin: this.contentMargin,
        contentPadding: this.contentPadding,
        titleMargin: this.titleMargin,
        isVisible: this.isVisible,
        callBack: this.callBack,
      );
}

class AgreementHeaderState extends State<AgreementHeader> {
  final Agreement agreement;
  final contentMargin;
  final contentPadding;
  final titleMargin;
  final isVisible;
  Function? callBack;
  bool _chkAll = false;

  AgreementHeaderState({
    required this.agreement,
    this.contentMargin,
    this.contentPadding,
    this.titleMargin,
    this.isVisible,
    this.callBack,
  });

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        lInkWell(
          onTap: () {
            agreement.toggleCompleted();
            Provider.of<AgreementModel>(context, listen: false).toggleList(agreement);
          },
          onFocusChange: (focus) {
            agreement.toggleCompleted();
            Provider.of<AgreementModel>(context, listen: false).toggleList(agreement);
          },
          child: Row(
            children: [
              Container(
                  margin: contentMargin,
                  decoration: BoxDecoration(shape: BoxShape.circle, color: agreement.completed ? color_00cfb5 : color_cecece),
                  child: Padding(
                      padding: contentPadding,
                      child: Lcons.check(
                        size: stAgrTitle.fontSize!,
                        color: Colors.white,
                        isEnabled: true,
                      ))),
              Container(
                  margin: titleMargin,
                  child: lText(
                    agreement.title,
                    style: stAgrTitle,
                  )),
            ],
          ),
        ),
        if (isVisible)
          Expanded(
            child: InkWell(
              onTap: () {
                log.d('『GGUMBI』>>> build ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
                setState(() {
                  callBack!(agreement.isVisible);
                  agreement.toggleVisible();
                  // Provider.of<AgreementModel>(context, listen: false).toggleList(agreement);
                });
              },
              child: Container(
                alignment: Alignment.centerRight,
                padding: padding_10_LR,
                child: Icon(!agreement.isVisible ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_up, size: 25, color: color_999999),
              ),
            ),
          ),
      ],
    );
  }
}
