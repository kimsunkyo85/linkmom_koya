import 'package:flutter/material.dart';
import 'package:linkmom/data/providers/model/agreement.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

import 'agreement_list_item.dart';

class AgreementList extends StatelessWidget {
  final List<Agreement> lists;
  final contentPadding;
  final double? itemExtent;

  AgreementList({required this.lists, this.contentPadding, this.itemExtent});

  @override
  Widget build(BuildContext context) {
    return lScrollbar(
        child: ListView(
      shrinkWrap: true,
      itemExtent: itemExtent,
      children: lists.map((agreement) => AgreementListItem(agreement: agreement, contentPadding: contentPadding)).toList(),
    ));
  }

  List<Widget> getChildrenList() {
    return lists.map((agreement) => AgreementListItem(agreement: agreement, contentPadding: contentPadding)).toList();
  }
}
