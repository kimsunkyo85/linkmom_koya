import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/providers/agreement_model.dart';
import 'package:linkmom/data/providers/model/agreement.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/cs/cs_terms_view_page.dart';
import 'package:provider/provider.dart';

class AgreementListItem extends BaseStateful {
  Agreement agreement;
  final contentPadding;

  AgreementListItem({required this.agreement, this.contentPadding});

  AgreementListItemState createState() => AgreementListItemState(agreement: this.agreement, contentPadding: this.contentPadding);
}

class AgreementListItemState extends BaseStatefulState<AgreementListItem> {
  final Agreement agreement;
  final contentPadding;
  bool isItem = true;

  AgreementListItemState({required this.agreement, this.contentPadding});

  bool isCheck = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    isCheck = agreement.completed;
    return GestureDetector(
      child: ListTile(
        contentPadding: contentPadding,
        leading: lInkWell(
          onTap: () {
            setState(() {
              isCheck = !isCheck;
              agreement.completed = isCheck;
            });
            Provider.of<AgreementModel>(context, listen: false).toggleList(agreement);
          },
          child: Container(
            alignment: Alignment.centerRight,
            width: 40, // margin 25, component size 15 = 40
            child: Lcons.check(color: color_00cfb5, disableColor: color_cecece, isEnabled: isCheck),
          ),
        ),
        title: lText(agreement.title, style: stAgree),
        trailing: IconButton(
          icon: Lcons.nav_right(
            color: color_cecece,
            size: stAgree.fontSize!,
          ),
          onPressed: () {
            agreement.toggleCompleted();
            Provider.of<AgreementModel>(context, listen: false).itemClick(agreement);
            Commons.page(context, routeCsTermsView, arguments: CsTermsViewPage(terms: agreement.id));
          },
        ),
      ),
    );
  }
}
