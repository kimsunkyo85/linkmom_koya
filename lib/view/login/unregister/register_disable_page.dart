import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/disable_request.dart';
import 'package:linkmom/data/network/models/disable_validate_response.dart';
import 'package:linkmom/data/network/models/withdrawal_response.dart';
import 'package:linkmom/data/storage/model/bank_model.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/login/unregister/register_disable_reason_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/recipe_view.dart';

class RegisterDisablePage extends BaseStateful {
  final DisableData data;

  RegisterDisablePage({required this.data});

  @override
  _RegisterDisablePageState createState() => _RegisterDisablePageState();
}

class _RegisterDisablePageState extends BaseStatefulState<RegisterDisablePage> {
  bool _checked = false;
  BankModel? _selectedBank;
  TextEditingController _accountCtrl = TextEditingController();
  WithdrawalData get _account => storageHelper.accountInfo;
  List<BankModel> get _bankList => storageHelper.bankList;
  DisableRequest _req = DisableRequest();

  @override
  void initState() {
    super.initState();
    flag.enableLoading(fn: onUpDate);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      try {
        if (_account.bankAccount.isEmpty) await storageHelper.getAccount();
        _accountCtrl.text = _account.bankAccount;
        if (_account.bankName.isNotEmpty) {
          _selectedBank = _bankList.singleWhere((b) => b.code == _account.bankName);
        }
        if (_bankList.isEmpty) await storageHelper.getBankListWithoutPath();
      } catch (e) {
        log.e("error: $e");
      } finally {
        flag.disableLoading(fn: onUpDate);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        context: context,
        appBar: appBar("탈퇴하기".tr(), hide: false),
        body: Container(
            child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Expanded(
            child: lScrollView(
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                sb_h_20,
                TypeComponent(
                  type: "보유캐시".tr(),
                  typeStyle: st_b_16(),
                  data: StringUtils.formatPay(widget.data.linkCash),
                  dataStyle: st_b_16(fontWeight: FontWeight.w700),
                  icon: Padding(
                    padding: padding_03_L,
                    child: Lcons.cash(isEnabled: true),
                  ),
                  isExpanded: true,
                ),
                sb_h_10,
                TypeComponent(
                  type: "보유포인트_전액소멸".tr(),
                  typeStyle: st_b_16(),
                  data: StringUtils.formatPay(widget.data.linkPoint),
                  dataStyle: st_b_16(fontWeight: FontWeight.w700),
                  icon: Padding(padding: padding_03_L, child: Lcons.point()),
                  isExpanded: true,
                ),
                sb_h_10,
                TypeComponent(
                  type: "정산해줘야하는금액".tr(),
                  typeStyle: st_b_16(),
                  data: StringUtils.formatPay(widget.data.toGiveCash),
                  dataStyle: st_b_16(fontWeight: FontWeight.w700),
                  icon: Padding(padding: padding_03_L, child: Lcons.cash(isEnabled: true)),
                  isExpanded: true,
                ),
                if (widget.data.toGiveCash > 0) lText("해당금액이링크쌤에게정산되고탈퇴처리됩니다".tr(), style: st_11(textColor: color_ff3b30)),
                sb_h_10,
                TypeComponent(
                  type: "정산받아야하는금액".tr(),
                  typeStyle: st_b_16(),
                  data: StringUtils.formatPay(widget.data.takeCash),
                  dataStyle: st_b_16(fontWeight: FontWeight.w700),
                  icon: Padding(padding: padding_03_L, child: Lcons.cash(isEnabled: true)),
                  isExpanded: true,
                ),
                if (widget.data.takeCash > 0) lText("맘대디로부터정산받을금액이남아있습니다".tr(), style: st_11(textColor: color_ff3b30)),
                if (!_isHide)
                  Column(children: [
                    lDivider(color: color_eeeeee, padding: padding_30_TB),
                    RoundData(
                      title: "예금주_본인명의계좌만가능".tr(),
                      style: st_13(textColor: color_545454, fontWeight: FontWeight.w500),
                      bodyWidget: lText(widget.data.accountHolder, style: st_15(textColor: color_999999)),
                    ),
                    Container(
                        margin: padding_10_B,
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                          Padding(
                            padding: padding_10_B,
                            child: lText("은행명".tr(), style: st_13(textColor: color_545454, fontWeight: FontWeight.w500)),
                          ),
                          Container(
                            height: kMinInteractiveDimension,
                            alignment: Alignment.center,
                            padding: padding_15,
                            decoration: BoxDecoration(border: Border.all(color: color_dedede), borderRadius: BorderRadius.circular(8)),
                            child: DropdownButton<BankModel>(
                              isDense: true,
                              isExpanded: true,
                              dropdownColor: Colors.white,
                              icon: Lcons.nav_bottom(color: color_545454),
                              underline: Container(),
                              iconSize: 30,
                              hint: lText("은행을선택해주세요".tr(), style: st_b_15()),
                              items: _bankList.map((e) => DropdownMenuItem<BankModel>(value: e, child: lText(e.name, style: st_b_15()))).toList(),
                              value: _selectedBank,
                              onChanged: (value) {
                                _selectedBank = value;
                                onUpDate();
                              },
                            ),
                          ),
                        ])),
                    RoundData(
                      title: "계좌번호".tr(),
                      style: st_13(textColor: color_545454, fontWeight: FontWeight.w500),
                      bodyWidget: TextField(
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(borderSide: BorderSide.none),
                          contentPadding: padding_0,
                          isDense: true,
                          hintText: "계좌번호를입력해주세요".tr(),
                          hintStyle: st_15(textColor: color_cecece),
                        ),
                        maxLines: 1,
                        controller: _accountCtrl,
                        onChanged: (value) => onConfirmBtn(),
                      ),
                    ),
                  ]),
                lDivider(padding: padding_20_TB),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                      children: List.generate(
                          7,
                          (index) => lDingbatText(
                                text: "회원탈퇴_안내_${index + 1}".tr(),
                                dingbatColor: color_545454,
                                dingbatSize: 2,
                                style: st_b_13(),
                                overflow: TextOverflow.fade,
                                width: Commons.width(context, 1) - 50,
                              ))),
                ),
                Padding(
                  padding: padding_20_TB,
                  child: lIconText(
                      icon: Lcons.check_circle_outline(color: Commons.getColor(), disableColor: color_dedede, isEnabled: _checked, size: 25),
                      title: "위안내사항을모두확인".tr(),
                      textStyle: st_b_14(),
                      onClick: () {
                        focusClear(context);
                        _checked = !_checked;
                        if (_checked) _showAlert();
                        onConfirmBtn();
                      }),
                ),
              ]),
            ),
          ),
          lBtn(
            "확인".tr(),
            isEnabled: flag.isConfirm,
            onClickAction: () => _moveReasons(_req),
          )
        ])),
      ),
    );
  }

  void _showAlert() {
    if (widget.data.takeClaim) {
      showNormalDlg(
        context: context,
        message: "정산보류상태입니다".tr(),
        onClickAction: (a) => {},
      );
    } else if (widget.data.toGiveClaim) {
      showNormalDlg(
          context: context,
          message: "정산보류상태입니다".tr(),
          btnRight: "정산보류취소하기".tr(),
          btnLeft: "확인".tr(),
          onClickAction: (a) {
            switch (a) {
              case DialogAction.yes:
                storageHelper.setUserType(USER_TYPE.mom_daddy);
                Commons.pageToMain(context, routeMomdadyDiary);
                break;
              default:
            }
          });
    } else if (widget.data.takeCash > 0) {
      showNormalDlg(
        context: context,
        message: "맘대디로부터정산받을".tr(),
        onClickAction: (a) => {},
      );
    } else if (widget.data.toGiveCash > 0) {
      showNormalDlg(
        context: context,
        message: "링크쌤에게정산이".tr(),
        btnLeft: "취소".tr(),
        btnRight: "탈퇴하기".tr(),
        onClickAction: (a) => {if (a == DialogAction.yes) _moveReasons(_req)},
      );
    } else if (_accountCtrl.text.isEmpty && widget.data.linkCash > 0) {
      showNormalDlg(message: "보유캐시가있습니다".tr());
    } else if (widget.data.isCareIng) {
      showNormalDlg(message: "진행중인돌봄이".tr());
    }
    onUpDate();
  }

  @override
  void onConfirmBtn() {
    if (_isEnable) {
      /// NOTE: can't disable
      _checked = false;
      flag.disableConfirm(fn: () => onUpDate());
    } else {
      flag.enableConfirm(fn: () => onUpDate());
    }
    super.onConfirmBtn();
  }

  void _moveReasons(DisableRequest req) {
    if (_selectedBank != null) {
      req.account_bank = _selectedBank!.code;
      req.account = _accountCtrl.text;
    }
    Commons.page(context, routeRegisterDisableReason, arguments: RegisterDisableReasonPage(data: widget.data, req: req));
  }

  bool get _isEnable => widget.data.takeClaim || widget.data.toGiveClaim || widget.data.takeCash > 0 || widget.data.toGiveCash > 0 || !_checked || (widget.data.linkCash > 0 && _accountCtrl.text.isEmpty) || widget.data.isCareIng;

  bool get _isHide => widget.data.linkCash < 1 && widget.data.takeCash < 1;
}
