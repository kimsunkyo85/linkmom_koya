import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';

class RegisterDisableCompletePage extends BaseStateful {
  @override
  _RegisterDisableCompleatPageState createState() =>
      _RegisterDisableCompleatPageState();
}

class _RegisterDisableCompleatPageState
    extends BaseStatefulState<RegisterDisableCompletePage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: lScaffold(
          appBar: appBar("탈퇴하기".tr(), hide: false, isBack: false),
          body: Container(
              child: Column(
            children: [
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  lText(
                    "탈퇴완료메세지1".tr(),
                    style: st_22(textColor: color_main, fontWeight: FontWeight.bold),
                  ),
                  lText("탈퇴완료메세지2".tr(), style: stSubText),
                ],
              )),
              lBtn(
                "확인".tr(),
                btnColor: color_main,
                onClickAction: () => Commons.pageClear(context, routeLogin),
              ),
            ],
          ))),
    );
  }
}
