import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/disable_request.dart';
import 'package:linkmom/data/network/models/disable_validate_response.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/view/board/board_view.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/content_view.dart';

class RegisterDisableReasonPage extends BaseStateful {
  final DisableData data;
  final DisableRequest req;

  RegisterDisableReasonPage({required this.data, required this.req});
  @override
  _RegisterDisablePageState createState() => _RegisterDisablePageState();
}

enum ReasonMessage { reason0, reason1, reason2, reason3, reason4 }

class _RegisterDisablePageState extends BaseStatefulState<RegisterDisableReasonPage> {
  List<DisableReason> _selectedReason = [];

  TextEditingController _otherReason = TextEditingController();

  @override
  void initState() {
    super.initState();
    if (widget.req.reason == null) {
      widget.req.reason = [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        context: context,
        appBar: appBar("탈퇴".tr(), hide: false),
        body: Column(children: [
          Expanded(
            child: lScrollView(
                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              sb_h_20,
              contentView(data: ContentData(content: "탈퇴사유_타이틀".tr(), contentStyle: st_b_18(fontWeight: FontWeight.w700))),
              sb_h_15,
              lText("회원탈퇴_사유".tr(), overflow: TextOverflow.clip),
              sb_h_30,
              lDivider(color: color_eeeeee),
              Container(
                  padding: padding_20_TB,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: widget.data.reason!
                        .map((e) => Padding(
                              padding: padding_10_B,
                              child: lIconText(
                                padding: padding_05_L,
                                onClick: () {
                                  if (_selectedReason.contains(e)) {
                                    _selectedReason.remove(e);
                                  } else {
                                    _selectedReason.add(e);
                                  }
                                  if (!_isSelectedOther) _otherReason.clear();
                                  onConfirmBtn();
                                },
                                icon: Lcons.check_circle(isEnabled: _selectedReason.contains(e), color: color_main, disableColor: color_cecece),
                                title: e.text,
                                textStyle: st_b_15(),
                              ),
                            ))
                        .toList(),
                  )),
              lRoundContainer(
                padding: padding_0,
                bgColor: Colors.transparent,
                borderColor: color_dedede,
                child: BoardWriteView(
                  leftCount: false,
                  hint: "탈퇴사유를입력".tr(),
                  controller: _otherReason,
                  update: () => onConfirmBtn(),
                  useImage: false,
                  padding: padding_20_LRB,
                ),
              )
            ])),
          ),
          lBtn("다음".tr(), isEnabled: flag.isConfirm, onClickAction: () => _requestDisable(widget.req), btnColor: color_main),
        ]),
      ),
    );
  }

  bool get _isSelectedOther => _selectedReason.contains(widget.data.reason!.last);

  @override
  void onConfirmBtn() {
    if (_isSelectedOther && _otherReason.text.isEmpty) {
      flag.disableConfirm(fn: onUpDate);
    } else if (_selectedReason.isNotEmpty) {
      flag.enableConfirm(fn: onUpDate);
    } else {
      flag.disableConfirm(fn: onUpDate);
    }
  }

  Future<void> _requestDisable(DisableRequest req) async {
    try {
      flag.enableLoading(fn: () => onUpDate());
      DisableRequest request = DisableRequest(
        other_reason: _otherReason.text,
        reason: _selectedReason.map((e) => e.id).toSet().toList(),
        account_bank: req.account_bank,
      );
      request.account = await encryptHelper.encodeData(req.account);
      apiHelper.requestDisable(request).then((response) async {
        if (response.getCode() == KEY_SUCCESS) {
          auth.setUserAllClear();
          Commons.pageToMain(context, routeRegisterDisableCompleat);
        } else {
          showNormalDlg(
              context: context,
              message: response.getMsg().isEmpty ? "시스템에러".tr() : response.getMsg(),
              onClickAction: (a) {
                if (a == DialogAction.yes) {
                  Commons.pageToMain(context, routeRegisterDisablePw);
                }
              });
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) {
        log.e({
          '--- TITLE    ': '--------------- ERR ---------------',
          '--- DATA     ': e,
        });
        return flag.disableLoading(fn: () => onUpDate());
      });
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }
}
