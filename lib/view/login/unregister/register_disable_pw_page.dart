import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/disable_validate_request.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/view/login/unregister/register_disable_page.dart';
import 'package:linkmom/view/view_modules.dart';

class RegisterDisablePwPage extends BaseStateful {
  @override
  _RegisterDisablePwPageState createState() => _RegisterDisablePwPageState();
}

class _RegisterDisablePwPageState extends BaseStatefulState<RegisterDisablePwPage> {
  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      lScaffold(
        context: context,
        appBar: appBar("탈퇴하기".tr(), hide: false),
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 20),
          child: Column(children: [
            sb_h_30,
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "비밀번호입력메세지".tr(),
                    style: stNext,
                  ),
                  sb_h_40,
                  PasswordField(
                      data: data,
                      flag: flag,
                      focusedColor: Commons.getColor(),
                      onEditingComplete: (value) {
                        _confirm(value);
                      },
                      onChanged: (value) {
                        _confirm(value);
                      }),
                ],
              ),
            ),
            lBtn("다음".tr(), isEnabled: flag.isConfirm, margin: padding_20_TB, onClickAction: () => _requestPwCheck(data.tcPassword.text)),
          ]),
        ),
      ),
    );
  }

  void _requestPwCheck(String password) {
    try {
      flag.enableLoading(fn: () => onUpDate());
      DisableValidateRequest req = DisableValidateRequest(password: password);
      apiHelper.reqeustDisableValidate(req).then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.page(context, routeRegisterDisable, arguments: RegisterDisablePage(data: response.getData()));
        } else {
          showNormalDlg(message: response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
    } catch (e) {
      flag.disableLoading(fn: () => onUpDate());
    }
  }

  void _confirm(String value) {
    if (StringUtils.validatePw(value) == null) flag.enableConfirm(fn: () => onUpDate());
  }
}
