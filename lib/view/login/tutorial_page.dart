import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:expandable_page_view/expandable_page_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:linkmom/data/network/models/index_response.dart';
import 'package:linkmom/data/storage/flag_manage.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/svg_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/main/home_view.dart';
import 'package:linkmom/view/main/mypage/mypage.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../lcons.dart';

class TutorialPage extends StatefulWidget {
  TutorialPage({Key? key}) : super(key: key);

  @override
  _TutorialPageState createState() => _TutorialPageState();
}

class _TutorialPageState extends State<TutorialPage> {
  List<Widget> _page = [
    TutorialMypage(),
    TutorialMHome(),
    TutorialLHome(),
    TutorialLast(),
  ];

  late PageController _pageCtrl;

  @override
  void initState() {
    super.initState();
    _pageCtrl = PageController();
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        body: DoubleBackToCloseApp(
      snackBar: SnackBar(
        content: lText("앱종료_메시지".tr(), style: stBack),
      ),
      child: lScrollView(
        padding: padding_0,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              child: ExpandablePageView(controller: _pageCtrl, children: _page),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.08,
              padding: padding_10,
              color: Colors.white,
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                TextButton(
                    onPressed: () {
                      Flags.addFlag(Flags.KEY_SHOW_LOGIN_TUTORIAL, isShown: true);
                      Commons.pageClear(context, routeHome);
                    },
                    child: lText("다시보지않기".tr(), style: st_15(textColor: color_b2b2b2, fontWeight: FontWeight.w500))),
                SmoothPageIndicator(
                  controller: _pageCtrl,
                  count: _page.length,
                  effect: ExpandingDotsEffect(
                    spacing: 8,
                    dotWidth: 6,
                    dotHeight: 6,
                    strokeWidth: 16,
                    activeDotColor: color_main,
                    dotColor: color_dedede,
                  ),
                ),
                TextButton(
                  onPressed: () => Commons.pageClear(context, routeHome),
                  child: lText("닫기".tr(), style: st_15(textColor: color_main, fontWeight: FontWeight.w500)),
                ),
              ]),
            ),
          ],
        ),
      ),
    ));
  }
}

class TutorialMypage extends StatefulWidget {
  TutorialMypage({Key? key}) : super(key: key);

  @override
  _TutorialMypageState createState() => _TutorialMypageState();
}

class _TutorialMypageState extends State<TutorialMypage> with SingleTickerProviderStateMixin {
  bool _enable = false;
  late AnimationController _controller;
  late Tween<Offset> _animOffset;
  late CurvedAnimation _curve;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(duration: const Duration(milliseconds: 700), vsync: this);
    _animOffset = Tween<Offset>(begin: Offset.zero, end: Offset(0.5, 0));
    _controller.repeat(period: Duration(seconds: 1));
    _curve = CurvedAnimation(curve: Curves.fastOutSlowIn, parent: _controller);
  }

  @override
  void dispose() {
    _controller.dispose();
    _curve.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        Column(
          children: [
            SizedBox(height: kTextTabBarHeight + 2),
            Container(
              height: kToolbarHeight,
              padding: padding_20_LR,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Lcons.settings(size: 24, color: color_b2b2b2), Lcons.nav_close(size: 24)],
              ),
            ),
            Container(
              height: kToolbarHeight,
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              color: _enable ? color_linkmom : color_main,
            ),
            Column(children: [
              Container(
                color: Colors.white,
                padding: padding_20,
                child: Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: [
                  lProfileAvatar(null, radius: 35),
                  sb_w_20,
                  Container(
                      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                    lIconText(
                      title: _enable ? "링크쌤_0".tr() : "맘대디_0".tr(),
                      textStyle: st_b_15(textColor: _enable ? color_linkmom : color_main, fontWeight: FontWeight.w500),
                      isExpanded: false,
                      isLeft: false,
                      padding: padding_0,
                      icon: lBtnWrapOutLine(
                        "등급".tr(),
                        textStyle: st_09(textColor: _enable ? color_linkmom : color_main),
                        padding: padding_07_LR,
                        height: 20,
                        sideEnableColor: _enable ? color_linkmom : color_main,
                      ),
                    ),
                    lIconText(
                      title: "박보희",
                      isExpanded: false,
                      isLeft: false,
                      icon: Lcons.nav_right(size: 15, color: color_cecece),
                      textStyle: st_b_18(fontWeight: FontWeight.w700),
                      padding: padding_05_R,
                    ),
                    sb_h_05,
                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                      TextHighlight(text: "${"자녀".tr()} 2${"명".tr()}", term: '2', textStyle: st_13(textColor: color_545454), textStyleHighlight: st_14(textColor: color_545454, fontWeight: FontWeight.w500)),
                      Container(height: 14, margin: padding_05_LR, child: lDividerVertical(color: color_dedede, thickness: 1)),
                      lText("워킹맘대디".tr(), overflow: TextOverflow.ellipsis, style: st_13(textColor: color_545454)),
                      Container(height: 14, margin: padding_05_LR, child: lDividerVertical(color: color_dedede, thickness: 1)),
                      TextHighlight(text: '${"후기".tr()} 12${"개".tr()}', term: '12', textStyle: st_13(textColor: color_545454), textStyleHighlight: st_14(textColor: color_545454, fontWeight: FontWeight.w500)),
                    ]),
                    sb_h_05,
                    lIconText(
                      title: "돌봄신청서보기".tr(),
                      isExpanded: false,
                      isLeft: false,
                      icon: Lcons.nav_right(size: 13, color: color_cecece),
                      textStyle: st_14(textColor: _enable ? color_linkmom : color_main, fontWeight: FontWeight.w700),
                      padding: padding_05_R,
                    ),
                  ])),
                ]),
              ),
              Container(
                  margin: padding_20_LRB,
                  padding: padding_20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: color_eeeeee.withOpacity(0.42),
                  ),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                    Column(children: [
                      lText("보유캐시".tr(), style: st_b_15(fontWeight: FontWeight.w500)),
                      sb_h_05,
                      lIconText(
                        title: '0',
                        icon: Lcons.cash(size: 16, isEnabled: true),
                        isExpanded: false,
                        padding: padding_06_R,
                        isLeft: false,
                        textStyle: st_16(textColor: _enable ? color_linkmom : color_main, fontWeight: FontWeight.w700),
                      ),
                    ]),
                    Container(height: 30, child: lDividerVertical(thickness: 1, color: color_dedede)),
                    Column(children: [
                      lText("보유포인트".tr(), style: st_b_15(fontWeight: FontWeight.w500)),
                      sb_h_05,
                      lIconText(title: '0', icon: Lcons.point(size: 16), isExpanded: false, padding: padding_06_R, isLeft: false, textStyle: st_16(textColor: _enable ? color_linkmom : color_main, fontWeight: FontWeight.w700)),
                    ]),
                  ])),
            ]),
            lDivider(thickness: 8.0, color: color_61e5e5ea),
            Container(
                padding: padding_20,
                child: Column(children: [
                  if (_enable)
                    Container(
                        decoration: BoxDecoration(border: Border.all(color: color_eeeeee), borderRadius: BorderRadius.circular(16)),
                        padding: padding_20,
                        margin: padding_20_B,
                        child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                          Row(children: [
                            lIconText(title: "총돌봄시간".tr(), isExpanded: false, textStyle: st_15(textColor: color_999999), icon: Lcons.total_time()),
                            Padding(
                              padding: padding_05_L,
                              child: TextHighlight(text: '0${"시간".tr()}', term: '0', textStyleHighlight: st_b_16(fontWeight: FontWeight.w700), textStyle: st_b_15()),
                            ),
                          ]),
                          sb_h_10,
                          Row(children: [
                            lIconText(title: "총돌봄소득".tr(), isExpanded: false, textStyle: st_15(textColor: color_999999), icon: Lcons.total_income()),
                            Padding(
                              padding: padding_05_L,
                              child: TextHighlight(text: '0${"원".tr()}', term: '0', textStyleHighlight: st_b_16(fontWeight: FontWeight.w700), textStyle: st_b_15()),
                            ),
                          ])
                        ])),
                  Container(
                      padding: padding_10_LTR,
                      child: AspectRatio(
                        aspectRatio: Commons.ratio(7.5),
                        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                          _enable ? TopIconButton(icon: Lcons.certified_center(size: 40), title: "인증센터".tr(), onClick: () => {}) : TopIconButton(icon: Lcons.my_child(size: 40), title: "우리아이".tr(), onClick: () => {}),
                          TopIconButton(icon: Lcons.review_management(size: 40, isEnabled: _enable), title: "후기관리".tr(), onClick: () => {}),
                          TopIconButton(icon: Lcons.like_list(size: 40, isEnabled: _enable), title: "관심목록".tr(), onClick: () => {}),
                          TopIconButton(icon: Lcons.payment_details(size: 40, isEnabled: _enable), title: "결제내역".tr(), onClick: () => {}),
                        ]),
                      )),
                ])),
            lDivider(thickness: 8.0, color: color_61e5e5ea),
            Container(
                padding: padding_20,
                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  lIconText(
                    title: "돌봄관리".tr(),
                    icon: Lcons.info(color: color_cecece, isEnabled: true, size: 25),
                    textStyle: st_b_18(fontWeight: FontWeight.w700),
                    isExpanded: false,
                    padding: padding_02_R,
                    onClick: () => {},
                    isLeft: false,
                  ),
                  sb_h_20,
                  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: !_enable
                          ? [
                              Flexible(child: lIconText(title: "돌봄일기_정산".tr(), icon: Lcons.care_diary(size: 30), textStyle: st_b_14(), isExpanded: false, padding: padding_10_L, onClick: () => {})),
                              Flexible(child: lIconText(title: "투약의뢰서".tr(), icon: Lcons.medication_form(size: 30), textStyle: st_b_14(), isExpanded: false, padding: padding_10_L, onClick: () => {}))
                            ]
                          : [
                              Flexible(child: lIconText(title: "돌봄일기작성".tr(), icon: Lcons.care_diary(size: 30, isEnabled: _enable), textStyle: st_b_14(), isExpanded: false, padding: padding_10_L, onClick: () => {})),
                              Flexible(child: lIconText(title: "출퇴근알림".tr(), icon: Lcons.commuting_to_work(size: 30), textStyle: st_b_14(), isExpanded: false, padding: padding_10_L, onClick: () => {}))
                            ])
                ])),
            lDivider(thickness: 8.0, color: color_61e5e5ea),
            // menu
            Container(
                padding: padding_20_LR,
                child: Column(
                  children: [
                    _menuComponent("나의성향".tr(), movePage: () => {}),
                    _menuComponent("인증센터".tr(), movePage: () => {}),
                    _menuComponent("계정관리".tr(), movePage: () => {}),
                    _menuComponent("인출신청".tr(), movePage: () => {}),
                    _menuComponent("벌점스티커".tr(), movePage: () => {}),
                  ],
                )),
          ],
        ),
        Container(
          width: Commons.width(context, 1),
          height: Commons.height(context, 1),
          decoration: BoxDecoration(
            color: color_060606.withOpacity(0.6),
          ),
        ),
        Container(
          margin: padding_30_TB,
          padding: padding_20_TB,
          child: Column(
            children: [
              Padding(
                padding: padding_15,
                child: lText("마이페이지".tr(), style: st_20(fontWeight: FontWeight.w500)),
              ),
              Container(
                height: kToolbarHeight,
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                color: _enable ? color_linkmom : color_main,
                child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  lText("맘대디".tr(), style: st_16(fontWeight: FontWeight.w500, textColor: !_enable ? Colors.white : Colors.white.withAlpha(108))),
                  Padding(
                      padding: padding_10_LR,
                      child: FlutterSwitch(
                          width: 60,
                          padding: 2.0,
                          toggleColor: _enable ? color_linkmom : color_main,
                          activeColor: Colors.white,
                          inactiveColor: Colors.white,
                          toggleSize: 30,
                          value: _enable,
                          onToggle: (value) {
                            setState(() {
                              _enable = value;
                            });
                          })),
                  lText("링크쌤".tr(), style: st_16(fontWeight: FontWeight.w500, textColor: _enable ? Colors.white : Colors.white.withAlpha(108))),
                ]),
              ),
              SlideTransition(position: _animOffset.animate(_curve), child: Lcons.handsign(size: 88)),
              Container(
                  width: 300,
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      SvgPicture.asset(SVG_TALK_BUBBLE),
                      Padding(
                        padding: padding_20,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                style: st_b_13(),
                                children: [
                                  TextSpan(text: "튜토리얼_1_1".tr()),
                                  TextSpan(text: "튜토리얼_1_2".tr()),
                                  TextSpan(text: "튜토리얼_1_3".tr(), style: st_13(textColor: color_main)),
                                  TextSpan(text: "튜토리얼_1_4".tr()),
                                  TextSpan(text: "튜토리얼_1_5".tr()),
                                  TextSpan(text: "튜토리얼_1_6".tr(), style: st_13(textColor: color_linkmom)),
                                  TextSpan(text: "튜토리얼_1_7".tr()),
                                  TextSpan(text: "튜토리얼_1_8".tr()),
                                ],
                              ),
                            ),
                            sb_h_20,
                            RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                style: st_b_13(),
                                children: [
                                  TextSpan(text: "튜토리얼_1_9".tr()),
                                  TextSpan(text: "튜토리얼_1_10".tr()),
                                  TextSpan(text: "튜토리얼_1_11".tr(), style: st_13(textColor: color_main)),
                                  TextSpan(text: "튜토리얼_1_12".tr()),
                                  TextSpan(text: "튜토리얼_1_13".tr(), style: st_13(textColor: color_linkmom)),
                                  TextSpan(text: "튜토리얼_1_14".tr()),
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  )),
            ],
          ),
        ),
      ],
    );
  }

  Widget _menuComponent(String title, {bool showBorder = true, required Function movePage, bool disable = false}) {
    return Column(children: [
      Container(
          padding: padding_20_TB,
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(border: Border(bottom: BorderSide(color: showBorder ? color_eeeeee : Colors.transparent))),
          child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            lText(title, style: st_b_15(fontWeight: FontWeight.w500, textColor: disable ? color_999999 : color_222222)),
            Lcons.nav_right(color: disable ? color_999999 : color_222222, size: 15),
          ])),
    ]);
  }
}

class TutorialMHome extends StatelessWidget {
  TutorialMHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TutorialUserHome(type: USER_TYPE.mom_daddy);
  }
}

class TutorialLHome extends StatelessWidget {
  TutorialLHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TutorialUserHome(type: USER_TYPE.link_mom);
  }
}

class TutorialLast extends StatelessWidget {
  const TutorialLast({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.topCenter, children: [
      TutorialHomeDummy(type: USER_TYPE.mom_daddy),
      Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: Commons.width(context, 1),
            height: Commons.height(context, 1),
            foregroundDecoration: BoxDecoration(
              color: color_060606.withOpacity(0.9),
            ),
          ),
          Container(
            padding: padding_20,
            child: Image.asset(IMG_TUTORIAL_PROCESS),
          ),
        ],
      ),
    ]);
  }
}

class HowTo extends TableRow {
  final TextStyle? style;
  final String rightMessage;
  final String centerMessage;
  final String leftMessage;

  HowTo({Key? key, this.rightMessage = '', this.centerMessage = '', this.leftMessage = '', this.style})
      : super(children: [
          lText(leftMessage, style: style ?? st_14(fontWeight: FontWeight.w700), textAlign: TextAlign.center),
          Container(
              height: 30,
              margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
              alignment: Alignment.center,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(50), border: Border.all(color: Colors.white)),
              child: lText(centerMessage, style: st_10(fontWeight: FontWeight.w700, letterSpacing: 0), textAlign: TextAlign.center)),
          lText(rightMessage, style: style ?? st_14(fontWeight: FontWeight.w700), textAlign: TextAlign.center)
        ]);
}

class TutorialHomeDummy extends StatelessWidget {
  final USER_TYPE type;

  const TutorialHomeDummy({Key? key, this.type = USER_TYPE.mom_daddy}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isEnable = type == USER_TYPE.mom_daddy;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(height: MediaQuery.of(context).padding.top),
        Container(
          padding: padding_15,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [Lcons.mypage(size: 30), Image.asset(IMG_LOGO, width: 130), Lcons.notification(size: 30)],
          ),
        ),
        HomeView(
            onRefresh: () => {},
            titles: ["맘대디".tr(), "링크쌤".tr()],
            type: type,
            onClickAction: () => {},
            desc: [
              LinkmomHomeBanner(
                isLinkmom: !isEnable,
                title: "우리아이맡기기_안내".tr(),
                illust: ILLUST_INTRO,
                onClickAction: () => {},
                onJoin: () => {},
              ),
              LinkmomHomeBanner(
                isLinkmom: !isEnable,
                title: "동네아이돌보기_안내".tr(),
                illust: ILLUST_GO_HOME_SCHOOL,
                onClickAction: () => {},
                onJoin: () => {},
              )
            ],
            bottom: Column(
              children: [
                Container(
                  padding: padding_20_LTR,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
                  child: Image.asset(IMG_TUTORIAL_BANNER),
                ),
                HomeRankTab(type,
                    onClick: () {},
                    rankList: List.generate(
                      3,
                      (index) => [
                        RankingData(
                          firstName: "박OO",
                          gender: "여자",
                          age: "30대 초반",
                          currentRegion3depth: "하동",
                          priceSum: 1400000,
                          reviewCnt: 265,
                          likeCnt: 425,
                        ),
                      ],
                    )),
              ],
            )),
      ],
    );
  }
}

class TutorialUserHome extends StatelessWidget {
  final USER_TYPE type;

  TutorialUserHome({Key? key, this.type = USER_TYPE.mom_daddy}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          // alignment: Alignment.topCenter,
          children: [
            Container(
              height: Commons.height(context, 1) - kBottomNavigationBarHeight - kToolbarHeight,
              child: lScrollView(
                physics: NeverScrollableScrollPhysics(),
                padding: padding_0,
                child: TutorialHomeDummy(type: type),
              ),
            ),
            Container(
              height: kBottomNavigationBarHeight,
              width: Commons.width(context, 1),
              color: Colors.transparent,
              child: BottomNavigationBar(
                selectedLabelStyle: st_11(fontWeight: FontWeight.w700, textColor: type == USER_TYPE.mom_daddy ? color_main : color_linkmom),
                unselectedLabelStyle: st_11(),
                backgroundColor: Colors.white,
                selectedItemColor: type == USER_TYPE.mom_daddy ? color_main : color_linkmom,
                unselectedItemColor: color_808080,
                type: BottomNavigationBarType.fixed,
                items: [
                  BottomNavigationBarItem(label: "홈".tr(), icon: Lcons.nav_home(isEnabled: true), activeIcon: Lcons.nav_home(isEnabled: true, color: type == USER_TYPE.mom_daddy ? color_main : color_linkmom)),
                  BottomNavigationBarItem(label: "커뮤니티".tr(), icon: Lcons.nav_community(isEnabled: true)),
                  BottomNavigationBarItem(label: type == USER_TYPE.mom_daddy ? "돌봄관리".tr() : "근무관리".tr(), icon: Lcons.nav_care_schedule(isEnabled: true)),
                  BottomNavigationBarItem(label: type == USER_TYPE.mom_daddy ? "링크쌤찾기".tr() : "맘대디찾기".tr(), icon: Lcons.nav_linkmom_search(isEnabled: true)),
                  BottomNavigationBarItem(label: "채팅".tr(), icon: Lcons.nav_chat()),
                ],
              ),
            ),
          ],
        ),
        Container(
          width: Commons.width(context, 1),
          height: Commons.height(context, 1),
          foregroundDecoration: BoxDecoration(
            color: color_060606.withOpacity(0.6),
          ),
        ),
        Positioned(
            left: 0,
            right: 0,
            top: 200,
            child: Column(
              children: [
                Stack(
                  alignment: Alignment.center,
                  children: [
                    SvgPicture.asset(SVG_TALK_BUBBLE_SMALL, height: 135, width: 214),
                    Container(
                      alignment: Alignment.center,
                      padding: padding_15_B,
                      child: Column(children: [
                        type == USER_TYPE.mom_daddy ? lText("튜토리얼_2_1".tr(), style: st_16(fontWeight: FontWeight.w700, textColor: color_main)) : lText("튜토리얼_3_1".tr(), style: st_16(fontWeight: FontWeight.w700, textColor: color_linkmom)),
                        sb_h_04,
                        type == USER_TYPE.mom_daddy ? lText("튜토리얼_2_2".tr(), style: st_13(textColor: color_545454), textAlign: TextAlign.center) : lText("튜토리얼_3_2".tr(), style: st_13(textColor: color_545454), textAlign: TextAlign.center),
                      ]),
                    )
                  ],
                ),
                type == USER_TYPE.mom_daddy
                    ? lBtn(
                        "우리아이맡기기".tr(),
                        btnColor: color_main,
                        style: st_17(fontWeight: FontWeight.w700),
                      )
                    : lBtn(
                        "동네아이돌보기".tr(),
                        btnColor: color_linkmom,
                        style: st_17(fontWeight: FontWeight.w700),
                      ),
              ],
            )),
        Positioned(
            left: 0,
            top: MediaQuery.of(context).padding.top + 5,
            child: Row(
              children: [
                Container(
                  height: kTextTabBarHeight,
                  width: kTextTabBarHeight,
                  margin: padding_10_LR,
                  padding: padding_07,
                  decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(40)),
                  child: Lcons.mypage(color: type == USER_TYPE.mom_daddy ? color_main : color_linkmom, isEnabled: type != USER_TYPE.mom_daddy),
                ),
                type == USER_TYPE.mom_daddy ? lText("튜토리얼_2_3".tr(), style: st_15()) : lText("튜토리얼_3_3".tr(), style: st_15()),
              ],
            )),
        Positioned(
            right: 15,
            bottom: kToolbarHeight - 5,
            child: Column(
              children: [
                type == USER_TYPE.mom_daddy ? lText("튜토리얼_2_4".tr(), style: st_15(), textAlign: TextAlign.center) : lText("튜토리얼_3_4".tr(), style: st_15(), textAlign: TextAlign.center),
                Container(
                    height: kBottomNavigationBarHeight,
                    margin: padding_15_T,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(32),
                      color: Colors.white,
                    ),
                    child: SvgPicture.asset(type == USER_TYPE.mom_daddy ? SVG_TUTORIAL_L_BOTTOM : SVG_TUTORIAL_M_BOTTOM)),
              ],
            )),
      ],
    );
  }
}
