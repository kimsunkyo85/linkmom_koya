import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/findpw_request.dart';
import 'package:linkmom/data/network/models/resetpw_request.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/login/password_change/password_change_complete_page.dart';
import 'package:linkmom/view/view_modules.dart';
import 'package:timer_builder/timer_builder.dart';

import '../../../main.dart';

class PasswordChangePage extends BaseStateful {
  final FindPwRequest? req;
  final DataManager? data;
  final String? accessToken;
  PasswordChangePage({this.data, this.accessToken, this.req});

  PasswordChangePageState createState() => PasswordChangePageState();
}

class PasswordChangePageState extends BaseStatefulState<PasswordChangePage> {
  ResetPwRequest _pwRequest = ResetPwRequest();
  int _remain = Duration(minutes: 5).inSeconds;

  @override
  void initState() {
    super.initState();
    super.onData(widget.data ?? DataManager());
    if (widget.req != null) _pwRequest.username = widget.req!.username;
    flag.disableConfirm(fn: () => onUpDate());
  }

  @override
  void dispose() {
    super.dispose();
    data = DataManager();
    log.d({
      '--- TITLE    ': '--------------- DEBUG ---------------',
      '--- MESSAGE  ': 'clear DataManager',
    });
  }

  bool _passwordConfirmCheck(String pw1, String pw2, Function onUpDate) {
    if (pw1 == pw2) {
      flag.enablePasswordConfirm(fn: () => onUpDate());
      return true;
    } else {
      flag.disablePasswordConfirm(fn: () => onUpDate());
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Commons.pageClear(context, routeLogin);
        return Future.value(false);
      },
      child: lScaffold(
          context: context,
          resizeToAvoidBottomInset: true,
          appBar: appBar("비밀번호재설정".tr(), isClose: false, hide: false, onBack: () {
            Commons.pageClear(context, routeLogin);
          }),
          body: Container(
              child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
            Expanded(
              child: Container(
                  padding: padding_20,
                  child: Column(children: [
                    sb_h_20,
                    PasswordField(
                        data: data,
                        flag: flag,
                        onEditingComplete: (value) {
                          _pwRequest.new_password = value;
                          _confirmPassword();
                        },
                        onChanged: (value) {
                          _pwRequest.new_password = value;
                          _confirmPassword();
                        }),
                    PasswordConfirmField(
                        data: data,
                        flag: flag,
                        onEditingComplete: (value) {
                          _pwRequest.new_password2 = value;
                          _confirmPassword();
                        },
                        onChanged: (value) {
                          _pwRequest.new_password2 = value;
                          _confirmPassword();
                        }),
                    Container(
                      margin: padding_14_B,
                      padding: padding_12_B,
                      alignment: Alignment.centerRight,
                      child: TimerBuilder.periodic(Duration(seconds: 1), alignment: Duration.zero, builder: (context) {
                        if (_remain == 0) {
                          Timer(Duration(milliseconds: 1), () {
                            showNormalDlg(message: "인증시간만료".tr(), onClickAction: (a) => Commons.pageToMain(context, routeLogin));
                          });
                        }
                        return StatefulBuilder(builder: (context, state) {
                          state(() {
                            if (this.mounted) _remain -= 1;
                          });
                          return lInkWell(
                              onLongPress: () {
                                if (Commons.isDebugMode) {
                                  _remain = Duration(seconds: 3).inSeconds;
                                  onUpDate();
                                }
                              },
                              child: lText(
                                _remain > 0 ? '${StringUtils.getNumberAddZero(Duration(seconds: _remain).inMinutes.remainder(60))}:${StringUtils.getNumberAddZero(Duration(seconds: _remain).inSeconds.remainder(60))}' : '00:00',
                                style: st_16(textColor: _remain > 0 ? color_ff3b30 : color_b2b2b2),
                              ));
                        });
                      }),
                    ),
                  ])),
            ),
            lBtn("비밀번호변경".tr(), isEnabled: flag.isConfirm, onClickAction: () => _requestPwChange(), btnColor: color_main),
          ]))),
    );
  }

  void _confirmPassword() {
    if (StringUtils.validatePwConfirm(_pwRequest.new_password, _pwRequest.new_password2) == null) {
      flag.enableConfirm(fn: () => onUpDate());
    } else {
      flag.disableConfirm(fn: () => onUpDate());
    }
  }

  ///확인 버튼 클릭시 이벤트
  void _requestPwChange() {
    flag.enableLoading(fn: () => onUpDate());
    apiHelper.requestPwReset(_pwRequest, widget.accessToken ?? auth.user.token_access).then((response) {
      try {
        if (response.getCode() == KEY_SUCCESS) {
          Commons.pageToMain(context, routePwChangeComplete, arguments: PasswordChangeCompletePage(id: _pwRequest.username, password: _pwRequest.new_password));
        } else {
          showNormalDlg(message: response.getMsg());
        }
        flag.disableLoading(fn: () => onUpDate());
      } catch (e) {
        flag.disableLoading(fn: () => onUpDate());
      }
    }).catchError((e) => flag.disableLoading(fn: () => onUpDate()));
  }
}
