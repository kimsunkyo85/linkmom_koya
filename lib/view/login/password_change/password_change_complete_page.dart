import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../../../base/base_stateful.dart';
import '../../../main.dart';
import '../../../route_name.dart';

class PasswordChangeCompletePage extends BaseStateful {
  PasswordChangeCompletePage({this.id, this.password});

  final String? id;
  final String? password;

  PasswordChangePageCompleteState createState() => PasswordChangePageCompleteState();
}

class PasswordChangePageCompleteState extends BaseStatefulState<PasswordChangeCompletePage> {
  @override
  initState() {
    super.initState();
    flag.enableConfirm();
    log.d({
      'widget.id': widget.id,
      'widget.password': widget.password,
    });
  }

  ///화면갱신
  _onUpdate() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
      flag.isLoading,
      WillPopScope(
        onWillPop: () => Commons.pageToMain(context, routeLogin),
        child: lScaffold(
          appBar: appBar("비밀번호재설정".tr(), isClose: false, hide: false),
          body: DoubleBackToCloseApp(
            snackBar: SnackBar(
              content: lText("앱종료_메시지".tr(), style: stBack),
            ),
            child: Container(
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      padding: padding_20, //전체 패딩 좌우
                      child: _contentPage(),
                    ),
                  ),
                  lBtn("로그인".tr(), onClickAction: () {
                    Commons.pageToMain(context, routeLogin);
                  }, btnColor: color_main),
                ],
              ),
            ),
          ),
        ),
      ),
      showText: false,
    );
  }

  Widget _contentPage() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        RichText(
          text: TextSpan(children: [
            TextSpan(text: "비밀번호변경_완료".tr(), style: TextStyle(color: color_00cfb5, fontWeight: FontWeight.bold, fontSize: 22)),
            TextSpan(text: "비밀번호변경_재로그인".tr(), style: TextStyle(color: color_545454, fontSize: 16)),
          ]),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
