import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:expandable_page_view/expandable_page_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:linkmom/data/storage/flag_manage.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/svg_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../route_name.dart';
import '../lcons.dart';

class IntroPage extends StatefulWidget {
  const IntroPage({Key? key}) : super(key: key);

  @override
  State<IntroPage> createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  List<Widget> _page = [
    PromotionPage(),
    IntroFirst(),
    IntroM(),
    IntroL(),
    IntroAuth(),
    IntroList(),
    IntroHome(),
    IntroNoti(),
  ];

  late PageController _pageCtrl;
  int _pageIndex = 0;

  @override
  void initState() {
    super.initState();
    _pageCtrl = PageController();
    if (DateTime.now().isAfter(DateTime(2022, 06, 30, 23, 59, 59))) {
      _page.removeAt(0);
    }
  }

  @override
  Widget build(BuildContext context) {
    return lScaffold(
        body: DoubleBackToCloseApp(
      snackBar: SnackBar(
        content: lText("앱종료_메시지".tr(), style: stBack),
      ),
      child: lScrollView(
        padding: padding_0,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              child: ExpandablePageView(
                controller: _pageCtrl,
                children: _page,
                onPageChanged: (page) => setState(() => _pageIndex = page),
              ),
            ),
            Container(
              height: 80,
              padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
              color: Colors.white,
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Container(
                  width: 80,
                  child: TextButton(
                      onPressed: () {
                        Flags.addFlag(Flags.KEY_SHOW_LOGIN_INTRO, isShown: true);
                        Commons.pageClear(context, routeLogin);
                      },
                      child: lText("건너뛰기".tr(), style: st_15(textColor: color_b2b2b2, fontWeight: FontWeight.w500))),
                ),
                SmoothPageIndicator(
                  controller: _pageCtrl,
                  count: _page.length,
                  effect: ExpandingDotsEffect(
                    spacing: 8,
                    dotWidth: 6,
                    dotHeight: 6,
                    strokeWidth: 16,
                    activeDotColor: color_main,
                    dotColor: color_dedede,
                  ),
                ),
                Container(
                  width: 80,
                  child: lIconText(
                    isExpanded: false,
                    align: MainAxisAlignment.center,
                    isLeft: false,
                    onClick: () {
                      if (_pageCtrl.page!.toInt() < _page.length - 1) {
                        _pageCtrl.nextPage(duration: Duration(milliseconds: 300), curve: Curves.easeIn);
                      } else {
                        Commons.pageClear(context, routeLogin);
                      }
                    },
                    title: _pageIndex == _page.length - 1 ? "시작하기".tr() : "다음".tr(),
                    textStyle: st_15(textColor: color_main, fontWeight: FontWeight.w500),
                    icon: Lcons.nav_right(color: color_main, size: 15),
                  ),
                ),
              ]),
            ),
          ],
        ),
      ),
    ));
  }
}

class IntroFirst extends StatelessWidget {
  const IntroFirst({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.symmetric(vertical: 100),
      child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Column(
          children: [
            lText("우리동네아이돌봄".tr(), style: st_22(textColor: color_cecece, fontWeight: FontWeight.w500)),
            Container(
              margin: EdgeInsets.fromLTRB(15, 15, 15, 20),
              child: Image.asset(IMG_LOGO, width: 240),
            ),
            lText("인트로_1".tr(), style: st_16(textColor: color_999999, height: 1.8), textAlign: TextAlign.center),
          ],
        ),
        Container(
          padding: padding_20,
          child: Image.asset(ILLUST_INTRO, width: Commons.width(context, 0.4)),
        ),
      ]),
    );
  }
}

class IntroM extends StatelessWidget {
  const IntroM({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Stack(
      children: [
        Container(
          height: height,
          padding: EdgeInsets.symmetric(vertical: 100),
          child: IntroTempleat(
            bg: Image.asset(IMG_INTRO_SCREEN_M, width: Commons.width(context, 0.1)),
            bgBottom: (height * 0.42) * -1,
            body: Container(
              child: Column(children: [
                Column(
                  children: [
                    TextHighlight(
                      text: "인트로_2_1".tr(),
                      term: "긴급돌봄".tr(),
                      softWrap: false,
                      textStyle: st_b_26(fontWeight: FontWeight.w700),
                      textStyleHighlight: st_26(textColor: color_main, fontWeight: FontWeight.w700),
                    ),
                    sb_h_20,
                    lText(
                      "인트로_2_2".tr(),
                      style: st_16(textColor: color_999999),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(20, height * 0.1, 20, 10),
                  child: Row(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: [
                    lText(
                      "맘대디_부모님".tr(),
                      style: st_16(fontWeight: FontWeight.w700, textColor: color_main),
                      textAlign: TextAlign.center,
                    ),
                    Padding(
                        padding: padding_10_LR,
                        child: FlutterSwitch(
                          width: 60,
                          padding: 3.0,
                          toggleColor: color_main,
                          activeColor: Colors.white,
                          inactiveColor: color_eeeeee,
                          toggleSize: 30,
                          value: false,
                          onToggle: (value) => {},
                        )),
                    lText(
                      "링크쌤_돌보미".tr(),
                      style: st_16(fontWeight: FontWeight.w700, textColor: color_cecece),
                      textAlign: TextAlign.center,
                    ),
                  ]),
                ),
              ]),
            ),
          ),
        ),
        Positioned.fill(
          top: (height / 2.7),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: padding_30_L,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [
                      BoxShadow(blurRadius: 16, offset: Offset(0, 4), color: Colors.black.withOpacity(0.12)),
                    ]),
                    child: SvgPicture.asset(SVG_INTRO_TALK_BG_CIRCLE, width: 100),
                  ),
                  TextHighlight(
                    text: "인트로_3_1".tr(),
                    term: "빠른매칭".tr(),
                    softWrap: false,
                    textStyle: st_14(textColor: color_545454, fontWeight: FontWeight.w700),
                    textStyleHighlight: st_14(textColor: color_main, fontWeight: FontWeight.w700),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class IntroL extends StatelessWidget {
  const IntroL({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Stack(
      children: [
        Container(
          height: height,
          padding: EdgeInsets.symmetric(vertical: 100),
          child: IntroTempleat(
            bg: Image.asset(IMG_INTRO_SCREEN_L, width: 100),
            bgBottom: (height * 0.42) * -1,
            body: Column(children: [
              Column(
                children: [
                  TextHighlight(
                    text: "인트로_4_1".tr(),
                    term: "일하고싶을때".tr(),
                    softWrap: false,
                    textStyle: st_b_26(fontWeight: FontWeight.w700),
                    textStyleHighlight: st_26(textColor: color_linkmom, fontWeight: FontWeight.w700),
                  ),
                  sb_h_20,
                  lText(
                    "인트로_4_2".tr(),
                    style: st_16(textColor: color_999999),
                    textAlign: TextAlign.center,
                  )
                ],
              ),
              Container(
                padding: EdgeInsets.fromLTRB(20, height * 0.1, 20, 10),
                child: Row(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: [
                  lText(
                    "맘대디_부모님".tr(),
                    style: st_16(fontWeight: FontWeight.w700, textColor: color_cecece),
                    textAlign: TextAlign.center,
                  ),
                  Padding(
                      padding: padding_10_LR,
                      child: FlutterSwitch(
                        width: 60,
                        padding: 3.0,
                        toggleColor: color_linkmom,
                        activeColor: color_eeeeee,
                        inactiveColor: color_eeeeee,
                        toggleSize: 30,
                        value: true,
                        onToggle: (value) => {},
                      )),
                  lText(
                    "링크쌤_돌보미".tr(),
                    style: st_16(fontWeight: FontWeight.w700, textColor: color_linkmom),
                    textAlign: TextAlign.center,
                  ),
                ]),
              ),
            ]),
          ),
        ),
        Positioned.fill(
          top: (height / 2.7),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: padding_30_L,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [
                      BoxShadow(blurRadius: 16, offset: Offset(0, 4), color: Colors.black.withOpacity(0.12)),
                    ]),
                    child: SvgPicture.asset(SVG_INTRO_TALK_BG_CIRCLE, width: 100),
                  ),
                  TextHighlight(
                    text: "인트로_4_3".tr(),
                    term: "일하잡".tr(),
                    softWrap: false,
                    textStyle: st_14(textColor: color_545454, fontWeight: FontWeight.w700),
                    textStyleHighlight: st_14(textColor: color_linkmom, fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class IntroAuth extends StatelessWidget {
  IntroAuth({Key? key}) : super(key: key);

  final List<int> _authPreview = [0, 1, 2];

  @override
  Widget build(BuildContext context) {
    return Container(
        height: Commons.height(context, 1),
        padding: EdgeInsets.symmetric(vertical: 100),
        child: IntroTempleat(
          bg: Image.asset(IMG_INTRO_AUTH),
          bgBottom: Commons.height(context, -0.45),
          body: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Column(
              children: [
                TextHighlight(
                  text: "인트로_5_1".tr(),
                  term: "9가지인증절차".tr(),
                  softWrap: false,
                  textAlign: TextAlign.center,
                  textStyle: st_b_26(fontWeight: FontWeight.w700),
                  textStyleHighlight: st_26(textColor: color_linkmom, fontWeight: FontWeight.w700),
                ),
                sb_h_20,
                lText(
                  "인트로_5_2".tr(),
                  style: st_16(textColor: color_999999),
                  textAlign: TextAlign.center,
                )
              ],
            ),
            Container(
              padding: padding_30_TB,
              alignment: Alignment.center,
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: _authPreview
                      .map((index) => Stack(
                            alignment: Alignment.center,
                            children: [
                              Container(decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [BoxShadow(blurRadius: 10, offset: Offset(0, 2), color: Colors.grey.withOpacity(0.1))]), child: Lcons.auth_bg(isEnabled: true, size: 80)),
                              Padding(padding: padding_03_B, child: Lcons.getAuthType(index, color: Colors.white, size: 30)),
                              Positioned(right: 5, top: 5, child: Lcons.check_circle(color: color_ffbb56, isEnabled: true)),
                            ],
                          ))
                      .toList()),
            ),
          ]),
        ));
  }
}

class IntroList extends StatelessWidget {
  const IntroList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: Commons.height(context, 1),
        padding: EdgeInsets.symmetric(vertical: 100),
        child: IntroTempleat(
          bgLeft: 0,
          bgRight: 0,
          bgBottom: Commons.height(context, -0.45),
          bg: Container(
            width: Commons.width(context, 1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(IMG_INTRO_LIST_M, width: Commons.width(context, 0.5)),
                Image.asset(IMG_INTRO_LIST_L, width: Commons.width(context, 0.5)),
              ],
            ),
          ),
          body: Column(children: [
            Column(
              children: [
                TextHighlight(
                  text: "인트로_6_1".tr(),
                  term: "무료".tr(),
                  softWrap: false,
                  textAlign: TextAlign.center,
                  textStyle: st_b_26(fontWeight: FontWeight.w700),
                  textStyleHighlight: st_26(textColor: color_linkmom, fontWeight: FontWeight.w700),
                ),
                sb_h_20,
                lText(
                  "인트로_6_2".tr(),
                  style: st_16(textColor: color_999999),
                  textAlign: TextAlign.center,
                )
              ],
            ),
            Padding(
              padding: padding_20_TB,
              child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Column(
                  children: [Padding(padding: padding_10, child: Image.asset(IMG_INTRO_PROFILE_M, width: 64)), lText("맘대디".tr(), style: st_18(textColor: color_main, fontWeight: FontWeight.w700))],
                ),
                Container(
                  margin: padding_30,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      RotationTransition(turns: AlwaysStoppedAnimation(180 / 360), child: SvgPicture.asset(SVG_INTRO_ARROW, width: 70, color: color_main)),
                      sb_h_15,
                      SvgPicture.asset(SVG_INTRO_ARROW, width: 70, color: color_linkmom),
                    ],
                  ),
                ),
                Column(
                  children: [Padding(padding: padding_10, child: Image.asset(IMG_INTRO_PROFILE_L, width: 64)), lText("링크쌤".tr(), style: st_18(textColor: color_linkmom, fontWeight: FontWeight.w700))],
                ),
              ]),
            ),
          ]),
        ));
  }
}

class IntroHome extends StatelessWidget {
  const IntroHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: Commons.height(context, 1),
        padding: EdgeInsets.symmetric(vertical: 100),
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Image.asset(IMG_INTRO_BG_HOUSE, width: Commons.width(context, 1)),
            IntroTempleat(
              bgLeft: 0,
              bgRight: 0,
              bgBottom: Commons.height(context, -0.45),
              widthFull: Commons.width(context, 0.8),
              bg: Image.asset(IMG_INTRO_LOCATE, width: Commons.width(context, 1)),
              body: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  TextHighlight(
                    text: "인트로_7_1".tr(),
                    term: "보육장소선택가능".tr(),
                    softWrap: false,
                    textStyle: st_b_26(fontWeight: FontWeight.w700),
                    textStyleHighlight: st_26(textColor: color_main, fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  ),
                  sb_h_20,
                  lText(
                    "인트로_7_2".tr(),
                    style: st_16(textColor: color_999999),
                    textAlign: TextAlign.center,
                  )
                ],
              ),
            ),
          ],
        ));
  }
}

class IntroNoti extends StatelessWidget {
  const IntroNoti({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Container(
        height: height,
        padding: EdgeInsets.only(top: 100),
        child: IntroTempleat(
          bgBottom: (height * 0.4) * -1,
          bg: Image.asset(IMG_INTRO_NOTI),
          body: Column(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(children: [
                      TextSpan(text: "도착알림".tr(), style: st_26(textColor: color_main, fontWeight: FontWeight.w700)),
                      TextSpan(text: "인트로_8_1".tr(), style: st_b_26(fontWeight: FontWeight.w700)),
                      TextSpan(text: "인트로_8_2".tr(), style: st_b_26(fontWeight: FontWeight.w700)),
                      TextSpan(text: "인트로_8_3".tr(), style: st_26(textColor: color_main, fontWeight: FontWeight.w700)),
                    ]),
                  ),
                  sb_h_20,
                ],
              ),
              Container(
                height: height * 0.5,
                margin: padding_20,
                child: Stack(
                  children: [
                    Positioned(right: 10, top: 120, child: Talk("인트로_8_5".tr(), highLight: "도착알림".tr())),
                    Positioned(left: 10, top: 90, child: Talk("인트로_8_6".tr(), highLight: "출퇴근알림".tr(), isLeft: false)),
                    Positioned(right: 10, top: 60, child: Talk("인트로_8_7".tr(), highLight: "인트로_8_7_1".tr())),
                    Positioned(left: 10, top: 30, child: Talk("인트로_8_8".tr(), highLight: "인트로_8_8_1".tr(), isLeft: false)),
                    Positioned(right: 10, child: Talk("인트로_8_9".tr(), highLight: "인트로_8_9_1".tr())),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}

class IntroTempleat extends StatelessWidget {
  final Widget body;
  final Widget bg;
  final Widget? float;
  final double? bgLeft, bgRight, bgTop, bgBottom;
  final double? widthFull;

  const IntroTempleat({Key? key, required this.body, required this.bg, this.float, this.bgLeft, this.bgRight, this.bgTop, this.bgBottom, this.widthFull}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    double margin = 0;
    if (Commons.ratioChecker(height, width)) {
      margin = width * 0.1;
    } else {
      margin = width * 0.05;
    }

    return Container(
      width: widthFull,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(left: bgLeft ?? margin, right: bgRight ?? margin, top: bgTop ?? 0, bottom: bgBottom ?? (height * -0.6), child: bg),
          Container(child: body),
          if (float != null) Positioned(left: margin, bottom: 30, child: float!),
        ],
      ),
    );
  }
}

class Talk extends StatelessWidget {
  final String text;
  final String highLight;
  final bool isLeft;

  const Talk(this.text, {Key? key, this.highLight = '', this.isLeft = true}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        alignment: Alignment.center,
        fit: StackFit.loose,
        children: [
          Container(
              margin: padding_02,
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                  offset: Offset(0, 4),
                  blurRadius: 8,
                  spreadRadius: -20,
                  color: Colors.black.withOpacity(0.1),
                ),
              ]),
              child: SvgPicture.asset(isLeft ? SVG_INTRO_TALK_BG : SVG_INTRO_BG_LEFT, height: 70)),
          Container(
            margin: padding_15_B,
            child: TextHighlight(
              text: text,
              term: highLight,
              softWrap: false,
              textStyle: st_14(textColor: color_545454, fontWeight: FontWeight.w700),
              textStyleHighlight: st_14(textColor: color_main, fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}

class PromotionPage extends StatelessWidget {
  const PromotionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: MediaQuery.of(context).size.height,
      color: color_linkmom,
      padding: EdgeInsets.fromLTRB(50, 30, 50, 50),
      child: Image.asset(IMG_PROMO_JUNE),
    );
  }
}
