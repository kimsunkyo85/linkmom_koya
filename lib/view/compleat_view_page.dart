import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

class CompleteViewPage extends BaseStateful {
  final DataManager? data;
  final String? title;
  final Widget? body;
  final String? resultMessage;
  final Function? finishAction;
  final Widget? footer;
  final Widget? topper;
  final Color? btnColor;
  final Widget? btn;
  CompleteViewPage({this.data, this.title, this.body, this.resultMessage, this.finishAction, this.topper, this.footer, this.btnColor, this.btn});

  @override
  _CompleatViewPageState createState() => _CompleatViewPageState();
}

class _CompleatViewPageState extends BaseStatefulState<CompleteViewPage> {
  @override
  Widget build(BuildContext context) {
    return lModalProgressHUD(
        flag.isLoading,
        lScaffold(
            appBar: appBar(widget.title ?? "완료".tr(), hide: false),
            body: Container(
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Expanded(
                        child: widget.body ??
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                if (widget.topper != null) widget.topper!,
                                Lcons.completed(size: 72),
                                sb_h_30,
                                lText(widget.resultMessage ?? "완료되었습니다".tr(), style: st_b_20(fontWeight: FontWeight.w500)),
                                sb_h_10,
                                if (widget.footer != null)
                                  Padding(
                                    padding: padding_20_LR,
                                    child: widget.footer!,
                                  )
                              ],
                            )),
                    widget.btn ?? lBtn("확인".tr(), btnColor: widget.btnColor ?? color_545454, onClickAction: widget.finishAction ?? () => Commons.pagePop(context))
                  ],
                ))));
  }
}
