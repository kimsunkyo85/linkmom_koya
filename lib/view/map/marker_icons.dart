import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_cluster_manager/google_maps_cluster_manager.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linkmom/utils/style/color_style.dart';

import '../../main.dart';
import 'page.dart';
import 'place.dart';

class MarkerIconsPage extends GoogleMapExampleAppPage {
  MarkerIconsPage() : super(const Icon(Icons.image), 'Marker icons');

  @override
  Widget build(BuildContext context) {
    return const MarkerIconsBody();
  }
}

class MarkerIconsBody extends StatefulWidget {
  const MarkerIconsBody();

  @override
  State<StatefulWidget> createState() => MarkerIconsBodyState();
}

///꿈비 37.287670, 127.060441
///광교센트럴타운 6001동 37.289096, 127.057972
///광교센트럴타운 6010동 37.290081, 127.059216
///광교자연앤자이 5301동 37.285420, 127.054237
///광교자연앤자이 5302동 37.285138, 127.055039
///광교힐스테이트 레이크아파트 102동 37.287946, 127.062103
///광교힐스테이트 레이크아파트 103동 37.288146, 127.062847
///광교센트럴타운 62단지아파트 6201동 37.289716, 127.062482
///광교센트럴타운 62단지아파트 6202동 37.289486, 127.062841
///광교래미안아파트 6302동 37.290788, 127.061025
///광교래미안아파트 6303동 37.290519, 127.061701
const LatLng _kMapCenter = LatLng(37.287670, 127.060441);

class MarkerIconsBodyState extends State<MarkerIconsBody> {
  GoogleMapController? controller;
  BitmapDescriptor? _markerIcon;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  MarkerId? selectedMarker;
  int _markerIdCounter = 1;
  late Location location;
  List<LatLng> _latLngs = [
    LatLng(37.287670, 127.060441),
    LatLng(37.289096, 127.057972),
    LatLng(37.290081, 127.059216),
    LatLng(37.285420, 127.054237),
    LatLng(37.285138, 127.055039),
    LatLng(37.287946, 127.062103),
    LatLng(37.288146, 127.062847),
    LatLng(37.289716, 127.062482),
    LatLng(37.289486, 127.062841),
    LatLng(37.290788, 127.061025),
    LatLng(37.290519, 127.061701),
  ];

  late ClusterManager _manager;
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = Set();
  final CameraPosition _parisCameraPosition = CameraPosition(target: _kMapCenter, zoom: 12.0);

  List<Place> items = [
    for (int i = 0; i < 10; i++) Place(name: 'Place $i', latLng: LatLng(48.848200 + i * 0.001, 2.319124 + i * 0.001)),
    for (int i = 0; i < 10; i++) Place(name: 'Restaurant $i', isClosed: i % 2 == 0, latLng: LatLng(48.858265 - i * 0.001, 2.350107 + i * 0.001)),
    for (int i = 0; i < 10; i++) Place(name: 'Bar $i', latLng: LatLng(48.858265 + i * 0.01, 2.350107 - i * 0.01)),
    for (int i = 0; i < 10; i++) Place(name: 'Hotel $i', latLng: LatLng(48.858265 - i * 0.1, 2.350107 - i * 0.01)),
    for (int i = 0; i < 10; i++) Place(name: 'Test $i', latLng: LatLng(66.160507 + i * 0.1, -153.369141 + i * 0.1)),
    for (int i = 0; i < 10; i++) Place(name: 'Test2 $i', latLng: LatLng(-36.848461 + i * 1, 169.763336 + i * 1)),
  ];

  @override
  void initState() {
    _manager = _initClusterManager();
    // _add();
    super.initState();
  }

  ClusterManager _initClusterManager() {
    return ClusterManager<Place>(items, _updateMarkers, markerBuilder: _markerBuilder);
  }

  void _updateMarkers(Set<Marker> markers) {
    log.d('『GGUMBI』>>> _updateMarkers : markers: ${markers.length},  <<< ');
    setState(() {
      this._markers = markers;
    });
  }

  Future<Marker> Function(Cluster<Place>) get _markerBuilder => (cluster) async {
        return Marker(
          markerId: MarkerId(cluster.getId()),
          position: cluster.location,
          infoWindow: await _getMarkerInfo(cluster),
          icon: await _getMarkerBitmap(cluster.isMultiple ? 125 : 75, text: cluster.isMultiple ? cluster.count.toString() : null),
          onTap: () async {
            // int index = cluster.items.length;
            // if (index != 1) {
            //   List<ListTileItem> item = <ListTileItem>[];
            //   cluster.items.forEach((value) {
            //     log.d('『GGUMBI』>>> _markerBuilder : value?.name: ${value.name},  <<< ');
            //     item.add(ListTileItem(text: value.name));
            //   });
            //   CustomDialogListTile("목록", item);
            // }
          },
        );
      };

  Future<InfoWindow> _getMarkerInfo(cluster) async {
    log.d('『GGUMBI』>>> _getMarkerInfo : cluster: ${cluster.toString()},  <<< ');
    int count = cluster.count;
    String? title;
    cluster.items.forEach((value) {
      if (count == 1) {
        title = value?.name;
      }
    });
    log.d('『GGUMBI』>>> _getMarkerInfo : title: $title,  <<< ');
    return InfoWindow(title: title, snippet: '*');
  }

  Future<BitmapDescriptor> _getMarkerBitmap(int size, {String? text}) async {

    final PictureRecorder pictureRecorder = PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);
    final Paint paint1 = Paint()..color = Colors.orange;
    final Paint paint2 = Paint()..color = color_white;

    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.0, paint1);
    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.2, paint2);
    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.8, paint1);

    if (text != null) {
      TextPainter painter = TextPainter(textDirection: TextDirection.ltr);
      painter.text = TextSpan(
        text: text,
        style: TextStyle(fontSize: size / 3, color: color_white, fontWeight: FontWeight.normal),
      );
      painter.layout();
      painter.paint(
        canvas,
        Offset(size / 2 - painter.width / 2, size / 2 - painter.height / 2),
      );
    }

    final img = await pictureRecorder.endRecording().toImage(size, size);
    final data = await img.toByteData(format: ImageByteFormat.png) as ByteData;

    return BitmapDescriptor.fromBytes(data.buffer.asUint8List());
  }

  // Set<Circle> circles = Set.from([Circle(
  //   circleId: CircleId(id),
  //   center: LatLng(latitude, longitude),
  //   radius: 4000,
  // )]);

  // TODO: GGUMBI 3/22/21 - 내 위치 반경 표시시 사용 할
  Set<Circle> circles = Set.from([
    Circle(
        circleId: CircleId("myCircle"),
        radius: 4000,
        center: LatLng(_kMapCenter.latitude, _kMapCenter.longitude),
        // center: _createCenter,
        fillColor: Color.fromRGBO(204, 255, 204, 0.2),
        strokeColor: Color.fromRGBO(153, 204, 153, 0.8),
        strokeWidth: 2,
        onTap: () {
          print('circle pressed');
        })
  ]);

  LatLng _createCenter() {
    return _createLatLng(_kMapCenter.latitude, _kMapCenter.longitude);
  }

  LatLng _createLatLng(double lat, double lng) {
    return LatLng(lat, lng);
  }

  @override
  Widget build(BuildContext context) {
    _createMarkerImageFromAsset(context);
    return Container(
      child: SizedBox(
        width: MediaQuery.of(context).size.width, // or use fixed size like 200
        height: MediaQuery.of(context).size.height /**0.4*/,

        // child: GoogleMap(
        //   initialCameraPosition: const CameraPosition(
        //     target: _kMapCenter,
        //     zoom: 7.0,
        //   ),
        //   markers: Set<Marker>.of(markers.values),
        //   onMapCreated: _onMapCreated,
        //   myLocationEnabled: true,
        // ),

        child: GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: _parisCameraPosition,
          markers: _markers,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
            _manager.setMapId(controller.mapId);
          },
          onCameraMove: _manager.onCameraMove,
          onCameraIdle: _manager.updateMap,
          mapToolbarEnabled: false,
          circles: circles,
        ),
      ),
    );
  }

/*
  Set<Marker> _createMarker() {
    // TODO(iskakaushik): Remove this when collection literals makes it to stable.
    // https://github.com/flutter/flutter/issues/28312
    // ignore: prefer_collection_literals
    return <Marker>[
      Marker(
        markerId: MarkerId("marker_1"),
        position: _kMapCenter,
        icon: _markerIcon,
      ),
    ].toSet();
  }
*/

  Future<void> _createMarkerImageFromAsset(BuildContext context) async {
    if (_markerIcon == null) {
      final ImageConfiguration imageConfiguration = createLocalImageConfiguration(context, size: Size.square(48));
      BitmapDescriptor.fromAssetImage(imageConfiguration, 'assets/red_square.png').then(_updateBitmap);
    }
  }

  void _updateBitmap(BitmapDescriptor bitmap) {
    setState(() {
      _markerIcon = bitmap;
    });
  }

/*
  void _onMapCreated(GoogleMapController controllerParam) {
    setState(() {
      controller = controllerParam;
      controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(_kMapCenter.latitude, _kMapCenter.longitude), zoom: 15),
        ),
      );

      // location.onLocationChanged.listen((l) {
      //   // controller.animateCamera(
      //   //   CameraUpdate.newCameraPosition(
      //   //     CameraPosition(target: LatLng(l.latitude, l.longitude),zoom: 15),
      //   //   ),
      //   // );
      // });
    });
  }*/

  void _add() {
    Marker marker;
    for (var i in _latLngs) {
      final String markerIdVal = 'marker_id_$_markerIdCounter';
      _markerIdCounter++;
      final MarkerId markerId = MarkerId(markerIdVal);
      marker = Marker(
        markerId: markerId,
        position: i,
        infoWindow: InfoWindow(title: markerIdVal, snippet: '*'),
        onTap: () {
          // _onMarkerTapped(markerId);
          log.d('『GGUMBI』>>> _add ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
        },
        onDragEnd: (LatLng position) {
          // _onMarkerDragEnd(markerId, position);
          log.d('『GGUMBI』>>> _add ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
        },
      );

      setState(() {
        markers[markerId] = marker;
      });

      log.d({
        'markers.length': markers.length,
        '_latLngs.length': _latLngs.length,
        'i': i,
      });
    }
  }
}
