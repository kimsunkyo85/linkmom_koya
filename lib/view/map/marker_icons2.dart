import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:linkmom/utils/style/color_style.dart';

import '../../main.dart';
import 'page.dart';

class MarkerIconsPage2 extends GoogleMapExampleAppPage {
  MarkerIconsPage2() : super(const Icon(Icons.image), 'Marker icons');

  @override
  Widget build(BuildContext context) {
    return const MarkerIconsBody2();
  }
}

class MarkerIconsBody2 extends StatefulWidget {
  const MarkerIconsBody2();

  @override
  State<StatefulWidget> createState() => MarkerIconsBodyState2();
}

///꿈비 37.287670, 127.060441
///광교센트럴타운 6001동 37.289096, 127.057972
///광교센트럴타운 6010동 37.290081, 127.059216
///광교자연앤자이 5301동 37.285420, 127.054237
///광교자연앤자이 5302동 37.285138, 127.055039
///광교힐스테이트 레이크아파트 102동 37.287946, 127.062103
///광교힐스테이트 레이크아파트 103동 37.288146, 127.062847
///광교센트럴타운 62단지아파트 6201동 37.289716, 127.062482
///광교센트럴타운 62단지아파트 6202동 37.289486, 127.062841
///광교래미안아파트 6302동 37.290788, 127.061025
///광교래미안아파트 6303동 37.290519, 127.061701
const LatLng _kMapCenter = LatLng(37.287670, 127.060441);

class MarkerIconsBodyState2 extends State<MarkerIconsBody2> {
  GoogleMapController? controller;
  BitmapDescriptor? _markerIcon;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  MarkerId? selectedMarker;
  int _markerIdCounter = 1;
  late Location location;
  List<LatLng> _latLngs = [
    LatLng(37.287670, 127.060441),
    LatLng(37.289096, 127.057972),
    LatLng(37.290081, 127.059216),
    LatLng(37.285420, 127.054237),
    LatLng(37.285138, 127.055039),
    LatLng(37.287946, 127.062103),
    LatLng(37.288146, 127.062847),
    LatLng(37.289716, 127.062482),
    LatLng(37.289486, 127.062841),
    LatLng(37.290788, 127.061025),
    LatLng(37.290519, 127.061701),
  ];

  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = Set();
  final CameraPosition _parisCameraPosition = CameraPosition(target: _kMapCenter, zoom: 12.0);

  @override
  void initState() {
    super.initState();
  }

  void _updateMarkers(Set<Marker> markers) {
    print('Updated ${markers.length} markers');
    setState(() {
      this._markers = markers;
    });
  }

  Future<BitmapDescriptor> _getMarkerBitmap(int size, {String? text}) async {

    final PictureRecorder pictureRecorder = PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);
    final Paint paint1 = Paint()..color = Colors.orange;
    final Paint paint2 = Paint()..color = color_white;

    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.0, paint1);
    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.2, paint2);
    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.8, paint1);

    if (text != null) {
      TextPainter painter = TextPainter(textDirection: TextDirection.ltr);
      painter.text = TextSpan(
        text: text,
        style: TextStyle(fontSize: size / 3, color: color_white, fontWeight: FontWeight.normal),
      );
      painter.layout();
      painter.paint(
        canvas,
        Offset(size / 2 - painter.width / 2, size / 2 - painter.height / 2),
      );
    }

    final img = await pictureRecorder.endRecording().toImage(size, size);
    final data = await img.toByteData(format: ImageByteFormat.png);

    return BitmapDescriptor.fromBytes(data!.buffer.asUint8List());
  }

  @override
  Widget build(BuildContext context) {
    _add();
    _createMarkerImageFromAsset(context);
    return Container(
      child: SizedBox(
        width: MediaQuery.of(context).size.width, // or use fixed size like 200
        height: MediaQuery.of(context).size.height /**0.4*/,

        child: GoogleMap(
          initialCameraPosition: const CameraPosition(
            target: _kMapCenter,
            zoom: 7.0,
          ),
          markers: Set<Marker>.of(markers.values),
          onMapCreated: _onMapCreated,
          myLocationEnabled: true,
          mapToolbarEnabled: false,
        ),
      ),
    );
  }

  // Set<Marker> _createMarker() {
  //   // TODO(iskakaushik): Remove this when collection literals makes it to stable.
  //   // https://github.com/flutter/flutter/issues/28312
  //   // ignore: prefer_collection_literals
  //   return <Marker>[
  //     Marker(
  //       markerId: MarkerId("marker_1"),
  //       position: _kMapCenter,
  //       icon: _markerIcon,
  //     ),
  //   ].toSet();
  // }

  Future<void> _createMarkerImageFromAsset(BuildContext context) async {
    if (_markerIcon == null) {
      final ImageConfiguration imageConfiguration = createLocalImageConfiguration(context, size: Size.square(48));
      BitmapDescriptor.fromAssetImage(imageConfiguration, 'assets/red_square.png').then(_updateBitmap);
    }
  }

  void _updateBitmap(BitmapDescriptor bitmap) {
    setState(() {
      _markerIcon = bitmap;
    });
  }

  void _onMapCreated(GoogleMapController controllerParam) {
    setState(() {
      controller = controllerParam;
      controller!.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(_kMapCenter.latitude, _kMapCenter.longitude), zoom: 15),
        ),
      );

      // location.onLocationChanged.listen((l) {
      //   // controller.animateCamera(
      //   //   CameraUpdate.newCameraPosition(
      //   //     CameraPosition(target: LatLng(l.latitude, l.longitude),zoom: 15),
      //   //   ),
      //   // );
      // });
    });
  }

  void _add() {
    Marker marker;
    for (var i = 0; i < _latLngs.length; i++) {
      final String markerIdVal = 'marker_id_$_markerIdCounter';
      _markerIdCounter++;
      final MarkerId markerId = MarkerId(markerIdVal);
      marker = Marker(
        markerId: markerId,
        position: _latLngs[i],
        infoWindow: InfoWindow(title: markerIdVal, snippet: '*'),
        onTap: () {
          // _onMarkerTapped(markerId);
          log.d('『GGUMBI』>>> _add ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
        },
        onDragEnd: (LatLng position) {
          // _onMarkerDragEnd(markerId, position);
          log.d('『GGUMBI』>>> _add ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
        },
      );

      setState(() {
        markers[markerId] = marker;
      });

      log.d({
        'markers.length': markers.length,
        '_latLngs.length': _latLngs.length,
        'i': i,
      });
    }
  }
}
