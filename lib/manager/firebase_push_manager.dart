import 'dart:convert';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:fingerpush_plugin/fingerpush_plugin.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:linkmom/data/network/models/data/chat_send_data.dart';
import 'package:linkmom/data/network/models/fcm_token_request.dart';
import 'package:linkmom/data/network/models/notification_list_response.dart';
import 'package:linkmom/data/push/model/linkmompush.dart';
import 'package:linkmom/utils/style/color_style.dart';

import '../main.dart';
import '../route_name.dart';
import '../utils/commons.dart';
import '../utils/custom_view/loading_page.dart';
import '../view/main/chat/chat_room_page.dart';
import 'enum_manager.dart';
import 'enum_utils.dart';

late AndroidNotificationChannel channel;
late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

class FirebasePushManager {
  Function? firebaseCallBack;
  String pushIcon = 'ic_stat_name';

  Future<void> initFirebasePush({Function? callBack}) async {
    if (this.firebaseCallBack == null) {
      this.firebaseCallBack = callBack;
    }
    log.d('『GGUMBI』>>> FirebasePush initFirebasePush :   <<< ');
    // channel = const AndroidNotificationChannel(
    //   'channel_id', // id
    //   '알림', // title
    //   'This channel is used for important notifications.', // description
    //   importance: Importance.high,
    // );
    //
    // flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    // await flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()?.createNotificationChannel(channel);
    // await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    //   alert: true,
    //   badge: true,
    //   sound: true,
    // );

    FirebaseMessaging.instance.onTokenRefresh.listen((event) {
      auth.user.token_fcm = event;
      auth.setUser(auth.user);
      _requestFcmToken(event);
    });

    FirebaseMessaging.instance.getInitialMessage().then((RemoteMessage? message) {
      log.d('『GGUMBI』>>> FirebasePush initState : message: $message,  <<< ');
      getToken();
      // if (message != null) {
      //   Navigator.pushNamed(context, '/message',
      //       arguments: MessageArguments(message, true));
      // }
    });

    ///Firebase Handler 사용시 주석 제거
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      log.d('『GGUMBI』>>> FirebasePush onMessage :  ${message.data},  <<< ');
      log.d('『GGUMBI』>>> FirebasePush onMessage :  ${FingerPush.isFingerPushPayLoad(message.data)}},  <<< ');
      // LinkmomPushData data2 = LinkmomPushData.fromJson(message.data);
      // log.d('『GGUMBI』>>> initFirebasePush : data2: $data2,  <<< ');
      bool isFingerPushNotication = FingerPush.isFingerPushPayLoad(message.data);
      if (isFingerPushNotication) {
        //data.chat  //  chat
        // 361,이하준 링크쌤과 홍지희 맘대디가 매칭 되었습니다.,2022-01-20 17:35
        // {
        //   "chattingroom": 361,
        //   "msg": "이하준 링크쌤과 홍지희 맘대디가 매칭 되었습니다. 맘대디의 결제 완료 후 돌봄이 확정됩니다. (결제 시한 : 2022-01-20 18:35까지)",
        // 	"sendtime": "2022-01-20 17:35:55.145173",
        // }
        ChatSendData chatSendData = ChatSendData();
        String msg = '';
        String? chatData;
        try {
          //실제 핑거푸쉬에서 오는 데이터는 pagename, chattingroom, sendtime 세팅
          //메시지 내용은 -> message.data['data.message'] Url.decode 이후, +는 ' ' 치환해서 사용한다.
          //그외 나머지 데이터 들은 pagename에 맞게끔 예외처리로 사용할것!
          //Uri.decodeFull("%EC%9D%B4%ED%95%98%EC%A4%80+%EB%A7%81%ED%81%AC%EB%A7%98%EA%B3%BC+%ED%99%8D%EC%A7%80%ED%9D%AC+%EB%A7%98%EB%8C%80%EB%94%94%EA%B0%80+%EB%A7%A4%EC%B9%AD+%EB%90%98%EC%97%88%EC%8A%B5%EB%8B%88%EB%8B%A4.+%EB%A7%98%EB%8C%80%EB%94%94%EC%9D%98+%EA%B2%B0%EC%A0%9C+%EC%99%84%EB%A3%8C+%ED%9B%84+%EB%8F%8C%EB%B4%84%EC%9D%B4+%ED%99%95%EC%A0%95%EB%90%A9%EB%8B%88%EB%8B%A4.+%28%EA%B2%B0%EC%A0%9C+%EC%8B%9C%ED%95%9C+%3A+2022-01-20+18%3A35%EA%B9%8C%EC%A7%80%29").replaceAll('+', ' ');
          if (Platform.isIOS) {
            chatSendData = ChatSendData.fromJson(json.decode(message.data['chat']));
            msg = Uri.decodeFull(message.data['alert.body']).replaceAll('+', ' ');
          } else if (Platform.isAndroid) {
            chatData = message.data['data.chat'];
            log.d('『GGUMBI』>>> FirebasePush initFirebasePush : chatData: $chatData, ${message.data['data.chat']} <<< ');
            if (chatData != null) {
              chatData = Uri.decodeFull(chatData).replaceAll('+', ' ');
              ChatFPData chatFPData = ChatFPData.fromJson(json.decode(chatData));
              chatSendData.pagename = chatFPData.pagename;
              chatSendData.chattingroom = chatFPData.chattingroom;
              chatSendData.sendtime = chatFPData.sendtime;
              chatSendData.sendtimestamp = chatFPData.sendtimestamp;
            }
            chatSendData.chattingtype = ChattingType.care.value;
            msg = Uri.decodeFull(message.data['data.message']).replaceAll('+', ' ');
          }

          chatSendData.msgdata = ContractData();
          chatSendData.msg = msg;
          log.d('『GGUMBI』>>> FirebasePush initFirebasePush : data 1 : $chatSendData,  <<< ');

          if (EnumUtils.getNotificationType(chatSendData.pagename).isChat) {
            pushData.setData(LinkmomPushData(
              chatSendData: chatSendData,
              pagename: chatSendData.pagename,
            ));
            log.d('『GGUMBI』>>> FirebasePush initFirebasePush : data 2 : ${pushData.pushData},  <<< ');
            log.d('『GGUMBI』>>> FirebasePush initFirebasePush : data 3 : ${pushData.chatSendData},  <<< ');
          }
        } catch (e) {
          log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
        }
        if (firebaseCallBack != null) {
          firebaseCallBack!(pushData.pushData);
        }
        return;
      }
      ChatSendData chatSendData = ChatSendData.fromJson(json.decode(message.data['data']));
      pushData.setData(LinkmomPushData(
        chatSendData: chatSendData,
        pagename: chatSendData.pagename,
        msgData: NotificationMsgData.fromJson(json.decode(message.data['data'])['msgdata']),
      ));
      // log.d('『GGUMBI』>>> onMessage : data: ${chatSendData},  <<< ');
      // log.d('『GGUMBI』>>> onMessage : data: ${message.data['data']},  <<< ');
      // log.d('『GGUMBI』>>> onMessage : data: ${NotificationMsgData.fromJson(json.decode(message.data['data'])['msgdata'])},  <<< ');
      // log.d('『GGUMBI』>>> onMessage : data: ${pushData.pushData},  <<< ');
      if (firebaseCallBack != null) {
        Map jsonData = json.decode(message.data['data']);
        if (pushData.pushData.msgData != null) {
          if (jsonData['id'] is String) {
            pushData.pushData.msgData!.nextId = int.tryParse(jsonData['id']) ?? 0;
          } else {
            pushData.pushData.msgData!.nextId = jsonData['id'];
          }

          if (pushData.pushData.msgData != null) {
            if (jsonData['send_code'] is String) {
              pushData.pushData.msgData!.sendCode = int.tryParse(jsonData['send_code']) ?? 0;
            } else {
              pushData.pushData.msgData!.sendCode = jsonData['send_code'];
            }
            firebaseCallBack!(pushData.pushData);
          }
        }
      }
      // showNotification(message);///파이어 베이스에선 아무런 처리를 하지 않는다h
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      log.d('『GGUMBI』>>> initState : message: $message,  <<< ');
      // Navigator.pushNamed(context, '/message',
      //     arguments: MessageArguments(message, true));
    });
  }

  Future<void> showNotificationMqtt(ChatSendData? message) async {
    log.d('『GGUMBI』>>> FirebasePush showMQTT : message: ${message.toString()},  <<< ');
    channel = const AndroidNotificationChannel(
      'channel_id', // fingerpush_plugin
      '알림', // title
      description: 'This channel is used for important notifications.',
      importance: Importance.high,
      showBadge: false,
    );

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid = AndroidInitializationSettings('@drawable/$pushIcon');
    var initializationSettingsIOS = IOSInitializationSettings(defaultPresentAlert: false, defaultPresentBadge: true, defaultPresentSound: true, onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: onSelectNotification);

    String msg = message!.msg;
    if (message.is_emoji) {
      msg = "이모티콘_안내".tr();
    }

    ///Firebase FCM 핸들러시 사용 주석 제거 후
    flutterLocalNotificationsPlugin.show(
        0,
        message.title,
        msg,
        NotificationDetails(
          android: AndroidNotificationDetails(
            channel.id,
            channel.name,
            icon: initializationSettingsAndroid.defaultIcon,
            channelDescription: channel.description,
            color: color_main,
          ),
        ),
        // payload: message.toJson().toString());
        payload: json.encode(message.toJson()).toString());
  }

  Future<void> onSelectNotification(String? payload) async {
    log.d('『GGUMBI』>>> FirebasePush onSelectNotification : payload: $payload,  <<< ');
    // if (!fcmData.route.isCurrent) {
    //   Navigator.push(context, fcmData.route);
    // }

    if (payload!.isNotEmpty) {
      ChatSendData chatSendData = ChatSendData.fromJson(json.decode(payload));
      log.d('『GGUMBI』>>> FirebasePush onSelectNotification : chatSendData: $chatSendData,  <<< ');
      if (EnumUtils.getNotificationType(chatSendData.pagename).isChat) {
        if (chatSendData.chattingroom == mqttManager.getChatroomId) {
          // log.d('『GGUMBI』>>> push pushFinger callBack 2 : id:$id, isBadge:$isBadge, isFcm:$isFcm, pagename: ${data.pagename}, roomId: ${mqttManager.getChatroomId}, chattingroom: ${data.data!.chattingroom}, data: $data');

          //내가 있는 채팅방이 다를 경우...
        } else if (chatSendData.chattingroom != mqttManager.getChatroomId) {
          //현재 채팅방이 아니면 stak add   ///내가 있는 화면이 채팅이 아니면, 화면을 이동한다.
          log.d('『GGUMBI』>>> FirebasePush Main build push pushFinger initManager : {}: ${navigatorKey.currentContext},  <<< ');
          if (mqttManager.getCurrentRouteName != routeChatRoom) {
            Commons.page(navigatorKey.currentContext, routeChatRoom,
                arguments: ChatRoomPage(
                  roomType: ChatRoomType.VIEW,
                  receiver: chatSendData.sender,
                  chatroom_id: chatSendData.chattingroom,
                ));
            // log.d('『GGUMBI』>>> FirebasePush push pushFinger callBack 3 : : id:$id, isBadge:$isBadge, isFcm:$isFcm, pagename: ${data.pagename}, roomId: ${mqttManager.getChatroomId}, chattingroom: ${data.data!.chattingroom}, data: $data');

            //현재 채팅방이면 채팅방 전환 화면(추후 작업시 주석 제거)
          } else if (mqttManager.getCurrentRouteName == routeChatRoom) {
            //채팅화면에 있고, (돌봄신청서, 계약서 등) 다른 화면이 있을경우 mainhome으로 보낸후 페이지를 체인지한다.
            Commons.pagePopUntilFirst(navigatorKey.currentContext);
            Commons.page(navigatorKey.currentContext, routeLoading,
                arguments: LoadingPage(
                  routeName: routeChatRoom,
                  pageData: ChatRoomPage(
                    roomType: ChatRoomType.VIEW,
                    receiver: chatSendData.sender,
                    chatroom_id: chatSendData.chattingroom,
                  ),
                ));
            // log.d('『GGUMBI』>>> FirebasePush push pushFinger callBack 4 : : id:$id, isBadge:$isBadge, isFcm:$isFcm, pagename: ${data.pagename}, roomId: ${mqttManager.getChatroomId}, chattingroom: ${data.data!.chattingroom}, data: $data');
          }
        }
      }
    }
  }

  Future onDidReceiveLocalNotification(int id, String? title, String? body, String? payload) async {
    log.i({
      'id': id,
      'title': title,
      'body': body,
      'payload': payload,
    });
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: navigatorKey.currentContext!,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title!),
        content: Text(body!),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Ok'),
            onPressed: () async {
              // Navigator.of(context, rootNavigator: true).pop();
              // await Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     // builder: (context) => SecondScreen(payload),
              //   ),
              // );
            },
          )
        ],
      ),
    );
  }

  ///fcm 토큰 가져오기
  Future<String> getToken() async {
    String token = '';
    await FirebaseMessaging.instance.getToken().then((value) {
      log.d('『GGUMBI』>>> FirebasePush getToken FCM : value: $value,  <<< ');
      auth.user.token_fcm = value!;
      auth.setUser(auth.user);
      _requestFcmToken(value);
    });
    return token;
  }

  ///토큰 등록 및 삭제
  _requestFcmToken(String newToken, {String? deleteToken}) async {
    apiHelper.requestFcmToken(FcmTokenRequest(fcm_token: newToken)).then((response) {
      log.d('『GGUMBI』>>> FirebasePush _requestFcmToken : value: $response,  <<< ');
    });
  }
}
