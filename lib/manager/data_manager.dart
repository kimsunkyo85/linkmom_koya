import 'dart:io';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/area_address_response.dart';
import 'package:linkmom/data/network/models/area_code_search_response.dart';
import 'package:linkmom/data/network/models/area_gps_view_response.dart';
import 'package:linkmom/data/network/models/authcenter_deungbon_request.dart';
import 'package:linkmom/data/network/models/authcenter_home_request.dart';
import 'package:linkmom/data/network/models/authcenter_home_response.dart';
import 'package:linkmom/data/network/models/block_list_response.dart';
import 'package:linkmom/data/network/models/care_list_response.dart';
import 'package:linkmom/data/network/models/care_request.dart';
import 'package:linkmom/data/network/models/care_view_response.dart';
import 'package:linkmom/data/network/models/cash_list_response.dart';
import 'package:linkmom/data/network/models/chat_main_response.dart';
import 'package:linkmom/data/network/models/chat_message_list_response.dart';
import 'package:linkmom/data/network/models/child_drug_response.dart';
import 'package:linkmom/data/network/models/child_info_request.dart';
import 'package:linkmom/data/network/models/child_info_response.dart';
import 'package:linkmom/data/network/models/data/auth_info_data.dart';
import 'package:linkmom/data/network/models/data/chat_main_result_data.dart';
import 'package:linkmom/data/network/models/data/chat_room_data.dart';
import 'package:linkmom/data/network/models/data/chat_send_data.dart';
import 'package:linkmom/data/network/models/data/matching_init_view_data.dart';
import 'package:linkmom/data/network/models/data/page_data.dart';
import 'package:linkmom/data/network/models/findpw_request.dart';
import 'package:linkmom/data/network/models/job_list_request.dart';
import 'package:linkmom/data/network/models/linkmom_list_response.dart';
import 'package:linkmom/data/network/models/matching_view_response.dart';
import 'package:linkmom/data/network/models/momdady_mycares_list_response.dart';
import 'package:linkmom/data/network/models/mypage_myinfo_response.dart';
import 'package:linkmom/data/network/models/mypage_myinfo_save_request.dart';
import 'package:linkmom/data/network/models/notification_list_response.dart';
import 'package:linkmom/data/network/models/pay_list_response.dart';
import 'package:linkmom/data/network/models/point_list_response.dart';
import 'package:linkmom/data/network/models/register_request.dart';
import 'package:linkmom/data/network/models/req_data.dart';
import 'package:linkmom/data/network/models/req_job_data.dart';
import 'package:linkmom/data/network/models/review_list_response.dart';
import 'package:linkmom/data/network/models/survey_answer_request.dart';
import 'package:linkmom/data/network/models/survey_list_response.dart';
import 'package:linkmom/data/network/models/survey_question_response.dart';
import 'package:linkmom/data/network/models/temp_area_response.dart';
import 'package:linkmom/data/network/models/temp_response.dart';
import 'package:linkmom/data/storage/model/child_info.dart';
import 'package:linkmom/data/storage/model/menu_file_data.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/listview/list_child_info_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/view/board/type_selecter.dart';
import 'package:linkmom/view/image/image_add_page.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/mypage/review/review_list_reviewinfo_response.dart';

import '../main.dart';

///성별 (1:남자, 2:여자)
const String GENDER_MAN = '1';

///성별 (1:남자, 2:여자)
const String GENDER_WOMAN = '2';
// enum GENDER_TYPE { normal, man, woman }

///내외국인 구분 (1:내국인, 2:외국인)
const String COUNTRY_LOCAL = '1';

///내외국인 구분 (1:내국인, 2:외국인)
const String COUNTRY_FOREGINER = '2';
// enum COUNTRY_TYPE { normal, local, foreigner }

///통신사 구분 (SKT, KT)
const TELCO_SKT = 'SKT';
const TELCO_KT = 'KT';
// enum TELCO_TYPE { SKT, KT }

///부모님으로 가입 (동의:1*, 비동의:0)
// enum PARENTS {agree, disagreement}

///돌보미로 가입 (동의:1*, 비동의:0)
// enum DOLBOMI {agree, disagreement}

///동의:1, 비동의:0
const int AGREE_int = 1;
const int DISAAGREE_int = 0;
// enum AGREE_TYPE { agree, disagreement }
const AGREE = '1';
const DISAGREE = '0';

///추가서비스(보육 - 21:위생, 22:식사, 23:케어)
///추가서비스(놀이 - 31:미술, 32:야외, 33:학습)
///추가서비스(가사 - 41:청소, 42:주방, 43:빨래)

const int TAB_HOME = 0;
const int TAB_COMMUNITY = 1;
const int TAB_SCHEDULE = 2;
const int TAB_LIST = 3;
const int TAB_CHAT = 4;

///미등록
const int NOT_REGISTER = 99;

class DataManager {
  DataManager({ChildInfoItem? childInfoItem, JobItem? jobItem});

  DataManager.clone(DataManager item) : this(childInfoItem: item.childInfoItem, jobItem: item.jobItem);

  ///로그인 플래그
  RegisterItem registerItem = RegisterItem();

  ///아이디, 비밀번호 찾기 플래그
  FindItem findItem = FindItem();

  ///비밀번호 변경 플래그
  PwChangeItem pwChangeItem = PwChangeItem();

  ///환경인증
  HomeStateItem homeItem = HomeStateItem._init();

  ///아이정보
  ChildInfoItem childInfoItem = ChildInfoItem.init();

  ///돌봄/구직신청
  JobItem jobItem = JobItem.init();

  ///돌봄신청
  PayItem payItem = PayItem._init();

  ///추가 서비스
  ServicesItem serviceItem = ServicesItem._init();

  ///맘대디,링크쌤 조회
  ListViewItem listViewItem = ListViewItem._init();

  ///맘대디,링크쌤 상세
  ListViewDetailItem listViewDetailItem = ListViewDetailItem._init();

  ///위치검색
  AddressItem addressItem = AddressItem._init();

  ///마이페이지-나의정보
  MyInfoItem myInfoItem = MyInfoItem._init();

  ///나의 동네 인증
  LocationAuthItem locationAuthItem = LocationAuthItem._init();

  ///인증센터
  AuthItem authItem = AuthItem._init();

  ///리스트 상세검색필터
  FilterItem filterItem = FilterItem.init();

  ///돌봄관리
  CaresItem caresItem = CaresItem();

  ReviewItem reviewItem = ReviewItem();

  bool isChildInfoData() {
    return isData(childInfoItem.lsChildInfo);
  }

  ///화면 애니메이션 타입
  AnimationType animationType = AnimationType.LEFT;

  ListViewItem listWishView = ListViewItem._init();

  BlockListData blockList = BlockListData(results: []);

  ///채팅
  ChatItem chat = ChatItem.init();

  ///채팅 룸
  ChatRoomItem chatRoom = ChatRoomItem.init();

  ///채팅 신청서 불러오역
  ChatCareInfoItem chatCareInfo = ChatCareInfoItem();

  // 결제내역
  // PayHistoryItem payHistory = PayHistoryItem();

  // 결제내역
  NotificationItem notification = NotificationItem();

  ///일반 폼 하나 있을 경우
  var formKey = GlobalKey<FormState>();

  ///아이디(이메일 형식), 로그인, 회원가입, 비밀번호 찾기에 사용
  var formId = GlobalKey<FormState>();

  ///비밀번호(로그인, 회원가입, 비밀번호재설정)
  var formPw = GlobalKey<FormState>();

  ///비밀번호 재입력(회원가입, 비밀번호 재설정)
  var formPw2 = GlobalKey<FormState>();

  ///휴대번번호 (회원가입, 아이디/비밀번호찾기)
  var formMobile = GlobalKey<FormState>();

  ///이름(회원가입, 아이디찾기(휴대폰)
  var formName = GlobalKey<FormState>();

  ///인증번호(아이디찾기(휴대폰)
  var formAuth = GlobalKey<FormState>();

  ///이름(아이디찾기(이메일)
  var formName2 = GlobalKey<FormState>();

  ///이메일(아이디찾기(이메일)
  var formEmail = GlobalKey<FormState>();

  ///인증번호(아이디찾기(이메일)
  var formAuth2 = GlobalKey<FormState>();

  ///생년월일
  var formBirth = GlobalKey<FormState>();

  ///아이에 대한 요청사항
  var formRemsg = GlobalKey<FormState>();

  ///알레르기가 있어
  var formAllergic = GlobalKey<FormState>();

  ///출발 장소
  var formPlaceStart = GlobalKey<FormState>();

  ///출발 장소 상세
  var formPlaceStartDetail = GlobalKey<FormState>();

  ///출발해야하는 시간
  var formPlaceStartTime = GlobalKey<FormState>();

  ///출발 장소설명
  var formPlaceStartContent = GlobalKey<FormState>();

  ///경유지 장소
  var formPlacePathRoute = GlobalKey<FormState>();

  ///도착 주소
  var formPlaceEnd = GlobalKey<FormState>();

  ///도착 주소 상세
  var formPlaceEndDetail = GlobalKey<FormState>();

  ///도착해야하는 시간
  var formPlaceEndTime = GlobalKey<FormState>();

  ///도착 추가설명
  var formPlaceEndContent = GlobalKey<FormState>();

  /// 일반 scaffoldKey
  var scaffoldKey = GlobalKey<ScaffoldState>();

  ///원하는 이웃집 위치1
  var formHomeLocation1 = GlobalKey<FormState>();

  ///원하는 이웃집 위치2
  var formHomeLocation2 = GlobalKey<FormState>();

  ///보육장소
  var formBoyukAddress = GlobalKey<FormState>();

  ///보육장소 상세
  var formBoyukAddressDetail = GlobalKey<FormState>();

  ///동네인증하기
  var formAddressAuth = GlobalKey<FormState>();

  ///나의 직업
  var formJob = GlobalKey<FormState>();

  ///돌봄방식
  var formJobCare = GlobalKey<FormState>();

  ///돌봄계약서 서명
  var formSign = GlobalKey<FormState>();

  ///돌봄계약서 서명 상세
  var formSignDetail = GlobalKey<FormState>();

  ///보유 포인트
  var formPoint = GlobalKey<FormState>();

  ///보유 캐시
  var formCache = GlobalKey<FormState>();

  ///알러지 이름
  var formAllergyName = GlobalKey<FormState>();

  ///알러지 내용
  var formAllergyMessage = GlobalKey<FormState>();

  DateTime alert = DateTime.now().add(Duration(minutes: 3));

  // DateTime alert = DateTime.now().add(Duration(seconds: 3));

  void resetAlert() {
    alert = DateTime.now().add(Duration(minutes: 3));
    // alert = DateTime.now().add(Duration(seconds: 3));
  }

  List<String> emails = [
    "이메일선택".tr(),
    "네이버_이메일".tr(),
    "한메일_이메일".tr(),
    "지메일_이메일".tr(),
    "네이트_이메일".tr(),
    "다음_이메일".tr(),
    "핫메일_이메일".tr(),
    "직접입력".tr(),
  ];

  TextEditingController tcId = TextEditingController(text: "");
  TextEditingController tcPassword = TextEditingController(text: "");
  TextEditingController tcPasswordConfirm = TextEditingController(text: "");
  TextEditingController tcMobile = TextEditingController(text: "");
  TextEditingController tcName = TextEditingController(text: "");
  TextEditingController tcAuth = TextEditingController(text: "");
  TextEditingController tcEmailEmail = TextEditingController(text: "");
  TextEditingController tcEmailName = TextEditingController(text: "");
  TextEditingController tcEmailAuth = TextEditingController(text: "");
  TextEditingController tcBirth = TextEditingController(text: "");
  TextEditingController tcReMsg = TextEditingController(text: "");
  TextEditingController tcAllergic = TextEditingController(text: "");

  ///출발 만나는 장소
  TextEditingController tcPlaceStart = TextEditingController(text: "");

  ///출발 만나는 장소 상세
  TextEditingController tcPlaceStartDetail = TextEditingController(text: "");

  ///출발해야하는 시간
  TextEditingController tcPlaceStartTime = TextEditingController(text: "");

  ///출발 장소설명
  TextEditingController tcPlaceStartComment = TextEditingController(text: "");

  ///경유지 장소
  TextEditingController tcPlacePathRoute = TextEditingController(text: "");

  ///도착 장소
  TextEditingController tcPlaceEnd = TextEditingController(text: "");

  ///도착해야하는 시간
  TextEditingController tcPlaceEndTime = TextEditingController(text: "");

  ///도착 장소 상세
  TextEditingController tcPlaceEndDetail = TextEditingController(text: "");

  ///도착 추가설명
  TextEditingController tcPlaceEndComment = TextEditingController(text: "");

  ///이웃집 원하는 위치 1순위
  TextEditingController tcHomeLocation1 = TextEditingController(text: "");

  ///이웃집 원하는 위치 2순위
  TextEditingController tcHomeLocation2 = TextEditingController(text: "");

  ///보육장소 주소
  TextEditingController tcBoyukAddress = TextEditingController(text: "");

  ///보육장소 주소 상세
  TextEditingController tcBoyukAddressDetail = TextEditingController(text: "");

  ///동네인증
  TextEditingController tcAddressAuth = TextEditingController(text: "");

  ///나의직업
  TextEditingController tcJob = TextEditingController(text: "");

  ///나의직업
  TextEditingController tcJobCare = TextEditingController(text: "");

  ///돌봄계약서 서명
  TextEditingController tcSign = TextEditingController(text: "");

  ///돌봄계약서 서명 상세
  TextEditingController tcSignDetail = TextEditingController(text: "");

  ///보유 포인트
  TextEditingController tcPoint = TextEditingController(text: "");

  ///보유 캐시
  TextEditingController tcCache = TextEditingController(text: "");

  ///알러지 이름
  TextEditingController tcAllergyName = TextEditingController(text: "");

  ///알러지 내용
  TextEditingController tcAllergyMessage = TextEditingController(text: "");

  String id = '';
  String password = '';
  String passwordConfirm = '';
  String mobile = '';
  String name = '';
  String auth = '';
  late String dropValue = "이메일선택".tr();
  String birth = '';
  late String remsg;
  late String allergic;

  late String emailMobile;
  late String emailName;
  late String emailAuth;

  late String placeStart;
  late String placeEnd;

  late String point;
  late String cash;

  bool enableRegister({VoidCallback? fn}) {
    if (fn != null) {
      fn();
    }
    return true;
  }

  bool isData(List<dynamic>? data) {
    if (data != null && data.length == 0) {
      return false;
    }
    return true;
  }

  bool isAll = false;

  bool isAnimation = false;

  ///일반 화면에서 체크 플래그로 사용
  bool isSelect = false;

// ChildInfo get childInfo => jobItem.childInfo;
// setChildInfo(ChildInfo childInfo){
//   jobItem.childInfo = childInfo;
// }
//
// CareRequest get careRequest => jobItem.request;
// setCareRequest(CareRequest careRequest){
//   jobItem.request = careRequest;
// }
//
// ReqData get careReqData => jobItem.reqData;
// setReqData(ReqData careReqData){
//   jobItem.reqData = careReqData;
// }
//
// TempResponse get tempResponse => jobItem.tempResponse;
// setTempResponse(TempResponse tempResponse){
//   jobItem.tempResponse = tempResponse;
// }
//
// Map get jobData => jobItem.jobData;
// setJobData(Map jobData){
//   jobItem.jobData = jobData;
// }
  double width(BuildContext context, double percent) => MediaQuery.of(context).size.width * percent;

  double height(BuildContext context, double percent) => MediaQuery.of(context).size.height * percent;
}

class ViewMode {
  ViewType viewType;
  ItemType itemType;

  ViewMode({this.viewType = ViewType.apply, this.itemType = ItemType.add});

  @override
  String toString() {
    return 'ViewMode{viewType: $viewType, itemType: $itemType}';
  }
}

class RegisterItem {
  ///회원가입 유효성 검사 에러 데이터
  RegisterRequest registerErrors = RegisterRequest();

  RegisterItem();

  bool idConfirm = false;
  bool passwordConfirm = false;
  bool passwordReConfirm = false;
  bool mobileConfirm = false;
  bool authConfirm = false;
  bool nameConfirm = false;

  ///로그인 버튼 활성화
  bool isLoginConfirm() {
    if (!idConfirm || !passwordConfirm) {
      return false;
    }
    return true;
  }

  ///회원가입 버튼 활성화
  bool isRegisterConfirm() {
    log.d({
      'idConfirm': idConfirm,
      'passwordConfirm': passwordConfirm,
      'passwordReConfirm': passwordReConfirm,
      'mobileConfirm': mobileConfirm,
      'nameConfirm': nameConfirm,
    });
    if (!idConfirm || !passwordConfirm || !passwordReConfirm || !mobileConfirm || !nameConfirm) {
      return false;
    }
    return true;
  }
}

class FindItem {
  FindItem();

  FindType findType = FindType.mobile;
  AuthType authType = AuthType.mobile;

  bool isMobile = true;
  bool isMobileNameConfirm = false;
  bool isMobileNumberConfirm = false;
  bool isMobileAuthConfirm = false;
  bool isMobileAuth = false;
  bool isMobileTimer = false;
  bool isEmail = false;
  bool isEmailNameConfirm = false;
  bool isEmailNumberConfirm = false;
  bool isEmailAuthConfirm = false;
  bool isEmailAuth = false;
  bool isEmailTimer = false;

  ///모바일 찾기 버튼 활성화
  bool getFindMobileConfirm() {
    log.d({
      'isMobileNameConfirm': isMobileNameConfirm,
      'isMobileNumberConfirm': isMobileNumberConfirm,
      'isMobileAuthConfirm': isMobileAuthConfirm,
    });
    if (!isMobileNameConfirm || !isMobileNumberConfirm || !isMobileAuthConfirm) {
      return false;
    }
    return true;
  }

  ///이메일 찾기 버튼 활성화
  bool getFindEmailConfirm() {
    log.d({
      'isEmailNameConfirm': isEmailNameConfirm,
      'isEmailNumberConfirm': isEmailNumberConfirm,
      'isEmailAuthConfirm': isEmailAuthConfirm,
    });
    if (!isEmailNameConfirm || !isEmailNumberConfirm || !isEmailAuthConfirm) {
      return false;
    }
    return true;
  }

  enableMobile({VoidCallback? fn}) {
    if (fn != null) {
      isMobile = true;
      isEmail = false;
      isEmailAuth = false;
      fn();
    }
  }

  disableMobile({VoidCallback? fn}) {
    if (fn != null) {
      isMobile = false;
      isMobileAuth = false;
      fn();
    }
  }

  enableMobileAuth({VoidCallback? fn}) {
    if (fn != null) {
      isMobileAuth = true;
      fn();
    }
  }

  disableMobileAuth({VoidCallback? fn}) {
    if (fn != null) {
      isMobileAuth = false;
      fn();
    }
  }

  enableEmail({VoidCallback? fn}) {
    if (fn != null) {
      isEmail = true;
      isMobile = false;
      isMobileAuth = false;
      fn();
    }
  }

  disableEmail({VoidCallback? fn}) {
    if (fn != null) {
      isEmail = false;
      isMobileAuth = false;
      fn();
    }
  }

  enableEmailAuth({VoidCallback? fn}) {
    if (fn != null) {
      isEmailAuth = true;
      fn();
    }
  }

  disableEmailAuth({VoidCallback? fn}) {
    if (fn != null) {
      isEmailAuth = false;
      fn();
    }
  }

  enableTimerMobile({VoidCallback? fn}) {
    if (fn != null) {
      isMobileTimer = true;
      isEmailTimer = false;
      fn();
    }
  }

  disableTimerMobile({VoidCallback? fn}) {
    if (fn != null) {
      isMobileTimer = false;
      fn();
    }
  }

  enableTimerEmail({VoidCallback? fn}) {
    if (fn != null) {
      isEmailTimer = true;
      isMobileTimer = false;
      fn();
    }
  }

  disableTimerEmail({VoidCallback? fn}) {
    if (fn != null) {
      isEmailTimer = false;
      fn();
    }
  }
}

class PwChangeItem {
  FindPwRequest? request;

  PwChangeItem();

  bool passwordConfirm = false;
  bool passwordReConfirm = false;

  ///비밀번호 변경 버튼 활성화
  bool getPasswordChangeConfirm() {
    log.d({
      'passwordConfirm': passwordConfirm,
      'passwordReConfirm': passwordReConfirm,
    });
    if (!passwordConfirm || !passwordReConfirm) {
      return false;
    }
    return true;
  }

  @override
  String toString() {
    return 'PwChangeItem{passwordConfirm: $passwordConfirm, passwordReConfirm: $passwordReConfirm}';
  }
}

class HomeStateItem {
  ///우리집 주거티입
  List<SingleItem> ls_ourhome = [];

  ///우리집 cctv
  List<SingleItem> ls_outhome_cctv = [];

  ///우리집 반려동물
  List<SingleItem> ls_outhome_animal = [];

  ///이웃집 주거타입
  List<SingleItem> ls_nbhhome = [];

  ///이웃집 cctv
  List<SingleItem> ls_nbhhome_cctv = [];

  ///이웃집 반려동물
  List<SingleItem> ls_nbhhome_animal = [];

  ///이웃집 사진
  List<SingleItem> ls_nbhhome_picture = [];
  bool isSkip = false;
  List<ImageItem> images = [];

  AuthCenterHomeRequest request = AuthCenterHomeRequest();
  AuthCenterHomeResponse response = AuthCenterHomeResponse();

  HomeStateItem._init() {
    ///우리집
    ls_ourhome.add(SingleItem("환경인증_아파트".tr(), keyName: HomeStateData.Key_ourhome_type, type: HomeType.apartment.index));
    ls_ourhome.add(SingleItem("환경인증_오피스텔".tr(), keyName: HomeStateData.Key_ourhome_type, type: HomeType.officetel.index));
    ls_ourhome.add(SingleItem("환경인증_빌라".tr(), keyName: HomeStateData.Key_ourhome_type, type: HomeType.villa.index));
    ls_ourhome.add(SingleItem("환경인증_전원주택".tr(), keyName: HomeStateData.Key_ourhome_type, type: HomeType.house.index));

    ls_outhome_cctv.add(SingleItem("환경인증_CCTV있음".tr(), keyName: HomeStateData.Key_ourhome_cctv, type: CctvType.yes.index));
    ls_outhome_cctv.add(SingleItem("환경인증_CCTV없음".tr(), keyName: HomeStateData.Key_ourhome_cctv, type: CctvType.no.index));
    ls_outhome_cctv.add(SingleItem("환경인증_설치예정".tr(), keyName: HomeStateData.Key_ourhome_cctv, type: CctvType.install.index));

    ls_outhome_animal.add(SingleItem("없음".tr(), keyName: HomeStateData.Key_ourhome_animal, type: AnimalType.no.index));
    ls_outhome_animal.add(SingleItem("환경인증_강아지".tr(), keyName: HomeStateData.Key_ourhome_animal, type: AnimalType.dog.index));
    ls_outhome_animal.add(SingleItem("환경인증_고양이".tr(), keyName: HomeStateData.Key_ourhome_animal, type: AnimalType.cat.index));
    ls_outhome_animal.add(SingleItem("기타".tr(), keyName: HomeStateData.Key_ourhome_animal, type: AnimalType.etc.index));

    ///이웃집
    ls_ourhome.forEach((value) {
      ls_nbhhome.add(SingleItem(value.name, keyName: HomeStateData.Key_nbhhome_type, image: value.image, type: value.type));
    });

    ls_nbhhome_cctv.add(SingleItem("환경인증_있으면좋겠어요".tr(), keyName: HomeStateData.Key_nbhhome_cctv, type: FineType.ok.index));
    ls_nbhhome_cctv.add(SingleItem("환경인증_없어도괜찮아요".tr(), keyName: HomeStateData.Key_nbhhome_cctv, type: FineType.no.index));

    ls_nbhhome_animal.add(SingleItem("환경인증_있어도괜찮아요".tr(), keyName: HomeStateData.Key_nbhhome_animal, type: FineType.ok.index));
    ls_nbhhome_animal.add(SingleItem("환경인증_없었으면좋겠어요".tr(), keyName: HomeStateData.Key_nbhhome_animal, type: FineType.no.index));

    ls_nbhhome_picture.add(SingleItem("환경인증_있으면좋겠어요".tr(), keyName: HomeStateData.Key_nbhhome_picture, type: FineType.ok.index));
    ls_nbhhome_picture.add(SingleItem("환경인증_없어도괜찮아요".tr(), keyName: HomeStateData.Key_nbhhome_picture, type: FineType.no.index));
  }

  @override
  String toString() {
    return 'HomeStateItem{ls_ourhome: $ls_ourhome, ls_outhome_cctv: $ls_outhome_cctv, ls_outhome_animal: $ls_outhome_animal, ls_nbhhome: $ls_nbhhome, ls_nbhhome_cctv: $ls_nbhhome_cctv, ls_nbhhome_animal: $ls_nbhhome_animal, ls_nbhhome_picture: $ls_nbhhome_picture, isSkip: $isSkip, images: $images, request: $request, response: $response}';
  }
}

class ChildInfoItem {
  ///우리집 주거티입
  List<ChildInfoItemData> lsChildInfo = [];

  // ///아이정보 선택 아이템
  // ChildInfoItemData childInfoItemData;

  ChildInfoRequest request = ChildInfoRequest();
  ChildInfoResponse response = ChildInfoResponse();
  ChildInfoData itemData = ChildInfoData();
  DrugInfoItem drugInfoItem = DrugInfoItem._init();

  String requestBirth = '';

  ///성별
  List<SingleItem> lsGender = [];

  ///보육기관
  List<SingleItem> lsNursery = [];

  ///아이성향
  int? characterValue;
  List<SelecterModel> lsCharacter = [];
  List<int> lsCharacterValue = [];

  bool isAllergic = false;
  bool isParm = false;

  ChildInfoItem.init() {
    lsGender.add(SingleItem("남아".tr(), type: 1));
    lsGender.add(SingleItem("여아".tr(), type: 2));

    lsNursery.add(SingleItem(ChildNurseryType.nursery_0.string));
    lsNursery.add(SingleItem(ChildNurseryType.nursery_1.string));
    lsNursery.add(SingleItem(ChildNurseryType.nursery_2.string));
    lsNursery.add(SingleItem(ChildNurseryType.nursery_3.string));
    lsNursery.add(SingleItem(ChildNurseryType.nursery_4.string));
    lsNursery.add(SingleItem(ChildNurseryType.nursery_5.string));

    lsCharacter = [
      SelecterModel(ChildCharacterType.character_0.string, ChildCharacterType.character_0.index),
      SelecterModel(ChildCharacterType.character_1.string, ChildCharacterType.character_1.index),
      SelecterModel(ChildCharacterType.character_2.string, ChildCharacterType.character_2.index),
      SelecterModel(ChildCharacterType.character_3.string, ChildCharacterType.character_3.index),
      SelecterModel(ChildCharacterType.character_4.string, ChildCharacterType.character_4.index),
      SelecterModel(ChildCharacterType.character_5.string, ChildCharacterType.character_5.index),
      SelecterModel(ChildCharacterType.character_6.string, ChildCharacterType.character_6.index),
      SelecterModel(ChildCharacterType.character_7.string, ChildCharacterType.character_7.index),
      SelecterModel(ChildCharacterType.character_8.string, ChildCharacterType.character_8.index),
      SelecterModel(ChildCharacterType.character_9.string, ChildCharacterType.character_9.index),
    ];
  }

  @override
  String toString() {
    return 'ChildInfoItem{lsChildInfo: $lsChildInfo, request: $request, response: $response, itemData: $itemData, requestBirth: $requestBirth, lsGender: $lsGender, lsNursery: $lsNursery, characterValue: $characterValue, lsCharacter: ${lsCharacter.first}, lsCharacterValue: $lsCharacterValue, isAllergic: $isAllergic, isParm: $isParm}';
  }
}

class CharacterData {
  static const String Key_value = 'value';
  static const String Key_title = 'title';
  static const String Key_brand = 'brand';
  static const String Key_body = 'body';

  String value;
  String title;
  String brand;
  String body;

  CharacterData({this.value = '', this.title = '', this.brand = '', this.body = ''});

  @override
  String toString() {
    return 'CharacterData{value: $value, title: $title, brand: $brand, body: $body}';
  }
}

class JobItem {
  ///돌봄신청
  CareRequest request = CareRequest();

  ///돌봄신청 상세데이터
  ReqData reqdata = ReqData.init();

  ///구직신청
  ReqJobData requestJob = ReqJobData.init();

  ///아이 기본 정보
  ChildInfo childInfo = ChildInfo();

  ///임시저장값
  TempResponse tempResponse = TempResponse();

  ///임시저장값(우리집/이웃집 클릭시 사용)
  TempResponse tempData = TempResponse();

  Map? jobData = Map();

  // List<ChildInfoItemData> lsJob = [];

  ///돌봄 유형(등원(go),하원(home),학습(nursery),보육(learning),가사 등)
  List<SingleItem> lsJobType = [];

  ///돌봄 타입(돌봄장소 (1:우리집(ourhome), 2:이웃집(nbhhome)))
  List<SingleItem> lsJobArea = [];

  ///돌봄 이동방법(0:도보, 1:자차, 2:대중교통)
  List<SingleItem> lsJobMoveType = [];

  ///돌봄 링크쌤집 거리(0:1km, 1:5km, 2:10km)
  List<SingleItem> lsJobLocation = [];

  ///등원/하원 출발,도착장소 및 이동방법 데이터
  ReqSchoolToHomeData moveData = ReqSchoolToHomeData();

  ///보육 출발,도착장소 및 이동방법 데이터
  ReqBoyukData boyukData = ReqBoyukData();

  ///출발시간
  DateTime startTime = DateTime.now();

  ///도착시간
  DateTime endTime = DateTime.now();

  ///돌봄 날짜 선택(0:매주반복)
  List<SingleItem> lsJobCalendarType = [];

  ///날짜/시간선택
  List<ReqJobScheduleData> careschedule = [];

  ///돌봄신청완료 이동화면 선택 (HOME, 예약관리, 추가돌봄)
  List<SingleItem> lsViewType = [];

  ///구직신청 돌봄대상 (0:신생아, 1:영아, 2:유아, 3:초등학생, 4:중학생, 5:고등학생) 선택 용도
  List<SingleItem> lsJobCareTargetType = [];

  ///구직신청 등록내역 돌봄유형
  List<SingleItem> lsJobCareData = [];

  ///구직신청 등록내역 보육장소
  List<SingleItem> lsJobAreaData = [];

  ///구직신청 등록내역 돌봄대상(등록내역에서 사용) (0:신생아, 1:영아, 2:유아, 3:초등학생, 4:중학생, 5:고등학생) 보는 용도
  List<SingleItem> lsJobCareTargetData = [];

  ///구직신청 등록내역 희망급여
  List<SingleItem> lsJobPayData = [];

  ///시작날짜시간
  DateTime startDate = DateTime(1970, 1, 1, 8, 0);

  ///종료날짜시간
  DateTime endDate = DateTime(1970, 1, 1, 8, 30);

  ///돌봄 총 신청 시간
  int totalTime = 0;

  ///옵션 선택시 시간
  int calcuTime = 0;

  int boyukCnt = 0;
  int homeCareCnt = 0;
  int playCnt = 0;

  bool isBoyuk = false;

  bool isHomeCare = false;

  bool isPlay = false;

  int scheduleLength = 0;

  ServiceType serviceType = ServiceType.serviceType_0;

  ///장소 리스트 크기
  int tempAreaLength = 0;

  ///돌봄장소 리스트 데이터
  List<TempAreaData> tempAreaList = [];

  ///출발장소 데이터
  TempAreaData temp_area_start = TempAreaData();

  ///도착장소 데이터
  TempAreaData temp_area_end = TempAreaData();

  JobItem.init() {
    request = CareRequest();
    reqdata = ReqData.init();
    requestJob = ReqJobData.init();
    lsJobType.add(SingleItem("등원돌봄".tr(), content: "등원_메시지".tr(), icon: Lcons.go_school(), type: ServiceType.serviceType_0.index));
    lsJobType.add(SingleItem("하원돌봄".tr(), content: "하원_메시지".tr(), icon: Lcons.go_home(), type: ServiceType.serviceType_1.index));
    lsJobType.add(SingleItem("보육돌봄".tr(), content: "보육_메시지".tr(), icon: Lcons.care(), type: ServiceType.serviceType_2.index));

    lsJobMoveType.add(SingleItem("도보".tr(), content: "도보_메시지".tr(), values: [SingleItem("도보_아이템_1".tr()), SingleItem("도보_아이템_2".tr())]));
    lsJobMoveType.add(SingleItem("자동차".tr(), content: "자동차_메시지".tr(), values: [SingleItem("자동차_아이템_1".tr()), SingleItem("자동차_아이템_2".tr())]));
    lsJobMoveType.add(SingleItem("대중교통".tr(), content: "대중교통_메시지".tr()));

    lsJobLocation.add(SingleItem("1km이내".tr(), content: '1', type: 1));
    lsJobLocation.add(SingleItem("5km이내".tr(), content: '5', type: 5));
    lsJobLocation.add(SingleItem("10km이내".tr(), content: '10', type: 10));

    lsJobCalendarType.add(SingleItem("매주반복".tr(), content: CalendarType.week.index.toString()));

    lsViewType.add(SingleItem(MoveMenuType.menu_0.name, data: MoveMenuType.menu_0));
    lsViewType.add(SingleItem(MoveMenuType.menu_1.name, data: MoveMenuType.menu_1));
    lsViewType.add(SingleItem(MoveMenuType.menu_2.name, data: MoveMenuType.menu_2));
    lsViewType.add(SingleItem(MoveMenuType.menu_3.name, data: MoveMenuType.menu_3));

    lsJobCareTargetType.add(SingleItem("신생아".tr(), content: "(0-6개월)".tr(), type: CareTargetType.careTarget_0.index));
    lsJobCareTargetType.add(SingleItem("영아".tr(), content: "(7-36개월)".tr(), type: CareTargetType.careTarget_1.index));
    lsJobCareTargetType.add(SingleItem("유아".tr(), content: "(4세-7세)".tr(), type: CareTargetType.careTarget_2.index));
    lsJobCareTargetType.add(SingleItem("초등학생".tr(), content: "(8세-13세)".tr(), type: CareTargetType.careTarget_3.index));
    lsJobCareTargetType.add(SingleItem("중학생".tr(), content: "(14세-16세)".tr(), type: CareTargetType.careTarget_4.index));
    lsJobCareTargetType.add(SingleItem("고등학생".tr(), content: "(17세-19세)".tr(), type: CareTargetType.careTarget_5.index));

    lsJobCareData = [];
    lsJobAreaData = [];
    lsJobCareTargetData = [];
    lsJobPayData = [];
    temp_area_start = TempAreaData();
    temp_area_end = TempAreaData();
  }

  makeData({ViewType? viewType, ServiceType? serviceType}) {
    lsJobArea.clear();
    String momdady = "맘대디집".tr();
    String momdady2 = '(${"우리집".tr()})';
    String linkmom = "링크쌤집".tr();
    String linkmom2 = '(${"이웃집".tr()})';
    serviceType = serviceType == null ? ServiceType.serviceType_0 : serviceType;
    if (ViewType.view == viewType) {
      if (!Commons.isLinkMom()) {
        lsJobArea.add(SingleItem(
          linkmom,
          name2: momdady2,
          content: "링크쌤집_메시지".tr(),
          icon: Lcons.my_house(),
          type: PossibleArea.mom_daddy.value,
          values: [
            SingleItem(
              "그룹보육가능".tr(),
              name2: "그룹보육가능_여부".tr(),
              content: "그룹보육가능_안내".tr(),
            )
          ],
        ));
        lsJobArea.add(SingleItem(
          momdady,
          name2: linkmom2,
          content: "맘대디집_메시지".tr(),
          icon: Lcons.neighbor_house(),
          type: PossibleArea.link_mom.value,
          values: [
            SingleItem(
              "동시보육가능".tr(),
              name2: "동시보육가능_여부".tr(),
              content: "동시보육가능_안내".tr(),
            )
          ],
        ));
      } else {
        lsJobArea.add(SingleItem(
          momdady,
          name2: momdady2,
          content: "우리집_등원_메시지".tr(),
          icon: Lcons.my_house(),
          type: PossibleArea.mom_daddy.value,
        ));
        lsJobArea.add(SingleItem(
          linkmom,
          name2: linkmom2,
          content: "이웃집_등원_메시지".tr(),
          icon: Lcons.neighbor_house(),
          type: PossibleArea.link_mom.value,
        ));
      }
    } else {
      if (!Commons.isLinkMom()) {
        lsJobArea.add(SingleItem(
          momdady,
          name2: momdady2,
          content: serviceType.service[0],
          icon: Lcons.my_house(),
          type: PossibleArea.mom_daddy.value,
        ));

        lsJobArea.add(SingleItem(
          linkmom,
          name2: linkmom2,
          content: serviceType.service[1],
          icon: Lcons.neighbor_house(),
          type: PossibleArea.link_mom.value,
        ));
      } else {
        lsJobArea.add(SingleItem(
          linkmom,
          name2: momdady2,
          content: "링크쌤집_메시지".tr(),
          icon: Lcons.my_house(),
          type: PossibleArea.mom_daddy.value,
          values: [
            SingleItem(
              "그룹보육가능".tr(),
              name2: "그룹보육가능_여부".tr(),
              content: "그룹보육가능_안내".tr(),
            )
          ],
        ));
        lsJobArea.add(SingleItem(
          momdady,
          name2: linkmom2,
          content: "맘대디집_메시지".tr(),
          icon: Lcons.neighbor_house(),
          type: PossibleArea.link_mom.value,
          values: [
            SingleItem(
              "동시보육가능".tr(),
              name2: "동시보육가능_여부".tr(),
              content: "동시보육가능_안내".tr(),
            )
          ],
        ));
      }
    }
  }

  int calcuTotalTime(DataManager data, ViewType viewType) {
    int totalTime = 0;
    if (data == null) {
      return totalTime;
    }
    log.d('『GGUMBI』>>> calcuTotalTime : : $viewType,  <<< ');
    if (viewType == ViewType.modify) {
      data.jobItem.reqdata.bookingboyukoptions?.forEach((serviceData) {
        data.serviceItem.lsBoyuk.forEach((item) {
          item.values!.forEach((value) {
            if (serviceData.product_id == value.products!.product_id) {
              totalTime += value.products!.product_data!.b_minutes;
            }
          });
        });
      });

      data.jobItem.reqdata.bookinghomecareoptions?.forEach((serviceData) {
        data.serviceItem.lsHomeCare.forEach((item) {
          item.values!.forEach((value) {
            if (serviceData.product_id == value.products!.product_id) {
              totalTime += value.products!.product_data!.b_minutes;
            }
          });
        });
      });

      data.jobItem.reqdata.bookingplayoptions?.forEach((serviceData) {
        data.serviceItem.lsPlay.forEach((item) {
          item.values!.forEach((value) {
            if (serviceData.product_id == value.products!.product_id) {
              totalTime += value.products!.product_data!.b_minutes;
            }
          });
        });
      });
    } else {
      if (data.jobItem.tempResponse.dataList == null) {
        return totalTime;
      }
      data.jobItem.tempResponse.getData().reqdata!.bookingboyukoptions?.forEach((serviceData) {
        data.serviceItem.lsBoyuk.forEach((item) {
          item.values!.forEach((value) {
            if (serviceData.product_id == value.products!.product_id) {
              totalTime += value.products!.product_data!.b_minutes;
            }
          });
        });
      });

      data.jobItem.tempResponse.getData().reqdata!.bookinghomecareoptions?.forEach((serviceData) {
        data.serviceItem.lsHomeCare.forEach((item) {
          item.values!.forEach((value) {
            if (serviceData.product_id == value.products!.product_id) {
              totalTime += value.products!.product_data!.b_minutes;
            }
          });
        });
      });

      data.jobItem.tempResponse.getData().reqdata!.bookingplayoptions?.forEach((serviceData) {
        data.serviceItem.lsPlay.forEach((item) {
          item.values!.forEach((value) {
            if (serviceData.product_id == value.products!.product_id) {
              totalTime += value.products!.product_data!.b_minutes;
            }
          });
        });
      });
    }

    log.d('『GGUMBI』>>> calcuTotalTime 4 : totalTime: $totalTime,  <<< ');
    return totalTime;
  }

  int selectTotalTime(DataManager data) {
    int totalTime = 0;
    if (data == null) {
      return totalTime;
    }
    data.serviceItem.lsBoyuk.forEach((item) {
      item.values!.forEach((value) {
        if (value.isSelected) {
          totalTime += value.products!.product_data!.b_minutes;
        }
      });
    });

    data.serviceItem.lsHomeCare.forEach((item) {
      item.values!.forEach((value) {
        if (value.isSelected) {
          totalTime += value.products!.product_data!.b_minutes;
        }
      });
    });

    data.serviceItem.lsPlay.forEach((item) {
      item.values!.forEach((value) {
        if (value.isSelected) {
          totalTime += value.products!.product_data!.b_minutes;
        }
      });
    });

    log.d('『GGUMBI』>>> selectTotalTime 4 : totalTime: $totalTime,  <<< ');
    return totalTime;
  }

// JobItem clone() {
//   final jsonResponse = json.decode(json.encode(this));
//   return JobItem.fromJson(jsonResponse as Map<String, dynamic>);
// }
}

///결제금액
class PayItem {
  ///스피드매칭( 1단계, 2단계, 3단계)
  List<SingleItem> lsSpeedType = [];

  ///그룹 보육신청 ( 1단계, 2단계, 3단계)
  List<SingleItem> lsGroupType = [];

  ///시급 협의가능 플래그
  bool isPay = false;

  ///시급 가격
  String payValue = '';

  ///기본 시급 9000원
  int payPrice = 9000;

  bool isSpeed = false;

  ///스피드매칭 값 (1,2,3단계)
  String speedValue = '';

  ///스피드매칭 선택 값
  String speedSelect = "스피드매칭_선택".tr();

  ///스피드매칭 선택 가격 계산값
  int speedPrice = 0;

  ///선택값
  late SingleItem speedItem;

  ///신청기간
  int day = 1;

  ///긴급돌봄
  int emergencyPrice = 0;

  ///그룹돌봄시
  bool isGroup = false;

  PayItem._init() {
    // lsSpeedType.add(SingleItem("스피드1".tr(), content: "스피드1내용".tr(), image: IMG_CARETYPE_SPEED_1_A, imageDisable: IMG_CARETYPE_SPEED_1_D, type: 1));
    // lsSpeedType.add(SingleItem("스피드2".tr(), content: "스피드2내용".tr(), image: IMG_CARETYPE_SPEED_2_A, imageDisable: IMG_CARETYPE_SPEED_2_D, type: 2));
    // lsSpeedType.add(SingleItem("스피드3".tr(), content: "스피드3내용".tr(), image: IMG_CARETYPE_SPEED_3_A, imageDisable: IMG_CARETYPE_SPEED_3_D, type: 3));

    lsSpeedType.add(SingleItem("스피드1".tr(), content: "스피드1내용".tr(), type: 5000));
    lsSpeedType.add(SingleItem("스피드2".tr(), content: "스피드2내용".tr(), type: 10000));
    lsSpeedType.add(SingleItem("스피드3".tr(), content: "스피드3내용".tr(), type: 20000));

    lsGroupType.add(SingleItem("아이1명만".tr(), content: "스피드1내용".tr()));
    lsGroupType.add(SingleItem("아이2명이하".tr(), content: "스피드2내용".tr()));
    lsGroupType.add(SingleItem("아이3명이하".tr(), content: "스피드3내용".tr()));
  }
}

///추가서비스(보육,놀이,가사서비스)
class ServicesItem {
  ///보육서비스
  List<ServicesSelectItem> lsBoyuk = [];

  ///가사서비스
  List<ServicesSelectItem> lsHomeCare = [];

  ///놀이서비스
  List<ServicesSelectItem> lsPlay = [];

  List<ListItem> info = [];

  var infos = Map();

  ServicesItem._init() {
    info = [];
    infos = Map();
  }

  ///보육 서비스 데이터 만들기
  makeCareData(DataManager _data, ViewType viewType, List<ServiceItem>? servicesData, {List<int>? viewData}) {
    if (servicesData != null) {
      ServicesSelectItem data = ServicesSelectItem.init();
      List<ServicesSelectItem> items = [];
      lsBoyuk = [];
      servicesData.forEach((value) {
        items = [];
        value.products!.forEach((products) {
          data = ServicesSelectItem.init();
          data.icon = Lcons.getServiceIcon(products.caretype, products.product_id);
          data.products = products;
          data.time = _data.jobItem.totalTime;

          if (viewType == ViewType.modify) {
            _data.jobItem.reqdata.bookingboyukoptions!.forEach((valueData) {
              if (valueData.product_id == products.product_id) {
                data.isSelected = true;
              }
            });
          } else if (viewType == ViewType.view) {
            viewData!.forEach((valueData) {
              if (valueData == products.product_id) {
                data.isSelected = true;
              }
            });
          } else {
            if (_data.jobItem.tempResponse.getDataList().isNotEmpty) {
              _data.jobItem.tempResponse.getData().reqdata!.bookingboyukoptions!.forEach((valueData) {
                if (valueData.product_id == products.product_id) {
                  data.isSelected = true;
                }
              });
            }
          }
          items.add(data);
        });

        lsBoyuk.add(ServicesSelectItem(cares: value.cares!, values: items));
      });
      makeInfoData(_data, servicesData);
    }
  }

  ///가사 서비스 데이터 만들기
  makeWorkData(DataManager _data, ViewType viewType, List<ServiceItem> servicesData, {List<int>? viewData}) {
    if (servicesData.isNotEmpty) {
      String title = '';
      ServicesSelectItem data = ServicesSelectItem.init();
      List<ServicesSelectItem> items = [];
      lsHomeCare = [];
      servicesData.forEach((value) {
        items = [];
        value.products!.forEach((products) {
          data = ServicesSelectItem.init();
          data.icon = Lcons.getServiceIcon(products.caretype, products.product_id);
          data.products = products;
          data.time = _data.jobItem.totalTime;

          if (viewType == ViewType.modify) {
            _data.jobItem.reqdata.bookinghomecareoptions!.forEach((valueData) {
              if (valueData.product_id == products.product_id) {
                data.isSelected = true;
              }
            });
          } else if (viewType == ViewType.view) {
            viewData!.forEach((valueData) {
              if (valueData == products.product_id) {
                data.isSelected = true;
              }
            });
          } else {
            if (_data.jobItem.tempResponse.getDataList().isNotEmpty) {
              _data.jobItem.tempResponse.getData().reqdata!.bookinghomecareoptions?.forEach((valueData) {
                if (valueData.product_id == products.product_id) {
                  data.isSelected = true;
                }
              });
            }
          }

          items.add(data);
        });

        lsHomeCare.add(ServicesSelectItem(cares: value.cares!, values: items));
      });
      makeInfoData(_data, servicesData);
    }
  }

  ///놀이 서비스 데이터 만들기
  makePlayData(DataManager _data, ViewType viewType, List<ServiceItem>? servicesData, {List<int>? viewData}) {
    if (servicesData != null) {
      ServicesSelectItem data = ServicesSelectItem.init();
      List<ServicesSelectItem> items = [];
      lsPlay = [];
      servicesData.forEach((value) {
        items = [];
        value.products!.forEach((products) {
          data = ServicesSelectItem.init();
          data.icon = Lcons.getServiceIcon(products.caretype, products.product_id);
          data.products = products;
          data.time = _data.jobItem.totalTime;
          if (viewType == ViewType.modify) {
            _data.jobItem.reqdata.bookingplayoptions!.forEach((valueData) {
              if (valueData.product_id == products.product_id) {
                data.isSelected = true;
              }
            });
          } else if (viewType == ViewType.view) {
            viewData!.forEach((valueData) {
              if (valueData == products.product_id) {
                data.isSelected = true;
              }
            });
          } else {
            if (_data.jobItem.tempResponse.getDataList().isNotEmpty) {
              _data.jobItem.tempResponse.getData().reqdata!.bookingplayoptions?.forEach((valueData) {
                if (valueData.product_id == products.product_id) {
                  data.isSelected = true;
                }
              });
            }
          }

          items.add(data);
        });

        lsPlay.add(ServicesSelectItem(cares: value.cares!, values: items));
      });
      makeInfoData(_data, servicesData);
    }
  }

  ///필요한 돌봄 가이드 만들기
  makeInfoData(DataManager data, List<ServiceItem> servicesData) {
    servicesData.forEach((servicesItem) {
      log.d('『GGUMBI』>>> makeInfoData : servicesItem.cares.type_name: ${servicesItem.cares!.type_name},  <<< ');
      data.serviceItem.info = [];
      servicesItem.products!.forEach((products) {
        // log.d('『GGUMBI』>>> onData : products: $products,  <<< ');
        // String minute = '';
        // if (products.product_data.b_minutes > 0) {
        //   minute = ': ${"기본".tr()} ${products.product_data.b_minutes}${"분".tr()} ${"소요".tr()}';
        // }
        //
        // data.serviceItem.info.add(ListItem('${products.product_data.name}$minute', items: '${products.product_data.ourhome_parents_message}', productData: products));

        data.serviceItem.info.add(ListItem(products.product_data!.name, items: '${products.product_data!.ourhome_parents_message}', productData: products));
      });
      data.serviceItem.infos[servicesItem.cares!.type_name] = data.serviceItem.info;
    });
  }
}

///주소위치 검색 (시도, 시군구, 동읍면)
class AddressItem {
  ///시도 리스트
  List<SingleItem> lsAddress1 = [];

  ///시군구 리스트
  List<SingleItem> lsAddress2 = [];

  ///동읍면 리스트
  List<SingleItem> lsAddress3 = [];

  late AreaCodeSearchData? codeSearchData;

  AddressItem._init() {
    lsAddress1 = [];
    lsAddress2 = [];
    lsAddress3 = [];
    codeSearchData = AreaCodeSearchData();
  }
}

///마이페이지-나의정보
class MyInfoItem {
  ///성별
  List<SingleItem> lsGender = [];

  ///아이수 (미등록 : 99, 그 외 아이 숫자 0, 1, 2, 3)
  List<SingleItem> lsChild = [];

  late MyPageMyInfoSaveRequest request;
  late MyPageMyInfoResponse response;

  late MyInfoData myInfoData;

  ///동네인증 완료 여부
  bool isAuth = false;

  late String dropValue;
  late List<String> jobs;

  ///인증 관련...내용(cctv,범죄인증, 보육교사 등등)
  List<String> lsAuthInfo = [];

  SurveyData surveyInfo = SurveyData();
  List<SurveyQuestionInfo> questions = [];
  List<SurveyAnswerData> surveyAnswer = [];

  ///사진을 변경시 선택을 안하거나, 사진 촬영을 안하고 뒤로가기 할시 이전 데이터로 돌려준다.
  late File originalFile;

  MyInfoItem._init() {
    request = MyPageMyInfoSaveRequest();
    response = MyPageMyInfoResponse();
    myInfoData = MyInfoData.init();
    lsAuthInfo = [];

    // MyInfoData
    lsGender.add(SingleItem(GenderType.gender_1.string, type: GenderType.gender_1.value));
    lsGender.add(SingleItem(GenderType.gender_2.string, type: GenderType.gender_2.value));

    lsChild.add(SingleItem("없음".tr(), type: 0));
    lsChild.add(SingleItem("1명".tr(), type: 1));
    lsChild.add(SingleItem("2명".tr(), type: 2));
    lsChild.add(SingleItem("3명이상".tr(), type: 3));
    dropValue = JobType.job_99.name;
    jobs = [
      JobType.job_99.name,
      JobType.job_0.name,
      JobType.job_1.name,
      JobType.job_2.name,
      JobType.job_3.name,
      JobType.job_4.name,
      JobType.job_5.name,
      JobType.job_6.name,
      JobType.job_7.name,
      JobType.job_8.name,
      JobType.job_9.name,
    ];
  }
}

///인증센터-동네인증
class LocationAuthItem {
  ///성별
  List<SingleItem> lsGender = [];

  ///아이수 (미등록 : 99, 그 외 아이 숫자 0, 1, 2, 3)
  List<SingleItem> lsChild = [];

  MyInfoData myInfoData = MyInfoData.init();

  late File imageFile;

  ///가져오기&저장하기 데이터
  AreaAddressResponse response = AreaAddressResponse();

  ///인증여부 데이터
  AreaGpsViewResponse responseGpsView = AreaGpsViewResponse();

  ///최근인증주소
  List<SingleItem> lsAuth = [];

  late String authTitle;

  ///인증 완료 여부
  bool isAuth = false;

  ///선택 주소
  String addressSelect = '';

  ///현재 나의 주소
  String addressCurrent = '';

  LocationAuthItem._init() {
    // MyInfoData
    lsGender.add(SingleItem(GenderType.gender_1.string, type: GenderType.gender_1.value));
    lsGender.add(SingleItem(GenderType.gender_2.string, type: GenderType.gender_2.value));

    lsChild.add(SingleItem("1명".tr(), type: 1));
    lsChild.add(SingleItem("2명".tr(), type: 2));
    lsChild.add(SingleItem("3명이상".tr(), type: 3));
    lsChild.add(SingleItem("없음".tr(), type: 99));

    response = AreaAddressResponse();
    authTitle = "동네인증하기".tr();
  }
}

class ListViewItem {
  late CareListResponse responseCareData;
  late CareListData careListData;
  late JobListRequest jobListRequest;

  late LinkMomListResponse responseLinkMomData;
  late LinkMomListData linkMomListData;

  late AreaAddressResponse addressResponse;
  List<AreaAddressData> lsAreaAddress = [];

  ///뷰모드
  ViewType viewMode = ViewType.normal;

  ///내 위치
  String areaValue = '';
  List<String> lsArea = [''];
  String bcode = '';
  String hcode = '';
  String searchZoneValue = '';
  List<String> lsSearchZone = [];

  List<SingleItem> lsAuth = [];
  List<SingleItem> lsServiceType = [];

  ///신규 신청서/지원서 세팅 플래그 (최초 세팅후 true 로 변경)
  bool isSendCode = false;

  ///위치 기본값(나의 동네 주소)
  late String address_name;
  int area_code = 0;

  ///거리 초기(5km)
  late String radiusValue;
  List<String> lsRadius = [];

  ///정렬
  late String sortValue;
  List<String> lsSort = [];

  ///인증 관련...내용(cctv,범죄인증, 보육교사 등등)
  List<SingleItem> lsAuthInfo = [];

  ListViewItem._init() {
    try {
      jobListRequest = JobListRequest.init();

      responseCareData = CareListResponse();
      careListData = CareListData();

      responseLinkMomData = LinkMomListResponse();
      linkMomListData = LinkMomListData();

      address_name = auth.user.my_info_data!.auth_address != null ? auth.user.my_info_data!.auth_address!.getAddressName() : ''; //추후세팅
      radiusValue = "10km이내".tr();
      lsRadius.clear();
      lsRadius.add("1km이내".tr());
      lsRadius.add("5km이내".tr());
      lsRadius.add("10km이내".tr());

      lsServiceType.add(SingleItem("등원".tr(), type: ServiceType.serviceType_0.index));
      lsServiceType.add(SingleItem("하원".tr(), type: ServiceType.serviceType_1.index));
      lsServiceType.add(SingleItem("보육".tr(), type: ServiceType.serviceType_2.index));

      lsSort.clear();
      log.d('『GGUMBI』>>> _init : storageHelper.user_type: ${storageHelper.user_type},  <<< ');
      if (storageHelper.user_type == USER_TYPE.mom_daddy) {
        sortValue = "최근등록순".tr();
        lsSort.add("최근등록순".tr());
        lsSort.add("빠른날짜순".tr());
        lsSort.add("낮은금액순".tr());
        lsSort.add("높은금액순".tr());
        lsSort.add("링크쌤인증".tr());
        lsSort.add("좋아요순".tr());
      } else {
        sortValue = "빠른날짜순".tr();
        lsSort.add("빠른날짜순".tr());
        lsSort.add("최근등록순".tr());
        lsSort.add("낮은금액순".tr());
        lsSort.add("높은금액순".tr());
      }
      searchZoneValue = SearchZoneStatus.all.name;
      lsSearchZone.add(SearchZoneStatus.all.name);
      lsSearchZone.add(SearchZoneStatus.auth_zone.name);
      lsSearchZone.add(SearchZoneStatus.care_zone.name);
    } catch (e) {}
  }

  setClear() {
    areaValue = '';
    searchZoneValue = '';
    bcode = '';
    hcode = '';
  }
}

class ListViewDetailItem {
  late CareViewResponse responseData;
  CareViewData? careViewData;
  late MatchingViewResponse responseContractData;
  MatchingInitViewData? matchingInitViewData;

  ///인증 관련...내용(cctv,범죄인증, 보육교사 등등)
  List<String> lsAuthInfo = [];

  ListViewDetailItem._init() {
    responseData = CareViewResponse();
    responseContractData = MatchingViewResponse();
    // careViewData = CareViewData(
    //   authinfo: AuthInfoData(),
    //   careboyuk: CareViewBoyukData(),
    //   caredate: [],
    //   careoptions: CareViewOptionData(),
    //   careschool: CareViewSchoolData(),
    //   childinfo: CareViewChildInfoData(),
    //   userinfo: CareViewUserInfoData(),
    // );
    matchingInitViewData = MatchingInitViewData(
        bookingcareservices_info: CareViewData(
          authinfo: AuthInfoData(),
          careboyuk: CareViewBoyukData(),
          caredate: [],
          careoptions: CareViewOptionData(),
          careschool: CareViewSchoolData(),
          childinfo: CareViewChildInfoData(),
          userinfo: CareViewUserInfoData(),
        ),
        contract_info: ContractInfoData(),
        msgdata: ContractData());
  }
}

class FilterItem {
  List<SingleItem> lsArea = [];
  List<SingleItem> lsDay = [];
  List<SingleItem> lsTime = [];
  List<SingleItem> lsJobCareTargetType = [];
  List<SingleItem> lsTag = [];

  JobListRequest request = JobListRequest();
  RangeValues currentRangeValues = RangeValues(9000, 20000);

  FilterItem.init() {
    if (storageHelper.user_type == USER_TYPE.mom_daddy) {
      lsArea.add(SingleItem('${"맘대디집".tr()}(${"우리집".tr()})', type: PossibleArea.mom_daddy.value));
      lsArea.add(SingleItem('${"링크쌤집".tr()}(${"이웃집".tr()})', type: PossibleArea.link_mom.value));

      lsTag.add(SingleItem("CCTV있음".tr(), keyName: HashTagLinkMom.hashtag_cctv.toString().split(".").last));
      lsTag.add(SingleItem("반려동물있음".tr(), keyName: HashTagLinkMom.hashtag_anmal.toString().split(".").last));
      lsTag.add(SingleItem("코로나백신".tr(), keyName: HashTagLinkMom.hashtag_codiv_vaccine.toString().split(".").last));
      lsTag.add(SingleItem("등본인증".tr(), keyName: HashTagLinkMom.hashtag_deungbon.toString().split(".").last));
      lsTag.add(SingleItem("범죄회보조회".tr(), keyName: HashTagLinkMom.hashtag_criminal.toString().split(".").last));
      //2022/01/05 현재 인적성 검사가 지원되지 않기 때문에 주석 처리
      // lsTag.add(SingleItem("인적성검사".tr(), keyName: HashTagLinkMom.hashtag_personality.toString().split(".").last));
      lsTag.add(SingleItem("건강진단서".tr(), keyName: HashTagLinkMom.hashtag_healthy.toString().split(".").last));
      lsTag.add(SingleItem("보육교사".tr(), keyName: HashTagLinkMom.hashtag_career_2.toString().split(".").last));
      lsTag.add(SingleItem("교육인증".tr(), keyName: HashTagLinkMom.hashtag_education.toString().split(".").last));
      lsTag.add(SingleItem("교사자격증".tr(), keyName: HashTagLinkMom.hashtag_career_1.toString().split(".").last));
      lsTag.add(SingleItem("학력인증".tr(), keyName: HashTagLinkMom.hashtag_graduated.toString().split(".").last));
      lsTag.add(SingleItem("베이비시터".tr(), keyName: HashTagLinkMom.hashtag_babysiter.toString().split(".").last));
    } else {
      lsArea.add(SingleItem('${"맘대디집".tr()}(${"이웃집".tr()})', type: PossibleArea.mom_daddy.value));
      lsArea.add(SingleItem('${"링크쌤집".tr()}(${"우리집".tr()})', type: PossibleArea.link_mom.value));

      lsTag.add(SingleItem("CCTV있음".tr(), keyName: HashTagLinkMom.hashtag_cctv.toString().split(".").last));
      lsTag.add(SingleItem("반려동물있음".tr(), keyName: HashTagLinkMom.hashtag_anmal.toString().split(".").last));
    }

    lsDay.add(SingleItem("월".tr(), type: DayOfWeekType.monday.index));
    lsDay.add(SingleItem("화".tr(), type: DayOfWeekType.tuesday.index));
    lsDay.add(SingleItem("수".tr(), type: DayOfWeekType.wednesday.index));
    lsDay.add(SingleItem("목".tr(), type: DayOfWeekType.thursday.index));
    lsDay.add(SingleItem("금".tr(), type: DayOfWeekType.friday.index));
    lsDay.add(SingleItem("토".tr(), type: DayOfWeekType.saturday.index));
    lsDay.add(SingleItem("일".tr(), type: DayOfWeekType.sunday.index));

    lsTime.add(SingleItem("오전".tr(), type: TimeType.off.index));
    lsTime.add(SingleItem("오후".tr(), type: TimeType.off.index));

    lsJobCareTargetType.clear();
    lsJobCareTargetType.addAll(JobItem.init().lsJobCareTargetType);
  }
}

class AuthItem {
  List<SingleItem> lsAuth = [];
  List<SingleItem> lsSelfAuth = [];

  AuthItem._init() {
    lsAuth.add(SingleItem("인증센터_본인인증".tr(), type: 0, data: SingleItem("본인인증_안내".tr()), icon: Lcons.self_certification_active()));
    lsAuth.add(SingleItem("인증센터_동네인증".tr(), type: 1, data: SingleItem("동네인증_안내".tr()), icon: Lcons.local_certification_active()));
    lsAuth.add(SingleItem("인증센터_집안환경".tr(), type: 2, data: SingleItem("집안환경_안내".tr()), icon: Lcons.family_environment_active()));
    lsAuth.add(SingleItem("인증센터_범죄인증".tr(), type: 3, data: SingleItem("범죄인증_안내".tr(), name2: "허위서류_안내".tr()), icon: Lcons.criminal_certification_active()));
    lsAuth.add(SingleItem("인증센터_건강인증".tr(), type: 4, data: SingleItem("건강인증_안내".tr(), name2: "허위서류_안내".tr()), icon: Lcons.health_certification_active()));
    lsAuth.add(SingleItem("인증센터_등본인증".tr(), type: 5, data: SingleItem("등본인증_안내".tr(), name2: "허위서류_안내".tr()), icon: Lcons.copy_certification_active()));
    lsAuth.add(SingleItem("인증센터_경력인증".tr(), type: 6, data: SingleItem("경력인증_안내".tr(), name2: "허위서류_안내".tr()), icon: Lcons.career_certification_active()));
    lsAuth.add(SingleItem("인증센터_학력인증".tr(), type: 7, data: SingleItem("학력인증_안내".tr(), name2: "허위서류_안내".tr()), icon: Lcons.academic_bg_certification()));
    lsAuth.add(SingleItem("인증센터_교육인증".tr(), type: 8, data: SingleItem("교육인증_안내".tr(), name2: "허위서류_안내".tr()), icon: Lcons.education_certification_active()));
    lsSelfAuth.add(SingleItem("인증센터_코로나".tr(), type: 9, data: SingleItem(''), icon: Lcons.covid_self_test()));
    lsSelfAuth.add(SingleItem("인증센터_스트레스".tr(), type: 10, data: SingleItem(''), icon: Lcons.stress_test()));
  }

  AuthCenterDeungbonRequest deungbonReq = AuthCenterDeungbonRequest();
  FormData deungbonForm = FormData();
}

class DrugInfoItem {
  List<ChildDrugReponseData> drugList = [];

  DrugInfoItem._init();
}

class CaresItem {
  late DateTimeRange? searchRange;
  Map<DateTime, List<int>> momdadyCareEvents = {};
  Map<DateTime, List<int>> momdadyMatchEvents = {};
  Map<DateTime, List<int>> linkmomApplyEvents = {};
  Map<DateTime, List<int>> linkmomMatchEvents = {};
}

class ChatItem {
  // List<Chat> lsChat = [];
  ChatMainResponse chatMainResponse = ChatMainResponse();
  List<ChatMainResultsData> lsChat = [];

  ///뷰모드
  ViewType viewMode = ViewType.normal;

  ChatItem.init() {
    log.d('『GGUMBI』>>> init : auth.user: ${auth.user},  <<< ');
    // lsChat.add(Chat(fromName: "링크쌤", fromImg: auth.myInfoData?.profileimg, name: auth.myInfoData?.first_name, dateTime: (DateTime.now().subtract(Duration(days: 365)).toString()), content: "감사합니다!"));
    // lsChat.add(Chat(fromName: "홍길순", fromImg: auth.myInfoData?.profileimg, name: auth.myInfoData?.first_name, dateTime: (DateTime.now().add(Duration(days: 10)).toString()), content: "안녕하세요??"));
    // lsChat.add(Chat(fromName: "홍길동", fromImg: auth.myInfoData?.profileimg, name: auth.myInfoData?.first_name, dateTime: (DateTime.now().subtract(Duration(days: 10)).toString()), content: "괜찮으신가요?"));
    // lsChat.add(Chat(fromName: "김철수", fromImg: auth.myInfoData?.profileimg, name: auth.myInfoData?.first_name, dateTime: (DateTime.now().subtract(Duration(days: 2)).toString()), content: "저기요?"));
    // lsChat.add(Chat(fromName: "아무개", fromImg: auth.myInfoData?.profileimg, name: auth.myInfoData?.first_name, dateTime: (DateTime.now().subtract(Duration(days: 1)).toString()), content: "네?"));
    // lsChat.add(Chat(fromName: "영심이", fromImg: auth.myInfoData?.profileimg, name: auth.myInfoData?.first_name, dateTime: (DateTime.now().subtract(Duration(hours: 2)).toString()), content: "계세요?"));
    // lsChat.add(Chat(fromName: "길동이", fromImg: auth.myInfoData?.profileimg, name: auth.myInfoData?.first_name, dateTime: (DateTime.now().subtract(Duration(hours: 12)).toString()), content: "네, 감사합니다!"));
  }
}

class ChatRoomItem {
  ///채팅방 생성 및 정보 가져오기
  ChatRoomData chatInfo = ChatRoomData();

  ///대화리스트
  List<ChatSendData> lsChat = [];

  ///대화데이터 연속 조회 및
  ChatMessageListResponse responseChat = ChatMessageListResponse();

  ///뷰모드
  ViewType viewMode = ViewType.normal;

  ///돌봄 채팅인 경우 데이터
  MyCareResultsData careItem = MyCareResultsData();

  ChatRoomItem.init();
}

class ChatCareInfoItem {
  List<MyCareResultsData> lsCareInfo = [];
  MyCareResultsData selectItem = MyCareResultsData();
  PageData? responseData;
}

class ReviewItem {
  ReviewListData mCompleatList = ReviewListData(results: []);
  ReviewListData mWritableList = ReviewListData(results: []);
  ReviewInfoResultData mReceiveList = ReviewInfoResultData(result: []);
  ReviewListData lCompleatList = ReviewListData(count: 0, results: []);
  ReviewListData lWritableList = ReviewListData(results: []);
  ReviewInfoResultData lReceiveList = ReviewInfoResultData(result: []);
}

// class PayHistoryItem {
//   PayList pay = PayList(results: []);
//   CashList cash = CashList(results: []);
//   PointList point = PointList(results: []);
// }

class NotificationItem {
  List<List<NotificationData>> list = List.generate(6, (index) => []);
}

class ModifyData {
  JobItem jobItem = JobItem.init();
  dynamic data;

  ModifyData({required this.jobItem, this.data});
}
