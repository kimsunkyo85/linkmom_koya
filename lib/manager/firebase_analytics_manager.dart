import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:linkmom/utils/commons.dart';

import '../main.dart';

enum FbaLogType {
  /// this is example
  bookingRequest,
  menu,
  event,
  click,
  mode,
}

extension ex on FbaLogType {
  String get name {
    switch (this) {
      case FbaLogType.bookingRequest:
        return 'booking_request';
      case FbaLogType.menu:
        return 'menu';
      case FbaLogType.event:
        return 'event';
      case FbaLogType.click:
        return 'click';
      case FbaLogType.mode:
        return 'mode';
      default:
        return '';
    }
  }
}

class FbaManager {
  static const LOG_BOOKING_REQUEST = 'booking_request';
  static late FirebaseAnalytics _fba;

  FbaManager() {
    _fba = FirebaseAnalytics.instance;
  }

  static FirebaseAnalytics get fba {
    _fba = FirebaseAnalytics.instance;
    return _fba;
  }

  ///모드_화면명_액션, parameters
  ///예제 : momdady_mainhome_menu, click: 홈
  ///Commons.isLinkMom() ? 'linkmom_' : 'momdady_';
  static Future<void> addLogEvent({String? viewName, FbaLogType type = FbaLogType.click, Map<String, Object?>? parameters}) async {
    if (Commons.isDebugMode) return;
    StringBuffer sb = StringBuffer();
    String user = Commons.isLinkMom() ? 'linkmom' : 'momdady';
    sb.write('$user');
    sb.write(viewName == null ? '_' : '_${viewName.replaceAll('/', '').toLowerCase()}_');
    sb.write('${type.name}');
    log.d('『GGUMBI』>>> addLogEvent : sb: ${sb.toString()}, $parameters <<< ');
    return _fba.logEvent(name: sb.toString(), parameters: parameters);
  }

  Future<void> addSelect(FbaLogType type, Map<String, Object?>? parameters) async {
    if (Commons.isDebugMode) return;
    return _fba.logSelectContent(contentType: type.name, itemId: '');
  }
}
