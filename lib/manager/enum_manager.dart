import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/view/lcons.dart';

import 'data_manager.dart';

///Version Type Check 1.공지사항에 대한 Alert를 먼저 확인한다.고지여부(0:고지X, 1 : 확인(메인진입) , 2 : APP종료)
enum VersionType {
  normal,
  confirm,
  finish,
}

extension VersionTypeEx on VersionType {
  int get value {
    switch (this) {
      case VersionType.normal:
        return 0;
      case VersionType.confirm:
        return 1;
      case VersionType.finish:
        return 2;
      default:
        return 0;
    }
  }
}

///다이얼로그
enum DialogAction {
  test,
  off,
  on,
  close,
  no,
  yes,
  cancel,
  cancelInfo,
  update,
  select,
  toggle,
  info,
  finish,
  setting,
  gallery,
  camera,
  confirm,
  delete,
  deleteAll,
  modify,
  review,
  location,
  careZone,
  filter,
  sort,
  follower,
  mode,
  back,
  find,
  previous,
  next,
  fail,
  success,
  contract,
  pay,
  schedule,
}

///MQTT
enum MqttType {
  onConnected,
  onDisconnected,
  onMsg,
  sndMsg,
  onSubscribed,
  onSubscribeFail,
  onUnsubscribed,
  pong,
}

///ChatSendType (화면에서 사용)
enum ChatSendType {
  send,
  receiver,
  mode_momdady,
  mode_linkmom,
}

///ChattingType 0:일반, 1:돌봄, 2:계약서, 3:돌봄신청서
enum ChattingType {
  /// 0:일반
  normal,

  ///1:돌봄
  care,

  ///2:계약서
  contract,

  ///3:돌봄신청서
  careApply,
}

extension ChatMatchingTypeEx on ChattingType {
  int get value {
    switch (this) {
      case ChattingType.normal:
        return 0;
      case ChattingType.care:
        return 1;
      case ChattingType.contract:
        return 2;
      case ChattingType.careApply:
        return 3;
      default:
        return 0;
    }
  }
}

///ChatContentType 0:text, 1:img, 2:matching, 3:신청서정보, 4:noData
enum ChatMsgType {
  ///0 = text
  text,

  ///1:img
  image,

  ///2:매칭정보
  matching,

  ///3:신청서정보
  careInfo,

  ///99=no data
  noData,
}

extension ChatMsgEx on ChatMsgType {
  int get value {
    switch (this) {
      case ChatMsgType.text:
        return 0;
      case ChatMsgType.image:
        return 1;
      case ChatMsgType.matching:
        return 2;
      case ChatMsgType.careInfo:
        return 3;
      case ChatMsgType.noData:
        return 99;
      default:
        return 0;
    }
  }

  ChatMsgType get type {
    switch (value) {
      case 0:
        return ChatMsgType.text;
      case 1:
        return ChatMsgType.image;
      case 2:
        return ChatMsgType.matching;
      case 3:
        return ChatMsgType.careInfo;
      case 99:
        return ChatMsgType.noData;
      default:
        return ChatMsgType.text;
    }
  }

  String get valueName {
    switch (this) {
      case ChatMsgType.text:
        return 'ChatMsgType.text';
      case ChatMsgType.image:
        return 'ChatMsgType.image';
      case ChatMsgType.matching:
        return 'ChatMsgType.matching';
      case ChatMsgType.careInfo:
        return 'ChatMsgType.careInfo';
      case ChatMsgType.noData:
        return 'ChatMsgType.noData';
      default:
        return 'ChatMsgType.text';
    }
  }
}

ChatMsgType getChatMsgTypeInt(String type) {
  switch (type) {
    case 'ChatMsgType.text':
      return ChatMsgType.text;
    case 'ChatMsgType.img':
      return ChatMsgType.image;
    case 'ChatMsgType.noData':
      return ChatMsgType.noData;
    default:
      return ChatMsgType.text;
  }
}

///ChatMenuType
enum ChatMenuType {
  apply_open,
  matching_request,
  matching_cancel,
  payment,
  care_cancel,
  photo,
}

///ChatRoomType
enum ChatRoomType {
  CREATE,
  VIEW,
}

///ChatTabType
enum ChatTabType {
  ///돌봄채팅
  carematching,

  ///일반
  normal,

  ///momdady
  momdady,

  ///linkmom
  linkmom,
}

extension ChatTebEx on ChatTabType {
  int get value {
    switch (this) {
      case ChatTabType.carematching:
        return 0;
      case ChatTabType.normal:
        return 1;
      case ChatTabType.momdady:
        return 2;
      case ChatTabType.linkmom:
        return 3;
      default:
        return 0;
    }
  }

  String get bywho {
    switch (value) {
      case 0:
        return "carematching";
      case 1:
        return "normal";
      case 2:
        return "momdady";
      case 3:
        return "linkmom";
      default:
        return "carematching";
    }
  }

  String get title {
    switch (value) {
      case 0:
        return "돌봄채팅".tr();
      case 1:
        return "일반".tr();
      default:
        return "돌봄채팅".tr();
    }
  }

  ChatTabType get type {
    switch (value) {
      case 0:
        return ChatTabType.carematching;
      case 1:
        return ChatTabType.normal;
      default:
        return ChatTabType.carematching;
    }
  }
}

///필요한 돌봄 보육,가사,놀이,투약
enum ServiceTabType {
  care,
  work,
  play,
  drug,
}

extension ServiceTabTypeEx on ServiceTabType {
  int get value {
    switch (this) {
      case ServiceTabType.care:
        return 0;
      case ServiceTabType.work:
        return 1;
      case ServiceTabType.play:
        return 2;
      case ServiceTabType.drug:
        return 3;
      default:
        return 0;
    }
  }

  String get name {
    switch (this) {
      case ServiceTabType.care:
        return "보육".tr();
      case ServiceTabType.work:
        return "가사".tr();
      case ServiceTabType.play:
        return "놀이".tr();
      case ServiceTabType.drug:
        return "투약".tr();
      default:
        return "보육".tr();
    }
  }

  String get iconPath {
    switch (this) {
      case ServiceTabType.care:
        return Lcons.nursery().name;
      case ServiceTabType.work:
        return Lcons.housework().name;
      case ServiceTabType.play:
        return Lcons.play().name;
      case ServiceTabType.drug:
        return Lcons.drug().name;
      default:
        return Lcons.nursery().name;
    }
  }
}

///돌봄신청서(맘대디 프로필 하단 버튼 세팅), 무조건안보기, 일반(신청서 작성 및 기본 모드), 채팅, 계약서
enum BottomBtnType {
  not,
  normal,
  chat,
  contract,
}

///로그인 타입
enum LoginType {
  login,
  logout,
}

///인증 방식 (1 : 이메일, 2: 핸드폰)
enum AuthType {
  normal,

  ///(1:이메일)
  email,

  ///(2:핸드폰)
  mobile,
}

///아이디 / 비밀번호 찾기에서 사용
enum FindType {
  mobile,
  email,
  id,
  pw,
}

///api 타입 post, get, put, patch, delete, read, head
enum HttpType {
  post,
  get,
  put,
  patch,
  delete,
  read,
  head,
}

/// 수요자(부모님 consumer), 공급자(돌봄이 provider) 선택
enum USER_TYPE {
  ///수요자(부모님 consumer)
  mom_daddy,

  ///공급자(돌봄이 provider)
  link_mom,
}

extension UserType on USER_TYPE {
  String get name {
    switch (this) {
      case USER_TYPE.link_mom:
        return "링크쌤".tr();
      case USER_TYPE.mom_daddy:
        return "맘대디".tr();
      default:
        return "";
    }
  }

  int get gubun {
    switch (this) {
      case USER_TYPE.link_mom:
        return 2;
      case USER_TYPE.mom_daddy:
        return 1;
      default:
        return 1;
    }
  }

  String get joinTxt {
    switch (this) {
      case USER_TYPE.link_mom:
        return "이".tr();
      case USER_TYPE.mom_daddy:
        return "가".tr();
      default:
        return "";
    }
  }

  ChatSendType get type {
    switch (this) {
      case USER_TYPE.link_mom:
        return ChatSendType.mode_linkmom;
      case USER_TYPE.mom_daddy:
        return ChatSendType.mode_momdady;
      default:
        return ChatSendType.mode_momdady;
    }
  }

  String get typeContent {
    switch (this) {
      case USER_TYPE.link_mom:
        return "링크쌤모드_안내".tr();
      case USER_TYPE.mom_daddy:
        return "맘대디모드_안내".tr();
      default:
        return "맘대디모드_안내".tr();
    }
  }

  String get typeContentHint {
    switch (this) {
      case USER_TYPE.link_mom:
        return "링크쌤모드_안내_힌트".tr();
      case USER_TYPE.mom_daddy:
        return "맘대디모드_안내_힌트".tr();
      default:
        return "맘대디모드_안내_힌트".tr();
    }
  }
}

///애니메이션 타입
enum AnimationType {
  LEFT,
  TOP,
  RIGHT,
  BOTTOM,
}

/// 환경인증 주거환경(우리집 타입 (0:아파트, 1:오피스텔, 2:빌라, 3:주택))
enum HomeType {
  ///0:아파트
  apartment,

  ///1:오피스텔
  officetel,

  ///2:빌라
  villa,

  ///3:주택
  house,
}

/// CCTV 환경(우리집 CCTV 설치여부 (0: 없음, 1: 있음, 2:설치예정)
enum CctvType {
  ///0: 없음
  no,

  ///1: 있음
  yes,

  ///2:설치예정
  install,
}

/// 애완동물(우리집 애완동물 (0:없음, 1:강아지, 2:고양이, 3:기타) - 중복가능
enum AnimalType {
  no,
  cat,
  dog,
  etc,
}

/// cctv여부, 애완동물, 사진(0:no안괜찮아요, 1:ok괜찮아요,좋겠어요), (오전/오후 = 사용여부, 0, 1)
enum FineType {
  no,
  ok,
}

/// 신청, 추가, 수정, 보기, 기본보기, 요약보기
enum ViewType {
  apply,
  add,
  modify,
  view,
  normal,
  summary,
}

///성별
enum GenderType {
  gender_1,
  gender_2,
}

extension GenderTypeEx on GenderType {
  String get stringChild {
    switch (this) {
      case GenderType.gender_1:
        return "남아".tr();
      case GenderType.gender_2:
        return "여아".tr();
      default:
        return '';
    }
  }

  String get string {
    switch (this) {
      case GenderType.gender_1:
        return "남자".tr();
      case GenderType.gender_2:
        return "여자".tr();
      default:
        return '';
    }
  }

  int get value {
    switch (this) {
      case GenderType.gender_1:
        return 1;
      case GenderType.gender_2:
        return 2;
      default:
        return 1;
    }
  }
}

///보육기관 (0:어린이집, 1:유치원, 2:초등학교, 3:중/고등학교, 4:특수학교, 5:가정보육)
enum ChildNurseryType {
  nursery_0,
  nursery_1,
  nursery_2,
  nursery_3,
  nursery_4,
  nursery_5,
}

extension ChildNurseryTypeEx on ChildNurseryType {
  String get string {
    switch (this) {
      case ChildNurseryType.nursery_0:
        return "아이등록_어린이집".tr();
      case ChildNurseryType.nursery_1:
        return "아이등록_유치원".tr();
      case ChildNurseryType.nursery_2:
        return "아이등록_초등학교".tr();
      case ChildNurseryType.nursery_3:
        return "아이등록_중고등학교".tr();
      case ChildNurseryType.nursery_4:
        return "아이등록_특수학교".tr();
      case ChildNurseryType.nursery_5:
        return "아이등록_가정보육".tr();
      default:
        return '';
    }
  }
}

///아이성향 (성향번호 : 0~9) *중복가능
/// (0, u'소심함'), (1, '섬세함'), (2, u'활발함'), (3, '낯가림'), (4, '조용한 편임'), (5, u'사교성이 좋음'), (6, '호기심이 많음'), (7, u'승부욕이 강함'), (8, '말하기 좋아함'), (9, '자기주장이 강함')
enum ChildCharacterType {
  character_0,
  character_1,
  character_2,
  character_3,
  character_4,
  character_5,
  character_6,
  character_7,
  character_8,
  character_9,
}

extension ChildCharacterTypeEx on ChildCharacterType {
  String get string {
    switch (this) {
      case ChildCharacterType.character_0:
        return "아이등록_소심함".tr();
      case ChildCharacterType.character_1:
        return "아이등록_섬세함".tr();
      case ChildCharacterType.character_2:
        return "아이등록_활발함".tr();
      case ChildCharacterType.character_3:
        return "아이등록_낮가림".tr();
      case ChildCharacterType.character_4:
        return "아이등록_조용한편임".tr();
      case ChildCharacterType.character_5:
        return "아이등록_사교성이좋음".tr();
      case ChildCharacterType.character_6:
        return "아이등록_호기심이많음".tr();
      case ChildCharacterType.character_7:
        return "아이등록_승부욕이강함".tr();
      case ChildCharacterType.character_8:
        return "아이등록_말하기좋아함".tr();
      case ChildCharacterType.character_9:
        return "아이등록_자기주장이강함".tr();
      default:
        return '';
    }
  }
}

///** 아이템 선택, 선택/수정/삭제/저장/닫기/신청/계약서*/
enum ItemType {
  add,
  select,
  modify,
  delete,
  save,
  close,
  apply,
  contract,
  chat,
}

///버전체크, 다운로드, 완료
enum SplashType {
  version,
  download,
  complete,
}

/// (0:등원, 1:하원, 2:보육, 3:학습, 4:입주가사, 5:이유식반찬)
enum ServiceType {
  ///(0:등원(go_school)
  serviceType_0,

  ///1:하원(go_home)
  serviceType_1,

  ///2:보육(nursery)
  serviceType_2,

  ///3:학습(learning)
  serviceType_3,

  ///4:입주가사(housework)
  serviceType_4,

  /// 5:이유식반찬(food)
  serviceType_5,
}

extension ServiceEx on ServiceType {
  String get string {
    switch (this) {
      case ServiceType.serviceType_0:
        return "등원".tr();
      case ServiceType.serviceType_1:
        return "하원".tr();
      case ServiceType.serviceType_2:
        return "보육".tr();
      case ServiceType.serviceType_3:
        return "학습".tr();
      case ServiceType.serviceType_4:
        return "입주가사".tr();
      case ServiceType.serviceType_5:
        return "이유식반찬".tr();
      default:
        return "전체".tr();
    }
  }

  String get stringCare {
    switch (this) {
      case ServiceType.serviceType_0:
        return "등원돌봄".tr();
      case ServiceType.serviceType_1:
        return "하원돌봄".tr();
      case ServiceType.serviceType_2:
        return "보육돌봄".tr();
      case ServiceType.serviceType_3:
        return "학습볼봄".tr();
      case ServiceType.serviceType_4:
        return "입주가사".tr();
      case ServiceType.serviceType_5:
        return "이유식반찬".tr();
      default:
        return "전체".tr();
    }
  }

  List<String> get service {
    switch (this) {
      case ServiceType.serviceType_0:
        return ["우리집_등원_안내".tr(), "이웃집_등원_안내".tr()];
      case ServiceType.serviceType_1:
        return ["우리집_하원_안내".tr(), "이웃집_하원_안내".tr()];
      case ServiceType.serviceType_2:
        return ["우리집_보육_안내".tr(), "이웃집_보육_안내".tr()];
      case ServiceType.serviceType_3:
        return [];
      case ServiceType.serviceType_4:
        return [];
      case ServiceType.serviceType_5:
        return [];
      default:
        return [];
    }
  }

  int get value {
    switch (this) {
      case ServiceType.serviceType_0:
        return 0;
      case ServiceType.serviceType_1:
        return 1;
      case ServiceType.serviceType_2:
        return 2;
      case ServiceType.serviceType_3:
        return 3;
      case ServiceType.serviceType_4:
        return 4;
      case ServiceType.serviceType_5:
        return 5;
      default:
        return 99;
    }
  }

  Color get color {
    switch (this) {
      case ServiceType.serviceType_0:
        return color_ffc064;
      case ServiceType.serviceType_1:
        return color_87d6f9;
      case ServiceType.serviceType_2:
        return color_f8859a;
      case ServiceType.serviceType_3:
        return color_ffc064;
      case ServiceType.serviceType_4:
        return color_87d6f9;
      case ServiceType.serviceType_5:
        return color_f8859a;
      default:
        return color_ffc064;
    }
  }
}

///돌봄장소
enum PossibleArea {
  not,

  ///(1:우리집 mom_daddy - ourhome)
  mom_daddy,

  ///(2:이웃집 link_mom - nbhhome)
  link_mom,
}

extension PossibleAreaEx on PossibleArea {
  int get value {
    switch (this) {
      case PossibleArea.mom_daddy:
        return 1;
      case PossibleArea.link_mom:
        return 2;
      default:
        return 1;
    }
  }

  String get name {
    switch (this) {
      case PossibleArea.not:
        return '';
      case PossibleArea.mom_daddy:
        return "맘대디집".tr();
      case PossibleArea.link_mom:
        return "링크쌤집".tr();
      default:
        return '';
    }
  }
}

///돌봄 이동방법 (1:도보, 2:자동차, 3:대중교통)
enum MoveType {
  not,

  ///1:도보
  waking,

  ///2:자동차
  car,

  ///3:대중교통
  transport,
}

extension MoveTypeEx on MoveType {
  int get value {
    switch (this) {
      case MoveType.waking:
        return 1;
      case MoveType.car:
        return 2;
      case MoveType.transport:
        return 3;
      default:
        return 1;
    }
  }

  String get name {
    switch (this) {
      case MoveType.waking:
        return "도보".tr();
      case MoveType.car:
        return "자동차".tr();
      case MoveType.transport:
        return "대중교통".tr();
      default:
        return '';
    }
  }
}

///출발장소(start), 출발장소상세(startDetail), 출발시간(startTime), 출발장소설명(startMsg), 도착장소(end), 도착장소상세(endDetail), 도착시간(endTime), 추가설명(endMsg), 경우지추가(routeAdd)
enum MoveViewType {
  start,
  startDetail,
  startTime,
  startMsg,
  end,
  endDetail,
  endTime,
  endMsg,
  routeAdd,
  check,
  location1,
  location2,
  locationType,
  startSelect,
  endSelect,
  startUpdate,
  endUpdate,
}

///돌봄계약서 작성시 모드
enum SignType {
  ///출발지 주소 입력
  startSign,

  ///도착지 주소 입력
  endSign,

  ///보육 주소 입력
  boyukSign,
}

///돌봄 날짜/시간 (0:매주반복)
enum CalendarType {
  week,
  day,
}

///(0, u'전업 맘/대디'),
/// (1, u'워킹 맘/대디'),
/// (2, u'출산휴가/육아휴직'),
/// (3, u'프리랜서/부업/파트타임'),
/// (4, u'자영업'),
/// (5, u'전문직/강사'),
/// (6, u'대학생'),
/// (7, u'미취업자/취준생'),
/// (8, u'회사원'),
/// (9, u'기타'),
/// (99, u'미등록')
enum JobType {
  job_0,
  job_1,
  job_2,
  job_3,
  job_4,
  job_5,
  job_6,
  job_7,
  job_8,
  job_9,
  job_99,
}

extension JobTypeEx on JobType {
  int get value {
    switch (this) {
      case JobType.job_0:
        return 0;
      case JobType.job_1:
        return 1;
      case JobType.job_2:
        return 2;
      case JobType.job_3:
        return 3;
      case JobType.job_4:
        return 4;
      case JobType.job_5:
        return 5;
      case JobType.job_6:
        return 6;
      case JobType.job_7:
        return 7;
      case JobType.job_8:
        return 8;
      case JobType.job_9:
        return 9;
      case JobType.job_99:
        return NOT_REGISTER;
      default:
        return 9;
    }
  }

  String get name {
    switch (this) {
      case JobType.job_0:
        return "전업맘대디".tr();
      case JobType.job_1:
        return "워킹맘대디".tr();
      case JobType.job_2:
        return "출산휴가육아휴직".tr();
      case JobType.job_3:
        return "프리랜서파트타임".tr();
      case JobType.job_4:
        return "자영업".tr();
      case JobType.job_5:
        return "전문직강사".tr();
      case JobType.job_6:
        return "대학생".tr();
      case JobType.job_7:
        return "미취업자취준생".tr();
      case JobType.job_8:
        return "회사원".tr();
      case JobType.job_9:
        return "기타".tr();
      case JobType.job_99:
        return "선택".tr();
      default:
        return "기타".tr();
    }
  }
}

///돌봄대상 (0:신생아, 1:영아, 2:유아, 3:초등학생, 4:중학생, 5:고등학생)
enum CareTargetType {
  ///0:신생아
  careTarget_0,

  ///1:영아
  careTarget_1,

  ///2:유아
  careTarget_2,

  ///3:초등학생
  careTarget_3,

  ///4:중학생
  careTarget_4,

  ///5:고등학생
  careTarget_5,
}

///맘대디 해시태그
enum AuthTagMomDady {
  hashtag_cctv,
  hashtag_anmal,
}

///링크쌤 해시태그
enum HashTagLinkMom {
  ///"hashtag_cctv": "CCTV있음"
  hashtag_cctv,

  ///"hashtag_anmal": "반려동물있음"
  hashtag_anmal,

  ///"hashtag_codiv_vaccine": "코로나백신"
  hashtag_codiv_vaccine,

  ///"hashtag_deungbon": "등본인증"
  hashtag_deungbon,

  ///"hashtag_criminal": "범죄회보조회"
  hashtag_criminal,

  ///"hashtag_personality": "인적성검사"
  hashtag_personality,

  ///"hashtag_healthy": "건강진단서"
  hashtag_healthy,

  ///"hashtag_career_2": "보육교사"
  hashtag_career_2,

  ///"hashtag_education": "교육인증"
  hashtag_education,

  ///"hashtag_career_1": "교사자격증"
  hashtag_career_1,

  ///"hashtag_graduated": "학력인증"
  hashtag_graduated,

  ///"hashtag_babysiter": "베이비시터"
  hashtag_babysiter,
}

///orderingtype : 정렬 조건
enum SortType {
  /// 돌봄날짜순 (caredate : default)
  caredate,

  /// 최신등록순 (createdate),
  createdate,

  /// 높은 금액순 (costmax),
  costmin,

  /// 낮은 금액순(costmin),
  costmax,

  /// 인증 수 (authlevel)
  authlevel,

  /// 좋아요 수 (favorite)
  favorite,
}

///요일 선택
enum DayOfWeekType {
  monday,
  tuesday,
  wednesday,
  thursday,
  friday,
  saturday,
  sunday,
}

///시간선택(오전(0:1), 오후(0:1)
enum TimeType {
  ///오전(0:1)
  off,

  ///오후(0:1)
  on,
}

enum ChildListPageMode {
  childInfo,
  drugInfo,
}

/// 가루약, 물약, 연고, 기타
enum DrugType {
  ///가루약
  potion,

  ///물약
  powder,

  ///연고
  ointment,

  ///기타
  other,
}

extension DrugExt on DrugType {
  String get name {
    switch (this) {
      case DrugType.potion:
        return "투약의뢰서_물약".tr();
      case DrugType.powder:
        return "투약의뢰서_가루약".tr();
      case DrugType.ointment:
        return "투약의뢰서_연고".tr();
      default:
        return "기타".tr();
    }
  }

  String get unit {
    switch (this) {
      case DrugType.potion:
        return "ml/cc".tr();
      case DrugType.powder:
        return "봉지".tr();
      case DrugType.ointment:
      default:
        return "";
    }
  }
}

enum SurveyType {
  mytype,
  covid19,
  stress,
  personality,
}

extension SurveyTypeExt on SurveyType {
  int get value {
    switch (this) {
      case SurveyType.mytype:
        return 1;
      case SurveyType.stress:
        return 2;
      case SurveyType.personality:
        return 3;
      case SurveyType.covid19:
        return 9;
      default:
        return 0;
    }
  }
}

enum AuthImageType {
  health,
  covid19Vaccin,
  daycareTeach,
  kindergarten,
  elementary,
  childSpecial,
  childArts,
  schoolCertif,
  graduation,
  babysitter,
  firstAid,
  childPsychology,
  deungbon,
  crims,
  other,
}

extension AuthImageTypeExt on AuthImageType {
  int get value {
    switch (this) {
      case AuthImageType.health:
        return 10;
      case AuthImageType.covid19Vaccin:
        return 11;
      case AuthImageType.daycareTeach:
        return 21;
      case AuthImageType.kindergarten:
        return 22;
      case AuthImageType.elementary:
        return 23;
      case AuthImageType.childSpecial:
        return 24;
      case AuthImageType.childArts:
        return 25;
      case AuthImageType.schoolCertif:
        return 31;
      case AuthImageType.graduation:
        return 32;
      case AuthImageType.babysitter:
        return 41;
      case AuthImageType.firstAid:
        return 42;
      case AuthImageType.childPsychology:
        return 43;
      case AuthImageType.other:
        return 44;
      default:
        return 0;
    }
  }

  String get name {
    switch (this) {
      case AuthImageType.health:
        return "건강진단결과서".tr();
      case AuthImageType.covid19Vaccin:
        return "백신접종확인서".tr();
      case AuthImageType.daycareTeach:
        return "보육교사".tr();
      case AuthImageType.kindergarten:
        return "유치원정교사".tr();
      case AuthImageType.elementary:
        return "초등학교정교사".tr();
      case AuthImageType.childSpecial:
        return "유아특수교사".tr();
      case AuthImageType.childArts:
        return "유아예체능교사".tr();
      case AuthImageType.schoolCertif:
        return "재학휴학재적증명서".tr();
      case AuthImageType.graduation:
        return "졸업증명서".tr();
      case AuthImageType.babysitter:
        return "베이비시터".tr();
      case AuthImageType.firstAid:
        return "응급처치교육안전사고".tr();
      case AuthImageType.childPsychology:
        return "아동심리치료사".tr();
      case AuthImageType.deungbon:
        return "주민등록등본".tr();
      case AuthImageType.crims:
        return "인증센터_범죄인증".tr();
      case AuthImageType.other:
        return "그외자격증".tr();
      default:
        return "";
    }
  }
}

///돌봄 상태
enum CareStatus {
  none, // nothing
  ///(1, '돌봄예정')
  ready, // 예정
  ///(2, '돌봄 중')
  inprogress, // 진행중
  ///(3, '돌봄종료(정상종료)')
  end, // 종료
  ///(4, '돌봄취소(부분종료&일부취소)')
  cancelParts, // 부분종료, 일부 진행 후 취소
  ///(5, '돌봄취소(전체취소)
  cancelAll // 전체 취소
}

extension CareStatusEx on CareStatus {
  int get value {
    switch (this) {
      case CareStatus.ready:
        return 1;
      case CareStatus.inprogress:
        return 2;
      case CareStatus.end:
        return 3;
      case CareStatus.cancelParts:
        return 4;
      case CareStatus.cancelAll:
        return 5;
      default:
        return 0;
    }
  }

  String get name {
    switch (this) {
      case CareStatus.ready:
        return "돌봄예정".tr();
      case CareStatus.inprogress:
        return "돌봄중".tr();
      case CareStatus.end:
        return "돌봄종료".tr();
      case CareStatus.cancelParts:
        return "부분취소".tr();
      case CareStatus.cancelAll:
        return "전체취소".tr();
      default:
        return "";
    }
  }

  String get desc {
    switch (this) {
      case CareStatus.ready:
        return "돌봄예정_설명".tr();
      case CareStatus.inprogress:
        return "돌봄중_설명".tr();
      case CareStatus.end:
        return "돌봄종료_설명".tr();
      case CareStatus.cancelParts:
      case CareStatus.cancelAll:
        return "돌봄취소_설명".tr();
      default:
        return "";
    }
  }

  Widget get icon {
    switch (this) {
      case CareStatus.ready:
        return Lcons.care_ready();
      case CareStatus.inprogress:
        return Lcons.taking_care();
      case CareStatus.end:
        return Lcons.care_completed();
      case CareStatus.cancelParts:
      case CareStatus.cancelAll:
        return Lcons.fail();
      default:
        return Container();
    }
  }

  String get message {
    switch (this) {
      case CareStatus.ready:
        return "돌봄서비스가예정되어있습니다".tr();
      case CareStatus.inprogress:
      case CareStatus.cancelParts:
        return "돌봄서비스가진행중입니다".tr();
      case CareStatus.end:
        return "돌봄서비스가종료되었습니다".tr();
      case CareStatus.cancelAll:
        return "돌봄서비스가취소되었습니다".tr();
      default:
        return '';
    }
  }

  operator >(status) {
    return this.index > status.index;
  }

  operator <(status) {
    return this.index < status.index;
  }
}

enum BookingStatus {
  /// 0:구인중
  inprogress,

  /// 8:기간만료 (지난일정이 있음)
  expired,

  /// 9:구인종료
  end,

  /// 99:계약완료
  waitPaid,

  /// 100:결제완료
  paid
}

extension BookingStatusEx on BookingStatus {
  int get value {
    switch (this) {
      case BookingStatus.inprogress:
        return 0;
      case BookingStatus.expired:
        return 8;
      case BookingStatus.end:
        return 9;
      case BookingStatus.waitPaid:
        return 99;
      case BookingStatus.paid:
        return 100;
      default:
        return 0;
    }
  }

  String get name {
    switch (this) {
      case BookingStatus.inprogress:
        return "구인중".tr();
      case BookingStatus.expired:
        return "기간만료".tr();
      case BookingStatus.end:
        return "구인종료".tr();
      case BookingStatus.waitPaid:
        return "계약완료".tr();
      case BookingStatus.paid:
        return "결제완료".tr();
      default:
        return "";
    }
  }

  bool get isExpire {
    switch (this) {
      case BookingStatus.expired:
        return true;
      default:
        return false;
    }
  }

  String get expire {
    switch (this) {
      case BookingStatus.expired:
        return Commons.isLinkMom() ? "신청서_기간만료_링크쌤_안내".tr() : "신청서_기간만료_안내".tr();
      default:
        return "";
    }
  }

  String get expireDlg {
    switch (this) {
      case BookingStatus.expired:
        return Commons.isLinkMom() ? "신청서_기간만료_링크쌤_매칭_안내".tr() : "신청서_기간만료_매칭_안내".tr();
      default:
        return "";
    }
  }
}

///구직 중:0, 구직 종료:1
enum BookingLinkMomStatus {
  /// 0:구직중
  inprogress,

  /// 9:구직종료
  end,
}

extension BookingLinkMomStatusEx on BookingLinkMomStatus {
  int get value {
    switch (this) {
      case BookingLinkMomStatus.inprogress:
        return 0;
      case BookingLinkMomStatus.end:
        return 1;
      default:
        return 0;
    }
  }

  String get name {
    switch (this) {
      case BookingLinkMomStatus.inprogress:
        return "구직중".tr();
      case BookingLinkMomStatus.end:
        return "구직종료".tr();
      default:
        return "구직중".tr();
    }
  }

  bool get status {
    switch (this) {
      case BookingLinkMomStatus.inprogress:
        return true;
      case BookingLinkMomStatus.end:
        return false;
      default:
        return false;
    }
  }
}

///매칭 상태
enum MatchingStatus {
  ///(0, '매칭중(대화중)')
  inprogress,

  /// (fail, app only)
  fail,

  ///(11, '매칭실패(링크쌤 응답없음)')
  failNoRespL,

  ///(12, '매칭실패(맘대디 응답없음)')
  failNoRespM,

  ///(13, '매칭실패(링크쌤 거절)')
  failDisagreeL,

  ///(14, '매칭실패(맘대디 거절)')
  failDisagreeM,

  /// (cancel, app only)
  cancel,

  ///(21, '매칭취소(링크쌤 계약취소)')
  cancelLinkmom,

  ///(22, '매칭취소(맘대디 계약취소)')
  cancelMomdaddy,

  ///(23, '매칭취소(결제시간만료)')
  cancelPaid,

  ///(91, '매칭대기(링크쌤 계약대기)')
  waitMomdaddy,

  ///(92, '매칭대기(맘대디 계약대기)')
  waitLinkmom,

  /// (success, app only)
  success,

  ///(99, '매칭성공(계약완료/결제대기중)')
  waitPaid,

  ///(100, '결제완료')
  paid
}

///매칭상태, int, string
extension MatchingStatusEx on MatchingStatus {
  int get value {
    switch (this) {
      case MatchingStatus.inprogress:
        return 0;
      case MatchingStatus.failNoRespL:
        return 11;
      case MatchingStatus.failNoRespM:
        return 12;
      case MatchingStatus.failDisagreeL:
        return 13;
      case MatchingStatus.failDisagreeM:
        return 14;
      case MatchingStatus.cancelLinkmom:
        return 21;
      case MatchingStatus.cancelMomdaddy:
        return 22;
      case MatchingStatus.cancelPaid:
        return 23;
      case MatchingStatus.waitMomdaddy:
        return 91;
      case MatchingStatus.waitLinkmom:
        return 92;
      case MatchingStatus.waitPaid:
        return 99;
      case MatchingStatus.paid:
        return 100;
      default:
        return -1;
    }
  }

  String get name {
    switch (this) {
      case MatchingStatus.inprogress:
        return "매칭중".tr();
      case MatchingStatus.failNoRespL:
      case MatchingStatus.failNoRespM:
      case MatchingStatus.failDisagreeL:
      case MatchingStatus.failDisagreeM:
      case MatchingStatus.fail:
        return "매칭실패".tr();
      case MatchingStatus.cancelLinkmom:
      case MatchingStatus.cancelMomdaddy:
      case MatchingStatus.cancel:
      case MatchingStatus.cancelPaid:
        return "매칭취소".tr();
      case MatchingStatus.waitMomdaddy:
      case MatchingStatus.waitLinkmom:
        return "매칭대기".tr();
      case MatchingStatus.success:
        return "매칭성공".tr();
      case MatchingStatus.waitPaid:
        return "결제대기".tr();
      case MatchingStatus.paid:
        return "결제완료".tr();
      default:
        return "";
    }
  }

  Widget get icon {
    switch (this) {
      case MatchingStatus.inprogress:
      case MatchingStatus.waitMomdaddy:
      case MatchingStatus.waitLinkmom:
        return Lcons.waiting_match();
      case MatchingStatus.failNoRespL:
      case MatchingStatus.failNoRespM:
      case MatchingStatus.failDisagreeL:
      case MatchingStatus.failDisagreeM:
      case MatchingStatus.fail:
        return Lcons.fail();
      case MatchingStatus.cancelLinkmom:
      case MatchingStatus.cancelMomdaddy:
      case MatchingStatus.cancelPaid:
      case MatchingStatus.cancel:
        return Lcons.fail();
      case MatchingStatus.success:
        return Lcons.matching_success();
      case MatchingStatus.waitPaid:
        return Lcons.waiting_payment();
      case MatchingStatus.paid:
        return Lcons.payment_completed();
      default:
        return Container();
    }
  }

  String get message {
    switch (this) {
      case MatchingStatus.waitLinkmom:
        return "상대방의서명을기다리고있어요".tr();
      case MatchingStatus.waitMomdaddy:
        return "나의서명을기다리고있어요".tr();
      case MatchingStatus.failNoRespL:
      case MatchingStatus.failDisagreeL:
      case MatchingStatus.failNoRespM:
      case MatchingStatus.failDisagreeM:
        return '';
      case MatchingStatus.cancelLinkmom:
      case MatchingStatus.cancelMomdaddy:
        return "매칭취소가접수되어매칭이취소되었습니다".tr();
      case MatchingStatus.cancelPaid:
        return "결제가진행되지않아매칭이취소되었습니다".tr();

      /// use custom message
      case MatchingStatus.waitPaid:
        return '';
      case MatchingStatus.paid:
        return "결제가완료되어돌봄이확정되었습니다".tr();
      default:
        return "";
    }
  }

  String get subName {
    switch (this) {
      case MatchingStatus.inprogress:
      case MatchingStatus.waitMomdaddy:
      case MatchingStatus.waitLinkmom:
        return "계약대기".tr();
      case MatchingStatus.success:
        return "계약완료".tr();
      case MatchingStatus.cancelLinkmom:
      case MatchingStatus.cancelMomdaddy:
        return "계약취소".tr();
      case MatchingStatus.cancelPaid:
        return "결제실패".tr();
      default:
        return "";
    }
  }

  operator >(status) {
    return this.index > status.index;
  }

  operator <(status) {
    return this.index < status.index;
  }
}

///보육, 등원, 하원 타입
enum CareType {
  /// 보육
  care,

  /// 등원
  goSchool,

  /// 하원
  backSchool
}

extension CareEx on CareType {
  String get name {
    switch (this) {
      case CareType.care:
        return "보육".tr();
      case CareType.goSchool:
        return "등원".tr();
      case CareType.backSchool:
        return "하원".tr();
      default:
        return "보육".tr();
    }
  }
}

enum ApplyStatus {
  on,

  /// 구인중
  off // 구인종료
}

enum ListType {
  /// 맘대디 돌봄신청서
  mycares,

  /// 맘대디 돌봄 매칭내역 (돌봄진행상황)
  mMatching,

  /// 링크쌤 지원내역
  apply,

  /// 링크쌤 매칭내역
  lMatching,

  /// 돌봄일기
  report,

  /// 결제내역
  payment,

  /// 입급처리상태
  withdrawalStatus
}

enum FilterType {
  /// 지원주체
  bywho,

  /// 구인상태
  bookingState,

  /// 돌봄상태
  careState,

  /// 돌봄유형
  serviceType,

  /// 돌봄일기 - 정산상태
  payState,

  /// 돌봄일기 열람여부
  readState,

  /// 포인트적립
  pointAdd,

  /// 포인트사용
  pointDisappreard,

  /// 캐시적립
  cashAdd,

  /// 캐시사용
  cashDisappreard,

  /// 이용구분
  paymentState,

  /// 입급처리상태
  withdrawalStatus
}

extension FilterEx on FilterType {
  String get name {
    switch (this) {
      case FilterType.bywho:
        return "지원주체".tr();
      case FilterType.bookingState:
        return "구인상태".tr();
      case FilterType.careState:
        return "돌봄진행상황".tr();
      case FilterType.serviceType:
        return "돌봄유형".tr();
      case FilterType.payState:
        return "정산상태".tr();
      case FilterType.readState:
        return "열람여부".tr();
      case FilterType.cashAdd:
        return "캐시적립항목".tr();
      case FilterType.cashDisappreard:
        return "캐시사용항목".tr();
      case FilterType.pointAdd:
        return "포인트적립항목".tr();
      case FilterType.pointDisappreard:
        return "포인트사용항목".tr();
      case FilterType.paymentState:
        return "이용구분".tr();
      case FilterType.withdrawalStatus:
        return "입금처리상태".tr();
      default:
        return '';
    }
  }
}

/*
(0, u'협의되지 않은 추가 업무 요구'),
(1, u'협의된 비용의 일방적인 할인 요구'),
(2, u'추가시간 업무에 따른 비용 미지급'),
(3, u'돌봄 당일 No-show (예약 불이행/연락두절'),
(4, u'링크쌤 앱 외의 거래, 결제유도 (보상 포인트 지급)'),
(5, u'폭언'),
(6, u'폭행'),
(7, u'광고/홍보'),
(8, u'음란/부적절한 대화'),
(10, u'협의된 업무 불이행'),
(11, u'약속된 근무시간 불이행'),
(12, u'돌봄 당일 No-show (예약 불이행/연락두절)'),
(13, u'협의되지 않은 추가비용 요구'),
(14, u'링크쌤 앱 외의 거래, 결제유도 (보상 포인트 지급)'),
(15, u'폭언'),
(16, u'폭행'),
(17, u'광고/홍보'),
(18, u'음란/부적절한 대화'),
(20, u'음란대화 또는 욕설 게시'),
(21, u'불법정보 또는 홍보성 내용'),
(22, u'회원 분란 유도 / 타인 비방'),
(23, u'게시글/ 댓글 도배'),
(24, u'개인 사생활 침해 / 개인정보 노출'),
(99, u'기타 (신고항목 추가요청)')
 */
enum NotifyReason {
  momdadyRequestOT,
  momdadyDiscountCost,
  momdadyUnpaidOT,
  momdadyNoshow,
  momdadyOutOfPlatform,
  momdadyInsult,
  momdadyAssult,
  momdadyAd,
  momdadyInappropriate,
  linkmomNotWork,
  linkmomNotSaveTime,
  linkomoNoshow,
  linkmomChargingOther,
  linkmomOutOfPlatform,
  linkmomInsult,
  linkmomAssult,
  linkmomAd,
  linkmomInappropriate,
  communityInappropriate,
  communityAd,
  communityInsult,
  communityPlastered,
  communityPrivacy,
  other // 99
}

extension NotifyReasonEx on NotifyReason {
  int get value {
    int momdady = 0;
    int linkmom = 10;
    int community = 20;
    switch (this) {
      case NotifyReason.momdadyRequestOT:
        return 0;
      case NotifyReason.momdadyDiscountCost:
        return 1;
      case NotifyReason.momdadyUnpaidOT:
        return 2;
      case NotifyReason.momdadyNoshow:
        return 3;
      case NotifyReason.momdadyOutOfPlatform:
        return 4;
      case NotifyReason.momdadyInsult:
        return 5;
      case NotifyReason.momdadyAssult:
        return 6;
      case NotifyReason.momdadyAd:
        return 7;
      case NotifyReason.momdadyInappropriate:
        return 8;
      case NotifyReason.linkmomNotWork:
        return 10;
      case NotifyReason.linkmomNotSaveTime:
        return 11;
      case NotifyReason.linkomoNoshow:
        return 12;
      case NotifyReason.linkmomChargingOther:
        return 13;
      case NotifyReason.linkmomOutOfPlatform:
        return 14;
      case NotifyReason.linkmomInsult:
        return 15;
      case NotifyReason.linkmomAssult:
        return 16;
      case NotifyReason.linkmomAd:
        return 17;
      case NotifyReason.linkmomInappropriate:
        return 18;
      case NotifyReason.communityInappropriate:
        return 20;
      case NotifyReason.communityAd:
        return 21;
      case NotifyReason.communityInsult:
        return 22;
      case NotifyReason.communityPlastered:
        return 23;
      case NotifyReason.communityPrivacy:
        return 24;
      case NotifyReason.other:
        return 99;
      default:
        return this.index;
    }
  }

  String get reason {
    switch (this) {
      case NotifyReason.linkmomNotWork:
        return "협의된업무불이행".tr();
      case NotifyReason.linkmomNotSaveTime:
        return "약속된근무시간불이행".tr();
      case NotifyReason.linkomoNoshow:
      case NotifyReason.momdadyNoshow:
        return "돌봄당일노쇼".tr();
      case NotifyReason.linkmomChargingOther:
        return "협의되지않은추가비용요구".tr();
      case NotifyReason.momdadyDiscountCost:
        return "협의된비용의일방적인할인요구".tr();
      case NotifyReason.momdadyRequestOT:
        return "협의되지않은추가업무요구".tr();
      case NotifyReason.momdadyUnpaidOT:
        return "추가시간업무에따른비용미지급".tr();
      case NotifyReason.momdadyOutOfPlatform:
      case NotifyReason.linkmomOutOfPlatform:
        return "링크맘외의거래결제유도".tr();
      case NotifyReason.linkmomInsult:
      case NotifyReason.momdadyInsult:
        return "폭언".tr();
      case NotifyReason.linkmomAssult:
      case NotifyReason.momdadyAssult:
        return "폭행".tr();
      case NotifyReason.linkmomAd:
      case NotifyReason.momdadyAd:
        return "광고홍보".tr();
      case NotifyReason.linkmomInappropriate:
      case NotifyReason.momdadyInappropriate:
        return "음란부적절한대화".tr();
      case NotifyReason.communityInappropriate:
        return "음란대화또는욕설게시".tr();
      case NotifyReason.communityAd:
        return "불법정보또는홍보성내용".tr();
      case NotifyReason.communityInsult:
        return "회원분란유도타인비방".tr();
      case NotifyReason.communityPlastered:
        return "게시글댓글도배".tr();
      case NotifyReason.communityPrivacy:
        return "개인사생활침해개인정보노출".tr();
      default:
        return "기타_신고항목추가요청".tr();
    }
  }
}

enum DrugTimeType {
  before,
  doing,
  after,
}

extension DrugTimeTypeEx on DrugTimeType {
  String get name {
    switch (this) {
      case DrugTimeType.before:
        return "식전".tr();
      case DrugTimeType.doing:
        return "식간".tr();
      case DrugTimeType.after:
        return "식후".tr();
      default:
        return "";
    }
  }
}

enum PayStatus {
  ///정산예정 0
  ready,

  ///정산완료 1
  compleat,

  ///정산보류 2
  delayed,

  ///(10, '돌봄취소(보상) 예정')
  cancelReady,

  ///(11, '돌봄취소(보상) 완료')
  cancel,

  /// (20, '돌봄취소(벌점부과) 예정'),
  penaltyReady,

  /// (21, '돌봄취소(벌점부과) 완료'),
  penalty,
}

extension PayStatusEx on PayStatus {
  int get value {
    switch (this) {
      case PayStatus.ready:
        return 0;
      case PayStatus.compleat:
        return 1;
      case PayStatus.delayed:
        return 2;
      case PayStatus.cancelReady:
        return 10;
      case PayStatus.cancel:
        return 11;
      case PayStatus.penaltyReady:
        return 20;
      case PayStatus.penalty:
        return 21;
      default:
        return 0;
    }
  }

  String get name {
    switch (this) {
      case PayStatus.ready:
        return "정산예정".tr();
      case PayStatus.compleat:
        return "정산완료".tr();
      case PayStatus.delayed:
        return "정산보류".tr();
      case PayStatus.cancelReady:
        return "돌봄취소예정".tr();
      case PayStatus.penaltyReady:
        return "돌봄취소_벌점부과예정".tr();
      case PayStatus.cancel:
      case PayStatus.penalty:
        return "돌봄취소".tr();
      default:
        return "돌봄시작".tr();
    }
  }

  operator >(status) {
    return this.value > status.value;
  }

  operator <(status) {
    return this.value < status.value;
  }
}

enum Category {
  careComm,
  give,
  review,
}

extension CommunityTypeEx on Category {
  String get name {
    switch (this) {
      case Category.careComm:
        return "육아소통".tr();
      case Category.give:
        return "물건나눔".tr();
      case Category.review:
        return "이용후기".tr();
      default:
        return "";
    }
  }

  String get hint {
    switch (this) {
      case Category.careComm:
        return "커뮤니티_글쓰기_힌트".tr();
      case Category.give:
        return "플리마켓_글쓰기_힌트".tr();
      case Category.review:
        return "이용후기_글쓰기_힌트".tr();
      default:
        return "";
    }
  }
}

enum ShareStatus {
  inprogress,
  reserve,
  compleate,
}

extension ShareStatusEx on ShareStatus {
  String get name {
    switch (this) {
      case ShareStatus.inprogress:
        return "진행중".tr();
      case ShareStatus.reserve:
        return "예약중".tr();
      case ShareStatus.compleate:
        return "나눔완료".tr();
      default:
        return "";
    }
  }
}

enum RankType { cost, like, review }

extension RankTypeEx on RankType {
  String get name {
    switch (this) {
      case RankType.cost:
        return "금액랭킹".tr();
      case RankType.like:
        return "좋아요랭킹".tr();
      case RankType.review:
        return "후기랭킹".tr();
      default:
        return "";
    }
  }
}

enum DiaryClaim {
  notwork,
  notSaveTime,
  chargingOther,
  noshow,
  insult,
  assult,
  other,
}

extension DiaryClaimEx on DiaryClaim {
  String get reason {
    switch (this) {
      case DiaryClaim.notwork:
        return "협의된업무불이행".tr();
      case DiaryClaim.notSaveTime:
        return "약속된근무시간불이행".tr();
      case DiaryClaim.chargingOther:
        return "협의되지않은추가비용요구".tr();
      case DiaryClaim.noshow:
        return "돌봄당일노쇼".tr();
      case DiaryClaim.insult:
        return "폭언".tr();
      case DiaryClaim.assult:
        return "폭행".tr();
      case DiaryClaim.other:
        return "기타".tr();
      default:
        return '';
    }
  }

  int get value {
    switch (this) {
      case DiaryClaim.notwork:
      case DiaryClaim.notSaveTime:
      case DiaryClaim.chargingOther:
      case DiaryClaim.noshow:
      case DiaryClaim.insult:
      case DiaryClaim.assult:
        return this.index;
      default:
        return 9;
    }
  }
}

enum ContactType {
  care,
  job,
  join,
  pay,
  community,
  other,
}

extension ContactEx on ContactType {
  String get name {
    switch (this) {
      case ContactType.care:
        return "돌봄신청".tr();
      case ContactType.job:
        return "구직신청".tr();
      case ContactType.join:
        return "회원가입".tr();
      case ContactType.pay:
        return "결제환불".tr();
      case ContactType.community:
        return "커뮤니티".tr();
      case ContactType.other:
        return "기타문의".tr();
      default:
        return "기타_신고항목추가요청".tr();
    }
  }
}

/// (0, u'채팅'),<br/>
/// (1, u'돌봄신청'), (2, u'구직신청'), (4, u'돌봄일기'), (5, u'돌봄관리'),(52, u'돌봄관리-링크맘찾기->맘대디 모드로 변경'),(53, u'돌봄관리-맘대디찾기->링크쌤모드로 변경', 54, 기간만료), (9, u'결제'),<br/>
/// (3, u'이벤트'),<br/>
/// (60, u'커뮤니티'), (61, u'커뮤니티-소통나눔'),<br/>
/// (70, u'마이페이지'), (71, u'마이페이지-벌점'), (72, u'마이페이지-후기'), (73, u'마이페이지-결제내역-포인트소멸'), (74, 인증센터)
/// (80, u'고객센터'), (81, u'고객센터-공지사항'), (82, u'고객센터-1:1문의')
/// (90, 제휴 특정지역, u'링크맘찾기->맘대디 모드로 변경'),(91, 제휴 특정지역, u'맘대디찾기->링크쌤모드로 변경') <br/>
enum NotificationType {
  chatting,
  all,
  chatCareManage,
  care,
  jobs,
  careManage,
  workNoti,
  jobListLinkMom,
  jobListMomDaddy,
  careExpired,
  applyLinkMom,
  applyMomDaddy,
  pay,
  diary,
  diaryWrite,
  event,
  community,
  communityBoard,
  mypage,
  mypagePenalty,
  mypageReview,
  mypageExpiredPoint,
  mypageAuthcenter,
  benefit,
  cs,
  csNotice,
  csContact,
  listLinkMom,
  listMomDaddy,
}

extension NotiEx on NotificationType {
  int get value {
    switch (this) {
      case NotificationType.chatting:
        return 0;
      case NotificationType.care:
        return 1;
      case NotificationType.jobs:
        return 2;
      case NotificationType.event:
        return 3;
      case NotificationType.diary:
        return 4;
      case NotificationType.diaryWrite:
        return 40;
      case NotificationType.chatCareManage:
        return 5;
      case NotificationType.pay:
        return 9;
      case NotificationType.careManage:
        return 50;
      case NotificationType.workNoti:
        return 51;
      case NotificationType.jobListLinkMom:
        return 52;
      case NotificationType.jobListMomDaddy:
        return 53;
      case NotificationType.careExpired:
        return 54;
      case NotificationType.applyMomDaddy:
        return 55;
      case NotificationType.applyLinkMom:
        return 56;
      case NotificationType.community:
        return 60;
      case NotificationType.communityBoard:
        return 61;
      case NotificationType.mypage:
        return 70;
      case NotificationType.mypagePenalty:
        return 71;
      case NotificationType.mypageReview:
        return 72;
      case NotificationType.mypageExpiredPoint:
        return 73;
      case NotificationType.mypageAuthcenter:
        return 74;
      case NotificationType.benefit:
        return 75;
      case NotificationType.cs:
        return 80;
      case NotificationType.csNotice:
        return 81;
      case NotificationType.csContact:
        return 82;
      case NotificationType.listLinkMom:
        return 90;
      case NotificationType.listMomDaddy:
        return 91;
      default:
        return 999;
    }
  }

  String get name {
    switch (this) {
      case NotificationType.chatting:
      case NotificationType.care:
      case NotificationType.jobs:
      case NotificationType.diary:
      case NotificationType.diaryWrite:
      case NotificationType.chatCareManage:
      case NotificationType.careManage:
      case NotificationType.workNoti:
      case NotificationType.jobListLinkMom:
      case NotificationType.jobListMomDaddy:
      case NotificationType.careExpired:
        return "돌봄관리".tr();
      case NotificationType.pay:
        return "결제".tr();
      case NotificationType.event:
      case NotificationType.listLinkMom:
      case NotificationType.listMomDaddy:
        return "이벤트".tr();
      case NotificationType.community:
      case NotificationType.communityBoard:
        return "커뮤니티".tr();
      case NotificationType.mypage:
      case NotificationType.mypagePenalty:
      case NotificationType.mypageReview:
      case NotificationType.mypageExpiredPoint:
      case NotificationType.mypageAuthcenter:
      case NotificationType.benefit:
        return "마이페이지".tr();
      case NotificationType.cs:
      case NotificationType.csNotice:
      case NotificationType.csContact:
        return "고객센터".tr();
      default:
        return "";
    }
  }

  bool get msgParse {
    switch (this) {
      case NotificationType.chatting:
      case NotificationType.care:
      case NotificationType.jobs:
      case NotificationType.diary:
      case NotificationType.chatCareManage:
      case NotificationType.careManage:
      case NotificationType.pay:
        return true;
      default:
        return false;
    }
  }

  bool get isChat {
    switch (this) {
      case NotificationType.chatting:
      case NotificationType.chatCareManage:
      case NotificationType.careManage:
      case NotificationType.pay:
        return true;
      default:
        return false;
    }
  }
}

/// (52001 : 52, u'돌봄관리-링크맘찾기->맘대디 모드로 변경'), (53001 :53, u'돌봄관리-맘대디찾기->링크쌤모드로 변경')
enum SendCodeType {
  SCLinkMom,
  SCMomDaddy,
  SCDefault,
}

extension SendCodeTypeEx on SendCodeType {
  int get value {
    switch (this) {
      case SendCodeType.SCLinkMom:
        return 52001;
      case SendCodeType.SCMomDaddy:
        return 53001;
      default:
        return 0;
    }
  }

  int get requestValue {
    switch (this) {
      case SendCodeType.SCLinkMom:
      case SendCodeType.SCMomDaddy:
        return 1;
      default:
        return 0;
    }
  }

  bool get isRegion {
    switch (this) {
      case SendCodeType.SCLinkMom:
      case SendCodeType.SCMomDaddy:
        return true;
      default:
        return false;
    }
  }

  String get title {
    switch (this) {
      // case SendCodeType.SCLinkMom:
      //   return "돌봄희망지역".tr();
      // case SendCodeType.SCMomDaddy:
      //   return "근무희망지역".tr();
      // case SendCodeType.SCLinkMom:
      //   return Commons.isLinkMom() ? "근무희망지역".tr() : "돌봄희망지역".tr();
      // case SendCodeType.SCMomDaddy:
      //   return Commons.isLinkMom() ? "돌봄희망지역".tr() : "근무희망지역".tr();
      case SendCodeType.SCLinkMom:
        return !Commons.isLinkMom() ? "돌봄희망지역".tr() : '';
      case SendCodeType.SCMomDaddy:
        return Commons.isLinkMom() ? "근무희망지역".tr() : '';
      default:
        return '';
    }
  }
}

///알림화면 탭 index (0:전체, 1:돌본관리, 2:커뮤니티, 3:마이페이지, 4:이벤트, 5:고객센터)
enum NotificationTabType {
  all,
  care,
  community,
  mypage,
  event,
  cs,
}

///결제방법(신용카드, 간편결제, 계좌이체)
enum PayType {
  ///카드 - 신용카드
  card,

  ///pay - 간편결제
  pay,

  ///bank - 계좌이체
  bank,

  ///포인트,캐시결제
  free,

  ///간편결제 포인트
  point,
}

extension PayTypeEx on PayType {
  int get value {
    switch (this) {
      case PayType.card:
        return 0;
      case PayType.pay:
      case PayType.point:
        return 1;
      case PayType.bank:
        return 2;
      case PayType.free:
        return 3;
      default:
        return 3;
    }
  }

  String get string {
    switch (this) {
      case PayType.card:
        return "신용카드".tr();
      case PayType.point:
      case PayType.pay:
        return "간편결제(카카오페이)".tr();
      case PayType.bank:
        return "계좌이체/무통장입금".tr();
      case PayType.free:
        return "free";
      default:
        return "free";
    }
  }

  String get payMethod {
    switch (this) {
      case PayType.card:
        return 'card';
      case PayType.pay:
        return 'card';
      case PayType.bank:
        return 'vbank';
      case PayType.free:
        return 'free';
      case PayType.point:
        return 'point';
      default:
        return 'free';
    }
  }

  String get payPg {
    switch (this) {
      case PayType.card:
        return 'uplus';
      case PayType.point:
      case PayType.pay:
        return 'kakaopay';
      case PayType.bank:
        return 'uplus';
      case PayType.free:
        return 'free';
      default:
        return 'free';
    }
  }
}

///결제관련 에러 번호
enum PayError {
  i_3000,
  i_6592,
  i_6593,
  i_6599,
  i_6000,
}

extension PayErrorEx on PayError {
  int get state {
    switch (this) {
      case PayError.i_3000:
        return 3000;
      case PayError.i_6592:
        return 6592;
      case PayError.i_6593:
        return 6593;
      case PayError.i_6000:
        return 6000;
      case PayError.i_6599:
        return 6599;
    }
  }

  String get value {
    switch (this) {
      case PayError.i_3000:
        return "등록에 실패하였습니다.";
      case PayError.i_6592:
        return "주문번호가 잘못되었습니다.";
      case PayError.i_6593:
        return "결제가 완료되지 않았습니다.";
      case PayError.i_6599:
        return "결제정보 누락. 확인 후 다시 결제해주세요.";
      case PayError.i_6000:
        return "이미 결제된 내역 입니다.";
      default:
        return "에러";
    }
  }
}

///홈, 커뮤니티, 관리, 찾기, 채팅
enum MenuType {
  home,
  community,
  care,
  matching,
  chat,
}

extension MenuTypeEx on MenuType {
  int get value {
    switch (this) {
      case MenuType.home:
        return 0;
      case MenuType.community:
        return 1;
      case MenuType.care:
        return 2;
      case MenuType.matching:
        return 3;
      case MenuType.chat:
        return 4;
      default:
        return 0;
    }
  }

  String get nameE {
    switch (this) {
      case MenuType.home:
        return 'home';
      case MenuType.community:
        return 'community';
      case MenuType.care:
        return 'care';
      case MenuType.matching:
        return 'matching';
      case MenuType.chat:
        return 'chat';
      default:
        return 'home';
    }
  }

  String get name {
    switch (this) {
      case MenuType.home:
        return "홈".tr();
      case MenuType.community:
        return "커뮤니티".tr();
      case MenuType.care:
        return Commons.isLinkMom() ? "근무관리".tr() : "돌봄관리".tr();
      case MenuType.matching:
        return Commons.isLinkMom() ? "맘대디찾기".tr() : "링크쌤찾기".tr();
      case MenuType.chat:
        return "채팅".tr();
      default:
        return "홈".tr();
    }
  }
}

///<맘대디측 사유>
///
/// (10, '[맘대디 사유] 계약중, 거절하기'),
///
/// (11, '[맘대디 사유] 돌봄 필요한 일정이 바뀌었어요.'),
///
/// (12, '[맘대디 사유] 링크쌤과 돌봄성향이 맞지 않아요.'),
///
/// (13, '[맘대디 사유] 질병, 사고 등으로 인해 서비스 이용이 어려워요.'),
///
/// (14, '[맘대디 사유] 더 이상 돌봄이 필요 없어 졌어요.'),
///
/// (15, '[맘대디 사유] 맘대디가 돌봄 당일 미리 연락없이 아이를 맡기지 않았어요.'),
///
/// (16, '[맘대디 사유] 맘대디가 취소를 요청해 왔어요.'),
///
/// (19, '[맘대디 사유] 기타')
///
/// <링크쌤측 사유>
///
/// (20, '[링크쌤 사유] 계약중, 거절하기'),
///
/// (21, '[링크쌤 사유] 돌봄 활동 가능한 일정이 바뀌었어요.'),
///
/// (22, '[링크쌤 사유] 맘대디와 돌봄성향이 맞지 않아요.'),
///
/// (23, '[링크쌤 사유] 질병, 사고 등으로 인해 서비스 제공이 어려워요.'),
///
/// (24, '[링크쌤 사유] 당분간 돌봄 활동을 하지 않을 생각이에요.')
///
/// (25, '[링크쌤 사유] 링크쌤이 돌봄 당일 연락없이 출근하지 않았어요.'),
///
/// (26, '[링크쌤 사유] 링크쌤이 취소를 요청해 왔어요.'),
///
/// (29, '[링크쌤 사유] 기타')
enum CancelType {
  /// (10, '[맘대디 사유] 계약중, 거절하기'),
  momdady_10,

  /// (11, '[맘대디 사유] 돌봄 필요한 일정이 바뀌었어요.'),
  momdady_11,

  /// (12, '[맘대디 사유] 링크쌤과 돌봄성향이 맞지 않아요.'),
  momdady_12,

  /// (13, '[맘대디 사유] 질병, 사고 등으로 인해 서비스 이용이 어려워요.'),
  momdady_13,

  /// (14, '[맘대디 사유] 더 이상 돌봄이 필요 없어 졌어요.'),
  momdady_14,

  /// (15, '[맘대디 사유] 맘대디가 돌봄 당일 미리 연락없이 아이를 맡기지 않았어요.'),
  momdady_15,

  /// (16, '[맘대디 사유] 맘대디가 취소를 요청해 왔어요.'),
  momdady_16,

  /// (19, '[맘대디 사유] 기타')
  momdady_19,

  /// (20, '[링크쌤 사유] 계약중, 거절하기'),
  linkmom_20,

  /// (21, '[링크쌤 사유] 돌봄 활동 가능한 일정이 바뀌었어요.'),
  linkmom_21,

  /// (22, '[링크쌤 사유] 맘대디와 돌봄성향이 맞지 않아요.'),
  linkmom_22,

  /// (23, '[링크쌤 사유] 질병, 사고 등으로 인해 서비스 제공이 어려워요.'),
  linkmom_23,

  /// (24, '[링크쌤 사유] 당분간 돌봄 활동을 하지 않을 생각이에요.')
  linkmom_24,

  /// (25, '[링크쌤 사유] 링크쌤이 돌봄 당일 연락없이 출근하지 않았어요.'),
  linkmom_25,

  /// (26, '[링크쌤 사유] 링크쌤이 취소를 요청해 왔어요.'),
  linkmom_26,

  /// (29, '[링크쌤 사유] 기타')
  linkmom_29,
}

extension CancelTypeEx on CancelType {
  int get value {
    switch (this) {
      case CancelType.momdady_10:
        return 10;
      case CancelType.momdady_11:
        return 11;
      case CancelType.momdady_12:
        return 12;
      case CancelType.momdady_13:
        return 13;
      case CancelType.momdady_14:
        return 14;
      case CancelType.momdady_15:
        return 15;
      case CancelType.momdady_16:
        return 16;
      case CancelType.momdady_19:
        return 19;
      case CancelType.linkmom_20:
        return 20;
      case CancelType.linkmom_21:
        return 21;
      case CancelType.linkmom_22:
        return 22;
      case CancelType.linkmom_23:
        return 23;
      case CancelType.linkmom_24:
        return 24;
      case CancelType.linkmom_25:
        return 25;
      case CancelType.linkmom_26:
        return 26;
      case CancelType.linkmom_29:
        return 29;
      default:
        return 0;
    }
  }

  ///momdady_13, linkmom_23 맘대디, 링크쌤를 붙여서 사용 할것!
  String get string {
    switch (this) {
      case CancelType.momdady_10:
        return "맘대디_취소사유_1".tr();
      case CancelType.momdady_11:
        return "맘대디_취소사유_1".tr();
      case CancelType.momdady_12:
        return "맘대디_취소사유_2".tr();
      case CancelType.momdady_13:
        return "맘대디_취소사유_3".tr(); //링크쌤에서 맘대디측 사유 맘대디 붙여서 사용 할것!
      case CancelType.momdady_14:
        return "맘대디_취소사유_4".tr();
      case CancelType.momdady_15:
        return "링크쌤_취소사유_6".tr();
      case CancelType.momdady_16:
        return "링크쌤_취소사유_5".tr();
      case CancelType.momdady_19:
        return "기타".tr();
      case CancelType.linkmom_20:
        return "링크쌤_취소사유_1".tr();
      case CancelType.linkmom_21:
        return "링크쌤_취소사유_1".tr();
      case CancelType.linkmom_22:
        return "링크쌤_취소사유_2".tr();
      case CancelType.linkmom_23:
        return "링크쌤_취소사유_3".tr(); //맘대디에서 링크쌤측 사유 링크쌤 붙여서 사용 할것!
      case CancelType.linkmom_24:
        return "링크쌤_취소사유_4".tr();
      case CancelType.linkmom_25:
        return "맘대디_취소사유_6".tr();
      case CancelType.linkmom_26:
        return "맘대디_취소사유_5".tr();
      case CancelType.linkmom_29:
        return "기타".tr();
      default:
        return '';
    }
  }

  String get userType {
    switch (this) {
      case CancelType.momdady_10:
      case CancelType.momdady_11:
      case CancelType.momdady_12:
      case CancelType.momdady_13:
      case CancelType.momdady_14:
      case CancelType.momdady_15:
      case CancelType.momdady_16:
      case CancelType.momdady_19:
        return "맘대디측사유".tr();
      case CancelType.linkmom_20:
      case CancelType.linkmom_21:
      case CancelType.linkmom_22:
      case CancelType.linkmom_23:
      case CancelType.linkmom_24:
      case CancelType.linkmom_25:
      case CancelType.linkmom_26:
      case CancelType.linkmom_29:
        return "링크쌤측사유".tr();
      default:
        return '';
    }
  }

  bool get isLinkMom {
    switch (this) {
      case CancelType.momdady_10:
      case CancelType.momdady_11:
      case CancelType.momdady_12:
      case CancelType.momdady_13:
      case CancelType.momdady_14:
      case CancelType.momdady_15:
      case CancelType.momdady_16:
      case CancelType.momdady_19:
        return false;
      case CancelType.linkmom_20:
      case CancelType.linkmom_21:
      case CancelType.linkmom_22:
      case CancelType.linkmom_23:
      case CancelType.linkmom_24:
      case CancelType.linkmom_25:
      case CancelType.linkmom_26:
      case CancelType.linkmom_29:
        return true;
      default:
        return false;
    }
  }

  Color get color {
    switch (this) {
      case CancelType.momdady_10:
      case CancelType.momdady_11:
      case CancelType.momdady_12:
      case CancelType.momdady_13:
      case CancelType.momdady_14:
      case CancelType.momdady_15:
      case CancelType.momdady_16:
      case CancelType.momdady_19:
        return color_momdady;
      case CancelType.linkmom_20:
      case CancelType.linkmom_21:
      case CancelType.linkmom_22:
      case CancelType.linkmom_23:
      case CancelType.linkmom_24:
      case CancelType.linkmom_25:
      case CancelType.linkmom_26:
      case CancelType.linkmom_29:
        return color_linkmom;
      default:
        return color_momdady;
    }
  }
}

///취소사유(1:맘대디, 2:링크쌤)
enum CancelUserType {
  momdady,
  linkmom,
}

extension CancelUserTypeEx on CancelUserType {
  int get value {
    switch (this) {
      case CancelUserType.momdady:
        return 1;
      case CancelUserType.linkmom:
        return 2;
      default:
        return 1;
    }
  }

  String get string {
    switch (this) {
      case CancelUserType.momdady:
        return "맘대디측사유".tr();
      case CancelUserType.linkmom:
        return "링크쌤측사유".tr();
      default:
        return "맘대디측사유".tr();
    }
  }

  Color get color {
    switch (this) {
      case CancelUserType.momdady:
        return color_momdady;
      case CancelUserType.linkmom:
        return color_linkmom;
      default:
        return color_momdady;
    }
  }

  bool get isLinkMom {
    switch (this) {
      case CancelUserType.momdady:
        return false;
      case CancelUserType.linkmom:
        return true;
      default:
        return false;
    }
  }
}

///취소하면 뷰 타입(결제, 돌봄취소, 돌봄취소 확인, 돌봄취소 예정)
enum PayViewType {
  payment,
  cancel_init,
  cancel_view,
  cancel_ready,
  cancel_complete,
}

enum CommunityTab { community, rank, event }

/// 결제내역 타입
enum PaymentType {
  add, // normal
  refund, // 환불
  cancel, // 취소
  paid, // 정상결제
  delete, // 소멸
}

/// 공지사항 ID
enum Terms {
  /// 1: 개인정보 취급방침
  privacy,

  /// 3: 통합 이용 약관 동의
  total,

  /// 4: 개인정보 수집 및 이용 동의
  personal,

  /// 5: 환불 및 취소 규정 동의
  refund,

  /// 6: 위치정보 이용 약관 동의
  location,

  /// 7: 마케팅 정보 수신 동의
  marketing,

  /// 8: 결제대행서비스 이용 약관 동의
  payment,

  /// 9: 구매조건 확인 및 취소 환불 규정 동의
  product,

  /// 10: 이벤트 및 광고 수신 동의
  event,

  /// 11: 통신사 이용약관 동의
  telecom,

  /// 12: 민감정보수집이용동의
  sensitive,
}

extension TemrsEx on Terms {
  int get value {
    switch (this) {
      case Terms.privacy:
        return 1;
      case Terms.total:
        return 3;
      case Terms.personal:
        return 4;
      case Terms.refund:
        return 5;
      case Terms.location:
        return 6;
      case Terms.marketing:
        return 7;
      case Terms.payment:
        return 8;
      case Terms.product:
        return 9;
      case Terms.event:
        return 10;
      case Terms.telecom:
        return 11;
      case Terms.sensitive:
        return 12;
      default:
        return 0;
    }
  }

  String get name {
    switch (this) {
      case Terms.privacy:
        return "개인정보수집".tr();
      case Terms.total:
        return "이용약관동의".tr();
      case Terms.personal:
        return "개인정보수집".tr();
      case Terms.refund:
        return "환불및취소약관".tr();
      case Terms.location:
        return "위치기반서비스".tr();
      case Terms.marketing:
        return "마케팅정보수신동의".tr();
      case Terms.payment:
        return "결제대행서비스이용약관동의".tr();
      case Terms.product:
        return "구매조건확인및취소환불규정".tr();
      case Terms.event:
        return "이벤트광고".tr();
      case Terms.telecom:
        return "통신사이용약관".tr();
      case Terms.sensitive:
        return "민감정보수집이용동의".tr();
      default:
        return "";
    }
  }
}

enum PaymentReason {
  add, // 소득
  refund, // 환불
  paid, // 결제
  withdrawal, // 인출
  disappeared, // 소멸
}

extension PaymentEx on PaymentReason {
  int get value {
    switch (this) {
      case PaymentReason.add:
        return 10;
      case PaymentReason.refund:
        return 11;
      case PaymentReason.paid:
        return 20;
      case PaymentReason.withdrawal:
        return 21;
      case PaymentReason.disappeared:
        return 99;
    }
  }
}

enum WithdrawalStatus {
  ready,
  complete,
}

extension WithdrawalEx on WithdrawalStatus {
  int get value {
    switch (this) {
      case WithdrawalStatus.ready:
        return 0;
      case WithdrawalStatus.complete:
        return 1;
    }
  }

  String get name {
    switch (this) {
      case WithdrawalStatus.ready:
        return "입금예정".tr();
      case WithdrawalStatus.complete:
        return "입금완료".tr();
    }
  }
}

enum HireStatus {
  inprogress,
  end,
  expired,
}

extension HireEx on HireStatus {
  int get value {
    switch (this) {
      case HireStatus.inprogress:
        return 0;
      case HireStatus.end:
        return 1;
      case HireStatus.expired:
        return 8;
    }
  }

  String get name {
    switch (this) {
      case HireStatus.inprogress:
        return "구인중".tr();
      case HireStatus.end:
        return "구인종료".tr();
      case HireStatus.expired:
        return "기간만료".tr();
    }
  }

  String get desc {
    switch (this) {
      case HireStatus.inprogress:
        return "구인중_설명".tr();
      case HireStatus.end:
        return "구인종료_설명".tr();
      case HireStatus.expired:
        return "기간만료_설명".tr();
    }
  }
}

enum CareStateType {
  ready,
  inprogress,
  end,
  canceled,
}

extension CareStateTypeEx on CareStateType {
  int get value {
    switch (this) {
      case CareStateType.ready:
        return 1;
      case CareStateType.inprogress:
        return 2;
      case CareStateType.end:
        return 3;
      case CareStateType.canceled:
        return 4;
      default:
        return 1;
    }
  }

  String get name {
    switch (this) {
      case CareStateType.ready:
        return "돌봄예정".tr();
      case CareStateType.inprogress:
        return "돌봄중".tr();
      case CareStateType.end:
        return "돌봄종료".tr();
      case CareStateType.canceled:
        return "돌봄취소".tr();
      default:
        return "돌봄예정".tr();
    }
  }

  String get desc {
    switch (this) {
      case CareStateType.ready:
        return "돌봄예정_설명".tr();
      case CareStateType.inprogress:
        return "돌봄중_설명".tr();
      case CareStateType.end:
        return "돌봄종료_설명".tr();
      case CareStateType.canceled:
        return "돌봄취소_설명".tr();
      default:
        return "돌봄예정_설명".tr();
    }
  }
}

/// For cs/userguide/view/
enum CsGuide {
  none,
  momdady,
  linkmom,
  penalty,
  reserveMomdady,
  reserveLinkmom,
}

///돌봄취소 상태
enum ContractStatus {
  ///(21, '부분취소')
  contract_cancel_parts,

  ///(22, '돌봄취소(전체취소)
  contract_cancel_all,
}

extension ContractStatusEx on ContractStatus {
  int get value {
    switch (this) {
      case ContractStatus.contract_cancel_parts:
        return 21;
      case ContractStatus.contract_cancel_all:
        return 22;
      default:
        return 21;
    }
  }
}

///검색(전제,동네인증지역,돌봄가능지역)
enum SearchZoneStatus {
  ///0 - 전체
  all,

  ///1 - 동네인증지역
  auth_zone,

  ///2 - 돌봉가능지역
  care_zone,
}

extension SearchZoneStatusEx on SearchZoneStatus {
  int get value {
    switch (this) {
      case SearchZoneStatus.all:
        return 0;
      case SearchZoneStatus.auth_zone:
        return 1;
      case SearchZoneStatus.care_zone:
        return 2;
      default:
        return 0;
    }
  }

  String get name {
    switch (this) {
      case SearchZoneStatus.all:
        return "전체".tr();
      case SearchZoneStatus.auth_zone:
        return "동네인증지역".tr();
      case SearchZoneStatus.care_zone:
        return "돌봄가능지역".tr();
      default:
        return "전체".tr();
    }
  }
}

enum RegisterIndex {
  none,
  agreement,
  auth,
  id,
  pw,
  invite,
}

enum JoinPath {
  other,
  internet,
  jobPortal,
  sns,
  community,
  suporters,
  outOfPlatform,
  introduce,
  news,
  broswer,
}

extension JoinPathEx on JoinPath {
  String get name {
    switch (this) {
      case JoinPath.other:
        return "기타".tr();
      case JoinPath.internet:
        return "인터넷포털검색".tr();
      case JoinPath.jobPortal:
        return "취업포털".tr();
      case JoinPath.sns:
        return "SNS".tr();
      case JoinPath.community:
        return "커뮤니티_카페".tr();
      case JoinPath.suporters:
        return "서포터즈모집".tr();
      case JoinPath.outOfPlatform:
        return "타사몰".tr();
      case JoinPath.introduce:
        return "주위권유소개".tr();
      case JoinPath.news:
        return "신문기사뉴스".tr();
      case JoinPath.broswer:
        return "전단지플랜카드".tr();
      default:
        return "기타".tr();
    }
  }
}

enum AuthStatus {
  ready,
  success,
  fail,
  none,
}

enum AuthImgType {
  health,
  career,
  gradue,
  educate,
}

enum GuideType {
  guid_0,
  guid_1,
  guid_2,
  guid_3,
  guid_4,
}

extension GuildeTypeEx on GuideType {
  int get value {
    switch (this) {
      case GuideType.guid_0:
        return 0;
      case GuideType.guid_1:
        return 1;
      case GuideType.guid_2:
        return 2;
      case GuideType.guid_3:
        return 3;
      case GuideType.guid_4:
        return 4;
      default:
        return 0;
    }
  }

  String get name {
    switch (this) {
      case GuideType.guid_0:
        return Commons.isLinkMom() ? "렛츠링크쌤".tr() : "헬로맘대디".tr();
      case GuideType.guid_1:
        return Commons.isLinkMom() ? "돌봄서비스".tr() : "빠른_매칭".tr();
      case GuideType.guid_2:
        return Commons.isLinkMom() ? "돌봄매칭".tr() : "채팅후매칭".tr();
      case GuideType.guid_3:
        return Commons.isLinkMom() ? "돌봄_일기".tr() : "돌봄_일기".tr();
      case GuideType.guid_4:
        return Commons.isLinkMom() ? "급여인출".tr() : "정산과후기".tr();
      default:
        return Commons.isLinkMom() ? "렛츠링크쌤".tr() : "헬로맘대디".tr();
    }
  }

  String get content {
    switch (this) {
      case GuideType.guid_0:
        return Commons.isLinkMom() ? "렛츠링크쌤_안내".tr() : "헬로맘대디_안내".tr();
      case GuideType.guid_1:
        return Commons.isLinkMom() ? "돌봄서비스_안내".tr() : "빠른매칭_안내".tr();
      case GuideType.guid_2:
        return Commons.isLinkMom() ? "돌봄매칭_안내".tr() : "채팅후매칭_안내".tr();
      case GuideType.guid_3:
        return Commons.isLinkMom() ? "돌봄일기_안내_링크쌤".tr() : "돌봄일기_안내_맘대디".tr();
      case GuideType.guid_4:
        return Commons.isLinkMom() ? "급여인출_안내".tr() : "정상과후기_안내".tr();
      default:
        return Commons.isLinkMom() ? "렛츠링크쌤_안내".tr() : "헬로맘대디_안내".tr();
    }
  }
}

enum MoveMenuType {
  menu_0,
  menu_1,
  menu_2,
  menu_3,
}

extension MoveMenuTypeEx on MoveMenuType {
  int get value {
    switch (this) {
      case MoveMenuType.menu_0:
        return 0;
      case MoveMenuType.menu_1:
        return 1;
      case MoveMenuType.menu_2:
        return 2;
      case MoveMenuType.menu_3:
        return 3;
      default:
        return 0;
    }
  }

  String get name {
    switch (this) {
      case MoveMenuType.menu_0:
        return Commons.isLinkMom() ? "맘대디찾기".tr() : "링크쌤찾기".tr();
      case MoveMenuType.menu_1:
        return Commons.isLinkMom() ? "근무관리".tr() : "돌봄관리".tr();
      case MoveMenuType.menu_2:
        return Commons.isLinkMom() ? "인증센터".tr() : "추가돌봄신청".tr();
      case MoveMenuType.menu_3:
        return "홈으로".tr();
      default:
        return Commons.isLinkMom() ? "맘대디찾기".tr() : "링크쌤찾기".tr();
    }
  }
}

enum Selecter { repeat, view }
