import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/data/chat_send_data.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/view/main/chat/chat_room_page.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';

import '../main.dart';
import '../utils/custom_dailog/custom_dailog.dart';
import 'enum_manager.dart';

class MqttManager extends ChangeNotifier {
  static const String KEY_ON = 'ON';
  static const String KEY_OFF = 'OFF';
  static const String KEY_READ = 'READ';
  static const String ID = 'LINKMOM';
  static const String PW = 'mqttfldzmaka^^';
  final String KEY_TOPIC = 'linkmom/user/'; //'/*"linkmom"*/;

  MqttServerClient client = MqttServerClient.withPort(ApiEndPoint.getBaseChatMqttUrl(), '60', 1883);

  setClient() {
    client = MqttServerClient.withPort(ApiEndPoint.getBaseChatMqttUrl(), '60', 1883);
  }

  MqttClientPayloadBuilder builder = MqttClientPayloadBuilder();

  var _mqttController = StreamController<AsyncSnapshot>();

  get mqttController => _mqttController;

  Stream<AsyncSnapshot> mqttStream() {
    _mqttController = StreamController<AsyncSnapshot>();
    return _mqttController.stream;
  }

  String topic = '';

  Function? onMsg;

  ///연결 여부
  get isConnect => _isConnect;
  bool _isConnect = false;

  ///백그라운드 여부
  get isPaused => _isPaused;
  bool _isPaused = false;

  ///콜백 플래그
  get action => _action;
  MqttType _action = MqttType.onDisconnected;

  ///MQTT DATA
  get payload => _payload;
  String _payload = '';

  ///MQTT DATA
  get payloadData => _payloadData;
  dynamic _payloadData;

  ///isAction (매칭 데이터 중 내가 내꺼 수신 처리시 사용)
  get isAction => _isAction;
  bool _isAction = false;

  void setAction(MqttType? action, {String payload = ''}) {
    _action = action!;
    _payload = payload;
    log.d('『GGUMBI』>>> MQTT setAction : _isAction: $_isAction, ${payload.isEmpty}, data: $payload, onMsg: $onMsg,  : $getReceiver, getChatroomId: $getChatroomId, getCurrentRouteName: $getCurrentRouteName <<< ');
    if (payload.isEmpty) {
      return;
    }
    if (onMsg != null) {
      log.d('『GGUMBI』>>> MQTT connect 1 : !_payload.contains(KEY_ON): $_payload, ${getPublishOn()}, ${getPublishOff()}, ${getPublishRoomInReceiver()} ${_payload.contains(getPublishOn())}, ${_payload.contains(getPublishOff())}, ${_payload.contains(getPublishRoomInReceiver())} <<< ');
      if (!StringUtils.validateString(payload) || _payload.contains(getPublishOn()) || _payload.contains(getPublishOff()) || _payload.contains(getPublishRoomInReceiver()) || _payload.contains(getPublishRoomOutReceiver()) /* || _isAction*/) {
        log.d('『GGUMBI』>>> MQTT connect 2 : !_payload.contains(KEY_ON): $_payload, ${getPublishOn()}, ${getPublishOff()}, ${getPublishRoomInReceiver()} ${_payload.contains(getPublishOn())}, ${_payload.contains(getPublishOff())}, ${_payload.contains(getPublishRoomInReceiver())} <<< ');
      } else {
        log.d('『GGUMBI』>>> MQTT connect 3 : !_payload.contains(KEY_ON): $_payload, ${getPublishOn()}, ${getPublishOff()}, ${getPublishRoomInReceiver()} ${_payload.contains(getPublishOn())}, ${_payload.contains(getPublishOff())}, ${_payload.contains(getPublishRoomInReceiver())} <<< ');
        onMsg!(_action, _payload);
      }
    }
    notifyListeners();
  }

  Future<void> setScroll(ChatScrollItem data) async {
    _mqttController.sink.add(AsyncSnapshot.withData(ConnectionState.done, data));
  }

  Future<void> setAddData(String value, {bool isFcm = false}) async {
    try {
      log.d('『GGUMBI』>>> MQTT setAddData : data : $value');
      log.d('『GGUMBI』>>> MQTT setAddData : setting : $getReceiver, $getChatroomId, ${_mqttController.isClosed}, ${getPublishRoomIn()},  ${value.contains(getPublishOn())}, ${value.contains(getPublishOff())}, ${value.contains(getPublishRoomInReceiver())}<<< ');
      if (!_mqttController.isClosed) {
        ///채팅방 입장 읽음 처리 (상대방 채팅리스트에 추가 할때 마다 수신 처리)
        if (value.contains(getPublishRoomInReceiver())) {
          _mqttController.sink.add(AsyncSnapshot.withData(ConnectionState.done, value));
          log.d('『GGUMBI』>>> MQTT setAddData : add 00 : isFcm: $isFcm,  <<< ');

          ///채팅방 나가기
        } else if (value.contains(getPublishRoomOutReceiver())) {
          _mqttController.sink.add(AsyncSnapshot.withData(ConnectionState.none, value));
          log.d('『GGUMBI』>>> MQTT setAddData : add 000 : isFcm: $isFcm,  <<< ');
          // } else if (value.contains(getPublishOn()) ||  value.contains(getPublishOff())) {

        } else if (value.contains(getRefresh())) {
          //채팅방에 있는데 Off 된 경우, 핑거푸쉬로 수신시 채팅방 리스트를 재조회 한다. 2022/03/16
          if (isFcm) {
            _mqttController.sink.add(AsyncSnapshot.withData(ConnectionState.active, null));
            setPublishOn('isFcm 2');
          }
          // } else if (value.contains(getPublishOn())) {
          //   return null;

        } else if (value.contains(getPublishOff())) {
          return null;

          ///정상적인 메시지
        } else {
          ChatSendData data;
          log.d('『GGUMBI』>>> MQTT setAddData add 1 : isFcm: $isFcm, data:$value <<< ');
          if (isFcm) {
            _mqttController.sink.add(AsyncSnapshot.withData(ConnectionState.active, value));
            setPublishOn('isFcm 2');
            return null;
          } else {
            data = ChatSendData.fromJson(json.decode(payload));
          }
          log.d('『GGUMBI』>>> MQTT setAddData add 2 : data: $data,  <<< ');
          log.d('『GGUMBI』>>> MQTT setAddData add 3 : ${data.chattingroom == getChatroomId && receiver == data.receiver && id == data.sender} <<< ');
          log.d('『GGUMBI』>>> MQTT setAddData add 4 : ${data.chattingroom == getChatroomId && receiver == data.sender && id == data.receiver} <<< ');
          log.d('『GGUMBI』>>> MQTT setAddData add 5 :  ${data.chattingroom == getChatroomId && data.msgtype == ChatMsgType.matching && id == data.receiver} <<< ');
          log.d('『GGUMBI』>>> MQTT setAddData chattingroom : 1 : chattingroom: ${data.chattingroom == getChatroomId}, receiver: ${receiver == data.receiver}, sender: ${id == data.sender}, ${data.chattingroom}, $getChatroomId <<< ');
          log.d('『GGUMBI』>>> MQTT setAddData chattingroom : 2 : chattingroom: ${data.chattingroom == getChatroomId}, receiver: ${receiver == data.sender}, sender: ${id == data.receiver}, ${data.chattingroom}, $getChatroomId <<< ');
          log.d('『GGUMBI』>>> MQTT setAddData chattingroom : 3 : chattingroom: ${data.chattingroom == getChatroomId}, msgtype: ${data.msgtype == ChatMsgType.matching}, sender: ${id == data.receiver}, ${data.chattingroom}, $getChatroomId <<< ');
          log.d('『GGUMBI』>>> MQTT setAddData chattingroom : 4 : data : ${data.chattingroom}, ${data.receiver}, ${data.sender}, ${data.msgtype} <<< ');
          log.d('『GGUMBI』>>> MQTT setAddData chattingroom : 5 : currunt : $getChatroomId, $receiver, $id <<< ');
          _isAction = false;
          if (data.chattingroom == getChatroomId && receiver == data.receiver && id == data.sender || data.chattingroom == getChatroomId && receiver == data.sender && id == data.receiver || data.chattingroom == getChatroomId && data.msgtype == ChatMsgType.matching && id == data.receiver) {
            //매칭 데이터 수신시, 내가 보낸 메시지 인 경우 수신 처리
            log.d('『GGUMBI』>>> MQTT setAddData add 6 : ${data.chattingroom == getChatroomId}, $receiver, ${receiver == data.receiver}, ${id == data.sender}, ${data.chattingroom == getChatroomId}, ${receiver == data.sender}, ${id == data.receiver}, data: $data <<< ');
            _isAction = true;
            _mqttController.sink.add(AsyncSnapshot.withData(ConnectionState.active, value));
            log.d('『GGUMBI』>>> MQTT setAddData add 7 : ${data.chattingroom == getChatroomId}, $receiver, ${receiver == data.receiver}, ${id == data.sender}, ${data.chattingroom == getChatroomId}, ${receiver == data.sender}, ${id == data.receiver}, data: $data <<< ');
            log.d('『GGUMBI』>>> MQTT setAddData add 77: : ${_mqttController.onListen},  <<< ');
            log.d('『GGUMBI』>>> MQTT setAddData add 777: : ${_mqttController.isPaused}, ${_mqttController.hasListener}, ${_mqttController.isClosed} <<< ');
          }
        }
      }
    } catch (e) {
      log.e('『GGUMBI』>>> MQTT Exception >>>  setAddData : Stream Send ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  Future<void> remove() async {
    try {
      setPublishOff('remove');
      setPublishRoomOut();
      int reset = -1;
      setId(reset);
      setReceiver(reset);
      setChatroomId(reset);
      setCurrentRouteName('');
      await unSubscribe();
      await _mqttController.close();
    } catch (e) {
      log.e('『GGUMBI』>>> MQTT Exception >>>  remove : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  int id = auth.user.id;

  int get getId => id;

  setId(int id) {
    this.id = id;
  }

  int receiver = auth.user.id;

  int get getReceiver => receiver;

  setReceiver(int receiver) {
    this.receiver = receiver;
  }

  int chatroomId = auth.user.id;

  int get getChatroomId => chatroomId;

  setChatroomId(int? chatroomId) {
    if (chatroomId == null) {
      chatroomId = -1;
    }
    this.chatroomId = chatroomId;
  }

  String currentRouteName = '';

  String get getCurrentRouteName => currentRouteName;

  setCurrentRouteName(String routeName) {
    currentRouteName = routeName;
  }

  Future<MqttServerClient> connect({Function? onMsg}) async {
    log.d('『GGUMBI』>>> MQTT connect : onMsg: ${client.connectionStatus!.state}, $onMsg, ${'$KEY_TOPIC$id'}, ${this.onMsg} $id, ${auth.user.id}, unique: ${deviceInfo.unique} <<< ');
    if (this.onMsg == null) {
      this.onMsg = onMsg!;
    }

    if (auth.user.id > 0) {
      setId(auth.user.id);
    }

    topic = '$KEY_TOPIC$id';

    client.logging(on: Commons.isDebugMode);
    client.autoReconnect = true;
    client.keepAlivePeriod = 600; //10분후 데이터 전송 없더라도 ping request 해주므로 연결 유지시킴
    client.onConnected = onConnected;
    client.onDisconnected = onDisconnected;
    client.onUnsubscribed = onUnsubscribed;
    client.onSubscribed = onSubscribed;
    client.onSubscribeFail = onSubscribeFail;
    client.pongCallback = pong;

    String identifier = '${auth.user.id}_${auth.user.username}_${deviceInfo.unique}';
    if(deviceInfo.unique.isEmpty){
      identifier = '${auth.user.id}_${auth.user.username}_${DateTime.now().millisecond}';
    }
    log.d('『GGUMBI』>>> MQTT connect : identifier: $identifier,  <<< ');

    final connMess = MqttConnectMessage()
        .withClientIdentifier(identifier)
        .withWillTopic('$KEY_TOPIC$id')
        .withWillMessage(getWillMessage()) //종료시 -1로 해서 보낸다.
        .startClean() // Non persistent se
        .withWillQos(MqttQos.atLeastOnce);
    client.connectionMessage = connMess;

    try {
      await client.connect(ID, PW);
    } on NoConnectionException catch (e) {
      log.e('『GGUMBI』>>> MQTT Exception >>> connect : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      if (Commons.isDebugMode) showNormalDlg(message: "MQTT NoConnectionException \n$e");
      client.disconnect();
    } on SocketException catch (e) {
      log.e('『GGUMBI』>>> MQTT Exception >>> connect : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      if (Commons.isDebugMode) showNormalDlg(message: "MQTT SocketException \n$e");
      client.disconnect();
    }

    if (client.connectionStatus!.state == MqttConnectionState.connected) {
      log.i('『GGUMBI』>>> MQTT client connected : ${client.connectionStatus!.state}<<< ');
      _isConnect = true;
    } else {
      log.i('『GGUMBI』>>> MQTT client connection failed - disconnecting, status is : ${client.connectionStatus!.state}<<< ');
      _isConnect = false;
      client.disconnect();
    }

    client.updates!.listen((dynamic c) {
      final MqttPublishMessage message = c[0].payload;
      String payload = MqttPublishPayload.bytesToStringAsString(message.payload.message);
      payload = utf8.decode(payload.codeUnits);
      _payload = payload;
      _payloadData = payload;
      // ChatSendData data = ChatSendData.fromJson(json.decode(payload));
      // if (data?.chattingroom == getChatroomId) {
      // }
      log.i('『GGUMBI』>>> MQTT Response add 0 : payload: $payload, resTopic: ${c[0].topic}, myTopic: $topic, $getChatroomId, $getReceiver<<< ');
      if (!_payload.contains('ON')) {
        setAddData(_payload);
      }
      if (!_payload.contains('OFF')) {
        setAction(MqttType.onMsg, payload: _payload);
      }
      log.i('『GGUMBI』>>> MQTT Response add 8 : payload: $payload, resTopic: ${c[0].topic}, myTopic: $topic, $getChatroomId, $getReceiver<<< ');
      // log.d('『GGUMBI』>>> MQTT Response add 88: : ${_mqttController.onListen},  <<< ');
      // log.d('『GGUMBI』>>> MQTT Response add 888: : ${_mqttController.isPaused}, ${_mqttController.hasListener}, ${_mqttController.isClosed} <<< ');
    });

    return client;
  }

  ///메시지 전송
  void sendMsg(String msg, {bool isRead = false}) {
    builder.clear();
    // builder.addString(msg);
    builder.addUTF8String(msg);
    log.d('『GGUMBI』>>> MQTT sendMsg : client: $client, topic: $topic, msg: $msg, ${mqttManager.KEY_TOPIC}$receiver, isRead: $isRead, isConnect: $isConnect, ${client.connectionStatus!.state} <<< ');

    try {
      if (client.connectionStatus!.state == MqttConnectionState.connected) {
        if (isRead) {
          client.publishMessage('${mqttManager.KEY_TOPIC}$receiver', MqttQos.atLeastOnce, builder.payload!);
        } else {
          client.publishMessage(topic, MqttQos.atLeastOnce, builder.payload!);
        }
      }
    } catch (e) {
      log.e('『GGUMBI』>>> MQTT Exception >>> sendMsg : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  Future<void> setSubscribe(String topic) async {
    this.topic = topic;
    log.d('『GGUMBI』>>> MQTT setSubscribe : : ${this.topic},  $topic, $isConnect, ${client.connectionStatus!.state},<<< ');
    if (client.connectionStatus!.state == MqttConnectionState.disconnected) {
      await connect();
    } else {
      client.subscribe(topic, MqttQos.atLeastOnce);
    }
  }

  Future<void> unSubscribe() async {
    log.d('『GGUMBI』>>> MQTT unSubscribe : : $topic, $isConnect, $client,<<< ');
    client.unsubscribe(topic);
  }

  ///상대방이 채팅방 입장시
  String getPublishRoomInReceiver() {
    return '$receiver|IN|$chatroomId';
  }

  ///내가 채팅방 입장시
  String getPublishRoomIn() {
    return '$id|IN|$chatroomId';
  }

  ///내가 채팅방 입장시 상대방에게 알리기 혹은 메시지 보낼때 ask 처리
  void setPublishRoomIn() {
    _isPaused = true;
    sendMsg(getPublishRoomIn(), isRead: true);
  }

  ///상대방이 채팅방 나갈때
  String getPublishRoomOutReceiver() {
    return '$receiver|OUT|$chatroomId';
  }

  ///내가 채팅방 나갈때
  String getPublishRoomOut() {
    return '$id|OUT|$chatroomId';
  }

  ///내가 채팅방 나갈시 상대방에게 알리기 혹은 메시지 보낼때 ask 처리
  void setPublishRoomOut() {
    // setPublishOff();
    _isPaused = false;
    sendMsg(getPublishRoomOut(), isRead: true);
  }

  ///linkmom|user|ON|chatroom > linkmom|4|READ|1
  String getWillMessage() {
    return '$id|OFF|-1';
  }

  ///채팅방 화면 IN (init, resume)
  Future<void> setPublishOn(String value) async {
    log.d('『GGUMBI』>>> MQTT setPublishOn setPublishOn : $getReceiver, getChatroomId: $getChatroomId, getCurrentRouteName: $getCurrentRouteName ,  value: $value, isConnect: $isConnect, ${client.connectionStatus!.state}, getPublishOn(): $getPublishOn() <<< ');
    if (client.connectionStatus!.state == MqttConnectionState.disconnected) {
      await connect();
      sendMsg(getPublishOn());
    } else {
      sendMsg(getPublishOn());
    }
  }

  ///linkmom|user|ON|chatroom > linkmom|4|ON|1
  String getPublishOn() {
    return '$id|ON|$chatroomId';
  }

  ///채팅방 화면 OUT (dispose, pause)
  void setPublishOff(String value) {
    log.d('『GGUMBI』>>> MQTT setPublishOff getReceiver : $getReceiver, getChatroomId: $getChatroomId, getCurrentRouteName: $getCurrentRouteName, $value, isConnect: $isConnect, ${client.connectionStatus!.state}, getPublishOff(): ${getPublishOff()} <<< ');
    sendMsg(getPublishOff());
  }

  ///linkmom|user|OFF|chatroom > linkmom|4|OFF|1
  String getPublishOff() {
    return '$id|OFF|$chatroomId';
  }

  ///채팅방에 있고, 핑거푸시로 온 경우 채팅방을 갱신 한다.
  String getRefresh() {
    return 'refresh';
  }

  /// connection succeeded
  void onConnected() {
    setSubscribe(topic);
    setPublishOff('onConnected');
    _isConnect = true;
    log.i('『GGUMBI』>>> MQTT onConnected ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
    setAction(MqttType.onConnected);
  }

  ///unconnected
  void onDisconnected() {
    _isConnect = false;
    log.i('『GGUMBI』>>> MQTT onDisconnected ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
    setAction(MqttType.onDisconnected);
    // if (client.connectionStatus!.disconnectionOrigin ==
    //     MqttDisconnectionOrigin.solicited) {
    //   print('EXAMPLE::OnDisconnected callback is solicited, this is correct');
    // }
    // exit(-1);
  }

  /// subscribe to topic succeeded
  void onSubscribed(String topic) {
    log.i('『GGUMBI』>>> MQTT onSubscribed : topic: $topic,  <<< ');
    setAction(MqttType.onSubscribed);
  }

  /// subscribe to topic failed
  void onSubscribeFail(String topic) {
    log.i('『GGUMBI』>>> MQTT onSubscribeFail : topic: $topic,  <<< ');
    setAction(MqttType.onSubscribeFail);
  }

  /// unsubscribe succeeded
  void onUnsubscribed(String? topic) {
    log.i('『GGUMBI』>>> MQTT onUnsubscribed : topic: $topic,  <<< ');
    setAction(MqttType.onUnsubscribed);
  }

  /// PING response received
  void pong() {
    log.i('『GGUMBI』>>> MQTT pong ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
    setAction(MqttType.pong);
  }
}
