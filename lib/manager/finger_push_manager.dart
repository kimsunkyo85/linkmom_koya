import 'dart:convert';
import 'dart:io';

import 'package:fingerpush_plugin/fingerpush_plugin.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/models/data/chat_send_data.dart';
import 'package:linkmom/data/push/model/fingerpush.dart';
import 'package:linkmom/data/push/model/linkmompush.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';

import '../main.dart';
import 'enum_utils.dart';

class FingerPushManager {
  // FingerPushManager({Function callBack});

  //핑거푸시 호출 결과
  //기기등록
  String strRegisterDeviceResult = '기기 토큰이 등록 후 결과 메시지가 여기에 표시됩니다.';

  //푸시 내용
  String strPushContentResult = '푸시 메시지가 수신되면 여기에 나타납니다.';

  //푸시 체크
  String strPushCheckResult = '푸시 메시지가 수신되면 여기에 나타납니다.';

  //태그 등록 설정
  String strRegTagResult = 'Tag를 설정할 수 있으며, 결과 메시지가 여기에 표시됩니다.';
  final TextEditingController regTagTextController = new TextEditingController();

  //태그 지우기
  String strRemoveTagResult = '입력한 Tag를 삭제할 수 있으며, 결과 메시지가 여기에 표시됩니다.';
  final TextEditingController removeTagTextController = new TextEditingController();

  //모든 태그 지우기
  String strRemoveAllTagResult = '모든 Tag를 삭제할 수 있으며, 결과 메시지가 여기에 표시됩니다.';

  //앱의 모든 태그 가져오기
  String strGetAllTagResult = '앱 전체 Tag를 가져와서 여기에 표시합니다.';

  //기기 태그 리스트 가져오기
  String strGetDeviceTagListResult = '디바이스 Tag를 가져와서 여기에 표시합니다.';

  //identity 설정
  String strRegIdResult = 'Identity를 설정할 수 있으며, 결과 메시지가 여기에 표시됩니다.';
  final TextEditingController regIdTextController = new TextEditingController();

  //uniqIdentity 설정
  String strRegUniqIdResult = 'Uniq Identity를 설정할 수 있으며, 결과 메시지가 여기에 표시됩니다.';
  final TextEditingController regUniqIdTextController = new TextEditingController();

  //Identity 삭제
  String strRemoveIdResult = '설정된 Identity를 삭제하며, 결과 메시지가 여기에 표시됩니다.';

  //앱정보 확인
  String strGetAppReportResult = '앱이 설치된 정보가 표시됩니다.';

  //푸시설정 가져오기
  String strPushInfoResult = '디바이스 타입과 푸시 수신여부, identity를 확인할 수 있습니다.';

  //setEnable
  String strSetEnableResult = '푸시 메시지 수신 여부를 설정할 수 있으며, true/false만 가능합니다.';

  //setAdEnable
  String strSetAdEnableResult = '광고 푸시 메시지 수신 여부를 설정할 수 있으며, true/false만 가능합니다.';

  //푸시 리스트 가져오기
  String strGetPushListResult = '푸시리스트를 표시합니다.';

  ///
  String _prefix = 'data.';

  ///생성
  Future<void> initFingerPush({Function? callBack}) async {
    // 리모트 푸시 메시지 수신 정보
    // 사용자가 푸시를 탭하였을 경우에만 반응합니다.
    FingerPush.shared.receivedNotificationHandler((notificationValue) {
      log.d('『GGUMBI』>>> FingerPush initFingerPush : notificationValue: ${notificationValue.length}, ${notificationValue == '{}'} <<< ');
      if (notificationValue.isNotEmpty && notificationValue != '{}') {
        Map notificationValueMap = json.decode(notificationValue);
        log.d('『GGUMBI』>>> FingerPush receivedNotificationHandler : notificationValue: $notificationValue, notificationValueMap: $notificationValueMap <<< ');
        // pushFirebase.getToken();
        //푸시 페이로드가 핑거푸시인지 확인
        bool isFingerPushNotication = FingerPush.isFingerPushPayLoad(notificationValueMap);
        if (!isFingerPushNotication) {
          //FingerPush 메시지가 아닐 경우 처리
          if (callBack != null) {
            if (Commons.isLinkmomPush(notificationValueMap)) {
              LinkmomPushData data = LinkmomPushData.fromJson(notificationValueMap);
              callBack(data, false, true, false);
            } else {
              try {
                ChatSendData chatSendData = ChatSendData.fromMap(notificationValueMap['payload']);
                LinkmomPushData data = LinkmomPushData(chatSendData: chatSendData, pagename: NotificationType.chatting.value); //chattingtype: data.chattingtype,
                callBack(data, false, false, false);
              } catch (e) {
                log.e('『GGUMBI』 FingerPush Exception >>> receivedNotificationHandler : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
              }
            }
          }
          return;
        } else {
          try {
            LinkmomPushData data = LinkmomPushData.fromJson(notificationValueMap, fingerPushAlias: Platform.isIOS ? '' : _prefix);
            if (callBack != null) callBack(data, false, false, false);
            return;
          } catch (e) {
            log.e('『GGUMBI』 FingerPush Exception >>> receivedNotificationHandler: isFingerPush <<< \n $e');
          }
        }

        //리모트 푸시 읽음 처리
        FingerPush.shared.checkPush(notificationValueMap).then((value) {
          strPushCheckResult = value;
          log.d('『GGUMBI』>>> FingerPush receivedNotificationHandler : strPushCheckResult: $strPushCheckResult,  <<< ');
        }).catchError((onError) {
          strPushCheckResult = onError.toString();
          log.d('『GGUMBI』>>> FingerPush receivedNotificationHandler : strPushCheckResult error : $strPushCheckResult,  <<< ');
        });

        // if (callBack != null) {
        //   log.d({
        //     '|| TITLE    ': '--------------- FingerPush ---------------',
        //     '|| DATA     ': notificationValueMap,
        //   });
        //   callBack(notificationValueMap, false, false, false);
        // }

        //푸시 페이로드드에 포함되어 않은 리모트 푸시 정보
        //일반적으로는 사용하지 않아도 무방
        FingerPush.shared.getPushContent(notificationValueMap).then((value) {
          strPushContentResult = value;
          // Map data = json.decode(value);
          // log.d('『GGUMBI』>>> initFingerPush : data: ${data["content"]},  <<< ');
          FingerPushData data = FingerPushData.fromJson(json.decode(value));
          log.d('『GGUMBI』>>> FingerPush receivedNotificationHandler : data: $data,  <<< ');
          log.d('『GGUMBI』>>> FingerPush receivedNotificationHandler : strPushContentResult : $strPushContentResult,  <<< ');

          // if (callBack != null) {
          //   callBack(data, true, false);
          // }
        }).catchError((onError) {
          strPushContentResult = onError.toString();
          log.d('『GGUMBI』>>> FingerPush receivedNotificationHandler : strPushContentResult error : $strPushContentResult,  <<< ');
        });
      }
    });

    // ios message 수신시만 처리...그외 나머지 receivedNotificationHandler 처리함.
    FingerPush.shared.willPresentNotificationHandler((notificationValue) {
      Map notificationValueMap = json.decode(notificationValue);
      log.d('『GGUMBI』>>> FingerPush willPresentNotificationHandler : notificationValue: $notificationValue, notificationValueMap: $notificationValueMap <<< ');
      // pushFirebase.getToken();
      //푸시 페이로드가 핑거푸시인지 확인
      bool isFingerPushNotication = FingerPush.isFingerPushPayLoad(notificationValueMap);
      if (isFingerPushNotication) {
        //data.chat  //  chat
        // 361,이하준 링크쌤과 홍지희 맘대디가 매칭 되었습니다.,2022-01-20 17:35
        // {
        //   "chattingroom": 361,
        //   "msg": "이하준 링크쌤과 홍지희 맘대디가 매칭 되었습니다. 맘대디의 결제 완료 후 돌봄이 확정됩니다. (결제 시한 : 2022-01-20 18:35까지)",
        // 	"sendtime": "2022-01-20 17:35:55.145173",
        // }
        ChatSendData chatSendData = ChatSendData();
        String? chatData;
        try {
          //실제 핑거푸쉬에서 오는 데이터는 pagename, chattingroom, sendtime 세팅
          //메시지 내용은 -> message.data['data.message'] Url.decode 이후, +는 ' ' 치환해서 사용한다.
          //그외 나머지 데이터 들은 pagename에 맞게끔 예외처리로 사용할것!
          //Uri.decodeFull("%EC%9D%B4%ED%95%98%EC%A4%80+%EB%A7%81%ED%81%AC%EB%A7%98%EA%B3%BC+%ED%99%8D%EC%A7%80%ED%9D%AC+%EB%A7%98%EB%8C%80%EB%94%94%EA%B0%80+%EB%A7%A4%EC%B9%AD+%EB%90%98%EC%97%88%EC%8A%B5%EB%8B%88%EB%8B%A4.+%EB%A7%98%EB%8C%80%EB%94%94%EC%9D%98+%EA%B2%B0%EC%A0%9C+%EC%99%84%EB%A3%8C+%ED%9B%84+%EB%8F%8C%EB%B4%84%EC%9D%B4+%ED%99%95%EC%A0%95%EB%90%A9%EB%8B%88%EB%8B%A4.+%28%EA%B2%B0%EC%A0%9C+%EC%8B%9C%ED%95%9C+%3A+2022-01-20+18%3A35%EA%B9%8C%EC%A7%80%29").replaceAll('+', ' ');
          if (Platform.isIOS) {
            chatData = notificationValueMap['chat'];
            // log.d('『GGUMBI』>>> initFirebasePush : chatData: $chatData, ${notificationValueMap['chat']} <<< ');
            if (chatData != null) {
              ChatFPData chatFPData = ChatFPData.fromJson(json.decode(chatData));
              chatSendData.pagename = chatFPData.pagename;
              chatSendData.chattingroom = chatFPData.chattingroom;
              chatSendData.sendtime = chatFPData.sendtime;
              chatSendData.sendtimestamp = chatFPData.sendtimestamp;
            }
            chatSendData.chattingtype = ChattingType.care.value;
            chatSendData.msg = notificationValueMap['aps']['alert']['body'];
          }

          log.d('『GGUMBI』>>> FingerPush initFingerPush : notificationValueMap : $chatSendData <<< ');

          if (EnumUtils.getNotificationType(chatSendData.pagename).isChat) {
            pushData.setData(LinkmomPushData(
              chatSendData: chatSendData,
              pagename: chatSendData.pagename,
            ));
            log.d('『GGUMBI』>>> FingerPush initFirebasePush : data: ${pushData.pushData},  <<< ');
            log.d('『GGUMBI』>>> FingerPush initFirebasePush : data: ${pushData.chatSendData},  <<< ');
          }
        } catch (e) {
          log.e('『GGUMBI』 FingerPush Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
        }
        if (callBack != null) {
          callBack(pushData, true, true, true);
        }
        return;
      } else {
        //FingerPush 메시지가 아닐 경우 처리
        if (callBack != null) {
          try {
            if (Commons.isLinkmomPush(notificationValueMap)) {
              LinkmomPushData data = LinkmomPushData.fromJson(notificationValueMap);
              pushData.setData(LinkmomPushData(chatSendData: data.chatSendData, chattingtype: data.chattingtype, pagename: data.pagename, msgData: data.msgData));
              callBack(data, true, true, true);
            } else {
              ChatSendData chatSendData = ChatSendData.fromMap(notificationValueMap['payload']);
              LinkmomPushData data = LinkmomPushData(chatSendData: chatSendData, pagename: NotificationType.chatting.value);
              pushData.setData(LinkmomPushData(chatSendData: data.chatSendData, chattingtype: data.chattingtype, pagename: data.pagename));
              callBack(data, true, false, true);
            }
          } catch (e) {
            log.e('『GGUMBI』 FingerPush Exception >>> willPresentNotificationHandler : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
          }
        }
        return;
      }
    });

    registerDevice();
  }

  //기기등록
  Future<void> registerDevice() async {
    log.d('『GGUMBI』>>> FingerPush registerDevice : kReleaseMode: $kReleaseMode, ${Commons.isDebugMode}, ${ApiEndPoint.getBaseUrl()} <<< ');
    if (kReleaseMode) {
      //배포용
      FingerPush.shared.setAppKey("M0V09BW2WLQZ");
      FingerPush.shared.setAppSecret("x2D4GAKPPQouAOVYyUINxfqbG4nyuvPd");
    } else {
      if (!Commons.isDebugMode) {
        //배포용
        FingerPush.shared.setAppKey("M0V09BW2WLQZ");
        FingerPush.shared.setAppSecret("x2D4GAKPPQouAOVYyUINxfqbG4nyuvPd");
      } else {
        //개발용
        FingerPush.shared.setAppKey("NV0MIEN56A19");
        FingerPush.shared.setAppSecret("wfe4GQ3c1v8Xnidt1Tf9AfkISlPRqbfi");
      }
    }
    if (Platform.isIOS) {
      FingerPush.shared.setDeviceForIOS();
      //기기 등록 후 결과 IOS, Android 단말기는 하단에서 핸들러된다.
      FingerPush.shared.setDeviceHandler((result) async {
        strRegisterDeviceResult = result;
        log.d('『GGUMBI』>>> FingerPush registerDevice : ios: $result,  <<< ');

        ///2021/11/09 로그인시 해당 id
        await FingerPush.shared.setIdentity(auth.user.username);
      });
    } else if (Platform.isAndroid) {
      await FingerPush.shared.setDevice.then((result) async {
        strRegisterDeviceResult = result;
        log.d('『GGUMBI』>>> FingerPush registerDevice : Android: $result,  <<< ');
        await FingerPush.shared.setIdentity(auth.user.username);
      }).catchError((e) {
        strRegisterDeviceResult = e.toString();
        //이미지 등록된 PlatformException(504, 이미 등록된 토큰입니다., null, null)
        if (strRegisterDeviceResult.contains('504')) {
          FingerPush.shared.setIdentity(auth.user.username);
          log.d('『GGUMBI』>>> FingerPush registerDevice : Android 1 : $strRegisterDeviceResult,  <<< ');
        }
        log.d('『GGUMBI』>>> FingerPush registerDevice : Android 2 : $strRegisterDeviceResult,  <<< ');
      });
    }
  }

  //태그 등록 설정
  void regTag(String str) {
    FingerPush.shared.setTag(str).then((value) {
      strRegTagResult = value;
    }).catchError((onError) {
      strRegTagResult = onError.toString();
    });
  }

  //태그 지우기
  void removeTag(String str) {
    FingerPush.shared.removeTag(str).then((value) {
      strRemoveTagResult = value;
    }).catchError((onError) {
      strRemoveTagResult = onError.toString();
    });
  }

  //모든 태그 지우기
  void removeAllTag() {
    FingerPush.shared.removeAllTag().then((value) {
      strRemoveAllTagResult = value;
    }).catchError((onError) {
      strRemoveAllTagResult = onError.toString();
    });
  }

  //앱의 모든 태그 가져오기
  void getAllTag() {
    FingerPush.shared.getAllTag().then((value) {
      strGetAllTagResult = value;
    }).catchError((onError) {
      strGetAllTagResult = onError.toString();
    });
  }

  //기기 태그 리스트 가져오기
  void getDeviceTagList() {
    FingerPush.shared.getTag().then((value) {
      strGetDeviceTagListResult = value;
    }).catchError((onError) {
      strGetDeviceTagListResult = onError.toString();
    });
  }

  //identity 설정
  void regId(String str) {
    FingerPush.shared.setIdentity(str).then((value) {
      strRegIdResult = value;
    }).catchError((onError) {
      strRegIdResult = onError.toString();
    });
  }

  //uniqIdentity 설정
  void regUniqId(String str) {
    FingerPush.shared.setUniqueIdentity(str, true, "").then((value) {
      strRegUniqIdResult = value;
    }).catchError((onError) {
      strRegUniqIdResult = onError.toString();
    });
  }

  //Identity 삭제
  void removeId() {
    FingerPush.shared.removeIdentity().then((value) {
      strRemoveIdResult = value;
    }).catchError((onError) {
      strRemoveIdResult = onError.toString();
    });
  }

  //앱정보 확인
  void getAppReport() {
    FingerPush.shared.getAppReport().then((value) {
      strGetAppReportResult = value;
    }).catchError((onError) {
      strGetAppReportResult = onError.toString();
    });
  }

  //푸시설정 가져오기
  void getPushInfo() {
    FingerPush.shared.getDeviceInfo().then((value) {
      strPushInfoResult = value;
    }).catchError((onError) {
      strPushInfoResult = onError.toString();
    });
  }

  //setEnable
  void setEnableOn() {
    FingerPush.shared.setPushAlive(true).then((value) {
      strSetEnableResult = value;
    }).catchError((onError) {
      strSetEnableResult = onError.toString();
    });
  }

  void setEnableOff() {
    FingerPush.shared.setPushAlive(false).then((value) {
      strSetEnableResult = value;
    }).catchError((onError) {
      strSetEnableResult = onError.toString();
    });
  }

  //setAdEnable
  void setAdEnableOn() {
    FingerPush.shared.setAdvertisePushEnable(true).then((value) {
      strSetAdEnableResult = value;
    }).catchError((onError) {
      strSetAdEnableResult = onError.toString();
    });
  }

  void setAdEnableOff() {
    FingerPush.shared.setAdvertisePushEnable(false).then((value) {
      strSetAdEnableResult = value;
    }).catchError((onError) {
      strSetAdEnableResult = onError.toString();
    });
  }

  //푸시 리스트 가져오기
  void getPushList() {
    FingerPush.shared.getPushList().then((value) {
      strGetPushListResult = value;
    }).catchError((onError) {
      strGetPushListResult = onError.toString();
    });
  }
}
