import 'package:flutter/cupertino.dart';

import 'enum_manager.dart';

class EnumUtils {
  static int getCareTypeIndex(String name) {
    if (name.isEmpty) return 0;
    return CareType.values.lastWhere((e) => e.name == name).index;
  }

  static String getCareTypeName(int index) {
    return CareType.values.lastWhere((e) => e.index == index).name;
  }

  static CareType getCareType({String? name, int index = 0}) {
    return CareType.values.lastWhere((e) => name != null && name.isNotEmpty ? e.name == name : e.index == index);
  }

  static int getMatchingStatusIndex(String name) {
    if (name.isEmpty) return 0;
    return MatchingStatus.values.lastWhere((e) => e.name == name).value;
  }

  static String getMatchingStatusName(int index) {
    return MatchingStatus.values.lastWhere((e) => e.value == index).name;
  }

  static String getMatchingStatusNameV(int value) {
    return MatchingStatus.values.lastWhere((e) => e.value == value).name;
  }

  static MatchingStatus getMatchingStatus({String? name, int index = 0}) {
    return MatchingStatus.values.lastWhere((e) => name != null && name.isNotEmpty ? e.name == name : e.value == index);
  }

  static bool isBookingStatus(int index) {
    return index <= BookingStatus.expired.value;
  }

  static int getBookingStatusIndex(String name) {
    if (name.isEmpty) return 0;
    return BookingStatus.values.lastWhere((e) => e.name == name || name.contains(e.name), orElse: () => BookingStatus.inprogress).value;
  }

  static String getBookingStatusName(int index) {
    return BookingStatus.values.lastWhere((e) => e.value == index).name;
  }

  static BookingStatus getBookingStatus({String? name, int value = 0}) {
    return BookingStatus.values.lastWhere((e) => name != null && name.isNotEmpty ? e.name == name || name.contains(e.name) : e.value == (value), orElse: () => BookingStatus.inprogress);
  }

  static int getCareStatusIndex(String name) {
    if (name.isEmpty) return 0;
    return CareStatus.values.lastWhere((e) => e.name == name).value;
  }

  static String getCareStatusName(int index) {
    return CareStatus.values.lastWhere((e) => e.value == index).name;
  }

  static CareStatus getCareStatus({String? name, int index = 0}) {
    return CareStatus.values.lastWhere((e) => name != null && name.isNotEmpty ? e.name == name : e.value == index, orElse: () => CareStatus.none);
  }

  static int getDrugTypeIndex(String name) {
    if (name.isEmpty) return 0;
    return DrugType.values.lastWhere((e) => e.name == name).index;
  }

  static String getDrugTypeName(int index) {
    return DrugType.values.lastWhere((e) => e.index == index).name;
  }

  static DrugType getDrugType({String? name, int index = 0}) {
    return DrugType.values.lastWhere((e) => name != null && name.isNotEmpty ? e.name == name : e.index == index);
  }

  static int getServiceTypeIndex(String name) {
    if (name.isEmpty) return 0;
    return ServiceType.values.lastWhere((e) => e.string == name).value;
  }

  static String getServiceTypeName(int index) {
    return ServiceType.values.lastWhere((e) => e.value == index).string;
  }

  static ServiceType getServiceType({String? name, int index = 0}) {
    return ServiceType.values.lastWhere((e) => name != null && name.isNotEmpty ? e.string == name : e.value == index);
  }

  static int getPayStatusIndex(String name) {
    if (name.isEmpty) return 0;
    return PayStatus.values.lastWhere((e) => e.name == name).value;
  }

  static String getPayStatusName(int index) {
    return PayStatus.values.lastWhere((e) => e.value == index).name;
  }

  static PayStatus getPayStatus({String? name, int index = 0}) {
    return PayStatus.values.lastWhere(((e) => name != null && name.isNotEmpty ? e.name.contains(name) : e.value == index), orElse: () {
      switch (name) {
        case '돌봄취소(보상) 예정':
          return PayStatus.cancelReady;
        case '돌봄취소(보상) 완료':
          return PayStatus.cancel;
        case '돌봄취소(벌점부과) 예정':
          return PayStatus.penaltyReady;
        case '돌봄취소(벌점부과) 완료':
          return PayStatus.penalty;
        default:
          return PayStatus.cancel;
      }
    });
  }

  static int getNotifyReasonIndex(String name) {
    if (name.isEmpty) return 0;
    return NotifyReason.values.lastWhere((e) => e.reason == name).value;
  }

  static String getNotifyReasonReason(int value) {
    return NotifyReason.values.lastWhere((e) => e.value == value).reason;
  }

  static NotifyReason getNotifyReason({int value = 0, int index = 0}) {
    return NotifyReason.values.lastWhere((e) => index != 0 ? e.index == index : e.value == value);
  }

  static Category getCategory({String? name, int index = 0}) {
    return Category.values.lastWhere((e) => name != null && name.isNotEmpty ? e.name == name : e.index == index);
  }

  static ShareStatus getShareStatus({String? name, int index = 0}) {
    return ShareStatus.values.lastWhere((e) => name != null && name.isNotEmpty ? e.name == name : e.index == index);
  }

  static NotificationType getNotificationType(int value) {
    return NotificationType.values.lastWhere((e) => e.value == value);
  }

  static bool getNotificationTypeMsgData(int value) {
    return NotificationType.values.lastWhere((e) => e.value == value).msgParse;
  }

  static PayType getPayType(int value) {
    return PayType.values.lastWhere((e) => e.value == value, orElse: () => PayType.pay);
  }

  static PaymentReason getPaymentReason(int value) {
    return PaymentReason.values.lastWhere((e) => e.value == value, orElse: () => PaymentReason.add);
  }

  static Terms getTerms(int value) {
    return Terms.values.lastWhere((e) => e.value == value, orElse: () => Terms.privacy);
  }

  static PayType getPayTypeString(String payMethod, String pgProvider) {
    try {
      PayType method = PayType.values.lastWhere((e) => e.payMethod == payMethod);
      PayType pg = PayType.values.lastWhere((e) => e.payPg == pgProvider);
      //uplus 이고, 카드이면 신용카드
      if (method.payMethod == PayType.card.payMethod && pg.payPg == PayType.card.payPg) {
        return PayType.card;
      } else if (method.payMethod == PayType.card.payMethod && pg.payPg == PayType.card.payPg) {
        return PayType.pay;
      } else if (method.payMethod == PayType.bank.payMethod && pg.payPg == PayType.bank.payPg) {
        return PayType.bank;
      } else if (method.payMethod == PayType.card.payMethod && pg.payPg == PayType.pay.payPg) {
        return PayType.card;
      } else if (method.payMethod == PayType.point.payMethod) {
        return PayType.pay;
      } else {
        return PayType.free;
      }
    } catch (e) {
      return PayType.card;
    }
  }

  static CancelType getCancelSelect(int value) {
    return CancelType.values.lastWhere((e) => e.value == value);
  }

  ///취소사유 측(맘대디, 링크쌤) 10~29)
  static String getCancelSelectUserType(int value) {
    return CancelType.values.lastWhere((e) => e.value == value).userType;
  }

  ///취소사유 측(맘대디 - false, 링크쌤 - true) 10~29)
  static bool getCancelSelectUserTypeBool(int? value) {
    if (value == null) {
      return false;
    }
    return CancelType.values.lastWhere((e) => e.value == value).isLinkMom;
  }

  ///취소사유 측(맘대디, 링크쌤) 10~29)
  static Color getCancelSelectUserTypeColor(int value) {
    return CancelType.values.lastWhere((e) => e.value == value).color;
  }

  ///취소사유 취소내역이나 환불에서 보는 경우 1-맘대디, 2-링크쌤
  static CancelUserType getCancelUserType(int value) {
    return CancelUserType.values.lastWhere((e) => e.value == value);
  }

  ///취소사유 취소내역이나 환불에서 보는 경우 1-맘대디, 2-링크쌤
  static String getCancelUserTypeString(int value) {
    return CancelUserType.values.lastWhere((e) => e.value == value).string;
  }

  ///취소사유 취소내역이나 환불에서 보는 경우 1-맘대디, 2-링크쌤
  static Color getCancelUserTypeColor(int value) {
    return CancelUserType.values.lastWhere((e) => e.value == value).color;
  }

  ///남아,여아(남자, 여자)
  static GenderType getGender(int value) {
    return GenderType.values.lastWhere((e) => e.value == value);
  }

  ///아이성향
  static ChildCharacterType getChildCharacter(int value) {
    return ChildCharacterType.values.lastWhere((e) => e.index == value);
  }

  ///아이성향
  static ChildCharacterType getChildCharacterString(String value) {
    return ChildCharacterType.values.lastWhere((e) => e.string == value);
  }

  ///채팅 타입, 링크쌤0, 맘대디1, 일반2
  static ChatTabType getChatTabType(int value) {
    return ChatTabType.values.lastWhere((e) => e.value == value);
  }

  static int getDrugTimeTypeIndex(String name) {
    if (name.isEmpty) return 0;
    return DrugTimeType.values.lastWhere((e) => e.name == name).index;
  }

  static String getDrugTimeTypeName(int index) {
    return DrugTimeType.values.lastWhere((e) => e.index == index).name;
  }

  static DrugTimeType getDrugTimeType({String? name, int index = 0}) {
    return DrugTimeType.values.lastWhere((e) => name != null && name.isNotEmpty ? e.name == name : e.index == index);
  }

  static PossibleArea getPossibleArea(int value) {
    return PossibleArea.values.lastWhere((e) => e.value == value);
  }

  static DiaryClaim getDiaryClaim(int index) {
    return DiaryClaim.values.lastWhere((e) => e.index == index);
  }

  static BookingLinkMomStatus getBookingLinkMomStatus(int index) {
    return BookingLinkMomStatus.values.lastWhere((e) => e.index == index);
  }

  static SearchZoneStatus getSearchZoneStatus(String name) {
    return SearchZoneStatus.values.lastWhere((e) => e.name == name);
  }

  static AuthImageType getAuthImageType(int type) {
    return AuthImageType.values.lastWhere((e) => e.value == type, orElse: () => AuthImageType.other);
  }

  static MenuType getMenuType(int value) {
    return MenuType.values.lastWhere((e) => e.value == value, orElse: () => MenuType.home);
  }

  static ServiceTabType getServiceTabType(int value) {
    return ServiceTabType.values.lastWhere((e) => e.value == value, orElse: () => ServiceTabType.care);
  }

  static GuideType getGuideType(int value) {
    return GuideType.values.lastWhere((e) => e.value == value, orElse: () => GuideType.guid_0);
  }

  static JobType getJobType(String value) {
    return JobType.values.lastWhere((e) => e.name == value, orElse: () => JobType.job_9);
  }

  static JobType getJobTypeValue(int value) {
    return JobType.values.lastWhere((e) => e.value == value, orElse: () => JobType.job_9);
  }

  static SendCodeType getSendCodeType(int value) {
    return SendCodeType.values.lastWhere((e) => e.value == value, orElse: () => SendCodeType.SCDefault);
  }
}
