import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/listview/list_speed_select.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../commons.dart';

///스피드매칭
showSpeedBottomSheet(
  BuildContext context, {
  DataManager? data,
  Function? onClick,
}) {
  int valueSpeed = 0;
  late SingleItem item;
  String speedTypeMsg = '';

  if (data!.payItem.lsSpeedType.length != 0) {
    data.payItem.lsSpeedType.forEach((value) {
      if (value.isSelected) {
        speedTypeMsg = value.content;
        item = value;
      }
    });
  }

  showModalBottomSheet(
      context: context,
      backgroundColor: color_transparent,
      enableDrag: false,
      isDismissible: false,
      builder: (context) {
        return StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
          return Material(
            color: color_white,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                // closeView(
                //     isShow: false,
                //     onClick: () {
                //       Commons.pagePop(context);
                //     }),
                sb_h_20,
                // Text(
                //   "스피드매칭".tr(),
                //   style: st_b_18(fontWeight: FontWeight.bold),
                //   textAlign: TextAlign.left,
                // ),
                // sb_h_10,
                // Text(
                //   "스피드매칭_선택".tr(),
                //   style: st_b_16(),
                // ),
                // sb_h_02,
                // Text(
                //   speedTypeMsg,
                //   style: st_b_14(textColor: color_aaaaaa),
                // ),
                Padding(
                  padding: padding_30_LR,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "스피드매칭".tr(),
                        style: st_b_20(fontWeight: FontWeight.bold),
                      ),
                      sb_h_10,
                      Text(
                        "스피드매칭_선택_내용".tr(),
                        style: st_b_18(fontWeight: FontWeight.w500),
                      ),
                      // sb_h_02,
                      // Text(
                      //   speedTypeMsg,
                      //   style: st_b_14(textColor: color_aaaaaa),
                      // ),
                    ],
                  ),
                ),
                ListSpeedSelector(
                  data: data.payItem.lsSpeedType,
                  count: 3,
                  margin: padding_10,
                  borderRadius: 30,
                  itemHeight: 120,
                  imageHeight: 60,
                  imageWidth: 60,
                  onItemClick: (value) {
                    item = value as SingleItem;
                    valueSpeed = item.type;
                    setState(() {
                      speedTypeMsg = item.content;
                    });

                    onClick!(DialogAction.update, null);
                  },
                ),
                lBtn("확인".tr(), isEnabled: true, sideColor: color_transparent, onClickAction: () {
                  onClick!(DialogAction.confirm, item);
                  Commons.pagePop(context);
                }),
              ],
            ),
          );
        });
      });
}
