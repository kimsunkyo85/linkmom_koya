import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:highlight_text/highlight_text.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../commons.dart';
import '../style/image_style.dart';

///돌봄신청서/링크쌤 지원서 알림
showPushBottomSheet(
  BuildContext context, {
  Function? onClick,
}) {
  bool isLinkMom = Commons.isLinkMom();
  showModalBottomSheet(
      context: context,
      backgroundColor: color_transparent,
      enableDrag: false,
      isDismissible: false,
      builder: (context) {
        return StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
          return WillPopScope(
            onWillPop: () => Future.value(false),
            child: Material(
              color: color_white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: padding_20,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        sb_h_20,
                        image(imagePath: BELL, size: 32),
                        sb_h_20,
                        lText(
                          isLinkMom ? "링크쌤지원서알림_안내".tr() : "돌봄신청서알림_안내".tr(),
                          style: st_b_20(fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                        sb_h_20,
                        TextHighlight(
                          text: "빠른매칭안내".tr(),
                          textStyle: st_b_14(),
                          words: {"빠른_매칭".tr(): HighlightedWord(onTap: () {}, textStyle: st_b_14(textColor: Commons.getColor()))},
                        ),
                        sb_h_30,
                        lText(
                          "알림을원하지않는다면".tr(),
                          style: st_12(textColor: color_999999),
                          textAlign: TextAlign.center,
                        ),
                        sb_h_05,
                        TextHighlight(
                          textAlign: TextAlign.center,
                          text: isLinkMom ? "링크쌤지원서알림설정_안내_1".tr() : "돌봄신청서알림설정_안내_1".tr(),
                          textStyle: st_12(textColor: color_999999),
                          words: {
                            isLinkMom ? "돌봄신청서알림".tr() : "링크쌤지원서알림".tr(): HighlightedWord(
                                onTap: () {
                                  Commons.page(context, routeSettings);
                                },
                                textStyle: st_12(textColor: color_999999, decoration: TextDecoration.underline))
                          },
                        ),
                      ],
                    ),
                  ),
                  lBtn("네확인했어요".tr(), isEnabled: true, sideColor: color_transparent, onClickAction: () {
                    Commons.pagePop(context);
                    if (onClick != null) {
                      onClick(DialogAction.confirm);
                    }
                  }),
                  if (Platform.isIOS) sb_h_10,
                ],
              ),
            ),
          );
        });
      });
}
