import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/listview/list_single_select.dart';
import 'package:linkmom/utils/modal_bottom_sheet/close_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../../main.dart';
import '../commons.dart';

///그룹 보육 신청
showGroupCareBottomSheet(
  BuildContext context, {
  DataManager? data,
  Function? onClick,
}) {
  showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      enableDrag: false,
      isDismissible: false,
      builder: (context) {
        return StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
          return Material(
            color: color_white,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                closeView(onClick: () {
                  Commons.pagePop(context);
                }),
                Text(
                  "그룹보육신청".tr(),
                  style: st_b_20(fontWeight: FontWeight.bold),
                ),
                sb_h_10,
                Text(
                  "그룹보육내용".tr(),
                  style: st_b_17(),
                ),
                sb_h_02,
                Text(
                  "그룹보육설명".tr(),
                  style: st_b_15(textColor: color_aaaaaa),
                ),
                sb_h_10,
                Row(
                  children: [
                    Expanded(flex: 1, child: lText('')),
                    Expanded(
                      flex: 4,
                      child: ListSingleSelector(
                        data: data!.payItem.lsGroupType,
                        count: 1,
                        margin: padding_30_LR,
                        borderRadius: 30,
                        itemHeight: 120,
                        imageHeight: 60,
                        imageWidth: 60,
                        onItemClick: (value) {
                          log.d('『GGUMBI』>>> build : data1111: $value,  <<< ');
                          onClick!(DialogAction.update, value);
                        },
                      ),
                    ),
                    Expanded(flex: 1, child: lText('')),
                  ],
                ),
                lBtn("확인".tr(), isEnabled: true, sideColor: color_transparent, onClickAction: () {
                  log.d('『GGUMBI』>>> showSpeedBottomSheet ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
                  // Commons.pagePop(context, data: data.confirmItem.lsSpeedType);
                  onClick!(DialogAction.confirm, data.payItem.lsGroupType);
                  Commons.pagePop(context);
                }),
              ],
            ),
          );
        });
      });
}
