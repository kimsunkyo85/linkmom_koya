import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../commons.dart';
import 'close_view.dart';

///인증센터 안내
showAuthBottomSheet(
  BuildContext context, {
  DataManager? data,
  SingleItem? item,
  Function? onClick,
}) {
  showModalBottomSheet(
      context: context,
      backgroundColor: color_transparent,
      enableDrag: false,
      isDismissible: false,
      builder: (context) {
        return StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
          ///집안환경 화면
          Widget _authHomeView() {
            int flex1 = 1;
            int flex2 = 2;
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(flex: flex1, child: lText("환경인증_우리집환경".tr(), style: st_15(textColor: color_999999, fontWeight: FontWeight.w500))),
                    sb_w_10,
                    Expanded(flex: flex2, child: lText('아파트', style: st_b_15(fontWeight: FontWeight.w500))),
                  ],
                ),
                sb_h_10,
                Row(
                  children: [
                    Expanded(flex: flex1, child: lText("환경인증_CCTV".tr(), style: st_15(textColor: color_999999, fontWeight: FontWeight.w500))),
                    sb_w_10,
                    Expanded(flex: flex2, child: lText('있다', style: st_15(fontWeight: FontWeight.w500))),
                  ],
                ),
                sb_h_10,
                Row(
                  children: [
                    Expanded(flex: flex1, child: lText("환경인증_반려동물".tr(), style: st_15(textColor: color_999999, fontWeight: FontWeight.w500))),
                    sb_w_10,
                    Expanded(flex: flex2, child: lText('강아지/고양이', style: st_b_15(fontWeight: FontWeight.w500))),
                  ],
                ),
                sb_h_10,
                lText("우리집사진".tr(), style: st_15(textColor: color_999999, fontWeight: FontWeight.w500)),
                sb_h_10,
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          Commons.nextPage(context,
                              fn: () => PhotoView(
                                      imageProvider: NetworkImage(
                                    "https://dreambcokr.cdn.smart-img.com/ggumbi/2019/pc/main/s71_logo.png",
                                  )));
                        },
                        child: Image.network(
                          // StringUtils.validateString(data.myInfoItem.myInfoData.profileimg) ? data.myInfoItem.myInfoData.profileimg : "",
                          "https://dreambcokr.cdn.smart-img.com/ggumbi/2019/pc/main/s71_logo.png",
                          loadingBuilder: (context, child, loadingProgress) {
                            if (loadingProgress == null) return child;
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          },
                          fit: BoxFit.cover,
                          width: widthFull(context),
                        ),
                      ),
                    ),
                    sb_w_10,
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          Commons.nextPage(context,
                              fn: () => PhotoView(
                                      imageProvider: NetworkImage(
                                    "https://dreambcokr.cdn.smart-img.com/ggumbi/2019/pc/main/s71_logo.png",
                                  )));
                        },
                        child: Image.network(
                          // StringUtils.validateString(data.myInfoItem.myInfoData.profileimg) ? data.myInfoItem.myInfoData.profileimg : "",
                          "https://dreambcokr.cdn.smart-img.com/ggumbi/2019/pc/main/s71_logo.png",
                          loadingBuilder: (context, child, loadingProgress) {
                            if (loadingProgress == null) return child;
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          },
                          fit: BoxFit.cover,
                          width: widthFull(context),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            );
          }

          ///내용화면
          Widget _contentView() {
            int flex1 = 1;
            int flex2 = 2;
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(flex: flex1, child: lText("일반 인증 센터 내용....", style: st_15(textColor: color_999999, fontWeight: FontWeight.w500))),
                    sb_w_10,
                    Expanded(flex: flex2, child: lText('ㅇㅇㅇㅇ', style: st_b_15(fontWeight: FontWeight.w500))),
                  ],
                ),
              ],
            );
          }

          return Material(
            color: color_white,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            child: Column(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      lScrollView(
                        padding: padding_0,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            sb_h_30,
                            Padding(
                              padding: padding_30_LR,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      image(imagePath: item!.image),
                                      sb_w_10,
                                      lText(item.name, style: st_b_20(fontWeight: FontWeight.bold), textAlign: TextAlign.left),
                                    ],
                                  ),
                                  sb_h_10,
                                  lText(
                                    item.data.childName,
                                    style: st_b_18(fontWeight: FontWeight.w500),
                                  ),

                                  lDivider(padding: padding_20_TB),
                                  _authHomeView(),
                                  _contentView(),
                                  // sb_h_02,
                                  // Text(
                                  //   speedTypeMsg,
                                  //   style: st_b_14(textColor: color_aaaaaa),
                                  // ),
                                ],
                              ),
                            ),
                            // ListSpeedSelector(
                            //   data: data.payItem.lsSpeedType,
                            //   count: 3,
                            //   margin: padding_10,
                            //   borderRadius: 30,
                            //   itemHeight: 120,
                            //   imageHeight: 60,
                            //   imageWidth: 60,
                            //   onItemClick: (value) {
                            //     item = value as SingleItem;
                            //     onClick(DialogAction.update);
                            //   },
                            // ),
                          ],
                        ),
                      ),
                      closeView(onClick: () {
                        onClick!(DialogAction.close);
                        Commons.pagePop(context);
                      }),
                    ],
                  ),
                ),
                lBtn("확인".tr(), isEnabled: true, sideColor: color_transparent, onClickAction: () {
                  onClick!(DialogAction.confirm);
                  Commons.pagePop(context);
                }),
              ],
            ),
          );
        });
      });
}
