import 'package:flutter/material.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/lcons.dart';

Widget closeView({bool isShow = true, padding = padding_10, onClick}) {
  return Container(
    alignment: Alignment.topRight,
    padding: padding,
    child: isShow
        ? IconButton(
            icon: Lcons.nav_close(size: 30),
      onPressed: () {
        if (onClick != null) {
          onClick();
        }
      },
    )
        : Container(),
  );
}
