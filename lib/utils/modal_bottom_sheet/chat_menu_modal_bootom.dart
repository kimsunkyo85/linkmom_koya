import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../commons.dart';
import 'close_view.dart';

///채팅 하단 메뉴
showChatMenu(BuildContext context, {int chatType = 0, Function? onClick}) {
  List<String> lsMenu = ["돌봄 신청서 불러오기", "매칭 요청하기", "매칭 취소하기", "결제하기", "돌봄취소 하기", "사진"];

  showModalBottomSheet(
      context: context,
      backgroundColor: color_transparent,
      enableDrag: false,
      isDismissible: false,
      builder: (context) {
        return StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
          return Wrap(
            children: [
              Material(
                color: color_white,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
                // borderRadius: BorderRadius.all(Radius.circular(20)),
                child: Stack(
                  children: [
                    Container(
                      // margin: padding_20_TB,
                      margin: EdgeInsets.only(top: 20, bottom: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          InkWell(
                            onTap: () {
                              onClick!(ChatMenuType.apply_open);
                              Commons.pagePop(context);
                            },
                            child: lText(
                              lsMenu[0],
                              style: st_b_16(),
                            ),
                          ),
                          lDivider(padding: padding_10_TB),
                          InkWell(
                            onTap: () {
                              onClick!(ChatMenuType.matching_request);
                              Commons.pagePop(context);
                            },
                            child: lText(
                              lsMenu[1],
                              style: st_b_16(),
                            ),
                          ),
                          lDivider(padding: padding_10_TB),
                          InkWell(
                            onTap: () {
                              onClick!(ChatMenuType.matching_cancel);
                              Commons.pagePop(context);
                            },
                            child: lText(
                              lsMenu[2],
                              style: st_b_16(),
                            ),
                          ),
                          lDivider(padding: padding_10_TB),
                          InkWell(
                            onTap: () {
                              onClick!(ChatMenuType.payment);
                              Commons.pagePop(context);
                            },
                            child: lText(
                              lsMenu[3],
                              style: st_b_16(),
                            ),
                          ),
                          lDivider(padding: padding_10_TB),
                          InkWell(
                            onTap: () {
                              onClick!(ChatMenuType.care_cancel);
                              Commons.pagePop(context);
                            },
                            child: lText(
                              lsMenu[4],
                              style: st_b_16(),
                            ),
                          ),
                          lDivider(padding: padding_10_TB),
                          InkWell(
                            onTap: () {
                              onClick!(ChatMenuType.photo);
                              Commons.pagePop(context);
                            },
                            child: lText(
                              lsMenu[5],
                              style: st_b_16(),
                            ),
                          ),
                        ],
                      ),
                    ),
                    closeView(
                        isShow: true,
                        padding: padding_05_R,
                        onClick: () {
                          Commons.pagePop(context);
                        }),
                  ],
                ),
              ),
            ],
          );
        });
      });
}
