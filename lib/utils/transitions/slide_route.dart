import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:swipe_back_detector/swipe_back_detector.dart';

class SlideRightRoute extends PageRouteBuilder {
  final Widget? page;
  dynamic value;
  SlideRightRoute({this.page, this.value})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page!,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) {
            var offsetAnimation = animation.drive(CurveTween(curve: Curves.easeInCirc));
            return SwipeBackDetector(
              cutoffVelocity: 100,
              child: SlideTransition(
                position: Tween<Offset>(
                  begin: const Offset(-1, 0),
                  end: Offset.zero,
                ).animate(offsetAnimation),
                child: child,
              ),
              popValue: value ?? true,
            );
          },
        );

  @override
  bool get hasScopedWillPopCallback => false;
}

class SlideLeftRoute extends PageRouteBuilder {
  final Widget? page;
  dynamic value;
  final RouteSettings? setting;
  SlideLeftRoute({this.page, this.value, this.setting})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page!,
          settings: setting,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) {
            var offsetAnimation = animation.drive(CurveTween(curve: Curves.easeInCirc));
            return SwipeBackDetector(
              cutoffVelocity: 100,
              child: SlideTransition(
                position: Tween<Offset>(
                  begin: const Offset(1, 0),
                  end: Offset.zero,
                ).animate(offsetAnimation),
                child: child,
              ),
              popValue: value ?? true,
            );
          },
        );
  @override
  bool get hasScopedWillPopCallback => false;
}

class SlideTopRoute extends PageRouteBuilder {
  final Widget? page;
  dynamic value;
  final RouteSettings? setting;
  SlideTopRoute({this.page, this.value, this.setting})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page!,
          settings: setting,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) {
            var offsetAnimation = animation.drive(CurveTween(curve: Curves.easeInCirc));
            return SwipeBackDetector(
              child: SlideTransition(
                position: Tween<Offset>(
                  begin: const Offset(0, 1),
                  end: Offset.zero,
                ).animate(offsetAnimation),
                child: child,
              ),
              popValue: value ?? true,
            );
          },
        );
}

class SlideBottomRoute extends PageRouteBuilder {
  final Widget? page;
  dynamic value;
  final RouteSettings? setting;
  SlideBottomRoute({this.page, this.value, this.setting})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page!,
          settings: setting,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              SwipeBackDetector(
            cutoffVelocity: 100,
            child: SlideTransition(
              position: Tween<Offset>(
                begin: const Offset(0, -1),
                end: Offset.zero,
              ).animate(animation),
              child: child,
            ),
            popValue: value ?? true,
          ),
        );
  @override
  bool get hasScopedWillPopCallback => false;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
    return super.buildPage(context, animation, secondaryAnimation);
  }
}

class CupertinoObserver extends NavigatorObserver {
  @override
  void didStartUserGesture(Route route, Route? previousRoute) {
    super.didStartUserGesture(route, previousRoute);
  }
}
