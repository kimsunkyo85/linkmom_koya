import 'dart:convert';

import 'package:flutter/material.dart';

/// Constants defined for Emoji.
class EmojiConst {
  static final String charNonSpacingMark = String.fromCharCode(0xfe0f);
  static const String charColon = ':';
  static const String charEmpty = '';
}

/// List of pre-defined message used in this library
class EmojiMessage {
  static const String errorMalformedEmojiName = 'Malformed emoji name';
}

/// Utilities to handle emoji operations.
class EmojiUtil {
  /// Strip colons for emoji name.
  /// So, ':heart:' will become 'heart'.
  static String stripColons(String name, [void onError(String message)?]) {
    Iterable<Match> matches = EmojiParser.REGEX_NAME.allMatches(name);
    if (matches.isEmpty) {
      if (onError != null) {
        onError(EmojiMessage.errorMalformedEmojiName);
      }
      return name;
    }
    return name.toLowerCase().replaceAll(EmojiConst.charColon, EmojiConst.charEmpty);
  }

  /// Wrap colons on both sides of emoji name.
  /// So, 'heart' will become ':heart:'.
  static String ensureColons(String name) {
    String res = name;
    if (!name.startsWith(EmojiConst.charColon, 0)) {
      res = EmojiConst.charColon + name;
    }

    if (!name.endsWith(EmojiConst.charColon)) {
      res += EmojiConst.charColon;
    }

    return res;
  }

  /// When processing emojis, we don't need to store the graphical byte
  /// which is 0xfe0f, or so-called 'Non-Spacing Mark'.
  static String stripNSM(String name) => name.replaceAll(RegExp(EmojiConst.charNonSpacingMark), EmojiConst.charEmpty);

  static final REGEX_EMOJI = RegExp(
    r'[\u{1f300}-\u{1f5ff}\u{1f900}-\u{1f9ff}\u{1f600}-\u{1f64f}'
    r'\u{1f680}-\u{1f6ff}\u{2600}-\u{26ff}\u{2700}'
    r'-\u{27bf}\u{1f1e6}-\u{1f1ff}\u{1f191}-\u{1f251}'
    r'\u{1f004}\u{1f0cf}\u{1f170}-\u{1f171}\u{1f17e}'
    r'-\u{1f17f}\u{1f18e}\u{3030}\u{2b50}\u{2b55}'
    r'\u{2934}-\u{2935}\u{2b05}-\u{2b07}\u{2b1b}'
    r'-\u{2b1c}\u{3297}\u{3299}\u{303d}\u{00a9}'
    r'\u{00ae}\u{2122}\u{23f3}\u{24c2}\u{23e9}'
    r'-\u{23ef}\u{25b6}\u{23f8}-\u{23fa}\u{200d}]+',
    unicode: true,
  );

  /// Returns true if the given text contains only emojis.
  ///
  /// "👋" -> true
  /// "👋 Hello" -> false
  /// ":wave:" --> false
  /// "👋👋" -> true
  /// "👋 👋" -> false (if [ignoreWhitespace] is true, result is true)
  static bool hasOnlyEmojis(String text, {bool ignoreWhitespace = false}) {
    if (ignoreWhitespace) text = text.replaceAll(' ', '');
    for (final c in Characters(text)) if (!REGEX_EMOJI.hasMatch(c)) return false;
    return true;
  }
}

/// The representation of an emoji.
/// There are three properties availables:
///   - 'name' : the emoji name (no colon)
///   - 'full' : the full emoji name. It is name with colons on both sides.
///   - 'code' : the actual graphic presentation of emoji.
///
/// Emoji.None is being used to represent a NULL emoji.
class Emoji {
  /// If emoji not found, the parser always returns this.
  static final Emoji None = Emoji(EmojiConst.charEmpty, EmojiConst.charEmpty);

  final String name;
  final String code;

  Emoji(this.name, this.code);

  String get full => EmojiUtil.ensureColons(this.name);

  @override
  bool operator ==(other) {
    if (other is! Emoji) {
      return false;
    }

    return this.name == other.name && this.code == other.code;
  }

  Emoji clone() {
    return Emoji(name, code);
  }

  @override
  String toString() {
    return 'Emoji{name="$name", full="$full", code="$code"}';
  }

  @override
  // TODO: implement hashCode
  int get hashCode => super.hashCode;

}

/// Emoji storage and parser.
/// You will need to instantiate one of this instance to start using.
class EmojiParser {
  static void init(BuildContext context) async {
    if (_emojiMap.isEmpty) {
      var data = await DefaultAssetBundle.of(context).loadString("assets/strings/emoji.json");
      _emojiMap = jsonDecode(data);
    }
  }

  static final REGEX_NAME = RegExp(r':([\w-+]+):');

  Emoji get(String name) => _emojisByName[EmojiUtil.stripColons(name)] ?? Emoji.None;

  Emoji getName(String name) => get(name);

  bool hasName(String name) => _emojisByName.containsKey(EmojiUtil.stripColons(name));

  /// Get info for an emoji.
  ///
  /// For example:
  ///
  ///   var parser = EmojiParser();
  ///   var emojiHeart = parser.info('heart');
  ///   print(emojiHeart); '{name: heart, full: :heart:, emoji: ❤️}'
  ///
  /// Returns Emoji.None if not found.
  Emoji info(String name) {
    return hasName(name) ? get(name) : Emoji.None;
  }

  /// Get emoji based on emoji code.
  ///
  /// For example:
  ///
  ///   var parser = EmojiParser();
  ///   var emojiHeart = parser.getEmoji('❤');
  ///   print(emojiHeart); '{name: heart, full: :heart:, emoji: ❤️}'
  ///
  /// Returns Emoji.None if not found.
  ///
  Emoji getEmoji(String emoji) {
    return _emojisByCode[EmojiUtil.stripNSM(emoji)] ?? Emoji.None;
  }

  bool hasEmoji(String emoji) {
    return _emojisByCode.containsKey(EmojiUtil.stripNSM(emoji));
  }

  /// Emojify the input text.
  ///
  /// For example: 'I :heart: :coffee:' => 'I ❤️ ☕'
  ///
  String emojify(String text) {
    Iterable<Match> matches = REGEX_NAME.allMatches(text);
    if (matches.isNotEmpty) {
      String result = text.toLowerCase();
      matches.toList().forEach((m) {
        var _e = EmojiUtil.stripColons(m.group(0)!);
        if (hasName(_e)) {
          result = result.replaceAll(m.group(0)!, get(_e).code);
        }
      });
      return result;
    }
    return text;
  }

  /// This method will unemojify the text containing the Unicode emoji symbols
  /// into emoji name.
  ///
  /// For example: 'I ❤️ Flutter' => 'I :heart: Flutter'
  ///
  String unemojify(String text) {
    // Convert to characters in order to properly match grapheme clusters.
    final characters = Characters(text);

    final buffer = StringBuffer();
    for (final character in characters) {
      if (hasEmoji(character)) {
        var result = character;
        result = result.replaceAll(
          character,
          getEmoji(character).full,
        );

        /// Just a quick hack to clear graphical byte from emoji.
        /// todo: find better way to get through this tweak
        result = result.replaceAll(
          EmojiConst.charNonSpacingMark,
          EmojiConst.charEmpty,
        );

        buffer.write(result);
      } else {
        buffer.write(character);
      }
    }
    return buffer.toString();
  }
}

final _emojisByName = <String, Emoji>{
  for (final entry in _emojiMap.entries) entry.key: Emoji(entry.key, entry.value),
};

final _emojisByCode = <String, Emoji>{
  for (final entry in _emojiMap.entries) EmojiUtil.stripNSM(entry.value): Emoji(entry.key, entry.value),
};

Map _emojiMap = {};
