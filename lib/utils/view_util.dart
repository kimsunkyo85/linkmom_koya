import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stack_trace/stack_trace.dart';

///다음 화면으로 포커스 넘기기
focusNextTextField(BuildContext context) {
  do {
    var foundFocusNode = FocusScope.of(context).nextFocus();
    if (!foundFocusNode) return;
  } while (FocusScope.of(context).focusedChild!.context!.widget is! EditableText);
}

///포커스 없애기
focusClear(BuildContext context) {
  FocusScope.of(context).unfocus();
}

///해당 포커스로 이동하기
focusRequest(BuildContext context, FocusNode focusNode){
  FocusScope.of(context).requestFocus(focusNode);
}

class ViewFlag {
  ViewFlag();

  bool _isLoading = false;
  bool _isEndLoading = false;
  bool _isShowPassword = true;
  bool _isShowPasswordConfirm = true;
  bool _isPasswordConfirm = false;
  bool _isSelectEmail = true;
  bool _isExists = true;
  bool _isMobile = false;
  bool _isTimer = false;
  bool _isConfirm = false;
  bool _isAuth = false;
  bool _isValid = false;

  bool get isLoading => _isLoading;

  bool get isEndLoading => _isEndLoading;

  bool get isShowPassword => _isShowPassword;

  bool get isShowPasswordConfirm => _isShowPasswordConfirm;

  bool get isPasswordConfirm => _isPasswordConfirm;

  bool get isSelectEmail => _isSelectEmail;

  bool get isExists => _isExists;

  bool get isMobile => _isMobile;

  bool get isTimer => _isTimer;

  bool get isConfirm => _isConfirm;

  bool get isAuth => _isAuth;

  bool get isValid => _isValid;

  enableLoading({VoidCallback? fn}) {
    if (fn != null) {
      _isLoading = true;
      fn();
    }
  }

  disableLoading({VoidCallback? fn}) {
    if (fn != null) {
      _isLoading = false;
      fn();
    }
  }

  enableEndLoading({VoidCallback? fn}) {
    if (fn != null) {
      _isEndLoading = true;
      fn();
    }
  }

  disableEndLoading({VoidCallback? fn}) {
    if (fn != null) {
      _isEndLoading = false;
      fn();
    }
  }

  ///paswwrod show/hide
  toggleShowPassword(bool isConfirmPw, {VoidCallback? fn}) {
    bool log = false;
    if (!isConfirmPw) {
      log = _isShowPassword;
      _isShowPassword = !_isShowPassword;
    } else {
      log = _isShowPasswordConfirm;
      _isShowPasswordConfirm = !_isShowPasswordConfirm;
    }
    print('ggumbi:' +
        Trace.current().frames[0].location +
        ' ' +
        (Trace.current().frames[0].member ?? '') +
        '::$fn, $log');
    if (fn != null) {
      fn();
    }
  }

  enablePasswordConfirm({VoidCallback? fn}) {
    if (fn != null) {
      _isPasswordConfirm = true;
      fn();
    }
  }

  disablePasswordConfirm({VoidCallback? fn}) {
    if (fn != null) {
      _isPasswordConfirm = false;
      fn();
    }
  }

  enableMobile({VoidCallback? fn}) {
    if (fn != null) {
      _isMobile = true;
      fn();
    }
  }

  disableSelectEmail({VoidCallback? fn}) {
    if (fn != null) {
      _isSelectEmail = false;
      fn();
    }
  }

  enableSelectEmail({VoidCallback? fn}) {
    if (fn != null) {
      _isSelectEmail = true;
      fn();
    }
  }

  disableExists({VoidCallback? fn}) {
    if (fn != null) {
      _isExists = false;
      fn();
    }
  }

  enableExists({VoidCallback? fn}) {
    if (fn != null) {
      _isExists = true;
      fn();
    }
  }

  disableMobile({VoidCallback? fn}) {
    if (fn != null) {
      _isMobile = false;
      fn();
    }
  }

  enableTimer({VoidCallback? fn}) {
    if (fn != null) {
      _isTimer = true;
      fn();
    }
  }

  disableTimer({VoidCallback? fn}) {
    if (fn != null) {
      _isTimer = false;
      fn();
    }
  }

  enableConfirm({VoidCallback? fn}) {
    _isConfirm = true;
    if (fn != null) {
      fn();
    }
  }

  disableConfirm({VoidCallback? fn}) {
    _isConfirm = false;
    if (fn != null) {
      fn();
    }
  }

  enableAuth({VoidCallback? fn}) {
    _isAuth = true;
    if (fn != null) {
      fn();
    }
  }

  disableAuth({VoidCallback? fn}) {
    _isAuth = false;
    if (fn != null) {
      fn();
    }
  }

  resetShowPassword({VoidCallback? fn}) {
    _isShowPassword = true;
    _isShowPasswordConfirm = true;
    if (fn != null) {
      fn();
    }
  }

  enableValid({VoidCallback? fn}) {
    _isValid = true;
    if (fn != null) {
      fn();
    }
  }

  disableValid({VoidCallback? fn}) {
    _isValid = false;
    if (fn != null) {
      fn();
    }
  }
}
