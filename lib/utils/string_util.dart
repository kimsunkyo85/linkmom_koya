import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/services.dart';
import 'package:jiffy/jiffy.dart';
import 'package:linkmom/manager/enum_manager.dart';

import '../main.dart';

///돌봄시간 포맷 오전 10:00 ('a h:mm')
String F_HMM = 'a hh:mm';

///링크쌤 목록-근무날짜 포멧(21.06.09(수))-yy.M.d(E)
String F_YYMMDDE = 'yy.MM.dd(E)';

///링크쌤 목록-근무날짜 포멧(06.09(수))-yy.M.d(E)
String F_MMDDE = 'MM.dd(E)';

///채팅 시간 포멧(2021.06.09)-yyyy.Mm.dd
String F_YYMMDD = 'yyyy.MM.dd';

///채팅 시간 포멧(06.09)-Mm.dd
String F_MMDD = 'MM.dd';
String BYME = 'byme';
String TOME = 'tome';
String BYMETOME = 'bymetome';

///8월31일 10시 30분
String F_MMDDHHMM = 'M${"월".tr()} d${"일".tr()} a hh:mm';

///채팅 시간 포멧(2021.06.09(수))-yy.M.d(E)
String F_YYYYMMDDE = 'yyyy. MM. dd E${"요일".tr()}';

///채팅 보낸 시간 20210719143900
String F_YYYYMMDDHHMMSS = 'yyyyMMddHHmmss';

///계약서 2021년 8월 1일
String F_YYYYMDHHMM = 'yyyy${"년".tr()} M${"월".tr()} d${"일".tr()} HH${"시".tr()} mm${"분".tr()}';

String emojiRegexp =
    '   /\uD83C\uDFF4\uDB40\uDC67\uDB40\uDC62(?:\uDB40\uDC77\uDB40\uDC6C\uDB40\uDC73|\uDB40\uDC73\uDB40\uDC63\uDB40\uDC74|\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67)\uDB40\uDC7F|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC68(?:\uD83C\uDFFF\u200D(?:\uD83E\uDD1D\u200D\uD83D\uDC68(?:\uD83C[\uDFFB-\uDFFE])|\uD83C[\uDF3E\uDF73\uDF7C\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFE\u200D(?:\uD83E\uDD1D\u200D\uD83D\uDC68(?:\uD83C[\uDFFB-\uDFFD\uDFFF])|\uD83C[\uDF3E\uDF73\uDF7C\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFD\u200D(?:\uD83E\uDD1D\u200D\uD83D\uDC68(?:\uD83C[\uDFFB\uDFFC\uDFFE\uDFFF])|\uD83C[\uDF3E\uDF73\uDF7C\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFC\u200D(?:\uD83E\uDD1D\u200D\uD83D\uDC68(?:\uD83C[\uDFFB\uDFFD-\uDFFF])|\uD83C[\uDF3E\uDF73\uDF7C\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFB\u200D(?:\uD83E\uDD1D\u200D\uD83D\uDC68(?:\uD83C[\uDFFC-\uDFFF])|\uD83C[\uDF3E\uDF73\uDF7C\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D)?\uD83D\uDC68|(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D[\uDC66\uDC67])|[\u2695\u2696\u2708]\uFE0F|\uD83D[\uDC66\uDC67]|\uD83C[\uDF3E\uDF73\uDF7C\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|(?:\uD83C\uDFFF\u200D[\u2695\u2696\u2708]|\uD83C\uDFFE\u200D[\u2695\u2696\u2708]|\uD83C\uDFFD\u200D[\u2695\u2696\u2708]|\uD83C\uDFFC\u200D[\u2695\u2696\u2708]|\uD83C\uDFFB\u200D[\u2695\u2696\u2708])\uFE0F|\uD83C\uDFFF|\uD83C\uDFFE|\uD83C\uDFFD|\uD83C\uDFFC|\uD83C\uDFFB)?|\uD83E\uDDD1(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83E\uDD1D\u200D\uD83E\uDDD1(?:\uD83C[\uDFFB-\uDFFF])|\uD83C[\uDF3E\uDF73\uDF7C\uDF84\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\u200D(?:\uD83E\uDD1D\u200D\uD83E\uDDD1|\uD83C[\uDF3E\uDF73\uDF7C\uDF84\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD]))|\uD83D\uDC69(?:\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D(?:\uD83D[\uDC68\uDC69])|\uD83D[\uDC68\uDC69])|\uD83C[\uDF3E\uDF73\uDF7C\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFF\u200D(?:\uD83C[\uDF3E\uDF73\uDF7C\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFE\u200D(?:\uD83C[\uDF3E\uDF73\uDF7C\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFD\u200D(?:\uD83C[\uDF3E\uDF73\uDF7C\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFC\u200D(?:\uD83C[\uDF3E\uDF73\uDF7C\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFB\u200D(?:\uD83C[\uDF3E\uDF73\uDF7C\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD]))|\uD83D\uDC69\uD83C\uDFFF\u200D\uD83E\uDD1D\u200D(?:\uD83D[\uDC68\uDC69])(?:\uD83C[\uDFFB-\uDFFE])|\uD83D\uDC69\uD83C\uDFFE\u200D\uD83E\uDD1D\u200D(?:\uD83D[\uDC68\uDC69])(?:\uD83C[\uDFFB-\uDFFD\uDFFF])|\uD83D\uDC69\uD83C\uDFFD\u200D\uD83E\uDD1D\u200D(?:\uD83D[\uDC68\uDC69])(?:\uD83C[\uDFFB\uDFFC\uDFFE\uDFFF])|\uD83D\uDC69\uD83C\uDFFC\u200D\uD83E\uDD1D\u200D(?:\uD83D[\uDC68\uDC69])(?:\uD83C[\uDFFB\uDFFD-\uDFFF])|\uD83D\uDC69\uD83C\uDFFB\u200D\uD83E\uDD1D\u200D(?:\uD83D[\uDC68\uDC69])(?:\uD83C[\uDFFC-\uDFFF])|\uD83D\uDC69\u200D\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D[\uDC66\uDC67])|(?:\uD83D\uDC41\uFE0F\u200D\uD83D\uDDE8|\uD83D\uDC69(?:\uD83C\uDFFF\u200D[\u2695\u2696\u2708]|\uD83C\uDFFE\u200D[\u2695\u2696\u2708]|\uD83C\uDFFD\u200D[\u2695\u2696\u2708]|\uD83C\uDFFC\u200D[\u2695\u2696\u2708]|\uD83C\uDFFB\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708])|\uD83C\uDFF3\uFE0F\u200D\u26A7|\uD83E\uDDD1(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708])|\uD83D\uDC3B\u200D\u2744|(?:(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC70\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD35\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDCD-\uDDCF\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])|\uD83D\uDC6F|\uD83E[\uDD3C\uDDDE\uDDDF])\u200D[\u2640\u2642]|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uFE0F|\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|\uD83C\uDFF4\u200D\u2620|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC70\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD35\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDCD-\uDDCF\uDDD6-\uDDDD])\u200D[\u2640\u2642]|[\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u2328\u23CF\u23ED-\u23EF\u23F1\u23F2\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB\u25FC\u2600-\u2604\u260E\u2611\u2618\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u265F\u2660\u2663\u2665\u2666\u2668\u267B\u267E\u2692\u2694-\u2697\u2699\u269B\u269C\u26A0\u26A7\u26B0\u26B1\u26C8\u26CF\u26D1\u26D3\u26E9\u26F0\u26F1\u26F4\u26F7\u26F8\u2702\u2708\u2709\u270F\u2712\u2714\u2716\u271D\u2721\u2733\u2734\u2744\u2747\u2763\u2764\u27A1\u2934\u2935\u2B05-\u2B07\u3030\u303D\u3297\u3299]|\uD83C[\uDD70\uDD71\uDD7E\uDD7F\uDE02\uDE37\uDF21\uDF24-\uDF2C\uDF36\uDF7D\uDF96\uDF97\uDF99-\uDF9B\uDF9E\uDF9F\uDFCD\uDFCE\uDFD4-\uDFDF\uDFF5\uDFF7]|\uD83D[\uDC3F\uDCFD\uDD49\uDD4A\uDD6F\uDD70\uDD73\uDD76-\uDD79\uDD87\uDD8A-\uDD8D\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA\uDECB\uDECD-\uDECF\uDEE0-\uDEE5\uDEE9\uDEF0\uDEF3])\uFE0F|\uD83D\uDC69\u200D\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08|\uD83D\uDC69\u200D\uD83D\uDC67|\uD83D\uDC69\u200D\uD83D\uDC66|\uD83D\uDC15\u200D\uD83E\uDDBA|\uD83D\uDC69(?:\uD83C\uDFFF|\uD83C\uDFFE|\uD83C\uDFFD|\uD83C\uDFFC|\uD83C\uDFFB)?|\uD83C\uDDFD\uD83C\uDDF0|\uD83C\uDDF6\uD83C\uDDE6|\uD83C\uDDF4\uD83C\uDDF2|\uD83D\uDC08\u200D\u2B1B|\uD83D\uDC41\uFE0F|\uD83C\uDFF3\uFE0F|\uD83E\uDDD1(?:\uD83C[\uDFFB-\uDFFF])?|\uD83C\uDDFF(?:\uD83C[\uDDE6\uDDF2\uDDFC])|\uD83C\uDDFE(?:\uD83C[\uDDEA\uDDF9])|\uD83C\uDDFC(?:\uD83C[\uDDEB\uDDF8])|\uD83C\uDDFB(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDEE\uDDF3\uDDFA])|\uD83C\uDDFA(?:\uD83C[\uDDE6\uDDEC\uDDF2\uDDF3\uDDF8\uDDFE\uDDFF])|\uD83C\uDDF9(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDED\uDDEF-\uDDF4\uDDF7\uDDF9\uDDFB\uDDFC\uDDFF])|\uD83C\uDDF8(?:\uD83C[\uDDE6-\uDDEA\uDDEC-\uDDF4\uDDF7-\uDDF9\uDDFB\uDDFD-\uDDFF])|\uD83C\uDDF7(?:\uD83C[\uDDEA\uDDF4\uDDF8\uDDFA\uDDFC])|\uD83C\uDDF5(?:\uD83C[\uDDE6\uDDEA-\uDDED\uDDF0-\uDDF3\uDDF7-\uDDF9\uDDFC\uDDFE])|\uD83C\uDDF3(?:\uD83C[\uDDE6\uDDE8\uDDEA-\uDDEC\uDDEE\uDDF1\uDDF4\uDDF5\uDDF7\uDDFA\uDDFF])|\uD83C\uDDF2(?:\uD83C[\uDDE6\uDDE8-\uDDED\uDDF0-\uDDFF])|\uD83C\uDDF1(?:\uD83C[\uDDE6-\uDDE8\uDDEE\uDDF0\uDDF7-\uDDFB\uDDFE])|\uD83C\uDDF0(?:\uD83C[\uDDEA\uDDEC-\uDDEE\uDDF2\uDDF3\uDDF5\uDDF7\uDDFC\uDDFE\uDDFF])|\uD83C\uDDEF(?:\uD83C[\uDDEA\uDDF2\uDDF4\uDDF5])|\uD83C\uDDEE(?:\uD83C[\uDDE8-\uDDEA\uDDF1-\uDDF4\uDDF6-\uDDF9])|\uD83C\uDDED(?:\uD83C[\uDDF0\uDDF2\uDDF3\uDDF7\uDDF9\uDDFA])|\uD83C\uDDEC(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEE\uDDF1-\uDDF3\uDDF5-\uDDFA\uDDFC\uDDFE])|\uD83C\uDDEB(?:\uD83C[\uDDEE-\uDDF0\uDDF2\uDDF4\uDDF7])|\uD83C\uDDEA(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDED\uDDF7-\uDDFA])|\uD83C\uDDE9(?:\uD83C[\uDDEA\uDDEC\uDDEF\uDDF0\uDDF2\uDDF4\uDDFF])|\uD83C\uDDE8(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDEE\uDDF0-\uDDF5\uDDF7\uDDFA-\uDDFF])|\uD83C\uDDE7(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEF\uDDF1-\uDDF4\uDDF6-\uDDF9\uDDFB\uDDFC\uDDFE\uDDFF])|\uD83C\uDDE6(?:\uD83C[\uDDE8-\uDDEC\uDDEE\uDDF1\uDDF2\uDDF4\uDDF6-\uDDFA\uDDFC\uDDFD\uDDFF])|[#\*0-9]\uFE0F\u20E3|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC70\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD35\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDCD-\uDDCF\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uFE0F|\uD83C[\uDFFB-\uDFFF])|\uD83C\uDFF4|(?:[\u270A\u270B]|\uD83C[\uDF85\uDFC2\uDFC7]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66\uDC67\uDC6B-\uDC6D\uDC72\uDC74-\uDC76\uDC78\uDC7C\uDC83\uDC85\uDCAA\uDD7A\uDD95\uDD96\uDE4C\uDE4F\uDEC0\uDECC]|\uD83E[\uDD0C\uDD0F\uDD18-\uDD1C\uDD1E\uDD1F\uDD30-\uDD34\uDD36\uDD77\uDDB5\uDDB6\uDDBB\uDDD2-\uDDD5])(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u270C\u270D]|\uD83D[\uDD74\uDD90])(?:\uFE0F|\uD83C[\uDFFB-\uDFFF])|[\u270A\u270B]|\uD83C[\uDF85\uDFC2\uDFC7]|\uD83D[\uDC08\uDC15\uDC3B\uDC42\uDC43\uDC46-\uDC50\uDC66\uDC67\uDC6B-\uDC6D\uDC72\uDC74-\uDC76\uDC78\uDC7C\uDC83\uDC85\uDCAA\uDD7A\uDD95\uDD96\uDE4C\uDE4F\uDEC0\uDECC]|\uD83E[\uDD0C\uDD0F\uDD18-\uDD1C\uDD1E\uDD1F\uDD30-\uDD34\uDD36\uDD77\uDDB5\uDDB6\uDDBB\uDDD2-\uDDD5]|\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC70\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD35\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDCD-\uDDCF\uDDD6-\uDDDD]|\uD83D\uDC6F|\uD83E[\uDD3C\uDDDE\uDDDF]|[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55]|\uD83C[\uDC04\uDCCF\uDD8E\uDD91-\uDD9A\uDE01\uDE1A\uDE2F\uDE32-\uDE36\uDE38-\uDE3A\uDE50\uDE51\uDF00-\uDF20\uDF2D-\uDF35\uDF37-\uDF7C\uDF7E-\uDF84\uDF86-\uDF93\uDFA0-\uDFC1\uDFC5\uDFC6\uDFC8\uDFC9\uDFCF-\uDFD3\uDFE0-\uDFF0\uDFF8-\uDFFF]|\uD83D[\uDC00-\uDC07\uDC09-\uDC14\uDC16-\uDC3A\uDC3C-\uDC3E\uDC40\uDC44\uDC45\uDC51-\uDC65\uDC6A\uDC79-\uDC7B\uDC7D-\uDC80\uDC84\uDC88-\uDCA9\uDCAB-\uDCFC\uDCFF-\uDD3D\uDD4B-\uDD4E\uDD50-\uDD67\uDDA4\uDDFB-\uDE44\uDE48-\uDE4A\uDE80-\uDEA2\uDEA4-\uDEB3\uDEB7-\uDEBF\uDEC1-\uDEC5\uDED0-\uDED2\uDED5-\uDED7\uDEEB\uDEEC\uDEF4-\uDEFC\uDFE0-\uDFEB]|\uD83E[\uDD0D\uDD0E\uDD10-\uDD17\uDD1D\uDD20-\uDD25\uDD27-\uDD2F\uDD3A\uDD3F-\uDD45\uDD47-\uDD76\uDD78\uDD7A-\uDDB4\uDDB7\uDDBA\uDDBC-\uDDCB\uDDD0\uDDE0-\uDDFF\uDE70-\uDE74\uDE78-\uDE7A\uDE80-\uDE86\uDE90-\uDEA8\uDEB0-\uDEB6\uDEC0-\uDEC2\uDED0-\uDED6]|(?:[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u270A\u270B\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55]|\uD83C[\uDC04\uDCCF\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE1A\uDE2F\uDE32-\uDE36\uDE38-\uDE3A\uDE50\uDE51\uDF00-\uDF20\uDF2D-\uDF35\uDF37-\uDF7C\uDF7E-\uDF93\uDFA0-\uDFCA\uDFCF-\uDFD3\uDFE0-\uDFF0\uDFF4\uDFF8-\uDFFF]|\uD83D[\uDC00-\uDC3E\uDC40\uDC42-\uDCFC\uDCFF-\uDD3D\uDD4B-\uDD4E\uDD50-\uDD67\uDD7A\uDD95\uDD96\uDDA4\uDDFB-\uDE4F\uDE80-\uDEC5\uDECC\uDED0-\uDED2\uDED5-\uDED7\uDEEB\uDEEC\uDEF4-\uDEFC\uDFE0-\uDFEB]|\uD83E[\uDD0C-\uDD3A\uDD3C-\uDD45\uDD47-\uDD78\uDD7A-\uDDCB\uDDCD-\uDDFF\uDE70-\uDE74\uDE78-\uDE7A\uDE80-\uDE86\uDE90-\uDEA8\uDEB0-\uDEB6\uDEC0-\uDEC2\uDED0-\uDED6])|(?:[#\*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u265F\u2660\u2663\u2665\u2666\u2668\u267B\u267E\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26A7\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299]|\uD83C[\uDC04\uDCCF\uDD70\uDD71\uDD7E\uDD7F\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE02\uDE1A\uDE2F\uDE32-\uDE3A\uDE50\uDE51\uDF00-\uDF21\uDF24-\uDF93\uDF96\uDF97\uDF99-\uDF9B\uDF9E-\uDFF0\uDFF3-\uDFF5\uDFF7-\uDFFF]|\uD83D[\uDC00-\uDCFD\uDCFF-\uDD3D\uDD49-\uDD4E\uDD50-\uDD67\uDD6F\uDD70\uDD73-\uDD7A\uDD87\uDD8A-\uDD8D\uDD90\uDD95\uDD96\uDDA4\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA-\uDE4F\uDE80-\uDEC5\uDECB-\uDED2\uDED5-\uDED7\uDEE0-\uDEE5\uDEE9\uDEEB\uDEEC\uDEF0\uDEF3-\uDEFC\uDFE0-\uDFEB]|\uD83E[\uDD0C-\uDD3A\uDD3C-\uDD45\uDD47-\uDD78\uDD7A-\uDDCB\uDDCD-\uDDFF\uDE70-\uDE74\uDE78-\uDE7A\uDE80-\uDE86\uDE90-\uDEA8\uDEB0-\uDEB6\uDEC0-\uDEC2\uDED0-\uDED6])\uFE0F|(?:[\u261D\u26F9\u270A-\u270D]|\uD83C[\uDF85\uDFC2-\uDFC4\uDFC7\uDFCA-\uDFCC]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66-\uDC78\uDC7C\uDC81-\uDC83\uDC85-\uDC87\uDC8F\uDC91\uDCAA\uDD74\uDD75\uDD7A\uDD90\uDD95\uDD96\uDE45-\uDE47\uDE4B-\uDE4F\uDEA3\uDEB4-\uDEB6\uDEC0\uDECC]|\uD83E[\uDD0C\uDD0F\uDD18-\uDD1F\uDD26\uDD30-\uDD39\uDD3C-\uDD3E\uDD77\uDDB5\uDDB6\uDDB8\uDDB9\uDDBB\uDDCD-\uDDCF\uDDD1-\uDDDD])/';
String emojiRegexp2 =
    '   /\uD83C\uDFF4\uDB40\uDC67\uDB40\uDC62(?:\uDB40\uDC77\uDB40\uDC6C\uDB40\uDC73|\uDB40\uDC73\uDB40\uDC63\uDB40\uDC74|\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67)\uDB40\uDC7F|(?:\uD83E\uDDD1\uD83C\uDFFB\u200D\uD83E\uDD1D\u200D\uD83E\uDDD1|\uD83D\uDC69\uD83C\uDFFC\u200D\uD83E\uDD1D\u200D\uD83D\uDC69)\uD83C\uDFFB|\uD83D\uDC68(?:\uD83C\uDFFC\u200D(?:\uD83E\uDD1D\u200D\uD83D\uDC68\uD83C\uDFFB|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFF\u200D(?:\uD83E\uDD1D\u200D\uD83D\uDC68(?:\uD83C[\uDFFB-\uDFFE])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFE\u200D(?:\uD83E\uDD1D\u200D\uD83D\uDC68(?:\uD83C[\uDFFB-\uDFFD])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFD\u200D(?:\uD83E\uDD1D\u200D\uD83D\uDC68(?:\uD83C[\uDFFB\uDFFC])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D)?\uD83D\uDC68|(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D[\uDC66\uDC67])|[\u2695\u2696\u2708]\uFE0F|\uD83D[\uDC66\uDC67]|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|(?:\uD83C\uDFFB\u200D[\u2695\u2696\u2708]|\uD83C\uDFFF\u200D[\u2695\u2696\u2708]|\uD83C\uDFFE\u200D[\u2695\u2696\u2708]|\uD83C\uDFFD\u200D[\u2695\u2696\u2708]|\uD83C\uDFFC\u200D[\u2695\u2696\u2708])\uFE0F|\uD83C\uDFFB\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C[\uDFFB-\uDFFF])|\uD83E\uDDD1(?:\uD83C\uDFFF\u200D\uD83E\uDD1D\u200D\uD83E\uDDD1(?:\uD83C[\uDFFB-\uDFFF])|\u200D\uD83E\uDD1D\u200D\uD83E\uDDD1)|\uD83D\uDC69(?:\uD83C\uDFFE\u200D(?:\uD83E\uDD1D\u200D\uD83D\uDC68(?:\uD83C[\uDFFB-\uDFFD\uDFFF])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFD\u200D(?:\uD83E\uDD1D\u200D\uD83D\uDC68(?:\uD83C[\uDFFB\uDFFC\uDFFE\uDFFF])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFC\u200D(?:\uD83E\uDD1D\u200D\uD83D\uDC68(?:\uD83C[\uDFFB\uDFFD-\uDFFF])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFB\u200D(?:\uD83E\uDD1D\u200D\uD83D\uDC68(?:\uD83C[\uDFFC-\uDFFF])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D(?:\uD83D[\uDC68\uDC69])|\uD83D[\uDC68\uDC69])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD])|\uD83C\uDFFF\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDAF-\uDDB3\uDDBC\uDDBD]))|(?:\uD83E\uDDD1\uD83C\uDFFE\u200D\uD83E\uDD1D\u200D\uD83E\uDDD1|\uD83D\uDC69\uD83C\uDFFF\u200D\uD83E\uDD1D\u200D(?:\uD83D[\uDC68\uDC69]))(?:\uD83C[\uDFFB-\uDFFE])|(?:\uD83E\uDDD1\uD83C\uDFFD\u200D\uD83E\uDD1D\u200D\uD83E\uDDD1|\uD83D\uDC69\uD83C\uDFFE\u200D\uD83E\uDD1D\u200D\uD83D\uDC69)(?:\uD83C[\uDFFB-\uDFFD])|(?:\uD83E\uDDD1\uD83C\uDFFC\u200D\uD83E\uDD1D\u200D\uD83E\uDDD1|\uD83D\uDC69\uD83C\uDFFD\u200D\uD83E\uDD1D\u200D\uD83D\uDC69)(?:\uD83C[\uDFFB\uDFFC])|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC69\u200D\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D[\uDC66\uDC67])|(?:\uD83D\uDC41\uFE0F\u200D\uD83D\uDDE8|\uD83D\uDC69(?:\uD83C\uDFFF\u200D[\u2695\u2696\u2708]|\uD83C\uDFFE\u200D[\u2695\u2696\u2708]|\uD83C\uDFFD\u200D[\u2695\u2696\u2708]|\uD83C\uDFFC\u200D[\u2695\u2696\u2708]|\uD83C\uDFFB\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708])|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDCD-\uDDCF\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uFE0F\u200D[\u2640\u2642]|(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642])|\uD83C\uDFF4\u200D\u2620|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC6F\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3C-\uDD3E\uDDB8\uDDB9\uDDCD-\uDDCF\uDDD6-\uDDDF])\u200D[\u2640\u2642])\uFE0F|\uD83D\uDC69\u200D\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08|\uD83D\uDC69\u200D\uD83D\uDC67|\uD83D\uDC69\u200D\uD83D\uDC66|\uD83D\uDC15\u200D\uD83E\uDDBA|\uD83C\uDDFD\uD83C\uDDF0|\uD83C\uDDF6\uD83C\uDDE6|\uD83C\uDDF4\uD83C\uDDF2|\uD83E\uDDD1(?:\uD83C[\uDFFB-\uDFFF])|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])|\uD83C\uDDFF(?:\uD83C[\uDDE6\uDDF2\uDDFC])|\uD83C\uDDFE(?:\uD83C[\uDDEA\uDDF9])|\uD83C\uDDFC(?:\uD83C[\uDDEB\uDDF8])|\uD83C\uDDFB(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDEE\uDDF3\uDDFA])|\uD83C\uDDFA(?:\uD83C[\uDDE6\uDDEC\uDDF2\uDDF3\uDDF8\uDDFE\uDDFF])|\uD83C\uDDF9(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDED\uDDEF-\uDDF4\uDDF7\uDDF9\uDDFB\uDDFC\uDDFF])|\uD83C\uDDF8(?:\uD83C[\uDDE6-\uDDEA\uDDEC-\uDDF4\uDDF7-\uDDF9\uDDFB\uDDFD-\uDDFF])|\uD83C\uDDF7(?:\uD83C[\uDDEA\uDDF4\uDDF8\uDDFA\uDDFC])|\uD83C\uDDF5(?:\uD83C[\uDDE6\uDDEA-\uDDED\uDDF0-\uDDF3\uDDF7-\uDDF9\uDDFC\uDDFE])|\uD83C\uDDF3(?:\uD83C[\uDDE6\uDDE8\uDDEA-\uDDEC\uDDEE\uDDF1\uDDF4\uDDF5\uDDF7\uDDFA\uDDFF])|\uD83C\uDDF2(?:\uD83C[\uDDE6\uDDE8-\uDDED\uDDF0-\uDDFF])|\uD83C\uDDF1(?:\uD83C[\uDDE6-\uDDE8\uDDEE\uDDF0\uDDF7-\uDDFB\uDDFE])|\uD83C\uDDF0(?:\uD83C[\uDDEA\uDDEC-\uDDEE\uDDF2\uDDF3\uDDF5\uDDF7\uDDFC\uDDFE\uDDFF])|\uD83C\uDDEF(?:\uD83C[\uDDEA\uDDF2\uDDF4\uDDF5])|\uD83C\uDDEE(?:\uD83C[\uDDE8-\uDDEA\uDDF1-\uDDF4\uDDF6-\uDDF9])|\uD83C\uDDED(?:\uD83C[\uDDF0\uDDF2\uDDF3\uDDF7\uDDF9\uDDFA])|\uD83C\uDDEC(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEE\uDDF1-\uDDF3\uDDF5-\uDDFA\uDDFC\uDDFE])|\uD83C\uDDEB(?:\uD83C[\uDDEE-\uDDF0\uDDF2\uDDF4\uDDF7])|\uD83C\uDDEA(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDED\uDDF7-\uDDFA])|\uD83C\uDDE9(?:\uD83C[\uDDEA\uDDEC\uDDEF\uDDF0\uDDF2\uDDF4\uDDFF])|\uD83C\uDDE8(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDEE\uDDF0-\uDDF5\uDDF7\uDDFA-\uDDFF])|\uD83C\uDDE7(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEF\uDDF1-\uDDF4\uDDF6-\uDDF9\uDDFB\uDDFC\uDDFE\uDDFF])|\uD83C\uDDE6(?:\uD83C[\uDDE8-\uDDEC\uDDEE\uDDF1\uDDF2\uDDF4\uDDF6-\uDDFA\uDDFC\uDDFD\uDDFF])|[#\*0-9]\uFE0F\u20E3|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDCD-\uDDCF\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u270A-\u270D]|\uD83C[\uDF85\uDFC2\uDFC7]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66\uDC67\uDC6B-\uDC6D\uDC70\uDC72\uDC74-\uDC76\uDC78\uDC7C\uDC83\uDC85\uDCAA\uDD74\uDD7A\uDD90\uDD95\uDD96\uDE4C\uDE4F\uDEC0\uDECC]|\uD83E[\uDD0F\uDD18-\uDD1C\uDD1E\uDD1F\uDD30-\uDD36\uDDB5\uDDB6\uDDBB\uDDD2-\uDDD5])(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u270A\u270B\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55]|\uD83C[\uDC04\uDCCF\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE1A\uDE2F\uDE32-\uDE36\uDE38-\uDE3A\uDE50\uDE51\uDF00-\uDF20\uDF2D-\uDF35\uDF37-\uDF7C\uDF7E-\uDF93\uDFA0-\uDFCA\uDFCF-\uDFD3\uDFE0-\uDFF0\uDFF4\uDFF8-\uDFFF]|\uD83D[\uDC00-\uDC3E\uDC40\uDC42-\uDCFC\uDCFF-\uDD3D\uDD4B-\uDD4E\uDD50-\uDD67\uDD7A\uDD95\uDD96\uDDA4\uDDFB-\uDE4F\uDE80-\uDEC5\uDECC\uDED0-\uDED2\uDED5\uDEEB\uDEEC\uDEF4-\uDEFA\uDFE0-\uDFEB]|\uD83E[\uDD0D-\uDD3A\uDD3C-\uDD45\uDD47-\uDD71\uDD73-\uDD76\uDD7A-\uDDA2\uDDA5-\uDDAA\uDDAE-\uDDCA\uDDCD-\uDDFF\uDE70-\uDE73\uDE78-\uDE7A\uDE80-\uDE82\uDE90-\uDE95])|(?:[#\*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u265F\u2660\u2663\u2665\u2666\u2668\u267B\u267E\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299]|\uD83C[\uDC04\uDCCF\uDD70\uDD71\uDD7E\uDD7F\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE02\uDE1A\uDE2F\uDE32-\uDE3A\uDE50\uDE51\uDF00-\uDF21\uDF24-\uDF93\uDF96\uDF97\uDF99-\uDF9B\uDF9E-\uDFF0\uDFF3-\uDFF5\uDFF7-\uDFFF]|\uD83D[\uDC00-\uDCFD\uDCFF-\uDD3D\uDD49-\uDD4E\uDD50-\uDD67\uDD6F\uDD70\uDD73-\uDD7A\uDD87\uDD8A-\uDD8D\uDD90\uDD95\uDD96\uDDA4\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA-\uDE4F\uDE80-\uDEC5\uDECB-\uDED2\uDED5\uDEE0-\uDEE5\uDEE9\uDEEB\uDEEC\uDEF0\uDEF3-\uDEFA\uDFE0-\uDFEB]|\uD83E[\uDD0D-\uDD3A\uDD3C-\uDD45\uDD47-\uDD71\uDD73-\uDD76\uDD7A-\uDDA2\uDDA5-\uDDAA\uDDAE-\uDDCA\uDDCD-\uDDFF\uDE70-\uDE73\uDE78-\uDE7A\uDE80-\uDE82\uDE90-\uDE95])\uFE0F|(?:[\u261D\u26F9\u270A-\u270D]|\uD83C[\uDF85\uDFC2-\uDFC4\uDFC7\uDFCA-\uDFCC]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66-\uDC78\uDC7C\uDC81-\uDC83\uDC85-\uDC87\uDC8F\uDC91\uDCAA\uDD74\uDD75\uDD7A\uDD90\uDD95\uDD96\uDE45-\uDE47\uDE4B-\uDE4F\uDEA3\uDEB4-\uDEB6\uDEC0\uDECC]|\uD83E[\uDD0F\uDD18-\uDD1F\uDD26\uDD30-\uDD39\uDD3C-\uDD3E\uDDB5\uDDB6\uDDB8\uDDB9\uDDBB\uDDCD-\uDDCF\uDDD1-\uDDDD])/';

/// If uploaded authenticate, change to date '1900-01-01' in server
String AUTH_READY = "1900-01-01";

/// Auth failed
String AUTH_FAIL = "1901-01-01";

String COOKIE_NAME_TOKEN = 'token';

String LOCALE = "ko-KR";

class StringUtils {
  /// 이메일 유효성 검사
  static String? validateEmail(String? value) {
    String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = RegExp(pattern);
    if (value!.length == 0) {
      return "에러_아이디_확인".tr();
    } else if (!regExp.hasMatch(value)) {
      return "에러_아이디_확인2".tr();
    } else {
      return null;
    }
  }

  static String? validateId(String value) {
    String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      return "에러_아이디_확인2".tr();
    } else {
      return null;
    }
  }

  ///비밀번호 유효성 검사
  static String? validatePw(String? value) {
    // String pattern = r'^[A-Za-z0-9]{6,12}$';
    // String pattern = r'^.*(?=^.{6,32}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$';
    // String pattern = r'^((/^.*(?=^.{6,32}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/))$';
    // String pattern = r'/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,32}$/';
    // RegExp regExp = RegExp(pattern);

    final validNumbers = RegExp(r'(\d+)');
    final validAlphabet = RegExp(r'[a-zA-Z]');
    final validSpecial = RegExp(r'^[a-zA-Z0-9 ]+$');
    if (value == null) {
      return "에러_비밀번호확인".tr();
    } else if (value.isEmpty || value.length < 8) {
      return "에러_비밀번호확인".tr();
    } else if (validSpecial.hasMatch(value)) {
      //특수기호가 있는지 확인
      return "비밀번호찾기_에러_특수문자".tr();
    } else if (!validAlphabet.hasMatch(value)) {
      //문자가 있는지 확인
      return "비밀번호찾기_에러_영문".tr();
    } else if (!validNumbers.hasMatch(value)) {
      //숫자가 있는지 확인
      return "비밀번호찾기_에러_숫자".tr();
    } else {
      return null;
    }
  }

  ///비밀번호 확인 유효성 검사
  static String? validatePwConfirm(String value, String value2) {
    String password = validatePw(value) ?? '';
    String passwordConfirm = validatePw(value2) ?? '';
    if (password.isEmpty && passwordConfirm.isEmpty) {
      if (value == value2) {
        return null;
      } else {
        return "에러_비밀번호불일치".tr();
      }
    } else {
      if (value == value2) {
        return null;
      } else {
        return "에러_비밀번호불일치".tr();
      }
    }
  }

  ///이름 유효성 검사
  static String? validateName(String? value) {
    String pattern = r'^[^ㄱ-ㅎㅏ-ㅣ0-9\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\"]*[가-힣]{2,}|[a-zA-Z]{2,}[^ㄱ-ㅎㅏ-ㅣ0-9\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\"]*$';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value!)) {
      return "에러_이름".tr();
    } else {
      return null;
    }
  }

  ///휴대폰번호 유효성 검사
  static String? validateMobile(String? value) {
    String pattern = "";
    if (value!.length < 11)
      pattern = r'(^(\+?01[1|6|7|8|9][0-9]{7})$)';
    else
      pattern = r'(^(\+?010[0-9]{8})$)';
    RegExp regExp = new RegExp(pattern);
    log.d('『GGUMBI』>>> validateMobile : value: $value,  <<< ');
    if (!validateString(value) || value.length > 11 || value.length < 10) {
      return "에러_휴대폰번호".tr();
    } else if (!regExp.hasMatch(value)) {
      return "에러_휴대폰번호".tr();
    }
    return null;
  }

  ///휴대폰번호 유효성 검사
  static String validateMobileLength(String value) {
    log.d('『GGUMBI』>>> validateMobileLength : value: $value, value.length: ${value.length}, <<< ');
    if (!validateString(value) || value.length < 11) {
      return "에러_휴대폰번호".tr();
    }
    return '';
  }

  ///인증번호 유효성 검사
  static String validateAuthNumber(String value) {
    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    log.d('『GGUMBI』>>> validateAuthNumber : value: $value, value.length: ${value.length}, <<< ');
    RegExp regExp = new RegExp(pattern);
    if (!validateString(value) || value.length < 6) {
      return "에러_인증번호".tr();
    } else if (!regExp.hasMatch(value)) {
      return "에러_인증번호".tr();
    }
    return '';
  }

  ///휴대폰번호 유효성 검사
  static String validateAuthNumberLength(String value) {
    log.d('『GGUMBI』>>> validateAuthNumberLength : value: $value, value.length: ${value.length}, <<< ');
    if (!validateString(value) || value.length < 6) {
      return "에러_인증번호".tr();
    } else if (value.length > 6) {
      return "에러_인증번호".tr();
    }
    return '';
  }

  ///출발 주소 유효성 검사
  static String validateAddress(String? value) {
    log.d('『GGUMBI』>>> validateDetailAddress : value: $value, value.length: ${value!.length}, <<< ');
    if (!validateString(value)) {
      return "장소_에러".tr();
    }
    return '';
  }

  ///상세주소 유효성 검사
  static String validateDetailAddress(String? value) {
    log.d('『GGUMBI』>>> validateDetailAddress : value: $value, value.length: ${value!.length}, <<< ');
    if (!validateString(value)) {
      return "장소_상세_힌트".tr();
    }
    return '';
  }

  ///포인트 유효성 검사
  static String validatePoint(String? value, String? max) {
    if (int.parse(value!.replaceAll(',', '')) > int.parse(max!.replaceAll(',', ''))) {
      return "포인트가부족합니다.".tr();
    }
    return '';
  }

  ///캐시 유효성 검사
  static String validateCash(String? value, String? max) {
    if (int.parse(value!.replaceAll(',', '')) > int.parse(max!.replaceAll(',', ''))) {
      return "캐시가부족합니다".tr();
    }
    return '';
  }

  ///알러지 유효성 검사
  static String validateAllergy(String? value) {
    if (!validateString(value)) {
      return "알레르기가_힌트".tr();
    }
    return '';
  }

  ///true, false
  static bool validateString(String? value) {
    if (value == null || value.isEmpty || value.length == 0) {
      return false;
    }
    return true;
  }

  ///true, false
  static bool getStringToBool(String? value) {
    if (value == null || value.length == 0) {
      return false;
    }
    return value == 'true' ? true : false;
  }

  ///true, false -> 1, 0
  static int getBoolToInt(bool isCheck) {
    return isCheck ? 1 : 0;
  }

  ///1, 0 -> true, false
  static bool getIntToBool(int value) {
    return value == 1 ? true : false;
  }

  /// 디바이스정보
  /// key : User-Agent
  /// value : 디바이스(OS버전)_제조사_모델명
  static String getUserAgent(String deviceType, String? osVersion, String? brandName, String? modelName) {
    StringBuffer stringBuffer = StringBuffer();
    String gubun = "_";
    stringBuffer.write(deviceType);
    stringBuffer.write(gubun);

    if (osVersion != null && osVersion.isEmpty) {
      return stringBuffer.toString().toLowerCase();
    }
    stringBuffer.write("(" + (osVersion ?? '') + ")");
    stringBuffer.write(gubun);

    if (brandName != null && brandName.isEmpty) {
      return stringBuffer.toString().toLowerCase();
    }
    stringBuffer.write(brandName);
    stringBuffer.write(gubun);

    if (modelName != null && modelName.isEmpty) {
      return stringBuffer.toString().toLowerCase();
    }
    stringBuffer.write(modelName);

    log.d('『GGUMBI』>>> getUserAgent : stringBuffer: $stringBuffer,  <<< ');
    return stringBuffer.toString().toLowerCase();
  }

  /// 디바이스정보
  /// value : 디바이스(OS버전)
  static String getDeviceVersion(String deviceType, String osVersion) {
    StringBuffer stringBuffer = StringBuffer();
    stringBuffer.write(deviceType);

    if (osVersion.isEmpty) {
      return stringBuffer.toString().toLowerCase();
    }
    stringBuffer.write("(" + osVersion + ")");

    log.d('『GGUMBI』>>> getDeviceVersion : stringBuffer: $stringBuffer,  <<< ');
    return stringBuffer.toString().toLowerCase();
  }

  /// 일반, 수요자, 공급자
  static USER_TYPE validateUserType(String value) {
    if (value.isEmpty) {
      return USER_TYPE.mom_daddy;
    }

    if (value == USER_TYPE.mom_daddy.toString()) {
      return USER_TYPE.mom_daddy;
    } else if (value == USER_TYPE.link_mom.toString()) {
      return USER_TYPE.link_mom;
    } else {
      return USER_TYPE.mom_daddy;
    }
  }

  ///시간 앞자리 붙여주기
  static String getNumberAddZero(int number) {
    if (number < 10) {
      return "0" + number.toString();
    }
    return number.toString();
  }

  ///시간 앞자리 붙여주기
  static String formatDuration(Duration d) {
    String f(int n) {
      return n.toString().padLeft(2, '0');
    }

    // We want to round up the remaining time to the nearest second
    d += Duration(microseconds: 999999);
    return "${f(d.inMinutes)}:${f(d.inSeconds % 60)}";
  }

  static String getEmail(String id, String dropValue) {
    log.d({
      'id': id,
      'dropValue': dropValue,
    });
    if (!validateString(id)) {
      return "";
    }

    StringBuffer sb = StringBuffer();
    if (dropValue != "직접입력".tr()) {
      String email = id;
      if (id.contains("@")) {
        email = id.substring(0, id.indexOf("@"));
      }
      sb.write(email);
      sb.write('@');
      sb.write(dropValue);
      log.d('『GGUMBI』>>> getEmail : sb.toString(): ${sb.toString()} , 111  <<< ');
    } else {
      sb.write(id);
      log.d('『GGUMBI』>>> getEmail : sb.toString(): ${sb.toString()}, 222  <<< ');
    }
    log.d('『GGUMBI』>>> getEmail : sb.toString(): ${sb.toString()}, 333 <<< ');
    return sb.toString();
  }

  static String getBirthToString(String valueTime, {String type = ''}) {
    StringBuffer sbBirth = StringBuffer();
    try {
      DateTime dateTime = DateTime.parse(valueTime);
      String year = dateTime.year.toString();
      String month = dateTime.month < 10 ? "0${dateTime.month.toString()}" : "${dateTime.month.toString()}";
      String day = dateTime.day < 10 ? "0${dateTime.day.toString()}" : "${dateTime.day.toString()}";

      if (type == '-') {
        sbBirth.write(year);
        sbBirth.write('-');
        sbBirth.write(month);
        sbBirth.write('-');
        sbBirth.write(day);
      } else {
        sbBirth.write(year);
        sbBirth.write("년".tr() + ' ');
        sbBirth.write(month);
        sbBirth.write("월".tr() + ' ');
        sbBirth.write(day);
        sbBirth.write("일".tr());
      }
    } catch (e) {
      sbBirth.write(valueTime);
      log.e('『GGUMBI』 Exception >>> getBirthToString : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return sbBirth.toString();
  }

  static String getBirthToStringYMD(String valueTime) {
    StringBuffer sbBirth = StringBuffer();
    try {
      List<String> lsBirth = valueTime.split('-');
      sbBirth.write(lsBirth[0]);
      sbBirth.write("년".tr() + ' ');
      sbBirth.write(lsBirth[1]);
      sbBirth.write("월".tr() + ' ');
      sbBirth.write(lsBirth[2]);
      sbBirth.write("일".tr());
    } catch (e) {
      sbBirth.write(valueTime);
      log.e('『GGUMBI』 Exception >>> getBirthToString : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return sbBirth.toString();
  }

  static List<String> getBirthToDateTime(String valueTime) {
    List<String> sbBirth = [];
    try {
      sbBirth = valueTime.split('-');
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getBirthToDateTime : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return sbBirth;
  }

  ///돌봄 시간 선택시 사용
  static DateTime getTimeToStringFormat(String time) {
    DateTime now = DateTime.now();
    DateTime value = DateFormat().addPattern(F_HMM).parse(time);
    value = DateTime(now.year, now.month, now.day, value.hour, value.minute);
    return value;
  }

  ///돌봄 신청시 현재 시간을 세팅
  static String getTimeToString(DateTime time) {
    // DateTime now = DateTime.now();

    // int min = now.minute - (now.minute % 10);
    // now = DateTime(now.year, now.month, now.day, now.hour, min, 00);
    // final dateFormat = DateFormat('a hh:mm', 'ko');
    final dateFormat = DateFormat('a hh:mm', 'ko');
    log.d('『GGUMBI』>>> getTimeToString : time: $time,  ${dateFormat.format(time)} <<< ');
    return dateFormat.format(time);
  }

  ///돌봄 신청시 출발시간 포멧 00:00
  static String getTimeToStartTime(DateTime time) {
    final dateFormat = DateFormat('HH:mm');
    log.d('『GGUMBI』>>> getTimeToString : time: $time,  ${dateFormat.format(time)} <<< ');
    return dateFormat.format(time);
  }

  ///돌봄 신청 서버데이터를 시간으로 만들기
  static DateTime getTimeToDateTime(String time) {
    // log.d('『GGUMBI』>>> getTimeToDateTime : time: $time,  <<< ');
    if (!validateString(time)) {
      time = "09:00";
    }
    var times = time.split(":");
    // log.d('『GGUMBI』>>> getTimeToDateTime : times.length: ${times.length},  <<< ');
    if (times.length < 3) {
      // log.d('『GGUMBI』>>> getTimeToDateTime : times.length: ${times.length},  <<< ');
      times.add("00");
    }
    // log.d('『GGUMBI』>>> getTimeToDateTime : times[0] as int: ${int.parse(times[0])},  <<< ');
    DateTime dateTime = DateTime.now();
    dateTime = DateTime(dateTime.year, dateTime.month, dateTime.day, int.parse(times[0]), int.parse(times[1]), int.parse(times[2]));
    // log.d('『GGUMBI』>>> getTimeToDateTime : dateTime: $dateTime,  <<< ');
    return dateTime;
  }

  ///돌봄 신청 내역 시간 표시 오전 09:00
  static String getStringToTime(String time, {String format = 'a hh:mm'}) {
    // log.d('『GGUMBI』>>> getTimeToDateTime : time: $time,  <<< ');
    if (!validateString(time)) {
      time = "";
      return time;
    }
    var times = time.split(":");
    // log.d('『GGUMBI』>>> getTimeToDateTime : times.length: ${times.length},  <<< ');
    if (times.length < 3) {
      // log.d('『GGUMBI』>>> getTimeToDateTime : times.length: ${times.length},  <<< ');
      times.add("00");
    }
    // log.d('『GGUMBI』>>> getTimeToDateTime : times[0] as int: ${int.parse(times[0])},  <<< ');
    DateTime dateTime = DateTime.now();
    dateTime = DateTime(dateTime.year, dateTime.month, dateTime.day, int.parse(times[0]), int.parse(times[1]), int.parse(times[2]));
    // log.d('『GGUMBI』>>> getTimeToDateTime : dateTime: $dateTime,  <<< ');
    final dateFormat = DateFormat(format, 'ko');
    // log.d('『GGUMBI』>>> getTimeToString : time: $time,  ${dateFormat.format(dateTime)} <<< ');
    time = dateFormat.format(dateTime);
    return time;
  }

  ///돌봄,구직 신청 내역 날짜 표시 5월 3일(월)
  static String getStringToDate(String date, {String format = 'M월 d일(E)'}) {
    // log.d('『GGUMBI』>>> getStringToDate : date: $date, $format  <<< ');
    if (!validateString(date)) {
      date = "";
      return date;
    }
    final dateFormat = DateFormat(format);
    DateTime valueDate = DateTime.parse(date);
    if (valueDate.year > DateTime.now().year) {
      return DateFormat('yy${"년".tr()} ' + format).format(valueDate);
    }
    date = dateFormat.format(valueDate);
    return date;
  }

  ///돌봄,구직 신청 내역 날짜 표시 5월 3일(월)
  static String getStringToDateYY(String firstDate, String lastDate) {
    // log.d('『GGUMBI』>>> getStringToDate : date: $date, $format  <<< ');
    String value = '';
    if (!validateString(firstDate) && !validateString(lastDate)) {
      return value;
    }
    String format = F_MMDDE;
    String formatYY = 'yy.' + F_MMDDE;

    final dateFormat = DateFormat(format);
    final dateFormatYY = DateFormat(formatYY);
    DateTime valueFirst = DateTime.parse(firstDate);
    DateTime valueLast = DateTime.parse(lastDate);
    int currentYear = DateTime.now().year;

    log.d('『GGUMBI』>>> getStringToDateYY : ========= : ${valueFirst.year}, ${valueLast.year}, $currentYear  <<< ');
    //시작,끝 날짜가 같고, 현재 년도와 같을시 01.01(토) - 12.31(일)
    if (valueFirst.year == valueLast.year && valueFirst.year == currentYear && valueLast.year == currentYear) {
      if (valueFirst.compareTo(valueLast) == 0) {
        value = dateFormat.format(valueFirst);
      } else {
        value = '${dateFormat.format(valueFirst)} - ${dateFormat.format(valueLast)}';
      }
      log.d('『GGUMBI』>>> getStringToDateYY : 2 : ${valueFirst.year}, ${valueLast.year}, $currentYear, $value, valueFirst: $valueFirst, valueLast: $valueLast  <<< ');
      return value;
    }

    //시작,끝 날짜가 다르고, 현재 년도 모두 다를시 21.01.01(토) - 23.12.31(일)
    if (valueFirst.year != valueLast.year && valueFirst.year < currentYear && valueLast.year > currentYear) {
      value = '${dateFormatYY.format(valueFirst)} - ${dateFormatYY.format(valueLast)}';
      log.d('『GGUMBI』>>> getStringToDateYY : 3 : ${valueFirst.year}, ${valueLast.year}, $currentYear, $value, valueFirst: $valueFirst, valueLast: $valueLast  <<< ');
      return value;
    }

    //시작,끝 날짜가 다르고, 현재 년도와 시작이 같고, 끝 날짜가 다를경우 01.01(토) - 23.12.31(일)
    if (valueFirst.year != valueLast.year && valueFirst.year == currentYear && valueLast.year > currentYear) {
      value = '${dateFormat.format(valueFirst)} - ${dateFormatYY.format(valueLast)}';
      log.d('『GGUMBI』>>> getStringToDateYY : 4 : ${valueFirst.year}, ${valueLast.year}, $currentYear, $value, valueFirst: $valueFirst, valueLast: $valueLast  <<< ');
      return value;
    }

    //시작,끝 날짜가 다르고, 현재 년도와 시작이 같고, 끝 날짜가 다를경우 01.01(토) - 23.12.31(일)
    if (valueFirst.year != valueLast.year && valueFirst.year < currentYear && valueLast.year == currentYear) {
      value = '${dateFormatYY.format(valueFirst)} - ${dateFormat.format(valueLast)}';
      log.d('『GGUMBI』>>> getStringToDateYY : 5 : ${valueFirst.year}, ${valueLast.year}, $currentYear, $value, valueFirst: $valueFirst, valueLast: $valueLast  <<< ');
      return value;
    }
    log.d('『GGUMBI』>>> getStringToDateYY : 6 : ${valueFirst.year}, ${valueLast.year}, $currentYear, $value, valueFirst: $valueFirst, valueLast: $valueLast  <<< ');
    return '${dateFormat.format(valueFirst)} - ${dateFormat.format(valueLast)}';
  }

  ///현재시간 포맷팅
  static String getStringToNewDate(String date, {String format = 'M월 d일(E) a h:mm:ss'}) {
    log.d('『GGUMBI』>>> getStringToDate : date: $date,  <<< ');
    if (!validateString(date)) {
      date = "";
      return date;
    }
    final dateFormat = DateFormat(format);
    DateTime valueDate = DateTime.parse(date);
    date = dateFormat.format(valueDate);
    return date;
  }

  ///채팅 보낸 시간
  static String getToSendTime() {
    final dateFormat = DateFormat(F_YYYYMMDDHHMMSS);
    String date = dateFormat.format(DateTime.now());
    return date;
  }

  ///채팅 보낸 시간
  static int getToSendTimeInt() {
    final dateFormat = DateFormat(F_YYYYMMDDHHMMSS);
    String date = dateFormat.format(DateTime.now());
    return int.parse(date);
  }

  ///계약서 날짜
  static String getContractDate(String date) {
    if (validateString(date)) {
      final dateFormat = DateFormat(F_YYYYMDHHMM);
      DateTime valueDate = DateTime.parse(date);
      date = dateFormat.format(valueDate);
    } else {
      date = '';
    }
    return date;
  }

  ///1000 -> 1,000
  static String formatPay(int? price) {
    if (price == null) {
      return "0";
    }
    var f = NumberFormat("#,###");
    return f.format(price);
  }

  ///총시간, 선택시간 -> 00시00분
  static String formatCalcuTime(int totalTime, int selectTime) {
    DateFormat formatTime = DateFormat('H시간 mm분');
    DateFormat formatHour = DateFormat('H시간');
    DateFormat formatMinute = DateFormat('mm분');
    int calcuTime = totalTime - selectTime;
    int hour = calcuTime ~/ 60;
    int min = calcuTime - (hour * 60);

    log.d('『GGUMBI』>>> formatCalcuTime : calcuTime: $calcuTime, totalTime: $totalTime, selectTime: $selectTime, $hour, $min <<< ');
    String time = '';
    DateTime dateTime;
    if (calcuTime <= 0) {
      hour = 0;
      min = 0;
      dateTime = DateTime(DateTime.now().year, 1, 1, hour, min, 0);
      time = formatMinute.format(dateTime);
    } else {
      if (min <= 0) {
        min = 0;
      }
      dateTime = DateTime(DateTime.now().year, 1, 1, hour, min, 0);
      time = formatTime.format(dateTime);
      if (totalTime == 60 && min == 0) {
        time = formatHour.format(dateTime);
      } else if (totalTime <= 60) {
        time = formatMinute.format(dateTime);
      }
    }

    return time;
  }

  ///총시간, 선택시간 -> 00시00분
  static String careTotalTime(String startTime, String endTime) {
    DateFormat formatTime = DateFormat('H시간 mm분');
    DateFormat formatHour = DateFormat('H시간');
    DateFormat formatMinute = DateFormat('mm분');
    DateFormat formatText = DateFormat('HH:mm');

    DateTime startDate = formatText.parse(startTime);
    DateTime endDate = formatText.parse(endTime);

    DateTime calcuDate = endDate.subtract(Duration(hours: startDate.hour, minutes: startDate.minute));

    DateTime dateTime = DateTime(DateTime.now().year, 1, 1, 1, 0, 0);
    String time = '';
    calcuDate = DateTime(DateTime.now().year, 1, 1, calcuDate.hour, calcuDate.minute, 0);
    if (calcuDate.compareTo(dateTime) >= 0) {
      time = formatTime.format(calcuDate);
      if (calcuDate.minute == 60 || calcuDate.minute <= 0) {
        time = formatHour.format(calcuDate);
      }
    } else {
      time = formatMinute.format(calcuDate);
    }

    return time;
  }

  static String? confirmPw(String? value) {
    if (value!.isNotEmpty && value.length <= 6) {
      return "에러_비밀번호확인".tr();
    } else {
      return null;
    }
  }

  static bool isValidatePw(String value) {
    // String pattern = r'^[A-Za-z0-9]{6,12}$';
    // String pattern = r'^.*(?=^.{6,32}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$';
    // String pattern = r'^((/^.*(?=^.{6,32}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&+=]).*$/))$';
    // String pattern = r'/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,32}$/';
    // RegExp regExp = RegExp(pattern);

    final validNumbers = RegExp(r'(\d+)');
    final validAlphabet = RegExp(r'[a-zA-Z]');
    final validSpecial = RegExp(r'^[a-zA-Z0-9 ]+$');

    if (value.length == 0 || value.length < 8) {
      return false;
    } else if (validSpecial.hasMatch(value)) {
      //특수기호가 있는지 확인
      return false;
    } else if (!validAlphabet.hasMatch(value)) {
      //문자가 있는지 확인
      return false;
    } else if (!validNumbers.hasMatch(value)) {
      //숫자가 있는지 확인
      return false;
    } else {
      return true;
    }
  }

  static List<String> nameToFormat(String name) {
    List<String> value = [];
    try {
      if (StringUtils.validateString(name)) {
        value.add(name.substring(0, 1));
        value.add(name.substring(1, name.length));
      }
      return value;
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> nameToFormat : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return value;
    }
  }

  static String hpnumberFormat(String hpnumber) {
    String hpPrefix = "010";
    if (hpnumber.isEmpty) return '';
    if (hpnumber.substring(0, 3) != hpPrefix) {
      return hpnumber.substring(0, 3) + '-' + hpnumber.substring(3, 6) + '-' + hpnumber.substring(6, hpnumber.length);
    } else {
      return hpnumber.substring(0, 3) + '-' + hpnumber.substring(3, 7) + '-' + hpnumber.substring(7, hpnumber.length);
    }
  }

  /// yyyy-MM-dd to DateTime
  static DateTime parseYMD(String date) {
    if (date.isEmpty) return DateTime.now();
    return DateFormat().addPattern('yyyy-MM-dd').parse(date);
  }

  /// DateTime to yyyy-MM-dd
  static String formatYMD(DateTime date) {
    DateFormat _dateFormat = DateFormat().addPattern("yyyy-MM-dd");
    return _dateFormat.format(date);
  }

  /// DateTime to yyyy.MM.dd
  static String formatDotYMD(DateTime date) {
    return DateFormat().addPattern("yyyy.MM.dd").format(date);
  }

  /// DateTime to HH:mm
  static String formatHD(DateTime date) {
    return DateFormat().addPattern('HH:mm').format(date);
  }

  /// HH:mm to DateTime
  static DateTime parseHD(String date) {
    if (date.isEmpty) return DateTime.now();
    return DateFormat().addPattern('HH:mm').parse(date);
  }

  ///채팅방 리스트 날짜
  static String getDateTime(String time, {Function? callBack}) {
    String value = time;
    String formatType = F_HMM;
    // log.d('『GGUMBI』>>> getDateTime : time: $time,  <<< ');
    try {
      DateTime today = DateTime.now();
      DateTime chatDate = DateTime.parse(time);
      // log.d('『GGUMBI』>>> getDateTime : today.year: ${today.year}, chatDate.year: ${chatDate.year}, <<< ');
      //년도가 다르면
      if (today.year != chatDate.year) {
        value = StringUtils.getStringToDate(time, format: F_YYMMDD);
        formatType = F_YYMMDD;
      } else {
        // log.d('『GGUMBI』>>> getDateTime : today.year: ${today}, chatDate.year: ${chatDate}, ${today.isAfter(chatDate)}<<< ');
        today = DateTime(today.year, today.month, today.day, today.hour, today.minute);
        chatDate = DateTime(chatDate.year, chatDate.month, chatDate.day, chatDate.hour, chatDate.minute);
        //이전
        if (today.isAfter(chatDate)) {
          int day = (today.day - chatDate.day).abs();
          // log.d('『GGUMBI』>>> getDateTime : day: $day,  <<< ');
          //오늘
          if (day == 0) {
            value = StringUtils.getStringToDate(time, format: F_HMM);
            formatType = F_HMM;
            //1일보다 크면 일반 날짜, 아니면 어제로 표기한다.
          } else if (day == 1) {
            value = "어제";
            formatType = F_MMDD;
            //1일보다 클 경우
          } else {
            value = StringUtils.getStringToDate(time, format: F_MMDD);
            formatType = F_MMDD;
          }
          //기타...
        } else {
          value = StringUtils.getStringToDate(time, format: F_HMM);
          formatType = F_HMM;
        }
      }

      if (callBack != null) {
        callBack(formatType);
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getDateTime : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return value;
  }

  /// MM.dd(E)
  static String parseMDE(String date) {
    if (date.isEmpty) return '';
    DateTime dateTime = DateFormat().addPattern('yyyy-MM-dd').parse(date);
    return DateFormat().addPattern(F_MMDDE).format(dateTime);
  }

  /// yyyy.MM.dd
  static String parseDotYMD(String date) {
    if (date.isEmpty) return '';
    DateTime dateTime = parseYMD(date);
    return DateFormat().addPattern("yyyy.MM.dd").format(dateTime);
  }

  /// a h:mm
  static String parseAHMM(String date) {
    if (date.isEmpty) return '';
    DateTime dateTime = parseHD(date);
    return DateFormat().addPattern(F_HMM).format(dateTime);
  }

  ///근무날짜
  static String getCareDate(List<String> date) {
    date.sort();
    String value = '';
    String min = date.first;
    String max = date.last;
    log.d('『GGUMBI』>>> getCareDate : min: $min, max: $max, ${(min == max)} <<< ');
    if (min == max) {
      int year = int.parse(min.split("-")[0]);
      DateTime now = DateTime.now();
      value = StringUtils.getStringToDate(min, format: F_MMDDE);
    } else {
      String minFormat = F_YYMMDDE;
      String maxFormat = F_YYMMDDE;
      var lsMin = min.split("-");
      var lsMax = min.split("-");
      int minYear = int.parse(lsMin[0]);
      int maxYear = int.parse(lsMax[0]);
      //앞 년도가 같으면 월까지만 표시
      if (minYear == maxYear) {
        minFormat = F_MMDDE;
        maxFormat = F_MMDDE;
      } else if (maxYear > minYear) {
        minFormat = F_MMDDE;
        maxFormat = F_YYMMDDE;
      } else {
        minFormat = F_YYMMDDE;
        maxFormat = F_MMDDE;
      }
      value = '${StringUtils.getStringToDate(min, format: minFormat)} ~ ${StringUtils.getStringToDate(max, format: maxFormat)}';
    }
    return value;
  }

  static String getCareDateCount(List<String> dateList, {bool showYear = false}) {
    String type = '';
    try {
      dateList.sort();
      int dateCnt = dateList.length;
      String date = dateList.first;
      if (showYear) {
        type = getStringToDate(date, format: F_YYMMDDE);
      } else {
        type = parseMDE(date);
      }
      if (dateCnt > 1) {
        type += '${"외".tr()} ${dateCnt - 1}${"일".tr()}';
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getCareDate : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }

    return type;
  }

  static String getCareTime(List<String> timeList) {
    return '${parseAHMM(timeList.first)} ~ ${parseAHMM(timeList.last)}';
  }

  /// yyyy.MM.dd (E)
  static String parseYMDE(DateTime date) {
    return DateFormat().addPattern("yyyy.MM.dd (E)").format(date);
  }

  ///종료시간 - 시작시간
  static String getCareTimeCalcu(List<String> timeList) {
    DateTime start = parseHD(timeList.first);
    DateTime end = parseHD(timeList.last);

    log.d('『GGUMBI』>>> getCareTimeCalcu : start: $start, end: $end, ${end.subtract(Duration(hours: start.hour))}<<< ');
    log.d('『GGUMBI』>>> getCareTimeCalcu : start: $start, end: $end, ${start.difference(end)}<<< ');
    return formatCalcuTime(end.subtract(Duration(hours: start.hour)).minute, 0);
  }

  /// MM$월 dd일 if(isShowMin) HH:mm else HH시
  static String payDeadline(String date, {bool showMin = false}) {
    if (date.isNotEmpty) {
      DateTime dateTime = DateFormat().addPattern('yyyy-MM-dd HH:mm').parse(date);
      if (!showMin) {
        return DateFormat().addPattern('MM${"월".tr()} dd${"일".tr()} a hh${"시".tr()}').format(dateTime);
      } else {
        return DateFormat().addPattern('MM${"월".tr()} dd${"일".tr()} a hh:mm').format(dateTime);
      }
    } else {
      return '';
    }
  }

  static String parseDateForNormal(String date) {
    if (date.length < 8) return '';
    String y = date.substring(0, 4);
    String m = date.substring(4, 6);
    String d = date.substring(6, 8);
    return '$y-$m-$d';
  }

  ///등원,하원,보육 구분 처리 (ServiceType 돌봄서비스 구분(0:등원(go_school), 1:하원(go_home), 2:보육(nursery), 3:학습(learning), 4:입주가사(housework), 5:이유식반찬(food), 전체 요청 99))
  static String getServiceTypeString(int serviceType) {
    String value = '';
    if (serviceType == ServiceType.serviceType_0.index) {
      value = "등원".tr();
    } else if (serviceType == ServiceType.serviceType_1.index) {
      value = "하원".tr();
    } else if (serviceType == ServiceType.serviceType_2.index) {
      value = "보육".tr();
    } else if (serviceType == ServiceType.serviceType_3.index) {
      value = "학습".tr();
    } else if (serviceType == ServiceType.serviceType_4.index) {
      value = "입주가사".tr();
    } else if (serviceType == ServiceType.serviceType_5.index) {
      value = "이유식반찬".tr();
    }
    return value;
  }

  /// yyyy-MM-dd HH:mm -> A HH:mm
  static String getTimeYMDHM(String date) {
    if (date.isNotEmpty) {
      DateTime dateTime = DateFormat().addPattern('yyyy-MM-dd hh:mm').parse(date);
      return DateFormat().addPattern(F_HMM).format(dateTime);
    } else {
      return "";
    }
  }

  /// yyyy-MM-dd HH:mm -> yyyy-MM-dd
  static String getDateYMDHM(String date) {
    if (date.isNotEmpty) {
      DateTime dateTime = DateFormat().addPattern('yyyy-MM-dd hh:mm').parse(date);
      return DateFormat().addPattern(F_MMDDE).format(dateTime);
    } else {
      return "";
    }
  }

  /// yyyy-MM-dd HH:mm -> DateTime(),
  static DateTime parseYMDHM(String date) {
    if (date.isEmpty) return DateTime.now();
    return DateFormat().addPattern('yyyy-MM-dd HH:mm').parse(date);
  }

  /// yy. MM. dd AHH:mm
  static String parseYMDAHM(String date) {
    if (date.isEmpty) return "";
    DateTime time = DateFormat().addPattern('yyyy-MM-dd HH:mm').parse(date);
    return DateFormat().addPattern('yy. MM. dd a hh:mm').format(time);
  }

  /// DateTime -> String / yyyy년 MM월 dd일
  static String parseYMDLocale(DateTime date, {bool showWeekday = false}) {
    return DateFormat().addPattern('yyyy${"년".tr()} MM${"월".tr()} dd${"일".tr()} ${showWeekday ? " (E)" : ""}').format(date);
  }

  /// yyyy-MM-dd -> yyyy년 MM월 dd일
  static String formatYMDLocale(String date) {
    if (date.isEmpty) return '';
    DateTime pared = parseYMD(date);
    return parseYMDLocale(pared);
  }

  /// yyyy-MM-dd -> yyyy. MM. dd a hh:mm
  static String parseYYMDAHM(String date) {
    if (date.isEmpty) return "";
    DateTime time = DateFormat().addPattern('yyyy-MM-dd HH:mm').parse(date);
    return DateFormat().addPattern('yyyy. MM. dd a hh:mm').format(time);
  }

  /// DateTime to A HH:mm
  static String formatAHM(DateTime date) {
    return DateFormat().addPattern('a hh:mm').format(date);
  }

  /// MM. dd
  static String parseDotMD(String date) {
    if (date.isEmpty) return "";
    return DateFormat().addPattern('MM. dd').format(parseYMD(date));
  }

  static String getCareDateTime(List<DateTime> timeList) {
    if (timeList.isEmpty) return "";
    return '${formatAHM(timeList.first)} ~ ${formatAHM(timeList.last)}';
  }

  /// YY. MM. dd
  static String parseYYMMDD(String date) {
    if (date.isEmpty) return "";
    return DateFormat().addPattern('yy. MM. dd').format(parseYMD(date));
  }

  static DateTime getFirstDay(DateTime time) => Jiffy(time).startOf(Units.MONTH).dateTime;

  static DateTime getLastDay(DateTime time) => Jiffy(time).endOf(Units.MONTH).dateTime;

  /// mm.dd(E)
  static String formatMDE(DateTime date) {
    return DateFormat().addPattern("MM. dd(E)").format(date);
  }

  /// yyyy.MM.dd HH:mm DateTime -> String
  static String formatDotYMDHM(DateTime date) {
    return DateFormat().addPattern("yyyy.MM.dd HH:mm").format(date);
  }

  /// yyyy.MM.dd HH:mm String -> DateTime
  static DateTime parseDotYMDHM(String date) {
    return DateFormat().addPattern("yyyy.MM.dd HH:mm").parse(date);
  }

  ///포인트,캐시 사용
  static int getStringToInt(String data) {
    int value = StringUtils.validateString(data) ? int.parse(data.replaceAll(',', '')) : 0;
    return value;
  }

  /// yyyy.MM.dd HH:mm -> yyyy년 MM월 dd일 HH시
  static String formatYYMMDDHH(DateTime date) {
    return DateFormat().addPattern('yy${"년".tr()} MM${"월".tr()} dd${"일".tr()} HH${"시".tr()}').format(date);
  }

  /// yyyy.MM.dd HH:mm -> MM월 dd일 HH시
  static String formatMMDDHH(DateTime date) {
    return DateFormat().addPattern('MM${"월".tr()} dd${"일".tr()} HH${"시".tr()}').format(date);
  }

  /// yyyy-mm-dd A hh
  static String parseYMDAH(String date) {
    if (date.isEmpty) return "";
    DateTime time = DateFormat().addPattern('yyyy-MM-dd HH:mm').parse(date);
    return DateFormat().addPattern('yyyy. MM. dd a h${"시".tr()}').format(time);
  }

  /// end - start -> hh시간 mm분
  static String differenceTime(List<String> date) {
    int hour, min;
    if (date.isEmpty) return "";
    date.sort();
    hour = parseHD(date.last).difference(parseHD(date.first)).inHours;
    min = parseHD(date.last).difference(parseHD(date.first)).inMinutes - (hour * 60);
    return '$hour${"시간".tr()}' + (min > 0 ? '$min${"분".tr()}' : '');
  }

  static String formatYYYYMMDD(DateTime date) {
    return DateFormat().addPattern('yyyyMMdd').format(date);
  }

  /// yyyy-MM-dd to DateTime
  static DateTime parseYYYYMMDD(String date) {
    if (date.isEmpty) return DateTime.now();
    return DateFormat().addPattern('yyyyMMdd').parse(date);
  }

  /// to HH:mm:ss
  static DateTime parseHHMMss(String date) {
    if (date.isEmpty) return DateTime.now();
    return DateFormat().addPattern('HH:mm:ss').parse(date);
  }

  /// yy.MM.dd -> yyyy-MM-dd
  static DateTime formatYYMMDD(String date) {
    if (date.isEmpty) return DateTime.now();
    return DateFormat().addPattern('yy.MM.dd').parse(date);
  }

  static String parseYMDW(DateTime date) {
    return DateFormat().addPattern('yyyy년 MM월 dd일 (E)').format(date);
  }

  static String parseYMDHMS(String date) {
    DateTime time = DateFormat().addPattern('yyyy-MM-dd hh:mm:ss').parse(date);
    return DateFormat().addPattern('yyyy-MM-dd HH:mm').format(time);
  }

  /// to HH:mm:ss
  static String formatHHmmss(DateTime date) {
    return DateFormat().addPattern('HH:mm:ss').format(date);
  }

  /// to M월 dd일
  static String formatLocalMD(DateTime date) {
    return DateFormat().addPattern("M월 dd일").format(date);
  }

  /// difference from DateTime
  static String differenceDateTime(List<DateTime> date) {
    int hour, min;
    if (date.isEmpty) return "";
    date.sort();
    hour = date.last.difference(date.first).inHours;
    min = date.last.difference(date.first).inMinutes.remainder(60);
    // min = parseHD(date.last).difference(parseHD(date.first)).inMinutes - (hour * 60);
    return (hour > 0 ? "$hour${"시간".tr()}" : '') + (min > 0 ? '$min${"분".tr()}' : '');
  }

  static DateTime formatYMDHMS(String date) {
    return DateFormat().addPattern('yyyy-MM-dd hh:mm:ss').parse(date);
  }

  static TextInputFormatter whiteSpaceFormat() {
    return FilteringTextInputFormatter.deny(new RegExp(r"\s\b|\b\s"));
  }

  static DateTime parseDiaryDate(String date, String time) {
    if (date.isEmpty) return DateTime.now();
    return DateFormat().addPattern('yyyy-MM-dd hh:mm:ss').parse("$date $time");
  }

  static String formatYMDHM(DateTime date) {
    return DateFormat().addPattern("yyyy-MM-dd hh:mm").format(date);
  }

  static String fromTo(String start, String end) {
    return formatDotYMD(parseYMD(start)) + ' ~ ' + DateFormat().add_Md().format(parseYMD(end));
  }

  /// yyyy.MM.dd HH:mm -> MM월 dd일 HH시
  static String parseMMDDHH(String date) {
    DateTime dateTime = parseYMDHM(date);
    return DateFormat().addPattern('MM${"월".tr()} dd${"일".tr()} HH${"시".tr()}').format(dateTime);
  }

  /// MM.dd a hh:mm
  static String toReplyDate(DateTime date) {
    return DateFormat().addPattern('MM.dd a hh:mm').format(date);
  }

  static String parseYYYYMM(DateTime date) {
    return DateFormat().addPattern('yyyy.MM').format(date);
  }

  /// ex. 2022. 05. 02. 17:43 => 2022. 05. 02. 17:50
  static DateTime now({int? later}) {
    DateTime now = DateTime.now().add(Duration(minutes: later ?? 0));
    now = Jiffy(now).startOf(Units.MINUTE).dateTime;
    return now.add(Duration(minutes: (later ?? 0) + ((now.minute % 10) * -1).ceil() + 10));
  }

  static DateTime getUtcFromDate(DateTime date) {
    return Jiffy(date).startOf(Units.DAY).dateTime.add(date.timeZoneOffset).toUtc();
  }

  static String formatYMDAHM(DateTime date) {
    return DateFormat().addPattern('yyyy-MM-dd HH:mm').format(date);
  }

  static DateTime convertNow({int? hour, int? min}) {
    DateTime now = DateTime.now();
    now = Jiffy(now).startOf(Units.DAY).dateTime;
    return now.add(Duration(hours: hour ?? 0, minutes: min ?? 0));
  }

  static DateTime convertNowFromString(String time) {
    DateTime date = parseHD(time);
    DateTime now = DateTime.now();
    now = Jiffy(now).startOf(Units.DAY).dateTime;
    return now.add(Duration(hours: date.hour, minutes: date.minute));
  }

  ///주소세팅 공통화 (region2th 현재 나의정보, 나의주소 부분에서 세팅)
  static String getAddressParser(String region3th, String region4th, {String region2th = ''}) {
    //2022/06/23 하동 -> 광교2동(하동) , 반월동(반월동)
    String value = '';
    if (region2th.isNotEmpty) {
      value = '$region2th $region3th';
      if (!StringUtils.validateString(region3th)) {
        value = '$region2th $region4th';
      }
    } else {
      value = region4th.isNotEmpty
          ? (region4th == region3th)
              ? region3th
              : '$region4th($region3th)'
          : region3th;
      if (region3th.isEmpty) {
        value = region4th;
      }
    }
    return value;
  }
}
