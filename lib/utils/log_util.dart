import 'package:logger/logger.dart';

Logger getLogger(/*{Level level = Level.verbose}*/) {
  return Logger(
    // level: level,
    printer: PrettyPrinter(
        methodCount: 1,
        // number of method calls to be displayed 실체 로그 위치
        errorMethodCount: 8,
        // number of method calls if stacktrace is provided
        lineLength: 100,
        // width of the output
        colors: false,
        // Colorful log messages
        printEmojis: true,
        // Print an emoji for each log message
        printTime: true // Should each log print contain a timestamp
        ),
  );
}

// class LogUtil {
//   static LogUtil _instance;
//   static Logger _log;
//
//   LogUtil._internal() {
//     setLevel();
//   }
//
//   static LogUtil getInstance() {
//     if (_instance == null) {
//       _instance = LogUtil._internal();
//     }
//
//     return _instance;
//   }
//
//   static void setLevel({Level level}) {
//     _log = Logger(
//       level: level,
//       // filter: null,//디버그 모드
//       output: null, //디버그 모드
//       printer: PrettyPrinter(
//           methodCount: 2,
//           // number of method calls to be displayed
//           errorMethodCount: 8,
//           // number of method calls if stacktrace is provided
//           lineLength: 120,
//           // width of the output
//           colors: true,
//           // Colorful log messages
//           printEmojis: true,
//           // Print an emoji for each log message
//           printTime: true // Should each log print contain a timestamp
//           ),
//     );
//   }
//
//   /// Log a message at level [Level.verbose].
//   static v(dynamic message, [dynamic error, StackTrace stackTrace]) {
//     log.log(Level.verbose, message, error, stackTrace);
//   }
//
//   /// Log a message at level [Level.debug].
//   static d(dynamic message, [dynamic error, StackTrace stackTrace]) {
//     log.log(Level.debug, message, error, stackTrace);
//   }
//
//   /// Log a message at level [Level.info].
//   static i(dynamic message, [dynamic error, StackTrace stackTrace]) {
//     log.log(Level.info, message, error, stackTrace);
//   }
//
//   /// Log a message at level [Level.warning].
//   static w(dynamic message, [dynamic error, StackTrace stackTrace]) {
//     log.log(Level.warning, message, error, stackTrace);
//   }
//
//   /// Log a message at level [Level.error].
//   static e(dynamic message, [dynamic error, StackTrace stackTrace]) {
//     log.log(Level.error, message, error, stackTrace);
//   }
//
//   /// Log a message at level [Level.wtf].
//   static wtf(dynamic message, [dynamic error, StackTrace stackTrace]) {
//     log.log(Level.wtf, message, error, stackTrace);
//   }
// }
