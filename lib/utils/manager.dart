const String apiURL = "https://reqres.in/api/users/2";
const bool devMode = false;
const double textScaleFactor = 1.0;

const String KEY_ID = "KEY_ID";
const String KEY_PASSWORD = "KEY_PASSWORD";
const String KEY_USER_NAME = "KEY_USER_NAME";
const String KEY_USER_MOBILE = "KEY_USER_MOBILE";
const String KEY_AUTH_NUMBER = "KEY_AUTH_NUMBER";
