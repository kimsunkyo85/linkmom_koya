import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/view/main/community/community_event_page.dart';
import 'package:linkmom/view/main/job_apply/guide/guide_momdady_page.dart';
import 'package:linkmom/view/main/matchings/linkmom_apply_page.dart';
import 'package:linkmom/view/main/matchings/momdady_apply_page.dart';
import 'package:linkmom/view/main/matchings/momdady_cares_page.dart';
import 'package:linkmom/view/main/mypage/benefit/benefit_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_claim_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_view_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_write_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/linkmom_diary_list_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/momdady_diary_list_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/payment/payment_history_page.dart';
import 'package:linkmom/view/main/mypage/cs/cs_contact_page.dart';
import 'package:linkmom/view/main/mypage/cs/cs_notice_page.dart';
import 'package:linkmom/view/main/mypage/cs/cs_notice_view_page.dart';

import '../data/network/models/notification_list_response.dart';
import '../data/network/models/pay_cancel_info_request.dart';
import '../data/push/model/linkmompush.dart';
import '../data/storage/flag_manage.dart';
import '../manager/data_manager.dart';
import '../manager/enum_manager.dart';
import '../manager/enum_utils.dart';
import '../utils/commons.dart';
import '../utils/string_util.dart';
import '../utils/style/color_style.dart';
import '../utils/style/linkmom_style.dart';
import '../utils/style/text_style.dart';
import '../view/login/agreement/register_agreement_page.dart';
import '../view/main/auth_center/authcenter_career_page.dart';
import '../view/main/auth_center/authcenter_covid19_page.dart';
import '../view/main/auth_center/authcenter_crims_page.dart';
import '../view/main/auth_center/authcenter_deungbon_page.dart';
import '../view/main/auth_center/authcenter_educate_page.dart';
import '../view/main/auth_center/authcenter_graduated_page.dart';
import '../view/main/auth_center/authcenter_health_page.dart';
import '../view/main/community/community_view_page.dart';
import '../view/main/job_apply/job_profile/linkmom_profile_page.dart';
import '../view/main/job_apply/job_profile/momdady_profile_page.dart';
import '../view/main/mypage/cs/cs_contact_view_page.dart';
import '../view/main/mypage/review/linkmom_review_page.dart';
import '../view/main/mypage/review/momdaddy_review_page.dart';
import '../view/main/mypage/work_noti_page.dart';

import 'commons.dart';
import 'custom_dailog/custom_dailog.dart';
import 'style/color_style.dart';
import 'style/linkmom_style.dart';
import 'style/text_style.dart';

class RouteAction {
  /// NOTE: 1st depth
  static const String linkCare = 'care';
  static const String linkMypage = 'mypage';
  static const String linkCommunity = 'com';
  static const String linkJoin = 'join';
  static const String linkEvent = 'event';
  static const String linkCs = 'cs';

  /// NOTE: 2nd depth
  static const String linkManage = 'manage';
  static const String linkDiary = 'diary';
  static const String linkApply = 'apply';
  static const String linkList = 'list';
  static const String linkPoint = 'point';
  static const String linkBenefit = 'benefit';
  static const String linkAuth = 'auth';
  static const String linkNoti = 'noti';
  static const String linkNotice = 'notice';
  static const String linkContact = 'contact';
  static const String linkPenalty = 'penalty';
  static const String linkReview = 'review';
  static const String linkPay = 'pay';

  /// NOTE: 3rd depth
  static const String linkDeungb = 'deungb';
  static const String linkCrime = 'crime';
  static const String linkHealth = 'health';
  static const String linkCareer = 'career';
  static const String linkGradua = 'gradua';
  static const String linkEducate = 'educate';
  static const String linkWrite = 'write';
  static const String linkView = 'view';
  static const String linkClaim = 'claim';
  static const String linkExpired = 'expired';

  /// NOTE: common prefix
  static const String linkLinkmom = 'l';
  static const String linkMomdaddy = 'm';

  static const String routeScheme = 'linkmom://';

  static const int pushTomorrow = 50013;
  static const String pushCovid = '50014';
  static const int importantDiaryClaim = 4016;
  static const int importantCareCancelL = 2010;
  static const int importantCareCancelM = 2011;

  static const int toastEventInvite5 = 3003;
  static const int toastEventInvite10 = 3004;

  /// sendcode - 등본 1
  static const int authDeungbon = 74001;

  /// sendcode - 범죄 2
  static const int authCrime = 74002;

  /// sendcode - 건강 3
  static const int authHealth = 74003;

  /// sendcode - 경력 4
  static const int authCareer = 74004;

  /// sendcode - 학력 5
  static const int authGradue = 74005;

  /// sendcode - 교육 6
  static const int authEdu = 74006;

  static const List<int> importantList = [
    importantDiaryClaim,
    importantCareCancelL,
    importantCareCancelM,
  ];

  /// Finger push / Dynamic link
  static void onLink(BuildContext context, String url, {Map<String, String>? params}) {
    try {
      log.d('|| link: $url');
      String clearUrl = url;
      int subIndex = clearUrl.indexOf('?');
      if (subIndex > -1) clearUrl = clearUrl.substring(0, subIndex);
      clearUrl = _schemeParse(clearUrl);
      log.d({'received': 'url $clearUrl'});
      List<String> routes = clearUrl.split('/').where((e) => e.isNotEmpty).toList();
      int id = int.tryParse((routes.last.contains('=') ? routes[routes.length - 2] : routes.last).replaceAll(new RegExp(r'[^0-9]'), '')) ?? 0;
      routes.toSet();
      String category = routes.first;
      String route = routes.length > 1 ? routes[1] : '';
      log.d({'onLink': 'parse link', 'url': url, 'routes': routes.toString(), 'category': category, 'route': route});
      if (params != null) log.d({'onLink params': params.toString()});
      int modeIdx = routes.length - 2;
      modeIdx = routes.lastIndexOf(linkLinkmom);
      if (modeIdx < 0) modeIdx = routes.lastIndexOf(linkMomdaddy);

      switch (category) {
        case linkCare:
          try {
            String mode = routes[modeIdx];
            switch (route) {
              case linkApply:
                userCheck(mode, () {
                  // NOTE: linkmom action
                  Commons.pageToMain(context, routeLinkmomApply, arguments: LinkmomApplyPage(isMatchingView: true, title: "매칭내역보기".tr(), id: id));
                }, () {
                  // NOTE: momdaddy action
                  Commons.pageToMain(context, routeMomdadyApply, arguments: MomdadyApplyPage(bookingId: id));
                }, () {
                  // NOTE: dafault action
                  Commons.pageToMainTabMove(context, tabIndex: TAB_SCHEDULE);
                });
                break;
              case linkDiary:
                String page = routes.length > 3 ? routes[3] : '';
                userCheck(mode, () {
                  // NOTE: linkmom action
                  switch (page) {
                    case linkWrite:
                      Commons.pageToMain(context, routeDiaryWrite, arguments: DiaryWritePage(id: id));
                      break;
                    case linkClaim:
                      Commons.pageToMain(context, routeClaimView, arguments: DiaryClaimViewPage(claimId: id, viewType: ViewType.modify));
                      break;
                    case linkList:
                      Commons.pageToMain(context, routeLinkmomDiary, arguments: LinkmomDiaryListPage());
                      break;
                    default:
                      Commons.pageToMain(context, routeDiaryView, arguments: DiaryViewPage(id: id));
                      break;
                  }
                }, () {
                  // NOTE: momdaddy action
                  switch (page) {
                    case linkClaim:
                      Commons.pageToMain(context, routeClaimView, arguments: DiaryClaimViewPage(claimId: id, viewType: ViewType.view));
                      break;
                    case linkList:
                      Commons.pageToMain(context, routeMomdadyDiary, arguments: MomdadyDiaryListPage());
                      break;
                    default:
                      Commons.pageToMain(context, routeDiaryView, arguments: DiaryViewPage(id: id));
                      break;
                  }
                }, () {
                  // NOTE: dafault action
                  if (Commons.isLinkMom()) {
                    Commons.pageToMain(context, routeLinkmomDiary);
                  } else {
                    Commons.pageToMain(context, routeMomdadyDiary);
                  }
                });
                break;
              case linkManage:
                userCheck(mode, () {
                  // NOTE: linkmom action
                  Commons.pageToMain(context, routeLinkmomApply, arguments: LinkmomApplyPage(id: id, title: "돌봄진행상황".tr()));
                }, () {
                  // NOTE: momdaddy action
                  Commons.pageToMain(context, routeMomdadyCares, arguments: MomdadyCaresPage(matchingId: id));
                }, () {
                  // NOTE: dafault action
                  Commons.pageToMainTabMove(context, tabIndex: TAB_SCHEDULE);
                });
                break;
              default:
                Commons.pageToMainTabMove(context, tabIndex: TAB_SCHEDULE);
                break;
            }
          } catch (e) {
            log.e({'error': e.toString(), 'data': routes.toString()});
            Commons.pageToMainTabMove(context, tabIndex: TAB_SCHEDULE);
          }
          break;
        case linkMypage:
          switch (route) {
            case linkPoint:
              Commons.pageToMain(context, routePaymentHistory, arguments: PaymentHistoryPage(tabIndex: PaymentHistoryPage.TAB_POINT));
              break;
            case linkBenefit:
              Commons.pageToMain(context, routeBenefit, arguments: BenefitPage());
              break;
            case linkAuth:
              String page = routes.length > 2 ? routes[2] : '';
              switch (page) {
                case linkDeungb:
                  Commons.pageToMain(context, routeAuthCenterDeungbon);
                  break;
                case linkCrime:
                  Commons.pageToMain(context, routeAuthCenterCrims);
                  break;
                case linkHealth:
                  Commons.pageToMain(context, routeAuthCenterHealth);
                  break;
                case linkCareer:
                  Commons.pageToMain(context, routeAuthCenterCareer);
                  break;
                case linkGradua:
                  Commons.pageToMain(context, routeAuthCenterGraduated);
                  break;
                case linkEducate:
                  Commons.pageToMain(context, routeAuthCenterEducate);
                  break;
                default:
                  Commons.pageToMain(context, routeAuthCenter);
                  break;
              }
              break;
            case linkPenalty:
              Commons.pageToMain(context, routePenaltyManage);
              break;
            case linkNoti:
              if (!Commons.isLinkMom()) Commons.setModeChange(changeType: USER_TYPE.link_mom);
              Commons.pageToMain(context, routeWorkNoti, arguments: WorkNotiPage());
              break;
            case linkPay:
              Commons.pageToMain(context, routePaymentHistory);
              break;
            case linkReview:
              String mode = routes.length > 2 ? routes[2] : '';
              switch (mode) {
                case linkLinkmom:
                  if (!Commons.isLinkMom()) Commons.setModeChange(changeType: USER_TYPE.link_mom);
                  Commons.pageToMain(context, routeMypageLinkmomReview);
                  break;
                case linkMomdaddy:
                  if (Commons.isLinkMom()) Commons.setModeChange(changeType: USER_TYPE.mom_daddy);
                  Commons.pageToMain(context, routeMypageMomdaddyReview);
                  break;
                default:
                  if (Commons.isLinkMom()) {
                    Commons.pageToMain(context, routeMypageLinkmomReview);
                  } else {
                    Commons.pageToMain(context, routeMypageMomdaddyReview);
                  }
                  break;
              }
              break;
            default:
              Commons.pageToMain(context, routeHome);
              break;
          }
          break;
        case linkLinkmom:
          if (!auth.user.my_info_data!.is_user_linkmom) {
            showNormalDlg(
              context: context,
              message: "링크쌤활동회원전용".tr(),
              btnRight: "구직신청서올리기".tr(),
              onClickAction: (a) {
                if (a == DialogAction.yes) {
                  if (!Commons.isLinkMom()) {
                    Commons.showToast('${USER_TYPE.link_mom.name} ${"모드변경".tr()}');
                    storageHelper.setUserType(USER_TYPE.link_mom);
                  }
                  Commons.moveApplyPage(context, DataManager());
                }
              },
            );
            return;
          } else if (auth.user.my_info_data!.is_user_linkmom && !Commons.isLinkMom()) {
            Commons.setModeChange(changeType: USER_TYPE.link_mom);
          }
          switch (route) {
            case linkList:
              Commons.pageToMainTabMove(context, tabIndex: TAB_LIST);
              break;
            case linkApply:
              String page = routes.length > 2 ? routes[2] : '';
              switch (page) {
                case linkWrite:
                  Commons.pageToMain(context, routeGuideLinkmom);
                  break;
                default:
                  if (auth.getMyInfoData.first_name.isNotEmpty) {
                    Commons.pageToMain(context, routeLinkMomProfile, arguments: LinkMomProfilePage(jobId: id));
                  } else {
                    showNormalDlg(message: "로그인해주세요".tr(), onClickAction: (a) => {if (a == DialogAction.yes) Commons.pageClear(context, routeLogin)});
                  }
                  break;
              }
              break;
            default:
              break;
          }
          break;
        case linkMomdaddy:
          if (Commons.isLinkMom()) Commons.setModeChange(changeType: USER_TYPE.mom_daddy);
          switch (route) {
            case linkList:
              Commons.pageToMainTabMove(context, tabIndex: TAB_LIST);
              break;
            case linkApply:
              String func = routes.length > 2 ? routes[2] : '';
              switch (func) {
                case linkWrite:
                  Commons.pageToMain(context, routeGuideMomdady);
                  break;
                case linkExpired:
                  Commons.pageToMain(context, routeMomDadyProfile, arguments: MomDadyProfilePage(viewMode: ViewMode(viewType: ViewType.modify), bookingId: id));
                  break;
                default:
                  if (auth.getMyInfoData.first_name.isNotEmpty) {
                    Commons.pageToMain(context, routeMomDadyProfile, arguments: MomDadyProfilePage(bookingId: id));
                  } else {
                    showNormalDlg(message: "로그인해주세요".tr(), onClickAction: (a) => {if (a == DialogAction.yes) Commons.pageClear(context, routeLogin)});
                  }
                  break;
              }
              break;
            default:
          }
          break;
        case linkCommunity:
          switch (route) {
            case linkView:
              if (id != 0) Commons.pageToMain(context, routeCommunityView, arguments: CommunityViewPage(id: id));
              break;
            default:
              Commons.pageToMainTabMove(context, tabIndex: TAB_COMMUNITY, subTabIndex: 0);
              break;
          }
          break;
        case linkEvent:
          if (id != 0) Commons.pageToMain(context, routeCommunityEvent, arguments: CommunityEventPage(id: id, url: url));
          break;
        case linkCs:
          switch (route) {
            case linkNotice:
              Commons.pageToMain(context, routeCsNotice, arguments: CsNoticePage());
              break;
            case linkContact:
              Commons.pageToMain(context, routeCsContact, arguments: CsContactPage());
              break;
            default:
              Commons.pageToMain(context, routeCs);
              break;
          }
          break;
        case linkJoin:
          if (Commons.isLogin) {
            showNormalDlg(message: "비회원만가능합니다".tr());
          } else {
            Commons.pageToMain(context, routeAgreement, arguments: RegisterAgreementPage(code: routes.last));
          }
          break;
        default:
          Commons.pageToMain(context, routeHome);
          break;
      }
    } catch (e) {
      log.d({'failed open page $url $e'});
      Commons.pageToMain(context, routeHome);
    }
  }

  /// Notification click
  static void onNotification(BuildContext context, int id, NotificationType type, dynamic push) {
    log.d('『GGUMBI』>>> onNotification : data: $type, $push,  <<< ');
    if (auth.user.my_info_data!.first_name.isEmpty) {
      Commons.page(context, routeLink);
      return;
    }
    //모드전환 코드
    //NotificationType.care - 1
    //NotificationType.jobs - 2
    //NotificationType.diary - 4
    //NotificationType.chatCareManage - 5
    //NotificationType.pay - 9
    //NotificationType.careManage - 50
    //NotificationType.workNoti - 51
    //NotificationType.mypageReview - 72
    /// change mode
    switch (type) {
      case NotificationType.care:
      case NotificationType.jobs:
      case NotificationType.diary:
      case NotificationType.chatCareManage:
      case NotificationType.pay:
      case NotificationType.careManage:
      case NotificationType.workNoti:
      case NotificationType.mypageReview:
        try {
          if (push is LinkmomPushData) {
            NotificationMsgData msg = push.msgData!;
            Commons.setModeChange(momdadyId: msg.momdady, linkMomId: msg.linkmom);
          } else if (push is NotificationMsgData) {
            Commons.setModeChange(momdadyId: push.momdady, linkMomId: push.linkmom);
          }
        } catch (e) {
          log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
        }
        break;
      default:
        break;
    }

    /// do action
    switch (type) {
      case NotificationType.care:
        Commons.pageToMain(context, routeLinkMomProfile, arguments: LinkMomProfilePage(jobId: id));
        break;
      case NotificationType.jobs:
        Commons.pageToMain(context, routeMomDadyProfile, arguments: MomDadyProfilePage(bookingId: id));
        break;
      case NotificationType.diary:
        if (push is LinkmomPushData) push = push.msgData!;
        if (push.popupCode > 0) {
          showEmergencyAlert(push.popupCode, momdaddy: push.momdadyFirstName, linkmom: push.linkmomFirstName, date: push.deadline, callback: () {
            Commons.pageToMain(context, routeClaimView, arguments: DiaryClaimViewPage(claimId: id, viewType: storageHelper.user_type == USER_TYPE.mom_daddy ? ViewType.view : ViewType.modify));
          });
        } else {
          if (push.sendCode == 4008 || push.sendCode == 4010) {
            Commons.pageToMain(context, routeClaimView, arguments: DiaryClaimViewPage(claimId: id, viewType: storageHelper.user_type == USER_TYPE.mom_daddy ? ViewType.view : ViewType.modify));
          } else if (id > 0) {
            Commons.pageToMain(context, routeDiaryView, arguments: DiaryViewPage(id: id));
          } else {
            if (Commons.isLinkMom()) {
              Commons.pageToMain(context, routeLinkmomDiary, arguments: LinkmomDiaryListPage());
            } else {
              Commons.pageToMain(context, routeMomdadyDiary, arguments: MomdadyDiaryListPage());
            }
          }
        }
        break;
      case NotificationType.diaryWrite:
        if (auth.user.my_info_data!.is_user_linkmom) {
          if (!Commons.isLinkMom()) Commons.setModeChange(changeType: USER_TYPE.link_mom);
          if (id > 0) Commons.pageToMain(context, routeDiaryWrite, arguments: DiaryWritePage(id: id));
        } else {
          showNormalDlg(
            context: context,
            message: "링크쌤활동회원전용".tr(),
            btnRight: "구직신청서올리기".tr(),
            onClickAction: (a) {
              if (a == DialogAction.yes) {
                if (!Commons.isLinkMom()) {
                  Commons.showToast('${USER_TYPE.link_mom.name} ${"모드변경".tr()}');
                  storageHelper.setUserType(USER_TYPE.link_mom);
                }
                Commons.moveApplyPage(context, DataManager());
              }
            },
          );
        }
        break;
      case NotificationType.careManage:
      case NotificationType.chatCareManage:
        NotificationMsgData? msg;
        if (push is LinkmomPushData) {
          msg = push.msgData!;
        } else {
          msg = push;
        }
        if (msg != null) {
          if (msg.popupCode > 0) {
            showEmergencyAlert(msg.popupCode, momdaddy: msg.momdadyFirstName, linkmom: msg.linkmomFirstName, date: msg.deadline, callback: () {
              Commons.page(context, routePayMentCancelList, arguments: PayInfoRequest(contract_id: msg!.contractId));
            });
          } else if (Commons.isLinkMom()) {
            Commons.pageToMain(context, routeLinkmomApply,
                arguments: LinkmomApplyPage(
                  id: id,
                  title: msg.sendCode == int.parse(pushCovid)
                      ? "돌봄진행상황".tr()
                      : msg.bywho == BYME
                          ? "내가지원한맘대디".tr()
                          : "나에게지원한맘대디".tr(),
                  isMatchingView: msg.careStatus == 0 || msg.sendCode == int.parse(pushCovid),
                ));
          } else {
            if (msg.sendCode == pushTomorrow || EnumUtils.getMatchingStatus(index: msg.matchingStatus) == MatchingStatus.paid) {
              Commons.pageToMain(context, routeMomdadyCares, arguments: MomdadyCaresPage(matchingId: id));
            } else {
              Commons.pageToMain(context, routeMomdadyApply, arguments: MomdadyApplyPage(bookingId: id, bywho: msg.bywho));
            }
          }
        } else {
          log.e({'--- MESSAGE     ': 'msg is null'});
        }
        break;
      case NotificationType.workNoti:
        if (Commons.isLinkMom()) {
          bool allim = false;
          if (push is LinkmomPushData) {
            allim = push.msgData!.allim;
          } else {
            allim = push.allim;
          }
          if (allim) Commons.pageToMain(context, routeWorkNoti, arguments: WorkNotiPage());
        } else {
          Commons.pageToMain(context, routeMomdadyDiary, arguments: MomdadyDiaryListPage());
        }
        break;
      case NotificationType.jobListLinkMom: //돌봄신청서 해당 지역에 등록시 알림 수신
        int sendCode = 0;
        if (push is NotificationMsgData) {
          sendCode = push.sendCode;
        } else if (push is LinkmomPushData) {
          NotificationMsgData pushData = push.msgData!;
          sendCode = pushData.sendCode;
        }
        if (Commons.isLinkMom()) {
          Commons.setModeChange(changeType: USER_TYPE.mom_daddy);
        }
        Commons.pageToMainTabMove(context, tabIndex: TAB_LIST, sendCode: sendCode);
        break;
      case NotificationType.jobListMomDaddy: //링크쌤 해당 지역에 등록시 알림 수신
        int sendCode = 0;
        if (push is NotificationMsgData) {
          sendCode = push.sendCode;
        } else if (push is LinkmomPushData) {
          NotificationMsgData pushData = push.msgData!;
          sendCode = pushData.sendCode;
        }
        if (!Commons.isLinkMom()) {
          Commons.setModeChange(changeType: USER_TYPE.link_mom);
        }
        Commons.pageToMainTabMove(context, tabIndex: TAB_LIST, sendCode: sendCode);
        break;
      case NotificationType.careExpired:
        Commons.pageToMain(context, routeMomDadyProfile, arguments: MomDadyProfilePage(viewMode: ViewMode(viewType: ViewType.modify), bookingId: id));
        break;
      case NotificationType.applyMomDaddy:
        if (Commons.isLinkMom()) Commons.setModeChange(changeType: USER_TYPE.mom_daddy);
        Commons.pageToMain(context, routeGuideMomdady);
        break;
      case NotificationType.applyLinkMom:
        if (!Commons.isLinkMom()) Commons.setModeChange(changeType: USER_TYPE.link_mom);
        Commons.pageToMain(context, routeGuideLinkmom);
        break;
      case NotificationType.event:
        bool showToast = false;
        int code = 0;
        int sendCode = 0;
        if (push is LinkmomPushData) {
          NotificationMsgData msg = push.msgData!;
          code = msg.popupCode;
          sendCode = msg.sendCode;
        } else {
          code = push.popupCode;
          sendCode = push.sendCode;
        }
        if (sendCode == NotificationType.listLinkMom.value) {
          if (Commons.isLinkMom()) {
            Commons.setModeChange(changeType: USER_TYPE.mom_daddy);
          }
          Commons.pageToMainTabMove(context, tabIndex: TAB_LIST, sendCode: sendCode);
        } else if (sendCode == NotificationType.listMomDaddy.value) {
          if (!Commons.isLinkMom()) {
            Commons.setModeChange(changeType: USER_TYPE.link_mom);
          }
          Commons.pageToMainTabMove(context, tabIndex: TAB_LIST, sendCode: sendCode);
        } else {
          showToast = isShowingToast(code);
          if (showToast) {
            Commons.showToast(
              push.message.isEmpty ? push.data_message : push.message,
              toastLength: Toast.LENGTH_LONG,
              backgroundColor: Colors.black.withOpacity(0.5),
            );
          } else {
            Commons.pageToMain(context, routeCommunityEvent, arguments: CommunityEventPage(id: id));
          }
        }
        break;
      case NotificationType.community:
        Commons.pageToMainTabMove(context, tabIndex: TAB_COMMUNITY, subTabIndex: 0);
        break;
      case NotificationType.communityBoard:
        Commons.pageToMain(context, routeCommunityView, arguments: CommunityViewPage(id: id));
        break;
      case NotificationType.mypage:
        Commons.pageToMain(context, routeMyPage);
        break;
      case NotificationType.mypagePenalty:
        Commons.pageToMain(context, routePenaltyManage);
        break;
      case NotificationType.mypageReview:
        if (storageHelper.user_type == USER_TYPE.link_mom) {
          Commons.pageToMain(context, routeMypageLinkmomReview, arguments: LinkmomReviewPage(index: LinkmomReviewPage.RECEIVE));
        } else {
          Commons.pageToMain(context, routeMypageMomdaddyReview, arguments: MomdaddyReviewPage(index: MomdaddyReviewPage.RECEIVE));
        }
        break;
      case NotificationType.mypageExpiredPoint:
        Commons.pageToMain(context, routePaymentHistory, arguments: PaymentHistoryPage(tabIndex: PaymentHistoryPage.TAB_POINT));
        break;
      case NotificationType.mypageAuthcenter:
        int code = 0;
        if (push is LinkmomPushData) {
          code = push.msgData!.sendCode;
        } else {
          code = push.sendCode;
        }
        switch (code) {
          case authDeungbon:
            Commons.page(context, routeAuthCenterDeungbon, arguments: AuthCenterDeungbonPage());
            break;
          case authCrime:
            Commons.page(context, routeAuthCenterCrims, arguments: AuthCenterCrimsPage());
            break;
          case authHealth:
            Commons.page(context, routeAuthCenterHealth, arguments: AuthCenterHealthPage());
            break;
          case authCareer:
            Commons.page(context, routeAuthCenterCareer, arguments: AuthCenterCareerPage());
            break;
          case authGradue:
            Commons.page(context, routeAuthCenterGraduated, arguments: AuthCenterGraduatedPage());
            break;
          case authEdu:
            Commons.page(context, routeAuthCenterEducate, arguments: AuthCenterEducatePage());
            break;
          default:
            Commons.pageToMain(context, routeAuthCenter);
            break;
        }
        break;
      case NotificationType.benefit:
        Commons.pageToMain(context, routeBenefit);
        break;
      case NotificationType.cs:
        Commons.pageToMain(context, routeCs);
        break;
      case NotificationType.csNotice:
        Commons.pageToMain(context, routeCsNotice);
        break;
      case NotificationType.csContact:
        Commons.pageToMain(context, routeCsContactView, arguments: CsContactViewPage(id: id));
        break;
      default:
        break;
    }
  }

  static void onDetail(
    BuildContext context,
    String detailCode, {
    String link = '',
  }) {
    switch (detailCode) {
      case pushCovid:
        int id = int.tryParse(link.split('/').last) ?? 0;
        Flags.showDialog(
          context,
          Flags.KEY_COVID_CHECK_ALERT,
          "매일자가진단이필요".tr(),
          id: id,
          onClickAction: (a) => {if (a == DialogAction.yes) Commons.page(context, routeAuthCenterCovid19, arguments: AuthCenterCovid19Page())},
        );
        break;
      default:
        onLink(context, link);
    }
  }

  static void userCheck(String mode, Function linkmomAction, Function momdaddyAction, Function defaultAction) {
    log.d({'onLink': 'userCheck: $mode'});
    if (mode == linkLinkmom) {
      log.d({'onLink': 'called linkmomAction'});
      Commons.setModeChange(changeType: USER_TYPE.link_mom);
      linkmomAction();
    } else if (mode == linkMomdaddy) {
      Commons.setModeChange(changeType: USER_TYPE.mom_daddy);
      log.d({'onLink': 'called momdaddyAction'});
      momdaddyAction();
    } else {
      log.d({'onLink': 'called defaultAction'});
      defaultAction();
    }
  }

  static void showEmergencyAlert(int type, {String momdaddy = '', String linkmom = '', String date = '', Function? callback}) {
    String title = '';
    String name = '';
    String reason = '';
    String hightlight = '';
    String postp = '';
    String btnTitle = '';
    date = StringUtils.parseMMDDHH(date);
    double textHeight = 1.3;
    switch (type) {

      /// NOTE: for momdaddy
      case 2010:
        name = '$momdaddy${"맘대디".tr()}';
        postp = "와".tr();
        reason = "취소".tr();
        hightlight = '$date${"까지".tr()} ${"소명".tr()}';
        btnTitle = "취소사유확인".tr();
        title = '$linkmom ${"링크쌤".tr()}${"의".tr()} ${"사유로".tr()}\n$reason${"접수되어벌점".tr()}\n${"부과됩니다".tr()}';
        break;

      /// NOTE: for linkmom
      case 2011:
        name = '$linkmom${"링크쌤".tr()}';
        postp = "과".tr();
        reason = "취소".tr();
        hightlight = '$date${"까지".tr()} ${"소명".tr()}';
        btnTitle = "취소사유확인".tr();
        title = '$momdaddy ${"맘대디".tr()}${"의".tr()} ${"사유로".tr()}\n$reason${"접수되어벌점".tr()}\n${"부과됩니다".tr()}';
        break;

      /// NOTE: from diary claim
      case 4016:
        name = '$momdaddy${"맘대디".tr()}';
        postp = "와".tr();
        reason = '${"정산보류".tr()} ';
        hightlight = '$date${"까지".tr()} ${"소명".tr()}';
        btnTitle = "정산보류사유확인".tr();
        title = '$name${"의".tr()} ${"정산보류".tr()}${"접수되어벌점".tr()}\n${"부과됩니다".tr()}';
        break;
      default:
        hightlight = '$date${"까지".tr()} ${"소명".tr()}';
        break;
    }
    showNormalDlg(
      messageWidget: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: [
              Container(
                width: 40,
                height: 40,
                padding: padding_05,
                margin: EdgeInsets.fromLTRB(0, 0, 10, 20),
                decoration: BoxDecoration(
                  color: color_fa784f,
                  shape: BoxShape.circle,
                ),
                child: Lcons.warning_sticker(),
              ),
              Container(
                  width: widthFull(navigatorKey.currentContext!),
                  constraints: BoxConstraints(
                    maxWidth: widthFull(navigatorKey.currentContext!) * 0.46,
                  ),
                  child: lText(title, style: st_b_16(fontWeight: FontWeight.w700), maxLines: 3)),
            ]),
            sb_h_25,
            RichText(
                text: TextSpan(children: [
              TextSpan(text: "$reason${"사유확인후".tr()} $name$postp ${"협의해주세요".tr()}\n", style: st_b_14(height: textHeight)),
              TextSpan(text: '$reason${"사유에문제가있어".tr()}', style: st_b_14(height: textHeight)),
              TextSpan(text: hightlight, style: st_14(textColor: Commons.getColor(), height: textHeight)),
              TextSpan(text: "해주세요".tr(), style: st_b_14(height: textHeight)),
              TextSpan(text: '\n', style: st_b_14(height: textHeight)),
              TextSpan(text: reason, style: st_b_14(height: textHeight)),
              TextSpan(text: "시점기준이내".tr(), style: st_b_14(height: textHeight)),
              TextSpan(text: "당사자간협의내용을카톡상담".tr(), style: st_14(textColor: Commons.getColor(), height: textHeight)),
              TextSpan(text: "관리자에게알려주시면".tr(), style: st_b_14(height: textHeight)),
              TextSpan(text: reason, style: st_b_14(height: textHeight)),
              TextSpan(text: "벌점5개는차감해드리며".tr(), style: st_b_14(height: textHeight)),
            ])),
            Container(
              padding: padding_15_TB,
              child: Column(
                children: [
                  lDingbatText(text: "부과된벌점으로인해".tr(), style: st_12(textColor: color_b2b2b2), dingbatSize: 2, width: 240, dingbatColor: color_b2b2b2),
                  lDingbatText(text: "상대방사유취소로인한돌봄".tr(), style: st_12(textColor: color_b2b2b2), dingbatSize: 2, width: 240, dingbatColor: color_b2b2b2),
                ],
              ),
            )
          ],
        ),
      ),
      btnRight: btnTitle,
      btnLeft: "닫기".tr(),
      onClickAction: (a) => {if (a == DialogAction.yes && callback != null) callback()},
    );
  }

  static bool isImportant(int type) {
    return importantList.contains(type);
  }

  static String _schemeParse(String url) {
    Uri? link = Uri.tryParse(url) ?? null;
    if (url.startsWith(routeScheme)) {
      return url.replaceAll(routeScheme, '').replaceAll(routeLink, '').replaceAll('link', '');
    } else if (url.startsWith('https://')) {
      return url.replaceAll(link!.origin, '');
    } else if (url.contains(routeLink)) {
      return url.replaceAll(routeLink, '');
    } else {
      return url;
    }
  }

  static bool isShowingToast(int code) {
    return [toastEventInvite5, toastEventInvite10].contains(code);
  }
}
