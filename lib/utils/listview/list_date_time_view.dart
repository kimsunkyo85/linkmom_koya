import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/req_data.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

class ListDateTimeView extends StatefulWidget {
  ListDateTimeView({
    this.title = '',
    this.data,
    this.user_type = USER_TYPE.mom_daddy,
    this.count = 1,
    this.childAspectRatio = 1.7,
    this.borderRadius = 30,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.enableColor,
    this.disableColor,
    this.onItemClick,
    this.isOverlap = true,
  });

  final String title;
  final dynamic data;
  final USER_TYPE user_type;
  final Function? onItemClick;
  final count;
  final childAspectRatio;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color? enableColor;
  final Color? disableColor;
  final bool isOverlap;

  ListDateTimeViewState createState() => ListDateTimeViewState();
}

class ListDateTimeViewState extends State<ListDateTimeView> {
  late String _title;
  List<ReqScheduleData> _data = [];
  late USER_TYPE user_type;

  ///그리드뷰 카운트
  int count = 1;
  late double childAspectRatio;
  late double borderRadius;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _data = widget.data ?? [];
    user_type = widget.user_type;
    count = widget.count;
    childAspectRatio = widget.childAspectRatio;
    borderRadius = widget.borderRadius;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor!;
    disableColor = widget.disableColor!;
    isOverlap = widget.isOverlap;
  }

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: _data.length,
        physics: const AlwaysScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          return lInkWell(
            onTap: () {
              onItemClick(index);
            },
            child: Row(
              children: [
                lText(
                  _data[index].bookingdate[0],
                  style: st_b_16(
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  onItemClick(int index) {
    setState(() {
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}
