import 'package:flutter/material.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../commons.dart';
import 'model/list_model.dart';

class ListViewSelector extends StatefulWidget {
  ListViewSelector({
    this.title = '',
    this.data,
    this.count = 1,
    this.padding = padding_10_LR,
    this.margin = padding_10,
    this.alignment = Alignment.center,
    this.borderRadius = 30,
    this.isBorderRadius = true,
    this.onItemClick,
    this.bgColor = Colors.transparent,
    this.height = 55,
    this.isWidth = false,
    this.calcu = 0.5,
  });

  final String title;
  final dynamic data;
  final Function? onItemClick;
  final count;
  final padding;
  final margin;
  final alignment;
  final double borderRadius;
  final bool isBorderRadius;
  final Color bgColor;
  final double height;
  final bool isWidth;
  final double calcu;
  ListViewSelectorState createState() => ListViewSelectorState();
}

class ListViewSelectorState extends State<ListViewSelector> {
  List<SingleItem> _data = [];

  int count = 1;
  late var padding;
  late var margin;
  late var alignment;
  late double borderRadius;
  late bool isBorderRadius;
  late TextStyle textStyle;
  late Color bgColor;
  late double height;
  late bool isWidth;
  late double calcu;

  @override
  void initState() {
    super.initState();
    _data = widget.data ?? [];
    count = widget.count;
    padding = widget.padding;
    margin = widget.margin;
    alignment = widget.alignment;
    borderRadius = widget.borderRadius;
    isBorderRadius = widget.isBorderRadius;
    bgColor = widget.bgColor;
    if (widget.height == 0) {
      height = 55;
    } else {
      height = widget.height;
    }
    isWidth = widget.isWidth;
    calcu = widget.calcu;
  }

  @override
  Widget build(BuildContext context) {
    return lContainer(
        color: bgColor,
        width: isWidth ? widthFull(context) : widthFull(context) * calcu,
        child: Scrollbar(
          child: ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: _data.length,
              physics: const AlwaysScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return lInkWell(
                  onTap: () {
                    onItemClick(index);
                  },
                  child: Card(
                    elevation: 0,
                    // color: _data[index].isSelected ? color_main : color_white,
                    shape: lRoundedRectangleBorder(
                      isSelected: _data[index].isSelected,
                      width: 2.0,
                      borderRadius: borderRadius,
                      disBorderColor: _data[index].isSelected ? Commons.getColor() : color_transparent,
                      enBorderColor: isBorderRadius ? Commons.getColor() : color_transparent,
                    ),
                    margin: margin,
                    child: Container(
                      height: height,
                      alignment: alignment,
                      child: lText(
                        _data[index].name,
                        style: st_b_16(
                          fontWeight: FontWeight.w500,
                          textColor: _data[index].isSelected ? Commons.getColor() : color_textDisable,
                        ),
                      ),
                    ),
                  ),
                );
              }),
        ));
  }

  onItemClick(int index) {
    setState(() {
      _data.forEach((gender) => gender.isSelected = false);
      _data[index].isSelected = true;
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}
