import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/main/job_apply/content_view.dart';

import 'model/list_model.dart';

class ListAuthTypeSelector extends StatefulWidget {
  ListAuthTypeSelector({
    this.title = '',
    this.data,
    this.childAspectRatio = 2.5,
    this.padding = padding_15_LR,
    this.borderRadius = 50,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.enableColor = color_545454,
    this.disableColor = color_999999,
    this.onItemClick,
    this.isOverlap = false,
    this.imagePath = '',
    this.isRight = true,
    this.isEditable = true,
  });

  final String title;
  final dynamic data;
  final Function? onItemClick;
  final childAspectRatio;
  final padding;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color enableColor;
  final Color disableColor;
  final bool isOverlap;
  final String imagePath;
  final bool isRight;
  final bool isEditable;

  ListAuthTypeSelectorState createState() => ListAuthTypeSelectorState();
}

class ListAuthTypeSelectorState extends State<ListAuthTypeSelector> {
  late String _title;
  List<SingleItem> _data = [];

  late double childAspectRatio;
  late var padding;
  late double borderRadius;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;
  late String imagePath;
  late bool isRight;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _data = widget.data ?? [];
    childAspectRatio = widget.childAspectRatio;
    padding = widget.padding;
    borderRadius = widget.borderRadius;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor;
    disableColor = widget.disableColor;
    isOverlap = widget.isOverlap;
    isRight = widget.isRight;
    imagePath = widget.imagePath;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (StringUtils.validateString(_title))
          Padding(
            padding: padding_10_B,
            child: contentView(
                data: ContentData(
                    content: _title,
                    contentStyle: st_18(textColor: color_222222, fontWeight: FontWeight.bold),
                    rightWidget: isRight
                        ? Row(
                            children: [lText("*", style: st_b_16(textColor: color_ff3b30)), if (isOverlap) lText("(중복가능)".tr(), style: st_14(textColor: color_b2b2b2))],
                          )
                        : Container(),
                    isOverLapRight: isOverlap,
                    icon: imagePath)),
          ),
        lContainer(
          height: 40,
          color: bgColor,
          child: ListView.builder(
              itemCount: _data.length,
              scrollDirection: Axis.horizontal,
              physics: const AlwaysScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                return lInkWell(
                    onTap: () {
                      if (widget.onItemClick != null && widget.isEditable) {
                        onItemClick(index);
                      }
                    },
                    child: AuthTypeView(
                      _data[index],
                      padding: padding,
                      borderRadius: borderRadius,
                      fontWeight: fontWeight,
                      bgColor: bgColor,
                      enableColor: enableColor,
                      disableColor: disableColor,
                    ));
              }),
        ),
      ],
    );
  }

  onItemClick(int index) {
    setState(() {
      if (isOverlap) {
        ///중복선택시
        if (_data[index].name == "없음".tr()) {
          _data.forEach((value) => value.isSelected = false);
          _data[index].isSelected = true;
        } else {
          _data.forEach((value) {
            if (value.name == "없음".tr()) {
              value.isSelected = false;
            }
          });
          _data[index].isSelected = !_data[index].isSelected;
        }
      } else {
        ///단일 선택시
        _data.forEach((gender) => gender.isSelected = false);
        _data[index].isSelected = true;
      }
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}

class AuthTypeView extends StatelessWidget {
  final SingleItem _data;
  final padding;
  final margin;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  Color enableColor = color_545454;
  Color disableColor = color_999999;
  final bool isOverlap;

  AuthTypeView(
    this._data, {
    this.padding,
    this.margin,
    this.borderRadius = 50,
    this.fontWeight = FontWeight.w400,
    this.bgColor = Colors.transparent,
    this.enableColor = color_545454,
    this.disableColor = color_999999,
    this.isOverlap = true,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 0,
        semanticContainer: false,
        borderOnForeground: false,
        shape: lRoundedRectangleBorder(isSelected: _data.isSelected, width: _data.isSelected ? 2.0 : 1.0, borderRadius: borderRadius, enBorderColor: enableColor),
        margin: padding_10_R,
        child: Container(
          alignment: Alignment.center,
          padding: padding,
          child: lText(
            _data.name,
            style: st_b_16(fontWeight: FontWeight.w500, textColor: _data.isSelected ? enableColor : disableColor),
          ),
        ));
  }
}
