import 'package:flutter/material.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

import '../../main.dart';
import 'model/list_model.dart';

///우리집에서 출발 28 -> 16
class ListCareTypeSelector extends StatefulWidget {
  ListCareTypeSelector({
    this.title = '',
    this.data,
    this.count = 1,
    this.margin = padding_05,
    this.borderRadius = 15,
    this.itemHeight = listImgHeight,
    this.itemWidth = listImgWidth,
    this.imageHeight = 35,
    this.imageWidth = 35,
    this.onItemClick,
  });

  final String title;
  final dynamic data;
  final Function? onItemClick;
  final count;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;

  ListCareTypeSelectorState createState() => ListCareTypeSelectorState();
}

class ListCareTypeSelectorState extends State<ListCareTypeSelector> {
  late String _title;
  List<SingleItem> _data = [];

  ///그리드뷰 카운트
  int count = 1;
  late var margin;
  late double borderRadius;
  late double itemHeight;
  late double itemWidth;
  late double imageHeight;
  late double imageWidth;
  late TextStyle textStyle;

  @override
  void initState() {
    super.initState();
    log.d('『GGUMBI』>>> initState : widget.data: ${widget.data},  <<< ');
    log.d('『GGUMBI』>>> initState : widget.data as List<SingleItem>: ${widget.data as List},  <<< ');
    _title = widget.title;
    _data = widget.data ?? [];
    count = widget.count;
    margin = widget.margin;
    borderRadius = widget.borderRadius;
    itemHeight = widget.itemHeight;
    itemWidth = widget.itemWidth;
    imageHeight = widget.imageHeight;
    imageWidth = widget.imageWidth;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (StringUtils.validateString(_title))
          lTvListTitle(
            _title,
          ),
        ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: _data.length,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return lContainer(
                child: lInkWell(
                    onTap: () {
                      onItemClick(index);
                    },
                    child: CareSingleRadio(
                      _data[index],
                      margin: margin,
                      borderRadius: borderRadius,
                      itemHeight: itemHeight,
                      itemWidth: itemWidth,
                      imageWidth: imageWidth,
                      imageHeight: imageHeight,
                      count: count,
                    )),
              );
            }),
      ],
    );
  }

  onItemClick(int index) {
    setState(() {
      _data.forEach((gender) => gender.isSelected = false);
      _data[index].isSelected = true;
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}

class CareSingleRadio extends StatelessWidget {
  final SingleItem _data;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;
  final int count;

  CareSingleRadio(this._data, {this.margin, this.borderRadius = 15, this.itemHeight = 0.0, this.itemWidth = 0.0, this.imageHeight = 35, this.imageWidth = 35, this.count = 0});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
            elevation: 0,
            color: _data.isSelected ? color_main : color_white,
            shape: lRoundedRectangleBorder(isSelected: _data.isSelected, width: 2.0, borderRadius: borderRadius),
            margin: margin,
            child: Container(
              height: itemHeight,
              alignment: Alignment.center,
              // margin: padding_10,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    _data.image,
                    width: imageWidth,
                    height: imageHeight,
                    color: _data.isSelected ? color_white : Colors.grey,
                  ),
                  sb_w_20,
                  lBtnWrapAll(_data.name, isEnabled: _data.isSelected, textEnableColor: color_main, textDisableColor: color_white, btnEnableColor: color_white, btnDisableColor: color_aaaaaa, padding: padding_careType, fontSize: 16),
                ],
              ),
            )),
        sb_h_10,
        lContentText(
          _data.content,
        ),
        sb_h_30,
      ],
    );
  }
}
