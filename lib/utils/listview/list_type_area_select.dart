import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../commons.dart';
import 'model/list_model.dart';

///구직신청 보육장소
class ListTypeAreaSelector extends StatefulWidget {
  ListTypeAreaSelector({
    this.title = '',
    this.data,
    this.user_type = USER_TYPE.mom_daddy,
    this.padding = padding_10,
    this.margin = padding_10_B,
    this.height = 120,
    this.borderRadius = 20,
    this.imageHeight = 50,
    this.imageWidth = 50,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.enableColor = color_main,
    this.disableColor = color_cecece,
    this.onItemClick,
    this.isOverlap = false,
  });

  final String title;
  final dynamic data;
  final USER_TYPE user_type;
  final Function? onItemClick;
  final padding;
  final margin;
  final double height;
  final double borderRadius;
  final double imageHeight;
  final double imageWidth;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color enableColor;
  final Color disableColor;
  final bool isOverlap;

  ListTypeAreaSelectorState createState() => ListTypeAreaSelectorState();
}

class ListTypeAreaSelectorState extends State<ListTypeAreaSelector> {
  late String _title;
  List<SingleItem> _data = [];
  late USER_TYPE user_type;
  late var padding;
  late var margin;
  late double height;
  late double borderRadius;
  late double imageHeight;
  late double imageWidth;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;
  late Function? onItemClick;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _data = widget.data ?? [];
    user_type = widget.user_type;
    padding = widget.padding;
    margin = widget.margin;
    height = widget.height;
    borderRadius = widget.borderRadius;
    imageHeight = widget.imageHeight;
    imageWidth = widget.imageWidth;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor;
    disableColor = widget.disableColor;
    isOverlap = widget.isOverlap;
    onItemClick = widget.onItemClick!;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (StringUtils.validateString(_title))
          lTvListTitle(
            _title,
          ),
        lContainer(
          color: bgColor,
          child: lScrollbar(
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: _data.length,
                physics: const AlwaysScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  var data = _data[index];
                  return lInkWell(
                    onTap: () {
                      if (onItemClick != null) _onItemClick(index, DialogAction.select);
                    },
                    child: Column(
                      children: [
                        Container(
                          height: height,
                          child: Card(
                            elevation: 0,
                            shape: lRoundedRectangleBorder(isSelected: data.isSelected, width: data.isSelected ? 2.0 : 1.0, borderRadius: borderRadius, enBorderColor: Commons.getColor()),
                            child: Padding(
                              padding: padding,
                              child: Row(
                                children: [
                                  Padding(
                                    padding: padding_15,
                                    child: data.icon != null
                                        ? Lcons(
                                            data.icon!.name,
                                            size: 60,
                                            color: Commons.getColor(),
                                            disableColor: color_cecece,
                                            isEnabled: data.isSelected,
                                          )
                                        : Image.asset(
                                      data.image,
                                      width: imageWidth,
                                      height: imageHeight,
                                      color: data.isSelected ? Commons.getColor() : color_b2b2b2,
                                    ),
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        lText(
                                          '${data.name}${data.name2}',
                                          style: st_b_18(textColor: data.isSelected ? Commons.getColor() : color_b2b2b2, fontWeight: FontWeight.bold),
                                        ),
                                        sb_h_03,
                                        Padding(
                                          padding: padding_10_R,
                                          child: lAutoSizeText(
                                            data.content,
                                            maxLines: 2,
                                            style: st_b_14(
                                              textColor: color_545454,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: padding_10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              InkWell(
                                onTap: () {
                                  _onItemClick(index, DialogAction.toggle);
                                },
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Lcons.check_circle(isEnabled: data.values![0].isSelected, color: Commons.getColor(), disableColor: color_dedede),
                                    sb_w_05,
                                    Expanded(
                                      child: Padding(
                                        padding: padding_10_R,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            lText(data.values![0].name2,
                                                style: st_b_16(
                                                  fontWeight: FontWeight.w500,
                                                )),
                                            sb_h_05,
                                            lText(data.values![0].content, style: st_14(textColor: color_b2b2b2)),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        sb_h_15,
                      ],
                    ),
                  );
                }),
          ),
        ),
      ],
    );
  }

  _onItemClick(int index, DialogAction action) {
    if (isOverlap) {
      ///중복선택시
      if (DialogAction.toggle == action) {
        _data[index].values![0].isSelected = !_data[index].values![0].isSelected;
        if (_data[index].values![0].isSelected) {
          _data[index].isSelected = true;
        }
      } else {
        _data[index].isSelected = !_data[index].isSelected;
        if (!_data[index].isSelected) {
          _data[index].values![0].isSelected = false;
        }
      }
    } else {
      ///단일 선택시
      _data.forEach((gender) => gender.isSelected = false);
      _data[index].isSelected = true;
    }
    if (widget.onItemClick != null) {
      widget.onItemClick!(_data[index]);
    }
  }
}
