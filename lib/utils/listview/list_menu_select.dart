import 'package:flutter/material.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../commons.dart';
import 'model/list_model.dart';

class ListMenuSelector extends StatefulWidget {
  ListMenuSelector({
    this.title = '',
    this.data,
    this.count = 1,
    this.childAspectRatio = 2.5,
    this.padding = padding_10,
    this.margin = padding_10,
    this.alignment = Alignment.center,
    this.borderRadius = 34,
    this.isBorderRadius = true,
    this.onItemClick,
    this.bgColor = Colors.transparent,
    this.height = 55,
    this.isWidth = false,
    this.calcu = 0.5,
  });

  final String title;
  final dynamic data;
  final Function? onItemClick;
  final count;
  final double childAspectRatio;
  final padding;
  final margin;
  final alignment;
  final double borderRadius;
  final bool isBorderRadius;
  final Color bgColor;
  final double height;
  final bool isWidth;
  final double calcu;

  ListMenuSelectorState createState() => ListMenuSelectorState();
}

class ListMenuSelectorState extends State<ListMenuSelector> {
  List<SingleItem> _data = [];

  int count = 1;
  double childAspectRatio = 2.5;
  late var padding;
  late var margin;
  late var alignment;
  late double borderRadius;
  late bool isBorderRadius;
  late TextStyle textStyle;
  late Color bgColor;
  late double height;
  late bool isWidth;
  late double calcu;

  @override
  void initState() {
    super.initState();
    _data = widget.data ?? [];
    count = widget.count;
    childAspectRatio = widget.childAspectRatio;
    padding = widget.padding;
    margin = widget.margin;
    alignment = widget.alignment;
    borderRadius = widget.borderRadius;
    isBorderRadius = widget.isBorderRadius;
    bgColor = widget.bgColor;
    if (widget.height == 0) {
      height = 35;
    } else {
      height = widget.height;
    }
    isWidth = widget.isWidth;
    calcu = widget.calcu;
  }

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      child: GridView.builder(
          padding: padding,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: count,
            childAspectRatio: 2.5,
          ),
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: _data.length,
          itemBuilder: (context, index) {
            bool isEnabled = true;
            return lBtnHL(
              _data[index].name,
              isEnabled: isEnabled,
              borderRadius: 34,
              margin: margin,
              padding: padding,
              btnColorBg: color_transparent,
              sideColor: color_eeeeee,
              textColorHighLight: Commons.getColor(),
              sideColorBg: Commons.getColor(),
              style: st_b_16(
                fontWeight: FontWeight.w500,
                textColor: _data[index].isSelected
                    ? Commons.getColor()
                    : isEnabled
                        ? color_textDisable
                        : color_textDisable.withOpacity(0.3),
              ),
              onPressed: () => onItemClick(index),
            );
            // return StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
            //   return lContainer(
            //     margin: margin,
            //     child: MaterialButton(
            //       elevation: 0,
            //       highlightColor: color_transparent,
            //       splashColor: color_transparent,
            //       highlightElevation: 0,
            //       disabledColor: color_btnDisable,
            //       disabledTextColor: color_textDisable,
            //       color:  Commons.getColor(),
            //       textColor: color_545454 ,
            //       shape: RoundedRectangleBorder(
            //         borderRadius: BorderRadius.circular(borderRadius),
            //         side: BorderSide(color: Commons.getColor()),
            //       ),
            //       onPressed: () {
            //
            //       },
            //       child: lText(_data[index].name,
            //               style: st_b_16(
            //                       fontWeight: FontWeight.w500,
            //                       textColor: _data[index].isSelected ? Commons.getColor() : color_textDisable,
            //                     ),
            //               ),
            //       ),
            //   );
            // });
            // return lInkWell(
            //   onTap: () {
            //     onItemClick(index);
            //   },
            //   child: Card(
            //     elevation: 0,
            //     color: color_white,
            //     shape: lRoundedRectangleBorder(
            //       isSelected: _data[index].isSelected,
            //       width: 1.0,
            //       borderRadius: borderRadius,
            //       disBorderColor: _data[index].isSelected ? Commons.getColor() : color_eeeeee,
            //       enBorderColor: isBorderRadius ? Commons.getColor() : color_transparent,
            //     ),
            //     margin: margin,
            //     child: Container(
            //       height: height,
            //       alignment: Alignment.center,
            //       child: lBtn(
            //         _data[index].name,
            //         style: st_b_16(
            //           fontWeight: FontWeight.w500,
            //           textColor: _data[index].isSelected ? Commons.getColor() : color_textDisable,
            //         ),
            //       ),
            //     ),
            //   ),
            // );
          }),
    );
  }

  onItemClick(int index) {
    setState(() {
      _data.forEach((gender) => gender.isSelected = false);
      _data[index].isSelected = true;
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}
