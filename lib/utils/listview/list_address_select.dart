
import 'package:flutter/material.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../../main.dart';
import 'model/list_model.dart';

class ListAddressSelector extends StatefulWidget {
  ListAddressSelector({
    this.title = '',
    this.data,
    this.count = 1,
    this.padding = padding_10_LR,
    this.alignment = Alignment.center,
    this.borderRadius = 15,
    this.onItemClick,
    this.bgColor = color_d6d6d6, //color_eafbf8
    this.enableColor = color_black, //color_main
  });

  final String title;
  final dynamic data;
  final Function? onItemClick;
  final count;
  final padding;
  final alignment;
  final double borderRadius;
  final Color bgColor;
  final Color enableColor;

  ListAddressSelectorState createState() => ListAddressSelectorState();
}

class ListAddressSelectorState extends State<ListAddressSelector> {
  late String _title;
  List<SingleItem> _data = [];

  ///그리드뷰 카운트
  int count = 4;
  late var padding;
  late var alignment;
  late double borderRadius;
  late TextStyle textStyle;
  late Color bgColor;
  late Color enableColor;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _data = widget.data ?? [];
    count = widget.count;
    padding = widget.padding;
    alignment = widget.alignment;
    borderRadius = widget.borderRadius;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor;

  }

  @override
  Widget build(BuildContext context) {

    return lContainer(
        child: Scrollbar(
      child: ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: _data.length,
          physics: const AlwaysScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return lInkWell(
                onTap: () {
                  onItemClick(index);
                },
                child: Container(
                  height: 60,
                  padding: padding,
                  alignment: alignment,
                  color: _data[index].isSelected ? bgColor : color_transparent,
                  child: lAutoSizeText(
                    _data[index].name,
                    maxLines: 1,
                    style: st_b_16(textColor: _data[index].isSelected ? enableColor : color_222222, fontWeight: _data[index].isSelected ? FontWeight.w500 : FontWeight.normal),
                  ),
                ));
            // child: SingleRadio(_data[index]));
          }),
    ));
  }

  onItemClick(int index) {
    log.d('『GGUMBI』>>> onItemClick : {_data[index]}:11 ${_data[index]},  <<< ');
    setState(() {
      _data.forEach((gender) => gender.isSelected = false);
      _data[index].isSelected = true;
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}
