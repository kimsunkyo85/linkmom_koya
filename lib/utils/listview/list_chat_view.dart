import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:linkmom/data/network/models/data/chat_main_result_data.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/enum_utils.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/chat/chat_view/chat_care_info_view.dart';

import '../../main.dart';
import '../string_util.dart';

///#### All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
///#### Created by   : platformbiz@ggumbi.com
///#### version      : 1.0.0
///#### see          : list_chat_view.dart - 채팅 목록
///#### since        : 2021/06/07 / update:
class ListChatView extends StatefulWidget {
  ListChatView({
    this.data,
    this.viewMode,
    this.padding = padding_15_LR,
    this.borderRadius = 16,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.enableColor = color_545454,
    this.disableColor = color_999999,
    this.onItemClick,
    this.isOverlap = false,
    this.imagePath = '',
    this.isRight = true,
    this.viewType = ViewType.normal,
    this.tabType = ChatTabType.normal,
    this.isLoading = false,
    this.emptyView,
    this.controller,
  });

  final dynamic data;
  final viewMode;
  final Function? onItemClick;
  final padding;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color enableColor;
  final Color disableColor;
  final bool isOverlap;
  final String imagePath;
  final bool isRight;
  final ViewType viewType;
  final ChatTabType tabType;
  final isLoading;
  final Widget? emptyView;
  final ScrollController? controller;

  ListChatViewState createState() => ListChatViewState();
}

class ListChatViewState extends State<ListChatView> {
  List<ChatMainResultsData> _data = [];

  var padding;
  late ViewMode viewMode;
  late double borderRadius;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;
  late String imagePath;
  late bool isRight;
  late bool isLoading;
  late ViewType viewType;
  late ChatTabType tabType;
  late ScrollController? controller;
  bool isDelete = false;

  @override
  void initState() {
    super.initState();
    viewMode = widget.viewMode;
    padding = widget.padding;
    borderRadius = widget.borderRadius;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor;
    disableColor = widget.disableColor;
    isOverlap = widget.isOverlap;
    isRight = widget.isRight;
    isLoading = widget.isLoading;
    controller = widget.controller;
  }

  @override
  Widget build(BuildContext context) {
    _data = widget.data ?? [];
    isLoading = widget.isLoading;
    viewType = widget.viewType;
    tabType = widget.tabType;
    return Stack(
      children: [
        lContainer(
          color: color_transparent,
          padding: padding_10_TB,
          alignment: Alignment.center,
          child: lScrollbar(
            child: ListView.separated(
              controller: controller,
              itemCount: _data.length,
              physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              itemBuilder: (BuildContext context, int index) {
                ChatMainResultsData item = _data[index];
                return Slidable(
                  key: Key(item.id.toString()),
                  endActionPane: ActionPane(
                    motion: ScrollMotion(), //옆에서 나오는 모션
                    // motion: BehindMotion(),//가만히 있는 모션
                    extentRatio: 0.2,
                    children: [
                      SlidableAction(
                        flex: 2,
                        onPressed: (value) => onItemClick(index, DialogAction.delete),
                        backgroundColor: color_ff4c1b,
                        foregroundColor: Colors.white,
                        icon: Icons.exit_to_app_rounded,
                        label: "나가기".tr(),
                      ),
                    ],
                  ),
                  child: Builder(builder: (context) {
                    return lInkWell(
                        onTap: () {
                          if (widget.onItemClick != null) {
                            Slidable.of(context)!.close();
                            onItemClick(index, DialogAction.select);
                          }
                        },
                        child: Container(
                          padding: padding_10_LR,
                          child: ChatView(
                            item,
                            viewMode: viewMode,
                            padding: padding,
                            borderRadius: borderRadius,
                            fontWeight: fontWeight,
                            bgColor: bgColor,
                            enableColor: enableColor,
                            disableColor: disableColor,
                            viewType: viewType,
                            tabType: tabType,
                            onItemClick: (action) {
                              onItemClick(index, action);
                            },
                          ),
                        ));
                  }),
                );
              },
              separatorBuilder: (BuildContext context, int index) => lDivider(color: color_eeeeee),
            ),
          ),
        ),
        if (_data.isEmpty && !isLoading) _emptyView(tabType),
      ],
    );
  }

  Widget _emptyView(ChatTabType type) {
    return type == ChatTabType.normal
        ? lEmptyView(
            img: Lcons.no_data_chat().name,
            title: "채팅친구가없어요".tr(),
            content: "링크쌤커뮤니티에서".tr(),
            textBtn: "커뮤니티마실가기".tr(),
            paddingBtn: padding_50_LR,
            fn: () => Commons.isGpsWithAddressCheck(context, nextRoute: routeChat, subIndex: TAB_COMMUNITY),
          )
        : lEmptyView(
            img: Lcons.no_data_chat().name,
            title: "채팅친구가없어요".tr(),
            content: Commons.isLinkMom() ? "당신과잘맞는맘대디".tr() : "당신과잘맞는링크쌤".tr(),
            textBtn: Commons.isLinkMom() ? "맘대디찾기".tr() : "링크쌤찾기".tr(),
            paddingBtn: padding_50_LR,
            fn: () => Commons.isGpsWithAddressCheck(context, nextRoute: routeChat, subIndex: TAB_LIST),
          );
  }

  // ///슬라이드 선택 0:슬라드시 바로보임, 1:서서히 나타남, 2:슬라이드, 3:Drawer
  // static Widget? _getActionPane(int index) {
  //   switch (index % 4) {
  //     case 0:
  //       return SlidableBehindActionPane();
  //     case 1:
  //       return SlidableStrechActionPane();
  //     case 2:
  //       return SlidableScrollActionPane();
  //     case 3:
  //       return SlidableDrawerActionPane();
  //     default:
  //       return null;
  //   }
  // }

  onItemClick(int index, DialogAction action) {
    setState(() {
      if (action == DialogAction.deleteAll) {
        _data.forEach((value) {
          value.isDelete = isDelete;
        });
      }
    });

    if (widget.onItemClick != null) {
      widget.onItemClick!(_data[index], action);
    }
  }
}

class ChatView extends StatelessWidget {
  final ChatMainResultsData _data;
  ViewMode? viewMode;
  final EdgeInsets? padding;
  final margin;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  Color enableColor = color_545454;
  Color disableColor = color_999999;
  final bool isOverlap;
  final ViewType? viewType;
  late ChatTabType? tabType;
  final Function? onItemClick;
  final int index;

  ChatView(
    this._data, {
    this.viewMode,
    this.padding,
    this.margin,
    this.borderRadius = 16,
    this.fontWeight = FontWeight.w400,
    this.bgColor = Colors.transparent,
    this.enableColor = color_545454,
    this.disableColor = color_999999,
    this.isOverlap = true,
    this.viewType,
    this.tabType,
    this.onItemClick,
    this.index = 0,
  });

  Size sizeName = Size(widthFull(navigatorKey.currentContext!) * 0.70, 0);
  Size sizeMomDady = Size(54, 0);
  Size sizeLinkMom = Size(54, 0);
  Size sizeTime = Size(59, 0);

  @override
  Widget build(BuildContext context) {
    return _chatListView(context);
  }

  ///채팅 목록 화면
  Widget _chatListView(BuildContext context) {
    double imageSize = 50;
    ServiceType serviceType = ServiceType.serviceType_0;
    if (_data.bookingcareservices_info!.careinfo != null) {
      serviceType = EnumUtils.getServiceType(index: _data.bookingcareservices_info!.careinfo!.servicetype);
    }

    String formatType = F_HMM;
    String time = StringUtils.getDateTime(_data.sendtime, callBack: (format) {
      formatType = format;
    });
    int termSize = 20;
    if (formatType == F_YYMMDD || formatType == F_HMM) {
      termSize = 55;
    }
    // log.d('『GGUMBI』>>> _chatListView : {sizeName.width: ${sizeName.width}, ${sizeMomDady.width}, ${sizeLinkMom.width}, ${sizeTime.width}  <<< ');
    double maxSize = sizeName.width - (sizeLinkMom.width + sizeMomDady.width + termSize);
    if (tabType == ChatTabType.normal) {
      maxSize = sizeName.width - (sizeLinkMom.width + sizeMomDady.width + termSize);
    } else {
      maxSize = sizeName.width - (sizeLinkMom.width + 30);
    }
    return Slidable(
        key: Key(_data.id.toString()),
        endActionPane: ActionPane(
          motion: ScrollMotion(), //옆에서 나오는 모션
          // motion: BehindMotion(),//가만히 있는 모션
          extentRatio: 0.2,
          children: [
            SlidableAction(
              flex: 2,
              onPressed: (ctx) => onItemClick!(_data, DialogAction.delete),
              backgroundColor: color_ff4c1b,
              foregroundColor: Colors.white,
              icon: Icons.exit_to_app_rounded,
              label: "나가기".tr(),
            ),
          ],
        ),
        child: Builder(
            builder: (context) => lInkWell(
                onTap: () {
                  if (onItemClick != null) {
                    Slidable.of(context)!.close();
                    onItemClick!(_data, DialogAction.select);
                  }
                },
                child: Container(
                  padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
                  child: Stack(
                    children: [
                      Container(
                        padding: padding ?? padding_10_LR,
                        width: widthFull(context),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              // crossAxisAlignment: CrossAxisAlignment.start,//주석처리시 가운데 정렬
                              children: [
                                //프로필사진
                                SizedBox(
                                  width: imageSize,
                                  height: imageSize,
                                  child: Material(elevation: 0, shape: CircleBorder(), clipBehavior: Clip.antiAlias, color: Colors.transparent, child: lProfileAvatar(_data.talker_info!.profileimg, radius: 30)),
                                ),
                                sb_w_15,
                                Expanded(
                                  child: Stack(
                                    children: [
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          //날짜 및 시간
                                          Row(
                                            children: [
                                              // Expanded(child: nameView(_data.idTo)),
                                              Expanded(
                                                child: Row(
                                                  children: [
                                                    Container(
                                                      constraints: BoxConstraints(
                                                        maxWidth: maxSize,
                                                      ),
                                                      child: lText(
                                                        _data.talker_info!.first_name,
                                                        maxLines: 1,
                                                        overflow: TextOverflow.ellipsis,
                                                        style: st_b_15(
                                                          fontWeight: FontWeight.bold,
                                                        ),
                                                      ),
                                                    ),
                                                    if (tabType == ChatTabType.carematching && _data.talker_info!.talker_matching_mode == ChatTabType.linkmom.bywho || _data.talker_info!.is_user_momdady && tabType == ChatTabType.normal)
                                                      Padding(
                                                        padding: EdgeInsets.only(left: 6, right: 3),
                                                        child: lTextOutLineBtn(
                                                          "링크쌤".tr(),
                                                          bg: color_linkmom,
                                                          padding: EdgeInsets.only(top: 2, bottom: 2, left: 7, right: 7),
                                                          style: st_11(fontWeight: FontWeight.bold, textColor: color_linkmom),
                                                        ),
                                                      ),
                                                    if (tabType == ChatTabType.carematching && _data.talker_info!.talker_matching_mode == ChatTabType.momdady.bywho || _data.talker_info!.is_user_linkmom && tabType == ChatTabType.normal)
                                                      Padding(
                                                        padding: EdgeInsets.only(left: tabType == ChatTabType.carematching ? 6 : 3, right: 6),
                                                        child: lTextOutLineBtn(
                                                          "맘대디".tr(),
                                                          bg: color_main,
                                                          padding: EdgeInsets.only(top: 2, bottom: 2, left: 7, right: 7),
                                                          style: st_11(fontWeight: FontWeight.bold, textColor: color_main),
                                                        ),
                                                      ),
                                                  ],
                                                ),
                                              ),
                                              sb_w_04,
                                              lText(
                                                time,
                                                style: st_b_14(textColor: color_b2b2b2),
                                              ),
                                            ],
                                          ),
                                          sb_h_02,
                                          //매시지 내용
                                          lText(
                                            // setMsgFormat(_data.msg),
                                            _data.msg,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                            style: st_b_14(),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),

                            //돌봄채팅인 경우
                            if (_data.chattingtype == ChattingType.care.index)
                              Container(
                                padding: padding_10_TB,
                                child: Row(
                                  children: [
                                    lTextBtn(
                                      serviceType.string,
                                      bg: serviceType.color,
                                      padding: EdgeInsets.only(top: 2, bottom: 2, left: 7, right: 7),
                                      style: st_12(fontWeight: FontWeight.bold),
                                    ),
                                    sb_w_05,
                                    Expanded(
                                      child: lScrollView(
                                        scrollDirection: Axis.horizontal,
                                        padding: padding_0,
                                        child: chatCareSummaryInfoView([
                                          _data.bookingcareservices_info!.childinfo!.child_name,
                                          _data.bookingcareservices_info!.childinfo!.child_age,
                                          _data.bookingcareservices_info!.childinfo!.child_gender,
                                          StringUtils.getCareDateCount(_data.bookingcareservices_info!.careinfo!.caredate!),
                                          StringUtils.getCareTime([_data.bookingcareservices_info!.careinfo!.stime, _data.bookingcareservices_info!.careinfo!.etime]),
                                        ], textColor: color_9e9e9e),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                          ],
                        ),
                      ),
                      if (!_data.is_receive)
                        Align(
                          alignment: Alignment.topLeft,
                          child: lBadge(1, isIcon: false, isStart: true, top: 0.0, end: 4.0),
                        ),
                    ],
                  ),
                ))));
  }

  ///메시지가 한줄 이상인 경우 표시
  String setMsgFormat(String msg) {
    if (StringUtils.validateString(msg)) {
      var value = msg.split('\n');
      if (value.length > 1) {
        msg = '${value[0]}...';
      }
      return msg;
    }
    return msg;
  }
}
