import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

import 'list_single_radio.dart';
import 'model/list_model.dart';

class ListSingleSelector extends StatefulWidget {
  ListSingleSelector({
    this.title = '',
    this.data,
    this.count = 4,
    this.margin = padding_05,
    this.borderRadius = 20,
    this.itemHeight = listImgHeight,
    this.itemWidth = listImgWidth,
    this.imageHeight = 35,
    this.imageWidth = 35,
    this.onItemClick,
    this.bgColor = color_transparent,
    this.isImage = true,
    this.isOverlap = false,
  });

  final String title;
  final dynamic data;
  final Function? onItemClick;
  final count;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;
  final Color? bgColor;
  final bool isImage;
  final bool isOverlap;

  ListSingleSelectorState createState() => ListSingleSelectorState();
}

class ListSingleSelectorState extends State<ListSingleSelector> {
  late String _title;
  List<SingleItem> _data = [];

  ///그리드뷰 카운트
  int count = 4;
  late var margin;
  late double borderRadius;
  late double itemHeight;
  late double itemWidth;
  late double imageHeight;
  late double imageWidth;
  late TextStyle textStyle;
  late Color bgColor;
  late bool isImage;
  late bool isOverlap;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _data = widget.data ?? [];
    count = widget.count;
    margin = widget.margin;
    borderRadius = widget.borderRadius;
    itemHeight = widget.itemHeight;
    itemWidth = widget.itemWidth;
    imageHeight = widget.imageHeight;
    imageWidth = widget.imageWidth;
    bgColor = widget.bgColor!;
    isImage = widget.isImage;
    isOverlap = widget.isOverlap;

    if (_data.length != 0) {
      if (count != 2) {
        if (!StringUtils.validateString(_data[0].image)) {
          count = 1;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return lContainer(
      color: bgColor,
      child: Column(
        children: [
          if (StringUtils.validateString(_title))
            lTvListTitle(
              _title,
            ),
          count == 1
              ? ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: _data.length,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return lInkWell(
                        onTap: () {
                          if (widget.onItemClick != null) {
                            onItemClick(index);
                          }
                        },
                        child: SingleRadio(_data[index]));
                  })
              : GridView.builder(
                  padding: padding_0,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: count,
                  ),
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: _data.length,
                  itemBuilder: (context, index) {
                    return lContainer(
                      child: lInkWell(
                          // splashColor: Colors.pinkAccent,
                          onTap: () {
                            onItemClick(index);
                          },
                          child: SingleRadio(
                            _data[index],
                            isImage: isImage,
                            margin: margin,
                            borderRadius: borderRadius,
                            itemHeight: itemHeight,
                            itemWidth: itemWidth,
                            imageWidth: imageWidth,
                            imageHeight: imageHeight,
                            count: count,
                          )),
                    );
                  }),
        ],
      ),
    );
  }

  onItemClick(int index) {
    setState(() {
      if (isOverlap) {
        ///중복선택시
        if (_data[index].name == "없음".tr()) {
          _data.forEach((value) => value.isSelected = false);
          _data[index].isSelected = true;
        } else {
          _data.forEach((value) {
            if (value.name == "없음".tr()) {
              value.isSelected = false;
            }
          });
          _data[index].isSelected = !_data[index].isSelected;
        }
      } else {
        ///단일 선택시
        _data.forEach((gender) => gender.isSelected = false);
        _data[index].isSelected = true;
      }
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}
