import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../commons.dart';
import 'model/list_model.dart';

class ListJobServiceTypeSelector extends StatefulWidget {
  ListJobServiceTypeSelector({
    this.data,
    this.padding = padding_15_LR,
    this.borderRadius = 50,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.enableColor = color_545454,
    this.disableColor = color_999999,
    this.onItemClick,
    this.isOverlap = false,
    this.imagePath = '',
    this.isRight = true,
  });

  final dynamic data;
  final Function? onItemClick;
  final padding;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color enableColor;
  final Color disableColor;
  final bool isOverlap;
  final String imagePath;
  final bool isRight;

  ListJobServiceTypeSelectorState createState() => ListJobServiceTypeSelectorState();
}

class ListJobServiceTypeSelectorState extends State<ListJobServiceTypeSelector> {
  List<SingleItem> _data = [];
  var padding;
  late double borderRadius;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;
  late String imagePath;
  late bool isRight;

  @override
  void initState() {
    super.initState();
    _data = widget.data ?? [];
    padding = widget.padding;
    borderRadius = widget.borderRadius;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor;
    disableColor = widget.disableColor;
    isOverlap = widget.isOverlap;
    isRight = widget.isRight;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        lContainer(
          height: 35,
          color: bgColor,
          margin: padding_10_TB,
          alignment: Alignment.center,
          // child: GridView.builder(
          //     padding: padding_0,
          //     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          //       crossAxisCount: 4,
          //       childAspectRatio: 2.3,
          //     ),
          //     shrinkWrap: true,
          //     physics: const NeverScrollableScrollPhysics(),
          //     itemCount: _data.length,
          //     itemBuilder: (context, index) {
          //       return lInkWell(
          //           onTap: () {
          //             // TODO: GGUMBI 2021/06/07 - 추후 학습 오픈시 수정
          //             if (widget.onItemClick != null && !isLearning(_data[index].name)) {
          //               onItemClick(index);
          //             }
          //           },
          //           child: ServiceTypeView(
          //             _data[index],
          //             padding: padding,
          //             borderRadius: borderRadius,
          //             fontWeight: fontWeight,
          //             bgColor: bgColor,
          //             enableColor: enableColor,
          //             disableColor: disableColor,
          //           ));
          //     }),

          child: ListView.builder(
              itemCount: _data.length,
              scrollDirection: Axis.horizontal,
              physics: const AlwaysScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                return lInkWell(
                    onTap: () {
                      if (widget.onItemClick != null) {
                        onItemClick(index);
                      }
                    },
                    child: ServiceTypeView(
                      _data[index],
                      padding: padding,
                      borderRadius: borderRadius,
                      fontWeight: fontWeight,
                      bgColor: bgColor,
                      enableColor: enableColor,
                      disableColor: disableColor,
                    ));
              }),
        ),
      ],
    );
  }

  onItemClick(int index) {
    setState(() {
      if (isOverlap) {
        ///중복선택시
        _data[index].isSelected = !_data[index].isSelected;
      } else {
        ///단일 선택시
        _data.forEach((gender) => gender.isSelected = false);
        _data[index].isSelected = true;
      }
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}

class ServiceTypeView extends StatelessWidget {
  final SingleItem _data;
  final padding;
  final margin;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  Color enableColor = color_545454;
  Color disableColor = color_999999;
  final bool isOverlap;

  ServiceTypeView(
    this._data, {
    this.padding,
    this.margin,
    this.borderRadius = 16,
    this.fontWeight = FontWeight.w400,
    this.bgColor = Colors.transparent,
    this.enableColor = color_545454,
    this.disableColor = color_999999,
    this.isOverlap = true,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 0,
        semanticContainer: false,
        borderOnForeground: false,
        color: _data.isSelected ? Commons.getColorServiceTypeBg() : color_f6f6f6,
        shape: lRoundedRectangleBorder(
          isSelected: _data.isSelected,
          width: 0,
          borderRadius: borderRadius,
          // enBorderColor: isLearning(_data.name) ? color_999999 : Commons.getColorRevers(),
          enBorderColor: color_transparent,
          disBorderColor: color_transparent,
        ),
        margin: padding_10_R,
        child: Container(
          alignment: Alignment.center,
          padding: padding,
          child: Row(
            children: [
              Lcons.check(size: 20, isEnabled: _data.isSelected, color: Commons.getColor(), disableColor: color_9e9e9e),
              sb_w_02,
              lText(
                _data.name,
                style: st_b_16(
                  fontWeight: FontWeight.bold,
                  textColor: isLearning(_data.name)
                      ? color_999999
                      : _data.isSelected
                          ? Commons.getColor()
                          : color_9e9e9e,
                ),
              ),
            ],
          ),
        ));
  }
}

bool isLearning(String type) {
  return type == "학습".tr() ? true : false;
}
