import 'package:flutter/material.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../commons.dart';
import 'model/list_model.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : list_tag_view.dart - 링크쌤 리스트 조회 하단, 해시태그
/// since        : 2021/06/09 / update:
class ListTagView extends StatefulWidget {
  ListTagView({
    this.data,
    this.height = 30.0,
    this.margin = padding_05_R,
    this.padding = padding_15_LR,
    this.borderRadius = 16,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.onItemClick,
  });

  final dynamic data;
  final height;
  final Function? onItemClick;
  final margin;
  final padding;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;

  ListTagViewState createState() => ListTagViewState();
}

class ListTagViewState extends State<ListTagView> {
  List<SingleItem> _data = [];
  late double height;
  late var margin;
  late var padding;
  late double borderRadius;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;

  @override
  void initState() {
    super.initState();
    _data = widget.data ?? [];
    height = widget.height;
    margin = widget.margin;
    padding = widget.padding;
    borderRadius = widget.borderRadius;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        lContainer(
          height: height,
          color: bgColor,
          child: ListView.builder(
              itemCount: _data.length,
              scrollDirection: Axis.horizontal,
              physics: const AlwaysScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                return lInkWell(
                    onTap: () {
                      if (widget.onItemClick != null) {
                        onItemClick(index);
                      }
                    },
                    child: TagView(
                      _data[index],
                      margin: margin,
                      padding: padding,
                      borderRadius: borderRadius,
                      fontWeight: fontWeight,
                      bgColor: bgColor,
                    ));
              }),
        ),
      ],
    );
  }

  onItemClick(int index) {
    setState(() {
      _data.forEach((gender) => gender.isSelected = false);
      _data[index].isSelected = true;
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}

class TagView extends StatelessWidget {
  final SingleItem _data;
  final padding;
  final margin;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;

  TagView(
    this._data, {
    this.padding,
    this.margin,
    this.borderRadius = 16,
    this.fontWeight = FontWeight.w400, this.bgColor = Colors.transparent
  });

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 0,
        semanticContainer: false,
        borderOnForeground: false,
        color: Commons.getColorBg(),
        shape: lRoundedRectangleBorder(width: 0, disBorderColor: color_transparent, borderRadius: borderRadius),
        margin: margin,
        child: Container(
          alignment: Alignment.center,
          padding: padding,
          child: lText(
            _data.name,
            style: st_b_12(fontWeight: FontWeight.w500, textColor: color_545454),
          ),
        ));
  }
}
