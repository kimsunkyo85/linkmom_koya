import 'package:flutter/material.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../commons.dart';
import 'model/list_model.dart';

class ListAreaView extends StatefulWidget {
  ListAreaView({
    this.title = '',
    this.data,
    this.viewMode,
    this.user_type = USER_TYPE.mom_daddy,
    this.count = 1,
    this.childAspectRatio = 3.0,
    this.imageHeight = 35,
    this.imageWidth = 35,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.enableColor = color_main,
    this.disableColor = color_cecece,
    this.isOverlap = true,
    this.onItemClick,
  });

  final String title;
  final dynamic data;
  final ViewMode? viewMode;
  final USER_TYPE user_type;
  final count;
  final childAspectRatio;
  final double imageHeight;
  final double imageWidth;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color enableColor;
  final Color disableColor;
  final bool isOverlap;
  final Function? onItemClick;

  ListAreaViewState createState() => ListAreaViewState();
}

class ListAreaViewState extends State<ListAreaView> {
  late String _title;
  List<SingleItem> _data = [];
  late ViewMode viewMode;
  late USER_TYPE user_type;

  ///그리드뷰 카운트
  int count = 1;
  late double childAspectRatio;
  late double imageHeight;
  late double imageWidth;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _data = widget.data ?? [];
    viewMode = widget.viewMode ?? ViewMode();
    user_type = widget.user_type;
    count = widget.count;
    childAspectRatio = widget.childAspectRatio;
    imageHeight = widget.imageHeight;
    imageWidth = widget.imageWidth;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor;
    disableColor = widget.disableColor;
    isOverlap = widget.isOverlap;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (StringUtils.validateString(_title))
          lTvListTitle(
            _title,
          ),
        lContainer(
            color: bgColor,
            child: GridView.builder(
                padding: padding_0,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: count,
                  childAspectRatio: 2.5,
                ),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: _data.length,
                itemBuilder: (context, index) {
                  return lInkWell(
                      onTap: () {
                        if (widget.onItemClick != null) {
                          onItemClick(index);
                        }
                      },
                      child: CareTypeView(
                        _data[index],
                        viewMode: viewMode,
                        imageWidth: imageWidth,
                        imageHeight: imageHeight,
                        count: count,
                        fontWeight: fontWeight,
                        bgColor: bgColor,
                        enableColor: enableColor,
                        disableColor: disableColor,
                      ));
                })),
      ],
    );
  }

  onItemClick(int index) {
    setState(() {
      if (isOverlap) {
        ///중복선택시
        _data[index].isSelected = !_data[index].isSelected;
      } else {
        ///단일 선택시
        _data.forEach((gender) => gender.isSelected = false);
        _data[index].isSelected = true;
      }
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}

class CareTypeView extends StatelessWidget {
  final SingleItem data;
  final ViewMode? viewMode;
  final double imageHeight;
  final double imageWidth;
  final int count;
  final FontWeight fontWeight;
  final Color? bgColor;
  Color enableColor;
  Color disableColor;
  final bool isOverlap;

  CareTypeView(
    this.data, {
    this.viewMode,
    this.imageHeight = 35,
    this.imageWidth = 35,
    this.count = 0,
    this.fontWeight = FontWeight.w400,
    this.bgColor,
    this.enableColor = color_b2b2b2,
    this.disableColor = color_main,
    this.isOverlap = true,
  });

  @override
  Widget build(BuildContext context) {
    if (bgColor == color_f8f8fa) {
      enableColor = color_white;
      disableColor = color_textDisable;
    }
    return Card(
        elevation: 0,
        child: Container(
          alignment: Alignment.center,
          child: Row(
            children: [
              data.icon != null
                  ? Lcons(
                      data.icon!.name,
                      size: imageWidth,
                      color: Commons.getColorViewType(viewMode!.viewType),
                      disableColor: color_b2b2b2,
                      isEnabled: data.isSelected,
                    )
                  : Image.asset(
                      data.image,
                      width: imageWidth,
                      height: imageHeight,
                      color: data.isSelected ? Commons.getColorViewType(viewMode!.viewType) : color_b2b2b2,
                    ),
              sb_w_10,
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  lText(
                    Commons.possibleAreaSubText(data.name),
                    style: st_b_14(
                      isSelect: data.isSelected,
                      textColor: color_dedede,
                      disableColor: Commons.getColorViewType(viewMode!.viewType),
                      fontWeight: data.isSelected ? FontWeight.bold : FontWeight.normal,
                    ),
                  ),
                  if (data.values != null)
                    Row(
                      children: [
                        Lcons.check_circle(
                          isEnabled: data.values![0].isSelected,
                          color: Commons.getColorViewType(viewMode!.viewType),
                          disableColor: color_dedede,
                        ),
                        lText(
                          data.values![0].name,
                          style: st_b_14(
                            isSelect: data.values![0].isSelected,
                            textColor: color_dedede,
                            disableColor: color_545454,
                          ),
                        ),
                      ],
                    ),
                ],
              ),
            ],
          ),
        ));
  }
}
