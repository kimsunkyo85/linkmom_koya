import 'package:flutter/material.dart';
import 'package:linkmom/main.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

class ListNextView extends StatefulWidget {
  final AppBar? appbar;
  final List<Widget> view;
  final Widget? header, emptyView, topper, footer;
  final Function onRefresh, onNext;
  final bool showNextLoading;
  final Color? bgColor;
  final ScrollController? controller;

  ListNextView({
    Key? key,
    this.appbar,
    required this.view,
    this.header,
    this.emptyView,
    this.topper,
    this.footer,
    required this.onRefresh,
    required this.onNext,
    required this.showNextLoading,
    this.bgColor,
    this.controller,
  }) : super(key: key);

  @override
  State<ListNextView> createState() => _ListNextViewState();
}

class _ListNextViewState extends State<ListNextView> {
  int _length = 0;
  ScrollController _ctrl = ScrollController();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _updateIndex();
    return Container(
      color: widget.bgColor ?? Colors.transparent,
      child: Column(
        children: [
          if (widget.topper != null) widget.topper!,
          Expanded(
            child: RefreshView(
              controller: widget.controller ?? PrimaryScrollController.of(context),
              onNext: widget.onNext,
              onRefresh: widget.onRefresh,
              child: Container(
                child: lScrollbar(
                  controller: _ctrl,
                  child: ListView.builder(
                    controller: _ctrl,
                    shrinkWrap: true,
                    physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                    itemBuilder: (ctx, idx) {
                      try {
                        if (idx == 0) {
                          return widget.header ?? Container();
                        } else if (_length < 1) {
                          return widget.emptyView ?? lEmptyView(height: Commons.height(context, 0.5));
                        } else if (widget.showNextLoading && idx == _listCount - 1) {
                          return LoadingIndicator(showText: false, background: Colors.transparent, size: 40);
                        } else if (idx == _listCount - 1) {
                          return Container();
                        } else {
                          int current = idx - 1;
                          return widget.view[current];
                        }
                      } catch (e) {
                        log.e({
                          '--- ERR     ': e,
                        });
                        return Container();
                      }
                    },
                    itemCount: _listCount,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _updateIndex() => _length = widget.view.length;

  int get _listCount => _length + 2;
}

class SliverNextListView extends StatefulWidget {
  final AppBar? appbar;
  final List<Widget> view;
  final Widget? header, emptyView, topper, footer, pinned;
  final Function onRefresh, onNext;
  final bool showNextLoading;
  final Color? bgColor;
  final ScrollController? controller;

  SliverNextListView({
    Key? key,
    this.appbar,
    required this.view,
    this.header,
    this.emptyView,
    this.topper,
    this.footer,
    this.pinned,
    required this.onRefresh,
    required this.onNext,
    required this.showNextLoading,
    this.bgColor,
    this.controller,
  }) : super(key: key);

  @override
  State<SliverNextListView> createState() => _SliverNextListViewState();
}

class _SliverNextListViewState extends State<SliverNextListView> {
  int _length = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _updateIndex();
    return Container(
        color: widget.bgColor ?? Colors.transparent,
        child: CustomScrollView(
          physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
          slivers: [
            if (widget.topper != null) SliverToBoxAdapter(child: widget.topper!),
            if (widget.pinned != null)
              SliverAppBar(
                flexibleSpace: widget.pinned,
                pinned: true,
                backgroundColor: Colors.transparent,
                leading: Container(),
                toolbarHeight: kMinInteractiveDimension,
              ),
            SliverFillRemaining(
              child: RefreshView(
                controller: widget.controller,
                onNext: widget.onNext,
                onRefresh: widget.onRefresh,
                child: ListView.builder(
                  physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
                  itemBuilder: (ctx, idx) {
                    try {
                      if (idx == 0) {
                        return widget.header ?? Container();
                      } else if (_length < 1) {
                        return widget.emptyView ?? lEmptyView(height: Commons.height(context, 0.5));
                      } else if (widget.showNextLoading && idx == _listCount - 1) {
                        return LoadingIndicator(showText: false, background: Colors.transparent, size: 40);
                      } else if (idx == _listCount - 1) {
                        return Container();
                      } else {
                        int current = idx - 1;
                        return widget.view[current];
                      }
                    } catch (e) {
                      log.e({
                        '--- ERR     ': e,
                      });
                      return Container();
                    }
                  },
                  itemCount: _listCount,
                ),
            ),
          ),
        ],
        ));
  }

  void _updateIndex() => _length = widget.view.length;

  int get _listCount => _length + 2;
}
