import 'package:dotted_line/dotted_line.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/req_data.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../../main.dart';

class ListAddMove extends StatefulWidget {
  ListAddMove({
    this.title = '',
    this.data,
    this.margin = padding_05,
    this.borderRadius = 15,
    this.itemHeight = listImgHeight,
    this.itemWidth = listImgWidth,
    this.imageHeight = 35,
    this.imageWidth = 35,
    this.onItemClick,
  });

  final String title;
  final dynamic data;
  final Function? onItemClick;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;

  ListAddMoveState createState() => ListAddMoveState();
}

class ListAddMoveState extends State<ListAddMove> {
  late String _title;
  List<ReqBoyukData> _data = [];
  late var margin;
  late double borderRadius;
  late double itemHeight;
  late double itemWidth;
  late double imageHeight;
  late double imageWidth;
  late TextStyle textStyle;

  @override
  void initState() {
    super.initState();
    log.d('『GGUMBI』>>> initState : dfadfadf: , ${widget.data} <<< ');
    _title = widget.title;
    _data = widget.data ?? [];
    margin = widget.margin;
    borderRadius = widget.borderRadius;
    itemHeight = widget.itemHeight;
    itemWidth = widget.itemWidth;
    imageHeight = widget.imageHeight;
    imageWidth = widget.imageWidth;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (StringUtils.validateString(_title))
          lTvListTitle(
            _title,
          ),
        ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: _data.length,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return MoveRadio(
                _data[index],
                index: index,
                length: _data.length,
                onClick: (action) {
                  log.d('『GGUMBI』>>> build : action: $action,  <<< ');
                  if (widget.onItemClick != null) {
                    widget.onItemClick!(action, index);
                  }
                },
              );
            })
      ],
    );
  }

  onItemClick(int index) {
    setState(() {
      // _data.forEach((gender) => gender.isSelected = false);
      // _data[index].isSelected = true;
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}

///아이템 선택 28 -> 16
class MoveRadio extends StatelessWidget {
  final ReqBoyukData data;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;
  final int index;
  final int length;
  final FontWeight fontWeight;
  final Function? onClick;

  MoveRadio(this.data, {this.margin, this.borderRadius = 15, this.itemHeight = 0.0, this.itemWidth = 0.0, this.imageHeight = 35, this.imageWidth = 35, this.index = 0, this.length = 0, this.fontWeight = FontWeight.normal, this.onClick});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          // height: itemHeight,
          // width: itemWidth,
          alignment: Alignment.centerLeft,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              lAutoSizeText(
                '${"추가이동장소-".tr()}${'data.product_name'}',
                style: st_b_16(fontWeight: FontWeight.bold),
              ),
              Row(
                children: [
                  Expanded(
                    child: lAutoSizeText(
                      'data.product_name',
                      style: st_14(textColor: color_aaaaaa),
                    ),
                  ),
                  lInkWell(
                    onTap: () {
                      log.d('『GGUMBI』>>> build ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
                      onClick!(DialogAction.modify);
                    },
                    child: lText("수정".tr()),
                  ),
                  sb_w_20,
                  lInkWell(
                      onTap: () {
                        log.d('『GGUMBI』>>> build ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
                        onClick!(DialogAction.delete);
                      },
                      child: lText("삭제".tr())),
                ],
              ),
              sb_h_05,
              lAutoSizeText(
                'data.route_address',
                style: st_14(textColor: color_aaaaaa),
              ),
              sb_h_05,
              lAutoSizeText(
                'data.route_comment',
                style: st_14(textColor: color_aaaaaa),
              )
            ],
          ),
        ),
        sb_h_15,
        if (index != length - 1)
          Column(
            children: [
              DottedLine(
                dashColor: color_d2d2d2,
                dashLength: 5.0,
                dashGapLength: 2.0,
              ),
              sb_h_15,
            ],
          ),
      ],
    );
  }
}
