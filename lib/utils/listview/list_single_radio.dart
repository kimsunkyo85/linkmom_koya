import 'package:flutter/material.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../commons.dart';
import 'model/list_model.dart';

///아이템 선택 28 -> 16
class SingleRadio extends StatelessWidget {
  final SingleItem data;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;
  final int count;
  final FontWeight fontWeight;
  final isImage;

  SingleRadio(this.data,
      {this.margin,
      this.borderRadius = 20,
      this.itemHeight = 0.0,
      this.itemWidth = 0.0,
      this.imageHeight = 35,
      this.imageWidth = 35,
      this.count = 0,
      this.fontWeight = FontWeight.normal,
      this.isImage = true});

  @override
  Widget build(BuildContext context) {
    if (!isImage) {
      data.image = '';
    }
    return !StringUtils.validateString(data.image)
        ? Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Lcons.check_circle(isEnabled: data.isSelected, color: Commons.getColor(), disableColor: color_dedede),
              sb_h_05,
              Expanded(
                child: Padding(
                  padding: padding_07,
                  child: lAutoSizeText(
                    data.name,
                    maxLines: 1,
                    style: st_b_16(fontWeight: fontWeight),
                  ),
                ),
              )
            ],
          )
        : Card(
            elevation: 0,
            // color: data.isSelected ? mainColor : color_white,
            shape: lRoundedRectangleBorder(isSelected: data.isSelected, width: data.isSelected ? 2.0 : 1.0, borderRadius: borderRadius, enBorderColor: Commons.getColor()),
            margin: margin,
            child: !StringUtils.validateString(data.image)
                ? Container(
                    padding: padding_10,
                    child: lText(
                      data.name,
                      style: st_b_14(),
                    ),
                  )
                : Container(
                    height: itemHeight,
                    width: itemWidth,
                    alignment: Alignment.center,
                    margin: padding_05,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          data.image,
                          width: imageWidth,
                          height: imageHeight,
                          color: data.isSelected ? Commons.getColor() : color_b2b2b2,
                        ),
                        count == 2 ? sb_h_15 : sb_h_05,
                        lText(
                          data.name,
                          style: count == 2 ? defaultStyle(isEnable: data.isSelected, fontSize: 20, color: color_b2b2b2, colorDis: Commons.getColor(), fontWeight: FontWeight.w500) : st_b_14(isSelect: data.isSelected, textColor: color_b2b2b2, disableColor: Commons.getColor()),
                        ),
                        if (data.name2 != null)
                          lText(
                            data.name2,
                            style: count == 2 ? defaultStyle(isEnable: data.isSelected, fontSize: 16, color: color_b2b2b2, colorDis: Commons.getColor(), fontWeight: FontWeight.w500) : st_b_14(isSelect: data.isSelected, textColor: color_b2b2b2, disableColor: Commons.getColor()),
                          )
                      ],
                    ),
                  ));
  }
}
