import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../commons.dart';
import 'model/list_model.dart';

class ListPayView extends StatefulWidget {
  ListPayView({
    this.title = '',
    this.data,
    this.viewMode,
    this.user_type = USER_TYPE.mom_daddy,
    this.count = 1,
    this.childAspectRatio = 1.7,
    this.borderRadius = 30,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.enableColor = color_main,
    this.disableColor = color_cecece,
    this.onItemClick,
    this.isOverlap = true,
  });

  final String title;
  final dynamic data;
  final ViewMode? viewMode;
  final USER_TYPE user_type;
  final Function? onItemClick;
  final count;
  final childAspectRatio;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color enableColor;
  final Color disableColor;
  final bool isOverlap;

  ListPayViewState createState() => ListPayViewState();
}

class ListPayViewState extends State<ListPayView> {
  late String _title;
  List<SingleItem> _data = [];
  late ViewMode viewMode;
  late USER_TYPE user_type;

  ///그리드뷰 카운트
  int count = 1;
  late double childAspectRatio;
  late double borderRadius;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _data = widget.data ?? [];
    viewMode = widget.viewMode ?? ViewMode();
    user_type = widget.user_type;
    count = widget.count;
    childAspectRatio = widget.childAspectRatio;
    borderRadius = widget.borderRadius;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor;
    disableColor = widget.disableColor;
    isOverlap = widget.isOverlap;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (StringUtils.validateString(_title))
          lTvListTitle(
            _title,
          ),
        lContainer(
            color: bgColor,
            child: GridView.builder(
                padding: padding_0,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: count,
                  childAspectRatio: childAspectRatio,
                ),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: _data.length,
                itemBuilder: (context, index) {
                  return lInkWell(
                      onTap: () {
                        if (widget.onItemClick != null) {
                          onItemClick(index);
                        }
                      },
                      child: PayView(
                        _data[index],
                        viewMode: viewMode,
                        borderRadius: borderRadius,
                        count: count,
                        fontWeight: fontWeight,
                        bgColor: bgColor,
                        enableColor: enableColor,
                        disableColor: disableColor,
                      ));
                })),
      ],
    );
  }

  onItemClick(int index) {
    setState(() {
      if (isOverlap) {
        ///중복선택시
        _data[index].isSelected = !_data[index].isSelected;
      } else {
        ///단일 선택시
        _data.forEach((gender) => gender.isSelected = false);
        _data[index].isSelected = true;
      }
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}

class PayView extends StatelessWidget {
  final SingleItem _data;
  final ViewMode? viewMode;
  final double borderRadius;
  final int count;
  final FontWeight fontWeight;
  final Color bgColor;
  Color enableColor;
  Color disableColor;
  final bool isOverlap;

  PayView(
    this._data, {
    this.viewMode,
    this.borderRadius = 30,
    this.count = 0,
    this.fontWeight = FontWeight.w400,
    this.bgColor = Colors.transparent,
    this.enableColor = color_b2b2b2,
    this.disableColor = color_main,
    this.isOverlap = true,
  });

  @override
  Widget build(BuildContext context) {
    if (bgColor == color_f8f8fa) {
      enableColor = color_white;
      disableColor = color_textDisable;
    }
    return Card(
        elevation: 0,
        shape: lRoundedRectangleBorder(isSelected: true, width: 0.0, borderRadius: borderRadius, enBorderColor: color_faf6fc),
        color: color_faf6fc,
        child: Container(
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              lText(
                _data.name,
                style: st_b_16(fontWeight: FontWeight.w500, textColor: _data.isSelected ? Commons.getColorViewType(viewMode!.viewType) : disableColor),
              ),
              sb_w_05,
              lText(
                '${"시급".tr()} ${StringUtils.formatPay(int.parse(_data.content))}${"원".tr()}',
                style: st_b_16(fontWeight: FontWeight.bold, textColor: Commons.getColorViewType(viewMode!.viewType)),
              ),
            ],
          ),
        ));
  }
}
