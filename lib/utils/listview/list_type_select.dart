import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../commons.dart';
import 'model/list_model.dart';

///돌봄 신청 돌봄유형, 보육장소 / 구직신청 돌봄유형
class ListTypeSelector extends StatefulWidget {
  ListTypeSelector(
      {this.title = '',
      this.data,
      this.user_type = USER_TYPE.mom_daddy,
      this.padding = padding_10,
      this.margin = padding_10_B,
      this.height = 120,
      this.borderRadius = 20,
      this.imageHeight = 50,
      this.imageWidth = 50,
      this.fontWeight = FontWeight.normal,
      this.bgColor = color_white,
      this.enableColor = color_main,
      this.disableColor = color_cecece,
      this.onItemClick,
      this.isOverlap = false,
      this.serviceType});

  final String title;
  final dynamic data;
  final USER_TYPE user_type;
  final Function? onItemClick;
  final padding;
  final margin;
  final double height;
  final double borderRadius;
  final double imageHeight;
  final double imageWidth;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color enableColor;
  final Color disableColor;
  final bool isOverlap;
  final ServiceType? serviceType;

  ListTypeSelectorState createState() => ListTypeSelectorState();
}

class ListTypeSelectorState extends State<ListTypeSelector> {
  late String _title;
  List<SingleItem> _data = [];
  late USER_TYPE user_type;
  late var padding;
  late var margin;
  late double height;
  late double borderRadius;
  late double imageHeight;
  late double imageWidth;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _data = widget.data ?? [];
    user_type = widget.user_type;
    padding = widget.padding;
    margin = widget.margin;
    height = widget.height;
    borderRadius = widget.borderRadius;
    imageHeight = widget.imageHeight;
    imageWidth = widget.imageWidth;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor;
    disableColor = widget.disableColor;
    isOverlap = widget.isOverlap;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (StringUtils.validateString(_title))
          lTvListTitle(
            _title,
          ),
        lContainer(
          color: bgColor,
          child: lScrollbar(
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: _data.length,
                physics: const AlwaysScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  var data = _data[index];
                  return lInkWell(
                    onTap: () => onItemClick(index),
                    child: Container(
                      height: height,
                      margin: margin,
                      child: Card(
                        elevation: 0,
                        shape: lRoundedRectangleBorder(isSelected: data.isSelected, width: data.isSelected ? 2.0 : 1.0, borderRadius: borderRadius, enBorderColor: Commons.getColor()),
                        child: Padding(
                          padding: padding,
                          child: Row(
                            children: [
                              Padding(
                                padding: padding_10,
                                child: data.icon != null
                                    ? Lcons(
                                        data.icon!.name,
                                        size: imageWidth,
                                        color: Commons.getColor(),
                                        isEnabled: data.isSelected,
                                        disableColor: color_b2b2b2,
                                      )
                                    : Image.asset(
                                        data.image,
                                        width: imageWidth,
                                        height: imageHeight,
                                        color: data.isSelected ? Commons.getColor() : color_b2b2b2,
                                      ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    lText(
                                      '${data.name}${data.name2}',
                                      style: st_b_18(textColor: data.isSelected ? Commons.getColor() : color_b2b2b2, fontWeight: FontWeight.bold),
                                    ),
                                    sb_h_03,
                                    Padding(
                                      padding: padding_10_R,
                                      child: lAutoSizeText(
                                        // widget.serviceType == null ? data.content : data.content + widget.serviceType!.string + "해주는서비스".tr(),
                                        data.content,
                                        maxLines: 2,
                                        style: st_b_14(
                                          textColor: color_545454,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                }),
          ),
        ),
      ],
    );
  }

  onItemClick(int index) {
    if (isOverlap) {
      ///중복선택시
      _data[index].isSelected = !_data[index].isSelected;
      if (_data[index].icon != null) _data[index].icon!.isEnabled = !_data[index].icon!.isEnabled;
    } else {
      ///단일 선택시
      _data.forEach((gender) => gender.isSelected = false);
      _data[index].isSelected = true;
      if (_data[index].icon != null) _data[index].icon!.isEnabled = true;
    }
    if (widget.onItemClick != null) {
      widget.onItemClick!(_data[index]);
    }
  }
}
