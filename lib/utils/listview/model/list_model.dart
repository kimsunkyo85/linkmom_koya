import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:linkmom/data/storage/model/address_model.dart';
import 'package:linkmom/data/storage/model/menu_file_data.dart';
import 'package:linkmom/view/lcons.dart';

class SingleItem {
  String name;
  String name2;
  String content;
  List<SingleItem>? values = [];
  String keyName;
  String image;
  String imageDisable;
  int type;
  bool isSelected;
  dynamic data;
  Lcons? icon;

  SingleItem(
    this.name, {
    this.name2 = '',
    this.content = '',
    this.keyName = '',
    this.image = '',
    this.imageDisable = '',
    this.isSelected = false,
    this.type = 0,
    this.values,
    this.data,
    this.icon,
  });

  SingleItem.clone(SingleItem item)
      : this(
          item.name,
          name2: item.name2,
          content: item.content,
          keyName: item.keyName,
          image: item.image,
          imageDisable: item.imageDisable,
          isSelected: item.isSelected,
          type: item.type,
          values: item.values,
          data: item.data,
          icon: item.icon,
        );

  @override
  String toString() {
    return 'SingleItem{name: $name, name2: $name2, content: $content, values: $values, keyName: $keyName, image: $image, imageDisable: $imageDisable, type: $type, isSelected: $isSelected, data: $data, icon: $icon}';
  }
}

class MultiItem {
  String name;
  String name2;
  String content;
  List<MultiItem>? values = [];
  String keyName;
  String price;
  String image;
  String imageDisable;
  int type;
  bool isSelected;
  dynamic data;

  MultiItem(
    this.name, {
    this.name2 = '',
    this.content = '',
    this.keyName = '',
    this.price = '',
    this.image = '',
    this.imageDisable = '',
    this.isSelected = false,
    this.type = 0,
    this.values,
    this.data,
  });

  MultiItem.clone(MultiItem item)
      : this(
          item.name,
          name2: item.name2,
          content: item.content,
          keyName: item.keyName,
          price: item.price,
          image: item.image,
          imageDisable: item.imageDisable,
          isSelected: item.isSelected,
          type: item.type,
          values: item.values,
          data: item.data,
        );

  @override
  String toString() {
    return 'MultiItem{name: $name, name2: $name2, content: $content, values: $values, keyName: $keyName, price: $price, image: $image, imageDisable: $imageDisable, type: $type, isSelected: $isSelected, data: $data}';
  }
}

///추가 서비스 선택시 아이템 만들기
class ServicesSelectItem {
  ///이미지 주소
  late String? image;

  ///선택유무
  late bool isSelected;

  ///총 시간
  int time = 0;

  late CareTypesData? cares;

  late ProductData? products;

  late List<ServicesSelectItem>? values;

  late Lcons? icon;

  ServicesSelectItem({this.image = '', this.isSelected = false, this.time = 0, this.cares, this.products, this.values, this.icon});

  ServicesSelectItem.init() {
    time = 0;
    image = null;
    icon = null;
    isSelected = false;
    cares = CareTypesData();
    products = ProductData();
    values = [];
  }

  ServicesSelectItem.clone(ServicesSelectItem item)
      : this(
          time: item.time,
          image: item.image,
          isSelected: item.isSelected,
          cares: item.cares,
          products: item.products,
          values: item.values,
          icon: item.icon,
        );

  @override
  String toString() {
    return 'ServicesSelectItem{image: $image, isSelected: $isSelected, time: $time, cares: $cares, products: $products, values: $values}';
  }
}

class ListItem {
  String title;

  // List<String> items;
  String? items;

  ProductData? productData;

  ListItem(this.title, {this.productData, this.items});

  @override
  String toString() {
    return 'InfoItem{title: $title, items: $items}';
  }
}

///주소 위치 검색용 (시도 + 시군구 리스트)
@JsonSerializable()
class AddressListItem {
  static const String Key_address1_id = 'address1_id';
  static const String Key_address1 = 'address1';
  static const String Key_data = 'data';

  AddressListItem({
    this.address1_id = '',
    this.address1 = '',
    this.data,
  });

  AddressListItem.init() {
    data = [];
  }

  @JsonKey(name: Key_address1_id)
  String address1_id = '';
  @JsonKey(name: Key_address1)
  String address1 = '';
  @JsonKey(name: Key_data)
  List<AddressDataItem>? data = [];

  factory AddressListItem.fromJson(Map<String, dynamic> json) {
    List<AddressDataItem> data = json[Key_data] == null ? [] : json[Key_data].map<AddressDataItem>((i) => AddressDataItem.fromJson(i)).toList();
    return AddressListItem(
      address1_id: json[Key_address1_id] ?? '',
      address1: json[Key_address1] ?? '',
      data: data,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[Key_address1_id] = this.address1_id;
    data[Key_address1] = this.address1;
    data[Key_data] = this.data;
    return data;
  }

  static String encode(List<AddressListItem> musics) => json.encode(
        musics.map<Map<String, dynamic>>((music) => music.toJson()).toList(),
      );

  static List<AddressListItem> decode(String musics) => (json.decode(musics) as List<dynamic>).map<AddressListItem>((item) => AddressListItem.fromJson(item)).toList();

  @override
  String toString() {
    return 'AddressListItem{address1_id: $address1_id, data: $data}';
  }
}

// class ContentItem {
//   String content;
//   bool isSelected;
//
//   ContentItem({this.content, this.isSelected});
//
//   @override
//   String toString() {
//     return 'ContentItem{content: $content, isSelected: $isSelected}';
//   }
// }
