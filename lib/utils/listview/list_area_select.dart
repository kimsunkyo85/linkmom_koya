import 'package:flutter/material.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../commons.dart';
import 'model/list_model.dart';

class ListAreaSelector extends StatefulWidget {
  ListAreaSelector({
    this.title = '',
    this.data,
    this.count = 1,
    this.padding = padding_10_LR,
    this.margin = padding_05,
    this.alignment = Alignment.center,
    this.borderRadius = 30,
    this.isBorderRadius = true,
    this.onItemClick,
    this.bgColor,
    this.height = 55,
    this.isWidth = false,
  });

  final String title;
  final dynamic data;
  final Function? onItemClick;
  final count;
  final padding;
  final margin;
  final alignment;
  final double borderRadius;
  final bool isBorderRadius;
  final Color? bgColor;
  final double height;
  final bool isWidth;

  ListAreaSelectorState createState() => ListAreaSelectorState();
}

class ListAreaSelectorState extends State<ListAreaSelector> {
  List<SingleItem> _data = [];

  int count = 1;
  late var padding;
  late var margin;
  late var alignment;
  late double borderRadius;
  late bool isBorderRadius;
  late TextStyle textStyle;
  late Color bgColor;
  late double height;
  late bool isWidth;

  @override
  void initState() {
    super.initState();
    _data = widget.data ?? [];
    count = widget.count;
    padding = widget.padding;
    margin = widget.margin;
    alignment = widget.alignment;
    borderRadius = widget.borderRadius;
    isBorderRadius = widget.isBorderRadius;
    bgColor = widget.bgColor ?? Colors.white;
    if (widget.height == 0) {
      height = 55;
    } else {
      height = widget.height;
    }
    isWidth = widget.isWidth;
  }

  @override
  Widget build(BuildContext context) {
    return lContainer(
        color: bgColor,
        width: isWidth ? widthFull(context) : widthFull(context) * 0.5,
        child: Scrollbar(
          child: ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: _data.length,
              physics: const AlwaysScrollableScrollPhysics(),
              padding: padding_0,
              itemBuilder: (context, index) {
                return Padding(
                  padding: padding_10_B,
                  child: MaterialButton(
                    height: height,
                    elevation: 0,
                    highlightElevation: 0,
                    color: bgColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(borderRadius),
                      side: BorderSide(color: _data[index].isSelected ? Commons.getColor() : color_dedede),
                    ),
                    onPressed: () {
                      onItemClick(index);
                    },
                    child: Row(
                      children: [
                        Lcons.my_location(
                          disableColor: _data[index].isSelected ? Commons.getColor() : color_222222,
                          size: 17,
                        ),
                        Expanded(
                          child: Padding(
                            padding: padding_05_LR,
                            child: lText(_data[index].name, color: _data[index].isSelected ? Commons.getColor() : color_222222, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Lcons.check(
                          disableColor: _data[index].isSelected ? Commons.getColor() : color_dedede,
                        ),
                      ],
                    ),
                  ),
                );
              }),
        ));
  }

  onItemClick(int index) {
    setState(() {
      //2021/12/30 최근 주소 추후 변경 예정(개선사항 88번, 최근주소 주소별 클릭되는 내용은 없음)
      _data.forEach((gender) => gender.isSelected = false);
      _data[index].isSelected = true;
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}
