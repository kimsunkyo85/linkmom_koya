import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/data/network/models/linkmom_list_response.dart';
import 'package:linkmom/data/network/models/mypage_myinfo_response.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_list/location_view.dart';
import 'package:linkmom/view/main/job_list/name_view.dart';
import 'package:linkmom/view/main/mypage/myinfo_view.dart';

import '../../main.dart';
import '../commons.dart';
import '../string_util.dart';
import 'list_tag_view.dart';
import 'model/list_model.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : list_momdady_view.dart - 링크쌤 목록 리스트 ListJobLinkMomView
/// since        : 2021/06/07 / update:
class ListLinkMomView extends BaseStateful {
  ListLinkMomView({
    this.data,
    this.viewMode,
    this.padding = padding_15_LR,
    this.borderRadius = 16,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.enableColor = color_545454,
    this.disableColor = color_999999,
    this.onItemClick,
    this.isOverlap = false,
    this.imagePath = '',
    this.isRight = true,
    this.viewType = ViewType.normal,
    this.isLoading = false,
    this.controller,
  });

  final dynamic data;
  final viewMode;
  final Function? onItemClick;
  final padding;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color enableColor;
  final Color disableColor;
  final bool isOverlap;
  final String imagePath;
  final bool isRight;
  final ViewType viewType;
  final isLoading;
  final ScrollController? controller;

  ListLinkMomViewState createState() => ListLinkMomViewState();
}

class ListLinkMomViewState extends BaseStatefulState<ListLinkMomView> {
  List<LinkMomListResultsData> _data = [];

  var padding;
  late ViewMode viewMode;
  late double borderRadius;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;
  late String imagePath;
  late bool isRight;
  late ScrollController? controller;
  late bool isLoading;
  late ViewType viewType;

  @override
  void initState() {
    super.initState();
    viewMode = widget.viewMode;
    padding = widget.padding;
    borderRadius = widget.borderRadius;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor;
    disableColor = widget.disableColor;
    isOverlap = widget.isOverlap;
    isRight = widget.isRight;
    isLoading = widget.isLoading;
    controller = widget.controller;
  }

  @override
  Widget build(BuildContext context) {
    _data = widget.data ?? [];
    isLoading = widget.isLoading;
    viewType = widget.viewType;
    return lContainer(
      color: color_transparent,
      alignment: Alignment.center,
      child: Container(
        height: Commons.height(context, 0.6),
        child: lScrollView(
          padding: padding_0,
          child: _data.isEmpty
              ? lEmptyView(
                  isLoading: isLoading,
                  height: Commons.height(context, 0.5),
                  img: Lcons.no_data_linkssam().name,
                  title: "해당조건의링크쌤이없어요".tr(),
                  textString: "다른조건으로재검색".tr(),
                  paddingBtn: padding_50_LR,
                  fn: () => onItemClick(-1, DialogAction.filter),
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: _data.map((e) {
                    List<SingleItem> lsAuthInfo = getAuthInfo(e);
                    int index = _data.indexOf(e);
                    return Container(
                        margin: padding_10_B,
                        decoration: BoxDecoration(boxShadow: [BoxShadow(color: Colors.black.withAlpha(11), offset: Offset(0, 4), blurRadius: 12.0)]),
                        child: LinkMomView(
                          e,
                          viewMode: viewMode,
                          lsAuthInfo: lsAuthInfo,
                          padding: padding,
                          borderRadius: borderRadius,
                          fontWeight: fontWeight,
                          bgColor: bgColor,
                          enableColor: enableColor,
                          disableColor: disableColor,
                          viewType: viewType,
                          onItemClick: (item, action) {
                            onItemClick(index, action);
                          },
                        ));
                  }).toList(),
                ),
        ),
      ),
    );
  }

  List<SingleItem> getAuthInfo(LinkMomListResultsData data) {
    List<SingleItem> lsAuth = [];

    //링크쌤 인증 리스트 만들기
    if (StringUtils.validateString(data.authinfo!.hashtag_cctv)) {
      lsAuth.add(SingleItem(data.authinfo!.hashtag_cctv));
    }
    if (StringUtils.validateString(data.authinfo!.hashtag_anmal)) {
      lsAuth.add(SingleItem(data.authinfo!.hashtag_anmal));
    }
    if (StringUtils.validateString(data.authinfo!.hashtag_codiv_vaccine)) {
      lsAuth.add(SingleItem(data.authinfo!.hashtag_codiv_vaccine));
    }
    if (StringUtils.validateString(data.authinfo!.hashtag_deungbon)) {
      lsAuth.add(SingleItem(data.authinfo!.hashtag_deungbon));
    }
    if (StringUtils.validateString(data.authinfo!.hashtag_criminal)) {
      lsAuth.add(SingleItem(data.authinfo!.hashtag_criminal));
    }
    if (StringUtils.validateString(data.authinfo!.hashtag_personality)) {
      lsAuth.add(SingleItem(data.authinfo!.hashtag_personality));
    }
    if (StringUtils.validateString(data.authinfo!.hashtag_healthy)) {
      lsAuth.add(SingleItem(data.authinfo!.hashtag_healthy));
    }
    if (StringUtils.validateString(data.authinfo!.hashtag_career_2)) {
      lsAuth.add(SingleItem(data.authinfo!.hashtag_career_2));
    }
    if (StringUtils.validateString(data.authinfo!.hashtag_education)) {
      lsAuth.add(SingleItem(data.authinfo!.hashtag_education));
    }
    if (StringUtils.validateString(data.authinfo!.hashtag_career_1)) {
      lsAuth.add(SingleItem(data.authinfo!.hashtag_career_1));
    }
    if (StringUtils.validateString(data.authinfo!.hashtag_graduated)) {
      lsAuth.add(SingleItem(data.authinfo!.hashtag_graduated));
    }
    if (StringUtils.validateString(data.authinfo!.hashtag_babysiter)) {
      lsAuth.add(SingleItem(data.authinfo!.hashtag_babysiter));
    }

    return lsAuth;
  }

  onItemClick(int index, DialogAction action) {
    setState(() {
      if (action == DialogAction.follower) {
        _data[index].userinfo!.is_follower = !_data[index].userinfo!.is_follower;
      }
    });

    if (widget.onItemClick != null) {
      if (action == DialogAction.filter) {
        widget.onItemClick!(null, action);
      } else {
        widget.onItemClick!(_data[index], action);
      }
    }
  }
}

class LinkMomView extends StatefulWidget {
  final LinkMomListResultsData _data;
  ViewMode? viewMode;
  final List<SingleItem>? lsAuthInfo;
  final padding;
  final margin;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  Color enableColor = color_545454;
  Color disableColor = color_999999;
  final bool isOverlap;
  final ViewType? viewType;
  final Function? onItemClick;
  final bool showFollow;
  final Widget? moreBtn;

  LinkMomView(
    this._data, {
    this.viewMode,
    this.lsAuthInfo,
    this.padding,
    this.margin,
    this.borderRadius = 16,
    this.fontWeight = FontWeight.w400,
    this.bgColor = Colors.transparent,
    this.enableColor = color_545454,
    this.disableColor = color_999999,
    this.isOverlap = true,
    this.viewType,
    this.onItemClick,
    this.showFollow = true,
    this.moreBtn,
  });

  @override
  State<LinkMomView> createState() => _LinkMomViewState();
}

class _LinkMomViewState extends State<LinkMomView> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0.1,
      semanticContainer: false,
      borderOnForeground: false,
      shape: lRoundedRectangleBorder(
        width: 0,
        disBorderColor: color_transparent,
        borderRadius: widget.borderRadius,
      ),
      margin: padding_10_B,
      child: InkWell(
        onTap: () {
          if (widget.onItemClick != null) {
            widget.onItemClick!(widget._data, DialogAction.select);
          }
        },
        child: Stack(
          children: [
            widget.viewType == ViewType.normal ? _viewModeNormal(context) : _viewModeSummary(context),
            if (widget.showFollow && widget._data.userinfo!.user_id != auth.user.id)
              widget.moreBtn ??
                  Align(
                    //내가 등록한 구직신청서이면 동작을 하지 않는다.
                    alignment: Alignment.centerRight,
                    child: InkWell(
                      onTap: () {
                        if (widget.onItemClick != null) {
                          setState(() {
                            widget.onItemClick!(widget._data, DialogAction.follower);
                            widget._data.userinfo!.is_follower = !widget._data.userinfo!.is_follower;
                          });
                        }
                      },
                      child: Padding(
                        padding: padding_20,
                        child: Lcons.scrap(size: 25, color: color_ffbb56, disableColor: color_cecece, isEnabled: widget._data.userinfo!.is_follower),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  ///기본 화면
  Widget _viewModeNormal(BuildContext context) {
    double imageSize = 70;
    return Container(
      padding: widget.padding,
      width: widthFull(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  SizedBox(
                    width: imageSize,
                    height: imageSize,
                    child: Material(
                      elevation: 0,
                      shape: CircleBorder(),
                      clipBehavior: Clip.hardEdge,
                      color: Colors.transparent,
                      child: lProfileAvatar(widget._data.userinfo!.profileimg),
                    ),
                  ),
                  sb_h_05,
                  nameView(widget._data.userinfo!.first_name),
                ],
              ),
              sb_w_10,
              Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Lcons.won(isEnabled: Commons.isLinkMom()),
                        sb_w_04,
                        lText(
                          getPay(),
                          style: st_18(
                            textColor: Commons.getColor(),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    sb_h_02,
                    locationAffiliateView(
                      address: widget._data.userinfo!.getAddress(),
                      isAffiliate: widget._data.userinfo!.is_affiliate,
                    ),
                    sb_h_05,
                    Row(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            lText(
                              "근무날짜".tr(),
                              style: st_b_15(textColor: color_999999),
                            ),
                            lText(
                              "돌봄유형".tr(),
                              style: st_b_15(textColor: color_999999),
                            ),
                            lText(
                              "보육장소".tr(),
                              style: st_b_15(textColor: color_999999),
                            ),
                          ],
                        ),
                        sb_w_10,
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              lScrollView(
                                scrollDirection: Axis.horizontal,
                                scrollBarSize: 2.0,
                                padding: padding_0,
                                child: lAutoSizeText(
                                  getCareDate(),
                                  style: st_b_15(
                                    textColor: color_545454,
                                  ),
                                ),
                              ),
                              lScrollView(
                                scrollDirection: Axis.horizontal,
                                scrollBarSize: 2.0,
                                padding: padding_0,
                                child: lAutoSizeText(
                                  widget._data.servicetype,
                                  style: st_b_15(
                                    textColor: color_545454,
                                  ),
                                ),
                              ),
                              lScrollView(
                                scrollDirection: Axis.horizontal,
                                scrollBarSize: 2.0,
                                padding: padding_0,
                                child: lText(
                                  Commons.possibleAreaSubText(widget._data.possible_area),
                                  style: st_b_15(
                                    textColor: color_545454,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
          lDivider(color: color_eeeeee, padding: padding_10_TB),
          Row(
            children: [
              Stack(
                alignment: Alignment.center,
                children: [
                  Lcons.polygonBg(Commons.isLinkMom(), size: 25),
                  lText('${widget._data.authinfo!.grade_authlevel}', style: st_12(fontWeight: FontWeight.bold, height: 1)),
                ],
              ),
              sb_w_05,
              lText(
                "링크쌤인증".tr(),
                style: st_b_15(fontWeight: FontWeight.w500),
              ),
              sb_w_10,
              Expanded(
                child: likeReview(
                    color: Commons.getColor(),
                    data: MyInfoLikeReViewItem(favoite_cnt_linkmom: widget._data.userinfo!.reviewinfo!.favoite_cnt_linkmom, review_cnt_linkmom: widget._data.userinfo!.reviewinfo!.review_cnt_linkmom),
                    viewType: widget.viewMode!.viewType,
                    mainAxisAlignment: MainAxisAlignment.start,
                    callBack: (action) {
                      if (action != null && action == DialogAction.review) {
                        if (widget.onItemClick != null) {
                          widget.onItemClick!(widget._data, DialogAction.review);
                        }
                      }
                    }),
              ),
            ],
          ),
          if (widget.lsAuthInfo != null && widget.lsAuthInfo!.isNotEmpty)
            Padding(
              padding: padding_10_T,
              child: ListTagView(
                data: widget.lsAuthInfo,
              ),
            ),
        ],
      ),
    );
  }

  ///요약화면
  Widget _viewModeSummary(BuildContext context) {
    double imageSize = 60;
    double photoHeight = heightFull(context) * 0.07;
    double contentHeight = heightFull(context) * 0.11;
    return Container(
      padding: widget.padding,
      width: widthFull(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: photoHeight,
            child: Row(
              children: [
                CachedNetworkImage(
                  imageUrl: widget._data.userinfo!.profileimg,
                  imageBuilder: (context, imageProvider) => CircleAvatar(
                    backgroundImage: imageProvider,
                    backgroundColor: color_transparent,
                    radius: 30,
                  ),
                  placeholder: (context, url) => placeholder(),
                  errorWidget: (context, url, error) => errorWidget(),
                ),
                sb_w_15,
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          lText(
                            "돌봄예상금액".tr(),
                            style: st_b_14(
                              textColor: color_999999,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          sb_w_10,
                          // lText(
                          //   '${StringUtils.formatPay(_data.cost_sum)}${"원".tr()}',
                          //   style: st_b_20(
                          //     fontWeight: FontWeight.bold,
                          //   ),
                          // ),
                        ],
                      ),
                      Row(
                        children: [
                          lText(
                            widget._data.userinfo!.first_name,
                            style: st_b_20(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          sb_w_10,
                          Lcons.my_location(size: 25),
                          Expanded(
                            child: lAutoSizeText(
                              widget._data.userinfo!.getAddress(),
                              maxLines: 1,
                              style: st_b_20(
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          sb_h_15,
          SizedBox(
            height: contentHeight,
            child: Row(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    lText(
                      "아이정보".tr(),
                      style: st_b_15(textColor: color_999999),
                    ),
                    lText(
                      "돌봄일정".tr(),
                      style: st_b_15(textColor: color_999999),
                    ),
                    lText(
                      "보육장소".tr(),
                      style: st_b_15(textColor: color_999999),
                    ),
                    lText(
                      "돌봄유형".tr(),
                      style: st_b_15(textColor: color_999999),
                    ),
                  ],
                ),
                sb_w_10,
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          lText(
                            '_data.childinfo.child_name',
                            style: st_b_15(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Container(height: 15, child: lDividerVertical(color: color_dedede, thickness: 1, padding: padding_10_LR)),
                          lText(
                            '_data.childinfo.child_age',
                            style: st_b_15(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Container(height: 15, child: lDividerVertical(color: color_dedede, thickness: 1, padding: padding_10_LR)),
                          lText(
                            '_data.childinfo.child_gender',
                            style: st_b_15(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                      lText(
                        getCareDate(),
                        style: st_b_15(
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      lText(
                        widget._data.possible_area,
                        style: st_b_15(
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      lText(
                        widget._data.servicetype,
                        style: st_b_15(
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  ///희망시급
  String getPay() {
    int min = widget._data.fee_range!.fee_min;
    int max = widget._data.fee_range!.fee_max;
    log.d('『GGUMBI』>>> getPay : min : $min , max: $max, <<< ');
    String value = '';
    if (min == max) {
      value = '${StringUtils.formatPay(min)}${"원".tr()}';
    } else {
      value = '${StringUtils.formatPay(min)}${"원".tr()} ~ ${StringUtils.formatPay(max)}${"원".tr()}';
    }
    return value;
  }

  ///근무날짜
  String getCareDate() {
    String value = '';
    String min = widget._data.caredate_min;
    String max = widget._data.caredate_max;
    log.d('『GGUMBI』>>> getCareDate : min: $min, max: $max, ${(min == max)} <<< ');
    if (min == max) {
      value = StringUtils.getStringToDate(min, format: F_YYMMDDE);
    } else {
      String minFormat = F_YYMMDDE;
      String maxFormat = F_YYMMDDE;
      var lsMin = min.split("-");
      var lsMax = min.split("-");
      int minYear = int.parse(lsMin[0]);
      int maxYear = int.parse(lsMax[0]);
      //앞 년도가 같으면 월까지만 표시
      if (minYear == maxYear) {
        minFormat = F_MMDDE;
        maxFormat = F_MMDDE;
      } else if (maxYear > minYear) {
        minFormat = F_MMDDE;
        maxFormat = F_YYMMDDE;
      } else {
        minFormat = F_YYMMDDE;
        maxFormat = F_MMDDE;
      }
      value = '${StringUtils.getStringToDate(min, format: minFormat)} ~ ${StringUtils.getStringToDate(max, format: maxFormat)}';
    }
    return value;
  }
}

bool isLearning(String type) {
  return type == "학습".tr() ? true : false;
}
