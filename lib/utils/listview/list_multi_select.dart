import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../string_util.dart';
import 'model/list_model.dart';

class ListMultiSelector extends StatefulWidget {
  ListMultiSelector({
    this.title = '',
    this.data,
    this.count = 4,
    this.margin = padding_05,
    this.borderRadius = 15,
    this.itemHeight = listImgHeight,
    this.itemWidth = listImgWidth,
    this.imageHeight = 35,
    this.imageWidth = 35,
    this.onItemClick,
  });

  final String title;
  final dynamic data;
  final Function? onItemClick;
  final count;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;

  @override
  _ListMultiSelectorState createState() => _ListMultiSelectorState();
}

class _ListMultiSelectorState extends State<ListMultiSelector> {
  late String _title;
  List<MultiItem> _data = [];

  ///그리드뷰 카운트
  int count = 4;
  var margin;
  late double borderRadius;
  late double itemHeight;
  late double itemWidth;
  late double imageHeight;
  late double imageWidth;
  late TextStyle textStyle;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _data = widget.data ?? [];
    count = widget.count;
    margin = widget.margin;
    borderRadius = widget.borderRadius;
    itemHeight = widget.itemHeight;
    itemWidth = widget.itemWidth;
    imageHeight = widget.imageHeight;
    imageWidth = widget.imageWidth;

    if (_data.length != 0) {
      if (count != 2) {
        if (!StringUtils.validateString(_data[0].image)) {
          count = 1;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        lTvListTitle(
          _title,
        ),
        sb_h_05,
        count == 1
            ? ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: _data.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Material(
                    child: lInkWell(
                        onTap: () {
                          onItemClick(index);
                        },
                        child: MultiRadio(_data[index])),
                  );
                })
            : GridView.builder(
                padding: padding_0,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: count,
                ),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: _data.length,
                itemBuilder: (context, index) {
                  return lContainer(
                    child: lInkWell(
                        // splashColor: Colors.pinkAccent,
                        onTap: () {
                          onItemClick(index);
                        },
                        child: MultiRadio(
                          _data[index],
                          margin: margin,
                          borderRadius: borderRadius,
                          itemHeight: itemHeight,
                          itemWidth: itemWidth,
                          imageWidth: imageWidth,
                          imageHeight: imageHeight,
                          count: count,
                        )),
                  );
                }),
      ],
    );
  }

  onItemClick(int index) {
    setState(() {
      if (_data[index].name == "없음".tr()) {
        _data.forEach((value) => value.isSelected = false);
        _data[index].isSelected = true;
      } else {
        // _data.forEach((value) => value.isSelected = false);
        _data.forEach((value) {
          if (value.name == "없음".tr()) {
            value.isSelected = false;
          }
        });
        _data[index].isSelected = !_data[index].isSelected;
      }
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data);
      }
    });
  }
}

class MultiRadio extends StatelessWidget {
  final MultiItem _data;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;
  final int count;

  MultiRadio(this._data, {this.margin, this.borderRadius = 15, this.itemHeight = 0, this.itemWidth = 0, this.imageHeight = 35, this.imageWidth = 35, this.count = 0});

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 0,
        // color: _data.isSelected ? mainColor : color_white,
        shape: lRoundedRectangleBorder(isSelected: _data.isSelected, width: _data.isSelected ? 2.0 : 1.0, borderRadius: borderRadius),
        margin: margin,
        child: !StringUtils.validateString(_data.image)
            ? Container(
                padding: padding_10,
                child: lText(
                  _data.name,
                  style: st_b_14(),
                ),
              )
            : Container(
                height: itemHeight,
                width: itemWidth,
                alignment: Alignment.center,
                margin: padding_05,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      _data.image,
                      width: imageWidth,
                      height: imageHeight,
                      color: _data.isSelected ? color_main : color_b2b2b2,
                    ),
                    sb_h_05,
                    count == 2 ? sb_h_15 : sb_h_05,
                    lText(
                      _data.name,
                      style: count == 2 ? defaultStyle(isEnable: _data.isSelected, fontSize: 20, color: color_b2b2b2, colorDis: color_main, fontWeight: FontWeight.w500) : st_b_14(isSelect: _data.isSelected, textColor: color_b2b2b2, disableColor: color_main),
                    )
                  ],
                ),
              ));
  }
}
