import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/child_info_response.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../commons.dart';

class ChildInfoSelector extends StatefulWidget {
  ChildInfoSelector({
    this.title = '',
    this.data,
    this.mode,
    this.viewType = ViewType.add,
    this.onItemClick,
  });

  final String title;
  final dynamic data;
  final ChildListPageMode? mode;
  final ViewType viewType;
  final Function? onItemClick;

  ChildInfoSelectorState createState() => ChildInfoSelectorState(
        title: title,
        data: data,
        mode: mode,
        viewType: viewType,
        onItemClick: onItemClick,
      );
}

class ChildInfoSelectorState extends State<ChildInfoSelector> {
  ChildInfoSelectorState({this.title, this.data, this.mode, this.viewType, this.onItemClick});

  String? title;
  List<ChildInfoItemData>? data = [];
  ChildListPageMode? mode;
  ViewType? viewType;
  Function? onItemClick;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (StringUtils.validateString(title))
          lTvListTitle(
            title!,
          ),
        ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: data!.length,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return Material(
                color: Colors.transparent,
                child: lInkWell(
                    onTap: viewType == ViewType.modify
                        ? null
                        : () {
                            data![index].index = index;
                            data![index].type = ItemType.select;
                            _onItemClick(index);
                          },
                    child: ChildInfoRadio(data![index], widget.mode!, index, viewType!, onItemClick!)),
              );
            })
      ],
    );
  }

  _onItemClick(int index) {
    if (data != null || data!.length != 0) {
      var item = data![index];
      if (item.type == ItemType.modify) {
      } else if (item.type == ItemType.delete) {
      } else {
        data!.forEach((gender) => gender.isSelected = false);
        data![index].isSelected = true;
      }

      if (widget.onItemClick != null) {
        widget.onItemClick!(data![index]);
      }
    }
    setState(() {});
  }
}

class ChildInfoItemData {
  ChildInfoData childData = ChildInfoData();
  String childName = '';
  int childAge = 0;
  int childNursery = 0;
  int childGender = 0;
  String childBirth = '';
  List<String> childCharacters = [];
  String keyName = '';
  bool isSelected = false;
  int index = 0;
  ItemType type = ItemType.select;

  List<String> lsGender = [];
  List<String> lsNursery = [];

  ChildInfoItemData({required this.childData, this.childName = '', this.childAge = 0, this.childNursery = 0, this.childGender = 0, this.childBirth = '', required this.childCharacters, this.keyName = '', this.isSelected = false}) {
    lsGender.clear();
    lsGender.add(GenderType.gender_1.stringChild);
    lsGender.add(GenderType.gender_2.stringChild);
    lsNursery.clear();
    lsNursery.add(ChildNurseryType.nursery_0.string);
    lsNursery.add(ChildNurseryType.nursery_1.string);
    lsNursery.add(ChildNurseryType.nursery_2.string);
    lsNursery.add(ChildNurseryType.nursery_3.string);
    lsNursery.add(ChildNurseryType.nursery_4.string);
    lsNursery.add(ChildNurseryType.nursery_5.string);
  }

  @override
  String toString() {
    return 'ChildInfoItemData{childData: $childData, childName: $childName, childAge: $childAge, childNursery: $childNursery, childGender: $childGender, childBirth: $childBirth, childCharacters: $childCharacters, keyName: $keyName, isSelected: $isSelected, index: $index, type: $type, lsGender: $lsGender, lsNursery: $lsNursery}';
  }
}

class ChildInfoRadio extends StatelessWidget {
  final ChildInfoItemData _data;
  final ChildListPageMode mode; // TODO: GGUMBI 5/26/21 - 투약의로서 작성시 수정,삭제 아이콘 숨김처리
  final int index;
  final ViewType viewType;
  final Function onItemClick;

  ChildInfoRadio(this._data, this.mode, this.index, this.viewType, this.onItemClick);

  @override
  Widget build(BuildContext context) {
    if (viewType == ViewType.view) {
      _data.isSelected = false;
    }
    return Container(
      alignment: Alignment.center,
      margin: padding_05_TB,
      child: Card(
        elevation: 0,
        shape: lRoundedRectangleBorder(isSelected: _data.isSelected, width: _data.isSelected ? 2.0 : 1.0, borderRadius: 20, enBorderColor: Commons.getColor()),
        child: Padding(
          padding: padding_20,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: padding_10_R,
                child: _data.childGender == 1 ? Lcons.boy() : Lcons.girl(),
              ),
              sb_w_05,
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      lText(
                        _data.childName,
                        style: st_b_18(fontWeight: FontWeight.w700),
                      ),
                      ]),
                    sb_h_03,
                    IntrinsicHeight(
                        child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      lText(
                        _setAge(_data.childAge),
                        style: st_b_14(fontWeight: FontWeight.w500),
                      ),
                      lText(
                        '(${_data.lsNursery[_data.childNursery]})',
                        style: st_b_14(fontWeight: FontWeight.w500),
                      ),
                      VerticalDivider(color: color_b2b2b2, thickness: 1, indent: 2),
                      lText(
                        _data.lsGender[_data.childGender - 1],
                        style: st_b_14(fontWeight: FontWeight.w500),
                      ),
                    ])),
                  ],
                ),
              ),
              if (mode == ChildListPageMode.childInfo)
                lInkWell(
                  onTap: () {
                    _data.index = index;
                    _data.type = ItemType.modify;
                    onItemClick(_data);
                  },
                  child: Padding(
                    padding: padding_10,
                    child: Lcons.create(color: color_b2b2b2
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  String _setAge(int childAge) {
    return '$childAge ${"세".tr()}';
  }
}
