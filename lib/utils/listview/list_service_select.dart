import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/providers/selete_model.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_dailog/dlg_service_view.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import '../commons.dart';
import 'model/list_model.dart';

class ListServiceSelector extends StatefulWidget {
  ListServiceSelector({
    this.title = '',
    this.data,
    this.count = 4,
    this.margin = padding_05,
    this.borderRadius = 50,
    this.itemHeight = listImgServices,
    this.itemWidth = listImgServices,
    this.imageHeight = 40,
    this.imageWidth = 40,
    this.dataManager,
    this.viewMode,
    this.onItemClick,
  });

  final String title;
  final dynamic data;
  final count;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;
  final DataManager? dataManager;
  final ViewMode? viewMode;
  final Function? onItemClick;

  @override
  _ListServiceSelectorState createState() => _ListServiceSelectorState();
}

class _ListServiceSelectorState extends State<ListServiceSelector> {
  late String _title;
  List<ServicesSelectItem> _data = [];

  ///그리드뷰 카운트
  int count = 4;
  var margin;
  late double borderRadius;
  late double itemHeight;
  late double itemWidth;
  late double imageHeight;
  late double imageWidth;
  late TextStyle textStyle;
  late DataManager dataManager;
  late ViewMode viewMode;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _data = widget.data ?? [];
    count = widget.count;
    margin = widget.margin;
    borderRadius = widget.borderRadius;
    itemHeight = widget.itemHeight;
    itemWidth = widget.itemWidth;
    imageHeight = widget.imageHeight;
    imageWidth = widget.imageWidth;
    dataManager = widget.dataManager!;
    viewMode = widget.viewMode!;
  }

  @override
  Widget build(BuildContext context) {
    final selectModel = Provider.of<SelectModel>(context, listen: false);
    if (selectModel.action == DialogAction.update) {
      onItemSelect(selectModel.isSelect);
    }
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: _data.length,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (context, pos) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            pos == 0 ? sb_h_10 : sb_h_25,
            Row(
              children: [
                lDot(color: Commons.getColor(), size: 5),
                lIconText(
                  title: _data[pos].cares!.type_name,
                  textStyle: st_b_18(textColor: color_161616, fontWeight: FontWeight.bold),
                  padding: padding_03_R,
                  icon: Lcons.info(color: color_cecece, isEnabled: true, size: 25),
                  isLeft: false,
                  isExpanded: false,
                  onClick: () {
                    if (widget.onItemClick != null) {
                      showNormalDlg(
                        title: "필요한돌봄가이드".tr(),
                        paddingTitle: EdgeInsets.fromLTRB(10, 20, 10, 10),
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 20),
                        messageWidget: dlgServiceView(listMsg: dataManager.serviceItem.infos[_data[pos].cares!.type_name]),
                        titleAlign: Alignment.centerLeft,
                      );
                    }
                  },
                ),
              ],
            ),
            sb_h_10,
            GridView.builder(
                padding: padding_0,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: count,
                  childAspectRatio: 0.85,
                ),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: _data[pos].values!.length,
                itemBuilder: (context, index) {
                  return lContainer(
                    child: lInkWell(
                        onTap: () {
                          if (widget.onItemClick != null && viewMode.viewType != ViewType.view) {
                            onItemClick(pos, index);
                          }
                        },
                        child: ServiceRadio(
                          _data[pos].values![index],
                          margin: margin,
                          borderRadius: borderRadius,
                          itemHeight: itemHeight,
                          itemWidth: itemWidth,
                          imageWidth: imageWidth,
                          imageHeight: imageHeight,
                          count: count,
                        )),
                  );
                }),
          ],
        );
      },
    );
  }

  onItemClick(int gPos, int index) {
    setState(() {
      _data[gPos].values![index].isSelected = !_data[gPos].values![index].isSelected;
      if (widget.onItemClick != null) {
        log.d('『GGUMBI』>>> onItemClick : _data«: ${_data[gPos].values![index]},  <<< ');

        int totalTime = dataManager.jobItem.selectTotalTime(dataManager);
        log.d('『GGUMBI』>>> onItemClick : calcu totalTime: $totalTime,  <<< ');
        if (_data[gPos].values![index].isSelected) {
          log.d('『GGUMBI』>>> onItemClick : dataManager.jobItem: ${dataManager.jobItem},  <<< ');
          if (dataManager.jobItem.calcuTime < totalTime) {
            showNormalDlg(title: "필요한돌봄알림".tr(), message: "서비스선택_초과안내".tr());
          }
        }
        widget.onItemClick!(DialogAction.select, totalTime);
        selectModel.setTotalTime(totalTime: totalTime);
      }
    });
  }

  onItemSelect(bool isSelect) {
    _data.forEach((group) {
      group.values!.forEach((value) {
        value.isSelected = isSelect;
      });
    });
  }
}

class ServiceRadio extends StatelessWidget {
  final ServicesSelectItem _data;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;
  final int count;

  ServiceRadio(this._data, {this.margin, this.borderRadius = 15, this.itemHeight = 0.0, this.itemWidth = 0.0, this.imageHeight = 35, this.imageWidth = 35, this.count = 0});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Stack(
          children: [
            Card(
                elevation: 0,
                child: Container(
                  child: _data.icon != null
                      ? Lcons(_data.icon!.name, size: 50, isEnabled: _data.isSelected, color: Commons.getColor(), disableColor: color_cecece)
                      : Image.asset(
                          _data.image!,
                          alignment: Alignment.center,
                          width: 45,
                          height: 45,
                          color: _data.isSelected ? Commons.getColor() : color_aaaaaa,
                        ),
                )),
            if (_data.products!.product_data!.b_cost != 0)
              Card(
                  //유료인 경유 표시
                  elevation: 0,
                  color: color_transparent,
                  margin: margin,
                  child: Container(
                    height: itemHeight,
                    width: itemWidth,
                    alignment: Alignment.center,
                    // margin: padding_05,
                    child: Container(alignment: Alignment.bottomRight, child: Lcons.won(isEnabled: Commons.isLinkMom())),
                  )),
          ],
        ),
        lAutoSizeText(
          _data.products!.product_data!.name,
          maxLines: 1,
          style: st_14(textColor: _data.isSelected ? Commons.getColor() : color_999999, fontWeight: FontWeight.w500),
        ),
        lAutoSizeText(
          _data.products!.product_data!.b_cost != 0 ? _data.products!.product_data!.b_cost.toString() : '',
          maxLines: 1,
          style: st_b_14(fontWeight: FontWeight.w500),
        ),
      ],
    );
  }
}
