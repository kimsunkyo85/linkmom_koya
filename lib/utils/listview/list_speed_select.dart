import 'package:flutter/material.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../../main.dart';
import '../commons.dart';
import 'model/list_model.dart';

class ListSpeedSelector extends StatefulWidget {
  ListSpeedSelector({
    this.title = '',
    this.data,
    this.count = 4,
    this.margin = padding_05,
    this.borderRadius = 15,
    this.itemHeight = listImgHeight,
    this.itemWidth = listImgWidth,
    this.imageHeight = 35,
    this.imageWidth = 35,
    this.onItemClick,
  });

  final String title;
  final dynamic data;
  final Function? onItemClick;
  final count;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;

  ListSpeedSelectorState createState() => ListSpeedSelectorState();
}

class ListSpeedSelectorState extends State<ListSpeedSelector> {
  late String _title;
  List<SingleItem> _data = [];

  ///그리드뷰 카운트
  int count = 4;
  var margin;
  late double borderRadius;
  late double itemHeight;
  late double itemWidth;
  late double imageHeight;
  late double imageWidth;
  late TextStyle textStyle;

  @override
  void initState() {
    super.initState();
    log.d('『GGUMBI』>>> initState : dfadfadf: , ${widget.data} <<< ');
    _title = widget.title;
    _data = widget.data ?? [];
    count = widget.count;
    margin = widget.margin;
    borderRadius = widget.borderRadius;
    itemHeight = widget.itemHeight;
    itemWidth = widget.itemWidth;
    imageHeight = widget.imageHeight;
    imageWidth = widget.imageWidth;

    log.d('『GGUMBI』>>> initState : count: $count, ${widget.count}  <<< ');

    if (_data.length != 0) {
      if (count != 2) {
        if (!StringUtils.validateString(_data[0].image)) {
          count = 1;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    log.d('『GGUMBI』>>> build ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< $count, $_data');
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (StringUtils.validateString(_title))
          lTvListTitle(
            _title,
          ),
        count == 1
            ? ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: _data.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return lInkWell(
                      onTap: () {
                        onItemClick(index);
                      },
                      child: SpeedRadio(_data[index]));
                })
            : GridView.builder(
                padding: padding_0,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: count,
                  childAspectRatio: 92 / 100,
                ),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: _data.length,
                itemBuilder: (context, index) {
                  return lContainer(
                    child: lInkWell(
                        // splashColor: Colors.pinkAccent,
                        onTap: () {
                          onItemClick(index);
                        },
                        child: SpeedRadio(
                          _data[index],
                          margin: margin,
                          borderRadius: borderRadius,
                          itemHeight: itemHeight,
                          itemWidth: itemWidth,
                          imageWidth: imageWidth,
                          imageHeight: imageHeight,
                          count: count,
                        )),
                  );
                }),
      ],
    );
  }

  onItemClick(int index) {
    setState(() {
      if (_data[index].isSelected) {
        _data[index].isSelected = false;
      } else {
        _data.forEach((value) => value.isSelected = false);
        _data[index].isSelected = !_data[index].isSelected;
        if (widget.onItemClick != null) {
          widget.onItemClick!(_data[index]);
        }
      }
    });
  }
}

///아이템 선택 28 -> 16
class SpeedRadio extends StatelessWidget {
  final SingleItem data;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;
  final int count;
  final FontWeight fontWeight;

  SpeedRadio(this.data, {this.margin, this.borderRadius = 15, this.itemHeight = 0.0, this.itemWidth = 0.0, this.imageHeight = 35, this.imageWidth = 35, this.count = 0, this.fontWeight = FontWeight.normal});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        sb_h_15,
        Container(
          // height: itemHeight,
          // width: itemWidth,
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Lcons.check_circle(isEnabled: data.isSelected, color: Commons.getColor(), disableColor: color_dedede),
              // svgRadio(isSelected: data.isSelected),
              Image.asset(
                data.isSelected ? data.image : data.imageDisable,
                width: imageWidth,
                height: imageHeight,
              ),
              lText(
                data.name,
                style: st_b_16(textColor: data.isSelected ? color_main : color_aaaaaa, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              lText(
                data.content,
                style: st_b_14(textColor: data.isSelected ? color_main : color_aaaaaa, fontWeight: FontWeight.w500),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
