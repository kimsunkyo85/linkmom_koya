import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/content_view.dart';

import '../../main.dart';
import 'model/list_model.dart';

class ListGenderTypeSelector extends StatefulWidget {
  ListGenderTypeSelector({
    this.title = '',
    this.data,
    this.padding = padding_10_R,
    this.margin = padding_05_LR,
    this.borderRadius = 50,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.enableColor = color_545454,
    this.disableColor = color_999999,
    this.onItemClick,
    this.isOverlap = false,
    this.imagePath = '',
    this.isRight = true,
  });

  final String title;
  final dynamic data;
  final Function? onItemClick;
  final padding;
  final margin;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color enableColor;
  final Color disableColor;
  final bool isOverlap;
  final String imagePath;
  final bool isRight;

  ListGenderTypeSelectorState createState() => ListGenderTypeSelectorState();
}

class ListGenderTypeSelectorState extends State<ListGenderTypeSelector> {
  late String _title;
  List<SingleItem> _data = [];

  var padding;
  var margin;
  late double borderRadius;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;
  late String imagePath;
  late bool isRight;

  @override
  void initState() {
    super.initState();
    log.d('『GGUMBI』>>> initState : widget.data: ${widget.data},  <<< ');
    log.d('『GGUMBI』>>> initState : widget.data as List<SingleItem>: ${widget.data as List},  <<< ');
    _title = widget.title;
    _data = widget.data ?? [];
    padding = widget.padding;
    margin = widget.margin;
    borderRadius = widget.borderRadius;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor;
    disableColor = widget.disableColor;
    isOverlap = widget.isOverlap;
    isRight = widget.isRight;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (StringUtils.validateString(_title))
          Padding(
            padding: padding_05_B,
            child: contentView(
                data: ContentData(
              content: _title,
              contentStyle: st_b_18(fontWeight: FontWeight.bold),
              rightWidget: Row(
                children: [lText("*", style: st_b_16(textColor: color_ff3b30)), if (isOverlap) lText("(중복가능)".tr(), style: st_14(textColor: color_b2b2b2))],
              ),
              isOverLapRight: isOverlap,
              icon: imagePath,
            )),
          ),
        lContainer(
          height: 40,
          color: bgColor,
          child: ListView.builder(
              padding: padding_0,
              itemCount: _data.length,
              scrollDirection: Axis.horizontal,
              physics: const AlwaysScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                return lInkWell(
                    onTap: () {
                      onItemClick(index);
                    },
                    child: MoveSingleRadio(
                      _data[index],
                      padding: padding,
                      margin: margin,
                      borderRadius: borderRadius,
                      fontWeight: fontWeight,
                      bgColor: bgColor,
                      enableColor: enableColor,
                      disableColor: disableColor,
                    ));
              }),
        ),
      ],
    );
  }

  onItemClick(int index) {
    setState(() {
      if (isOverlap) {
        ///중복선택시
        if (_data[index].name == "없음".tr()) {
          _data.forEach((value) => value.isSelected = false);
          _data[index].isSelected = true;
        } else {
          _data.forEach((value) {
            if (value.name == "없음".tr()) {
              value.isSelected = false;
            }
          });
          _data[index].isSelected = !_data[index].isSelected;
        }
      } else {
        ///단일 선택시
        _data.forEach((gender) => gender.isSelected = false);
        _data[index].isSelected = true;
      }
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}

class MoveSingleRadio extends StatelessWidget {
  final SingleItem _data;
  final padding;
  final margin;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  Color enableColor = color_545454;
  Color disableColor = color_999999;
  final bool isOverlap;

  MoveSingleRadio(
    this._data, {
    this.padding,
    this.margin,
    this.borderRadius = 50,
    this.fontWeight = FontWeight.w400,
    this.bgColor = Colors.transparent,
    this.enableColor = color_545454,
    this.disableColor = color_999999,
    this.isOverlap = true,
  });

  @override
  Widget build(BuildContext context) {
    log.d('『GGUMBI』>>> build : {_data.name}: ${_data.name}, ${_data.isSelected}  <<< ');
    return Padding(
      padding: padding_10_R,
      child: Row(
        children: [
          lText(
            _data.name,
            style: st_b_16(fontWeight: FontWeight.w500, textColor: _data.isSelected ? enableColor : disableColor),
          ),
          Lcons.radio(isEnabled: _data.isSelected),
        ],
      ),
    );
  }
}
