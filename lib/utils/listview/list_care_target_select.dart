import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../commons.dart';
import 'model/list_model.dart';

class ListCareTargetSelector extends StatefulWidget {
  ListCareTargetSelector({
    this.title = '',
    this.data,
    this.height = 55.0,
    this.user_type = USER_TYPE.mom_daddy,
    this.padding = padding_moveType,
    this.margin = padding_05_LR,
    this.borderRadius = 30,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.enableColor,
    this.disableColor,
    this.onItemClick,
    this.isOverlap = true,
  });

  final String title;
  final dynamic data;
  final height;
  final USER_TYPE user_type;
  final Function? onItemClick;
  final padding;
  final margin;
  final double borderRadius;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color? enableColor;
  final Color? disableColor;
  final bool isOverlap;

  ListCareTargetSelectorState createState() => ListCareTargetSelectorState();
}

class ListCareTargetSelectorState extends State<ListCareTargetSelector> {
  late String _title;
  List<SingleItem> _data = [];
  late USER_TYPE user_type;
  late var height;
  late var padding;
  late var margin;
  late double borderRadius;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _data = widget.data ?? [];
    height = widget.height;
    user_type = widget.user_type;
    padding = widget.padding;
    margin = widget.margin;
    borderRadius = widget.borderRadius;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor!;
    disableColor = widget.disableColor!;
    isOverlap = widget.isOverlap;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (StringUtils.validateString(_title))
          // contentView(
          //     data: ContentData(
          //   content: _title,
          //   contentStyle: st_18(textColor: color_222222, fontWeight: FontWeight.w500),
          // )),
          lTvListTitle(_title, padding: padding_10_TB, style: st_b_18(fontWeight: FontWeight.w500)),
        lContainer(
          height: height,
          color: bgColor,
          child: ListView.builder(
              itemCount: _data.length,
              scrollDirection: Axis.horizontal,
              physics: const AlwaysScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                return lInkWell(
                    onTap: () {
                      if (widget.onItemClick != null) {
                        onItemClick(index);
                      }
                    },
                    child: ListCareTargetView(
                      _data[index],
                      padding: padding,
                      borderRadius: borderRadius,
                      fontWeight: fontWeight,
                      bgColor: bgColor,
                      enableColor: enableColor,
                      disableColor: disableColor,
                    ));
              }),
        ),
      ],
    );
  }

  onItemClick(int index) {
    setState(() {
      if (isOverlap) {
        ///중복선택시
        _data[index].isSelected = !_data[index].isSelected;
      } else {
        ///단일 선택시
        _data.forEach((gender) => gender.isSelected = false);
        _data[index].isSelected = true;
      }
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}

class ListCareTargetView extends StatelessWidget {
  final SingleItem _data;
  final padding;
  final margin;
  final double borderRadius;
  final int count;
  final FontWeight fontWeight;
  final Color bgColor;
  Color enableColor;
  Color disableColor;

  ListCareTargetView(
    this._data, {
    this.padding,
    this.margin,
    this.borderRadius = 30,
    this.count = 0,
    this.fontWeight = FontWeight.w400,
    this.bgColor = Colors.transparent,
    this.enableColor = color_b2b2b2,
    this.disableColor = color_main,
  });

  @override
  Widget build(BuildContext context) {
    if (enableColor == null) {
      enableColor = Commons.getColor();
    }
    return Card(
      elevation: 0,
      // margin: padding_10_R,
      shape: lRoundedRectangleBorder(isSelected: _data.isSelected, width: _data.isSelected ? 2.0 : 1.0, borderRadius: borderRadius, enBorderColor: enableColor),
      child: Padding(
        padding: padding,
        child: Center(
          child: lText(
            _data.name,
            style: st_b_16(
              fontWeight: FontWeight.w500,
              textColor: _data.isSelected ? enableColor : disableColor,
            ),
          ),
        ),
      ),
    );
  }
}
