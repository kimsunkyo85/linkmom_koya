import 'package:flutter/material.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../../main.dart';
import '../commons.dart';

class ListAuthView extends StatefulWidget {
  ListAuthView({
    this.title = '',
    this.data,
    this.count = 4,
    this.margin = padding_05,
    this.borderRadius = 50,
    this.itemHeight = listImgServices,
    this.itemWidth = listImgServices,
    this.imageHeight = 60,
    this.imageWidth = 60,
    this.dataManager,
    this.onItemClick,
    this.isSelf = false,
  });

  final String title;
  final dynamic data;
  final count;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;
  final DataManager? dataManager;
  final Function(AuthcenterItem)? onItemClick;
  final bool isSelf;

  @override
  _ListAuthViewState createState() => _ListAuthViewState();
}

class _ListAuthViewState extends State<ListAuthView> {
  late String _title;
  List<AuthcenterItem> _data = [];

  ///그리드뷰 카운트
  int count = 4;
  late var margin;
  late double borderRadius;
  late double itemHeight;
  late double itemWidth;
  late double imageHeight;
  late double imageWidth;
  late TextStyle textStyle;
  late DataManager? dataManager;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _data = widget.data ?? [];
    count = widget.count;
    margin = widget.margin;
    borderRadius = widget.borderRadius;
    itemHeight = widget.itemHeight;
    itemWidth = widget.itemWidth;
    imageHeight = widget.imageHeight;
    imageWidth = widget.imageWidth;
    dataManager = widget.dataManager;
    log.d('『GGUMBI』>>> initState : : $imageWidth,  <<< ');
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        padding: padding_0,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: count,
          childAspectRatio: Commons.ratio(1.6),
        ),
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: _data.length,
        itemBuilder: (context, index) {
          // log.d('『GGUMBI』>>> build :  _data[pos].values: ${_data[pos].values.length}, $index,  ${_data[pos].values},  <<< ');
          return lContainer(
            child: lInkWell(
                // splashColor: Colors.pinkAccent,
                onTap: () {
                  onItemClick(index);
                },
                child: ServiceRadio(
                  _data[index],
                  margin: margin,
                  borderRadius: borderRadius,
                  itemHeight: itemHeight,
                  itemWidth: itemWidth,
                  imageWidth: imageWidth,
                  imageHeight: imageHeight,
                  count: count,
                  isSelf: widget.isSelf,
                )),
          );
        });
  }

  onItemClick(int index) {
    if (widget.onItemClick != null) {
      widget.onItemClick!(_data[index]);
    }
    /* setState(() {
      ///단일 선택시
      _data.forEach((gender) => gender.isSelected = false);
      _data[index].isSelected = true;
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });*/
  }
}

class ServiceRadio extends StatelessWidget {
  final AuthcenterItem _data;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;
  final int count;
  final bool isSelf;

  ServiceRadio(this._data, {this.margin, this.borderRadius = 15, this.itemHeight = 0.0, this.itemWidth = 0.0, this.imageHeight = 35, this.imageWidth = 35, this.count = 0, this.isSelf = false});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        AuthSymbol(
          data: _data,
          imageHeight: imageHeight,
          imageWidth: imageWidth,
          isSelf: isSelf,
        ),
        lAutoSizeText(
          _data.name,
          maxLines: 1,
          style: st_b_14(fontWeight: FontWeight.w700),
        ),
        lAutoSizeText(
          _data.date,
          maxLines: 1,
          style: st_b_12(textColor: color_545454),
        ),
      ],
    );
  }
}

class AuthSymbol extends StatefulWidget {
  final AuthcenterItem data;
  final double imageHeight, imageWidth;
  final bool showCheck;
  final bool isSelf;
  final bool enable;
  AuthSymbol({Key? key, required this.data, this.imageHeight = 60, this.imageWidth = 60, this.showCheck = true, this.isSelf = false, this.enable = false}) : super(key: key);

  @override
  _AuthSymbolState createState() => _AuthSymbolState();
}

class _AuthSymbolState extends State<AuthSymbol> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        alignment: Alignment.topRight,
        children: [
          Container(
              alignment: Alignment.center,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                      decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [
                        BoxShadow(
                          blurRadius: 10,
                          offset: Offset(0, 2),
                          color: color_727272.withOpacity(0.42),
                          spreadRadius: -10,
                        )
                      ]),
                      child: Lcons.polygon(
                        color: Colors.white,
                        disableColor: widget.isSelf ? color_eeeeee : color_e0cfe5,
                        isEnabled: widget.enable ? widget.enable : widget.data.isSelected,
                      )),
                  Container(
                      decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [
                        BoxShadow(
                          blurRadius: 10,
                          offset: Offset(0, 2),
                          color: color_727272.withOpacity(0.42),
                          spreadRadius: -10,
                        )
                      ]),
                      child: Lcons.polygon(
                        color: widget.isSelf ? color_545454 : color_linkmom,
                        disableColor: Colors.white,
                        size: 60,
                        isEnabled: widget.enable ? widget.enable : widget.data.isSelected,
                      )),
                  Padding(
                    padding: padding_03_B,
                    child: Lcons(widget.data.icon.name, isEnabled: widget.enable ? widget.enable : widget.data.isSelected, size: 30, color: Colors.white, disableColor: (widget.isSelf ? color_b2b2b2 : color_b579c8).withOpacity(0.32)),
                  ),
                ],
              )
              // Image.asset(
              //     widget.data.isSelected ? widget.data.image : widget.data.imageDisable,
              //     alignment: Alignment.center,
              //     width: widget.imageWidth,
              //     height: widget.imageHeight,
              //   ),
              ),
          if (widget.showCheck && widget.data.isSelected) Container(margin: EdgeInsets.only(right: 22, top: 2), height: 22, width: 22, child: Lcons.check_bg_white_active(isEnabled: widget.data.isSelected)),
        ],
      ),
    );
  }
}

class AuthcenterItem {
  final String name;
  final String desc;
  final String warn;
  bool isSelected;
  bool isEnabled;
  final int type;
  AuthStatus state;
  final Lcons icon;
  String date;
  AuthcenterItem({
    this.name = '',
    this.desc = '',
    this.warn = '',
    this.isSelected = true,
    this.type = 0,
    this.state = AuthStatus.none,
    required this.icon,
    this.date = '',
    this.isEnabled = true,
  });
}
