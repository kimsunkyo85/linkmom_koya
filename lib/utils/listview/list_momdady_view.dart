import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/care_list_response.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/job_profile/listview_child_info_view.dart';
import 'package:linkmom/view/main/job_list/location_view.dart';
import 'package:linkmom/view/main/job_list/name_view.dart';

import '../../main.dart';
import '../commons.dart';

/// All Rights Reserved. Copyright(c) 2020 Ggumbi CO., Ltd
/// Created by   : platformbiz@ggumbi.com
/// version      : 1.0.0
/// see          : list_momdady_view.dart - 맘대리 목록 리스트
/// since        : 2021/06/07 / update:
class ListMomDadyView extends StatefulWidget {
  ListMomDadyView({
    this.data,
    this.padding = padding_15_LR,
    this.borderRadius = 16,
    this.elevation = 0.1,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.enableColor = color_545454,
    this.disableColor = color_999999,
    this.onItemClick,
    this.isOverlap = false,
    this.imagePath = '',
    this.isRight = true,
    this.viewMode = ViewType.normal,
    this.isLoading = false,
    this.controller,
  });

  final dynamic data;
  final Function? onItemClick;
  final padding;
  final double borderRadius;
  final double elevation;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color enableColor;
  final Color disableColor;
  final bool isOverlap;
  final String imagePath;
  final bool isRight;
  final ViewType viewMode;
  final isLoading;
  final ScrollController? controller;

  ListMomDadyViewState createState() => ListMomDadyViewState();
}

class ListMomDadyViewState extends State<ListMomDadyView> {
  List<CareListResultsData> _data = [];

  var padding;
  late double borderRadius;
  late double elevation;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;
  late String imagePath;
  late bool isRight;
  late ScrollController? controller;
  late bool isLoading;
  late ViewType viewMode;

  @override
  void initState() {
    super.initState();
    padding = widget.padding;
    elevation = widget.elevation;
    borderRadius = widget.borderRadius;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor;
    disableColor = widget.disableColor;
    isOverlap = widget.isOverlap;
    isRight = widget.isRight;
    isLoading = widget.isLoading;
    controller = widget.controller;
  }

  @override
  Widget build(BuildContext context) {
    _data = widget.data ?? [];
    isLoading = widget.isLoading;
    viewMode = widget.viewMode;
    return lContainer(
      color: color_transparent,
      alignment: Alignment.center,
      child: _data.isEmpty
          ? lEmptyView(
              isLoading: isLoading,
              height: Commons.height(context, 0.5),
              img: Lcons.no_data_momdaddy().name,
              title: "해당조건의맘대디가없어요".tr(),
              textString: "다른조건으로재검색".tr(),
              paddingBtn: padding_50_LR,
              fn: () => onItemClick(-1, DialogAction.filter),
            )
          : lScrollbar(
              child: ListView.builder(
                  controller: controller,
                  itemCount: _data.length,
                  physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                  itemBuilder: (BuildContext context, int index) {
                    CareListResultsData item = _data[index];
                    return MomDadyView(
                      item,
                      padding: padding,
                      borderRadius: borderRadius,
                      elevation: elevation,
                      fontWeight: fontWeight,
                      bgColor: bgColor,
                      enableColor: enableColor,
                      disableColor: disableColor,
                      viewMode: viewMode,
                      onItemClick: (action) {
                        onItemClick(index, action);
                      },
                    );
                  }),
            ),
    );
  }

  onItemClick(int index, DialogAction action) {
    setState(() {
      if (action == DialogAction.follower) {
        _data[index].userinfo!.is_follower = !_data[index].userinfo!.is_follower;
      }
    });

    if (widget.onItemClick != null) {
      if (action == DialogAction.filter) {
        widget.onItemClick!(null, action);
      } else {
        widget.onItemClick!(_data[index], action);
      }
    }
  }
}

class MomDadyView extends StatefulWidget {
  final CareListResultsData _data;
  final padding;
  final margin;
  final double borderRadius;
  final double elevation;
  final FontWeight fontWeight;
  final Color bgColor;
  Color enableColor = color_545454;
  Color disableColor = color_999999;
  final bool isOverlap;
  final ViewType? viewMode;
  final Function? onItemClick;
  final bool showFollow;
  final Widget? moreBtn;
  final Color borderColor;

  MomDadyView(
    this._data, {
    this.padding,
    this.margin,
    this.borderRadius = 16,
    this.elevation = 0.1,
    this.fontWeight = FontWeight.w400,
    this.bgColor = Colors.transparent,
    this.enableColor = color_545454,
    this.disableColor = color_999999,
    this.isOverlap = true,
    this.viewMode,
    this.onItemClick,
    this.showFollow = true,
    this.moreBtn,
    this.borderColor = Colors.transparent,
  });

  @override
  State<MomDadyView> createState() => _MomDadyViewState();
}

class _MomDadyViewState extends State<MomDadyView> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: widget.elevation,
      semanticContainer: false,
      borderOnForeground: false,
      shape: lRoundedRectangleBorder(
        width: 0,
        disBorderColor: widget.borderColor,
        borderRadius: widget.borderRadius,
      ),
      margin: padding_10_B,
      child: InkWell(
        onTap: () {
          if (widget.onItemClick != null) {
            widget.onItemClick!(widget._data, DialogAction.select);
          }
        },
        child: Stack(
          children: [
            widget.viewMode == ViewType.normal ? _viewModeNormal(context) : _viewModeSummary(context),
            if (widget.showFollow && widget._data.userinfo!.user_id != auth.user.id)
              widget.moreBtn ??
                  Align(
                    //내가 등록한 구직신청서이면 동작을 하지 않는다.
                    alignment: Alignment.centerRight,
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          widget.onItemClick!(widget._data, DialogAction.follower);
                          widget._data.userinfo!.is_follower = !widget._data.userinfo!.is_follower;
                        });
                      },
                      child: Padding(
                        padding: padding_15,
                        child: Lcons.scrap(isEnabled: widget._data.userinfo!.is_follower, disableColor: color_cecece, color: color_ffbb56, size: 25),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }

  ///기본 화면
  Widget _viewModeNormal(BuildContext context) {
    double imageSize = 70;
    return Container(
      padding: widget.padding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                children: [
                  SizedBox(
                    width: imageSize,
                    height: imageSize,
                    child: Material(
                      elevation: 0,
                      shape: CircleBorder(),
                      clipBehavior: Clip.hardEdge,
                      color: Colors.transparent,
                      child: lProfileAvatar(widget._data.userinfo!.profileimg),
                    ),
                  ),
                  sb_h_05,
                  nameView(widget._data.userinfo!.first_name, firstStyle: st_b_14()),
                ],
              ),
              sb_w_10,
              Expanded(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Lcons.won(isEnabled: Commons.isLinkMom()),
                        sb_w_04,
                        lText(
                          '${StringUtils.formatPay(widget._data.cost_sum)}${"원".tr()}',
                          style: st_18(
                            textColor: Commons.getColor(),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    sb_h_02,
                    locationAffiliateView(
                      address: widget._data.userinfo!.getAddress(),
                      isAffiliate: widget._data.userinfo!.is_affiliate,
                    ),
                    sb_h_05,
                    Row(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            lText(
                              "아이정보".tr(),
                              style: st_b_15(textColor: color_999999),
                            ),
                            lText(
                              "돌봄유형".tr(),
                              style: st_b_15(textColor: color_999999),
                            ),
                            lText(
                              "돌봄날짜".tr(),
                              style: st_b_15(textColor: color_999999),
                            ),
                            lText(
                              "돌봄시간".tr(),
                              style: st_b_15(textColor: color_999999),
                            ),
                            lText(
                              "보육장소".tr(),
                              style: st_b_15(textColor: color_999999),
                            ),
                          ],
                        ),
                        sb_w_15,
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              listViewChildInfoView(name: widget._data.childinfo!.child_name, age: widget._data.childinfo!.child_age, gender: widget._data.childinfo!.child_gender),
                              lText(
                                widget._data.servicetype,
                                style: st_b_15(
                                  textColor: color_545454,
                                ),
                              ),
                              lScrollView(
                                scrollDirection: Axis.horizontal,
                                scrollBarSize: 2.0,
                                padding: padding_0,
                                child: lText(
                                  getCareDate(),
                                  style: st_b_15(
                                    textColor: color_545454,
                                  ),
                                ),
                              ),
                              lScrollView(
                                scrollDirection: Axis.horizontal,
                                scrollBarSize: 2.0,
                                padding: padding_0,
                                child: lText(
                                  getCareTime(),
                                  style: st_b_15(
                                    textColor: color_545454,
                                  ),
                                ),
                              ),
                              lScrollView(
                                scrollDirection: Axis.horizontal,
                                scrollBarSize: 2.0,
                                padding: padding_0,
                                child: lText(
                                  Commons.possibleAreaSubText(widget._data.possible_area),
                                  style: st_b_15(
                                    textColor: color_545454,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  ///요약화면
  Widget _viewModeSummary(BuildContext context) {
    double imageSize = 60;
    double photoHeight = heightFull(context) * 0.07;
    double contentHeight = heightFull(context) * 0.11;
    return Container(
      padding: widget.padding,
      width: widthFull(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: photoHeight,
            child: Row(
              children: [
                lProfileAvatar(widget._data.userinfo!.profileimg),
                sb_w_15,
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          lText(
                            "돌봄예상금액".tr(),
                            style: st_b_14(
                              textColor: color_999999,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          sb_w_10,
                          lText(
                            '${StringUtils.formatPay(widget._data.cost_sum)}${"원".tr()}',
                            style: st_b_20(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          lText(
                            widget._data.userinfo!.first_name,
                            style: st_b_20(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          sb_w_10,
                          Lcons.location(size: 25),
                          Expanded(
                            child: lAutoSizeText(
                              widget._data.userinfo!.getAddress(),
                              maxLines: 1,
                              style: st_b_20(
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          sb_h_15,
          SizedBox(
            height: contentHeight,
            child: Row(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    lText(
                      "아이정보".tr(),
                      style: st_b_15(textColor: color_999999),
                    ),
                    lText(
                      "돌봄일정".tr(),
                      style: st_b_15(textColor: color_999999),
                    ),
                    lText(
                      "보육장소".tr(),
                      style: st_b_15(textColor: color_999999),
                    ),
                    lText(
                      "돌봄유형".tr(),
                      style: st_b_15(textColor: color_999999),
                    ),
                  ],
                ),
                sb_w_10,
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          lText(
                            widget._data.childinfo!.child_name,
                            style: st_b_15(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Container(height: 15, child: lDividerVertical(color: color_dedede, thickness: 1, padding: padding_10_LR)),
                          lText(
                            widget._data.childinfo!.child_age,
                            style: st_b_15(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Container(height: 15, child: lDividerVertical(color: color_dedede, thickness: 1, padding: padding_10_LR)),
                          lText(
                            widget._data.childinfo!.child_gender,
                            style: st_b_15(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          lText(
                            getCareDate(),
                            style: st_b_15(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          sb_w_05,
                          Expanded(
                            child: lAutoSizeText(
                              getCareTime(),
                              style: st_b_15(
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ],
                      ),
                      lText(
                        widget._data.possible_area,
                        style: st_b_15(
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      lText(
                        widget._data.servicetype,
                        style: st_b_15(
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  ///근무날짜
  String getCareDate() {
    String value = widget._data.caredate[0];
    return StringUtils.getCareDateCount(widget._data.caredate, showYear: true);
  }

  ///근무시간
  String getCareTime() {
    String value = '${StringUtils.getStringToTime(widget._data.stime, format: F_HMM)} ~ ${StringUtils.getStringToTime(widget._data.etime, format: F_HMM)}';
    return value;
  }
}

bool isLearning(String type) {
  return type == "학습".tr() ? true : false;
}
