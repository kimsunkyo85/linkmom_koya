import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import 'model/list_model.dart';

class GridCareLinkmom extends StatefulWidget {
  GridCareLinkmom({
    this.title = '',
    this.data,
    this.user_type = USER_TYPE.mom_daddy,
    this.count = 1,
    this.childAspectRatio = 2.5,
    this.padding = padding_moveType,
    this.margin = padding_05_LR,
    this.borderRadius = 30,
    this.itemHeight = listImgHeight,
    this.itemWidth = listImgWidth,
    this.imageHeight = 35,
    this.imageWidth = 35,
    this.fontWeight = FontWeight.normal,
    this.bgColor = color_white,
    this.enableColor = color_linkmom,
    this.disableColor = color_cecece,
    this.onItemClick,
    this.isOverlap = true,
  });

  final String title;
  final dynamic data;
  final USER_TYPE user_type;
  final Function? onItemClick;
  final count;
  final childAspectRatio;
  final padding;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;
  final FontWeight fontWeight;
  final Color bgColor;
  final Color enableColor;
  final Color disableColor;
  final bool isOverlap;

  GridCareLinkmomState createState() => GridCareLinkmomState();
}

class GridCareLinkmomState extends State<GridCareLinkmom> {
  late String _title;
  List<SingleItem> _data = [];
  late USER_TYPE user_type;

  ///그리드뷰 카운트
  int count = 1;
  late double childAspectRatio;
  late var padding;
  late var margin;
  late double borderRadius;
  late double itemHeight;
  late double itemWidth;
  late double imageHeight;
  late double imageWidth;
  late TextStyle textStyle;
  late FontWeight fontWeight;
  late Color bgColor;
  late Color enableColor;
  late Color disableColor;
  late bool isOverlap;

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _data = widget.data ?? [];
    user_type = widget.user_type;
    count = widget.count;
    childAspectRatio = widget.childAspectRatio;
    padding = widget.padding;
    margin = widget.margin;
    borderRadius = widget.borderRadius;
    itemHeight = widget.itemHeight;
    itemWidth = widget.itemWidth;
    imageHeight = widget.imageHeight;
    imageWidth = widget.imageWidth;
    fontWeight = widget.fontWeight;
    bgColor = widget.bgColor;
    enableColor = widget.enableColor;
    disableColor = widget.disableColor;
    isOverlap = widget.isOverlap;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        lContainer(
            color: bgColor,
            child: GridView.builder(
                padding: padding_0,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: count,
                  childAspectRatio: childAspectRatio,
                  // childAspectRatio: MediaQuery.of(context).size.width /
                  //     (MediaQuery.of(context).size.height / (count*2)),
                ),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: _data.length,
                itemBuilder: (context, index) {
                  return lInkWell(
                      onTap: () {
                        if (widget.onItemClick != null) {
                          onItemClick(index);
                        }
                      },
                      child: CareTargetView(
                        _data[index],
                        padding: padding,
                        margin: margin,
                        borderRadius: borderRadius,
                        itemHeight: itemHeight,
                        itemWidth: itemWidth,
                        imageWidth: imageWidth,
                        imageHeight: imageHeight,
                        count: count,
                        fontWeight: fontWeight,
                        bgColor: bgColor,
                        enableColor: enableColor,
                        disableColor: disableColor,
                      ));
                })),
      ],
    );
  }

  onItemClick(int index) {
    setState(() {
      if (isOverlap) {
        ///중복선택시
        _data[index].isSelected = !_data[index].isSelected;
      } else {
        ///단일 선택시
        _data.forEach((gender) => gender.isSelected = false);
        _data[index].isSelected = true;
      }
      if (widget.onItemClick != null) {
        widget.onItemClick!(_data[index]);
      }
    });
  }
}

class CareTargetView extends StatelessWidget {
  final SingleItem _data;
  final padding;
  final margin;
  final double borderRadius;
  final double itemHeight;
  final double itemWidth;
  final double imageHeight;
  final double imageWidth;
  final int count;
  final FontWeight fontWeight;
  final Color? bgColor;
  Color? enableColor;
  Color disableColor;
  final bool isOverlap;

  CareTargetView(
    this._data, {
    this.padding,
    this.margin,
    this.borderRadius = 30,
    this.itemHeight = 0.0,
    this.itemWidth = 0.0,
    this.imageHeight = 35,
    this.imageWidth = 35,
    this.count = 0,
    this.fontWeight = FontWeight.w400,
    this.bgColor,
    this.enableColor,
    this.disableColor = color_main,
    this.isOverlap = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          lText(
            _data.name,
            style: st_b_16(fontWeight: FontWeight.w500),
          ),
          if (StringUtils.validateString(_data.content)) sb_w_02,
          if (StringUtils.validateString(_data.content))
            lText(
              _data.content,
              style: st_b_14(),
            ),
        ],
      ),
    );
  }
}
