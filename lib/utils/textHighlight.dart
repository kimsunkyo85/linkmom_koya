library substring_highlight;

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import 'style/color_style.dart';

/// Widget that renders a string with sub-string highlighting.
class TextHighlight extends StatelessWidget {
  ///기본 텍스트
  final String text;

  ///Highlight 텍스트
  final String term;

  ///기본 텍스트 스타일
  final TextStyle textStyle;

  ///HighLight 텍스트 스타일
  final TextStyle textStyleHighlight;

  ///텍스트 정렬 기본(시작 위치 start)
  final TextAlign textAlign;

  final bool? softWrap;

  TextHighlight({
    required this.text,
    required this.term,
    this.softWrap = true,
    this.textStyle = const TextStyle(
      color: color_black,
      letterSpacing: -0.7,
    ),
    this.textStyleHighlight = const TextStyle(
      color: color_main,
      letterSpacing: -0.7,
    ),
    this.textAlign = TextAlign.start,
  });

  @override
  Widget build(BuildContext context) {
    if (term.isEmpty) {
      return Text(text, style: textStyle, textAlign: textAlign);
    } else {
      String termLC = term.toLowerCase();

      List<InlineSpan> children = [];
      List<String> spanList = text.toLowerCase().split(termLC);
      int i = 0;

      spanList.forEach((v) {
        if (v.isNotEmpty) {
          children.add(TextSpan(text: text.substring(i, i + v.length), style: textStyle));
          i += v.length;
        }
        if (i < text.length) {
          children.add(TextSpan(text: text.substring(i, i + term.length), style: textStyleHighlight));
          i += term.length;
        }
      });
      return RichText(
        softWrap: softWrap!,
        text: TextSpan(children: children),
        textAlign: textAlign,
      );
    }
  }
}

class LAutoSizeHightLightText extends StatelessWidget {
  final String text;
  final String term;
  final TextStyle? style;
  final TextStyle? highlightStyle;
  final TextAlign? textAlign;
  final bool softWrap;
  final double? minSize;
  final double? maxSize;
  final int? maxLines;

  const LAutoSizeHightLightText(this.text, {this.term = '', this.style, this.highlightStyle, this.textAlign, this.softWrap = true, this.minSize, this.maxSize, this.maxLines});

  @override
  Widget build(BuildContext context) {
    if (term.isEmpty) {
      return Text(text, style: style);
    } else {
      String termLC = term.toLowerCase();

      List<InlineSpan> children = [];
      List<String> spanList = text.toLowerCase().split(termLC);
      int i = 0;
      spanList.forEach((v) {
        if (v.isNotEmpty) {
          children.add(TextSpan(text: text.substring(i, i + v.length), style: style));
          i += v.length;
        }
        if (i < text.length) {
          children.add(TextSpan(text: text.substring(i, i + term.length), style: highlightStyle));
          i += term.length;
        }
      });
      return AutoSizeText.rich(
        TextSpan(children: children),
        softWrap: softWrap,
        textAlign: textAlign,
        minFontSize: minSize ?? 0,
        maxFontSize: maxSize ?? (style != null ? style!.fontSize ?? 13 : 13),
        maxLines: maxLines ?? 1,
        overflow: TextOverflow.ellipsis,
      );
    }
  }
}
