import 'package:flutter/material.dart';
import 'package:linkmom/main.dart';

class HomeNavigationProvider with ChangeNotifier {
  bool _isMove = false;

  get isMove => _isMove;

  int _subIndex = 0;

  int get subIndex => _subIndex;

  int _sendCode = 0;

  int get sendCode => _sendCode;

  setHomeData(int index, {int? subIndex, bool isMove = true, int sendCode = 0}) {
    currentIndex = index;
    if (subIndex != null) _subIndex = subIndex;
    _isMove = isMove;
    _sendCode = sendCode;
  }

  setHomeDataClear() {
    _isMove = false;
  }

  void clearSubIndex() => _subIndex = 0;

  setSendCodeReset() {
    _sendCode = 0;
  }

  ///화면 전부 업데이트 하도록 변경
  void updatedRest() {
    bool flag = false;
    _needUpdate = true;
    _mainUpdated = flag;
    _communityUpdated = flag;
    _scheduleUpdated = flag;
    _listUpdated = flag;
    _chatUpdated = flag;
  }

  int _currentIndex = 0;

  int get currentIndex => _currentIndex;

  set currentIndex(int index) {
    _currentIndex = index;
    _needUpdate = true;
    switch (index) {
      case 0:
        _mainUpdated = false;
        break;
      case 1:
        _communityUpdated = false;
        break;
      case 2:
        _scheduleUpdated = false;
        break;
      case 3:
        _listUpdated = false;
        break;
      case 4:
        _chatUpdated = false;
        break;
      default:
        break;
    }
    log.d("NOTIFYLISTENER #################### HomeNavigationProvider: currentIndex $index | $_needUpdate ####################");
    notifyListeners();
  }

  bool _needUpdate = false;
  bool _communityUpdated = true;
  bool _mainUpdated = true;
  bool _listUpdated = true;
  bool _scheduleUpdated = true;
  bool _chatUpdated = true;

  bool get needUpdate => _needUpdate;

  set needUpdate(bool flag) => _needUpdate = flag;

  bool get communityUpdated => !_communityUpdated;

  bool get mainUpdated => !_mainUpdated;

  bool get listUpdated => !_listUpdated;

  void setListUpdated(bool flag, {bool isUpate = false}) {
    _listUpdated = flag;
    if (isUpate) notifyListeners();
  }

  bool get scheduleUpdated => !_scheduleUpdated;

  bool get chatUpdated => !_chatUpdated;

  void updatedEvent(int index) {
    bool flag = true;
    switch (index) {
      case 0:
        _mainUpdated = flag;
        break;
      case 1:
        _communityUpdated = flag;
        break;
      case 2:
        _scheduleUpdated = flag;
        break;
      case 3:
        _listUpdated = flag;
        break;
      case 4:
        _chatUpdated = flag;
        break;
      default:
        break;
    }
    _needUpdate = false;
    log.d("NOTIFYLISTENER #################### HomeNavigationProvider: updatedEvent | index: $index, $_needUpdate ####################");
  }

  @override
  String toString() {
    return 'HomeNavigationProvider{_isMove: $_isMove, _subIndex: $_subIndex, _sendCode: $_sendCode, _currentIndex: $_currentIndex, _needUpdate: $_needUpdate, _communityUpdated: $_communityUpdated, _mainUpdated: $_mainUpdated, _listUpdated: $_listUpdated, _scheduleUpdated: $_scheduleUpdated, _chatUpdated: $_chatUpdated}';
  }
}
