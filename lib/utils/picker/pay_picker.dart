import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/lcons.dart';

import '../../main.dart';
import '../commons.dart';

///가격 피커
showPickerPay(BuildContext context, {int initPrice = 9, Function? onClick}) {
  String price = "9000원";
  Picker ps = new Picker(
      // hideHeader: true,
      backgroundColor: Colors.transparent,
      cancel: Text(''),
      confirm: IconButton(
        icon: Lcons.nav_close(size: 30),
        padding: padding_05,
        onPressed: () {
          Commons.pagePop(context);
        },
      ),
      headerDecoration: BoxDecoration(border: Border()),
      onSelect: (picker, index, selecteds) {
        print('$index, $selecteds');
        print('$index, $selecteds');
        log.d('『GGUMBI』>>> showPickerPay : index: $index, selected: $selecteds, <<< ');
        log.d('『GGUMBI』>>> showPickerPay : print((picker.adapter as NumberPickerAdapter).data[index]);: ${(picker.adapter as NumberPickerAdapter).cur},  <<< ');
        NumberPickerColumn? current = (picker.adapter as NumberPickerAdapter).cur;
        log.d('『GGUMBI』>>> showPickerPay : current: ${current!.getValueText(selecteds[index])},  <<< ');
        price = current.getValueText(selecteds[index]);
        // price = int.parse(current.initValue);
        // log.d('『GGUMBI』>>> showPickerDate : : ${(picker.adapter as DateTimePickerAdapter).value},  <<< ');
      },
      footer: lBtn("확인".tr(), isEnabled: true, onClickAction: () {
        if (onClick != null) {
          Commons.pagePop(context);
          onClick(price);
        }
      }),
      itemExtent: 35,
      magnification: 1.1,
      // changeToFirst: true,//하나 선택시 다음 피커가 처음으로 이동
      // reversedOrder: false,//시간 앞뒤로 순서 변경
      // looping: true,//계속 롤링
      adapter: NumberPickerAdapter(data: [
        NumberPickerColumn(
            initValue: initPrice,
            columnFlex: 2,
            begin: 9,
            end: 20,
            onFormatValue: (value) {
              return '${StringUtils.formatPay(value * 1000)}${"원".tr()}';
            }),
      ]),
      onConfirm: (Picker picker, List value) {
        print((picker.adapter as DateTimePickerAdapter).value);
        value.forEach((element) {
          log.d('『GGUMBI』>>> showPickerDate : element: $element,  <<< ');
        });
      });

  showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      enableDrag: false,
      isDismissible: false,
      builder: (context) {
        return Material(
          color: color_white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
          child: Container(
            child: ps.makePicker(),
          ),
        );
      });
}

String formatTime(int value) {
  String type = '오전';
  int time = value;
  if (value < 12) {
    type = '오전';
    if (value < 10) {
      type = '$type 0';
    }
    // print('NumberPickerColumn 1 $value, $time');
  } else {
    type = '오후';
    time = value - 12;
    if (time == 0) {
      time = 12;
    }
    if (time < 10) {
      type = '$type 0';
    }
    // print('NumberPickerColumn 2 $value, $time');
  }

  return "$type$time";
}

String formatTimeString(int value) {
  return value < 10 ? "0$value" : "$value";
}
