import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:numberpicker/numberpicker.dart';

import '../../main.dart';
import '../commons.dart';

///시,분,오전/오후
showPickerDate(
  BuildContext context, {
  DateTime? valueTime,
  DateTime? startTime,
  DateTime? endTime,
  DateTime? minValue,
  DateTime? maxValue,
  Function? onClick,
}) {
  DateTime currentTime = DateTime.now();

  if (valueTime != null) {
    currentTime = getTime(valueTime);
  }

  bool isChange = false;
  Picker ps = new Picker(
      // hideHeader: true,
      // cancel:lText(''),
      //가운데 센터를 맞춰주기 위해 추가
      cancel: IconButton(
        alignment: Alignment.topRight,
        icon: Lcons.nav_close(size: 30, color: color_transparent),
        padding: padding_10_L,
        onPressed: () {},
      ),
      title: Column(
        children: [
          sb_h_15,
          lText("돌봄이용시간".tr(), textAlign: TextAlign.center, style: st_b_20(fontWeight: FontWeight.bold)),
          sb_h_05,
          lText('${StringUtils.getTimeToString(minValue!)}-${StringUtils.getTimeToString(maxValue!)}', textAlign: TextAlign.center, style: st_20(fontWeight: FontWeight.bold, textColor: Commons.getColor())),
          sb_h_10,
        ],
      ),
      confirm: IconButton(
        alignment: Alignment.topRight,
        icon: Lcons.nav_close(size: 30),
        padding: padding_10_R,
        onPressed: () {
          Commons.pagePop(context);
        },
      ),
      // title: lText("돌봄이용시간"),
      headerDecoration: BoxDecoration(border: Border()),
      onSelect: (picker, index, selecteds) {
        currentTime = (picker.adapter as DateTimePickerAdapter).value!;
        isChange = true;
        if (selecteds[1] == 11) {
          //오전이면 오후로 변경한다.
          int hour = 12;
          if (selecteds[0] == 0) {
            hour = 00;
            currentTime = DateTime(currentTime.year, currentTime.month, currentTime.day, hour, currentTime.minute);
          } else {
            hour = 12;
            currentTime = DateTime(currentTime.year, currentTime.month, currentTime.day, hour, currentTime.minute);
          }
        }
        // log.d('『GGUMBI』>>> showPickerDate : select currentTime: $currentTime, startTime:$startTime, endTime:$endTime, isChange:$isChange <<< ');
      },
      footer: lBtn("확인".tr(), isEnabled: true, onClickAction: () {
        if (onClick != null) {
          //아무것도 하지 않고 확인 버튼을 누르면 화면만 닫는다.
          if (!isChange) {
            currentTime = getTime(currentTime);
          }
          // log.d('『GGUMBI』>>> showPickerDate : confirm currentTime: $currentTime, valueTime:$valueTime, startTime:$startTime, endTime:$endTime, isChange:$isChange, minValue:$minValue, maxValue:$maxValue, <<< ');
          if (currentTime.isBefore(minValue) || currentTime.isAfter(maxValue)) {
            showNormalDlg(message: "돌봄이용시간안내".tr());
            return;
          }

          // log.d('『GGUMBI』>>> showPickerDate 1 : startTime:$startTime, endTime: $endTime, currentTime:$currentTime <<< ');
          //출발 시간이 도착 시간 보다 느린 경우
          if (endTime != null && currentTime.isAfter(endTime)) {
            showNormalDlg(message: "돌봄이용시간안내_출발".tr());
            return;
          }
          // log.d('『GGUMBI』>>> showPickerDate 2 : startTime:$startTime, endTime: $endTime, currentTime:$currentTime <<< ');
          //도착 시간이 출발 시간 보다 빠를 경우
          if (startTime != null && currentTime.isBefore(startTime)) {
            showNormalDlg(message: "돌봄이용시간안내_도착".tr());
            return;
          }
          onClick(currentTime);
          Commons.pagePop(context);
        }
      }),
      itemExtent: 35,
      magnification: 1.1,
      // changeToFirst: true,//하나 선택시 다음 피커가 처음으로 이동
      // reversedOrder: false,//시간 앞뒤로 순서 변경
      // looping: true,//계속 롤링
      textStyle: st_b_18(fontWeight: FontWeight.w500),
      adapter: DateTimePickerAdapter(
        isNumberMonth: false,
        customColumnType: [6, 7, 4],
        minuteInterval: 5,
        strAMPM: ["오전".tr(), "오후".tr()],
        value: currentTime,
        // maxValue: maxValue,
        // minValue: minValue,
        // maxHour: maxValue.hour,
        // minHour: minValue.hour,
      ),
      onConfirm: (Picker picker, List value) {
        print((picker.adapter as DateTimePickerAdapter).value);
        value.forEach((value) {
          log.d('『GGUMBI』>>> showPickerDate : element: $value,  <<< ');
        });
      });

  showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isDismissible: false,
      builder: (context) {
        return Material(
          color: color_white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
          child: Container(
            child: ps.makePicker(),
          ),
        );
      });
}

DateTime getTime(DateTime valueTime, {bool isCalculate = false}) {
  int hour = valueTime.hour;
  int calcu = valueTime.minute % 10;
  calcu = calcu > 5 ? calcu - 5 : calcu;
  int minute = valueTime.minute - calcu;
  String time = StringUtils.getTimeToString(valueTime);
  if (StringUtils.validateString(time)) {
    if (time.contains("오전".tr())) {
      if (hour == 00) {
        hour = valueTime.hour + 12;
      }
    } else {
      if (hour == 12) {
        hour = valueTime.hour - 12;
      }
    }
  }
  // log.d('『GGUMBI』>>> getTime : valueTime 1 : $valueTime, $time, ${valueTime.hour} <<< ');
  valueTime = DateTime(valueTime.year, valueTime.month, valueTime.day, hour, minute);
  // log.d('『GGUMBI』>>> getTime : valueTime 2 : $valueTime, $time, ${valueTime.hour} <<< ');
  return valueTime;
}

// DateTime _setDateAmPm(DateTime time){
//   if(selecteds[1] == 11){
//     log.d('『GGUMBI』>>> showPickerDate ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ $index, ${selecteds[1]} <<< ');
//     //오전이면 오후로 변경한다.
//     int hour = 12;
//     log.d('『GGUMBI』>>> showPickerDate : 111 : $currentTime,  <<< ');
//     if(selecteds[0] == 0){
//       hour = 00;
//       currentTime = DateTime(currentTime.year, currentTime.month, currentTime.day, hour, currentTime.minute);
//     }else{
//       hour = 12;
//       currentTime = DateTime(currentTime.year, currentTime.month, currentTime.day, hour, currentTime.minute);
//     }
//     log.d('『GGUMBI』>>> showPickerDate : 222 : $currentTime,  <<< ');
//     // picker.adapter.doSelect(0, selecteds[0] == 1 ? 0 : 1);
//     // picker.adapter.notifyDataChanged();
//   }
//   return time;
// }

String formatTime(int value) {
  String type = "오전".tr();
  int time = value;
  if (value < 12) {
    type = "오전".tr();
    if (value < 10) {
      type = '$type 0';
    }
    // print('NumberPickerColumn 1 $value, $time');
  } else {
    type = "오후".tr();
    time = value - 12;
    if (time == 0) {
      time = 12;
    }
    if (time < 10) {
      type = '$type 0';
    }
    // print('NumberPickerColumn 2 $value, $time');
  }

  return "$type$time";
}

String formatTimeString(int value) {
  return value < 10 ? "0$value" : "$value";
}

List<Picker> showPickerAmPm(ScaffoldState state, BuildContext context, {var startTime, var endTime, Function onClick(DialogAction action, var startTime, var endTime)?}) {
  List<Picker> pickes = [];
  Picker ps = Picker(
      // itemExtent: 28,//아이템 높이
      squeeze: 1.2,
      //간격
      // columnPadding: EdgeInsets.all(2.0),
      adapter: NumberPickerAdapter(data: [
        NumberPickerColumn(
            columnFlex: 2,
            begin: 0,
            end: 23,
            onFormatValue: (value) {
              return formatTime(value);
            }),
        NumberPickerColumn(
            columnFlex: 1,
            begin: 00,
            end: 50,
            initValue: 00,
            jump: 10,
            onFormatValue: (value) {
              return formatTimeString(value);
            }),
      ]),
      delimiter: [
        PickerDelimiter(
          child: Container(
            color: color_white,
            alignment: Alignment.center,
            child: Text(':', textAlign: TextAlign.center, style: st_b_16()),
          ),
        ),
      ],
      hideHeader: true,
      cancelText: '',
      confirmText: '',
      headerDecoration: BoxDecoration(border: Border()),
      itemExtent: 35,
      magnification: 1.1,
      selectedTextStyle: TextStyle(color: Colors.blue),
      onSelect: (picker, index, selecteds) {
        log.d('『GGUMBI』>>> showPickerAmPm2 : index: $index, selecteds: $selecteds, <<< ');
        startTime[0] = selecteds[0];
        startTime[1] = selecteds[1];
        if (onClick != null) {
          onClick(DialogAction.update, startTime, endTime);
        }
      },
      onConfirm: (Picker picker, List value) {
        // print(value.toString());
        // print(picker.getSelectedValues());
      });

  Picker pe = Picker(
      adapter: NumberPickerAdapter(data: [
        NumberPickerColumn(
            columnFlex: 2,
            begin: 0,
            end: 23,
            onFormatValue: (value) {
              return formatTime(value);
            }),
        NumberPickerColumn(
            columnFlex: 1,
            begin: 00,
            end: 50,
            initValue: 00,
            jump: 10,
            onFormatValue: (value) {
              return formatTimeString(value);
            }),
      ]),
      delimiter: [
        PickerDelimiter(
          child: Container(
            color: color_white,
            alignment: Alignment.center,
            child: Text(':', textAlign: TextAlign.center, style: st_b_16()),
          ),
        ),
      ],
      hideHeader: true,
      cancelText: '',
      confirm: IconButton(
        icon: Lcons.nav_close(size: 30),
        padding: padding_05,
        onPressed: () {
          Commons.pagePop(state.context);
        },
      ),
      headerDecoration: BoxDecoration(border: Border()),
      // onSelect: (picker, index, selecteds) {
      //   print('$index, $selecteds');
      //   // print((picker.adapter as DateTimePickerAdapter).value);
      //   // log.d('『GGUMBI』>>> showPickerDate : : ${(picker.adapter as DateTimePickerAdapter).value},  <<< ');
      // },
      onSelect: (picker, index, selecteds) {
        log.d('『GGUMBI』>>> showPickerAmPm2 : index: $index, selecteds: $selecteds, <<< ');
        endTime[0] = selecteds[0];
        endTime[1] = selecteds[1];
        if (onClick != null) {
          onClick(DialogAction.update, startTime, endTime);
        }
      },
      itemExtent: 35,
      magnification: 1.1,
      selectedTextStyle: TextStyle(color: Colors.blue),
      onConfirm: (Picker picker, List value) {
        // print(value.toString());
        // print(picker.getSelectedValues());
      });
  pickes.add(ps);
  pickes.add(pe);
  return pickes;
}

DateTime convertToTime(List<int> selected) {
  DateTime now = DateTime.now();
  DateTime ret = DateTime(now.year, now.month, now.day, selected[0], selected[1] * 10);
  return ret;
}

NumberPickerAdapter _numberPickerAdapter(int initHour, int initMin) {
  return NumberPickerAdapter(data: [
    NumberPickerColumn(columnFlex: 1, begin: 0, end: 23, initValue: initHour, onFormatValue: (value) => formatTimeString(value)),
    NumberPickerColumn(columnFlex: 1, begin: 0, end: 50, initValue: initMin, jump: 10, onFormatValue: (value) => formatTimeString(value)),
  ]);
}

List<Picker> showPicker24H(ScaffoldState state, BuildContext context, {DateTime? startTime, DateTime? endTime, Function onClick(DialogAction action, DateTime startTime, DateTime endTime)?}) {
  return [
    Picker(
      squeeze: 1.2,
      adapter: _numberPickerAdapter(startTime!.hour, (startTime.minute / 10 * 10).ceil()),
      hideHeader: true,
      cancelText: '',
      confirmText: '',
      itemExtent: 35,
      magnification: 1.1,
      looping: true,
      delimiter: [
        PickerDelimiter(
          child: Container(
            color: color_white,
            alignment: Alignment.center,
            child: Text(':', textAlign: TextAlign.center, style: st_b_16()),
          ),
        )
      ],
      textStyle: st_18(textColor: color_b2b2b2),
      selectedTextStyle: st_b_18(fontWeight: FontWeight.w500),
      onSelect: (picker, index, selecteds) {
        log.d('『GGUMBI』>>> showPicker24H : index: $index, selecteds: $selecteds, <<< ');
        startTime = convertToTime(selecteds);
        if (onClick != null) {
          onClick(DialogAction.update, startTime!, endTime!);
        }
      },
    ),
    Picker(
      squeeze: 1.2,
      adapter: _numberPickerAdapter(endTime!.hour, (endTime.minute / 10 * 10).ceil()),
      hideHeader: true,
      cancelText: '',
      confirmText: '',
      itemExtent: 35,
      magnification: 1.1,
      looping: true,
      delimiter: [
        PickerDelimiter(
          child: Container(
            color: color_white,
            alignment: Alignment.center,
            child: Text(':', textAlign: TextAlign.center, style: st_b_16()),
          ),
        )
      ],
      textStyle: st_18(textColor: color_b2b2b2),
      onSelect: (picker, index, selecteds) {
        log.d('『GGUMBI』>>> showPickerAmPm2 : index: $index, selecteds: $selecteds, <<< ');
        endTime = convertToTime(selecteds);
        if (onClick != null) {
          onClick(DialogAction.update, startTime!, endTime!);
        }
      },
      selectedTextStyle: st_b_18(fontWeight: FontWeight.w500),
    )
  ];
}

void showSingleTimePicker(BuildContext context, Function onClick, String title, {DateTime? initTime}) {
  if (initTime == null) initTime = DateTime.now();
  int hour = initTime.hour, min = initTime.minute % 10 == 0 ? initTime.minute : initTime.minute + (10 - initTime.minute % 10);
  if (min > 59) {
    hour += 1;
    min = 0;
  }
  showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isDismissible: false,
      isScrollControlled: true,
      builder: (context) {
        return StatefulBuilder(builder: (BuildContext context, setState) {
          Picker picker = Picker(
            squeeze: 1.2,
            adapter: _numberPickerAdapter(hour, min),
            hideHeader: true,
            cancelText: '',
            confirmText: '',
            itemExtent: 35,
            magnification: 1.1,
            looping: true,
            delimiter: [
              PickerDelimiter(
                child: Container(
                  color: color_white,
                  alignment: Alignment.center,
                  child: Text(':', textAlign: TextAlign.center, style: st_b_16()),
                ),
              )
            ],
            textStyle: st_18(textColor: color_b2b2b2),
            onSelect: (picker, index, selecteds) {
              log.d('『GGUMBI』>>> showSingleTimePicker : index: $index, selecteds: $selecteds, <<< ');
              setState(() {
                hour = selecteds[0];
                min = selecteds[1] * 10;
              });
            },
            selectedTextStyle: st_b_18(fontWeight: FontWeight.w500),
          );
          return Container(
            padding: padding_30_T,
            decoration: BoxDecoration(
              color: color_white,
              borderRadius: BorderRadius.circular(20),
            ),
            child: Column(mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.center, children: [
              Stack(
                alignment: Alignment.topRight,
                children: [
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                    lText(title, style: st_b_20(fontWeight: FontWeight.w700)),
                    sb_w_08,
                    lText("$hour:${StringUtils.getNumberAddZero(min)}", style: st_20(fontWeight: FontWeight.w700, textColor: Commons.getColor())),
                  ]),
                  Container(
                      padding: padding_05_R,
                      child: MaterialButton(
                        minWidth: 24,
                        onPressed: () {
                          Commons.pagePop(context);
                        },
                        child: Lcons.nav_close(color: color_545454, size: 29),
                      )),
                ],
              ),
              Container(padding: EdgeInsets.symmetric(horizontal: 40), child: picker.makePicker()),
              lBtn("확인".tr(), btnColor: Commons.getColor(), onClickAction: () {
                onClick(hour, min);
                Commons.pagePop(context);
              })
            ]),
          );
        });
      });
}

class Time24HPicker extends StatefulWidget {
  final DateTime? startTime;
  final DateTime? endTime;
  final Function onChanged;

  Time24HPicker({this.startTime, this.endTime, required this.onChanged});

  @override
  _Time24HPickerState createState() => _Time24HPickerState();
}

class _Time24HPickerState extends State<Time24HPicker> {
  late DateTime _start;
  late DateTime _end;
  int _startMin = 0, _startHour = 1;
  int _endMin = 0, _endHour = 1;
  double _width = 100;
  double _height = 44;

  @override
  void initState() {
    super.initState();
    _start = widget.startTime ?? _setTime(8, 0);
    _end = widget.endTime ?? _setTime(8, 30);

    _startHour = _start.hour;
    _startMin = (_start.minute / 10).floor() * 10;
    _endHour = _end.hour;
    _endMin = (_end.minute / 10).floor() * 10;
  }

  DateTime _setTime(int hour, int min) {
    DateTime now = DateTime.now();
    DateTime time = DateTime(now.year, now.month, now.day, hour, min);
    return time;
  }

  @override
  Widget build(BuildContext context) {
    _width = Commons.width(context, 0.17);
    _height = Commons.height(context, 0.033);
    return Container(
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
          foregroundDecoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [
              Colors.white,
              Colors.white.withOpacity(0.01),
              Colors.transparent,
              Colors.white.withOpacity(0.01),
              Colors.white,
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          )),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(_width, _height, _width, _height),
                decoration: BoxDecoration(
                  color: color_b2b2b2.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _buildPicker(1, 23, 1, _startHour, (value) {
                    _startHour = value;
                    _start = _setTime(_startHour, _startMin);
                    widget.onChanged(_start, _end);
                  }),
                  Padding(
                    padding: padding_06_LR,
                    child: lText(':', style: st_b_18(fontWeight: FontWeight.w500)),
                  ),
                  _buildPicker(0, 50, 10, _startMin, (value) {
                    _startMin = value;
                    _start = _setTime(_startHour, _startMin);
                    widget.onChanged(_start, _end);
                  }),
                ],
              ),
            ],
          ),
        ),
        Container(
          padding: padding_15_LR,
          child: Container(width: 7, height: 2, color: color_b2b2b2),
        ),
        Container(
          foregroundDecoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [
              Colors.white,
              Colors.white.withOpacity(0.01),
              Colors.white.withOpacity(0.01),
              Colors.white.withOpacity(0.01),
              Colors.white,
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          )),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(_width, _height, _width, _height),
                decoration: BoxDecoration(
                  color: color_b2b2b2.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _buildPicker(1, 23, 1, _endHour, (value) {
                    _endHour = value;
                    _end = _setTime(_endHour, _endMin);
                    widget.onChanged(_start, _end);
                  }),
                  Padding(
                    padding: padding_06_LR,
                    child: lText(':', style: st_b_18(fontWeight: FontWeight.w500)),
                  ),
                  _buildPicker(0, 50, 10, _endMin, (value) {
                    _endMin = value;
                    _end = _setTime(_endHour, _endMin);
                    widget.onChanged(_start, _end);
                  })
                ],
              ),
            ],
          ),
        )
      ]),
    );
  }

  Widget _buildPicker(int min, int max, int step, int current, Function callback) {
    return NumberPicker(
      itemWidth: Commons.width(context, 0.1),
      itemHeight: 40,
      itemCount: 5,
      minValue: min,
      maxValue: max,
      step: step,
      infiniteLoop: true,
      zeroPad: true,
      onChanged: (value) => setState(() {
        callback(value);
      }),
      value: current,
      haptics: true,
      textStyle: st_18(fontWeight: FontWeight.w700, textColor: color_b2b2b2),
      selectedTextStyle: st_b_18(fontWeight: FontWeight.w500),
    );
  }
}
