import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:app_settings/app_settings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:crypto/crypto.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image/image.dart' as imgDec;
import 'package:image_picker/image_picker.dart';
import 'package:linkmom/data/encrypt/encrypt_helper.dart';
import 'package:linkmom/data/holidays/holidays_helper.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/data/network/api_header.dart';
import 'package:linkmom/data/network/api_helper.dart';
import 'package:linkmom/data/network/models/authcenter_home_request.dart';
import 'package:linkmom/data/network/models/authcenter_home_response.dart';
import 'package:linkmom/data/network/models/data/auth_info_data.dart';
import 'package:linkmom/data/network/models/data/image_form_data.dart';
import 'package:linkmom/data/network/models/header_response.dart';
import 'package:linkmom/data/network/models/login_response.dart';
import 'package:linkmom/data/network/models/payload.dart';
import 'package:linkmom/data/network/models/req_data.dart';
import 'package:linkmom/data/network/models/temp_save_request.dart';
import 'package:linkmom/data/providers/auth_model.dart';
import 'package:linkmom/data/storage/flag_manage.dart';
import 'package:linkmom/data/storage/model/address_model.dart';
import 'package:linkmom/data/storage/model/flag_data.dart';
import 'package:linkmom/data/storage/model/menu_file_data.dart';
import 'package:linkmom/data/storage/model/version_model.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/route_action.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/transitions/slide_route.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/auth_center/home_state/auth_home_page.dart';
import 'package:linkmom/view/main/mypage/my_info/my_account_email_page.dart';
import 'package:linkmom/view/main/mypage/my_info/my_info_page.dart';
import 'package:logger/logger.dart';
import 'package:package_info/package_info.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../main.dart';
import '../route_name.dart';
import '../splash_page.dart';
import '../view/main/job_apply/guide/guide_linkmom_page.dart';
import '../view/main/job_apply/guide/guide_momdady_page.dart';
import '../view/main/main_home.dart';
import 'home_provider.dart';
import 'listview/model/list_model.dart';
import 'style/color_style.dart';
import 'style/linkmom_style.dart';

///200 (OK)
const int KEY_SUCCESS = 200;

///201 (OK) 회원가입 성공
const int KEY_SUCCESS_CREATE = 201;

///205 (OK) 암호화
const int KEY_205_SUCCESS_ENCRYPTION = 205;

///토큰 갱신시 완료로 처리 코드
const int KEY_250_RESOLVE = 250;

///400 (잘못된 요청입니다.) BAD_REQUEST, Validate ERROR
const int KEY_400_BAD_REQUEST = 400;

///401 (로그인을 다시 시도해주세요.) UNAUTHORIZED
const int KEY_401_UNAUTHORIZED = 401;

///403 (로그인을 다시 시도해주세요.) UNAUTHORIZED
const int KEY_403_UNAUTHORIZED = 403;

///404 (이용에 불편을 드려 죄송합니다. 찾을수 없는 URL 입니다.)NOT_FOUND
const int KEY_404_NOT_FOUND = 404;

///405 (이용에 불편을 드려 죄송합니다. 찾을수 없는 URL 입니다.)METHOD_NOT_ALLOWED
const int KEY_405_METHOD_NOT_ALLOWED = 405;

///413 (이미지 사이즈 크기 에러)Request Entity Too Large
const int KEY_413_REQUEST_ENTITY_TOO_LARGE = 413;

///429 (서비스 상태가 원할하지 않습니다. 이용에 불편을 드려 죄송합니다. 잠시 후 다시 시도해주세요.)TOO_MANY_REQUESTS
const int KEY_429_TOO_MANY_REQUESTS = 429;

///431 (서비스 상태가 원할하지 않습니다. 이용에 불편을 드려 죄송합니다. 잠시 후 다시 시도해주세요.)REQUEST_HEADER_FIELDS_TOO_LARGE
const int KEY_431_REQUEST_HEADER_FIELDS_TOO_LARGE = 431;

///500 (시스템에러) INTERNAL_SERVER_ERROR
const int KEY_500_INTERNAL_SERVER_ERROR = 500;

///501 NOT_IMPLEMENTED
const int KEY_501_NOT_IMPLEMENTED = 501;

///502 BAD_GATEWAY
const int KEY_502_BAD_GATEWAY = 502;

///503 SERVICE_UNAVAILABLE
const int KEY_503_SERVICE_UNAVAILABLE = 503;

///504 GATEWAY_TIMEOUT
const int KEY_504_GATEWAY_TIMEOUT = 504;

///505 HTTP_VERSION_NOT_SUPPORTED
const int KEY_505_HTTP_VERSION_NOT_SUPPORTED = 505;

///506 VARIANT_ALSO_NEGOTIATES
const int KEY_506_VARIANT_ALSO_NEGOTIATES = 506;

///507 INSUFFICIENT_STORAGE
const int KEY_507_INSUFFICIENT_STORAGE = 507;

///508 LOOP_DETECTED
const int KEY_508_LOOP_DETECTED = 508;

///509 BANDWIDTH_LIMIT_EXCEEDED
const int KEY_509_BANDWIDTH_LIMIT_EXCEEDED = 509;

///510 NOT_EXTENDED
const int KEY_510_NOT_EXTENDED = 510;

///511 NETWORK_AUTHENTICATION_REQUIRED
const int KEY_511_NETWORK_AUTHENTICATION_REQUIRED = 511;

///1401 (세션이 만료 되었습니다.) 우리 서버에서 에러코드(accessToken exp, refreshToken exp)
const int KEY_1401_UNAUTHORIZED = 1401;

///1402 (세션이 만료 되었습니다.) 우리 서버에서 에러코드(accessToken exp, refreshToken exp)
const int KEY_1402_UNAUTHORIZED = 1402;

///1403 (세션이 만료 되었습니다.) NICE 계좌인증 세션만료
const int KEY_1403_NICE_UNAUTHORIZED = 1403;

///2002 NOT
const int KEY_2002_NOT = 2002;

///네트워크 에러(모바일 데이터, 와이파이가 안될경우 코드) 1000
const int KEY_100_NETWORK = 1000;

///등록에 실패하였습니다. 3000
const int KEY_3000 = 3000;

///출퇴근알림이 존재하지 않습니다. 3001
const int KEY_3001 = 3001;

/// 이미 정산이 완료되었습니다.
const int KEY_ALREADY_CALC = 3002;

///대화요청자와 상대방이 동일 합니다 3080
const int KEY_3080 = 3080;

///결제 내역 전송 등을 위해 이메일 인증이 필요합니다. 이메일 인증을 해주세요. 3700
const int KEY_3700 = 3700;

///데이터가 없습니다. 4000
const int KEY_4000_NO_DATA = 4000;

///데이터가 삭제되었습니다. 4100
const int KEY_4100_DATA_DELETE = 4100;

///동시에 매칭 요청에러시 상황 처리 5000 -> 매칭 _requestMatchingView 재호출
const int KEY_5000_CONTRACTING = 5000;

///이미 취소된 계약 입니다. 5011
const int KEY_5011 = 5011;

///서명 시한이 만료되었습니다. 5012
const int KEY_5012 = 5012;

///이미 서명을 완료하였습니다. 5013
const int KEY_5013 = 5013;

///결제 시한이 만료되었습니다. 5014
const int KEY_5014 = 5014;

///매칭 진행 상황을 다시 확인해주세요. 5999
const int KEY_5999 = 5999;

///결제관련 포인트/캐시 사전차감 오류
const int KEY_6001 = 6001;

///결제관련 포인트/캐시 사전차감 오류
const int KEY_6002 = 6002;

///결제관련 포인트/캐시 사전차감 오류
const int KEY_6003 = 6003;

///결제 완료된 계약 입니다. 돌봄취소 메뉴를 이용해주세요. 5100
const int KEY_5100 = 5100;

///error 9999
const int KEY_ERROR = 9999;

///error 409
const int KEY_USER_NOT_FONUD = 409;

///스키마
const String KEY_APP_SCHEME = "linkmom";

class Commons {
  ///로그인선택, 서버 선택시 모드가 "true" 로그 출력 등등...
  static bool isDebugMode = true;
  static bool isDebugViewMode = false;
  static const baseURL = "https://api.chucknorris.io/";

  static bool _needUpdate = false;

  static bool get needUpdate => _needUpdate;

  static set setUpdateFlag(flag) => _needUpdate = flag;

  static const String ANDROID = "android";
  static const String IOS = "ios";
  static const String APPLE = "apple";

  static String path = "";

  static dynamic returnResponse(Response response) {
    switch (response.statusCode) {
      case KEY_SUCCESS:
      case KEY_SUCCESS_CREATE:
        var responseJson = jsonDecode(response.toString());
        HeaderResponse headerResponse = HeaderResponse.fromJson(responseJson);
        return catchApi(headerResponse.getCode(), response);
      default:
        return catchApi(response.statusCode, response);
    }
  }

  ///1차로 먼저 apihelper에서 [ApiHelper], [AppInterceptors]의 [onResponse] 에서 처리한다.
  static dynamic catchApi(int? statusCode, Response response) {
    try {
      switch (statusCode) {
        case KEY_SUCCESS:
        case KEY_SUCCESS_CREATE:
          var responseJson = jsonDecode(response.toString());
          return responseJson;
        case KEY_205_SUCCESS_ENCRYPTION:
          var responseJson = jsonDecode(response.toString());
          // NOTE: from enc response
          if (responseJson[Key_data] is List) {
            var decResp = encryptHelper.decrypt(responseJson[Key_data].first);
            if (decResp is List) {
              responseJson[Key_data] = decResp;
            } else {
              responseJson[Key_data] = [decResp];
            }
          } else {
            responseJson[Key_data] = encryptHelper.decrypt(responseJson[Key_data]);
          }
          responseJson[Key_msg_cd] = KEY_SUCCESS;
          log.i({
            '--- TITLE    ': '--------------- CALL PAGE RESPONSE END onResponse DECODE DATA ---------------',
            '--- M.T Name ': 'onResponse',
            '--- statusCode ': response.statusCode,
            '--- realUri ': response.realUri,
            '--- statusMessage ': response.statusMessage,
            '--- response ': responseJson,
            '--- END      ': '=================================================================',
          });
          return responseJson;
        case KEY_400_BAD_REQUEST:
        case KEY_403_UNAUTHORIZED:
          var responseJson = jsonDecode(response.toString());
          try {
            if (responseJson[Key_data].isNotEmpty) {
              if (responseJson[Key_data].values.first.runtimeType.toString().contains('List')) {
                showNormalDlg(message: responseJson[Key_data].values.first.first.toString(), msgCode: statusCode);
              } else {
                showNormalDlg(message: responseJson[Key_data].values.first.toString(), msgCode: statusCode);
              }
            }
          } catch (e) {
            log.e('『GGUMBI』 Exception >>> catchApi : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
          }
          return responseJson;
        case KEY_100_NETWORK:
          HeaderResponse headerResponse = HeaderResponse(message: "네트워크확인".tr(), msg_cd: KEY_100_NETWORK);
          return headerResponse.toJson();
        case KEY_500_INTERNAL_SERVER_ERROR:
        default:
          var responseJson = jsonDecode(response.toString());
          return responseJson;
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> catchApi : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      HeaderResponse headerResponse = HeaderResponse(message: "에러".tr(), msg_cd: KEY_ERROR);
      return headerResponse.toJson();
    }
  }

  static Widget chuckyLoader() {
    return Center(child: SpinKitFoldingCube(
      itemBuilder: (BuildContext context, int index) {
        return DecoratedBox(
          decoration: BoxDecoration(
            color: index.isEven ? Color(0xFFFFFFFF) : Color(0xFF311433),
          ),
        );
      },
    ));
  }

  static void showError(BuildContext context, String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: Text(message),
              backgroundColor: color_white,
              shape: lRoundedRectangleBorder(borderRadius: 15),
              actions: <Widget>[
                FlatButton(
                  child: lText("Ok"),
                  textColor: color_black,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ));
  }

  ///앱 종료
  static void appFinish() {
    if (Platform.isIOS) {
      exit(0); // 앱 종료
    } else {
      SystemNavigator.pop(); // 앱 종료
    }
  }

  ///앱 재시작
  static void appReStart() {
    // Phoenix.rebirth(navigatorKey.currentContext!);
    RestartWidget.restartApp(navigatorKey.currentContext!);
  }

  static void showErrorAppFinish(BuildContext context, String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: Text(message),
              backgroundColor: color_white,
              shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(15)),
              actions: <Widget>[
                FlatButton(
                  child: lText("OK"),
                  textColor: color_black,
                  onPressed: () {
                    appFinish();
                  },
                ),
              ],
            ));
  }

  ///임시저장 다이얼로그
  static showSaveTempDlg({required DataManager data, ViewType? viewType, ReqData? reqData}) {
    showNormalDlg(
        message: "임시저장설명".tr(),
        btnLeft: "나가기".tr(),
        btnRight: "저장".tr(),
        closeable: true,
        onClickAction: (action) {
          viewType = ViewType.apply;
          switch (action) {
            case DialogAction.no:
              // pageToMain(navigatorKey.currentContext!, routeHome, arguments: MainHome(tabIndex: TAB_SCHEDULE));
              pagePopUntilFirst(navigatorKey.currentContext!);
              break;
            case DialogAction.yes:
              log.d('『GGUMBI』>>> showSaveTempDlg : data.jobItem.request.reqdata: ${data.jobItem.request},  <<< ');
              log.d('『GGUMBI』>>> showSaveTempDlg : data.jobItem.request.reqdata: ${data.jobItem.reqdata},  <<< ');
              log.d('『GGUMBI』>>> showSaveTempDlg : data.jobItem.request.reqdata: ${data.jobItem.reqdata.careschedule},  <<< ');
              log.d('『GGUMBI』>>> showSaveTempDlg : data.jobItem.request.reqdata: ${data.jobItem.reqdata.bookingboyuk},  <<< ');
              log.d('『GGUMBI』>>> showSaveTempDlg : data.jobItem.request.reqdata: ${data.jobItem.reqdata.bookinggotoschool},  <<< ');
              log.d('『GGUMBI』>>> showSaveTempDlg : data.jobItem.request.reqdata: ${data.jobItem.reqdata.bookingafterschool},  <<< ');

              ///2021/06/21 임시 저장후, 재 사용시 과거 또는 미래 날짜를 세팅시 여러 문제 발생으로 날짜 데이터만 초기화해서 저장하기로 함.
              data.jobItem.reqdata.careschedule!.clear();
              log.d('『GGUMBI』>>> showSaveTempDlg : data.jobItem.reqdata.careschedule: ${data.jobItem.reqdata.careschedule},  <<< ');
              log.d('『GGUMBI』>>> showSaveTempDlg : data.jobItem.reqdata: ${data.jobItem.reqdata},  <<< ');
              apiHelper.requestTempSave(TempSaveRequest(
                child: data.jobItem.request.child,
                servicetype: data.jobItem.request.servicetype,
                possible_area: data.jobItem.request.possible_area,
                reqdata: jsonEncode(data.jobItem.reqdata),
              ));
              // pageToMain(navigatorKey.currentContext!, routeHome, arguments: MainHome(tabIndex: TAB_SCHEDULE));
              pagePopUntilFirst(navigatorKey.currentContext!);
              break;
            default:
          }
        });
  }

  ///구직신청에서 닫기
  static showCloseDlg({DataManager? data, ReqData? reqData}) {
    showNormalDlg(
        message: "구직신청_나가기_안내".tr(),
        btnLeft: "취소".tr(),
        btnRight: "나가기".tr(),
        closeable: true,
        onClickAction: (action) {
          switch (action) {
            case DialogAction.no:
              break;
            case DialogAction.yes:
              // pageClear(navigatorKey.currentContext!, routeHome);
              pageToMain(navigatorKey.currentContext!, routeHome);
              break;
            default:
          }
        });
  }

  static Widget chuckyLoading(String message) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(padding: EdgeInsets.all(18), child: Text(message)),
        chuckyLoader(),
      ],
    );
  }

  static Future logout(BuildContext context) async {
    AuthModel().requestLogout(context);
  }

  ///권한 체크....
  static Future<bool> checkPermission() async {
    Map<Permission, PermissionStatus> statuses = await [Permission.location, Permission.camera, Permission.storage, Permission.photos].request(); //여러가지 퍼미션을하고싶으면 []안에 추가하면된다. (팝업창이뜬다)

    bool per = true;
    statuses.forEach((permission, permissionStatus) async {
      if (permissionStatus.isDenied) {
        await permission.request();
        per = permissionStatus.isGranted; //하나라도 허용이안됐으면 false
      }
      log.d('『GGUMBI』>>> checkPermission : $permission : ${permissionStatus.isGranted},  <<< ');
    });
    log.d('『GGUMBI』>>> checkPermission: $per,  <<< ');
    return per;
  }

  ///권한 체크....
  static Future<bool> permissionStorage() async {
    //사진가져오기 권한이 필요한 경우...
    if (await Permission.storage.isPermanentlyDenied || await Permission.storage.isDenied) {
      showNormalDlg(
          message: "저장소권한요청".tr(),
          onClickAction: (action) {
            if (DialogAction.yes == action) {
              openAppSettings();
            }
          });
      log.d('『GGUMBI』>>> permissionStorage : await Permission.storage 1 : ${await Permission.storage.status},  <<< ');
      return true;
    }
    log.d('『GGUMBI』>>> permissionStorage : await Permission.storage 2 : ${await Permission.storage.status},  <<< ');
    return false;
  }

  ///token jwt Encode
  static String dataEncode(String key, Object obj) {
    String encode;
    var byteKey = utf8.encode(key);
    Payload payload = obj as Payload;

    var jsonData = jsonEncode(payload);
    var byteDatas = utf8.encode(jsonData);

    var hmacSha256 = new Hmac(sha256, byteKey); // HMAC-SHA256
    Digest digest = hmacSha256.convert(byteDatas);

    var encodedSignature = base64UrlEncode(byteDatas);
    var encodedPayload = base64UrlEncode(utf8.encode(digest.toString()));

    encode = "/?data=$encodedSignature&signature=$encodedPayload";
    log.d('『GGUMBI』>>> dataEncode : encode: $encode,  <<< ');

    //서버 값
    //?data=eyJuYW1lIjoi7ZSM656r7Y-8IiwiY2F0ZWdvcnkiOiJwZW9wbGUiLCJhY3Rpb24iOiJ0cmFuc3BvcnQiLCJ3aGVyZSI6InBsdXRvIiwidGltZXN0YW1wIjoiMTYwNjIwMzI3MyJ9&signature=ZTI0ZDIzZjAxYTc4Y2E4NTg0NDZiOTdmY2E1OWE1MzU4NmJkYTYxZTBiOWY1MWQ2N2E5ZjA1OTkzODU2ZDdkNQ==
    //클라 값
    //data=eyJuYW1lIjoi7ZSM656r7Y-8IiwiY2F0ZWdvcnkiOiJwZW9wbGUiLCJhY3Rpb24iOiJ0cmFuc3BvcnQiLCJ3aGVyZSI6InBsdXRvIiwidGltZXN0YW1wIjoiMTYwNjIwMzI3MyJ9&signature=ZTI0ZDIzZjAxYTc4Y2E4NTg0NDZiOTdmY2E1OWE1MzU4NmJkYTYxZTBiOWY1MWQ2N2E5ZjA1OTkzODU2ZDdkNQ==

    return encode;
  }

  ///Expires 날짜 비교 지났으면 true (5분전)
  static bool isExpired(DateTime? expires, {int day = 0, int minute = 0}) {
    bool isAfter = false;
    try {
      if (expires == null) {
        return isAfter;
      }
      DateTime today = DateTime.now().subtract(Duration(days: day, minutes: minute));
      isAfter = today.isAfter(expires);
      // log.d('『GGUMBI』>>> dateExpires \n: today: $today, \nexpires: $expires, \nisAfter: $isAfter, <<< ');
    } catch (e) {
      log.e('『GGUMBI』>>> dateExpires : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return isAfter;
  }

  ///Expires 날짜 비교 지났으면 true (5분전)
  static bool isExpiredTime(DateTime? expires, {int minute = 30}) {
    bool isAfter = false;
    try {
      if (expires == null) {
        return isAfter;
      }
      DateTime today = DateTime.now().subtract(Duration(minutes: minute));
      isAfter = today.isAfter(expires);
      // log.d('『GGUMBI』>>> isExpiredTime \n: today: $today, \nexpires: $expires, \nisAfter: $isAfter, <<< ');
    } catch (e) {
      log.e('『GGUMBI』>>> isExpiredTime : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return isAfter;
  }

  ///페이지 교체
  static nextPage(BuildContext? context, {String? routeName, Object? arguments, Function? fn}) async {
    if (StringUtils.validateString(routeName)) {
      return Navigator.of(context!).pushNamed(routeName ?? '', arguments: arguments);
    } else {
      return Navigator.push(
        context!,
        MaterialPageRoute(builder: (ctx) => fn!()),
      );
    }
  }

  ///첫번째 화면으로 클리어 후 메인 탭으로 이동하기, 메인 탭 : index, SubTab : subTabIndex, 이동Flag:isMove, 지금화면종료:isPop
  static pageToMainTabMove(BuildContext? context, {int tabIndex = TAB_HOME, int? subTabIndex, bool isMove = true, bool isPop = false, int sendCode = 0}) async {
    if (isPop) Navigator.pop(context!);
    pagePopUntilFirst(context);
    if (routeObserver.routes.contains(routeHome)) {
      Provider.of<HomeNavigationProvider>(context!, listen: false).setHomeData(tabIndex, subIndex: subTabIndex, isMove: isMove, sendCode: sendCode);
    } else {
      log.d({'--- pageToMainTabMove': "home haven't navigator $sendCode"});
      pageClear(context, routeHome, arguments: MainHome(tabIndex: tabIndex, pageTabIndex: subTabIndex, sendCode: sendCode));
    }
  }

  ///페이지 이동, 메인 화면은 유지후 나머지 클리어 toPage(이동), (MainHome 유지)
  static pageToMain(BuildContext? context, String toPage, {Object? arguments}) async {
    routeObserver.clearWithoutMain();
    return Navigator.pushNamedAndRemoveUntil(context!, toPage, (route) => route.isFirst, arguments: arguments);
  }

  ///앞단계 페이지 제거후 이동
  static pagePopAndPushNamed(BuildContext? context, String route, {Object? arguments}) async {
    return Navigator.popAndPushNamed(context!, route, arguments: arguments);
  }

  ///첫 번째로 보내기!
  static pagePopUntilFirst(BuildContext? context) async {
    return Navigator.popUntil(context!, (route) => route.isFirst);
  }

  ///페이지 교체 (isPrevious = true이면, 해당 화면을 종료하고, 이전페이지를 교체한다)
  static pageReplace(BuildContext? context, String route, {Object? arguments, bool isPrevious = false}) async {
    if (isPrevious) Navigator.pop(context!, arguments);
    return Navigator.pushReplacementNamed(context!, route, arguments: arguments);
  }

  ///다음 페이지로 이동(애니메이션 -> )
  static page(BuildContext? context, String route, {Object? arguments, bool isPrevious = false}) async {
    if (isPrevious) Navigator.pop(context!, arguments);
    return Navigator.of(context!).pushNamed(route, arguments: arguments);
    // return Navigator.pushNamed(context, route, arguments: arguments);
  }

  ///이전 페이지 전부 클리어 후 화면 이동
  static pageClear(BuildContext? context, String route, {Object? arguments}) async {
    routeObserver.clearAll();
    return Navigator.pushNamedAndRemoveUntil(context!, route, (route) => false, arguments: arguments);
  }

  ///다음 페이지로 이동(애니메이션 -> )
  static pagePushLeft(BuildContext context, {fn}) async {
    return Navigator.push(
        context,
        SlideLeftRoute(
          page: fn == null ? null : fn(),
        ));
  }

  ///다음 페이지로 이동(애니메이션 <-)
  static pagePushRight(BuildContext context, {fn}) async {
    Navigator.push(
        context,
        SlideRightRoute(
          page: fn == null ? null : fn(),
        ));
  }

  ///해당 페이지 종료
  static pagePop(BuildContext? context, {dynamic data, ViewType? viewType, AnimationType? animationType}) {
    if (data is DataManager) {
      if (viewType == ViewType.modify) {
        SlideBottomRoute();
      }
    } else {
      SlideBottomRoute();
    }
    Navigator.pop(context!, data);
  }

  static Map<String, String> parseSetCookieValue(String data) {
    String _name;
    String _value;
    Map<String, String> _cookies = {};

    // log.d('『GGUMBI』>>> parseSetCookieValue : data: $data,  <<< ');
    if (data.length == 0) {
      return _cookies;
    }

    if (!data.contains(HEADER_KEY_REFRESH_TOKEN) && !data.contains(HEADER_KEY_CSRFTOKEN)) {
      // log.d('『GGUMBI』>>> parseSetCookieValue 1 : data: $data,  <<< ');
      return _cookies;
    } else {
      // log.d('『GGUMBI』>>> parseSetCookieValue 2 : data: $data,  <<< ');
    }

    try {
      int index = 0;
      bool done() => index == data.length;

      String parseName() {
        int start = index;
        while (!done()) {
          if (data[index] == "=") break;
          index++;
        }
        return data.substring(start, index).trim();
      }

      String parseValue() {
        int start = index;
        while (!done()) {
          if (data[index] == ";") {
            break;
          }
          index++;
        }
        return data.substring(start, index).trim();
      }

      if (data.contains(HEADER_KEY_REFRESH_TOKEN)) {
        index = data.indexOf(HEADER_KEY_REFRESH_TOKEN);
        _name = parseName();
        index++; // Skip the = character.
        _value = parseValue();
        _cookies[_name] = _value;
      }

      if (data.contains(HEADER_KEY_CSRFTOKEN)) {
        index = data.indexOf(HEADER_KEY_CSRFTOKEN);
        _name = parseName();
        index++; // Skip the = character.
        _value = parseValue();
        _cookies[_name] = _value;
      }

      if (data.contains(HEADER_KEY_CSRFTOKEN_EXPIRE_KEY)) {
        index = data.indexOf(HEADER_KEY_CSRFTOKEN_EXPIRE_KEY);
        _name = parseName();
        _name = HEADER_KEY_CSRFTOKEN_EXPIRE;
        index++; // Skip the = character.
        _value = parseValue();
        _cookies[_name] = _value;
      }
      log.d('『GGUMBI』>>> parseSetCookieValue : _cookies: $_cookies,  <<< ');
    } catch (e) {
      log.e('『GGUMBI』>>> parseSetCookieValue : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }

    return _cookies;
  }

  ///파이어 베이스 원격 리모트 구성 세팅
  static Future getRemoteConfig() async {
    await Firebase.initializeApp();
    final RemoteConfig remoteConfig = RemoteConfig.instance;
    log.i('『GGUMBI』>>> getRemoteConfig : remoteConfig.getAll(): ${remoteConfig.getAll()},  <<< ');
    final defaults = <String, dynamic>{'type': 'default welcome'};
    await remoteConfig.setDefaults(defaults);
    // remoteConfig.setDefaults(<String, dynamic>{
    //   'welcome': 'default welcome',
    //   'hello': 'default hello',
    // });
    await remoteConfig.fetchAndActivate();
    // await remoteConfig.activateFetched();
    log.i({
      'type': remoteConfig.getString('type'),
      'login': remoteConfig.getString('login'),
      'type_login': remoteConfig.getString('type_login'),
    });
  }

  static Future<bool> isConnect({Function? onClick}) async {
    var connectivityResult = await network.isCheckNetwork();
    if (connectivityResult == ConnectivityResult.none) {
      showNetwork(
        onClickAction: (action) {
          onClick!();
        },
      );
      return false;
    } else {
      return true;
    }
  }

  ///디자이스 언어 정보 가져오기
  static String getLanguagerCode() {
    String languageCode = Platform.localeName.split('_')[0];
    String countryCode = Platform.localeName.split('_')[1];
    log.d('『GGUMBI』>>> getLanguagerCode : languageCode: $languageCode, \countryCode: $countryCode, <<< ');
    return languageCode;
  }

  // Parse a cookie date string.
  static DateTime parseCookieDate(String date) {
    const List monthsLowerCase = const ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];

    int position = 0;

    Never error() {
      throw new HttpException("Invalid cookie date $date");
    }

    bool isEnd() => position == date.length;

    bool isDelimiter(String s) {
      int char = s.codeUnitAt(0);
      if (char == 0x09) return true;
      if (char >= 0x20 && char <= 0x2F) return true;
      if (char >= 0x3B && char <= 0x40) return true;
      if (char >= 0x5B && char <= 0x60) return true;
      if (char >= 0x7B && char <= 0x7E) return true;
      return false;
    }

    bool isNonDelimiter(String s) {
      int char = s.codeUnitAt(0);
      if (char >= 0x00 && char <= 0x08) return true;
      if (char >= 0x0A && char <= 0x1F) return true;
      if (char >= 0x30 && char <= 0x39) return true; // Digit
      if (char == 0x3A) return true; // ':'
      if (char >= 0x41 && char <= 0x5A) return true; // Alpha
      if (char >= 0x61 && char <= 0x7A) return true; // Alpha
      if (char >= 0x7F && char <= 0xFF) return true; // Alpha
      return false;
    }

    bool isDigit(String s) {
      int char = s.codeUnitAt(0);
      if (char > 0x2F && char < 0x3A) return true;
      return false;
    }

    int getMonth(String month) {
      if (month.length < 3) return -1;
      return monthsLowerCase.indexOf(month.substring(0, 3));
    }

    int toInt(String s) {
      int index = 0;
      for (; index < s.length && isDigit(s[index]); index++) {}
      return int.parse(s.substring(0, index));
    }

    var tokens = [];
    while (!isEnd()) {
      while (!isEnd() && isDelimiter(date[position])) position++;
      int start = position;
      while (!isEnd() && isNonDelimiter(date[position])) position++;
      tokens.add(date.substring(start, position).toLowerCase());
      while (!isEnd() && isDelimiter(date[position])) position++;
    }

    String? timeStr;
    String? dayOfMonthStr;
    String? monthStr;
    String? yearStr;

    for (var token in tokens) {
      if (token.length < 1) continue;
      if (timeStr == null && token.length >= 5 && isDigit(token[0]) && (token[1] == ":" || (isDigit(token[1]) && token[2] == ":"))) {
        timeStr = token;
      } else if (dayOfMonthStr == null && isDigit(token[0])) {
        dayOfMonthStr = token;
      } else if (monthStr == null && getMonth(token) >= 0) {
        monthStr = token;
      } else if (yearStr == null && token.length >= 2 && isDigit(token[0]) && isDigit(token[1])) {
        yearStr = token;
      }
    }

    if (timeStr == null || dayOfMonthStr == null || monthStr == null || yearStr == null) {
      error();
    }

    int year = toInt(yearStr);
    if (year >= 70 && year <= 99)
      year += 1900;
    else if (year >= 0 && year <= 69) year += 2000;
    if (year < 1601) error();

    int dayOfMonth = toInt(dayOfMonthStr);
    if (dayOfMonth < 1 || dayOfMonth > 31) error();

    int month = getMonth(monthStr) + 1;

    var timeList = timeStr.split(":");
    if (timeList.length != 3) error();
    int hour = toInt(timeList[0]);
    int minute = toInt(timeList[1]);
    int second = toInt(timeList[2]);
    if (hour > 23) error();
    if (minute > 59) error();
    if (second > 59) error();

    return new DateTime.utc(year, month, dayOfMonth, hour, minute, second, 0);
  }

  ///안드로이드 파일 경로
  static Future<String> findLocalPath({String foldName = 'Download'}) async {
    Directory? directory;
    if (path.isEmpty) directory = platform == TargetPlatform.android ? await getExternalStorageDirectory() : await getApplicationDocumentsDirectory();
    if (directory != null) {
      log.d('『GGUMBI』>>> _findLocalPath :platform: $platform, directory: $directory,  <<< ');
      path = (directory.path) + Platform.pathSeparator + foldName;
    }
    log.d('『GGUMBI』>>> _findLocalPath :path: $path');
    return path;
  }

  ///RSA public delete
  static Future<void> setRSAKeyDelete() async {
    try {
      String path = '${await findLocalPath()}/${EncryptHelper.KEY_PUBLIC}';
      File file = File(path);
      if (file.isAbsolute) {
        file.delete();
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> setRSAKeyDelete : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
  }

  ///이미지 여러 파일 만들기
  static FormData setFormDataFiles(AuthCenterHomeRequest data) {
    FormData formData = FormData();
    try {
      log.d('『GGUMBI』>>> setFormDataFiles : data: $data,  <<< ');

      List<ImageFormData> files = [];

      if (data.ourhome_picture1 != null) {
        files.add(ImageFormData(AuthCenterHomeRequest.Key_ourhome_picture1, data.ourhome_picture1!));
      }
      if (data.ourhome_picture2 != null) {
        files.add(ImageFormData(AuthCenterHomeRequest.Key_ourhome_picture2, data.ourhome_picture2!));
      }
      Future.forEach(files, (ImageFormData value) async {
        String fileName = value.file.path.split('/').last;
        log.d('『GGUMBI』>>> setFormDataFiles : fileName: $fileName,  <<< ');
        formData.files.add(MapEntry(value.key, await MultipartFile.fromFile(value.file.path, filename: fileName, contentType: MediaType('image', 'jpeg'))));
      });
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> setFormData : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return formData;
  }

  ///이미지 여러 파일 만들기
  static Future<FormData> setFormDataFiles2(String key, List<File> files) async {
    FormData formData = FormData();
    try {
      if (files.isNotEmpty) {
        await Future.forEach(files, (File value) async {
          String fileName = value.path.split('/').last;
          log.d('『GGUMBI』>>> setFormDataFiles : fileName: $fileName,  <<< ');
          formData.files.add(MapEntry(key, await MultipartFile.fromFile(value.path, filename: fileName, contentType: MediaType('image', 'jpeg'))));
          log.d('『GGUMBI』>>> setFormDataFiles: ');
        });
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> setFormData : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return formData;
  }

  ///이미지 파일 만들기
  static Future<FormData> setFormDataFiles3(String key, File? files, {String mediaType = 'jpeg'}) async {
    FormData formData = FormData();
    try {
      if (files != null) {
        String fileName = files.path.split('/').last;
        log.d('『GGUMBI』>>> setEncryptFormDataFiles : fileName: $fileName,  <<< ');
        formData.files.add(MapEntry(key, await MultipartFile.fromFile(files.path, filename: fileName, contentType: MediaType('image', mediaType))));
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> setFormData : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return formData;
  }

  ///이미지 파일 만들기
  static Future<FormData> setFormDataEncFiles(String key, File? files) async {
    FormData formData = FormData();
    try {
      if (files != null) {
        String fileName = files.path.split('/').last;
        log.d('『GGUMBI』>>> setEncryptFormDataFiles : fileName: $fileName,  <<< ');
        var file = await encryptHelper.encodeData(files);
        formData.files.add(MapEntry(key, MultipartFile.fromString(file, filename: fileName, contentType: MediaType('image', 'jpeg'))));
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> setFormData : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    }
    return formData;
  }

  ///url 이미지를 로컬 이미지로 만들기
  static Future<File> setImage(String imageUrl) async {
    final documentDirectory = await getApplicationDocumentsDirectory();
    String fileName = imageUrl.split('/').last;
    var response = await Dio().get(imageUrl, options: Options(responseType: ResponseType.bytes));
    final file = File(p.join(documentDirectory.path, fileName));
    file.writeAsBytesSync(response.data);
    log.d('『GGUMBI』>>> setImage : file: $file,  <<< ');
    return file;
  }

  ///url 이미지를 로컬 이미지로 만들기
  static Future<List<File>> setImages(List<HomeStateImageData> datas) async {
    List<HomeStateImageData> _imageData = [];
    List<File> _images = [];

    final documentDirectory = await getApplicationDocumentsDirectory();
    log.d('『GGUMBI』>>> setImages : _imageData.length: ${_imageData.length},  <<< ');

    _imageData.asMap().forEach((index, value) async {
      var response = await Dio().get(value.ourhome_picture, options: Options(responseType: ResponseType.bytes));
      final file = File(p.join(documentDirectory.path, value.filename));
      file.writeAsBytesSync(response.data);
      _images.add(file);

      // await Future.forEach(_imageData, (value) async {
      //   var response = await Dio().get(value.ourhome_picture, options: Options(responseType: ResponseType.bytes));
      //   final file = File(p.join(documentDirectory.path, value.filename));
      //   file.writeAsBytesSync(response.data);
      //   _images.add(file);
      //   log.d('『GGUMBI』>>> setImages : _imageData[index].ourhome_picture: $_images,  <<< ');
      //   notifyListeners();
      // });
      log.d('『GGUMBI』>>> setImages : _imageData[index].ourhome_picture: $index : , $_images,  <<< ');
      if (index == _imageData.length - 1) {
        log.d('『GGUMBI』>>> setImages ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
        return;
      }
    });
    return _images;
  }

  static Future<File> bytesToImage(Uint8List bytes, String fileName, String ext) async {
    String tempPath = (await getTemporaryDirectory()).path;
    File file = File('$tempPath/$fileName.$ext');
    await file.writeAsBytes(bytes.buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes));
    return file;
  }

  ///메뉴 파일로 화면에 사용할 데이터
  static Map getJobData(int servicetype, int possibleArea) {
    var data = Map();
    List<ServiceItem> servicesData = [];
    ServiceItem serviceItem = ServiceItem.init();
    try {
      MenuFileData menufile = storageHelper.menu_file_data;
      if (menufile.services!.isNotEmpty) {
        log.d('『GGUMBI』>>> setJobData : menufile.services: ${menufile.services},  <<< ');
        menufile.services!.forEach((services) {
          ServicesData item = services as ServicesData;
          log.d('『GGUMBI』>>> setJobData : item: ${storageHelper.user_type}, $possibleArea,, ${item.area_no}  <<< ');
          //서비스에 대한 내용을 찾는다. services
          if (servicetype == item.servicetype_no && possibleArea == item.area_no) {
            log.d('『GGUMBI』>>> build : services: $item,  <<< ');
            //이동 수단에 대한 정보를 담는다. transports
            item.transports!.forEach((transportsItem) {
              serviceItem = ServiceItem.init();
              menufile.caretypes!.forEach((caretypes) {
                CareTypesData item = caretypes as CareTypesData;
                if (transportsItem.caretype_no == item.type_id) {
                  // log.d('『GGUMBI』>>> build : caretypes transports item : ${item},  <<< ');
                  serviceItem.cares = item;
                }
              });

              menufile.products!.forEach((products) {
                ProductData item = products as ProductData;
                transportsItem.basic_product!.forEach((basic) {
                  if (basic == item.product_id) {
                    // log.d('『GGUMBI』>>> build : products transports basic item : ${item},  <<< ');
                    serviceItem.products!.add(item);
                  }
                });

                transportsItem.add_product!.forEach((add) {
                  if (add == item.product_id) {
                    // log.d('『GGUMBI』>>> build : products transports add item : ${item},  <<< ');
                    serviceItem.products!.add(item);
                  }
                });
              });
              servicesData.add(serviceItem);
              log.d('『GGUMBI』>>> setJobData : serviceItem: $serviceItem,  <<< ');
            });

            data[ServicesData.Key_transports] = servicesData;

            servicesData = [];

            //보육 서비스 대한 정보를 담는다. boyuks
            item.boyuks!.forEach((boyuks) {
              serviceItem = ServiceItem.init();
              menufile.caretypes!.forEach((caretypes) {
                CareTypesData item = caretypes as CareTypesData;
                if (boyuks.caretype_no == item.type_id) {
                  serviceItem.cares = item;
                  menufile.products!.forEach((products) {
                    ProductData item = products as ProductData;
                    boyuks.basic_product!.forEach((basic) {
                      if (basic == item.product_id) {
                        // log.d('『GGUMBI』>>> build : products boyuks basic item : ${item},  <<< ');
                        serviceItem.products!.add(item);
                      }
                    });

                    boyuks.add_product!.forEach((add) {
                      if (add == item.product_id) {
                        // log.d('『GGUMBI』>>> build : products boyuks add item : ${item},  <<< ');
                        serviceItem.products!.add(item);
                      }
                    });
                  });

                  servicesData.add(serviceItem);
                  // log.d('『GGUMBI』>>> setJobData : serviceItem: $serviceItem,  <<< ');
                }
              });
            });

            data[ServicesData.Key_boyuks] = servicesData;

            servicesData = [];
            //가사 서비스 대한 정보를 담는다. homecares
            item.homecares!.forEach((homecares) {
              serviceItem = ServiceItem.init();
              menufile.caretypes!.forEach((caretypes) {
                CareTypesData item = caretypes as CareTypesData;
                // log.d('『GGUMBI』>>> setJobData : homecares.caretype_no: ${homecares.caretype_no},  item.type_id: ${item.type_id}, serviceItem: ${serviceItem}, <<< ');
                if (homecares.caretype_no == item.type_id) {
                  serviceItem.cares = item;
                }
              });

              menufile.products!.forEach((products) {
                ProductData item = products as ProductData;
                homecares.basic_product!.forEach((basic) {
                  if (basic == item.product_id) {
                    // log.d('『GGUMBI』>>> build : products homecares basic item : ${item},  <<< ');
                    serviceItem.products!.add(item);
                  }
                });

                homecares.add_product!.forEach((add) {
                  if (add == item.product_id) {
                    // log.d('『GGUMBI』>>> build : products homecares add item : ${item},  <<< ');
                    serviceItem.products!.add(item);
                  }
                });
              });

              servicesData.add(serviceItem);
              // log.d('『GGUMBI』>>> setJobData : serviceItem: $serviceItem,  <<< ');
            });

            data[ServicesData.Key_homecares] = servicesData;

            servicesData = [];
            //놀이 서비스 대한 정보를 담는다. plays
            item.plays!.forEach((plays) {
              serviceItem = ServiceItem.init();
              menufile.caretypes!.forEach((caretypes) {
                CareTypesData item = caretypes as CareTypesData;
                if (plays.caretype_no == item.type_id) {
                  // log.d('『GGUMBI』>>> build : caretypes plays item : ${item},  <<< ');
                  serviceItem.cares = item;
                }
              });

              menufile.products!.forEach((products) {
                ProductData item = products as ProductData;
                plays.basic_product!.forEach((basic) {
                  if (basic == item.product_id) {
                    // log.d('『GGUMBI』>>> build : products plays basic item : ${item},  <<< ');
                    serviceItem.products!.add(item);
                  }
                });

                plays.add_product!.forEach((add) {
                  if (add == item.product_id) {
                    // log.d('『GGUMBI』>>> build : products plays add item : ${item},  <<< ');
                    serviceItem.products!.add(item);
                  }
                });
              });

              servicesData.add(serviceItem);
              // log.d('『GGUMBI』>>> setJobData : serviceItem: $serviceItem,  <<< ');
            });

            data[ServicesData.Key_plays] = servicesData;
          }
        });
      }
    } catch (e) {
      log.e("Exception >>> getJobData ------------- ${e.toString()}");
      storageHelper.getMenuFileData().then((value) {
        log.d("Exception >>> getJobData ------------- Reload menufile");
      });
    }
    return data;
  }

  ///메뉴 파일로 화면에 사용할 데이터(구직신청 시 사용 - 이동수단에 사용 목적)
  static Map getJobTransportsData() {
    var data = Map();
    List<ServiceItem> servicesData = [];
    ServiceItem serviceItem = ServiceItem.init();
    MenuFileData menufile = storageHelper.menu_file_data;
    log.d('『GGUMBI』>>> getJobTransportsData : auth.menuFileData: $menufile,  <<< ');
    if (menufile.services!.isNotEmpty) {
      log.d('『GGUMBI』>>> getJobTransportsData : menufile: $menufile,  <<< ');
      log.d('『GGUMBI』>>> setJobData : menufile.services: ${menufile.services!.length},  <<< ');
      menufile.services!.forEach((services) {
        ServicesData item = services as ServicesData;
        log.d('『GGUMBI』>>> setJobData : item transports : ${item.transports},  <<< ');
        item.transports!.forEach((transportsItem) {
          serviceItem = ServiceItem.init();
          servicesData = [];
          menufile.caretypes!.forEach((caretypes) {
            CareTypesData item = caretypes as CareTypesData;
            if (transportsItem.caretype_no == item.type_id) {
              log.d('『GGUMBI』>>> build : caretypes transports item : $item,  <<< ');
              serviceItem.cares = item;
            }
          });

          menufile.products!.forEach((products) {
            ProductData item = products as ProductData;
            transportsItem.basic_product!.forEach((basic) {
              if (basic == item.product_id) {
                log.d('『GGUMBI』>>> build : products transports basic item : $item,  <<< ');
                serviceItem.products!.add(item);
              }
            });

            transportsItem.add_product!.forEach((add) {
              if (add == item.product_id) {
                log.d('『GGUMBI』>>> build : products transports add item : $item,  <<< ');
                serviceItem.products!.add(item);
              }
            });
          });
          servicesData.add(serviceItem);
        });

        log.d('『GGUMBI』>>> setJobData : serviceItem: $servicesData,  <<< ');
        data[ServicesData.Key_transports] = servicesData;
      });
    }
    return data;
  }

  ///주소 위치 검색 데이터 만들기 isUpdate : true 강제업데이트//
  static Future<List<AddressListItem>> loadAddressFile({bool isUpdate = false}) async {
    // log.d('『GGUMBI』>>> loadJson : storageHelper.getAddressData(): ${storageHelper.getAddressData()},  <<< ');
    List<AddressListItem> listData = [];

    await rootBundle.loadString('assets/strings/address.json').then((value) async {
      try {
        var json = await jsonDecode(value);
        AddressData addressData = AddressData.fromJson(json);

        //강제 업데이트 사용
        if (isUpdate) {
          listData = makeAddressData(addressData);
        } else {
          //주소 파일 버전 가져오기
          int version = await storageHelper.getAddressVersion();
          log.d('『GGUMBI』>>> loadAddressFile :version : $version, ${addressData.version} <<< ');
          if (addressData.version > version) {
            listData = makeAddressData(addressData);
            log.d('『GGUMBI』>>> loadAddressFile : 데이터 만들기: $listData,  <<< ');
          } else {
            storageHelper.getAddressData().then((response) async {
              if (response.isEmpty) {
                await rootBundle.loadString('assets/strings/address.json').then((value) async {
                  try {
                    var json = await jsonDecode(value);
                    AddressData addressData = AddressData.fromJson(json);
                    listData = makeAddressData(addressData);
                  } catch (e) {
                    log.e('『GGUMBI』 Exception >>> loadJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
                  }
                });
              } else {
                listData = await storageHelper.getAddressData();
              }
              return listData;
            });
          }
        }
      } catch (e) {
        log.e('『GGUMBI』 Exception >>> loadJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      }
    });
    return listData;

    // storageHelper.getAddressData().then((response) async {
    //   log.d('『GGUMBI』>>> loadJson : response: ${response},  <<< ');
    //   if (response.isEmpty || isUpdate) {
    //     await rootBundle.loadString('assets/strings/address.json').then((value) async {
    //       try {
    //         var json = await jsonDecode(value);
    //         AddressData addressData = AddressData.fromJson(json);
    //
    //         //주소 파일 버전 가져오기
    //         var version = await storageHelper.getAddressVersion();
    //         Map address = Map();
    //         AddressListItem listItem = AddressListItem.init();
    //         int length = addressData.data.length - 1;
    //         addressData.data.asMap().forEach((index, value) {
    //           // log.d('『GGUMBI』>>> loadJson : : ${value},  <<< ');
    //           //시군구가 없거나 코드가 000이면 대표 지역으로 간주
    //           if (value.address2_id == "000") {
    //             if (address[listItem.address1_id] != null) {
    //               listData.add(listItem);
    //             }
    //             listItem = AddressListItem.init();
    //             listItem.address1_id = value.address1_id;
    //             listItem.address1 = value.address1;
    //             listItem.data.add(value);
    //           } else {
    //             listItem.data.add(value);
    //             address[listItem.address1_id] = listItem;
    //           }
    //
    //           if (length == index) {
    //             if (address[listItem.address1_id] != null) {
    //               listData.add(listItem);
    //             }
    //           }
    //         });
    //         log.d('『GGUMBI』>>> loadJson : listData: ${listData.length},  <<< ');
    //         listItem = address["11"] as AddressListItem;
    //         log.d('『GGUMBI』>>> loadJson : listItem: $listItem,  <<< ');
    //         listItem = address["48"] as AddressListItem;
    //         log.d('『GGUMBI』>>> loadJson : listItem: $listItem,  <<< ');
    //         storageHelper.setAddressDataa(listData);
    //       } catch (e) {
    //         log.e('『GGUMBI』 Exception >>> loadJson : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
    //       }
    //     });
    //   } else {
    //     listData = await storageHelper.getAddressData();
    //     log.d('『GGUMBI』>>> loadJson : listData: $listData,  <<< ');
    //   }
    //   return listData;
    // });
  }

  ///행정동 주소 데이터 만들기
  static List<AddressListItem> makeAddressData(AddressData addressData) {
    List<AddressListItem> listData = [];
    Map address = Map();
    AddressListItem listItem = AddressListItem.init();
    int length = addressData.data!.length - 1;
    addressData.data!.asMap().forEach((index, value) {
      //시군구가 없거나 코드가 000이면 대표 지역으로 간주
      //2021/12/21 세종시 구분하기 위해 예외처리 추가
      if (value.address2_id == "000") {
        if (address[listItem.address1_id] != null) {
          listData.add(listItem);
        }
        listItem = AddressListItem.init();
        listItem.address1_id = value.address1_id;
        listItem.address1 = value.address1;
        listItem.data!.add(value);
      } else {
        listItem.data!.add(value);
        address[listItem.address1_id] = listItem;
      }

      if (length == index) {
        if (address[listItem.address1_id] != null) {
          listData.add(listItem);
        }
      }
    });
    // log.d('『GGUMBI』>>> loadJson : listData: ${listData.length},  <<< ');
    // listItem = address["11"] as AddressListItem;
    // log.d('『GGUMBI』>>> loadJson : listItem: $listItem,  <<< ');
    // listItem = address["48"] as AddressListItem;
    // log.d('『GGUMBI』>>> loadJson : listItem: $listItem,  <<< ');
    storageHelper.setAddressData(listData);
    storageHelper.setAddressVersion(addressData.version);
    return listData;
  }

  ///이동수단 만들기
  static List<SingleItem> makeTransports(List<ServiceItem> serviceDatas) {
    List<SingleItem> _list = [];

    ///도보 - 1, 자동차 - 2, 대중교통 - 3
    serviceDatas.forEach((serviceItem) {
      serviceItem.products!.forEach((value) {
        log.d('『GGUMBI』>>> _setMakeData : value: $value,  <<< ');
        if (value.product_id == MoveType.waking.index || value.product_data!.name == "도보".tr()) {
          //추후 product id 값으로 변경 예정 1,2,3
          _list.add(SingleItem("도보".tr(), content: "도보_메시지".tr(), data: value, type: value.product_id, values: [SingleItem("도보_아이템_1".tr()), SingleItem(value.product_data!.b_cost.toString())])); //메시지,비용
        } else if (value.product_id == MoveType.car.index || value.product_data!.name == "자동차".tr()) {
          _list.add(SingleItem("자동차".tr(), content: "자동차_메시지".tr(), data: value, type: value.product_id, values: [SingleItem("자동차_안내".tr()), SingleItem(value.product_data!.b_cost.toString())])); //메시지,비용
        } else if (value.product_id == MoveType.transport.index || value.product_data!.name == "대중교통".tr()) {
          _list.add(SingleItem("대중교통".tr(), content: "대중교통_메시지".tr(), data: value, type: value.product_id, values: [SingleItem("대중교통_안내".tr()), SingleItem(value.product_data!.b_cost.toString())])); //메시지,비용
        }
        log.d('『GGUMBI』>>> _setMakeData : : $_list,  <<< ');
      });
    });
    return _list;
  }

  ///맘대디(수요자),링크쌤(공급자) 컬러 값
  static Color getColor({bool isExpire = false}) {
    Color value = color_momdady;
    if (storageHelper.user_type == USER_TYPE.mom_daddy) {
      value = isExpire ? color_00b09a : color_momdady;
    } else {
      value = isExpire ? color_a26db2 : color_linkmom;
    }
    return value;
  }

  ///맘대디(수요자),링크쌤(공급자) 컬러 값 반대로 지정하기
  static Color getColorRevers() {
    Color value = color_main;
    if (storageHelper.user_type == USER_TYPE.mom_daddy) {
      value = color_b579c8;
    } else {
      value = color_main;
    }
    return value;
  }

  ///맘대디(수요자),링크쌤(공급자)
  static Color getColorBg() {
    Color value = color_main;
    if (storageHelper.user_type == USER_TYPE.mom_daddy) {
      value = color_f2faf9;
    } else {
      value = color_faf6fc;
    }
    return value;
  }

  ///맘대디(수요자),링크쌤(공급자) 컬러 값 반대로 지정하기
  static Color getColorReversBg() {
    Color value = color_main;
    if (storageHelper.user_type == USER_TYPE.mom_daddy) {
      value = color_faf6fc;
    } else {
      value = color_f2faf9;
    }
    return value;
  }

  ///맘대디(수요자),링크쌤(공급자) 컬러 값 반대로 지정하기
  static Color getColorViewType(ViewType viewType) {
    Color value = color_main;
    if (ViewType.view == viewType) {
      value = color_main;
    } else {
      value = color_b579c8;
    }
    /*if (storageHelper.user_type == USER_TYPE.mom_daddy) {
      if (ViewType.view == viewType) {
        value = color_b579c8;
      } else {
        value = color_main;
      }
    } else {
      if (ViewType.view == viewType) {
        value = color_main;
      } else {
        value = color_b579c8;
      }
    }*/
    return value;
  }

  ///맘대디(수요자),링크쌤(공급자) 컬러 값 반대로 지정하기
  static Color getColorServiceTypeBg({bool isReverse = false}) {
    Color value = color_main;
    if (storageHelper.user_type == USER_TYPE.mom_daddy && !isReverse) {
      value = color_eef8f6;
    } else {
      value = color_f7f3f9;
    }
    return value;
  }

  ///맘대디(수요자),링크쌤(공급자) 컬러 값 반대로 지정하기
  static Color getColorServiceTypeText({bool isReverse = false}) {
    Color value = color_main;
    if (storageHelper.user_type == USER_TYPE.mom_daddy && !isReverse) {
      value = color_75b4ac;
    } else {
      value = color_a485ad;
    }
    return value;
  }

  ///보낸,받은 메시지 배경 색상
  static Color getColorChatSend({bool isSend = true}) {
    Color value = color_main;
    if (!isSend) {
      if (storageHelper.user_type == USER_TYPE.mom_daddy) {
        value = color_effbf8;
      } else {
        value = color_f6fff8;
      }
    } else {
      value = color_f4f4f4;
    }

    return value;
  }

  ///받은 메시지 배경 색상
  static Color getColorChatReceiver() {
    Color value = color_f4f4f4;
    return value;
  }

  // ///맘대디(수요자),링크쌤(공급자) 컬러 값 반대로 지정하기
  // static Color getColorChatBg(int id, int sender) {
  //   Color value = color_main;
  //   if (storageHelper.user_type == USER_TYPE.mom_daddy) {
  //     value = sender == id ? color_eafbf9 : color_ede2fb;
  //   } else {
  //     value = sender == id ? color_ede2fb : color_eafbf9;
  //   }
  //   return value;
  // }

  ///맘대디(수요자),링크쌤(공급자) 컬러 값 반대로 지정하기
  static Color getColorChat(int id, int sender) {
    Color value = color_main;
    if (storageHelper.user_type == USER_TYPE.mom_daddy) {
      value = sender == id ? color_main : color_b579c8;
    } else {
      value = sender == id ? color_b579c8 : color_main;
    }
    return value;
  }

  ///하단 버튼 공통화 체크
  static String bottomBtnTitle(ViewType viewType) {
    String title = '';
    if (viewType == ViewType.modify) {
      title = "저장".tr();
    } else if (viewType == ViewType.view) {
      title = "확인".tr();
    } else {
      title = "다음".tr();
    }
    return title;
  }

  ///갤러리 또는 이미지 파일 가져오기
  static Future<File> getImage(ImageSource type, {double? maxWidth}) async {
    final pickedFile = await ImagePicker().pickImage(source: type, maxWidth: maxWidth);
    late File _image;
    if (pickedFile != null) {
      // _image = File(pickedFile.path);
      _image = replaceFileName(File(pickedFile.path));
    }
    return _image;
  }

  ///성별(남:1, 여:2 -> 남자, 여자)
  static String getGenderToString(int gender) {
    String value = "여자".tr();
    if (gender == GenderType.gender_1.value) {
      value = "남자".tr();
    }
    return value;
  }

  ///나의 등급 이미지 가져오기(0,1,2,3,4...)
  static Widget getGradeImage(int grade) {
    String imagePath = '';
    if (grade == 0) {
      imagePath = IMG_RANK_1;
    } else if (grade == 1) {
      imagePath = IMG_RANK_2;
    } else if (grade == 2) {
      imagePath = IMG_RANK_3;
    } else {
      imagePath = IMG_RANK_1;
    }
    return image(imagePath: imagePath);
  }

  ///내위치 반경(거리)
  static int getRadiusType(String radiusValue) {
    int radius = 10;
    if (radiusValue.contains("10")) {
      radius = 10;
    } else if (radiusValue.contains("5")) {
      radius = 5;
    } else if (radiusValue.contains("1")) {
      radius = 1;
    }
    log.d('『GGUMBI』>>> getRadiusType : radius: $radius,  <<< ');
    return radius;
  }

  ///정렬타입
  static String getSortType(String sortValue) {
    String value = SortType.createdate.toString().split(".").last;
    if (sortValue == "최근등록순".tr()) {
      value = SortType.createdate.toString().split(".").last;
    } else if (sortValue == "빠른날짜순".tr()) {
      value = SortType.caredate.toString().split(".").last;
    } else if (sortValue == "낮은금액순".tr()) {
      value = SortType.costmin.toString().split(".").last;
    } else if (sortValue == "높은금액순".tr()) {
      value = SortType.costmax.toString().split(".").last;
    } else if (sortValue == "링크쌤인증".tr()) {
      value = SortType.authlevel.toString().split(".").last;
    } else if (sortValue == "좋아요순".tr()) {
      value = SortType.favorite.toString().split(".").last;
    }
    log.d('『GGUMBI』>>> getSortType : value: $value,  <<< ');
    return value;
  }

  /// 등원,하원,보육...서비스 타입
  static String getServiceTypeToString(ServiceType serviceType, {String? type}) {
    String type = '';
    if (serviceType == ServiceType.serviceType_0) {
      type = "등원돌봄".tr();
    } else if (serviceType == ServiceType.serviceType_1) {
      type = "하원돌봄".tr();
    } else if (serviceType == ServiceType.serviceType_2) {
      type = "보육돌봄".tr();
    } else if (serviceType == ServiceType.serviceType_3) {
      type = "학습볼봄".tr();
    } else if (serviceType == ServiceType.serviceType_5) {
      type = "이유식반찬".tr();
    }
    log.d('『GGUMBI』>>> getServiceTypeToString : type: $type, $serviceType <<< ');
    return type;
  }

  /// 등원,하원,보육...서비스 타입
  static String getServiceTypeToString2(ServiceType serviceType, {String? type}) {
    String type = '';
    if (serviceType == ServiceType.serviceType_0) {
      type = "등원".tr();
    } else if (serviceType == ServiceType.serviceType_1) {
      type = "하원".tr();
    } else if (serviceType == ServiceType.serviceType_2) {
      type = "보육".tr();
    } else if (serviceType == ServiceType.serviceType_3) {
      type = "학습".tr();
    } else if (serviceType == ServiceType.serviceType_5) {
      type = "이유식반찬".tr();
    }
    log.d('『GGUMBI』>>> getServiceTypeToString : type: $type, $serviceType <<< ');
    return type;
  }

  /// 등원,하원,보육...서비스 타입
  static int getServiceTypeToInt(String serviceType) {
    int type = ServiceType.serviceType_0.index;
    if (serviceType == "등원돌봄".tr() || serviceType == "등원".tr()) {
      type = ServiceType.serviceType_0.index;
    } else if (serviceType == "하원돌봄".tr() || serviceType == "하원".tr()) {
      type = ServiceType.serviceType_1.index;
    } else if (serviceType == "보육돌봄".tr() || serviceType == "보육".tr()) {
      type = ServiceType.serviceType_2.index;
    } else if (serviceType == "학습볼봄".tr() || serviceType == "학습".tr()) {
      type = ServiceType.serviceType_3.index;
    } else if (serviceType == "이유식반찬".tr()) {
      type = ServiceType.serviceType_5.index;
    }
    log.d('『GGUMBI』>>> getServiceTypeToInt : type: $type, $serviceType <<< ');
    return type;
  }

  /// 보육장소 우리집,이웃집
  static String getAreaToString(PossibleArea areaType, {int type = 0}) {
    String value = '';
    if (type == 0) {
      if (areaType == PossibleArea.mom_daddy) {
        value = '${"돌봄장소".tr()} : ${"우리집".tr()}';
      } else if (areaType == PossibleArea.link_mom) {
        value = '${"돌봄장소".tr()} : ${"이웃집".tr()}';
      }
      return value;
    } else {
      if (areaType == PossibleArea.mom_daddy) {
        value = '${"맘대디집".tr()}(${"우리집".tr()})';
        // value = '${"맘대디집".tr()}(${"이웃집".tr()})';
      } else if (areaType == PossibleArea.link_mom) {
        value = '${"링크쌤집".tr()}(${"이웃집".tr()})';
        // value = '${"링크쌤집".tr()}(${"우리집".tr()})';
      }
      return value;
    }
    log.d('『GGUMBI』>>> getServiceTypeToString : type: $value, $areaType <<< ');
  }

  /// 보육장소 우리집,이웃집
  static int getAreaToInt(String areaType) {
    int value = PossibleArea.mom_daddy.value;
    if (areaType == '${"맘대디집".tr()}(${"이웃집".tr()})' || areaType == '${"맘대디집".tr()}(${"우리집".tr()})' || areaType.contains("맘대디집".tr())) {
      value = PossibleArea.mom_daddy.value;
    } else if (areaType == '${"링크쌤집".tr()}(${"우리집".tr()})' || areaType == '${"링크쌤집".tr()}(${"이웃집".tr()})' || areaType.contains("링크쌤집".tr())) {
      value = PossibleArea.link_mom.value;
    }
    return value;
    log.d('『GGUMBI』>>> getServiceTypeToString : type: $value, $areaType <<< ');
  }

  /// 보육장소 우리집,이웃집
  static String getAreaToImagePath(PossibleArea areaType) {
    String type = '';
    if (areaType == PossibleArea.mom_daddy) {
      type = Lcons.my_house().name;
    } else if (areaType == PossibleArea.link_mom) {
      type = Lcons.neighbor_house().name;
    }
    log.d('『GGUMBI』>>> getServiceTypeToString : type: $type, $areaType <<< ');
    return type;
  }

  ///이동방법 타이틀
  static String getMoveTitle(ServiceType serviceType) {
    String type = "이동방법".tr();
    if (serviceType == ServiceType.serviceType_0) {
    } else if (serviceType == ServiceType.serviceType_1) {
    } else if (serviceType == ServiceType.serviceType_2) {
      type = "돌봄주소".tr();
    } else if (serviceType == ServiceType.serviceType_3) {
    } else if (serviceType == ServiceType.serviceType_5) {}
    log.d('『GGUMBI』>>> getMoveTitle : type: $type, $serviceType <<< ');
    return type;
  }

/*

  /// 출발해야하는시간
  static String getStartTimeToString(int serviceType) {
    String type = '';
    if (serviceType == ServiceType.go_school.index) {
      type = "등원볼봄".tr();
    } else if (serviceType == ServiceType.go_home.index) {
      type = "하원볼봄".tr();
    } else if (serviceType == ServiceType.nursery.index) {
      type = "보육돌봄".tr();
    } else if (serviceType == ServiceType.learning.index) {
      type = "학습볼봄".tr();
    } else if (serviceType == ServiceType.food.index) {
      type = "이유식반찬".tr();
    }
    log.d('『GGUMBI』>>> getStartTimeToString : type: $type, $serviceType <<< ');
    return type;
  }
*/

  /// 출발장소 제목
  static String getStartAddressTitle(ServiceType serviceType, {int type = 0}) {
    String value = "출발장소".tr();
    if (serviceType == ServiceType.serviceType_0) {
    } else if (serviceType == ServiceType.serviceType_1) {
    } else if (serviceType == ServiceType.serviceType_2) {
      // type = "돌봄장소".tr();
      log.d('『GGUMBI』>>> getStartAddressTitle : type: $value,  <<< ');
      if (type == 0) {
        value = ''; //아무런 표시하지 않는다. 돌봄신청에서는 아무런 표시를 하지 않는다. 상단 타이틀이 있기 때문에
      } else {
        value = "돌봄장소".tr(); //맘대디 프로필에서는 아무런 정보가 없기 때문에 표시해준다.
      }
    } else if (serviceType == ServiceType.serviceType_3) {
    } else if (serviceType == ServiceType.serviceType_5) {}
    log.d('『GGUMBI』>>> getStartAddressTitle : type: $value, $serviceType <<< ');
    return value;
  }

  ///보육이 아닐 경우는 중복가능 문구를 보여준다.
  static bool isBoyuk(ServiceType serviceType) {
    bool isBoyuk = false;
    if (serviceType == ServiceType.serviceType_2) {
      isBoyuk = true;
    }
    return isBoyuk;
  }

  ///인증 데이터 해시태그 리스트 만들기
  static List<String> getAuthList(AuthInfoData authinfo) {
    List<String> lsAuth = [];
    if (StringUtils.validateString(authinfo.hashtag_cctv)) {
      lsAuth.add(authinfo.hashtag_cctv);
    }
    if (StringUtils.validateString(authinfo.hashtag_anmal)) {
      lsAuth.add(authinfo.hashtag_anmal);
    }
    if (StringUtils.validateString(authinfo.hashtag_codiv_vaccine)) {
      lsAuth.add(authinfo.hashtag_codiv_vaccine);
    }
    if (StringUtils.validateString(authinfo.hashtag_deungbon)) {
      lsAuth.add(authinfo.hashtag_deungbon);
    }
    if (StringUtils.validateString(authinfo.hashtag_criminal)) {
      lsAuth.add(authinfo.hashtag_criminal);
    }
    if (StringUtils.validateString(authinfo.hashtag_personality)) {
      lsAuth.add(authinfo.hashtag_personality);
    }
    if (StringUtils.validateString(authinfo.hashtag_healthy)) {
      lsAuth.add(authinfo.hashtag_healthy);
    }
    if (StringUtils.validateString(authinfo.hashtag_career_2)) {
      lsAuth.add(authinfo.hashtag_career_2);
    }
    if (StringUtils.validateString(authinfo.hashtag_education)) {
      lsAuth.add(authinfo.hashtag_education);
    }
    if (StringUtils.validateString(authinfo.hashtag_career_1)) {
      lsAuth.add(authinfo.hashtag_career_1);
    }
    if (StringUtils.validateString(authinfo.hashtag_graduated)) {
      lsAuth.add(authinfo.hashtag_graduated);
    }
    if (StringUtils.validateString(authinfo.hashtag_babysiter)) {
      lsAuth.add(authinfo.hashtag_babysiter);
    }
    return lsAuth;
  }

  ///Toast 공통사용
  static void showToast(String msg, {Color backgroundColor = color_highlight, Toast? toastLength = Toast.LENGTH_SHORT, ToastGravity? gravity = ToastGravity.BOTTOM}) {
    Fluttertoast.showToast(msg: msg, backgroundColor: backgroundColor, toastLength: toastLength, gravity: gravity);
  }

  ///하단 스낵바
  static void showSnackBar(BuildContext context, String text, {int second = 2, Color bg = color_black, Function? callBack}) {
    ScaffoldMessenger.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(SnackBar(
        content: lText(text, style: st_13()),
        duration: Duration(seconds: second),
        backgroundColor: color_black,
        onVisible: () {
          if (callBack != null) {
            callBack();
          }
        },
      ));
  }

  static setTestMode({Function? callBack}) {
    showNormalDlg(
      title: "개발/운영 서버 선택\n(기본 운영서버입니다.)",
      messageWidget: SizedBox(
        height: heightDlg(navigatorKey.currentContext!),
        width: widthDlg(navigatorKey.currentContext!),
        child: ListView(
          shrinkWrap: true,
          children: [
            ListTile(
              title: lText("개발서버"),
              onTap: () {
                ApiEndPoint.setMode(type: SERVER_TYPE.DEBUG);
                setTestModeUpdate(msg: ApiEndPoint.getBaseUrl(), callBack: callBack);
              },
            ),
            ListTile(
              title: lText("운영서버"),
              onTap: () {
                ApiEndPoint.setMode(type: SERVER_TYPE.RELEASE);
                setTestModeUpdate(msg: ApiEndPoint.getBaseUrl(), callBack: callBack);
              },
            ),
            ListTile(
              title: lText("개발서버(개인-로그)"),
              onTap: () {
                ApiEndPoint.setMode(type: SERVER_TYPE.DEBUG_LOG);
                setTestModeUpdate(msg: ApiEndPoint.getBaseUrl(), callBack: callBack);
              },
            ),
            ListTile(
              title: lText("로컬서버"),
              onTap: () {
                ApiEndPoint.setMode(type: SERVER_TYPE.DEBUG_LOCAL);
                setTestModeUpdate(msg: ApiEndPoint.getBaseUrl(), callBack: callBack);
              },
            ),
            ListTile(
              title: lText("로컬서버2 : 172 ~"),
              onTap: () {
                ApiEndPoint.setMode(type: SERVER_TYPE.DEBUG_LOCAL2);
                setTestModeUpdate(msg: ApiEndPoint.getBaseUrl(), callBack: callBack);
              },
            ),
            ListTile(
              title: lText("로그인화면으로 이동"),
              onTap: () {
                Commons.pageClear(navigatorKey.currentContext!, routeLogin);
              },
            ),
            ListTile(
              title: lText('로고 활성화(ON)'),
              onTap: () {
                setDebugLog(level: Level.debug, callBack: callBack);
              },
            ),
            ListTile(
              title: lText('로고 비활성화(OFF)'),
              onTap: () {
                setDebugLog(level: Level.nothing, callBack: callBack);
              },
            ),

            ListTile(
              title: lText('디버그 모드 ON/OFF'),
              onTap: () async {
                if (await storageHelper.getDebug()) {
                  storageHelper.setDebug(false);
                } else {
                  storageHelper.setDebug(true);
                }
                showToast('디버그 모드 ${await storageHelper.getDebug()}');
              },
            ),
            // setDebugLog(callBack: callBack),
          ],
        ),
      ),
    );
  }

  static setDebugLog({Level level = Level.debug, Function? callBack}) {
    // Logger.level = level;
    // (log as Logger).log(level, '');
    log = Logger(
      level: level,
      printer: PrettyPrinter(
          methodCount: 1,
          // number of method calls to be displayed 실체 로그 위치
          errorMethodCount: 8,
          // number of method calls if stacktrace is provided
          lineLength: 100,
          // width of the output
          colors: false,
          // Colorful log messages
          printEmojis: true,
          // Print an emoji for each log message
          printTime: true // Should each log print contain a timestamp
          ),
    );
    showToast(Logger.level == Level.debug ? '로고 활성화(ON)' : '로고 비활성화(OFF)');
    if (callBack != null) {
      callBack();
    }
  }

  static Future<void> setTestModeUpdate({String msg = '변경되었습니다.', Function? callBack}) async {
    Commons.pagePop(navigatorKey.currentContext!);
    showToast('$msg 변경되었습니다.');
    await initPlatformDevice();
    callBack!();
  }

  ///링크쌤 여부(true:링크쌤, false:맘대디)
  static bool isLinkMom() {
    return storageHelper.user_type == USER_TYPE.link_mom;
  }

  ///내신청서 인지 구별
  static bool isCareIsMine(int momdady) {
    if (momdady == auth.user.id) {
      return true;
    }
    return false;
  }

  ///맘대디 또는 링크쌤으로 전환시 사용 momdadyId: 맘대디 id, linkMomId : 링크쌤 id, changeType : 즉시변경)
  static USER_TYPE? setModeChange({String? talker_matching_mode, int momdadyId = 0, int linkMomId = 0, USER_TYPE? changeType, bool isShowToast = true}) {
    USER_TYPE? type;
    log.d('『GGUMBI』>>> setModeChange : talker_matching_mode: $talker_matching_mode, id: ${auth.user.id}, momdadyId: $momdadyId, linkMomId: $linkMomId, changeType: $changeType <<< ');
    if (changeType == null) {
      //링크쌤으로 전환 (링크쌤 아이디와 현재 로그인 아이디가 같고, 맘대디모드일경우)
      if (linkMomId == auth.user.id && !isLinkMom()) {
        type = USER_TYPE.link_mom;
        //맘대디로 전환 (맘대디 아이디와 현재 로그인 아이디가 같고, 링크쌤모드일경우)
      } else if (momdadyId == auth.user.id && isLinkMom()) {
        type = USER_TYPE.mom_daddy;
      }
    } else {
      type = changeType;
    }

    if (type != null && type != storageHelper.user_type) {
      storageHelper.setUserType(type, callBack: (userType) {
        type = userType;
        String value = '${(userType == USER_TYPE.link_mom ? USER_TYPE.link_mom.name : USER_TYPE.mom_daddy.name)} ${"모드변경".tr()}';
        if (isShowToast) {
          Commons.showToast(value);
        }
      });
    }
    return type;
  }

  static bool isLinkmomPush(Map notificationValueMap) {
    String src = "";
    if (Platform.isIOS) {
      src = notificationValueMap["src"];
    } else if (Platform.isAndroid) {
      src = notificationValueMap["src"];
    }

    if (src == "linkmom") {
      return true;
    }
    return false;
  }

  static bool isContract(int status) {
    bool isContract = false;
    if (status >= MatchingStatus.waitMomdaddy.value) {
      isContract = true;
    }
    // if (status >= MatchingStatus.waitPaid.value) {
    //   isContract = true;
    // }else if (storageHelper.user_type == USER_TYPE.mom_daddy && status >= MatchingStatus.waitMomdaddy.value) {
    //   isContract = true;
    // } else if (storageHelper.user_type == USER_TYPE.link_mom && status >= MatchingStatus.waitMomdaddy.value) {
    //   isContract = true;
    // }
    return isContract;
  }

  static Future<File> networkToFile(BuildContext context, String? url) async {
    if (url != null) {
      var dir = await getApplicationDocumentsDirectory();
      Uri uri = Uri.parse(url);
      await Dio().downloadUri(uri, '${dir.path}/${url.split('/').last}');
      return File('${dir.path}/${url.split('/').last}');
    } else {
      return File('');
    }
  }

  ///지원서 클릭시 -> 1.동네인증 체크, 2.집안환경 순으로 하여 진행한다. 처리중
  static moveApplyPage(BuildContext context, DataManager data, {ViewMode? viewMode, bool? isLinkMomMode, Function? callBack}) async {
    log.d('『GGUMBI』>>> moveApplyPage : 동네인증: ${auth.user.my_info_data!.auth_address!.is_gpswithaddress}, 집안환경인증:${auth.user.my_info_data!.isAuthHomestate}  <<< ');
    if (isLinkMomMode == null) {
      isLinkMomMode = isLinkMom();
    }
    //동네인증 체크
    if (!auth.user.my_info_data!.auth_address!.is_gpswithaddress) {
      showNormalDlg(
        message: "동네인증체크_안내".tr(),
        onClickAction: (action) async {
          if (action == DialogAction.yes) {
            var response = await Commons.page(context, routeMyInfo, arguments: MyInfoPage(data: data, viewMode: viewMode ?? ViewMode(viewType: ViewType.apply)));
            if (response != null) {
              //집안환경인증 체크
              if (!auth.user.my_info_data!.isAuthHomestate) {
                Commons.page(context, routeAuthHome, arguments: AuthHomeStatePage(data: data, viewType: ViewType.apply));
              }
            }
          }
        },
      );
    } else {
      //집안환경인증 체크
      if (!auth.user.my_info_data!.isAuthHomestate) {
        Commons.page(context, routeAuthHome, arguments: AuthHomeStatePage(data: data, viewType: ViewType.apply));
      } else {
        if (isLinkMomMode) {
          if (!auth.indexInfo.userinfo.isUserLinkmom || !auth.indexInfo.linkmominfo.isBookingjobs) {
            Commons.page(context, routeGuideLinkmom);
          } else {
            var result = await Commons.page(context, routeGuideLinkmom, arguments: GuideLinkMomPage(viewMode: ViewMode(viewType: ViewType.modify)));
            if (result != null && callBack != null) {
              callBack(result);
            }
          }
        } else {
          Commons.page(context, routeGuideMomdady, arguments: GuideMomDadyPage());
        }
      }
    }
  }

  ///동네인증 여부 체크하기! (동네 인증후 반드시 indexInfo를 갱신하여 체크한다)
  static bool isGpsWithAddressCheck(BuildContext context, {String? nextRoute, int? subIndex}) {
    if (!auth.user.my_info_data!.auth_address!.is_gpswithaddress) {
      showNormalDlg(
        message: "동네인증체크_안내".tr(),
        onClickAction: (action) async {
          if (action == DialogAction.yes) {
            var response = await Commons.page(context, routeMyInfo, arguments: MyInfoPage(viewMode: ViewMode(viewType: ViewType.modify)));
            if (response != null) {
              if (nextRoute != null) {
                //채팅 ->맘대디/링크쌤찾기, 커뮤니티 가기
                if (nextRoute == routeChat) {
                  if (subIndex != null) {
                    Commons.pageToMainTabMove(context, tabIndex: subIndex);
                  }
                } else {
                  // Commons.page(context, nextRoute, arguments: MyInfoPage(data: data, viewMode: ViewMode(viewType: ViewType.modify)));
                }
              }
            }
          }
        },
      );
      return auth.user.my_info_data!.auth_address!.is_gpswithaddress;
    } else {
      if (nextRoute != null) {
        if (subIndex != null) {
          Commons.pageToMainTabMove(context, tabIndex: subIndex);
        }
      }
    }
    return true;
  }

  static File replaceFileName(File file) {
    List<String> dir = file.path.split('/');
    String newPath = dir.sublist(0, dir.length - 1).join('/') + '/${DateTime.now().toUtc().microsecondsSinceEpoch.toString()}.jpg';
    file = file.renameSync(newPath);
    return file;
  }

  ///소명을 할 수 있는 조건
  ///링크쌤이고, 맘대디가 링크쌤 사유로 취소시 소명하기!, 맘대디이고, 링크쌤이 맘대디 사유로 취소시 소명하기!
  static bool isExplain(CancelUserType cancelUserType, bool isCancelLinkMom) {
    if (isLinkMom() && cancelUserType == CancelUserType.momdady && isCancelLinkMom || !isLinkMom() && cancelUserType == CancelUserType.linkmom && !isCancelLinkMom) {
      return true;
    }
    return false;
  }

  ///분 -> 시 단위로 계산
  static double getTotalTime(int totalTime) {
    return totalTime / 60;
  }

  ///분당 환산금액(신청시간에 표시)
  static int getMinutePrice(int payPrice) {
    return payPrice ~/ 6;
  }

  ///신청금액(기간 X 시급(보육비) X 신청시간)
  static int getPrice(int day, int payPrice, int totalTime) {
    // double calcuHourSum = data.payItem.day * data.payItem.payPrice * getTotalTime(data.jobItem.totalTime);
    double calcuHourSum = day * payPrice * getTotalTime(totalTime);
    int price = calcuHourSum.toInt();
    log.d('『GGUMBI』>>> getPrice : 신청금액: 신청금액:$price, 실제금액:$calcuHourSum, 시간: ${getTotalTime(totalTime)}, 신청기간:$day, 시급:$payPrice <<< ');
    return price;
  }

  ///총 결제금액의 10분당 환산 금액 10분당 단가 → 전체금액 합계 ((cost_perhour X 신청시간) + cost_negotiable) / 60(분)
  ///시급x전체시간(분) / 60(분)
  static int getAvgPrice(int payPrice, int totalTime, int negotiabl) {
    log.d('『GGUMBI』>>> getAvgPrice : 10분당 환산 금액 : 신청금액:$payPrice, 시간: ${getTotalTime(totalTime)}, 협의금액: $negotiabl <<< ');
    if (negotiabl == null) {
      negotiabl = 0;
    }
    double time = 6;
    // if (totalTime < 60) {
    //   time = totalTime / 10;
    // } else {
    //   time = time;
    // }
    int calcu = 0;
    try {
      calcu = (payPrice ~/ time);
    } catch (e) {
      calcu = 0;
    }
    return calcu;
    // return ((payPrice * calcuTime) + negotiabl) ~/ time;
  }

  ///보육비+스피드매칭+긴급 = 총합계
  static int getTotalPay(int day, int payPrice, int totalTime) {
    log.d('『GGUMBI』>>> getTotalPay : 총 결제금액 : 신청기간 : $day, 시급: $payPrice, 신청시간 : $totalTime <<< ');
    int total = getPrice(day, payPrice, totalTime);
    log.d('『GGUMBI』>>> getTotalPay : 총 결제금액 : $total,  <<< ');
    var roundedInteger = (total / 10).floor() * 10; //절삭된 금액
    log.d('『GGUMBI』>>> getTotalPay : : $total, $roundedInteger, ${total / 10}, ${(total / 10).floor()}, ${(total / 10).floor() * 10} <<< ');
    log.d('『GGUMBI』>>> getTotalPay : total: $total, ${total - roundedInteger}, total:$roundedInteger  <<< ');
    return roundedInteger;
  }

  ///보육비+스피드매칭+긴급 = 총합계 적살됨 금액
  static int getTotalFloor(int day, int payPrice, int totalTime, int speedPrice, int emergencyPrice) {
    int total = getPrice(day, payPrice, totalTime) + speedPrice + emergencyPrice;
    var roundedInteger = (total / 10).floor() * 10; //절삭된 금액
    log.d('『GGUMBI』>>> getTotalPay getTotalFloor : : $roundedInteger, ${total / 10}, ${(total / 10).floor()}, ${(total / 10).floor() * 10} <<< ');
    log.d('『GGUMBI』>>> getTotalPay : total: $total, ${total - roundedInteger}  <<< ');

    return total - roundedInteger;
  }

  static double height(BuildContext context, double rate) => MediaQuery.of(context).size.height * rate;

  static double width(BuildContext context, double rate) => MediaQuery.of(context).size.width * rate;

  static Future<AndroidDeviceInfo> onAndroidInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    return androidInfo;
  }

  static Future<IosDeviceInfo> onIosInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    return iosInfo;
  }

  static Future<VersionData> getDeviceInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    VersionData deviceData = VersionData();

    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidDeviceInfo(await onAndroidInfo(), packageInfo);
      } else if (Platform.isIOS) {
        deviceData = _readIosDeviceInfo(await onIosInfo(), packageInfo);
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> getDeviceInfo : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      deviceData = VersionData();
    }

    return deviceData;
  }

  static VersionData _readAndroidDeviceInfo(AndroidDeviceInfo info, PackageInfo packageInfo) {
    var release = info.version.release;
    var sdkInt = info.version.sdkInt;
    var manufacturer = info.manufacturer;
    var model = info.model;
    var unique = info.androidId;
    VersionData item = VersionData.fromJson({
      VersionData.Key_version: packageInfo.version,
      VersionData.Key_code: packageInfo.buildNumber,
      VersionData.Key_os: 'Android $release (SDK $sdkInt), device: $manufacturer $model',
      VersionData.Key_unique: unique,
    });
    // item.versionName = packageInfo.version;
    // item.versionCode = packageInfo.buildNumber;
    // item.os = 'Android $release (SDK $sdkInt), device: $manufacturer $model';
    log.d('『GGUMBI』>>> deviceVersion _readAndroidDeviceInfo : item: $item,  <<< ');
    return item;
  }

  static VersionData _readIosDeviceInfo(IosDeviceInfo info, PackageInfo packageInfo) {
    var systemName = info.systemName;
    var version = info.systemVersion;
    var machine = info.utsname.machine;
    var unique = info.identifierForVendor;

    VersionData item = VersionData.fromJson({
      VersionData.Key_version: packageInfo.version,
      VersionData.Key_code: packageInfo.buildNumber,
      VersionData.Key_os: 'iOS $systemName (SDK $version), device: $machine',
      VersionData.Key_unique: unique,
    });
    // item.versionName = packageInfo.version;
    // item.versionCode = packageInfo.buildNumber;
    // item.os = 'iOS $systemName (SDK $version), device: $machine';
    log.d('『GGUMBI』>>> deviceVersion _readIosDeviceInfo : item: $item,  <<< ');
    return item;
  }

  ///pku, holiday, hank download
  static Future<Widget> doLoginFlow(
    BuildContext context,
    String pageName,
    LoginData data, {
    Function? callBack,
  }) async {
    if (data.user!.penalty_15.isNotEmpty) {
      storageHelper.setPenalty(PenaltyFlag(third: false));
      showNormalDlg(
          context: context,
          message: "벌점15개_로그인".tr(),
          onClickAction: (action) async {
            await auth.requestLogout(context);
            Commons.pageClear(context, routeLogin);
          });
      auth.setAutoLogin = false;
      return pageName == '$SplashPage' ? lLoadingIndicator() : Center();
    } else if (data.user!.penalty_10.isNotEmpty) {
      DateTime date = StringUtils.parseYMD(data.user!.penalty_10).add(Duration(days: 7));
      if (date.isBefore(DateTime.now())) {
        storageHelper.setPenalty(PenaltyFlag(second: false));
        return _moveTutorial();
      } else {
        String limit = StringUtils.parseYMDLocale(date);
        storageHelper.setPenaltyDate(data.user!.penalty_10);
        showNormalDlg(context: context, message: '${"벌점10개_로그인_1".tr()} $limit${"벌점10개_로그인_2".tr()}', onClickAction: (action) => _moveTutorial());
        return pageName == '$SplashPage' ? lLoadingIndicator() : _moveTutorial();
      }
    } else {
      return _moveTutorial();
    }
  }

  ///PKU파일 다운로드
  static Future downloadPku(String savePath) async {
    try {
      String savedName = await storageHelper.getPkuName();
      apiHelper.requestPku().then((response) {
        if (response.getCode() == KEY_SUCCESS) {
          String url = response.getData().pku;
          var split = url.split('/');
          String name = split[split.length - 1];
          savePath = '$savePath/$name';
          if (savedName != name) {
            // log.d('|| $savedName, $name <<< ');
            apiHelper.downLoadFile(url).then((response) async {
              if (response.statusCode == KEY_SUCCESS) {
                File file = File(savePath);
                var raf = file.openSync(mode: FileMode.write);
                raf.writeFromSync(response.data);
                await raf.close();
                storageHelper.setPkuName(name);
                if (Commons.isDebugMode) showToast("PKU 파일로드 OK");
              }
            });
          }
        }
      });
      //로그인후 RSA키 생성하기
      await encryptHelper.createRSAKey();
    } catch (e) {
      await encryptHelper.createRSAKey();
      log.e('『GGUMBI』 Exception >>> download : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  ///공휴일 파일 다운로드
  static Future<bool> downloadHoliday(List<String> url, String savePath) async {
    try {
      List<String> path = [];
      String name, year;
      url.forEach((element) {
        var split = element.split('/');
        name = split.last;
        year = split[split.length - 2];
        path.add('${year}_$name');
      });
      List<String> savedName = await storageHelper.getHolidayName();
      await HolidaysHelper.loadHolidays();
      if (savedName.where((name) => !path.contains(name)).isNotEmpty || HolidaysHelper.getHolidays.isEmpty) {
        storageHelper.setHolidayName(path);
        for (String link in url) {
          await apiHelper.downLoadFile(link).then((response) async {
            if (response.statusCode == KEY_SUCCESS) {
              var split = response.realUri.toString().split('/');
              name = split.last;
              year = split[split.length - 2];
              File file = File('$savePath/${year}_$name');
              var raf = file.openSync(mode: FileMode.write);
              raf.writeFromSync(response.data);
              log.i("|| $link download: ${file.path}");
              await raf.close();
            }
          });
        }
      } else {
        log.d('Already downloaded $savedName');
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> download : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return true;
  }

  static Future<bool> downloadBank(String url, String savePath, int length) async {
    try {
      String name = url.split('/').last;
      await storageHelper.getBankList(url);
      log.i('『GGUMBI』 banklist length: ${storageHelper.bankListLegnth} / $length');
      if (storageHelper.bankListLegnth != length || storageHelper.bankList.isEmpty) {
        apiHelper.downLoadFile(url).then((response) async {
          if (response.statusCode == KEY_SUCCESS) {
            storageHelper.setBankList('$savePath/$name', response.data);
          }
        });
      }
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> download : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
    return true;
  }

  static void penaltyCheck(Function onClick, {bool forceEnable = false, String? message}) {
    if (storageHelper.penalty && !forceEnable) {
      showNormalDlg(context: navigatorKey.currentContext, message: '${"벌점이열개".tr()} ${StringUtils.parseYMDLocale(storageHelper.penaltyDate.add(Duration(days: 7)))}${"까지".tr()}${"사용이중지된계정입니다".tr()}');
    } else {
      lLoginCheck(navigatorKey.currentContext!, () => onClick(), message: message);
    }
  }

  static Widget _moveTutorial() {
    String route = routeHome;
    if (Flags.getFlag(Flags.KEY_SHOW_LOGIN_TUTORIAL).isShown) {
      route = routeHome;
    } else {
      route = routeTutorial;
    }
    return pageReplace(navigatorKey.currentContext, route);
  }

  static bool ratioChecker(double height, double width) {
    if (height / width > 1.9) {
      return true;
    } else {
      return false;
    }
  }

  /// get device ratio. percent: 100% == 10
  /// refer to mypage line 161
  static double ratio(double percent) {
    double ratio = ui.window.physicalSize.aspectRatio;
    return ratio * percent;
  }

  static String get appleAppId => '1593591340';

  static String possibleAreaSubText(String area) {
    if (area.contains(',')) {
      List<String> areas = area.split(',');
      areas = areas.map((e) => possibleAreaSubText(e)).toList();
      return areas.toString().replaceAll('[', '').replaceAll(']', '');
    } else {
      return area + '(${area.contains(storageHelper.user_type.name) ? "우리집".tr() : "이웃집".tr()})';
    }
  }

  static void moveIntro(BuildContext? context) {
    if (!Flags.getFlag(Flags.KEY_SHOW_LOGIN_INTRO).isShown) {
      pageReplace(context ?? navigatorKey.currentContext, routeIntro);
    } else {
      pageReplace(context ?? navigatorKey.currentContext, routeLogin);
    }
  }

  static void showCatchError(String e) {
    if (isDebugMode) showSnackBar(navigatorKey.currentContext!, e, bg: color_error, second: 10);
  }

  static void showExceptionError(String e) {
    if (isDebugMode) showSnackBar(navigatorKey.currentContext!, e, bg: color_f66962, second: 5);
  }

  static void openLocationSetting() {
    if (Platform.isAndroid) {
      AppSettings.openLocationSettings();
    } else if (Platform.isIOS) {
      AppSettings.openAppSettings();
    }
  }

  static bool _isLogin = false;

  static bool get isLogin => _isLogin;

  static set setLogined(bool logined) => _isLogin = logined;

  static bool isExplanable(CancelUserType cancelUserType, bool isCancelLinkMom) {
    if (isLinkMom() && isCancelLinkMom) {
      /// linkom from cancel, reason linkom
      return cancelUserType == CancelUserType.momdady;
    } else if (isLinkMom() && !isCancelLinkMom) {
      /// linkom from cancel, reason momdaddy
      return cancelUserType == CancelUserType.linkmom;
    } else if (!isLinkMom() && isCancelLinkMom) {
      /// momdaddy from cancel, reason linkmom
      return cancelUserType == CancelUserType.momdady;
    } else if (!isLinkMom() && !isCancelLinkMom) {
      /// momdaddy from cancel, reason momdaddy
      return cancelUserType == CancelUserType.linkmom;
    } else {
      return false;
    }
  }

  static void copyToClipBoard(String value, {String? message}) {
    Clipboard.setData(ClipboardData(text: value));
    Commons.showToast(message ?? '$value ${"복사완료".tr()}', toastLength: Toast.LENGTH_LONG);
  }

  static List<String> get linkRoutes => [
        '/' + RouteAction.linkCare,
        '/' + RouteAction.linkMypage,
        '/' + RouteAction.linkCommunity,
        '/' + RouteAction.linkJoin,
        '/' + RouteAction.linkLinkmom,
        '/' + RouteAction.linkMomdaddy,
        '/' + RouteAction.linkMomdaddy,
        '/' + RouteAction.linkEvent,
      ];

  static File? imageResizer(File? image, {int width = 700}) {
    try {
      final decoded = imgDec.decodeImage(image!.readAsBytesSync())!;
      final thumbnail = imgDec.copyResize(decoded, width: width);
      image.writeAsBytesSync(imgDec.encodePng(thumbnail));
    } catch (e) {
      log.e("imageResizer error: $e");
    }
    return image;
  }

  static void loadFiles() async {
    storageHelper.getBankListWithoutPath();
    storageHelper.getMenuFile();
    storageHelper.getMenuFileData();
    storageHelper.getHolidayName().then((value) => HolidaysHelper.loadHolidays());
  }

  ///이메일인증 체크하기
  static Future<bool> isEmailAuthCheck(BuildContext context) async {
    try {
      //이메일 미인증
      if (!auth.account.is_auth_email) {
        //이메일 미발송 -> 이메일 인증
        if (!auth.account.is_auth_email_send) {
          showNormalDlg(message: "이메일인증_안내_2".tr(), onClickAction: (action) => Commons.page(context, routeMyAccountEmail, arguments: MyAccountEmailPage()));
        } else {
          //이메일 발송 -> 이메일 여부 확인후 ->
          await apiHelper.requestAccountEmailAuth().then((response) {
            if (response.getCode() == KEY_SUCCESS) {
              if (!auth.account.is_auth_email) {
                showNormalDlg(
                    message: '${auth.account.email} ${"이메일인증_안내_3".tr()}',
                    btnLeft: "확인".tr(),
                    btnRight: "이메일인증하기".tr(),
                    onClickAction: (action) {
                      if (action == DialogAction.yes) {
                        Commons.page(context, routeMyAccountEmail, arguments: MyAccountEmailPage());
                      }
                    });
              }
            }
          });
        }
        return auth.account.is_auth_email;
      }
      return auth.account.is_auth_email;
    } catch (e) {
      return false;
    }
  }

  ///이벤트 베너 비율
  static double getEventBannerHeight() {
    return 335 / 132;
  }

  /// launch(_eventBanner.linkUrl, forceSafariVC: false) -> launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication);
  static lLaunchUrl(String url) {
    launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication);
  }
}
