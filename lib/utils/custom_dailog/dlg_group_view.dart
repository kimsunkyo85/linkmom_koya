import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/textHighlight.dart';

import '../../main.dart';
import '../commons.dart';
import '../string_util.dart';

///그룹보육 안내 안내 view
Widget dlgGroupView(BuildContext context, PossibleArea areaType, {int cost_perhour = 10000, Function? onClick}) {
  return Container(
    alignment: Alignment.centerLeft,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        lText(areaType == PossibleArea.mom_daddy ? "동시보육_안내".tr() : "그룹보육_안내".tr(), fontSize: 15, color: color_545454),
        sb_h_10,
        TextHighlight(
          text: "동시보육돌봄비안내".tr(),
          term: "협의가능".tr(),
          textStyle: st_14(textColor: color_999999),
          textStyleHighlight: st_14(textColor: Commons.getColor()),
        ),
        sb_h_10,
        lRoundContainer(
          borderRadius: 0.0,
          borderColor: color_dfe3e4,
          borderWidth: 1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              sb_h_10,
              if (areaType == PossibleArea.mom_daddy)
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    lText("동시보육으로".tr(), style: st_b_16(fontWeight: FontWeight.w500, textColor: Commons.getColor())),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        lText('${"시간당돌봄비인하".tr()} ↓', style: st_b_16(fontWeight: FontWeight.w500, textColor: Commons.getColor())),
                      ],
                    ),
                  ],
                ),

              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   children: [
              //     lText("동시보육으로시간당돌봄비인하".tr(), style: st_b_16(fontWeight: FontWeight.w500, textColor: Commons.getColor())),
              //     Lcons.icon_arrow_down(color: Commons.getColor()),
              //   ],
              // ),
              if (areaType == PossibleArea.link_mom)
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    lText("최대4명그룹돌봄으로".tr(), style: st_b_16(fontWeight: FontWeight.w500, textColor: Commons.getColor())),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        lText('${"시간당돌봄비반값".tr()} ↓', style: st_b_16(fontWeight: FontWeight.w500, textColor: Commons.getColor())),
                      ],
                    ),
                  ],
                ),
              sb_h_10,
              viewGroup(areaType, cost_perhour),
              sb_h_10,
            ],
          ),
        ),
        sb_h_05,
      ],
    ),
  );
}

Widget viewGroup(PossibleArea areaType, int costPerhour) {
  String ex = '${"예시".tr()})';
  TextStyle style = st_b_14(fontWeight: FontWeight.w500);
  return Row(
    // crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          lText(
            areaType == PossibleArea.mom_daddy ? '$ex${"우리아이1명".tr()}' : '$ex${"우리아이만".tr()}',
            style: style,
          ),
          sb_h_03,
          lText(
            areaType == PossibleArea.mom_daddy ? '$ex${"우리아이2명".tr()}' : '$ex${"우리아이외1명".tr()}',
            style: style,
          ),
          sb_h_03,
          lText(
            areaType == PossibleArea.mom_daddy ? '$ex${"우리아이3명".tr()}' : '$ex${"우리아이외2명".tr()}',
            style: style,
          ),
          sb_h_03,
          lText(
            areaType == PossibleArea.mom_daddy ? '$ex${"우리아이4명".tr()}' : '$ex${"우리아이외3명".tr()}',
            style: style,
          ),
        ],
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          lText(
            ' : ${StringUtils.formatPay(costPerhour)}${"원".tr()}',
            style: style,
          ),
          sb_h_03,
          Row(
            children: [
              lText(
                ' : ${StringUtils.formatPay(calcuPrice(areaType, costPerhour)[0])}${"원".tr()}',
                style: style,
              ),
              lText(
                '  20% ↓',
                style: st_b_14(fontWeight: FontWeight.w500, textColor: color_ff3b30),
              ),
            ],
          ),
          sb_h_03,
          Row(
            children: [
              lText(
                ' : ${StringUtils.formatPay(calcuPrice(areaType, costPerhour)[1])}${"원".tr()}',
                style: style,
              ),
              lText(
                '  30% ↓',
                style: st_b_14(fontWeight: FontWeight.w500, textColor: color_ff3b30),
              ),
            ],
          ),
          sb_h_03,
          Row(
            children: [
              lText(
                ' : ${StringUtils.formatPay(calcuPrice(areaType, costPerhour)[2])}${"원".tr()}',
                style: style,
              ),
              lText(
                '  50% ↓',
                style: st_b_14(fontWeight: FontWeight.w500, textColor: color_ff3b30),
              ),
            ],
          ),
        ],
      ),
    ],
  );
}

List<int> calcuPrice(PossibleArea areaType, int costPerhour) {
  List<int>? values = [];
  if (areaType == PossibleArea.mom_daddy) {
    values.add(((costPerhour * 2) * 0.8).toInt());
    values.add(((costPerhour * 3) * 0.7).toInt());
    values.add(((costPerhour * 5) * 0.5).toInt());
  } else {
    values.add(costPerhour - (costPerhour * 0.2).toInt());
    values.add(costPerhour - (costPerhour * 0.3).toInt());
    values.add(costPerhour - (costPerhour * 0.5).toInt());
  }
  return values;
}

Widget dlgGroup(BuildContext context, int timePrice) {
  log.d('『GGUMBI』>>> _dlgGroup : timePirce/10: ${timePrice ~/ 10},  <<< ');
  log.d('『GGUMBI』>>> _dlgGroup : timePirce/10: ${timePrice ~/ 20},  <<< ');
  log.d('『GGUMBI』>>> _dlgGroup : timePirce/10: ${timePrice ~/ 30},  <<< ');
  log.d('『GGUMBI』>>> _dlgGroup : timePirce/10: ${(timePrice * 0.1).toInt()},  <<< ');
  log.d('『GGUMBI』>>> _dlgGroup : timePirce/10: ${(timePrice * 0.2).toInt()},  <<< ');
  int price1 = timePrice - (timePrice * 0.1).toInt();
  int price2 = timePrice - (timePrice * 0.2).toInt();
  int price3 = timePrice - (timePrice * 0.3).toInt();
  return Container(
    alignment: Alignment.center,
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        lText("그룹보육안내".tr(), textAlign: TextAlign.center, style: st_b_17()),
        // lText("그룹보육안내".tr(), textAlign: TextAlign.center),
        // lText("그룹보육안내".tr(), textAlign: TextAlign.center),
        // lText("그룹보육안내".tr(), textAlign: TextAlign.center),
        // lText("그룹보육안내".tr(), textAlign: TextAlign.center),
        // lText("그룹보육안내".tr(), textAlign: TextAlign.center),
        // lText("그룹보육안내".tr(), textAlign: TextAlign.center),
        // lText("그룹보육안내".tr(), textAlign: TextAlign.center),

        sb_h_20,
        Stack(
          alignment: Alignment.topCenter,
          children: [
            // lRoundContainer(child: lText("아이추가시".tr()), borderRadius: 50.0),
            // lTextRectBg('test', ),
            Card(
                elevation: 0,
                color: color_white,
                shape: lRoundedRectangleBorder(width: 1.0, disBorderColor: color_aaaaaa),
                margin: padding_15_T,
                child: Container(
                  width: widthFull(context),
                  padding: padding_10,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      sb_h_20,
                      lText(
                        '${"우리아이만".tr()} ${StringUtils.formatPay(timePrice)}${"원".tr()}',
                        style: st_15(textColor: color_767676),
                      ),
                      sb_h_05,
                      lText(
                        '${"우리아이외1명".tr()} ${StringUtils.formatPay(price1)}${"원".tr()}${"우리아이외1명%".tr()}',
                        // "우리아이외1명".tr(),
                        style: st_15(textColor: color_767676),
                      ),
                      sb_h_05,
                      lText(
                        '${"우리아이외2명".tr()} ${StringUtils.formatPay(price2)}${"원".tr()}${"우리아이외2명%".tr()}',
                        style: st_15(textColor: color_767676),
                      ),
                      sb_h_05,
                      lText(
                        '${"우리아이외3명".tr()} ${StringUtils.formatPay(price3)}${"원".tr()}${"우리아이외3명%".tr()}',
                        style: st_15(textColor: color_767676),
                      ),
                      sb_h_05,
                    ],
                  ),
                )),
            lBtnWrap("아이추가시".tr(), isEnabled: false, height: 30, btnDisableColor: color_aaaaaa, textStyle: st_17()),
          ],
        ),
      ],
    ),
  );
}
