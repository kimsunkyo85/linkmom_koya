import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

///시급 안내 view
Widget dlgPayView() {
  return Column(
    children: [
      sb_h_10,
      lText("돌봄시급_안내".tr(), style: st_15(textColor: color_545454)),
      sb_h_03,
      lText("돌봄시급_가격".tr(), style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)),
      sb_h_03,
      lText("식사협의_안내".tr(), style: st_15(textColor: color_545454)),
    ],
  );
}
