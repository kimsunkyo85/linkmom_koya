import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

double maxHeight = 350.0;
double minHeight = 200.0;

///필요한 돌봄 서비스 리스트 뷰
Widget dlgServiceView({List<ListItem>? listMsg}) {
  double height = (listMsg!.length * 40.0);
  if (height > maxHeight) {
    height = maxHeight;
  } else if (height < minHeight) {
    height = minHeight;
  }
  return Column(
    children: [
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          lText(
            "필요한돌봄가이드안내".tr(),
            style: st_b_16(textColor: color_545454),
          ),
          sb_h_10,
          lDivider(),
          sb_h_10,
        ],
      ),
      Container(
        height: height, // Change as per your requirement
        width: 300.0, // Change as per
        // padding: padding_10_LTR,
        child: Theme(
          data: ThemeData(
            highlightColor: color_dbdbdb, colorScheme: ColorScheme.fromSwatch().copyWith(secondary: color_white),
          ),
          child: Scrollbar(
            thickness: 3,
            radius: Radius.circular(10),
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: listMsg.length,
                // physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          lText(listMsg[index].title, style: st_b_16(fontWeight: FontWeight.bold)),
                          lText(' : ${"기본".tr()}', style: st_b_16(fontWeight: FontWeight.w500)),
                          lText(' ${listMsg[index].productData!.product_data!.b_minutes}${"분".tr()}', style: st_b_16(textColor: color_ff3b30, fontWeight: FontWeight.bold)),
                          lText(' ${"소요".tr()}', style: st_b_16(fontWeight: FontWeight.w500)),
                        ],
                      ),
                      sb_h_02,
                      lText(listMsg[index].items!, style: st_b_16(textColor: color_999999)),
                      sb_h_10,
                    ],
                  );
                }),
          ),
        ),
      ),
    ],
  );
}
