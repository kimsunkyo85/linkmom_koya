import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:provider/provider.dart';
import 'package:signature/signature.dart';

import '../../main.dart';
import '../commons.dart';
import '../textHighlight.dart';

class DialogProvider with ChangeNotifier {
  static DialogProvider of(BuildContext context) {
    return Provider.of<DialogProvider>(context);
  }

  DialogAction _action = DialogAction.off;

  DialogAction get action => _action;

  late Object _data;

  Object get data => _data;

  ///데이터 세팅
  setData(Object data) {
    _data = data;
  }

  ///기본적인 액션 값
  setAction() {
    _action = DialogAction.on;
  }

  ///일반 화면 닫기
  onClickClose() {
    _action = DialogAction.close;
    notifyListeners();
  }

  ///취소,아니오 선택
  onClickNo() {
    _action = DialogAction.no;
    notifyListeners();
  }

  ///확인,예 선택
  onClickYes() {
    _action = DialogAction.yes;
    notifyListeners();
  }

  ///앱 업데이트 진행
  onClickUpdate() {
    _action = DialogAction.update;
    notifyListeners();
  }

  ///어플리케이션 종료
  onClickFinish() {
    _action = DialogAction.finish;
    notifyListeners();
  }
}

const double _width = 240;
const double _height = 200;
const double radius = 4.0;
const double edge = 18.0;
const int colorAlpha = 700;
// bool isClose = false;
///기본 다이얼로그
///title : 제목 (기본 가운데 정렬)
///iconPath : 컨텐츠 정보 및 기타
///isClose : 상단 X 버튼 아이콘 (외부 클릭시 dismiss)
///isGuide : 필요한 돌봄시 사용 플래
showDlg({
  BuildContext? context,
  title: '',
  TextAlign titleAlign = TextAlign.center,
  CrossAxisAlignment titleCrossAxisAlignment = CrossAxisAlignment.center,
  MainAxisAlignment titleMainAxisAlignment = MainAxisAlignment.center,
  Alignment titleAlignment = Alignment.center,
  imagePath = '',
  bool isClose = false,
  Widget? icon,
  int? msgCode: 200,
  msg: '',
  Widget? msgWidget,
  btnLeft: '',
  FontWeight weightLeft = FontWeight.w500,
  leftColor = color_979797,
  btnRight: '',
  FontWeight weightRight = FontWeight.w500,
  rightColor = color_white,
  double width = _width,
  double height = _height,
  double btnHeight = 50,
  double borderRadius = 15.0,
  EdgeInsets? insetPadding, //전체 좌우 여백
  EdgeInsetsGeometry contentPadding = padding_0, //내용
  EdgeInsets? iconsPadding,
  onClickAction(DialogAction action)?,
  Color btnLeftBgColor = color_eeeeee,
  Color? btnRightBgColor,
  bool isLeftHighLight = true,
  bool isRightHighLight = true,
  bool isLine = false,
  bool isLineBtn = false,
  bool isBgWhite = false,
}) {
  if (msgCode == KEY_100_NETWORK) {
    btnRight = "설정".tr();
  }

  if (msgCode == KEY_401_UNAUTHORIZED || msgCode == KEY_403_UNAUTHORIZED) {
    btnRightBgColor = color_main;
  }

  String left = btnLeft;
  if (!StringUtils.validateString(btnLeft)) {
    left = "취소".tr();
  }

  String right = btnRight;
  if (!StringUtils.validateString(btnRight)) {
    right = "확인".tr();
  }

  bool isHighlightLeft = false;
  bool isHighlightRight = false;

  ///content padding default
  if (insetPadding == null) {
    insetPadding = EdgeInsets.symmetric(vertical: 24.0, horizontal: 40.0);
  }

  //배경 화이트로 사용시
  if (isBgWhite) {
    btnLeftBgColor = color_white;
    leftColor = color_b2b2b2;
    btnRightBgColor = color_white;
    rightColor = Commons.getColor();
    isLine = true;
    if (StringUtils.validateString(btnLeft)) {
      isLineBtn = true;
    } else {
      isLineBtn = false;
    }
  }

  showDialog(
    context: context ?? navigatorKey.currentContext!,
    // barrierDismissible: isClose ? true : false,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
        return AlertDialog(
          buttonPadding: padding_0,
          contentPadding: contentPadding,
          insetPadding: insetPadding!,
          elevation: 0,
          shape: RoundedRectangleBorder(
            //테두리
            borderRadius: BorderRadius.circular(borderRadius),
          ),
          content: Container(
            width: width,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ///닫기 X 버튼이 있을 경우
                if (isClose)
                  Padding(
                    padding: padding_20,
                    child: Row(
                      children: [
                        if (StringUtils.validateString(imagePath)) image(imagePath: imagePath),
                        sb_w_10,
                        Expanded(
                          child: lText(
                            title,
                            textAlign: titleAlign,
                            style: st_b_18(fontWeight: FontWeight.bold),
                          ),
                        ),
                        GestureDetector(
                            onTap: () {
                              Commons.pagePop(
                                navigatorKey.currentContext!,
                              );
                            },
                            child: Lcons.nav_close(size: 25)),
                      ],
                    ),
                  ),

                ///닫기 버튼이 없는 경우
                if (!isClose)
                  if (StringUtils.validateString(title))
                    Padding(
                      padding: padding_Dlg_Title,
                      child: Column(
                        crossAxisAlignment: titleCrossAxisAlignment,
                        mainAxisAlignment: titleMainAxisAlignment,
                        children: [
                          Container(
                            alignment: titleAlignment,
                            child: lText(
                              title,
                              textAlign: titleAlign,
                              style: st_b_18(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                if (icon != null)
                  Padding(
                    padding: iconsPadding ?? padding_20,
                    child: icon,
                  ),
                Padding(
                    padding: isClose
                        ? padding_20LRB
                        : StringUtils.validateString(title)
                            ? padding_Dlg_Msg
                            : padding_20,
                    child: msgWidget == null
                        ? lScrollView(
                            padding: padding_0,
                            child: Container(
                              alignment: StringUtils.validateString(title) ? Alignment.centerLeft : Alignment.center,
                              child: lText(msg, style: st_b_16(), textAlign: TextAlign.center),
                            ))
                        : Container(
                            alignment: Alignment.center,
                            child: lScrollView(padding: padding_0, child: msgWidget),
                          )),
              ],
            ),
          ),
          actions: [
            if (!isClose)
              Container(
                padding: padding_0,
                width: widthFull(context),
                child: Column(
                  children: [
                    if (isLine) lDivider(), //가로 라인 추가시 주석 제거
                    IntrinsicHeight(
                      child: Container(
                          child: Row(children: <Widget>[
                        if (StringUtils.validateString(btnLeft))
                          Expanded(
                              child: FlatButton(
                            height: btnHeight,
                            splashColor: color_transparent,
                            highlightColor: color_transparent /*color_d4d4d4*/,
                            onHighlightChanged: (values) {
                              setState(() {
                                if (isLeftHighLight) {
                                  isHighlightLeft = values;
                                }
                              });
                            },
                            onPressed: () {
                              Navigator.of(context).pop();
                              if (onClickAction != null) {
                                onClickAction(DialogAction.no);
                              }
                            },
                            child: lText(
                              left,
                              style: st_b_14(
                                textColor: isHighlightLeft ? color_545454 : leftColor,
                                fontWeight: weightLeft,
                              ),
                            ),
                            // color: color_eeeeee,
                            color: isHighlightLeft ? color_highlight.withOpacity(0.1) : btnRightBgColor ?? Commons.getColor(),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(borderRadius)),
                            ),
                          )),
                        // if (StringUtils.validateString(btnLeft)) lDividerVertical(thickness: 1),//세로라인 확인,취소시 주석 제거
                        if (isLineBtn) lDividerVertical(thickness: 1), //세로라인 확인,취소시 주석 제거
                        Expanded(
                          child: FlatButton(
                            height: btnHeight,
                            splashColor: color_transparent,
                            highlightColor: color_transparent /*color_d4d4d4*/,
                            onHighlightChanged: (values) {
                              setState(() {
                                if (isRightHighLight) {
                                  isHighlightRight = values;
                                }
                              });
                            },
                            onPressed: () {
                              Navigator.of(context).pop();
                              if (onClickAction != null) {
                                onClickAction(DialogAction.yes);
                              }
                            },
                            child: lText(
                              right,
                              style: st_b_14(
                                textColor: rightColor,
                                fontWeight: weightRight,
                              ),
                            ),
                            // color: StringUtils.validateString(btnLeft) ? null : color_main,//배경없이 사용할 경우
                            color: isHighlightRight ? color_highlight.withOpacity(0.1) : btnRightBgColor ?? Commons.getColor(),
                            //오른쪽 버튼 [확인만] 있을 경우
                            shape: StringUtils.validateString(btnLeft)
                                ? RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(bottomRight: Radius.circular(borderRadius)),
                                  )
                                : RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(borderRadius), bottomRight: Radius.circular(borderRadius)),
                                  ),
                          ),
                        )
                      ])),
                    )
                  ],
                ),
              ),
          ],
        );
      });
    },
  );
}

Widget showDlgVersion({msg: '', btnTitle: "확인", onClickAction(DialogAction action)?}) {
  return showNormalDlg(
      message: msg,
      btnRight: "업데이트".tr(),
      btnLeft: "취소".tr(),
      onClickAction: (a) {
        if (a == DialogAction.yes) {
          if (onClickAction != null) onClickAction(DialogAction.update);
        } else {
          if (onClickAction != null) onClickAction(DialogAction.cancel);
        }
      });
}

Widget showDlgVersionFinish({msg: '', btnTitle: '앱 종료', onClickAction(DialogAction action)?}) {
  return CupertinoAlertDialog(
    content: lText(
      msg,
      style: bodyStyle,
    ),
    actions: <Widget>[
      CupertinoDialogAction(
          textStyle: TextStyle(color: Colors.blue),
          isDefaultAction: true,
          onPressed: () {
            Commons.appFinish();
            if (onClickAction != null) {
              onClickAction(DialogAction.finish);
            }
          },
          child: lText(btnTitle)),
    ],
  );
}

void showDlgNotBtn({msg: '', highlight: ''}) {
  showDialog(
      context: navigatorKey.currentContext!,
      barrierDismissible: true, //outside dismiss

      builder: (context) {
        return CupertinoAlertDialog(
          // content: Text.rich(
          //   TextSpan(
          //     text: msg,
          //     style: tvBgBody,
          //     children: <TextSpan>[
          //       TextSpan(text: 'beautiful ', style: tvBgHighlight),
          //       TextSpan(text: 'world', style: TextStyle(fontWeight: FontWeight.bold)),
          //     ],
          //   ),
          // ),
          content: TextHighlight(
            text: msg,
            term: highlight,
            textStyleHighlight: tvBgHighlight,
            textStyle: tvBgBody,
            textAlign: TextAlign.center,
          ),
        );
      });
}

showNetwork({msg: '네트워크를 확인해주세요.', btnTitle: '설정', onClickAction(DialogAction action)?}) {
  showNormalDlg(
      context: navigatorKey.currentContext!,
      closeable: false,
      message: msg,
      msgCode: KEY_100_NETWORK,
      onClickAction: (action) {
        if (onClickAction != null) {
          onClickAction(DialogAction.yes);
        }
      });
}

///통신 요청 에러 캐치시 사용
void showDlgException({msg: '', btnTitle: '확인', Function onClickAction(DialogAction action)?}) {
  log.d('『GGUMBI』>>> showDlgExpiton : msg: $msg,  <<< ');
  showDialog(
      context: navigatorKey.currentContext!,
      barrierDismissible: false, //outside dismiss
      builder: (context) {
        return CupertinoAlertDialog(
          content: lText(
            !StringUtils.validateString(msg) ? "에러_예외처리확인".tr() : msg,
            style: bodyStyle,
          ),
          actions: <Widget>[
            CupertinoDialogAction(
                textStyle: TextStyle(color: Colors.blue),
                isDefaultAction: true,
                onPressed: () {
                  Navigator.pop(navigatorKey.currentContext!);
                  if (onClickAction != null) {
                    if (btnTitle == "업데이트".tr()) {
                      onClickAction(DialogAction.on);
                    } else {
                      onClickAction(DialogAction.yes);
                    }
                  }
                },
                child: lText(btnTitle)),
          ],
        );
      });
}

void showDlgYesNo({title: '', msg: '', btnLeft: '취소', btnRight: '확인', onClickAction(DialogAction action)?}) {
  showDialog(
      context: navigatorKey.currentContext!,
      barrierDismissible: false, //outside dismiss
      builder: (context) {
        return CupertinoAlertDialog(
          title: StringUtils.validateString(title) ? Text(title) : null,
          content: lText(
            msg,
            style: bodyStyle,
          ),
          actions: <Widget>[
            CupertinoDialogAction(
                isDefaultAction: true,
                textStyle: TextStyle(color: Colors.black87),
                onPressed: () {
                  Navigator.pop(navigatorKey.currentContext!);
                  if (onClickAction != null) {
                    onClickAction(DialogAction.on);
                  }
                },
                child: lText(btnLeft)),
            CupertinoDialogAction(
                textStyle: TextStyle(color: Colors.blue),
                isDefaultAction: true,
                onPressed: () {
                  Navigator.pop(navigatorKey.currentContext!);
                  if (onClickAction != null) {
                    onClickAction(DialogAction.yes);
                  }
                },
                child: lText(btnRight)),
          ],
        );
      });
}

void showDlgFinish({msg: '', btnTitle: '앱 종료', onClickAction(DialogAction action)?}) {
  showDialog(
      context: navigatorKey.currentContext!,
      barrierDismissible: false, //outside dismiss
      builder: (context) {
        return CupertinoAlertDialog(
          content: lText(
            msg,
            style: bodyStyle,
          ),
          actions: <Widget>[
            CupertinoDialogAction(
                textStyle: TextStyle(color: Colors.blue),
                isDefaultAction: true,
                onPressed: () {
                  Commons.appFinish();
                  if (onClickAction != null) {
                    onClickAction(DialogAction.finish);
                  }
                },
                child: lText(btnTitle)),
          ],
        );
      });
}

void showDlgCenter(BuildContext context, DialogProvider? provider, {msg: '', btnTitle: '확인'}) {
  showDialog(
      context: context,
      barrierDismissible: false, //outside dismiss
      builder: (context) {
        return CupertinoAlertDialog(
          content: lText(
            msg,
            style: bodyStyle,
          ),
          actions: <Widget>[
            CupertinoDialogAction(
                textStyle: TextStyle(color: Colors.blue),
                isDefaultAction: true,
                onPressed: () {
                  Navigator.pop(context);
                  if (provider != null) {
                  if (btnTitle == "업데이트".tr()) {
                    provider.onClickUpdate();
                  } else {
                    provider.onClickYes();
                    }
                  }
                },
                child: lText(btnTitle)),
          ],
        );
      });
}

void showDlgYesNo2(BuildContext context, DialogProvider provider, {msg: '', btnLeft: '취소', btnRight: '확인'}) {
  showDialog(
      context: context,
      barrierDismissible: false, //outside dismiss
      builder: (context) {
        return CupertinoAlertDialog(
          content: lText(
            msg,
            style: bodyStyle,
          ),
          actions: <Widget>[
            CupertinoDialogAction(
                isDefaultAction: true,
                textStyle: TextStyle(color: Colors.black87),
                onPressed: () {
                  Navigator.pop(context);
                  provider.onClickNo();
                },
                child: lText(btnLeft)),
            CupertinoDialogAction(
                textStyle: TextStyle(color: Colors.blue),
                isDefaultAction: true,
                onPressed: () {
                  Navigator.pop(context);
                  provider.onClickYes();
                },
                child: lText(btnRight)),
          ],
        );
      });
}

///사진 선택 및 카메라 다이얼로그 선택
void showDlgCamera({title: '', message: '', btn1: 'Photo Select', String btn2: 'Camera', btn3: '', onClickAction(DialogAction action)?}) {
  showCupertinoModalPopup(
    context: navigatorKey.currentContext!,
    barrierColor: Colors.black.withAlpha(82),
    builder: (BuildContext context) => Container(
      padding: padding_20_LR,
      child: CupertinoActionSheet(
          title: StringUtils.validateString(title) ? lText(title) : null,
          message: StringUtils.validateString(message) ? lText(message) : null,
          actions: <Widget>[
            Container(
              color: Colors.white,
              child: CupertinoActionSheetAction(
                child: lText(btn1, style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)),
                onPressed: () {
                  if (onClickAction != null) {
                    onClickAction(DialogAction.gallery);
                  }
                  Navigator.pop(navigatorKey.currentContext!);
                },
              ),
            ),
            if (btn2.isNotEmpty)
              Container(
              color: Colors.white,
              child: CupertinoActionSheetAction(
                child: lText(btn2, style: st_15(textColor: color_545454, fontWeight: FontWeight.w500)),
                onPressed: () {
                  if (onClickAction != null) {
                    onClickAction(DialogAction.camera);
                  }
                  Navigator.pop(navigatorKey.currentContext!);
                },
              ),
            ),
            if (StringUtils.validateString(btn3))
              Container(
                color: Colors.white,
                child: CupertinoActionSheetAction(
                  child: lText(btn3, style: st_15(textColor: color_ff3b30, fontWeight: FontWeight.w500)),
                  onPressed: () {
                    if (onClickAction != null) {
                      onClickAction(DialogAction.delete);
                    }
                    Navigator.pop(navigatorKey.currentContext!);
                  },
                ),
              )
          ],
          cancelButton: Container(
            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16)),
            child: CupertinoActionSheetAction(
              child: lText("취소".tr(), style: st_15(textColor: color_0085ff, fontWeight: FontWeight.w500)),
              isDefaultAction: true,
              onPressed: () {
                if (onClickAction != null) {
                  onClickAction(DialogAction.cancel);
                }
                Navigator.pop(navigatorKey.currentContext!);
              },
            ),
          )),
    ),
  );
}

void showWidgetDlgYesNo(BuildContext context, DialogProvider provider, {msg: '', btnLeft: '취소', btnRight: '확인', Widget? widget, Function? rightAction, Function? leftAction}) {
  showDialog(
      context: context,
      barrierDismissible: false, //outside dismiss
      builder: (context) {
        return CupertinoAlertDialog(
          content: widget,
          actions: <Widget>[
            FlatButton(
                // isDefaultAction: true,
                onPressed: () {
                  Navigator.pop(context);
                  leftAction!();
                  provider.onClickNo();
                },
                child: lText(btnLeft, style: st_14(textColor: color_545454))),
            FlatButton(
                // isDefaultAction: true,
                onPressed: () {
                  Navigator.pop(context);
                  rightAction!();
                  provider.onClickYes();
                },
                child: lText(btnRight, style: st_14(textColor: color_ff3b30))),
          ],
        );
      });
}

///사인하기
void showDlgSign(
  BuildContext context, {
  String? content,
  String btnLeft = '',
  String btnRight = '',
  double width = _width,
  double height = _height,
  double contentHeight = 130,
  String keyFile = 'siganture',
  Function? onClickAction,
  Color color = Colors.transparent,
}) {
  Uint8List _signature;
  SignatureController _controller;
  // await Commons.bytesToImage(_signature, "criminal_siganture", "png"));
  _controller = SignatureController(
    penStrokeWidth: 2,
    penColor: Colors.black,
    exportBackgroundColor: Colors.transparent,
  );
  double width = widthFull(context) * 0.9;
  content = content ?? "투약의뢰서_서명하기".tr();
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          contentPadding: padding_0,
          insetPadding: EdgeInsets.all(20),
          content: Container(
              width: width,
              child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.end, children: [
                IconButton(
                    padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                    icon: Lcons.nav_close(size: 30),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                Container(
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
                  color: color,
                  child: Stack(alignment: AlignmentDirectional.center, children: [lText(content!, style: st_16(textColor: color_cecece)), Signature(controller: _controller, height: contentHeight, width: width, backgroundColor: color)]),
                ),
              ])),
          buttonPadding: padding_0,
          actions: [
            lDivider(color: color_dedede),
            IntrinsicHeight(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                      child: Padding(
                    padding: padding_05_TB,
                    child: TextButton(
                        onPressed: () {
                          // onClickAction(DialogAction.cancel);
                          _controller.clear();
                          _signature = Uint8List(0);
                        },
                        child: lText(btnLeft, style: st_b_16())),
                  )),
                  lDividerVertical(color: color_dedede, thickness: 1),
                  Expanded(
                      child: Padding(
                    padding: padding_05_TB,
                    child: TextButton(
                        onPressed: () async {
                          if (_controller.isNotEmpty) {
                            _signature = (await _controller.toPngBytes())!;
                            final imageFile = await Commons.bytesToImage(_signature, '${keyFile}_${StringUtils.formatYMD(DateTime.now())}', "png");
                            onClickAction!(DialogAction.confirm, imageFile, _signature);
                            Navigator.pop(context);
                          }
                        },
                        child: lText(btnRight, style: st_16(textColor: Commons.getColor(), fontWeight: FontWeight.w500))),
                  )),
                ],
              ),
            ),
          ],
        );
      });
}

void showDlgDismissAble(BuildContext context, {Widget? widget, Function? onClickAction, String btnLeft = '', String btnRight = ''}) {
  showDialog(
      context: context,
      barrierDismissible: true, //outside dismiss
      builder: (context) {
        return AlertDialog(
          actions: [
            Divider(thickness: 1),
            IntrinsicHeight(
                child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, mainAxisSize: MainAxisSize.max, children: [
              Expanded(
                  child: TextButton(
                      onPressed: () {
                        onClickAction!(DialogAction.cancel);
                      },
                      child: lText(btnLeft, style: st_b_16()))),
              VerticalDivider(color: color_dedede),
              Expanded(
                  child: TextButton(
                      onPressed: () {
                        onClickAction!(DialogAction.confirm);
                        Navigator.pop(context);
                      },
                      child: lText(btnRight, style: st_16(textColor: color_main)))),
            ]))
          ],
          contentPadding: padding_0,
          content: Container(
              width: 335,
              child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.end, children: [
                IconButton(
                    padding: padding_20,
                    icon: Lcons.nav_close(size: 25),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
                widget!,
              ])),
        );
      });
}

dynamic showNormalDlg({
  BuildContext? context,
  String message = '',
  String btnRight = '',
  Function(DialogAction)? onClickAction,
  Color? color,
  String title = '',
  Widget? titleWidget,
  String? btnLeft,
  TextAlign? msgAlign,
  EdgeInsets? paddingTitle,
  EdgeInsets? padding,
  Alignment? titleAlign,
  Widget? messageWidget,
  bool closeable = false,
  int? msgCode = KEY_SUCCESS,
}) async {
  if (btnRight.isEmpty) btnRight = "확인".tr();
  if (msgCode == KEY_100_NETWORK) btnRight = "설정".tr();
  if (btnRight.contains("닫기".tr())) color = color_b2b2b2;
  showDialog(
      context: context ?? navigatorKey.currentContext!,
      barrierDismissible: closeable,
      barrierColor: color_060606.withOpacity(0.32),
      builder: (context) {
        return AlertDialog(
          contentPadding: padding_0,
          buttonPadding: padding_0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          titlePadding: title.isNotEmpty ? paddingTitle ?? padding_20_T : padding_0,
          title: title.isNotEmpty
              ? Container(
                  alignment: titleAlign ?? Alignment.center,
                  padding: padding_20_LR,
                  child: lText(
                    title,
                    style: st_b_18(fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.clip,
                  ),
                )
              : null,
          content: Container(
            // padding: padding ?? EdgeInsets.fromLTRB(10, 30, 10, 20),
            padding: padding ?? EdgeInsets.fromLTRB(10, 30, 10, 20),
            child: Column(mainAxisAlignment: MainAxisAlignment.center, mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.center, children: [
              messageWidget == null
                  ? Container(
                      alignment: Alignment.bottomCenter,
                      padding: padding_20_LR,
                      child: lText(
                        message,
                        style: st_14(textColor: color_545454),
                        textAlign: msgAlign ?? TextAlign.center,
                        overflow: TextOverflow.clip,
                      ),
                    )
                  : Container(
                      alignment: Alignment.center,
                      padding: padding_20_LR,
                      child: messageWidget,
                    ),
            ]),
          ),
          actions: [
            lDivider(color: color_dedede),
            Container(
              height: 52,
                child: Row(children: [
              btnLeft != null
                  ? Flexible(
                      child: Container(
                      child: CupertinoDialogAction(
                          onPressed: () {
                            Commons.pagePop(context);
                            if (onClickAction != null) onClickAction(DialogAction.no);
                          },
                          child: Padding(
                            padding: padding_03_TB,
                            child: lText(btnLeft, style: st_14(textColor: color ?? color_b2b2b2, fontWeight: FontWeight.w500), textAlign: TextAlign.center),
                          )),
                    ))
                  : Container(),
              Flexible(
                  child: Container(
                decoration: BoxDecoration(border: Border(left: BorderSide(color: btnLeft == null ? Colors.transparent : color_dedede, width: 1))),
                child: CupertinoDialogAction(
                    isDefaultAction: true,
                    onPressed: () {
                      Commons.pagePop(context);
                      if (onClickAction != null) onClickAction(DialogAction.yes);
                    },
                    child: Padding(
                      padding: padding_03_TB,
                      child: lText(btnRight, style: st_14(textColor: color ?? Commons.getColor(), fontWeight: FontWeight.w500), textAlign: TextAlign.center),
                    )),
              )),
            ]))
          ],
        );
      }).then((value) {
    //그냥 화면 닫기시 처리
    if (onClickAction != null && closeable) onClickAction(DialogAction.finish);
  });
}

void showMenuModal(BuildContext context, Map menu, {bool enableCancel = true}) {
  if (menu.isNotEmpty)
  showCupertinoModalPopup(
      context: context,
      barrierColor: Colors.black.withAlpha(82),
      builder: (BuildContext context) => Container(
            padding: padding_20_LR,
            child: CupertinoActionSheet(
                actions: menu.keys
                    .map((e) => Container(
                        color: Colors.white,
                        child: CupertinoActionSheetAction(
                          child: e,
                          onPressed: () {
                            log.d('『GGUMBI』>>> showMenuModal : e: $e, ${menu[e].runtimeType}, ${e.key} <<< ');
                            if (menu[e].runtimeType != Null) {
                              Commons.pagePop(context);
                              menu[e]!();
                            }
                          },
                        )))
                    .toList(),
                cancelButton: enableCancel
                    ? Container(
                        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16)),
                        child: CupertinoActionSheetAction(
                          child: lText("취소".tr(), style: st_b_16(textColor: color_0085ff, fontWeight: FontWeight.w500)),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ))
                    : Container()),
          ));
}

///시스템 에러 표시
showErrorDlg({String? msg, Function? callBack}) {
  msg = msg ?? "시스템에러".tr();
  showNormalDlg(
      message: msg,
      closeable: true,
      btnLeft: "앱재실행".tr(),
      btnRight: "종료하기".tr(),
      onClickAction: (action) {
        switch (action) {
          case DialogAction.no:
            Commons.appReStart();
            if (callBack != null) {
              callBack(action);
            }
            break;
          case DialogAction.yes:
            Commons.appFinish();
            if (callBack != null) {
              callBack(action);
            }
            break;
          default:
        }
      });
}
