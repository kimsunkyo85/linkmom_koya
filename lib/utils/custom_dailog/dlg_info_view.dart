import 'package:flutter/material.dart';
import 'package:linkmom/utils/listview/model/list_model.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

import '../commons.dart';

double maxHeight = 350.0;
double minHeight = 200.0;

///정보 리스트 뷰
Widget dlgInfoView({String title = '', List<SingleItem>? data}) {
  double height = (data!.length * 40.0);
  if (height > maxHeight) {
    height = maxHeight;
  } else if (height < minHeight) {
    height = minHeight;
  }
  return Padding(
    padding: padding_10_LTR,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Column(
          children: [
            lText(
              title,
              style: st_b_20(fontWeight: FontWeight.bold),
            ),
            sb_h_20,
          ],
        ),
        Container(
          height: height, // Change as per your requirement
          width: 300.0, // Change as per
          // padding: padding_10_LTR,
          child: Theme(
            data: ThemeData(
              highlightColor: color_dbdbdb, colorScheme: ColorScheme.fromSwatch().copyWith(secondary: color_white),
            ),
            child: Scrollbar(
              thickness: 3,
              radius: Radius.circular(10),
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: data.length,
                  // physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        lText(data[index].name, style: st_b_16(textColor: Commons.getColor(), fontWeight: FontWeight.bold)),
                        lText(data[index].content, style: st_b_16(textColor: color_545454)),
                        // lText('·${data[index].content}', style: st_b_16(fontWeight: FontWeight.w500)),
                        sb_h_10,
                      ],
                    );
                  }),
            ),
          ),
        ),
      ],
    ),
  );
}