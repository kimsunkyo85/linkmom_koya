import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/view/main/chat/chat_room_page.dart';

import '../../main.dart';
import '../../route_name.dart';
import '../commons.dart';

class LoadingPage extends BaseStateful {
  LoadingPage({this.isReplace = true, this.routeName = '', this.message = '', this.pageData});

  final bool isReplace;
  final String routeName;
  final String message;
  final dynamic pageData;

  @override
  _LoadingPageState createState() => _LoadingPageState(isReplace: isReplace, routeName: routeName, message: message, pageData: pageData);
}

class _LoadingPageState extends BaseStatefulState<LoadingPage> {
  _LoadingPageState({this.isReplace = true, this.routeName = '', this.message = '', this.pageData});

  final bool isReplace;
  final String routeName;
  String message;
  dynamic pageData;

  @override
  void initState() {
    super.initState();
    if (!StringUtils.validateString(message)) {
      message = "채팅방 이동중입니다...";
    }
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 1)).then((value) {
      log.d('『GGUMBI』>>> build : routeName : $routeName, ${routeName == routeChatRoom}, isReplace: $isReplace, message: $message, pageData: $pageData <<< ');
      if (routeName == routeChatRoom) {
        ChatRoomPage _data = pageData as ChatRoomPage;
        log.d('『GGUMBI』>>> build : routeName 1 : $routeName, ${routeName == routeChatRoom}, isReplace: $isReplace, message: $message, pageData: $pageData <<< ');
        Commons.pageReplace(context, routeName,
            arguments: ChatRoomPage(
              roomType: _data.roomType,
              receiver: _data.receiver,
              bookingcareservices: _data.bookingcareservices,
              chatroom_id: _data.chatroom_id,
            ));
        /* Commons.pageToClear(context, routeName, routeHome,
            arguments: ChatRoomPage(
              roomType: _data.roomType,
              receiver: _data.receiver,
              bookingcareservices: _data.bookingcareservices,
              chatroom_id: _data.chatroom_id,
            ));*/
        // Commons.pagePop(context);
        // Commons.page(context, routeName,
        //     arguments: ChatRoomPage(
        //       roomType: _data.roomType,
        //       receiver: _data.receiver,
        //       bookingcareservices: _data.bookingcareservices,
        //       chatroom_id: _data.chatroom_id,
        //     ));
      } else {
        log.d('『GGUMBI』>>> build : routeName 2 : $routeName, ${routeName == routeChatRoom}, isReplace: $isReplace, message: $message, pageData: $pageData <<< ');
        Commons.page(context, routeName, arguments: pageData);
        // Commons.pagePop(context);
        // Commons.pageToClear(context, routeName, routeHome, arguments: pageData);
      }
    });
    return Material(
      child: lContainer(
        height: heightFull(context),
        width: widthFull(context),
        alignment: Alignment.center,
        child: lLoadingIndicator(message: message),
      ),
    );
  }
}
