
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/base/base_stateful.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

class NoticeModal extends BaseStateful {
  final double width;
  final double height;
  final Color backgroundColor;
  final Widget? body;
  final String? message;
  final Widget? icon;
  final bool useIcon;

  NoticeModal({this.width = double.infinity, this.height = 68.0, this.backgroundColor = Colors.transparent, this.message = '', this.body, this.icon, this.useIcon = false});

  @override
  _NoticeModalState createState() => _NoticeModalState();
}

class _NoticeModalState extends BaseStatefulState<NoticeModal> {
  bool _showModal = true;
  @override
  Widget build(BuildContext context) {
    return _showModal
        ? Container(
            width: widget.width,
            height: widget.height,
            color: widget.backgroundColor,
            padding: padding_20_LR,
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.center, children: [
              widget.body ??
                  Flexible(
                      child: Row(
                    children: [
                      if (widget.useIcon)
                        widget.icon ??
                            Container(margin: padding_10_R, padding: padding_05, decoration: BoxDecoration(color: Colors.white.withAlpha(108), borderRadius: BorderRadius.circular(56)), child: Lcons.diary_like(size: 30, color: Colors.white)),
                      if (widget.message!.isNotEmpty) Flexible(child: lText(widget.message ?? ' ', style: st_14(), overflow: TextOverflow.visible)),
                    ],
                  )),
              lInkWell(
                  onTap: () {
                    _showModal = !_showModal;
                    onUpDate();
                  },
                  child: Container(
                    padding: padding_10_T,
                    child: Lcons.nav_close(size: 25, color: Colors.white, isEnabled: true),
                  ))
            ]),
          )
        : Container();
  }
}

class ReleativeModal extends StatefulWidget {
  final Size? size;
  final Key? key;
  final Offset? offset;
  final String message;
  final TextStyle? messageStyle;
  final String title;
  final TextStyle? titleStyle;
  final Widget? bodyWidget;
  final bool showClose;
  final Widget? icon;
  final Function? onClickAction;
  final Widget? background;
  final bool showPin;
  final Color color;
  ReleativeModal({
    this.key,
    this.size,
    this.offset,
    this.message = '',
    this.messageStyle,
    this.title = '',
    this.titleStyle,
    this.bodyWidget,
    this.showClose = true,
    this.icon,
    this.onClickAction,
    this.background,
    this.showPin = true,
    this.color = color_main,
  });

  @override
  _ReleativeModalState createState() => _ReleativeModalState();
}

class _ReleativeModalState extends State<ReleativeModal> {
  @override
  Widget build(BuildContext context) {
    return bubbleModal(
        key: widget.key,
        padding: padding_15,
        showPin: widget.showPin,
        color: widget.color,
        width: widget.size != null ? widget.size!.width : null,
        height: widget.size != null ? widget.size!.height : null,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
          if (widget.title.isNotEmpty)
            Container(
              padding: padding_10_LTR,
              child: Row(
                children: [
                  if (widget.icon != null)
                    Padding(
                      padding: padding_05_R,
                      child: widget.icon!,
                    ),
                  lText(widget.title, style: widget.titleStyle ?? st_b_16(fontWeight: FontWeight.w700, textColor: color_545454)),
                ],
              ),
            ),
          if (widget.message.isNotEmpty) sb_h_06,
          if (widget.message.isNotEmpty) lText(widget.message, style: widget.messageStyle ?? st_b_14(), overflow: TextOverflow.visible),
          if (widget.bodyWidget != null) widget.bodyWidget!,
          if (widget.showClose) lDivider(padding: padding_13_TB, color: color_eeeeee),
          if (widget.showClose)
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                lInkWell(
                  onTap: () {
                    setState(() {
                      if (widget.onClickAction != null) widget.onClickAction!();
                    });
                  },
                  child: Container(padding: padding_05, child: lText("네이해했어요".tr(), style: st_16(textColor: widget.color))),
                ),
              ],
            ),
        ]));
  }
}
