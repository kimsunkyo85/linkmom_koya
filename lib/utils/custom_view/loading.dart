import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/lcons.dart';

///기본 스타입 st_14(fontWeight: FontWeight.w500)
class LoadingIndicator extends StatefulWidget {
  final bool showText;
  final String? message;
  final TextStyle? style;
  final double size;
  final Color background;
  final Alignment alignment;

  LoadingIndicator({Key? key, this.message, this.style, this.showText = true, this.size = 54, this.background = Colors.black38, this.alignment = Alignment.center}) : super(key: key);

  @override
  _LoadingIndicatorState createState() => _LoadingIndicatorState();
}

class _LoadingIndicatorState extends State<LoadingIndicator> with TickerProviderStateMixin {
  List<Widget> _icons = [];

  int _index = 0;
  late Timer _timer;
  final Duration _duration = Duration(milliseconds: 150);
  double _size = 40;

  @override
  void initState() {
    super.initState();
    _size = widget.size;
    _timer = Timer.periodic(_duration, (t) {
      if (this.mounted) setState(() => _index++);
    });
    _icons = [
      Lcons.housework(size: _size, color: Colors.white, isEnabled: true),
      Lcons.care(size: _size, color: Colors.white, isEnabled: true),
      Lcons.play(size: _size, color: Colors.white, isEnabled: true),
      Lcons.go_home(size: _size, color: Colors.white, isEnabled: true),
      Lcons.neighbor_house(size: _size, color: Colors.white, isEnabled: true),
      Lcons.art(size: _size, color: Colors.white, isEnabled: true),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.background,
      alignment: widget.alignment,
      child: Wrap(
        direction: Axis.vertical,
        alignment: WrapAlignment.center,
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [
          Container(
            width: widget.size + 10,
            height: widget.size + 10,
            padding: padding_10,
            margin: padding_20,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Commons.getColor(),
            ),
            child: AnimatedSwitcher(
              duration: _duration,
              layoutBuilder: (currentChild, previousChildren) => currentChild!,
              switchInCurve: Curves.linear,
              switchOutCurve: Curves.linear,
              child: _icons[_index % _icons.length],
            ),
          ),
          if (widget.showText) Material(type: MaterialType.transparency, child: lText(widget.message ?? "잠시만요...".tr(), style: widget.style ?? st_14(fontWeight: FontWeight.w500))),
        ],
      ),
    );
  }

  void stopAnimate() => _timer.cancel();
}
