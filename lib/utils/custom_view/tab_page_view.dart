import 'package:flutter/material.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/utils/view_util.dart';

class TabPageView extends StatefulWidget {
  TabPageView({
    this.tabbarName,
    this.tabbarTitle,
    required this.tabbarView,
    this.indicatorColor,
    this.tabbarStyle,
    this.focusedTabbarStyle,
    this.emptyView,
    this.topper,
    this.footer,
    this.bgColor = Colors.white,
    this.floatingActionButton,
    this.hideBgColor = false,
    this.onCreateView,
    this.useOtherTabbar = false,
    this.disableExpanded = false,
    this.tabbarPadding = padding_0,
    this.tabBgColor = Colors.white,
    this.isScrollable = true,
  }) {
    if (tabbarName != null) {
      assert(this.tabbarName!.length == this.tabbarView.length);
    } else {
      assert(this.tabbarTitle!.length == this.tabbarView.length);
    }
    assert(!(this.tabbarName != null && this.tabbarTitle != null));
  }

  final TextStyle? focusedTabbarStyle;
  final Color? indicatorColor;
  final List<TabItem>? tabbarName;
  final TextStyle? tabbarStyle;
  final List<Widget> tabbarView;
  final List<Widget>? tabbarTitle;
  final Widget? emptyView;
  final Widget? topper;
  final Widget? footer;
  final Color? bgColor;
  final Color? tabBgColor;
  final FloatingActionButton? floatingActionButton;
  final bool hideBgColor;
  final Function(TabController)? onCreateView;
  final bool useOtherTabbar;
  final bool disableExpanded;
  final EdgeInsets tabbarPadding;
  final bool? isScrollable;

  @override
  TabPageViewState createState() => TabPageViewState();
}

class TabPageViewState<T extends TabPageView> extends State<T> with TickerProviderStateMixin {
  late TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: widget.tabbarTitle != null ? widget.tabbarTitle!.length : widget.tabbarName!.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.onCreateView != null) {
      widget.onCreateView!(this._controller);
    }
    return lScaffold(
      backgroundColor: widget.hideBgColor ? Colors.transparent : Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        shadowColor: Colors.transparent,
        backgroundColor: Colors.white,
        flexibleSpace: widget.tabbarTitle == null
            ? widget.useOtherTabbar
                ? VerticalDecoTabBar(
                    tabCount: widget.tabbarName!.length,
                    bgColor: widget.bgColor ?? Colors.white,
                    tabBar: TabBar(
                        padding: widget.tabbarPadding,
                        controller: _controller,
                        onTap: (index) => focusClear(context),
                        tabs: widget.tabbarName!.map((e) => Tab(text: e.title)).toList(),
                        labelStyle: widget.focusedTabbarStyle ?? st_14(fontWeight: FontWeight.w700),
                        unselectedLabelStyle: widget.tabbarStyle ?? st_14(fontWeight: FontWeight.w500),
                        labelColor: widget.focusedTabbarStyle == null ? Commons.getColor() : widget.focusedTabbarStyle!.color,
                        unselectedLabelColor: widget.tabbarStyle == null ? color_cecece : widget.tabbarStyle!.color,
                        indicatorColor: Colors.transparent))
                : DecoratedTabBar(
                    tabBar: TabBar(
                        controller: _controller,
                        onTap: (index) => focusClear(context),
                        tabs: widget.tabbarName!
                            .map((e) => Tab(
                                  text: e.title,
                                ))
                            .toList(),
                        labelStyle: widget.focusedTabbarStyle ?? st_b_16(fontWeight: FontWeight.w500),
                        unselectedLabelStyle: widget.tabbarStyle ?? st_16(fontWeight: FontWeight.w500),
                        labelColor: widget.focusedTabbarStyle == null ? st_b_16().color : widget.focusedTabbarStyle!.color,
                        unselectedLabelColor: widget.tabbarStyle == null ? color_b2b2b2 : widget.tabbarStyle!.color,
                        indicatorColor: widget.indicatorColor ?? Commons.getColor()),
                    decoration: BoxDecoration(
                      color: widget.tabBgColor,
                      border: Border(
                        bottom: BorderSide(
                          color: color_dedede,
                          width: 1.0,
                        ),
                      ),
                    ))
            : DefaultTabBar(
                tabBar: TabBar(
                  controller: _controller,
                  onTap: (index) => focusClear(context),
                  tabs: widget.tabbarTitle!.map((e) => SizedBox(height: 70, child: Tab(child: e))).toList(),
                  indicatorColor: widget.indicatorColor ?? Commons.getColor(),
                ),
                decoration: BoxDecoration(border: Border(bottom: BorderSide(color: color_dedede, width: 1.0)))),
      ),
      body: Container(
          color: widget.bgColor,
          child: Column(mainAxisSize: MainAxisSize.max, crossAxisAlignment: CrossAxisAlignment.stretch, mainAxisAlignment: MainAxisAlignment.start, children: [
            if (widget.topper != null) Flexible(flex: 1, fit: FlexFit.loose, child: widget.topper!),
            widget.disableExpanded
                ? Container(height: 80, child: TabBarView(controller: _controller, children: widget.tabbarView.isEmpty ? [widget.emptyView ?? lEmptyView(height: Commons.height(context, 0.5))] : widget.tabbarView.map((e) => e).toList()))
                : Expanded(flex: 15, child: TabBarView(controller: _controller, children: widget.tabbarView.isEmpty ? [widget.emptyView ?? lEmptyView(height: Commons.height(context, 0.5))] : widget.tabbarView.map((e) => e).toList())),
            if (widget.footer != null) Flexible(flex: 1, fit: FlexFit.loose, child: widget.footer!)
          ])),
      floatingActionButton: widget.floatingActionButton ?? Container(),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }

  TabController get getController => _controller;
}

class DecoratedTabBar extends StatelessWidget implements PreferredSizeWidget {
  DecoratedTabBar({required this.tabBar, required this.decoration});

  final TabBar tabBar;
  final BoxDecoration decoration;

  @override
  Size get preferredSize => tabBar.preferredSize;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      fit: StackFit.expand,
      children: [
        Positioned.fill(child: Container(decoration: decoration)),
        PreferredSize(preferredSize: Size.fromHeight(kToolbarHeight * 1.5), child: tabBar),
      ],
    );
  }
}

class VerticalDecoTabBar extends StatelessWidget implements PreferredSizeWidget {
  VerticalDecoTabBar({required this.tabBar, this.tabCount = 2, this.bgColor = Colors.white});

  final TabBar tabBar;
  final int tabCount;
  final Color bgColor;

  @override
  Size get preferredSize => tabBar.preferredSize;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
            color: bgColor,
            alignment: Alignment.center,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: List.filled(tabCount - 1, Container(height: kToolbarHeight / 3, child: lDividerVertical(thickness: 1))),
            )),
        PreferredSize(preferredSize: Size.fromHeight(kToolbarHeight), child: tabBar),
      ],
    );
  }
}

class DefaultTabBar extends StatelessWidget implements PreferredSizeWidget {
  DefaultTabBar({required this.tabBar, required this.decoration});

  final TabBar tabBar;
  final BoxDecoration decoration;

  @override
  Size get preferredSize => tabBar.preferredSize;

  @override
  Widget build(BuildContext context) {
    return AppBar(shadowColor: Colors.transparent, backgroundColor: Colors.transparent, automaticallyImplyLeading: false, toolbarHeight: kToolbarHeight, flexibleSpace: DecoratedTabBar(tabBar: tabBar, decoration: decoration));
  }
}

class SlideTabPageView extends TabPageView {
  SlideTabPageView({
    TextStyle? focusedTabbarStyle,
    Color? indicatorColor,
    List<TabItem>? tabbarName,
    TextStyle? tabbarStyle,
    required List<Widget> tabbarView,
    List<Widget>? tabbarTitle,
    Widget? emptyView,
    Widget? topper,
    Widget? footer,
    Color? bgColor,
    FloatingActionButton? floatingActionButton,
    bool hideBgColor = false,
    Function(TabController)? onCreateView,
    bool useOtherTabbar = false,
    bool? isScrollable = true,
  }) : super(
          focusedTabbarStyle: focusedTabbarStyle,
          indicatorColor: indicatorColor,
          tabbarName: tabbarName,
          tabbarStyle: tabbarStyle,
          tabbarView: tabbarView,
          tabbarTitle: tabbarTitle,
          emptyView: emptyView,
          topper: topper,
          footer: footer,
          bgColor: bgColor,
          floatingActionButton: floatingActionButton,
          hideBgColor: hideBgColor,
          onCreateView: onCreateView,
          useOtherTabbar: useOtherTabbar,
          isScrollable: isScrollable,
        );

  @override
  _SlideTabPageViewState createState() => _SlideTabPageViewState();
}

class _SlideTabPageViewState extends TabPageViewState<SlideTabPageView> {
  @override
  Widget build(BuildContext context) {
    if (widget.onCreateView != null) {
      widget.onCreateView!(this._controller);
    }
    return lScaffold(
      backgroundColor: widget.hideBgColor ? Colors.transparent : Colors.white,
      appBar: widget.tabbarTitle == null
          ? DecoratedTabBar(
              tabBar: TabBar(
                  isScrollable: widget.isScrollable!,
                  controller: _controller,
                  tabs: widget.tabbarName!.map((e) => badgeView(e.title, e.badge, alignment: Alignment.topLeft, top: 5, end: 2, padding: padding_04)).toList(),
                  labelStyle: widget.focusedTabbarStyle ?? st_b_16(fontWeight: FontWeight.w500),
                  unselectedLabelStyle: widget.tabbarStyle ?? st_16(fontWeight: FontWeight.w500),
                  labelColor: widget.focusedTabbarStyle == null ? st_b_16().color : widget.focusedTabbarStyle!.color,
                  unselectedLabelColor: widget.tabbarStyle == null ? color_b2b2b2 : widget.tabbarStyle!.color,
                  indicatorColor: widget.indicatorColor ?? Commons.getColor()),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: color_dedede,
                    width: 1.0,
                  ),
                ),
              ))
          : AppBar(
              shadowColor: Colors.transparent,
              backgroundColor: Colors.transparent,
              automaticallyImplyLeading: false,
              toolbarHeight: appTabHeight(),
              flexibleSpace: DecoratedTabBar(
                  tabBar: TabBar(
                    isScrollable: widget.isScrollable!,
                    controller: _controller,
                    tabs: widget.tabbarTitle!
                        .map((e) => SizedBox(
                              height: appTabHeight(),
                              child: Tab(child: e),
                            ))
                        .toList(),
                    indicatorColor: widget.indicatorColor ?? Commons.getColor(),
                  ),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: color_dedede,
                        width: 1.0,
                      ),
                    ),
                  ))),
      body: Container(
          color: widget.bgColor,
          child: Column(mainAxisSize: MainAxisSize.max, crossAxisAlignment: CrossAxisAlignment.stretch, mainAxisAlignment: MainAxisAlignment.start, children: [
            if (widget.topper != null) Flexible(flex: 1, fit: FlexFit.loose, child: widget.topper!),
            Expanded(child: TabBarView(controller: _controller, children: widget.tabbarView.isEmpty ? [widget.emptyView ?? lEmptyView(height: Commons.height(context, 0.5))] : widget.tabbarView.map((e) => e).toList())),
            if (widget.footer != null) Flexible(flex: 1, fit: FlexFit.loose, child: widget.footer!)
          ])),
      floatingActionButton: widget.floatingActionButton ?? Container(),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}

class TabItem {
  final String title;
  final int badge;

  TabItem({this.title = '', this.badge = 0});
}
