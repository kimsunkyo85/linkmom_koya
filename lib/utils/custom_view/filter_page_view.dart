import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:jiffy/jiffy.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';
import 'package:linkmom/view/calendar/calendar_selecter.dart';
import 'package:linkmom/view/lcons.dart';

class FilterPageView extends StatefulWidget {
  final DataManager? data;
  final List<int> rangeType;
  final List<String> rangeText;
  final List<FilterOption> filterOptions;
  final ListType listType;
  final FilterSelect filterSelect;
  final Function searchAction;
  final DateTime? endDate;
  final DateTime? first;
  final DateTime? last;

  FilterPageView({this.data, required this.filterOptions, required this.listType, required this.rangeType, required this.rangeText, required this.filterSelect, required this.searchAction, this.endDate, this.first, this.last});

  @override
  FilterPageViewState createState() => FilterPageViewState();
}

class FilterPageViewState<T extends FilterPageView> extends State<T> {
  DateTime _first = Jiffy(DateTime.now()).startOf(Units.MONTH).dateTime;
  DateTime _last = Jiffy(DateTime.now()).endOf(Units.MONTH).dateTime;

  late DateTimeRange _range;
  bool _searchMatching = true;
  bool _searchMatched = true;
  int _selected = -1;
  bool _isConfirm = false;

  @override
  void initState() {
    super.initState();
    _first = widget.first ?? _first;
    _last = widget.last ?? _last;
    if (widget.filterSelect.range == null) {
      _range = DateTimeRange(start: _first, end: _last);
    } else {
      _first = widget.filterSelect.range!.start;
      _last = widget.filterSelect.range!.end;
      _range = DateTimeRange(start: _first, end: _last);
      _selected = widget.filterSelect.index;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: lScrollView(
            padding: padding_0,
            child: Column(
              children: [
                Container(
                    padding: padding_20,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        lText("조회기간".tr(), style: st_b_18(fontWeight: FontWeight.w700)),
                        Container(
                          child: AspectRatio(
                            aspectRatio: widget.rangeType.length > 3 ? 318 / 110 : 318 / 55,
                            child: GridView.builder(
                                shrinkWrap: true,
                                padding: padding_10_TB,
                                physics: NeverScrollableScrollPhysics(),
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  childAspectRatio: 106 / 45,
                                ),
                                itemCount: widget.rangeType.length,
                                itemBuilder: (context, index) {
                                  return AspectRatio(
                                    aspectRatio: 106 / 35,
                                    child: lInkWell(
                                      onTap: () {
                                        setState(() {
                                          if (widget.rangeType[index] < 0) {
                                            _range = DateTimeRange(start: DateTime.now().add(Duration(days: 30 * widget.rangeType[index])), end: DateTime.now());
                                          } else {
                                            _range = DateTimeRange(start: DateTime.now(), end: DateTime.now().add(Duration(days: 30 * widget.rangeType[index])));
                                          }
                                          _selected = index;
                                        });
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(border: Border.all(color: _selected == index ? Commons.getColor() : color_dedede, width: _selected == index ? 1.8 : 1), borderRadius: BorderRadius.circular(26)),
                                        padding: padding_20_LR,
                                        margin: padding_04,
                                        alignment: Alignment.center,
                                        child: lAutoSizeText(
                                          widget.rangeText.isNotEmpty ? widget.rangeText[index] : "${widget.rangeType}${"개월".tr()}",
                                          style: st_b_14(textColor: _selected == index ? Commons.getColor() : color_999999, fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                          ),
                        ),
                        Container(
                            padding: padding_10_TB,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                lInkWell(
                                    onTap: () {
                                      setState(() {
                                        List<DateTime> range = [];
                                        showNormalDlg(
                                            context: context,
                                            title: "날짜선택".tr(),
                                            messageWidget: Container(
                                                width: 300,
                                                height: 330,
                                                child: DateRangeSelecter(
                                                  color: [Commons.getColor().withAlpha(102), Commons.getColor().withAlpha(51)],
                                                  selectCallback: (date, events) {
                                                    setState(() {
                                                      if (range.isNotEmpty && range.contains(date)) {
                                                        range.remove(date);
                                                      } else {
                                                        range.add(date);
                                                      }
                                                      range.sort();
                                                      if (range.isNotEmpty && (range.last.difference(range.first).inDays).abs() > 90) {
                                                        Commons.showToast("최대3개월단위로조회".tr(), toastLength: Toast.LENGTH_LONG, gravity: ToastGravity.TOP);
                                                        _isConfirm = false;
                                                        return;
                                                      }
                                                      _isConfirm = true;
                                                      _range = DateTimeRange(start: _first, end: _last);
                                                      _selected = -1;
                                                    });
                                                  },
                                                  mode: Selecter.view,
                                                  endDate: widget.endDate,
                                                )),
                                            btnRight: "선택완료".tr(),
                                            padding: padding_0,
                                            onClickAction: (a) {
                                              if (!_isConfirm) {
                                                showNormalDlg(message: "최대3개월단위로조회".tr());
                                                return;
                                              }
                                              range.sort();
                                              setState(() {
                                                _range = DateTimeRange(start: range.first, end: range.last);
                                              });
                                            });
                                      });
                                    },
                                    child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                                      Container(padding: padding_10_R, child: Lcons.calendar()),
                                      Container(child: lText('${StringUtils.parseYMDE(_range.start)} - ${StringUtils.parseYMDE(_range.end)}', style: st_15(textColor: Commons.getColor(), fontWeight: FontWeight.w500), overflow: TextOverflow.ellipsis)),
                                    ])),
                                Container(padding: padding_10_TB, child: lDivider()),
                                lText("최대3개월단위로조회가능".tr(), style: st_12(textColor: color_545454))
                              ],
                            )),
                      ],
                    )),
                Column(children: widget.filterOptions.map((e) => FilterList(title: e.type, info: e.info)).toList()),
              ],
            ),
          ),
        ),
        lBtn("조회하기".tr(), isEnabled: (_searchMatched || _searchMatching), onClickAction: () {
          widget.filterSelect.range = _range;
          widget.filterSelect.index = _selected;
          widget.searchAction(widget.filterSelect, widget.filterOptions);
        }),
      ],
    );
  }
}

class FilterSelect {
  int index;
  DateTimeRange? range;

  FilterSelect({
    this.index = -1,
    this.range,
  });

  @override
  String toString() {
    return 'FilterSelect{index: $index, range: $range}';
  }
}

class FilterOption {
  final FilterType type;
  final List<FilterInfo> info;

  FilterOption({required this.type, required this.info});
}

class FilterInfo {
  final int value;
  final String title;
  final String desc;
  bool checked = true;

  FilterInfo({this.value = 0, this.title = '', this.desc = ''});
}

class FilterList extends StatefulWidget {
  List<FilterInfo> info;
  final FilterType title;

  FilterList({required this.title, required this.info});

  @override
  _FilterListState createState() => _FilterListState();
}

class _FilterListState extends State<FilterList> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(border: Border(top: BorderSide(width: 8, color: color_fafafa))),
        padding: padding_20,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          lText(widget.title.name, style: st_b_18(fontWeight: FontWeight.w700)),
          sb_h_10,
          AspectRatio(
            aspectRatio: 190 / ((widget.info.length / 2) * 22),
            child: Container(
              padding: padding_20_R,
              child: widget.info.length < 4
                  ? Row(
                      mainAxisAlignment: widget.info.length < 3 ? MainAxisAlignment.start : MainAxisAlignment.spaceBetween,
                      children: widget.info
                          .map((e) => lInkWell(
                                onTap: () {
                                  setState(() {
                                    e.checked = !e.checked;
                                    if (widget.info.where((e) => e.checked).isEmpty) {
                                      showNormalDlg(
                                          context: context,
                                          message: "최소한개는선택되어야합니다".tr(),
                                          onClickAction: (action) {
                                            setState(() {
                                              e.checked = !e.checked;
                                            });
                                          });
                                    }
                                  });
                                },
                                child: Container(
                                  margin: widget.info.length < 3
                                      ? padding_30_R
                                      : e == widget.info.last
                                          ? padding_30_R
                                          : padding_0,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Lcons.check_circle(color: Commons.getColor(), disableColor: color_dedede, isEnabled: e.checked, size: 25),
                                      sb_w_05,
                                      lAutoSizeText(e.title, style: st_b_16(), overflow: TextOverflow.ellipsis),
                                      sb_w_05,
                                    ],
                                  ),
                                ),
                              ))
                          .toList())
                  : GridView.builder(
                      padding: padding_20_B,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 70 / 15,
                      ),
                      itemCount: widget.info.length,
                      itemBuilder: (context, index) {
                        return lInkWell(
                          onTap: () {
                            setState(() {
                              widget.info[index].checked = !widget.info[index].checked;
                              if (widget.info.where((e) => e.checked).isEmpty) {
                                showNormalDlg(
                                    context: context,
                                    message: "최소한개는선택되어야합니다".tr(),
                                    onClickAction: (action) {
                                      setState(() => widget.info[index].checked = !widget.info[index].checked);
                                    });
                              }
                            });
                          },
                          child: Container(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Lcons.check_circle(color: Commons.getColor(), disableColor: color_dedede, isEnabled: widget.info[index].checked, size: 25),
                              sb_w_05,
                              lAutoSizeText(widget.info[index].title, style: st_b_16()),
                            ],
                          )),
                        );
                      }),
            ),
          ),
          sb_h_10,
          //2021/11/30 구인상태, 돌봄유형 오른족 넓이와 320 사이즈보다 작아서 오류사ㅓ 넓이를 지정해줌 Z Flip 등등...
          Column(children: widget.info.where((d) => d.desc.isNotEmpty).map((e) => lDingbatText(text: e.desc, dingbatSize: 2, dingbatColor: color_545454, style: st_12(textColor: color_545454), width: widthFull(context) * 0.8)).toList())
        ]));
  }
}
