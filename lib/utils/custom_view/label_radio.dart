import 'package:flutter/material.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';

import '../commons.dart';

class LabeledRadio<T> extends StatelessWidget {
  const LabeledRadio({
    Key? key,
    required this.label,
    required this.padding,
    required this.groupValue,
    required this.value,
    this.isLine = false,
    this.materialTapTargetSize = MaterialTapTargetSize.shrinkWrap,
    required this.onChanged,
    this.style,
    this.radioColor = color_main,
    this.toggleable = true,
  }) : super(key: key);

  final String label;
  final EdgeInsets padding;
  final T groupValue;
  final T value;
  final MaterialTapTargetSize materialTapTargetSize;
  final isLine;
  final Function onChanged;
  final TextStyle? style;
  final Color? radioColor;
  final bool toggleable;

  @override
  Widget build(BuildContext context) {
    Color color = radioColor!;
    if (radioColor == null) {
      color = Commons.getColor();
    }
    return InkWell(
      onTap: () {
        if (value != groupValue) {
          onChanged(value);
        }
      },
      child: Column(
        children: [
          Padding(
            padding: padding,
            child: Row(
              children: [
                Radio<T>(
                  toggleable: toggleable,
                  groupValue: toggleable ? groupValue : null,
                  value: value,
                  visualDensity: VisualDensity.comfortable,
                  materialTapTargetSize: materialTapTargetSize,
                  activeColor: color,
                  fillColor: toggleable ? null : MaterialStateProperty.all(color_dedede),
                  onChanged: (T? newValue) {
                    onChanged(newValue);
                  },
                ),
                lText(label, style: style),
              ],
            ),
          ),
          if (isLine) lDivider(color: color_eeeeee, padding: padding_10_LR),
        ],
      ),
    );
  }
}
