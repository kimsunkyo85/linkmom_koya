import 'package:flutter/material.dart';

/* --------------------------------- ICONS --------------------------------- */

const String _path = 'assets/icons/';

const BANK_WOORI = '${_path}bank_woori.png';
const BANK_TOSS = '${_path}bank_toss.png';
const BANK_SHINHYOP = '${_path}bank_shinhyop.png';
const BANK_SHINHAN = '${_path}bank_shinhan.png';
const BANK_SH = '${_path}bank_sh.png';
const BANK_SC = '${_path}bank_sc.png';
const BANK_NH = '${_path}bank_nh.png';
const BANK_MG = '${_path}bank_mg.png';
const BANK_KYONGNAM = '${_path}bank_kyongnam.png';
const BANK_KWANGJU = '${_path}bank_kwangju.png';
const BANK_KOPO = '${_path}bank_kopo.png';
const BANK_KDB = '${_path}bank_kdb.png';
const BANK_KB = '${_path}bank_kb.png';
const BANK_KAKAO = '${_path}bank_kakao.png';
const BANK_K = '${_path}bank_k.png';
const BANK_JEJU = '${_path}bank_shinhan.png';
const BANK_JB = '${_path}bank_jb.png';
const BANK_IBK = '${_path}bank_ibk.png';
const BANK_HANA = '${_path}bank_hana.png';
const BANK_DGB = '${_path}bank_dgb.png';
const BANK_CITI = '${_path}bank_citi.png';
const BANK_BUSAN = '${_path}bank_busan.png';
const BANK_BOA = '${_path}bank_BOA.png';
const BANK_DEUTSCHE = '${_path}bank_deutsche.png';
const BANK_HSBC = '${_path}bank_HSBC.png';
const BANK_MORGAN = '${_path}bank_morgan.png';
const BANK_UFJ = '${_path}bank_UFJ.png';
const BANK_SAVE = '${_path}bank_korea_federation.png';
const STOCK_SK = '${_path}stockfirm_sk.png';
const STOCK_DB = '${_path}stockfirm_db.png';
const STOCK_DGB = '${_path}stockfirm_dgb.png';
const STOCK_HANA = '${_path}stockfirm_hana.png';
const STOCK_HANHWA = '${_path}stockfirm_hanwha.png';
const STOCK_KOR = '${_path}stockfirm_koreainvestment.png';
const STOCK_KYOBO = '${_path}stockfirm_kyobo.png';
const STOCK_MIRAE = '${_path}stockfirm_mirae.png';
const STOCK_NH = '${_path}stockfirm_nh.png';
const STOCK_SAMSUNG = '${_path}stockfirm_samsung.png';
const STOCK_SHINHAN = '${_path}stockfirm_shinhan.png';
const STOCK_DAISHIN = '${_path}stockfirm_daishin.png';
const STOCK_DONGYANG = '${_path}stockfirm_dongyang.png';
const STOCK_HMC = '${_path}stockfirm_hmc.png';
const STOCK_HYUNDAI = '${_path}stockfirm_hyundai.png';
const STOCK_KDB = '${_path}stockfirm_kdb.png';
const STOCK_MERITZ = '${_path}stockfirm_meritz.png';
const STOCK_MIZUHO = '${_path}stockfirm_mizuho.png';
const STOCK_SHINYOUNG = '${_path}stockfirm_shinyoung.png';
const STOCK_YOOJIN = '${_path}stockfirm_yoojin.png';

const KAKAO_CHAT = '${_path}icon_kakao_chat.png';
const BELL = '${_path}bell.png';

const IMG_LOGO = 'assets/images/logo.png';
// const IMG_SPLASH_BG = 'assets/images/splash_progress_bg.png';
const IMG_AUTH_EXAMPLE_EDUCATE = 'assets/images/auth_education.png';
const IMG_AUTH_EXAMPLE_DEUNGBON = 'assets/images/auth_deungbon.png';
const IMG_AUTH_EXAMPLE_GRADUATION = 'assets/images/auth_graduation.png';
const IMG_AUTH_EXAMPLE_CERTIFICATION = 'assets/images/auth_certification.png';
const IMG_AUTH_EXAMPLE_HEALTH = 'assets/images/auth_health.png';
const IMG_RANK_1 = 'assets/icons/rank_1.png';
const IMG_RANK_2 = 'assets/icons/rank_2.png';
const IMG_RANK_3 = 'assets/icons/rank_3.png';
const IMG_PROFILE_LINKMOM = 'assets/images/profile_linkmom.png';
const IMG_PROFILE_MOMDADY = 'assets/images/profile_momdady.png';
const IMG_PERSON_SMALL_COMMUNITY = 'assets/images/person_small_community.png';
const IMG_PERSON_SMALL_LINKMOM = 'assets/images/person_small_linkmom.png';
const IMG_PERSON_SMALL_MOMDADY = 'assets/images/person_small_momdady.png';
const IMG_KAKAO = 'assets/images/kakao_icon.png';

const IMG_INTRO_AUTH = 'assets/images/intro_auth.png';
const IMG_INTRO_BG_HOUSE = 'assets/images/intro_bg_house.png';
const IMG_INTRO_SCREEN_L = 'assets/images/intro_l.png';
const IMG_INTRO_LIST_L = 'assets/images/intro_list_l.png';
const IMG_INTRO_LIST_M = 'assets/images/intro_list_m.png';
const IMG_INTRO_LOCATE = 'assets/images/intro_locate.png';
const IMG_INTRO_SCREEN_M = 'assets/images/intro_m.png';
const IMG_INTRO_NOTI = 'assets/images/intro_noti.png';
const IMG_INTRO_PROFILE_L = 'assets/images/intro_profile_l.png';
const IMG_INTRO_PROFILE_M = 'assets/images/intro_profile_m.png';
const IMG_DRUG_LINKMOM = 'assets/images/drug_linkmom.png';
const IMG_DRUG_MOMDADY = 'assets/images/drug_momdady.png';
const IMG_GUIDE_LINKMOM_CARE = 'assets/images/guide_linkmom_care.png';
const IMG_GUIDE_LINKMOM_DIARY_1 = 'assets/images/guide_linkmom_diary_1.png';
const IMG_GUIDE_LINKMOM_DIARY_2 = 'assets/images/guide_linkmom_diary_2.png';
const IMG_GUIDE_LINKMOM_DIARY_3 = 'assets/images/guide_linkmom_diary_3.png';
const IMG_GUIDE_LINKMOM_DIARY_4 = 'assets/images/guide_linkmom_diary_4.png';
const IMG_GUIDE_LINKMOM_MATCHING_1 = 'assets/images/guide_linkmom_matching_1.png';
const IMG_GUIDE_LINKMOM_MATCHING_2 = 'assets/images/guide_linkmom_matching_2.png';
const IMG_GUIDE_LINKMOM_MATCHING_3 = 'assets/images/guide_linkmom_matching_3.png';
const IMG_GUIDE_LINKMOM_MATCHING_4 = 'assets/images/guide_linkmom_matching_4.png';
const IMG_GUIDE_LINKMOM_MATCHING_5 = 'assets/images/guide_linkmom_matching_5.png';
const IMG_GUIDE_LINKMOM_PAY = 'assets/images/guide_linkmom_pay.png';
const IMG_GUIDE_LINKMOM_START = 'assets/images/guide_linkmom_start.png';
const IMG_GUIDE_MOMDADY_CHAT_1 = 'assets/images/guide_momdady_chat_1.png';
const IMG_GUIDE_MOMDADY_CHAT_2 = 'assets/images/guide_momdady_chat_2.png';
const IMG_GUIDE_MOMDADY_CHAT_3 = 'assets/images/guide_momdady_chat_3.png';
const IMG_GUIDE_MOMDADY_CHAT_4 = 'assets/images/guide_momdady_chat_4.png';
const IMG_GUIDE_MOMDADY_CHAT_5 = 'assets/images/guide_momdady_chat_5.png';
const IMG_GUIDE_MOMDADY_DIARY = 'assets/images/guide_momdady_diary.png';
const IMG_GUIDE_MOMDADY_MATCHING_1 = 'assets/images/guide_momdady_matching_1.png';
const IMG_GUIDE_MOMDADY_MATCHING_2 = 'assets/images/guide_momdady_matching_2.png';
const IMG_GUIDE_MOMDADY_MATCHING_3 = 'assets/images/guide_momdady_matching_3.png';
const IMG_GUIDE_MOMDADY_MATCHING_4 = 'assets/images/guide_momdady_matching_4.png';
const IMG_GUIDE_MOMDADY_MATCHING_5 = 'assets/images/guide_momdady_matching_5.png';
const IMG_GUIDE_MOMDADY_PAY_1 = 'assets/images/guide_momdady_pay_1.png';
const IMG_GUIDE_MOMDADY_PAY_2 = 'assets/images/guide_momdady_pay_2.png';
const IMG_GUIDE_MOMDADY_START = 'assets/images/guide_momdady_start.png';
const IMG_PROMO_JUNE = 'assets/images/promo_june.png';

const IMG_TUTORIAL_L_HOME = 'assets/illust/tutorial_l_home.png';
const IMG_TUTORIAL_LAST = 'assets/illust/tutorial_last.png';
const IMG_TUTORIAL_M_HOME = 'assets/illust/tutorial_m_home.png';
const IMG_TUTORIAL_MYPAGE = 'assets/illust/tutorial_mypage.png';
const IMG_TUTORIAL_PROCESS = 'assets/illust/tutorial_last_process.png';
const IMG_TUTORIAL_BANNER = 'assets/illust/tutorial_banner.png';

/* --------------------------------- ILLUST --------------------------------- */

const ILLUST_INTRO = 'assets/illust/intro.png';
const ILLUST_CARE = 'assets/illust/care.png';
const ILLUST_GO_HOME_SCHOOL = 'assets/illust/go_home_school.png';
const ILLUST_STRESS = 'assets/illust/stress.png';
const ILLUST_TUTORIAL = 'assets/illust/tutorial_illust.png';

/* --------------------------------- BANK --------------------------------- */

const double SIZE_20 = 20;

/// 기본
Widget image({
  String imagePath = '',
  double size = SIZE_20,
  Alignment alignment = Alignment.center,
  double? width = 0.0,
  double? height = 0.0,
}) =>
    Image.asset(
      imagePath,
      width: width == 0 ? size : width,
      height: height == 0 ? size : height,
      alignment: alignment,
      // width: width == 0 ? size : width,
      // height: height == 0 ? size : height,
    );

String bank(int code) {
  switch (code) {

    /// 산업은행
    case 2:
      return BANK_KDB;

    /// 기업은행
    case 3:
      return BANK_IBK;

    /// 국민은행
    case 4:
      return BANK_KB;

    /// 수협
    case 7:
      return BANK_SH;

    /// 농협중앙회 (NH농협)
    case 11:

    /// 농/축협 (단위농협)
    case 12:
      return BANK_NH;

    /// 우리은행
    case 20:
      return BANK_WOORI;

    /// 제일은행
    case 23:
      return BANK_SC;

    /// 한국씨티은행
    case 27:
      return BANK_CITI;

    /// 대구은행
    case 31:
      return BANK_DGB;

    /// 부산은행
    case 32:
      return BANK_BUSAN;

    /// 하나은행(충청은행)
    case 33:
      return BANK_HANA;

    /// 광주은행
    case 34:
      return BANK_KWANGJU;

    /// 제주은행
    case 35:
      return BANK_JEJU;

    /// 전북은행
    case 37:
      return BANK_JB;

    /// 경남은행
    case 39:
      return BANK_KYONGNAM;

    ///새마을
    case 45:
      return BANK_MG;

    /// 신협
    case 48:
      return BANK_SHINHYOP;

    /// 저축은행
    case 50:
      return BANK_SAVE;

    /// 모건스탠리은행
    case 52:
      return BANK_MORGAN;

    /// 홍콩상하이은행(HSBC)
    case 54:
      return BANK_HSBC;

    /// 도이치은행
    case 55:
      return BANK_DEUTSCHE;

    /// 유에프제이은행
    case 57:
      return BANK_UFJ;

    /// 미즈호코퍼레이트은행
    case 58:
      return STOCK_MIZUHO;

    /// 미쓰비시도쿄은행
    case 59:
      return BANK_UFJ;

    /// 뱅크오프아메리카
    case 60:
      return BANK_BOA;

    /// 우체국
    case 71:
      return BANK_KOPO;

    /// 하나은행
    case 81:
      return BANK_HANA;

    /// 신한은행
    case 88:
      return BANK_SHINHAN;

    /// 케이뱅크
    case 89:
      return BANK_K;

    /// 카카오뱅크
    case 90:
      return BANK_KAKAO;

    /// 토스뱅크
    case 92:
      return BANK_TOSS;

    /// 동양종합금융증권
    case 209:
      return STOCK_DONGYANG;

    /// 현대증권
    case 218:
      return STOCK_HYUNDAI;

    /// 미래에셋증권
    case 230:
      return STOCK_MIRAE;

    /// 대우증권
    case 238:
      return STOCK_KDB;

    /// 삼성증권
    case 240:
      return STOCK_SAMSUNG;

    /// 한국투자증권
    case 243:
      return STOCK_KOR;

    /// 우리투자증권
    case 247:
      return STOCK_NH;

    /// 교보증권
    case 261:
      return STOCK_KYOBO;

    /// 하이투자증권
    case 262:
      return STOCK_DGB;

    /// 투자증권 HMC
    case 263:
      return STOCK_HMC;

    /// SK증권
    case 266:
      return STOCK_SK;

    /// 대신증권
    case 267:
      return STOCK_DAISHIN;

    /// 한화증권
    case 269:
      return STOCK_HANHWA;

    /// 하나대투증권
    case 270:
      return STOCK_HANA;

    /// 신한금융투자
    case 278:
      return STOCK_SHINHAN;

    /// 동부증권
    case 279:
      return STOCK_DB;

    /// 유진투자증권
    case 280:
      return STOCK_YOOJIN;

    /// 메리츠증권
    case 287:
      return STOCK_MERITZ;

    /// NH 투자증권
    case 289:
      return STOCK_NH;

    /// 신영증권
    case 291:
      return STOCK_SHINYOUNG;

    /// other
    default:
      return BANK_KAKAO;
  }
}

Widget smallProfile(bool isLinkmom, {bool isCommunity = false}) {
  return Image.asset(isCommunity
      ? IMG_PERSON_SMALL_COMMUNITY
      : isLinkmom
          ? IMG_PERSON_SMALL_LINKMOM
          : IMG_PERSON_SMALL_MOMDADY);
}

Widget largeProfile(bool isLinkmom) {
  return Image.asset(isLinkmom ? IMG_PROFILE_LINKMOM : IMG_PROFILE_MOMDADY);
}
