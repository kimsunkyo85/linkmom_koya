import 'dart:math' as math;
import 'dart:ui' as ui;

import 'package:auto_size_text/auto_size_text.dart';
import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cupertino_will_pop_scope/cupertino_will_pop_scope.dart';
import 'package:custom_refresh_indicator/custom_refresh_indicator.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:linkmom/data/network/api_end_point.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/loading.dart';
import 'package:linkmom/utils/picker/date_picker.dart';
import 'package:linkmom/utils/shape/custom_trackshape.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:linkmom/utils/style/svg_style.dart';
import 'package:linkmom/utils/textHighlight.dart';
import 'package:linkmom/view/calendar/repeat_selecter_view.dart';
import 'package:linkmom/view/lcons.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_1_page.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../../main.dart';
import '../../manager/enum_utils.dart';
import '../../view/main/chat/full_photo.dart';
import '../commons.dart';
import 'color_style.dart';
import 'image_style.dart';
import 'text_style.dart';

double appBarHeight() => AppBar().preferredSize.height;

double statusBarHeight = 47.0;

double getStatusBarHeight() => statusBarHeight;

double appTabHeight() => 70.0;

double heightNoti(BuildContext context) => MediaQuery.of(context).size.shortestSide;

double heightFull(BuildContext context) => MediaQuery.of(context).size.height;

double widthFull(BuildContext context) => MediaQuery.of(context).size.width;

double heightDlg(BuildContext context) => MediaQuery.of(context).size.height * 0.2;

double widthDlg(BuildContext context) => MediaQuery.of(context).size.width * 0.8;

double contentHeight(BuildContext context) => heightFull(context) - (appBarHeight() + appTabHeight() + getStatusBarHeight());

///기본 테마 설정
final linkmomTheme = ThemeData(
    primaryColor: mainAppColor,
    fontFamily: 'NotoSansCJKkr',
    hintColor: hintColor,
    backgroundColor: color_white,
    brightness: Brightness.light,
    highlightColor: color_transparent,
    splashColor: color_transparent,
    scrollbarTheme: ScrollbarThemeData(
      radius: Radius.circular(10),
      thickness: MaterialStateProperty.all(7),
      thumbColor: MaterialStateProperty.all(color_black_20),
    ),
    pageTransitionsTheme: PageTransitionsTheme(builders: {TargetPlatform.iOS: CupertinoWillPopScopePageTransionsBuilder(), TargetPlatform.android: CupertinoWillPopScopePageTransionsBuilder()}));

Widget lBadge(int _badge, {isContent = true, isIcon = true, isStart = false, top = 10.0, end = 10.0, onBadge()?, Widget? icon, EdgeInsetsGeometry? padding}) {
  return Badge(
      alignment: Alignment.center,
      showBadge: _badge == 0 ? false : true,
      elevation: 0.0,
      position: isStart ? BadgePosition.topStart(top: top, start: end) : BadgePosition.topEnd(top: top, end: end),
      badgeColor: color_ff4c1b,
      padding: padding ?? padding_04,
      badgeContent: isContent
          ? null
          : _badge == 0
              ? lText('')
              : lText(
                  '$_badge',
                  style: stBadge,
                ),
      child: isIcon
          ? IconButton(
              iconSize: 40,
              icon: icon ?? Icon(Icons.notifications_none),
              color: color_black,
              onPressed: () {
                if (onBadge != null) {
                  onBadge();
                }
              },
            )
          : Center());
}

Widget badgeView(String title, int badge, {AlignmentGeometry alignment = Alignment.center, bool isStart = false, double top = 5.0, double end = 10.0, EdgeInsetsGeometry? padding}) {
  return Stack(
    alignment: alignment,
    children: [
      lBadge(badge, isIcon: false, isStart: isStart, top: top, end: end, padding: padding),
      Tab(text: title),
    ],
  );
}

///테스트 모드 코드 및 정보 단말기
testModeView() {
  if (Commons.isDebugViewMode) log.d('『GGUMBI』>>> testModeView : deviceInfo: $deviceInfo,  <<< ');
  return Commons.isDebugViewMode
      ? Padding(
          padding: padding_05_R,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            // children: deviceInfo!.entries.map((e) => lText('${e.key}: ${e.value}', fontSize: 8, textAlign: TextAlign.left)).toList(),
            children: [
              lText('versionName: ${deviceInfo.versionName}', fontSize: 8, textAlign: TextAlign.left),
              lText('versionCode: ${deviceInfo.versionCode}', fontSize: 8, textAlign: TextAlign.left),
              lText('mode: ${ApiEndPoint.getBaseUrl()}', fontSize: 8, textAlign: TextAlign.left),
              lText('OS: ${deviceInfo.os}', fontSize: 8, textAlign: TextAlign.left),
            ],
          ),
        )
      : null;
}

///basic App Bar
AppBar appBar(
  String title, {
  isBack = true,
  onBack()?,
  isClose = false,
  Function? onClick,
  hide = true,
  reset = false,
  rightWidget,
}) =>
    AppBar(
      leading: isBack
          ? IconButton(
              icon: Lcons.nav_left(color: color_222222, size: 25),
              onPressed: () {
                if (onBack != null) {
                  onBack();
                } else {
                  Navigator.of(navigatorKey.currentContext!).pop();
                }
              },
            )
          : lText(''),
      backgroundColor: color_white,
      bottomOpacity: 1,
      elevation: 0.0,
      centerTitle: true,
      title: InkWell(
        onTap: () {
          onClick!(DialogAction.test);
        },
        child: lText(
          title,
          style: stAppBarTitle,
        ),
      ),
      actions: [
        if (isClose)
          IconButton(
            icon: Lcons.nav_close(color: color_222222, size: 25),
            color: color_222222,
            onPressed: () {
              if (onClick != null) {
                onClick(DialogAction.close);
              }
            },
          ),
        if (reset)
          InkWell(
            onTap: () {
              if (onClick != null) {
                onClick(DialogAction.delete);
              }
            },
            child: Row(
              children: [
                svgImage(
                  imagePath: SVG_RESET,
                  size: 25,
                ),
                sb_w_05,
                lText("초기화".tr(), style: st_14(textColor: color_999999)),
                sb_w_20,
              ],
            ),
          ),
        if (rightWidget != null) rightWidget,
      ],
      bottom: PreferredSize(
        child: Container(
          color: hide ? Colors.transparent : color_dedede,
          height: 1,
        ),
        preferredSize: Size.fromHeight(0),
      ),
    );

///main App Bar
AppBar mainAppBar(
  var _badge, {
  Widget? titleWidget,
  String title = '',
  hide = true,
  Function? onBadge,
  Widget? leading,
  Widget? icon,
  double width = 100,
}) =>
    AppBar(
      title: titleWidget == null
          ? lText(
              title,
              style: stAppBarTitle,
            )
          : titleWidget,
      iconTheme: IconThemeData(
        color: color_black,
      ),
      backgroundColor: color_white,
      bottomOpacity: 1,
      elevation: 0.0,
      centerTitle: true,
      actions: [
        Container(
          alignment: Alignment.centerRight,
          /*width: width,*/ child: lBadge(_badge, onBadge: () => onBadge!(), icon: icon),
        ),
      ],
      bottom: PreferredSize(
        child: hide ? lDivider() : lDivider(color: Colors.transparent),
        preferredSize: Size.fromHeight(0),
      ),
      leading: leading ?? Container(),
      // leadingWidth: width,
    );

///main Job Bar
AppBar jobAppBar(String title, {BuildContext? context, Key? key, isBack = true, isClose = true, hide = false, onClick()?}) => AppBar(
      key: key,
      leading: isBack
          ? IconButton(
              icon: Lcons.nav_left(color: color_222222, size: 25),
              onPressed: () => context == null ? Navigator.of(navigatorKey.currentContext!).pop() : Commons.pagePop(context, data: true),
              // onPressed: () => {if (context != null) Commons.pagePop(context, data: true)},
            )
          : lText(''),
      title: lText(
        title,
        style: stAppBarTitle,
      ),
      iconTheme: IconThemeData(
        color: color_black,
      ),
      backgroundColor: color_white,
      bottomOpacity: 1,
      elevation: 0.0,
      centerTitle: true,
      actions: [
        if (isClose)
          IconButton(
            icon: Lcons.nav_close(color: color_222222, size: 25),
            color: color_222222,
            onPressed: () {
              if (onClick != null) {
                onClick();
              }
            },
          ),
      ],
      bottom: PreferredSize(
        child: Container(
          color: hide ? Colors.transparent : color_dedede,
          height: 1,
        ),
        preferredSize: Size.fromHeight(0),
      ),
    );

///로딩 Dlg
ModalProgressHUD lModalProgressHUD(bool isLoading, Widget child, {opacity = 0.5, String? loadingMessage, bool showText = false, bool isOnlyLoading = false, Color bgColor = Colors.white10}) {
  if (isOnlyLoading) {
    showText = false;
    bgColor = color_transparent;
  }
  return ModalProgressHUD(
    inAsyncCall: isLoading,
    // demo of some additional parameters
    opacity: opacity,
    dismissible: false,
    progressIndicator: LoadingIndicator(message: loadingMessage, showText: showText, background: bgColor),
    child: child,
  );
}

///기본 스크롤뷰 상/하/좌/우/
Scrollbar lScrollView({
  Key? key,
  Axis scrollDirection = Axis.vertical,
  bool reverse = false,
  EdgeInsets padding = padding_Item,
  bool? primary,
  ScrollPhysics? physics,
  ScrollController? controller,
  Widget? child,
  DragStartBehavior dragStartBehavior = DragStartBehavior.start,
  Clip clipBehavior = Clip.hardEdge,
  String? restorationId,
  double scrollBarSize = 7.0,
}) =>
    Scrollbar(
      radius: Radius.circular(5),
      thickness: scrollBarSize,
      child: SingleChildScrollView(
        key: key,
        scrollDirection: scrollDirection,
        reverse: reverse,
        padding: padding,
        primary: primary,
        physics: physics ?? AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
        controller: controller,
        child: child,
        dragStartBehavior: dragStartBehavior,
        clipBehavior: clipBehavior,
        restorationId: restorationId,
      ),
    );

Scaffold lScaffold({
  Key? key,
  PreferredSizeWidget? appBar,
  Widget? body,
  Widget? floatingActionButton,
  FloatingActionButtonLocation? floatingActionButtonLocation,
  FloatingActionButtonAnimator? floatingActionButtonAnimator,
  List<Widget>? persistentFooterButtons,
  Widget? drawer,
  Widget? endDrawer,
  Widget? bottomNavigationBar,
  Widget? bottomSheet,
  Color backgroundColor = color_white,
  bool resizeToAvoidBottomPadding = true,
  bool resizeToAvoidBottomInset = true,
  bool primary = true,
  DragStartBehavior drawerDragStartBehavior = DragStartBehavior.start,
  bool extendBody = false,
  bool extendBodyBehindAppBar = false,
  Color? drawerScrimColor,
  double? drawerEdgeDragWidth,
  bool drawerEnableOpenDragGesture = true,
  bool endDrawerEnableOpenDragGesture = true,
  BuildContext? context,
}) =>
    Scaffold(
      key: key,
      appBar: appBar,
      body: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            if (context != null) {
              FocusScope.of(context).requestFocus(new FocusNode());
            }
          },
          child: body),
      floatingActionButton: floatingActionButton,
      floatingActionButtonLocation: floatingActionButtonLocation,
      floatingActionButtonAnimator: floatingActionButtonAnimator,
      persistentFooterButtons: persistentFooterButtons,
      drawer: drawer,
      endDrawer: endDrawer,
      bottomNavigationBar: bottomNavigationBar,
      bottomSheet: bottomSheet,
      backgroundColor: backgroundColor,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      primary: primary,
      drawerDragStartBehavior: drawerDragStartBehavior,
      extendBody: extendBody,
      extendBodyBehindAppBar: extendBodyBehindAppBar,
      drawerScrimColor: drawerScrimColor,
      drawerEdgeDragWidth: drawerEdgeDragWidth,
      drawerEnableOpenDragGesture: drawerEnableOpenDragGesture,
      endDrawerEnableOpenDragGesture: endDrawerEnableOpenDragGesture,
    );

///기본 Text 이걸로 사용할 것
Text lText(
  String data, {
  Key? key,
  TextStyle? style,
  color = color_textDisable,
  double fontSize = 14,
  FontWeight fontWeight = FontWeight.normal,
  StrutStyle? strutStyle,
  TextAlign? textAlign,
  ui.TextDirection? textDirection,
  Locale? locale,
  bool? softWrap,
  TextOverflow? overflow,
  double textScaleFactor = 1.0,
  int? maxLines,
  String? semanticsLabel,
  TextWidthBasis? textWidthBasis,
  ui.TextHeightBehavior? textHeightBehavior,
}) {
  // if(color != null && style != null){
  //   TextStyle value = style.copyWith(color: color);
  //   style = value;
  // }
  return Text(
    data,
    key: key,
    style: style == null ? defaultStyle(color: color, fontWeight: fontWeight, fontSize: fontSize) : style,
    strutStyle: strutStyle,
    textAlign: textAlign,
    textDirection: textDirection,
    locale: locale,
    softWrap: softWrap,
    overflow: overflow,
    textScaleFactor: textScaleFactor,
    maxLines: maxLines,
    semanticsLabel: semanticsLabel,
    textWidthBasis: textWidthBasis,
    textHeightBehavior: textHeightBehavior,
  );
}

///오토 텍스트 기본으로 사용 하것
AutoSizeText lAutoSizeText(
  String data, {
  Key? key,
  Key? textKey,
  TextStyle? style,
  StrutStyle? strutStyle,
  double minFontSize = 10,
  double maxFontSize = double.infinity,
  double stepGranularity = 1,
  List<double>? presetFontSizes,
  AutoSizeGroup? group,
  TextAlign? textAlign,
  ui.TextDirection? textDirection,
  Locale? locale,
  bool? softWrap,
  bool wrapWords = true,
  TextOverflow? overflow,
  Widget? overflowReplacement,
  double? textScaleFactor,
  int maxLines = 1,
  String semanticsLabel = '',
}) {
  return AutoSizeText(
    data,
    key: key,
    textKey: textKey,
    style: style == null ? defaultStyle() : style,
    strutStyle: strutStyle,
    minFontSize: minFontSize,
    maxFontSize: maxFontSize,
    stepGranularity: stepGranularity,
    presetFontSizes: presetFontSizes,
    group: group,
    textAlign: textAlign,
    textDirection: textDirection,
    locale: locale,
    softWrap: softWrap,
    wrapWords: wrapWords,
    overflow: overflow,
    overflowReplacement: overflowReplacement,
    textScaleFactor: textScaleFactor,
    maxLines: maxLines,
    semanticsLabel: semanticsLabel,
  );
}
// AutoSizeText(
//   data,
//   key: key,
//   textKey: textKey,
//   style: style == null ? defaultStyle() : style,
//   strutStyle: strutStyle,
//   minFontSize: minFontSize,
//   maxFontSize: maxFontSize,
//   stepGranularity: stepGranularity,
//   presetFontSizes: presetFontSizes,
//   group: group,
//   textAlign: textAlign,
//   textDirection: textDirection,
//   locale: locale,
//   softWrap: softWrap,
//   wrapWords: wrapWords,
//   overflow: overflow,
//   overflowReplacement: overflowReplacement,
//   textScaleFactor: textScaleFactor,
//   maxLines: maxLines,
//   semanticsLabel: semanticsLabel,
// );

///기본 컨테이너 배경 색상 화이트
Container lContainer({
  Key? key,
  AlignmentGeometry? alignment,
  EdgeInsets? padding,
  Color? color = color_white,
  Decoration? decoration,
  Decoration? foregroundDecoration,
  double? width,
  double? height,
  BoxConstraints? constraints,
  EdgeInsets? margin,
  Matrix4? transform,
  Widget? child,
  Clip clipBehavior = Clip.none,
}) =>
    Container(
      key: key,
      alignment: alignment,
      padding: padding,
      color: color,
      decoration: decoration,
      foregroundDecoration: foregroundDecoration,
      width: width,
      height: height,
      constraints: constraints,
      margin: margin,
      transform: transform,
      child: child,
      clipBehavior: clipBehavior,
    );

///리스트 제목 배경(텍스트 왼쪽, 공통 사용)
Widget lTvListTitle(String text, {padding = padding_10_LR, alignment = Alignment.centerLeft, style}) {
  if (style == null) {
    style = tvListTitle;
  }
  return Container(
    padding: padding,
    alignment: alignment,
    child: lText(
      text,
      style: style,
    ),
  );
}

///가로 라인(맘대디,링크쌤 프로필 또는 결제 하단 큰 영역에서 사용 센터 가운데)
Widget lDivider20({Color color = color_d4d4d4, thickness = 10.0, padding = padding_20_TB, height}) => Padding(
      padding: padding,
      child: Divider(
        color: color,
        thickness: thickness,
        height: height,
      ),
    );

///가로 라인
Widget lDivider({Color color = color_d6d6d6, double thickness = 1.0, padding = padding_0, height = 1.0}) => Padding(
      padding: padding,
      child: Divider(
        color: color,
        thickness: thickness,
        height: height,
      ),
    );

///위아래 패딩 라인
Widget lDividerVertical({Color color = color_d6d6d6, double thickness = 2, padding = padding_0}) => Row(
      children: [
        Padding(
          padding: padding,
          child: VerticalDivider(
            color: color,
            thickness: thickness,
            width: 1,
          ),
        ),
      ],
    );

///PercentIndicator
///stepCnt 카운트에 따라 변경하기....
Widget lPercentIndicator({
  int step = 1,
  int stepCnt = 8,
  double lineHeight = 4.0,
  LinearStrokeCap linearStrokeCap = LinearStrokeCap.butt,
  Color backgroundColor = color_f1f1f5,
  EdgeInsets padding = EdgeInsets.zero,
  bool animation = false,
  int animationDuration = 1000,
}) =>
    Column(
      children: [
        LinearPercentIndicator(
          percent: (1 / stepCnt) * step,
          lineHeight: lineHeight,
          linearStrokeCap: linearStrokeCap,
          progressColor: Commons.getColor(),
          backgroundColor: backgroundColor,
          padding: padding,
          animation: animation,
          animationDuration: animationDuration,
        ),
      ],
    );

SnackBar lSnackBar(String msg) => SnackBar(
      duration: Duration(seconds: 10),
      content: Row(
        children: <Widget>[CircularProgressIndicator(), lText(msg)],
      ),
    );

SnackBar lSnackBarMsg(String msg) => SnackBar(
      duration: Duration(seconds: 2),
      content: lText(msg, style: st_15(textColor: color_white)),
    );

///아이정보 - 이름/나이/성별 28sp -> 16
Widget lChildInfoText(String value) => lText(value, style: st_16(textColor: color_191919));

///돌봄유형 선택 하단, 돌봄장소선택 하단 폰트 16,
Widget lContentText(String contentText, {TextAlign textAlign = TextAlign.center}) => lText(
      contentText,
      textAlign: textAlign,
      style: defaultStyle(fontSize: 16, color: color_black),
    );

///돌봄유형 선택 하단, 돌봄장소선택 하단 폰트 16,
Widget lContentItemText(
  String contentText, {
  Color textColor = color_aaaaaa,
  double fontSize = 14,
  TextAlign? textAlign,
  FontWeight? fontWeight,
  Function? onClick,
}) =>
    lInkWell(
      onTap: onClick != null ? () => onClick() : null,
      child: lText(
        contentText,
        style: defaultStyle(fontSize: fontSize, color: textColor, fontWeight: fontWeight ?? FontWeight.w400),
        textAlign: textAlign,
      ),
    );

///둥근 컨테이너 공통
Widget lRoundContent({
  String content1 = '',
  String content2 = '',
  String content3 = '',
  TextAlign? textAlign,
  FontWeight? fontWeight,
  EdgeInsets margin = padding_0,
  EdgeInsets padding = padding_10,
  AlignmentGeometry alignment = Alignment.center,
  Color textColor = color_aaaaaa,
  Color bgColor = color_f8f8fa,
  Color? borderColor = color_dfe3e4,
  double fontSize = 14,
  CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.center,
  double borderRadius = 10.0,
  double borderWidth = 1.0,
  Function? onClick,
}) =>
    Container(
      alignment: alignment,
      margin: margin,
      padding: padding,
      decoration: decorationChildInfo(color: bgColor, borderColor: borderColor ?? color_dfe3e4, borderRadius: borderRadius, borderWidth: borderWidth),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: crossAxisAlignment,
        children: [
          if (StringUtils.validateString(content1))
            lContentItemText(content1, textColor: textColor, fontSize: fontSize, textAlign: textAlign, fontWeight: fontWeight, onClick: () {
              if (onClick != null) {
                onClick();
              }
            }),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (StringUtils.validateString(content2))
                lContentItemText(
                  content2,
                  textColor: textColor,
                ),
              if (StringUtils.validateString(content3))
                lContentItemText(
                  content3,
                  textColor: color_fe6060,
                ),
            ],
          ),
        ],
      ),
    );

///둥근 컨테이너 공통 (wrap 이용시 Alignment null)
Widget lRoundContainer({
  Color bgColor = color_f8f8fa,
  EdgeInsets padding = padding_10,
  EdgeInsets margin = padding_0,
  AlignmentGeometry alignment = Alignment.center,
  borderRadius = 10.0,
  Color borderColor = color_f8f8fa,
  double borderWidth = 1.0,
  Widget? child,
}) =>
    Align(
      alignment: Alignment.center,
      child: Container(
        alignment: alignment,
        margin: margin,
        padding: padding,
        decoration: decorationChildInfo(color: bgColor, borderRadius: borderRadius, borderColor: borderColor, borderWidth: borderWidth),
        child: child,
      ),
    );

///하단 라인
Widget lUnderLineContainer({
  String value = '',
  EdgeInsets padding = padding_10,
  AlignmentGeometry alignment = Alignment.centerLeft,
  Color textColor = color_aaaaaa,
}) =>
    Container(
      alignment: alignment,
      padding: padding,
      decoration: decorationUnderLine(),
      child: lText(
        value,
        style: st_14(textColor: textColor),
      ),
    );

///텍스트 입력, 오른쪽 버튼 화면
Widget lTextRightBtn({
  String? value,
  String? hint,
  String? btnTitle,
  Function? onClick,
}) =>
    Row(
      children: [
        Expanded(
          child: lUnderLineContainer(
            padding: padding_10_TB, value: value ?? hint!,
            alignment: Alignment.centerLeft,
            // padding: padding_05_B,
          ),
        ),
        lBtnWrap(btnTitle!, borderRadius: 15, btnEnableColor: color_white, sideColor: color_aaaaaa, textStyle: st_14(textColor: color_aaaaaa), padding: padding_05_L, height: 40, onClickAction: () {
          if (onClick != null) {
            onClick();
          }
          log.d('『GGUMBI』>>> placeView ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
        }),
      ],
    );

const double height_list = 200;
const double select_height = 80;
const double select_height_grid = 90;
const double size_10 = 10;
const double size_15 = 15;
const double size_16 = 16;
const double size_17 = 17;
const double size_18 = 18;
const double size_19 = 19;
const double size_20 = 20;
const double size_25 = 25;
const double size_30 = 30;
const double size_35 = 35;
const double size_40 = 40;
const double size_45 = 45;
const double size_50 = 50;
const double size_60 = 60;

///리스트 아이템 이미지 사이즈
const double listImgHeight = 60;
const double listImgWidth = 65;
const double listImgServices = 65;

///tab size
const double tabHeight = 50;

const SizedBox sb_h_01 = SizedBox(height: 1);
const SizedBox sb_h_02 = SizedBox(height: 2);
const SizedBox sb_h_03 = SizedBox(height: 3);
const SizedBox sb_h_04 = SizedBox(height: 4);
const SizedBox sb_h_05 = SizedBox(height: 5);
const SizedBox sb_h_06 = SizedBox(height: 6);
const SizedBox sb_h_07 = SizedBox(height: 7);
const SizedBox sb_h_08 = SizedBox(height: 8);
const SizedBox sb_h_09 = SizedBox(height: 9);
const SizedBox sb_h_10 = SizedBox(height: 10);
const SizedBox sb_h_15 = SizedBox(height: 15);
const SizedBox sb_h_17 = SizedBox(height: 17);
const SizedBox sb_h_20 = SizedBox(height: 20);
const SizedBox sb_h_25 = SizedBox(height: 25);
const SizedBox sb_h_30 = SizedBox(height: 30);
const SizedBox sb_h_40 = SizedBox(height: 40);
const SizedBox sb_h_50 = SizedBox(height: 50);
const SizedBox sb_h_60 = SizedBox(height: 60);
const SizedBox sb_h_70 = SizedBox(height: 70);
const SizedBox sb_h_80 = SizedBox(height: 80);
const SizedBox sb_h_90 = SizedBox(height: 90);
const SizedBox sb_h_100 = SizedBox(height: 100);

const SizedBox sb_w_01 = SizedBox(width: 1);
const SizedBox sb_w_02 = SizedBox(width: 2);
const SizedBox sb_w_03 = SizedBox(width: 3);
const SizedBox sb_w_04 = SizedBox(width: 4);
const SizedBox sb_w_05 = SizedBox(width: 5);
const SizedBox sb_w_06 = SizedBox(width: 6);
const SizedBox sb_w_07 = SizedBox(width: 7);
const SizedBox sb_w_08 = SizedBox(width: 8);
const SizedBox sb_w_09 = SizedBox(width: 9);
const SizedBox sb_w_10 = SizedBox(width: 10);
const SizedBox sb_w_15 = SizedBox(width: 15);
const SizedBox sb_w_20 = SizedBox(width: 20);
const SizedBox sb_w_30 = SizedBox(width: 30);
const SizedBox sb_w_40 = SizedBox(width: 40);
const SizedBox sb_w_50 = SizedBox(width: 50);
const SizedBox sb_w_60 = SizedBox(width: 60);
const SizedBox sb_w_70 = SizedBox(width: 70);
const SizedBox sb_w_80 = SizedBox(width: 80);
const SizedBox sb_w_90 = SizedBox(width: 90);
const SizedBox sb_w_100 = SizedBox(width: 100);

const SizedBox sbList = SizedBox(height: height_list);

const EdgeInsets padding_0 = EdgeInsets.zero;
const EdgeInsets padding_01 = EdgeInsets.all(1);
const EdgeInsets padding_02 = EdgeInsets.all(2);
const EdgeInsets padding_03 = EdgeInsets.all(3);
const EdgeInsets padding_04 = EdgeInsets.all(4);
const EdgeInsets padding_05 = EdgeInsets.all(5);
const EdgeInsets padding_06 = EdgeInsets.all(6);
const EdgeInsets padding_07 = EdgeInsets.all(7);
const EdgeInsets padding_08 = EdgeInsets.all(8);
const EdgeInsets padding_09 = EdgeInsets.all(9);
const EdgeInsets padding_10 = EdgeInsets.all(10);
const EdgeInsets padding_11 = EdgeInsets.all(11);
const EdgeInsets padding_12 = EdgeInsets.all(12);
const EdgeInsets padding_13 = EdgeInsets.all(13);
const EdgeInsets padding_14 = EdgeInsets.all(14);
const EdgeInsets padding_15 = EdgeInsets.all(15);
const EdgeInsets padding_16 = EdgeInsets.all(16);
const EdgeInsets padding_17 = EdgeInsets.all(17);
const EdgeInsets padding_18 = EdgeInsets.all(18);
const EdgeInsets padding_19 = EdgeInsets.all(19);
const EdgeInsets padding_20 = EdgeInsets.all(20);
const EdgeInsets padding_21 = EdgeInsets.all(21);
const EdgeInsets padding_22 = EdgeInsets.all(22);
const EdgeInsets padding_23 = EdgeInsets.all(23);
const EdgeInsets padding_24 = EdgeInsets.all(24);
const EdgeInsets padding_25 = EdgeInsets.all(25);
const EdgeInsets padding_26 = EdgeInsets.all(26);
const EdgeInsets padding_27 = EdgeInsets.all(27);
const EdgeInsets padding_28 = EdgeInsets.all(28);
const EdgeInsets padding_29 = EdgeInsets.all(29);
const EdgeInsets padding_30 = EdgeInsets.all(30);
const EdgeInsets padding_40 = EdgeInsets.all(40);
const EdgeInsets padding_50 = EdgeInsets.all(50);

const EdgeInsets padding_01_TB = EdgeInsets.symmetric(vertical: 1);
const EdgeInsets padding_02_TB = EdgeInsets.symmetric(vertical: 2);
const EdgeInsets padding_03_TB = EdgeInsets.symmetric(vertical: 3);
const EdgeInsets padding_04_TB = EdgeInsets.symmetric(vertical: 4);
const EdgeInsets padding_05_TB = EdgeInsets.symmetric(vertical: 5);
const EdgeInsets padding_06_TB = EdgeInsets.symmetric(vertical: 6);
const EdgeInsets padding_07_TB = EdgeInsets.symmetric(vertical: 7);
const EdgeInsets padding_08_TB = EdgeInsets.symmetric(vertical: 8);
const EdgeInsets padding_09_TB = EdgeInsets.symmetric(vertical: 9);
const EdgeInsets padding_10_TB = EdgeInsets.symmetric(vertical: 10);
const EdgeInsets padding_11_TB = EdgeInsets.symmetric(vertical: 11);
const EdgeInsets padding_12_TB = EdgeInsets.symmetric(vertical: 12);
const EdgeInsets padding_13_TB = EdgeInsets.symmetric(vertical: 13);
const EdgeInsets padding_14_TB = EdgeInsets.symmetric(vertical: 14);
const EdgeInsets padding_15_TB = EdgeInsets.symmetric(vertical: 15);
const EdgeInsets padding_16_TB = EdgeInsets.symmetric(vertical: 16);
const EdgeInsets padding_17_TB = EdgeInsets.symmetric(vertical: 17);
const EdgeInsets padding_18_TB = EdgeInsets.symmetric(vertical: 18);
const EdgeInsets padding_19_TB = EdgeInsets.symmetric(vertical: 19);
const EdgeInsets padding_20_TB = EdgeInsets.symmetric(vertical: 20);
const EdgeInsets padding_21_TB = EdgeInsets.symmetric(vertical: 21);
const EdgeInsets padding_22_TB = EdgeInsets.symmetric(vertical: 22);
const EdgeInsets padding_23_TB = EdgeInsets.symmetric(vertical: 23);
const EdgeInsets padding_24_TB = EdgeInsets.symmetric(vertical: 24);
const EdgeInsets padding_25_TB = EdgeInsets.symmetric(vertical: 25);
const EdgeInsets padding_26_TB = EdgeInsets.symmetric(vertical: 26);
const EdgeInsets padding_27_TB = EdgeInsets.symmetric(vertical: 27);
const EdgeInsets padding_28_TB = EdgeInsets.symmetric(vertical: 28);
const EdgeInsets padding_29_TB = EdgeInsets.symmetric(vertical: 29);
const EdgeInsets padding_30_TB = EdgeInsets.symmetric(vertical: 30);
const EdgeInsets padding_40_TB = EdgeInsets.symmetric(vertical: 40);
const EdgeInsets padding_50_TB = EdgeInsets.symmetric(vertical: 50);

const EdgeInsets padding_01_LR = EdgeInsets.symmetric(horizontal: 1);
const EdgeInsets padding_02_LR = EdgeInsets.symmetric(horizontal: 2);
const EdgeInsets padding_03_LR = EdgeInsets.symmetric(horizontal: 3);
const EdgeInsets padding_04_LR = EdgeInsets.symmetric(horizontal: 4);
const EdgeInsets padding_05_LR = EdgeInsets.symmetric(horizontal: 5);
const EdgeInsets padding_06_LR = EdgeInsets.symmetric(horizontal: 6);
const EdgeInsets padding_07_LR = EdgeInsets.symmetric(horizontal: 7);
const EdgeInsets padding_08_LR = EdgeInsets.symmetric(horizontal: 8);
const EdgeInsets padding_09_LR = EdgeInsets.symmetric(horizontal: 9);
const EdgeInsets padding_10_LR = EdgeInsets.symmetric(horizontal: 10);
const EdgeInsets padding_11_LR = EdgeInsets.symmetric(horizontal: 11);
const EdgeInsets padding_12_LR = EdgeInsets.symmetric(horizontal: 12);
const EdgeInsets padding_13_LR = EdgeInsets.symmetric(horizontal: 13);
const EdgeInsets padding_14_LR = EdgeInsets.symmetric(horizontal: 14);
const EdgeInsets padding_15_LR = EdgeInsets.symmetric(horizontal: 15);
const EdgeInsets padding_16_LR = EdgeInsets.symmetric(horizontal: 16);
const EdgeInsets padding_17_LR = EdgeInsets.symmetric(horizontal: 17);
const EdgeInsets padding_18_LR = EdgeInsets.symmetric(horizontal: 18);
const EdgeInsets padding_19_LR = EdgeInsets.symmetric(horizontal: 19);
const EdgeInsets padding_20_LR = EdgeInsets.symmetric(horizontal: 20);
const EdgeInsets padding_21_LR = EdgeInsets.symmetric(horizontal: 21);
const EdgeInsets padding_22_LR = EdgeInsets.symmetric(horizontal: 22);
const EdgeInsets padding_23_LR = EdgeInsets.symmetric(horizontal: 23);
const EdgeInsets padding_24_LR = EdgeInsets.symmetric(horizontal: 24);
const EdgeInsets padding_25_LR = EdgeInsets.symmetric(horizontal: 25);
const EdgeInsets padding_26_LR = EdgeInsets.symmetric(horizontal: 26);
const EdgeInsets padding_27_LR = EdgeInsets.symmetric(horizontal: 27);
const EdgeInsets padding_28_LR = EdgeInsets.symmetric(horizontal: 28);
const EdgeInsets padding_29_LR = EdgeInsets.symmetric(horizontal: 29);
const EdgeInsets padding_30_LR = EdgeInsets.symmetric(horizontal: 30);
const EdgeInsets padding_40_LR = EdgeInsets.symmetric(horizontal: 40);
const EdgeInsets padding_50_LR = EdgeInsets.symmetric(horizontal: 50);

const EdgeInsets padding_01_L = EdgeInsets.fromLTRB(1, 0, 0, 0);
const EdgeInsets padding_02_L = EdgeInsets.fromLTRB(2, 0, 0, 0);
const EdgeInsets padding_03_L = EdgeInsets.fromLTRB(3, 0, 0, 0);
const EdgeInsets padding_04_L = EdgeInsets.fromLTRB(4, 0, 0, 0);
const EdgeInsets padding_05_L = EdgeInsets.fromLTRB(5, 0, 0, 0);
const EdgeInsets padding_06_L = EdgeInsets.fromLTRB(6, 0, 0, 0);
const EdgeInsets padding_07_L = EdgeInsets.fromLTRB(7, 0, 0, 0);
const EdgeInsets padding_08_L = EdgeInsets.fromLTRB(8, 0, 0, 0);
const EdgeInsets padding_09_L = EdgeInsets.fromLTRB(9, 0, 0, 0);
const EdgeInsets padding_10_L = EdgeInsets.fromLTRB(10, 0, 0, 0);
const EdgeInsets padding_11_L = EdgeInsets.fromLTRB(11, 0, 0, 0);
const EdgeInsets padding_12_L = EdgeInsets.fromLTRB(12, 0, 0, 0);
const EdgeInsets padding_13_L = EdgeInsets.fromLTRB(13, 0, 0, 0);
const EdgeInsets padding_14_L = EdgeInsets.fromLTRB(14, 0, 0, 0);
const EdgeInsets padding_15_L = EdgeInsets.fromLTRB(15, 0, 0, 0);
const EdgeInsets padding_16_L = EdgeInsets.fromLTRB(16, 0, 0, 0);
const EdgeInsets padding_17_L = EdgeInsets.fromLTRB(17, 0, 0, 0);
const EdgeInsets padding_18_L = EdgeInsets.fromLTRB(18, 0, 0, 0);
const EdgeInsets padding_19_L = EdgeInsets.fromLTRB(19, 0, 0, 0);
const EdgeInsets padding_20_L = EdgeInsets.fromLTRB(20, 0, 0, 0);
const EdgeInsets padding_21_L = EdgeInsets.fromLTRB(21, 0, 0, 0);
const EdgeInsets padding_22_L = EdgeInsets.fromLTRB(22, 0, 0, 0);
const EdgeInsets padding_23_L = EdgeInsets.fromLTRB(23, 0, 0, 0);
const EdgeInsets padding_24_L = EdgeInsets.fromLTRB(24, 0, 0, 0);
const EdgeInsets padding_25_L = EdgeInsets.fromLTRB(25, 0, 0, 0);
const EdgeInsets padding_26_L = EdgeInsets.fromLTRB(26, 0, 0, 0);
const EdgeInsets padding_27_L = EdgeInsets.fromLTRB(27, 0, 0, 0);
const EdgeInsets padding_28_L = EdgeInsets.fromLTRB(28, 0, 0, 0);
const EdgeInsets padding_29_L = EdgeInsets.fromLTRB(29, 0, 0, 0);
const EdgeInsets padding_30_L = EdgeInsets.fromLTRB(30, 0, 0, 0);
const EdgeInsets padding_40_L = EdgeInsets.fromLTRB(40, 0, 0, 0);
const EdgeInsets padding_50_L = EdgeInsets.fromLTRB(50, 0, 0, 0);

const EdgeInsets padding_01_T = EdgeInsets.fromLTRB(0, 1, 0, 0);
const EdgeInsets padding_02_T = EdgeInsets.fromLTRB(0, 2, 0, 0);
const EdgeInsets padding_03_T = EdgeInsets.fromLTRB(0, 3, 0, 0);
const EdgeInsets padding_04_T = EdgeInsets.fromLTRB(0, 4, 0, 0);
const EdgeInsets padding_05_T = EdgeInsets.fromLTRB(0, 5, 0, 0);
const EdgeInsets padding_06_T = EdgeInsets.fromLTRB(0, 6, 0, 0);
const EdgeInsets padding_07_T = EdgeInsets.fromLTRB(0, 7, 0, 0);
const EdgeInsets padding_08_T = EdgeInsets.fromLTRB(0, 8, 0, 0);
const EdgeInsets padding_09_T = EdgeInsets.fromLTRB(0, 9, 0, 0);
const EdgeInsets padding_10_T = EdgeInsets.fromLTRB(0, 10, 0, 0);
const EdgeInsets padding_11_T = EdgeInsets.fromLTRB(0, 11, 0, 0);
const EdgeInsets padding_12_T = EdgeInsets.fromLTRB(0, 12, 0, 0);
const EdgeInsets padding_13_T = EdgeInsets.fromLTRB(0, 13, 0, 0);
const EdgeInsets padding_14_T = EdgeInsets.fromLTRB(0, 14, 0, 0);
const EdgeInsets padding_15_T = EdgeInsets.fromLTRB(0, 15, 0, 0);
const EdgeInsets padding_16_T = EdgeInsets.fromLTRB(0, 16, 0, 0);
const EdgeInsets padding_17_T = EdgeInsets.fromLTRB(0, 17, 0, 0);
const EdgeInsets padding_18_T = EdgeInsets.fromLTRB(0, 18, 0, 0);
const EdgeInsets padding_19_T = EdgeInsets.fromLTRB(0, 19, 0, 0);
const EdgeInsets padding_20_T = EdgeInsets.fromLTRB(0, 20, 0, 0);
const EdgeInsets padding_21_T = EdgeInsets.fromLTRB(0, 21, 0, 0);
const EdgeInsets padding_22_T = EdgeInsets.fromLTRB(0, 22, 0, 0);
const EdgeInsets padding_23_T = EdgeInsets.fromLTRB(0, 23, 0, 0);
const EdgeInsets padding_24_T = EdgeInsets.fromLTRB(0, 24, 0, 0);
const EdgeInsets padding_25_T = EdgeInsets.fromLTRB(0, 25, 0, 0);
const EdgeInsets padding_26_T = EdgeInsets.fromLTRB(0, 26, 0, 0);
const EdgeInsets padding_27_T = EdgeInsets.fromLTRB(0, 27, 0, 0);
const EdgeInsets padding_28_T = EdgeInsets.fromLTRB(0, 28, 0, 0);
const EdgeInsets padding_29_T = EdgeInsets.fromLTRB(0, 29, 0, 0);
const EdgeInsets padding_30_T = EdgeInsets.fromLTRB(0, 30, 0, 0);
const EdgeInsets padding_40_T = EdgeInsets.fromLTRB(0, 40, 0, 0);
const EdgeInsets padding_50_T = EdgeInsets.fromLTRB(0, 50, 0, 0);

const EdgeInsets padding_01_R = EdgeInsets.fromLTRB(0, 0, 1, 0);
const EdgeInsets padding_02_R = EdgeInsets.fromLTRB(0, 0, 2, 0);
const EdgeInsets padding_03_R = EdgeInsets.fromLTRB(0, 0, 3, 0);
const EdgeInsets padding_04_R = EdgeInsets.fromLTRB(0, 0, 4, 0);
const EdgeInsets padding_05_R = EdgeInsets.fromLTRB(0, 0, 5, 0);
const EdgeInsets padding_06_R = EdgeInsets.fromLTRB(0, 0, 6, 0);
const EdgeInsets padding_07_R = EdgeInsets.fromLTRB(0, 0, 7, 0);
const EdgeInsets padding_08_R = EdgeInsets.fromLTRB(0, 0, 8, 0);
const EdgeInsets padding_09_R = EdgeInsets.fromLTRB(0, 0, 9, 0);
const EdgeInsets padding_10_R = EdgeInsets.fromLTRB(0, 0, 10, 0);
const EdgeInsets padding_11_R = EdgeInsets.fromLTRB(0, 0, 11, 0);
const EdgeInsets padding_12_R = EdgeInsets.fromLTRB(0, 0, 12, 0);
const EdgeInsets padding_13_R = EdgeInsets.fromLTRB(0, 0, 13, 0);
const EdgeInsets padding_14_R = EdgeInsets.fromLTRB(0, 0, 14, 0);
const EdgeInsets padding_15_R = EdgeInsets.fromLTRB(0, 0, 15, 0);
const EdgeInsets padding_16_R = EdgeInsets.fromLTRB(0, 0, 16, 0);
const EdgeInsets padding_17_R = EdgeInsets.fromLTRB(0, 0, 17, 0);
const EdgeInsets padding_18_R = EdgeInsets.fromLTRB(0, 0, 18, 0);
const EdgeInsets padding_19_R = EdgeInsets.fromLTRB(0, 0, 19, 0);
const EdgeInsets padding_20_R = EdgeInsets.fromLTRB(0, 0, 20, 0);
const EdgeInsets padding_21_R = EdgeInsets.fromLTRB(0, 0, 21, 0);
const EdgeInsets padding_22_R = EdgeInsets.fromLTRB(0, 0, 22, 0);
const EdgeInsets padding_23_R = EdgeInsets.fromLTRB(0, 0, 23, 0);
const EdgeInsets padding_24_R = EdgeInsets.fromLTRB(0, 0, 24, 0);
const EdgeInsets padding_25_R = EdgeInsets.fromLTRB(0, 0, 25, 0);
const EdgeInsets padding_26_R = EdgeInsets.fromLTRB(0, 0, 26, 0);
const EdgeInsets padding_27_R = EdgeInsets.fromLTRB(0, 0, 27, 0);
const EdgeInsets padding_28_R = EdgeInsets.fromLTRB(0, 0, 28, 0);
const EdgeInsets padding_29_R = EdgeInsets.fromLTRB(0, 0, 29, 0);
const EdgeInsets padding_30_R = EdgeInsets.fromLTRB(0, 0, 30, 0);
const EdgeInsets padding_40_R = EdgeInsets.fromLTRB(0, 0, 40, 0);
const EdgeInsets padding_50_R = EdgeInsets.fromLTRB(0, 0, 50, 0);

const EdgeInsets padding_01_B = EdgeInsets.fromLTRB(0, 0, 0, 1);
const EdgeInsets padding_02_B = EdgeInsets.fromLTRB(0, 0, 0, 2);
const EdgeInsets padding_03_B = EdgeInsets.fromLTRB(0, 0, 0, 3);
const EdgeInsets padding_04_B = EdgeInsets.fromLTRB(0, 0, 0, 4);
const EdgeInsets padding_05_B = EdgeInsets.fromLTRB(0, 0, 0, 5);
const EdgeInsets padding_06_B = EdgeInsets.fromLTRB(0, 0, 0, 6);
const EdgeInsets padding_07_B = EdgeInsets.fromLTRB(0, 0, 0, 7);
const EdgeInsets padding_08_B = EdgeInsets.fromLTRB(0, 0, 0, 8);
const EdgeInsets padding_09_B = EdgeInsets.fromLTRB(0, 0, 0, 9);
const EdgeInsets padding_10_B = EdgeInsets.fromLTRB(0, 0, 0, 10);
const EdgeInsets padding_11_B = EdgeInsets.fromLTRB(0, 0, 0, 11);
const EdgeInsets padding_12_B = EdgeInsets.fromLTRB(0, 0, 0, 12);
const EdgeInsets padding_13_B = EdgeInsets.fromLTRB(0, 0, 0, 13);
const EdgeInsets padding_14_B = EdgeInsets.fromLTRB(0, 0, 0, 14);
const EdgeInsets padding_15_B = EdgeInsets.fromLTRB(0, 0, 0, 15);
const EdgeInsets padding_16_B = EdgeInsets.fromLTRB(0, 0, 0, 16);
const EdgeInsets padding_17_B = EdgeInsets.fromLTRB(0, 0, 0, 17);
const EdgeInsets padding_18_B = EdgeInsets.fromLTRB(0, 0, 0, 18);
const EdgeInsets padding_19_B = EdgeInsets.fromLTRB(0, 0, 0, 19);
const EdgeInsets padding_20_B = EdgeInsets.fromLTRB(0, 0, 0, 20);
const EdgeInsets padding_21_B = EdgeInsets.fromLTRB(0, 0, 0, 21);
const EdgeInsets padding_22_B = EdgeInsets.fromLTRB(0, 0, 0, 22);
const EdgeInsets padding_23_B = EdgeInsets.fromLTRB(0, 0, 0, 23);
const EdgeInsets padding_24_B = EdgeInsets.fromLTRB(0, 0, 0, 24);
const EdgeInsets padding_25_B = EdgeInsets.fromLTRB(0, 0, 0, 25);
const EdgeInsets padding_26_B = EdgeInsets.fromLTRB(0, 0, 0, 26);
const EdgeInsets padding_27_B = EdgeInsets.fromLTRB(0, 0, 0, 27);
const EdgeInsets padding_28_B = EdgeInsets.fromLTRB(0, 0, 0, 28);
const EdgeInsets padding_29_B = EdgeInsets.fromLTRB(0, 0, 0, 29);
const EdgeInsets padding_30_B = EdgeInsets.fromLTRB(0, 0, 0, 30);
const EdgeInsets padding_40_B = EdgeInsets.fromLTRB(0, 0, 0, 40);
const EdgeInsets padding_50_B = EdgeInsets.fromLTRB(0, 0, 0, 50);

const EdgeInsets padding_01_LB = EdgeInsets.fromLTRB(1, 0, 0, 1);
const EdgeInsets padding_02_LB = EdgeInsets.fromLTRB(2, 0, 0, 2);
const EdgeInsets padding_03_LB = EdgeInsets.fromLTRB(3, 0, 0, 3);
const EdgeInsets padding_04_LB = EdgeInsets.fromLTRB(4, 0, 0, 4);
const EdgeInsets padding_05_LB = EdgeInsets.fromLTRB(5, 0, 0, 5);
const EdgeInsets padding_06_LB = EdgeInsets.fromLTRB(6, 0, 0, 6);
const EdgeInsets padding_07_LB = EdgeInsets.fromLTRB(7, 0, 0, 7);
const EdgeInsets padding_08_LB = EdgeInsets.fromLTRB(8, 0, 0, 8);
const EdgeInsets padding_09_LB = EdgeInsets.fromLTRB(9, 0, 0, 9);
const EdgeInsets padding_10_LB = EdgeInsets.fromLTRB(10, 0, 0, 10);
const EdgeInsets padding_11_LB = EdgeInsets.fromLTRB(11, 0, 0, 11);
const EdgeInsets padding_12_LB = EdgeInsets.fromLTRB(12, 0, 0, 12);
const EdgeInsets padding_13_LB = EdgeInsets.fromLTRB(13, 0, 0, 13);
const EdgeInsets padding_14_LB = EdgeInsets.fromLTRB(14, 0, 0, 14);
const EdgeInsets padding_15_LB = EdgeInsets.fromLTRB(15, 0, 0, 15);
const EdgeInsets padding_16_LB = EdgeInsets.fromLTRB(16, 0, 0, 16);
const EdgeInsets padding_17_LB = EdgeInsets.fromLTRB(17, 0, 0, 17);
const EdgeInsets padding_18_LB = EdgeInsets.fromLTRB(18, 0, 0, 18);
const EdgeInsets padding_19_LB = EdgeInsets.fromLTRB(19, 0, 0, 19);
const EdgeInsets padding_20_LB = EdgeInsets.fromLTRB(20, 0, 0, 20);
const EdgeInsets padding_21_LB = EdgeInsets.fromLTRB(21, 0, 0, 21);
const EdgeInsets padding_22_LB = EdgeInsets.fromLTRB(22, 0, 0, 22);
const EdgeInsets padding_23_LB = EdgeInsets.fromLTRB(23, 0, 0, 23);
const EdgeInsets padding_24_LB = EdgeInsets.fromLTRB(24, 0, 0, 24);
const EdgeInsets padding_25_LB = EdgeInsets.fromLTRB(25, 0, 0, 25);
const EdgeInsets padding_26_LB = EdgeInsets.fromLTRB(26, 0, 0, 26);
const EdgeInsets padding_27_LB = EdgeInsets.fromLTRB(27, 0, 0, 27);
const EdgeInsets padding_28_LB = EdgeInsets.fromLTRB(28, 0, 0, 28);
const EdgeInsets padding_29_LB = EdgeInsets.fromLTRB(29, 0, 0, 29);
const EdgeInsets padding_30_LB = EdgeInsets.fromLTRB(30, 0, 0, 30);
const EdgeInsets padding_40_LB = EdgeInsets.fromLTRB(40, 0, 0, 40);
const EdgeInsets padding_50_LB = EdgeInsets.fromLTRB(50, 0, 0, 50);

const EdgeInsets padding_01_RB = EdgeInsets.fromLTRB(0, 0, 1, 1);
const EdgeInsets padding_02_RB = EdgeInsets.fromLTRB(0, 0, 2, 2);
const EdgeInsets padding_03_RB = EdgeInsets.fromLTRB(0, 0, 3, 3);
const EdgeInsets padding_04_RB = EdgeInsets.fromLTRB(0, 0, 4, 4);
const EdgeInsets padding_05_RB = EdgeInsets.fromLTRB(0, 0, 5, 5);
const EdgeInsets padding_06_RB = EdgeInsets.fromLTRB(0, 0, 6, 6);
const EdgeInsets padding_07_RB = EdgeInsets.fromLTRB(0, 0, 7, 7);
const EdgeInsets padding_08_RB = EdgeInsets.fromLTRB(0, 0, 8, 8);
const EdgeInsets padding_09_RB = EdgeInsets.fromLTRB(0, 0, 9, 9);
const EdgeInsets padding_10_RB = EdgeInsets.fromLTRB(0, 0, 10, 10);
const EdgeInsets padding_11_RB = EdgeInsets.fromLTRB(0, 0, 11, 11);
const EdgeInsets padding_12_RB = EdgeInsets.fromLTRB(0, 0, 12, 12);
const EdgeInsets padding_13_RB = EdgeInsets.fromLTRB(0, 0, 13, 13);
const EdgeInsets padding_14_RB = EdgeInsets.fromLTRB(0, 0, 14, 14);
const EdgeInsets padding_15_RB = EdgeInsets.fromLTRB(0, 0, 15, 15);
const EdgeInsets padding_16_RB = EdgeInsets.fromLTRB(0, 0, 16, 16);
const EdgeInsets padding_17_RB = EdgeInsets.fromLTRB(0, 0, 17, 17);
const EdgeInsets padding_18_RB = EdgeInsets.fromLTRB(0, 0, 18, 18);
const EdgeInsets padding_19_RB = EdgeInsets.fromLTRB(0, 0, 19, 19);
const EdgeInsets padding_20_RB = EdgeInsets.fromLTRB(0, 0, 20, 20);
const EdgeInsets padding_21_RB = EdgeInsets.fromLTRB(0, 0, 21, 21);
const EdgeInsets padding_22_RB = EdgeInsets.fromLTRB(0, 0, 22, 22);
const EdgeInsets padding_23_RB = EdgeInsets.fromLTRB(0, 0, 23, 23);
const EdgeInsets padding_24_RB = EdgeInsets.fromLTRB(0, 0, 24, 24);
const EdgeInsets padding_25_RB = EdgeInsets.fromLTRB(0, 0, 25, 25);
const EdgeInsets padding_26_RB = EdgeInsets.fromLTRB(0, 0, 26, 26);
const EdgeInsets padding_27_RB = EdgeInsets.fromLTRB(0, 0, 27, 27);
const EdgeInsets padding_28_RB = EdgeInsets.fromLTRB(0, 0, 28, 28);
const EdgeInsets padding_29_RB = EdgeInsets.fromLTRB(0, 0, 29, 29);
const EdgeInsets padding_30_RB = EdgeInsets.fromLTRB(0, 0, 30, 30);
const EdgeInsets padding_40_RB = EdgeInsets.fromLTRB(0, 0, 40, 40);
const EdgeInsets padding_50_RB = EdgeInsets.fromLTRB(0, 0, 50, 50);

const EdgeInsets padding_01_LTR = EdgeInsets.fromLTRB(1, 1, 1, 0);
const EdgeInsets padding_02_LTR = EdgeInsets.fromLTRB(2, 2, 2, 0);
const EdgeInsets padding_03_LTR = EdgeInsets.fromLTRB(3, 3, 3, 0);
const EdgeInsets padding_04_LTR = EdgeInsets.fromLTRB(4, 4, 4, 0);
const EdgeInsets padding_05_LTR = EdgeInsets.fromLTRB(5, 5, 5, 0);
const EdgeInsets padding_06_LTR = EdgeInsets.fromLTRB(6, 6, 6, 0);
const EdgeInsets padding_07_LTR = EdgeInsets.fromLTRB(7, 7, 7, 0);
const EdgeInsets padding_08_LTR = EdgeInsets.fromLTRB(8, 8, 8, 0);
const EdgeInsets padding_09_LTR = EdgeInsets.fromLTRB(9, 9, 9, 0);
const EdgeInsets padding_10_LTR = EdgeInsets.fromLTRB(10, 10, 10, 0);
const EdgeInsets padding_11_LTR = EdgeInsets.fromLTRB(11, 11, 11, 0);
const EdgeInsets padding_12_LTR = EdgeInsets.fromLTRB(12, 12, 12, 0);
const EdgeInsets padding_13_LTR = EdgeInsets.fromLTRB(13, 13, 13, 0);
const EdgeInsets padding_14_LTR = EdgeInsets.fromLTRB(14, 14, 14, 0);
const EdgeInsets padding_15_LTR = EdgeInsets.fromLTRB(15, 15, 15, 0);
const EdgeInsets padding_16_LTR = EdgeInsets.fromLTRB(16, 16, 16, 0);
const EdgeInsets padding_17_LTR = EdgeInsets.fromLTRB(17, 17, 17, 0);
const EdgeInsets padding_18_LTR = EdgeInsets.fromLTRB(18, 18, 18, 0);
const EdgeInsets padding_19_LTR = EdgeInsets.fromLTRB(19, 19, 19, 0);
const EdgeInsets padding_20_LTR = EdgeInsets.fromLTRB(20, 20, 20, 0);
const EdgeInsets padding_21_LTR = EdgeInsets.fromLTRB(21, 21, 21, 0);
const EdgeInsets padding_22_LTR = EdgeInsets.fromLTRB(22, 22, 22, 0);
const EdgeInsets padding_23_LTR = EdgeInsets.fromLTRB(23, 23, 23, 0);
const EdgeInsets padding_24_LTR = EdgeInsets.fromLTRB(24, 24, 24, 0);
const EdgeInsets padding_25_LTR = EdgeInsets.fromLTRB(25, 25, 25, 0);
const EdgeInsets padding_26_LTR = EdgeInsets.fromLTRB(26, 26, 26, 0);
const EdgeInsets padding_27_LTR = EdgeInsets.fromLTRB(27, 27, 27, 0);
const EdgeInsets padding_28_LTR = EdgeInsets.fromLTRB(28, 28, 28, 0);
const EdgeInsets padding_29_LTR = EdgeInsets.fromLTRB(29, 29, 29, 0);
const EdgeInsets padding_30_LTR = EdgeInsets.fromLTRB(30, 30, 30, 0);
const EdgeInsets padding_40_LTR = EdgeInsets.fromLTRB(40, 40, 40, 0);
const EdgeInsets padding_50_LTR = EdgeInsets.fromLTRB(50, 50, 50, 0);

const EdgeInsets padding_01_LRB = EdgeInsets.fromLTRB(1, 0, 1, 1);
const EdgeInsets padding_02_LRB = EdgeInsets.fromLTRB(2, 0, 2, 2);
const EdgeInsets padding_03_LRB = EdgeInsets.fromLTRB(3, 0, 3, 3);
const EdgeInsets padding_04_LRB = EdgeInsets.fromLTRB(4, 0, 4, 4);
const EdgeInsets padding_05_LRB = EdgeInsets.fromLTRB(5, 0, 5, 5);
const EdgeInsets padding_06_LRB = EdgeInsets.fromLTRB(6, 0, 6, 6);
const EdgeInsets padding_07_LRB = EdgeInsets.fromLTRB(7, 0, 7, 7);
const EdgeInsets padding_08_LRB = EdgeInsets.fromLTRB(8, 0, 8, 8);
const EdgeInsets padding_09_LRB = EdgeInsets.fromLTRB(9, 0, 9, 9);
const EdgeInsets padding_10_LRB = EdgeInsets.fromLTRB(10, 0, 10, 10);
const EdgeInsets padding_11_LRB = EdgeInsets.fromLTRB(11, 0, 11, 11);
const EdgeInsets padding_12_LRB = EdgeInsets.fromLTRB(12, 0, 12, 12);
const EdgeInsets padding_13_LRB = EdgeInsets.fromLTRB(13, 0, 13, 13);
const EdgeInsets padding_14_LRB = EdgeInsets.fromLTRB(14, 0, 14, 14);
const EdgeInsets padding_15_LRB = EdgeInsets.fromLTRB(15, 0, 15, 15);
const EdgeInsets padding_16_LRB = EdgeInsets.fromLTRB(16, 0, 16, 16);
const EdgeInsets padding_17_LRB = EdgeInsets.fromLTRB(17, 0, 17, 17);
const EdgeInsets padding_18_LRB = EdgeInsets.fromLTRB(18, 0, 18, 18);
const EdgeInsets padding_19_LRB = EdgeInsets.fromLTRB(19, 0, 19, 19);
const EdgeInsets padding_20_LRB = EdgeInsets.fromLTRB(20, 0, 20, 20);
const EdgeInsets padding_21_LRB = EdgeInsets.fromLTRB(21, 0, 21, 21);
const EdgeInsets padding_22_LRB = EdgeInsets.fromLTRB(22, 0, 22, 22);
const EdgeInsets padding_23_LRB = EdgeInsets.fromLTRB(23, 0, 23, 23);
const EdgeInsets padding_24_LRB = EdgeInsets.fromLTRB(24, 0, 24, 24);
const EdgeInsets padding_25_LRB = EdgeInsets.fromLTRB(25, 0, 25, 25);
const EdgeInsets padding_26_LRB = EdgeInsets.fromLTRB(26, 0, 26, 26);
const EdgeInsets padding_27_LRB = EdgeInsets.fromLTRB(27, 0, 27, 27);
const EdgeInsets padding_28_LRB = EdgeInsets.fromLTRB(28, 0, 28, 28);
const EdgeInsets padding_29_LRB = EdgeInsets.fromLTRB(29, 0, 29, 29);
const EdgeInsets padding_30_LRB = EdgeInsets.fromLTRB(30, 0, 30, 30);
const EdgeInsets padding_40_LRB = EdgeInsets.fromLTRB(40, 0, 40, 40);
const EdgeInsets padding_50_LRB = EdgeInsets.fromLTRB(50, 0, 50, 50);

const EdgeInsets padding_20LRB = EdgeInsets.fromLTRB(20, 0, 20, 20);
const EdgeInsets padding_location = EdgeInsets.fromLTRB(10, 0, 20, 0);
const EdgeInsets padding_listview = EdgeInsets.fromLTRB(20, 0, 20, 20);
const EdgeInsets padding_careType = EdgeInsets.fromLTRB(20, 10, 20, 10);
const EdgeInsets padding_moveType = EdgeInsets.fromLTRB(10, 0, 10, 0);
const EdgeInsets padding_moveTypeLinkMom = EdgeInsets.fromLTRB(30, 20, 30, 20);
const EdgeInsets padding_calcuTime = EdgeInsets.fromLTRB(20, 0, 40, 0);

const EdgeInsets padding_Dlg_Title = EdgeInsets.fromLTRB(20, 20, 20, 10);
const EdgeInsets padding_Dlg_Title_Guide = EdgeInsets.fromLTRB(20, 20, 20, 10);
const EdgeInsets padding_Dlg_Msg = EdgeInsets.fromLTRB(20, 0, 20, 20);
const EdgeInsets padding_Item = EdgeInsets.fromLTRB(20, 10, 20, 20);
const EdgeInsets padding_Item_TB = EdgeInsets.fromLTRB(0, 10, 0, 20);
const EdgeInsets padding_Item_LR = EdgeInsets.fromLTRB(20, 0, 20, 0);
const EdgeInsets paddingItemCareType = EdgeInsets.fromLTRB(10, 5, 10, 5);
const EdgeInsets stepMargin = EdgeInsets.fromLTRB(0, 5, 0, 20);
const EdgeInsets careTypeMargin = EdgeInsets.fromLTRB(20, 20, 20, 40);
const EdgeInsets stepDateMargin = EdgeInsets.fromLTRB(0, 5, 0, 15);

const EdgeInsets chat_content_padding = EdgeInsets.fromLTRB(15, 10, 15, 10);
const EdgeInsets chat_content_right_margin = EdgeInsets.only(left: 10);

const int maxMobileLength = 11;
const int maxAuthLength = 6;
const int maxPasswordLength = 16;

InputDecoration bgDecoration(
  String hint, {
  Color bgColor = color_f8f8fa,
  Color lineColor = color_b2b2b2,
  Color lineDisableColor = color_d2d2d2,
  Color lineErrorColor = color_b2b2b2 /*color_f37370*/,
  Color textColor = color_aaaaaa,
  double radius = 10.0,
  bool isLine = false,
  int maxLength = 0,
  TextStyle? errorStyle,
}) =>
    InputDecoration(
      hintText: hint,
      hintStyle: st_14(textColor: color_aaaaaa),
      // hintStyle: TextStyle(color: color_b2b2b2),
      contentPadding: padding_10,
      filled: true,
      fillColor: bgColor,
      border: InputBorder.none,
      // border: isLine ? OutlineInputBorder(borderRadius: BorderRadius.circular(radius), borderSide: BorderSide(color: isLine ? lineColor : bgColor)) : InputBorder.none,
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: isLine ? lineColor : bgColor),
        // borderSide: BorderSide(color: bgColor),
        borderRadius: BorderRadius.circular(radius),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: isLine ? lineColor : bgColor),
        // borderSide: BorderSide(color: bgColor),
        borderRadius: BorderRadius.circular(radius),
      ),
      disabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: isLine ? lineDisableColor : bgColor),
        // borderSide: BorderSide(color: bgColor),
        borderRadius: BorderRadius.circular(radius),
      ),
      errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: isLine ? lineErrorColor : bgColor),
        borderRadius: BorderRadius.circular(radius),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: isLine ? lineErrorColor : bgColor),
        borderRadius: BorderRadius.circular(radius),
      ),
      counterText: maxLength == 0 ? '' : null,
      counterStyle: st_14(textColor: textColor),
      errorStyle: errorStyle ?? InputDecorationTheme().errorStyle,
    );

// InputDecoration underLineDecoration(String hint, {Color bgColor = color_f8f8fa}) => InputDecoration(
//       hintText: hint,
//       hintStyle: formStyle,
//       // contentPadding: padding_10,
//       // border: UnderlineInputBorder(borderSide: BorderSide(color: bgColor)),
//       // focusedBorder: UnderlineInputBorder(
//       //   borderSide: BorderSide(color: bgColor),
//       // ),
//       // enabledBorder: UnderlineInputBorder(
//       //   borderSide: BorderSide(color: bgColor),
//       // ),
//       counterText: '',
//     );

///채팅 입력 input
InputDecoration inputChatDecoration({
  String hintText = '',
  TextStyle? hintStyle,
  bool isDense = true,
  bool filled = true,
  double gapPadding = 0,
  double borderRadius = 24,
  Color fillColor = color_white,
  Color borderColor = color_eeeeee,
  EdgeInsets padding = padding_10,
}) {
  if (!StringUtils.validateString(hintText)) {
    hintText = "메시지를입력해주세요.".tr();
  }

  if (hintStyle == null) {
    hintStyle = st_b_15(textColor: color_cecece);
  }
  return InputDecoration(
    ///입력 필드 가득 채우기 Flag
    isDense: isDense,
    filled: filled,
    fillColor: fillColor,
    contentPadding: padding,
    border: OutlineInputBorder(
      gapPadding: gapPadding,
      borderRadius: BorderRadius.circular(borderRadius),
      borderSide: BorderSide(color: borderColor),
    ),
    enabledBorder: OutlineInputBorder(
      gapPadding: gapPadding,
      borderRadius: BorderRadius.circular(borderRadius),
      borderSide: BorderSide(color: borderColor),
    ),
    focusedBorder: OutlineInputBorder(
      gapPadding: gapPadding,
      borderRadius: BorderRadius.circular(borderRadius),
      borderSide: BorderSide(color: borderColor),
    ),
    hintText: hintText,
    hintStyle: hintStyle,
  );
}

///텍스트 필드  readOnly:true이면 width 1로 세팅한다.
InputDecoration underLineDecoration(
  String hint, {
  TextStyle? hintStyle,
  Color bgColor = Colors.transparent,
  Color enableColor = color_dbdbdb,
  Color focusColor = color_main,
  Widget? icon,
  Alignment alignmentIcon = Alignment.center,
  Widget? suffix,
  bool suffixIconConstraints = false,
  bool isFn = true,
  Function? fn,
  bool focus = false,
  double width = 2.0,
  TextStyle? errorStyle,
  EdgeInsets? contentPadding,
  String? counterText = ' ',
  bool isDense = false,
  int maxLength = 0,
}) =>
    InputDecoration(
      fillColor: bgColor,
      hintText: hint,
      hintStyle: hintStyle ?? stRegisterHint,
      // counterText: counterText,
      counterText: maxLength == 0 ? counterText : null,
      contentPadding: contentPadding,
      isDense: isDense,
      suffixIconConstraints: suffixIconConstraints ? BoxConstraints(minHeight: 24, minWidth: 30) : null,
      suffixIcon: icon == null || !focus ? null : IconButton(icon: icon, alignment: alignmentIcon, padding: padding_0, onPressed: fn == null ? null : () => fn(), constraints: BoxConstraints()),
      suffix: suffix == null || !focus ? null : InkWell(onTap: fn == null ? null : () => fn(), child: suffix),
      focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: focus ? focusColor : enableColor, width: width)),
      enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: enableColor)),
      errorBorder: UnderlineInputBorder(borderSide: BorderSide(color: enableColor)),
      focusedErrorBorder: UnderlineInputBorder(borderSide: BorderSide(color: focusColor, width: width)),
      errorStyle: errorStyle ?? InputDecorationTheme().errorStyle,
    );

InputDecoration lineDecoration(String hint) => InputDecoration(
      labelText: hint,
      labelStyle: labelFormStyle,
      contentPadding: padding_10,
// control your hints text size
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide()),
      counterText: '',
    );

InputDecoration getDecorationPassword(String hint, IconData icon_1, IconData icon_2, bool isShow, Function fn, {Color? colors}) => InputDecoration(
      labelText: hint,
      labelStyle: labelFormStyle,
      contentPadding: padding_10,
// control your hints text size
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide()),
      counterText: '',
      suffixIcon: IconButton(
        icon: Icon(
          isShow ? icon_1 : icon_2,
          color: colors == null ? null : colors,
        ),
        onPressed: () => fn(),
        color: Colors.grey[400],
      ),
    );

InputDecoration getDecorationIcon(String hint, IconData icon, {Color? colors}) => InputDecoration(
      labelText: hint,
      labelStyle: labelFormStyle,
      contentPadding: padding_10,
// control your hints text size
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0), borderSide: BorderSide(color: Colors.grey[400]!)),
      counterText: '',
      suffixIcon: IconButton(
        icon: Icon(
          icon,
          color: colors == null ? null : colors,
        ),
        color: Colors.grey[400],
        onPressed: () {},
      ),
    );

ToolbarOptions getToolbarOptions({paste: false, cut: false, selectAll: false, copy: false}) => ToolbarOptions(
      paste: false,
      cut: false,
      selectAll: false,
      copy: false,
    );

BoxDecoration decorationRound({
  Color color = color_transparent,
  Color borderColor = color_eeeeee,
  double width = 1,
  double radius = 10,
}) =>
    BoxDecoration(
      color: color,
      border: Border.all(width: width, color: borderColor),
      borderRadius: BorderRadius.circular(radius),
    );

BoxDecoration decorationChildInfo({Color color = color_f8f8fa, borderRadius = 10.0, Color borderColor = color_dfe3e4, double borderWidth = 1}) => BoxDecoration(
      color: color,
      border: borderColor == null ? null : Border.all(width: borderWidth, color: borderColor),
      borderRadius: BorderRadius.circular(borderRadius),
    );

BoxDecoration decorationChatSend({Color color = color_00cfb5}) => BoxDecoration(
    borderRadius: BorderRadius.only(topLeft: Radius.circular(10), bottomRight: Radius.circular(10), bottomLeft: Radius.circular(10)),
    border: Border.all(
      color: color,
      width: 1.0,
    ));

BoxDecoration decorationChatReciver({Color color = color_b579c8}) => BoxDecoration(
    borderRadius: BorderRadius.only(topRight: Radius.circular(10), bottomRight: Radius.circular(10), bottomLeft: Radius.circular(10)),
    border: Border.all(
      color: color,
      width: 1.0,
    ));

BoxDecoration decorationChatSystem({Color color = color_00cfb5}) => BoxDecoration(
    borderRadius: BorderRadius.all(Radius.circular(16)),
    border: Border.all(
      color: color,
      width: 1.0,
    ));

// BoxDecoration decorationChildInfo({Color color = color_f8f8fa, borderRadius = 10.0, Color borderColor = color_dfe3e4, double borderWidth = 1}) {
//   log.d('『GGUMBI』>>> decorationChildInfo : borderColor: $borderColor,  <<< ');
//   return BoxDecoration(
//     color: color,
//     border: borderColor == null ? Border.all(width: 0.0, color: color_transparent) : Border.all(width: borderWidth, color: borderColor),
//     borderRadius: BorderRadius.circular(borderRadius),
//   );
// }

///하단 라인
BoxDecoration decorationUnderLine({Color color = color_aaaaaa}) => BoxDecoration(
        border: Border(
            bottom: BorderSide(
      color: color,
      width: 1.0,
    )));

///기본 버튼 (폰트크기:20, 폰트색상:화이트, 색상 메인 컬러)
Widget lBtn(
  String btnTitle, {
  isEnabled = true,
  isWidth = true,
  EdgeInsets margin = padding_20,
  EdgeInsets padding = padding_0,
  Color? sideColor,
  Color? btnColor,
  Color highLightText = color_white,
  Color highLightBg = color_highlight,
  double borderRadius = 30.0,
  Function? onClickAction,
  TextStyle? style,
  GlobalKey? key,
  double height = 56,
  Widget? icon,
  bool isIconLeft = true,
  MainAxisAlignment align = MainAxisAlignment.center,
  Function? disableAction,
  TextAlign? textAlign,
  Color highListColor = color_white,
  Color disabledColor = color_btnDisable,
  Color disabledTextColor = color_textDisable,
}) {
  if (btnColor == null) {
    // || btnColor == color_btnEnable
    btnColor = Commons.getColor();
  }
  bool isHighlight = false;
  return StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
    return Container(
      key: key,
      width: isWidth ? MediaQuery.of(navigatorKey.currentContext!).size.width : 0,
      height: height,
      margin: margin,
      child: MaterialButton(
        elevation: 0,
        padding: padding,
        highlightColor: color_transparent,
        splashColor: color_transparent,
        highlightElevation: 0,
        disabledColor: disabledColor,
        disabledTextColor: disabledTextColor,
        color: isHighlight ? highLightBg : btnColor,
        textColor: isHighlight ? color_545454 : color_white,
        onHighlightChanged: (value) {
          setState(() {
            isHighlight = value;
          });
        },
        shape: sideColor == null
            ? RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(borderRadius),
              )
            : RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(borderRadius),
                side: BorderSide(color: sideColor),
              ),
        onPressed: isEnabled ? () => onClickAction == null ? null : onClickAction() : null,
        child: Row(
          mainAxisAlignment: align,
          children: [
            if (icon != null && isIconLeft) icon,
            Text(btnTitle,
                textAlign: textAlign,
                style: style == null
                    ? stBtnBottom(isEnable: isEnabled, textDisColor: color_white)
                    : defaultStyle(
                        fontSize: style.fontSize ?? 16,
                        fontWeight: style.fontWeight ?? FontWeight.bold,
                        color: isHighlight ? highListColor : style.color ?? Colors.white,
                      )),
            if (icon != null && !isIconLeft) icon,
          ],
        ),
      ),
    );
  });
}

///기본 버튼 (폰트크기:20, 폰트색상:화이트, 색상 메인 컬러, 기본 높이 45)
Widget lBtnWrap(
  String btnTitle, {
  double height = size_45,
  double heightBtn = size_45,
  isEnabled = true,
  EdgeInsets margin = padding_0,
  EdgeInsets padding = padding_15_LR,
  Color btnDisableColor = color_btnDisable,
  Color textDisableColor = color_textDisable,
  Color btnEnableColor = color_btnEnable,
  Color textEnableColor = color_textEnable,
  Color? sideColor,
  Color sideEnableColor = color_transparent,
  double fontSize = 13,
  TextStyle? textStyle,
  double borderRadius = 30.0,
  Alignment alignment = Alignment.center,
  FontWeight fontWeight = FontWeight.normal,
  int? animationDuration,
  Function? onClickAction,
}) {
  return Container(
    alignment: alignment,
    height: height,
    margin: margin,
    child: MaterialButton(
      elevation: 0,
      height: heightBtn,
      minWidth: 20,
      padding: padding,
      highlightColor: color_transparent,
      splashColor: color_transparent,
      highlightElevation: 0,
      disabledColor: btnDisableColor,
      disabledTextColor: textDisableColor,
      color: btnEnableColor,
      textColor: textEnableColor,
      animationDuration: animationDuration == null ? null : Duration(seconds: animationDuration),
      shape: sideColor == null
          ? RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(borderRadius),
              side: BorderSide(color: sideEnableColor),
            )
          : RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(borderRadius),
              side: BorderSide(color: isEnabled ? sideEnableColor : sideColor),
            ),
      onPressed: isEnabled ? () => onClickAction == null ? null : onClickAction() : null,
      child: lText(
        btnTitle,
        style: textStyle == null ? defaultStyle(isEnable: isEnabled, color: isEnabled ? textEnableColor : textDisableColor, colorDis: textEnableColor, fontSize: fontSize, fontWeight: fontWeight) : textStyle,
      ),
    ),
  );
}

///기본 버튼 (폰트크기:20, 폰트색상:화이트, 색상 메인 컬러, 가로/세로 X)
Widget lBtnWrapAll(
  String btnTitle, {
  isEnabled = true,
  EdgeInsets margin = padding_0,
  EdgeInsets padding = padding_15_LR,
  Color btnDisableColor = color_btnDisable,
  Color textDisableColor = color_textDisable,
  Color btnEnableColor = color_btnEnable,
  Color textEnableColor = color_textEnable,
  double fontSize = 20.0,
  TextStyle? textStyle,
  double borderRadius = 30.0,
  FontWeight fontWeight = FontWeight.normal,
  Function? onClickAction,
}) {
  return Container(
    margin: margin,
    child: MaterialButton(
      elevation: 0,
      padding: padding,
      highlightColor: color_transparent,
      splashColor: color_transparent,
      highlightElevation: 0,
      disabledColor: btnDisableColor,
      disabledTextColor: textDisableColor,
      color: btnEnableColor,
      textColor: textEnableColor,
      shape: lRoundedRectangleBorder(borderRadius: borderRadius, disBorderColor: color_transparent),
      // shape: RoundedRectangleBorder(
      //   borderRadius: BorderRadius.circular(borderRadius),
      // ),
      onPressed: isEnabled ? () => onClickAction == null ? null : onClickAction() : null,
      child: lText(
        btnTitle,
        style: textStyle == null ? defaultStyle(isEnable: isEnabled, color: isEnabled ? textEnableColor : textDisableColor, colorDis: textEnableColor, fontSize: fontSize, fontWeight: fontWeight) : textStyle,
      ),
    ),
  );
}

///기본 버튼 (폰트크기:15, 폰트색상:화이트, 색상 메인 컬러)
Widget lBtnWrap_15(
  String btnTitle, {
  double height = size_45,
  isEnabled = true,
  EdgeInsets margin = padding_0,
  EdgeInsets padding = padding_10_LR,
  Color btnColor = color_btnEnable,
  TextStyle? textStyle,
  Function? onClickAction,
}) {
  return lBtnWrap(btnTitle, margin: margin, padding: padding, textStyle: st_b_15(isSelect: isEnabled), height: height, btnEnableColor: btnColor, isEnabled: isEnabled, onClickAction: onClickAction);
}

///기본 버튼 (폰트크기:14, 폰트색상:화이트, 색상 메인 컬러)
Widget lBtnStep(
  String btnTitle, {
  isEnabled = true,
  EdgeInsets margin = padding_0,
  EdgeInsets padding = padding_05_LR,
  Color? btnColor,
  TextStyle? textStyle,
  Function? onClickAction,
}) {
  if (btnColor == null) {
    btnColor = Commons.getColor();
  }
  return lBtnWrapAll(btnTitle, margin: margin, padding: padding, btnEnableColor: btnColor, textStyle: st_b_12(isSelect: isEnabled), isEnabled: isEnabled, borderRadius: 30, onClickAction: onClickAction);
}

///버튼 highLight 적용 버전
Widget lBtnHL(
  String btnTitle, {
  GlobalKey? key,
  Widget? icon,
  bool isIconLeft = true,
  isEnabled = true,
  isWidth = true,
  MainAxisAlignment align = MainAxisAlignment.center,
  EdgeInsets margin = padding_10,
  EdgeInsets padding = padding_10,
  Color? btnColorBg,
  Color? sideColor,
  Color sideColorBg = color_highlight,
  Color textColorHighLight = color_white,
  Color btnColorHighLightBg = color_highlight,
  Color disabledColor = color_btnDisable,
  Color disabledTextColor = color_textDisable,
  double opacity = 0.05,
  double borderRadius = 30.0,
  double height = 56,
  TextStyle? style,
  TextAlign? textAlign,
  Function? onPressed,
}) {
  bool isHighlight = false;
  return StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
    return lContainer(
      key: key,
      width: isWidth ? MediaQuery.of(navigatorKey.currentContext!).size.width : 0,
      height: height,
      margin: margin,
      child: MaterialButton(
        elevation: 0,
        padding: padding,
        highlightColor: color_transparent,
        splashColor: color_transparent,
        highlightElevation: 0,
        disabledColor: disabledColor,
        disabledTextColor: disabledTextColor,
        color: isHighlight ? btnColorHighLightBg.withOpacity(opacity) : btnColorBg ?? Commons.getColor(),
        textColor: isHighlight ? color_545454 : color_white,
        onHighlightChanged: (value) {
          setState(() {
            isHighlight = value;
          });
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(borderRadius),
          side: BorderSide(
            color: isHighlight ? sideColorBg : sideColor ?? color_transparent,
            width: isHighlight ? 2.0 : 1.0,
          ),
        ),
        onPressed: isEnabled ? () => onPressed == null ? null : onPressed() : null,
        child: Row(
          mainAxisAlignment: align,
          children: [
            if (icon != null && isIconLeft) icon,
            lText(btnTitle,
                textAlign: textAlign,
                style: style ??
                    defaultStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: isHighlight
                          ? textColorHighLight
                          : isEnabled
                              ? color_textEnable
                              : color_textDisable.withOpacity(0.3),
                    )),
            if (icon != null && !isIconLeft) icon,
          ],
        ),
      ),
    );
  });
}

///아이콘 텍스트 형식(아이콘 위젯, 타이틀)
Widget lIconText({
  Widget? icon,
  String title = '',
  TextStyle? textStyle,
  isLeft = true,
  isExpanded = true,
  EdgeInsets padding = padding_05_LR,
  Function? onClick,
  MainAxisAlignment align = MainAxisAlignment.start,
}) {
  return lInkWell(
    onTap: onClick != null ? () => onClick() : null,
    child: Container(
      child: Row(
        mainAxisAlignment: align,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (icon != null)
            if (isLeft) icon,
          isExpanded
              ? Expanded(
                  child: Padding(
                    padding: icon != null ? padding : padding_0,
                    child: lAutoSizeText(
                      title,
                      maxLines: 1,
                      style: textStyle == null ? st_b_18(fontWeight: FontWeight.bold) : textStyle,
                    ),
                  ),
                )
              : Padding(
                  padding: icon != null ? padding : padding_0,
                  child: lAutoSizeText(
                    title,
                    textAlign: TextAlign.center,
                    maxLines: 1,
                    style: textStyle == null ? st_b_18(fontWeight: FontWeight.bold) : textStyle,
                  ),
                ),
          if (icon != null)
            if (!isLeft) icon,
        ],
      ),
    ),
  );
}

///텍스트+아이콘 형식
Widget lTextIcon({
  Widget? leftIcon,
  String title = '',
  Widget? rightIcon,
  TextStyle? textStyle,
  EdgeInsets paddingLeft = padding_0,
  EdgeInsets paddingRight = padding_0,
  Function? onClick,
  MainAxisAlignment align = MainAxisAlignment.start,
}) {
  return lInkWell(
    onTap: onClick != null ? () => onClick() : null,
    child: Row(
      mainAxisAlignment: align,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        if (leftIcon != null) Padding(padding: paddingLeft, child: leftIcon),
        lAutoSizeText(
          title,
          maxLines: 1,
          style: textStyle == null ? st_b_18(fontWeight: FontWeight.bold) : textStyle,
        ),
        if (rightIcon != null) Padding(padding: paddingRight, child: rightIcon),
      ],
    ),
  );
}

Widget lInkWell({
  Key? key,
  Widget? child,
  GestureTapCallback? onTap,
  GestureTapCallback? onDoubleTap,
  GestureLongPressCallback? onLongPress,
  GestureTapDownCallback? onTapDown,
  GestureTapCancelCallback? onTapCancel,
  ValueChanged<bool>? onHighlightChanged,
  ValueChanged<bool>? onHover,
  MouseCursor? mouseCursor,
  Color? focusColor,
  Color? hoverColor,
  Color highlightColor = color_transparent,
  MaterialStateProperty<Color>? overlayColor,
  Color splashColor = color_transparent,
  InteractiveInkFeatureFactory? splashFactory,
  double radius = 0.0,
  BorderRadius? borderRadius,
  ShapeBorder? customBorder,
  bool enableFeedback = true,
  bool excludeFromSemantics = false,
  FocusNode? focusNode,
  bool canRequestFocus = true,
  ValueChanged<bool>? onFocusChange,
  bool autofocus = false,
}) {
  return InkWell(
    key: key,
    child: child,
    onTap: onTap,
    onDoubleTap: onDoubleTap,
    onLongPress: onLongPress,
    onTapDown: onTapDown,
    onTapCancel: onTapCancel,
    onHighlightChanged: onHighlightChanged,
    onHover: onHover,
    mouseCursor: mouseCursor,
    focusColor: focusColor,
    hoverColor: hoverColor,
    highlightColor: highlightColor,
    overlayColor: overlayColor,
    splashColor: splashColor,
    splashFactory: splashFactory,
    radius: radius,
    borderRadius: borderRadius,
    customBorder: customBorder,
    enableFeedback: enableFeedback,
    excludeFromSemantics: excludeFromSemantics,
    focusNode: focusNode,
    canRequestFocus: canRequestFocus,
    onFocusChange: onFocusChange,
    autofocus: autofocus,
  );
}

OutlinedBorder lRoundedRectangleBorder({isSelected = false, double width = 1.0, double borderRadius = 15.0, Color enBorderColor = color_main, Color disBorderColor = color_dbdbdb}) => RoundedRectangleBorder(
    side: BorderSide(
      color: isSelected ? enBorderColor : disBorderColor,
      width: width,
    ),
    borderRadius: BorderRadius.circular(borderRadius));

InputDecoration lineDecorationExceptHint(
  String hint,
  Function fn,
  Color color,
  bool focus, {
  FloatingLabelBehavior floatingLabelBehavior = FloatingLabelBehavior.auto,
  EdgeInsets? contentPadding,
  double radius = 3.5,
}) =>
    InputDecoration(
      labelText: hint,
      floatingLabelBehavior: floatingLabelBehavior,
      contentPadding: contentPadding,
      labelStyle: TextStyle(color: color),
      suffixIcon: focus
          ? lInkWell(
              child: Padding(padding: padding_10, child: Lcons.clear(disableColor: color_cecece)),
              onTap: () => fn(),
            )
          : Icon(null),
      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: color), borderRadius: BorderRadius.circular(radius)),
      disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: color_dedede), borderRadius: BorderRadius.circular(radius)),
      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: color_00cfb5), borderRadius: BorderRadius.circular(radius)),
      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: color_00cfb5), borderRadius: BorderRadius.circular(radius)),
      errorText: null,
      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: color_00cfb5), borderRadius: BorderRadius.circular(radius)),
    );

InputDecoration lineDecorationPwExceptHint(String hint, bool isShow, Function showFn, Function clearFn, Color color, bool focus) => InputDecoration(
      labelText: hint,
      labelStyle: TextStyle(color: color),
      suffixIcon: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, mainAxisSize: MainAxisSize.min, children: [
        focus ? lInkWell(child: Padding(padding: padding_10_R, child: Lcons.clear(disableColor: color_cecece)), onTap: () => clearFn()) : Icon(null),
        focus ? lInkWell(child: Padding(padding: padding_10_R, child: Lcons.visibility(isEnabled: isShow, color: color_cecece, disableColor: color_cecece)), onTap: () => showFn()) : Icon(null),
      ]),
      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: color), borderRadius: BorderRadius.circular(3.5)),
      disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: color), borderRadius: BorderRadius.circular(3.5)),
      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: color_00cfb5), borderRadius: BorderRadius.circular(3.5)),
      focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: color_00cfb5), borderRadius: BorderRadius.circular(3.5)),
      errorBorder: OutlineInputBorder(borderSide: BorderSide(color: color_00cfb5), borderRadius: BorderRadius.circular(3.5)),
    );

Widget lBtnOutline(
  String btnTitle, {
  isEnabled = true,
  EdgeInsets margin = padding_0,
  EdgeInsets padding = padding_0,
  Color? textColor,
  Color? sideColor,
  double borderRadius = 30.0,
  Function? onClickAction,
  TextStyle? style,
}) {
  if (textColor == null) {
    textColor = Commons.getColor();
  }
  if (sideColor == null) {
    sideColor = Commons.getColor();
  }
  return Container(
    width: MediaQuery.of(navigatorKey.currentContext!).size.width,
    height: 56,
    margin: margin,
    child: MaterialButton(
      elevation: 0,
      highlightElevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(borderRadius),
        side: BorderSide(color: sideColor),
      ),
      onPressed: isEnabled ? () => onClickAction == null ? null : onClickAction() : null,
      child: Text(
        btnTitle,
        style: style ?? TextStyle(color: textColor),
      ),
    ),
  );
}

Widget lBtnOutlineLeftImage(
  String btnTitle, {
  isEnabled = true,
  EdgeInsets margin = padding_0,
  EdgeInsets padding = padding_0,
  Color? sideColor,
  double borderRadius = 30.0,
  Function? onClickAction,
  TextStyle? style,
  Color? bgColor,
  String? imagePath,
  Color imageColor = color_545454,
  double imageSize = 15,
  double height = 56,
  MainAxisAlignment mainAxisAlignment = MainAxisAlignment.center,
}) {
  return Container(
    width: MediaQuery.of(navigatorKey.currentContext!).size.width,
    height: height,
    margin: margin,
    child: MaterialButton(
      elevation: 0,
      highlightElevation: 0,
      color: bgColor,
      shape: sideColor == null
          ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(borderRadius), side: BorderSide(color: color_00cfb5))
          : RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(borderRadius),
              side: BorderSide(color: sideColor),
            ),
      onPressed: isEnabled ? () => onClickAction == null ? null : onClickAction() : null,
      child: Row(
        mainAxisAlignment: mainAxisAlignment,
        children: [
          svgImage(imagePath: imagePath ?? '', color: imageColor, size: imageSize),
          sb_w_05,
          Text(
            btnTitle,
            style: style ??
                st_16(
                  textColor: color_00cfb5,
                ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    ),
  );
}

InputDecoration underlineDecorationMultiIcon(
  String hint, {
  TextStyle? hintStyle,
  Color bgColor = Colors.transparent,
  Color enableColor = color_dbdbdb,
  Color focusColor = color_main,
  List<Widget>? icons,
  bool focus = false,
  String conterText = ' ',
  double width = 2.0,
  TextStyle? errorStyle,
  EdgeInsets? contentPadding,
}) {
  return InputDecoration(
    fillColor: bgColor,
    hintText: hint,
    hintStyle: hintStyle ?? stRegisterHint,
    counterText: conterText,
    contentPadding: contentPadding,
    suffixIcon: Row(mainAxisAlignment: MainAxisAlignment.end, mainAxisSize: MainAxisSize.min, children: icons ?? []),
    focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: focusColor, width: width)),
    enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: enableColor)),
    errorBorder: UnderlineInputBorder(borderSide: BorderSide(color: enableColor)),
    focusedErrorBorder: UnderlineInputBorder(borderSide: BorderSide(color: focusColor, width: width)),
    errorStyle: errorStyle ?? InputDecorationTheme().errorStyle,
  );
}

InputDecoration inputDecoration(String hint, {TextStyle? hintStyle, Color? borderColor, Icon? suffix}) {
  return InputDecoration(hintText: hint, hintStyle: hintStyle ?? labelFormStyle14, border: OutlineInputBorder(borderSide: BorderSide(color: borderColor ?? color_dedede)), suffixIcon: suffix ?? suffix);
}

Widget lBtnWrapCustom(
  String btnTitle, {
  double height = size_45,
  double width = 50,
  isEnabled = true,
  EdgeInsets margin = padding_0,
  EdgeInsets padding = padding_15_LR,
  Color btnDisableColor = color_btnDisable,
  Color textDisableColor = color_textDisable,
  Color btnEnableColor = color_btnEnable,
  Color textEnableColor = color_textEnable,
  Color? sideColor,
  Color sideEnableColor = color_transparent,
  double fontSize = 13.0,
  TextStyle? textStyle,
  double borderRadius = 30.0,
  Alignment alignment = Alignment.center,
  FontWeight fontWeight = FontWeight.normal,
  Function? onClickAction,
}) {
  return Container(
    alignment: alignment,
    height: height,
    margin: margin,
    child: MaterialButton(
      elevation: 0,
      minWidth: width,
      padding: padding,
      highlightColor: color_transparent,
      splashColor: color_transparent,
      highlightElevation: 0,
      disabledColor: btnDisableColor,
      disabledTextColor: textDisableColor,
      color: btnEnableColor,
      textColor: textEnableColor,
      shape: sideColor == null
          ? RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(borderRadius),
            )
          : RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(borderRadius),
              side: BorderSide(color: isEnabled ? sideEnableColor : sideColor),
            ),
      onPressed: isEnabled ? () => onClickAction == null ? null : onClickAction() : null,
      child: lText(
        btnTitle,
        style: textStyle == null ? defaultStyle(isEnable: isEnabled, color: isEnabled ? textEnableColor : textDisableColor, colorDis: textEnableColor, fontSize: fontSize, fontWeight: fontWeight) : textStyle,
      ),
    ),
  );
}

Widget lBtnWrapOutLine(
  String btnTitle, {
  double height = size_45,
  isEnabled = true,
  EdgeInsets margin = padding_0,
  EdgeInsets? padding,
  TextStyle? textStyle,
  Function? onClickAction,
  Color sideEnableColor = color_222222,
  Color sideDisableColor = color_b2b2b2,
}) {
  return lBtnWrap(
    btnTitle,
    textStyle: textStyle ??
        st_b_14(
          disableColor: color_b2b2b2,
          isSelect: !isEnabled,
        ),
    height: height,
    margin: margin,
    padding: padding ?? EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 15),
    isEnabled: isEnabled,
    sideEnableColor: sideEnableColor,
    btnEnableColor: Colors.white,
    sideColor: sideDisableColor,
    btnDisableColor: Colors.white,
    onClickAction: onClickAction,
  );
}

///로딩 Indicator
Widget lLoadingIndicator({String message = '', Color? loadingColor, Function? fn}) {
  return Commons.isDebugMode
      ? Center(
          child: GestureDetector(
            onDoubleTap: () {
              log.d('『GGUMBI』>>> lLoadingIndicator : :   <<< ');
              fn!();

              ///일반적으로 사용하지 않음, 앱 실행시 초기화면에서 만 사용 개발, 운영서버 선택
            },
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(
                  color: loadingColor == null ? Commons.getColor() : loadingColor,
                ),
                if (StringUtils.validateString(message)) sb_h_20,
                if (StringUtils.validateString(message))
                  lText(
                    message,
                    style: st_b_16(),
                  ),
              ],
            ),
          ),
        )
      : Container();
}

///이미지 로딩
Widget imageBuilder(ImageProvider imageProvider, {padding = padding_0, double width = 100, double height = 100, Color color = color_white, double? borderRadius, BoxFit fit = BoxFit.cover}) {
  return Container(
    width: width,
    height: height,
    padding: padding,
    decoration: BoxDecoration(
      color: color,
      borderRadius: borderRadius == null
          ? null
          : BorderRadius.all(
              Radius.circular(borderRadius),
            ),
      image: DecorationImage(
        image: imageProvider,
        fit: fit,
      ),
    ),
  );
}

///이미지 로딩
Widget placeholder({double size = 40.0}) {
  return Center(
    child: LoadingIndicator(showText: false, size: size, background: color_transparent),
  );
}

///이미지 로딩 error
Widget errorWidget({double size = 35, Color color = color_dbdbdb}) {
  return Center(
    child: Lcons.empty(size: size),
  );
}

///이미지 로딩 error
Widget errorMsgWidget({double size = 35, Color color = color_dbdbdb}) {
  return Container(
    color: Commons.getColor().withOpacity(0.05),
    child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      lText("이미지를표시할수없습니다".tr(), style: st_12(textColor: Commons.getColor())),
    ]),
  );
}

AppBar settingBtnAppBar(String title, {isBack = true, required Function onBack, onClick()?, hide = true, bool useSettings = true}) => AppBar(
      leading: IconButton(
        icon: Lcons.settings(color: color_b2b2b2, size: 25),
        onPressed: () {
          if (onClick != null) {
            onClick();
          }
        },
      ),
      backgroundColor: color_white,
      bottomOpacity: 1,
      elevation: 0.0,
      centerTitle: true,
      title: lText(
        title,
        style: stAppBarTitle,
      ),
      actions: [
        if (useSettings)
          IconButton(
            icon: Lcons.nav_close(color: color_545454, size: 25),
            onPressed: () => onBack(),
          ),
      ],
      bottom: PreferredSize(
        child: Container(
          color: hide ? Colors.transparent : color_dedede,
          height: 1,
        ),
        preferredSize: Size.fromHeight(0),
      ),
    );

Widget lIndigator(List<dynamic> data, int current) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: data.map((url) {
      int index = data.indexOf(url);
      return Container(
        width: 8.0,
        height: 8.0,
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: current == index ? Color.fromRGBO(0, 0, 0, 0.9) : Color.fromRGBO(0, 0, 0, 0.4),
        ),
      );
    }).toList(),
  );
}

///슬라이드바 테마
lSliderThemeData(
  BuildContext context, {
  activeTrackColor = color_main,
  inactiveTrackColor = color_dedede,
  thumbColor = color_main,
  trackHeight = 5.0,
  radius = 5.0,
  enabledThumbRadius = 12.0,
}) {
  return SliderTheme.of(context).copyWith(
    inactiveTrackColor: inactiveTrackColor,
    inactiveTickMarkColor: color_transparent,
    activeTrackColor: activeTrackColor,
    activeTickMarkColor: color_transparent,
    thumbColor: thumbColor,
    trackHeight: trackHeight,
    valueIndicatorColor: color_daf5f0,
    valueIndicatorTextStyle: st_b_16(),
    // showValueIndicator: ShowValueIndicator.always,//보기
    showValueIndicator: ShowValueIndicator.never,
    trackShape: RoundSliderTrackShape(radius: radius),
    thumbShape: RoundSliderThumbShape(enabledThumbRadius: enabledThumbRadius),
    //일반 슬라이드 바
    rangeThumbShape: RoundRangeSliderThumbShape(enabledThumbRadius: enabledThumbRadius), //범위지정지
    //안보기
    // trackShape: CustomTrackShape(),
  );
}

///기본 스크롤바 공통
Widget lScrollbar({
  Key? key,
  required Widget child,
  ScrollController? controller,
  bool? isAlwaysShown,
  bool? showTrackOnHover,
  double? hoverThickness,
  double thickness = 7.0,
  Radius? radius,
  ScrollNotificationPredicate? notificationPredicate,
  bool? interactive,
}) {
  if (radius == null) {
    radius = Radius.circular(10);
  }
  return Scrollbar(
    key: key,
    child: child,
    controller: controller,
    thumbVisibility: isAlwaysShown,
    showTrackOnHover: showTrackOnHover,
    hoverThickness: hoverThickness,
    thickness: thickness,
    radius: radius,
    notificationPredicate: notificationPredicate,
    interactive: interactive,
  );
}

Widget lTextBtn(String title, {Function? onClickEvent, TextStyle? style, Widget? leftIcon, Widget? rightIcon, Color? bg, double radius = 15, EdgeInsets? padding, Border? border, MainAxisAlignment mainAxisAlignment = MainAxisAlignment.center}) {
  return lInkWell(
      onTap: onClickEvent != null ? () => onClickEvent() : null,
      child: Container(
          decoration: BoxDecoration(color: bg == null ? Commons.getColor().withAlpha(41) : bg, borderRadius: BorderRadius.circular(radius), border: border ?? Border.all(color: Colors.transparent)),
          padding: padding ?? EdgeInsets.fromLTRB(10, 5, 10, 5),
          child: Row(mainAxisSize: MainAxisSize.min, mainAxisAlignment: mainAxisAlignment, children: [leftIcon ?? Container(), lText(title, style: style ?? st_12(textColor: Commons.getColor())), rightIcon ?? Container()])));
}

Widget lTextOutLineBtn(String title, {Function? onClickEvent, TextStyle? style, Icon? leftIcon, Icon? rightIcon, Color? bg, double radius = 15, EdgeInsets? padding, Border? border}) {
  return lInkWell(
      onTap: onClickEvent != null ? () => onClickEvent() : null,
      child: Container(
          decoration: BoxDecoration(color: color_transparent, borderRadius: BorderRadius.circular(radius), border: border ?? Border.all(color: bg == null ? Commons.getColor().withAlpha(41) : bg)),
          padding: padding ?? EdgeInsets.fromLTRB(10, 5, 10, 5),
          child: Row(mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.center, children: [leftIcon ?? Container(), lText(title, style: style ?? st_12(textColor: Commons.getColor())), rightIcon ?? Container()])));
}

Widget lNameText(String name, {TextStyle? style, TextStyle? highlightStyle}) {
  if (name.contains("O")) {
    return RichText(
        text: TextSpan(children: [
      TextSpan(text: name[0], style: style ?? st_b_13()),
      TextSpan(text: name.substring(1, name.length), style: highlightStyle ?? st_b_13(textColor: color_cecece)),
    ]));
  } else {
    return lText(name, style: style ?? st_b_13());
  }
}

Widget lProfileAvatar(String? url, {double radius = 40, bool fromCommunity = false}) {
  return StringUtils.validateString(url)
      ? CachedNetworkImage(
          width: radius * 2,
          height: radius * 2,
          imageUrl: url!,
          imageBuilder: (context, imageProvider) => CircleAvatar(
            backgroundImage: imageProvider,
            backgroundColor: color_transparent,
            radius: radius,
          ),
          placeholder: (context, url) => placeholder(size: radius),
          errorWidget: (context, url, error) => CircleAvatar(
            child: radius > 20 ? largeProfile(Commons.isLinkMom()) : smallProfile(Commons.isLinkMom(), isCommunity: fromCommunity),
            radius: radius,
            backgroundColor: color_eeeeee,
          ),
        )
      : CircleAvatar(
          child: radius > 20 ? largeProfile(Commons.isLinkMom()) : smallProfile(Commons.isLinkMom(), isCommunity: fromCommunity),
          radius: radius,
          backgroundColor: color_eeeeee,
        );
}

Widget lDot({Color? color, double? size, EdgeInsets? padding}) {
  return Container(
    margin: padding ?? padding_05_R,
    width: size ?? 6,
    height: size ?? 6,
    decoration: BoxDecoration(color: color ?? color_ff4c1b, borderRadius: BorderRadius.circular(40)),
  );
}

AppBar diaryAppbar({var badge = 0, Widget? titleWidget, String title = ' ', bool hide = true, Widget? rightW, BuildContext? ctx, Function? onBack}) => AppBar(
      leading: IconButton(
        icon: Lcons.nav_left(color: color_222222, size: 25),
        onPressed: () {
          if (onBack != null) {
            onBack();
          } else {
            Commons.pagePop(ctx ?? navigatorKey.currentContext!, data: true);
          }
        },
      ),
      title: titleWidget == null
          ? lText(
              title,
              style: stAppBarTitle,
            )
          : titleWidget,
      iconTheme: IconThemeData(
        color: color_black,
      ),
      backgroundColor: color_white,
      bottomOpacity: 1,
      elevation: 0.0,
      centerTitle: true,
      actions: [if (rightW != null) rightW],
      bottom: PreferredSize(
        child: lDivider(color: hide ? Colors.transparent : color_dedede),
        preferredSize: Size.fromHeight(0),
      ),
    );

Widget lCountText(String name, int current, int length, {TextStyle? style, TextStyle? pointStyle}) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      lText("$name (", style: style ?? st_b_16(fontWeight: FontWeight.w500)),
      lText("$current", style: pointStyle ?? st_16(textColor: Commons.getColor(), fontWeight: FontWeight.w500)),
      lText("/$length)", style: style ?? st_b_16(fontWeight: FontWeight.w500)),
    ],
  );
}

Widget lDingbatText({
  String text = ' ',
  Widget? child,
  double? dingbatSize,
  Color? dingbatColor,
  TextStyle? style,
  double? width,
  EdgeInsets padding = padding_0,
  TextOverflow? overflow,
}) {
  return Container(
      padding: padding,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(padding: padding_07_TB, child: lDot(size: dingbatSize, color: dingbatColor), alignment: Alignment.center),
          Container(
            width: width,
            child: child ?? lText(text, style: style ?? st_b_14(), overflow: overflow),
          )
        ],
      ));
}

/// 데이터 화면 처리
/// img - 아이콘 있을 경우
/// title - 타이틀 제목
/// content - 내용
/// textBtn - 버튼 사용시
/// textString - 텍스트만 사용시
Widget lEmptyView({
  bool isLoading = false,
  bool isIcon = true,
  String? img,
  String? title,
  String? content,
  String? textBtn,
  String? textString,
  EdgeInsets padding = padding_0,
  EdgeInsets paddingBtn = padding_30_LR,
  double? height,
  double? size,
  Function? fn,
}) {
  double imgSize = size ?? 62;
  String imgPath = img ?? Lcons.empty(size: imgSize).name;
  return isLoading
      ? Center()
      : Center(
          child: Container(
              height: height,
              padding: padding,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (isIcon) svgImage(imagePath: imgPath, size: imgSize),
                  Padding(
                    padding: EdgeInsets.only(top: 26),
                    child: lText(
                      title ?? "해당조건의데이터가없어요".tr(),
                      style: st_16(textColor: color_999999, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  if (content != null)
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 8, 0, 20),
                      child: lText(
                        content,
                        style: st_14(textColor: color_999999),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  //버튼 형식
                  if (textBtn != null)
                    lBtnWrap(
                      textBtn,
                      padding: paddingBtn,
                      btnEnableColor: Commons.getColor(),
                      textStyle: st_16(fontWeight: FontWeight.bold),
                      onClickAction: () {
                        if (fn != null) fn();
                      },
                    ),
                  //텍스트 형식
                  if (textString != null)
                    InkWell(
                      onTap: () {
                        if (fn != null) fn();
                      },
                      child: Padding(
                        padding: content == null ? padding_15_T : padding_0,
                        child: lText(
                          textString,
                          textAlign: TextAlign.center,
                          style: st_14(
                            textColor: Commons.getColor(),
                            fontWeight: FontWeight.w500,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ),
                    ),
                ],
              )),
        );
}

Widget getRankImg(int rank, {double radius = 28, bool showNumber = false}) {
  switch (rank) {
    case 1:
      return Image.asset(IMG_RANK_1, width: radius, height: radius);
    case 2:
      return Image.asset(IMG_RANK_2, width: radius, height: radius);
    case 3:
      return Image.asset(IMG_RANK_3, width: radius, height: radius);
    default:
      return showNumber
          ? Container(
              width: radius,
              alignment: Alignment.center,
              child: lText('$rank', style: st_b_22(fontWeight: FontWeight.w700)),
            )
          : Container();
  }
}

void lLoginCheck(BuildContext context, Function onClick, {String? message}) {
  if (auth.user.username.isNotEmpty) {
    onClick();
  } else {
    showNormalDlg(context: context, message: message ?? "회원전용서비스입니다".tr(), onClickAction: (action) => Commons.pageToMain(context, routeLogin));
  }
}

///금액 텍스트(결제금액+아이콘      1000 원or아이콘)
///isInfo - 우측에 설명 아이콘 사용:true, 사용안함:false
///
///isPoint - 포인트 아이콘 사용:true, 사용안함:false
///
///isCache - 캐시 아이콘  사용:true, 사용안함:false
///
///isString - '-' 사용 유무 포인트/캐시 차감시  사용:true, 사용안함:false
///
///isValue - 우측 아이콘 및 '원'문구 사용 여부 사용:false, 사용안함:true
Widget lPriceText({
  String title = '',
  double titleFontSize = 16,
  titleColor = color_222222,
  titleFontWeight = FontWeight.w500,
  int value = 0,
  double valueFontSize = 16,
  valueColor = color_222222,
  valueFontWeight = FontWeight.w500,
  double imageSize = 18,
  isInfo = false,
  infoColor = color_cecece,
  isPoint = false,
  isCache = false,
  isString = true,
  String valueString = '',
  isValue = false,
  padding = padding_10_B,
  Function? callBack,
}) {
  return Padding(
    padding: padding,
    child: Row(
      children: [
        lText(
          title,
          style: defaultStyle(
            color: titleColor,
            fontWeight: titleFontWeight,
            fontSize: titleFontSize,
          ),
        ),
        if (isInfo)
          Row(
            children: [
              sb_w_05,
              lInkWell(onTap: callBack != null ? () => callBack() : null, child: Lcons.info(color: color_cecece, isEnabled: true)),
            ],
          ),
        Expanded(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              lText(
                  isValue
                      ? valueString
                      : '${value == 0 || !isPoint && !isCache ? '' : isString ? '-' : ''} ${StringUtils.formatPay(value)}',
                  style: defaultStyle(fontSize: valueFontSize, color: valueColor, fontWeight: valueFontWeight)),
              if (!isValue)
                Row(
                  children: [
                    sb_w_05,
                    if (!isPoint && !isCache) lText("원".tr(), style: defaultStyle(fontSize: valueFontSize, color: valueColor, fontWeight: valueFontWeight)),
                    if (isPoint) Lcons.point(size: 15),
                    if (isCache) Lcons.cash(size: 15, isEnabled: true),
                  ],
                ),
            ],
          ),
        ),
      ],
    ),
  );
}

//
// ///포인트 텍스트(포인트      1000)
// Widget lPointTextIcon(int value, {Color textColor = color_545454, FontWeight fontWeight = FontWeight.w500, String imagePath = SVG_ICON_CASH, double imageSize = 18}) {
//   return Row(
//     crossAxisAlignment: CrossAxisAlignment.center,
//     mainAxisAlignment: MainAxisAlignment.end,
//     children: [
//       lText('${value == 0 ? '' : '-'} ${StringUtils.formatPay(value)}', style: st_16(textColor: textColor, fontWeight: fontWeight)),
//       sb_w_05,
//       svgImage(imagePath: imagePath, size: 18),
//     ],
//   );
// }
//
// ///캐시 텍스트(캐시      1000)
// Widget lCacheTextIcon(int value, {Color textColor = color_545454, FontWeight fontWeight = FontWeight.w500, String imagePath = SVG_ICON_CASH, double imageSize = 18}) {
//   return Row(
//     crossAxisAlignment: CrossAxisAlignment.center,
//     mainAxisAlignment: MainAxisAlignment.end,
//     children: [
//       lText('${value == 0 ? '' : '-'} ${StringUtils.formatPay(value)}', style: st_16(textColor: textColor, fontWeight: fontWeight)),
//       sb_w_05,
//       svgImage(imagePath: imagePath, size: 18),
//     ],
//   );
// }
Widget lTitleBodyView(
  String title,
  String body, {
  Color backgroundColor = Colors.white,
  Color borderColor = color_dedede,
  double borderRadius = 6.0,
  EdgeInsets padding = padding_15,
  TextStyle? titleStyle,
  TextStyle? bodyStyle,
  double thickness = 1,
  EdgeInsets margin = padding_10_TB,
}) {
  return Container(
      margin: margin,
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        lText(title, style: titleStyle ?? st_b_18(fontWeight: FontWeight.w700)),
        sb_h_20,
        lRoundContainer(
          padding: padding,
          alignment: Alignment.topLeft,
          child: lText(body, style: bodyStyle ?? st_b_14()),
          bgColor: backgroundColor,
          borderWidth: thickness,
          borderColor: borderColor,
          borderRadius: borderRadius,
        ),
      ]));
}

Widget lTextCircleIcon(String unit, {Color unitColor = color_222222, double radius = 18, double unitSize = 10, EdgeInsets padding = padding_0, Color backgroundColor = color_dedede}) {
  return Container(
    alignment: Alignment.center,
    width: radius,
    height: radius,
    padding: padding,
    margin: padding_03_L,
    decoration: BoxDecoration(color: backgroundColor, borderRadius: BorderRadius.circular(50), border: Border.all(color: color_dedede)),
    child: lText(unit, style: TextStyle(fontSize: unitSize, color: unitColor, fontWeight: FontWeight.w700), textAlign: TextAlign.center),
  );
}

class LinkmomRefresh extends StatelessWidget {
  final Widget child;
  final Function onRefresh;
  final EdgeInsets padding;
  final ScrollController? controller;
  final bool isExpanded;
  final bool isFull;
  final bool anotherScrollView;

  LinkmomRefresh({required this.child, required this.onRefresh, this.padding = padding_20, this.controller, this.isExpanded = false, this.isFull = false, this.anotherScrollView = false});

  double _indicatorSize = 50;
  final _helper = IndicatorStateHelper();
  ScrollDirection prevScrollDirection = ScrollDirection.idle;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: CustomRefreshIndicator(
          builder: (ctx, child, ctrl) {
            return Stack(
              children: [
                AnimatedBuilder(
                  animation: ctrl,
                  builder: (ctx, _) {
                    _helper.update(ctrl.state);
                    final containerHeight = ctrl.value * _indicatorSize;
                    return Container(
                      margin: padding_10,
                      alignment: Alignment.center,
                      height: containerHeight,
                      child: AnimatedContainer(
                        duration: const Duration(milliseconds: 150),
                        alignment: Alignment.center,
                        child: CircleIndicator(showText: false, size: _indicatorSize),
                      ),
                    );
                  },
                ),
                child
              ],
            );
          },
          onRefresh: () => Future.sync(() => onRefresh()),
          child: Container(
            child: anotherScrollView
                ? child
                : Container(
                    height: Commons.height(context, 0.9),
                    child: isExpanded
                        ? LayoutBuilder(builder: (context, constraints) {
                            return lScrollView(
                              controller: controller ?? PrimaryScrollController.of(context),
                              physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                              padding: padding,
                              child: ConstrainedBox(
                                constraints: BoxConstraints(minHeight: constraints.maxHeight - (isFull ? 0 : kToolbarHeight)),
                                child: IntrinsicHeight(child: Expanded(child: child)),
                              ),
                            );
                          })
                        : lScrollView(
                            controller: controller ?? PrimaryScrollController.of(context),
                            physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                            padding: padding,
                            child: child,
                          ),
                  ),
          )),
    );
  }
}

class Filter extends StatelessWidget {
  final DateTimeRange? range;
  final String? filterText;
  final int count;
  final Function onClickAction;
  final double height;

  const Filter({this.range, this.filterText, required this.onClickAction, this.count = 0, this.height = 50.0});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: height,
        padding: padding_20_LR,
        color: color_f3f3f3,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                child: Row(
              children: [
                if (range != null) lText('${StringUtils.formatDotYMD(range!.start)} ~ ${StringUtils.formatDotYMD(range!.end)}'),
                sb_w_05,
                count > 0
                    ? Container(
                        decoration: BoxDecoration(border: Border.all(color: color_545454), borderRadius: BorderRadius.circular(16)),
                        padding: EdgeInsets.fromLTRB(8, 3, 8, 3),
                        child: lText('$count${"건".tr()}', style: st_b_13(textColor: color_545454, fontWeight: FontWeight.w500)),
                      )
                    : Container(),
              ],
            )),
            TextButton(
                onPressed: () => onClickAction(),
                child: Row(
                  children: [Lcons.filter(color: color_222222, isEnabled: true, size: 18), sb_w_05, lText(filterText ?? "상세검색".tr(), style: st_b_14())],
                )),
          ],
        ));
  }
}

class RoundData extends StatelessWidget {
  final String title;
  final TextStyle? style;
  final Widget? bodyWidget;

  const RoundData({this.title = '', this.bodyWidget, this.style});

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      lText(title, style: style ?? st_b_14()),
      lRoundContainer(
        alignment: Alignment.centerLeft,
        margin: padding_10_TB,
        borderColor: color_dedede,
        bgColor: Colors.transparent,
        padding: padding_15,
        borderRadius: 8.0,
        child: bodyWidget,
      ),
    ]));
  }
}

class PreviousRequestListener extends StatefulWidget {
  final Widget child;
  final Function onPreviousRequest;

  PreviousRequestListener({required this.child, required this.onPreviousRequest});

  @override
  _PreviousRequestListenerState createState() => _PreviousRequestListenerState();
}

class _PreviousRequestListenerState extends State<PreviousRequestListener> {
  @override
  Widget build(BuildContext context) {
    return NotificationListener(
        onNotification: (s) {
          if (s is ScrollEndNotification) {
            if (s.metrics.pixels == s.metrics.minScrollExtent) widget.onPreviousRequest();
            return true;
          } else {
            return false;
          }
        },
        child: widget.child);
  }
}

class NextRequestListener extends StatefulWidget {
  final Widget child;
  final Function onNextRequest;

  NextRequestListener({required this.child, required this.onNextRequest});

  @override
  _NextRequestListenerState createState() => _NextRequestListenerState();
}

class _NextRequestListenerState extends State<NextRequestListener> {
  @override
  Widget build(BuildContext context) {
    return NotificationListener(
        onNotification: (s) {
          if (s is ScrollEndNotification) {
            if (s.metrics.pixels == s.metrics.maxScrollExtent) widget.onNextRequest();
            return true;
          } else {
            return false;
          }
        },
        child: widget.child);
  }
}

class MultiBtn extends StatefulWidget {
  final List<String>? btnTextlist;
  final String btnText;
  final Function? onClickAction;
  final List<Function>? onClickActions;
  final EdgeInsets? padding;
  final int? disableBtn;
  final List<Widget>? btnList;

  MultiBtn({this.btnTextlist, this.btnText = '', this.onClickAction, this.onClickActions, this.padding, this.disableBtn, this.btnList});

  @override
  _MultiBtnState createState() => _MultiBtnState();
}

class _MultiBtnState extends State<MultiBtn> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: widget.btnText.isEmpty
          ? Row(
              children: widget.btnList != null
                  ? widget.btnList!.map((e) => Flexible(child: e)).toList()
                  : widget.btnTextlist!
                      .map((e) => Flexible(
                              child: lBtn(
                            e,
                            margin: widget.padding ?? padding_05_LR,
                            onClickAction: widget.onClickActions![widget.btnTextlist!.indexOf(e)],
                            btnColor: _checkDisable(widget.btnTextlist!.indexOf(e)),
                          )))
                      .toList())
          : lBtn(
              widget.btnText,
              onClickAction: () => widget.onClickAction!(),
            ),
    );
  }

  Color _checkDisable(int idx) {
    if (widget.disableBtn != null && idx == widget.disableBtn) {
      return color_dedede;
    } else {
      return Commons.getColor();
    }
  }
}

void showRankInfo({USER_TYPE? type}) {
  String title = ' ';
  String level0 = ' ';
  String level1 = ' ';
  String level2 = ' ';
  String level3 = ' ';
  String level0Desc1 = ' ';
  String level1Desc1 = ' ';
  String level1Desc2 = ' ';
  String level2Desc1 = ' ';
  String level2Desc2 = ' ';
  String level3Desc1 = ' ';
  String level3Desc2 = ' ';
  if ((type ?? storageHelper.user_type) == USER_TYPE.mom_daddy) {
    title = "맘대디등급".tr();
    level0 = "맘대디_0".tr();
    level1 = "맘대디_1".tr();
    level2 = "맘대디_2".tr();
    level3 = "맘대디_3".tr();
    level0Desc1 = "맘대디_0_안내".tr();
    level1Desc1 = "맘대디_1_안내_1".tr();
    level1Desc2 = "맘대디_1_안내_2".tr();
    level2Desc1 = "맘대디_2_안내_1".tr();
    level2Desc2 = "맘대디_2_안내_2".tr();
    level3Desc1 = "맘대디_3_안내_1".tr();
    level3Desc2 = "맘대디_3_안내_2".tr();
  } else {
    title = "링크쌤등급".tr();
    level0 = "링크쌤_0".tr();
    level1 = "링크쌤_1".tr();
    level2 = "링크쌤_2".tr();
    level3 = "링크쌤_3".tr();
    level0Desc1 = "링크쌤_0_안내".tr();
    level1Desc1 = "링크쌤_1_안내_1".tr();
    level1Desc2 = "링크쌤_1_안내_2".tr();
    level2Desc1 = "링크쌤_2_안내_1".tr();
    level2Desc2 = "링크쌤_2_안내_2".tr();
    level3Desc1 = "링크쌤_3_안내_1".tr();
    level3Desc2 = "링크쌤_3_안내_2".tr();
  }
  showNormalDlg(
    //2021/11/30 오른족이 레이아웃이 맞지 않아 padding 으로 조정
    padding: EdgeInsets.fromLTRB(5, 25, 5, 20),
    messageWidget: Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          lText(title, style: st_b_18(fontWeight: FontWeight.w700)),
          Container(
              width: double.infinity,
              decoration: BoxDecoration(color: color_fafafa, border: Border.all(color: color_eeeeee)),
              padding: padding_20,
              margin: padding_10_TB,
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                _levelInfo(level0, desc1: level0Desc1),
                _levelInfo(level1, desc1: level1Desc1, desc2: level1Desc2, term1: "20만원".tr(), term2: "5건".tr()),
                _levelInfo(level2, desc1: level2Desc1, desc2: level2Desc2, term1: "50만원".tr(), term2: "10건".tr()),
                _levelInfo(level3, desc1: level3Desc1, desc2: level3Desc2, term1: "100만원".tr(), term2: "20건".tr()),
              ])),
          lText("등급집계".tr(), style: st_12(textColor: color_b2b2b2)),
        ],
      ),
    ),
  );
}

Widget _levelInfo(String title, {String desc1 = '', String desc2 = '', String term1 = ' ', String term2 = ' '}) {
  return Container(
      margin: padding_10_B,
      child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: [
        lText(title, style: st_b_14(textColor: Commons.getColor(), fontWeight: FontWeight.w700)),
        sb_h_05,
        desc2.isEmpty
            ? TextHighlight(text: desc1, term: term1, textStyle: st_b_14(), textStyleHighlight: st_b_14(fontWeight: FontWeight.w700))
            : Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextHighlight(text: desc1, term: term1, textStyle: st_b_14(), textStyleHighlight: st_14(fontWeight: FontWeight.w700, textColor: color_545454)),
                  TextHighlight(text: desc2, term: term2, textStyle: st_b_14(), textStyleHighlight: st_14(fontWeight: FontWeight.w700, textColor: color_545454)),
                ],
              )
      ]));
}

class CircleIndicator extends StatefulWidget {
  final bool showText;
  final double size;

  CircleIndicator({Key? key, this.showText = true, this.size = 66}) : super(key: key);

  @override
  _CircleIndicatorState createState() => _CircleIndicatorState();
}

class _CircleIndicatorState extends State<CircleIndicator> with SingleTickerProviderStateMixin {
  late AnimationController _indicatorCtrl;

  @override
  void initState() {
    super.initState();
    _indicatorCtrl = AnimationController(
      duration: const Duration(milliseconds: 1500),
      vsync: this,
    )..repeat();
  }

  @override
  void dispose() {
    _indicatorCtrl.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      alignment: Alignment.center,
      child: Wrap(
        direction: Axis.vertical,
        alignment: WrapAlignment.center,
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [
          AnimatedBuilder(
            animation: _indicatorCtrl,
            builder: (BuildContext context, Widget? child) {
              return Stack(alignment: Alignment.center, children: [
                Lcons.loading_round(size: widget.size),
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 10),
                  child: Lcons.loading_person(size: widget.size - 20),
                ),
                Transform.rotate(
                  angle: _indicatorCtrl.value * 2.0 * math.pi,
                  child: Lcons.loading_bar(size: widget.size, color: Commons.getColor()),
                ),
              ]);
            },
          ),
          if (widget.showText)
            Padding(
              padding: padding_15_T,
              child: lText(
                "잠시만요".tr(),
                style: st_14(textColor: color_b2b2b2, fontWeight: FontWeight.w500),
                textAlign: TextAlign.center,
              ),
            ),
        ],
      ),
    );
  }
}

void showScheduleTimePicker(BuildContext context, Function callback, {DateTime? start, DateTime? end, String? title, String? btnStr}) {
  DateTime sTime = start ?? StringUtils.now();
  DateTime eTime = end ?? StringUtils.now(later: 30);
  Duration difference = eTime.difference(sTime);
  showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      isDismissible: false,
      isScrollControlled: true,
      builder: (context) {
        return StatefulBuilder(builder: (BuildContext context, state) {
          return Container(
              padding: padding_25_T,
              decoration: BoxDecoration(
                color: color_white,
                borderRadius: BorderRadius.vertical(top: ui.Radius.circular(20)),
              ),
              child: Stack(
                children: [
                  Container(
                    child: Wrap(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 10, 0, 34),
                          child: eTime.difference(sTime) > Duration(minutes: 0)
                              ? Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                                  lText(title ?? "돌봄시간".tr(), style: st_b_20(fontWeight: FontWeight.w700)),
                                  sb_w_08,
                                  lText(
                                    (difference.inHours.remainder(60) > 0 ? '${difference.inHours.remainder(60)}${"시간".tr()}' : '') + '${difference.inMinutes.remainder(60)}${"분".tr()}',
                                    style: st_b_20(textColor: Commons.getColor(), fontWeight: FontWeight.w700),
                                    textAlign: TextAlign.center,
                                  )
                                ])
                              : Container(
                                  width: double.infinity,
                                  alignment: Alignment.center,
                                  child: lText("시간확인".tr(), style: st_b_20(fontWeight: FontWeight.w700)),
                                ),
                        ),
                        sb_h_10,
                        Container(
                            padding: EdgeInsets.symmetric(horizontal: 24),
                            child: Time24HPicker(
                              startTime: sTime,
                              endTime: eTime,
                              onChanged: (start, end) {
                                state(() {
                                  sTime = start;
                                  eTime = end;
                                  difference = eTime.difference(sTime);
                                  callback(DialogAction.update, start, end);
                                });
                              },
                            )),
                        lBtn(btnStr ?? "확인".tr(), btnColor: Commons.getColor(), isEnabled: (eTime.difference(sTime).inMinutes > 29), onClickAction: () {
                          Commons.pagePop(context);
                          callback(DialogAction.confirm, sTime, eTime);
                        }),
                      ],
                    ),
                  ),
                  Positioned(
                    right: 0,
                    top: 0,
                    child: Container(
                        padding: padding_05_R,
                        child: MaterialButton(
                          minWidth: 24,
                          onPressed: () {
                            Commons.pagePop(context);
                            callback(DialogAction.close, null, null);
                          },
                          child: Lcons.nav_close(size: 30, color: color_545454),
                        )),
                  )
                ],
              ));
        });
      });
}

void showRepeatSelecter(
  BuildContext context,
  DateTime endDate,
  Function callback, {
  Map<DateTime, List<int>>? originEvents,
  int end = 30,
  int index = 0,
  String info = '',
  String infoHighlight = '',
}) {
  PanelController ctrl = PanelController();
  List<DateTime> update = originEvents!.keys.toList();
  showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      isDismissible: false,
      backgroundColor: Colors.transparent,
      barrierColor: Colors.transparent,
      builder: (context) {
        return StatefulBuilder(builder: (context, setter) {
          return SlidingUpPanel(
            controller: ctrl,
            minHeight: MediaQuery.of(context).size.height * 0.1,
            maxHeight: MediaQuery.of(context).size.height * 0.7,
            defaultPanelState: PanelState.OPEN,
            color: Colors.white,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(24.0), topRight: Radius.circular(24.0)),
            panel: Container(
              padding: padding_20,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: lScrollView(
                      padding: padding_0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: padding_10_B,
                            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.start, children: [
                              Column(mainAxisAlignment: MainAxisAlignment.start, children: [
                                TextHighlight(text: "매주반복안내".tr(), term: "매주반복안내_하이라이트1".tr(), textStyleHighlight: st_b_14(fontWeight: FontWeight.w700), textStyle: st_b_14()),
                                TextHighlight(text: info, term: infoHighlight, textStyleHighlight: st_b_14(fontWeight: FontWeight.w700), textStyle: st_b_14()),
                              ]),
                              lInkWell(
                                  child: Lcons.nav_close(),
                                  onTap: () {
                                    Commons.pagePop(context);
                                    callback(DialogAction.close, originEvents);
                                  })
                            ]),
                          ),
                          lInkWell(
                            onTap: () => {},
                            child: Container(
                              padding: padding_10_B,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  lText("종료날짜".tr(), style: st_b_18(fontWeight: FontWeight.w700)),
                                  sb_h_05,
                                  lText(StringUtils.parseYMDW(endDate),
                                      style: st_b_16(
                                        textColor: Commons.getColor(),
                                        fontWeight: FontWeight.w500,
                                        decoration: TextDecoration.underline,
                                        decorationColor: Commons.getColor(),
                                      )),
                                ],
                              ),
                            ),
                          ),
                          RepeatSelecter(
                            onEventUpdate: (events) {
                              setter(() {
                                update = events;
                                callback(DialogAction.update, events);
                              });
                            },
                            selectCallback: (date, events) {
                              setter(() {
                                endDate = date;
                                callback(DialogAction.update, update);
                              });
                              return true;
                            },
                            onAlertAction: (a) {
                              switch (a) {
                                case DialogAction.delete:
                                  callback(DialogAction.fail, []);
                                  break;
                                default:
                                  break;
                              }
                            },
                            mode: Selecter.repeat,
                            colors: [Commons.isLinkMom() ? colorList[index] : color_main],
                            event: originEvents,
                            end: end,
                            duration: Duration(days: 372),
                            onCalendarChanged: (focused) => {},
                          ),
                        ],
                      ),
                    ),
                  ),
                  lBtn("확인".tr(), margin: padding_20_T, onClickAction: () {
                    Commons.pagePop(context);
                    callback(DialogAction.confirm, update);
                  }),
                ],
              ),
            ),
          );
        });
      });
}

///필수입력항목
Widget requiredText({String value = '*', TextStyle? style}) {
  return lText(value, style: st_b_16(textColor: color_ff3b30));
}

void showCancelAlert(Function onClickAction, {bool diaryClaimView = false}) {
  showNormalDlg(
    messageWidget: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(crossAxisAlignment: CrossAxisAlignment.start, mainAxisAlignment: MainAxisAlignment.start, children: [
            Container(
              width: 40,
              height: 40,
              padding: padding_05,
              margin: EdgeInsets.fromLTRB(0, 0, 10, 20),
              decoration: BoxDecoration(
                color: color_fa784f,
                shape: BoxShape.circle,
              ),
              child: Lcons.warning_sticker(),
            ),
            Container(
                width: widthFull(navigatorKey.currentContext!),
                height: 50,
                constraints: BoxConstraints(
                  //2021/11/30 상단 텍스트가 오른쪽으로 나가서 깨질 경우 하단 maxWidth 크기 수정  05 -> 0.46 변경
                  maxWidth: widthFull(navigatorKey.currentContext!) * 0.46,
                ),
                child: lAutoSizeText(diaryClaimView ? "정산보류요청시".tr() : "상대방사유로취소시".tr(), style: st_b_16(fontWeight: FontWeight.w700), maxLines: 2)),
          ]),
          lText(diaryClaimView ? "정산보류로분쟁시".tr() : "상대방사유취소로분쟁시".tr(), style: st_13(textColor: color_b2b2b2)),
          sb_h_20,
          lText(diaryClaimView ? "정산보류가합의".tr() : "상대방사유취소가합의".tr(), style: st_b_14(fontWeight: FontWeight.w700)),
          sb_h_05,
          RichText(
              text: TextSpan(children: [
            TextSpan(text: "취소시점기준".tr(), style: st_b_14()),
            TextSpan(text: "5일이내".tr(), style: st_14(textColor: Commons.getColor())),
            TextSpan(text: "합의내용을".tr(), style: st_b_14()),
            TextSpan(text: "벌점을다시차감".tr(), style: st_14(textColor: Commons.getColor())),
            TextSpan(text: "해드립니다".tr(), style: st_b_14()),
          ])),
          sb_h_20,
          lText(diaryClaimView ? "정산보류가합의되지않은".tr() : "상대방사유취소가합의되지않은".tr(), style: st_b_14(fontWeight: FontWeight.w700)),
          sb_h_05,
          RichText(
              text: TextSpan(children: [
            TextSpan(text: "취소시점기준".tr(), style: st_b_14()),
            TextSpan(text: "5일이내".tr(), style: st_14(textColor: Commons.getColor())),
            TextSpan(text: "합의가이루어지지".tr(), style: st_b_14()),
            TextSpan(text: "그대로부과".tr(), style: st_14(textColor: Commons.getColor())),
            TextSpan(text: "외부기관에".tr(), style: st_b_14()),
          ])),
          sb_h_20,
          lDingbatText(text: "부과된벌점으로인해".tr(), style: st_13(textColor: color_b2b2b2), dingbatSize: 2, dingbatColor: color_b2b2b2)
        ],
      ),
    ),
    btnLeft: "취소".tr(),
    btnRight: "다음".tr(),
    onClickAction: (a) => onClickAction(a),
  );
}

Widget rankEmptyView(BuildContext context, DataManager? data, bool isLoading, {String? title, bool isHome = false}) {
  return lEmptyView(
    padding: isHome ? padding_20_TB : padding_0,
    isLoading: isLoading,
    isIcon: isHome ? false : true,
    img: isHome ? null : Lcons.no_data_ranking().name,
    title: title ?? "랭킹에도전해보세요".tr(),
    content: isHome ? null : "데이터없음_랭킹내용".tr(),
    textString: isHome && !Commons.isLinkMom()
        ? null
        : auth.indexInfo.linkmominfo.isBookingjobs
            ? "맘대디찾기".tr()
            : "링크쌤지원하기".tr(),
    paddingBtn: padding_50_LR,
    fn: () {
      if (!Commons.isLinkMom()) {
        Commons.setModeChange(changeType: USER_TYPE.link_mom);
      }
      if (auth.indexInfo.linkmominfo.isBookingjobs) {
        Commons.pageToMainTabMove(context, tabIndex: TAB_LIST);
      } else {
        Commons.page(context, routeCare1, arguments: CareStep1Page(data: data ?? DataManager()));
      }
    },
  );
}

class SliverFilterLists extends StatelessWidget {
  final Widget? topper;
  final Widget? filter;
  final Widget? footer;
  final Function? onNext;
  final Function? onRefresh;

  const SliverFilterLists({Key? key, this.topper, this.filter, this.footer, this.onNext, this.onRefresh}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NextRequestListener(
      onNextRequest: onNext ?? () => {},
      child: LinkmomRefresh(
          anotherScrollView: true,
          onRefresh: onRefresh ?? () => {},
          child: CustomScrollView(
            physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
            slivers: <Widget>[
              if (topper != null) SliverToBoxAdapter(child: topper),
              if (filter != null)
                SliverAppBar(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent,
                  foregroundColor: Colors.transparent,
                  pinned: true,
                  flexibleSpace: filter,
                ),
              if (footer != null) SliverToBoxAdapter(child: footer),
            ],
          )),
    );
  }
}

class CachedImage extends StatelessWidget {
  final String url;
  final double indicatorSize;
  final double width;
  final double height;

  const CachedImage(this.url, {this.indicatorSize = 30, this.width = 64, this.height = 64});

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: this.url,
      fit: BoxFit.cover,
      width: width,
      height: height,
      placeholder: (context, url) => CircleIndicator(
        showText: false,
        size: this.indicatorSize,
      ),
    );
  }
}

Widget lineStepIndicator(
  int max,
  int current, {
  Color selected = color_00cfb5,
  Color unselected = color_f1f1f5,
  double padding = 0,
}) {
  return StepProgressIndicator(
    totalSteps: max,
    currentStep: current,
    padding: padding,
    selectedColor: selected,
    unselectedColor: unselected,
  );
}

void showBottomSelectBox(BuildContext context, {required dynamic list, FixedExtentScrollController? ctrl, required Function(dynamic) onChanged, double height = 300}) {
  showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) {
        return Container(
            color: Colors.white,
            height: height,
            child: Column(children: [
              Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                CupertinoButton(
                  child: lText("완료".tr(), style: st_15(fontWeight: FontWeight.w500, textColor: Colors.blue)),
                  onPressed: () => Commons.pagePop(context),
                ),
              ]),
              Expanded(
                  child: CupertinoPicker(
                      scrollController: ctrl,
                      useMagnifier: true,
                      onSelectedItemChanged: (idx) {
                        onChanged(idx);
                      },
                      itemExtent: 50,
                      children: list
                          .map<Widget>((e) => Container(
                              alignment: Alignment.center,
                              child: lText(
                                e.toString(),
                                style: st_18(textColor: color_545454, fontWeight: FontWeight.w500),
                              )))
                          .toList()))
            ]));
      });
}

Widget bubbleModal({
  String message = '',
  Widget? child,
  double? width,
  double? height,
  EdgeInsets? padding,
  Key? key,
  bool showPin = true,
  Color color = color_main,
}) {
  return Stack(
    key: key,
    alignment: Alignment.bottomCenter,
    children: [
      Container(
        width: width,
        height: height,
        padding: padding ?? padding_10,
        margin: padding_05_B,
        decoration: BoxDecoration(
          border: Border.all(color: color),
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
        ),
        child: child ?? lText(message),
      ),
      if (showPin)
        Transform.rotate(
          angle: 3.14 / 4,
          child: Container(
            width: 10,
            height: 10,
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: color), right: BorderSide(color: color)),
              color: Colors.white,
            ),
          ),
        ),
    ],
  );
}

Widget imageThumbnailView(
  BuildContext context,
  List<String> images, {
  double height = 122,
  double width = 122,
  Function? onClickAction,
  List<String>? origin,
}) {
  return Container(
      width: double.infinity,
      height: height,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: images
            .map((e) => e.isEmpty
                ? Container()
                : FittedBox(
                    fit: BoxFit.cover,
                    child: Container(
                      margin: padding_10_R,
                      width: width,
                      height: height,
                      child: lInkWell(
                        onTap: () {
                          onClickAction == null ? Commons.nextPage(context, fn: () => FullPhotoPage(index: images.indexOf(e), urlList: origin)) : onClickAction();
                        },
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.network(
                              e,
                              fit: BoxFit.cover,
                              loadingBuilder: (ctx, child, event) => event != null
                                  ? CircleIndicator(
                                      size: 40,
                                      showText: false,
                                    )
                                  : child,
                            )),
                      ),
                    ),
                  ))
            .toList(),
      ));
}

class RefreshView extends StatelessWidget {
  final Widget child;
  final Function onRefresh, onNext;
  final EdgeInsets padding;
  final ScrollController? controller;
  final bool isExpanded;
  final bool isFull;
  final bool anotherScrollView;

  RefreshView({required this.child, required this.onRefresh, this.padding = padding_20, this.controller, this.isExpanded = false, this.isFull = false, this.anotherScrollView = false, required this.onNext});

  double _indicatorSize = 50;
  ScrollDirection prevScrollDirection = ScrollDirection.idle;

  @override
  Widget build(BuildContext context) {
    return NextRequestListener(
      onNextRequest: onNext,
      child: Container(
        child: CustomRefreshIndicator(
          builder: (ctx, child, ctrl) {
            return Stack(
              children: [
                AnimatedBuilder(
                  animation: ctrl,
                  builder: (ctx, _) {
                    final containerHeight = ctrl.value * _indicatorSize;
                    return Container(
                      margin: padding_10,
                      alignment: Alignment.center,
                      height: containerHeight,
                      child: AnimatedContainer(
                        duration: const Duration(milliseconds: 150),
                        alignment: Alignment.center,
                        child: CircleIndicator(showText: false, size: _indicatorSize),
                      ),
                    );
                  },
                ),
                child
              ],
            );
          },
          onStateChanged: (IndicatorStateChange change) {
            if (change.didChange(from: IndicatorState.dragging, to: IndicatorState.armed)) {
              prevScrollDirection = ScrollDirection.forward;
            } else if (change.didChange(to: IndicatorState.idle)) {
              prevScrollDirection = ScrollDirection.idle;
            }
          },
          onRefresh: () => Future.sync(() => onRefresh()),
          child: child,
        ),
      ),
    );
  }
}

///-돌봄관리, 돌봄채팅, 돌봄신청에서 사용
Widget expiredView({
  int status = 0, //구인중
  String? name,
  String? expire,
  bool isIcons = true,
  double height = 44,
  EdgeInsets padding = padding_20_LR,
  Alignment alignment = Alignment.centerLeft,
  Color bgColor = color_eeeeee,
}) {
  BookingStatus bookingStatus = EnumUtils.getBookingStatus(value: status);
  return bookingStatus.isExpire
      ? Container(
          color: Commons.getColor().withOpacity(0.08),
          padding: padding,
          child: Row(
            children: [
              if (isIcons)
                Padding(
                  padding: padding_10_R,
                  child: lTextBtn(
                    name ?? bookingStatus.name,
                    padding: EdgeInsets.fromLTRB(10, 4, 10, 4),
                    style: st_13(textColor: color_999999, fontWeight: FontWeight.bold),
                    bg: bgColor,
                    border: Border.all(color: color_dedede),
                  ),
                ),
              Expanded(
                child: Container(
                  height: height,
                  alignment: alignment,
                  child: lAutoSizeText(expire ?? bookingStatus.expire, style: st_13(textColor: Commons.getColor(isExpire: true))),
                ),
              ),
            ],
          ),
        )
      : Center();
}

Widget emptySignature(String url) {
  return url.isEmpty ? Center(child: lText("서명을기다리고있어요".tr(), style: st_10(textColor: color_b2b2b2))) : placeholder();
}

Widget rankTransition(int data) {
  Widget icon = Icon(Icons.arrow_drop_up, color: color_f8859a, size: 15);
  if (data == 999) {
    return Padding(
      padding: padding_05_T,
      child: lText(
        'NEW',
        style: st_10(fontWeight: FontWeight.w500, textColor: color_ffc064),
      ),
    );
  } else if (data > 0) {
    return lIconText(
      icon: icon,
      title: '$data',
      textStyle: st_12(fontWeight: FontWeight.w500, textColor: color_f8859a),
      isExpanded: false,
      padding: padding_02_R,
      align: MainAxisAlignment.center,
    );
  } else if (data < 0) {
    icon = Icon(Icons.arrow_drop_down, color: color_87d6f9, size: 15);
    return lIconText(
      icon: icon,
      title: '$data',
      textStyle: st_12(fontWeight: FontWeight.w500, textColor: color_87d6f9),
      isExpanded: false,
      padding: padding_02_R,
      align: MainAxisAlignment.center,
    );
  } else {
    return lText('-', style: st_20(textColor: color_c4c4c4, fontWeight: FontWeight.bold));
  }
}
