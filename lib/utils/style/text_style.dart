import 'package:flutter/material.dart';

import 'color_style.dart';

///20px 돌봄신청 컨텐츠 제목(피룡한 돌봄 유형을 선택해주세요)
TextStyle stAppBarTitle = defaultStyle(fontSize: 20.0, color: color_222222, fontWeight: FontWeight.w500);
TextStyle stBadge = defaultStyle(fontSize: 10.0, color: color_white, fontWeight: FontWeight.bold);
TextStyle stRegister = defaultStyle(fontSize: 14.0, color: color_222222, fontWeight: FontWeight.bold);
TextStyle stRegisterEmail = defaultStyle(fontSize: 14.0, color: color_222222);
TextStyle stRegisterEmailDisable = defaultStyle(fontSize: 14.0, color: Colors.grey);
TextStyle stLogin = defaultStyle(fontSize: 16.0, color: Colors.black);
TextStyle stLoginBtn = defaultStyle(fontSize: 20, color: color_white);
TextStyle stLoginAccountBtn = defaultStyle(fontSize: 13, fontWeight: FontWeight.bold);
TextStyle stLoginRegisterBtn = defaultStyle(color: gradientBackgroundColorEnd, fontSize: 13, fontWeight: FontWeight.bold);
TextStyle stPwForgot = defaultStyle(fontSize: 14, fontWeight: FontWeight.bold);
TextStyle stRegisterComplete = defaultStyle(fontSize: 24, color: color_222222, fontWeight: FontWeight.bold);
TextStyle stRegisterCompleteContent = defaultStyle(fontSize: 18, color: color_222222);
TextStyle stRegisterCompleteIdContent = defaultStyle(fontSize: 18, color: Colors.blue, decoration: TextDecoration.underline);
TextStyle stRegisterCompleteSelect = defaultStyle(fontSize: 18, color: color_222222, fontWeight: FontWeight.bold);
TextStyle stNext = defaultStyle(fontSize: 18, color: color_222222, fontWeight: FontWeight.bold);
TextStyle stFindBtn = defaultStyle(color: color_999999, fontSize: 14);
TextStyle stPreview = defaultStyle(color: color_cecece, fontSize: 14);
TextStyle stSubTitle = defaultStyle(color: color_222222, fontSize: 22, fontWeight: FontWeight.w700);
TextStyle stAgrTitle = defaultStyle(color: color_222222, fontSize: 16, fontWeight: FontWeight.w700);
TextStyle stRegisterHint = defaultStyle(color: color_b2b2b2, fontSize: 16);
TextStyle stFindResult = defaultStyle(color: color_222222, fontSize: 20, fontWeight: FontWeight.w500);
TextStyle stCashTitle = defaultStyle(color: color_222222, fontSize: 16, fontWeight: FontWeight.w400);
TextStyle stAgree = defaultStyle(color: color_222222, fontSize: 14, fontWeight: FontWeight.w400);

///뒤로가기 종료 스타일
TextStyle stBack = defaultStyle(fontSize: 15, color: color_white);

///환경인증
TextStyle tvBgTop = defaultStyle(fontSize: 16, color: Colors.grey);
TextStyle tvBgTitle = defaultStyle(fontSize: 18, color: color_222222);
TextStyle tvListTitle = defaultStyle(fontSize: 18, color: color_222222);

///하이라이트 텍스트 스타일
TextStyle tvBgHighlight = defaultStyle(fontSize: 16, color: Colors.red, fontWeight: FontWeight.bold);
TextStyle tvBgBody = defaultStyle(fontSize: 16, color: Colors.black87, height: 1.5);

TextStyle bodyStyle = defaultStyle(fontSize: 18, color: Colors.black87);

TextStyle childStyle() => st_16(textColor: color_191919);

TextStyle careTypeStyle({
  bool isEnable = false,
  double fontSize = 20,
  FontWeight fontWeight = FontWeight.normal,
}) =>
    defaultStyle(fontSize: fontSize, color: isEnable ? color_main : color_white, fontWeight: fontWeight);

TextStyle placeStyle({
  bool isEnable = false,
  double fontSize = 20,
  FontWeight fontWeight = FontWeight.normal,
}) =>
    defaultStyle(fontSize: fontSize, color: isEnable ? color_main : color_white, fontWeight: fontWeight);

const double textHeight = 1.3;

TextStyle defaultStyle({
  bool isEnable = false,
  double fontSize = 14.0,
  Color color = color_textDisable,
  Color colorDis = color_btnDisable,
  FontWeight fontWeight = FontWeight.normal,
  double letterSpacing = -0.7,
  TextDecoration? decoration,
  Color? decorationColor,
  double height = textHeight,
}) {
  return TextStyle().copyWith(
    fontFamily: 'NotoSansCJKkr',
    fontSize: fontSize,
    color: isEnable
        ? colorDis == null
            ? color_textEnable
            : colorDis
        : color,
    fontWeight: fontWeight,
    letterSpacing: letterSpacing,
    decoration: decoration,
    decorationColor: decorationColor,
    height: height,
  );
}

///공통 하단 버튼 스타일 (16 size)
TextStyle stBtnBottom({
  bool isEnable = false,
  double fontSize = 16,
  Color textColor = color_textEnable,
  Color textDisColor = color_textDisable,
  FontWeight fontWeight = FontWeight.bold,
}) =>
    defaultStyle(fontSize: fontSize, color: isEnable ? textColor : textDisColor, fontWeight: fontWeight);

TextStyle st_01(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 01, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_02(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 02, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_03(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 03, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_04(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 04, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_05(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 05, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_06(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 06, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_07(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 07, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_08(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 08, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_09(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 09, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_10(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 10, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_11(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 11, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_12(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 12, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_13(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 13, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_14(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 14, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_15(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 15, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_16(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 16, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_17(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 17, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_18(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 18, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_19(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 19, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_20(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 20, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_21(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 21, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_22(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 22, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_23(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 23, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_24(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 24, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_25(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 25, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_26(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 26, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_27(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 27, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_28(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 28, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_29(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 29, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_30(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 30, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_31(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 31, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_32(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 32, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_33(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 33, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_34(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 34, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_35(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 35, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_36(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 36, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_37(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 37, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_38(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 38, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_39(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 39, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_40(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 40, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_41(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 41, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_42(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 42, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_43(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 43, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_44(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 44, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_45(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 45, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_46(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 46, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_47(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 47, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_48(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 48, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_49(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 49, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_50(
        {bool isSelect = false,
        Color textColor = color_white,
        Color disableColor = color_aaaaaa,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 50, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle default_b_Style(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 20, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_01(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 01, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_02(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 02, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_03(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 03, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_04(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 04, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_05(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 05, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_06(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 06, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_07(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 07, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_08(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 08, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_09(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 09, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_10(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 10, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_11(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 11, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_12(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 12, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_13(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 13, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_14(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 14, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_15(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 15, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_16(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 16, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_17(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 17, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_18(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 18, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_19(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 19, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_20(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 20, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_21(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 21, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_22(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 22, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_23(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 23, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_24(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 24, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_25(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 25, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_26(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 26, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_27(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 27, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_28(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 28, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_29(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 29, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_30(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 30, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_31(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 31, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_32(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 32, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_33(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 33, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_34(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 34, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_35(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 35, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_36(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 36, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_37(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 37, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_38(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 38, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_39(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 39, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_40(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 40, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_41(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 41, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_42(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 42, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_43(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 43, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_44(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 44, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_45(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 45, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_46(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 46, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_47(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 47, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_48(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 48, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_49(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 49, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_b_50(
        {bool isSelect = false,
        Color textColor = color_222222,
        Color disableColor = color_white,
        FontWeight fontWeight = FontWeight.normal,
        double letterSpacing = -0.7,
        TextDecoration? decoration,
        Color? decorationColor,
        double height = textHeight}) =>
    defaultStyle(fontSize: 50, color: isSelect ? disableColor : textColor, fontWeight: fontWeight, letterSpacing: letterSpacing, decoration: decoration, decorationColor: decorationColor, height: height);

TextStyle st_modify = defaultStyle(fontSize: 15, color: color_main);

TextStyle labelFormStyle = defaultStyle(fontSize: 20, color: color_b2b2b2);

TextStyle labelFormStyle_cf = defaultStyle(fontSize: 20, color: color_00cfb5);

TextStyle authButton = defaultStyle(fontSize: 14, color: color_00cfb5);

TextStyle formStyle = defaultStyle(fontSize: 14, color: color_aaaaaa);

TextStyle labelFormStyle14 = defaultStyle(fontSize: 14, color: color_b2b2b2);

TextStyle stSubText = defaultStyle(fontSize: 16, color: color_545454);

TextStyle stErrorText = defaultStyle(fontSize: 12, color: color_error);

TextStyle stSuccessText = defaultStyle(fontSize: 12, color: color_success);
