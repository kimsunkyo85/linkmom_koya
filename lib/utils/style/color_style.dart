
import 'package:flutter/material.dart';

//100% — FF// 99% — FC// 98% — FA// 97% — F7// 96% — F5// 95% — F2// 94% — F0// 93% — ED// 92% — EB// 91% — E8// 90% — E6
// 89% — E3// 88% — E0// 87% — DE// 86% — DB// 85% — D9// 84% — D6// 83% — D4// 82% — D1// 81% — CF// 80% — CC// 79% — C9
// 78% — C7// 77% — C4// 76% — C2// 75% — BF// 74% — BD// 73% — BA// 72% — B8// 71% — B5// 70% — B3// 69% — B0// 68% — AD
// 67% — AB// 66% — A8// 65% — A6// 64% — A3// 63% — A1// 62% — 9E// 61% — 9C// 60% — 99// 59% — 96// 58% — 94// 57% — 91
// 56% — 8F// 55% — 8C// 54% — 8A// 53% — 87// 52% — 85// 51% — 82// 50% — 80// 49% — 7D// 48% — 7A// 47% — 78// 46% — 75
// 45% — 73// 44% — 70// 43% — 6E// 42% — 6B// 41% — 69// 40% — 66// 39% — 63// 38% — 61// 37% — 5E// 36% — 5C// 35% — 59
// 34% — 57// 33% — 54// 32% — 52// 31% — 4F// 30% — 4D// 29% — 4A// 28% — 47// 27% — 45// 26% — 42// 25% — 40// 24% — 3D
// 23% — 3B// 22% — 38// 21% — 36// 20% — 33// 19% — 30// 18% — 2E// 17% — 2B// 16% — 29// 15% — 26// 14% — 24// 13% — 21
// 12% — 1F// 11% — 1C// 10% — 1A// 9% — 17// 8% — 14// 7% — 12// 6% — 0F// 5% — 0D// 4% — 0A// 3% — 08// 2% — 05// 1% — 03// 0% — 00
///appBar Title, 이전, 닫기 버튼 등 const color_ = const Color(0xff);
const color_transparent = Color(0x00000000);
const color_white = Color(0xffffffff);
const color_black = Color(0xff000000);
const color_black_20 = Color(0x33000000);
const color_highlight = Color(0x80191919);
const color_191919 = Color(0xff191919);
const color_060606 = Color(0x03060606);
const color_00cfb5 = Color(0xff00cfb5);
const color_f1f1f5 = Color(0xfff1f1f5);
const color_ffffff = Color(0xffffffff);
const color_f8f8fa = Color(0xfff8f8fa);
const color_dbdbdb = Color(0xffdbdbdb);
const color_80dbdbdb = Color(0x80dbdbdb);
const color_e0e0e0 = Color(0xffe0e0e0);
const color_eeeeee = Color(0xffeeeeee);
const color_767676 = Color(0xff767676);
const color_e5e5e5 = Color(0xffe5e5e5);
const color_aaaaaa = Color(0xffaaaaaa);
const color_d6d6d6 = Color(0xffd6d6d6);
const color_999999 = Color(0xff999999);
const color_fe6060 = Color(0xfffe6060);
const color_707070 = Color(0x66707070);
const color_ededed = Color(0xffededed);
const color_d4d4d4 = Color(0x5Cd4d4d4);
const color_d2d2d2 = Color(0xffd2d2d2);
const color_161616 = Color(0xff161616);
const color_f37370 = Color(0xfff37370);
const color_b2b2b2 = Color(0xffb2b2b2);
const color_80ffffff = Color(0x80ffffff);
const color_61e5e5ea = Color(0x61e5e5ea);
const color_f4f4f4 = Color(0xfff4f4f4);
const color_979797 = Color(0xff979797);
const color_82f8f8f8 = Color(0xd1f8f8f8);
const color_ff3b30 = Color(0xffff3b30);
const color_545454 = Color(0xff545454);
const color_222222 = Color(0xff222222);
const color_dfe3e4 = Color(0xffdfe3e4);
const color_4de5e5ea = Color(0x4de5e5ea);
const color_030303 = Color(0xff030303);
const color_dedede = Color(0xffdedede);
const color_eafbf8 = Color(0xffeafbf8);
const color_5f8f3 = Color(0xffe5f8f3);
const color_ede2fb = Color(0xffede2fb);
const color_daf5f0 = Color(0xffdaf5f0);
const color_cecece = Color(0xffcecece);
const color_b579c8 = Color(0xffb579c8);
const color_0085ff = Color(0xff0085ff);
const color_f1f5f5 = Color(0xfff1f5f5);
const color_ff3093 = Color(0xffff3093);
const color_c4c4c4 = Color(0xffc4c4c4);
const color_00b2ff = Color(0xff00b2ff);
const color_87d6f9 = Color(0xff87d6f9);
const color_f8859a = Color(0xfff8859a);
const color_ffc064 = Color(0xffffc064);
const color_ffbb56 = Color(0xffffbb56);
const color_a8d42a = Color(0xffa8d42a);
const color_faf6fc = Color(0xfffaf6fc);
const color_f2faf9 = Color(0xfff2faf9);
const color_error = Color(0xffff3b30);
const color_success = Color(0xff56d5e7);
const color_e4f1f0 = Color(0xffe4f1f0);
const color_ede6ef = Color(0xffede6ef);
const color_6bd5eae8 = Color(0x6bD5EAE8);
const color_fbf8f3 = Color(0xfffbf8f3);
const color_f7f3f9 = Color(0xfff7f3f9);
const color_a485ad = Color(0xffa485ad);
const color_eef8f6 = Color(0xffeef8f6);
const color_75b4ac = Color(0xff75b4ac);
const color_9e9e9e = Color(0xff9e9e9e);
const color_f6f6f6 = Color(0xfff6f6f6);
const color_75d74f = Color(0xff75d74f);
const color_f19a23 = Color(0xfff19a23);
const color_ec4258 = Color(0xffec4258);
const color_ff4c1b = Color(0xffff4c1b);
const color_f26a63 = Color(0xfff26a63);
const color_f66962 = Color(0xfff66962);
const color_f0544c = Color(0xfff0544c);
const color_cae1de = Color(0xffcae1de);
const color_fceeb2 = Color(0xfffceeb2);
const color_f9f9f9 = Color(0xfff9f9f9);
const color_e4f5f4 = Color(0xffe4f5f4);
const color_00b09a = Color(0xff00B09A);
const color_a26db2 = Color(0xffA26DB2);
const color_fcf4f6 = Color(0xfffcf4f6);
const color_f1faf8 = Color(0xfff1faf8);
const color_b9bcbe = Color(0xffb9bcbe);
const color_808080 = Color(0xff808080);
const color_057668 = Color(0xff057668);
const color_7F4B8F = Color(0xff7F4B8F);
const color_9edd6d = Color(0xff93dd6d);
const color_faa139 = Color(0xfffaa139);
const color_fa784f = Color(0xfffa784f);
const color_80ada7 = Color(0xff80ada7);
const color_a0c6c1 = Color(0xffA0C6C1);
const color_f8e100 = Color(0xffF8E100);
const color_402522 = Color(0xff402522);
const color_727272 = Color(0xff727272);
const color_e0cfe5 = Color(0xffe0cfe5);
const color_e5e5ea = Color(0xffe5e5ea);
const color_f5f5f5 = Color(0xfff5f5f5);
const color_f3f3f3 = Color(0xfff3f3f3);
const color_d8afe5 = Color(0xffd8afe5);
const color_747474 = Color(0xff747474);

/// without opacity from color_32eeeeee
const color_fafafa = Color(0xfffafafa);
const color_ab81b8 = Color(0xffab81b8);

///채팅 bg
const color_f6fff8 = Color(0xffF6EFF8);
const color_effbf8 = Color(0xffEFFBF8);
const color_f8f8f8 = Color(0xffF8F8F8);

///기본 메인 컬러
const color_main = color_00cfb5;

///맘대디
const color_momdady = color_main;

///링크쌤 색상 color_b579c8
const color_linkmom = color_b579c8;

///app Bar Title
const color_appBarTitle = color_191919;

///기본 제목
const color_title = color_191919;

///기본 버튼 배경 활성화 mainColor color_00cfb5
const color_btnEnable = color_main;

///기본 버튼 배경 비활성화 ///기본 버튼 배경 비활성화
const color_btnDisable = color_dfe3e4;

///기본 텍스트 활성화 color_ffffff
const color_textEnable = color_ffffff;

///기본 텍스트 비활성화 color_191919
const color_textDisable = color_191919;

const color_tileBackground = Color(0xFFF1F1F1);
const chuckyJokeBackgroundColor = Color(0xFFF1F1F1);
const chuckyJokeWaveBackgroundColor = Color(0xFFA8184B);
const gradientBackgroundColorEnd = Color(0xFF81baac);
const gradientBackgroundColorWhite = Color(0xFFFFFFFF);
const mainAppFontColor = Color(0xFF4D0F29);
const appBarBackGroundColor = Color(0xFF4D0F28);
const categoriesBackGroundColor = Color(0xFFA8184B);
const hintColor = Color(0xFF4D0F29);
const mainAppColor = Color(0xFFa2e9d8);
const gradientBackgroundColorStart = Color(0xFFa2e9d8);
const popupItemBackColor = Color(0xFFDADADB);

const color_00cfb5_op10 = Color(0x1900cfb5);
const color_b579c8_op10 = Color(0x19b579c8);

const colorList = [color_f8859a, color_ffc064, color_a8d42a, Colors.cyan, Colors.blueGrey];
const careColor = [color_ffc064, color_87d6f9, color_f8859a];
