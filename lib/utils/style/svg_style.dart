import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


const SVG_RESET = 'assets/svg/reset.svg';
const SVG_ICON_L = 'assets/svg/icon_L.svg';
const SVG_SORT = 'assets/svg/sort.svg';
const SVG_TALK_BG = 'assets/svg/talk_bg.svg';
const SVG_CHAT_BUBBLE_CENTER = 'assets/svg/chat_bubble_center.svg';
const SVG_TALK_BUBBLE = 'assets/svg/talk_bubble_white.svg';
const SVG_TALK_BUBBLE_SMALL = 'assets/svg/talk_bubble_small.svg';
const SVG_INTRO_TALK_BG = 'assets/svg/intro_talk_bg.svg';
const SVG_INTRO_TALK_BG_CIRCLE = 'assets/svg/intro_talk_bg_circle.svg';
const SVG_INTRO_BG_LEFT = 'assets/svg/intro_talk_bg_left.svg';
const SVG_INTRO_ARROW = 'assets/svg/intro_arrow.svg';
const SVG_KB_INSURE = 'assets/icons/kb_insure.svg';
const SVG_COUPON_BG = 'assets/svg/coupon_bg.svg';
const SVG_COUPON_DISABLE = 'assets/svg/coupon_disable.svg';
const SVG_BG_PENTAGON_ACTIVE = 'assets/svg/bg_pentagon_active.svg';
const SVG_BG_PENTAGON_INACTIVE = 'assets/svg/bg_pentagon_inactive.svg';
const SVG_TUTORIAL_L_BOTTOM = 'assets/svg/momdaddy_bottom.svg';
const SVG_TUTORIAL_M_BOTTOM = 'assets/svg/linkmom_bottom.svg';

const double SIZE_10 = 10;
const double SIZE_15 = 15;
const double SIZE_20 = 20;
const double SIZE_25 = 25;

/// 기본
Widget svgImage({
  String imagePath = '',
  double size = SIZE_15,
  double? width,
  double? height,
  BoxFit fit = BoxFit.contain,
  Alignment alignment = Alignment.center,
  bool allowDrawingOutsideViewBox = false,
  Color? color,
  BlendMode colorBlendMode = BlendMode.srcIn,
}) =>
    SvgPicture.asset(
      imagePath,
      width: width == null ? size : width,
      height: height == null ? size : height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      color: color,
      colorBlendMode: colorBlendMode,
    );

