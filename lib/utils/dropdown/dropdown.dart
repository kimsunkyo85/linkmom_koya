import 'package:flutter/material.dart';

class Dropdown extends Object {
  const Dropdown(this.title, this.widget);
  final Widget widget;
  final String title;

  @override
  bool operator ==(dynamic other) => other != null && other is Dropdown && this.title == other.title;

  @override
  int get hashCode => super.hashCode;
}
