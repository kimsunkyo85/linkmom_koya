import 'package:flutter/material.dart';
import 'package:linkmom/data/network/models/area_address_response.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

class AreaDropDown extends StatelessWidget {
  final value;
  final List<AreaAddressData> itemsList;
  final Color dropdownColor;
  final Widget? hint;
  final Widget? disabledHint;
  final int elevation;
  final TextStyle? style;
  final Widget? underline;
  final Widget? icon;
  final Color? iconDisabledColor;
  final Color? iconEnabledColor;
  final double iconSize;
  final bool isDense;
  final bool isExpanded;
  final double itemHeight;
  final Color? focusColor;
  final FocusNode? focusNode;
  final bool autofocus;
  final margin;
  final bgColor;
  final Function(dynamic value) onChanged;

  AreaDropDown({
    required this.value,
    required this.itemsList,
    this.dropdownColor = color_white,
    this.hint,
    this.disabledHint,
    this.elevation = 1,
    this.style,
    this.underline,
    this.icon,
    this.iconDisabledColor,
    this.iconEnabledColor,
    this.iconSize = 30,
    this.isDense = false,
    this.isExpanded = true,
    this.itemHeight = kMinInteractiveDimension,
    this.focusColor,
    this.focusNode,
    this.margin = padding_10_LR,
    this.bgColor = color_white,
    this.autofocus = false,
    required this.onChanged,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: bgColor,
      ),
      child: DropdownButtonHideUnderline(
        child: Padding(
          padding: padding_0,
          child: DropdownButton(
            elevation: elevation,
            isExpanded: isExpanded,
            dropdownColor: dropdownColor,
            value: value,
            icon: icon,
            iconSize: iconSize,
            selectedItemBuilder: (BuildContext context) {
              return itemsList.map((AreaAddressData value) {
                return Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    value.getAddress(),
                    style: style == null ? st_b_18() : style,
                    textAlign: TextAlign.center,
                  ),
                );
              }).toList();
            },
            items: itemsList
                .map((AreaAddressData item) => DropdownMenuItem<AreaAddressData>(
                      value: item,
                      child: lText(item.getAddress(),
                          style: style == null
                              ? st_18(textColor: value == item.getAddress() ? color_545454 : color_222222, fontWeight: value == item.getAddress() ? FontWeight.bold : FontWeight.normal)
                              : defaultStyle(fontSize: style?.fontSize ?? 18, color: value == item.getAddress() ? color_545454 : color_222222, fontWeight: value == item.getAddress() ? FontWeight.bold : FontWeight.normal)),
                      /*child: Container(
                          color: value == item ? color_f1f5f5 : color_transparent,
                          child: lText(item, style: style)),*/
                    ))
                .toList(),
            onChanged: (value) => onChanged(value),
          ),
        ),
      ),
    );
  }
}
