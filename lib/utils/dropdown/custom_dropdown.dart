import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/string_util.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/linkmom_style.dart';
import 'package:linkmom/utils/style/text_style.dart';

class CustomDropDown extends StatelessWidget {
  var value;
  final List<String> itemsList;
  final Color dropdownColor;
  final Widget? leftWidget;
  final Widget? hint;
  final Widget? disabledHint;
  final int elevation;
  final TextStyle? style;
  final Widget? underline;
  final Widget? icon;
  final Color? iconDisabledColor;
  final Color? iconEnabledColor;
  final double iconSize;
  final bool isDense;
  final bool isExpanded;
  final double itemHeight;
  final Color? focusColor;
  final FocusNode? focusNode;
  final bool autofocus;
  final margin;
  final bgColor;
  final TextOverflow overflow;
  final Function(dynamic value) onChanged;
  final double? menuMaxHeight;

  CustomDropDown({
    required this.value,
    required this.itemsList,
    this.dropdownColor = color_white,
    this.leftWidget,
    this.hint,
    this.disabledHint,
    this.elevation = 1,
    this.style,
    this.underline,
    this.icon,
    this.iconDisabledColor,
    this.iconEnabledColor,
    this.iconSize = 30,
    this.isDense = false,
    this.isExpanded = true,
    this.itemHeight = kMinInteractiveDimension,
    this.focusColor,
    this.focusNode,
    this.margin = padding_10_LR,
    this.bgColor = color_white,
    this.autofocus = false,
    this.overflow = TextOverflow.visible,
    required this.onChanged,
    Key? key,
    this.menuMaxHeight,
  }) : super(key: key);

  bool isSearch = false;
  int dataLength = 0;
  int index = 0;
  @override
  Widget build(BuildContext context) {
    dataLength = itemsList.length;
    index = 0;
    if(!StringUtils.validateString(value)){
      value = itemsList.first;
    }
    return Container(
      margin: margin,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: bgColor,
      ),
      child: DropdownButtonHideUnderline(
        child: Padding(
          padding: padding_0,
          child: Row(
            children: [
              if (leftWidget != null) leftWidget!,
              isExpanded
                  ? Flexible(
                      child: DropdownButton(
                      itemHeight: itemHeight,
                      elevation: elevation,
                      isExpanded: isExpanded,
                      dropdownColor: dropdownColor,
                      value: value,
                      icon: icon,
                      hint: hint,
                      iconSize: iconSize,
                      menuMaxHeight: menuMaxHeight,
                      selectedItemBuilder: (BuildContext context) {
                        return itemsList.map((String value) {
                          return Container(
                            alignment: Alignment.centerLeft,
                            child: lText(
                              value,
                              style: style == null ? st_18(textColor: value == "선택".tr() ? color_b2b2b2 : color_222222) : style,
                              textAlign: TextAlign.center,
                              overflow: overflow,
                            ),
                          );
                        }).toList();
                      },
                      items: itemsList
                          .map((String item) => DropdownMenuItem<String>(
                                value: item,
                                child: lAutoSizeText(item,
                                    style: style == null
                                        ? st_18(textColor: value == item ? color_545454 : color_222222, fontWeight: value == item ? FontWeight.bold : FontWeight.normal)
                                        : defaultStyle(
                                            fontSize: style?.fontSize ?? 18, color: value == item ? color_545454 : color_222222, fontWeight: value == item ? FontWeight.bold : FontWeight.normal)),
                              ))
                          .toList(),
                      onChanged: (value) => onChanged(value),
                    ))
                  : DropdownButton(
                      itemHeight: itemHeight,
                      elevation: elevation,
                      isExpanded: isExpanded,
                      dropdownColor: dropdownColor,
                      value: value,
                      icon: icon,
                      iconSize: iconSize,
                      hint: hint,
                      menuMaxHeight: menuMaxHeight,
                      selectedItemBuilder: (BuildContext context) {
                        return itemsList.map((String value) {
                          return Container(
                            alignment: Alignment.centerLeft,
                            child: lText(
                              value,
                              style: style == null ? st_18(textColor: value == "선택".tr() ? color_b2b2b2 : color_222222) : style,
                              textAlign: TextAlign.center,
                              overflow: overflow,
                            ),
                          );
                        }).toList();
                      },
                      items: itemsList.map((String item) {
                        String name = item;
                        index++;
                        if (isSearch) {
                          // name = /*'•'*/ '+ ' + name;
                          isSearch = false;
                        }
                        if (item == "다른지역검색".tr()) {
                          isSearch = true;
                          if (index == dataLength) {
                            isSearch = false;
                          }
                        }
                        return DropdownMenuItem(
                          value: item,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              if (isSearch) Padding(padding: padding_10_T),
                              lAutoSizeText(name,
                                  style: style == null
                                      ? st_18(textColor: value == item ? color_545454 : color_222222, fontWeight: value == item ? FontWeight.bold : FontWeight.normal)
                                      : defaultStyle(
                                          fontSize: style?.fontSize ?? 18, color: value == item ? color_545454 : color_222222, fontWeight: value == item ? FontWeight.bold : FontWeight.normal)),
                              if (isSearch)
                                // lDivider(),
                                Padding(
                                  padding: padding_15_T,
                                  child: lDivider(),
                                ),
                            ],
                          ),
                        );
                      }).toList(),
                      onChanged: (value) {
                        isSearch = false;
                        index = 0;
                        return onChanged(value);
                      }),
            ],
          ),
        ),
      ),
    );
  }
}
