import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screen_wake/flutter_screen_wake.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:linkmom/data/holidays/holidays_helper.dart';
import 'package:linkmom/data/storage/storage_help.dart';
import 'package:linkmom/view/link_page.dart';
import 'package:linkmom/manager/data_manager.dart';
import 'package:linkmom/manager/enum_manager.dart';
import 'package:linkmom/manager/firebase_analytics_manager.dart';
import 'package:linkmom/services/connectivity_service.dart';
import 'package:linkmom/splash_page.dart';
import 'package:linkmom/utils/custom_dailog/custom_dailog.dart';
import 'package:linkmom/utils/custom_view/loading_page.dart';
import 'package:linkmom/view/compleat_view_page.dart';
import 'package:linkmom/view/login/agreement/policy_page.dart';
import 'package:linkmom/view/login/find/find_checkplus_page.dart';
import 'package:linkmom/view/login/preview_page.dart';
import 'package:linkmom/view/login/register/register_id_page.dart';
import 'package:linkmom/view/login/register/register_pw_page.dart';
import 'package:linkmom/view/login/unregister/register_disable_complete_page.dart';
import 'package:linkmom/view/login/unregister/register_disable_pw_page.dart';
import 'package:linkmom/view/login/unregister/register_disable_reason_page.dart';
import 'package:linkmom/view/main/auth_center/auth_center_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_career_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_covid19_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_crims_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_deungbon_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_educate_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_graduated_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_stress_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_stress_result_page.dart';
import 'package:linkmom/view/main/auth_center/home_state/home_state_page.dart';
import 'package:linkmom/view/main/auth_center/location/location_page.dart';
import 'package:linkmom/view/main/chat/chat_room_page.dart';
import 'package:linkmom/view/main/chat/full_photo.dart';
import 'package:linkmom/view/main/community/community_event_page.dart';
import 'package:linkmom/view/main/community/community_rank_page.dart';
import 'package:linkmom/view/main/job_apply/address_page.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_1_page.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_4_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/linkmom_profile_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/linkmom_schedule_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/momdady_profile_page.dart';
import 'package:linkmom/view/main/job_list/linkmom_list_page.dart';
import 'package:linkmom/view/main/job_list/momdady_list_page.dart';
import 'package:linkmom/view/main/main_home.dart';
import 'package:linkmom/view/main/matchings/momdady_apply_page.dart';
import 'package:linkmom/view/main/matchings/momdady_cares_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_drug_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_filter_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_view_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_write_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/recipe_view_page.dart';
import 'package:linkmom/view/main/mypage/child_info/child_drug_view_page.dart';
import 'package:linkmom/view/main/mypage/child_info/child_info_add_page.dart';
import 'package:linkmom/view/main/mypage/cs/cs_faq_page.dart';
import 'package:linkmom/view/main/mypage/cs/cs_notice_page.dart';
import 'package:linkmom/view/main/mypage/cs/cs_terms_view_page.dart';
import 'package:linkmom/view/main/mypage/my_info/my_account_manager_page.dart';
import 'package:linkmom/view/main/mypage/mypage.dart';
import 'package:linkmom/view/main/mypage/penalty/penalty_manage_page.dart';
import 'package:linkmom/view/main/mypage/review/momdaddy_review_page.dart';
import 'package:linkmom/view/main/mypage/review/review_view_page.dart';
import 'package:linkmom/view/main/mypage/settings_block_page.dart';
import 'package:linkmom/view/main/mypage/withdrawal/bank_list_page.dart';
import 'package:linkmom/view/main/payment/payment_cancel_list_page.dart';
import 'package:linkmom/view/main/payment/payment_cancel_page.dart';
import 'package:linkmom/view/main/payment/payment_cancel_reason_page.dart';
import 'package:linkmom/view/main/payment/payment_explain_complete_page.dart';
import 'package:linkmom/view/main/payment/payment_explain_page.dart';
import 'package:linkmom/view/main/payment/payment_iamport_page.dart';
import 'package:linkmom/view/main/payment/payment_page.dart';
import 'package:linkmom/view/main/payment/payment_refund_rule_page.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';

import 'base/base_route_aware.dart';
import 'data/encrypt/encrypt_helper.dart';
import 'data/network/api_end_point.dart';
import 'data/network/api_header.dart';
import 'data/network/api_helper.dart';
import 'data/network/models/data/chat_send_data.dart';
import 'data/network/models/pay_cancel_info_request.dart';
import 'data/network/models/version_check_response.dart';
import 'data/providers/agreement_model.dart';
import 'data/providers/auth_model.dart';
import 'data/providers/community_event_provider.dart';
import 'data/providers/community_list_provider.dart';
import 'data/providers/community_scarp_provider.dart';
import 'data/providers/image_loader_provider.dart';
import 'data/providers/link_provider.dart';
import 'data/providers/loading_model.dart';
import 'data/providers/model/community_rank_provider.dart';
import 'data/providers/payment_history_provider.dart';
import 'data/providers/push_model.dart';
import 'data/providers/selete_model.dart';
import 'data/push/model/linkmompush.dart';
import 'data/storage/model/version_model.dart';
import 'main.mapper.g.dart';
import 'manager/enum_utils.dart';
import 'manager/finger_push_manager.dart';
import 'manager/firebase_push_manager.dart';
import 'manager/mqtt_manager.dart';
import 'route_name.dart';
import 'utils/commons.dart';
import 'utils/emoji/emoji.dart';
import 'utils/home_provider.dart';
import 'utils/log_util.dart';
import 'utils/route_action.dart';
import 'utils/style/linkmom_style.dart';
import 'utils/transitions/slide_route.dart';
import 'view/login/agreement/register_agreement_page.dart';
import 'view/login/find/find_id_complete_page.dart';
import 'view/login/find/find_id_page.dart';
import 'view/login/find/find_page.dart';
import 'view/login/find/find_password_page.dart';
import 'view/login/intro_page.dart';
import 'view/login/login_page.dart';
import 'view/login/password_change/password_change_complete_page.dart';
import 'view/login/password_change/password_change_page.dart';
import 'view/login/register/register_auth_page.dart';
import 'view/login/register/register_complete_page.dart';
import 'view/login/register/register_invite_page.dart';
import 'view/login/tutorial_page.dart';
import 'view/login/unregister/register_disable_page.dart';
import 'view/main/auth_center/authcenter_health_page.dart';
import 'view/main/auth_center/authcenter_personality_page.dart';
import 'view/main/auth_center/home_state/auth_home_page.dart';
import 'view/main/auth_center/home_state/nbhhome_page.dart';
import 'view/main/auth_center/home_state/ourhome_page.dart';
import 'view/main/chat/chat_care_info_page.dart';
import 'view/main/community/community_view_page.dart';
import 'view/main/community/community_write_page.dart';
import 'view/main/job_apply/address_search_page.dart';
import 'view/main/job_apply/care/care_step_2_page.dart';
import 'view/main/job_apply/care/care_step_3_page.dart';
import 'view/main/job_apply/care/care_step_4_page.dart';
import 'view/main/job_apply/care/care_step_5_page.dart';
import 'view/main/job_apply/care/care_step_6_page.dart';
import 'view/main/job_apply/care/care_step_7_page.dart';
import 'view/main/job_apply/guide/guide_linkmom_page.dart';
import 'view/main/job_apply/guide/guide_momdady_page.dart';
import 'view/main/job_apply/job_profile/matching_sign_page.dart';
import 'view/main/job_apply/job_profile/momdady_schedule_page.dart';
import 'view/main/job_apply/parents/parents_step_2_page.dart';
import 'view/main/job_apply/parents/parents_step_3_page.dart';
import 'view/main/job_apply/parents/parents_step_4_page.dart';
import 'view/main/job_apply/parents/parents_step_5_page.dart';
import 'view/main/job_apply/parents/parents_step_6_page.dart';
import 'view/main/job_apply/parents/parents_step_7_page.dart';
import 'view/main/job_apply/parents/parents_step_8_page.dart';
import 'view/main/job_apply/parents/parents_step_9_page.dart';
import 'view/main/job_apply/temp_area/temp_area_page.dart';
import 'view/main/job_apply/temp_area/temp_area_view_page.dart';
import 'view/main/job_list/job_filter_page.dart';
import 'view/main/matchings/linkmom_apply_page.dart';
import 'view/main/matchings/linkmom_matching_page.dart';
import 'view/main/matchings/matching_filter_page.dart';
import 'view/main/matchings/matching_filter_result_page.dart';
import 'view/main/matchings/momdady_matching_page.dart';
import 'view/main/mypage/benefit/benefit_page.dart';
import 'view/main/mypage/benefit/coupon_detail_page.dart';
import 'view/main/mypage/care_diary/diary_care_list_page.dart';
import 'view/main/mypage/care_diary/diary_claim_answer_page.dart';
import 'view/main/mypage/care_diary/diary_claim_page.dart';
import 'view/main/mypage/care_diary/diary_drug_list_page.dart';
import 'view/main/mypage/care_diary/diary_filter_result_page.dart';
import 'view/main/mypage/care_diary/diary_write_complete_page.dart';
import 'view/main/mypage/care_diary/linkmom_diary_list_page.dart';
import 'view/main/mypage/care_diary/momdady_diary_list_page.dart';
import 'view/main/mypage/care_diary/payment/pay_recipe_page.dart';
import 'view/main/mypage/care_diary/payment/payment_expired_page.dart';
import 'view/main/mypage/care_diary/payment/payment_filter_page.dart';
import 'view/main/mypage/care_diary/payment/payment_filter_result_page.dart';
import 'view/main/mypage/care_diary/payment/payment_history_page.dart';
import 'view/main/mypage/child_info/child_drug_add_page.dart';
import 'view/main/mypage/child_info/child_drug_list_page.dart';
import 'view/main/mypage/child_info/child_info_page.dart';
import 'view/main/mypage/child_info/child_menu_page.dart';
import 'view/main/mypage/cs/cs_contact_page.dart';
import 'view/main/mypage/cs/cs_contact_view_page.dart';
import 'view/main/mypage/cs/cs_guide_page.dart';
import 'view/main/mypage/cs/cs_notice_view_page.dart';
import 'view/main/mypage/cs/cs_page.dart';
import 'view/main/mypage/cs/cs_service_page.dart';
import 'view/main/mypage/cs/cs_terms_page.dart';
import 'view/main/mypage/favorite/linkmom_favorite_page.dart';
import 'view/main/mypage/favorite/momdaddy_favorite_page.dart';
import 'view/main/mypage/my_info/my_account_email_page.dart';
import 'view/main/mypage/my_info/my_account_hp_change_page.dart';
import 'view/main/mypage/my_info/my_account_hp_page.dart';
import 'view/main/mypage/my_info/my_account_pw_page.dart';
import 'view/main/mypage/my_info/my_info_page.dart';
import 'view/main/mypage/penalty/penalty_history_page.dart';
import 'view/main/mypage/penalty/penalty_remove_page.dart';
import 'view/main/mypage/review/linkmom_review_page.dart';
import 'view/main/mypage/review/review_save_page.dart';
import 'view/main/mypage/settings_noti_page.dart';
import 'view/main/mypage/settings_page.dart';
import 'view/main/mypage/withdrawal/register_account_page.dart';
import 'view/main/mypage/withdrawal/withdrawal_filter_page.dart';
import 'view/main/mypage/withdrawal/withdrawal_filter_result_page.dart';
import 'view/main/mypage/withdrawal/withdrawal_page.dart';
import 'view/main/mypage/work_noti_page.dart';
import 'view/main/notification_center_page.dart';
import 'view/main/notify/care_notify_page.dart';
import 'view/main/notify/community_notify_page.dart';
import 'view/main/notify/diary_notify_page.dart';
import 'view/main/payment/iamport_page.dart';
import 'view/main/payment/payment_cancel_complete_page.dart';
import 'view/main/payment/payment_cancel_select_page.dart';
import 'view/main/payment/payment_complete_page.dart';
import 'view/main/payment/payment_result.dart';
import 'view/main/payment/payment_test.dart';
import 'view/main/survey/survey_complete_page.dart';
import 'view/main/survey/survey_page.dart';
import 'view/main/survey/survey_question_page.dart';

final auth = AuthModel();
final apiHelper = ApiHelper();
final apiHeader = ApiHeader();
final storageHelper = StorageHelper();
final provider = DialogProvider();
final loadingModel = LoadingModel();
final navigatorKey = GlobalKey<NavigatorState>();
var log;
final network = NetworkProvider();
final selectModel = SelectModel();
final pushFirebase = FirebasePushManager();
final pushFinger = FingerPushManager();
final mqttManager = MqttManager();
final pushData = LinkmomPushData();
final HolidaysHelper holidays = HolidaysHelper();
final pushProvider = PushModel();
final encryptHelper = EncryptHelper();
late StreamSubscription _sub;
String? deeplinkAction;

final BaseRouteAware routeObserver = BaseRouteAware();

var platform;

var verMajor = 1;
var verMinor = 0;
var verPatch = 0;
Map<String, String> cookies = {};
bool isNetwork = true;
VersionCheckResponse? versionCheckData;
var connectivityResult;
const debug = true;

VersionData deviceInfo = VersionData();

///start
void main() async {
  initializeJsonMapper();
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) {
    run();
  });
  WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
    await storageHelper.getFlags();
    auth.setSettings = await storageHelper.getSettings();
    Commons.loadFiles();
  });

  log = getLogger();
}

///원격 리모트 확인, 로그인정보 확인, 디바이스 정보 세팅, 언어설정
void run() async {
  //다국어 지원 및 String 리소스로
  auth.loadSettings();
  await initPlatformDevice();
  deviceInfo = await Commons.getDeviceInfo();

  connectivityResult = await network.isCheckNetwork();
  Commons.isDebugMode = await storageHelper.getDebug();
  if (connectivityResult == ConnectivityResult.none) {
    isNetwork = false;
  } else {
    isNetwork = true;
  }
  FbaManager.fba.logAppOpen();

  runApp(RestartWidget(
    child: EasyLocalization(
        //기본 한국어, 영어 지원
        supportedLocales: [Locale('en', 'US'), Locale('ko', 'KR')],
        path: 'assets/strings',
        fallbackLocale: Locale('ko', 'KR'),
        child: Center(
          child: MyApp(),
        )),
  ));
}

///Firebase, FinngerPush, MQTT
initManager() async {
  //Firebase FCM 핸들러시 사용 주석 제거 후
  await pushFirebase.initFirebasePush(callBack: (message) {
    // log.d('『GGUMBI』>>> push pushFirebase callBack : pageName 1 : ${mqttManager.getCurrentRouteName}, message:$message,  <<< ');
    // log.d('『GGUMBI』>>> showNotificationMqtt : mqttManager.getCurrentRouteName: ${mqttManager.getCurrentRouteName},  <<< ');
    LinkmomPushData _pushData = message as LinkmomPushData;
    // log.d('『GGUMBI』>>> push pushFirebase callBack : pageName 2 : ${mqttManager.getCurrentRouteName}, _pushData:$_pushData,  <<< ');
    if (EnumUtils.getNotificationType(_pushData.pagename).isChat) {
      if (mqttManager.getCurrentRouteName == routeChatRoom) {
        //채팅방에 있는데 Off 된 경우, 핑거푸쉬로 수신시 채팅방 리스트를 재조회 한다. 2022/03/16
        if (_pushData.chatSendData!.msgtype == null) {
          // log.d('『GGUMBI』>>> push pushFirebase callBack : pageName 3 : ${mqttManager.getCurrentRouteName}, _pushData:$_pushData,  <<< ');
          mqttManager.setAddData(mqttManager.getRefresh(), isFcm: true);
        } else {
          // log.d('『GGUMBI』>>> push pushFirebase callBack : pageName 4 : ${mqttManager.getCurrentRouteName}, _pushData:$_pushData,  <<< ');
          mqttManager.setAddData(json.encode(_pushData.chatSendData), isFcm: true);
        }
      }
    }
    setBadge(_pushData);
    if (_pushData.msgData != null && RouteAction.isImportant(_pushData.msgData!.popupCode)) {
      RouteAction.onNotification(navigatorKey.currentContext!, _pushData.msgData!.nextId, EnumUtils.getNotificationType(_pushData.pagename), _pushData);
    }
  });

  //화면이동 true ios (isBadge)
  await pushFinger.initFingerPush(callBack: (message, isBadge, isFcm, isIOS) {
    LinkmomPushData _pushData = message as LinkmomPushData;
    // log.d('『GGUMBI』>>> push pushFinger callBack isBadge:$isBadge, isFcm:$isFcm, isIOS:$isIOS, ${_pushData.pagename == 0}, routeChatRoom: ${mqttManager.getCurrentRouteName}, data: $data <<< ');
    // log.d('『GGUMBI』>>> push pushFinger callBack 1 : pagename: ${_pushData.pagename}, roomId: ${mqttManager.getChatroomId}, data.pushData: ${_pushData.pushData}, data.data: ${_pushData.chatSendData}');
    // log.d('『GGUMBI』>>> push pushFinger callBack 11 : pagename: ${_pushData.pagename}, roomId: ${mqttManager.getChatroomId}, chattingroom: ${_pushData.data!.chattingroom}, data: $_pushData');

    setBadge(_pushData);
    // Provider.of<HomeNavigationProvider>(navigatorKey.currentContext, listen: true).updatedRest();
    //ios FCM 수신
    if (isFcm || isIOS) {
      if (EnumUtils.getNotificationType(_pushData.pagename).isChat) {
        if (mqttManager.getCurrentRouteName == routeChatRoom) {
          //채팅방에 있는데 Off 된 경우, 핑거푸쉬로 수신시 채팅방 리스트를 재조회 한다. 2022/03/16
          if (_pushData.chatSendData == null) {
            mqttManager.setAddData(mqttManager.getRefresh(), isFcm: isFcm);
          } else {
            mqttManager.setAddData(json.encode(_pushData.chatSendData), isFcm: isFcm);
          }
        }
      } else {
        // log.d('『GGUMBI』>>> push pushFinger callBack 00000 : id:$id, isBadge:$isBadge, isFcm:$isFcm, pagename: ${data.pagename}, roomId: ${mqttManager.getChatroomId}, chattingroom: ${data.data!.chattingroom}, data: $data');
        pushData.setData(LinkmomPushData(chatSendData: _pushData.chatSendData, chattingtype: _pushData.chattingtype, pagename: _pushData.pagename));
      }
    }
    // log.d('『GGUMBI』>>> push pushFinger callBack 11111 : id:$id, isBadge:$isBadge, isFcm:$isFcm, pagename: ${data.pagename}, roomId: ${mqttManager.getChatroomId}, chattingroom: ${data.data!.chattingroom}, data: $data');

    //타입이 채팅이고,
    if (EnumUtils.getNotificationType(_pushData.pagename).isChat && !isBadge) {
      int id = auth.user.id == _pushData.chatSendData!.sender ? _pushData.chatSendData!.receiver : _pushData.chatSendData!.sender;
      //내가 같은 채팅방이면 아무런 처리를 하지 않는다.
      if (_pushData.chatSendData!.chattingroom == mqttManager.getChatroomId) {
        // log.d('『GGUMBI』>>> push pushFinger callBack 2 : id:$id, isBadge:$isBadge, isFcm:$isFcm, pagename: ${data.pagename}, roomId: ${mqttManager.getChatroomId}, chattingroom: ${data.data!.chattingroom}, data: $data');

        //내가 있는 채팅방이 다를 경우...
      } else if (_pushData.chatSendData!.chattingroom != mqttManager.getChatroomId) {
        //현재 채팅방이 아니면 stak add   ///내가 있는 화면이 채팅이 아니면, 화면을 이동한다.
        log.d('『GGUMBI』>>> Main build push pushFinger initManager : {}: ${navigatorKey.currentContext},  <<< ');
        if (mqttManager.getCurrentRouteName != routeChatRoom) {
          Commons.page(navigatorKey.currentContext, routeChatRoom,
              arguments: ChatRoomPage(
                roomType: ChatRoomType.VIEW,
                receiver: id,
                chatroom_id: _pushData.chatSendData!.chattingroom,
              ));
          // log.d('『GGUMBI』>>> push pushFinger callBack 3 : : id:$id, isBadge:$isBadge, isFcm:$isFcm, pagename: ${data.pagename}, roomId: ${mqttManager.getChatroomId}, chattingroom: ${data.data!.chattingroom}, data: $data');

          //현재 채팅방이면 채팅방 전환 화면(추후 작업시 주석 제거)
        } else if (mqttManager.getCurrentRouteName == routeChatRoom) {
          //채팅화면에 있고, (돌봄신청서, 계약서 등) 다른 화면이 있을경우 mainhome으로 보낸후 페이지를 체인지한다.
          Commons.pagePopUntilFirst(navigatorKey.currentContext);
          Commons.page(navigatorKey.currentContext, routeLoading,
              arguments: LoadingPage(
                routeName: routeChatRoom,
                pageData: ChatRoomPage(
                  roomType: ChatRoomType.VIEW,
                  receiver: id,
                  chatroom_id: _pushData.chatSendData!.chattingroom,
                ),
              ));
          // log.d('『GGUMBI』>>> push pushFinger callBack 4 : : id:$id, isBadge:$isBadge, isFcm:$isFcm, pagename: ${data.pagename}, roomId: ${mqttManager.getChatroomId}, chattingroom: ${data.data!.chattingroom}, data: $data');
        }
      }
    } else {
      //ios는 FingerPush.shared.willPresentNotificationHandler((notificationValue) {
      //에서 수신받아 바로 처리 하기 때문에 푸시 수신시 화면이동으로 인해 return 처리
      //일반적으로 푸시 탭 했을경우 해당 메서드에서 처리 FingerPush.shared.receivedNotificationHandler((notificationValue) {
      if (RouteAction.isImportant(_pushData.msgData!.popupCode)) {
        RouteAction.onNotification(navigatorKey.currentContext!, _pushData.msgData!.nextId, EnumUtils.getNotificationType(_pushData.pagename), _pushData);
      } else if (RouteAction.isShowingToast(_pushData.msgData!.popupCode)) {
        RouteAction.onNotification(navigatorKey.currentContext!, _pushData.msgData!.nextId, EnumUtils.getNotificationType(_pushData.pagename), _pushData);
      }
      if (isIOS) return;

      if (_pushData.chatSendData?.msgdata is ChatInfoData) {
        ChatInfoData chatInfoData = _pushData.chatSendData!.msgdata;
        // log.d('『GGUMBI』>>> initState : momdady: ${chatInfoData.momdady}, linkmom:${chatInfoData.linkmom}, ${auth.user.id} <<< ');
        Commons.setModeChange(momdadyId: chatInfoData.momdady, linkMomId: chatInfoData.linkmom);
      }

      if (_pushData.sendCode == RouteAction.pushCovid) {
        RouteAction.onDetail(navigatorKey.currentContext!, _pushData.sendCode, link: _pushData.weblink);
      } else {
        //추후 해당 화면 유무에 따라서 Stack 처리...지금은 메인외 모든 화면을 닫고 화면이동으로 처리. 2021/11/25
        NotificationType type = EnumUtils.getNotificationType(_pushData.pagename);
        int id = _pushData.chatSendData!.id == 0 ? _pushData.msgData!.nextId : _pushData.chatSendData!.id;
        if (_pushData.weblink.isNotEmpty) {
          log.d('call weblink: ${_pushData.weblink},  <<< ');
          RouteAction.onLink(navigatorKey.currentContext!, _pushData.weblink);
        } else {
          RouteAction.onNotification(navigatorKey.currentContext!, id, type, _pushData);
        }
      }
    }
  });

  await mqttManager.connect(
    onMsg: (action, payload) {
      try {
        ChatSendData data = ChatSendData.fromJson(json.decode(payload));

        //채팅화면에 있을때 화면 리스트를 갱신한다.
        pushData.setData(LinkmomPushData(chatSendData: data, chattingtype: data.chattingtype, pagename: data.pagename));

        //채팅 방에 있을 경우, 대화하는 사람 외 다른 사람이 보낸 경우 알림과 함께 다른 화면으로 점프가 가능하다.
        //채팅방에 있다가, 백그라운드로 나갈때 채팅방에서는 리스트에 대화를 추가하고, 노티로 알려준다.
        if (mqttManager.getReceiver != data.sender || mqttManager.getChatroomId != data.chattingroom || !mqttManager.isPaused && mqttManager.getCurrentRouteName == routeChatRoom) {
          pushFirebase.showNotificationMqtt(data);
          pushProvider.setBadgeChat();
        }
      } catch (e) {
        log.e('『GGUMBI』 Exception >>> initState : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      }
    },
  );
}

///알림, 채팅 Badge처리
setBadge(dynamic message) {
  // log.d('『GGUMBI』>>> build : _pushProvider: $message,  <<< ');
  if (message is LinkmomPushData) {
    if (EnumUtils.getNotificationType(message.pagename).isChat) {
      pushProvider.setBadgeChat(isUpdate: true);
    } else {
      pushProvider.setBadgeNotify(isUpdate: true);
    }
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  late String autoLogin;
  bool isInit = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    FlutterScreenWake.keepOn(true);
    platform = Theme.of(context).platform;

    if (kReleaseMode) {
      Commons.isDebugMode = false;
      Logger.level = Level.nothing;
    } else {
      // if (kDebugMode) {
      Logger.level = Level.debug;

      //기본 주소를 디버그 모드시 개발로 해준다. 플래그 변경시 운영서버
      if (Commons.isDebugMode) {
        ApiEndPoint.setMode(type: SERVER_TYPE.DEBUG);
        initPlatformDevice();
      }
      // }
    }

    FirebaseDynamicLinks.instance.onLink.listen((dynamicLinkData) {
      log.d({'dynamic_data': dynamicLinkData.link});
      RouteAction.onLink(navigatorKey.currentContext!, dynamicLinkData.link.toString(), params: dynamicLinkData.link.queryParameters);
    }).onError((error) {
      log.e(error);
    });

    //Emoji init
    EmojiParser.init(context);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  ///LifeCycle시 주석 제거
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // log.d('『GGUMBI』>>> didChangeAppLifecycleState : state main : ${state},  <<< ');
    if (state == AppLifecycleState.resumed) {
    } else if (state == AppLifecycleState.inactive) {
    } else if (state == AppLifecycleState.paused) {
    } else if (state == AppLifecycleState.detached) {
      //2022/06/22 메인 종료시 MQTT 종료 추가
      try {
        mqttManager.remove();
      } catch (e) {
        log.e('『GGUMBI』 Exception >>>  : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ApiHelper>.value(value: apiHelper),
        ChangeNotifierProvider<AuthModel>.value(value: auth),
        ChangeNotifierProvider<StorageHelper>.value(value: storageHelper),
        ChangeNotifierProvider<DialogProvider>.value(value: provider),
        ChangeNotifierProvider<AgreementModel>.value(value: AgreementModel()),
        ChangeNotifierProvider<LoadingModel>.value(value: loadingModel),
        ChangeNotifierProvider<NetworkProvider>.value(value: network),
        ChangeNotifierProvider<SelectModel>.value(value: selectModel),
        ChangeNotifierProvider<MqttManager>.value(value: mqttManager),
        ChangeNotifierProvider<LinkmomPushData>.value(value: pushData),
        ChangeNotifierProvider<HomeNavigationProvider>.value(value: HomeNavigationProvider()),
        ChangeNotifierProvider<PushModel>.value(value: pushProvider),
        ChangeNotifierProvider<CouponList>(create: (_) => CouponList()),
        ChangeNotifierProvider<ExpiredList>(create: (_) => ExpiredList()),
        ChangeNotifierProvider<ImageLoader>(create: (_) => ImageLoader()),
        ChangeNotifierProvider<CommunityListProvider>(create: (_) => CommunityListProvider()),
        ChangeNotifierProvider<CommunityRankProvider>(create: (_) => CommunityRankProvider()),
        ChangeNotifierProvider<CommunityEventProvider>(create: (_) => CommunityEventProvider()),
        ChangeNotifierProvider<CommunityScrapProvider>(create: (_) => CommunityScrapProvider()),
        ChangeNotifierProvider<LinkProvider>(create: (_) => LinkProvider()),
        ChangeNotifierProvider<PaymentHistoryProvider>(create: (_) => PaymentHistoryProvider()),
      ],
      child: ScreenUtilInit(
        designSize: Size(375, 817),
        builder: (context, child) => MaterialApp(
          home: child,
          localizationsDelegates: context.localizationDelegates,
          supportedLocales: context.supportedLocales,
          locale: context.locale,
          navigatorObservers: [routeObserver],
          navigatorKey: navigatorKey,
          debugShowCheckedModeBanner: false,
          title: "appName".tr(),
          theme: linkmomTheme,
          builder: (context, child) => MediaQuery(data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0), child: child!),
          //시스템 폰트 일정 크길 고정 추가 2021-10-23 0023
          initialRoute: routeSplash,

          ///데이터 없는 페이지
          routes: <String, WidgetBuilder>{
            routeSplash: (context) => SplashPage(),
            routeFindId: (context) => FindIdPage(),
            routeIamPort: (context) => IamPortPage(),
            routePayMentTest: (context) => PaymentTestPage(),
            routePayMentResult: (context) => PaymentResultPage(),
            routeTutorial: (context) => TutorialPage(),
            routeIntro: (context) => IntroPage(),
          },

          ///데이터가 있는 페이지
          onGenerateRoute: (settings) {
            log.d('『GGUMBI』>>> build : settings: $settings,  <<< ');
            if (settings.name == routeHome) {
              final MainHome? args = settings.arguments as MainHome?;
              return MaterialPageRoute(
                builder: (context) => args == null ? MainHome() : MainHome(tabIndex: args.tabIndex, pageTabIndex: args.pageTabIndex, sendCode: args.sendCode),
                settings: settings,
              );
              //로그인
            } else if (settings.name == routeLogin) {
              final LoginPage? args = settings.arguments as LoginPage;
              return MaterialPageRoute(builder: (context) => args == null ? LoginPage() : LoginPage(id: args.id), settings: settings);
              //이용약관
            } else if (settings.name == routeAgreement) {
              final RegisterAgreementPage? args = settings.arguments as RegisterAgreementPage?;
              return SlideLeftRoute(page: args ?? RegisterAgreementPage(), setting: settings);
              //회원가입
            } else if (settings.name == routeRegisterAuth) {
              final RegisterAuthPage? args = settings.arguments as RegisterAuthPage?;
              return SlideLeftRoute(page: RegisterAuthPage(registerData: args!.registerData), setting: settings);
              //회원가입완료
            } else if (settings.name == routeRegisterComplete) {
              final RegisterCompletePage? args = settings.arguments as RegisterCompletePage?;
              return SlideLeftRoute(page: args ?? RegisterCompletePage(), setting: settings);
              //아이디/비밀번호찾기 탭 페이지
            } else if (settings.name == routeFind) {
              final FindPage? args = settings.arguments as FindPage?;
              return SlideLeftRoute(page: FindPage(pageType: args!.pageType, id: args.id), setting: settings);
              //아이디찾기완료
            } else if (settings.name == routeFindIdComplete) {
              final FindIdCompletePage? args = settings.arguments as FindIdCompletePage?;
              return SlideLeftRoute(page: FindIdCompletePage(data: args!.data, id: args.id, name: args.name), setting: settings);
              //비밀번호찾기
            } else if (settings.name == routeFindPw) {
              final FindPasswordPage? args = settings.arguments as FindPasswordPage?;
              return MaterialPageRoute(builder: (context) => FindPasswordPage(data: args!.data, id: args.id), settings: settings);
              //비밀번호변경
            } else if (settings.name == routePwChange) {
              final PasswordChangePage? args = settings.arguments as PasswordChangePage?;
              return SlideLeftRoute(page: PasswordChangePage(data: args!.data, req: args.req, accessToken: args.accessToken), setting: settings);
              //비밀번호변경완료
            } else if (settings.name == routePwChangeComplete) {
              final PasswordChangeCompletePage? args = settings.arguments as PasswordChangeCompletePage?;
              return MaterialPageRoute(builder: (context) => PasswordChangeCompletePage(id: args!.id, password: args.password), settings: settings);
              //인증센터
            } else if (settings.name == routeAuthCenter) {
              final AuthCenterPage? args = settings.arguments as AuthCenterPage?;
              return SlideLeftRoute(page: args ?? AuthCenterPage(), setting: settings);
              //환경인증 우리집
            } else if (settings.name == routeNbhHome) {
              final NbhHomeStatePage? args = settings.arguments as NbhHomeStatePage?;
              return SlideLeftRoute(page: NbhHomeStatePage(data: args!.data, viewType: args.viewType), setting: settings);
              //환경인증 이웃집
            } else if (settings.name == routeOurHome) {
              final OurHomeStatePage? args = settings.arguments as OurHomeStatePage?;
              return SlideLeftRoute(page: OurHomeStatePage(data: args!.data, viewType: args.viewType), setting: settings);
              //환경인증 페이지
            } else if (settings.name == routeAuthHome) {
              final AuthHomeStatePage? args = settings.arguments as AuthHomeStatePage?;
              return SlideLeftRoute(page: AuthHomeStatePage(data: args!.data, viewType: args.viewType), setting: settings);
              //아이정보
            } else if (settings.name == routeChildInfo) {
              final ChildInfoPage? args = settings.arguments as ChildInfoPage?;
              if (args == null) {
                return MaterialPageRoute(builder: (context) => ChildInfoPage(), settings: settings);
              } else {
                return SlideLeftRoute(page: ChildInfoPage(data: args.data, mode: args.mode, viewMode: args.viewMode), setting: settings);
              }
              //아이정보(추가/수정)
            } else if (settings.name == routeChildInfoAdd) {
              final ChildInfoAddPage? args = settings.arguments as ChildInfoAddPage?;
              return SlideLeftRoute(page: ChildInfoAddPage(data: args!.data, child_id: args.child_id, viewMode: args.viewMode), setting: settings);
              //나의정보
            } else if (settings.name == routeMyInfo) {
              final MyInfoPage? args = settings.arguments as MyInfoPage?;
              if (args != null && args.viewMode.viewType == ViewType.modify) {
                return MaterialPageRoute(builder: (context) => MyInfoPage(data: args.data, viewMode: args.viewMode), settings: settings);
              } else {
                return SlideLeftRoute(page: args ?? MyInfoPage(viewMode: ViewMode()), setting: settings);
              }
              //돌봄신청2 - 돌봄유형선택
            } else if (settings.name == routeParents2) {
              final ParentsStep2Page? args = settings.arguments as ParentsStep2Page?;
              return SlideLeftRoute(page: ParentsStep2Page(data: args!.data, viewMode: args.viewMode), setting: settings);
              //돌봄신청3 - 장소선택
            } else if (settings.name == routeParents3) {
              final ParentsStep3Page? args = settings.arguments as ParentsStep3Page?;
              return SlideLeftRoute(page: ParentsStep3Page(data: args!.data, viewMode: args.viewMode), setting: settings);
              //돌봄신청4 - 날짜/시간
            } else if (settings.name == routeParents4) {
              final ParentsStep4Page? args = settings.arguments as ParentsStep4Page?;
              return SlideLeftRoute(page: ParentsStep4Page(data: args!.data, viewMode: args.viewMode), setting: settings);
              //돌봄신청5 - 이동방법
            } else if (settings.name == routeParents5) {
              final ParentsStep5Page? args = settings.arguments as ParentsStep5Page?;
              return SlideLeftRoute(page: ParentsStep5Page(data: args!.data, viewMode: args.viewMode), setting: settings);
              //돌봄신청6 - 추가서비스 옵션
            } else if (settings.name == routeParents6) {
              final ParentsStep6Page? args = settings.arguments as ParentsStep6Page?;
              return SlideLeftRoute(page: ParentsStep6Page(data: args!.data, viewMode: args.viewMode, tabType: args.tabType, matchingStatus: args.matchingStatus, childInfo: args.childInfo), setting: settings);
              //돌봄신청7 - 신청내역
            } else if (settings.name == routeParents7) {
              final ParentsStep7Page? args = settings.arguments as ParentsStep7Page?;
              return SlideLeftRoute(page: ParentsStep7Page(data: args!.data, viewMode: args.viewMode), setting: settings);
              //돌봄신청8 - 결제금액
            } else if (settings.name == routeParents8) {
              final ParentsStep8Page? args = settings.arguments as ParentsStep8Page?;
              return SlideLeftRoute(page: ParentsStep8Page(data: args!.data, viewMode: args.viewMode), setting: settings);
              //돌봄신청9 - 완료
            } else if (settings.name == routeParents9) {
              final ParentsStep9Page? args = settings.arguments as ParentsStep9Page?;
              return SlideLeftRoute(page: ParentsStep9Page(data: args!.data), setting: settings);
              //돌봄 주소관리
            } else if (settings.name == routeTempAreaView) {
              final TempAreaViewPage? args = settings.arguments as TempAreaViewPage?;
              return SlideLeftRoute(page: TempAreaViewPage(viewMode: args!.viewMode, tempAreaData: args.tempAreaData), setting: settings);
              //돌봄 주소관리 추가/수정/삭제
            } else if (settings.name == routeTempArea) {
              final TempAreaPage? args = settings.arguments as TempAreaPage?;
              return SlideLeftRoute(page: TempAreaPage(data: args!.data, viewMode: args.viewMode, tempAreaData: args.tempAreaData), setting: settings);
              //돌봄신청 프로필 보기
            } else if (settings.name == routeMomDadyProfile) {
              final MomDadyProfilePage? args = settings.arguments as MomDadyProfilePage?;
              return SlideLeftRoute(
                  setting: settings,
                  page: MomDadyProfilePage(
                    data: args!.data,
                    viewMode: args.viewMode,
                    bookingId: args.bookingId,
                    chattingtype: args.chattingtype,
                    matchingData: args.matchingData,
                    receiver: args.receiver,
                    btnType: args.btnType,
                  ));

              //구직신청 - 돌봄유형선택
            } else if (settings.name == routeCare1) {
              final CareStep1Page? args = settings.arguments as CareStep1Page?;
              return SlideLeftRoute(page: CareStep1Page(data: args!.data, viewMode: args.viewMode), setting: settings);
              //구직신청 - 돌봄장소선택
            } else if (settings.name == routeCare2) {
              final CareStep2Page? args = settings.arguments as CareStep2Page?;
              return SlideLeftRoute(page: CareStep2Page(data: args!.data, viewMode: args.viewMode), setting: settings);
              //구직신청 - 이동방법선택
            } else if (settings.name == routeCare3) {
              final CareStep3Page? args = settings.arguments as CareStep3Page?;
              return SlideLeftRoute(page: CareStep3Page(data: args!.data, viewMode: args.viewMode), setting: settings);
              //구직신청 - 날짜/시간
            } else if (settings.name == routeCare4) {
              final CareStep4Page? args = settings.arguments as CareStep4Page?;
              return SlideLeftRoute(page: CareStep4Page(data: args!.data, viewMode: args.viewMode), setting: settings);
              //구직신청 - 희망근무조건
            } else if (settings.name == routeCare5) {
              final CareStep5Page? args = settings.arguments as CareStep5Page?;
              return SlideLeftRoute(page: CareStep5Page(data: args!.data, viewMode: args.viewMode), setting: settings);
              //구직신청 - 등록내역
            } else if (settings.name == routeCare6) {
              final CareStep6Page? args = settings.arguments as CareStep6Page?;
              return SlideLeftRoute(page: CareStep6Page(data: args!.data, viewMode: args.viewMode), setting: settings);
              //구직신청 - 완료
            } else if (settings.name == routeCare7) {
              final CareStep7Page? args = settings.arguments as CareStep7Page?;
              return SlideLeftRoute(page: CareStep7Page(data: args!.data, viewMode: args.viewMode), setting: settings);
              //구직신청 프로필 보기
            } else if (settings.name == routeLinkMomProfile) {
              final LinkMomProfilePage? args = settings.arguments as LinkMomProfilePage?;
              return SlideLeftRoute(page: LinkMomProfilePage(data: args!.data, viewMode: args.viewMode, jobId: args.jobId, first_name: args.first_name, btnType: args.btnType), setting: settings);
            } else if (settings.name == routeMomDadyList) {
              final MomDadyListPage? args = settings.arguments as MomDadyListPage?;
              return SlideLeftRoute(page: MomDadyListPage(data: args!.data, sendCode: args.sendCode), setting: settings);
            } else if (settings.name == routeLinkMomList) {
              final LinkMomListPage? args = settings.arguments as LinkMomListPage?;
              return SlideLeftRoute(page: LinkMomListPage(data: args!.data, sendCode: args.sendCode), setting: settings);
            } else if (settings.name == routeJobFilter) {
              final JobFilterPage? args = settings.arguments as JobFilterPage?;
              return SlideLeftRoute(page: JobFilterPage(data: args!.data), setting: settings);
              //행정동 주소 검색(시도, 시군구, 읍면동)
            } else if (settings.name == routeAddress) {
              final AddressPage? args = settings.arguments as AddressPage?;
              return SlideLeftRoute(page: AddressPage(data: args!.data), setting: settings);
            } else if (settings.name == routePolicy) {
              return SlideLeftRoute(page: PolicyPage(), setting: settings);
            } else if (settings.name == routeRegisterId) {
              final RegisterIdPage? args = settings.arguments as RegisterIdPage?;
              return SlideLeftRoute(page: RegisterIdPage(registerData: args!.registerData, enableInvite: args.enableInvite), setting: settings);
            } else if (settings.name == routeRegisterPw) {
              final RegisterPwPage? args = settings.arguments as RegisterPwPage?;
              return SlideLeftRoute(page: RegisterPwPage(registerData: args!.registerData, enableInvite: args.enableInvite), setting: settings);
            } else if (settings.name == routeRegisterDisableReason) {
              final RegisterDisableReasonPage? args = settings.arguments as RegisterDisableReasonPage?;
              return SlideLeftRoute(page: RegisterDisableReasonPage(data: args!.data, req: args.req), setting: settings);
            } else if (settings.name == routeRegisterDisablePw) {
              return SlideLeftRoute(page: RegisterDisablePwPage(), setting: settings);
            } else if (settings.name == routeRegisterDisableCompleat) {
              return SlideLeftRoute(page: RegisterDisableCompletePage(), setting: settings);
              //동네인증
            } else if (settings.name == routeLocation) {
              final LocationPage? args = settings.arguments as LocationPage?;
              return SlideLeftRoute(page: LocationPage(data: args!.data), setting: settings);
            } else if (settings.name == routeChildDrugAdd) {
              final ChildDrugAddPage? args = settings.arguments as ChildDrugAddPage?;
              return SlideLeftRoute(page: ChildDrugAddPage(data: args!.data, child: args.child, drug: args.drug), setting: settings);
            } else if (settings.name == routeChildDrugList) {
              final ChildDrugListPage? args = settings.arguments as ChildDrugListPage?;
              return SlideLeftRoute(page: ChildDrugListPage(data: args!.data, child: args.child), setting: settings);
            } else if (settings.name == routeChildMenu) {
              final ChildMenuPage? args = settings.arguments as ChildMenuPage?;
              return SlideLeftRoute(page: ChildMenuPage(data: args!.data), setting: settings);
            } else if (settings.name == routeChildDrugView) {
              final ChildDrugViewPage? args = settings.arguments as ChildDrugViewPage?;
              return SlideLeftRoute(page: ChildDrugViewPage(data: args!.data, drugInfo: args.drugInfo), setting: settings);
            } else if (settings.name == routeMomDadyList) {
              final MomDadyListPage? args = settings.arguments as MomDadyListPage?;
              return SlideLeftRoute(page: MomDadyListPage(data: args!.data), setting: settings);
            } else if (settings.name == routeLinkMomList) {
              final LinkMomListPage? args = settings.arguments as LinkMomListPage?;
              return SlideLeftRoute(page: LinkMomListPage(data: args!.data), setting: settings);
            } else if (settings.name == routeMyInfoAccount) {
              final AccountManagerPage? args = settings.arguments as AccountManagerPage?;
              return SlideLeftRoute(page: AccountManagerPage(data: args!.data), setting: settings);
            } else if (settings.name == routeMyAccountEmail) {
              final MyAccountEmailPage? args = settings.arguments as MyAccountEmailPage?;
              return SlideLeftRoute(page: MyAccountEmailPage(data: args!.data), setting: settings);
            } else if (settings.name == routeMyAccountPw) {
              final MyAccountPwPage? args = settings.arguments as MyAccountPwPage?;
              return SlideLeftRoute(page: MyAccountPwPage(data: args!.data), setting: settings);
            } else if (settings.name == routeMyAccountHp) {
              final MyAccountHpPage? args = settings.arguments as MyAccountHpPage?;
              return SlideLeftRoute(page: MyAccountHpPage(data: args!.data), setting: settings);
            } else if (settings.name == routeMyPage) {
              final MyPage? args = settings.arguments as MyPage?;
              return SlideLeftRoute(page: MyPage(data: args!.data), setting: settings);
            } else if (settings.name == routeSettings) {
              return SlideLeftRoute(page: SettingsPage(), setting: settings);
            } else if (settings.name == routeSurvey) {
              final SurveyPage? args = settings.arguments as SurveyPage?;
              return SlideLeftRoute(page: SurveyPage(data: args!.data), setting: settings);
            } else if (settings.name == routeSurveyQuestion) {
              final SurveyQuestionPage? args = settings.arguments as SurveyQuestionPage?;
              return SlideLeftRoute(page: SurveyQuestionPage(data: args!.data, question: args.question, sequence: args.sequence, count: args.count, type: args.type, surveyNumber: args.surveyNumber, title: args.title), setting: settings);
            } else if (settings.name == routeSurveyCompleat) {
              final SurveyCompletePage? args = settings.arguments as SurveyCompletePage?;
              return SlideLeftRoute(page: SurveyCompletePage(data: args!.data), setting: settings);
            } else if (settings.name == routeAuthCenterCovid19) {
              final AuthCenterCovid19Page? args = settings.arguments as AuthCenterCovid19Page?;
              return SlideLeftRoute(page: AuthCenterCovid19Page(data: args!.data, surveyName: args.surveyName), setting: settings);
            } else if (settings.name == routeAuthCenterStress) {
              final AuthCenterStressPage? args = settings.arguments as AuthCenterStressPage?;
              return SlideLeftRoute(page: AuthCenterStressPage(data: args!.data, surveyName: args.surveyName), setting: settings);
            } else if (settings.name == routeAuthCenterStressResult) {
              final AuthcenterStressResultPage? args = settings.arguments as AuthcenterStressResultPage?;
              return SlideLeftRoute(page: AuthcenterStressResultPage(data: args!.data, surveyName: args.surveyName), setting: settings);
            } else if (settings.name == routeAuthCenterCrims) {
              final AuthCenterCrimsPage? args = settings.arguments as AuthCenterCrimsPage?;
              return SlideLeftRoute(page: args ?? AuthCenterCrimsPage(), setting: settings);
            } else if (settings.name == routeAuthCenterEducate) {
              final AuthCenterEducatePage? args = settings.arguments as AuthCenterEducatePage?;
              return SlideLeftRoute(page: args ?? AuthCenterEducatePage(), setting: settings);
            } else if (settings.name == routeAuthCenterGraduated) {
              final AuthCenterGraduatedPage? args = settings.arguments as AuthCenterGraduatedPage?;
              return SlideLeftRoute(page: args ?? AuthCenterGraduatedPage(), setting: settings);
            } else if (settings.name == routeAuthCenterCareer) {
              final AuthCenterCareerPage? args = settings.arguments as AuthCenterCareerPage?;
              return SlideLeftRoute(page: args ?? AuthCenterCareerPage(), setting: settings);
            } else if (settings.name == routeAuthCenterDeungbon) {
              final AuthCenterDeungbonPage? args = settings.arguments as AuthCenterDeungbonPage?;
              return SlideLeftRoute(page: args ?? AuthCenterDeungbonPage(), setting: settings);
            } else if (settings.name == routeAuthCenterHealth) {
              final AuthCenterHealthPage? args = settings.arguments as AuthCenterHealthPage?;
              return SlideLeftRoute(page: args ?? AuthCenterHealthPage(), setting: settings);
            } else if (settings.name == routeAuthCenterPersonality) {
              final AuthCenterPersonaltyPage? args = settings.arguments as AuthCenterPersonaltyPage?;
              return SlideLeftRoute(page: args ?? AuthCenterPersonaltyPage(), setting: settings);
            } else if (settings.name == routeMypageMomdaddyFavorite) {
              final MomdaddyFavoritePage? args = settings.arguments as MomdaddyFavoritePage?;
              return SlideLeftRoute(page: MomdaddyFavoritePage(data: args!.data), setting: settings);
            } else if (settings.name == routeMypageLinkmomFavorite) {
              final LinkmomFavoritePage? args = settings.arguments as LinkmomFavoritePage?;
              return SlideLeftRoute(page: LinkmomFavoritePage(data: args!.data), setting: settings);
            } else if (settings.name == routeSettingsUserBlock) {
              final SettingsUserBlockPage? args = settings.arguments as SettingsUserBlockPage?;
              return SlideLeftRoute(page: SettingsUserBlockPage(data: args!.data), setting: settings);
            } else if (settings.name == routeMypageMomdaddyReview) {
              final MomdaddyReviewPage? args = settings.arguments as MomdaddyReviewPage?;
              return SlideLeftRoute(page: args ?? MomdaddyReviewPage(), setting: settings);
            } else if (settings.name == routeMypageLinkmomReview) {
              final LinkmomReviewPage? args = settings.arguments as LinkmomReviewPage?;
              return SlideLeftRoute(page: args ?? LinkmomReviewPage(), setting: settings);
            } else if (settings.name == routeMomdadyDiary) {
              final MomdadyDiaryListPage? args = settings.arguments as MomdadyDiaryListPage?;
              return SlideLeftRoute(page: args ?? MomdadyDiaryListPage(), setting: settings);
            } else if (settings.name == routeReviewSave) {
              final ReviewSavePage? args = settings.arguments as ReviewSavePage?;
              return SlideLeftRoute(page: ReviewSavePage(viewType: args!.viewType, viewData: args.viewData, result: args.result, matchingId: args.matchingId), setting: settings);
            } else if (settings.name == routeMomdadyMatchingManage) {
              final MomdadyMatchingPage? args = settings.arguments as MomdadyMatchingPage?;
              return SlideLeftRoute(page: MomdadyMatchingPage(data: args!.data), setting: settings);
            } else if (settings.name == routeNotiSettings) {
              final SettingsNotiPage? args = settings.arguments as SettingsNotiPage?;
              return SlideLeftRoute(page: SettingsNotiPage(data: args!.data), setting: settings);
            } else if (settings.name == routeMatchingFilter) {
              final MatchingFilterPage? args = settings.arguments as MatchingFilterPage?;
              return SlideLeftRoute(page: MatchingFilterPage(data: args!.data, filterOptions: args.filterOptions, listType: args.listType), setting: settings);
            } else if (settings.name == routeMatchingFilterResult) {
              final MatchingFilterResultPage? args = settings.arguments as MatchingFilterResultPage?;
              return SlideLeftRoute(page: args ?? MatchingFilterResultPage(data: args!.data, result: args.result), setting: settings);
            } else if (settings.name == routeLinkmomMatchingManage) {
              final LinkmomMatchingPage? args = settings.arguments as LinkmomMatchingPage?;
              return SlideLeftRoute(page: LinkmomMatchingPage(data: args!.data), setting: settings);
            } else if (settings.name == routeMomdadyApply) {
              final MomdadyApplyPage? args = settings.arguments as MomdadyApplyPage?;
              return SlideLeftRoute(page: MomdadyApplyPage(data: args!.data, bookingId: args.bookingId, info: args.info, tabIndex: args.tabIndex, bywho: args.bywho), setting: settings);
            } else if (settings.name == routeMomdadyCares) {
              final MomdadyCaresPage? args = settings.arguments as MomdadyCaresPage?;
              return SlideLeftRoute(page: args ?? MomdadyCaresPage(data: args!.data, matchingId: args.matchingId), setting: settings);
            } else if (settings.name == routeLoading) {
              final LoadingPage? args = settings.arguments as LoadingPage?;
              return MaterialPageRoute(
                  builder: (context) => LoadingPage(
                        routeName: args!.routeName,
                        message: args.message,
                        isReplace: args.isReplace,
                        pageData: args.pageData,
                      ),
                  settings: settings);
            } else if (settings.name == routeChatRoom) {
              final ChatRoomPage? args = settings.arguments as ChatRoomPage?;
              mqttManager.setCurrentRouteName(routeChatRoom);
              mqttManager.setReceiver(args!.receiver);
              return SlideLeftRoute(page: ChatRoomPage(roomType: args.roomType, receiver: args.receiver, bookingcareservices: args.bookingcareservices, chatroom_id: args.chatroom_id, roomName: args.roomName), setting: settings);
            } else if (settings.name == routeChatCareInfo) {
              final ChatCareInfoPage? args = settings.arguments as ChatCareInfoPage?;
              return SlideLeftRoute(page: ChatCareInfoPage(receiver: args!.receiver, className: args.className), setting: settings);
            } else if (settings.name == routeLinkmomApply) {
              final LinkmomApplyPage? args = settings.arguments as LinkmomApplyPage?;
              return SlideLeftRoute(page: LinkmomApplyPage(data: args!.data, title: args.title, isMatchingView: args.isMatchingView, id: args.id), setting: settings);
            } else if (settings.name == routeGuideMomdady) {
              final GuideMomDadyPage? args = settings.arguments as GuideMomDadyPage?;
              return SlideLeftRoute(page: args ?? GuideMomDadyPage(), setting: settings);
            } else if (settings.name == routeGuideLinkmom) {
              final GuideLinkMomPage? args = settings.arguments as GuideLinkMomPage?;
              return SlideLeftRoute(page: args ?? GuideLinkMomPage(), setting: settings);
            } else if (settings.name == routeFindCheckplus) {
              final FindCheckplusPage? args = settings.arguments as FindCheckplusPage?;
              return SlideLeftRoute(page: FindCheckplusPage(data: args!.data, id: args.id), setting: settings);
            } else if (settings.name == routeCompleteView) {
              final CompleteViewPage? args = settings.arguments as CompleteViewPage?;
              return SlideLeftRoute(
                  page: CompleteViewPage(data: args!.data, title: args.title, body: args.body, resultMessage: args.resultMessage, finishAction: args.finishAction, topper: args.topper, footer: args.footer, btnColor: args.btnColor, btn: args.btn));
            } else if (settings.name == routeMyAccountHpChange) {
              final MyAccountHpChangePage? args = settings.arguments as MyAccountHpChangePage?;
              return SlideLeftRoute(page: MyAccountHpChangePage(data: args!.data), setting: settings);
            } else if (settings.name == routeDiaryFilter) {
              final DiaryFilterPage? args = settings.arguments as DiaryFilterPage?;
              return SlideLeftRoute(page: DiaryFilterPage(data: args!.data, type: args.type), setting: settings);
            } else if (settings.name == routeFullPhoto) {
              final FullPhotoPage? args = settings.arguments as FullPhotoPage?;
              return SlideLeftRoute(
                  setting: settings,
                  page: FullPhotoPage(
                    name: args!.name,
                    urlList: args.urlList,
                    fileList: args.fileList,
                    onPageChanged: args.onPageChanged,
                  ));
            } else if (settings.name == routeDiaryWrite) {
              final DiaryWritePage? args = settings.arguments as DiaryWritePage?;
              return SlideLeftRoute(page: DiaryWritePage(id: args!.id), setting: settings);
            } else if (settings.name == routeDiaryView) {
              final DiaryViewPage? args = settings.arguments as DiaryViewPage?;
              return SlideLeftRoute(page: DiaryViewPage(id: args!.id, type: args.type), setting: settings);
            } else if (settings.name == routePenaltyManage) {
              final PenaltyManagePage? args = settings.arguments as PenaltyManagePage?;
              return SlideLeftRoute(page: args ?? PenaltyManagePage(), setting: settings);
            } else if (settings.name == routeWorkNoti) {
              final WorkNotiPage? args = settings.arguments as WorkNotiPage?;
              return SlideLeftRoute(page: WorkNotiPage(data: args!.data), setting: settings);
            } else if (settings.name == routeLinkmomDiary) {
              final LinkmomDiaryListPage? args = settings.arguments as LinkmomDiaryListPage?;
              return SlideLeftRoute(page: args ?? LinkmomDiaryListPage(), setting: settings);
            } else if (settings.name == routeDiaryFilterResult) {
              final DiaryFilterResultPage? args = settings.arguments as DiaryFilterResultPage?;
              return SlideLeftRoute(page: args ?? DiaryFilterResultPage(data: args!.data, type: args.type, resp: args.resp, range: args.range, req: args.req), setting: settings);
            } else if (settings.name == routeDiaryDrug) {
              final DiaryDrugPage? args = settings.arguments as DiaryDrugPage?;
              return SlideLeftRoute(page: DiaryDrugPage(data: args!.data, momdadyName: args.momdadyName, isDrug: args.isDrug, linkmomName: args.linkmomName, viewType: args.viewType, diaryId: args.diaryId), setting: settings);
            } else if (settings.name == routeDiaryCareList) {
              final DiaryCareListPage? args = settings.arguments as DiaryCareListPage?;
              return SlideLeftRoute(
                  setting: settings,
                  page: DiaryCareListPage(
                    options: args!.options,
                    compleats: args.compleats,
                    type: args.type,
                    viewType: args.viewType,
                    onSelected: args.onSelected,
                    service: args.service,
                    area: args.area,
                  ));
            } else if (settings.name == routeRecipe) {
              final RecipeViewPage? args = settings.arguments as RecipeViewPage?;
              return SlideLeftRoute(
                  page: RecipeViewPage(appbarTitle: args!.appbarTitle, id: args.id, type: args.type, status: args.status, claimId: args.claimId, matchingId: args.matchingId, isLast: args.isLast, isSignOut: args.isSignOut), setting: settings);
            } else if (settings.name == routeClaimView) {
              final DiaryClaimViewPage? args = settings.arguments as DiaryClaimViewPage?;
              return SlideLeftRoute(page: DiaryClaimViewPage(claimId: args!.claimId, viewType: args.viewType), setting: settings);
            } else if (settings.name == routeClaimAnswer) {
              final DiaryClaimAnswerPage? args = settings.arguments as DiaryClaimAnswerPage?;
              return SlideLeftRoute(page: DiaryClaimAnswerPage(id: args!.id, claimReason: args.claimReason), setting: settings);
            } else if (settings.name == routePenaltyHistory) {
              final PenaltyHistoryPage? args = settings.arguments as PenaltyHistoryPage?;
              return SlideLeftRoute(page: PenaltyHistoryPage(penalty: args!.penalty, index: args.index), setting: settings);
            } else if (settings.name == routePenaltyRemove) {
              final PenaltyRemovePage? args = settings.arguments as PenaltyRemovePage?;
              return SlideLeftRoute(page: PenaltyRemovePage(penalty: args!.penalty), setting: settings);
            } else if (settings.name == routeCommunityView) {
              final CommunityViewPage? args = settings.arguments as CommunityViewPage?;
              return SlideLeftRoute(page: args ?? CommunityViewPage(id: args!.id), setting: settings);
            } else if (settings.name == routeCommunityWrite) {
              final CommunityWritePage? args = settings.arguments as CommunityWritePage?;
              return SlideLeftRoute(page: args ?? CommunityWritePage(), setting: settings);
            } else if (settings.name == routeCommunityEvent) {
              final CommunityEventPage? args = settings.arguments as CommunityEventPage?;
              return SlideLeftRoute(page: args ?? CommunityEventPage(fromTo: args!.fromTo, id: args.id, title: args.title, event: args.event, url: args.url), setting: settings);
            } else if (settings.name == routeCommunityRank) {
              final CommunityRankPage? args = settings.arguments as CommunityRankPage?;
              return SlideLeftRoute(page: CommunityRankPage(rank: args!.rank, type: args.type, id: args.id, hcode: args.hcode, scroll: args.scroll), setting: settings);
            } else if (settings.name == routeCs) {
              final CsPage? args = settings.arguments as CsPage?;
              return SlideLeftRoute(page: args ?? CsPage(), setting: settings);
            } else if (settings.name == routeCsNotice) {
              final CsNoticePage? args = settings.arguments as CsNoticePage?;
              return SlideLeftRoute(page: args ?? CsNoticePage(), setting: settings);
            } else if (settings.name == routeCsQna) {
              final CsFaqPage? args = settings.arguments as CsFaqPage?;
              return SlideLeftRoute(page: args ?? CsFaqPage(), setting: settings);
            } else if (settings.name == routeCsService) {
              final CsServicePage? args = settings.arguments as CsServicePage?;
              return SlideLeftRoute(page: args ?? CsServicePage(), setting: settings);
            } else if (settings.name == routeCsGuide) {
              final CsGuidePage? args = settings.arguments as CsGuidePage?;
              return SlideLeftRoute(page: args ?? CsGuidePage(), setting: settings);
            } else if (settings.name == routeCsContact) {
              final CsContactPage? args = settings.arguments as CsContactPage?;
              return SlideLeftRoute(page: args ?? CsContactPage(), setting: settings);
            } else if (settings.name == routeCsTerms) {
              final CsTermsPage? args = settings.arguments as CsTermsPage?;
              return SlideLeftRoute(page: args ?? CsTermsPage(), setting: settings);
            } else if (settings.name == routeCsContactView) {
              final CsContactViewPage? args = settings.arguments as CsContactViewPage?;
              return SlideLeftRoute(page: CsContactViewPage(id: args!.id), setting: settings);
            } else if (settings.name == routeCsTermsView) {
              final CsTermsViewPage? args = settings.arguments as CsTermsViewPage?;
              return SlideLeftRoute(page: CsTermsViewPage(terms: args!.terms), setting: settings);
            } else if (settings.name == routeCsNoticeView) {
              final CsNoticeViewPage? args = settings.arguments as CsNoticeViewPage?;
              return SlideLeftRoute(page: CsNoticeViewPage(notice: args!.notice), setting: settings);
            } else if (settings.name == routeNotiCenter) {
              final NotiCenterPage? args = settings.arguments as NotiCenterPage?;
              return SlideLeftRoute(page: NotiCenterPage(data: args!.data), setting: settings);
            } else if (settings.name == routeDiaryDrugList) {
              final DiaryDrugListPage? args = settings.arguments as DiaryDrugListPage?;
              return SlideLeftRoute(
                  setting: settings,
                  page: DiaryDrugListPage(drugId: args!.drugId, scheduleId: args.scheduleId, diaryId: args.diaryId, childName: args.childName, linkmomName: args.linkmomName, momdadyName: args.momdadyName, onChecked: args.onChecked));
            } else if (settings.name == routePayMent) {
              final PaymentPage? args = settings.arguments as PaymentPage?;
              return SlideLeftRoute(
                  setting: settings,
                  page: PaymentPage(
                    data: args!.data,
                    callBack: args.callBack,
                  ));
            } else if (settings.name == routePayMentIamPort) {
              final PaymentIamportPage? args = settings.arguments as PaymentIamportPage?;
              return SlideLeftRoute(
                  setting: settings,
                  page: PaymentIamportPage(
                    payInitData: args!.payInitData,
                    payData: args.payData,
                    payRrevRequest: args.payRrevRequest,
                  ));
            } else if (settings.name == routePayMentCancel) {
              final PaymentCancelPage? args = settings.arguments as PaymentCancelPage?;
              return SlideLeftRoute(
                  setting: settings,
                  page: PaymentCancelPage(
                    payCancelInitRequest: args!.payCancelInitRequest,
                    payCancelViewRequest: args.payCancelViewRequest,
                    images: args.images,
                    viewType: args.viewType,
                    callBack: args.callBack,
                  ));
            } else if (settings.name == routePayMentCancelComplete) {
              return SlideLeftRoute(page: PaymentCancelCompletePage(), setting: settings);
            } else if (settings.name == routePayMentComplete) {
              final PaymentCompletePage? args = settings.arguments as PaymentCompletePage?;
              return MaterialPageRoute(
                  settings: settings,
                  builder: (context) {
                    return PaymentCompletePage(
                      payMentCompleteData: args!.payMentCompleteData,
                    );
                  });
            } else if (settings.name == routePayMentCancelReason) {
              final PaymentCancelReasonPage? args = settings.arguments as PaymentCancelReasonPage?;
              return SlideLeftRoute(
                  setting: settings,
                  page: PaymentCancelReasonPage(
                    payCancelInitRequest: args!.payCancelInitRequest,
                    images: args.images,
                    viewMode: args.viewMode ?? ViewMode(),
                    callBack: args.callBack,
                  ));
            } else if (settings.name == routePayMentCancelSelect) {
              final PaymentCancelSelectPage? args = settings.arguments as PaymentCancelSelectPage?;
              return SlideLeftRoute(
                  setting: settings,
                  page: PaymentCancelSelectPage(
                    payCancelSelectData: args!.payCancelSelectData,
                    isLinkMom: args.isLinkMom,
                    viewType: args.viewType,
                    callBack: args.callBack,
                  ));
            } else if (settings.name == routePayMentExplain) {
              final PaymentExplainPage? args = settings.arguments as PaymentExplainPage?;
              return SlideLeftRoute(setting: settings, page: PaymentExplainPage(explainRequest: args!.explainRequest, callBack: args.callBack));
            } else if (settings.name == routePayMentExplainComplete) {
              return SlideLeftRoute(page: PaymentExplainCompletePage(), setting: settings);
            } else if (settings.name == routePayMentRefundRule) {
              // final DiaryDrugListPage? args = settings.arguments as DiaryDrugListPage?;
              return SlideLeftRoute(page: PaymentRefundRulePage(), setting: settings);
            } else if (settings.name == routePayMentCancelList) {
              final PayInfoRequest? args = settings.arguments as PayInfoRequest?;
              return SlideLeftRoute(setting: settings, page: PaymentCancelListPage(payCancelInfoRequest: args));
            } else if (settings.name == routeMomdadySchedule) {
              final MomdadySchedulePage? args = settings.arguments as MomdadySchedulePage?;
              return SlideLeftRoute(page: MomdadySchedulePage(data: args!.data, sTime: args.sTime, eTime: args.eTime), setting: settings);
            } else if (settings.name == routeLinkmomSchedule) {
              final LinkmomSchedulePage? args = settings.arguments as LinkmomSchedulePage?;
              return SlideLeftRoute(page: LinkmomSchedulePage(data: args!.data), setting: settings);
            } else if (settings.name == routePreview) {
              final PreviewPage? args = settings.arguments as PreviewPage;
              return SlideLeftRoute(page: args ?? PreviewPage(), setting: settings);
            } else if (settings.name == routeReviewView) {
              final ReviewViewPage? args = settings.arguments as ReviewViewPage;
              return SlideLeftRoute(page: ReviewViewPage(userId: args!.userId, gubun: args.gubun), setting: settings);
            } else if (settings.name == routePaymentHistory) {
              final PaymentHistoryPage? args = settings.arguments as PaymentHistoryPage;
              return SlideLeftRoute(page: args ?? PaymentHistoryPage(), setting: settings);
            } else if (settings.name == routePaymentFilter) {
              final PaymentFilterPage? args = settings.arguments as PaymentFilterPage;
              return SlideLeftRoute(page: PaymentFilterPage(onSearch: args!.onSearch, filterOptions: args.filterOptions), setting: settings);
            } else if (settings.name == routePaymentRecipe) {
              final PaymentRecipePage? args = settings.arguments as PaymentRecipePage;
              return SlideLeftRoute(page: PaymentRecipePage(url: args!.url), setting: settings);
            } else if (settings.name == routePaymentFilterResult) {
              final PaymentFilterResultPage? args = settings.arguments as PaymentFilterResultPage;
              return SlideLeftRoute(page: PaymentFilterResultPage(range: args!.range, info: args.info, onSearch: args.onSearch), setting: settings);
            } else if (settings.name == routePaymentExpired) {
              final PaymentExpiredPage? args = settings.arguments as PaymentExpiredPage;
              return SlideLeftRoute(page: PaymentExpiredPage(info: args!.info), setting: settings);
            } else if (settings.name == routeRegisterDisable) {
              final RegisterDisablePage? args = settings.arguments as RegisterDisablePage;
              return SlideLeftRoute(page: RegisterDisablePage(data: args!.data), setting: settings);
            } else if (settings.name == routeDiaryWriteComplete) {
              final DiaryWriteCompletePage? args = settings.arguments as DiaryWriteCompletePage;
              return SlideLeftRoute(page: args ?? DiaryWriteCompletePage(), setting: settings);
            } else if (settings.name == routeRegisterAccount) {
              final RegisterAccountPage? args = settings.arguments as RegisterAccountPage;
              return SlideLeftRoute(page: args ?? RegisterAccountPage(), setting: settings);
            } else if (settings.name == routeWithdrawalFilter) {
              final WithdrawalFilterPage? args = settings.arguments as WithdrawalFilterPage;
              return SlideLeftRoute(page: args ?? WithdrawalFilterPage(), setting: settings);
            } else if (settings.name == routeWithdrawalFilterResult) {
              final WithdarawalFilterResultPage? args = settings.arguments as WithdarawalFilterResultPage;
              return SlideLeftRoute(page: args ?? WithdarawalFilterResultPage(list: null, range: null), setting: settings);
            } else if (settings.name == routeWithdrawal) {
              final WithdrawalPage? args = settings.arguments as WithdrawalPage;
              return SlideLeftRoute(page: args ?? WithdrawalPage(), setting: settings);
            } else if (settings.name == routeBankList) {
              final BankListPage? args = settings.arguments as BankListPage;
              return SlideLeftRoute(page: args ?? BankListPage(), setting: settings);
            } else if (settings.name == routeCommunityNotify) {
              final CommunityNotifyPage? args = settings.arguments as CommunityNotifyPage;
              return SlideLeftRoute(page: args ?? CommunityNotifyPage(boardId: args!.boardId, userId: args.userId, eventReplyId: args.eventReplyId), setting: settings);
            } else if (settings.name == routeCareNotify) {
              final CareNotifyPage? args = settings.arguments as CareNotifyPage;
              return SlideLeftRoute(page: args ?? CareNotifyPage(userId: args!.userId, matchingId: args.matchingId, chatId: args.chatId, reasons: args.reasons), setting: settings);
            } else if (settings.name == routeDiaryNotify) {
              final DiaryNotifyPage? args = settings.arguments as DiaryNotifyPage;
              return SlideLeftRoute(page: args ?? DiaryNotifyPage(id: args!.id), setting: settings);
            } else if (settings.name == routeMatchingSign) {
              final MatchingSignPage? args = settings.arguments as MatchingSignPage;
              return SlideLeftRoute(page: MatchingSignPage(callback: args!.callback, type: args.type), setting: settings);
            } else if (settings.name == routeRegisterInvite) {
              final RegisterInvitePage? args = settings.arguments as RegisterInvitePage;
              return SlideLeftRoute(page: args ?? RegisterInvitePage(registerData: args!.registerData, enableInvite: args.enableInvite), setting: settings);
            } else if (settings.name == routeBenefit) {
              final BenefitPage? args = settings.arguments as BenefitPage;
              return SlideLeftRoute(page: args ?? BenefitPage(data: args!.data), setting: settings);
            } else if (settings.name == routeCouponDetail) {
              final CouponDetailPage? args = settings.arguments as CouponDetailPage;
              return SlideLeftRoute(page: args ?? CouponDetailPage(), setting: settings);
            } else if (settings.name == routeHomeState) {
              final HomeStatePage? args = settings.arguments as HomeStatePage;
              return SlideLeftRoute(page: args ?? HomeStatePage(), setting: settings);
            } else if (settings.name == routeAddressSearch) {
              final AddressSearchPage? args = settings.arguments as AddressSearchPage;
              return SlideLeftRoute(page: args ?? AddressSearchPage(), setting: settings);
            } else if (settings.name!.contains(routeLink) || Commons.linkRoutes.where((route) => settings.name!.startsWith(route)).isNotEmpty) {
              log.d('called deeplink : settings: $settings,  <<< ');
              final DeepLinkingPage? args = settings.arguments as DeepLinkingPage;
              return SlideLeftRoute(page: args ?? DeepLinkingPage(url: settings.name ?? ''), setting: settings);
            } else {
              if (settings.name != '/') return SlideLeftRoute(page: DeepLinkingPage(), setting: settings);
            }
            return null;
          },
        ),
      ),
    );
  }
}

class RestartWidget extends StatefulWidget {
  RestartWidget({required this.child});

  final Widget child;

  static void restartApp(BuildContext context) {
    context.findAncestorStateOfType<_RestartWidgetState>()!.restartApp();
  }

  @override
  _RestartWidgetState createState() => _RestartWidgetState();
}

class _RestartWidgetState extends State<RestartWidget> {
  Key key = UniqueKey();

  void restartApp() {
    setState(() {
      key = UniqueKey();
    });
  }

  @override
  Widget build(BuildContext context) {
    return KeyedSubtree(
      key: key,
      child: widget.child,
    );
  }
}
