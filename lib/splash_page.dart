import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:linkmom/data/storage/model/menu_file_data.dart';
import 'package:linkmom/route_name.dart';
import 'package:linkmom/utils/style/color_style.dart';
import 'package:linkmom/utils/style/image_style.dart';
import 'package:package_info/package_info.dart';

import 'base/base_stateful.dart';
import 'data/network/api_header.dart';
import 'data/network/models/login_request.dart';
import 'data/network/models/version_check_response.dart';
import 'data/storage/model/menu_file.dart';
import 'main.dart';
import 'manager/enum_manager.dart';
import 'utils/commons.dart';
import 'utils/custom_dailog/custom_dailog.dart';
import 'utils/style/linkmom_style.dart';
import 'utils/style/text_style.dart';

class SplashPage extends BaseStateful {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends BaseStatefulState<SplashPage> {
  ConnectivityResult _connectivityResult = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  String msg = "네트워크확인".tr();
  String btn = "설정".tr();
  late String _localPath;
  SplashType splashType = SplashType.version;
  String progressPercent = '0';
  late Widget? _view;

  bool isNext = false;
  bool isTestMode = false;
  bool isLogin = false;

  var _splashController = StreamController<AsyncSnapshot>();

  Stream<AsyncSnapshot> splashStream() {
    _splashController = StreamController<AsyncSnapshot>();
    return _splashController.stream;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(AssetImage(IMG_LOGO), this.context);
  }

  @override
  void initState() {
    super.initState();

    log.d('『GGUMBI』>>> initState App Mode - DebugMode: $kDebugMode, ReleaseMode: $kReleaseMode, ProfileMode: $kProfileMode  <<< ');

    _connectivityResult = network.connectivityResult;
    initConnectivity();
    _initLocalPath();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    storageHelper.getUserType();
    Commons.checkPermission();
  }

  @override
  Widget build(BuildContext context) {
    statusBarHeight = MediaQuery.of(context).padding.top;
    if (_connectivityResult != ConnectivityResult.none) {
      msg = "네트워크확인중".tr();
      btn = "확인".tr();
    }
    return Stack(children: [
      splashCircularIndicate(),
      _initSplash(context),
    ]);
  }

  ///메뉴 파일 경로 읽어오기
  Future<void> _initLocalPath() async {
    _localPath = await Commons.findLocalPath();

    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    //해당 폴더가 없으면 만듬
    if (!hasExisted) {
      savedDir.create();
    }
  }

  ///메뉴 파일 데이터 가져오기
  Future<void> _menuFileLoad(File file) async {
    log.d('『GGUMBI』>>> _initLocalPath : _localFile: $file,  <<< ');

    try {
      String contents = await file.readAsString();
      log.d('『GGUMBI』>>> _menuFileLoad : contents: $contents,  <<< ');
      Map<String, dynamic> fileData = jsonDecode(contents);
      MenuFileData data = MenuFileData.fromJson(fileData);
      storageHelper.setMenuFileData(data);
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> _initLocalPath : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $e');
    }
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    log.d('『GGUMBI』>>> _updateConnectionStatus : result: $result,  <<< ');
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
      case ConnectivityResult.none:
        setState(() => _connectivityResult = result);
        break;
      default:
        setState(() => _connectivityResult = result);
        break;
    }
  }

  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  @override
  void onResume() async {
    var connectivityResult = await network.isCheckNetwork();
    log.d('『GGUMBI』>>> didChangeAppLifecycleState : connectivityResult: ${connectivityResult.toString()},  <<< ');
    if (connectivityResult != ConnectivityResult.none) {
      msg = "네트워크확인중".tr();
      btn = "확인".tr();
    } else {
      msg = "네트워크확인".tr();
      btn = "설정".tr();
    }
    super.onResume();
  }

  Future<Widget?> onVersionCheck(VersionCheckResponse data, BuildContext context) async {
    try {
      if (data.dataList == null) {
        return showNormalDlg(
            context: context,
            message: "잠시후다시시도해주세요".tr(),
            onClickAction: (action) {
              splashType = SplashType.version;
              onUpDate();
              _splashController.add(AsyncSnapshot.withData(ConnectionState.waiting, splashType));
            }); //버전체크 에러시....예외처리하기
      }

      VersionCheckData item = data.dataList == null ? VersionCheckData() : data.getData();
      log.d('『GGUMBI』>>> onVersionCheck ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< 111 $item ');

      ///응답 받은 값으로 세팅
      auth.onUpdateUser(
        auth.user.id,
        auth.user.username,
        auth.user.password,
        auth.user.auto_login,
        auth.user.token_access,
        auth.user.token_refresh,
        data.dataCookies[HEADER_KEY_CSRFTOKEN],
        data.dataCookies[HEADER_KEY_CSRFTOKEN_EXPIRE],
      );

      log.d('『GGUMBI』>>> onVersionCheck : serverVersion 1 : loginL${auth.getAutoLogin}, ${item.verMajor}.${item.verMinor}.${item.verPatch}, AppVersion: ${deviceInfo.major}.${deviceInfo.minor}.${deviceInfo.patch}, data: $item  <<< ');
      //1. 앱종료 Notice 2 이면, 서버에서 긴급점검 또는 불가피한 사유로 정지시 1순위로 체크한다.
      if (item.notice == VersionType.finish.value) {
        log.d('『GGUMBI』>>> onVersionCheck : serverVersion 2 : loginL${auth.getAutoLogin}, ${item.verMajor}.${item.verMinor}.${item.verPatch}, AppVersion: ${deviceInfo.major}.${deviceInfo.minor}.${deviceInfo.patch}, data: $item  <<< ');
        return showNormalDlg(
            message: item.contents,
            btnLeft: "앱종료".tr(),
            onClickAction: (action) {
              switch (action) {
                case DialogAction.yes:
                  Commons.appFinish();
                  break;
                default:
              }
            });
      }

      //버전확인
      bool isUpdate = isVersionCheck(item);

      //2.강제 업데이트가 1(강제종료) 이면 무조건 스토어로 보낸다. "새로운 버전이 출시되었습니다. 업데이트 후 사용해주세요. (계속 진행시 원활한 서비스가 제공되지 않을 수 있습니다.)",
      if (item.update == VersionType.confirm.value) {
        log.d('『GGUMBI』>>> onVersionCheck : serverVersion 3 : loginL${auth.getAutoLogin}, ${item.verMajor}.${item.verMinor}.${item.verPatch}, AppVersion: ${deviceInfo.major}.${deviceInfo.minor}.${deviceInfo.patch}, data: $item  <<< ');
        if (isUpdate) {
          return showNormalDlg(
              message: item.contents,
              onClickAction: (action) {
                switch (action) {
                  case DialogAction.yes:
                    Commons.lLaunchUrl(item.marketUrl);
                    Future.delayed(Duration(seconds: 1)).then((value) => Commons.appFinish());
                    break;
                  default:
                }
              });
        }
      }

      //3.업데이트 여부 확인 (서버 버전이 높으면 알림창 노출)
      if (isUpdate && item.notice == 1) {
        log.d('『GGUMBI』>>> onVersionCheck : serverVersion 4 : loginL${auth.getAutoLogin}, ${item.verMajor}.${item.verMinor}.${item.verPatch}, AppVersion: ${deviceInfo.major}.${deviceInfo.minor}.${deviceInfo.patch}, data: $item  <<< ');
        return showNormalDlg(
            message: item.contents,
            btnLeft: "계속진행".tr(),
            btnRight: "업데이트".tr(),
            onClickAction: (action) {
              switch (action) {
                case DialogAction.no:
                  Commons.lLaunchUrl(item.marketUrl);
                  Future.delayed(Duration(seconds: 1)).then((value) => Commons.appFinish());
                  break;
                case DialogAction.yes:
                  splashType = SplashType.download;
                  onUpDate();
                  break;
                default:
              }
            });
      }
      log.d(
          '『GGUMBI』>>> onVersionCheck : serverVersion 5 : loginL${auth.getAutoLogin}, ${item.verMajor}, ${item.verMajor}.${item.verMinor}.${item.verPatch}, AppVersion: ${deviceInfo.major}.${deviceInfo.minor}.${deviceInfo.patch}, data: $item  <<< ');
      return null;
    } catch (e) {
      return Container();
    }
  }

  bool isVersionCheck(VersionCheckData item) {
    //3.버전확인
    bool isUpdate = false;

    //1. 리뉴얼 및 고도화 작업시
    if (item.verMajor > deviceInfo.major) {
      isUpdate = true;
    }

    //2. API 및 신규 서비스 업데이트시
    if (item.verMajor >= deviceInfo.major && item.verMinor > deviceInfo.minor) {
      isUpdate = true;
    }

    //3. 버그 픽스 및 문구 수정
    if (item.verMajor >= deviceInfo.major && item.verMinor >= deviceInfo.minor && item.verPatch > deviceInfo.patch) {
      isUpdate = true;
    }

    return isUpdate;
  }

  Widget? onLoginCheck() {
    try {
      if (isTestMode) {
        return lLoadingIndicator(fn: setTestMode);
      }
      if (auth.user.username.isNotEmpty && auth.getAutoLogin) {
        LoginRequest login = LoginRequest(
          username: auth.user.username,
          password: auth.user.password,
          is_user_linkmom: auth.user.linkmom,
          is_user_momdady: auth.user.momdaddy,
        );
        apiHelper.requestLogin(login).then((response) async {
          if (response.getCode() == KEY_SUCCESS) {
            var name = await storageHelper.getPkuName();
            if (name.isEmpty) {
              storageHelper.setUserType(response.getData().user!.is_user_momdady ? USER_TYPE.mom_daddy : USER_TYPE.link_mom);
            }
            Commons.doLoginFlow(context, '$SplashPage', response.getData());
            apiHelper.requestSettingsView().then((response) {
              if (response.getCode() == KEY_SUCCESS) {
                storageHelper.setSettings(response.getData());
              }
            });
          } else {
            if (response.getCode() == KEY_1401_UNAUTHORIZED || response.getCode() == KEY_1402_UNAUTHORIZED) {
              auth.setAutoLogin = false;
              showNormalDlg(
                message: response.getMsg(),
                onClickAction: (action) => Commons.pageClear(navigatorKey.currentContext!, routeLogin),
              );
            }
          }
        }).catchError((e) {});
      } else {
        log.d('『GGUMBI』>>> onLoginCheck ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
        storageHelper.removeAll().then((value) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Commons.moveIntro(context);
          });
        });
      }
      return null;
    } catch (e) {
      log.e('『GGUMBI』 Exception >>> onLoginCheck : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n ${e.toString}');
      return null;
    }
  }

  Widget splashCircularIndicate() {
    return lScaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(children: [
              Padding(
                padding: const EdgeInsets.only(top: 150),
                child: lText("우리동네아이돌봄".tr(), style: st_18(textColor: color_999999, fontWeight: FontWeight.w500)),
              ),
              //2021/11/30 하단부분 짤리는 경우가 있어서 100-> 80 으로 변경
              Container(padding: EdgeInsets.fromLTRB(70, 5, 70, 12), child: Image.asset(IMG_LOGO)),
              lText("인트로_메세지".tr(), style: st_16(textColor: color_999999)),
              sb_h_80,
            ]),
            Stack(
              alignment: Alignment.bottomCenter,
              children: [
                Image.asset(ILLUST_INTRO, width: 250, fit: BoxFit.fitWidth),
                Padding(
                  padding: padding_20_B,
                  child: lText(
                    splashType == SplashType.version
                        ? "버전체크중".tr()
                        : splashType == SplashType.download
                            ? '${"로딩중".tr()}${progressPercent == '0' ? '' : progressPercent}'
                            : "로그인중".tr(),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _initSplash(BuildContext context) {
    return StreamBuilder(
      stream: splashStream(),
      builder: (BuildContext context, snapshot) {
        log.d('『GGUMBI』>>> _initSplash : snapshot: $splashType, \n${snapshot.connectionState}, \n${_splashController.isClosed}, \n${!snapshot.hasData}, \n${snapshot.data as AsyncSnapshot}, \n$isNext  <<< ');
        switch (splashType) {
          case SplashType.version:
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                //데이터가 없을 때 호출.
                if (!snapshot.hasData) {
                  log.d('『GGUMBI』>>> _initSplash : snapshot 222 : $splashType, \n${snapshot.connectionState}, \n${_splashController.isClosed}, \n${!snapshot.hasData}, \n${snapshot.data as AsyncSnapshot}, \n$isNext  <<< ');
                  _splashController.add(AsyncSnapshot.withData(ConnectionState.active, splashType));
                } else {
                  //ios15에서 응답이 없는 경우
                  if (!isNext) {
                    _splashController.add(AsyncSnapshot.withData(ConnectionState.active, splashType));
                    log.d('『GGUMBI』>>> _initSplash : snapshot 333 : $splashType, \n${snapshot.connectionState}, \n${_splashController.isClosed}, \n${!snapshot.hasData}, \n${snapshot.data as AsyncSnapshot}, \n$isNext  <<< ');
                  }
                }
                return lLoadingIndicator(fn: setTestMode);
              case ConnectionState.active:
                if (snapshot.data is AsyncSnapshot) {
                  isNext = true;
                  AsyncSnapshot _data = snapshot.data as AsyncSnapshot;
                  log.d('『GGUMBI』>>> _initSplash : _data: ${_data.data}, ${_data.connectionState}  <<< ');
                  switch (_data.connectionState) {
                    case ConnectionState.active:
                      apiHelper.requestVersionCheck().then((response) async {
                        log.d('『GGUMBI』>>> _initSplash  requestVersionCheck : response: $response,  <<< ');
                        versionCheckData = response;
                        if (versionCheckData != null) {
                          storageHelper.setAppVersion(versionCheckData!.getData());
                          Commons.getRemoteConfig();
                          //위젯이 null 이면 정상 코드
                          _view = await onVersionCheck(versionCheckData!, context);
                          if (_view != null) {
                            return _view;
                          }
                          log.d('『GGUMBI』>>> _initSplash : currentPage: $_view, \nSplashType: $splashType}, <<< ');
                          splashType = SplashType.download;
                          onUpDate();
                          _splashController.add(AsyncSnapshot.withData(ConnectionState.done, splashType));
                        }
                      }).catchError((onError) {
                        log.e('『GGUMBI』 Exception >>> requestVersionCheck : ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆★ <<< \n $onError');
                      });
                      break;
                    default:
                      break;
                  }
                }
                break;
              default:
            }
            return lLoadingIndicator(fn: setTestMode);
          case SplashType.download:
            if (versionCheckData!.dataList == null) {
              return lLoadingIndicator(fn: setTestMode);
            }
            VersionCheckData item = versionCheckData!.dataList == null ? VersionCheckData() : versionCheckData!.getData();
            String url = item.utils!.menu_file;
            var splVersion = url.split('/');
            var splName = splVersion[splVersion.length - 1].split('.');
            String name = splName[splName.length - 1];
            log.d('『GGUMBI』>>> _initMenuFile onVersionCheck : splName: $splName, $item <<< ');

            int start = 0;
            int end = splVersion[splVersion.length - 1].indexOf(name) - 1;
            String version = splVersion[splVersion.length - 1].substring(start, end);

            MenuFile newMenuFile = MenuFile(version: version, name: name);
            if (newMenuFile.version != storageHelper.menu_file.version) {
              apiHelper.downLoadFile(
                url,
                callBack: (received, total) {
                  if (total != -1) {
                    progressPercent = (received / total * 100).toStringAsFixed(0) + "%";
                    onUpDate();
                  }
                  return null;
                },
              ).then((response) async {
                if (response.statusCode == KEY_SUCCESS) {
                  String savePath = getMenuPath(newMenuFile);
                  File file = File(savePath);
                  var raf = file.openSync(mode: FileMode.write);
                  raf.writeFromSync(response.data);
                  await raf.close().then((value) async {
                    await _menuFileLoad(file);
                  });
                  // NOTE: 해당 버전과 이름을 저장한다.
                  storageHelper.setMenuFile(newMenuFile);
                }
              });
            } else {
              storageHelper.getMenuFileData();
            }
            _download(item.utils!.bank_file_size, item.utils!.bank, item.utils!.holiday!);
            log.d('『GGUMBI』>>> _download ★★★★★★★★★★★★★ CHECK ★★★★★★★★★★★★★ <<< ');
            splashType = SplashType.complete;
            _splashController.add(AsyncSnapshot.withData(ConnectionState.active, splashType));

            // NOTE : 행정동 주소 데이터 만들기
            Commons.loadAddressFile();
            return lLoadingIndicator(fn: setTestMode);
          case SplashType.complete:
            log.d('『GGUMBI』>>> _initSplash : SplashType.complete: ${SplashType.complete},  <<< ');
            if (isLogin) {
              return lLoadingIndicator(fn: setTestMode);
            }

            _splashController.close();
            isLogin = true;
            return onLoginCheck() ?? lLoadingIndicator(fn: setTestMode);
        }
      },
    );
  }

  String getMenuPath(MenuFile menu) {
    return '$_localPath/${menu.version}.${menu.name}';
  }

  void setTestMode() {
    isTestMode = true;
    if (!Commons.isDebugMode) {
      return;
    }

    Commons.setTestMode(callBack: () => setTestModeUpdate());
  }

  Future<void> setTestModeUpdate() async {
    isTestMode = false;
    splashType = SplashType.version;
    Commons.appReStart();
    onUpDate();
  }

  Future<void> _download(int bankSize, String bankUrl, List<String> holidayUrl) async {
    String localPath = await Commons.findLocalPath();
    await Commons.downloadBank(bankUrl, localPath, bankSize);
    await Commons.downloadHoliday(holidayUrl, localPath);
  }
}

///디바이스 정보 가져오기 (Android, iOS)
///공통 헤드 부분 세팅
initPlatformDevice() async {
  final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  try {
    late String osType, osVer, deviceBrand, deviceModel;
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      osType = Commons.ANDROID;
      osVer = androidInfo.version.release!;
      deviceBrand = androidInfo.brand!.toLowerCase();
      deviceModel = androidInfo.model!.toLowerCase();
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      osType = Commons.IOS;
      osVer = iosInfo.systemVersion!;
      deviceBrand = Commons.APPLE;
      deviceModel = iosInfo.model!.toLowerCase();
    }

    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String versionCode = packageInfo.version;

    var version = versionCode.split(".");
    verMajor = int.parse(version[0]);
    verMinor = int.parse(version[1]);
    verPatch = int.parse(version[2]);

    String appName = packageInfo.appName;
    String packageName = packageInfo.packageName;
    String buildNumber = packageInfo.buildNumber;

    apiHeader.setApiHeaderDevice(cookies[HEADER_KEY_CSRFTOKEN] ?? '', cookies[HEADER_KEY_COOKIE] ?? '', packageInfo.version, osType, osVer, deviceBrand, deviceModel);
    mqttManager.setClient();

    log.d({
      '_apiHeader.basicApiHeader': apiHeader.basicApiHeader,
      'appName': appName,
      'packageName': packageName,
      'versionCode': versionCode,
      'buildNumber': buildNumber,
    });
  } on PlatformException {}
}
