import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:linkmom/utils/commons.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class NetworkProvider extends ChangeNotifier {
  static NetworkProvider of(BuildContext context) {
    return Provider.of<NetworkProvider>(context);
  }

  NetworkProvider() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      resultHandler(result);
    });
  }

  ConnectivityResult _connectivityResult = ConnectivityResult.none;

  // String _svgUrl = 'assets/serverDown.svg';
  String _pageText = 'Currently connected to no network. Please connect to a wifi network!';

  ConnectivityResult get connectivityResult => _connectivityResult;

  bool _isConnect = false;

  bool get isConnect => _isConnect;

  // String get svgUrl => _svgUrl;
  String get pageText => _pageText;

  late String _language;

  String get language => _language;

  void resultHandler(ConnectivityResult result) {
    _connectivityResult = result;
    if (result == ConnectivityResult.none) {
      // _svgUrl = 'assets/serverDown.svg';
      // _pageText = 'Currently connected to no network. Please connect to a wifi network!';
      _pageText = 'Not Network';
      _isConnect = false;
      log.d('『GGUMBI』>>> resultHandler : _pageText: $_pageText,  <<< ');
    } else if (result == ConnectivityResult.mobile) {
      // _svgUrl = 'assets/noWifi.svg';
      // _pageText = 'Currently connected to a celluar network. Please connect to a wifi network!';
      _pageText = 'Mobile';
      _isConnect = true;
      log.d('『GGUMBI』>>> resultHandler : _pageText: $_pageText,  <<< ');
    } else if (result == ConnectivityResult.wifi) {
      // _svgUrl = 'assets/connected.svg';
      // _pageText = 'Connected to a wifi network!';
      _pageText = 'WIFI';
      _isConnect = true;
      log.d('『GGUMBI』>>> resultHandler : _pageText: $_pageText,  <<< ');
    }
    _language = Commons.getLanguagerCode();
    notifyListeners();
  }

  void initialLoad() async {
    ConnectivityResult connectivityResult = await (Connectivity().checkConnectivity());
    resultHandler(connectivityResult);
  }

  Future<bool> isNetwork() async {
    var status = await (Connectivity().checkConnectivity());

    log.d('『GGUMBI』>>> isNetwork : status: $status,  <<< ');
    if (status == ConnectivityResult.none) {
      return false;
    }
    return true;
  }

  Future<ConnectivityResult> isCheckNetwork() async {
    var status = await (Connectivity().checkConnectivity());
    return status;
  }
}

// class ConnectivityService {
//   // Create our public controller
//   StreamController<ConnectivityStatus> connectionStatusController = StreamController<ConnectivityStatus>();
//
//   ConnectivityService() {
//     // Subscribe to the connectivity Chanaged Steam
//     Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
//       // Use Connectivity() here to gather more info if you need t
//
//       connectionStatusController.add(_getStatusFromResult(result));
//     });
//
//     // initConnectivity(value) => _getStatusFromResult(value);
//   }
//
//   // Convert from the third part enum to our own enum
//   ConnectivityStatus _getStatusFromResult(ConnectivityResult result) {
//     switch (result) {
//       case ConnectivityResult.mobile:
//         return ConnectivityStatus.Cellular;
//       case ConnectivityResult.wifi:
//         return ConnectivityStatus.WiFi;
//       case ConnectivityResult.none:
//         return ConnectivityStatus.Offline;
//       default:
//         return ConnectivityStatus.Offline;
//     }
//   }
//
//   Future<bool> isNetwork() async {
//     var status = await initConnectivity();
//     // status = status as ConnectivityStatus
//
//     print('ggumbi:' + Trace.current().frames[0].location + ' ' + Trace.current().frames[0].member + '::${status}');
//     if (status == ConnectivityStatus.Offline) {
//       print('ggumbi:' + Trace.current().frames[0].location + ' ' + Trace.current().frames[0].member + '::');
//       return false;
//     }
//     print('ggumbi:' + Trace.current().frames[0].location + ' ' + Trace.current().frames[0].member + '::');
//     return true;
//   }
//
//   initConnectivity() async {
//     print('ggumbi:' + Trace.current().frames[0].location + ' ' + Trace.current().frames[0].member + '::');
//     ConnectivityResult result;
//     // Platform messages may fail, so we use a try/catch PlatformException.
//     try {
//       result = await Connectivity().checkConnectivity();
//       print('ggumbi:' + Trace.current().frames[0].location + ' ' + Trace.current().frames[0].member + '::');
//     } on PlatformException catch (e) {
//       print('ggumbi:' + Trace.current().frames[0].location + ' ' + Trace.current().frames[0].member + '::$e');
//     }
//
//     return _getStatusFromResult(result);
//   }
// }
