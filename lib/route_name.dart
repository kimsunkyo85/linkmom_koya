import 'package:linkmom/utils/custom_view/loading_page.dart';
import 'package:linkmom/view/compleat_view_page.dart';
import 'package:linkmom/view/login/find/find_checkplus_page.dart';
import 'package:linkmom/view/login/find/find_page.dart';
import 'package:linkmom/view/login/preview_page.dart';
import 'package:linkmom/view/login/register/register_id_page.dart';
import 'package:linkmom/view/login/register/register_pw_page.dart';
import 'package:linkmom/view/login/unregister/register_disable_complete_page.dart';
import 'package:linkmom/view/login/unregister/register_disable_page.dart';
import 'package:linkmom/view/login/unregister/register_disable_pw_page.dart';
import 'package:linkmom/view/login/unregister/register_disable_reason_page.dart';
import 'package:linkmom/view/main/auth_center/auth_center_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_career_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_covid19_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_crims_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_deungbon_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_educate_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_graduated_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_stress_page.dart';
import 'package:linkmom/view/main/auth_center/authcenter_stress_result_page.dart';
import 'package:linkmom/view/main/chat/chat_page.dart';
import 'package:linkmom/view/main/chat/full_photo.dart';
import 'package:linkmom/view/main/community/community_event_page.dart';
import 'package:linkmom/view/main/community/community_rank_page.dart';
import 'package:linkmom/view/main/community/community_view_page.dart';
import 'package:linkmom/view/main/community/community_write_page.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_1_page.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_2_page.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_3_page.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_4_page.dart';
import 'package:linkmom/view/main/job_apply/care/care_step_5_page.dart';
import 'package:linkmom/view/main/job_apply/guide/guide_linkmom_page.dart';
import 'package:linkmom/view/main/job_apply/guide/guide_momdady_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/linkmom_profile_page.dart';
import 'package:linkmom/view/main/job_apply/job_profile/linkmom_schedule_page.dart';
import 'package:linkmom/view/main/job_apply/parents/parents_step_2_page.dart';
import 'package:linkmom/view/main/job_apply/parents/parents_step_3_page.dart';
import 'package:linkmom/view/main/job_apply/parents/parents_step_4_page.dart';
import 'package:linkmom/view/main/job_apply/parents/parents_step_5_page.dart';
import 'package:linkmom/view/main/job_apply/parents/parents_step_6_page.dart';
import 'package:linkmom/view/main/job_apply/parents/parents_step_7_page.dart';
import 'package:linkmom/view/main/job_apply/services/care_serivces_page.dart';
import 'package:linkmom/view/main/job_apply/services/housework_serivces_page.dart';
import 'package:linkmom/view/main/job_apply/services/play_serivces_page.dart';
import 'package:linkmom/view/main/job_apply/temp_area/temp_area_view_page.dart';
import 'package:linkmom/view/main/job_list/job_filter_page.dart';
import 'package:linkmom/view/main/job_list/linkmom_list_page.dart';
import 'package:linkmom/view/main/job_list/momdady_list_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/diary_drug_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/payment/payment_expired_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/payment/payment_history_page.dart';
import 'package:linkmom/view/main/mypage/care_diary/recipe_view_page.dart';
import 'package:linkmom/view/main/mypage/cs/cs_contact_view_page.dart';
import 'package:linkmom/view/main/mypage/cs/cs_page.dart';
import 'package:linkmom/view/main/mypage/cs/cs_terms_view_page.dart';
import 'package:linkmom/view/main/mypage/mypage.dart';
import 'package:linkmom/view/main/mypage/penalty/penalty_history_page.dart';
import 'package:linkmom/view/main/mypage/review/momdaddy_review_page.dart';
import 'package:linkmom/view/main/mypage/settings_block_page.dart';
import 'package:linkmom/view/main/mypage/withdrawal/bank_list_page.dart';
import 'package:linkmom/view/main/notification_center_page.dart';
import 'package:linkmom/view/main/payment/payment_cancel_list_page.dart';
import 'package:linkmom/view/main/payment/payment_iamport_page.dart';
import 'package:linkmom/view/main/payment/payment_refund_rule_page.dart';

import 'splash_page.dart';
import 'view/login/agreement/policy_page.dart';
import 'view/login/agreement/register_agreement_page.dart';
import 'view/login/find/find_id_complete_page.dart';
import 'view/login/find/find_id_page.dart';
import 'view/login/find/find_password_page.dart';
import 'view/login/intro_page.dart';
import 'view/login/login_page.dart';
import 'view/login/password_change/password_change_complete_page.dart';
import 'view/login/password_change/password_change_page.dart';
import 'view/login/register/register_auth_page.dart';
import 'view/login/register/register_complete_page.dart';
import 'view/login/register/register_invite_page.dart';
import 'view/login/tutorial_page.dart';
import 'view/main/auth_center/authcenter_health_page.dart';
import 'view/main/auth_center/authcenter_personality_page.dart';
import 'view/main/auth_center/home_state/auth_home_page.dart';
import 'view/main/auth_center/home_state/home_state_page.dart';
import 'view/main/auth_center/home_state/nbhhome_page.dart';
import 'view/main/auth_center/home_state/ourhome_page.dart';
import 'view/main/auth_center/location/location_page.dart';
import 'view/main/chat/chat_care_info_page.dart';
import 'view/main/chat/chat_room_page.dart';
import 'view/main/job_apply/address_page.dart';
import 'view/main/job_apply/address_search_page.dart';
import 'view/main/job_apply/care/care_step_6_page.dart';
import 'view/main/job_apply/care/care_step_7_page.dart';
import 'view/main/job_apply/job_profile/matching_sign_page.dart';
import 'view/main/job_apply/job_profile/momdady_profile_page.dart';
import 'view/main/job_apply/job_profile/momdady_schedule_page.dart';
import 'view/main/job_apply/parents/parents_step_8_page.dart';
import 'view/main/job_apply/parents/parents_step_9_page.dart';
import 'view/main/job_apply/temp_area/temp_area_page.dart';
import 'view/main/main_home.dart';
import 'view/main/matchings/linkmom_apply_page.dart';
import 'view/main/matchings/linkmom_matching_page.dart';
import 'view/main/matchings/matching_filter_page.dart';
import 'view/main/matchings/matching_filter_result_page.dart';
import 'view/main/matchings/momdady_apply_page.dart';
import 'view/main/matchings/momdady_cares_page.dart';
import 'view/main/matchings/momdady_matching_page.dart';
import 'view/main/mypage/benefit/benefit_page.dart';
import 'view/main/mypage/benefit/coupon_detail_page.dart';
import 'view/main/mypage/care_diary/diary_care_list_page.dart';
import 'view/main/mypage/care_diary/diary_claim_answer_page.dart';
import 'view/main/mypage/care_diary/diary_claim_page.dart';
import 'view/main/mypage/care_diary/diary_drug_list_page.dart';
import 'view/main/mypage/care_diary/diary_filter_page.dart';
import 'view/main/mypage/care_diary/diary_filter_result_page.dart';
import 'view/main/mypage/care_diary/diary_view_page.dart';
import 'view/main/mypage/care_diary/diary_write_complete_page.dart';
import 'view/main/mypage/care_diary/diary_write_page.dart';
import 'view/main/mypage/care_diary/linkmom_diary_list_page.dart';
import 'view/main/mypage/care_diary/momdady_diary_list_page.dart';
import 'view/main/mypage/care_diary/payment/pay_recipe_page.dart';
import 'view/main/mypage/care_diary/payment/payment_filter_page.dart';
import 'view/main/mypage/care_diary/payment/payment_filter_result_page.dart';
import 'view/main/mypage/child_info/child_drug_add_page.dart';
import 'view/main/mypage/child_info/child_drug_list_page.dart';
import 'view/main/mypage/child_info/child_drug_view_page.dart';
import 'view/main/mypage/child_info/child_info_add_page.dart';
import 'view/main/mypage/child_info/child_info_page.dart';
import 'view/main/mypage/child_info/child_menu_page.dart';
import 'view/main/mypage/cs/cs_contact_page.dart';
import 'view/main/mypage/cs/cs_faq_page.dart';
import 'view/main/mypage/cs/cs_guide_page.dart';
import 'view/main/mypage/cs/cs_notice_page.dart';
import 'view/main/mypage/cs/cs_notice_view_page.dart';
import 'view/main/mypage/cs/cs_service_page.dart';
import 'view/main/mypage/cs/cs_terms_page.dart';
import 'view/main/mypage/favorite/linkmom_favorite_page.dart';
import 'view/main/mypage/favorite/momdaddy_favorite_page.dart';
import 'view/main/mypage/my_info/my_account_email_page.dart';
import 'view/main/mypage/my_info/my_account_hp_change_page.dart';
import 'view/main/mypage/my_info/my_account_hp_page.dart';
import 'view/main/mypage/my_info/my_account_manager_page.dart';
import 'view/main/mypage/my_info/my_account_pw_page.dart';
import 'view/main/mypage/my_info/my_info_page.dart';
import 'view/main/mypage/penalty/penalty_manage_page.dart';
import 'view/main/mypage/penalty/penalty_remove_page.dart';
import 'view/main/mypage/review/linkmom_review_page.dart';
import 'view/main/mypage/review/review_save_page.dart';
import 'view/main/mypage/review/review_view_page.dart';
import 'view/main/mypage/settings_noti_page.dart';
import 'view/main/mypage/settings_page.dart';
import 'view/main/mypage/withdrawal/register_account_page.dart';
import 'view/main/mypage/withdrawal/withdrawal_filter_page.dart';
import 'view/main/mypage/withdrawal/withdrawal_filter_result_page.dart';
import 'view/main/mypage/withdrawal/withdrawal_page.dart';
import 'view/main/mypage/work_noti_page.dart';
import 'view/main/notify/care_notify_page.dart';
import 'view/main/notify/community_notify_page.dart';
import 'view/main/notify/diary_notify_page.dart';
import 'view/main/notify/notify_page.dart';
import 'view/main/payment/iamport_page.dart';
import 'view/main/payment/payment_cancel_complete_page.dart';
import 'view/main/payment/payment_cancel_page.dart';
import 'view/main/payment/payment_cancel_reason_page.dart';
import 'view/main/payment/payment_cancel_select_page.dart';
import 'view/main/payment/payment_complete_page.dart';
import 'view/main/payment/payment_explain_complete_page.dart';
import 'view/main/payment/payment_explain_page.dart';
import 'view/main/payment/payment_page.dart';
import 'view/main/payment/payment_result.dart';
import 'view/main/payment/payment_test.dart';
import 'view/main/survey/survey_complete_page.dart';
import 'view/main/survey/survey_page.dart';
import 'view/main/survey/survey_question_page.dart';

///인트로 SplashPage
String routeSplash = '/$SplashPage';

///메인페이지 MainHome
String routeHome = '/$MainHome';

///로그인페이지 LoginPage
String routeLogin = '/$LoginPage';

///로딩페이지
String routeLoading = '/$LoadingPage';

///채팅
String routeChat = '/$ChatPage';

///채팅 방
String routeChatRoom = '/$ChatRoomPage';

///채팅 신청서 불러오기
String routeChatCareInfo = '/$ChatCareInfoPage';

///결제 아임포트
String routeIamPort = '/$IamPortPage';

///결제화면
String routePayMent = '/$PaymentPage';

///결제취소화면
String routePayMentCancel = '/$PaymentCancelPage';

///결제취소완료화면
String routePayMentCancelComplete = '/$PaymentCancelCompletePage';

///결제화면 아임포트
String routePayMentIamPort = '/$PaymentIamportPage';

///결제화면 결과값
String routePayMentResult = '/$PaymentResultPage';

///결제화면 테스트
String routePayMentTest = '/$PaymentTestPage';

///결제완료 페이지
String routePayMentComplete = '/$PaymentCompletePage';

///결제취소 사유 선택 화면
String routePayMentCancelReason = '/$PaymentCancelReasonPage';

///결제취소 선택 화면
String routePayMentCancelSelect = '/$PaymentCancelSelectPage';

///결제취소 소명하기 화면
String routePayMentExplain = '/$PaymentExplainPage';

///결제취소 소명하기 완료 화면
String routePayMentExplainComplete = '/$PaymentExplainCompletePage';

///결제취소 내역 리스트 화면
String routePayMentCancelList = '/$PaymentCancelListPage';

///환불규정 안내 화면
String routePayMentRefundRule = '/$PaymentRefundRulePage';

///회원가입-약관동의 페이지 AgreementPage
String routeAgreement = '/$RegisterAgreementPage';

///회원가입-회원가입 입력 페이지
String routeRegisterAuth = '/$RegisterAuthPage';

///회원가입-회원가입 완료 페이지
String routeRegisterComplete = '/$RegisterCompletePage';

///아이디/비밀번호 찾기 탭 페이지
String routeFind = '/$FindPage';

///아이디 찾기 페이지
String routeFindId = '/$FindIdPage';

///아이디 찾기 완료 페이지
String routeFindIdComplete = '/$FindIdCompletePage';

///비밀번호 찾기 페이지
String routeFindPw = '/$FindPasswordPage';

///비밀번호 변경 페이지
String routePwChange = '/$PasswordChangePage';

///비밀번호 변경 완료 페이지
String routePwChangeComplete = '/$PasswordChangeCompletePage';

///인증센터
String routeAuthCenter = '/$AuthCenterPage';

///환경인증 우리집 페이지
String routeNbhHome = '/$NbhHomeStatePage';

///환경인증 이웃집 페이지
String routeOurHome = '/$OurHomeStatePage';

///환경인증 페이지
String routeAuthHome = '/$AuthHomeStatePage';

///마이페이지-아이정보 페이지
String routeChildInfo = '/$ChildInfoPage';

///마이페이지-아이정보 추가 페이지
String routeChildInfoAdd = '/$ChildInfoAddPage';

///마이페이지-아이정보 추가 정보 페이지
// String routeChildInfoAddInfo = '/$ChildInfoAddInfoPage';

///마이페이지-나의정보 페이지
String routeMyInfo = '/$MyInfoPage';

///돌봄신청 2 - 돌봄유형선택
String routeParents2 = '/$ParentsStep2Page';

///돌봄신청 3 - 장소선택
String routeParents3 = '/$ParentsStep3Page';

///돌봄신청 4 - 이동방법
String routeParents4 = '/$ParentsStep4Page';

///돌봄신청 5 - 날짜/시간
String routeParents5 = '/$ParentsStep5Page';

///돌봄신청 6 - 추가서비스
String routeParents6 = '/$ParentsStep6Page';

///돌봄신청 7 - 신청내역
String routeParents7 = '/$ParentsStep7Page';

///돌봄신청 8 - 결제내역
String routeParents8 = '/$ParentsStep8Page';

///돌봄신청 9 - 완료
String routeParents9 = '/$ParentsStep9Page';

///보육서비스 추가
String routeCareServices = '/$CareServicesPage';

///놀이서비스 추가
String routePlayServices = '/$PlayServicesPage';

///가사서비스 추가
String routeHouseWorkServices = '/$HouseWorkServicesPage';

///맘대디(돌봄신청) 프로필
String routeMomDadyProfile = '/$MomDadyProfilePage';

///돌봄구직 1(돌봄유형)
String routeCare1 = '/$CareStep1Page';

///돌봄구직 2(보육장소)
String routeCare2 = '/$CareStep2Page';

///돌봄구직 3(이동방법)
String routeCare3 = '/$CareStep4Page';

///돌봄구직 4(날짜/시간)
String routeCare4 = '/$CareStep3Page';

///돌봄구직 5(희망근무조건)
String routeCare5 = '/$CareStep5Page';

///돌봄구직 6(등록 내역)
String routeCare6 = '/$CareStep6Page';

///돌봄구직 7(등록 완료)
String routeCare7 = '/$CareStep7Page';

///링크쌤(돌봄구직) 프로필
String routeLinkMomProfile = '/$LinkMomProfilePage';

///주소검색 행정동(시도, 시군구, 읍면동)
String routeAddress = '/$AddressPage';

///돌봄주소관리 View
String routeTempAreaView = '/$TempAreaViewPage';

///돌봄주소관리
String routeTempArea = '/$TempAreaPage';

/// Policy
String routePolicy = '/$PolicyPage';

/// Register ID
String routeRegisterId = '/$RegisterIdPage';

/// Register PW
String routeRegisterPw = '/$RegisterPwPage';

/// Register PW
String routeRegisterDisableReason = '/$RegisterDisableReasonPage';

/// Register PW
String routeRegisterDisablePw = '/$RegisterDisablePwPage';

/// Register PW
String routeRegisterDisableCompleat = '/$RegisterDisableCompletePage';

///동네인증
String routeLocation = '/$LocationPage';

///투약의뢰서
String routeChildDrugAdd = '/$ChildDrugAddPage';

///투약의뢰서 리스트
String routeChildDrugList = '/$ChildDrugListPage';

///아이선택 - 투약의뢰서
String routeChildMenu = '/$ChildMenuPage';

///아이선택 - 투약의뢰서
String routeChildDrugView = '/$ChildDrugViewPage';

///링크쌤 리스트 조회화면
String routeLinkMomList = '/$LinkMomListPage';

///맘대디 리스트 조회화면
String routeMomDadyList = '/$MomDadyListPage';

///맘대디,링크쌤 상세검색 조회화면
String routeJobFilter = '/$JobFilterPage';

///Myinfo - Account manager
String routeMyInfoAccount = '/$AccountManagerPage';

///Myinfo - Account Email
String routeMyAccountEmail = '/$MyAccountEmailPage';

///Myinfo - Account pw
String routeMyAccountPw = '/$MyAccountPwPage';

///Myinfo - Account hp
String routeMyAccountHp = '/$MyAccountHpPage';

// Mypage
String routeMyPage = '/$MyPage';

// Mypage - settings
String routeSettings = '/$SettingsPage';

// Mypage - survey
String routeSurvey = '/$SurveyPage';

// Mypage - survey start
String routeSurveyQuestion = '/$SurveyQuestionPage';

// Mypage - survey compleat
String routeSurveyCompleat = '/$SurveyCompletePage';

// Covid
String routeAuthCenterCovid19 = '/$AuthCenterCovid19Page';

// Stress
String routeAuthCenterStress = '/$AuthCenterStressPage';

// Stress result
String routeAuthCenterStressResult = '/$AuthcenterStressResultPage';

// Mypage - auth center crims
String routeAuthCenterCrims = '/$AuthCenterCrimsPage';

// 교육인증
String routeAuthCenterEducate = '/$AuthCenterEducatePage';

// 학력인증
String routeAuthCenterGraduated = '/$AuthCenterGraduatedPage';

// 경력인증
String routeAuthCenterCareer = '/$AuthCenterCareerPage';

// 등본
String routeAuthCenterDeungbon = '/$AuthCenterDeungbonPage';

// 건강인증
String routeAuthCenterHealth = '/$AuthCenterHealthPage';

// 인적성
String routeAuthCenterPersonality = '/$AuthCenterPersonaltyPage';

// 관심
String routeMypageMomdaddyFavorite = '/$MomdaddyFavoritePage';

// 차단
String routeSettingsUserBlock = '/$SettingsUserBlockPage';

// momdaddy review
String routeMypageMomdaddyReview = '/$MomdaddyReviewPage';

// linkmom review
String routeMypageLinkmomReview = '/$LinkmomReviewPage';

// save review
String routeReviewSave = '/$ReviewSavePage';

// work report
String routeMomdadyDiary = '/$MomdadyDiaryListPage';

// 관심
String routeMypageLinkmomFavorite = '/$LinkmomFavoritePage';

// 예약관리
String routeMomdadyMatchingManage = '/$MomdadyMatchingPage';

// 알림
String routeNotiSettings = '/$SettingsNotiPage';

// 예약관리 - 상세조회
String routeMatchingFilter = '/$MatchingFilterPage';

// 예약관리 - 상세조회 결과
String routeMatchingFilterResult = '/$MatchingFilterResultPage';

// 예약관리
String routeLinkmomMatchingManage = '/$LinkmomMatchingPage';

// 예약관리 - 돌봄진행상황
String routeMomdadyApply = '/$MomdadyApplyPage';

// 예약관리 - 신고하기
String routeNotify = '/$NotifyPage';

// 예약관리 - 돌봄진행상황 detail
String routeMomdadyCares = '/$MomdadyCaresPage';

// 예약관리 - 돌봄진행상황 detail
String routeLinkmomApply = '/$LinkmomApplyPage';

// 비밀번호 - NICE 인증
String routeFindCheckplus = '/$FindCheckplusPage';

// 완료페이지 - dynamic
String routeCompleteView = '/$CompleteViewPage';

// 완료페이지 - dynamic
String routeMyAccountHpChange = '/$MyAccountHpChangePage';

// 돌봄일기 - 필터
String routeReportFilter = '/$DiaryFilterPage';

// 사진 펼쳐보기
String routeFullPhoto = '/$FullPhotoPage';

// 돌봄일기 - 작성
String routeDiaryWrite = '/$DiaryWritePage';

// 돌봄일기 - 조회
String routeDiaryView = '/$DiaryViewPage';

// 돌봄일기 - 조회
String routePenaltyManage = '/$PenaltyManagePage';

// 출퇴근알림
String routeWorkNoti = '/$WorkNotiPage';

// work Diary
String routeLinkmomDiary = '/$LinkmomDiaryListPage';

// work Diary
String routeDiaryFilter = '/$DiaryFilterPage';

// work Diary
String routeDiaryFilterResult = '/$DiaryFilterResultPage';

// diary drug
String routeDiaryDrug = '/$DiaryDrugPage';

// diary drug
String routeDiaryCareList = '/$DiaryCareListPage';

// recipe page
String routeRecipe = '/$RecipeViewPage';

// recipe page
String routeClaimView = '/$DiaryClaimViewPage';

// recipe page
String routeClaimAnswer = '/$DiaryClaimAnswerPage';

// penaltyHistory
String routePenaltyHistory = '/$PenaltyHistoryPage';

// penalty remove
String routePenaltyRemove = '/$PenaltyRemovePage';

/// community view
String routeCommunityView = '/$CommunityViewPage';

/// community write
String routeCommunityWrite = '/$CommunityWritePage';

/// community event view
String routeCommunityEvent = '/$CommunityEventPage';

/// community ranking view
String routeCommunityRank = '/$CommunityRankPage';

/// cs page
String routeCs = '/$CsPage';

/// cs notice
String routeCsNotice = '/$CsNoticePage';

/// cs notice view
String routeCsNoticeView = '/$CsNoticeViewPage';

/// cs qna
String routeCsQna = '/$CsFaqPage';

/// cs service guide
String routeCsService = '/$CsServicePage';

/// cs guide
String routeCsGuide = '/$CsGuidePage';

/// cs contact us
String routeCsContact = '/$CsContactPage';

/// cs terms
String routeCsTerms = '/$CsTermsPage';

/// cs contact view
String routeCsContactView = '/$CsContactViewPage';

/// cs contact view
String routeCsTermsView = '/$CsTermsViewPage';

/// noticenter
String routeNotiCenter = '/$NotiCenterPage';

/// diary drug list
String routeDiaryDrugList = '/$DiaryDrugListPage';

/// momdady schedule
String routeMomdadySchedule = '/$MomdadySchedulePage';

/// linkom schedule
String routeLinkmomSchedule = '/$LinkmomSchedulePage';

/// 둘러보기
String routePreview = '/$PreviewPage';

/// 후기
String routeReviewView = '/$ReviewViewPage';

/// 결제내역
String routePaymentHistory = '/$PaymentHistoryPage';

/// 결제내역필터
String routePaymentFilter = '/$PaymentFilterPage';

/// 결제영수증
String routePaymentRecipe = '/$PaymentRecipePage';

/// 결제내역 필터결과
String routePaymentFilterResult = '/$PaymentFilterResultPage';

/// 결제내역 만료내역
String routePaymentExpired = '/$PaymentExpiredPage';

/// 회원탈퇴
String routeRegisterDisable = '/$RegisterDisablePage';

/// 돌봄일기 작성완료
String routeDiaryWriteComplete = '/$DiaryWriteCompletePage';

/// 계좌등록
String routeRegisterAccount = '/$RegisterAccountPage';

/// 인출내역 필터
String routeWithdrawalFilter = '/$WithdrawalFilterPage';

/// 인출내역 필터결과
String routeWithdrawalFilterResult = '/$WithdarawalFilterResultPage';

/// 인출내역
String routeWithdrawal = '/$WithdrawalPage';

/// 은행리스트
String routeBankList = '/$BankListPage';

/// Deeplink handling page, route to LinkPage()
String routeLink = '/link';

/// Tutorial
String routeTutorial = '/$TutorialPage';

/// Intro
String routeIntro = '/$IntroPage';

/// Community notify
String routeCommunityNotify = '/$CommunityNotifyPage';

/// Care notify
String routeCareNotify = '/$CareNotifyPage';

/// diary notify
String routeDiaryNotify = '/$DiaryNotifyPage';

/// matching sign
String routeMatchingSign = '/$MatchingSignPage';

/// register invite page
String routeRegisterInvite = '/$RegisterInvitePage';

/// benefits
String routeBenefit = '/$BenefitPage';

/// coupon
String routeCouponDetail = '/$CouponDetailPage';

/// guide linkmom
String routeGuideLinkmom = '/$GuideLinkMomPage';

/// guide momdady
String routeGuideMomdady = '/$GuideMomDadyPage';

/// authcenter home state view
String routeHomeState = '/$HomeStatePage';

/// address
String routeAddressSearch = '/$AddressSearchPage';
