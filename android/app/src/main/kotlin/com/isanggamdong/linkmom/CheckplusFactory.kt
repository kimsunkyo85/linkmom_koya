package com.isanggamdong.linkmom

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.webkit.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.startActivity
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.StandardMessageCodec
import io.flutter.plugin.platform.PlatformView
import io.flutter.plugin.platform.PlatformViewFactory
import io.flutter.plugins.GeneratedPluginRegistrant

class CheckPlusFactory(private var channel: ((String) -> Unit)) : PlatformViewFactory(StandardMessageCodec.INSTANCE) {

	@SuppressLint("SetJavaScriptEnabled")
	@RequiresApi(Build.VERSION_CODES.M)
	internal class CheckPlus(context: Context, id: Int, creationParams: Map<String?, Any?>?, private var checkPlusChannel: ((String) -> Unit)) : PlatformView, FlutterActivity() {
		private var view: View = LayoutInflater.from(context).inflate(R.layout.checkplus_main, null)
		private lateinit var webView : WebView
		private val CHECK_PLUS = "checkplus"
		var TAG = "CheckPlus"

		override fun getView(): View {
			return view
    }

    override fun dispose() {}

    init {
			try {
				WebView.setWebContentsDebuggingEnabled(true) // NOTE: debug enable = true

				webView = view.findViewById(R.id.webView)
				webView.requestFocus(View.FOCUS_DOWN)
				webView.settings.javaScriptEnabled = true
				webView.settings.domStorageEnabled = true
				webView.settings.javaScriptCanOpenWindowsAutomatically = true
				webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
				webView.settings.loadsImagesAutomatically = true
				webView.settings.builtInZoomControls = true
				webView.settings.loadWithOverviewMode = true
				webView.settings.useWideViewPort = true

				webView.webViewClient = CheckPlusClient(context) { value ->
					if(BuildConfig.DEBUG) Log.d(TAG, value)
				}
				webView.addJavascriptInterface(WebAppInterface(context, webView) { value ->
					if(BuildConfig.DEBUG) Log.d(TAG, value)
					checkPlusChannel(value)
				}, CHECK_PLUS)
				webView.loadUrl(creationParams?.get("URL").toString())
			} catch (e: Exception) {
				if(BuildConfig.DEBUG) Log.e(TAG, "$e")
			}
    }
	}

	override fun create(p0: Context?, p1: Int, p2: Any?): PlatformView {
		val creationParams = p2 as Map<String?, Any?>?
		return CheckPlus(p0!!, p1, creationParams, channel)
	}
}

class CheckPlusClient(viewContext: Context, private val callback: ((String) -> Unit)) : WebViewClient() {
	private var context: Context = viewContext
	var TAG = "CheckPlusClient"

	override fun onPageFinished(view: WebView?, url: String?) {
		super.onPageFinished(view, url)
		view?.evaluateJavascript("document.cookie") { value ->
			if(BuildConfig.DEBUG) Log.d(TAG, "url: $url, cookie: $value")
			view.evaluateJavascript("console.log('url: $url, cookie: $value')") {
				ret	-> if(BuildConfig.DEBUG) Log.d(TAG, "success write log")
			}
		}
		view?.evaluateJavascript("document.referrer") {
			ret	-> if(BuildConfig.DEBUG) Log.d(TAG, "referrer $ret")
			view.evaluateJavascript("console.log('referrer $ret')") {
				ret	-> if(BuildConfig.DEBUG) Log.d(TAG, "success write log")
			}
		}
		callback(url.toString())
	}

	override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
		if (url.startsWith("intent://")) {
			var intent: Intent? = null
			try {
				intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME)
				if (intent != null) {
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
					startActivity(this.context, intent, null)
				}
			} catch (e: Exception) {
				val packageName: String? = intent!!.getPackage()
				if (packageName != "") {
          intent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName"))
          if(BuildConfig.DEBUG) Log.d(TAG, "$packageName")
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
					startActivity(this.context, intent, null)
				}
        if(BuildConfig.DEBUG) Log.d(TAG, "$e")
			}
			return true
		} else if (url.startsWith("https://play.google.com/store/apps/details?id=") || url.startsWith("market://details?id=")) {
			val uri: Uri = Uri.parse(url)
			val packageName: String? = uri.getQueryParameter("id")
			if (packageName != null && packageName != "") {
				startActivity(this.context, Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")), null)
			}
			return true
		} else {
			if(BuildConfig.DEBUG) Log.d(TAG, url)
		}
		return false
	}
}

class WebAppInterface(private val mContext: Context, private val view: WebView, private val callback: ((String) -> Unit)) {
	var TAG = "WebAppInterface"
	@JavascriptInterface
	fun postMessage(data: String) {
		if(BuildConfig.DEBUG) Log.d(TAG, data)
		callback(data)
	}
}
