package com.isanggamdong.linkmom

import android.content.Intent
import io.flutter.embedding.android.FlutterActivity

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.annotation.UiThread
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import org.json.JSONObject

class MainActivity: FlutterActivity(), MethodChannel.MethodCallHandler{
  private lateinit var channel: MethodChannel
  private lateinit var checkPlusChannel: MethodChannel
  private lateinit var deeplinkChannel: MethodChannel
  private var notification = ""
  private lateinit var mResult: MethodChannel.Result
  private var CHECKPLUS_CHANNEL = "startCheckPlus"
  private var HYBRID_VIEW_TYPE = "hybrid-view-type"
  private var DEEPLINK_CHANNEL = "deeplink"
  private var mSended = false
  private var startString: String? = null

  override fun onNewIntent(intent: Intent) {
    super.onNewIntent(intent)
    if (intent.extras != null) {
      val jsonObject = JSONObject()
      val keys = intent.extras!!.keySet()

      for (key in keys) {
        jsonObject.put(key, intent.extras!!.getString(key))
      }

      channel.invokeMethod("onNotification", jsonObject.toString())
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    if (intent.extras != null) {
      val jsonObject = JSONObject()
      val keys = intent.extras!!.keySet()

      for (key in keys) {
        jsonObject.put(key, intent.extras!!.getString(key))
      }

      notification = jsonObject.toString()
    }
  }

  override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
    GeneratedPluginRegistrant.registerWith(flutterEngine)
    channel = MethodChannel(flutterEngine.dartExecutor, "FingerPushOnNotification")
    channel.setMethodCallHandler(this@MainActivity)
    checkPlusChannel = MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHECKPLUS_CHANNEL)
    checkPlusChannel.setMethodCallHandler {
      call, result ->
        if(call.method.equals(CHECKPLUS_CHANNEL)) {
          mResult = result
        }
    }
    flutterEngine
      .platformViewsController
      .registry
      .registerViewFactory(HYBRID_VIEW_TYPE, CheckPlusFactory { result ->
        run {
          runOnUiThread {
            try {
              mResult.success(result)
            } catch (e: Exception) {
              mResult.error("400", "Already sended", e)
            }
            checkPlusChannel.invokeMethod(CHECKPLUS_CHANNEL, result)
          }
        }
      })
    deeplinkChannel = MethodChannel(flutterEngine.dartExecutor, DEEPLINK_CHANNEL)
    deeplinkChannel.setMethodCallHandler { call, result ->
        if (call.method.equals(DEEPLINK_CHANNEL)) {
            if (startString != null) {
                result.success(startString)
            }
        }
    }
  }

  override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
    if (call.method.equals("onNotification")) {
      if (notification != "") {
        channel.invokeMethod("onNotification", notification)
        result.success("success")
      }
    }
  }
}