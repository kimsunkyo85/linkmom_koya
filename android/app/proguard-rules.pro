-keepparameternames
-renamesourcefileattribute SourceFile
-keepattributes InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable #소스파일, 라인 정보 유지

# Add when compile with JDK 1.7
-keepattributes EnclosingMethod

# Smart Band
-dontwarn com.partron.wearable.**

## Flutter wrapper
-keep class io.flutter.app.** { *; }
-keep class io.flutter.plugin.**  { *; }
-keep class io.flutter.util.**  { *; }
-keep class io.flutter.view.**  { *; }
-keep class io.flutter.**  { *; }
-keep class io.flutter.plugins.**  { *; }
-dontwarn io.flutter.embedding.**
-keep class com.google.firebase.** { *; }