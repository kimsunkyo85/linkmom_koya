# LinkMom
- 스토어 출시 2021.12.30 (1.0.0) 

# 2022.01.12 (1.0.2)
- API Error handled
- 채팅창 아이콘 수정
- 결제취소 팝업 추가
- Android 앱링크 버그 수정
- Firebase Analytics 추가
- 본인인증시 ‘세션이 만료되었습니다’ 에러 수정

# 2022.01.06 (1.0.1)
- 앱링크 수정
- 이모지 다양화
- 신고하기 페이지 안내 문구 추가
- 돌봄일기 작성 영역 수정
- 버그 수정

# 2021.12.30 (1.0.0)
- 테스트 모드 추가 
- Splash 에서 LoadingDlg 빠르게 더블 클릭시 해당 서버 선택
- 메인화면 "홈" 탭에서 10번 이상 클릭시 해당 서번 변경 다이얼로그 선택 추가
- 로그인화면에서 -> [로그인] 버튼 롱 클릭시 해당 아이디 선택 추가


# 2021.09.08 - iamport add

1. build.yaml 생성
flutter root 디렉토리에 build.yaml 파일을 생성하고 아래 내용을 붙여넣는다.

targets:
  $default:
    builders:
      dart_json_mapper:
          generate_for:
          # here should be listed entry point files having 'void main()' function
            - lib/main.dart

      # This part is needed to tell original reflectable builder to stay away
      # it overrides default options for reflectable builder to an **empty** set of files
      reflectable:
        generate_for:
          - no/files

2. pubspec 추가하기
plugin add
    dependencies:
        dart_json_mapper: ^2.1.2
        iamport_flutter: ^0.10.0-dev.4
    dev_dependencies:
        build_runner: ^2.0.3
    
3. 추가후 터미널에서
flutter pub run build_runner watch --delete-conflicting-outputs

4. 종료
Succeeded after 15.6s with 1 outputs (1 actions) 문구가 나오면 Ctrl+C 로 프로세스를 종료



